
blast_make=$(MAKE) -C blast

all:
	$(blast_make) all

clean doc:
	$(blast_make) $@

# Source distribution (without tests etc)

# Change this at bump
blast_version = 2.7.1

# Distribute these files in both binary and source distribs
copyright_files = COPYRIGHT LICENSE NOTICE README Release_Notes.txt

# Subdirs that contain sources (to be distcleaned)
src_src_subdirs = blast caddie cil cudd cvc3-dev/src csisat-bin tests utils smt_lib/server
# Files to be distributed
src_distrib_files = $(src_src_subdirs) $(copyright_files) Makefile
# Patterns of files and directories to be ignored when packaging a distribution
src_ignore = .* *~

# GNU find predicate to match any of the ignore patterns
m_preds = $(src_ignore:%=-o -iname '%')
matches_ignore = '(' $(wordlist 2,$(words $(m_preds)),$(m_preds)) ')'

distrib: srctgz;
srctgz: distclean
	find $(src_distrib_files) '(' -type d -a $(matches_ignore) -a -prune ')' -o '(' -type f -a '!' $(matches_ignore) -a -print0 ')' | xargs -0 pax -H -s '/^/blast-$(blast_version)\//' -z -w -f blast-$(blast_version).tgz

distclean: $(src_src_subdirs:%=%-distclean);

# Ignore return code, because if distclean has been completed, it's not applicable anymore!
%-distclean:
	-$(MAKE) -C $* distclean

# Binary distribution and installation
arch=$(shell uname -m)

# Binaries to install from blast/bin
binaries=csisat csisatServer pblast.opt smtlibServer smt_solver cvc3 ocamltune

bin_distrib_files = $(binaries:%=blast/bin/%) $(copyright_files)

bindist: all
	pax -L -s '/blast\/bin/blast-$(blast_version)\/bin/' -s '/^/blast-$(blast_version)\//' -z -w -f blast-$(blast_version)-bin-$(arch).tgz $(bin_distrib_files)

## INSTALLATION

# Copy command that resolves symlinks
inscp=cp -L

blast_bindir=$(prefix)/bin

# Special target to install binaries after BLAST was compiled.  We need to generate them first, then depend on them.
bins: $(patsubst %,$(blast_bindir)/%,$(binaries))

# Recursively install binaries after BLAST was compiled
install:
	if [ -z "$(prefix)" ] ; then echo " *** Error! Specify a prefix=/where/to/install, please!"; false; fi
	$(MAKE) bins
	@echo BLAST installed to $(prefix)

$(blast_bindir)/%: blast/bin/%
	@mkdir -p $(@D)
	$(inscp) blast/bin/$* $@
	@chmod +x $@

