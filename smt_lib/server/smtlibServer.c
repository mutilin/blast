/*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2007, The BLAST Team.
All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the authors nor their organizations 
   may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(This is the Modified BSD License, see also
 http://www.opensource.org/licenses/bsd-license.php)

The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 */

#include <sys/types.h> 
#include <stdio.h>    
#include <unistd.h>   
#include <stdlib.h>   
#include <sys/stat.h> 
#include <sys/wait.h> 
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>


#define NBSOLVER 1
//TODO why it only works with one thread ??

typedef struct {
	pthread_t thread;
	pid_t child;
	int pipe_to_solver[2];
	int pipe_from_solver[2];
} solver;

void init_solver(solver* s){
	s->child = 0;
	if(pipe(s->pipe_to_solver) != 0){
		perror("creating the pipe for child-parent communication (to child)");
		exit(-1);
	}
	if(pipe(s->pipe_from_solver) != 0){
		perror("creating the pipe for child-parent communication (from child)");
		exit(-1);
	}
}

solver solvers[NBSOLVER];

pthread_mutex_t mutex;

int init_mutex(){
	int e = pthread_mutex_init( &mutex, NULL);
	if( e != 0 ){
		perror("error: creating the pthread_mutex_t. ");
	}
	return e;
}

void destroy_mutex(){
	pthread_mutex_destroy(&mutex);
}

int counter = 0;//count the number of process created

/*do not takes care of error, just clean*/
void terminate(){
	//kill the child processes
	int i;
	for( i = 0; i < NBSOLVER; ++i){
		int child = solvers[i].child;
		if( child > 0 ){
			kill(child, SIGKILL);
		}
	}
	//destroy_mutex();
	printf("smtlibServer stopped (%d queries)\n", counter-NBSOLVER);
}

void create(solver* s){
	//printf("sleeping\n"); fflush(stdout);sleep(1);
	++counter;
	//printf("creation %d\n", counter );
	//fflush(stdout);
	init_solver(s);
	//fork + multithreading => danger
	//and the only existing thread in the child should be this one.
	s->child = fork();
	if ( s->child == 0 ){
		//child part
		dup2(s->pipe_to_solver[0], STDIN_FILENO);//redirect in
		dup2(s->pipe_from_solver[1], STDERR_FILENO);//redirect err
		dup2(s->pipe_from_solver[1], STDOUT_FILENO);//redirect stdout
		if(close(s->pipe_from_solver[0]) != 0){ perror("closing pipe"); }
		if(close(s->pipe_from_solver[1]) != 0){ perror("closing pipe"); }
		if(close(s->pipe_to_solver[0]) != 0){ perror("closing pipe"); }
		if(close(s->pipe_to_solver[1]) != 0){ perror("closing pipe"); }
		execlp("smt_solver", "smt_solver", (char*)NULL);
		perror("error during execlp ");
		terminate();
		exit(666);
	}else if (s->child == -1){
		//error part
		printf("error creating the %dth process\n", counter);
		terminate();
		exit(0);
	}else{
		//parent part
		//printf("child: %d\n", s->child);fflush(stdout);//DEBUG
		if(close(s->pipe_to_solver[0]) != 0){//close anyway, only used by the child
			perror("line: __LINE__");
		}
		if(close(s->pipe_from_solver[1]) != 0){//close anyway, only used by the child
			perror("line: __LINE__");
		}
	}
}

int forward_answer(FILE* in, FILE* out){
	//printf("reading ");fflush(stdout);//DEBUG
	char c = (char) fgetc(in);
	while(c != EOF && c != '\n'){
		if(putc(c, out) == EOF){
			perror("forwarding from SMT solver");
		}
		fflush(out);
		//printf(".");fflush(stdout);//DEBUG
		c = (char) fgetc(in);
	}
	//put a line return
	putc('\n', out);
	return 0;
}

int forward_query(FILE* in, FILE* out){
	int counter = 0;
	int stack = 0;
	int skip = 0;
	char c = (char) fgetc(in);
	while(c != EOF && !(stack == 0 && c == '\n')){
		if(putc(c, out) == EOF){
			perror("forwarding to SMT solver");
		}
		if(skip == 1){
			--skip;
		}else if(c == '('){
			++stack;
		}else if(c == ')'){
			--stack;
		}else if(c == '\\'){
			skip = 1;
		}
		++counter;
		c = (char) fgetc(in);
	}
	//put a line return
	if(putc('\n', out) == EOF){
		perror("forwarding to SMT solver");
	}
	//printf("forward_query %d\n", counter );fflush(stdout);
	return counter;
}


int white_line_counter = 0;

void* read_from_stdin(void* v){
	solver* s = (solver*) v;
	while(! feof(stdin) && white_line_counter < 10){
		//printf("sleeping\n"); fflush(stdout);sleep(1);
		//printf("bla %d\n", 1); fflush(stdout);
		create(s);
		//printf("bla %d\n", 2); fflush(stdout);
		FILE* out = fdopen( s->pipe_to_solver[1], "w");
		if( out == NULL){
			perror("opening pipe to solver");
		}
		//printf("bla %d\n", 4); fflush(stdout);
		FILE* in = fdopen( s->pipe_from_solver[0], "r");
		if( in == NULL){
			perror("opening pipe from solver");
		}
		//printf("locking\n"); fflush(stdout);
		if(pthread_mutex_lock(&mutex) != 0){
			perror("locking mutex ");
		}
		//printf("locked\n"); fflush(stdout);
		if(forward_query(stdin, out) <= 20){//benchmark + logic + formula + ...
			++white_line_counter;//not to do like clp and loop forever when blast crash
		}
		//printf("bla %d\n", 31); fflush(stdout);
		if(fclose(out) != 0){
			perror("closing pipe to solver");
		}
		//printf("bla %d\n", 5); fflush(stdout);
		forward_answer(in, stdout);
		fflush(stdout);
		//TODO syncro fin
		if(pthread_mutex_unlock(&mutex) != 0){
			perror("unlocking mutex ");
		}
		//printf("unlocked\n"); fflush(stdout);
		if(fclose(in) != 0){
			perror("closing pipe from solver");
		}
		//printf("waiting for %d\n", s->child); fflush(stdout);
		if(waitpid(s->child, NULL, 0) == -1){
			perror("waitpid failed");
		}
		//printf("wait OK for %d\n", s->child); fflush(stdout);
		s->child = 0;
	}
	return NULL;
}

void signal_handler(int sig){
	terminate();
	fprintf(stderr, "RECEIVED SIGNAL %d, exiting\n", sig);
	exit(0);
}

void sig_pipe(int sig){
	//fprintf(stderr, "RECEIVED SIGNAL %d, SIGPIPE\n", sig);
}

void setup_signal_hanlder(){
	signal(SIGQUIT, signal_handler);
	signal(SIGABRT, signal_handler);
	signal(SIGALRM, signal_handler);
	signal(SIGHUP, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGSEGV, signal_handler);
	signal(SIGPIPE, sig_pipe);
}

int main(int argc, char* argv[]){
	setup_signal_hanlder();
	init_mutex();
	//pthread_attr_t attr;
	//pthread_attr_init(&attr);
	//if(pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM) != 0){
	//	perror("unable to set thread scope");
	//}
	int i;
	for(i = 1; i < NBSOLVER; ++i){
		if(pthread_create(&(solvers[i].thread), NULL, &read_from_stdin, &(solvers[i])) != 0){
			fprintf( stderr, "failed to create thread. \n");
			terminate();
			exit(-1);
		}
	}
	read_from_stdin(&(solvers[0]));
	terminate();
	return 0;
}
