(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module gives an implementation of labeled bi-directional trees.
 * Trees created by this module have labeled nodes and labeled edges, and
 * each node has links both to its parent and to its children.  Labels can
 * be modified and children can be added to a node.  However, the parent
 * edge cannot be changed after creation. Hence this modules enforces to
 * construct trees from the root to the leaves.
 *
 * Child removal is not currently supported.
 *)


(* the node type *)
type ('a, 'b) node = {
  (* the label of this node *)
  mutable node_label  : 'a ;
  (* the edge from the parent of this node to this node *)
  mutable parent_edge : (('a, 'b) incoming_edge) option ;
  (* this node's children *)
  mutable children : (('a, 'b) node) list ;
}
(* the edge type *)
and ('a, 'b) incoming_edge = {
  (* the label of this edge *)
  mutable edge_label : 'b ;
  (* the source of this edge *)
  source     : ('a, 'b) node ;
}


(**
 * Accessor functions.
 *)
let get_node_label n = n.node_label

let get_parent_edge n = n.parent_edge

let get_children n = n.children

let get_edge_label e = e.edge_label

let get_source e = e.source

let get_parent n = 
  match (n.parent_edge) with
    None -> failwith ("get_parent called on root")
  | Some e -> e.source
	
(**
 * Label mutator functions.
 *)
let set_node_label n nl = n.node_label <- nl

let set_edge_label e el = e.edge_label <- el


(**
 * Useful predicates on nodes.
 *)
let has_parent n = n.parent_edge <> None

let has_child n = n.children <> []


(**
 * Node constructors.
 *)

(* root node creation *)
let create_root nl =
  { node_label  = nl ;
    parent_edge = None ;
    children = [] }

(* child node creation *)
let create_child nl el parent =
  let child = { node_label  = nl ;
                parent_edge = Some { edge_label = el ; source = parent } ;
                children = [] }
  in
    parent.children <- child::parent.children ;
    child
 
let delete_children parent = 
  parent.children <- []; ()

(* must be supplied a non-root node else it barfs *)
let delete_child child =
  let parent = get_parent child in
  parent.children <- Misc.list_remove parent.children child;
  child.parent_edge <- None


let rec descendants n = 
  n::(List.flatten (List.map descendants n.children))
(**
 * Iterators.
 *)
let rec iter_descendants f n =
  f n.node_label ;
  List.iter (iter_descendants f) n.children

let rec count_nodes_descendants n =
  1 + Misc.list_add (List.map count_nodes_descendants n.children)

let rec count_f_descendants f n = 
  let addnow = if (f(n)) then 1 else 0 in
    addnow +  (Misc.list_add (List.map (count_f_descendants f) n.children))

let rec iter_ancestors f n =
  f n.node_label ;
  match n.parent_edge with
      None    -> ()
    | Some(e) -> iter_ancestors f e.source

let rec fold_left_descendants f accu n =
  List.fold_left (fold_left_descendants f) (f accu n.node_label) n.children

let rec fold_right_descendants f n accu =
  f n.node_label (List.fold_right (fold_right_descendants f) n.children accu)

let rec fold_left_ancestors f accu n =
  match n.parent_edge with
      None    -> f accu n.node_label
    | Some(e) -> fold_left_ancestors f (f accu n.node_label) e.source

let rec fold_right_ancestors f n accu =
  match n.parent_edge with
      None    -> f n.node_label accu
    | Some(e) -> f n.node_label (fold_right_ancestors f e.source accu)

  
let rec node_depth node = 
  match node.parent_edge with
    None -> 1
  | Some(e) -> node_depth (e.source) + 1

let rec path_to_root node = 
  match node.parent_edge with
    None -> [node]
  | Some(e) -> (path_to_root e.source)@[node]


  
let traverse_bfs (f_leaf : ('a, 'b) node -> unit) (f_internal : ('a, 'b) node -> unit) n = 
  let rec _tr nlist = 
    match nlist with
	[] -> ()
      | h::t -> 
	  begin
	    Message.msg_string Message.Debug "In traverse";
	    if not (has_child h) then
	      begin
		(f_leaf h);
		_tr t
	      end
	    else
	      begin
		(f_internal h);
		_tr (t@ (get_children h))
	      end
	  end
  in
    _tr [n]


let rec iter_descendants_edges f n =
  let check_node = 
    match get_parent_edge n with
	None -> ()
      | Some e -> f (e.source,e.edge_label,n)
  in
    List.iter (iter_descendants_edges f) n.children

let rec iter_descendants_nodes f n = 
  f n;
  List.iter (iter_descendants_nodes f) n.children
  
  
 let rec subtree_depth n =
  match n.children with
    [] -> 1
  | l -> 1+ (Misc.list_max (List.map subtree_depth l))

let subtree_size n =
  let ctr = ref 0 in
  let _do _ = ctr := 1 + !ctr in
  iter_descendants_nodes _do n;
  !ctr
 

let output_tree_dot nl_to_string el_to_string ch n_list = 
  begin
    let visited_table = Hashtbl.create 31 in
    let rec output_node n  =
      let output_child n' =
        let e = 
          match n'.parent_edge with
          Some (e') -> e'
          | None -> failwith "not a child!"
        in
        let n1s = nl_to_string e.source.node_label in
        let n2s = nl_to_string n'.node_label in
        let  n1s = List.hd (Misc.bounded_chop n1s "\$\$" 2) in
        let  n2s = List.hd (Misc.bounded_chop n2s "\$\$" 2) in
        let es = el_to_string e.edge_label in
        Printf.fprintf ch "  %s -> %s [label=\"%s\"]\n" n1s n2s es 
      in  
      let ns = nl_to_string (get_node_label n) in
(* let _ = Message.msg_string Message.Debug ("chopping2: "^ns) in *)
      let [ns1;ns2] = Misc.bounded_chop ns "\$\$" 2 in
      let ns = Printf.sprintf "%s  [label = \"%s\"]" ns1 ns2 in
      match Hashtbl.mem visited_table n with
	true -> ()
      | _ -> 
	  begin
	    Hashtbl.add visited_table n true;
	    Printf.fprintf ch "  %s;\n" ns;
	    List.iter
	      (fun n -> output_child n; output_node n)
	      n.children
	  end
    in
    Printf.fprintf ch "digraph %s {\n" "main";
    List.iter output_node n_list;
    Printf.fprintf ch "}\n\n";
    ()
  end

