(*  
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



  (* this class defines the search strategy *)
  class virtual ['a] iterator =
    object (self)
    method virtual add_element : 'a -> unit
    method virtual add_list : 'a list -> unit
    method virtual next : unit -> 'a
    method virtual has_next : unit -> bool

    method virtual iter : ('a -> unit) -> unit
      (* to be used with map -- since there's some issue with unbound type variables ... *)
    method virtual to_list : unit -> 'a list
    method virtual sizeof : unit -> int
    method virtual empty_out : unit -> unit
    method virtual filter : ('a -> bool) -> unit
    (* return true if a bounded search that exceeded its
       bound *)
    method virtual was_bound_exceeded : unit -> bool
  end

  class ['a] dfsiterator =
  object (self)
    inherit ['a] iterator

    val mutable q = ([] : 'a list)
    method add_element e = q <- e::q
    method add_list l = List.iter (function e -> self#add_element e) l
    method next () =  match q with
        [] -> raise Not_found
      | h::t -> (q <- t ; h)

    method iter f = List.iter f q

    method to_list () = q

    method has_next () = q != []

    method sizeof () = List.length q
	
    method empty_out () = q <- []

    method filter f = 
      let oq = q in
	q <- (List.filter f oq)

    method was_bound_exceeded () = false
  end

  class ['a] bfsiterator =
    object (self)
      
    inherit ['a] iterator

    val mutable q = Queue.create () (* does this have to be mutable ?  *)
    method add_element e = 
      Queue.add e q
    method add_list l = List.iter (function e -> self#add_element e) l
	
    method iter f = Queue.iter f q

    method to_list () = (Misc.queue_to_list q)

    method next () = Queue.take q
    
    method has_next () = try Queue.peek q; true with Queue.Empty -> false

    method sizeof () = Queue.length q

    method empty_out () = Queue.clear q

    method filter f = 
      let new_q = Queue.create () in
      let mover n = if f n then Queue.add n new_q in
	Queue.iter mover q;
	Queue.clear q;
	Queue.transfer new_q q

    method was_bound_exceeded () = false
  end


 (* Iterator that performs a dfs search to a maximum depth of bound_.
  * The depth_ function should return the depth of a given tree node.
  * The depth of the root is "1".
  *)
 class ['a] bounded_dfs_iterator (bound_ : int) (depth_ : 'a -> int) =
  object (self)
    inherit ['a] iterator

    val mutable q = ([] : 'a list)
    val bound = bound_
    val depth = depth_
    val mutable exceededBound = false
    method add_element e =
      if (depth e) <= bound then
	q <- e::q
      else
	exceededBound <- true
    method add_list l = List.iter (function e -> self#add_element e) l
    method next () =  match q with
        [] -> raise Not_found
      | h::t -> (q <- t ; h)

    method iter f = List.iter f q

    method to_list () = q

    method has_next () = q != []

    method sizeof () = List.length q
	
    method empty_out () = q <- []

    method filter f = 
      let oq = q in
	q <- (List.filter f oq)

    method was_bound_exceeded () = exceededBound
  end

  class ['a] testiterator = 
    object (self)
    
    inherit ['a] iterator

    val mutable is_interesting : ('a -> bool) = (fun e -> true)

    val mutable q1 = Queue.create () 
    val mutable q2 = Queue.create () 
   
    method set_interesting f = (is_interesting <- f) ; ()

    method add_element e =
      if (is_interesting e) then (Queue.add e q1) else (Queue.add e q2)

    method add_element_high e =
      (Queue.add e q1) 

    method add_element_low e =
      (Queue.add e q2)

    method add_list l = List.iter (self#add_element) l

    method iter f =
      Queue.iter f q1 ; Queue.iter f q2
 
    method next () = 
      if not (Queue.is_empty q1) then Queue.take q1 else
      Queue.take q2

    method has_next () = 
      not (Queue.is_empty q1 ) || not (Queue.is_empty q2)

    method sizeof () =
      (Queue.length q1) + (Queue.length q2)

    method empty_out () =
      (Queue.clear q1); (Queue.clear q2)

    method filter f = failwith "unimplemented : testiterator filter"

    method to_list () = failwith "unimplemented : testiterator to_list"

    method was_bound_exceeded () = false

  end
	
	
  class ['a] joiniterator = 
    object (self)
    
    inherit ['a] iterator

    val mutable is_interesting : ('a -> bool) = (fun e -> true)

    val mutable q1 = Queue.create () 
    val mutable q2 = Stack.create () 
   
    method set_interesting f = (is_interesting <- f) ; ()

    method add_element e =
      if (is_interesting e) then (Queue.add e q1) else (Stack.push e q2)

    method add_element_high e =
      (Queue.add e q1) 

    method add_element_low e =
      (Stack.push e q2)

    method add_list l = List.iter (self#add_element) l

    method iter f =
      Queue.iter f q1 ; Stack.iter f q2
 
    method next () = 
      if not (Queue.is_empty q1) then Queue.take q1 else Stack.pop q2 

    method has_next () = 
      not (Queue.is_empty q1 ) || not (Stack.is_empty q2)

    method sizeof () =
      (Queue.length q1) + (Stack.length q2)

    method empty_out () =
      (Queue.clear q1); (Stack.clear q2)

    method filter f = failwith "unimplemented : joiniterator filter"

    method to_list () = failwith "unimplemented : joiniterator to_list"

    method was_bound_exceeded () = false

  end
	
	
