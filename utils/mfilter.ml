
let write_list_to_file fname string_list =
  let oc = open_out fname in
    List.iter (output_string oc) string_list;
    close_out oc
;;

(* ambitious -- take a text file and return a list of lines that is the file *)
let string_list_of_file fname = 
  let ic  = open_in fname in
  let doneflag = ref false in
  let listoflines = ref [] in
    while not (!doneflag) do
      try
	listoflines := (input_line ic)::(!listoflines)
      with
	  End_of_file -> doneflag := true
    done;
    close_in ic;
  List.rev !listoflines
;;

let allblank s = 
let rec _allblank i = 
	if i = String.length s then true
	else if String.get s i <> ' ' then false
	else _allblank (i+1)
in
	_allblank 0
;;


let f s = 
	if allblank s then false 
	else not ((String.get s 0 = '#') || (String.get s 0 = '/' && String.get
        s 1 = '/' && String.get s 2 = '#'))
;;
  
let main () = 
  let fname = Sys.argv.(1) in
  let lines = string_list_of_file fname in
  let lines' = List.filter f lines in
	List.iter (fun x -> output_string stdout (x^"\n")) lines'
(*    (write_list_to_file fname lines') *)
;;

main ()
