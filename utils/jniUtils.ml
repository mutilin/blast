open Jni

let com = call_object_method
let gmi = get_methodID
let cbm = call_boolean_method
let cvm = call_void_method
let csom = call_static_object_method
let csbm = call_static_boolean_method
let gsmi = get_static_methodID
let cim = call_camlint_method

class hashtable =
object
  val sclass = find_class "java/util/Hashtable"
  val sobj = alloc_object (find_class "java/util/Hashtable")
  method init size =
    call_void_method sobj (get_methodID sclass "<init>" "(I)V") [| Camlint size |]
  method containsKey b =
    call_boolean_method sobj (get_methodID sclass  "containsKey" "(Ljava/lang/Object;)Z") [| Obj b |]
  method get b =
    call_object_method sobj (get_methodID sclass "get" "(Ljava/lang/Object;)Ljava/lang/Object;") [| Obj b |]
  method put b loc =
    call_object_method sobj (get_methodID sclass "put" "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;")
                                                                                       [| Obj b ; Obj loc |]
end  

module JList =
struct
  let sclass = find_class "java/util/List"
  let iterator obj =
    com obj (gmi sclass "iterator" "()Ljava/util/Iterator;") [| |]
  let size obj =
    cim obj (gmi sclass "size" "()I") [||]
end

module JVector =
struct
  let sclass = find_class "java/util/Vector"

  let init obj =
    cvm obj (gmi sclass "<init>" "()V") [||]
  let add obj element =
    cbm obj (gmi sclass "add" "(Ljava/lang/Object;)Z") [|Obj element|]
end
  
module Collection =
struct
  let sclass = find_class "java/util/Collection"

  let iterator obj =
    com obj (gmi sclass "iterator" "()Ljava/util/Iterator;") [||]
end
  
module Integer =
struct
  let sclass = find_class "java/lang/Integer"

  let intValue obj =
    cim obj (gmi sclass "intValue" "()I") [||]
  let init obj intval =
    com obj (gmi sclass "<init>" "(I)V") [| Camlint intval |]

end

module JIterator =
struct
  let sclass = find_class "java/util/Iterator"

  let next obj =
    com obj (gmi sclass "next" "()Ljava/lang/Object;") [||]
  let hasNext obj =
    cbm obj (gmi sclass "hasNext" "()Z") [| |]
end
  
let print_java_class o =
  let jcls_object = find_class "java/lang/Object" in
  let jcls_class = find_class "java/lang/Class" in
  let cls = call_object_method o (get_methodID jcls_object "getClass" "()Ljava/lang/Class;" ) [| |] in
  let name = string_from_java 
      (call_object_method cls (get_methodID jcls_class "getName" "()Ljava/lang/String;") [| |] ) in
  print_string "Class " ;
  print_string name ;
  print_newline()

let jcls_java_util_List = find_class "java/util/List" ;;
let jcls_java_util_Iterator = find_class "java/util/Iterator" ;;

(** create a caml list of objects from a Java List.
   The function is written in a stateful style, the state is maintained in
   the iterator, so _buildlist does not require an argument.
   
   Not tail recursive though! Does the compiler optimize this?
*)
let javaList_to_ocaml_list jl =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let rec _buildlist () =
    if (call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) then
      let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
      o :: (_buildlist ())
    else
      []
  in
  _buildlist ()

let ocaml_list_to_javaList cl =
  let vectorc = find_class "java/util/Vector"
  in let vectoro = alloc_object vectorc
  in
    call_void_method vectoro (get_methodID vectorc "<init>" "()V") [||];
    let add obj = call_void_method vectoro (get_methodID vectorc "add" "(Ljava/lang/Object;)V") [|Obj obj|]
    in
      List.iter add cl;
      vectoro

let javaList_length jl =
  call_camlint_method jl (get_methodID jcls_java_util_List "size" "()I") [||]

let javaList_hd jl =
  call_object_method jl (get_methodID jcls_java_util_List "get" "(I)Ljava/lang/Object;") [| Camlint 0 |]

let javaList_mem jl o =
  call_boolean_method jl (get_methodID jcls_java_util_List "contains" "(Ljava/lang/Object;)Z") [| Obj o |]


let javaList_find (f: obj -> bool)  jl (* : java.util.List *) : obj =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let found = ref false in
  let obj = ref Jni.null in
  while (not (!found) && call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    if f o then begin found := true ; obj := o end 
  done ;
  if !found then !obj else raise Not_found

let javaList_iter (f: obj -> unit)  jl (* : java.util.List *) : unit =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  while (call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    f o
  done ;
  ()

let javaList_map (f: obj -> 'a ) jl (* : java.util.List *) : 'a list =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let alist = ref [] in
  while (call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    alist := (f o) :: !alist
  done ;
  List.rev !alist

let javaList_fold_left  (f: 'a -> obj -> 'a  ) init  jl (* : java.util.List *) init : 'a =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let b = ref init in
  while (call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    b := (f !b o) 
  done ;
  !b

let javaList_exists (f: obj -> bool) jl =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let flag = ref false in
  while (not (!flag) && call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    if f o then flag := true else ();
  done ;
  !flag

let javaList_forall (f: obj -> bool) jl =
  let iterator = call_object_method jl (get_methodID jcls_java_util_List "iterator" "()Ljava/util/Iterator;") [||] in
  let flag = ref true in
  while ((!flag) && call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    if not (f o) then flag := false else ();
  done ;
  !flag

let chain_iter (f : obj -> unit) jl =
  let jcls_soot_util_Chain = find_class "soot/util/Chain" in
  let iterator = call_object_method jl (get_methodID jcls_soot_util_Chain "iterator" "()Ljava/util/Iterator;") [||] in
  while (call_boolean_method iterator (get_methodID jcls_java_util_Iterator "hasNext" "()Z") [||]) do
    let o = call_object_method iterator (get_methodID jcls_java_util_Iterator "next" "()Ljava/lang/Object;") [||] in
    f o
  done ;
  ()

let construct_object_array cls camlarray =
  let l = Array.length camlarray in
  let oarray = new_object_array l cls in
  for i = 0 to l-1 do
    set_object_array_element oarray i camlarray.(i)
  done ;
  oarray
