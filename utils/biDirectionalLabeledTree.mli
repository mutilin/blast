(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module gives an implementation of labeled bi-directional trees.
 * Trees created by this module have labeled nodes and labeled edges, and
 * each node has links both to its parent and to its children.  Labels can
 * be modified and children can be added to a node.  However, the parent
 * edge cannot be changed after creation. Hence this modules enforces to
 * construct trees from the root to the leaves.
 *
 * Child removal is not currently supported.
 *)


(** the node type *)
type ('a, 'b) node

(** the edge type *)
and  ('a, 'b) incoming_edge

(**
 * {b Accessor functions.}
 *)

val get_node_label  : ('a, 'b) node -> 'a

val get_parent_edge : ('a, 'b) node -> ('a, 'b) incoming_edge option

val get_children    : ('a, 'b) node -> ('a, 'b) node list

val get_edge_label  : ('a, 'b) incoming_edge -> 'b

val get_source      : ('a, 'b) incoming_edge -> ('a, 'b) node

val get_parent : ('a, 'b) node -> ('a, 'b) node
(**
 * {b Label mutator functions.}
 *)

val set_node_label : ('a, 'b) node -> 'a -> unit

val set_edge_label : ('a, 'b) incoming_edge -> 'b -> unit


(**
 * {b Useful predicates on nodes.}
 *)

(** return true if node has a parent *)
val has_parent : ('a, 'b) node -> bool

(** return true if node has at least one child *)
val has_child  : ('a, 'b) node -> bool


(**
 * {b Node constructors.}
 *)

(** root node creation *)
val create_root  : 'a -> ('a, 'b) node

(** child node creation *)
val create_child : 'a -> 'b -> ('a, 'b) node -> ('a, 'b) node

val delete_children : ('a, 'b) node -> unit

val delete_child : ('a, 'b) node -> unit

val descendants : ('a, 'b) node -> ('a, 'b) node list
(**
 * {b Iterators.}
 *)

val iter_descendants : ('a -> unit) -> ('a, 'b) node -> unit

val iter_descendants_edges :
  ((('a, 'b) node * 'b * ('a, 'b) node) -> unit) 
  -> ('a, 'b) node -> unit 
    
val iter_descendants_nodes :
  ((('a, 'b) node ) -> unit) -> ('a, 'b) node -> unit 


val iter_ancestors   : ('a -> unit) -> ('a, 'b) node -> unit

val fold_left_descendants  : ('c -> 'a -> 'c) -> 'c -> ('a, 'b) node -> 'c

val fold_left_ancestors    : ('c -> 'a -> 'c) -> 'c -> ('a, 'b) node -> 'c

val fold_right_descendants : ('a -> 'c -> 'c) -> ('a, 'b) node -> 'c -> 'c

val fold_right_ancestors   : ('a -> 'c -> 'c) -> ('a, 'b) node -> 'c -> 'c

val count_nodes_descendants : ('a, 'b) node -> int

val count_f_descendants : (('a, 'b) node -> bool) -> ('a, 'b) node -> int

val node_depth : ('a, 'b) node -> int

val path_to_root : ('a, 'b) node -> ('a, 'b) node list

val subtree_depth : ('a, 'b) node -> int

val subtree_size : ('a, 'b) node -> int

val traverse_bfs : 
  (('a, 'b) node -> unit) -> 
    (('a, 'b) node -> unit) -> 
      ('a, 'b) node -> unit

val output_tree_dot : ('a -> string) -> ('b -> string ) -> out_channel -> ('a,'b) node list -> unit


