(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(**
  Process command line options.

   Build database with
     buildOptionsDatabase : unit -> unit

   Values of flags accessible with
	isSet : string -> bool
  and
	getValueOf : valueType -> valueType
  and its incarnations.

  Note: values of all types must be added to the database with
   their default values.
   To get the value of an option, query the database with
   the corresponding command line option, e.g., query "L"
   to get the value of the reachLabel

*)

type valueType =
  Bool of bool
| String of string
| Int of int
| Float of float
| StringList of string list
| IntList of int list


exception OptionNotFoundException of string


let optionsDatabase = Hashtbl.create 13

(*** Command line arguments and global variables to be set ***)
let _ = Hashtbl.add optionsDatabase "L" (String "ERROR")
let _ = Hashtbl.add optionsDatabase "funlabel" (String "")
let _ = Hashtbl.add optionsDatabase "interactive" (Bool false)
let _ = Hashtbl.add optionsDatabase "msvc" (Bool false)
let _ = Hashtbl.add optionsDatabase "dmod" (Bool false)
(* O stands for optimization level. Default is 0 (no optimizations).
   Optimization level 1 : do cone of influence slicing.
   Optimization level 2 : do partial evaluation (see also -pe)
                          and cfg compaction. Note that cfg compaction
                          can add infeasible paths in the program, so
                          you can end up with infeasible error traces.
*)
let _ = Hashtbl.add optionsDatabase "O" (Int 0)
let _ = Hashtbl.add optionsDatabase "bvcons" (Int 0)
let _ = Hashtbl.add optionsDatabase "main" (StringList [ "main" ])
let _ = Hashtbl.add optionsDatabase "pta" (String "noflow")
let _ = Hashtbl.add optionsDatabase "pred" (String "")
let _ = Hashtbl.add optionsDatabase "shape-class" (String "")
let _ = Hashtbl.add optionsDatabase "par" (Bool false)
let _ = Hashtbl.add optionsDatabase "init" (String "")
let _ = Hashtbl.add optionsDatabase "error" (String "")
let _ = Hashtbl.add optionsDatabase "query" (String "")
let _ = Hashtbl.add optionsDatabase "ax" (String "")
let _ = Hashtbl.add optionsDatabase "custom" (String "")
let _ = Hashtbl.add optionsDatabase "refine" (String "fwd")
let _ = Hashtbl.add optionsDatabase "subgoal" (String "")
let _ = Hashtbl.add optionsDatabase "spec_fun_pref" (String "__BLAST_")
let _ = Hashtbl.add optionsDatabase "spec_arg_pref" (String "__BLAST_PARAM_")
let _ = Hashtbl.add optionsDatabase "quiet" (Bool false)
let _ = Hashtbl.add optionsDatabase "nofp" (Bool true)
let _ = Hashtbl.add optionsDatabase "par" (Bool false)
let _ = Hashtbl.add optionsDatabase "initialize" (Bool true)
let _ = Hashtbl.add optionsDatabase "simplemem" (Bool true)
let _ = Hashtbl.add optionsDatabase "simplify" (Bool false)
let _ = Hashtbl.add optionsDatabase "pe" (Bool false)
let _ = Hashtbl.add optionsDatabase "pf" (Bool false)
let _ = Hashtbl.add optionsDatabase "pfgen" (Bool false)
let _ = Hashtbl.add optionsDatabase "pffile" (String "/tmp/lf.pfs")
let _ = Hashtbl.add optionsDatabase "forget" (Bool false)
(*let _ = Hashtbl.add optionsDatabase "killLoc" (String "_BLAST_KILL_LOCATION")*)
let _ = Hashtbl.add optionsDatabase "killLoc" (String "")
let _ = Hashtbl.add optionsDatabase "killFile" (String "")
let _ = Hashtbl.add optionsDatabase "forget" (Bool false)
let _ = Hashtbl.add optionsDatabase "cfa-dot" (String "")
let _ = Hashtbl.add optionsDatabase "trace-dot" (String "")
let _ = Hashtbl.add optionsDatabase "tree-dot" (String "")
let _ = Hashtbl.add optionsDatabase "callgraph" (String "")
let _ = Hashtbl.add optionsDatabase "nocache" (Bool false)
let _ = Hashtbl.add optionsDatabase "reclaim" (Bool false)
let _ = Hashtbl.add optionsDatabase "cov" (Bool false)
let _ = Hashtbl.add optionsDatabase "cover" (String "")
let _ = Hashtbl.add optionsDatabase "covercheck" (String "single")


let _ = Hashtbl.add optionsDatabase "stop" (Bool false)
let _ = Hashtbl.add optionsDatabase "norefine" (Bool false)
let _ = Hashtbl.add optionsDatabase "bfs" (Bool true)
let _ = Hashtbl.add optionsDatabase "dfs" (Bool false)
let _ = Hashtbl.add optionsDatabase "bnddfs" (Bool false)
let _ = Hashtbl.add optionsDatabase "depth" (Int 1)
let _ = Hashtbl.add optionsDatabase "fdepth" (Int 0)
(* Setting default to 0, as -iclos (default) will take care of it. *)
let _ = Hashtbl.add optionsDatabase "cldepth" (Int 0) (* -1 indicates full unroll - modulo recursion *)

let _ = Hashtbl.add optionsDatabase "ignoredepthfor" (String "")
let _ = Hashtbl.add optionsDatabase "important-labels" (StringList [])
let _ = Hashtbl.add optionsDatabase "important-attrs" (StringList [])
let _ = Hashtbl.add optionsDatabase "inline-attrs" (StringList [])
let _ = Hashtbl.add optionsDatabase "enablerecursion" (Bool false)
let _ = Hashtbl.add optionsDatabase "ecov" (Bool false)
let _ = Hashtbl.add optionsDatabase "dsq" (Bool false) (* dotted squish *)
let _ = Hashtbl.add optionsDatabase "atc" (Bool false) (* atomic check *)
let _ = Hashtbl.add optionsDatabase "alt" (Bool false) (* atomic check *)
let _ = Hashtbl.add optionsDatabase "csq" (Bool false) (* dotted squish *)
let _ = Hashtbl.add optionsDatabase "tc2" (Bool false) (* dotted squish *)
let _ = Hashtbl.add optionsDatabase "exit" (Bool false) (* track _EXIT in tproj*)
let _ = Hashtbl.add optionsDatabase "losstrans" (Bool false)
let _ = Hashtbl.add optionsDatabase "tg" (Bool false)
let _ = Hashtbl.add optionsDatabase "constpred" (Bool false)
let _ = Hashtbl.add optionsDatabase "lvc" (Bool true)
(* dotted squish -- false was supposed to be faster but f-it *)
let _ = Hashtbl.add optionsDatabase "gpred" (Bool false) (* use global preds globally *)
let _ = Hashtbl.add optionsDatabase "bddmin" (Bool true) (* default! *)
let _ = Hashtbl.add optionsDatabase "bddcov" (Bool true) (* default! *)
let _ = Hashtbl.add optionsDatabase "bddsat" (Bool true) (* default! *)
let _ = Hashtbl.add optionsDatabase "lvframe" (Bool true) (* default! *)

let _ = Hashtbl.add optionsDatabase "scope" (Bool false)
let _ = Hashtbl.add optionsDatabase "dc" (Bool false)
let _ = Hashtbl.add optionsDatabase "restart" (Bool false)
let _ = Hashtbl.add optionsDatabase "cref" (Bool false)
let _ = Hashtbl.add optionsDatabase "wpsat" (Bool false)
let _ = Hashtbl.add optionsDatabase "auto" (String "")
let _ = Hashtbl.add optionsDatabase "simpquery" (Bool false)
let _ = Hashtbl.add optionsDatabase "uif" (Bool false)
(*
let _ = Hashtbl.add optionsDatabase "allPreds" (Bool false)
*)
let _ = Hashtbl.add optionsDatabase "cg" (Bool false)
let _ =  Hashtbl.add optionsDatabase "mod" (Bool false)
let _ = Hashtbl.add optionsDatabase "modopt" (Bool true)
let _ = Hashtbl.add optionsDatabase "fldunroll" (Bool true)

(* the first thing to do is in main is to set this to true if ... *)
(* Possible value of predH
   0 : just the vanilla thing where it extracts predicates from proofs
   1 : every time it adds a predicate p, it adds all the constant predicates associated with it
   2 : does the above AND calls find_all_predicates if a trace fails
*)

let _ = Hashtbl.add optionsDatabase "traces" (Bool false)
let _ = Hashtbl.add optionsDatabase "predH" (Int 7)
let _ = Hashtbl.add optionsDatabase "block" (Bool true)
let _ = Hashtbl.add optionsDatabase "craig" (Int 2)

(* Use CSIsat as default. *)
let _ = Hashtbl.add optionsDatabase "itp" (String "csisat")

let _ = Hashtbl.add optionsDatabase "bset" (Int 8)
let _ = Hashtbl.add optionsDatabase "localrestart" (Bool false)
let _ = Hashtbl.add optionsDatabase "tsd" (String "")

(* the craig option is only useful if you have chosen block *)

(* Options relating to dumping traces *)
let _ = Hashtbl.add optionsDatabase "tracefile" (String "error.btr")
let _ = Hashtbl.add optionsDatabase "plaintracefile" (String "")
let _ = Hashtbl.add optionsDatabase "file" (String "")
let _ = Hashtbl.add optionsDatabase "demo" (Bool false)
let _ = Hashtbl.add optionsDatabase "loadabs" (String "")
let _ = Hashtbl.add optionsDatabase "dumpabs" (String "")

let _ = Hashtbl.add optionsDatabase "loadtest" (Bool false)
let _ = Hashtbl.add optionsDatabase "skiptest" (Int 0)
let _ = Hashtbl.add optionsDatabase "selecttest" (Bool false)
let _ = Hashtbl.add optionsDatabase "checktest" (IntList [])
let _ = Hashtbl.add optionsDatabase "qid" (IntList [])
let _ = Hashtbl.add optionsDatabase "edgemode" (Bool true)
let _ = Hashtbl.add optionsDatabase "funmode" (Int 0)
let _ = Hashtbl.add optionsDatabase "timeout" (Int 0)
let _ = Hashtbl.add optionsDatabase "iter-bound" (Int 0)
let _ = Hashtbl.add optionsDatabase "testfile" (String "test.cfa")

let _ = Hashtbl.add optionsDatabase "scheck" (Bool false)
let _ = Hashtbl.add optionsDatabase "incref" (Bool true)

let _ = Hashtbl.add optionsDatabase "incmod" (Bool true)
let _ = Hashtbl.add optionsDatabase "recompmod" (Bool true)
let _ = Hashtbl.add optionsDatabase "nomod" (Bool false)

let _ = Hashtbl.add optionsDatabase "clock" (Bool false)
let _ = Hashtbl.add optionsDatabase "fociUF" (Bool false)
let _ = Hashtbl.add optionsDatabase "fociminp" (Bool false)
let _ = Hashtbl.add optionsDatabase "multi" (Int 0)
let _ = Hashtbl.add optionsDatabase "efilter" (Bool false)

 (* Possible algorithms for post.
   Options are
   1. H -- for the DasDillPark algorithm
   2. slam -- for the SLAM-like algorithm with no optimizations
   3. slam1 -- slam + basic block optimizations
   4. slam2 -- slam1 + branch optimizations
   5. slam-dumb -- slam ignoring branches
*)
let _ = Hashtbl.add optionsDatabase "post" (String "slam2")
let _ = Hashtbl.add optionsDatabase "comp" (String "cut")

let _ = Hashtbl.add optionsDatabase "checkRace" (String "")
let _ = Hashtbl.add optionsDatabase "racevar" (String "")
let _ = Hashtbl.add optionsDatabase "simpres" (Int 0)
(* restart simplify every now and then ... *)

let _ = Hashtbl.add optionsDatabase "bccscc" (Bool false)
let _ = Hashtbl.add optionsDatabase "skipfun" (Bool false)
let _ = Hashtbl.add optionsDatabase "bmc" (Bool false)
let _ = Hashtbl.add optionsDatabase "depthoffset" (Int 5)
let _ = Hashtbl.add optionsDatabase "errlv" (Bool false)


let _ = Hashtbl.add optionsDatabase "skipcheck" (Bool false)
let _ = Hashtbl.add optionsDatabase "errorseedfun" (String "")(*internal option*)
let _ = Hashtbl.add optionsDatabase "exitDepth" (Int 1)
let _ = Hashtbl.add optionsDatabase "nomp" (Bool false)

let _ = Hashtbl.add optionsDatabase "omega" (Int 0)

let _ = Hashtbl.add optionsDatabase "maxcounter" (Int 1)
let _ = Hashtbl.add optionsDatabase "finitecounter" (Bool false)
let _ = Hashtbl.add optionsDatabase "events" (Bool false)
let _ = Hashtbl.add optionsDatabase "sets" (Bool false)
let _ = Hashtbl.add optionsDatabase "getfunsize" (String "__BLAST_DISPATCH_FUNCTION")(*internal option*)

(** What lattices to use.
	Options are Const, <more here>
 *)
let _ = Hashtbl.add optionsDatabase "include-lattice" (StringList [])
let _ = Hashtbl.add optionsDatabase "nolattice" (Bool true)

(* Possible theorem provers that can be used.
   Options are
   1. ICS
   2. Vampyre
   3. Simplify
   4. smt
   The default is simplify
*)

let _ = Hashtbl.add optionsDatabase "s" (String "smt") (* use as the "postdp" *)
let _ = Hashtbl.add optionsDatabase "smtuf" (Bool false)
let _ = Hashtbl.add optionsDatabase "ufannot" (Bool true)

let _ = Hashtbl.add optionsDatabase "bits" (Int 4)

(* What kind of analysis to do: single-threaded (look for an ERROR label), or
   multi-threaded race analysis (look for unprotected variable accesses) *)

let _ = Hashtbl.add optionsDatabase "checkRace" (String "")
let _ = Hashtbl.add optionsDatabase "dotsep" (Bool false)
let _ = Hashtbl.add optionsDatabase "test" (Bool false)
let _ = Hashtbl.add optionsDatabase "ccured" (Bool false)

let _ = Hashtbl.add optionsDatabase "qf" (String "")
let _ = Hashtbl.add optionsDatabase "qcfile" (String "")
let _ = Hashtbl.add optionsDatabase "qc" (Bool true)
let _ = Hashtbl.add optionsDatabase "cf" (Bool false)
let _ = Hashtbl.add optionsDatabase "cfb" (Bool false)
let _ = Hashtbl.add optionsDatabase "reachloc" (Bool false)
let _ = Hashtbl.add optionsDatabase "par" (Bool false)
let _ = Hashtbl.add optionsDatabase "sumcache" (Bool true)
let _ = Hashtbl.add optionsDatabase "inv" (String "")
let _ = Hashtbl.add optionsDatabase "alias" (String "bdd")
let _ = Hashtbl.add optionsDatabase "talias" (Bool false)
let _ = Hashtbl.add optionsDatabase "nomusts" (Bool true)
let _ = Hashtbl.add optionsDatabase "blastmust" (Bool false)
let _ = Hashtbl.add optionsDatabase "aiter" (Bool false)
	  (* the alias file containing a list of alias pairs *)
let _ = Hashtbl.add optionsDatabase "aliasfile" (String "")
let _ = Hashtbl.add optionsDatabase "easm" (String "") (* the env. assumptions file *)


let _ = Hashtbl.add optionsDatabase "interface" (String "")
let _ = Hashtbl.add optionsDatabase "component" (String "")


let _ = Hashtbl.add optionsDatabase "posp" (Bool false)
let _ = Hashtbl.add optionsDatabase "ti" (Bool false)
let _ = Hashtbl.add optionsDatabase "fmc" (Bool false)

let _ = Hashtbl.add optionsDatabase "focisymm" (Bool false)
let _ = Hashtbl.add optionsDatabase "lazysymm" (Bool false)
(*let _ = Hashtbl.add optionsDatabase "dasdill" (Bool false) (* only use with focisymm! *)*)
let _ = Hashtbl.add optionsDatabase "fmcpa" (Bool false)
let _ = Hashtbl.add optionsDatabase "slicesymm" (Bool false)

let _ = Hashtbl.add optionsDatabase "invbdd" (Bool false) (* only use with focisymm! *)
let _ = Hashtbl.add optionsDatabase "fqdump" (Bool false)

let _ = Hashtbl.add optionsDatabase "multistr" (Bool false)
let _ = Hashtbl.add optionsDatabase "trelc" (Int 1)

let _ = Hashtbl.add optionsDatabase "bddpost" (Bool false)
let _ = Hashtbl.add optionsDatabase "hybrid" (Bool false)
let _ = Hashtbl.add optionsDatabase "bddprint" (Bool false) (* print bdds during debug etc. *)

let _ = Hashtbl.add optionsDatabase "focimax" (Int (-1))

let _ = Hashtbl.add optionsDatabase "fmcc" (Bool false)
let _ = Hashtbl.add optionsDatabase "blockfmc" (Bool false)
let _ = Hashtbl.add optionsDatabase "regcons" (Bool false)

let _ = Hashtbl.add optionsDatabase "trunc" (Int 50)

let _ = Hashtbl.add optionsDatabase "lockset" (Bool false)

let _ = Hashtbl.add optionsDatabase "checktree" (Bool false)
let _ = Hashtbl.add optionsDatabase "showtree" (Bool false)
let _ = Hashtbl.add optionsDatabase "savetree" (Bool false)
let _ = Hashtbl.add optionsDatabase "exitvar" (String "_EXIT")
let _ = Hashtbl.add optionsDatabase "errorvar" (String "__BLAST_error")

let _ = Hashtbl.add optionsDatabase "tproj" (Bool false)
let _ = Hashtbl.add optionsDatabase "dom"  (Bool false)
let _ = Hashtbl.add optionsDatabase "tpbug" (Bool false)
let _ = Hashtbl.add optionsDatabase "projfun" (Bool false)
let _ = Hashtbl.add optionsDatabase "itpn" (Bool false)
let _ = Hashtbl.add optionsDatabase "timezero" (Bool false)
let _ = Hashtbl.add optionsDatabase "mccarthy" (Bool false)
let _ = Hashtbl.add optionsDatabase "rp" (Bool true)
let _ = Hashtbl.add optionsDatabase "prop" (Bool true)
let _ = Hashtbl.add optionsDatabase "preqfilter" (Bool false)
let _ = Hashtbl.add optionsDatabase "incdp" (String "smt")
let _ = Hashtbl.add optionsDatabase "ubdp" (String "simplify")

(* temporary var for more lvals discovery *)
let _ = Hashtbl.add optionsDatabase "nostaticalias" (Bool false)

(* temporary var for more lvals discovery *)
let _ = Hashtbl.add optionsDatabase "aliasclos" (Bool false)

(* to print alias statistics *)
let _ = Hashtbl.add optionsDatabase "aliasstat" (Bool false)

(* temporary var for more lvals discovery *)
let _ = Hashtbl.add optionsDatabase "const" (Bool false)

(* Add stuff to options database *)
let _ = Hashtbl.add optionsDatabase "nosserr" (Bool false)

(* enable vardec (disabled now by default *)
let _ = Hashtbl.add optionsDatabase "vardec" (Bool false)

(* ignore duplicated functions *)
let _ = Hashtbl.add optionsDatabase "ignoredupfn" (Bool false)

(* Special debugging mode for developers *)
let _ = Hashtbl.add optionsDatabase "devdebug" (Bool false)

(* Calculare system time in addition to user time *)
let _ = Hashtbl.add optionsDatabase "systime" (Bool false)

(* Do not normalize predicates in BLAST, leave it to solvers *)
let _ = Hashtbl.add optionsDatabase "skipnorm" (Bool true)

(* Do not use built-in profiler *)
let _ = Hashtbl.add optionsDatabase "noprofile" (Bool false)

(* turns coverage checks off *)
let _ = Hashtbl.add optionsDatabase "devnocov" (Bool false)

(* Do not use fixed useful-blocks algorithm for SMT*)
let _ = Hashtbl.add optionsDatabase "nosmtgub" (Bool false)

(* Display unaccounted profile time above this threshold *)
let _ = Hashtbl.add optionsDatabase "unaccprof" (Int 20)

(* Incremental alias analysis options *)
(* Whether incremental analysis should be carried on *)
let _ = Hashtbl.add optionsDatabase "ialias" (Bool true)
(* Whether incremental analysis should be used instead of the usual *)
let _ = Hashtbl.add optionsDatabase "use-ialias" (Bool true)

(* Incremental closure options *)
(* Whether incremental closure should be computed *)
let _ = Hashtbl.add optionsDatabase "iclos" (Bool true)
(* Whether incremental closure should be used instead of the usual cldepth *)
let _ = Hashtbl.add optionsDatabase "use-iclos" (Bool true)

(* Options added for SV-comp 2011 *)
(* Treat absence of error label as SAFE *)
let _ = Hashtbl.add optionsDatabase "nolabelsafe" (Bool false)
(* Functions to be treated as memory allocation sites *)
let _ = Hashtbl.add optionsDatabase "malloc_sites" (StringList [])
(* Additional options for CSIsat *)
let _ = Hashtbl.add optionsDatabase "csisat_opts" (String "")
(* Whether we perform abstraction dumps each 100 iterations *)
let _ = Hashtbl.add optionsDatabase "do-dumps" (Bool false)

(* Options added for SV-comp 2014 *)
let _ = Hashtbl.add optionsDatabase "sv-comp14-match-prop" (Bool false)

(* Call rerouting functionality.
   In call rerouting, we reroute functions listed under "reroute-foreach" and "map-foreach" options.

   1. When a map-foreach funciton is called, it's replaced with a call to its copy.  In this copy, all names that contain "reroute-placeholder" are replaced with the value of the key.  The key is taken from the funciton call, and usually is the name of the field.
   2. When a "reroute-foreach" function is encountered, it's replaced with a sequence of copies of the coresponding funciton.  For each key taken form the analysis of "map" functions, a call is inserted.

Index in reroute-map denotes the number of function parameter to replace placeholder with.  If the index is not specified, then all args are concatenated.  -reroute-first assumes all -map are actually -map1 options.
*)
let _ = Hashtbl.add optionsDatabase "reroute-foreach" (StringList [])
let _ = Hashtbl.add optionsDatabase "reroute-map" (StringList [])
let _ = Hashtbl.add optionsDatabase "reroute-map2" (StringList [])
let _ = Hashtbl.add optionsDatabase "reroute" (Bool false)
let _ = Hashtbl.add optionsDatabase "reroute-placeholder" (String "TEMPLATE")
let _ = Hashtbl.add optionsDatabase "reroute-first" (Bool false)

(* cpa fix: use stop-sep instead of stop-join *)
let _ = Hashtbl.add optionsDatabase "stop-sep" (Bool true)
let _ = Hashtbl.add optionsDatabase "merge" (String "bdd")

let usageMsg = "usage: blast <options> <source files>\n"

let sourceFiles = ref []

let append_to_string_list_option op a =
  match Hashtbl.find optionsDatabase op with
      StringList l ->
	if not (List.mem a l) then
	  Hashtbl.replace optionsDatabase op
	    (StringList (a::l))

let set_cref () =
    Hashtbl.replace optionsDatabase "incref" (Bool false)
		(* cref used to set "cldepth" as well.  We removed it, because cldepth has a separate setting, and cref SURPRISINGLY rewrote it if it was specified after it in the command line. *)

let set_focisymm () =
  set_cref ();
  Hashtbl.replace optionsDatabase "fmc" (Bool true);
  Hashtbl.replace optionsDatabase "bddpost" (Bool true);
  Hashtbl.replace optionsDatabase "itpn" (Bool true);
  Hashtbl.replace optionsDatabase "focisymm" (Bool true);
  Hashtbl.replace optionsDatabase "ubdp" (String "foci")

let set_fmcpa () =
  Hashtbl.replace optionsDatabase "fmc" (Bool true);
  Hashtbl.replace optionsDatabase "fmcpa" (Bool true)
  (* Hashtbl.replace optionsDatabase "bddpost" (Bool true);*)
  (* Hashtbl.replace optionsDatabase "itpn" (Bool true)*)

let unset_fmcpa () =
  Hashtbl.replace optionsDatabase "fmc" (Bool false);
  Hashtbl.replace optionsDatabase "fmcpa" (Bool false)
  (*Hashtbl.replace optionsDatabase "itpn" (Bool false)*)
  (*Hashtbl.replace optionsDatabase "bddpost" (Bool false)*)

let set_focimax n =
  Hashtbl.replace optionsDatabase "focimax" (Int n);
  Hashtbl.replace optionsDatabase "ubdp" (String "foci")




let set_cfb () =
  Hashtbl.replace optionsDatabase "cf" (Bool true);
  Hashtbl.replace optionsDatabase "ecov" (Bool true);
  Hashtbl.replace optionsDatabase "mod" (Bool true);
  Hashtbl.replace optionsDatabase "cfb" (Bool true) ;
  Hashtbl.replace optionsDatabase "nolattice" (Bool true);
  Hashtbl.replace optionsDatabase "sumcache" (Bool true);
  unset_fmcpa ()

let set_mccarthy () =
  Hashtbl.replace optionsDatabase "mccarthy" (Bool true);
  Hashtbl.replace optionsDatabase "lazysymm" (Bool true)


let set_fmc () =
  set_cref ();
  Hashtbl.replace optionsDatabase "fmc" (Bool true);
  Hashtbl.replace optionsDatabase "bddpost" (Bool true);
  Hashtbl.replace optionsDatabase "itpn" (Bool true)



let arg_spec = [
  ("", Arg.Unit (fun () -> ()), "\n\t\t Model Checking Options\n");

  ("-main", Arg.String (fun a ->
(*
	let oldlist = let ol = Hashtbl.find optionsDatabase "main" in
	  match ol with StringList l -> l
	  | _ -> failwith "Strange type of function list" in
*)
	 Hashtbl.replace optionsDatabase "main" (StringList ([ a ] (*:: oldlist *) ))),
   "entry point; start reachability verification from this function (default main())");
  ("-L", Arg.String (fun a -> Hashtbl.replace optionsDatabase "L" (String a)),
   "target label for reachability (default ERROR)");
  ("-alias",  Arg.String (fun a -> Hashtbl.replace optionsDatabase "alias" (String a)),
   "alias options\n\tOptions: If file name is bdd, invokes a bdd-based alias analysis,\n\totherwise reads the alias relationships from a file.");
  ("-cref", Arg.Unit set_cref,"complete counterexample analysis (should be turned on for alias analysis)");
  ("-cldepth", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "cldepth" (Int a)), "Expression Closure Unroll Depth");
  ("-fp", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nofp" (Bool false)), "use alias analysis to distpatch calls via function pointers.  Doesn't work well if a pointer may point to several functions.");

  ("", Arg.Unit (fun () -> ()), "\n\n\t\t Combination of analyses\n");

  ("-lattice", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nolattice"
    (Bool false)),
    "Activate Lattices, combine predicate analysis with lattice-based analysis");
  ("-include-lattice", Arg.String (fun a -> match Hashtbl.find optionsDatabase "include-lattice" with
    StringList l -> Hashtbl.replace optionsDatabase "include-lattice" (StringList (a::l))) ,
   "include a specific lattice ('symb' and 'list' are supported)");
  ("-nolattice", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nolattice" (Bool true)),
   "No lattices");
  ("-nosserr",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nosserr" (Bool true)),
   "SymbolicStore Lattice won't complain about assigning integer to structure pointer etc. In such cases, precision is lost.");
  ("-no-stop-sep", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "stop-sep" (Bool false)),
   "Use stop-join instead of stop-sep");
  ("-merge", Arg.String (fun a -> Hashtbl.replace optionsDatabase "merge" (String a)),
   "Use cpa merge with -stop-sep. Possible values are 'no' - do not merge, 'bdd' - merge regions with equal predicate BDDs (default), 'location' - merge at equal locations");


  ("", Arg.Unit (fun () -> ()), "\n\n\t\t C code perception options\n");

  ("-ignoredupfn",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "ignoredupfn" (Bool true)),
   "Ignore duplicate non-inline functions in source files");


  ("", Arg.Unit (fun () -> ()), "\n\n\t\t Analysis heuristics\n");
  ("-no-skipnorm", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "skipnorm" (Bool false)), "normalize predicates in BLAST, do not let solvers do it on their own (for bad solvers; otherwise it's slow)");
  ("-enable-recursion", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "enablerecursion" (Bool true)),
   "treat recursive function calls as skips (UNSOUND)");
  ("-nomusts",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nomusts" (Bool true)),
   "Treat must- and may-aliases differently (speeds up alias analysis, incompatible with -nosimplemem) (default)");
  ("-const",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "const" (Bool true)),
   "Assume that values declared as const do not change, and use this information in aliasing procedure");
  ("-noinitialize", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "initialize" (Bool false)), "do not initialize compound global variables");
  ("-fdepth", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "fdepth" (Int a)), "Maximal stack depth (treat deeper calls as skips)");
  ("-important-attr", Arg.String (fun a -> match Hashtbl.find optionsDatabase "important-attrs" with
	  StringList l -> Hashtbl.replace optionsDatabase "important-attrs" (StringList (a::l))),
   "attribute that the functions the path should never be cut to when -fdepth restrictions are imposed have.  May be specified multiple times.  By default the list is empty.  It would be sane to assign one of the attributes specified to each function you instrument.");
  ("-inline-attr", Arg.String (fun a -> match Hashtbl.find optionsDatabase "inline-attrs" with
	  StringList l -> Hashtbl.replace optionsDatabase "inline-attrs" (StringList (a::l))),
   "direct calls to functions with these attributes will never be cut due to -fdepth (indirect calls, however, may be cut).  May be specified multiple times.  By default the list is empty.  It would be sane to assign this attribute to small important 'inline' functions, such as __builtin_expect.");

  ("", Arg.Unit (fun () -> ()), "\n\n\t\t General Options\n");

  ("-quiet", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "quiet" (Bool true); Message.set_verbosity Message.Quiet), "quiet mode");
  ("-nodebug", Arg.Unit (fun () -> Message.set_verbosity Message.Default), "normal verbosity mode");
  ("-verbose", Arg.Unit (fun () -> Message.set_verbosity Message.Verbose), "verbose mode");
  ("-debug", Arg.Unit (fun () -> Message.set_verbosity Message.Debugging), "debug mode");
  ("-log", Arg.String (fun s -> Message.enable_logging_normal s),
   "turn on logging for specified type");
  ("-logdbg", Arg.String (fun s -> Message.enable_logging_debug s),
   "turn on debug logging for specified type");
  ("-loghelp", Arg.Unit (fun () -> Message.print_loghelp_msg ()), "print help message for logging");
  ("-devdebug", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "devdebug" (Bool true); Hashtbl.replace optionsDatabase "bddprint" (Bool true)), "developer's debug mode (less that Debug, but more than Normal.  Also turns -bddprint on.)");
  ("-bddprint", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddprint" (Bool true)), "examine predicates stored as BDDs");
  ("-noprofile", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "noprofile" (Bool true)), "do not profile");
  ("-alloc-function", Arg.String (fun a -> match Hashtbl.find optionsDatabase "malloc_sites" with
    StringList l -> Hashtbl.replace optionsDatabase "malloc_sites" (StringList (a::l))),
    "function to be treated as memory allocation sites (useful for alias analysis).  May be specified multiple times."
  );
  ("-csisat-opts", Arg.String (fun a -> Hashtbl.replace optionsDatabase "csisat_opts" (String a)),
   "additional options for CSIsat interpolating prover");

  ("", Arg.Unit (fun () -> ()), "\n\n\t\t Software Verificaiton competition 2011 options\n");
  ("-nolabelmeanssafe", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nolabelsafe" (Bool true)), "treat absence of error label (syntactical inreachability) as SAFE instead of error");
  ("-sv-comp", Arg.Unit (fun () ->
    (* Nondetermined pointer is sometimes used as an allocation site *)
    Hashtbl.replace optionsDatabase "malloc_sites" (StringList ["__VERIFIER_nondet_pointer"]);
    (* Use exact LP solver in CSIsat (solver time is negligible anyway) *)
    Hashtbl.replace optionsDatabase "csisat_opts" (String "-LAsolver exact");
    (* Analyze function pointers *)
    Hashtbl.replace optionsDatabase "nofp" (Bool false);
    Hashtbl.replace optionsDatabase "sv-comp14-match-prop" (Bool true);
  ), "Competition-specific tuning.  Sets [-alloc-function __VERIFIER_nondet_pointer -csisat-opts 'LAsolver exact' -fp]. Enables support for property files according to competition rules.");

  (*** OPTIONS FOR EXPERIMENTS, OBSOLETE, ET AL ***)
  ("", Arg.Unit (fun () -> ()), "\n\n\t***\t The rest is experimental options. \n\t***\t Use at your own risk!\n");

  ("-funlabel", Arg.String (fun a -> Hashtbl.replace optionsDatabase "funlabel" (String a)),
   "target function for reachability: runs each call separately. ");
  ("-cf", Arg.Unit (fun () ->
		      Hashtbl.replace optionsDatabase "cf" (Bool true);
		      Hashtbl.replace optionsDatabase "ecov" (Bool true);
		      Hashtbl.replace optionsDatabase "mod" (Bool true)
		      (*Hashtbl.replace optionsDatabase "nolattice" (Bool true)*)
		   ),
   "use context free reachability");
  ("-cfb", Arg.Unit set_cfb, "use context free reachability [bfs]");
  ("-force", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "qc" (Bool false);), "Force model checking -- avoid mc query caching");
  ("-prop", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "prop" (Bool true);), "Propagate parent preds in -craig 2");
  ("-preqfilter", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase
   "preqfilter" (Bool true);), "Only fire queries if some pre_pred holds (-craig 2)");
  ("-reachloc", Arg.Unit (fun () ->
                            Hashtbl.replace optionsDatabase "reachloc" (Bool true);
                            Hashtbl.replace optionsDatabase "norefine" (Bool true)
                         ), "Compute all unreachable locations -- supply initial abs!  ");
  ("-nocf" , Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "cf" (Bool false) ;
			 Hashtbl.replace optionsDatabase "mod" (Bool false)
		      ),
   "use context free reachability");

  ("-modopt", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "modopt" (Bool false)), "Turn OFF mod optimizations for CF checking");
  ("-bfs", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bfs" (Bool true);Hashtbl.replace optionsDatabase "dfs" (Bool false)), "breadth first search (Default)");
  ("-dfs", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dfs" (Bool true);Hashtbl.replace optionsDatabase "bfs" (Bool false)), "depth first search");
  ("-bnddfs", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bnddfs" (Bool true)), "bounded depth first search");
  ("-pred", Arg.String (fun a -> Hashtbl.replace optionsDatabase "pred" (String a)), "seed predicates file");

  ("-qf", Arg.String (fun a ->
    Hashtbl.replace optionsDatabase "funmode" (Int 5);
    Hashtbl.replace optionsDatabase "qf" (String a)),"query id file");
  ("-qcfile", Arg.String (fun a -> Hashtbl.replace optionsDatabase "qcfile" (String a)), "query cache file");
  ("-init", Arg.String (fun a -> Hashtbl.replace optionsDatabase "init" (String a)), "initial state file");

  ("-vardec",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "vardec" (Bool true)),
   "Enables printing vardecs (pollutes trace)");


  ("-ignore-depth-for", Arg.String (fun a -> Hashtbl.replace optionsDatabase "ignoredepthfor" (String a)),
   "ignore fdepth restriction for functions with this prefix.  Do not ignore if called indirectly.  Please, use '-important-attr' instead.");
  ("-important-label", Arg.String (fun a -> match Hashtbl.find optionsDatabase "important-labels" with
	  StringList l -> Hashtbl.replace optionsDatabase "important-labels" (StringList (a::l))),
   "(Please do not use, as CIL discards 'useless' labels!) label the path should never be cut to when -fdepth restrictions are imposed.  May be specified multiple times.  By default the list is empty.  It would be sane to include the error label here.");
  ("-important-attr", Arg.String (fun a -> match Hashtbl.find optionsDatabase "important-attrs" with
	  StringList l -> Hashtbl.replace optionsDatabase "important-attrs" (StringList (a::l))),
   "attribute that the functions the path should never be cut to when -fdepth restrictions are imposed have.  May be specified multiple times.  By default the list is empty.  It would be sane to assign one of the attributes specified to each function you instrument.");
  ("-inline-attr", Arg.String (fun a -> match Hashtbl.find optionsDatabase "inline-attrs" with
	  StringList l -> Hashtbl.replace optionsDatabase "inline-attrs" (StringList (a::l))),
   "direct calls to functions with these attributes will never be cut due to -fdepth (indirect calls, however, may be cut).  May be specified multiple times.  By default the list is empty.  It would be sane to assign this attribute to small important 'inline' functions, such as __builtin_expect.");
(* error predicate file should be moved to spec *)
  ("-error", Arg.String (fun a -> Hashtbl.replace optionsDatabase "error" (String a)), "error predicate file");
(* *)
  ("-query", Arg.String (fun a -> Hashtbl.replace optionsDatabase "query" (String a)), "query file");

  ("-inv",  Arg.String (fun a -> Hashtbl.replace optionsDatabase "inv" (String a)), "invariants file");
  ("-nocache", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nocache"
  (Bool true)), "do not cache theorem prover calls");
  ("-cache", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nocache"
  (Bool false)),
   "cache theorem prover calls");
  ("-cov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "cov" (Bool true)),
   "keep only coverable nodes");
  ("-cover", Arg.String (fun a ->
      Hashtbl.replace optionsDatabase "cov" (Bool true);
      Hashtbl.replace optionsDatabase "cover" (String a)),
   "Cover node heuristic -- [join,loop]");
("-covercheck", Arg.String (fun a ->
      Hashtbl.replace optionsDatabase "cov" (Bool true);
      Hashtbl.replace optionsDatabase "covercheck" (String a);
      if a <> "single" then Hashtbl.replace optionsDatabase "ecov" (Bool false)),
   "Cover node heuristic -- [single,predicated,predicated-delta,all]");

  ("-fqdump", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "fqdump" (Bool true)),
   "Dump foci queries to file");

  ("-losstrans", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "losstrans" (Bool true)),
   "lossy translation in fmc");
  ("-trelc", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "trelc" (Int i)),
   "Transition Relation Constraint: 0 -- use full trace, nonzero: use itp, 2 -- build conjunctive trel");
  ("-multistr", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "multistr" (Bool true)),
   "Multiple foci strengthenings using same trace");

  ("-reclaim", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "rec" (Bool true)),
   "dont keep whole tree around reclaim on backtrack");

  ("-s", Arg.String (fun a -> Hashtbl.replace optionsDatabase "s" (String a)),
    "satisfiability solver to be used for post-image computation
    {vampyre,simplify,simplify-bits,cvc,foci}");

  ("-smt",  Arg.Unit (fun () ->
    Hashtbl.replace optionsDatabase "incdp" (String "smt");
    Hashtbl.replace optionsDatabase "s" (String "smt")),
   "use SMT solver for sat solving and incremental decision procedures (default)");

  ("-simplify-solver",  Arg.Unit (fun () ->
    Hashtbl.replace optionsDatabase "incdp" (String "simplify");
    Hashtbl.replace optionsDatabase "s" (String "simplify")),
   "use Simplify solver for sat solving and incremental decision procedures (obsolete!)");

  ("-smtUF",  Arg.Unit (fun () ->
    Hashtbl.replace optionsDatabase "incdp" (String "smt");
    Hashtbl.replace optionsDatabase "s" (String "smt");
    Hashtbl.replace optionsDatabase "smtuf" (Bool true)),
   "use SMT solver for sat solving and incremental decision procedures,\n\tbut only with uninterpreted function symbols (i.e. no rules like: Select(Address(?x) 0) = ?x)");

  ("-noUFannot",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "ufannot" (Bool false)),
   "Do not annotate uninterpreted functions (useful in MathSAT, in addition to -smtUF)");

  ("-bits", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "bits" (Int a)),
   "bits to be used in bitvector queries when using simplify-bits");

  ("-ax", Arg.String (fun a -> Hashtbl.replace optionsDatabase "ax" (String a)),
   "additional axioms for simplify to add reasoning about other theories");
  ("-aliasclos",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "aliasclos" (Bool true)),
   "For bdd-alias analysis, introduce also expressions absent in source code");
  ("-aliasstat",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "aliasstat" (Bool true)),
   "For bdd-alias analysis, print statistics on how many alias pairs is there");
  ("-nostaticalias",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nostaticalias" (Bool true)),
   "When constructing path formula, do not analyze aliases for global variables (so they're considered constant)");
  ("-blastmust",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "blastmust" (Bool true)),
   "Treat temporary variables introduced by LDV-fixed LLVM (that start with blast_must_) as must-aliases.");
  ("-aliasfile",  Arg.String (fun a -> Hashtbl.replace optionsDatabase "aliasfile" (String a)),
   "alias override file: same format as .pred, use only == or != predicates.");
  ("-incdp",  Arg.String (fun a -> Hashtbl.replace optionsDatabase "incdp" (String a)),
   "incremental decision procedures {simplify,foci}");
  ("-ubdp", Arg.String (fun a -> Hashtbl.replace optionsDatabase "ubdp" (String a)),
   "useful blocks decision procedures {simplify,foci}");
  ("-pta", Arg.String (fun a -> Hashtbl.replace optionsDatabase "pta" (String a)),
   "Mode for points to analysis. noflow runs a flow insensitive version, flow a
    flow sensitive version.\n\tThis version of Blast only supports noflow.");
  ("-nofp", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nofp" (Bool true)), "ignore function pointer calls (Unsound: Blast does not handle function pointers)");
  ("-fp", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nofp" (Bool false)), "do not ignore function pointer calls");
  ("-noinitialize", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "initialize" (Bool false)), "do not initialize compound globals");
  ("-initialize", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "initialize" (Bool true)), "initialize compound globals (will lead to a really large basic block in initializer!)");
  ("-simplemem", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "simplemem" (Bool true)), "Simplify memory expressions as a prepass (default)");
  ("-nosimplemem", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "simplemem" (Bool false)), "Do not simplify memory expressions");
  ("-simplify", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "simplify" (Bool true)), "Simplify boolean expressions used as integers (default)");
  ("-nosimplify", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "simplify" (Bool false)), "Do not simplify boolean expressions used as integers ");

  (* ("-tsd", Arg.String (fun a -> Hashtbl.replace optionsDatabase "tsd" (String a)),
   "trace search direction. [zig] goes zigzagging [] restarts from the end");*)
  ("-ecov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "ecov" (Bool true)),
   "the regions are EQUAL, not just leq");
  ("-no-ecov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "ecov" (Bool false)),
   "the regions are just LEQ");
  ("-dsq", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dsq" (Bool true)),
   "Dotted squish");
  ("-csq", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "csq" (Bool true)),
   "cartes'ing the Dotted squish");
  ("-tc2", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "tc2" (Bool true)),
   "tree completeness check 2");
  ("-atc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "atc" (Bool true)),
   "skip atomic check");
  ("-alt", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "alt" (Bool true)),
   "skip atomic check");
  ("-lvc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "lvc" (Bool true)),
   "Dont merge if diff. lvals when squishing");

  ("-gpred", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "gpred" (Bool true)),
   "Use global preds globally");

  ("-bddcov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddcov" (Bool true)),
   "use bdds when doing cover check");
  ("-nobddcov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddcov" (Bool false)),
   "do not use bdds when doing cover check");
  ("-bddmin", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddmin" (Bool true)),
   "use bdds when building auto from tree");
  ("-nobddmin", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddmin" (Bool false)),
   "do full cover check");
  ("-scope", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "scope" (Bool true)), "scoping via inferred predicates ");

  ("-wpsat", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "wpsat" (Bool true)), " keep only satisfiable disjuncts in wp ");
  ("-restart", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "restart" (Bool true)),
   "restart computation (SLAM mode)");
  ("-cg", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "cg" (Bool true)),
   " Output callgraph info only (and then exit)");
  ("-mod", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "mod" (Bool true)),
   " Use mod-analysis for globals (useful with cf only)");

  ("-par", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "par" (Bool true)),
   "Use parallel Blast data structures");

 ("-projfun", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "projfun" (Bool true)),
   " Jump through functions in tproj ");
  ("-dom", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dom" (Bool true)),
   " Use dominator based always-reach for tproj ");


  ("-tpbug",
   Arg.Unit
     (fun () ->
	Hashtbl.replace optionsDatabase "tpbug" (Bool true);
	Hashtbl.replace optionsDatabase "tproj" (Bool true);
	Hashtbl.replace optionsDatabase "mod" (Bool true)
     ),
     " Use trace-project AND declare an error if the cone becomes empty ");
  ("-tproj", Arg.Unit
     (fun () ->
	Hashtbl.replace optionsDatabase "tproj" (Bool true);
	Hashtbl.replace optionsDatabase "mod" (Bool true)
     ),
     " Use TRACE-PROJECTION ");
     ("-exit", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "exit" (Bool
     true))," Track exit() during Trace Projection");
  ("-talias", Arg.Unit
     (fun () ->
	Hashtbl.replace optionsDatabase "talias" (Bool true);
     ),
   "Only compute aliases for the lvals in the trace during refinement.  Faster, but false positives for some programs. ");
  ("-aiter", Arg.Unit
     (fun () ->
	Hashtbl.replace optionsDatabase "aiter" (Bool true);
     ),
     " Use alias iterators instead of actual alias queries");
  ("-ialias", Arg.Unit
     (fun () -> Hashtbl.replace optionsDatabase "ialias" (Bool true);
                Hashtbl.replace optionsDatabase "use-ialias" (Bool true);
                (*Hashtbl.replace optionsDatabase "old-alias" (Bool false);*)
     ),
   "Use incremental alias analysis instead of the usual one");
  ("-ialias-exp-only", Arg.Unit
     (fun () -> Hashtbl.replace optionsDatabase "ialias" (Bool true);
                Hashtbl.replace optionsDatabase "use-ialias" (Bool false);
     ),
   "Perform incremental alias analysis, but use the results of the usual one (default)");
  ("-no-ialias", Arg.Unit
     (fun () -> Hashtbl.replace optionsDatabase "ialias" (Bool false);
                Hashtbl.replace optionsDatabase "use-ialias" (Bool false);
     ),
   "Do not use incremental alias analysis instead of the usual one");
  ("-iclos", Arg.Unit
     (fun () -> Hashtbl.replace optionsDatabase "iclos" (Bool true);
                Hashtbl.replace optionsDatabase "use-iclos" (Bool true);
                Hashtbl.replace optionsDatabase "cldepth" (Int 0);
     ),
   "Use incremental closure instead of the usual (default). This ignores -cldepth and works as if -cldepth +INFINITY");
  ("-no-iclos", Arg.Unit
     (fun () -> Hashtbl.replace optionsDatabase "iclos" (Bool false);
                Hashtbl.replace optionsDatabase "use-iclos" (Bool false);
     ),
   "Do not use incremental closure instead of the usual one, with -cldepth option.");
  ("-stop-sep", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "stop-sep" (Bool true)),
   "Use stop-set instead of stop-join (default)");



(*
   ("-allPreds", Arg.Unit (fun ()-> Hashtbl.replace optionsDatabase "allPreds" (Bool true)), "do focus on ALL subtraces ");
*)

  ("-scheck", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "scheck" (Bool true)), "Additional safety checks for aliasing");
  ("-incref", Arg.Unit (fun a->
   Hashtbl.replace optionsDatabase "incref" (Bool true);), "incomplete counterex. analysis");
  ("-cref", Arg.Unit set_cref,"complete counterex. analysis");

("-incmod", Arg.Unit
(fun a-> Hashtbl.replace optionsDatabase "incmod" (Bool true)),
 "Incrementally add mod hooks ");
("-noincmod", Arg.Unit
(fun a->  Hashtbl.replace optionsDatabase "incmod" (Bool false)),
 "Don't incrementally add mod hooks ");
("-dmod", Arg.Unit
(fun a-> Hashtbl.replace optionsDatabase "dmod" (Bool true)),
 "Demand-driven tproj ");
("-recompmod", Arg.Unit
(fun a-> Hashtbl.replace optionsDatabase "recompmod" (Bool true)),
 "Recompute mods (set by default)");
("-reloadmod", Arg.Unit
(fun a-> Hashtbl.replace optionsDatabase "recompmod" (Bool false)),
 "Reload mods from previously saved file ");
 ("-nomod", Arg.Unit
(fun a-> Hashtbl.replace optionsDatabase "nomod" (Bool true)),
 "No Mod info -- UNSOUND!!");


  ("-clock", Arg.Unit
    (fun a-> Hashtbl.replace optionsDatabase "clock" (Bool true);
        Hashtbl.replace optionsDatabase "fociUF" (Bool true)
    ), "chlvals using clock");

  ("-fociUF", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "fociUF" (Bool true)),
   "In foci conversion, convert Chlval using UF ");
  ("-fociminp", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "fociminp" (Bool true)),
   "Find minimum unsat prefix (using binary search) for all traces before interpolating ");
  ("-tg", Arg.Int (fun a-> Hashtbl.replace optionsDatabase "tg" (Bool true)),
   "take all global predicates in every query");
  ("-constpred", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "constpred"
   (Bool true)), "take constant closure of predicates");
  ("-predH", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "predH" (Int a)),
   "new predicate heuristic level\n\tOptions: 2: add x = c for all constant c, 6, 7: Additional Heuristics. Recommended Level is 7 (and other levels just do not work in this version ;-) ).");
  ("-block", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "block" (Bool true)), "Analyze trace as blocks ");
  ("-noblock", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "block" (Bool false)), "Use the old trace analysis ");
  ("-craig", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "craig" (Int a);
      Hashtbl.replace optionsDatabase "block" (Bool true);
      if a >=2 then Hashtbl.replace optionsDatabase "prop" (Bool true)),
   "Use craig interpolants (FOCI) to get predicates ([1] use with scope, [2] goes the whole hog)\n\tSee the paper Abstractions from Proofs");
  ("-clp",
   Arg.Unit (fun () ->
	       Hashtbl.replace optionsDatabase "itp" (String "clp")
	    ), "Use CLP-prover instead of CSIsat");
  ("-foci",
   Arg.Unit (fun () ->
	       Hashtbl.replace optionsDatabase "itp" (String "foci")
	    ), "Use Foci instead of CSIsat");

  ("", Arg.Unit (fun () -> ()), "\n\t\t Old Heuristics that are no longer used/supported\n");

  ("-comp", Arg.String (fun a -> Hashtbl.replace optionsDatabase "comp" (String a)),
   "Completeness level:\n\tOptions = CFB mode {lazy,cut,demand} regular mode
   {cut,path} (default cut)");
  ("-forget", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "forget" (Bool true)),
   "forget predicates outside subtree");
  ("-post", Arg.String (fun a -> Hashtbl.replace optionsDatabase "post" (String a)),
   "post algorithm to be used:\n\tOptions are slam and H (default slam)");
  ("-dc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dc" (Bool true)), " dont care heuristic for predicates not in scope ");
  ("-refine", Arg.String (fun a -> Hashtbl.replace optionsDatabase "refine" (String a)),
   "direction of refinement (fwd or bwd), default fwd. No longer used, superceded by -block and -craig.");
  ("-custom", Arg.String (fun a -> Hashtbl.replace optionsDatabase "custom" (String a)), "custom abstraction file");




  ("", Arg.Unit (fun () -> ()), "\n\t\t Program Optimization Options\n");
  ("-pe", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "pe" (Bool true)), "partial evaluation (Constant propagation)");
(*  ("-subgoal", Arg.String (fun a -> Hashtbl.replace optionsDatabase "subgoal" (String a)), "subgoals file"); *)
  ("-O", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "O" (Int a)), "Program optimizations");
  ("-depth", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "depth" (Int a)), "Unroll Depth");
  ("-fdepth", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "fdepth" (Int a)), "Maximal stack depth");
  ("-cldepth", Arg.Int (fun a -> Hashtbl.replace optionsDatabase "cldepth" (Int a)), "Expression Closure Unroll Depth");
  ("", Arg.Unit (fun () -> ()), "\n\t\t Parallel Model Checking and Races\n");
  ("-events", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "events" (Bool true)),
   "check program with asynchronous function calls");
  ("-getfunsize", Arg.String (fun s -> Hashtbl.replace optionsDatabase
  "getfunsize" (String s)),"print size of prooftree nodes of a particular function, e.g. __BLAST_DISPATCH_FUNCTION");
  ("-sets", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "sets" (Bool true)),
   "compile sets optimizations");
  ("-checkRace", Arg.String (fun s -> Hashtbl.replace optionsDatabase "checkRace" (String s)),
   "check for races on variable accesses");
  ("-racevar", Arg.String (fun s -> Hashtbl.replace optionsDatabase "racevar" (String s)),
   "variable to check for races on");
  ("-simpres", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "simpres" (Int i)),
   "reset simplify every k steps");
  ("-skipfun", Arg.Unit (fun () ->
			 Hashtbl.replace optionsDatabase "skipfun" (Bool true);
			 Hashtbl.replace optionsDatabase "mod" (Bool true)
		      ),
   "skip functions that dont modify any variable in the present pred set");
  ("-skipcheck", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "skipcheck" (Bool true)),
   "Check if skipped funs need unskipping in TPROJ");
  ("-bmc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bmc" (Bool true)),
   "Use bmc heuristic in skipfun");
  ("-errlv", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "errlv" (Bool true)),
   "Add functions modifying error lvals");



  ("-exitDepth", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "exitDepth"
    (Int i)), "how far from exit funs should be added in skipfun cone");

  ("-bccscc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bccscc"
    (Bool true)), "Use scc heuristic in skipfun seed. Yes, it is mumbo jumbo.");

("-multi", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "multi" (Int i)),
   "Max #errors");
("-efilter", Arg.Unit (fun i -> Hashtbl.replace optionsDatabase "efilter" (Bool true)),
   "Filter traces with events");
  ("-maxcounter", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "maxcounter" (Int i)), "Max counter-size");
  ("-finitecounter", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "finitecounter" (Bool true)), "Only count upto maxcounter");
  ("-omega", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "omega" (Int i)),"Max #threads");
  ("-nomp", Arg.Unit (fun i -> Hashtbl.replace optionsDatabase "nomp" (Bool true)),
   "Dont merge parents ...");
  ("-par", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "par" (Bool true)),
   "use data structures for parallel blast");
  ("-dotsep", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dotsep" (Bool false)),
   "squish dotted states as well...");
  ("-easm",  Arg.String (fun a -> Hashtbl.replace optionsDatabase "easm" (String a)), "environment assumptions file");

  ("", Arg.Unit (fun () -> ()), "\n\t\t Saved Abstractions and Summarization\n");
  ("-loadabs", Arg.String (fun a-> Hashtbl.replace optionsDatabase "loadabs" (String a)), " Load Abstraction from fname.abs ");
  ("-dumpabs", Arg.String (fun a-> Hashtbl.replace optionsDatabase "dumpabs" (String a)), " Dump Abstraction to fname.abs ");
  ("-do-dumps",  Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "do-dumps" (Bool true)), "Perform abstraction dumps each 100 iterations");
  ("-loadtest", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "loadtest" (Bool true)), " Load tests from fname.tst ");
  ("-skiptest", Arg.Int (fun a-> Hashtbl.replace optionsDatabase "skiptest" (Int a)), " Skip the next n tests ");
  ("-edgemode", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "edgemode" (Bool true)), "In funlabel mode, replace each edge individually");
  ("-funmode", Arg.Int (fun a-> Hashtbl.replace optionsDatabase "edgemode" (Bool false);
                                Hashtbl.replace optionsDatabase "funmode" (Int a)),
   "In funlabel mode, 0: replace all edges in the same function together , 1: partition according to variable");
  ("-selecttest", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "selecttest" (Bool true)), "Test selectively, use checktest option to register checks to be done ");
  ("-checktest", Arg.Int (fun a-> let oldl = Hashtbl.find optionsDatabase "checktest" in match oldl with
		IntList x -> Hashtbl.replace optionsDatabase "checktest" (IntList (a::x) )
                | _ -> failwith "checktest: Unexpected failure"), "Check only these tests");
  ("-timeout", Arg.Int (fun a-> Hashtbl.replace optionsDatabase "timeout" (Int a)), " Time out the model checker (unlimited by default)");
  ("-iter-bound", Arg.Int (fun a-> Hashtbl.replace optionsDatabase "iter-bound" (Int a)), "Limit the number of big-while-loop iterations ");
  ("-interface", Arg.String (fun a -> Hashtbl.replace optionsDatabase "interface" (String a)), "name of a function to take as describing a component interface");
  ("-component", Arg.String (fun a -> Hashtbl.replace optionsDatabase "component" (String a)), "name of a function to check for satisfying the interface");

  ("", Arg.Unit (fun () -> ()), "\n\t\t Proof Generation Options\n");
  ("-pf", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "pf" (Bool true)), "generate proof tree");
  ("-pfgen",Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "pfgen" (Bool true)), "spit out vampyre proofs");
  ("-pffile", Arg.String (fun a -> Hashtbl.replace optionsDatabase "pffile" (String a)),
   "file to write vampyre proofs (default /tmp/lf.pfs)");

  ("", Arg.Unit (fun () -> ()), "\n\t\t General Options\n");

  ("-quiet", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "quiet" (Bool true); Message.set_verbosity Message.Quiet), "quiet mode");
  ("-nodebug", Arg.Unit (fun () -> Message.set_verbosity Message.Default), "normal verbosity mode");
  ("-verbose", Arg.Unit (fun () -> Message.set_verbosity Message.Verbose), "verbose mode");
  ("-debug", Arg.Unit (fun () -> Message.set_verbosity Message.Debugging), "debug mode");
  ("-log", Arg.String (fun s -> Message.enable_logging_normal s),
   "turn on logging for specified type");
  ("-logdbg", Arg.String (fun s -> Message.enable_logging_debug s),
   "turn on debug logging for specified type");
  ("-loghelp", Arg.Unit (fun () -> Message.print_loghelp_msg ()), "print help message for logging");
  ("-devdebug", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "devdebug" (Bool true); Hashtbl.replace optionsDatabase "bddprint" (Bool true)), "developer's debug mode (less that Debug, but more than Normal.  Also turns bddprint on.");
  ("-test", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "test" (Bool true)), "test mode");
  ("-testfile", Arg.String (fun s -> Hashtbl.replace optionsDatabase "testfile" (String s)), "file to which tested CFA is dumped");
  ("-cfa-dot", Arg.String (fun a -> Hashtbl.replace optionsDatabase "cfa-dot" (String a)), "output the CFA of every function in ATT dot format");
  ("-trace-dot", Arg.String (fun a -> Hashtbl.replace optionsDatabase "trace-dot" (String a)), "output the graph of the error trace in ATT dot format");
  ("-tree-dot", Arg.String (fun a -> Hashtbl.replace optionsDatabase "tree-dot" (String a)), "output the final ART in ATT dot format");
  ("-callgraph", Arg.String (fun a -> Hashtbl.replace optionsDatabase "callgraph" (String a)), "output the callgraph in ATT dot format");
  ("-systime", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "systime" (Bool true)), "include system time in profile calculations as well");
  ("-noprofile", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "noprofile" (Bool true)), "do not profile");
  ("-unaccprof", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "unaccprof" (Int i)), "threshold to display unprofiled time only above (in percent). Default: 20");
  ("-devnocov", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "devnocov" (Bool true)), "(developers only) turn off coverage checks");
  ("-nosmtgub", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "nosmtgub" (Bool true)), "(developers only) do not use SMT-gub algorithm");
  ("-stop-sep", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "stop-sep" (Bool true)),
   "Use stop-sep instead of stop-join (default)");
  ("-auto", Arg.String (fun a -> Hashtbl.replace optionsDatabase "auto" (String a)), "output the auto of the tree in ATT dot format");
  ("-simpquery", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "simpquery" (Bool true)), "dump simplify queries to file");
  ("-uif", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "uif" (Bool true)), "arithmetic uninterpreted for refinement");

  ("-I", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "interactive" (Bool true)), "interactive shell mode");
  ("-msvc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "msvc" (Bool true) ), "parse MS VC");

  ("-norefine", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "norefine" (Bool true)), "don't refine -- only with CFB");
  ("-stop", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "stop" (Bool true)), "stop when it hits first counterex");
  ("-traces", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "traces" (Bool true)), "dump bogus traces");
  ("-tracefile", Arg.String (fun a-> Hashtbl.replace optionsDatabase "tracefile" (String a)), " file containing trace information -- for trace viewer");
  ("-plaintracefile", Arg.String (fun a-> Hashtbl.replace optionsDatabase "plaintracefile" (String a)), " file containing trace information -- for Eclipse viewer");
  ("-file", Arg.String (fun a-> Hashtbl.replace optionsDatabase "file" (String a)), " file containing trace information -- for trace viewer");
  ("-demo", Arg.Unit (fun a-> Hashtbl.replace optionsDatabase "demo" (Bool true)), "Demo Mode for the GUI");
  ("-ti", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "ti" (Bool true)), "Build Transition Invariant (!)");
  ("-posp", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "posp" (Bool true)), "Use predicates positively -- i.e. pretend they are closed under negation");

  ("-fmc", Arg.Unit set_fmc, "foci model checker");
  ("-bddpost",
   Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "fmc" (Bool true);
                      Hashtbl.replace optionsDatabase "bddpost" (Bool true)),
                      "Use BDD-based post-images, requires fmc");

  ("-slicesymm", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "slicesymm" (Bool true)),
   "use predicates just discovered for refining trel");
  ("-focisymm", Arg.Unit set_focisymm, "Use BDD-based post-images, find trel via symm itp, requires fmc");
  ("-fmcpa", Arg.Unit set_fmcpa, "Use fmc instead of lmc");
  ("-timezero", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "timezero"
   (Bool true)), "Debugging option only! Use 0 as last chlval id in trace formulas");
  ("-mccarthy", Arg.Unit set_mccarthy ,
   "Use select/store in refinement. Only with (split) foci, set using focimax, without cf.");
  ("-rp", Arg.Int (fun n -> set_focimax n;
    Hashtbl.replace optionsDatabase "rp" (Bool true);set_mccarthy ()),
   "Use range predicates. n is paramater used for focimax. Use with -mccarthy , without cf.");
  ("-norp", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "rp" (Bool false)),
   "Turn off range predicates.");

  ("-itpn", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "itpn"
   (Bool true)), "Use single foci call for refinement");
  (*("-dasdill", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "dasdill" (Bool true)), "Das-Dill trel refinement: Only use with -focisymm!");*)
  ("-invbdd", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "invbdd" (Bool true)), "Globally useful facts. Only use with -focisymm!");
  ("-bddprint", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "bddprint" (Bool true)), "DNF bddprint");

  ("-hybrid", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "hybrid" (Bool true)), "Hybrid post");
  ("-fmcc", Arg.Unit (fun () ->
     Hashtbl.replace optionsDatabase "fmcc" (Bool true);
     Hashtbl.replace optionsDatabase "incref" (Bool false);
   ), "foci model checker concrete");
  ("-focimax", Arg.Int set_focimax, "max constant allowed in interpolant ");
  ("-blockfmc", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "blockfmc"
   (Bool true)), "use get useful blocks in fmc");
  ("-regcons", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "regcons"
   (Bool true)), "use region constraints when refining in fmc");

  ("-lockset", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "lockset" (Bool true)), "Do a lock set based analysis for deadlock");


  ("-showtree", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "showtree" (Bool true)), "Print Reachability Tree");
  ("-savetree", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "savetree" (Bool true)), "Save abstractions by iterating over Reachability Tree --use with focisymm to find the actual localization info");
 ("-checktree", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "checktree" (Bool true)), "Check Reachability Tree");
 ("-qid", Arg.Rest (fun s -> let oldq = Hashtbl.find optionsDatabase "qid" in
    match oldq with IntList x -> Hashtbl.replace optionsDatabase "qid" (IntList
    (x@[int_of_string s])) | _ -> failwith "options qid: error!"), "Run specific query -- put at
END of options! after filename");
  ("-bvcons", Arg.Int (fun i -> Hashtbl.replace optionsDatabase "bvcons" (Int i)),
   "bitv: 0 -- all, 1 -- drop BOT, 2 -- drop IeQ, 3 -- drop triv eq");
  ("-reroute-foreach", Arg.String (fun a -> match Hashtbl.find optionsDatabase "reroute-foreach" with
    StringList l -> Hashtbl.replace optionsDatabase "reroute-foreach" (StringList (a::l))) ,
   "what functions to clone as foreach");
  ("-reroute-map", Arg.String (fun a -> match Hashtbl.find optionsDatabase "reroute-map" with
    StringList l -> Hashtbl.replace optionsDatabase "reroute-map" (StringList (a::l))) ,
   "what functions to clone as map");
  ("-reroute-map2", Arg.String (fun a -> match Hashtbl.find optionsDatabase "reroute-map2" with
    StringList l -> Hashtbl.replace optionsDatabase "reroute-map2" (StringList (a::l))) ,
   "what functions to clone as map (with 2nd argument only)");
  ("-reroute", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "reroute" (Bool true)),
   "Turn rerouting on");
  ("-reroute-placeholder", Arg.String (fun a-> Hashtbl.replace optionsDatabase "reroute-placeholder" (String a)), "placeholder for rerouting functionality");
  ("-reroute-first", Arg.Unit (fun () -> Hashtbl.replace optionsDatabase "reroute-first" (Bool true)),
   "Reroute only by the first parameter of function calls");
]



let buildOptionsDatabase () =
  Arg.parse arg_spec (fun s -> (sourceFiles := s :: (!sourceFiles))) usageMsg ;
  sourceFiles := List.rev !sourceFiles;
  let filename_prefix =
    if !sourceFiles = [] then "__foo"
    else
    begin
    let fname = List.hd !sourceFiles in
      match Misc.chop fname "\." with
	  [] -> fname
	|  e -> List.hd e
     end
  in
    Hashtbl.replace optionsDatabase "mainsourcename" (String filename_prefix)


let getSourceFiles () = !sourceFiles

let isSet arg =
  try
    let a = Hashtbl.find optionsDatabase arg in
    match a with
      Bool b -> b
    | _ -> failwith "Options: isSet -- non boolean type"
  with
    Not_found ->
      (Message.msg_string Message.Major ("\nOptions: isSet: arg '" ^ arg ^ "' not found in database!\n");
      (* continue *)
       false)

let getValueOf arg =
  try
	Hashtbl.find optionsDatabase arg
  with Not_found -> raise (OptionNotFoundException arg)

let getValueOfString arg =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    String s -> s
  | _ -> failwith ("Options: getValueOfString -- non string type: "^arg)
  with Not_found -> raise (OptionNotFoundException arg)

let setValueOfString arg str_v =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    String s -> Hashtbl.replace optionsDatabase arg (String str_v)
  | _ -> failwith "Options: setValueOfString -- non string type"
  with Not_found -> raise (OptionNotFoundException arg)

let getValueOfInt arg =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    Int s -> s
  | _ -> failwith "Options: getValueOfInt -- non int type"
  with Not_found -> raise (OptionNotFoundException arg)

let setValueOfInt arg int_v =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    Int s -> Hashtbl.replace optionsDatabase arg (Int int_v)
  | _ -> failwith "Options: setValueOfInt -- non int type"
  with Not_found -> raise (OptionNotFoundException arg)


let getValueOfBool arg =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    Bool s -> s
  | _ -> failwith "Options: getValueOfBool -- non bool type"
  with Not_found -> raise (OptionNotFoundException arg)

let setValueOfBool arg bool_v =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    Bool s -> Hashtbl.replace optionsDatabase arg (Bool bool_v)
  | _ -> failwith "Options: setValueOfBool -- non bool type"
  with Not_found -> raise (OptionNotFoundException arg)


let getValueOfStringList arg =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    StringList s -> s
  | _ -> failwith "Options: getValueOfStringList -- non StringList type"
  with Not_found -> raise (OptionNotFoundException arg)

let getValueOfIntList arg =
  try
  let a =   Hashtbl.find optionsDatabase arg in
  match a with
    IntList s -> s
  | _ -> failwith "Options: getValueOfIntList -- non IntList type"
  with Not_found -> raise (OptionNotFoundException arg)


let errorseed_table : (string,bool) Hashtbl.t = Hashtbl.create  31
let set_errorseed fn_list =
  Hashtbl.clear errorseed_table;
  List.iter (fun fname -> Hashtbl.replace errorseed_table fname true) fn_list

let get_errorseed () = Misc.hashtbl_keys errorseed_table
let is_errorseed fname = Hashtbl.mem errorseed_table fname
