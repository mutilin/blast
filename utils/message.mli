(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



 (**
   * This module allows to write messages filtered by their significance.
   *)


type verbosity =
    Quiet      (** only error and major messages shall be displayed *)
  | Default    (** only error, major and normal messages shall be displayed *)
  | Verbose    (** all messages except debug messages shall be displayed *)
  | Debugging  (** all messages shall be displayed *)

type significance =
    Error     (** Error: for error messages *)
  | Major     (** Major: for crucial messages *)
  | Normal    (** Normal: for normal messages *)
  | Minor     (** Minor: for minor messages *)
  | Debug     (** Debug: for debug messages *)

(** sets the verbosity used subsequently by the msg function *)
val set_verbosity : verbosity -> unit

(** get the verbosity level *)
val get_verbosity : unit -> verbosity

(** displays a message according to its significance and to the current
   verbosity level *)
val msg_printer : significance -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** displays a message string according to its significance and to the current
   verbosity level *)
val msg_string  : significance -> string -> unit

(** applies a function to an argument according to its significance and to the current
   verbosity level 
   This is useful, eg when I want to trawl a data structure and print out its contents,
   but only if the verbosity level is high enough
*)
val msg_apply  : significance -> ('a -> unit) -> 'a -> unit


(** {1 Logging facility}
This facility provides the ability to selectively print debug messages from a given area. A
message type (of type logmsg_type) is passed to the logging call for identifying the area which
originates a message. The printing of messages from a given area is enabled by specifying either
-log <mtype> or -logdbg <mtype> on the Blast command line. Each line of a message is printed with
a prefix which identifies its area (e.g. "SYML>" or "PRED>").

A given message can be printed with one of four severity levels:
 - {i Error} - Messages logged with the log_print_err and log_string_err functions are always printed to stderr.
 - {i Always} - Messages logged with log_print_aways and log_string_aways are always printed, unless -quiet is specified on the command line.
 - {i Normal} - Messages logging with log_print_norm and log_string_norm are printed if any of -log <mtype>, -logdbg <mtype>, or -debug are specified on the command line.
 - {i Debug} - messages logged with log_print_dbg and log_string_dbg are printed if either -log <mtype> or -debug is specified on the command line.
  
*)

(** The various types of log messages. To create a new type, add an entry to this union and then a
    definition entry to msg_types in message.ml *)
type logmsg_type = 
    Rgn (** Print the region calculated for each iteration of model check loop *)
  | SymLat (** Logging for symbolic execution lattice *)
  | UnionLat (** Logging for union lattice *)
  | ListLat  (** Logging for list lattice *)
  | BddPost (** Logging for bdd-post in abstraction.ml *)
  | Cil (** Logging about program metadata obtained from Cil *)
  | Ast (** Warnings from ast.ml *)
  | SA (** Logging for shape analysis *)

(** The different logging levels *)
type logmsg_level =
    LogErrors (** An Error. Always print to stderr. *)
  | LogAlways  (** Informational message that should always be printed. *)
  | LogNormal (** Normal level - printed when logging is enabled for the specified type *)
  | LogDebug  (** Debug level - printed when debug logging is enabled for the specified type *)

(** Log using a printer with the specified log level and type. *)
val log_print : logmsg_level -> logmsg_type -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** Log the provided string with the specified log level and type. *)
val log_string : logmsg_level -> logmsg_type -> string -> unit

(** Log the specified data using the specified printer. This is for error messages -
    they are always printed to stderr, using a special ERROR prefix. *)
val log_print_err : logmsg_type -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** Log the specified string. This is for error messages -
    they are always printed to stderr, using a special ERROR prefix. *)
val log_string_err : logmsg_type -> string -> unit

(** Log the specified data using the specified printer. The message is always printed,
    unless the -quiet command line option is specified. *)
val log_print_always : logmsg_type -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** Log the specified string. The message is always printed, unless the -quiet command
    line option is specified. *)
val log_string_always : logmsg_type -> string -> unit

(** Log the specified data using the specified printer. The message is only printed
    when any of the -log <mtype>, -logdbg <mtype>, or -debug command line options are
    specified. *)
val log_print_norm : logmsg_type -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** Log the specified string. The message is only printed
    when any of the -log <mtype>, -logdbg <mtype>, or -debug command line options ar
    specified. *)
val log_string_norm : logmsg_type -> string -> unit

(** Log the specified data using the specified printer. The message is only printed
    when either the -logdbg <mtype> command line option is specified. *)
val log_print_dbg : logmsg_type -> (Format.formatter -> 'a -> unit) -> 'a -> unit

(** Log the specified string. The message is only printed
    when either the -logdbg <mtype> command line option is
    specified. *)
val log_string_dbg : logmsg_type -> string -> unit

(** Return true if normal level logging is enabled for the specified type *)
val is_normal_logging_enabled : logmsg_type -> bool

(** Return true if debug level logging is enabled for the specified type *)
val is_dbg_logging_enabled : logmsg_type -> bool

(** Turn on normal logging for the specified type (called when encountering the -log command
    line option). *)
val enable_logging_normal : string -> unit

(** Turn on debug logging for the specified type (called when encountering the -logdbg command
    line option). *)
val enable_logging_debug : string -> unit

(** Print help message for logging and exit (called by the -loghelp command line option) *)
val print_loghelp_msg : unit -> unit
