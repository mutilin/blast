type valueType =
    Bool of bool
  | String of string
  | Int of int
  | Float of float
  | StringList of string list
  | IntList of int list
exception OptionNotFoundException of string

(* val optionsDatabase : (string, valueType) Hashtbl.t
val usageMsg : string
val sourceFiles : string list ref
val arg_spec : (string * Arg.spec * string) list *)

val buildOptionsDatabase : unit -> unit
val getSourceFiles : unit -> string list
val isSet : string -> bool
val getValueOf : string -> valueType
val getValueOfString : string -> string
val setValueOfString : string -> string -> unit
val getValueOfInt : string -> int
val setValueOfInt : string -> int -> unit
val getValueOfBool : string -> bool
val setValueOfBool : string -> bool -> unit
val getValueOfStringList : string -> string list
val getValueOfIntList : string -> int list
val errorseed_table : (string, bool) Hashtbl.t
val set_errorseed : string list -> unit
val get_errorseed : unit -> string list
val is_errorseed : string -> bool
