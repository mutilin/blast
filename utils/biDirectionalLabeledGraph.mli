(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(** the node type *)
type ('a, 'b) node

(** the edge type *)
and  ('a, 'b) edge

(** root node creation *)
val create_root  : 'a -> ('a, 'b) node
						
val get_node_label : ('a, 'b) node -> 'a

val get_source : ('a, 'b) edge -> ('a, 'b) node

val get_target : ('a, 'b) edge -> ('a, 'b) node

val get_edge_label : ('a, 'b) edge -> 'b

val get_children : ('a, 'b) node -> ('a,'b) node list

val get_parents : ('a, 'b) node -> ('a,'b) node list

val exists_edge : ('a, 'b) node -> ('a, 'b) node -> 'b -> bool

val hookup_parent_child : ('a, 'b) node -> ('a, 'b) node -> 'b -> unit

val delete_parent_child : ('a, 'b) node -> ('a, 'b) node -> unit

val delete_node : ('a, 'b) node -> unit

val delete_edge : ('a, 'b) edge -> unit

val copy_edge_source : ('a, 'b) node -> ('a, 'b) edge -> unit

val copy_edge_target : ('a, 'b) node -> ('a, 'b) edge -> unit


val ancestors : ('a, 'b) node list -> ('a, 'b) node list

val descendants : ('a, 'b) node list -> ('a, 'b) node list

val prune : (('a, 'b) node -> bool) -> ('a, 'b) node list  -> unit 

val output_graph_dot : ('a -> string) -> ('b -> string ) -> string -> ('a,'b) node list -> unit

val output_multi_graph_dot : ('a -> string) -> ('b -> string) -> string -> 
  (('a, 'b) node list * string) list -> unit

val get_out_edges : ('a, 'b) node -> ('a, 'b) edge list

val get_in_edges : ('a, 'b) node -> ('a, 'b) edge list

val deconstruct_edge : ('a, 'b) edge -> ('a, 'b) node * 'b * ('a, 'b) node

(** local_connect f src targ : returns a path (if one exists) as a list of edges *)
val local_connect : 
  (('a, 'b) node -> ('a,'b) edge list) 
  -> ('a,'b) node 
    -> ('a,'b) node 
      -> (('a,'b) edge) list option 


val collect_component :
  (('a, 'b) node -> ('a,'b) node list) 
  ->  ('a,'b) node list
    -> ('a, 'b) node list

val get_all_edges_like : ('a, 'b) edge -> ('a, 'b) edge list

val find_edges : ('a, 'b) node -> ('a, 'b) node -> ('a, 'b) edge list 

val hookup_wo_duplicate : ('a, 'b) node -> ('a, 'b) node -> 'b -> unit

val scc : ('a,'b) node -> ('a,'b) node list list

val shortest_path : (('a,'b) node -> 'b -> bool)  -> ('a, 'b) node -> ('a, 'b) node -> (('a, 'b) node * 'b) list

val shortest_path_lengths : ('a, 'b) node -> (('a,'b) node * int) list

val has_child : ('a, 'b) node -> bool
