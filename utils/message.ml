(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module allows to write messages filtered by their significance.
 *)


type verbosity =
    Quiet
  | Default
  | Verbose
  | Debugging

type significance =
    Error
  | Major
  | Normal
  | Minor
  | Debug

let get_verbosity_level = function
    Quiet -> 0
  | Default -> 1
  | Verbose -> 2
  | Debugging -> 3

let get_significance_level = function
    Error -> -1
  | Major -> 0
  | Normal -> 1
  | Minor -> 2
  | Debug -> 3 (* JHALA HACK HACK HACK! *)

let verbosity_level = ref 0

let set_verbosity v = verbosity_level := (get_verbosity_level v)

let get_verbosity v = 
  let v_l = function
      0 -> Quiet
    | 1 -> Default
    | 2 -> Verbose
    | 3 -> Debugging
    | _ -> Debugging
  in
  v_l (!verbosity_level)

let msg_printer sl printer data =
  let fmt =
    if (sl == Error) then
      Format.err_formatter
    else
      Format.std_formatter
  in
    if (get_significance_level sl) <= !verbosity_level then
      begin
        Format.pp_open_hbox fmt () ;
        printer fmt data ;
        Format.pp_print_newline fmt () ;
        Format.pp_close_box fmt ()
      end
    else
      ()

let msg_string sl str =
  msg_printer sl Format.pp_print_string str


let msg_apply sl func data =
    if (get_significance_level sl) <= !verbosity_level then
      begin
        func data ;
      end
    else
      ()

(**************************************************************)
(* Logging facility. See message.mli for descriptive comments *)
(**************************************************************)

type logmsg_type = 
    Rgn
  | SymLat
  | UnionLat
  | ListLat
  | BddPost
  | Cil
  | Ast
  | SA

type logmsg_level =
    LogErrors
  | LogAlways
  | LogNormal
  | LogDebug

type logmsg_type_defn =
    { mtype : logmsg_type; prefix : string; desc : string }

(* Define new message types here *)
let msg_types =
  [ {mtype = Rgn; prefix = "RGN";
     desc = "Print the region calculated for each iteration of model check loop"};
    {mtype = SymLat; prefix = "SYML"; desc = "Symbolic Execution Lattice"};
    {mtype = UnionLat; prefix = "UL"; desc = "Union Lattice"};
    {mtype = ListLat; prefix = "LISTL"; desc = "List Lattice"};
    {mtype = BddPost; prefix = "BDDPOST";
     desc = "Bdd-post calls in abstraction.ml"};
    {mtype = Cil; prefix = "CIL"; desc = "Program metadata obtained from Cil"};
    {mtype = Ast; prefix = "AST"; desc = "Warnings issued by Ast.ml"};
    {mtype = SA; prefix = "SA"; desc = "Shape Analysis" }]

type logmsg_info = { fmt : Format.formatter; errFmt : Format.formatter; mutable loglevel : int }

let make_prefix_formatter prefix chan =
  let rec start_of_line = ref true
  and output_body str pos len =
    let endpos = pos + len - 1 in
      if !start_of_line then (output_string chan prefix; start_of_line := false);
      try
	let eol = String.index_from str pos '\n' in
	  if eol <= endpos then
	    (output chan str pos (eol - pos + 1);
	     start_of_line := true;
	     (if eol <> endpos then output_body str (eol + 1) (len - (eol - pos + 1))))
	  else output chan str pos len
      with Not_found ->
	output chan str pos len
  in Format.make_formatter (fun str pos len -> output_body str pos len) (fun () -> flush chan)

let log_entries = Hashtbl.create 15

let initialize () =
  List.iter (fun defn ->
	       Hashtbl.replace log_entries defn.mtype
	         {fmt = (make_prefix_formatter (defn.prefix ^ "> ") stdout);
		  errFmt = (make_prefix_formatter ("ERROR(" ^ defn.prefix ^ ")> ") stderr);
		  loglevel = 0}) msg_types

exception Unknown_logtype of logmsg_type
exception Unknown_logtypename of string

let _get_entry mtype =
  try
    Hashtbl.find log_entries mtype
  with Not_found -> raise (Unknown_logtype mtype)

let _get_entry_by_name mname =
  (* slow version to search by a string. Only called when processing command line options. *)
  let mtype =
    try
      (List.find (fun msg_type_entry -> msg_type_entry.prefix = mname) msg_types).mtype
    with Not_found -> raise (Unknown_logtypename mname)
  in
    _get_entry mtype

let _do_print fmt printer data =
  Format.pp_open_hbox fmt () ;
  printer fmt data ;
  Format.pp_print_newline fmt () ;
  Format.pp_close_box fmt ();
  Format.pp_print_flush fmt ()

let log_print_err (mtype:logmsg_type) printer data =
  let entry = _get_entry mtype
  in
    _do_print entry.errFmt printer data

let log_string_err (mtype:logmsg_type) str =
  log_print_err mtype Format.pp_print_string str

let log_print_always (mtype:logmsg_type) printer data =
  if !verbosity_level > 0 then
    begin
      let entry = _get_entry mtype
      in 
      _do_print entry.fmt printer data
    end

let log_string_always (mtype:logmsg_type) str =
  log_print_always mtype Format.pp_print_string str

let log_print_norm (mtype:logmsg_type) printer data =
  let entry = _get_entry mtype
  in
    if (entry.loglevel>=1) || (!verbosity_level>2) then
      _do_print entry.fmt printer data

let log_string_norm (mtype:logmsg_type) str =
  log_print_norm mtype Format.pp_print_string str

let log_print_dbg (mtype:logmsg_type) printer data =
  let entry = _get_entry mtype
  in
    if (entry.loglevel>=2) then
      _do_print entry.fmt printer data

let log_string_dbg (mtype:logmsg_type) str =
  log_print_norm mtype Format.pp_print_string str

let log_print (level:logmsg_level) (mtype:logmsg_type) printer data =
  match level with
      LogErrors -> log_print_err mtype printer data
    | LogAlways -> log_print_always mtype printer data
    | LogNormal -> log_print_norm mtype printer data
    | LogDebug  -> log_print_dbg mtype printer data

let log_string (level:logmsg_level) (mtype:logmsg_type) str =
  match level with
      LogErrors -> log_string_err mtype str
    | LogAlways -> log_string_always mtype str
    | LogNormal -> log_string_norm mtype str
    | LogDebug  -> log_string_dbg mtype str

let is_normal_logging_enabled (mtype:logmsg_type) :bool =
  let entry = _get_entry mtype
  in
    entry.loglevel >= 1

let is_dbg_logging_enabled (mtype:logmsg_type) :bool =
  let entry = _get_entry mtype
  in
    entry.loglevel >= 2

let enable_logging_normal (typename:string) =
  let entry = _get_entry_by_name typename
  in
    entry.loglevel <- 1

let enable_logging_debug (typename:string) =
  let entry = _get_entry_by_name typename
  in
    entry.loglevel <- 2

let print_loghelp_msg () =
  Printf.fprintf stderr "To enable logging for a specified type, add -log <type> to the command line\n";
  Printf.fprintf stderr "To enable debug logging, add -logdbg <type> to the command line\n";
  Printf.fprintf stderr "The defined log types are:\n";
  Printf.fprintf stderr "%-10s %s\n" "TYPE" "DESCRIPTION";
  List.iter (fun defn ->
	       Printf.fprintf stderr "%-10s %s\n" defn.prefix defn.desc) msg_types;
  exit 0

let () = initialize ()
