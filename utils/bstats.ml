(*
 *
 * Copyright (c) 2001 by
 *  George C. Necula	necula@cs.berkeley.edu
 *  Scott McPeak        smcpeak@cs.berkeley.edu
 *  Wes Weimer          weimer@cs.berkeley.edu
 *   
 * All rights reserved.  Permission to use, copy, modify and distribute
 * this software for research purposes only is hereby granted, 
 * provided that the following conditions are met: 
 * 1. XSRedistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 3. The name of the authors may not be used to endorse or promote products 
 * derived from  this software without specific prior written permission. 
 *
 * DISCLAIMER:
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS 
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *)

(* Fraction of total time, of which unaccounted time should be to display the proper warning in the profile *)
(* By default, don't bother *)
let unacc_threshold = ref 1.0

                                        (* A hierarchy of timings *)
type t = { name : string;
           mutable time : float;
           mutable last_called_at : float;
           mutable ncalls : int;
           mutable sub  : t list}

                                        (* Create the top level *)
let top = { name = "TOTAL";
            time = 0.0;
            ncalls = 1;
            sub  = [];
	    last_called_at = 0.0}

                                        (* The stack of current path through 
                                         * the hierarchy. The first is the 
                                         * leaf. *)
let current : t list ref = ref [top]

let timefun : (unit -> float) ref = ref (fun () -> 0.0 )

let user_time () = (Unix.times ()).Unix.tms_utime
let user_sys_time () = let times = Unix.times () in times.Unix.tms_utime +. times.Unix.tms_stime

let noprofile = ref false

let measure_sys_time really noprof unacc_thr =
	let _ = if really
		then timefun := user_sys_time
		else timefun := user_time
	in
	noprofile := noprof;
	unacc_threshold := unacc_thr

let reset () =
	top.sub <- [](*;*)
	(*measure_sys_time false*)



let print chn msg = if !noprofile then Printf.fprintf chn "Profiling is off (-noprofile).\n" else begin
  (* Total up *)
  top.time <- List.fold_left (fun sum f -> sum +. f.time) 0.0 top.sub;
  let rec prTree ind node = 
    Printf.fprintf chn "%s%-20s          %6.3f s (%d)\n" 
      (String.make ind ' ') node.name node.time node.ncalls ;
    (* We reverse the sub list to print stats in the order they were first called.  To optimize, we were adding newly called subroutines to the beginning of the list, and now we should reverse it to get the natural order of calls *)
    List.iter (prTree (ind + 2)) (List.rev node.sub);
    (* Print time that was unaccounted--only if it exceeds the threshold *)
    if List.length node.sub > 0 then
      let unaccounted = node.time -. (List.fold_left (fun a sub -> a +. sub.time) 0.0 node.sub) in
      if (unaccounted >= (!unacc_threshold *. node.time)) && (unaccounted >= 0.001) then
	Printf.fprintf chn "%s%-20s          %6.3f s (%d%%)\n"
	  (String.make (ind+2) ' ') "UNACCOUNTED" unaccounted (int_of_float (unaccounted /. node.time *. 100.0))
  in
  Printf.fprintf chn "%s" msg;
  List.iter (prTree 0) [ top ]
end

let push_profile str = if !noprofile then () else begin
  let stat : t =
    let curr = List.hd !current in
    let rec loop = function
        h :: _ when h.name = str -> h
      | _ :: rest -> loop rest
      | [] ->
          let nw = {name = str; time = 0.0; ncalls = 0; sub = []; last_called_at = 0.0} in
            curr.sub <- nw :: curr.sub;
            nw
    in
    loop curr.sub
  in
  current := stat :: !current;
  (* record startup time *)
  stat.last_called_at <- !timefun ()
end

let pop_profile str = if !noprofile then () else begin
  let finish = !timefun () in
  let stat :: tail = !current in
  assert (stat.name = str);
  stat.time <- stat.time +. finish -. stat.last_called_at;
  stat.ncalls <- stat.ncalls + 1;
  current := tail
end

let time str f arg = if !noprofile then f arg else begin
  push_profile str;
  let res = try (f arg)
    with x -> begin
      pop_profile str;
      raise x;
    end
  in
  pop_profile str;
  res
end

