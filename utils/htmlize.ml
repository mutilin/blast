let write_list_to_file fname string_list =
  let oc = open_out fname in
    List.iter (output_string oc) string_list;
    close_out oc
;;

(* ambitious -- take a text file and return a list of lines that is the file *)
let string_list_of_file fname = 
  let ic  = open_in fname in
  let doneflag = ref false in
  let listoflines = ref [] in
    while not (!doneflag) do
      try
	listoflines := (input_line ic)::(!listoflines)
      with
	  End_of_file -> doneflag := true
    done;
    close_in ic;
  List.rev !listoflines
;;

let allblank s = 
let rec _allblank i = 
	if i = String.length s then true
	else if String.get s i <> ' ' then false
	else _allblank (i+1)
in
	_allblank 0
;;


let f s = 
	if allblank s then false 
	else not (String.get s 0 = '#') 
;;

let make_line_string s = 
  let reg_lt = Str.regexp "<" in
  let reg_gt = Str.regexp ">" in
  let s' = Str.global_replace reg_lt " &lt; " s in
  let s'' = Str.global_replace reg_gt " &gt; " s' in
  Printf.sprintf "<tr class=\"code\"><td><pre>%s</pre></td></tr>" s''
  
let make_html_string s1 s2 = 
Printf.sprintf 
"<html>
  <head><title>%s</title><style type=\"text/css\" media=\"screen\">
@import \"trace.css\";
</style>
<script src=\"trace.js\" type=\"text/javascript\"></script>
</head>
<body><table>
%s
</table>
</body>
</html>"
s1 s2

let main () = 
  let fname = Sys.argv.(1) in
  let lines = string_list_of_file fname in
  let lines' = String.concat "\n" (List.map make_line_string lines) in
  let outs = make_html_string fname lines' in 
  output_string stdout outs
(*    (write_list_to_file fname lines') *)
;;

main ()
