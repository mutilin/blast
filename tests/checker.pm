
# Plugin for collect_reports.pl script from ldv
# Attach it like this: /path/to/collect_reports.pl --plugin=checker.pm --filters=blast_autotest

sub blast_autotest
{
	return [100,sub {
		my $l=shift;
		$l=~/BLAST TEST SYSTEM VERDICT: PASSED/ and return {'AUTOTEST'=>'PASSED'};
		$l=~/BLAST TEST SYSTEM VERDICT: FAILED/ and return {'AUTOTEST'=>'FAILED'};
		!defined $l and return {'AUTOTEST'=>'UNKNOWN'};
		return undef;
	}];
}

sub compete_score
{
	return [100,sub {
		my $l=shift;
		defined ($l=~/COMPETITION SCORE: ([-]?\d+)/) and return {'SCORE'=>$1};
		return undef;
	}];
}

# For unexpected result, detects whether it's positive (failed test started to pass) or negative.
sub blast_autotest_expected
{
	my $autotest_result = '';
	my $regression_result;
	my $result_sub = blast_autotest()->[1];
	return [100,sub {
		my $l=shift;
		if (my $r = $result_sub->($l)){
			$autotest_result ||= $r->{'AUTOTEST'}
		}
		$l=~/AT THE CURRENT STATE OF ART: SHOULD BE LIKE THAT/ and $regression_result = 'EXPECTED';
		$l=~/AT THE CURRENT STATE OF ART: SHOULD NOT BE LIKE THAT/ and $regression_result = 'UNEXPECTED';
		if ($autotest_result && $regression_result){
			return {'REGRESSION' => 'EXPECTED'} if $regression_result eq 'EXPECTED';
			return {'REGRESSION' => 'POSITIVE'} if $regression_result ne 'EXPECTED' && $autotest_result eq 'PASSED';
			return {'REGRESSION' => 'NEGATIVE'} if $regression_result ne 'EXPECTED' && $autotest_result ne 'PASSED';
		}
		return undef;
	}];
}


1;

