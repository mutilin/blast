# Utilites for working with blast via makefiles

# Sanity checking
ifndef TIMEOUT_PROG
$(error please, specify timeout program in $$TIMEOUT_PROG env variable!)
endif

ifndef BLAST
blast_default=ocamltune pblast.opt
$(warning BLAST variable is not defined!  Using default value $(blast_default))
BLAST=$(blast_default)
endif

# CALLING BLAST
# Options for blast script
# Function to run blast
# 	$(call blast_cmd,options)
define blast_cmd
	$(BLAST) $(1)
endef

# Function to run collect_reports
# 	$(call collect_reports_cmd,arguments)
define collect_reports_cmd
	./collect_reports.pl $(1)
endef


# COMMAND ECHOING
# Do not print commands by default
blast_echo?=no

ifeq ($(blast_echo),debug)
suppress_echo=

else ifeq ($(blast_echo),no)
suppress_echo=@

else
$(error Incorrect value of blast_echo=$(blast_echo))
endif


# OPTIONS CONTROLLING RESOURCES
#timeout support
TIMEOUT_PROG?=$(LDV_HOME)/shared/sh/timeout
#timeout in seconds.  Can't be empty
TIMEOUT?=900
#Memory limit in Kb
MEMLIMIT?=1000000

#Function to enforce resource restrictions
#	$(call limit_cmd,command_to_limit[,time[,memory]])
define limit_cmd
$(TIMEOUT_PROG) $(if $(2),-t $(2),) $(if $(3),-m $(3),) -p '.*pblast\.opt.*,CORE;.*,ALL;CHILD:.*smtlibServer.*,SMT_SOLVER;CHILD:.*csisatServer.*,PROVER' $(1)
endef

# Archive support.  Traces are big so we mostly want to archive them.
# archivate is a funciton.  Call syntax: $(call archivate,command_whose_output_to_archivate)
ifdef noarchive
ARCHIVED=
archive_suffix=
archivate_cmd= ( $(1) ) >$@
else
# Gzip is used because it's FASTER that bzip2
ARCHIVED=.gz
archive_suffix=.gz
# I'll use a taskset hack.  It seems that if I assign archiver to processor #0, then the scheduler
# will tend to split blast and archiver into two processors.
archivate_cmd= ( $(1) ) | taskset -c 0 gzip >$@
endif

# PREPROCESSING
# We don't use CPP here as it'll resolve in "cc -E", and this will NOT preprocess input files if they end with .i extension!
define cpp
cpp $(1)
endef

define sed
$(shell echo '$(1)' | sed $(2))
endef


