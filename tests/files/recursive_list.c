
/* A recursive list.  Usual -cldepth can't process it, as closing under recursive fields is prohibited.
 * To win this example, you need -iclos. */

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
#endif


void err()
{ ERROR: goto ERROR; }

struct list {
	int n;
	struct list *next;
};

// "Naive" implementation of malloc() without theory for pointer inequalities
// We do not name it "malloc", as the reasoning becomes inconsistent: BLAST will think that malloc has returned the same address for both append calls.
int i = 1;
void * allocate_memory()
{
	return (void*)i++;
}

struct list* append(struct list *l, int n)
{
	struct list *new_el;

	new_el = allocate_memory();

	new_el->n = n;
	new_el->next = l;

	return new_el;
}

int main()
{
	struct list *l,m;
	l = &m;
	l->next = 0;
	l->n = 0;

	l = append(l, 1);
	l = append(l, 2);

#ifdef BLAST_AUTO_1
	if (l->next->next->n != 0) err();
#else
	if (l->next->next->n == 0) err();
#endif

}

