#include <assert.h>
#include "1_3.h"

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_UNSAFE;
int CURRENTLY_SAFE;
#endif

/*
	getPtr returns structure rr with field state, initialized to 1
	freePtr sets field state to 2
	second call to freePtr should fail on assert(ptr -> state == 1);
*/

/* The reason why it fails is that all the pointers shown here are not treated as aliasing each-other.
 * There is no value on stack they all may point to, and __undefrr()'s return value is not considered as such.
 * While -ialias lets us solve this problem on backward trace analysis, the subsequent forward analysis doesn't use -ialias, and incorrectly rules out the error in the second freePtr call
 */

rr * getrr()
{
	rr * r = __undefrr();
	r -> state = 0;
	return r;
}

rr * getPtr()
{
	rr * r = getrr();
	r -> state = 1;
	return r;
}

void freePtr(rr * ptr)
{
	assert(ptr -> state == 1);
	ptr -> state = 2;
}

int main()
{
	rr * ptr1 = 0;
	ptr1 = getPtr();
	freePtr(ptr1);
#ifndef BLAST_AUTO_1
	freePtr(ptr1);
#endif
	return 0;
}
