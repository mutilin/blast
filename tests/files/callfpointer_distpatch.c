/*
Call function by pointer with a conditional distpatching.

Currently, it yields correct formulae with -fp option, but doesn't realize that addresses of check_two and check_three are different!
*/

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_UNSAFE;
#else
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
#endif

void err()
{
	ERROR: goto ERROR;
}

void check_two(int i)
{
	if (i != 2) err();
}

void check_three(int t)
{
	if (t != 3) err();
}

int nondet();

int main(void) {
	int x;
	void (*fp)(int);

	if (nondet()){
		x = 2;
		fp = &check_two;
	}else{
		x = 3;
#ifdef BLAST_AUTO_1
		fp = &check_three;
#else
		fp = &check_two;
#endif
	}

	// Perform the check
	(*fp)(x);

	return 0;
}
