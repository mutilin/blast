/* How mutex locking rule would look like */

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
#endif

void err()
{ ERROR: goto ERROR; }

struct mutex {
	int is_locked;
};

void mutex_lock(struct mutex *a)
{
	if (a->is_locked == 1) err();
	a->is_locked = 1;
}

void mutex_unlock(struct mutex *b)
{
	if (b->is_locked != 1) err();
	b->is_locked = 0;
}

int main()
{
	struct mutex m;
	m.is_locked = 0;

	mutex_lock(&m);
#ifdef BLAST_AUTO_1
	mutex_unlock(&m);
#else
	mutex_lock(&m);
#endif

}

