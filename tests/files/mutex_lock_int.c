/* How mutex locking rule would look like--simplified */


#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
#endif

void err()
{ ERROR: goto ERROR; }

void mutex_lock(int *a)
{
	if (*a == 1) err();
	*a = 1;
}

void mutex_unlock(int *b)
{
	if (*b != 1) err();
	*b = 0;
}

int main()
{
	int m;
	m = 0;

	mutex_lock(&m);
#ifdef BLAST_AUTO_1
	mutex_unlock(&m);
#else
	mutex_lock(&m);
#endif

}

