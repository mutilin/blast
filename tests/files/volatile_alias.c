// Bug #260 - int is not aliased with volatile int
// RESULT: yes, it flows through.

#ifdef BLAST_AUTO_1
// without volatile
#define volatile
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
// with volatile
int VERDICT_SAFE;
int CURRENTLY_UNSAFE;
#endif

int main()
{
        int volatile a = 4;
        int * const p = &a;
        p = &a;
        a = a - 4;
        if (*p != 0){
                ERROR: goto ERROR;
        }
        return 0;
}

