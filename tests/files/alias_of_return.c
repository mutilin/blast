

/* Make sure that aliases propagate via return (case one, simple function with unchanged return) */

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#endif


void err()
{ ERROR: goto ERROR; }


int * return_self (int * p)
{
	return p;
}

int main()
{
	int a,*q;

	a = 1;
#ifdef BLAST_AUTO_1
	q = &a;
#else
	q = return_self(&a);
#endif

	*q = 2;

	if (a != 2) err();
}

