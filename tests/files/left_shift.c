/* Check that left shifts are converted into multiplications *both* at interpolation *and* at forward exploration */
void err() {ERROR: goto ERROR;}

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#endif

int main()
{
	int x,y;

	y = x;

#ifdef BLAST_AUTO_1
	if ((y & (1<<15)) == 0)
#else
	if ((x & (1<<15)) == 0)
#endif
	if ((x & (1<<15)) != 0)
			err();
}
