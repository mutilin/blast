

/* Make sure that aliases propagate via return.
 * Case two, function with a return changed by CIL. */

#ifdef BLAST_AUTO_1
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#else
int VERDICT_SAFE;
int CURRENTLY_SAFE;
#endif


void err()
{ ERROR: goto ERROR; }

int __nondet();

int * return_self (int * p)
{
	if (__nondet()){
		return p;
	}else{
		return p;
	}
}

int main()
{
	int a,*q;

	a = 1;
#ifdef BLAST_AUTO_1
	q = &a;
#else
	q = return_self(&a);
#endif

	*q = 2;

	if (a != 2) err();
}

