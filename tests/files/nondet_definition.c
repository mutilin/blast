/* This test demonstrates a bug in BLAST found during SV-COMP */
/* If a nondetermined function is defined like this:

	int __VERIFIER_nondet_int(void) { int x; return x; }

BLAST thinks that it always returns the equivalent value.  Instead, BLAST should think that each call returns an arbitrary value. */

#ifdef BLAST_AUTO_1
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
int __VERIFIER_nondet_int(void) { int x; return x; }
#else
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;
extern int __VERIFIER_nondet_int(void);
#endif

int main()
{
	int a,b;
	a = __VERIFIER_nondet_int();
	b = __VERIFIER_nondet_int();
	if (a != b) {
		ERROR: goto ERROR;
	}
}


