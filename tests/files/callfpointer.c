/*
system is unsafe.  With -fp option, BLAST should realize this.
*/
int VERDICT_UNSAFE;
int CURRENTLY_UNSAFE;

void f(void g(int)) {
	g(1);
}

void h(int i) {
	if(i==1) {
		ERROR: goto ERROR;
	} else {
		//ok
	}
}
int main(void) {
	f(h);
	//h(0);
	return 0;
}
