# -*- Mode: makefile -*-
# Copyright (c) 2001-2002, 
#  George C. Necula    <necula@cs.berkeley.edu>
#  Scott McPeak        <smcpeak@cs.berkeley.edu>
#  Wes Weimer          <weimer@cs.berkeley.edu>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. The names of the contributors may not be used to endorse or promote
# products derived from this software without specific prior written
# permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  # Generic Makefile for Ocaml projects
  # Written by necula@cs.berkeley.edu
  # 
  # Features: 
  #   - keeps byproducts of building in a separate directory
  #   - handles dependencies automatically
  #   - user specifies just what modules go into a project and 
  #     everything else is done automatically
  #   - you can use one Makefile for several Ocaml projects
  #   
  # You must include this file in your Makefile. Before the include point 
  # you must defined the following variables (which are glob al for all Ocaml 
  # projects specified in one Makefile):
  # 
  # OBJDIR     - the directory where to put all object files. This directory 
  #              must exist (default obj)
  # DEPENDDIR  - the directory where to put dependency files. This directory 
  #              must exist.  (default obj/.depend)
  # NATIVECAML - if set then will use the native compiler
  # UNSAFE     - if set then will turn off safety checks (only with NATIVECAML)
  # PROFILE    - if set then it will compile and link with "gprof" profiling
  #              support (NATIVECAML mode only)
  # STATIC     - if set then it will compile and link statically
  #              (NATIVECAML mode only)

  # MODULES    - a list of all modules for all projects defined in the 
  #              Makefile. Give only the basenames (no directory, 
  #              no extension).  This is used to create the dependencies.
  # SOURCEDIRS - a list of all directories containing sources for all 
  #              projects defined in a Makefile. This is used to set vpath.
  # MLLS       - a list of all .mll (ocamllex input) files for all 
  #              projects defined in the Makefile. 
  # MLYS       - a list of all .mly (ocamlyacc input) files for all 
  #              projects defined in the Makefile. 
  # ECHO       - if specifically set to nothing then it will print 
  #              all of the commands issued. Set this in the command line
  #              if you want to see what is going on.
  #
  # COMPILEFLAGS      - if defined, then it is passed as argument to ocamlc
  #                     and ocamlopt
  #
  #
  # After you set all of the above you must do the following for EACH separate 
  # executable that you want to build.
  #
  # Define the following:
  # PROJECT_EXECUTABLE - the name of the executable you want to build. To take 
  #                      advantage of the naming scheme that separates the 
  #                      bytecode version and the native version, use the 
  #                      $(EXE) variable which is defined to either .byte.exe 
  #                      or .asm.exe. I typically put the executable in 
  #                      $(OBJDIR) as well.
  # PROJECT_MODULES    - the base names of the modules that make this 
  #                      executable in the order in which they must be
  #                      passed to the linker. Make sure that all of
  #                      the names mentioned here are also mentioned in 
  #                      MODULES.
  # PROJECT_CMODULES   - same as modules but for the C modules. These
  #                      do not need to be mentioned in MODULES
  # PROJECT_LIBS       - the base names of the libraries that you 
  #                      want to link in the executable.
  #
  #
  # Then include Makefile.ocaml.build to generate a customized
  # rule for making your executable.
  #
  # Example:
  # 
  # OBJDIR     = obj
  # DEPENDDIR  = obj/.depend
  # SOURCEDIRS = src src/special
  # MLLS       = mylex 
  # MLYS       = myparse
  #
  # MODULES    = mod11 mod12 mod21 modcommon
  #
  #  # Rules for project 1
  # PROJECT_EXECUTABLE = $(OBJDIR)/proj1$(EXE)
  # PROJECT_MODULES    = mod11 mod12 modcommon
  # PROJECT_CMODULES   =  
  # PROJEC_LIBS        = unix
  # include Makefile.ocaml.build
  #
  #
  #  # Rules for project 2 
  # PROJECT_EXECUTABLE = $(OBJDIR)/proj2$(EXE)
  # PROJECT_MODULES    = mod21 modcommon
  # PROJECT_CMODULES   =  
  # PROJEC_LIBS        = unix str
  # include Makefile.ocaml.build


##################################################################
## Rupak: This is from config.make. Needs to be cleaned up.
SHELL        = /bin/sh
C_C          = gcc
CPP_C        = g++

# To make the foci stuff working under OS different from linux.
ifeq ($(OSTYPE), linux)
  C_CPP_OPT = -O3
else
  C_CPP_OPT = -O1
endif

ifdef DEBUG
  C_CPP_FLAGS  = $(C_CPP_OPT) -g
endif

C_LD         = $(C_C)
CPP_LD       = $(CPP_C)

C_YACC       = bison
C_LEX        = flex

AR           = ar
RANLIB       = ranlib

OCAML_C  = $(shell if which ocamlc.opt 2> /dev/null > /dev/null ; then echo ocamlc.opt ; else echo ocamlc ; fi)
#OCAML_C      = ocamlc -dtypes
OCAML_OPT_C  = $(shell if which ocamlopt.opt 2> /dev/null > /dev/null ; then echo ocamlopt.opt; else echo ocamlopt; fi)
#OCAML_OPT_C  = ocamlopt
OCAML_LD     = $(OCAML_C)
OCAML_OPT_LD = $(OCAML_OPT_C)
ifndef DEBUG
OCAML_C_FLAGS     =
OCAML_OPT_C_FLAGS = -noassert
else
OCAML_C_FLAGS     = -g
OCAML_OPT_C_FLAGS =
endif
ifdef PROFILE
OCAML_OPT_C_FLAGS += -p
OCAML_OPT_LD += -p
endif
OCAML_MKTOP  = ocamlmktop
OCAML_CP     = ocamlcp
OCAML_DEP    = ocamldep
OCAML_LEX    = ocamllex
OCAML_YACC   = ocamlyacc

OCAML_C_CPP_INC = -I $(shell $(OCAML_C) -v | tail -1 | sed -e \
			's/^Standard library directory: //')
##################################################################


CAMLFLAGS    =$(OPT_FLAGS) -I $(OBJDIR)

# sm: two styles for echoing compilation progress:
#   style 1, by George:
#     - print English descriptions of what's happening
#     - set ECHO to "" to see *everything*
#   style 2, by Scott:
#     - do not print English descriptions
#     - print every shell command that is executed which has a side effect,
#       so that they could be pasted into a shell to reproduce manually
#     - omit some of the details of dependency generation
#
# to be able to choose which style, several variables are used:
#   @$(NARRATIVE)  - put this before English descriptions for style 1
#   @$(COMMAND)    - put this before shell commands which are to be
#                    printed for style 2; the command is *not* executed
#   $(AT)          - put this before shell commands which are to be executed,
#                    and also printed in style 2
#   $(ECHO)        - use in place of '@' for things not printed in either style
ifdef ECHOSTYLE_SCOTT
  # 'true' silently consumes its arguments, whereas 'echo' prints them
  NARRATIVE   := true           
  COMMAND     := echo
  AT          := 
  ECHO        := @
else
  NARRATIVE   := echo
  COMMAND     := true
  # change these next two definitions to <empty> to echo everything,
  # or leave as @ to suppress echoing
  AT          := @
  ECHO        := @
endif



ifdef NATIVECAML
 ifdef PROFILE
   COMPILEFLAGS   += -p
   LINKFLAGS      += -p
 endif
 ifdef STATIC
   COMPILEFLAGS   += -ccopt -static
   LINKFLAGS      += -ccopt -static
 endif
 #foo := $(shell echo "I am in NATIVECAML mode" >&2; echo whatever)
 CAMLC          = $(OCAML_OPT_C) $(COMPILEFLAGS)
 CAMLLINK       = $(OCAML_OPT_C) $(LINKFLAGS)
 CMO            = cmx
 CMC            = opt.o  # compiled (and optimized) C
 CMXA           = cmxa
 EXEEXT         = .asm
 MOVEAFTERCAMLC = cmi cmx $(OBJ)
 COMPILETOWHAT  = native code
 # sm: by adding -native in native mode, we prevent spurious
 # dependencies on .cmo files which were causing lots of
 # extra recompilation
 OCAML_DEP        = ocamldep -native
 CAMLFLAGS      += -ccopt -O3
else 
 CAMLC          = $(OCAML_C) -dtypes -g $(COMPILEFLAGS)
 CAMLLINK       = $(OCAML_C) -dtypes -custom -g $(LINKFLAGS)
 CMO            = cmo
 CMXA           = cma
 CMC            = o
 EXEEXT         = .byte
 MOVEAFTERCAMLC = cmi cmo
 COMPILETOWHAT  = bytecode
endif


ifdef UNSAFE 
 CAMLC          := $(CAMLC) -noassert
endif

OBJ             = o
EXE		= $(EXEEXT).exe


export EXE



    # Allow searching for .ml and .mli
vpath %.mll $(SOURCEDIRS)
vpath %.mly $(SOURCEDIRS)
vpath %.ml  $(SOURCEDIRS) $(OBJDIR)
vpath %.mli $(SOURCEDIRS) $(OBJDIR)
vpath %.c   $(SOURCEDIRS)


#  Secondaries are intermediates that we don't want make to delete
#  By giving the right names to secondary files we tell make where to make
#  them if they are not already made. VERY USEFUL!!
.SECONDARY : $(MLLS:%.mll=$(OBJDIR)/%.ml) $(MLYS:%.mly=$(OBJDIR)/%.ml) \
             $(MLYS:%.mly=$(OBJDIR)/%.mli)

             # Run the lexer generator
             # Move the result to the OBJDIR directory
             # If there is a .mli file in the same directory with .mll then
             # copy it to OBJDIR (where the .ml) file will live.
$(OBJDIR)/%.ml: %.mll
	$(OCAML_LEX) $<
	$(AT)mv -f $(basename $<).ml $(OBJDIR)
	$(ECHO)if test -f $(basename $<).mli ;then \
	  $(COMMAND) cp -f $(basename $<).mli $(OBJDIR); \
	  cp -f $(basename $<).mli $(OBJDIR) \
        ;fi

             # Run the parser generator
             # Move the result to the $(OBJDIR) directory.
$(OBJDIR)/%.ml $(OBJDIR)/%.mli: %.mly
	$(OCAML_YACC) $(CAMLYACCFLAGS) $<
	$(AT)mv -f $(basename $<).ml $(basename $<).mli $(OBJDIR)

         # Compile an MLI file. After compilation move the result to OBJDIR
$(OBJDIR)/%.cmi: %.mli
	@$(NARRATIVE) Compiling interface $<
	$(AT)$(CAMLC) $(CAMLFLAGS) -c $<
	$(ECHO)if test $(OBJDIR) != $(<D) ;then \
                   $(COMMAND) mv -f $(basename $<).cmi $(OBJDIR); \
                   mv -f $(basename $<).cmi $(OBJDIR) \
        ;fi

         # Compile an ML file. After compilation we 
         # copy to $(OBJDIR) the .cmi and the result of compilation.
$(OBJDIR)/%.$(CMO): %.ml
	@$(NARRATIVE) "Compiling           $< to $(COMPILETOWHAT)"
	$(ECHO)# if test $(OBJDIR) != $(<D) -a -f $(OBJDIR)/$(basename $(<F)).cmi ;then \
        #   $(COMMAND) mv -f $(OBJDIR)/$(basename $(<F)).cmi $(<D); \
        #   mv -f $(OBJDIR)/$(basename $(<F)).cmi $(<D); \
        #fi
	@$(COMMAND) $(CAMLC) $(CAMLFLAGS) -c $<
	$(ECHO)$(CAMLC) $(CAMLFLAGS) -c $< ; res=$$?; \
	   if test $(OBJDIR) != $(<D) ;then \
              for ext in $(MOVEAFTERCAMLC); do \
                if test -f $(basename $<).$$ext ;then \
                  $(COMMAND) mv -f $(basename $<).$$ext $(OBJDIR); \
                  mv -f $(basename $<).$$ext $(OBJDIR); \
                fi; \
              done; \
           fi; exit $$res

             # Compile C files
             # They appear to be left in the current directory as .o files
$(OBJDIR)/%.$(CMC): %.c
	@$(NARRATIVE) Compiling C file $<
	$(AT)$(CAMLC) $(CAMLFLAGS) -c $< -o $@
	$(AT)mv -f $(basename $(notdir $<)).$(OBJ) $@



#	Phonies should be "remade" even if someone mistakenly creates them
.PHONY: default-clean
default-clean:
	-find obj \( \
		-name '*.cmi' -o \
		-name '*.cmo' -o \
                -name '*.cmx' -o \
		-name '*.cma' -o \
		-name '*.cmxa' -o \
		-name '*.exe' -o \
		-name '*.o' -o \
		-name '*.a' -o \
		-name '*.ml' -o \
		-name '*.mli' -o \
		-name '*.annot' -o \
		-name '*.obj' \
	\) -exec rm {} \;
	-find . \( \
		-name '*.annot' \) -exec rm {} \; 
	-find obj \( \
		-name '*.annot' \) -exec rm {} \; 
	-rm -f $(DEPENDDIR)/*.d $(DEPENDDIR)/*.di
	-rm -f $(MLLS:%.mll=$(OBJDIR)/%.ml) \
               $(MLLS:%.mll=$(OBJDIR)/%.mli) \
               $(MLYS:%.mly=$(OBJDIR)/%.ml) \
               $(MLYS:%.mly=$(OBJDIR)/%.mli)


# Automatic dependency generation (see GNU info for details)
#
# Each .ml file has a .d (dependency file) which is automatically
# generated and included by the rules below.  The perl script replaces
# directory paths with $(OBJDIR)/
#
# Dependencies for .mli files reside in corresponding .di files.
#

FIXDEPEND:=perl -e 'while(<>) { s%[^/\\ :]+[/\\]% %g; s%([-a-zA-Z0-9+-.:/\/_]+)%\$$(OBJDIR)/$$1%g; print $$_;}'
# FIXDEPEND:=cat
DEPINCLUDES= -I $(OBJDIR) $(SOURCEDIRS:%=-I %)
$(DEPENDDIR)/%.d: %.ml
	@$(NARRATIVE) Generating dependency information for $<
	@$(COMMAND) $(OCAML_DEP) $(DEPINCLUDES) $<
	$(ECHO)$(OCAML_DEP) $(DEPINCLUDES) $< | $(FIXDEPEND) > $@

$(DEPENDDIR)/%.di: %.mli
	@$(NARRATIVE) Generating dependency information for $<
	@$(COMMAND) $(OCAML_DEP) $(DEPINCLUDES) $<
	$(ECHO)$(OCAML_DEP) $(DEPINCLUDES) $< | $(FIXDEPEND) > $@

# sm: it turns out there's a variable which lists all the goals
# specified on the command line; I'll use this to set CLEANING
# (which is not set anywhere else, currently)
ifeq ($(MAKECMDGOALS),clean)
  #$(warning "Skipping dependency rules because we're cleaning")
  CLEANING := 1
endif

ifndef CLEANING
-include $(MODULES:%=$(DEPENDDIR)/%.d)
-include $(MODULES:%=$(DEPENDDIR)/%.di)
endif

