(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
type token =
  | Id of (string)
  | Const of (string)
  | Num of (int)
  | GLOBAL
  | LOCAL
  | ENV
  | STORE
  | PROC
  | PREDICATES
  | ANNOTATIONS
  | SEMICOLON
  | COLON
  | EOF
  | LPAREN
  | RPAREN
  | LINDEX
  | RINDEX
  | QUESTION
  | DOT
  | ARROW
  | BITAND
  | BITOR
  | DIV
  | LSHIFT
  | MINUS
  | MUL
  | PLUS
  | REM
  | RSHIFT
  | XOR
  | EQ
  | GE
  | GT
  | LE
  | LT
  | NE
  | INDEX
  | AND
  | OR
  | COMMA
  | NOT
  | BITNOT
  | DEREF
  | ADDRESS
  | SYMBOL
  | UMINUS
  | UPLUS
  | DOTSTAR
  | ARROWSTAR
  | ICR
  | DECR
  | SIZEOF
  | ASSIGN
  | MULTassign
  | DIVassign
  | MODassign
  | PLUSassign
  | MINUSassign
  | LSassign
  | RSassign
  | ANDassign
  | XORassign
  | ORassign
  | INT
  | CHAR
  | FLOAT
  | DOUBLE
  | LONG
  | VOID
  | CONSTANT

val fun_main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> ((string * Ast.Predicate.predicate list) list)
val main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> (Ast.Predicate.predicate list)
val env_assumptions :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> ((Ast.Expression.lval * (Ast.Predicate.predicate)) list)
val lval_list :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> (Ast.Expression.lval list)
val modifies_info :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> ((int * (Ast.Expression.lval list)) list ) * ((string * (Ast.Expression.lval list)) list) * ((Ast.Expression.lval * (string list)) list) * (( (int * int * int * int) * Ast.Expression.lval ) list)
