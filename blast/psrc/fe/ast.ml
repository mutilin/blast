(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)


(**
  * This module allows the parsing of C programs.
*)

module Symbol = 
struct 
  type symbol = string (* change later *)
  type t = symbol

  let compare = Pervasives.compare

  let toString s = s       (* name of symbol *)

  let print fmt s =
    Format.pp_print_string fmt s
end

type modifier = Static | Abstract | Final | Public | Protected | Private
		| Strictfp (* what is Strictfp? *)
		| Native | Synchronized | Transient | Volatile
		| Interface (* Wes did not have interface as a modifier, but Soot has it *)

module JavaType =
struct
  type type_or_void = (* method result type *) 
    | Type of java_type
    | Void
	
  and java_type = 
      Primitive of primitive_type
    | Reference of reference_type
	
  and primitive_type = 
      BooleanType | CharType | ByteType | ShortType | IntType | LongType 
    | FloatType | DoubleType 
    | VoidType (* For C *)
	
  and reference_type = 
    | ClassOrInterfaceType of Symbol.symbol
    | ArrayType of java_type (* base element type *) * int32 (* number of dimensions *)
    | NullType (* Soot defines a type for Java's null *)
	
  let primToString ptyp =
    match ptyp with
	BooleanType -> "boolean" 
      | CharType -> "char"
      | ByteType -> "byte" 
      | ShortType -> "short"
      | IntType -> "int" 
      | LongType -> "long"
      | FloatType -> "float" | DoubleType -> "double"
      | VoidType -> "void"
	  
  let primPrint fmt ptyp =
    match ptyp with
	BooleanType -> Format.pp_print_string fmt "boolean" 
      | CharType -> Format.pp_print_string fmt "char"
      | ByteType -> Format.pp_print_string fmt "byte" 
      | ShortType -> Format.pp_print_string fmt "short"
      | IntType -> Format.pp_print_string fmt "int" 
      | LongType -> Format.pp_print_string fmt "long"
      | FloatType -> Format.pp_print_string fmt "float" | DoubleType -> Format.pp_print_string fmt "double"
      | VoidType -> Format.pp_print_string fmt "void"
	  
  let rec toString t = 
    match t with
	Primitive ptyp -> primToString ptyp
      | Reference rtyp -> refToString rtyp 
  and refToString r =
    match r with
	ClassOrInterfaceType s -> s
      | ArrayType (jt, i) -> (toString jt)^(Int32.to_string i)
      | NullType -> "null"
	  
  let rec print fmt t = 
    match t with
	Primitive ptyp -> primPrint fmt ptyp
      | Reference rtyp -> refPrint fmt rtyp
  and refPrint fmt r =
    match r with
	ClassOrInterfaceType s -> Format.pp_print_string fmt ("class "^s)
      | ArrayType (jt, i) -> (print fmt jt) ; Format.pp_print_string fmt (Int32.to_string i)
      | NullType -> Format.pp_print_string fmt "null"
	  
end

(* The variable undefinedVariable is used to encode nondeterministic
   choice. If it appears in an If, then the predicate is discarded
   and the If can nondeterministically branch in either direction.

   See also build_cfa, where If statements are checked for this variable, and
   replaced by nondeterministic choice.
*)
let undefinedVariable = "__BLAST_NONDET" (* formerly, arbit *)

module Constant =
struct
  type constant = 
      Int     of int		(* SLAM uses int32?? so that we get all 32 bits *)
    | Long    of int64
    | Float   of float
    | String  of string
    |	EnumC   of int * string (* SLAM uses int32 *)
    |	Null                    (* corresponds to Java's null *)

  let toString c =
    match c with
	Int i -> string_of_int i
      | Long l -> Int64.to_string l
      |	Float f -> string_of_float f
      |	String s -> Printf.sprintf "\"%s\"" (String.escaped s)
      |	EnumC (i,name) -> Printf.sprintf "%s(%s)" name (string_of_int i)
      | Null -> "null"
end

module Expression = 
struct
  type access_op = Dot | Arrow
      
  type assign_op = Assign | AssignBitAnd | AssignBitOr | AssignDiv | AssignLShift | AssignMinus
                   | AssignMul | AssignPlus | AssignRem | AssignRShift | AssignXor
			 
			 
  type unary_op  = UnaryMinus | Not | BitNot | Address | UnaryPlus | DestructorCall | Assume | Assert
                   | PreInc | PreDec | PostInc | PostDec

  type binary_op = BitAnd | BitOr | Div | LShift | Minus | Mul | Plus | Rem | RShift | Xor 
                   | Eq | Ge | Gt | Le | Lt | Ne | Index | And | Or | Comma 
                   | FieldOffset | Offset
		       
  type lval =
      Symbol of Symbol.symbol
    | This
    | Access of access_op * expression * Symbol.symbol
    | Dereference of expression
    | Indexed of expression * expression

  and expression =
      Lval of lval
    | Chlval of lval * string (* changed lvals to help implement skolemization in wp, sp computation *)
    | Assignment of assign_op * lval * expression (* op, target, value *)
    | Binary of binary_op * expression * expression
    | Cast of JavaType.java_type * expression
    | CastCil of Cil.typ * expression 
	(* hack to distinguish from Manav's stuff *)
    | Constant of Constant.constant
    | Constructor of Symbol.symbol * JavaType.java_type
    | MethodCall of expression * expression * expression * expression * (expression list)
    | FunctionCall of expression * expression list
    | Select of expression * expression
    | Store of expression * expression * expression
    | Sizeof of int
    | Unary of unary_op * expression
    | InstanceOf of expression * JavaType.java_type
	

  type t = expression

  let compare = Pervasives.compare

  let unaryOpToString = function
      UnaryMinus     -> "-"
    | Not            -> "!"
    | BitNot         -> "~"
    | Address        -> "&"
    | UnaryPlus      -> "+"
    | DestructorCall -> "Destruct"
    | Assume         -> "__assume"
    | Assert         -> "__assert"
    | PreInc       -> "++"
    | PreDec       -> "--"
    | PostInc      -> "++"
    | PostDec      -> "--"

  let unaryOpFromString = function
    | "-" -> UnaryMinus
    | "!" -> Not
    | "~" -> BitNot
    | "&" -> Address
    | "+" -> UnaryPlus
    | "Destruct" -> DestructorCall
    | "__assume" -> Assume
    | "__assert" -> Assert
    | "++" -> PreInc
    | "--" -> PreDec
    | "++" -> PostInc
    | "--" -> PostDec


  let unaryOpToVanillaString = function
      UnaryMinus     -> "UnaryMinus"
    | Not            -> "Not"
    | BitNot         -> "BitNot"
    | Address        -> "Address"
    | UnaryPlus      -> "UnaryPlus"
    | DestructorCall -> "Destruct"
    | Assume         -> "Assume"
    | Assert         -> "Assert"
    | PreInc       -> "PreInc"
    | PreDec       -> "PreDec"
    | PostInc      -> "PostInc"
    | PostDec      -> "PostDec"

  let assignOpToString = function
      Assign       -> "="
    | AssignBitAnd -> "&="
    | AssignBitOr  -> "|="
    | AssignDiv    -> "/="
    | AssignLShift -> "<<="
    | AssignMinus  -> "-="
    | AssignMul    -> "*="
    | AssignPlus   -> "+="
    | AssignRem    -> "%="
    | AssignRShift -> ">>="
    | AssignXor    -> "^="

  let accessOpToString = function
      Dot       -> "."
    | Arrow     -> "->"

  let binaryOpToString = function
      BitAnd    -> "&"
    | BitOr     -> "|"
    | Div       -> "/"
    | LShift    -> "<<"
    | Minus     -> "-"
    | Mul       -> "*"
    | Plus      -> "+"
    | Rem       -> "%"
    | RShift    -> ">>"
    | Xor       -> "^"
    | Eq        -> "=="
    | Ge        -> ">="
    | Gt        -> ">"
    | Le        -> "<="
    | Lt        -> "<"
    | Ne        -> "!="
    | Index     -> "[]"
    | And       -> "&&"
    | Or        -> "||"
    | Comma     -> ","
    | FieldOffset -> "foffset"
    | Offset -> "offset"

  let binaryOpFromString = function
    | "&" -> BitAnd
    | "|" -> BitOr
    | "/" -> Div
    | "<<" -> LShift
    | "-" -> Minus
    | "*" -> Mul
    | "+" -> Plus
    | "%" -> Rem
    | ">>" -> RShift
    | "^" -> Xor
    | "==" -> Eq
    | ">=" -> Ge
    | ">" -> Gt
    | "<=" -> Le
    | "<" -> Lt
    | "!=" -> Ne
    | "[]" -> Index
    | "&&" -> And
    | "||" -> Or
    | "," -> Comma
    | "foffset" -> FieldOffset



  let binaryOpToVanillaString = function
      BitAnd    -> "BitAnd"
    | BitOr     -> "BitOr"
    | Div       -> "Div"
    | LShift    -> "LShift"
    | Minus     -> "Minus"
    | Mul       -> "Mul"
    | Plus      -> "Plus"
    | Rem       -> "Rem"
    | RShift    -> "RShift"
    | Xor       -> "Xor"
    | Eq        -> "Eq"
    | Ge        -> "Ge"
    | Gt        -> "Gt"
    | Le        -> "Le"
    | Lt        -> "Lt"
    | Ne        -> "Ne"
    | Index     -> "Index"
    | And       -> "And"
    | Or        -> "Or"
    | Comma     -> "Comma"
    | FieldOffset -> "Foffset"
    | Offset -> "offset"
  
  (* let uf_store = Lval (Symbol "BLAST_store")
     let uf_select = Lval (Symbol "BLAST_select") *)

  let binaryOpFromVanillaString = function
      "BitAnd"    -> BitAnd
    | "BitOr"     -> BitOr
    | "Div"       -> Div
    | "LShift"    -> LShift
    | "Minus"     -> Minus
    | "Mul"       -> Mul
    | "Plus"      -> Plus
    | "Rem"       -> Rem
    | "RShift"    -> RShift
    | "Xor"       -> Xor
    | "Eq"        -> Eq
    | "Ge"        -> Ge
    | "Gt"        -> Gt
    | "Le"        -> Le
    | "Lt"        -> Lt
    | "Ne"        -> Ne
    | "Index"     -> Index
    | "And"       -> And
    | "Or"        -> Or
    | "Comma"     -> Comma
    | "FOffset"   -> FieldOffset 
    | "Offset"    -> Offset
    | _ -> failwith "Unknown VanillaString" 

    
  let rec print fmt exp =
    begin
      match exp with
	  Lval lv -> print_lval fmt lv
	| Chlval (lv,i) -> 
	    begin
	      Format.fprintf fmt "Primed(" ;
	      print_lval fmt lv ;
	      Format.fprintf fmt ", %s)" i 
	    end
    	| Assignment (op, target, value) ->
	    let op_string = assignOpToString op in
	      begin
		print_lval fmt target;
		Format.fprintf fmt "@ %s@ " op_string;
		print fmt value
	      end
	| Binary (bop, left, right) ->
	    let op_string = " "^(binaryOpToString bop)^" " in
	      print fmt left;
	      Format.fprintf fmt "@ %s@ " op_string;
	      print fmt right;

	| Cast (typ, exp) ->
	    begin
	      Format.fprintf fmt "(<cast>)(@[%a@])" print exp
	    end

	| CastCil (typ, exp) ->
	    begin
	      Format.fprintf fmt "(<castcil>)(@[%a@])" print exp
	    end
    	| Constant ac -> 
	    Format.fprintf fmt "%s" (Constant.toString ac)

	| Constructor (sym, typ) ->
	    begin
	      Format.fprintf fmt "%s" (Symbol.toString sym)
	    end

	| FunctionCall (exp, es) ->
	    print fmt exp;
	    Format.fprintf fmt "(";
            begin match es with
		[] -> ()
              | e0 :: et ->
                  print fmt e0;
	          List.iter (fun a -> Format.fprintf fmt ", "; print fmt a) et
            end;
	    Format.fprintf fmt ")"

	| MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : print @ ast.ml"
        | Select (e1,e2) -> 
            (Format.fprintf fmt "(select "; 
             print fmt e1; print fmt e2; 
             Format.fprintf fmt ")")  
        | Store (e1,e2,e3) -> 
            (Format.fprintf fmt "(store "; 
             print fmt e1; print fmt e2; print fmt e3; 
             Format.fprintf fmt ")")  
        | Sizeof i ->
	    Format.fprintf fmt "sizeof(<%d>)" i
    	| Unary (op, arg) ->
	    let op_string = unaryOpToString op in
	      begin
		match op with
	      	    (PostInc | PostDec) ->
		      Format.fprintf fmt "(" ;
		      print fmt arg;
		      Format.fprintf fmt ")" ;
		      Format.fprintf fmt "%s" op_string
		  | _ ->
	      	      Format.fprintf fmt "%s" op_string;
		      Format.fprintf fmt "(" ;
	      	      print fmt arg ;
		      Format.fprintf fmt ")"
	      end
	| InstanceOf (e, typ) ->
	    begin
	      Format.fprintf fmt "(" ;
	      print fmt e ;
	      Format.fprintf fmt " instanceof " ;
	      JavaType.print fmt typ ;
	      Format.fprintf fmt ")" 
	    end
    end

  and print_vec fmt ev =
    List.iter (fun a -> print fmt a; Printf.printf " ") ev

  and print_lval fmt lv =
    match lv with
      | Symbol s -> 
	  begin
	    try Format.fprintf fmt "%s" (Symbol.toString s)
	    with
		_ -> Format.fprintf fmt "exn in Symbol (expr)@."
	  end
      | This -> Format.fprintf fmt "(this)"
      | Access (op, exp, sym) ->
	  begin
	    let op_string = accessOpToString op in
	      print fmt exp;
	      Format.fprintf fmt "%s" op_string;
              Format.fprintf fmt "%s" (sym)
	  end
      | Dereference exp ->
	  begin
	    Format.fprintf fmt "* (" ;
	    print fmt exp ;
	    Format.fprintf fmt " )" ;
	  end
      | Indexed (exp1, exp2) ->
	  begin
	    print fmt exp1 ;
	    Format.fprintf fmt "[ " ;
	    print fmt exp2 ;
	    Format.fprintf fmt " ]" 
	  end

  let rec toString e =
    try
      begin
	match e with
	  | Lval lv -> (lvalToString lv)
	  | Chlval (lv, suffix) ->
	      "Primed ("^(lvalToString lv)^","^suffix^" )"
	  | Assignment (op, target, value) ->
	      let op_string = assignOpToString op in
		(lvalToString target)^op_string^(toString value)
	  | Binary (bop, left, right) ->
	      let op_string = binaryOpToString bop in
		(toString left)^op_string^(toString right)
	  | Cast (typ, exp) ->
	      "(<cast>)("^(toString exp)^")"
          | CastCil (typ, exp) -> 
	      "(<cast>)("^(toString exp)^")"
    	  | Constant ac -> 
	      (Constant.toString ac)
	  | Constructor (sym, typ) ->
	      (Symbol.toString sym)
	  | FunctionCall (exp, ev) ->
	      (toString exp)^
	      "("^
	      (toStringList  ev)^")"
	  | MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : toString @ ast.ml"
	  | Select (e1,e2) -> 
              Printf.sprintf "(select %s %s)" (toString e1) (toString e2) 
          | Store (e1,e2,e3) -> 
              Printf.sprintf "(store %s %s %s)" (toString e1) (toString e2) (toString e3) 
          | Sizeof i ->
	      "sizeof(<"^(string_of_int i)^">)"
    	  | Unary (op, arg) ->
	      let op_string = unaryOpToString op in
		begin
		  match op with
	      	      (PostInc | PostDec) ->
			"("^(toString arg)^")"^op_string
		    | _ ->
	      		op_string^"("^(toString arg)^")"
		end
	  | InstanceOf (e, t) -> 
	      begin
		"("^(toString e)^" instanceof "^(JavaType.toString t)^")"
	      end
      end
    with e -> Message.msg_string Message.Error 
    ("Expression.toString is the culprit: "^(Printexc.to_string e))  ; "foo__"

  and toStringList  ev =
    List.fold_left (fun a -> fun b -> a ^ "," ^ (toString b)) "" ev

  and lvalToString lv =
    try
      match lv with
	  Access (op, exp, sym) ->
	    begin
	      let op_string = accessOpToString op in
		"("^(toString exp)^")"^op_string^(sym)
	    end
	| Symbol s -> 
	    begin
	      (Symbol.toString s)
	    end
	| This -> "(this)"
	| Dereference e ->
	    begin
	      "* ("^(toString e)^")"
	    end
	| Indexed (e1, e2) ->
	    (toString e1)^"[ ("^(toString e2)^")  ]"
    with _ -> Message.msg_string Message.Error "lval to string is the culprit" ; "foo__1"

      

  (* miscellaneous utilities *)
  let isRelOp op =
    match op with
	Eq | Ge | Gt | Le | Lt | Ne -> true
      | _ -> false

  (* make symbolic variable *)
  (** Dealing with symbolic variables *)
  (* we have a hash function for an lvalue --> string, the latter being the symbolic constant 
     and keep a hashtable to know what the "inverse" of a symbolic constant is.
  *)
  let lv_to_sym_table = Hashtbl.create 101
  let sym_to_lv_table = Hashtbl.create 101
  let sym_ctr = ref 0
		  
  let make_symvar fname lv =
    let is_global sym =
      not (String.contains sym '@')
    in
    let fresh_symvar lv =
      let new_sym_string =
	match lv with
	    Symbol x ->
	      begin
		if (is_global x) then (x^"#@"^fname) 
		else 
		  try
		    match (Misc.bounded_chop x "@" 2) with
		      | [pre;suff] -> pre^"#@"^suff
		      | _ -> failwith "Unexpected error : make_symvar @ ast.ml"
	          with _ -> Message.msg_string Message.Normal ("Match failed with "^lvalToString lv) ; failwith "ouch"
	      end
	  | _ ->
	      let _ = sym_ctr := !sym_ctr + 1 in
		"_bsyv_"^(string_of_int !sym_ctr)^"#@"^fname 
      in
	Hashtbl.replace lv_to_sym_table (fname,lv) new_sym_string;
	Hashtbl.replace sym_to_lv_table new_sym_string (fname,lv);
	(new_sym_string)
    in
    let s = 
      try
	Hashtbl.find lv_to_sym_table (fname,lv)
      with Not_found -> fresh_symvar lv
    in
      Symbol s


  (* not really needed ? no for alias queries its needed.
     exp made using symconsts is aliased to l iff
     exp made after peeling symconsts is aliased to l
  *)

  let  peel_symbolic sym_lv =
    match sym_lv with
	Symbol s ->
	  begin
	    try
	      snd (Hashtbl.find sym_to_lv_table s)
            with Not_found -> sym_lv (* not a symconst *)
          end
      | _ -> sym_lv

  let lv_of_expr e = 
    match e with
	Lval l -> l
      | _ -> let s = ("Warning:bad arg to lv_of_expr"^(toString e)) in
	  Message.msg_string Message.Debug s;
	  failwith s 
	    
  let is_symbolic lv = not ((peel_symbolic lv) = lv)

  let interpreteds = Hashtbl.create 17
  let interpreteds_ren = Hashtbl.create 17
  
  let _alpha = "&arp"
  let interpreted_renamable = [_alpha;"&f<>";"&f=";"&f!=";"&f<=";"&f!<=";"&N"]
  let interpreted_non_renamable = ["&RP";"&FRP";"&FRPL";"&FRPR";"&ftrue"]
  let _ = List.iter (fun s -> Hashtbl.replace interpreteds s true) 
                       (interpreted_renamable @ interpreted_non_renamable)
  let _ = List.iter (fun s -> Hashtbl.replace interpreteds_ren s true) interpreted_renamable
  
  let _is_alpha s = Misc.is_prefix _alpha s
  
  let is_interpreted s = (Options.getValueOfBool "rp") && (Hashtbl.mem interpreteds s || _is_alpha s) 
  
  (* TBD: HACK. fix foci so all the interpreteds can be "timestamped" in symm mode  *)
  let is_interpreted_renamable s = (Options.getValueOfBool "rp") && (Hashtbl.mem interpreteds_ren s || _is_alpha s)
  
  let normalize_interpreted e =
    match e with 
      Lval (Symbol s) when _is_alpha s -> Lval (Symbol _alpha)
    | _ -> e

  (* compute the address of an Expression *)
  let rec mc_addressOf (e : expression) = 
      match e with 
	  Lval lv ->
	    begin
	      match lv with
		  Dereference e1 -> e1
		| Access (aop, e1, fld) -> mc_addressOf e1
		| Indexed (e1, e2) -> e2 
		| Symbol _ -> Unary(Address, e)
		| _ -> failwith ("Pattern matching failed in addressOf with "^(lvalToString lv))
	    end
	| Chlval (l, _) -> mc_addressOf (Lval l)
	| _ -> failwith ("addressOf : strange expression "^(toString e))


  (* compute the address of an Expression *)
  let rec addressOf (e : expression) = 
    if Options.getValueOfBool "mccarthy" then mc_addressOf e else  
    match e with 
	  Lval lv ->
	    begin
	      match lv with
		  Dereference e1 -> e1
		| Access (aop, e1, fld) -> 
                    Binary (FieldOffset, addressOf e1, Lval (Symbol fld))
		| Indexed (e1, e2) -> Binary(Offset, e1,e2) (* Plus --> Offset *)
		| Symbol _ -> Unary(Address, e)
		| _ -> failwith ("Pattern matching failed in addressOf with "^(lvalToString lv))
	    end
	| Chlval (l, _) -> (* We push address-of operators through Chlvals *)
	    addressOf (Lval l)
	| _ -> failwith ("addressOf : strange expression "^(toString e))

     
  (* Push the top level dereference below address-ofs and fieldoffsets.
     Use:
     * (& x) = x
     * (fieldoffset e fld) = access (Dot, *e, fld)
     Notice, that this does NOT work for Chlvals -- i.e. it does nothing for Chlvals *)
  let rec push_deref (l : lval) =
    begin
      match l with
          Dereference e ->
            begin
              match e with
		  Unary(Address, Lval l') -> l'
		| Unary(Address, _) -> failwith ("push_deref: Address applied to non-lval "^(lvalToString l))
		| Binary(FieldOffset, e', Lval (Symbol fld)) -> Access(Dot, Lval (push_deref (Dereference e')), fld)
		| Binary(FieldOffset, _, _) -> failwith ("push_deref: strange field offset "^(lvalToString l))
                | Binary(Offset,e1,e2) -> Indexed (e1,e2)
                | _ -> l  
            end
        | _ -> l
    end

      
  (* replace all occurrences of Chlval by the corresponding Lval *)
  let rec replaceChlvals exp =
    let replaceChlvals_lval l =
      match l with
    	  Access(op, e, f) -> 
	    begin
	      Access (op, replaceChlvals e, f) 
	    end
	| Dereference e -> 
	    begin
	      push_deref (Dereference (replaceChlvals e))
	    end
	| Symbol(_) -> l
	| Indexed (e1, e2) -> Indexed (replaceChlvals e1, replaceChlvals e2)
	| _ -> failwith "Unexpected error : replaceChlvals @ ast.ml"
    in
      match exp with
	| Lval l -> Lval (replaceChlvals_lval l)
	| Chlval (l, _) -> Lval (replaceChlvals_lval l)
		  
	| Assignment(op, target, value) -> Assignment (op, replaceChlvals_lval target, replaceChlvals value)
	| Binary (op, e1, e2) -> Binary (op, replaceChlvals e1, replaceChlvals e2)
	| Cast (t, e1) -> Cast(t, replaceChlvals e1)
	| Constant(_) -> exp
	| Constructor (_, _) -> exp
	| FunctionCall (func, args) -> 
	    FunctionCall (replaceChlvals func, List.map replaceChlvals args)
	| MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : replaceChlvals @ ast.ml"
	| Unary (Address, e) ->
		  let e' = replaceChlvals e in
		    addressOf e'
        | Select(e1,e2) -> Select (replaceChlvals e1, replaceChlvals e2)
        | Store(e1,e2,e3) -> Store (replaceChlvals e1, replaceChlvals e2, replaceChlvals e3)
        | Unary(op, e) -> 
	    Unary (op, replaceChlvals e)
	| InstanceOf (e, t) ->
	    InstanceOf (replaceChlvals e, t)
	|	_ -> failwith ("Pattern matching failed in replaceChlvals with "^(toString exp))

		  

  (* check if an expression is a memory op *)
  let rec isDeref e =
    match e with
	Lval lv ->
	  begin
	    match lv with
		Symbol _ -> false
	      |	_ -> true
	  end
      |	Chlval (lv, _) -> isDeref (Lval lv)
      |	_ -> failwith ("isDeref: Strange expression in isDeref "^(toString e))
	  
  (* given a deref lval *p or ( *p ).f or p->f, return the derefed symbol p *)
  let derefedVar lv =
    match lv with
    	Dereference e -> e
      | Access(Arrow, e,_) -> e
      | Access(Dot, e,_) -> 
	  begin
	    match e with
		Lval l -> 
		  if isDeref e then e 
		  else 
		    failwith ("derefedVar:  parameter is not a deref expression "^(lvalToString l))
	      | Chlval (l, _) ->
		  failwith ("derefedVar: Chlval not handled")
		    (* 
		       if isDeref e then e 
		       else 
		       failwith ("derefedVar:  parameter is not a deref expression "^(lvalToString l))
		    *)
	      | _ -> failwith ("derefedVar: strange operator to Dot : "^(toString e))
	  end
      | _ -> failwith ("derefedVar:  parameter is not a deref expression "^(lvalToString lv))

  (* get the variables p s.t. *p or ( *p ).f or p->f appear in varExps *)
  let getParents varExps =
    let derefExps = (List.filter (fun l -> isDeref (Lval l)) varExps) in
    let rec getDerefVars vars exps =
      match exps with
	  [] -> vars
    	| (hd::tl) -> getDerefVars (Misc.add vars (derefedVar hd)) tl
    in
      getDerefVars [] derefExps
	
  (** return the expressions of the form x, x.f, *x, &x, x->f, and ( *x ).f in the 
    given expression.  we assume those are the only forms. 
    also, because of our assumption that whole-struct assignments do not occur, we
    do not go "inside" an expression of the form x->f or ( *x ).f .  that is, we return it, but not
    *x as a variable in the expression.  
  *)
  let rec allVarExps exp =
    let allVarExps_lval l =
      match l with
    	  Access(Dot, e, _) -> 
	    begin
	      match e with
		  Lval (Symbol (_)) -> 
		    [(Lval l)]
		| Lval (Dereference e) -> [Lval l]
		|   _ -> (Lval l) :: (allVarExps e)
	    end
	| Access(Arrow, e, _) ->
	    begin
	      match e with
		  Lval (Symbol _) -> [Lval l]
		| _ -> [] (* failwith ("Strange expression in allVarExps :"^(toString exp)) *)
	    end
	| Dereference e -> 
	    begin
	      match e with
		  Unary (Address, Lval l' ) -> [Lval l']  
		| Unary (Address, _ ) -> failwith "allVarExps: Address taken of something which is not an lvalue"
		| _ -> [Lval l] 
	    end
	| Symbol(_) -> [Lval l]
	| Indexed (e1, e2) -> (Lval l) :: (Misc.union (allVarExps e1) (allVarExps e2))
	| _ -> failwith "Unexpected error_4 : allVarExps @ ast.ml"
    in
      match exp with
	| Lval l -> allVarExps_lval l
	| Chlval (l, _) -> 
	    begin
	      let _t = 
		match l with
		    Symbol _ -> []
		  | Access (_, e, label) -> allVarExps e
		  | Dereference e -> allVarExps e
		  | Indexed (e1, e2) -> Misc.union (allVarExps e1) (allVarExps e2)
		  | _ -> failwith "Unexpected error_3 : allVarExps @ ast.ml"
	      in
		Misc.union [exp] _t
	    end
	| Assignment(op, target, value) -> Misc.union [(Lval target)] (allVarExps value)
	| Binary (FieldOffset, e1, e2) -> 
	    allVarExps e1
	| Binary (op, e1, e2) -> 
	    Misc.union (allVarExps e1) (allVarExps e2)
	|	Cast (t, e1) -> allVarExps e1
	| Constant(_) -> []
	|	Constructor (_, _) -> []
	| FunctionCall (func, args) -> 
	    Misc.union (allVarExps func) 
	      (List.fold_left Misc.union [] (List.map allVarExps args))
	| MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : allVarExps @ ast.ml"
        | Select (e1,e2) -> Misc.union (allVarExps e1) (allVarExps e2)
        | Store (e1,e2,e3) -> Misc.union (allVarExps e1) (Misc.union (allVarExps e2) (allVarExps e3))
        | Unary(op, e) -> 
	    (match op with
	       |	Address -> [exp]
	       |	_ -> allVarExps e)
	| InstanceOf (e, t) -> allVarExps e  
	| _ -> []



  let rec allVarExps_deep exp =
    match exp with
      | Lval l -> l::(_allvarsLval l)
      | Chlval(l,_) -> [l] (* failwith "chlval not handled"*)
      | Assignment(op, target, value) -> 
	  Misc.union (_allvarsLval target) (allVarExps_deep value)
      | Binary (FieldOffset, e1, e2) -> 
	  allVarExps_deep e1
      | Binary (op, e1, e2) -> 
	  Misc.union 
	    (allVarExps_deep e1) 
	    (allVarExps_deep e2)
      | Constant(_) -> []
      | FunctionCall (func, args) -> 
	  Misc.union (allVarExps_deep func) 
	    (List.fold_left Misc.union [] (List.map allVarExps_deep args))
      | MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : allVarExps_deep @ ast.ml"
      | Select (e1,e2) -> Misc.union (allVarExps_deep e1) (allVarExps_deep e2)
      | Store (e1,e2,e3) -> Misc.union (allVarExps_deep e1) (Misc.union (allVarExps_deep e2) (allVarExps_deep e3))
      | Unary(op, e) -> 
	  (match op with
	     |	Address -> allVarExps_deep e
	     |	_ -> allVarExps_deep e) 
      | InstanceOf (e, t) ->
	  allVarExps_deep e
      | _ -> [] (* Jhala: Say some sort of message ?? *)
  and _allvarsLval l =
    match l with
      | Symbol(sym) -> [] (* [sym] *)
      | Dereference e ->
	  begin
	    (*match e with 
	      Lval (Symbol (x)) -> [x]
	      |	_ ->*) allVarExps_deep e
	  end
      | Access(Dot, e, sym) -> 
	  ((*match e with
	     Lval (Symbol(x)) -> [x] 
	     |	Lval (Dereference (Lval (Symbol(x)))) -> 
	     [x]
	     |	_ ->*) allVarExps_deep e)
      |	Access(Arrow, e, sym) -> allVarExps_deep e
      | Indexed (e1, e2) -> Misc.union (allVarExps_deep e1) (allVarExps_deep e2)
      | _ -> failwith "Unexpected error : allVarExps_deep @ ast.ml"
	  
  (* takes an exp and returns a string list of the variables that appear in the lval *)
  let rec varsOfExp exp =
    let rec varsOfExp_lval l =
      match l with
    	  Access(Dot, e, _) -> varsOfExp e
	| Access(Arrow, e, _) -> varsOfExp e
	| Dereference e -> 
	    begin
	      match e with
		  Unary (Address, Lval l' ) -> varsOfExp_lval l'  
		| Unary (Address, _ ) -> failwith "varsOfExp: Address taken of something which is not an lvalue"
		| _ -> varsOfExp e
	    end
	| Symbol s  -> [s]
	| Indexed (e1, e2) -> (Misc.union (varsOfExp e1) (varsOfExp e2))
	| _ -> failwith "Unexpected error_2 : varsOfExp @ ast.ml"
    in
      match exp with
	|	Lval l -> varsOfExp_lval l
	|	Chlval (l, _) -> 
		  begin
		    let _t = 
		      match l with
			  Symbol _ -> []
			| Access (_, e, label) -> varsOfExp e
			| Dereference e -> varsOfExp e
			| Indexed (e1, e2) -> Misc.union (varsOfExp e1) (varsOfExp e2)
			| _ -> failwith "Unexpected error : varsOfExp @ ast.ml"
		    in
		      _t
		  end
	| Assignment(op, target, value) -> Misc.union ( varsOfExp_lval target) (varsOfExp value)
	| Binary (FieldOffset, e1, e2) -> 
	    varsOfExp e1
	| Binary (op, e1, e2) -> 
	    Misc.union (varsOfExp e1) (varsOfExp e2)
	|	Cast (t, e1) -> varsOfExp e1
	| Constant(_) -> []
	|	Constructor (_, _) -> []
	| FunctionCall (func, args) -> 
	    Misc.union [] (* (varsOfExp func)  *)
	      (List.fold_left Misc.union [] (List.map varsOfExp args))
        | Select(e1,e2) -> Misc.union (varsOfExp e1) (varsOfExp e2)
        | Store(e1,e2,e3) -> Misc.union (varsOfExp e1) (Misc.union (varsOfExp e2) (varsOfExp e3))
        | Unary(op, e) -> varsOfExp e
	|	InstanceOf (e, t) -> varsOfExp e
	|	_ -> []

  (* takes an exp and returns a string list of the variables that appear in the lval *)
  let rec varsOfExpMulti exp =
    let rec varsOfExpMulti_lval l =
      match l with
    	  Access(Dot, e, _) -> varsOfExpMulti e
	| Access(Arrow, e, _) -> varsOfExpMulti e
	| Dereference e -> 
	    begin
	      match e with
		  Unary (Address, Lval l' ) -> varsOfExpMulti_lval l'  
		| Unary (Address, _ ) -> failwith "varsOfExpMulti: Address taken of something which is not an lvalue"
		| _ -> varsOfExpMulti e
	    end
	| Symbol s  -> [s]
	| Indexed (e1, e2) -> ((varsOfExpMulti e1) @ (varsOfExpMulti e2))
	| _ -> failwith "Unexpected error_2 : varsOfExpMulti @ ast.ml"
    in
      match exp with
	|	Lval l -> varsOfExpMulti_lval l
	|	Chlval (l, _) -> 
		  begin
		    let _t = 
		      match l with
			  Symbol _ -> []
			| Access (_, e, label) -> varsOfExpMulti e
			| Dereference e -> varsOfExpMulti e
			| Indexed (e1, e2) -> (varsOfExpMulti e1) @ (varsOfExpMulti e2)
			| _ -> failwith "Unexpected error : varsOfExpMulti @ ast.ml"
		    in
		      _t
		  end
	| Assignment(op, target, value) -> (varsOfExpMulti_lval target) @ (varsOfExpMulti value)
	| Binary (FieldOffset, e1, e2) -> 
	    varsOfExpMulti e1
	| Binary (op, e1, e2) -> 
	   (varsOfExpMulti e1) @ (varsOfExpMulti e2)
	|	Cast (t, e1) -> varsOfExpMulti e1
	| Constant(_) -> []
	|	Constructor (_, _) -> []
	| FunctionCall (func, args) -> 
	    [] (* (varsOfExpMulti func)  *) @
	      (List.fold_left (fun a -> fun b -> a @ b) [] (List.map varsOfExpMulti args))
	| Unary(op, e) -> varsOfExpMulti e
	|	InstanceOf (e, t) -> varsOfExpMulti e
	|	_ -> []



  let negateRel exp =
    match exp with
	Binary (op, e1, e2) -> 
	  begin
	    if (isRelOp op)
	    then
	      begin
		match op with
		    Eq -> Binary(Ne, e1, e2)
		  | Ge -> Binary(Lt, e1, e2)
		  | Gt -> Binary(Le, e1, e2)
		  | Le -> Binary(Gt, e1, e2)
		  | Lt -> Binary(Ge, e1, e2)
		  | Ne -> Binary(Eq, e1, e2)
		  | _ -> failwith ("negateRel: expected binary relational predicate, got "^(toString exp))
	      end
	    else
	      failwith ("negateRel: expected binary relational predicate, got "^(toString exp))
	  end
      |	_ -> failwith ("negateRel: expected binary relational predicate, got "^(toString exp))

  (* A fresh lval given some lval -- ie given an expression which is an lval it returns an appropriate new chlval *)
  let fresh_lval_name = ref 0
			  
  let new_tag () = 
    fresh_lval_name := !fresh_lval_name + 1;
    string_of_int !fresh_lval_name
      
  let fresh_lval lv = Chlval (lv,new_tag () )

  (* Renaming of variables *)
  let rec alpha_convert f = 
    begin
      function
	| Lval l -> Lval (f l) (* Lval (alpha_convert_lval f l) *)
	| Chlval (l,s) -> 
	    begin
	      match l with
		  Symbol _ -> Chlval (l, s)
		| Access (op, e, label) -> Chlval (Access (op, alpha_convert f e, label), s)
		| Dereference e -> Chlval (Dereference (alpha_convert f e), s)
		| Indexed (e1, e2) -> Chlval (Indexed (alpha_convert f e1, alpha_convert f e2), s)
		| _ -> failwith "Unexpected error : alpha_convert @ ast.ml"
	    end
	| Assignment (op, l, r) ->
	    (*WAS:        Assignment (op, alpha_convert_lval f l, alpha_convert f r) *)
            Assignment (op, f l, alpha_convert f r)
	| Binary (FieldOffset, l, fld) -> Binary (FieldOffset, alpha_convert f l, fld)
	| Binary (op, l, r) -> Binary (op, alpha_convert f l, alpha_convert f r)
	| Cast (ty, e) -> Cast (ty, alpha_convert f e)
	| Constant _ as e -> e
	| Constructor _ as e -> e
	| FunctionCall (fct, args) -> FunctionCall (alpha_convert f fct, List.map (alpha_convert f) args)
        | Select (e1,e2) -> Select (alpha_convert f e1, alpha_convert f e2)
        | Store(e1,e2,e3) -> Store (alpha_convert f e1, alpha_convert f e2, alpha_convert f e3)
        | MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : alpha_convert @ ast.ml"
	| Sizeof _ as e -> e
	| Unary (op, e) -> Unary (op, alpha_convert f e)
	| InstanceOf (e, t) -> InstanceOf (alpha_convert f e, t)
        | _ -> failwith "match_failure: alpha_convert"
    end 
  and alpha_convert_lval f =
    function
      | Access (op, e, label) -> Access (op, alpha_convert f e, label)
      | Symbol sym -> f (Symbol sym)
      | Dereference exp -> Dereference (alpha_convert f exp)
      | Indexed (exp1, exp2) -> Indexed (alpha_convert f exp1, alpha_convert f exp2)
      | _ -> failwith "Unexpected error : alpha_convert @ ast.ml"

	  
  (* Renaming of variables *)
  let rec symbol_alpha_convert f = 
    begin
      function
	| Lval l -> Lval (symbol_alpha_convert_lval f l) 
	| Chlval (l,s) ->
            let s = match f (Symbol s) with Symbol s' -> s' | _ -> failwith "symbol alpha_convert -- bad result for f on chlval idx" in
	      (* this is so that the <_,s> s also gets renamed during fmc timeframe renaming  -- comes in as result of unk. function is a chlval *)
              begin 
		match l with
		    Symbol _  -> Chlval ((f l), s)
		  | Access (op, e, label) -> Chlval (Access (op, symbol_alpha_convert f e, label), s)
		  | Dereference e -> Chlval (Dereference (symbol_alpha_convert f e), s)
		  | Indexed (e1, e2) -> Chlval (Indexed (symbol_alpha_convert f e1, symbol_alpha_convert f e2), s)
		  | _ -> failwith "Unexpected error_2 : symbol_alpha_convert @ ast.ml"
	      end
	| Assignment (op, l, r) ->
	    (*WAS:        Assignment (op, alpha_convert_lval f l, alpha_convert f r) *)
            Assignment (op, f l, symbol_alpha_convert f r)
	| Binary (FieldOffset, l, fld) -> Binary (FieldOffset, symbol_alpha_convert f l, fld)
	| Binary (op, l, r) -> Binary (op, symbol_alpha_convert f l, symbol_alpha_convert f r)
	| Cast (ty, e) -> Cast (ty, symbol_alpha_convert f e)
	| Constant _ as e -> e
	| Constructor _ as e -> e
	| FunctionCall (fct, args) ->
            FunctionCall (symbol_alpha_convert f fct, List.map (symbol_alpha_convert f) args)
        | Select (e1,e2) -> Select (symbol_alpha_convert f e1, symbol_alpha_convert f e2)
        | Store(e1,e2,e3) -> Store (symbol_alpha_convert f e1, symbol_alpha_convert f e2, symbol_alpha_convert f e3)
        | MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : symbol_alpha_convert @ ast.ml"
	| Sizeof _ as e -> e
	| Unary (op, e) -> Unary (op, symbol_alpha_convert f e)
	| InstanceOf (e, t) -> InstanceOf (symbol_alpha_convert f e, t)
        | _ -> failwith "match failure: symbol_alpha_convert"
    end 
  and symbol_alpha_convert_lval f =
    function
      | Access (op, e, label) -> Access (op, symbol_alpha_convert f e, label)
      | Symbol sym -> f (Symbol sym)
      | Dereference exp -> Dereference (symbol_alpha_convert f exp)
      | Indexed (exp1, exp2) -> Indexed (symbol_alpha_convert f exp1, symbol_alpha_convert f exp2)
      | _ -> failwith "Unexpected error : symbol_alpha_convert @ ast.ml"

	  
  (** deep_alpha_convert f: lval -> expr 
    where f: lval -> expr essentially f has the chlval information ... this substitutes the lval at the
    top level and then goes inside and recursively goes in and substitutes
  *)
  let rec deep_alpha_convert f = 
    begin
      function
	| Lval l -> 
	    begin
	      let fl = f l in
              match fl with
                  Lval l' -> Lval (deep_alpha_convert_lval f l')
		| Chlval (l',s) -> Chlval (deep_alpha_convert_lval f l',s)
		| _ -> deep_alpha_convert f fl 
	    end
	| Chlval (l,s) -> Chlval (deep_alpha_convert_lval f l,s)
	| Assignment (op, l, r) ->
	    begin
	      match deep_alpha_convert f (Lval l) with
		  Lval l' -> Assignment ( op, l', deep_alpha_convert f r)
		|	_ -> failwith "deep_alpha_convert : strange assignment"
	    end

	| Binary (FieldOffset, l, r) -> Binary (FieldOffset, deep_alpha_convert f l, r)
	| Binary (op, l, r) -> Binary (op, deep_alpha_convert f l, deep_alpha_convert f r)
	| Cast (ty, e) -> Cast (ty, deep_alpha_convert f e)
	| Constant _ as e -> e
	| Constructor _ as e -> e
	| FunctionCall (fct, args) ->
(*
            FunctionCall (deep_alpha_convert f fct, List.map (deep_alpha_convert f) args)
*)
            FunctionCall (fct, List.map (deep_alpha_convert f) args)
	| MethodCall (_, _, _, _, _) -> failwith "MethodCall unimplemented : deep_alpha_convert @ ast.ml"
	| Select (e1,e2) -> Select (deep_alpha_convert f e1, deep_alpha_convert f e2)
        | Store(e1,e2,e3) -> Store (deep_alpha_convert f e1, deep_alpha_convert f e2, deep_alpha_convert f e3)
        | Sizeof _ as e -> e
	| Unary (op, e) -> 
	    begin
	      match op with
		  Address -> Unary (op, deep_alpha_convert f e)
		| _ -> Unary (op, deep_alpha_convert f e)
	    end
	| InstanceOf (e, t) -> InstanceOf (deep_alpha_convert f e, t)
        | _ -> failwith "match failure: deep_alpha_convert"
            end 
  and deep_alpha_convert_lval f =
    function
      | Access (op, e, label) -> Access (op, deep_alpha_convert f e, label)
      | Symbol sym -> (Symbol sym)
      | Dereference exp -> Dereference (deep_alpha_convert f exp)
      | Indexed (exp1, exp2) -> Indexed (deep_alpha_convert f exp1, deep_alpha_convert f exp2)
      | _ -> failwith "Unexpected error : deep_alpha_convert @ ast.ml"


  (* substitute -- this is required by wp *)
  (* substitute in an expression. *)
  (* Arguments :
     exp : Expression
     pairs:  [(old1,new1),...,(old_n, new_n)], where new_i and old_i are
     expression's *)
  (* return exp[new_i/old_i] *)  

  let rec substituteExpression pairs exp =
    let sep = substituteExpression pairs in 
    (try List.assoc exp pairs
     with Not_found ->
       match exp with
	   Lval l -> Lval (subsExpInLval pairs l)
	 | Chlval (l, s) -> 
	     begin
	       match l with
		   Symbol _ -> Chlval (l, s)
		 | Access (op, e, label) -> Chlval (Access (op, sep e, label), s)
		 | Dereference e -> Chlval (push_deref (Dereference (sep e)), s)
		 | Indexed (e1, e2) -> Chlval (Indexed (sep e1, sep e2), s)
		 | _ -> failwith "Unexpected error_1 : substituteExpression @ ast.ml"
	     end
	 | Binary(FieldOffset, e1, e2) -> Binary(FieldOffset,sep e1,e2)
	 | Binary(op, e1, e2) -> Binary(op,sep e1,sep e2)
	 | Constant(_) -> exp
	 | Unary(op, e) ->
             (match op with
                  (* don't substitute underneath an & operator *)
		  Address -> exp
		| _ -> Unary(op, sep e))
         | FunctionCall (Lval (Symbol f), args) ->
             FunctionCall(Lval (Symbol f), List.map (sep) args)
         | Select (e1,e2) -> Select (sep e1, sep e2)
         | Store(e1,e2,e3) -> Store (sep e1, sep e2, sep e3)
         | _ -> failwith ("substitute: not implemented. Offending expression "^(toString exp)))
  and
    subsExpInLval pairs l =
    let sep = substituteExpression pairs in
    match l with 
      | Access(op, e, sym) -> Access(op, sep e, sym)
      |	Dereference e -> push_deref (Dereference (sep e))
      |	Symbol _ -> l
      |	Indexed (e1, e2) -> Indexed (sep e1, sep e2)
      | _ -> failwith "Unexpected error_2 :  substituteExpression @ ast.ml"
	  
	  
  let rec assignments_of_expression exp =
    match exp with
        Assignment (op, l, r) -> (Lval l, r)::(assignments_of_expression r)
      | Binary (op, l, r) -> (if (op = Eq) then [(l,r)] else [])@((assignments_of_expression l)@(assignments_of_expression r))
      | Cast (ty, e) -> assignments_of_expression e
      | Unary (op, e) -> assignments_of_expression e
      | _ -> []



  (* this function gets SYMBOLs from lvalues. it is used in the points to analyzer. *)
  (* To get symbol from lvalue means to get identifier name, on top of which the lvalue is built. *)
  (* However, the function returns lval of form (Symbol s), not symbol itself. *)
  let rec fullsimp_lval lv = 
    match lv with
	Symbol s -> lv
      | Access(_, e,_) ->
	  begin
	    match e with 
		Lval e_lv -> fullsimp_lval e_lv
	      | _ -> failwith "illegal structure access"
	  end
      | Dereference e ->
	  begin
	    match e with
		Unary(Address, Lval lval) -> fullsimp_lval lval
	      | Lval lval -> fullsimp_lval lval
	      | _ -> fullsimp_exp e 
	  end
      | Indexed(e,_) ->
	  begin
	    match e with
		Lval e_lv -> fullsimp_lval e_lv
	      | _ -> failwith "illegal array index"
	  end
      | _ -> failwith "Unexpected error : fullsimp_lval @ ast.ml"
    
    and fullsimp_exp e =
      match e with
        Lval l -> fullsimp_lval l
      | Unary (Address, e') -> fullsimp_exp e'
      | Binary(Plus, e1, e2) -> fullsimp_exp e1
      | Binary (Minus, e1, e2) -> fullsimp_exp e1
      | Binary(Offset, e1, e2) -> fullsimp_exp e1
      | Chlval (e', _) -> fullsimp_lval e'
      | _ -> failwith ("fullsimp_lval: Strange expression "^(toString e)) 
	    
  let rec deep_transform f exp =
    let df = deep_transform f in
    let transform_lval l =
      match l with 
	| Access(op, e, sym) -> Access(op, df e, sym)
	| Dereference e -> push_deref (Dereference (df e))
	| Symbol _ -> l (* use alpha_convert or somesuch to rename these first ... *)
	| Indexed (e1, e2) -> Indexed (df e1, df e2)
	| _ -> failwith "Unexpected error : deep_transform @ ast.ml"
    in
      match exp with
	  Lval l -> f (Lval (transform_lval l))
	| Chlval (l, s) -> f (Chlval (transform_lval l,s))
	| Binary(FieldOffset, e1, e2) -> f (Binary(FieldOffset, df e1, df e2))
	| Binary(op, e1, e2) -> f (Binary(op, df e1, df e2))
	| Constant(_) -> f exp
	| Unary(op, e) ->f (Unary(op, df e))
            (* RJ: WARNING!! don't substitute underneath an & operator -- will do so here anyway ... *)
	| InstanceOf (e, t) -> f (InstanceOf (df e, t))
        | Sizeof _ -> f exp
        | FunctionCall (ef,e_l) -> f (FunctionCall (df ef, List.map df e_l))
        | Select (e1,e2) -> f (Select (df e1,df e2))
        | Store (e1,e2,e3) -> f (Store (df e1, df e2, df e3))
        | _ -> 
            begin
              Message.msg_string Message.Error ("deep_transform: not implemented. Offending expression "^(toString exp));
              exp
            end 
	
	      
  let depth_of_lval lv lv' =
    Message.msg_string Message.Debug 
      (Printf.sprintf "In depth_of_lval: %s : %s " 
         (lvalToString lv) (lvalToString lv'));
    let depth_ref = ref (-1) in 
    let depth_gather e = 
      let _ = 
        match e with
            Lval lv' -> (depth_ref := 0) 
(*          | Lval(Dereference (_)) -> 
              (if !depth_ref >= 0 then depth_ref := !depth_ref + 1)*)
          | _ -> ()
      in      
        e
    in 
    let _ = deep_transform depth_gather (Lval lv) in
      Message.msg_string Message.Debug ("depth = "^(string_of_int
						      !depth_ref));
      !depth_ref

        
  (* true iff e1 occurs inside e2 *) 
  let occurs_check e1 e2 =
    let occurs = ref false in
    let checker e' =
      if e' = e1 then occurs := true;
      e'
    in
    let _ = deep_transform checker e2 in
      !occurs


  let fields_of_expr e =
    let flds = ref [] in
    let gather e' =
      let _ =
	match e' with
	    Lval (Access(_,_,fld)) -> flds := fld::!flds
	  | _ -> ()
      in
	e'
    in
    let _ = deep_transform gather e in
      Misc.sort_and_compact !flds
	
  let fields_of_lval lv =
    fields_of_expr (Lval lv)
      

  let is_lval expr = 
    match expr with
        Lval (_) -> true
      | _ -> false

  let lvals_of_expression e = 
    let lv_list = ref [] in
    let gather e' = 
      let _ = match e' with Lval lv -> lv_list := lv::!lv_list | _ -> () in
      e' in
    let _ = deep_transform gather e in
      Misc.sort_and_compact !lv_list


  (* WARNING -- make sure that f always returns a SMALLER e, or you will loop forever!! *)
  let rec deep_transform_top f exp =
    let df = deep_transform_top f in
    let transform_lval l =
      match l with 
	| Access(op, e, sym) -> Access(op, df e, sym)
	| Dereference e -> push_deref (Dereference (df e))
	| Symbol _ -> l (* use alpha_convert or somesuch to rename these first ... *)
	| Indexed (e1, e2) -> Indexed (df e1, df e2)
	| _ -> failwith "Unexpected error : deep_transform_top @ ast.ml"
    in
    let fexp = f exp in
      match fexp with
	  Lval l -> (Lval (transform_lval l))
	| Chlval (l, s) -> (Chlval (transform_lval l,s))
	| Binary(FieldOffset, e1, e2) -> (Binary(FieldOffset, df e1, df e2))
	| Binary(op, e1, e2) -> (Binary(op, df e1, df e2))
	| Constant(_) -> exp
	| Unary(op, e) -> (Unary(op, df e))
            (* RJ: WARNING!! don't substitute underneath an & operator -- will do so here anyway ... *)
	| InstanceOf (e, t) -> (InstanceOf (df e, t))
        | FunctionCall (ef,e_l) -> FunctionCall (df ef, List.map df e_l)
	| Select (e1,e2) -> (Select (df e1,df e2))
        | Store (e1,e2,e3) -> (Store (df e1, df e2, df e3))
        | _ -> fexp (* failwith ("deep_transform_top: not implemented. Here: Offending expression "^(toString fexp)) *)


  let rec deep_transform_top_cond f exp =
    let df = deep_transform_top_cond f in
    let transform_lval l =
      match l with 
	| Access(op, e, sym) -> Access(op, df e, sym)
	| Dereference e -> push_deref (Dereference (df e))
	| Symbol _ -> l (* use alpha_convert or somesuch to rename these first ... *)
	| Indexed (e1, e2) -> Indexed (df e1, df e2)
	| _ -> failwith "Unexpected error : deep_transform_top @ ast.ml"
    in
    let (fexp,b) = f exp in
    if (not b) then fexp else
      match fexp with
	  Lval l -> (Lval (transform_lval l))
	| Chlval (l, s) -> (Chlval (transform_lval l,s))
	| Binary(FieldOffset, e1, e2) -> (Binary(FieldOffset, df e1, df e2))
	| Binary(op, e1, e2) -> (Binary(op, df e1, df e2))
	| Constant(_) -> exp
	| Unary(op, e) -> (Unary(op, df e))
            (* RJ: WARNING!! don't substitute underneath an & operator -- will do so here anyway ... *)
	| InstanceOf (e, t) -> (InstanceOf (df e, t))
        | FunctionCall (ef,e_l) -> FunctionCall (df ef, List.map df e_l)
        | Select (e1,e2) -> (Select (df e1,df e2))
        | Store (e1,e2,e3) -> (Store (df e1, df e2, df e3))
	| _ -> fexp (* failwith ("deep_transform_top: not implemented. Here: Offending expression "^(toString fexp)) *)


  let is_symm x = 
    match x with Eq | Ne | Plus | Mul -> true | _ -> false

  (* jhala here *)  
  let canonicize e = 
    let f e = 
      match e with
        Binary(op,x,y) when is_symm op -> 
          if List.exists is_interpreted (varsOfExp e) then e 
          else
            let (x',y') = Misc.bi_sort (x,y) in 
            Binary(op,x',y')
      | _ -> e in
    deep_transform f e

  let unprime exp = 
    let f e = 
      match e with
	  Chlval (l,s) -> Lval l
	| e' -> e'
    in
      deep_transform f exp

  (* Normalize addresses -- replace  occurrences of Unary(Address,e) with (addressOf e)
     Q: do we then recur inside the replaced expression ?
     A: YES. To deal with expressions with nested "&" -- do these arise ? hmmm...
     Also, hopefully the expressions sent here are the output of push_deref so things like
     * (& x) don't happen in them. If they do, tough luck, as this function doesnt normalize
     such things.
  *)
	
  let address_normalize =
    let f e =
      match e with
	  Unary (Address,e') -> addressOf e'
	| _ -> e
    in
      deep_transform f
	

  (* implement constant folding on expressions. maptable is a map from
     lvalues to constants. 
  *)
  let rec constantfold e maptable =
    match e with 
      | Lval l -> if (Hashtbl.mem maptable l) then Hashtbl.find maptable l else Lval (constantfold_lval l maptable)
      | Chlval (l,s) -> e
      | Assignment (op, l, r) -> Assignment (op, l, constantfold e maptable)
      | Binary (FieldOffset, l, fld) -> e 
      | Binary (op, Constant c, Constant c') ->
          (match (c, c') with 
               (Constant.Int n1, Constant.Int n2) ->
		 begin
	           match op with
                     | BitAnd -> Constant (Constant.Int (n1 land n2))
                     | BitOr -> Constant (Constant.Int (n1 lor n2))
                     | Div -> Constant (Constant.Int (n1 / n2))
                     | LShift -> Constant (Constant.Int (n1 lsl n2))
                     | Minus -> Constant (Constant.Int (n1 - n2))
                     | Mul -> Constant (Constant.Int (n1 * n2))
                     | Rem -> Constant (Constant.Int (n1 mod  n2))
                     | Plus -> Constant (Constant.Int (n1 + n2))
                     | Offset -> Constant (Constant.Int (n1 + n2))
                     | RShift -> Constant (Constant.Int (n1 lsr n2))
                     | Xor -> Constant (Constant.Int (n1 lxor n2))
                     | Eq -> Constant (Constant.Int (if n1=n2 then 1 else 0)) 
	             | Ge -> Constant (Constant.Int (if n1>=n2 then 1 else 0)) 
                     | Gt -> Constant (Constant.Int (if n1>n2 then 1 else 0)) 
                     | Le -> Constant (Constant.Int (if n1<=n2 then 1 else 0)) 
                     | Lt -> Constant (Constant.Int (if n1<n2 then 1 else 0)) 
                     | Ne -> Constant (Constant.Int (if n1<> n2 then 1 else 0)) 
			 (* | Eq | Ge | Gt | Le | Lt | Ne ??? Why were these commented out? *)
                     | FieldOffset
                     | Index 
                     | And 
                     | Or 
                     | Comma -> e
		 end
             | _ -> e)
      | Binary (op, e1, e2) -> let c1 = constantfold e1 maptable in let c2 = constantfold e2 maptable in
          (match (c1, c2) with (Constant _, Constant _) -> constantfold (Binary(op, c1, c2)) maptable 
             | _ -> Binary(op, c1, c2) )
      | Cast (ty, e) -> Cast(ty, constantfold e maptable) 
      | Constant _ -> e
      | Constructor _ as e -> e
      | FunctionCall (fct, args) -> FunctionCall(fct, List.map (fun ex -> constantfold ex maptable) args)
      | MethodCall (_, _, _, _, _) -> failwith "MethodCall not yet implemented : constantfold @ ast.ml"
      | Select (e1,e2) -> 
          let c1 = constantfold e1 maptable in
          let c2 = constantfold e2 maptable in
             Select (c1,c2)
      | Store (e1,e2,e3) -> 
           let c1 = constantfold e1 maptable in
           let c2 = constantfold e2 maptable in
           let c3 = constantfold e3 maptable in
             Store (c1,c2,c3)
      | Sizeof _ -> e
      | Unary (op, Constant c) -> 
          begin
            match op with 
		UnaryMinus ->
                  (match c with Constant.Int cc -> Constant (Constant.Int (-cc)) | _ -> e )
              | UnaryPlus ->
                  (match c with Constant.Int cc -> Constant (Constant.Int (cc)) | _ -> e )
              | BitNot -> 
                  (match c with Constant.Int cc -> Constant (Constant.Int (lnot cc)) | _ -> e )
              | Not 
              | Address 
              | DestructorCall 
              | Assume 
              | Assert
              | PreInc | PreDec | PostInc | PostDec -> e
          end 
      | Unary (op, e1) ->
          begin 
            let ce = constantfold e1 maptable in
              match ce with Constant _ -> constantfold (Unary(op,ce)) maptable 
		| _ -> Unary (op, ce)
	  end 
      | InstanceOf (ex, t) -> InstanceOf (constantfold ex maptable, t)
      | _ -> failwith "match failure: constantfold"
    and 
    constantfold_lval l maptable =
    match l with
      | Symbol _ -> l 
      | Access (op, e, label) -> Access (op, constantfold e maptable, label) 
      | Dereference e -> Dereference (constantfold  e maptable)
      | Indexed (e1, e2) -> (Indexed (constantfold e1 maptable, constantfold  e2 maptable))
      | _ -> failwith "Unexpected error : constantfold @ ast.ml"
         



(*****************************************************************************)
(************************ McCarthy Code **************************************)
(*****************************************************************************)

let mc_var_table : (lval,expression option) Hashtbl.t = Hashtbl.create 37
let mc_v_ref = ref 0

let mc_prefix = "BLAST_mcv_"

let new_mc_var () =
  mc_v_ref :=  1 + !mc_v_ref;
  Lval (Symbol (mc_prefix^(string_of_int !mc_v_ref)))

let is_mc_var lv = 
  match lv with Symbol s -> Misc.is_prefix mc_prefix s | _ -> false
  
let is_single fa lv = 
  match lv with Indexed _ -> false 
  | _ -> let t = fa lv in (t = [] || t = [lv])

let mc_var fa lv =
  let rv = 
    try Hashtbl.find mc_var_table lv 
    with Not_found ->
      if is_single fa lv then (Hashtbl.replace mc_var_table lv None; None) 
      else
        (let cand = lv::(fa lv) in
        assert (not (List.exists (Hashtbl.mem mc_var_table) cand));
        let data = Some (new_mc_var ()) in
        List.iter (fun k -> Hashtbl.replace mc_var_table k data) cand;
        data)
  in
  (*Message.msg_string Message.Debug (Printf.sprintf "mc_var: %s : returns: %s " 
  (lvalToString lv) (match rv with None -> "None" | Some e -> toString e)); *)
  rv
  
let to_mccarthy fa e =
  let f e =
    (*Message.msg_string Message.Debug ("In to_mccarthy f "^(toString e));*)
    match e with
      Lval (lv)  -> 
        (match mc_var fa lv with None -> (e,false) 
        | Some e' -> (Select (e',mc_addressOf (Lval lv)),true))
    | Unary (Address, _) -> (e,false)
    | _ -> (e,true)
  in
  deep_transform_top_cond f e




end

type predicateVal = True | False | Dontknow

(**
  A predicate is a boolean combination of atomic expressions.
*)
module E = Expression
module Predicate =
struct
  type predicate =
      True
    | False
    | And of predicate list
    | Or of predicate list
    | Not of predicate
    | Implies of predicate * predicate
    | Iff of predicate * predicate
    | All of Symbol.symbol * predicate (* Forall quantifier -- symbol is the variable 
					  being quantified
				       *)
    | Exist of Symbol.symbol * predicate (* Exists quantifier -- symbol is the variable 
					    being quantified
					 *)
    | Atom of E.expression

    | Next of predicate     (* TBD:PNEXT Used PURELY for printing -- donot use -- watch out for pattern match fails! elsewhere!!! *)

  let rec toString pred =
    try
      match pred with
	  True -> "true"
	| False -> "false"
	| And plist -> "And ["^(toStringList plist)^"]"
	| Or plist -> "Or ["^(toStringList plist)^"]"
	| Not p -> "Not ("^(toString p)^")"
	| Implies (p1, p2) -> "("^(toString p1)^"=>"^(toString p2)^")"
	| Iff (p1, p2) -> "("^(toString p1)^"<=>"^(toString p2)^")"
	| All (sym, p) -> "All "^(Symbol.toString sym)^". "^(toString p)
	| Exist (sym, p) -> "Exists "^(Symbol.toString sym)^". "^(toString p)
	| Atom expr -> (E.toString expr)
        | Next p -> "("^(toString p)^")#"
    with _ -> Message.msg_string Message.Error "Predicate to string is the culprit" ; "__Foo__"
  and toStringList plist =
    List.fold_left (fun a -> fun b -> a ^ "," ^ (toString b)) "" plist

  (** Pretty print the specified predicate, providing appropriate line breaks, etc. *)
  let print fmt pr = 
    let rec toStringPrint fmt pred =
      try
	match pred with
	    True -> Format.pp_print_string fmt "true"
	  | False -> Format.pp_print_string fmt "false"
	  | And plist -> printStringList fmt "And" plist
	  | Or plist -> printStringList fmt "Or" plist
	  | Not p ->
	      Format.fprintf fmt  "Not (";
	      toStringPrint fmt p;
	      Format.fprintf fmt ")"
	  | Implies (p1, p2) ->
	      Format.fprintf fmt "(";
	      toStringPrint fmt p1;
	      Format.fprintf fmt "@,=>@,";
	      toStringPrint fmt p2;
	      Format.fprintf fmt ")"
	  | Iff (p1, p2) ->
	      Format.fprintf fmt "(";
	      toStringPrint fmt p1;
	      Format.fprintf fmt "@,<=>@,";
	      toStringPrint fmt p2;
	      Format.fprintf fmt ")"
	  | All (sym, p) ->
	      Format.fprintf fmt "All %s. " (Symbol.toString sym);
	      toStringPrint fmt p
	  | Exist (sym, p) ->
	      Format.fprintf fmt "Exists %s. " (Symbol.toString sym);
	      toStringPrint fmt p
	  | Atom expr ->
	      Format.pp_open_box fmt 0;
	      E.print fmt expr;
	      Format.pp_close_box fmt ()
          | Next p ->
	      Format.pp_print_string fmt "(";
	      toStringPrint fmt p;
	      Format.pp_print_string fmt ")#"
      with _ -> Format.pp_print_string fmt "???"
    and printStringList fmt opname plist =
      Format.fprintf fmt "@[%s [" opname;
      (match plist with
	   [] -> ()
	 | hd :: rest ->
	     toStringPrint fmt hd;
	     List.iter (fun p -> Format.fprintf fmt ",@ "; toStringPrint fmt p) rest
	 (*| [hd] -> toStringPrint fmt hd*));
      Format.fprintf fmt "]@]"
    in toStringPrint fmt pr

  (* SKY: this is a bottleneck for large drivers.  0.015 sec/run is an awful runtime *)
  (* It apparently tries to make predicate shorter... but solvers may do it faster, so here is the skipnorm function *)
  let rec normalize pred = if Options.getValueOfBool "skipnorm" then
    (* While solvers are indeed fast in normalizing queries, on a large scale, de-normalized formulae also suffer from inapplicability of caching.  Therefore, let's do a very simple "normalization", i.e. unwrapping conjunctions and disjunctions of one element.
    Instead of this, predicate generation in postconditions should really be fixed.  But that's simple enough, and experiments show that it's also has a positive impact *)
    let rec norm = function
      |And[only_one]
      |And[only_one;True]
      |Or [only_one]
      |Or [only_one;False] -> norm only_one
      |_ as pred           -> pred
    in
    norm pred
    else
    (* remove duplicates from list.
       SKY: It was O(n^2), and I wanted to rewrite it to use hashtbl, but found same procedure in Utils. *)
    let drop_equal = Misc.compact in
    let flatten predlist i =
      if (i==0) (* And *)
      then
	begin
	  List.fold_left 
	    (fun a -> fun b ->
	       begin
		 match b with
		     And pl -> List.append pl a 
		   | _ -> b::a
	       end) 
	    [] predlist 
	end
      else (* Or *)
	begin
	  List.fold_left 
	    (fun a -> fun b ->
	       begin
		 match b with
		     Or pl -> List.append pl a 
		   | _ -> b::a
	       end) 
	    [] predlist 
	end
    in
      match pred with
	  And plist ->
	    begin
	      match plist with
	      	  []  -> True
		| [a] -> normalize a
		| _   -> 
		    begin
		      (* normlist is a list without tautological True-s and without repeating predicates *)
		      let normlist = 
			List.filter (fun a-> (a!= True)) 
			  (drop_equal (flatten (List.map normalize plist) 0))
		      in
			(* if list contains False then the predicate is tautologically false *)
			if (List.mem False normlist)
			then False
			else
			  begin
			    match normlist with 
				[] -> True
			      | [a] -> a
			      | _ -> And normlist
			  end
		    end
	    end
	| Or plist ->
	    begin
	      match plist with
	      	  []  -> False
		| [a] -> normalize a
		| _   -> 
		    begin
		      let normlist = 
			List.filter (fun a-> (a!=False)) 
			  (drop_equal (flatten (List.map normalize plist) 1))
		      in
			if (List.mem True normlist)
			then True
			else
			  begin
			    match normlist with
				[] -> False
			      | [a] -> a
			      | _ -> Or normlist
			  end
		    end
	    end
	| Implies (p, q) -> Implies (normalize p, normalize q)
	| Iff (p, q) -> Iff (normalize p, normalize q)
	| Not p -> Not (normalize p)
	| All (a,b) -> All (a, normalize b)
	| Exist (a,b) -> Exist (a, normalize b)
	| Next p -> Next (normalize p)
        | _ -> pred

  let conjoinL plist = normalize (And (plist))
			 
  let disjoinL plist = normalize (Or (plist))

  let extract_terms_and p = 
    match p with 
	And l -> l
      | _ -> [p]
	  
  let extract_terms_or p =
    match p with 
	Or l -> l
      | _ -> [p] 
	  
  let rec unzip ll = 
    match ll with
	[t] ->  List.map (function x -> [x] ) t
      | h::tl ->  List.flatten (List.map (function x -> (List.map (function y -> x::y) (unzip tl) )) h)
      | [] -> []

  let rec to_dnf p =
    match p with 
      | And l -> (List.map conjoinL (unzip (List.map to_dnf l)))
      | Or l -> (List.flatten (List.map to_dnf l))
      |  _ -> [p]


  let implies p1 p2 = normalize (Implies (p1, p2))

  let iff p1 p2 = normalize (Iff (p1, p2))
		    
  let negateAtom p =
    match p with
      | Atom e ->
	 begin
	   try
	     Atom (E.negateRel e)
	   with
	      _ -> Not p
	 end
      | _ -> failwith "Predicate.negateAtom expects an atomic predicate!"

  let rec negate p =
    match p with
	True -> False
      | False -> True
      | And pl -> Or (List.map negate pl)
      | Or pl -> And (List.map negate pl)
      | Not q -> q
      | Implies (p,q) -> And [p; negate q]
      | Iff (p,q) -> Or [ And [p; negate q] ; And [negate p; q] ]
      | All (a,b) -> Exist (a, negate b)
      | Exist (a,b) -> All (a, negate b)
      | Next p -> Next (negate p)
      | Atom _ -> negateAtom p


  (** Propagate the negation to the atoms and replaces disequalities
      by disjunction of strict inequalities. *)
  let rec push_negation negate p = 
    match p with
      | True -> if negate then False else p
      | False -> if negate then True else p
      | And ps -> 
	  let negated_ps = List.map (push_negation negate) ps in
	    if negate then Or negated_ps else And negated_ps
      | Or ps ->
	  let negated_ps = List.map (push_negation negate) ps in
	    if negate then And negated_ps else Or negated_ps
      | Not p' -> push_negation (not negate) p'
      | Implies (pa, pb) -> 
	  let pushed_pa = push_negation false pa in
	  let negated_pb = push_negation negate pb in
	    if negate then 
	      And [pushed_pa; negated_pb] 
	    else
	      Implies (pushed_pa, negated_pb)
      | Iff (pa, pb) ->
	  if negate then
	    Or 
	      [
		And [push_negation true pa; push_negation false pb]
		; 
		And [push_negation false pa; push_negation true pb]
	      ]
	  else 
	    Iff (push_negation false pa, push_negation false pb)
      | All (s, p') -> 
	  let negated_p' = push_negation negate p' in
	    if negate then Exist (s, negated_p') else All (s, negated_p')
      | Exist (s, p') -> 
	  let negated_p' = push_negation negate p' in
	    if negate then All (s, negated_p') else Exist (s, negated_p')
      | Atom exp -> 
	  begin
	    match exp with 
	      | E.Binary (E.Ne, exp1, exp2) -> 
		  if negate then 
		    Atom (E.Binary (E.Eq, exp1, exp2)) 
		  else
		    Or 
		      [
			Atom (E.Binary (E.Lt, exp1, exp2))
			; 
			Atom (E.Binary (E.Gt, exp1, exp2))
		      ]
	      | E.Binary (E.Eq, exp1, exp2) -> 
		  if negate then 
		    Or 
		      [
			Atom (E.Binary (E.Lt, exp1, exp2))
			; 
			Atom (E.Binary (E.Gt, exp1, exp2))
		      ]
                  else
                    p
	      | _ -> if negate then negateAtom p else p
	  end 
      | Next _ -> failwith "push_negation: TBD:PNEXT Used PURELY for printing -- donot use -- watch out for pattern match fails! elsewhere!!!"

  let push_negation p = push_negation false p

  (** Distribute conjunction over disjunctions and return a list of
      disjunction-free predicates. IMPORTANT: push_negation should be
      applied before distributing conjunction *)
  let rec distribute_conjunction p = 
    match p with 
      | True 
      | False 
      | Not _ -> (* IMPORTANT: push_negation should be applied before distributing conjunction *)
	  [p]
      | And ps ->  
	  let dfpss = List.map distribute_conjunction ps in
          let ps = ref [True] in
	    List.iter
	      (fun dfps ->
		 let tmp = !ps in
		   ps := [];
		   List.iter 
		     (fun dfp ->
			List.iter 
			  (fun p ->
			     ps := And [dfp; p] :: !ps
			       
			  ) tmp
		     ) dfps
		     
	      ) dfpss;
	    !ps


      | Or ps -> List.flatten (List.map distribute_conjunction ps)
      | Implies (pl, pr) -> 
	  (distribute_conjunction (negate pl)) @ (distribute_conjunction pr)
      | Iff (pl, pr) ->
	  distribute_conjunction (And [pl; pr]) @ 
	    distribute_conjunction (And [negate pl; negate pr])
      | All _ 
      | Exist _ ->
	  failwith ("distribute_conjunction: usage of " ^ toString p)
      | Atom _ -> [p]
      | Next _ -> failwith "distribute_conjunction: TBD:PNEXT Used PURELY for printing -- donot use -- watch out for pattern match fails! elsewhere!!!"




  let rec convertDNF p = (* EXPENSIVE !! *)
    match p with
	True -> True
      | False -> False
      | Or plist -> normalize (Or (List.map convertDNF plist))
      | And plist -> (* difficult case *)
	  begin
	    let listOfOrs = List.map (fun a -> normalize (convertDNF a)) plist in
	    let (pureOrs, rest) = List.partition (function a -> match a with Or _ -> true | _ -> false) listOfOrs
	    in
	    let cartesianProduct ll =
	      let  cartprodIt xs ys = List.fold_right 
					(function x -> function pairs -> 
					   List.fold_right (function y -> function l -> 
							      (x::y):: l) ys pairs) 
					xs []
	      in
		List.fold_right cartprodIt ll [[]]
	    in
	    let andLists = 
	      cartesianProduct (List.map (function p -> match p with Or a -> a | _-> failwith "convertDNF:only Ors") pureOrs)
	    in
	      normalize (Or (List.map (function a -> normalize (And (a @ rest))) andLists)) 
	  end
      | Not pr -> 
	  begin
	    match pr with
		Atom _ -> p
	      |	_ -> convertDNF (negate p)
	  end
      | Atom _ -> p
      | Implies (p1, p2) -> Implies (convertDNF p1, p2)
      | _ -> failwith ("convertDNF: Unexpected input "^(toString p))
	  
  (* return the Expressions of the form x, x.f, *x, &x, x->f and ( *x ).f in the 
     given Predicate.  we assume those are the only forms. 
     also, because of our assumption that whole-struct assignments do not occur, we
     do not go "inside" an expression of the form ( *x ).f or x->f.  that is, we return it, but not
     *x as a variable in the expression.  *)
	  
  let rec allVarExps pred =
    match pred with
        Atom e -> E.allVarExps e
      | True -> []
      | False -> []
      | And plist -> List.fold_left (fun a -> fun b -> Misc.union a (allVarExps b)) [] plist
      | Or plist -> List.fold_left (fun a -> fun b -> Misc.union a (allVarExps b)) [] plist
      | Implies (p1,p2) -> Misc.union (allVarExps p1) (allVarExps p2)
      | Iff (p1,p2) -> Misc.union (allVarExps p1) (allVarExps p2)
      | Not p -> allVarExps p
      | All (s,p) -> let v_l = allVarExps p in Misc.list_remove v_l (E.Lval (E.Symbol s))
      | Exist (s,p) -> let v_l = allVarExps p in Misc.list_remove v_l (E.Lval (E.Symbol s))
      | _ -> failwith "match failure: allVarExps"		 

  let rec varsOfPred pred =
    match pred with
        Atom e -> E.varsOfExp e
      |        True -> []
      |        False -> []
      |        And plist -> List.fold_left (fun a -> fun b -> Misc.union a (varsOfPred b)) [] plist
      |        Or plist -> List.fold_left (fun a -> fun b -> Misc.union a (varsOfPred b)) [] plist
      |        Implies (p1,p2) -> Misc.union (varsOfPred p1) (varsOfPred p2)
      |        Iff (p1,p2) -> Misc.union (varsOfPred p1) (varsOfPred p2)
      |        Not p -> varsOfPred p       
      |        All (s,p) -> s::(varsOfPred p)
      |        _ -> failwith "varsOfPred : Not implemented"

  let rec varsOfPredMulti pred =
    match pred with
        Atom e -> E.varsOfExpMulti e
      |        True -> []
      |        False -> []
      |        And plist -> List.fold_left (fun a -> fun b -> a @ (varsOfPredMulti b)) [] plist
      |        Or plist -> List.fold_left (fun a -> fun b -> a @ (varsOfPredMulti b)) [] plist
      |        Implies (p1,p2) -> (varsOfPredMulti p1) @ (varsOfPredMulti p2)
      |        Iff (p1,p2) -> (varsOfPredMulti p1) @ (varsOfPredMulti p2)
      |        Not p -> varsOfPredMulti p       
      |        All (s,p) -> s::(varsOfPredMulti p)
      |        _ -> failwith "varsOfPredMulti : Not implemented"




  (* Deep allVarExp -- go inside the x.foo, x -> foo etc. to return "x" *)
  let rec allVarExps_deep pred =
    match pred with
	Atom e -> E.allVarExps_deep  e
      |	True -> []
      |	False -> []
      |	And plist -> List.fold_left (fun a -> fun b -> 
				       Misc.union a (allVarExps_deep b)) [] plist
      |	Or plist -> List.fold_left (fun a -> fun b -> Misc.union 
				      a (allVarExps_deep b)) [] plist
      |	Implies (p1,p2) -> Misc.union (allVarExps_deep p1) 
	  (allVarExps_deep p2)
      |	Iff (p1,p2) -> Misc.union (allVarExps_deep p1) 
	  (allVarExps_deep p2)
      |	Not p -> allVarExps_deep p
      |       All (s,p) -> (E.Symbol s)::(allVarExps_deep p)
      |	_ -> failwith "allvarExps_deep : Not implemented"
	  
	  
  module SymbolSet = Set.Make(
    struct type t = E.lval
	   let compare = Pervasives.compare
    end
  )
  let map sigma eta p0 =
    let rec traverse bound = function
      | True | False as p -> p
      | And ps -> And (List.map (traverse bound) ps)
      | Or ps -> Or (List.map (traverse bound) ps)
      | Not p -> Not (traverse bound p)
      | Implies (p1, p2) -> Implies (traverse bound p1, traverse bound p2)
      | Iff (p1, p2) -> Iff (traverse bound p1, traverse bound p2)
      | All (sym, p) -> All (sigma sym, traverse (SymbolSet.add (E.Symbol sym) bound) p)
      | Exist (sym, p) -> Exist (sigma sym, traverse (SymbolSet.add (E.Symbol sym) bound) p)
      | Next p -> Next (traverse bound p)
      | Atom e -> Atom (eta (fun sym -> SymbolSet.mem sym bound) e)
    in
      traverse SymbolSet.empty p0

  let alpha_convert f =
    map (fun x -> x) 
      (function is_bound ->
         E.alpha_convert
         (fun lv -> if is_bound lv then lv else f lv))
      

  let symbol_alpha_convert f = 
    map (fun x -> x) 
      (function is_bound ->
         E.symbol_alpha_convert
         (fun lv -> if is_bound lv then lv else f lv))

  let deep_alpha_convert f = 
    map (fun x -> x) 
      (function is_bound ->
         E.deep_alpha_convert
         (fun lv -> if is_bound lv then E.Lval lv else f lv))

      
  (* Substitute In A Predicate *)
  (* Arguments:
     f: Predicate
     pairs:  [(old1,new1),...,(old_n, new_n)], where new_i and old_i are
     E.expression's *)
  (* return f[new_i/old_i] *)
  (* NOTE:  We make use of the fact that the only things we are substituting
     for are expressions of the form x and *x. *)

  let rec substitute pairs f =
    match f with
        True      -> True
      | False     -> False
      | Atom e    -> Atom (E.substituteExpression pairs e)
      | And plist -> And (List.map (substitute pairs) plist)
      | Or plist  -> Or (List.map (substitute pairs) plist)
      | Not p     -> Not (substitute pairs p)
      | Implies (p1, p2) -> Implies (substitute pairs p1, substitute pairs p2)
      | Iff (p1, p2) -> Iff (substitute pairs p1, substitute pairs p2)
      | All (s,p1) -> All(s,substitute pairs p1)
      | _         -> failwith "Substitute: Quantified predicate not implemented"

  (* We will represent predicates in (pseudo-)canonical form:
     - Equalities rather than disequalities
     - <= rather than >=
     - < rather than >
     - lexicographically smaller symbol to the left in case of sym1 <relop> sym2
  *)
		 
  (* f is a function from predicate -> predicate. Typically called with f being the id on everything except atom *) 
  let rec deep_transform f p =
    let df  = deep_transform f in
      match p with
	  True -> f True
	| False -> f False
	| Atom e -> f (Atom e)
	| And plist -> f (And (List.map df plist))
	| Or plist -> f (Or (List.map df  plist))
	| Not p -> f (Not (df p))
	| Implies (p1,p2) -> f (Implies (df p1, df p2))
	| Iff (p1,p2) -> f (Iff (df p1,df p2))
        | All (s,p') -> f(All (s,df p'))
        | _ -> failwith "deep_transform: Quantified predicate not implemented"

  (** f is a function from expression -> expression *) 
  let rec atom_transform f p =
    let df  = atom_transform f in
      match p with
	  True -> True
	| False -> False
	| Atom e -> Atom (f e)
	| And plist -> And (List.map df plist)
	| Or plist -> Or (List.map df  plist)
	| Not p -> Not (df p)
	| Implies (p1,p2) -> Implies (df p1, df p2)
	| Iff (p1,p2) -> Iff (df p1,df p2)
        | All (s,p') -> All (s, df p')
	| _ -> failwith "deep_transform: Quantified predicate not implemented"
	    
	    

  let expr_equate e e' = Atom (E.Binary(E.Eq,e,e'))

			   
  (* unlike in the paper, this requires the "chlvals" to be supplied *)
  let ac_equate comb_fun elist e'list =
    let ee'list = try comb_fun elist e'list with _ -> failwith
      (Printf.sprintf "Combine fails in equate!: %s : %s" 
	 (Misc.toString_list E.toString elist) (Misc.toString_list
							   E.toString e'list))
    in
      conjoinL (List.map (fun (e,e') -> expr_equate e e') ee'list)
      (* SKY: makes a conjunction of (list1[i] = list2[i]) statements *)
	

  let equate = ac_equate List.combine
  let min_equate = ac_equate Misc.list_min_combine
  (* SKY: list_min_combine is list of pairs of elements until one list terminates *)
  (* SKY: min_equate makes a conjunction of (list1[i] = list2[i]) statements until one list terminates *)
		     
  let unprime pred = 
    let f p = 
      match p with
	  Atom e -> Atom (E.unprime e)
	| p' -> p'
    in
      deep_transform f pred

  let address_normalize =
    let f p =
      match p with
	  Atom e -> Atom (E.address_normalize e)
	| _ -> p
    in
      deep_transform f
	
    
  
   let is_neg r = 
    match r with E.Ne -> Some (E.Eq) | _ -> None

  let normalize_interpreted p = 
    atom_transform (E.deep_transform_top E.normalize_interpreted) p

  (* assert: (eq_canonicize p) <-> p *)
  let eq_canonicize pred = 
    let f p = 
      let p =
        match p with 
          Atom (E.Binary (r,x,y)) -> Atom (E.Binary (r,E.canonicize x,E.canonicize y)) 
        | _ -> p in
      match p with
        Atom (E.Binary (r,x,y)) when E.is_symm r ->
          let (x',y') = Misc.bi_sort (x,y) in
          (match is_neg r with None -> Atom (E.Binary (r,x',y'))
             | Some r' -> Not (Atom (E.Binary (r',x',y'))))
      | Atom (E.Binary (E.Ge, x, y)) -> Atom (E.Binary (E.Le,y,x))
      | Atom (E.Binary (E.Gt, x, y)) -> Atom (E.Binary (E.Lt, y,x))
      | Atom (E.Binary (E.Le, E.Constant (Constant.Int c1), e2)) ->
          (match e2 with
             E.Binary(E.Plus, E.Constant (Constant.Int c), E.Lval l) ->
               Atom (E.Binary (E.Le, E.Constant (Constant.Int (c1-c)), E.Lval l))
           | E.Binary(E.Plus, E.Constant (Constant.Int c), E.Unary (E.UnaryMinus, E.Lval l)) ->
               Atom (E.Binary (E.Le, E.Lval l, E.Constant (Constant.Int (c-c1))))
	   | _ -> p)
      | _ -> p in
    let rv = deep_transform f (normalize_interpreted pred) in
    Message.msg_string Message.Debug (Printf.sprintf "eq_canon: in = %s , out = %s \n" (toString pred) (toString rv));
    rv

  (*TODO take the bigger ??? *)
  let canonicize pred = 
    match pred with
      Atom _ -> 
        (match eq_canonicize pred with 
           Atom _ as p -> p 
         | Not (Atom _ as p) -> p 
         | _ -> failwith "error in canonicize")
    | _ -> failwith ("canonicize: Predicate "^(toString pred)^" is not atomic")

	(* get atomic predicates (i.e. ``predicate symbols'', expressions, that are joined with high-level predicate logic relations *)
  let getAtoms pred =  
    let rec _ga pred =
      match pred with
	True -> []
      | False -> []
      | And plist -> List.fold_left (fun l1 l2 -> Misc.union l1 (_ga l2)) [] plist
      | Or plist -> List.fold_left (fun l1 l2 -> Misc.union l1 (_ga l2)) [] plist
      | Not p -> _ga p
      | Implies (p1, p2) -> Misc.union (_ga p1) (_ga p2)
      | Iff (p1, p2) -> Misc.union (_ga p1) (_ga p2)
      | Atom expr -> [pred]
      | All _ -> Message.msg_string Message.Normal ("Warning: getAtoms quantified atom"^(toString pred)); [pred]
      | _ -> failwith "getAtoms: input predicate should quantifier-free!" in
    _ga (eq_canonicize pred)



  let rec getAtoms_parity parity pred =
    match pred with
	True -> []
      | False -> []
      | And plist -> List.fold_left (fun l1 l2 -> Misc.union l1 (getAtoms_parity parity l2)) [] plist
      | Or plist -> List.fold_left (fun l1 l2 -> Misc.union l1 (getAtoms_parity parity l2)) [] plist
      | Not p -> getAtoms_parity (not parity) p
      | Implies (p1, p2) -> Misc.union (getAtoms_parity (not parity) p1) (getAtoms_parity parity p2)
      | Iff (p1, p2) -> Misc.union (getAtoms_parity parity p1) (getAtoms_parity parity p2)
      | Atom expr -> [(pred,parity)]
      | All _ -> Message.msg_string Message.Normal ("Warning: getAtoms_parity quantified atom"^(toString pred)); [(pred,parity)]
      | _ ->
          failwith "getAtoms: input predicate should quantifier-free!"

  let lvals_of_predicate p =
    let atom_suck a = 
      match a with
	  Atom e -> E.lvals_of_expression e
	| _ -> [] in
    List.flatten (List.map atom_suck (getAtoms p))
  
  let rec constantfold p maptable = 
    match p with
      | True 
      | False     -> p
      | Atom e    -> 
	  begin
	    let ec = (E.constantfold  e maptable) in
              match ec with
		  E.Constant (Constant.Int 1) -> True
		|  E.Constant (Constant.Int 0) -> False
		|  _ -> Atom ec
          end
      | And plist -> And (List.map (fun p -> constantfold p maptable) plist)
      | Or plist -> Or (List.map (fun p -> constantfold p maptable) plist)
      | Not p     -> Not (constantfold p maptable)
      | Implies (p1, p2) -> Implies (constantfold  p1 maptable, constantfold p2 maptable)
      | Iff (p1, p2) -> Iff (constantfold  p1 maptable, constantfold p2 maptable)
      | _         -> p (* failwith "constantfold: Quantified predicate not implemented" *)

let to_mccarthy fa p = atom_transform (E.to_mccarthy fa) p
 

end

(**
  Module that implements the 0,1, ..., k, omega counter abstraction.
  Maxcount is stored internally as an integer, and omega is represented simply
  by Maxcount + 1. The representation is opaque to the callers.
*)
module Counter =
struct
  type counter = int 
      
  let maxcount = ref 1 (* default: 0, nonzero *)
		   
  (* omega = !maxcount + 1 *)
  let set_max i = maxcount := i
  let set_counter i = maxcount := i

  let omega () = !maxcount + 1
  
  let is_omega c = (c >= !maxcount + 1)
  
  let is_zero c = (c = 0)
  
  let make_counter i = if (i > !maxcount) then !maxcount+1 else i
  
  let to_string c = if c > !maxcount then "omega" else string_of_int c 

  let normalize i = 
    if (i < 0) then 0
    else if (i <= !maxcount) then i
    else if Options.getValueOfBool "finitecounter" then !maxcount
    else (!maxcount + 1)
  
  let to_int c =
    if c > !maxcount then failwith "Counter.to_int called with omega!"
    else c

  let plus ctr1 ctr2 = normalize (ctr1 + ctr2)  
    (* omega + anything = anything + omega = omega *)

  let minus ctr1 ctr2 =
    (* omega - anything = omega *)
    if ctr1 > !maxcount then !maxcount + 1
    else if ctr1 > ctr2 then ctr1 - ctr2 else 0

  let leq c1 c2 =
    if (c2 >= !maxcount+1) then true
    else (c1 <= c2)

  let geq c1 c2 =
    if (c1 >= !maxcount+1) then true
    else (c1 >= c2)
	
  let eq c1 c2 = leq c1 c2 && leq c2 c1
	
  (* the bdd is the binary representation of the counter *)
  let rec getbdd c = 
    if c > !maxcount+1 then getbdd (!maxcount+1) 
    else () (* TBD *)

  let toString c = if c  > !maxcount then "omega" else (string_of_int c)

  let print fmt c = Format.pp_print_string fmt (toString c)

  let ctrmax c1 c2 = normalize (max c1 c2) 

  let ctrmin c1 c2 = normalize (min c1 c2)

  let array_op op cmap1 cmap2 =
    assert (Array.length cmap1 = Array.length cmap2) ;
    let result = Array.make (Array.length cmap1) (0:counter) in
      for i = 0 to (Array.length cmap1 - 1) do
        result.(i) <- op cmap1.(i) cmap2.(i)
      done ;
      result

  let array_max = array_op ctrmax
  let array_min = array_op ctrmin
  let array_plus = array_op plus
  let array_minus = array_op minus
		      
end

(***************************************************************************************)
(* Code to deal with timeouts  *)
let sig_caught_bit = ref false 
exception TimeOutException

(* Set time out signal *)
let set_time_out_signal () =
  (let t = Options.getValueOfInt "timeout" in
     if t >= 0 then
       begin 
	 Message.msg_string Message.Error ("Setting signal for "^(string_of_int t)^" seconds") ;
	 sig_caught_bit := false ;
	 ignore (Sys.signal
                   Sys.sigalrm
                   (Sys.Signal_handle
                      (fun i ->
			 Message.msg_string Message.Error "Caught exception sigalrm in handler!" ;
			 sig_caught_bit := true ; (* The trouble with raising TimeOutException here is
                                                     that if TimeOutException is caught
                                                     by some function called by model_check,
                                                     we have lost the alarm.
                                                     Moreover this exception can be raised at
                                                     an awkward moment, leaving the internal state
                                                     inconsistent.
                                                     So we set a global bit that the model
                                                     checker will check at the beginning
                                                     of each loop. The model_check routine
                                                     raises TimeOutException if this bit is set.

                                                     This bit is reset by the catchers of
                                                     TimeOutException
                                                  *)
			 (*exit 1;*)
                      )) ) ;
	 ignore (Unix.alarm (t)) ;
       end )
let reset_time_out_signal () =
  Sys.set_signal Sys.sigalrm (Sys.Signal_ignore) ;
  sig_caught_bit := false


let check_time_out () =
  if (!sig_caught_bit) then (* Time out has occurred *)
    begin
      reset_time_out_signal () ; (* reset the timeout for next iteration *)
      Message.msg_string Message.Error "Time out!\n\n" ;(* exit  1 ;*)
      raise TimeOutException
    end

(******************************************************************************)


