(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

open BlastArch

(**
  * The basic internal representation of C program operations.
  * A command is either Skip, Block of statements, an assume
  * Predicate, or a FunctionCall.
  * A statement is an expression (basically assignment) or a return. 
  * Notice that all control flow information is maintained explicitly
  * in the control flow automaton.
  *
  * @see 'blastArch.ml' for a description of the CFA
  *)
module C_Command :
sig
  type statement =
      Expr of Ast.Expression.expression
    | Return of Ast.Expression.expression (* option *)

  type command =
      Skip 
    | Block of statement list 
      
    | Pred of Ast.Predicate.predicate  (** Remark: at the C level, a predicate is just another expression
					   but it may prove useful to differentiate them when we can (e.g.
					   for conditionnals) *)
   
    | FunctionCall of Ast.Expression.expression (** Remark: the expression of a function call is assumed to be
					   either:
					   - an Expression.FunctionCall(_), or
					   - an Expression.Assignment (_, _, Expression.FunctionCall(_)) *)

    | GuardedFunctionCall of (Ast.Predicate.predicate * Ast.Expression.expression) 
		(** Remark: GuardedFunctionCalls are used to disambiguate function pointer calls.
			The predicate is of the form (Predicate.Atom (Expression.Binary(Expression.Eq, fnptr, Lval (Symbol fname)))
			that says fnptr is assumed to be the function fname.
			the expression of a function call is assumed to be as for FunctionCall above.
		 *)
    | Havoc of Ast.Expression.lval (*Ast.Symbol.symbol*)
        (* [Havoc x]
           Forget all that is known about x, unless \phi(x) is known to
           hold. This action models the behavior of other threads. *)
    | HavocAll
        (* Havoc all global variables *)
    | Phi of Ast.Expression.lval * bool
        (* [Phi (x, sign)]
           This is semantically equivalent to [Pred <<sign iff \phi(x)>>]. *)
    | SymbolicHook of string
        (* To enable the generation of constraints for globals that MAY be
         * modified inside fname == string *)
  type t = {
      code : command;
      read : Ast.Expression.lval list;
      written : Ast.Expression.lval list;
    }

  (** transform a code to a Command.t by gathering reads and writes *)
  val gather_accesses : command -> t

  (** the pretty-printer for this type *)
  val print : Format.formatter -> t -> unit

  val print_statement : Format.formatter -> statement -> unit
	
  (** the string converter for this type *)
  val to_string : t -> string
end

(** Internal description of a C program. *)
module C_System_Descr :
sig
  include SYSTEM_DESCRIPTION

  (** Take a list of file names, parse it and build the internal
      representation.
   *)
  val initialize : string list -> unit 

  (** Do the parsing and building CFA in two separate phases
    so that Cil specific optimizations can be run. *) 
  val initialize_cil : string list -> Cil.file list
    
  val initialize_blast : Cil.file list -> unit

  (* some more initialization -- namely that which requires aliasing info:
     1. mods and 2. symbolic hooks *)
  val post_alias_analysis : (string -> Ast.Expression.lval -> Ast.Expression.lval list) -> unit -> unit
    
  (** Our hacky spec routine. *)
  val instrument_with_spec : string -> string -> unit

  (** Print a description of the CFA *)
  val print_system : Format.formatter -> unit -> unit

  (** Print a dot representation of the CFA *)
  val print_system_dot : unit -> unit

  (** get original cil files *)
  val get_cil_files : unit -> Cil.file list

  (** get the varinfo structure associated with a string.
      Raise Not_found if string is not found.
  *)
  val get_varinfo : string -> Cil.varinfo

  (** check if a function is declared to be an allocation function, ie has attribute "alloc" 
      allocation functions add new locations in the heap.
   *)
  val is_alloc : string -> bool
  (** functions for interface generation *)

  
  (** check if a function is declared to be an interface function, ie has attribute "public" *)
  val is_interface : string -> bool
  (* build a maximal automaton for interface functions *)
  val build_automaton : string list -> (location * location)

  (* Hack into the lvalues for alias purposes *)
  val add_lval : Ast.Expression.lval -> Cil.lval option -> unit

  (* Exception for missing main function *)
  exception Not_found_main

  (* Find a return block in the function; if it has undergone the "one return" transformation, the result will be deterministic *)
  val return_of_fn : string -> Ast.Expression.expression option

  val is_important : string -> bool

end with module Command = C_Command

