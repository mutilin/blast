(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module M = Message
open FociInterface

let csisat : (Pervasives.in_channel * Pervasives.out_channel * Pervasives.in_channel) option ref = ref None
let get_csisat () =
  match !csisat with
    | None -> 
        let cmdline = Printf.sprintf "csisatServer -round -int %s" (Options.getValueOfString "csisat_opts") in
        M.msg_string M.Normal (Printf.sprintf "Forking CSIsat process with cmdline '%s'..." cmdline);
        let ic, oc, ec = Unix.open_process_full cmdline (Unix.environment ()) in
          csisat := Some(ic, oc, ec) ;
          M.msg_string M.Normal "done!\n";
          (ic,oc)
    | Some(ic, oc, _) -> (ic, oc)
and stop_csisat() = 
  match !csisat with
    | None -> ()
    | Some (ic,oc,ec) -> 
        begin
          try
            M.msg_string M.Debug "Stopping CSIsat process...";

            ignore (Unix.close_process_full (ic,oc,ec) );

            csisat := None;
            M.msg_string M.Normal "CSIsat stopped."
          with _ -> failwith "failed to stop CSIsat!"
        end

let interpolateN pred_lst =
  (* some IO helper*)
  let line: string option ref = ref None in

  let peek_line in_channel =
    M.msg_string M.Debug "peeking line";
    match !line with
    | Some l -> l
    | None ->
      begin
        let str = input_line in_channel in
          line := Some str;
          str
      end
  in

  let pop_line in_channel =
    M.msg_string M.Debug "poping line";
    match !line with
    | Some l ->
      begin
        line := None;
        l
      end
    | None -> input_line in_channel
  in
  (* end IO helpers *)

  let (csisat_out, csisat_in) = get_csisat () in
    clear_symbol_table();
    let normalize_pred_lst = List.map (normalize_symbol) pred_lst in
    let query = print_foci normalize_pred_lst in
      M.msg_string M.Debug ("CSIsat query: " ^ query);
      output_string csisat_in query ;
      output_char csisat_in '\n';
      flush csisat_in;
      let n = List.tl pred_lst in (*Number of expected interpolants*)
      M.msg_string M.Debug ("query sent, expecting " ^ string_of_int (List.length n) ^ " interpolants");
      try
          if Misc.is_prefix "Satisfiable" (peek_line csisat_out) then
              raise (SAT ([],[]))
          else
              List.map (fun _ -> restore_symbol (List.hd (parse_foci (pop_line csisat_out)))) n
        with Failure e ->
          failwith ("error when reading CSIsat output: " ^ e)
