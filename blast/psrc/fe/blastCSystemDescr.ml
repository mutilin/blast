(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(**
   Depends on:
     Ast

**)

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate

(* Alias old(?) vampyre stats to usual stats *)
module VampyreStats = Stats


module FakeAliasAnalyzer = (* To hack around the problem of alias analysis
			      calls made by the TAR-havoc code, we introduce
			      this analyzer
			      *)
struct
  let getAllAliases lval = []
end


module C_Command =
struct
  type statement =
      Expr of Expression.expression
    | Return of Expression.expression (* option *)

  type command =
      Skip
    | Block of statement list
      (* Remark: at the C level, a predicate is just another expression
         but it may prove useful to differentiate them when we can (e.g.
         for conditionals.) *)
    | Pred of Predicate.predicate
      (* Remark: the expression of a function call is assumed to be
         either:
           - an Expression.FunctionCall(_), or
           - an Expression.Assignment (_, _, Expression.FunctionCall(_)) *)
    | FunctionCall of Expression.expression
    | GuardedFunctionCall of (Predicate.predicate * Expression.expression) (* guarded function calls disambiguate function pointer
									      calls. A function pointer call *f() translates to
									      GuardedFunctionCall( *f==foo, foo()) 
									      for each function foo that can be aliased to *f 

									      More notes: function pointer calls are disambiguated
									      to GuardedFunctionCall in enabled_ops
									      *)
    | Havoc of Expression.lval (*Symbol.symbol*)
        (* [Havoc x]
           Forget all that is known about x, unless \phi(x) is known to
           hold. This action models the behavior of other threads. *)
    | HavocAll
        (* Havoc all global variables *)
	(* Not currently used *)
    | Phi of Ast.Expression.lval * bool
        (* [Phi (x, sign)]
           This is semantically equivalent to [Pred <<sign iff \phi(x)>>].
           Only used by race condition algorithm. *)
    | SymbolicHook of string
        (* used in CF reach -- behaves as "skip" except when generating
         * constraints during refine -- then, for each global_mod lv,
         * an __assignment__ lv = lv *)

  type t = {
      code : command;
      read : Expression.lval list;
      written : Expression.lval list;
    }

  let local_cmd code = {code = code; read = []; written = []}
  let skip = local_cmd Skip
  let cmd_true = local_cmd (Pred Predicate.True)


  let gather_accesses code =
    let module S = Set.Make(
      struct type t = Expression.lval
	    let compare = Pervasives.compare
      end
	    ) 
    in
    let is_global sym =
      not (String.contains sym '@')
    in
      (* JHALA: note that expr_w is not used anywhere ! 
	 expr_w w  = function
	 | Expression.Lval lv -> lvalue_w w lv
	 | Expression.Chlval _ -> failwith "gather_accesses : Chlval unexpected!"
	 | Expression.Binary (Expression.FieldOffset, e1, e2) -> w (* CHECK THIS *) 
	 | Expression.Binary (op, e1, e2) -> let w' = expr_w w e1 in expr_w w' e2
	 | Expression.Cast (ty, e) -> expr_w w e
	 | Expression.Constant _ -> w
	 | Expression.Constructor _ -> w
	 | Expression.FunctionCall (e, es) -> List.fold_left expr_w w (e :: es)
	 | Expression.Sizeof _ -> w
         | Expression.Unary (Expression.AddressOf, e) -> w (* nothing under an address-of is written *)
	 | Expression.Unary (op, e) -> expr_w w e
	 | e -> failwith ("expr_w : Strange expression "^(Expression.toString e))
	 and *)

(* JHALA: so in the next 2 functions,
   the l is an lval, but we have to 
   recursively go down inside it ... *)

  let rec
      lvalue_w (r, w as rw) l = 
      let (r', w') = 
	match l with
	  | Expression.Symbol s -> rw
          | Expression.This -> failwith "lvalue_w: This not handled!"
	  | Expression.Access (op,e,s) -> expr rw e
	  | Expression.Dereference e -> expr rw e 
	  | Expression.Indexed (e1, e2) -> 
	      begin 
		let rw' = expr rw e1 in expr rw' e2
	      end
      in
	(r', S.add l w')
	
    and
      lvalue (r, w as rw) l = 
      
      let (r',w') = 
	match l with
	  | Expression.Symbol s -> rw
          | Expression.This -> failwith "lvalue: This not handled!"
	  | Expression.Access (op,e,s) -> expr rw e
	  | Expression.Dereference e -> expr rw e 
	  | Expression.Indexed (e1, e2) -> 
	      begin
		let rw' = expr rw e1 in
		expr rw' e2
	      end
      in
	(S.add l r', w')
    and
      expr (r, w as rw) = function
	| Expression.Lval lv -> lvalue rw lv
	| Expression.Chlval _ -> failwith "gather_accesses : Chlval unexpected!"
	| Expression.Assignment (op, lv, e2) ->
            let rw' = expr rw e2 in
  	      lvalue_w rw' lv 
	| Expression.Binary (Expression.FieldOffset, e1, e2) -> expr rw e1 
	| Expression.Binary (op, e1, e2) -> let rw' = expr rw e1 in expr rw' e2
	| Expression.Cast (ty, e) -> expr rw e
	| Expression.Constant _ -> rw
	| Expression.Constructor _ -> rw
	| Expression.FunctionCall (e, es) -> List.fold_left expr rw ((* e ::*) es)
	    (* when the day dawns where we deal with function pointers we shall throw the "e" back in  
	       but for now it puts the function names in which is not so nice... *)
	| Expression.Sizeof _ -> rw
	| Expression.Unary (Expression.Address, e) -> rw (* don't go below address-ofs *)
	| Expression.Unary (op, e) -> expr rw e
	| _ -> failwith "blastCSystemDescr: expression not handled in expr"
    in
    let rec predicate rw = function
      | Predicate.True | Predicate.False -> rw
      | Predicate.And ps -> List.fold_left predicate rw ps
      | Predicate.Or ps -> List.fold_left predicate rw ps
      | Predicate.Not p -> predicate rw p
      | Predicate.Implies (p1, p2) -> predicate (predicate rw p1) p2
      | Predicate.Iff (p1, p2) -> predicate (predicate rw p1) p2
      | Predicate.All (sym, p) -> predicate rw p
      | Predicate.Exist (sym, p) -> predicate rw p
      | Predicate.Atom e -> expr rw e
      | _ -> failwith "match failure: lval_w predicate"
    in
    let collect (r, w) =
      { code = code;
	read = S.fold (fun v vs -> v :: vs) r [];
	written = S.fold (fun v vs -> v :: vs) w [] }
    in
      match code with
	| Skip -> skip
	| Block statements ->
	    collect (List.fold_left
		       (fun rw block ->
				  expr rw (match block with Expr e -> e | Return e -> e))
		       (S.empty, S.empty) statements)
	| Pred p -> collect (predicate (S.empty, S.empty) p)
	| FunctionCall e -> collect (expr (S.empty, S.empty) e)
	| GuardedFunctionCall (p, e) -> collect (expr (predicate (S.empty, S.empty) p) e)
	| Havoc lv -> collect (S.empty, S.singleton lv)
        | HavocAll -> assert false
	| Phi _ -> assert false
        | SymbolicHook _ -> skip

  let print_statement fmt stm =
    match stm with
      Expr(e) ->
        Expression.print fmt e ;
        Format.fprintf fmt ";"
    | Return(e) ->
        Format.fprintf fmt "Return(" ;
        Expression.print fmt e ;
        Format.fprintf fmt ");"
	  
	  
  (* the pretty-printer for this type *)
  let print fmt cmd =
    Format.fprintf fmt "@[" ;
    begin
      match cmd.code with
        Skip ->
          Format.fprintf fmt "Skip"
	    
      | Block(l) ->
          Format.fprintf fmt "Block(" ;
          List.iter (print_statement fmt) l ;
          Format.fprintf fmt ")"
	    
      | Pred(p) ->
          Format.fprintf fmt "Pred(" ;
          Predicate.print fmt p ;
          Format.fprintf fmt ")"
	    
      | FunctionCall(e)  ->
          Format.fprintf fmt "FunctionCall(" ;
          Expression.print fmt e ;
          Format.fprintf fmt ")"
      | GuardedFunctionCall(p, e)  ->
          Format.fprintf fmt "GuardedFunctionCall(" ;
          Predicate.print fmt p ;
	  Format.fprintf fmt " => ";
          Expression.print fmt e ;
          Format.fprintf fmt ")"
      | Havoc v ->
          Format.fprintf fmt "Havoc (%s)" (Expression.lvalToString v)
      | HavocAll ->
          Format.fprintf fmt "HavocAll"
      | Phi (v, sign) ->
          Format.fprintf fmt "Pred(%s\\phi(%s))"
            (if sign then "" else "\\not") (Expression.lvalToString v)
      | SymbolicHook fname -> Format.fprintf fmt "SymHook(%s)" fname
              
    end ;
    Format.fprintf fmt "@]"
      
  (* the string converter for this type *)
  let to_string x = 
   try
     let cmd_string = Misc.to_string_from_printer print x in
  (*let maxlen = Options.getValueOfInt "trunc" in
    if String.length cmd_string > maxlen 
    then ((String.sub cmd_string 0 maxlen)^"...")
    else*) cmd_string
   with _ -> Message.msg_string Message.Error "Command to string is the culprit" ; "foo"
      
end
    
    
module C_System_Descr =
  struct
    
    module CFA = BlastControlFlowAutomaton
	

	
  (* The list of CIL files that were processed.
     This is initially "None".
     It is set in initilize 
     *)
    let global_cil_files : Cil.file list option ref = ref None
	
  (* get the list of Cil files.
     This call makes sense only after the files have been parsed.
     *)
    let get_cil_files () =
      match !global_cil_files with
	Some f -> f
      | None -> failwith "No CIL file found!"
	    
	    
  (*******************************************************)
  (** Parameter variables                                *)
  (** These are variables with special meaning for blast *)
  (*******************************************************)
	    
    let tid = "tid"       (* thread id of this thread    *)
    let notTid = "notTid" (* thread id of another thread *)
    let self = "self"     (* generic memory location     *)
    let self_lval = Expression.Symbol self
	
  (*********************************************************************************************)
  (* Blast internal data structures                                                            *)
  (*********************************************************************************************)
	
  (* Source position information in Blast *)
    type source_position = {
	file_name : string ;
	line      : int ;
	column    : int ;
      }
	  
    let print_source_position fmt sp =
      Format.fprintf fmt "@[src=\"%s\"; line=%d@]" sp.file_name sp.line
	
    let cil_location_to_source_position cil_loc =
      { line      = cil_loc.Cil.line ;
	column    = -1 ;
      file_name = cil_loc.Cil.file ;
      }
	
    let source_position_to_string = Misc.to_string_from_printer print_source_position
  (* End Source position information in Blast *)
	
    type location_attributes =
	Original of source_position
      | Artificial
	  
    let print_location_attributes fmt = function
	Original (sp) ->
          Format.fprintf fmt "@[Original(" ;
          print_source_position fmt sp ;
          Format.fprintf fmt ")@]"
      | Artificial ->
          Format.fprintf fmt "@[Artificial@]"
	    
    type automaton_attributes = {
	label_table : (string, (automaton_attributes,
				location_attributes,
				C_Command.t) CFA.location) Hashtbl.t ;
      }
	  
    type formals_kind =
	Fixed of Symbol.symbol list
      | Variable of Symbol.symbol list
	    
    let print_formals fmt = function
	Fixed l -> (Misc.list_printer_from_printer Symbol.print) fmt l
      | Variable l -> (Misc.list_printer_from_printer Symbol.print) fmt (l@["..."])
	    
    type function_definition = {
	function_name    : string ;
	formals          : formals_kind ;
	locals           : string list ;
	cfa              : (automaton_attributes,
                            location_attributes,
                            C_Command.t) CFA.automaton ;
	fun_src_pos      : source_position ;
	fun_attributes   : Cil.attributes ; }
	  
    type variable_definition = {
	var_name         : string ;
	var_src_pos      : source_position ;
	var_is_lock      : bool ;
	var_vaddrof : bool ; (* the Cil vaddrof flag: true if the address of this variable has been taken *)
      }
	  
    let global_function_table = Hashtbl.create 101
    let global_label_table = Hashtbl.create 101

    let global_coords_location_table = Hashtbl.create 1001
   
    let cfa_create_location fname a attrs =
      let loc = CFA.create_location fname a attrs in
      let coords = (loc.CFA.automaton.CFA.aut_id, loc.CFA.loc_id) in
      Hashtbl.replace global_coords_location_table coords loc;
      loc

    let lookup_location_from_loc_coords (x,y) = 
      try Hashtbl.find global_coords_location_table (x,y)
      with Not_found -> failwith (Printf.sprintf "Cannot find location: (%d,%d)" x y)
      
    
    (* The dummy function table is used in the slicing optimizations. The idea is that if a function
       is not relevant, its body can be replaced by a dummy function that nondeterministically calls
       all the called functions in any order.

       The dummy cfa of a function fname has the name __BLAST_DUMMY_fname. These are created by
       the function build_dummy_cfa fname
     *)	
    let dummy_function_table = Hashtbl.create 101 

  (*********************************************************************************************)
  (* Function call depth restiction helpers                                                    *)
  (*********************************************************************************************)

    (* A function call stack depth restriction may be simple or adaptive.  A simple restriction
       just cuts calls to the functions at certain stack depth.  An adaptive restriction may
       never cut a call that happens on a path to certain sets of labels (for instance, to an
       error label.
       This table stores functions calls can't be cut to.
     *)
    let important_function_table = Hashtbl.create 101
	
  (************************************************************************)
  (* Global lvalues and associated information maintained as a hash table *)
  (* Functions for handling global lvalues                                *)
  (************************************************************************)
  (*                                                                                          *)
  (* Iterate over all lvalues in a Cil file and add them to the all_lvals_hashtbl *)
	
    let scope_of_lval lval =
      let rec _lvs_e e =
	match e with
	  Expression.Lval l -> _lvs l
        | Expression.Binary (_, e1, e2) -> Misc.sort_and_compact ((_lvs_e e1) @ (_lvs_e e2))
        | Expression.Unary (_, e1) -> (_lvs_e e1)
	| _ -> (* Message.msg_string Message.Major
	      ("Warning: scope_of_lval : Strange expression"^(Expression.lvalToString lval));*) []
      and _lvs lv =
	match lv with
	  Expression.Symbol s ->
	    if not (String.contains s '@') then [""]
	    else [(Str.string_after s ((String.index s '@')+1))]
        | Expression.This -> failwith "This not handled in scope_of_lval"
	| Expression.Dereference e -> _lvs_e e
	| Expression.Access (_,e,_) -> _lvs_e e
	| Expression.Indexed (e1, e2) ->
	    Misc.sort_and_compact ((_lvs_e e1)@(_lvs_e e2))
      in
      let rv = _lvs lval in
(*
      Message.msg_string Message.Debug
	("In scope_of_lval: "^(Expression.lvalToString lval)^" : "^(Misc.strList rv));
*)
      rv
	
	
  (***********************************************************************************************)
  (************************* Additional stuff to partition lvals by scope ************************)
  (***********************************************************************************************)
	
    let scope_lvals_table = Hashtbl.create 31 (* vicious. fname -> (hashtbl ref) *)
	
    let get_scope_lvals_table fname =
      try
	let rv =Hashtbl.find scope_lvals_table fname in
	!rv
      with
	Not_found ->
	  let new_table = Hashtbl.create 31 in
	  Hashtbl.replace scope_lvals_table fname (ref new_table);
	  new_table
	    
    let add_scope_lval fname =
      let table = get_scope_lvals_table fname in
      Hashtbl.replace table 
	
    let remove_scope_lval fname =
      let table = get_scope_lvals_table fname in
      Hashtbl.remove table 
	
    let exists_scope_lval fname lv =
      let table = get_scope_lvals_table fname in
      let gtable = get_scope_lvals_table "" in
      Hashtbl.mem table lv || Hashtbl.mem gtable lv
	
	
	
    let iterate_all_scope_lvals fname f =
      Hashtbl.iter (fun lv _ -> f lv) (get_scope_lvals_table fname); 
      Hashtbl.iter (fun lv _ -> f lv) (get_scope_lvals_table "")
      (* global lvals -- always in scope. Ja. *)
	
  (*** ONLY USE add_scope_lval, remove_scope_lval, exists_scope_lval, iterate_all_scope_lvals *)
	
  (***********************************************************************************************)

	(** vardecl_hashtbl keeps track of the Cil vardecl structure associated
	    with every variable. This is used in finding out types etc 
         *)
    let vardecl_hashtbl = Hashtbl.create 101

    let add_vardecl lv vdecl =
      Hashtbl.add vardecl_hashtbl lv vdecl 

    let find_vardecl = Hashtbl.find vardecl_hashtbl
    let exists_vardecl = Hashtbl.mem vardecl_hashtbl

    let all_lvals_hashtbl = Hashtbl.create 101
    
    let rec add_lval lv (d : Cil.lval option) =
      Message.msg_string Message.Debug ("add_lval: "^(Expression.lvalToString lv));
      if not (Hashtbl.mem all_lvals_hashtbl lv) then
        begin
          Hashtbl.add all_lvals_hashtbl lv d;
          let scopes = scope_of_lval lv in
          (List.iter (fun fn -> (add_scope_lval fn) lv d) scopes);
          List.iter (fun l -> add_lval l None)
            (Expression.lvals_of_expression (Expression.Lval lv))
        end

    let remove_lval lv =
      Hashtbl.remove all_lvals_hashtbl lv;
      let scopes = scope_of_lval lv in
      List.iter (fun fn -> (remove_scope_lval fn) lv) scopes
	
    let exists_lval = Hashtbl.mem all_lvals_hashtbl 

    let find_lval = Hashtbl.find all_lvals_hashtbl (* may throw Not_found *)

    let iterate_all_lvals f = Hashtbl.iter (fun lv _ -> f lv) all_lvals_hashtbl
  (* only use add_lval,remove_lval,exists_lval,find_lval,iterate_all_lvals *)

  (***********************************************************************************************)

	(* Utils for discarding must aliases 
	*)
    let all_but_musts_lvals = ref [] 
    (* dummy value -- just to make iteration do something and prevent compiler optimization *)
    let valuu = ref 0 

    (* create a new list with only non-must aliases *)
    let withdraw_musts musts =
      all_but_musts_lvals := Hashtbl.fold 
        (fun must_alias _ l ->
	  if (Hashtbl.mem musts must_alias) then l else must_alias::l
	)
	all_lvals_hashtbl []
    
    let iterate_all_but_musts_lvals f = 
    List.iter (fun lv -> 
    f lv) !all_but_musts_lvals
  (* only use add_lval,remove_lval,exists_lval,find_lval,iterate_all_lvals *)
	
 	
    class addLvalClass = object  
      inherit Cil.nopCilVisitor
	  
      method vlval lv =
	let blast_lval = BlastCilInterface.convertCilLval lv in
	if exists_lval blast_lval then () 
	else
	  begin
          (* keep the original Cil lval as well,
             to get type info etc.
             *)
            match blast_lval with
              Expression.Symbol s -> 
		if (Misc.is_prefix s "__BLAST_NONDET") 
		then () 
		else
		  add_lval blast_lval (Some lv)  
		   
            | Expression.This -> failwith "this not handled in addLval" 
            | Expression.Dereference e -> 
		add_lval blast_lval (Some lv)  
            | Expression.Access (Expression.Arrow, Expression.Lval l, _) -> 
		add_lval blast_lval (Some lv) ;
              (* add *l to lvals *)
		if not (exists_lval l) then add_lval l None 
            | Expression.Access (Expression.Dot, Expression.Lval l, _) -> 
		add_lval blast_lval (Some lv) ;
              (* add l to lvals *)
		if not (exists_lval l)  then add_lval l None 
	    | Expression.Indexed (e1, e2) -> 
		add_lval blast_lval (Some lv) 
	    | _ -> failwith ("add_lvals : Strange lval "^(Expression.lvalToString blast_lval))
	  end ;
	Cil.DoChildren
	  
      method vvdec vardec = 
	if (not (exists_vardecl (Expression.Symbol vardec.Cil.vname))) then
	  add_vardecl (Expression.Symbol vardec.Cil.vname) (vardec) ;
        Cil.DoChildren	
    end
      
    let add_all_lvals cil_file =
    let ob = new addLvalClass in
    let _ = Cil.visitCilFile ob cil_file
    in
    ()
          
  let print_all_lvals () = 
    iterate_all_lvals 
      (fun l -> 
	Message.msg_string 
	  Message.Normal 
	  ("Lval " ^ (Expression.lvalToString l)))
      
(**********************************************************************************)
  (************************************************************************)
  (* Some generic utilities to get information out of the Cil structures. *)
  (************************************************************************)
      

(*
  (* Given a string, find the varinfo structure corresponding to that
     variable name (ie with the same vname)
     
   Since, in the files we consider, all variables have unique names,
     the list vinfoL will have exactly one member.
     
     Returns an empty list if there is no variable with that name.
     *)
  let vinfoL = ref []
      
  class getVarinfoClass str = object
    inherit Cil.nopCilVisitor
	
	
    method vvdec vardec =
      if (vardec.Cil.vname = str) then
        vinfoL := vardec :: !vinfoL
      else ();
      Cil.DoChildren
	
  end
*)      
	(* given a string, return the Cil varinfo associated with it,
	   and raise Not_found if there is no such variable.
	   *)
  let get_varinfo str =
    try
      find_vardecl (Expression.Symbol str) 
    with _ ->
      begin
        Message.log_string_norm Message.Cil ("get_varinfo cannot find lvalue "^str) ;
	failwith ("get_varinfo cannot find lvalue "^str) 
(*
	let getVars str file =
	  let ob = new getVarinfoClass str in
	  let _ = Cil.visitCilFile ob file in
	  let vL = !vinfoL
	  in
	  vinfoL := []; vL
	in
	let cil_files = get_cil_files () in
	let vars =  List.fold_left (fun vars f -> List.append (getVars str f) vars) [] cil_files
	in
	match vars with
	  [] -> raise Not_found
	| [vinfo] -> vinfo
	| vinfo:: _ -> vinfo
*)
      end
	
	  (* get the Cil.typ type of an lvalue *)
  let rec get_type_of_lval lv =
    (* Message.msg_string Message.Debug ("get_type_of_lval : "^(Expression.lvalToString lv)) ; *)
    match lv with
      Expression.Symbol s -> 
	begin
	  if Misc.is_prefix "BLAST_UNKNOWN" s then Cil.intType
	    (* TBD:POINTERS we have to have an unknown "address" type as well *)
	  else
	    try
	      Cil.unrollType (get_varinfo s).Cil.vtype
	    with
		ex ->
                  Message.msg_string Message.Debug ("Exn: "^(Printexc.to_string ex));
                  (* see if its a symvar *)
		  let lv' = Expression.peel_symbolic lv in
		    if lv <> lv' then get_type_of_lval lv'
		    else
                      let s' = ("get_type_of_lval fails: "^(Printexc.to_string ex)^" on variable "^s) in
		      (Message.msg_string Message.Debug s';
                      failwith s')
	end
    |	Expression.Dereference e ->
	begin
	  let etyp = Cil.unrollType (get_type_of_exp e) in
	  match etyp with
	    Cil.TPtr (t, att) -> Cil.unrollType t
	  | Cil.TArray (t, eop, att) -> Cil.unrollType t
	  | _ -> failwith ((Expression.toString e)^ "is Not a pointer type "^(Expression.lvalToString lv))
	end
    |	Expression.Access (Expression.Dot, e, fld) ->
	begin
	  let etyp = Cil.unrollType (get_type_of_exp e) in
	  match etyp with
	    Cil.TComp (c, att) -> 
	      begin
		let fldinfo = Cil.getCompField c fld in
		Cil.unrollType fldinfo.Cil.ftype
	      end
	  | _ -> failwith ("Not a composite type "^ (Expression.lvalToString lv) )
	end
    |	Expression.Access (Expression.Arrow, e, fld) ->
	get_type_of_lval (Expression.Access (Expression.Dot, Expression.Lval (Expression.Dereference e), fld))
    |	Expression.Indexed (e1, e2) ->
	  begin
	  let etyp = Cil.unrollType (get_type_of_exp e1) in
	  match etyp with
	    Cil.TArray (at, _, _) -> 
	      begin
		Cil.unrollType at
	      end
	  | Cil.TPtr (t, _) -> Cil.unrollType t
	  | _ -> failwith ("Not an array type "	^ (Expression.lvalToString lv))
	  end
	  (* get the type of an expression *)
    | _ -> failwith "match failure: get_type_of_lval"
  and get_type_of_exp exp =
    begin
      match exp with
	Expression.Lval l -> get_type_of_lval l
      | Expression.Chlval _ -> failwith ("get_type : Chlval not handled "^(Expression.toString exp))
      | Expression.Binary (Expression.Plus, e1, e2) (* pointer arithmetic. Assume pointer variable first *) 
      | Expression.Binary (Expression.Minus, e1, e2)  
      | Expression.Binary (Expression.Offset, e1, e2) -> 
        begin
	  let t1 = get_type_of_exp e1 and t2 = get_type_of_exp e2 in
          match t1 with
            Cil.TPtr _ -> t1
          | Cil.TArray _ -> t1
          | _ -> t2
        end
      | Expression.Binary (Expression.FieldOffset, e1, Expression.Lval (Expression.Symbol f)) -> 
	Cil.TPtr (get_type_of_lval (Expression.Access(Expression.Dot, Expression.Lval (Expression.Dereference e1), f)), [])	
      | Expression.Binary (_, _, _) -> failwith ("get_type :  not handled "^(Expression.toString exp))
      |	Expression.Unary (Expression.Address, e) -> 
	  Cil.TPtr (get_type_of_exp e, [])
      | Expression.Unary (_, _) ->failwith ("get_type :  not handled "^(Expression.toString exp))
      |	Expression.Constant c ->
	  begin
	    match c with
	      Constant.Int _ -> Cil.intType
	    | Constant.Float _ -> Cil.doubleType
            | Constant.String _ -> Cil.charConstPtrType
	    | _ -> failwith ("get_type : not handled "^(Expression.toString exp))
	   
	  end
      | Expression.Select (_,a) -> get_type_of_exp (Expression.Lval (Expression.push_deref (Expression.Dereference a)))
      | _ -> failwith ("get_type : not handled "^(Expression.toString exp))
    end
      
	  (* Given an "access" lvalue, find the Cil fieldinfo associated with it *)
  let get_fieldinfo lval =
    match lval with
      Expression.Access (Expression.Dot, e, fld) ->
	begin
	  try
	    let typ = get_type_of_exp e
	    in
	    match typ with
	      Cil.TComp (c, _) -> Cil.getCompField c fld
	    | _ -> failwith "get_fieldinfo : Strange type "
	  with
	    Not_found -> failwith ("get_fieldinfo : No such field " ^ (Expression.lvalToString lval))
	end
    |  Expression.Access (Expression.Arrow, e, fld) ->
	begin
	  try
	    let typ = get_type_of_exp e
	    in
	    match typ with
	      Cil.TPtr (Cil.TComp (c, _), _) -> Cil.getCompField c fld
	    | _ -> failwith "get_fieldinfo : Strange type "
	  with
	    Not_found -> failwith ("get_fieldinfo : No such field " ^ (Expression.lvalToString lval))
	end
    | _ -> failwith "get_fieldinfo: Not a field access"
	  
	  
   let is_struct lval = 
     let tp = get_type_of_lval lval in
     match tp with
       Cil.TComp _ -> true
     | _ -> false
    
   let is_array lval =
     let tp = get_type_of_lval lval in
     match tp with
       Cil.TArray _ -> true
     | _ -> false

   let is_ptr lval =
     let _ = Message.msg_string Message.Debug ("In is_ptr: "^(Expression.lvalToString lval)) in
     let rv = 
       let tp = get_type_of_lval lval in
	 match tp with
	     Cil.TPtr _ -> true
	   | _ -> false
     in
     let _ = Message.msg_string Message.Debug ("Returns: "^(string_of_bool rv)) in
       rv
	 
   let is_skolem lv = 
     let rec find_base lv =
       match lv with 
       | Expression.Symbol s -> lv
       | Expression.Dereference e -> find_base_e e
       | Expression.Access(_, e1, _) -> find_base_e e1
       | Expression.Indexed(e1, _) -> find_base_e e1
     and find_base_e e =
       match e with
       | Expression.Lval l -> find_base l
       | _ -> failwith "C_System_Descr.is_skolem: strange expression in find_base"
     in
     try 
       let base = find_base lv in
       match find_lval base with
         Some (Cil.Var cilv,_) -> Cil.hasAttribute "skolem" cilv.Cil.vattr
       | _ -> false
     with Not_found -> false
      
  (*********************************************************************************************)
  (* End general Cil utilities                                                                 *)
  (*********************************************************************************************)

  


  (* Information associated with an lvalue: 
     Cil.varinfo if the lvalue is a variable.
     Cil.fieldinfo if the lvalue is a field access.
     *)
  type global_lval_information = 
      Varinfo of Cil.varinfo
    | Fieldinfo of Cil.fieldinfo
	  
  let global_variable_table = Hashtbl.create 31
     
  let type_global_map_table = Hashtbl.create 31

  (* iterate over the global variables table and apply function f to each element *)
  let iter_global_variables f = Hashtbl.iter (fun name _ -> f name) global_variable_table 
      
  (* TODO : disambiguate two field references of the same name:
     e.g., struct foo { int data ; }
     struct bar { int data ; }
     will now have just one entry : this.data.
     *)
  let rec add_global lval info =
    let rec_add styp =
      let rec walk_ptrs t =
	match t with
	  Cil.TPtr (t', _) -> walk_ptrs t'
	| t' -> t
      in
      match styp with
	Cil.TComp (c, _) -> 
	  begin (* composite type : recursively add all field information to global table. *)
		(* for each fieldinfo, add this.fieldinfo to global table *)
	    List.iter 
	      (fun f -> 
		add_global (Expression.Access (Expression.Dot, Expression.Lval self_lval, f.Cil.fname)) (Fieldinfo f))
	      c.Cil.cfields
	  end
      | Cil.TPtr (pt, _) ->
	  begin
	    match walk_ptrs pt with 
	      Cil.TComp (c, _) ->
				(* for each fieldinfo, add this.fieldinfo to global table *)
		List.iter 
		  (fun f -> 
		    add_global (Expression.Access (Expression.Dot, Expression.Lval self_lval, f.Cil.fname)) (Fieldinfo f))
		  c.Cil.cfields
	    | Cil.TVoid _ -> () (* ignore void * !!  failwith "Do not handle void *"*)
	    | _ -> ()
	  end
      |	_ -> () (* do not add anything *)
    in
    if (Hashtbl.mem global_variable_table lval) then (* this has already been seen *)
      () 
    else
      begin
	Misc.hashtbl_update type_global_map_table (get_type_of_lval lval) lval;
        Hashtbl.add global_variable_table lval info
	(*
	match lval with
	  Expression.Symbol s -> 
	    begin
	      if s = self then () 
	      else
		let styp = get_type_of_lval lval in
		rec_add styp
	    end
	| Expression.Access (Expression.Dot, e, fld) ->
	    ()
	    
	(* if the fieldinfo is composite, recursively add these to the global table *)
	    begin
	      match info with
		Fieldinfo finfo ->
		  begin
		    let ftyp = Cil.unrollType finfo.Cil.ftype in
		    rec_add ftyp
		  end
	      | _ -> failwith "add_global: an access is accompanied by the wrong information!"
	    end 
	| _ -> failwith "add_global : Not handled" *)
      end

(*****************************************************************************************)
	
  let rec is_global s =
    let rec is_global_e e =
      match e with
	Expression.Lval l -> is_global l
      |	Expression.Chlval _ -> false
      |	_ -> false (* failwith ("is_global: Strange expression "^(Expression.toString e))*)
    in
    match s with 
      Expression.Symbol ss ->
(*	assert (Hashtbl.mem global_variable_table s == not (String.contains ss '@')) ; *)
	not (String.contains ss '@')
    | Expression.This -> failwith "is_global: This not handled"
    | Expression.Dereference e -> is_global_e e
    | Expression.Access (_, e, _) -> is_global_e e
    | Expression.Indexed (e1, _) -> is_global_e e1

  let can_escape lv =
    Message.msg_string Message.Debug ("IN can_escape, with lvalue "^(Expression.lvalToString lv)) ;
    let rec _can_escape seen lv =
      let rec exp_can_escape e =
	match e with
	| Expression.Lval l -> _can_escape seen l
	| Expression.Chlval (l,_) -> _can_escape seen l
	| Expression.Binary (_, e1, e2) -> exp_can_escape e1 || exp_can_escape e2
	| Expression.Cast (t, e1) -> exp_can_escape e1
	| Expression.Constant(_) -> false
	| Expression.Constructor (_, _) -> failwith "Constructor not handled in can_escape"
	| Expression.FunctionCall (func, args) ->
	    begin
	      let rec loop lst =
		match lst with 
		  [] -> false
		| a:: rest -> if exp_can_escape a then true else loop rest
	      in
	      exp_can_escape func || (loop args) 
	    end
	| Expression.Unary(_, e) -> exp_can_escape e
        | Expression.Select (e1,e2) -> exp_can_escape e1 || exp_can_escape e2
        | Expression.Store (e1,e2,e3) -> List.exists exp_can_escape [e1;e2;e3]
        | _ -> failwith "abstractor.can_escape : Strange expression"
      in
      if List.mem lv seen then false else
      begin
	let escapes_directly = 
	  match lv with
	  Expression.Symbol s -> is_global lv 
          | Expression.This -> failwith "escapes_directly: This not handled"
	  | Expression.Dereference e -> exp_can_escape e 
	  | Expression.Access (_, e, _) -> exp_can_escape e 
	  | Expression.Indexed (e1, e2) -> exp_can_escape e1
	in
	escapes_directly || (List.exists (_can_escape (lv::seen)) (FakeAliasAnalyzer.getAllAliases lv))
      end
    in
    let r = _can_escape [] lv
    in
    Message.msg_string Message.Debug ("can_escape returns " ^ (if r then "true" else "false")) ;
    r

  (*
    Make is_a_lock more comprehensive as follows.
    
    An lvalue is a lock if
    (1) it is a symbol, and the symbol has attribute lock.
    (2) it is a field access, and the field has attribute lock.
    (3) it is a dereference, and *all* basic aliases (i.e., symbols
        and field references) of this dereference are locks.
    (4) it is an array, and *all* elements of the array are locks.
    We need all basic aliases to be locks to catch the (unlikely) case when the dereference
    is used sometimes as a lock, and sometimes as data.
    *)
  let rec is_a_lock v =
    try
      match v with 
	Expression.Symbol s ->    
	  begin
	    (* only a moron would have a local lock variable. :-0 *)
            (* Prove me wrong? *)
	    match (Hashtbl.find global_variable_table v) with
	      Varinfo cilv -> 
		let ans = Cil.hasAttribute "lock" cilv.Cil.vattr
		in
		Message.msg_string Message.Debug ("Answer to is_a_lock is "^(if ans then "true" else "false")) ;
		ans
	    | _ -> failwith ("Incorrect information in global table associated with variable "^s)
	  end
      | Expression.Access (_, _, fld) -> 
	  begin
	    let l = Expression.Access (Expression.Dot, Expression.Lval self_lval, fld)
	    in
	    match (Hashtbl.find global_variable_table l) with
	      Fieldinfo f -> 
		let ans = Cil.hasAttribute "lock" f.Cil.fattr
		in
		Message.msg_string Message.Debug ("Answer to is_a_lock is "^(if ans then "true" else "false")) ;
		ans
	    | _ -> failwith ("Incorrect information in global table associated with lvalue "^(Expression.lvalToString l))
	  end
      | Expression.Dereference e ->
	  begin
	    let rec all_locks lst at_least_one_lock_seen =
	      Message.msg_string Message.Debug ("In all_locks, flag is "^(if at_least_one_lock_seen then "true" else "false")) ;
	      match lst with
		[] -> at_least_one_lock_seen
	      |	a :: rest ->
		  begin
		    Message.msg_string Message.Debug ("In all_locks, current alias is "^(Expression.lvalToString a)) ;
		    match a with
		      Expression.Symbol s -> 
			begin
			  try
			    match (Hashtbl.find global_variable_table a) with
			      Varinfo vinfo -> 
				if Cil.hasAttribute "lock" vinfo.Cil.vattr
				then
				  all_locks rest true
				else
				  false
			    | _ -> failwith ("Incorrect information in global table associated with variable "^s)
			  with Not_found -> Message.msg_string Message.Debug "Not found! Retur false" ; false
			end
		    | Expression.Access (_, _, fld) ->
			begin		
			  let l = Expression.Access (Expression.Dot, Expression.Lval self_lval, fld)
			  in
			  try
			    match (Hashtbl.find global_variable_table l) with
			      Fieldinfo f -> 
				if Cil.hasAttribute "lock" f.Cil.fattr then
				  all_locks rest true
				else
				  false
			    | _ -> failwith 
				  ("Incorrect information in global table associated with lvalue "^(Expression.lvalToString l))
			  with Not_found -> false (* failwith ("Field "^fld^" not found in global fields table.") *)
			end
		    | _ -> all_locks rest at_least_one_lock_seen
		  end
	    in
	    let list_of_aliases = FakeAliasAnalyzer.getAllAliases v in
	    List.iter (fun l -> print_string (Expression.lvalToString l)) list_of_aliases ;
	    let ans = all_locks list_of_aliases false
	    in
	    Message.msg_string Message.Debug ("Answer is "^(if ans then "true" else "false")) ;
	    ans
	  end
      | Expression.Indexed (e1, e2) -> 
	  begin
	    match e1 with
	      Expression.Lval (l) -> is_a_lock l
	    | _ -> failwith ("is_a_lock: Do not handle indexed lvalues of this type! (To be done)"^(Expression.lvalToString v))
	  end
      | _ -> failwith ("is_a_lock : Do not handle lvalues of this type "^(Expression.lvalToString v)) 
    with Not_found -> Message.msg_string Message.Debug "is_a_lock: Not found!! Returning false." ; false
	
  let is_in_scope fname lval =
     let rec is_in_scope_e e  = 
      match e with
      Expression.Lval l -> is_in_scope_lv l
    | _ -> 
	  begin
	    Message.msg_string Message.Major
	      ("Warning: is_in_scope_e : Strange expression "^(Expression.lvalToString lval)) ;
	    false
	  end
  and is_in_scope_lv lv = 
	match lv with
	 Expression.Symbol s ->
	      begin 	  (* s should have @fname or no \@ *)
		not (String.contains s '@') ||
		((Str.string_after s ((String.index s '@')+1)) = fname)
	      end
          | Expression.This -> true
	  | Expression.Dereference e -> is_in_scope_e e 
          | Expression.Access (_, e, _) -> is_in_scope_e e 
          | Expression.Indexed (e1, e2) ->
	      ((is_in_scope_e e1) && (is_in_scope_e e2))
  in
    is_in_scope_lv lval

  let globals_of_type t = 
    let l = try Hashtbl.find type_global_map_table t with _ -> [] in
    List.filter is_global l 

  
(*****************************************************************************************************************)
(********************************************LVALUE CLOSURE STUFF ************************************************)
(*****************************************************************************************************************)

let stats_nb_cached_exp_closure = ref 0
let stats_nb_recomp_exp_closure = ref 0

(* lval -> lval list *)
  let exp_closure_table = Hashtbl.create 1009

  (* Returns list of lvalues that can be accessed through this one.  On the path to each such lval not more than "-cldepth" (that's a name of an option) dereferences should occur.  *)
  (* fcond fld -- the funciton returns true for the structure fields that we should traverse through.  Note that the amount of field traverses doesn't affect "-cldepth" constraint. *)
  let expression_closure_cond fcond e =
    let fld_seen_tbl = Hashtbl.create 5 in
    (* After that fld_seen_tbl will contain fields (such as in rec.field) that present in expression and its subexpressions *)
    List.iter (fun fld -> Hashtbl.replace fld_seen_tbl fld true) (Expression.fields_of_expr e);
    let rec _rfl depth fstk e  = 
      if depth = 0 then []
      else
        try
	begin
	  match get_type_of_exp e with
	      Cil.TPtr (pt, _) -> 
		let e' = Expression.Lval (Expression.Dereference e) in
		  e'::(_rfl (depth - 1) fstk e') (* only decrement depth for derefs *) 
	    | Cil.TComp (c,_) -> 
		let e's =
                  let check_field fld =
                    let fln = fld.Cil.fname in
                    let c1 = fcond fln in
                    let c2 = not (List.mem fln fstk) (*not (Hashtbl.mem fld_seen_tbl fln) *) 
                    in
                    Message.msg_string Message.Debug 
                    (Printf.sprintf "check field : %s , %b , %b" fln c1 c2);
                    c1 && c2
                  in
		  (* we shall only consider those fields that f okays *)
		  let nice_fields = List.filter check_field (c.Cil.cfields) in
		    List.map 
		      (fun f ->
                        let fln = f.Cil.fname in
			 Hashtbl.replace fld_seen_tbl fln true;
			 (fln,Expression.Lval (Expression.Access (Expression.Dot, e, fln))))
		      nice_fields
		in
                List.flatten 
                (List.map (fun (fln,e') -> e'::( _rfl depth (fln::fstk) e')) e's)
		    (* Note: we do not decrement depth ! *)
	    | _ -> []
	end
          with _-> [e] 
            (*(Message.msg_string Message.Error 
          ("get_type_fails on:"^(Expression.toString e));[e])*)
    in
	let cld = Options.getValueOfInt "cldepth" in
        let seed_fields = if not (Options.getValueOfBool "incref") then
          (Expression.fields_of_expr e) else [] 
        in
	let clos = e::(_rfl cld seed_fields e) in 
	Message.msg_string Message.Debug (Printf.sprintf "Exp closure: cld = %d,
        e = %s, fld = %s, clos = %s"  
        cld (Expression.toString e) (Misc.strList seed_fields)
        (Misc.strList (List.map Expression.toString clos)));
        clos

  (* memoized version of expression_closure_cond *)
  let expression_closure e =
    try Hashtbl.find exp_closure_table e
    with Not_found ->
      let clos =  expression_closure_cond (fun fld -> true) e in
	Hashtbl.replace exp_closure_table e clos;
	clos

  let exp_closure_table_stamp = Hashtbl.create 1009

  let fields_to_unroll_table_all = Hashtbl.create 17  (* monolithic, for now *) 
  let fields_to_unroll_timestamp_all = ref 0
  let fields_to_unroll_table_image = Hashtbl.create 17  (* monolithic, for now *) 
  let fields_to_unroll_timestamp_image = ref 0
				     

let get_table_stamp all_flag = 
  if all_flag then (fields_to_unroll_table_all,fields_to_unroll_timestamp_all)
  else (fields_to_unroll_table_image,fields_to_unroll_timestamp_image)

(* The "add_field_to_unroll" function makes expression closure functions traverse only through the selected fields.  The motivation is that if a field is never encountered in a trace, then the assignment to it is useless (and if such an assignment is not constructed, and there was no field f in the trace, then it wouldn't appear on the right-hand side either.) *)
  let add_field_to_unroll all_flag fld = 
    let (table,stamp) = get_table_stamp all_flag in
    if Hashtbl.mem table fld then false
    else 
      begin
	Message.msg_string Message.Normal 
	  (Printf.sprintf "Adding stamp field: all? %b :%s" all_flag fld);
	Hashtbl.replace table fld true;
        stamp := !stamp + 1;
        true
      end

let recompute_closure_and_add all_flag e = 
  let _ = stats_nb_recomp_exp_closure := !stats_nb_recomp_exp_closure + 1 in
  let (table,stamp) = get_table_stamp all_flag in 
  let fcond_stamp = Hashtbl.mem table in  
  let clos = (expression_closure_cond fcond_stamp e) in
    Hashtbl.replace exp_closure_table_stamp (all_flag,e) (!stamp,clos);
    clos
  
let expression_closure_stamp all_flag e =
  let (time,l) = 
    try Hashtbl.find exp_closure_table_stamp (all_flag,e) 
    with Not_found -> (-1,[])
  in
  let (_,stamp) = get_table_stamp all_flag in
    if time = !stamp then
      (stats_nb_cached_exp_closure := !stats_nb_cached_exp_closure + 1; l)
    else VampyreStats.time "recompute_and_add" recompute_closure_and_add all_flag e


      
  let ac_lvalue_closure ecf lv = 
    (* Message.msg_string Message.Debug ("In lvalue_closure_cond: "^(Expression.lvalToString lv)); *)
    let rv = 
	try
	  ecf (Expression.Lval lv)
	with _ ->
	  (* for some reason, it can't find the types or something! *)
	  begin
	    Message.msg_string Message.Major
	      ("Warning: lvalue_closure_cond - unknown type: "^(Expression.lvalToString lv));
	    [(Expression.Lval lv)]  (* or should this list be empty ? *)
	  end
    in
      List.map
	(fun e -> Expression.push_deref (Expression.lv_of_expr e))
	(* no exception will be raised *)
	rv

  let lvalue_closure_cond f_cond = ac_lvalue_closure (expression_closure_cond f_cond)
  let lvalue_closure = ac_lvalue_closure (expression_closure)
  let lvalue_closure_stamp all_flag = ac_lvalue_closure (expression_closure_stamp all_flag)

(* use: 
   add_field_to_unroll fld
   {expression,lvalue}_closure{ ,_stamp}
*)


(***********************************************************************************)
(* Incremental Expression Closures                                                 *)
(***********************************************************************************)
(*
This "incremental" version of expression closures allows us to compute precise closures, including those for the recursive types with infinite depth, without losing precision.
When a trace is analyzed from the last element, it's most likely an assume statement.  Note that, unlike in POPL'04 paper, this version of BLAST (without -cf) does not use closures in assume-s.  So only the values that actually matter for disproving a spurious trace at this point are those of the lvals in the predicate, and there's no point in assigning anything to the other lvals.  Applying this reasonoing inductively, we only need to include the lvals in the already build part of the trace to the closure of the current one.

Actually, if the assignments to these closures are nevertheless laid out, they'll just be ruled out by a solver.  However, incremental expression closure aims to improve the overall system performance, and alleviating solvers' work is not among its aims.
*)


type closure_elem_t = Expression.expression
let str_of_elem = Expression.toString
type closure_result_t = Expression.expression
let str_of_result = Expression.toString

module ClosMap = Map.Make(struct
  type t = closure_elem_t
  let compare = compare
end)

type closure_state = {
  (* mapping from the lvals to the lists of closure entities.  We will query it in O(1), but take the time to build when lvals are queried *)
  closure_map : (closure_result_t list) ClosMap.t;

  (* we don't need to add an expression twice, so this is just a shortcut test *)
  closure_results : bool ClosMap.t;
}

let empty_closurer = {
  closure_map = ClosMap.empty;
  closure_results = ClosMap.empty;
}

let debug_closure_each_step = ref false

let print_closurer fmt c =
  (* helper *)
  let fp = Format.fprintf fmt in
  fp "[CLOSURE-->@\n";
  ClosMap.iter (fun b ll ->
    Format.fprintf fmt "%s -> @[" (str_of_elem b);
    List.iter (fun l -> Format.fprintf fmt "%s@ " (str_of_result l)) ll;
    fp "@]@\n";
  ) c.closure_map;
  fp "<--closure]"

let add_expr_to_closurer c e =
  let moz map x = try ClosMap.find x map with Not_found -> [] in
  let _add_expr_to_closurer c e =
    let lvals_to_close = Expression.lvals_of_expression e in
    let new_map = List.fold_left (fun cm lv -> let lve = (Expression.Lval lv) in ClosMap.add lve (e::(moz cm lve)) cm) c.closure_map lvals_to_close in
    let new_results = ClosMap.add e true c.closure_results in
    {c with closure_map = new_map; closure_results = new_results}
  in
  (* Do not add anything if it will change nothing *)
  if ClosMap.mem e c.closure_results then c
  else
  let rv = Stats.time "Incremental Closure update" (_add_expr_to_closurer c) e in
  let _ = Message.msg_string Message.Debug (Printf.sprintf "add_expr_to_closurer %s -> " (str_of_elem e) ) in
  if (!debug_closure_each_step) then Message.msg_printer Message.Debug print_closurer rv else ();
  rv

let query_expr_closure c e =
  let _query_expr_closure c e =
    (* the closure should return the expression given among the result.  That's why it's two steps *)
    let closure_candidate = try ClosMap.find e c.closure_map with Not_found -> [] in 
    if ClosMap.mem e c.closure_results then closure_candidate else e::closure_candidate
  in
  let rv = Stats.time "Incremental Closure query" (_query_expr_closure c) e in
  Message.msg_string Message.Debug (Printf.sprintf "query_expr_closure %s -> %s" (str_of_elem e) (Misc.strList (List.map str_of_result rv)));
  rv




(**********END LVALUE CLOSURE ETC. **************************************************************************************)			


  (********************************************************************************************)


(* jhala: adding code to get around the memtemp problem, which is there are
certain lvals eg. x[i] that blast NEEDS to know about, but doesn't know about,
because Cil does memtemp = x+i followed by a *memtemp. Because blast doesnt know
about the existence of the lval *(x+i), it never considers it as an alias
candidate i.e. a cell that may get smashed when, say *(x+j) is being updated,
which leads to imprecision in the counterexample analysis. The hack is that
whenever we process an assignment: targ = value
and targ is a mem_temp, we shall add to the list of lvals, the closure of targ
with value plugged in in place of targ ... *)
 

  let is_mem_temp lv = 
    match lv with
        Expression.Symbol x -> Misc.is_prefix "mem_temp" x
      | _ -> false
  
  let add_memtemp_closure_lvals targ valu = 
    let clos = expression_closure (Expression.Lval targ) in
    let clos_sub = List.map (Expression.substituteExpression [(Expression.Lval targ, valu)]) clos in
    let clos_sub_lvs = Misc.map_partial (fun e -> (try Some (Expression.lv_of_expr e) with _ -> None)) clos_sub in
    List.iter (fun lv -> add_lval lv None) clos_sub_lvs


  exception CilAsmUnimplemented

  let skip = C_Command.skip

  let rec cil_instr_to_stmt_and_src_pos cil_instr =
    let convert_assignment target value =
      let blast_target = BlastCilInterface.convertCilLval target in
      let blast_value = BlastCilInterface.convertCilExp value in
      if (is_mem_temp blast_target) then add_memtemp_closure_lvals blast_target blast_value ;
      Expression.Assignment (Expression.Assign, blast_target,blast_value)
    and convert_call fn_exp args =
      Expression.FunctionCall (BlastCilInterface.convertCilExp fn_exp,
                               List.map BlastCilInterface.convertCilExp
                                 args)
    in
      match cil_instr with
          Cil.Set (target, value, loc) ->
            (convert_assignment target value,
             cil_location_to_source_position loc)
        | Cil.Call (None, fn_exp, args, loc) ->
            (convert_call fn_exp args,
             cil_location_to_source_position loc)
        | Cil.Call (Some target, fn_exp, args, loc) ->
            (Expression.Assignment (Expression.Assign,
                                    BlastCilInterface.convertCilLval target,
                                    (convert_call fn_exp args)),
             cil_location_to_source_position loc)
        | Cil.Asm _ -> raise CilAsmUnimplemented

  (* this function splits a CIL instruction list into a list of couples
     (cmd, src_pos) where cmd is a C command (block or function call) and
     src_pos is the source_position of the command's starting point *)
  let rec split_cil_instr_list instr_l =
    match instr_l with
        []          -> []
      | instr::tail ->
          match instr with
              Cil.Set _ ->
                let (stmt, src_pos) = cil_instr_to_stmt_and_src_pos instr
                in
                  split_cil_instr_list_cont tail [stmt] src_pos
            | Cil.Call _ ->
                let (stmt, src_pos) = cil_instr_to_stmt_and_src_pos instr in
		let code = C_Command.FunctionCall stmt
		in
                (C_Command.gather_accesses code, src_pos) ::
                (split_cil_instr_list tail)
            | Cil.Asm (_, _, _, _, _, loc) ->
                let msg = loc.Cil.file ^ ":" ^ (string_of_int loc.Cil.line) ^
                          ": " ^ "warning: inline assembly not supported --- skipping it."
                in
                  Message.msg_string Message.Minor msg ;
                  split_cil_instr_list tail

  and split_cil_instr_list_cont instr_l curr_block_stmt_l curr_block_src_pos =
    let make_curr_block () =
      let statements =
        List.rev_map (fun x -> C_Command.Expr x) curr_block_stmt_l
      in
      C_Command.gather_accesses (C_Command.Block statements)
    in
    match instr_l with
        [] ->
          (* remark: current_block_stmt_l cannot be empty *)
          [(make_curr_block ()), curr_block_src_pos]
      | instr::tail ->
            match instr with
                Cil.Set _ ->
                  let (stmt, src_pos) = cil_instr_to_stmt_and_src_pos instr
                  in
                    split_cil_instr_list_cont tail (stmt::curr_block_stmt_l)
                                              curr_block_src_pos
              | Cil.Call _ ->
                  let (stmt, src_pos) = cil_instr_to_stmt_and_src_pos instr in
                  let code = C_Command.FunctionCall stmt in
                  let cmd = C_Command.gather_accesses code in
                    ((make_curr_block ()),
                     curr_block_src_pos) ::
                    (cmd, src_pos) ::
                    (split_cil_instr_list tail)
              | Cil.Asm (_, _, _, _, _, loc) ->
                  let msg = loc.Cil.file ^ ":" ^ (string_of_int loc.Cil.line)
                            ^ ": " ^ "warning: inline assembly not supported --- skipping it."
                  in
                    Message.msg_string Message.Minor msg ;
                    split_cil_instr_list_cont tail curr_block_stmt_l
                                              curr_block_src_pos


  exception CilInstrWithIncorrectNumberOfSuccessors
  exception CilGotoWithIncorrectNumberOfSuccessors
  exception CilIfWithIncorrectNumberOfSuccessors
  exception CilAssumeWithIncorrectNumberOfSuccessors
  exception CilBlockWithIncorrectNumberOfSuccessors
  exception CilBreakUnimplemented
  exception CilContinueUnimplemented
  exception CilSwitchUnimplemented
  exception CilBlockWithNoStatement


  module Havoc = struct
    type havoc_implementation =
      | Undifferentiated
      | Locks_and_optimized_data

    let implementation = Undifferentiated


    let insert_havoc automaton l vl =
      let fname = CFA.location_function_name l in
      if not (can_escape vl)
      then begin
        Message.msg_string Message.Debug ("No havoc of " ^ (Expression.lvalToString vl));
        l (* [vl] is actually a function, hence immutable, so don't havoc it *)
      end else
        let l' = cfa_create_location fname automaton Artificial in
        Message.msg_string Message.Debug ("Inserting havoc of " ^ (Expression.lvalToString vl));
        let havoc_cmd =
          { C_Command.code = C_Command.Havoc vl;
            C_Command.read = []; (* TODO: this changes if \phi changes *)
	    C_Command.written = [ ]; }
        in
        CFA.add_edge l l' havoc_cmd;
        l'

    let insert_havocs automaton vars l0 =
      List.fold_left (insert_havoc automaton) l0 vars

    let insert_all_havocs automaton l =
      let fname = CFA.location_function_name l in
      let l' = cfa_create_location fname automaton Artificial in
      Message.msg_string Message.Debug ("Inserting HavocAll");
      let havoc_cmd =
        { C_Command.code = C_Command.HavocAll;
          C_Command.read = []; (* should be all locks *)
          C_Command.written = []; (* should be all globals *) }
      in
      CFA.add_edge l l' havoc_cmd;
      l'

    let global_variables = ref ([] : Ast.Expression.lval list)
        
    let havoc_cmd lv =
      { C_Command.code = C_Command.Havoc lv;
        C_Command.read = [];
	C_Command.written = [] }

    let phi_cmd sym sign =
      { C_Command.code = C_Command.Phi (sym, sign);
        C_Command.read = [ ];
        C_Command.written = [] }

    let und_one automaton choice_loc entry_loc (x : Expression.lval) =
      
      let fname = CFA.location_function_name entry_loc in
      
      let _add_cfa_edges x =
	if ((is_in_scope fname x ) && (can_escape x )) then 
	  begin
(*
	    Message.msg_string Message.Normal ("Adding havoc for variable "^(Expression.lvalToString x)) ;
*)
	    let havoc_loc = cfa_create_location fname automaton Artificial in
	    CFA.add_edge choice_loc havoc_loc (phi_cmd x true);
	    CFA.add_edge havoc_loc entry_loc (havoc_cmd x);
	    ()
	  end
	else
	  begin
	    Message.msg_string Message.Debug 
	      ("Not adding havoc for lvalue "^(Expression.lvalToString x)^" (Not in scope, or cannot escape).") ;
	    ()
	  end

      in
      match x with
	Expression.Symbol sym ->
	  begin
	    match sym with
	        "tid" 
	    |	"notTid" 
(*
	    |	"self"
*)
	      -> ()
	    |	_	->
		begin
		  _add_cfa_edges x 
		end
	  end
(* Don't need self anymore...
      |	Expression.Access (Expression.Dot, Expression.Lval (Expression.Symbol "self"), fld) ->
	  begin
	    _add_cfa_edges x
	  end
*)
      |	_ -> _add_cfa_edges x

    let notTid_assumption_cmd =
      let zero = Expression.Constant (Constant.Int 0)
      and not_eq z =
        Predicate.Atom (Expression.Binary (Expression.Ne,
                                           (Expression.Lval (Expression.Symbol notTid)),
                                           z))
      in
      { C_Command.code =
        C_Command.Pred
          (Predicate.And [not_eq zero; not_eq (Expression.Lval (Expression.Symbol tid))]);
        C_Command.read = [];
        C_Command.written = [] }

    let undifferentiated automaton vars l0 =
      let fname = CFA.location_function_name l0 in
      let l0' = cfa_create_location fname automaton Artificial in
      let l0'' = cfa_create_location fname automaton Artificial in
      let l1 = cfa_create_location fname automaton Artificial in
      let l1' = cfa_create_location fname automaton Artificial in
      (* let l2 = cfa_create_location fname automaton Artificial in *)
      CFA.add_edge l0 l0' C_Command.cmd_true;
      CFA.add_edge l0 l0'' C_Command.cmd_true;
      CFA.add_edge l0' l1 (havoc_cmd (Expression.Symbol notTid) );
      CFA.add_edge l1 l1' notTid_assumption_cmd;
      (* CFA.add_edge l1' l2 (havoc_cmd self_lval) ; *)
      List.iter (und_one automaton l1' l0) (List.filter (fun lv -> List.mem lv !global_variables) vars);
      CFA.add_edge l1' l0 C_Command.skip ;
      l0''

    let undifferentiated_all automaton l0 =
      undifferentiated automaton (!global_variables) l0
  end

  let insert_havocs, insert_all_havocs =
    match Havoc.implementation with
    | Havoc.Undifferentiated ->
        Havoc.undifferentiated,
        Havoc.undifferentiated_all
    | Havoc.Locks_and_optimized_data ->
        Havoc.insert_havocs, Havoc.insert_all_havocs


  let stmt_id_attrib_table = Hashtbl.create 1009 (* random number here ... *)
  (* an attribute is converted to a list of expressions . *)
  
  let rec add_block_attributes fname attribs block =
    let battrs = List.map BlastCilInterface.convertCilAttrToExpList block.Cil.battrs in
    let new_attribs = battrs @ attribs in
      List.iter (add_stmt_attributes fname new_attribs) block.Cil.bstmts
  and 
    add_stmt_attributes fname attribs stmt = 
    let _ = List.iter (Misc.hashtbl_update stmt_id_attrib_table (fname, stmt.Cil.sid)) attribs in
      match stmt.Cil.skind with
	  Cil.Instr _ 
	| Cil.Return _ 
	| Cil.Goto _  
	| Cil.Break _ 
	| Cil.Continue _ -> ()
	| Cil.If (_,b1,b2,_) -> 
	    begin
	      add_block_attributes fname attribs b1;
	      add_block_attributes fname attribs b2
	    end
	| Cil.Switch (_,b,s_list,_) ->
	    begin
	      add_block_attributes fname attribs b;
	      List.iter (add_stmt_attributes fname attribs) s_list
	    end
	| Cil.Loop (b,_, _, _) -> add_block_attributes fname attribs b
	| Cil.Block b -> add_block_attributes fname attribs b
        | _ -> failwith "match failure: add_block_attributes"

  let loc_attrib_table = Hashtbl.create 1009
  
  let add_loc_attribs attribs loc = 
    List.iter (Misc.hashtbl_update loc_attrib_table 
		 (loc.CFA.automaton.CFA.aut_id, loc.CFA.loc_id)) 
      attribs

  let get_stmt_attribs (fname : string) stmt = 
    try Hashtbl.find stmt_id_attrib_table (fname, stmt.Cil.sid) 
    with Not_found -> []

  (* the main function that makes the CFA *)
  let get_loc_attribs loc_id = 
    try Hashtbl.find loc_attrib_table loc_id with Not_found -> []

  let print_attribute_table () =
    Message.msg_string Message.Debug "Attribute table";
    let print_elt (i1,i2) att_list = 
      let s = Printf.sprintf "Location: (%d,%d) : %s" i1 i2 
		(Misc.strList (List.map fst att_list)) in
	Message.msg_string Message.Normal s
    in
      Hashtbl.iter print_elt loc_attrib_table 

  let global_loc_table = Hashtbl.create 2000



(**************************************************************************)
(********************** Assumes via Attributes ****************************)
(**************************************************************************)

  let debug_print_attrs attrs = 
    let print_attr a =
      let (s,e_list) = BlastCilInterface.convertCilAttrToExpList a in
      let e_list_s = Misc.strList (List.map Expression.toString e_list) in
      Message.msg_string Message.Debug (Printf.sprintf "%s: %s" s e_list_s)
    in
    List.iter print_attr attrs

  let convert_havoc e = 
    match e with
    Expression.Lval lv -> lv
    | _ -> failwith ("Bad havoc attrib: "^(Expression.toString e))

  (* replace occurrences of bitwise operators with regular operators *)
  let rec convert_assume expr =
    let rec tp e =
    match e with
      Expression.FunctionCall(Expression.Lval(Expression.Symbol "AND"),e_list) ->
        Predicate.And (List.map tp e_list)
    | Expression.FunctionCall(Expression.Lval(Expression.Symbol "OR"),e_list) ->
        Predicate.Or (List.map tp e_list)
    | Expression.FunctionCall(Expression.Lval(Expression.Symbol "NOT"),[e']) ->
        Predicate.Not (tp e')
    | Expression.FunctionCall(Expression.Lval(Expression.Symbol "IFF"),[e1;e2]) -> Predicate.Iff (tp e1,tp e2)
    | _ -> Predicate.Atom e
    in
    let sub_array e =
      let te e = 
        match e with
        Expression.FunctionCall(Expression.Lval a,e_list) ->
          (try 
            let _ = get_type_of_lval a in (* TBD: match type with args *)
            Expression.Lval 
            (List.fold_left (fun lv e -> Expression.Indexed (Expression.Lval lv,e)) a e_list)
          with _ -> failwith ("Unknown Call in assume: "^(Expression.toString e)))  
        | _ -> e
      in 
      Expression.deep_transform_top te e
    in
    Predicate.atom_transform sub_array (tp expr)


            
  let is_quantified lv = 
    match lv with Expression.Symbol s -> Misc.is_prefix "__BLAST_q_" s
    | _ -> false
    
  let universally_quantify p =
    let qvars = List.filter  is_quantified (Predicate.lvals_of_predicate p) in
    let qsyms = 
      Misc.map_partial 
      (fun lv -> match lv with Expression.Symbol s -> Some s | _ -> None)
      qvars
    in
    List.fold_left (fun p x -> Predicate.All (x, p)) p qsyms
   
(******************************************************************************)
    
    
  let get_position_sources pos =
    (try
      Hashtbl.find global_loc_table pos
    with Not_found -> [])

  let set_location_attributes loc attr =
    CFA.set_location_attributes loc attr;
    (match attr with
      Artificial -> ()
    | Original pos ->
	let pos = (pos.file_name, pos.line) in
	Hashtbl.replace global_loc_table pos (loc :: get_position_sources pos))

  let cil_fun_block_to_cfa atomic fun_attribs cil_fun_block fun_name =

   (* first, add all the attributes to the attribute table ... *)
   let _ = add_block_attributes fun_name fun_attribs cil_fun_block in
   let label_table = Hashtbl.create 31 in
   let automaton = CFA.create_automaton { label_table = label_table ; } in
   let end_location = cfa_create_location fun_name automaton Artificial
   and entry_location = cfa_create_location fun_name automaton Artificial in
   let _ = List.iter (add_loc_attribs fun_attribs) [end_location;entry_location] in 
   let start_location =
     if (Options.getValueOfString "checkRace" = "tar") && atomic then begin
        Message.msg_string Message.Debug "Adding all havocs at the start of an atomic function";
        insert_all_havocs automaton entry_location
      end else entry_location
   and void_return_c_cmd =
      { C_Command.code = C_Command.Block [C_Command.Return
                                           (Expression.Constant (Constant.Int 0))];
        C_Command.read = [];
        C_Command.written = [] }
    in

    let get_real_labels cil_stmt =
      List.fold_left (function acc -> function
                          Cil.Label (label, _, b) ->
                            if b then label::acc else acc
                        | _                       ->
                            acc)
                     [] cil_stmt.Cil.labels
    (* This table maps CIL statements to their entry locations.  Several CIL
       statements might be mapped to the same location, see e.g. the
       processing of blocks and rerouting with last_skipped_cil_stmts *)
    and cil_stmt_to_loc_table = Hashtbl.create 31 in
    (* This recursive function is the heart of the automaton creation
       process: the translation from the CIL CFG to our CFA structure
       takes place here.  The meaning of the first 2 arguments is quite
       straightforward.  Let us detail the last one, last_skipped_cil_stmts:
       This list keeps track of the latest consecutive CIL statements we
       skipped during our translation, in order to avoid introducing skips,
       such as empty blocks or empty instruction lists.  This is necessary
       to avoid screwing up the translation of e.g. { label: } along with
       goto label: C constructs -- see rerouting *)
    let rec process_cil_stmt entry_loc cil_stmt last_skipped_cil_stmts =
      (* This entry_loc's attribute should be Artificial, the default
         attribute upon creation *)
      assert ((CFA.get_location_attributes entry_loc) = Artificial) ;
      (* let's first check whether we already processed this statement *)
      if (Hashtbl.mem cil_stmt_to_loc_table cil_stmt.Cil.sid)
      then
        begin
          (* entry_loc should not have any successor *)
          assert ((CFA.get_succs entry_loc) = []) ;
          (* entry_loc should have exactly one predecessor *)
          assert ((List.length (CFA.get_preds entry_loc)) = 1) ;
          let (inc_edge_cmd, inc_edge_loc) = List.hd (CFA.get_preds entry_loc) 
          and reroute_loc = Hashtbl.find cil_stmt_to_loc_table
                                         cil_stmt.Cil.sid
          in
            (* We redirect entry_loc's incoming edge to reroute_loc *)
            CFA.remove_edge inc_edge_loc entry_loc inc_edge_cmd ;
            CFA.add_edge inc_edge_loc reroute_loc inc_edge_cmd ;
            (* entry_loc should not have any predecessor now *)
            assert ((CFA.get_preds entry_loc) = []) ;
            (* We update cil_stmt_to_loc_table so that reroute_loc becomes
               the new entry location of all last skipped statements *)
            List.iter (fun s ->
                         Hashtbl.replace cil_stmt_to_loc_table
                                         s.Cil.sid reroute_loc)
                      last_skipped_cil_stmts ;
            (* We update label_table so that reroute_loc becomes the new
               location associated with all labels of all last skipped
               statements *)
            List.iter (fun s ->
                         List.iter (fun l ->
                                      Hashtbl.replace label_table l
                                                      reroute_loc)
                                   (get_real_labels s))
                      last_skipped_cil_stmts
            (* No recursive call, meaning that the CIL successors of this
               CIL statement should not be explored.
               Here the reason is: this statement, and its successors, have
               already been processed *)
;
        end
      else
        begin
          (* entry_loc becomes the entry location for this statement *)
          Hashtbl.add cil_stmt_to_loc_table cil_stmt.Cil.sid entry_loc ;
          (* update the label table *)
          List.iter (function label ->
                       assert (not (Hashtbl.mem label_table label)) ;
                       Hashtbl.add label_table label entry_loc)
                    (get_real_labels cil_stmt) ;
          match cil_stmt.Cil.skind with
              Cil.Instr (instr_l) ->
                begin
                  let process_cmd entry_loc (cmd, src_pos) =
                    (* A new location has by default an Artificial attribute.
                       If when processing the next statement this location
                       turns out to be the entry location for that statement,
                       then its attributes shall be changed --- see below *)
                    let new_loc = cfa_create_location fun_name automaton Artificial in
		    let stmt_attribs = get_stmt_attribs fun_name cil_stmt in
		    let _ = add_loc_attribs stmt_attribs new_loc in  
		    let _ = add_loc_attribs stmt_attribs entry_loc in
                    let start_loc =
                      if (Options.getValueOfString "checkRace" <> "tar") || atomic then entry_loc else
                      match cmd.C_Command.read, cmd.C_Command.written with
                      | _, [v] when is_a_lock v -> (* mutex action *)
                          Message.msg_string Message.Debug (Printf.sprintf "Adding havocs before action on lock %s at %s" (Expression.lvalToString v) (source_position_to_string src_pos));
                          insert_all_havocs automaton entry_loc
                      | [], [] -> (* local action *)
                          Message.msg_string Message.Debug (Printf.sprintf "Not adding havocs before local action at %s" (source_position_to_string src_pos));
                          entry_loc
                      | [], _ -> (* write action *)
			  Message.msg_string Message.Debug (Printf.sprintf "Not adding havocs before write action at %s" (source_position_to_string src_pos));
                          entry_loc
                      | vars, _ -> (* read action *)
                          Message.msg_string Message.Debug (Printf.sprintf "Adding havocs before read action at %s" (source_position_to_string src_pos));
                          insert_havocs automaton vars entry_loc
                    in
                    set_location_attributes start_loc
                      (Original src_pos) ;
                    
                    
                    (** Greg: We inssert a skip after each function call,
                       since the abstraction module assumes that the
                       target of each function call has only one incoming
                       edge *)
                    match cmd.C_Command.code with
                      C_Command.FunctionCall (Expression.FunctionCall(expr1, expList)) 
		    | C_Command.FunctionCall (Expression.Assignment(_, _, Expression.FunctionCall(expr1,expList))) ->
			if ((Expression.toString expr1) = "blast_assume") then 
			  match expList with 
			    eFirst::[] ->
			      let pred = Predicate.Atom(eFirst) in
			      let cmd_t = C_Command.gather_accesses (C_Command.Pred pred) in
			      begin
				print_string ("found cast \n");
				CFA.add_edge start_loc new_loc cmd_t;
			      end
			  | _ -> ();
			else
			  CFA.add_edge start_loc new_loc cmd ;
			
			let extra_loc = cfa_create_location fun_name automaton
                            Artificial
                        in
			let _ = add_loc_attribs stmt_attribs extra_loc in
                        CFA.add_edge new_loc extra_loc C_Command.skip;
			Message.msg_string Message.Debug ("********* function call: " ^ (Expression.toString expr1));
			
                        extra_loc
                    | _ ->
			CFA.add_edge start_loc new_loc cmd ;
                        new_loc
                  in
                  let new_loc = List.fold_left process_cmd entry_loc
                                               (split_cil_instr_list instr_l)
                  in
                    (* This CIL statement should have a unique successor *)
                    match cil_stmt.Cil.succs with

                        [next_cil_stmt] ->
                          (* Recursive call, meaning that the CIL successor
                             of this CIL statement should be explored, with
                             an entry location of new_loc *)
                          (* But we first have to check whether new_loc is
                             equal to entry_loc *)
                          if (new_loc == entry_loc)
                          then
                            begin
                              (* this means that instr_l was actually empty *)
                              (* [Greg] well, actually, since we just drop
                                 Cil.Asm instructions (for now), the previous
                                 claim is not true, so I comment the following
                                 assert *)
                              (* assert (instr_l = []) ; *)
                              skip_cil_stmt cil_stmt entry_loc next_cil_stmt
                                            last_skipped_cil_stmts None
                            end
                          else
                            process_cil_stmt new_loc next_cil_stmt []
                      | _ -> raise CilInstrWithIncorrectNumberOfSuccessors
                end

            | Cil.Return (cil_exp_opt, cil_loc) ->
                begin
                  let src_pos = cil_location_to_source_position cil_loc
                  and cmd =
                    match cil_exp_opt with
                        None ->
                          (* [Greg:] Hack to be cleaned...  This is because
                             our Return datatype in C_Command enforces to
                             return something *)
                          void_return_c_cmd
                      | Some (cil_exp) ->
                          let e = BlastCilInterface.convertCilExp cil_exp in
                          let code = C_Command.Block [C_Command.Return e] in
                          C_Command.gather_accesses code
                  in
                    (* all return edges point to the end location *)
                    set_location_attributes entry_loc (Original src_pos) ;
                    let start_loc =
		      if (Options.getValueOfString "checkRace" <> "tar") then entry_loc 
		      else
		      begin
		        (* Do add havocs at the end of atomic functions.
			   This takes care of local variables in atomic functions
			   that go out of scope, yet can be pointing to globals.
		         *)
                        (* A return instruction may not contain assignments or
                           mutex actions *)
                        assert (cmd.C_Command.written = []);
                        Message.msg_string Message.Debug (Printf.sprintf "Adding havocs before read action (return ...) at %s" (source_position_to_string src_pos));
                        insert_havocs automaton cmd.C_Command.read entry_loc
                      end
                    in
                      CFA.add_edge start_loc end_location cmd ;
                    (* This CIL statement should not have any successor *)
                    assert (cil_stmt.Cil.succs = []) ;
                    (* No recursive call, since this CIL statement does not
                       have any CIL successors *)
                end

            | Cil.Goto (next_cil_stmt_ref, cil_loc) ->
                begin
                  let next_cil_stmt = !next_cil_stmt_ref
                  in
                    (* This CIL statement should have a unique successor:
                       !next_cil_stmt_ref *)
                    (match cil_stmt.Cil.succs with
                         [s] -> assert (s == next_cil_stmt)
                       | _   -> raise CilGotoWithIncorrectNumberOfSuccessors) ;
                    (* Recursive call on the successor of this CIL statement *)
                    skip_cil_stmt cil_stmt entry_loc next_cil_stmt
                                  last_skipped_cil_stmts (Some cil_loc)
                end

            | Cil.Break _ -> raise CilBreakUnimplemented
            | Cil.Continue _ -> raise CilContinueUnimplemented

            | Cil.If (cil_exp, block_t, block_f, cil_loc) ->
                begin
                  let src_pos = cil_location_to_source_position cil_loc
                  and (cmd_t, cmd_t_flag, cmd_f, cmd_f_flag) =
                    let pred = BlastCilInterface.convertCilExpToPred cil_exp
                    in
                      (* [Greg] DIRTY... DIRTY... DIRTY...
                         But it's the way we model non-determinism... :-/ *)
                    match pred with
		      (Predicate.Atom (Expression.Binary (_, Expression.Lval (Expression.Symbol nondet), _)))
                      when (Misc.is_prefix "__BLAST_NONDET" nondet) ->
			(C_Command.cmd_true, true, C_Command.cmd_true, true)
		    | (Predicate.Atom (Expression.Binary (_, _, Expression.Lval (Expression.Symbol nondet))))
                      when (Misc.is_prefix "__BLAST_NONDET" nondet) ->
			(C_Command.cmd_true, true, C_Command.cmd_true, true)
                    | _ ->
                        let cmd_t = C_Command.gather_accesses (C_Command.Pred pred) in
                        let negate_pred = Predicate.negate pred in
                        let cmd_f = { cmd_t with C_Command.code = C_Command.Pred negate_pred } in 
                        let cmd_t_flag = try not (TheoremProver.queryExp (Predicate.Implies (pred, Predicate.False))) with _ -> true in
                        let cmd_f_flag = try not (TheoremProver.queryExp (Predicate.Implies (negate_pred, Predicate.False))) with _ -> true in
                        (cmd_t, cmd_t_flag, cmd_f, cmd_f_flag)
                  in
                
                  set_location_attributes entry_loc (Original src_pos) ;
                  match cil_stmt.Cil.succs with
                        [next_cil_stmt] ->
                          (* only one successor: this should happen when both
                             blocks are empty.  Let's check it *)
                          assert ((cil_block_is_empty block_t) &&
                                  (cil_block_is_empty block_f)) ;
                          let new_loc = cfa_create_location fun_name automaton Artificial
                          in
			  let _ = add_loc_attribs (get_stmt_attribs fun_name next_cil_stmt) new_loc in
                            let start_loc =
                              if (Options.getValueOfString "checkRace" <>  "tar") || atomic then entry_loc else begin
                                (* A predicate may not contain assignments or
                                   mutex actions (?? for the latter) *)
                                assert (cmd_t.C_Command.written = []);
                                Message.msg_string Message.Debug (Printf.sprintf "Adding havocs before read action (if ...) at %s" (source_position_to_string src_pos));
                                insert_havocs automaton cmd_t.C_Command.read entry_loc
                              end
                            in
                            if cmd_t_flag then CFA.add_edge start_loc new_loc cmd_t ;
                            if cmd_f_flag then CFA.add_edge start_loc new_loc cmd_f ;
                            (* Recursive call, meaning that the CIL successor
                               of this CIL statement should be explored, with
                               an entry location of new_loc *)
                            process_cil_stmt new_loc next_cil_stmt []
                      | [next_cil_stmt_f ; next_cil_stmt_t] ->
                          let new_loc_t = cfa_create_location fun_name automaton Artificial
                          and new_loc_f = cfa_create_location fun_name automaton Artificial
                          in
			  let _ = add_loc_attribs (get_stmt_attribs fun_name next_cil_stmt_t) new_loc_t 
			  in
			  let _ = add_loc_attribs (get_stmt_attribs fun_name next_cil_stmt_f) new_loc_f 
			  in
                            if cmd_t_flag then CFA.add_edge entry_loc new_loc_t cmd_t ;
                            if cmd_f_flag then CFA.add_edge entry_loc new_loc_f cmd_f ;
                            (* two successors:  Let's check that the CIL
                               successors are correctly set.  We only check
                               the sucessors corresponding to the non-empty
                               blocks *)
                            let check_successors () =
                              if (cil_block_is_empty block_f)
                              then
                                  let first_stmt_t = List.hd block_t.Cil.bstmts
                                  in
                                    (not (cil_block_is_empty block_t)) &&
                                    (first_stmt_t == next_cil_stmt_t)
                              else
                                if (cil_block_is_empty block_t)
                                then
                                    let first_stmt_f = List.hd block_f.Cil.bstmts
                                    in
                                      (first_stmt_f == next_cil_stmt_f)
                                else
                                    let first_stmt_t = List.hd block_t.Cil.bstmts
                                    and first_stmt_f = List.hd block_f.Cil.bstmts
                                    in
                                      (first_stmt_t == next_cil_stmt_t) &&
                                      (first_stmt_f == next_cil_stmt_f)
                            in
                              assert (check_successors ()) ;
                              (* Recursive calls, meaning that the 2 CIL successors
                                 of this CIL statement should be explored, with
                                 entry locations of new_loc_t and new_loc_f *)
                              if cmd_t_flag then process_cil_stmt new_loc_t next_cil_stmt_t [] ;
                              if cmd_f_flag then process_cil_stmt new_loc_f next_cil_stmt_f []
                      | _ -> raise CilIfWithIncorrectNumberOfSuccessors
                end

            | Cil.Switch _ -> raise CilSwitchUnimplemented

	    | Cil.Loop ({Cil.bstmts =
			 [{Cil.skind = Cil.If (cond,
					       {Cil.bstmts = [{Cil.skind = Cil.Break _}]},
					       {Cil.bstmts = []}, _)}]},
			cil_loc, _, _) ->
	      (* C: while (not_cond); *)
              (* Cil: while (1) {if (cond) {break;}} *)
	      (* We prefer to see this as: assume(cond); *)
              let src_pos = cil_location_to_source_position cil_loc in
              let pred = BlastCilInterface.convertCilExpToPred cond in
	      let cmd = C_Command.gather_accesses (C_Command.Pred pred) in
	      let new_loc = cfa_create_location fun_name automaton Artificial in
	      let _ = add_loc_attribs (get_stmt_attribs fun_name cil_stmt) new_loc in
      	      let start_loc =
		if (Options.getValueOfString "checkRace" <> "tar") || atomic then entry_loc else begin
                  (* A predicate may not contain assignments or mutex actions
		     (?? for the latter) *)
                  assert (cmd.C_Command.written = []);
                  Message.msg_string Message.Debug (Printf.sprintf "Adding havocs before read action (if ...) at %s" (source_position_to_string src_pos));
                  insert_havocs automaton cmd.C_Command.read entry_loc
		end
	      in
	      let issat = try not (TheoremProver.queryExp (Predicate.Implies(pred, Predicate.False))) with _ -> true in
              if issat then CFA.add_edge start_loc new_loc cmd;
              let next_cil_stmt =
		match cil_stmt.Cil.succs with
		| [s] -> s
		| _ -> raise CilAssumeWithIncorrectNumberOfSuccessors
	      in
	      process_cil_stmt new_loc next_cil_stmt []

              (* We process a CIL loop just as a block, since control flow
                 information from the CIL CFG takes care of the looping,
                 unless the loop's block is empty -- see below *)
            | Cil.Loop (block, _, _, _)
            | Cil.Block block ->
                begin
                let next_cil_stmt =
                    (match cil_stmt.Cil.succs with
                         [s] -> s
                       | _   -> raise CilBlockWithIncorrectNumberOfSuccessors)
                  in
                    if (cil_block_is_empty block)
                    then
                      begin
                        (* here we have to distinguish between loops and simple blocks *)
                        match cil_stmt.Cil.skind with
                            Cil.Loop (block, cil_loc, _, _) ->
                              let src_pos = cil_location_to_source_position cil_loc
                              and cmd = C_Command.cmd_true
                              in
                                set_location_attributes entry_loc (Original src_pos) ;
                                CFA.add_edge entry_loc entry_loc cmd
                                (* No recursive call, since this CIL statement
                                   does not have any CIL successors *)
                          | Cil.Block b ->
                             begin
                               let add_single_edge cmd = 
                                 let new_loc = cfa_create_location fun_name automaton Artificial in
                                 add_loc_attribs (get_stmt_attribs fun_name cil_stmt) new_loc;
                                 CFA.add_edge entry_loc new_loc cmd;
                                 new_loc
                               in
                               let _ = debug_print_attrs b.Cil.battrs in
                               match b.Cil.battrs with
				[attr] ->
                                  Message.msg_string Message.Debug "[att] blockatt"; 
				  let (aname,attrexp_list) = BlastCilInterface.convertCilAttrToExpList attr in
				  if ( List.length attrexp_list = 1 
                                       && (aname = "assume" || aname = "havoc")) 
                                  then 
                                    begin
                                    let cmd = 
                                      if (aname="assume") then 
                                        let pred = universally_quantify 
                                        (convert_assume (List.hd attrexp_list)) in
                                        C_Command.gather_accesses (C_Command.Pred pred)
                                      else (* aname = "havoc" *)
                                        let lv = convert_havoc (List.hd attrexp_list) in 
                                        C_Command.gather_accesses (C_Command.Havoc lv)
                                    in
                                    let new_loc = add_single_edge cmd in
                                    let next_cil_stmt =
                                      match cil_stmt.Cil.succs with
                                      | [s] -> s
                                      | _ -> raise CilAssumeWithIncorrectNumberOfSuccessors
                                    in
                                    process_cil_stmt new_loc next_cil_stmt [] 
                                    end
                                  else
				    skip_cil_stmt cil_stmt entry_loc next_cil_stmt
                                    last_skipped_cil_stmts None
				| _  -> 
				    (* Recursive call on the successor of this CIL
                                       statement *)
				    skip_cil_stmt cil_stmt entry_loc next_cil_stmt
                                    last_skipped_cil_stmts None
                             end
                          | _ -> assert false (* can't happen for empty blocks *)
                      end
                    else
                      begin
                        (* The successor of this CIL statement should be the
                           first statement of the block *)
                        assert (next_cil_stmt == (List.hd block.Cil.bstmts)) ;
                        (* Recursive call on the successor of this CIL statement *)
                        skip_cil_stmt cil_stmt entry_loc next_cil_stmt
                                      last_skipped_cil_stmts None
                      end
                end
            | _ -> failwith "match failure : process_cil_stmt"
        end
    (* This function tells whether we can safely skip a cil_stmt.  It is
       used during the translation on statements for which skipping
       preserves the semantics of the original CIL file.  This function
       just checks for other attributes (e.g. labels) *)
    and skipping_ok cil_stmt =
      (* It would be nicer to check only whether this statement has some
         original label atached to it (not some CIL generated label), but
         CIL does not allow that *)
      (get_real_labels cil_stmt) = []
    (* This function takes care of extra checks when processing a CIL
       statement that could be safely (w.r.t to the semantics of the original
       CIL file) skipped *)
    and skip_cil_stmt cil_stmt entry_loc next_cil_stmt last_skipped_cil_stmts
                      cil_loc_option =
      if (skipping_ok cil_stmt)
      then
        process_cil_stmt entry_loc next_cil_stmt
                         (cil_stmt::last_skipped_cil_stmts)
      else
        (* We can not skip it.  So we create a new location and introduce a
           skip.  Let's try to get the source location for entry_loc. *)
        let get_label_loc = function
            Cil.Label (_, cil_loc, _)
          | Cil.Case (_, cil_loc)
          | Cil.Default (cil_loc) ->
              cil_loc in
        let get_best_cil_loc cil_loc label =
          let label_loc = get_label_loc label
          in
            { Cil.file =
                if (cil_loc.Cil.file = "")
                then label_loc.Cil.file
                else
                  begin
                    assert (label_loc.Cil.file = (max "" cil_loc.Cil.file)) ;
                    cil_loc.Cil.file
                  end ;
              Cil.line =
                if (label_loc.Cil.file = "")
                then cil_loc.Cil.line
                else max (cil_loc.Cil.line) (label_loc.Cil.line) ;
              Cil.byte = 
                if (label_loc.Cil.file = "")
                then cil_loc.Cil.byte
                else max (cil_loc.Cil.byte) (label_loc.Cil.byte) ;
            } 
	in
        let cil_loc =
          match cil_loc_option with
              None ->
                List.fold_left get_best_cil_loc
                               { Cil.file = "" ;
                                 Cil.line = -1 ; Cil.byte = -1; 
                               }
                               cil_stmt.Cil.labels
            | Some c -> c 
	in
        let src_pos = cil_location_to_source_position cil_loc
        and new_loc = cfa_create_location fun_name automaton Artificial
 in 
let _ = add_loc_attribs (get_stmt_attribs fun_name cil_stmt) new_loc in
          (* We insert a skip *)
          set_location_attributes entry_loc (Original src_pos) ;
          CFA.add_edge entry_loc new_loc C_Command.skip ;
          process_cil_stmt new_loc next_cil_stmt []
    (* this function returns true if cil_block is either an empty list of
       statements or a list of empty blocks statements *)
    and cil_block_is_empty cil_block =
      List.fold_left (fun curr stmt -> if (curr)
                                       then (cil_stmt_is_empty_block stmt)
                                       else false)
                     true cil_block.Cil.bstmts
    and cil_stmt_is_empty_block stmt =
      match stmt.Cil.skind with
          Cil.Block block -> cil_block_is_empty block
        | _               -> false
    in
    CFA.set_start_location automaton entry_location ;
    CFA.set_end_location automaton end_location ;
    process_cil_stmt start_location (List.hd cil_fun_block.Cil.bstmts ) [] ;
    automaton


  exception CilFunDecTypeMismatch


  
  let cil_fundec_to_fundef (cil_fundec, cil_location) =
    let src_pos = cil_location_to_source_position cil_location
    and formals_as_symbols = List.map BlastCilInterface.cil_varinfo_to_symbol
                                      cil_fundec.Cil.sformals in
    let locals = Misc.union (List.map BlastCilInterface.cil_varinfo_to_symbol cil_fundec.Cil.slocals)
                            formals_as_symbols
    in
    let formals =
      if (match cil_fundec.Cil.svar.Cil.vtype with
            | Cil.TFun (_, _, b, _) -> b
            | _                      -> raise CilFunDecTypeMismatch)
      then Variable formals_as_symbols
      else Fixed formals_as_symbols
    in
    let atomic = Cil.hasAttribute "atomic" cil_fundec.Cil.svar.Cil.vattr in
    let fun_attribs = List.map BlastCilInterface.convertCilAttrToExpList cil_fundec.Cil.svar.Cil.vattr
    in
    let res =
      { function_name    = cil_fundec.Cil.svar.Cil.vname ;
        locals           = locals ;
        formals          = formals ;
        cfa              = cil_fun_block_to_cfa atomic fun_attribs cil_fundec.Cil.sbody cil_fundec.Cil.svar.Cil.vname;
        fun_src_pos      = src_pos ; 
	fun_attributes   = cil_fundec.Cil.svar.Cil.vattr ; }
    in
    res



  exception DuplicateFunctionName of string

	(* SKY: here file is processed and variables and functions are found *)
  let process_cil_file cil_file =
    let process_cil_global_declarations cil_global =
      match cil_global with
      | Cil.GVar (cil_varinfo, _, cil_location) -> 
	  let name = cil_varinfo.Cil.vname in
	  add_global (Expression.Symbol name) (Varinfo cil_varinfo)
      | _ -> ()
    in
    let process_cil_global_functions cil_global =
      match cil_global with
          Cil.GFun (cil_fundec, cil_location) -> begin
            let fn_name = cil_fundec.Cil.svar.Cil.vname in
            (* First check that a function with the same name has not
               already been processed *)
	    let function_seen = Hashtbl.mem global_function_table fn_name in
	    (* Check if function is static inline, so it can be duplicated and should be scipped if it is *)
	    let check_attr name = Cil.hasAttribute name cil_fundec.Cil.svar.Cil.vattr in
	    (*let function_inline = (check_attr "inline") in*)
	    let function_inline = cil_fundec.Cil.svar.Cil.vinline in
	    let _ = Message.msg_string Message.Normal ("function: "^fn_name) in
	    let _ = List.iter (fun (Cil.Attr(an, _)) -> Message.msg_string Message.Normal ("attr: "^an)) cil_fundec.Cil.svar.Cil.vattr in
	    (*let function_inline = (check_attr "static") && (check_attr "inline") in*)
	    match (function_seen, function_inline) with
               (true, false) -> if (Options.getValueOfBool "ignoredupfn")
	         then Message.msg_string Message.Error ("Duplicate function skipped due to -ignoredupfn ("^fn_name^")")
		 else raise (DuplicateFunctionName fn_name)
	      |(true, true) -> Message.msg_string Message.Normal "skipped";
	      |(false, _) -> begin
		Message.msg_string Message.Debug ("Encountered function " ^ fn_name);
		let fundef = cil_fundec_to_fundef (cil_fundec, cil_location)
		in
		Message.msg_string Message.Debug ("Finished converting function " ^ fn_name);
		  Hashtbl.add global_function_table fn_name fundef ;
		  Hashtbl.iter (fun label loc ->
				  Hashtbl.add global_label_table label loc)
			       (CFA.get_automaton_attributes fundef.cfa).label_table;
	       end
	   end
      |	_ -> ()

    in
    let is_fun_name lv =
      match lv with
	Expression.Symbol s ->
	  let rec _find_fname lst =
	    match lst with 
	      [] -> false
	    | a :: rest ->
		begin
		  match a with
		    Cil.GFun (cil_fundec, _) -> 
		      if s = cil_fundec.Cil.svar.Cil.vname then true else
		      _find_fname rest
		  | Cil.GVarDecl (vinfo, _) ->
		      begin
			match vinfo.Cil.vtype with
			  Cil.TFun _ ->
			    if s = vinfo.Cil.vname then true else _find_fname rest
			| _ -> _find_fname rest
		      end	
		  | _ -> _find_fname rest
		end
	  in
	  _find_fname cil_file.Cil.globals
      |	_ -> false
    in 
     (* RJ : adding global info for cf reachability as well *)
    (*if (Options.getValueOfString "checkRace" = "tar") then *)
      add_all_lvals cil_file ;

    (* start comment: This is no longer needed: since we keep track of all lvals. 
       I will expunge this after I add the field/varinfo to the lval table. *)
      (* we need info about global vars for cf reachability as well *)
      List.iter process_cil_global_declarations cil_file.Cil.globals ;
    (* end comment *)
	
	(** At this point, the global_variable_table has been
	populated. So I can iterate over its entries and get all the lvalues
	that need to be havoced.
      *)

    if (Options.getValueOfString "checkRace" = "tar") then
	(* add variables to havoc only if we are running in TAR mode for races *)
      iterate_all_lvals 
	(fun lv -> 
	  if is_fun_name lv then () else 
	  Havoc.global_variables := lv :: !Havoc.global_variables) 
    else () ;
    
    List.iter process_cil_global_functions cil_file.Cil.globals
  
  let get_source (s, c, t) = s
  let get_target (s, c, t) = t
  let get_command (s, c, t) = c


  module OrderedInt = struct type t = int let compare a b = b - a end
  module IntSet = Set.Make(OrderedInt)

  let output_cfa_dot ch fundef =
    let known_locs = ref IntSet.empty in
    let rec output_loc loc =
       Printf.fprintf ch "  %d;\n" loc.CFA.loc_id;
      known_locs := IntSet.add loc.CFA.loc_id !known_locs;
       List.iter
        (fun (cmd, l) ->
            Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"
              loc.CFA.loc_id l.CFA.loc_id
              (String.escaped (C_Command.to_string cmd));
          if not (IntSet.mem l.CFA.loc_id !known_locs) then output_loc l)
        loc.CFA.succs
    in
    match fundef.cfa.CFA.start_location with
    | None -> ()
    | Some loc ->
	let fname_string =
	  if  Misc.is_prefix "__BLAST_initialize" fundef.function_name
	  then "INIT" else fundef.function_name
	in
	  Printf.fprintf ch "digraph %s {\n" fname_string;
	  output_loc loc;
          Printf.fprintf ch "}\n\n";
          ()



let print_dot loc_to_no op_list =
	let print_err_dot ch loc_to_no op_list =
			let known_locs = ref IntSet.empty in
			let known_edges = ref [] in
			let print_one_edge op = 
			(*    let ch = open_out fname in *)
		    	let loc1 = get_source op in
			let loc2 = get_target op in
		   	let cmd = get_command op in
    			let rec output_loc loc =
       				if (not (IntSet.mem loc.CFA.loc_id !known_locs)) then 
				       begin
				       if (Hashtbl.mem loc_to_no loc) then
				       begin 
				       Printf.fprintf ch "  %d [label = \"%d    %d\"]\n" loc.CFA.loc_id loc.CFA.loc_id (Hashtbl.find loc_to_no loc) ; 
				       known_locs := IntSet.add loc.CFA.loc_id !known_locs ;
				       end
				       else
				       begin
				       Printf.fprintf ch "  %d [label = \"%d    %d\"]\n" loc.CFA.loc_id loc.CFA.loc_id 0 ; (*(Hashtbl.find loc_to_no loc);*)
				       known_locs := IntSet.add loc.CFA.loc_id !known_locs ;
				       end
				       end
			       else ();
    			in
			output_loc loc1 ;
			output_loc loc2 ;
			if (not (List.mem op !known_edges)) then
			begin
			Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"  loc1.CFA.loc_id  loc2.CFA.loc_id  (String.escaped (C_Command.to_string cmd));
			known_edges := (op :: !known_edges) ;
			end
			else
			begin
			();
			end
		    in
		    Printf.fprintf ch "digraph {\n" ;
		    List.iter print_one_edge op_list;
		    Printf.fprintf ch "}\n\n";
	in
	let fname = Options.getValueOfString "trace-dot" in
	if (fname <> "")
	then
	begin
	let temp = open_out_gen [Open_creat; Open_append; Open_text ] 0o644 fname in
	print_err_dot temp loc_to_no op_list;
	close_out temp ;
	let _ = ignore (Sys.command ("dot -Tps -o " ^ fname ^ ".ps " ^ fname)) in
	()
	end
	else
	()

(* This function prints the complete CFA with each node marked
   with its number of occurrences. :Rajhans*)
(*let mod_output_cfa_dot ch loc_to_no fundef =
    let known_locs = ref IntSet.empty in
    let rec output_loc loc =
       Printf.fprintf ch "  %d [label = \"%d    %d\"]\n" loc.CFA.loc_id loc.CFA.loc_id (Hashtbl.find loc_to_no loc);
      known_locs := IntSet.add loc.CFA.loc_id !known_locs;
       List.iter
        (fun (cmd, l) ->
            Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"
              loc.CFA.loc_id l.CFA.loc_id
              (String.escaped (C_Command.to_string cmd));
          if not (IntSet.mem l.CFA.loc_id !known_locs) then output_loc l)
        loc.CFA.succs
    in
    match fundef.cfa.CFA.start_location with
    | None -> ()
    | Some loc ->
	let fname_string =
	  if  Misc.is_prefix "__BLAST_initialize" fundef.function_name
	  then "INIT" else fundef.function_name
	in
	  Printf.fprintf ch "digraph %s {\n" fname_string;
	  output_loc loc;
          Printf.fprintf ch "}\n\n";
          ()       

let print_dot loc_to_no = 
    print_string "here 1" ;
    let temp = open_out "temp.dot" in
   	List.iter (mod_output_cfa_dot temp loc_to_no)
	  (List.sort (fun fundef1 fundef2 ->
            compare fundef1.function_name fundef2.function_name)
	     (Hashtbl.fold (fun _ fundef l -> fundef :: l)
		global_function_table []));
	close_out temp;
    let _ = ignore (Sys.command ("dot -Tps -o temp.ps temp.dot")) in
   print_string "here 2" 
*)





(* Stuff required by the SYSTEM_DESCRIPTION signature *)

  module Command = C_Command


  type location = (automaton_attributes,
                   location_attributes,
                   C_Command.t) CFA.location

  let fake_location = (* generic location that matches all locations *)
    { CFA.loc_id = -1;
      CFA.succs = [];
      CFA.preds = [];
      CFA.automaton = { CFA.aut_id = -1;
                        CFA.start_location = None;
                        CFA.end_location = None;
                        CFA.aut_attributes = {label_table = Hashtbl.create 1}};
      CFA.loc_attributes = Artificial}

  let fake_edge =
    (fake_location, { Command.code = Command.Skip ; Command.read = [] ; Command.written = [] } , fake_location)
   
      
  let get_source_position loc =
    match (CFA.get_location_attributes loc) with
      Original sp ->  Some (sp.file_name, sp.line, sp.column)
    | Artificial -> None


  let print_location fmt loc =
    Format.fprintf fmt "@[Location: id=%d#%d " loc.CFA.automaton.CFA.aut_id
      loc.CFA.loc_id ;
    match (CFA.get_location_attributes loc) with
	Original (sp) ->
	  print_source_position fmt sp;
	  Format.fprintf fmt "@]"
      | Artificial ->
	  Format.fprintf fmt "(Artificial)@]"


  type edge = location * Command.t * location

  let edge_to_command (_, c, _) = c

  let print_edge fmt (s, c, t) =
    Format.fprintf fmt "@[" ;
    print_location fmt s ;
    Format.fprintf fmt "---" ;
    Command.print fmt c ;
    Format.fprintf fmt "--->" ;
    print_location fmt t ;
    Format.fprintf fmt "@]"

  let print_code fmt c = Command.print fmt c

  let location_to_string = Misc.to_string_from_printer print_location

  let location_coords loc = (loc.CFA.automaton.CFA.aut_id , loc.CFA.loc_id)

  let edge_to_string = Misc.to_string_from_printer print_edge


  (** Create a unique name for the edge, using the location coordinates of the
      source and target.  *)
  let get_edge_name edge =
    let (start_fn, start_node) = location_coords (get_source edge)
    and (targ_fn, targ_node) = location_coords (get_target edge)
    in
      (string_of_int start_fn) ^ "," ^ (string_of_int start_node) ^ "->" ^
      (string_of_int targ_fn) ^ "," ^ (string_of_int targ_node)

  (* a non-string version of the above *)
  let get_edge_coords edge = 
    let (s,_,t) = edge in
    let rv = (location_coords s, location_coords t)
    in
        rv

  let make_edge_coords l1 l2 = 
    (location_coords l1, location_coords l2)

        
  let make_edge (src : location) (target : location) newcode =
    ((src, newcode, target) : edge ) 

  let get_outgoing_edges loc =
    let succs = CFA.get_succs loc in
      List.map (fun (cmd, succ) -> (loc, cmd, succ)) succs

  let get_ingoing_edges loc = 
    let preds = CFA.get_preds loc in
      List.map (fun (cmd, pred) -> (pred, cmd, loc)) preds


  let reads_and_writes_at_edge e =
    let cmd = get_command e in
    cmd.C_Command.read, cmd.C_Command.written


  exception NoSuchFunctionException of string
  
  let is_defined fn_name =
    Hashtbl.mem global_function_table fn_name

 let lookup_entry_location fn_name =
    try
      CFA.get_start_location (Hashtbl.find global_function_table fn_name).cfa
    with Not_found ->
      raise (NoSuchFunctionException fn_name)
 
 let lookup_exit_location fname = 
   try
     CFA.get_end_location (Hashtbl.find global_function_table fname).cfa
   with Not_found ->
     raise (NoSuchFunctionException fname)
  
   let get_location_fname loc = 
     CFA.location_function_name loc

let is_call_on_edge e = 
    match (get_command e).C_Command.code with
    Command.FunctionCall _ -> true
    | Command.GuardedFunctionCall _ -> true
    | _ -> false


let get_location_entry_location loc =
   lookup_entry_location (get_location_fname loc)

let get_location_exit_location loc = failwith "TBD: get location exit location " 

let get_fname_id fname = 
   fst (location_coords (lookup_entry_location fname))

let list_of_functions () = Misc.run_on_table (fun fname _ -> fname) global_function_table

let is_join loc =
  let in_size = (List.length (CFA.get_preds loc)) in 
  in_size > 1
  || (in_size = 1 && (location_coords loc = location_coords (get_location_entry_location loc)))
    
let lookup_formals fn_name =
    try (Hashtbl.find global_function_table fn_name).formals
    with Not_found -> raise (NoSuchFunctionException fn_name)

let lookup_locals fn_name =
    try
      (Hashtbl.find global_function_table fn_name).locals
    with Not_found ->
      raise (NoSuchFunctionException fn_name)
     

let get_fname fname = (* TBD:FUNCTIONPTR *)
   Message.msg_string Message.Debug ("get_fname: "^(Expression.toString fname));
   match fname with
      Expression.Lval (Expression.Symbol name) -> name
    | _  -> "__BLAST_fnptr"

let get_name_of_call call_exp = (* TBD:FUNCTIONPTR *)
   Message.msg_string Message.Debug ("get_name_of_call: "^(Expression.toString call_exp));
   match call_exp with
    | Expression.FunctionCall(l, _)
    | Expression.Assignment(_, _, Expression.FunctionCall(l, _)) ->
      get_fname l
    | _ -> raise (NoSuchFunctionException "WTF")

  (** Return target used by deconstructFunCall when function is
      void *)
let junkRet = Expression.Dereference (Expression.Sizeof 0)

let __SKIPFunctionName = "__BLAST_IRRELEVANT_SKIP_FUNCTION_" 
let __NotImplementedFunctionName = "NOT_IMPLEMENTED_FUNCTION"
let __BLAST_DummyFunctionName = "__BLAST_DUMMY_FUNCTION"
let __BLAST_DispatchFunctionName = "__BLAST_DISPATCH_FUNCTION"
let __BLAST_AsyncFunctionName = "__BLAST_ASYNC_"
let __BLAST_ExecAsyncFunction = "__BLAST_FIRE_EVENT"
let __BLAST_EventHandlerAttrib = "eventhandler"

(**
   Parse a function expression, returning a triple of
      (function name, (formal name, formal value expr) list, result target lval).

   This function has a peculiar interface in case a function is not implemented.
   Suppose foo is not defined, that is C_SYSTEM_DESCR.is_defined says false.
   1. If the function call is
	foo(...)
      with no return target, then deconstructFunCall returns (__NotImplementedFunctionName, ...)
      as the name of the function.
      Subsequently, this can be treated as a skip.
   2. If the function call is
        x = foo(...)
      then deconstructFunCall returns (foo, ...)
 
   This is just NOT nice, but I am afraid to change it because it can break other code.
   What we should really use to distinguish the two cases is the return target, which
   is junkRet in the first case, and a valid target in the second.


   Note: deconstructFunCall expects that function pointer disambiguation has been done before.
   Hence if it sees a function pointer call, it returns a __NotImplementedFunctionName for the
   function name under -nofp and raises a FunctionPointerException under -fp 
 *)
exception FunctionPointerException 

let isEventHandler_fname fn = 
    (* Message.msg_string Message.Debug ("isEventHandler_fname : "^fn);*)
    let rv = 
    try 
      let attrs = (Hashtbl.find global_function_table fn).fun_attributes in
      Cil.hasAttribute __BLAST_EventHandlerAttrib attrs 
    with Not_found -> false  in
    (* Message.msg_string Message.Debug (Printf.sprintf "isEventHandler_fname : %s : %b " fn rv);*)
    rv


let deconstructFunCall (exp:Ast.Expression.expression)
      : (Ast.Symbol.symbol * (string * Ast.Expression.expression) list * Ast.Expression.lval) =
  let arg_issue fname = 
    if Options.getValueOfBool "events" && isEventHandler_fname fname then [] 
    else failwith ("deconstructFunCall: FunCall with too many/few args "^(Expression.toString exp)) in
  let retval = 
    match exp with 
    Expression.FunctionCall(Expression.Lval (Expression.Symbol(fname)), elist) -> 
      begin
	try
	  let formalList = lookup_formals fname in
      (* check for vararg: in case there is a Variable formal argument, take only the first n+1 arguments,
         and print a warning message. vararg functions are usually things like printk which we ignore
         anyway, so correctness should not be affected.
	 *)
	  match formalList with
	    Variable formals ->
	      begin
		Message.msg_string Message.Minor 
                ("WARNING: Function "^fname^" has variable arguments. May discard arguments in this call.");
		let _len = List.length formals and _lenactuals = List.length elist in
		if (_len == _lenactuals) then (* this is the easy else case *)
		  let nameAssocList = try List.combine formals elist with _ -> arg_issue fname in
		  (fname,nameAssocList, junkRet)
		else 
		    if (_len < _lenactuals) then
		      let nameAssocList = 
                        try (List.combine formals (Misc.truncateList elist _len)) 
                        with _ -> arg_issue fname in
		      (fname,nameAssocList, junkRet)
		    else
		      let nameAssocList = 
                        try (List.combine (Misc.truncateList formals _lenactuals) elist)
                        with _ -> arg_issue fname in
		      (fname,nameAssocList, junkRet)
	      end
	  |	Fixed formals ->
	      let nameAssocList = 
                try (List.combine formals elist) with _ -> arg_issue fname in
	      (fname,nameAssocList, junkRet)
	with NoSuchFunctionException f -> (__NotImplementedFunctionName,[],junkRet)
      end
  | Expression.Assignment (Expression.Assign,target, 
			   Expression.FunctionCall(Expression.Lval (Expression.Symbol(fname)), elist)) ->
     begin   
      try
      let formalList = lookup_formals fname in
      (* let actuals = List.map _get_param elist in *)
      (* check for vararg: in case there is a $vararg argument, take only the first n+1 arguments,
         and print a warning message. vararg functions are usually things like printk which we ignore
         anyway, so correctness should not be affected.
      *)
      match formalList with
	Variable formals ->
	  begin
	    Message.msg_string Message.Minor ("WARNING: Function "^fname^" has variable arguments. May discard arguments in this call.");
	    let _len = List.length formals and _lenactuals = List.length elist in
	    if (_len == _lenactuals) then (* this is the easy else case *)
              let nameAssocList = try (List.combine formals elist) with _ -> arg_issue fname in
              (fname,nameAssocList, target)
            else (* more difficult : no arg, or more *)
		if (_len < _lenactuals) then
		  let nameAssocList = 
                    try (List.combine formals (Misc.truncateList elist _len)) with _ -> arg_issue fname in
		  (fname,nameAssocList, target)
		else
		  let nameAssocList = 
                    try (List.combine (Misc.truncateList formals _lenactuals) elist) with _ -> arg_issue fname in
		  (fname,nameAssocList, target)
	  end
      |	Fixed formals ->
	  let nameAssocList = try (List.combine formals elist) with _ -> arg_issue fname in
	  (fname,nameAssocList,target)
      with NoSuchFunctionException f -> (f,[],target)
     end
  | _ -> 
      if(Options.getValueOfBool "nofp") then
        begin
	  Message.msg_string Message.Debug ("Bad Call to deconstruct function call ! "^(Expression.toString exp));
	    (__NotImplementedFunctionName, [], junkRet)
	end
      else 
	begin
	  (match exp with
	    Expression.FunctionCall(Expression.Lval l, _)
	  | Expression.Assignment(_, _, Expression.FunctionCall(Expression.Lval l, _)) ->
	      (* List.iter (fun s -> Message.msg_string Message.Normal ("Aliased fn: " ^ (Expression.lvalToString s))) 
		(AliasAnalyzer.getFunctionsPointedTo (Expression.Lval l) ) *)
	      failwith ("barf! this should be handled in disambiguate_function_pointers in abs-include")
	  | _ -> failwith ("Deconstruct fn call: strange function pointer " ^ (Expression.toString exp))) ;
          (* failwith ("Bad Call to deconstruct function call ! "^(Expression.toString exp)) *)
          raise FunctionPointerException
	end
  in
  retval

      
  let call_on_edge e =
    match (get_command e).Command.code with
        Command.FunctionCall (e') ->
          begin
		match e' with
		    (Expression.FunctionCall (fname, _))   
		  | (Expression.Assignment (_, _, Expression.FunctionCall (fname, _))) ->
		      Some (get_fname fname,e')
		  | _ -> 
		      Message.msg_string Message.Major
		      ("Strange fcallexp "^(Expression.toString e'));
		      None
	      end
	| Command.GuardedFunctionCall _ -> failwith "Guarded function call not handled"
	| _ -> None


  let get_locations_at_label label =
    Hashtbl.find_all global_label_table label

    (* TBD: URGENT -- lookup location from loc_coords!!! *)
  (*let lookup_location_from_loc_coords (loc1, loc2) = 
     CFA.location_coords_to_location (loc1,loc2)
    
    let entry_loc_for_fn = try (* this is inefficient *)
      lookup_entry_location 
	(List.find (fun a ->  let (i,j) = location_coords (lookup_entry_location a) in  i = loc1) 
	(list_of_functions ()) )
    with Not_found -> failwith "lookup_location_from_locid: No function found with correct id!"
    in
    (* now I have the right function, look for the location with the right 2nd id *)
    let module LocationSet =
      Set.Make (struct
        type t = int 
        let compare x y =  compare x  y
      end )
	in
	let already_visited_source_locations = ref LocationSet.empty in
	let rec find_loc loc_list =
	  match loc_list with
	    [] -> failwith ("lookup_location_from_locid: No location found with this id "^(string_of_int loc1)^"," ^(string_of_int loc2))
	  | loc::t -> 
	      begin
		let (_,loc_id) = location_coords loc in
		if (LocationSet.mem loc_id !already_visited_source_locations) 
		then 
		  (find_loc t)
                else
		  begin
		    if (loc_id = loc2) then loc
                    else
		      begin
			(already_visited_source_locations := LocationSet.add loc_id !already_visited_source_locations) ;
			let children_h = List.map get_target (get_outgoing_edges loc) in
			(find_loc (children_h @ t))
		      end
		  end
	      end
	in
	find_loc [entry_loc_for_fn]*)

 
    
  let is_atomic loc =
    (* right now, a context switch is possible iff the current function is not atomic. *)
    try
      let fname = CFA.location_function_name loc in
      let loc_attribs  = List.map fst (get_loc_attribs (location_coords loc)) in
(* RJ: I dont remember which version is the correct version!! 
   Version 1.
	(List.mem "atomic" loc_attribs) ||
	(List.mem "signal" loc_attribs) *)
	(* JHALA: handle with care! may be unsound if the function enables interrupts! *)
(* version 2. *)
	List.mem "atomic" loc_attribs
	  || (Cil.hasAttribute "atomic" (Hashtbl.find global_function_table fname).fun_attributes)

    with Not_found -> false

 let is_task loc =
   try
     let fname = CFA.location_function_name loc in
     let loc_attribs  = List.map fst (get_loc_attribs (location_coords loc)) in
       List.mem "task" loc_attribs
	  (*
	    (Cil.hasAttribute "atomic" (Hashtbl.find global_function_table fname).fun_attributes) *)
    with Not_found -> false

 let is_event loc =
   (* right now, a context switch is possible iff the current function is not atomic. *)
   try
      let fname = CFA.location_function_name loc in
      let loc_attribs  = List.map fst (get_loc_attribs (location_coords loc)) in
	List.mem "event" loc_attribs
	  (*
	    (Cil.hasAttribute "atomic" (Hashtbl.find global_function_table fname).fun_attributes) *)
    with Not_found -> false


 let is_spec loc = 
   (* was this location -- hence instruction a monitor instruction *)
   try 
     let fname = CFA.location_function_name loc in
     let loc_attribs  = List.map fst (get_loc_attribs (location_coords loc)) in
       List.mem "spec" loc_attribs
	  (* (Cil.hasAttribute "atomic" (Hashtbl.find global_function_table fname).fun_attributes) *)
   with Not_found -> false

 
 let map_nodes_fname f fname  = 
    let module LocationSet =
      Set.Make (struct
                  type t = int 
                  let compare x y =  compare x  y
		end) 
    in
    let already_visited_source_locations = ref LocationSet.empty in
    let rec map_loc_and_edges loc_list =
      match loc_list with
	  [] -> []
	| loc::t -> 
	    begin
	      let (_,loc_id) = location_coords loc in
		if (LocationSet.mem loc_id !already_visited_source_locations) 
		then 
		  (map_loc_and_edges t)
                else
		  begin
		    let _ = 
		      (already_visited_source_locations := LocationSet.add loc_id !already_visited_source_locations) 
		    in
		    let children_h = List.map get_target (get_outgoing_edges loc) in
		      (f loc)::(map_loc_and_edges (children_h @ t))
		  end
	    end
    in
      map_loc_and_edges [(lookup_entry_location fname)]
    
  let map_edges_fname f fname  = 
    let node_fn n = List.map f (get_outgoing_edges n) in
      List.flatten (map_nodes_fname node_fn fname)
	
  let map_edges f =
    List.flatten (List.map (map_edges_fname f) (list_of_functions ()))

  let map_nodes f = 
    List.flatten (List.map (map_nodes_fname f) (list_of_functions ()))


  let lookup_all_locations fname = map_nodes_fname (fun x -> x) fname
  
  (* various utilities used in abstraction *)
 
   let isReturn op = 
    let cmd = get_command op in
    match cmd.Command.code with
      Command.Block l -> (List.exists (function x -> match x with Command.Return e -> true | _ -> false) l)
   | _ -> false
	 
  let returnExp op = 
    let cmd = get_command op in
    match cmd.Command.code with
      Command.Block l -> 
	begin
	  match List.hd (List.rev l) with 
	    Command.Return e -> e 
	  | _ -> failwith "Bad arg to returnExp 1"
	end
    | _ -> failwith "Bad arg to returnExp"
	  
  let isBlock op =
    let cmd = get_command op in
    match cmd.Command.code with
      Command.Block _ -> true
    | _ -> false
	  
  let isPredicate op =
    let cmd = get_command op in
    match cmd.Command.code with
      Command.Pred _ -> true
    | _ -> false
	  
  let isFunCall op = 
    let cmd = get_command op in
    match cmd.Command.code with
      Command.FunctionCall _ -> true 
    | Command.GuardedFunctionCall _ -> true 
    | _ -> false

   let isDefinedFunCall op = 
     let cmd = get_command op in
      match cmd.Command.code with
        Command.FunctionCall e 
      | Command.GuardedFunctionCall (_,e) ->  
          (let (s,_,_) = deconstructFunCall e in is_defined s) 
      | _ -> false
  
let is_alloc fn =
  List.mem fn ["alloc"; "malloc"; "calloc"; "realloc"; "kmalloc" ]
  ||
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "alloc" function_def.fun_attributes
  with Not_found -> false



let is_dealloc fn =
  List.mem fn ["free" ]
  ||
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "free" function_def.fun_attributes
  with Not_found -> false

(** a function marked noexpand is treated as a possibly uninterpreted
    function by Blast. it is assumed that the function has no side
    effects. The sp of such a function call x = f(y) is just
    x = f(y), ie f(y) is treated as a term in the logic.
*)
let is_noexpand fn =
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "noexpand" function_def.fun_attributes
  with Not_found -> false

let is_noreturn fn =
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "noreturn" function_def.fun_attributes
  with Not_found -> false

let is_interface fn =
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "public" function_def.fun_attributes
  with Not_found -> false

(* Find a return block in the function; if it has undergone the "one return" transformation, the result will be deterministic *)
let return_of_fn fname =
  let retloc = ref None in
  let proc_edge e = try retloc := Some(returnExp e) with _ -> () in
  map_edges_fname proc_edge fname;
  !retloc

(*****************************************************************************************)
(******************************* Event Handler Code **************************************)
(*****************************************************************************************)

  let is_dispatch_loc loc = (CFA.location_function_name loc = __BLAST_DispatchFunctionName)
  

  let isDispatchCall op = 
    match (get_command op).Command.code with
       Command.FunctionCall e 
     | Command.GuardedFunctionCall (_,e) -> 
        let (s,_,_) = deconstructFunCall e in s = __BLAST_DispatchFunctionName
     | _ -> false

  
  let _isAsyncFn op = 
    Options.getValueOfBool "events" && 
    (match (get_command op).Command.code with
      Command.FunctionCall e | Command.GuardedFunctionCall (_,e) -> 
        let (s,_,_) = deconstructFunCall e in isEventHandler_fname s
     | _ -> false)
   
  let isAsyncCall op = (_isAsyncFn op) && not (is_dispatch_loc (get_source op))
  
  let isAsyncExec op = (_isAsyncFn op) && (is_dispatch_loc (get_source op)) 

  let acAsyncCall op =
    Message.msg_string Message.Debug "in acAsyncCall";
    let rv = 
      if not (isAsyncCall op) then op else
      let src = get_source op in
      let did = location_coords (get_target op) in
      try List.find (fun e -> location_coords (get_target e) = did) (get_outgoing_edges src)
      with Not_found -> (assert false; failwith "no ac asyncCall!") in
    Message.msg_string Message.Debug "done acAsyncCall";
    rv
(**************************************************************************************************)

let enter_call op = 
  match (get_command op).Command.code with
       Command.FunctionCall e 
     | Command.GuardedFunctionCall (_,e) -> 
        let (fn,_,_) = deconstructFunCall e in 
          not (is_noexpand fn || (not (is_defined fn)) || isAsyncCall op)
     | _ -> false


  
(**********START Loop head computation ************************************************************)			
let loop_head_table : ((int * int), bool) Hashtbl.t = Hashtbl.create 101

(* called by initialize_blast *)
let fill_loop_head_table () = 
let compute_loop_heads fname =
  let visited_table = Hashtbl.create 37 in
  let rec _dfs stack node = 
    let node_id = location_coords node in
    if (List.mem node_id stack) then
      begin
        Message.msg_string Message.Debug 
        (Printf.sprintf "Loop head : (%d,%d)" (fst node_id) (snd node_id));
        Hashtbl.replace loop_head_table node_id true
      end;
    if not (Hashtbl.mem visited_table node_id) then 
      begin
        Hashtbl.replace visited_table node_id true;
        List.iter
        (fun (_,child) -> _dfs (node_id::stack) child)
        (CFA.get_succs node)
      end;
  in
  _dfs [] (lookup_entry_location fname)
in
Misc.run_on_table (fun fname _ -> try compute_loop_heads fname with _ -> ()) global_function_table

let is_loopback loc = 
  Hashtbl.mem loop_head_table (location_coords loc)

(**********END Loop head computation ************************************************************)			


	(* this should be in CFA or something! *)
  module LocationSet =
    Set.Make(struct
               type t = location
               let compare loc_1 loc_2 = compare loc_1.CFA.loc_id loc_2.CFA.loc_id 
             end)

let print_system_dot () =
    let cfa_file = Options.getValueOfString "cfa-dot" in
    if cfa_file <> "" then
      begin
	let ch = open_out cfa_file in
	List.iter (output_cfa_dot ch)
	  (List.sort (fun fundef1 fundef2 ->
            compare fundef1.function_name fundef2.function_name)
	     (Hashtbl.fold (fun _ fundef l -> fundef :: l)
		global_function_table []));
	close_out ch;
	ignore (Sys.command ("dot -Tps -o "^cfa_file^".ps "^cfa_file)) ;
	()
      end
    else ()

  let print_system fmt () =
    let print_function function_def =
      let already_printed_locations = ref LocationSet.empty in
      let rec print_location_and_edges loc =
        if (LocationSet.mem loc !already_printed_locations) then
          ()
        else
          begin
            let ingoing_edges = List.rev (get_ingoing_edges loc)
            and outgoing_edges = List.rev (get_outgoing_edges loc)
            in
              Format.fprintf fmt "@ @    @[<v>Location: " ;
              print_location fmt loc ;
              Format.fprintf fmt "@ " ;
              Format.fprintf fmt "@ Attributes: ";
	      List.iter (fun (s,_) -> Format.fprintf fmt "%s" s)
		(get_loc_attribs (loc.CFA.automaton.CFA.aut_id, loc.CFA.loc_id));
	      Format.fprintf fmt "   @[<v>Outgoing edges:" ;
              List.iter (fun e -> Format.fprintf fmt "@    " ; print_edge fmt e)
                        outgoing_edges ;
              Format.fprintf fmt "@ Ingoing edges:" ;
              List.iter (fun e -> Format.fprintf fmt "@    "; print_edge fmt e)
                        ingoing_edges ;
              Format.fprintf fmt "@]@]" ;
              already_printed_locations := (LocationSet.add loc !already_printed_locations) ;
              List.iter (fun e -> print_location_and_edges (get_target e)) outgoing_edges
          end in
      let label_table = (CFA.get_automaton_attributes function_def.cfa).label_table
      in
        Format.fprintf fmt "@ @ @ Function: %s" function_def.function_name ;
        if (Cil.hasAttribute "atomic" function_def.fun_attributes)
        then Format.fprintf fmt "@  atomic";
        if (Cil.hasAttribute __BLAST_EventHandlerAttrib function_def.fun_attributes)
        then Format.fprintf fmt "@  eventhandler";
        if (Cil.hasAttribute "noreturn" function_def.fun_attributes) 
        then Format.fprintf fmt "@  __noreturn";
        Format.fprintf fmt "@  Formals: " ;
        print_formals fmt (function_def.formals) ;
        Format.fprintf fmt "@  Labels: @[<v>" ;
        Hashtbl.iter (fun lbl loc ->
                        Format.fprintf fmt "%s: " lbl ;
                        print_location fmt loc ;
                        Format.fprintf fmt "@ ")
                     label_table ;
        Format.fprintf fmt "@]" ;
        print_location_and_edges (CFA.get_start_location function_def.cfa)
    and print_variable var_def =
       match var_def with
	Varinfo v -> 
	  Format.fprintf fmt "@ %s%s@\n"
            v.Cil.vname
            (if (Cil.hasAttribute "lock" v.Cil.vattr) then " (lock)" else "");
	  ()
      |	Fieldinfo f -> 
	  Format.fprintf fmt "@ %s.%s%s@\n"
	    self
            f.Cil.fname
            (if (Cil.hasAttribute "lock" f.Cil.fattr) then " (lock)" else "");
	  ()
    in
    Format.fprintf fmt "@[<v>" ;
    Format.fprintf fmt "Functions are: @[" ;
    Hashtbl.iter (fun fn _ -> Format.fprintf fmt "%s@ " fn) global_function_table ;
    Format.fprintf fmt "@]" ;
    Hashtbl.iter (fun _ fn_def -> print_function fn_def) global_function_table ;
    Format.fprintf fmt "@]@\n" ;
    Format.fprintf fmt "@[Global variables are:@\n";
    Hashtbl.iter (fun _ var_def -> print_variable var_def) global_variable_table;
    Format.fprintf fmt "@]@\n";
    Format.fprintf fmt "@[Lvalues are:@\n";
    print_all_lvals () ;
    Format.fprintf fmt "@]@\n";
    print_attribute_table ();
    ()

  let get_spec_functions spec_function_prefix =
    Hashtbl.fold (fun fn_name _ curr ->
                    if (Misc.is_prefix spec_function_prefix fn_name)
                    then fn_name::curr
                    else curr)
                 global_function_table []

  let get_args_for_spec_call spec_formal_prefix formals spec_formals fun_name =
    let process_formal s =
      (* [Greg] I believe the following comment is no longer relevant, but
         I leave it there just in case *)
      (* Jhala: change the thing below to change the prefix *)
      if (not (Misc.is_prefix spec_formal_prefix s)) then
        failwith("Bad formal '"^ s ^"' in spec function!")
      else
	begin
	  let  _s = Misc.chop_after_prefix spec_formal_prefix s
          in
	    if _s = "ret" then Expression.Lval (Expression.Symbol("__retres_" ^ fun_name))
	    else (* the string is an int *)
	      let num = (int_of_string _s) - 1 in
		Expression.Lval (Expression.Symbol(List.nth formals num))
	end
    in
      List.map (process_formal) spec_formals


   let hook_cmd_before_start cfa hook_cmd =
    let start_loc = CFA.get_start_location cfa in
    let fname = CFA.location_function_name start_loc in
    let new_start_loc = cfa_create_location fname cfa Artificial
    in
      CFA.set_start_location cfa new_start_loc ;
      CFA.add_edge new_start_loc start_loc hook_cmd

  let hook_cmd_before_return cfa hook_cmd =
    let end_loc = CFA.get_end_location cfa
    in
      (* Since we call oneret on each function, there can be only one edge
         leading to the end location (and it is labeled by a return) *)
      assert ((List.length (CFA.get_preds end_loc)) = 1) ;
      let (return_cmd, end_loc_pred) = List.hd (CFA.get_preds end_loc) in
      let end_loc_pred_attr = CFA.get_location_attributes end_loc_pred in
      let fname = CFA.location_function_name end_loc in
      let new_end_loc_pred = cfa_create_location fname cfa end_loc_pred_attr
      in
        (* end_loc_pred should have only one successor *)
        assert ((List.length (CFA.get_succs end_loc_pred)) = 1) ;
        (* Let's replace end_loc_pred by new_end_loc_pred *)
        set_location_attributes end_loc_pred Artificial ;
        (* We redirect end_loc's incoming edge to new_end_loc_pred *)
        CFA.remove_edge end_loc_pred end_loc return_cmd ;
        CFA.add_edge new_end_loc_pred end_loc return_cmd ;
        (* end_loc_pred should not have any successor now *)
        assert ((CFA.get_succs end_loc_pred) = []) ;
        (* we now can hook the command hook_cmd *)
        CFA.add_edge end_loc_pred new_end_loc_pred hook_cmd

  let instrument_with_spec spec_function_prefix spec_formal_prefix =
    let instrument_spec_function spec_fun_name =
      let chopped_sfn = Misc.chop_after_prefix spec_function_prefix
                                               spec_fun_name
      in
      let (fun_name, is_call, is_return) =
        if (Misc.is_prefix "call_" chopped_sfn)
        then
          (Misc.chop_after_prefix "call_" chopped_sfn, true, false)
        else
          if (Misc.is_prefix "return_" chopped_sfn)
          then
            (Misc.chop_after_prefix "return_" chopped_sfn, false, true)
          else
            ("", false, false)
      in
        if (not (is_call || is_return))
        then
          (* We do nothing, this might be an internal spec function *)
          let src_pos = (Hashtbl.find global_function_table
                                      spec_fun_name).fun_src_pos in
          let msg = src_pos.file_name ^ ":" ^ (string_of_int src_pos.line)
                    ^ ": " ^ "warning: un-hooked spec-like function '" ^
                    spec_fun_name ^ "'."
          in
            Message.msg_string Message.Normal msg ;
        else
          let get_formals_as_string_list = function
              Variable l | Fixed l -> l in
          let formals =
            get_formals_as_string_list (lookup_formals fun_name)
          and spec_formals =
            get_formals_as_string_list (lookup_formals spec_fun_name) in
          let spec_args = get_args_for_spec_call spec_formal_prefix formals
                                                 spec_formals fun_name in
          let hook_code =
            C_Command.FunctionCall
              (Expression.FunctionCall(Expression.Lval (Expression.Symbol(spec_fun_name)),
                                       spec_args))
          and cfa = (Hashtbl.find global_function_table fun_name).cfa
          in
          let hook_cmd = C_Command.gather_accesses hook_code in
            if (is_call)
            then hook_cmd_before_start cfa hook_cmd
            else hook_cmd_before_return cfa hook_cmd
    in
      List.iter instrument_spec_function (get_spec_functions spec_function_prefix)

  (* the following function replaces a function call by a new function call.
     I need this in the ccured stuff -- rupak *)
  let replace_code_in_cfa e newcode =
    let newcmd = C_Command.gather_accesses (Command.FunctionCall newcode) in
    let (src, target) = (get_source e, get_target e) in	  
    if ((List.length (CFA.get_preds target)) != 1) then failwith "replace fails" else ();
    let (oldcmd, pred_loc) = List.hd (CFA.get_preds target) in
    CFA.remove_edge src target oldcmd ;
    CFA.add_edge src target newcmd


(* these are redefined later !*)
(* ESCHEW! 

let globals () = 
  
  Misc.map_partial 
    (function Expression.Symbol s -> Some s | _ -> None) 
    (Misc.hashtbl_keys global_variable_table) 
    
let make_symvar f x = 
  if (is_global (Expression.Symbol x)) then (x^"#@"^f) 
  else 
    let [pre;suff] = Misc.bounded_chop x "@" 2 in
      pre^"#@"^suff

  
let add_symbolic_hooks () =
  Message.msg_string Message.Debug "In add_symbolic_hooks";
  let globs = globals () (* actually we should be doing a mod analysis blah blah blah *) in
  let sym_assume f =
    let formals = 
      match lookup_formals f with
	  Variable l -> l
	| Fixed l -> l
    in
    let make_eq x = 
      Command.Expr( Expression.Assignment (Expression.Assign,Expression.Symbol x, Expression.Lval( Expression.Symbol(make_symvar f x))))
    in
      { Command.code = C_Command.Block (List.map make_eq (formals@(globs)));
	Command.read = [];    (* and its not so hard to change *)
	Command.written = []; (* um er. Should be changed ...*)
      }
  in
  let process_function f = 
    Message.msg_string Message.Debug ("Processing function: "^f);
    let cfa = (Hashtbl.find global_function_table f).cfa in
      hook_cmd_before_start cfa (sym_assume f)
  in
    List.iter process_function (Misc.hashtbl_keys global_function_table)

*)
(****************************************************************) 
(*-------------------- call graph interface --------------------*) 
(****************************************************************)

(* we shall use the biDirectionalLabeledGraph module to represent
   the callGraph *)

(* a call node describes the local calling structure for a
 * single function: which functions it calls, and which
 * functions call it *)
  
type edge_id_t = (int * int) * (int * int)
type callnode = (string,(edge_id_t * Expression.expression)) BiDirectionalLabeledGraph.node  

(* this is the "global" structure through which one can access the call graph *)
let fun_node_table = Hashtbl.create 31 

(* string -> string list -- fn -> funs that call fn in their body *)
let syntactic_calls_into_table = Hashtbl.create 101

let getNode (fname:string) : callnode =
  try
    Hashtbl.find fun_node_table fname
  with Not_found -> failwith ("getNode fails -- unknown function :"^fname)
    
exception Not_found_main

let get_main_node () = 
  let main = List.hd (Options.getValueOfStringList "main") in
    try
      getNode main
    with _ ->
      (Message.msg_string Message.Error ("Main function "^main^" not found in call graph!");
       raise Not_found_main ) 
      
let get_callee_nodes (node: callnode) : callnode list =
  BiDirectionalLabeledGraph.get_children node (* returns duplicates if multiple call edges *)

let get_caller_nodes (node: callnode) : callnode list =
  BiDirectionalLabeledGraph.get_parents node
    

let get_node_fname (node : callnode) : string =
  BiDirectionalLabeledGraph.get_node_label node

let id_of_calledge e = fst (BiDirectionalLabeledGraph.get_edge_label e)
let fcexp_of_calledge e = snd (BiDirectionalLabeledGraph.get_edge_label e)

(* these should be the ONLY function exposed to the outside world *)
let calls_made_by fname =
  try
    let fnode = getNode fname in
      List.map
	(fun e -> (fcexp_of_calledge e,
		   BiDirectionalLabeledGraph.get_node_label
		     (BiDirectionalLabeledGraph.get_target e)))
	(BiDirectionalLabeledGraph.get_out_edges fnode)
  with _ ->
    begin
      Message.msg_string Message.Normal ("Unknown fun :"^fname);
      []
    end

let calls_made_into fname =
  try
    let fnode = getNode fname in
      List.map
	(fun e -> (BiDirectionalLabeledGraph.get_edge_label e, (* we need the edge_id *)
		   BiDirectionalLabeledGraph.get_node_label
		     (BiDirectionalLabeledGraph.get_source e)))
	(BiDirectionalLabeledGraph.get_in_edges fnode)
  with _ ->
    begin
      Message.msg_string Message.Normal ("Unknown fun :"^fname);
      []
    end
    
let callgraph_built_flag = ref false (* used to check if the callgraph has been built *)
 
    
    (* We need only:
       a. scc_fname_map : int -> fname list
       a'. fname_scc_map : fname -> int
       b. scc_pre : int -> int list
       c. scc_sinks : int list 
       where int is the "ID" of the SCC
    *)

type call_graph_scc_info = 
  { scc_fname_map : int -> string list;
        fname_scc_map : string -> int;
        scc_pre : int -> int list;
        scc_sinks : int list ;
        scc_size : int;
        scc_post : int -> int list;
        topo_sort : int list
      }

let this_scc_info = ref (None) (* (call_graph_scc_info option) ref  *)

let calldepth_table = Hashtbl.create 101 
(* JHALA: used to store the "depth" of each function in callgraph 
 * i.e. length of SP from main to fn. We should unify 
 * the callgraph data structures. sigh. *)
 

(**************************************************************************)
(************************* BEGIN SCC INTERFACE ****************************)
(**************************************************************************)

let this_scc_fnames i = 
  match !this_scc_info with
  Some(t) -> t.scc_fname_map i
  | _ -> failwith "wherefore ? this_scc_fnames"

let this_fname_scc fname = 
  match !this_scc_info with
  Some(t) -> 
    begin
      try t.fname_scc_map fname with e -> 
        begin
         let es = Printexc.to_string e in
           Message.msg_string Message.Error ("this_fname_scc fails, raises:"^es);
           raise e     
         end 
    end
  | _ -> failwith "wherefore ? this_fname_scc"


let make_scc () =
  assert (!callgraph_built_flag);
  if !this_scc_info <> None then ()
  else
  (*1. build callgraph *)
  (* callgraph is built. the ONLY fun that calls this is compute_callgraph *)
  let main_root = get_main_node () in
    (* 2. build scc dag *)
     let sccs =
    List.map (List.map (fun n -> BiDirectionalLabeledGraph.get_node_label n)) 
      (BiDirectionalLabeledGraph.scc main_root) 
  in
  let num_sccs = List.length sccs in
  let scc_array = Array.create (1 + num_sccs) [] in
  let fn_scc_table = Hashtbl.create 31 in
  let filler count scc =
    Array.set scc_array count scc;
    List.iter (fun fname -> Hashtbl.replace fn_scc_table fname count) scc;
    Message.msg_string Message.Debug
      (Printf.sprintf "SCC %d: %s" count (Misc.strList scc));
    (count + 1)
  in
  let _ = Message.msg_string Message.Debug "Filling SCC info" in
  let _ = List.fold_left filler 1 sccs in
  let _ = Message.msg_string Message.Debug "SCC info filled" in
    (* scc_table filled: maps fname -> int where int is the fname the scc belongs to *)
  let scc_fname_map i = scc_array.(i) in
  let fname_scc_map fname =
    try Hashtbl.find fn_scc_table fname
    with Not_found -> -1 (* INV: fname is unknown or unreachable *)
  in
  let (scc_pre,scc_post) = 
    let scc_next _plug i =
      let  _ = assert (i <> -1) in 
            (* RJ: actually this is a really involved 
                * array bounds check instance.. *)
      let i_funs = scc_array.(i) in
      let i_nodes =
	try List.map getNode i_funs 
	with Not_found -> failwith "unexpected not found!" 
      in
	(* ASSERT: this should never raise a not_found *)
      let i_next_funs =
	List.map get_node_fname
	  (List.flatten (List.map _plug i_nodes))
      in
	List.filter
        (fun i' -> (i' <> i) && (i' <> -1)) 
        (* no self loops, or non-scc's thank you. *)
	  (Misc.sort_and_compact (List.map fname_scc_map i_next_funs))
	  
    in
      (scc_next get_caller_nodes, scc_next get_callee_nodes)
  in
  let scc_sinks =
    List.filter
      (fun i ->
	 let p = scc_post i in
	 let _ =
	   Message.msg_string Message.Debug
	     (Printf.sprintf "Post %d : %s" i (Misc.strIntList p))
	 in
	   (p = []))
      (Misc.make_list num_sccs)
  in
  let _ = Message.msg_string Message.Debug
	    ("SCC sinks: "^(Misc.strList (List.map string_of_int scc_sinks)))
  in
  (* topo sort. *)
  let topo_sort_array = Array.create (1 + num_sccs) 0 in
  (* fill *)
  let outlist = ref [] in
  let worklist = ref scc_sinks in
  let ch i = 
    let (cur) = topo_sort_array.(i) in
    Array.set topo_sort_array i (cur-1);
    if cur = 1 then worklist := i::!worklist
  in
  let topo_sorted_sccs = 
    for i = 1 to num_sccs do
        Array.set topo_sort_array i (List.length (scc_post i))  
    done;
    while !worklist <> [] do
      let h = List.hd !worklist in
      worklist := List.tl !worklist;
      outlist := h::!outlist;
           List.iter ch (scc_pre h)
    done;
    if (List.length (Misc.sort_and_compact !outlist) <> num_sccs) 
    then failwith "AAARGH-- bug in toposort!";
    List.rev !outlist
  in
    this_scc_info := Some { scc_fname_map = scc_fname_map;
                            fname_scc_map = fname_scc_map;
                            scc_pre = scc_pre;
                            scc_sinks = scc_sinks;
                            scc_size = num_sccs;
                            scc_post = scc_post;
                            topo_sort = topo_sorted_sccs
                          }
    
let compute_calldepths () = 
  let proc (n,d) = Hashtbl.replace calldepth_table (get_node_fname n) d in
  let n_root = get_main_node () in
  let sp_list = BiDirectionalLabeledGraph.shortest_path_lengths n_root in
  List.iter proc sp_list 
			     
let compute_callgraph () =
  let _ = callgraph_built_flag := true in 
  let nodes = List.map
		(fun f -> 
		   let f_node : callnode
		     = BiDirectionalLabeledGraph.create_root f
		   in
		     Hashtbl.replace fun_node_table f f_node;
		     f_node
		)
		(list_of_functions ()) 
  in
  let link_edges n =
    let proc_edge e =
      let add_call (f',callexp) =
        ignore(Misc.hashtbl_check_update syntactic_calls_into_table f' (get_node_fname n));
	try
	  let n' = Hashtbl.find fun_node_table f' in
	    BiDirectionalLabeledGraph.hookup_parent_child n n' (get_edge_coords e, callexp)
	with Not_found -> Message.msg_string Message.Debug ("Unknown called fun: "^f')
      in
      match call_on_edge e with
        Some(f,e) -> add_call (f,e)
      | None -> ()
    in
    ignore ( map_edges_fname proc_edge (BiDirectionalLabeledGraph.get_node_label n) );
      ()
  in
    List.iter link_edges nodes;
    make_scc ();
    compute_calldepths ()
 

(** Check for cycles in the call graph - return true if a cycle found, false otherwise. Prints
  the cycles that were found. *)      
let check_cycles (node : callnode) =
  let flag = ref false in
  let rec print_cycle node stack =
    match stack with 
        [] -> failwith "print_cycle: node not found in stack"
      | h:: t ->
	  Message.msg_string Message.Normal
	  ("Calls "^(get_node_fname h));
	  if not (compare h node = 0) then print_cycle node t
  in
  let rec _do_dfs  (node : callnode) (stack : callnode list) (marked : callnode list)  =
   if (List.mem node marked) then 
     begin (* marked, i.e. visited node *)
       if (List.mem node stack) then 
	 (* cycle found *)
	 begin
	   Message.msg_string Message.Normal "Another cycle found!" ;
	   Message.msg_string Message.Normal ("Function "^ (get_node_fname node));
	   print_cycle node stack ; flag:= true ;
	   Message.msg_string Message.Normal "That's it!" ;
	 end ;
       marked
     end 
   else (* unmarked, i.e. new node *)
     begin
       if (List.mem node stack) then 
	 (* cycle found *)
         begin
	   Message.msg_string Message.Normal "Another cycle found!" ;
	   Message.msg_string Message.Normal ("Function "^ (get_node_fname node));
	   print_cycle node stack ;
	   flag := true
	 end;
       List.fold_right
	 (fun childnode -> fun markednodes -> _do_dfs childnode (node::stack) markednodes) 
	 (get_callee_nodes node)
	 (node :: marked)(* new marked nodes *)
     end
 in
  ignore (_do_dfs node [] []) ;
  !flag

    
let output_callgraph filename =
  assert (!callgraph_built_flag = true);
  let output_node ch _ node =
    Printf.fprintf ch "  %s;\n" (get_node_fname node);
    List.iter
      (fun callee_node ->
	 Printf.fprintf ch "  %s -> %s \n"
         (get_node_fname node) (get_node_fname callee_node)
      )
      (get_callee_nodes node)
  in
  let outch = open_out filename in
    Printf.fprintf outch "digraph callGraph {\n" ;
    Hashtbl.iter (output_node outch) fun_node_table;
    Printf.fprintf outch "}\n\n" ;
    close_out outch;
    ()   

let cg_closure succ_fn fname_list = 
  let reach_nodes =
    let myget fn =
      try Some (getNode fn) with _ -> None
    in
        succ_fn (Misc.map_partial myget fname_list)
  in
  let reach_funs = List.map (get_node_fname) reach_nodes in
    Misc.sort_and_compact reach_funs


let caller_closure = cg_closure BiDirectionalLabeledGraph.descendants 
let backwards_caller_closure = cg_closure BiDirectionalLabeledGraph.ancestors
  
let bounded_backwards_caller_closure depth seed = 
  let get_callers fn = 
          try Hashtbl.find syntactic_calls_into_table fn 
          with Not_found -> [] 
  in
    let rec bfs d s =
    if d <= 0 then s else
       let s' = List.flatten (List.map get_callers s) in
       s@(bfs (d-1) s')
  in
  Misc.sort_and_compact (bfs depth seed)
  
let check_recursion () =
  let startcallnode = get_main_node () in
    Message.msg_string Message.Normal "cr Checking for cycles in the call graph." ;
    let has_cycles = check_cycles startcallnode 
    in Message.msg_string Message.Normal "cr Done checking for cycles in the call graph.";
      has_cycles

let get_reachable_functions () =
  let main = List.hd (Options.getValueOfStringList "main") in
    caller_closure [main]


(* this next thing gives an estimate of the number of paths through the graph *)

let output_call_paths () =
  let _ = Message.msg_string Message.Debug "Start Building Callgraph-ocp" in
  let _ = if not (!callgraph_built_flag) then compute_callgraph () in
  let _ = Message.msg_string Message.Debug "Done Building Callgraph-ocp" in

  let _ = Message.msg_string Message.Debug "In output_call_paths" in
  let recursive = check_recursion () in
    if (recursive &&
	not (Options.getValueOfBool "cf" || Options.getValueOfBool "enablerecursion")) then
      (Message.msg_string Message.Normal "Recursive Call Graph!";  failwith "Recursive Call Graph: use -cf(b)")
    else
      (* (hopefully) the above throws an exception if the graph is recursive, and enablecursion is false *)
      let visited_table = Hashtbl.create 101 in
      let rec num_paths stk (fnode) =
	let f = get_node_fname fnode in
	  if (List.mem f stk) then
	    begin
	      (* HMMM only happens when there is recursion *)
	      let _ = Message.msg_string Message.Normal "Recursive Call Graph!" in
		(* return 1, without adding anything to the result table *)
		Int64.one
	    end
	  else
	    try 
	      Hashtbl.find visited_table f 
	    with
		Not_found ->
		  begin
		    let callees = get_callee_nodes fnode in 
		    let res_list = List.map (num_paths (f::stk)) callees in
		    let my_list_add l = List.fold_right Int64.add l Int64.zero in
		    let rv = Int64.add Int64.one
			       (Int64.add (my_list_add res_list)
				  (Int64.of_int (List.length callees)))
		    in
		      Hashtbl.add visited_table f rv;
		      rv
		  end
      in 
      let startcallnode  = get_main_node () in
      let paths = num_paths [] startcallnode  in
      let visited_funs = (Misc.hashtbl_keys visited_table) in
      let ac_visited_funs = (List.filter is_defined visited_funs) in
	Message.msg_string Message.Normal ("Call Graph Paths: "^(Int64.to_string paths));
	Message.msg_string Message.Normal ("Visited functions: "^(string_of_int (List.length visited_funs)));
	Message.msg_string Message.Normal ("Visited existing functions: "^(string_of_int (List.length ac_visited_funs)));
	Message.msg_string Message.Debug ("Defined functions:");
	List.iter (fun s -> Message.msg_string Message.Debug (Printf.sprintf "%s \n" s)) (list_of_functions ());
	if (Options.getValueOfBool "cg") then exit 0
	else ()
	  
      
(************************** END Callgraph functions ********************************************)


(* dealing with symbolic variables and parameter loading etc etc 

   for each function f, with formals x, globals g, add an assume p, where p is:

   x = x#f  && g = g#f 
*)
	  (* this function should be called BEFORE the model checking begins,
	     if we are doing CF reachability *)


  (* we need the call graph to be built before running this function *)

 (* here, e is an edge in the CFA:
    direct writes simply gathers the lhs of assignments.
  *)
	  (* the aim of the next few functions is to fill these tables *)


			       
(* let lvals_mod_table = Hashtbl.create 101
			 
   let direct_mod_table = Hashtbl.create 101
   
   let globals_modified_table = Hashtbl.create 101 *)


(* fname -> lval list *)	  
let formal_lvals_modified_table = Hashtbl.create 101

(* scc_id -> lval list *)
let global_lvals_modified_table = Hashtbl.create 101


(* we shall also maintain reverse maps for reasons I am too tired to explain *)
(* given a global lval lv, which functions modify it ?*) 
let global_lvals_mod_by_table : (Expression.lval, int list ) Hashtbl.t 
        = Hashtbl.create 101
(* given some random (local) lval, which functions modify it -- via copy-back effects *)
let may_mod_by_table : (Expression.lval, string list) Hashtbl.t = Hashtbl.create 101
			 
(* ("edge",lv)-> bool table "edge" because actually we use the
* (int,int),(int,int) to do the hashing *)
let calledge_lv_effect_table : (edge_id_t * Expression.lval, bool) Hashtbl.t = Hashtbl.create 1001

(* (scc,lv) set *)
let global_lv_effect_table : (int * Expression.lval, bool) Hashtbl.t = Hashtbl.create 1001


(*****************************************************************************)
(*************************** MODS INTERFACE BEGIN ****************************)
(*****************************************************************************)

let global_lvals_modified fname =          
  let scc_id = this_fname_scc  fname in              
  try Hashtbl.find global_lvals_modified_table scc_id
  with Not_found -> []
    
let formal_lvals_modified fname =
  try
    Hashtbl.find formal_lvals_modified_table fname
  with Not_found -> []
    
let global_lvals_mod_by lv =
  let scc_list =
    try Hashtbl.find global_lvals_mod_by_table lv with Not_found -> []
  in
  List.flatten (List.map this_scc_fnames scc_list)
  
let may_mod_by lv =
  try
    Hashtbl.find may_mod_by_table lv
  with Not_found -> []
    
let local_mod_on_edge e lv = 
 let e_id = get_edge_coords e in
 Message.msg_string Message.Debug 
 (Printf.sprintf "e-coords (%d,%d) --> (%d,%d)" (fst (fst e_id)) (snd (fst
 e_id)) (fst (snd e_id)) (snd (snd e_id)));
  Hashtbl.mem calledge_lv_effect_table ((get_edge_coords e),lv)

let global_mod_on_call fn lv = 
  let scc_id = this_fname_scc fn in
  Hashtbl.mem global_lv_effect_table (scc_id,lv)

(************************* MODS INTERFACE END ********************************)
 


(*****************************************************************************)
(*********************** BEGIN MODS COMPUTATION ******************************)
(*****************************************************************************)

(* returns the set of lvals that are smashed by this call -- fcexp.
   Dont forget to add the GLOBALS smashed *)
let effects_of_call fcexp =
  let (fname,args) =
    match fcexp with (* TBD:FUNCTIONPTR *)
	Expression.FunctionCall(fn,elist) -> (get_fname fn , elist)
      | (Expression.Assignment (_, _ ,Expression.FunctionCall(fn,elist))) ->
	  (get_fname fn, elist)
      | _ -> failwith ("bad arg to effects_of_call "^ (Expression.toString fcexp))
  in
  let lift fname e = 
    let subs_pairs  =
      let params = 
	List.map
	  (fun s -> Expression.Lval (Expression.Symbol s))
	  (match lookup_formals fname with
	       Fixed l -> l
	     | Variable l -> l)
      in
	Misc.partial_combine params args
    in
    let transformer e' =
      (* replace occurrences of * fml with *actual *)
      (* ugh! the above is not done! instead we are replacing fml with actual!*)
      if Expression.isDeref e' then
	Expression.substituteExpression subs_pairs e'
      else e'
    in
    let rv = transformer e in
      if rv <> e then
	begin
	  let lve = (Expression.lv_of_expr (rv)) in
	    (* TBD:SPEED *)
	  let _ = Misc.hashtbl_check_update may_mod_by_table lve fname in 
	    Some lve
	end
      else None 
  in    
  let fn_smash =
    List.map (fun lv -> Expression.Lval lv)
      (try Hashtbl.find formal_lvals_modified_table fname
      with Not_found -> []) (* nonexistent, or in same SCC *)
  in
  let formal_effects = Misc.map_partial (lift fname) fn_smash in
  let global_effects = global_lvals_modified fname in
  let rv = (global_effects @ formal_effects) in
  let _ = Message.msg_string Message.Debug 
  (Printf.sprintf "Call %s effects %s" fname (Misc.strList (List.map
  Expression.lvalToString rv))) in
  rv

let get_writes_of_stmt stmt =
  match stmt with
      Command.Expr (Expression.Assignment(Expression.Assign, target, valu)) -> Some target
    | _ -> None


(* ASSUME that this knows NOTHING about aliases. *)
let get_direct_writes e =
  (* INV: If, there is a call to fname'
     either fname' has its values computed, OR, if not, it is in the same SCC as the function in which e resides,
     OR, fname' is undefined -- we don't distinguish between the last two cases
  *) (* TBD:SECURITY *)
    let rv =
    match (get_command e).C_Command.code with
	Command.Block l ->
          let writes = Misc.map_partial get_writes_of_stmt l in
          (*let e_id = get_edge_coords e in
                (* God forgive us for blowing up this table even more! *)
          List.iter 
          (fun lv -> Hashtbl.replace calledge_lv_effect_table (e_id,lv) true)
          writes;*)
          writes
      | Command.FunctionCall (Expression.Assignment (_, lv , fcexp)) ->
	  begin
	    lv::(effects_of_call fcexp)
	  end
      | Command.FunctionCall (fcexp) -> (effects_of_call fcexp)
      | Command.GuardedFunctionCall (_, fcexp) -> (effects_of_call fcexp)
      | _ -> []
  in
    rv


 (* if we add something of too great depth, then add *x *)
    
  let lift_to_top fname lv =   
    
        let syms = 
                Misc.sort_and_compact 
                 (Expression.lvals_of_expression (Expression.Lval lv)) 
        in
          let formals_in_lv = 
        let fmls = 
            match lookup_formals fname with
              Fixed l -> l
            | Variable l -> l
        in
        let fmls = List.map (fun s -> Expression.Symbol s) fmls in
        List.filter (fun l' -> List.mem l' fmls) syms
    in
    let too_deep lv' = 
            let occ_in = List.filter 
            (fun sym -> 
              Expression.occurs_check 
              (Expression.Lval (Expression.Dereference (Expression.Lval
            lv'))) (Expression.Lval sym)) syms in
            List.length occ_in > Options.getValueOfInt "cldepth"
    in
    let deep_formals_in_lv = 
       Misc.map_partial 
        (fun lv' -> 
          if too_deep lv'
          then Some (Expression.Dereference (Expression.Lval lv')) else None)
        formals_in_lv
    in
    if deep_formals_in_lv <> [] then
        let _ = 
        Message.msg_string Message.Normal 
            (Printf.sprintf "lift_to_top: %s : %s" (Expression.lvalToString lv)
            (Misc.strList (List.map Expression.lvalToString
            deep_formals_in_lv))) in
        deep_formals_in_lv
    else
        [lv]

let project_lv_to_visible aliases_fn fname lv =
 let fmls =
   match lookup_formals fname with
       Variable l -> l 
     | Fixed l -> l
 in
 let aliases = lv::(aliases_fn fname lv) in
   (* TBD:ARRAYS *)
 (*let _ = Message.msg_string Message.Normal
	   (Printf.sprintf "project_lv in %s: %s aliases %s"
	      fname (Expression.lvalToString lv)
	      (Misc.strList (List.map Expression.lvalToString aliases)))
 in *)
 let formal_lvals =
   let is_formal lv' =
     Misc.nonempty_intersect compare
       (Expression.varsOfExp (Expression.Lval lv'))
       fmls
   in
     List.filter (fun lv' -> is_formal lv') aliases
 in
 let (global_lvals,local_lvals) =
   Misc.filter_cut (fun lv' -> is_global lv') aliases
   (* List.mem "" (scope_of_lval lv')) aliases *)
 in 
 let formal_lvals = 
  List.flatten (List.map (lift_to_top fname) formal_lvals) 
 in
 (global_lvals,formal_lvals,local_lvals)
   

   
(* VERY VERY IMPORTANT:
   this computation does no "closure" -- yet, I argue that it is sound for a very subtle
   reason: if lv1 is aliased to something in CLOS(lv2) but NOT to lv2, then writing to lv2 DOES NOT CHANGE lv1...
   This was buried somewhere in the reasoning for the POPL04 paper.
   Hence, we fret here about aliasing, but NOT about closure.
   RJ: hmm. this seems specious. certainly writing to lv2 changes things in closure(lv2) ... hmm ? but but but...
   So, when adding to the mod_tables, if i mod lv, I add all of closure(lv)... Just that I dont take "aliases" of closures,
   and add that to the set.. Yes, my head hurts too.
*)
(* INV: if lv \in MODS, then clos(lv) \in MODS *)

        
let update_formal_modifies_table fname lval =
    (* ASSERT: lval belongs to "scope" of fname *)
  let table = formal_lvals_modified_table in
  let occurs_checker lv lv' =
    (* elt is the new fellow -- we add it only if elt' is NOT a sub-lval of elt *)
    let varcheck =
      match lv' with
	  Expression.Symbol _ -> (lv <> lv') (* TBD:MESS! *)
	| _ -> false
    in
      if varcheck then false
      else
	Expression.occurs_check (Expression.Lval lv') (Expression.Lval lv)
  in 
  let my_check_upd table fname k =
      Misc.hashtbl_check_update table fname k
  in
    (* check if the root is new.
       If so add the lot and return TRUE. If the root is "dominated" then so is the closure, so lets not bother *)
    
  let dom_check_upd table fname lv =
    let ts = (* if glob_flag then "Global" else*) "Formal" in
      (* Message.msg_string Message.Normal
	(Printf.sprintf "Mods: Adding %s %s %s" ts fname
        (Expression.lvalToString lv));*)
      (* if (glob_flag) then
	ignore(Misc.hashtbl_check_update global_lvals_mod_by_table lv fname);*)
    let rv = Misc.hashtbl_fun_check_update occurs_checker table fname lv in
      if rv then 
         ignore (List.map
		  (Misc.hashtbl_fun_check_update occurs_checker table fname)
		  (lvalue_closure lv));
      rv
  in
    dom_check_upd table fname lval

      
let proc_dw e_id_opt aliases_fn fname lv =
  let (glvs,flvs,local_visible_lvals) = project_lv_to_visible aliases_fn fname lv in (* get aliases *)
  let upd_glb fname lv = 
    let scc_id = this_fname_scc fname in
    let _ = Hashtbl.replace global_lv_effect_table (scc_id,lv) true in
    let _ = Misc.hashtbl_check_update global_lvals_modified_table scc_id lv in
    Misc.hashtbl_check_update global_lvals_mod_by_table lv scc_id
  in
  let _ = List.map (upd_glb fname) glvs in
  let l_fmls =  (List.map (update_formal_modifies_table fname) flvs) in
  let _ = 
    match e_id_opt with
        Some (e_id) -> 
          List.iter 
          (fun lv -> Hashtbl.replace calledge_lv_effect_table (e_id,lv) true)
          local_visible_lvals    
        |   None -> ()
  in
    List.exists (fun b -> b) ( l_fmls)
    

(* the argument is a way around the circular static dependance.
   get_all_aliases : string -> lval -> lval list
   (get_all_aliases_in fname lv) is the set of all aliases of lv "visible" to fname
*)

let print_mods () =
  let _pr scci modlist  =
    Message.msg_string Message.Debug ("SCC "^(string_of_int scci)^" modifies");
    Message.msg_string Message.Debug (Misc.strList (List.map Expression.lvalToString modlist));
  in
  let _pr2 fname modlist =
    Message.msg_string Message.Debug ("Function "^fname^" modifies");
    Message.msg_string Message.Debug (Misc.strList (List.map Expression.lvalToString modlist));
  in

  let _pr1 (((i1,i2),(i3,i4)),lv) _ =
    Message.msg_string Message.Debug 
    (Printf.sprintf "edge-mod: ((%d,%d),(%d,%d)) -- %s" i1 i2 i3 i4 
    (Expression.lvalToString lv))
  in
    Message.msg_string Message.Debug ("MODS INFORMATION : GLOBAL LVALUES");
    Hashtbl.iter _pr global_lvals_modified_table;
    Message.msg_string Message.Debug ("MODS INFORMATION : FORMAL LVALUES");
    Hashtbl.iter _pr2 formal_lvals_modified_table;
    Hashtbl.iter _pr1 calledge_lv_effect_table   
    
    
let compute_modifies aliasfn =
  let compute_scc_mods scc_fname_map fname_scc_map i =  
    let _ = assert (i <> -1) in (* RJ: again -- a very subtle array bounds check *)
    let _ = Message.msg_string Message.Normal ("Proc. scc--"^(string_of_int i))
    in
    let process_direct fname = (* TBD:DWTAG *)
      let direct_writes = 
        let dwrites = ref [] in
	let proc_edge e =
          let this_dw = get_direct_writes e in
          let e_id_o = if (isFunCall e) then Some (get_edge_coords e) else None in
	  dwrites := (e_id_o,this_dw) :: !dwrites in
	ignore(map_edges_fname proc_edge fname);
	!dwrites
      in
      let _ =  Message.msg_string Message.Normal 
      (Printf.sprintf "Direct Wr:%d:%s" i fname) in
      (*let _ = Message.msg_string Message.Normal
		(Misc.strList (List.map Expression.lvalToString
                direct_writes))*)
      
      let _dwp (e_id_o,lv_list) = 
        List.map (proc_dw e_id_o aliasfn fname) lv_list
      in
      let dwp_res = List.flatten (List.map _dwp direct_writes) in
        List.exists (fun b -> b) dwp_res 
        (* (List.map (proc_dw None aliasfn fname) direct_writes) *)
    in	 
    let push_effects ((e_id,fcexp),caller_name)  =
      let effects_lv = effects_of_call fcexp in
      let proc_effect x = 
        let rv = proc_dw (Some e_id) aliasfn caller_name x in
        (*if rv then Message.msg_string Message.Normal 
        (Printf.sprintf "New effect for %s : %s" caller_name
        (Expression.lvalToString x));*)
        rv
      in
	List.exists (fun b -> b) (List.map (proc_effect) effects_lv)
    in
      (* compute scc_mods *)
    let rec effect_push flist =
      match flist with
	  [] -> ()
	| f::t ->
	    begin
	      (*let _ =  Message.msg_string Message.Normal ("effect push "^f)
            in*)
	      let calls_into_list =
		List.filter
		  (fun (_,fname') ->
		     let rv = (fname_scc_map fname') = (fname_scc_map f) in
		       (*if rv then
			 Message.msg_string Message.Normal ("propagate to  "^fname');*)
		       rv) (* same SCC *)
		  (calls_made_into f)
	      in
	      let propagate ((e_id, fcexp),f') =
		if push_effects ((e_id,fcexp),f') then Some f' else None
	      in
	      effect_push (t (*@(Misc.map_partial propagate calls_into_list)*))
	    end
    in
    let all_scc_funs = (scc_fname_map i) in
    assert (List.length all_scc_funs > 0);
    Message.msg_string Message.Normal
	(Printf.sprintf "Compute scc_mods: %d : %d : %s" i (List.length all_scc_funs) (Misc.strList all_scc_funs)) ;
    ignore(List.map process_direct all_scc_funs);
    Message.msg_string Message.Error "COMP MOD iter1 done";
    effect_push all_scc_funs; (*[List.hd all_scc_funs])*)
    Message.msg_string Message.Error "COMP MOD iter2 done"
 in
 let read_gl_list fname = 
   if not (Sys.file_exists fname) then (raise Not_found)
   else
       let ic = open_in fname in
     let gl_list = 
       Message.msg_string Message.Normal ("Found existing gl mods file "^fname);
       (Marshal.from_channel ic : ((int * (Expression.lval list)) list))
     in
     close_in ic;
     gl_list
 in
 let read_fl_list fname = 
   if not (Sys.file_exists fname) then (raise Not_found)
   else 
     

       let ic = open_in fname in
     let fl_list = 
       Message.msg_string Message.Normal ("Found existing fl mods file "^fname);
       try 
         (Marshal.from_channel ic: ((string * (Expression.lval list)) list))
       with _ -> failwith ("Please erase "^fname)
     in
     let _ = close_in ic in
     fl_list
 in
 let read_mmb_list fname = 
   if not (Sys.file_exists fname) then (raise Not_found)
   else

       let ic = open_in fname in
     let mmb_list = 
       Message.msg_string Message.Normal ("Found existing mmb mods file "^fname);
       try 
         (Marshal.from_channel ic: ((Expression.lval * (string list)) list))
       with _ -> failwith ("Please erase "^fname)
     in
     let _ = close_in ic in
     mmb_list
 in
 let read_ceef_list fname =
   if not (Sys.file_exists fname) then (raise Not_found)
   else

       let ic = open_in fname in
     let ceef_list = 
       Message.msg_string Message.Normal ("Found existing ceef mods file "^fname);
       try 
         (Marshal.from_channel ic: ((edge_id_t * Expression.lval) list))
       with _ -> failwith ("Please erase "^fname)
     in
     let _ = close_in ic in
     ceef_list
 in
 let read_mods () =
   let paste_table table (fname,lv_list) =
       Hashtbl.replace table fname lv_list
   in
   let glp i lv = 
       Misc.hashtbl_update global_lvals_mod_by_table lv i;
       Hashtbl.replace global_lv_effect_table (i,lv) true
   in
   if (Options.getValueOfBool "nomod") then ()
   else
     let base_mod_file = (Options.getValueOfString "mainsourcename")^".mods" in
     let gl_list = read_gl_list (base_mod_file^".gl") in
     
     let _ = Message.msg_string Message.Error ("Done reading mods .gl") in  
     let fml_list = read_fl_list (base_mod_file^".fl") in
     
     let _ = Message.msg_string Message.Error ("Done reading mods .fl") in  
     let may_mod_by_list = read_mmb_list (base_mod_file^".mmb") in
     
     let _ = Message.msg_string Message.Error ("Done reading mods .mmb") in  
     let calledge_effects_list  = read_ceef_list (base_mod_file^".ceef") in
     
     let _ = Message.msg_string Message.Error ("Done reading mods .ceef") in  
     let _ = Message.msg_string Message.Error ("Done reading mods") in  
     Stats.time "glv_table paste" (List.iter (paste_table global_lvals_modified_table)) gl_list;
       Stats.time "flv_table paste" (List.iter (paste_table formal_lvals_modified_table)) fml_list;
       Stats.time "glv_mod_by_table paste" 
        (List.iter (fun (scc_id,lv_list) -> List.iter (glp scc_id) lv_list)) gl_list;
       Stats.time "may_mod_by_table paste" 
        (List.iter (fun (lv,fn_list) -> Hashtbl.replace may_mod_by_table lv fn_list))
        may_mod_by_list;
       Stats.time "calledge_effects pair paste" 
        (List.iter (fun x -> Hashtbl.replace calledge_lv_effect_table x true))
     calledge_effects_list
 in
 let write_obj fname obj = 
    let _ = Message.msg_string Message.Error ("Write mods to : "^fname) in
    let oc = open_out fname in
    Marshal.to_channel oc obj [];
     close_out oc;
     Message.msg_string Message.Error ("Done writing to :"^fname)
   in

 let write_mods () =
   let mod_file = (Options.getValueOfString "mainsourcename")^".mods" in
   let gl_list = Misc.hashtbl_to_list global_lvals_modified_table in
   let fml_list = Misc.hashtbl_to_list formal_lvals_modified_table in
   let may_mod_by_list = Misc.hashtbl_to_list may_mod_by_table in 
   let calledge_effects_list = Misc.hashtbl_to_list calledge_lv_effect_table in
   write_obj (mod_file^".gl") gl_list;
   write_obj (mod_file^".fl") fml_list;
   write_obj (mod_file^".mmb") may_mod_by_list;
   write_obj (mod_file^".ceef") calledge_effects_list;
   Message.msg_string Message.Error "Done writing mods";
   ()
in 
   (* First, check if the mods-info file exists, if so, just load the bloody thing! *)
   let _ =
     if (not !callgraph_built_flag) then compute_callgraph ();
     try
       if (Options.getValueOfBool "recompmod") then (raise Not_found)
       else
        (Stats.time "read mods" read_mods ())
     with _ ->  
       begin
         let _ = 
             match (!this_scc_info) with
                Some (t) -> 
                  let proc () = (compute_scc_mods t.scc_fname_map t.fname_scc_map)
                  in
                  List.fold_left proc () t.topo_sort;
                  Message.msg_string Message.Normal "Done computing mods"
                  
                | None -> failwith "this_scc_info where aret thou ??"
         in (* send to file *)
	   write_mods ()
       end
   in
     () (* print_mods () *) (* SLOW! *) 
(*********************************************************************************************************)

      
let globals () = (* TBD HEREHEREHEREHEREHERE *)
    Misc.map_partial 
    (function Expression.Symbol s -> Some ((*Expression.Symbol*) s) | _ -> None) 
     (Misc.hashtbl_keys global_variable_table) 
    
(* this function tells you which globals are modified by some function ... 
   used for the symbolic variable thing *)
(*
  DELETE
let globals_modified fname = 
  if (Options.getValueOfBool "mod") then
    let rv = 
      try
	Hashtbl.find globals_modified_table fname
      with
	  Not_found -> [] (* if the function is not there, we assume that it doesnot change the globals ... *)
    in    
      Misc.map_partial 
	(function Expression.Symbol s -> Some ((* Expression.Symbol*) s) | _ -> None) rv
	(* for now, I shall deal with only the variables, no pointer-y things *)
  else
    globals() (* say that ALL the globals can be modified ... *)
*)
      
(** returns the list of functions 
 * that _can syntactically reach_ fname, may be
 * faster than backwards_caller_closure *)

let get_scc_gates next_fn scc_id all_scc_funs =
  let checker fn = 
    let nexts = next_fn fn in
    List.exists ( fun fn' -> this_fname_scc fn' <> scc_id) nexts
  in
  List.filter checker all_scc_funs

let get_scc_entries = 
  let callers fn = List.map snd (calls_made_into fn) in
  get_scc_gates callers

let get_scc_exits =
  let callees fn = List.map snd (calls_made_by fn) in
  get_scc_gates callees


let call_depth loc = 
  let fname = get_location_fname loc in
  try Hashtbl.find calldepth_table fname with Not_found -> 
    failwith ("Function not in calldepth table!: "^fname)

let shortest_path f_in f_out = 
  Message.msg_string Message.Normal (Printf.sprintf "Shortest path: %s %s" f_in f_out);
  let n_s = getNode f_in in
  let n_t = getNode f_out in
  let rv' = 
    List.map (fun (n,_) -> get_node_fname n)
    (BiDirectionalLabeledGraph.shortest_path (fun x y -> true) n_s n_t) 
  in
  let rv = rv'@[f_out] in
  Message.msg_string Message.Normal ("SPath is: "^(Misc.strList rv));
  rv

  (* 
  let succ fn = List.map snd (calls_made_by fn) in
  let sptable = Hashtbl.create 31 in
  let proc (fn,fn'_list) = 
      let _filter fn' = 
        if (Hashtbl.mem sptable fn') then None  else
          (Hashtbl.replace sptable fn' fn; (Some fn'))
      in
      Misc.map_partial _filter fn'_list
  in
  let rec iter frontier =
     let _ = Message.msg_string Message.Normal "sp_iter" in
     let s_s'_list = List.map (fun fn -> (fn,succ fn)) frontier in
     let frontier' = List.flatten (List.map proc s_s'_list) in
     if (Hashtbl.mem sptable f_out) then ()
     else 
        if (frontier' = []) then failwith "unreachable!" 
        else (iter frontier')
  in
  let rec getpath path targ =
    let par = try Hashtbl.find sptable targ with Not_found -> 
      failwith "iter is broken"
    in
    let path' = par::path in
    if par = f_in then path' else getpath path' par
  in
  let rv = 
    if (f_in = f_out) then [f_in]
    else
      begin
         Hashtbl.replace sptable f_in f_in;
         iter [f_in];
         getpath [f_out] f_out 
       end
  in *)  
      
let expand_scc seed_fn scc_i =
  let  main_fn = List.hd (Options.getValueOfStringList "main") in
  let main_scc = this_fname_scc main_fn in
  let all_scc_funs = this_scc_fnames scc_i in
  let scc_entries = 
    let en = get_scc_entries scc_i all_scc_funs in
    if main_scc = scc_i then main_fn::en else en 
  in
  let scc_exits = get_scc_exits scc_i all_scc_funs in
  let this_scc_funs = 
    if (this_fname_scc seed_fn = scc_i) then
        begin
        List.flatten (List.map (fun fn -> shortest_path fn seed_fn) scc_entries)
        end
    else
        begin
        let i_o_list = Misc.cross_product scc_entries scc_exits in
        let _ =  Message.msg_string Message.Normal 
        ("scc_io product: "^(string_of_int (List.length i_o_list))) in
        List.flatten (List.map (fun (f_i,f_o) -> shortest_path f_i f_o) i_o_list)
        end
  in
  let rv = Misc.sort_and_compact this_scc_funs in
       Message.msg_string Message.Normal 
       (Printf.sprintf "expand-scc-bfs: %d size = %d" scc_i (List.length rv));
       rv
     
let scc_caller_closure fname =
  let _ = if (!this_scc_info = None) then ignore (make_scc ()) in
  let _ = Message.msg_string Message.Normal "make_scc finished" in
  match !this_scc_info with
    None -> failwith("this_scc_info? where art thou ?")
  | Some(info) ->
      let _ = Message.msg_string Message.Normal "In scc_caller_closure" in
      let fname_scc = info.fname_scc_map fname in
      let _back_reach_sccs = Misc.compute_fixpoint info.scc_pre [fname_scc] in
      let (max_scc_size,max_scc_depth) =
        let get_max_depth (size,depth) (i,d) =
          let i_size = List.length (info.scc_fname_map i) in
          if i_size > size then (i_size,d) else (size,depth)
        in
        List.fold_left get_max_depth (0,0) _back_reach_sccs 
      in
      let back_reach_sccs = List.map fst _back_reach_sccs in
      Message.msg_string Message.Normal 
      ("scc_caller_clos -- #sccs: "^(string_of_int (List.length back_reach_sccs)));
      Message.msg_string Message.Normal 
        (Printf.sprintf "scc_caller_clos -- max scc (size,depth): %d, %d" max_scc_size
        max_scc_depth);  
      (* let scc_list = (List.map info.scc_fname_map back_reach_sccs) in
      List.flatten scc_list *)
      let rv = List.flatten (List.map (expand_scc fname) back_reach_sccs)
      in
      Message.msg_string Message.Normal 
      (Printf.sprintf "br_sccs returns: %d : %s" (List.length rv) (Misc.strList rv));
        rv

        
  (* to enable the passing of the error pred between "main" thats juggling
  the edges and the model checker *)      
  let error_lvals = ref [] 
  let reset_error_lvals () = error_lvals := [] 
  let set_error_lvals lvs = error_lvals := lvs
  let get_error_lvals () = !error_lvals 
 
(* Ensure that upon the start of a function, the symconsts have the "starting values" of the various
   parameters.
   i.e. for each lv in clos(x) for x a formal \cup global,
   assign((sym f e), e) *)
let lookup_local_lvals fname =
  let make_lv s = Expression.Symbol s in
  let sym_const_clos =
    let glob_lvs = global_lvals_modified fname in
    let formal_lvs =
      List.map make_lv 
	(match lookup_formals fname with
	     Variable l -> l
	   | Fixed l -> l)
    in
(* RUPAK: unoptimized!
    let all_lvals = List.flatten
		      (List.map
			 (fun lv -> lvalue_closure lv)
			 (formal_lvs@glob_lvs))
    in
    let sym_consts = List.map (Expression.make_symvar fname) all_lvals in
      List.flatten (List.map lvalue_closure sym_consts)
*)
  (* RUPAK: Better optimized: *)
  let _worker lv =
    let lv_closure = lvalue_closure lv in
    let lv_sym_consts = List.map (fun l -> lvalue_closure (Expression.make_symvar fname l) ) lv_closure in
    List.flatten lv_sym_consts
  in
  List.flatten (List.map _worker (formal_lvs @ glob_lvs)) 
  (* END OPTIMIZED *)
  in
  let locals_clos = List.flatten
		      (List.map
			 (fun s -> lvalue_closure (make_lv s))
			 (lookup_locals fname))
  in
    sym_const_clos @ locals_clos 
   
let add_symbolic_hooks () =
  Message.msg_string Message.Normal "In add_symbolic_hooks";
  let sym_assume f =
    (*let glob_lvs = global_lvals_modified f in
    let formal_lvs =
      List.map (fun s -> Expression.Symbol s)
	(match lookup_formals f with
	     Variable l -> l
	   | Fixed l -> l)
    in
    let all_lvals = List.flatten
		      (List.map
			 (fun lv -> lvalue_closure lv)
			 (formal_lvs@glob_lvs))
    in
    let all_sym_consts = List.map (Expression.make_symvar f) all_lvals in
    let make_eq sym_lv lv =
 	Command.Expr(
	  Expression.Assignment
		       (Expression.Assign,sym_lv,Expression.Lval(lv)))
    in*)
      { Command.code = C_Command.SymbolicHook f;
        (* C_Command.Block (List.map2 make_eq all_sym_consts all_lvals) ;*) 
	Command.written =  [];(*all_sym_consts;*)
	Command.read = [];(*all_lvals;*)
      }
  in
  let process_function f = 
    Message.msg_string Message.Normal ("[add_symbolic_hooks] Processing function: "^f);
    let cfa = (Hashtbl.find global_function_table f).cfa in
      hook_cmd_before_start cfa (sym_assume f)
  in
  let proc_table = Hashtbl.create 31 in
  let rec _proc fn =
    if Hashtbl.mem proc_table fn then ()
    else
      begin
	Hashtbl.add proc_table fn true;
	process_function fn;
	List.iter (fun s -> _proc (snd s)) (calls_made_by fn)
      end
  in
  let main = List.hd (Options.getValueOfStringList "main") in   
  List.iter (fun s -> _proc (snd s)) (calls_made_by main) 
      



(***************************)	  
(**
  let oL = ref []

  class getFilePointers = object
    inherit Cil.nopCilVisitor


    method vvdec vardec =
      let t = Cil.unrollType vardec.Cil.vtype in
      (match t with
        Cil.TPtr (Cil.TComp (c, _), _) ->
          if (c.Cil.cname = "FILE") then oL := vardec.Cil.vname :: !oL else ();
      | Cil.TPtr (Cil.TNamed (n, _),_) ->
          if (n.Cil.tname = "FILE") then oL := vardec.Cil.vname :: !oL else ();
      | _ -> ()
       ) ;
      Cil.DoChildren

  end

    let write_out_file_preds () = 
    let file = List.hd (get_cil_files ()) in 
    let ch = open_out "pred" in
    let o = new getFilePointers in 
    let _ = Cil.visitCilFile o file in
    
List.iter (fun v -> (* write out v == 0 *)
	Printf.fprintf ch "%s == 0;\n" v ) !oL ;
    oL := [] ;
    close_out ch


    write_out_file_preds () 
*)
(***************************)	  


(*******************************************************************************)
(***************************   Computing Dominators ****************************)
(*******************************************************************************)

(*Ripped from Muchnick Section 7.3 *)
(* tag "integer" with fname (fname_id,integer *)
(* Renames:
  * N -- nodes 
  * n0 -- (fid,-2) 
  * "exit" node is (fid,-3)
  * *)


(* NEEDS: dom_succ, dom_pred *)

type loc_id_t = int * int

module Loc_id_Set = Set.Make(struct 
  type t = int * int
  let compare x y = compare x y 
  end)

let nodes_tbl : (loc_id_t, bool) Hashtbl.t = Hashtbl.create 1001  
let label_tbl : (loc_id_t , loc_id_t) Hashtbl.t = Hashtbl.create 1001
let parent_tbl : (loc_id_t , loc_id_t) Hashtbl.t  = Hashtbl.create 1001
let ancestor_tbl : (loc_id_t , loc_id_t) Hashtbl.t  = Hashtbl.create 1001
let child_tbl : (loc_id_t , loc_id_t) Hashtbl.t = Hashtbl.create 1001
let ndfs_tbl : (int, loc_id_t) Hashtbl.t = Hashtbl.create 1001
let dfn_tbl : (loc_id_t, int) Hashtbl.t = Hashtbl.create 1001
let sdno_tbl : (loc_id_t, int) Hashtbl.t = Hashtbl.create 1001
let size_tbl : (loc_id_t, int) Hashtbl.t = Hashtbl.create 1001
let bucket_tbl : (loc_id_t, Loc_id_Set.t) Hashtbl.t = Hashtbl.create 1001

let idom_tbl : (loc_id_t, loc_id_t) Hashtbl.t = Hashtbl.create 1001
let n_ref = ref 0


let tasgn tbl k d = Hashtbl.replace tbl k d 
let tget tbl k = try Hashtbl.find tbl k with _ -> failwith "DOM:tget!"

(* BLAST SPECIFIC *)
(* BACKWARDS *)
let dom_pred l = 
  let loc = lookup_location_from_loc_coords l in
  let elist = get_outgoing_edges loc in
  let cands = List.map (fun e -> location_coords (get_target e)) elist in
  List.filter (Hashtbl.mem nodes_tbl) cands

let dom_succ l = 
  let loc = lookup_location_from_loc_coords l in
  let elist = get_ingoing_edges loc in
  let cands = List.map (fun e -> location_coords (get_source e)) elist in
  cands

let get_dom_root l =
  let loc = lookup_location_from_loc_coords l in
  let rv = location_coords (lookup_exit_location (get_location_fname loc)) in
  rv

let get_dom_nodes l = 
  let loc = lookup_location_from_loc_coords l in
  let end_loc = location_coords (lookup_exit_location (get_location_fname loc)) in 
  Hashtbl.clear nodes_tbl;
  let rec _iter l' = 
    if Hashtbl.mem nodes_tbl l' then () 
    else
      (Hashtbl.replace nodes_tbl l' true;
       List.iter _iter (dom_succ l'))
  in
  _iter end_loc

(* END BLAST SPECIFIC *)  


let link_dom v w = 
  Message.msg_string Message.Debug "[DOM]: link_dom";
  let n0 = (fst v,-2) in
  let s_ref = ref w in
  while tget sdno_tbl (tget label_tbl w) < tget sdno_tbl (tget label_tbl (tget child_tbl !s_ref))
  do
    let child_s = tget child_tbl !s_ref in
    let size_s = tget size_tbl !s_ref in
    if ((size_s + (tget size_tbl (tget child_tbl child_s))) 
       >= (2* (tget size_tbl child_s))) then
         begin
           tasgn ancestor_tbl child_s !s_ref;
           tasgn child_tbl !s_ref (tget child_tbl child_s)
         end
    else
      begin
        tasgn size_tbl child_s size_s;
        tasgn ancestor_tbl !s_ref child_s;
        s_ref := child_s
      end;
    tasgn label_tbl !s_ref (tget label_tbl w);
    let size_v = tget size_tbl v in
    let size_w = tget size_tbl w in
    tasgn size_tbl v (size_v + size_w);
    if (tget size_tbl v < 2 * size_w) then
      begin
        let tmp = !s_ref in
        s_ref := tget child_tbl v;
        tasgn child_tbl v tmp
      end;
    while !s_ref <> n0 do
        tasgn ancestor_tbl !s_ref v;
        s_ref := tget child_tbl !s_ref
    done;
  done
  

let rec compress_dom v = 
  Message.msg_string Message.Debug "[DOM]: compress_dom";
  let n0 = (fst v,-2) in
  let anc_v = tget ancestor_tbl v in
  if (tget ancestor_tbl anc_v) <> n0 then
    begin
      compress_dom anc_v;
      let anc_v = tget ancestor_tbl v in
      let _ = if tget sdno_tbl (tget label_tbl anc_v) < tget sdno_tbl (tget label_tbl v) then
        tasgn label_tbl v (tget label_tbl anc_v)
      in
      tasgn ancestor_tbl v (tget ancestor_tbl anc_v)
    end

let eval_dom v = 
 Message.msg_string Message.Debug (Printf.sprintf "[DOM]: eval_dom (%d,%d)" (fst v)(snd v)); 
  let n0 = (fst v, -2) in
  let anc_v = tget ancestor_tbl v in
  if anc_v = n0 then (* return *) tget label_tbl v
  else
    begin
      compress_dom v;
      let anc_v = tget ancestor_tbl v in
      let lab_v = tget label_tbl v in
      if tget sdno_tbl (tget label_tbl anc_v) >= tget sdno_tbl lab_v then
        (* return *) lab_v
      else
        (* return *) tget label_tbl anc_v
    end


let rec depth_first_search_dom v = 
  Message.msg_string Message.Debug (Printf.sprintf "[DOM]: depth_first_search_dom (%d,%d)" (fst v) (snd v));
  let n0 = (fst v, -2) in
  n_ref := !n_ref + 1;
  tasgn sdno_tbl v !n_ref;
  tasgn label_tbl v v; 
  tasgn ndfs_tbl !n_ref v;
  tasgn child_tbl v n0;
  tasgn ancestor_tbl v n0;
  tasgn size_tbl v 1;
  List.iter 
  (fun w -> 
    if (tget sdno_tbl w = 0) then
      (tasgn parent_tbl w v; 
      depth_first_search_dom w)) 
  (dom_succ v)
  


let domin_fast r =
  let nodes = get_dom_nodes r; Misc.hashtbl_keys nodes_tbl in
  let nodes_s = String.concat ";" (List.map (fun (x,y) -> (Printf.sprintf "(%d,%d)" x y)) nodes) 
  in
  Message.msg_string Message.Debug "[DOM]: domin_fast";
  Message.msg_string Message.Debug (Printf.sprintf "[DOM]: r = (%d,%d) N = %s"
  (fst r) (snd r) nodes_s); 
  let n0 = (fst r,-2) in
  List.iter 
  (fun v -> 
    Hashtbl.replace bucket_tbl v Loc_id_Set.empty ;
    Hashtbl.replace sdno_tbl v 0) 
  (n0::nodes);
  tasgn size_tbl n0 0; tasgn sdno_tbl n0 0;
  tasgn ancestor_tbl n0 n0; tasgn label_tbl n0 n0;
  n_ref := 0;
  Hashtbl.clear ndfs_tbl;
  depth_first_search_dom r;
  let i_ref = ref (!n_ref) in
  while (!i_ref >= 2) do
    let w = tget ndfs_tbl !i_ref in
    List.iter 
    (fun v -> 
      let u = eval_dom v in 
      if (tget sdno_tbl u < tget sdno_tbl w) then
        tasgn sdno_tbl w (tget sdno_tbl u);)
    (dom_pred w);
    let k = tget ndfs_tbl (tget sdno_tbl w) in
    let curr = tget bucket_tbl k in
    tasgn bucket_tbl k (Loc_id_Set.add w curr);
    let par_w = tget parent_tbl w in
    link_dom par_w w;
    while not (Loc_id_Set.is_empty (tget bucket_tbl par_w)) do
      let buck_par_w = tget bucket_tbl par_w in
      let v = Loc_id_Set.choose buck_par_w in
      tasgn bucket_tbl par_w (Loc_id_Set.remove v buck_par_w);
      let u = eval_dom v in
      if tget sdno_tbl u < tget sdno_tbl v then
        tasgn idom_tbl v u
      else
        tasgn idom_tbl v par_w;
    done;
    i_ref := !i_ref - 1;
  done;
  for i = 2 to !n_ref do
    let w = tget ndfs_tbl i in
    let idom_w = tget idom_tbl w in
    if (idom_w <> tget ndfs_tbl (tget sdno_tbl w)) then
      tasgn idom_tbl w (tget idom_tbl idom_w);
  done
  (* end domin_fast *)
  
let get_idom l = 
  try Hashtbl.find idom_tbl l 
  with Not_found ->
    begin
      let loc = lookup_location_from_loc_coords l in
      Message.msg_string Message.Normal ("Compute Dominator for: "^(get_location_fname loc));
      let r = get_dom_root l in
      (* TBD: what if l "cannot reach exit" *)
      assert (not (Hashtbl.mem idom_tbl r));
      domin_fast r;
      try Hashtbl.find idom_tbl l with _ -> failwith "DOM: error !"
    end

    (* TBD: what if l or l' cannot reach exit ? *)
let rec dom l l' = 
  Message.msg_string Message.Debug 
  (Printf.sprintf "[DOM]: dom (%d,%d) (%d,%d)" (fst l) (snd l) (fst l') (snd l'));
  if (l = l') then true 
  else 
    let idom_l' = get_idom l' in
    if idom_l' = l' then false (* root *) else dom l idom_l'




(***********************************************************************************************************)
(******************************* General Fixpoint Computation **********************************************)
(***********************************************************************************************************) 

(* Given:
   Domain D, Range R, Relation --> < D x Sigma x D, Predicate > : R x R -> bool,
   a function \cup : R \times R -> R
   We want to compute the lfp of F: D x D -> R given using systems of equations like:
   1. F(u,v) > base(u,v)
   2. F(u,v) > f[(u-->u',F(u',v));...]

   For us, dom = location (ids) of a function,
   --> the CFA edges,
   so we take as parameters, cup, gt (>), base, f
   preds: D -> (D x Sigma) list
   succs: D -> (Sigma x D) list
*)


      
  let preds v = (* preds v = { u | u --> v } *)
    let v_loc = lookup_location_from_loc_coords v in
      List.map (fun e -> (location_coords (get_source e))) (get_ingoing_edges v_loc)

  let succs v = (* preds v = { (l,w) |  v -l-> w } *)
    let v_loc = lookup_location_from_loc_coords v in
      List.map (fun e -> (e,location_coords (get_target e))) (get_outgoing_edges v_loc)


let print_loc_list loc_list =
  Message.msg_string Message.Debug
    (Misc.strList
       (List.map (fun u -> (Printf.sprintf " (%d,%d) " (fst u) (snd u)))
	  loc_list))
  
  
   (* experimental for now just make the SCC and dump it*)
  let scc_table = Hashtbl.create 101 (* (string,loc_id list list) table *)
  let get_scc loc_id =
    let (fname,start_loc) = 
      let loc =  lookup_location_from_loc_coords loc_id in
	(get_location_fname loc, location_coords (get_location_entry_location loc))
    in
      try
	Hashtbl.find scc_table fname
      with Not_found ->
	let node_table = Hashtbl.create 31 in
	let rec _proc loc_id' =
	  if Hashtbl.mem node_table loc_id' then
	    Hashtbl.find node_table loc_id'
	  else
	    let v' : ((int * int),unit) BiDirectionalLabeledGraph.node  
	      = BiDirectionalLabeledGraph.create_root loc_id'
	    in
	    let _ = Hashtbl.replace node_table loc_id' v' in
	      List.iter
		( fun v'' -> BiDirectionalLabeledGraph.hookup_parent_child v' v'' ())
		(List.map _proc (List.map snd (succs loc_id')));
	      v'
	in
	let v_root = _proc start_loc in
	let sccs = 
	  let _sccs = List.map
			(List.map BiDirectionalLabeledGraph.get_node_label)
			(BiDirectionalLabeledGraph.scc v_root)
	  in
	    List.map (List.sort compare) _sccs
	in
	  Hashtbl.replace scc_table fname sccs; 
	  Message.msg_string Message.Normal
	    ("SCC size: "^(string_of_int (List.length sccs)));
	  List.iter print_loc_list sccs;
	  sccs
	    
  
	    
  let is_sink_scc scc =
    let scc_post = List.map snd (List.flatten (List.map succs scc)) in
      Misc.sorted_subsetOf (List.sort compare scc_post)  (List.sort compare scc)
				       
	    
let sink_table = Hashtbl.create 101 (* (string, int list) Hashtbl.t *)
let sink_scc_table = Hashtbl.create 101 (* (loc_id,loc_id list) Hashtbl.t *)

(* sinks v = one element from each sink SCC of v's CFA *)
let sinks v = 
  let print_sinks l = 
    Misc.strList (List.map (fun (x,y) -> (Printf.sprintf "(%d,%d)" x y)) l)
  in
  let v_loc = lookup_location_from_loc_coords v in
  let v_fname = get_location_fname v_loc in
  let _ = Message.msg_string Message.Debug 
  (Printf.sprintf "Find sinks of (%d,%d)" (fst v) (snd v))
  in
  try
    let rv =   Hashtbl.find sink_table v_fname in
        ignore(print_sinks rv);
        rv
    with Not_found ->
      let sccs = get_scc v in
      let rv =
	List.flatten
	  (List.map
	     (fun scc ->
		if is_sink_scc scc then
		  let _rv = List.hd scc in
		    Hashtbl.replace sink_scc_table _rv scc;
		    [_rv]
		else [])
	     sccs)
      in
	Hashtbl.replace sink_table v_fname rv;
	Message.msg_string Message.Debug
	  (Printf.sprintf "Sinks of (%d,%d) are: " (fst v) (snd v));
        ignore (print_sinks rv);
	rv
		  
(* seed v = S, s.t. pre*(S) contains all u s.t. fixpoint (u,v) <> base(u,v) *)
	  
  let make_fixpoint seed cup gt base f range_to_string =
    let ac_value_table : ((int * int) * (int * int) , 'a) Hashtbl.t = Hashtbl.create 1009 in
    let propagate_from v =
      let _ = Message.msg_string Message.Debug
		(Printf.sprintf "Calling propagate_from (%d, %d)" (fst v) (snd v))
      in
      (* computes R(u,v) for all u, that reach v *)
      let v_table : ((int * int) ,'a) Hashtbl.t  = Hashtbl.create 1009 in
	(* iterate till fixpoint *)
      let lookup u =
	try Hashtbl.find v_table u with Not_found -> base u v
      in
      let worklist = ref (seed v) in (* TBD:THIS REALLY NEEDS A PROOF OF CORRECTNESS !*)
	(* initialize 
	   let _ = List.iter (fun u -> Hashtbl.replace v_table (base u v)) (!worklist) in *)
      Message.msg_apply Message.Debug 
	(List.iter
	  (fun (x,y) -> Message.msg_string Message.Debug (Printf.sprintf "sink: (%d,%d)" x y)))
	  !worklist ;
      let _ = 
	while !worklist <> [] do
	  Ast.check_time_out () ;
	  let u = List.hd !worklist in
	  let _ = worklist := List.tl !worklist in
	  let args_u = List.map (fun (e,u') -> (e,lookup u')) (succs u) in
	  let old_r = lookup u in
	  let new_r = cup (f (u,v) args_u) old_r in
	    (* check if there is a strict increase -- there's no possibility of decrease *)
	    if gt new_r old_r then
	      begin
		Hashtbl.replace v_table u new_r;
		worklist := !worklist @ (preds u) 
	      end
	done
      in
      let _iter u r_uv =
	(* Message.msg_string Message.Debug
	  (Printf.sprintf "Add to FP: (%d,%d), (%d,%d) |-> %s"
	     (fst u) (snd u) (fst v) (snd v) (range_to_string r_uv));*)
	Hashtbl.replace ac_value_table (u,v) r_uv
      in
      Hashtbl.replace ac_value_table (v,v) (base v v);
      Hashtbl.iter _iter v_table (* will overwrite the default value if its different *)
    in (* end propagate_from *)
    let getval u v =
      try Hashtbl.find ac_value_table (u,v) 
      with Not_found -> base u v (* failwith "Error in make_fixpoint!" *)
	(* TBD: PROVE that this is correct ! *)
    in
    let get_fixpoint u v =
      if not (Hashtbl.mem ac_value_table (v,v)) then
	propagate_from v;
      getval u v
    in
    get_fixpoint 
	
(***********************************************************************************************************)
(*********************** A few functions for TRACE PROJECTION **********************************************)
(***********************************************************************************************************) 

 		      
  let gt l1 l2 =
    (List.length l1) > (List.length l2)

    
  let always_reach_id =
    let range_to_string l =
      if l = [] then "true" else Misc.strList (List.map string_of_bool l)
    in
    let base = fun x y -> [] (* if x <> y then [true] else [] *) in 
    let f =
      fun (u,v) e_r_list ->
	Message.msg_string Message.Debug "called f";
	 (* Message.msg_string Message.Debug (Printf.sprintf "r_uv = %b" (r_uv <> [false])) *)
        let myiter (e,r_e) =
	  Message.msg_string Message.Debug
	    (Printf.sprintf "%s : %b"
	       (location_to_string (get_target e))
	       (r_e <> [false]))
	in
	Message.msg_apply Message.Debug (List.iter myiter) e_r_list ;
	let false_cond_1 =
	  if (List.mem u (sinks u)) then
	    not (try
		   List.mem v (Hashtbl.find sink_scc_table u)
		 with Not_found -> true)
	  else false
	in
	let false_cond_2 = (List.exists (fun (_,r_u'v) -> r_u'v = [false]) e_r_list)
	in
	  if (( u <> v) && (false_cond_1 || false_cond_2))
	  then
	    begin
	      Message.msg_string Message.Debug
		(Printf.sprintf "NOT always reach (%d,%d) --> (%d,%d)" 
		   (fst u) (snd u) (fst v) (snd v));
	      [false]
	    end
	  else []
    in
    make_fixpoint sinks (Misc.union) gt base f range_to_string
	   


    
  let always_reach loc1 loc2 =
    let _ = Message.msg_string Message.Normal "Calling always_reach" in
    let l1 = location_coords loc1 in
    let l2 = location_coords loc2 in
    if (Options.getValueOfBool "dom") then dom l2 l1 
    else (always_reach_id l1 l2 = [])
    

  let mods_on_path_id =
    let range_to_string l = (* "" *)
         Misc.strList (List.map Expression.lvalToString l)
  in
  let junk_lv = Expression.Symbol "__BLAST_junk_lv" in  (* INV:this is NEVER read in any program *)
  let e_writes e = junk_lv :: (get_direct_writes e) in 
  let base = fun x y -> [] in
  let f =
    let stitch uv (e,e_list) =
      if (e_list <> [] || (snd uv = location_coords (get_target e)))
      then List.fold_right (fun lv el -> Misc.cons_if lv el) (e_writes e) e_list
      else []
      in
	fun uv e_r_list ->
	  if (fst uv) = (snd uv) then []
	    (* alternately, treat all out edges of "v" as non-existent, i.e. take writes along all
	       paths to v, not using an outedge of v. Actually, this latter approach FAILS
	       as you would simply end up eschewing the writes on the outedges but keeping everything else.
	       The present approach ensures that mods(v,v) = emptyset, so you don't add mods along paths
	       between v and v ...
	    *)
	  else  Misc.compact (List.flatten (List.map (stitch uv) e_r_list))
    in 
      make_fixpoint preds (Misc.union) gt base f range_to_string

let mods_on_path loc1 loc2 =
    let _ = Message.msg_string Message.Normal "Calling mods_on_path" in
  mods_on_path_id (location_coords loc1) (location_coords loc2) 


(* the above is just too slow. Suppose you are given a function f : edge -> bool,
   now, given loc1, loc2 qn is: exists a path loc1 ... loc --e--> loc' ... loc2 where f e is true 
   this could even be in BiDir... -- but but but -- unhappily CFA is not a BIDIR .. *)

let is_relevant_path f loc1 loc2 =
  let loc_cmp l1 l2 =  compare l1.CFA.loc_id l2.CFA.loc_id in
  (* let edge_cmp e1 e2 =compare (get_edge_coords e1) (get_edge_coords e2) in *)
  let f' e = if f e then Some (get_edge_coords e) else None in
  let edges sloc dir =
    let e_list = ref [] in
    let loc_tbl = Hashtbl.create 101 in
    let rec adder loc = 
      let loc_id = (loc.CFA.loc_id) in
      if Hashtbl.mem loc_tbl (loc_id) then ()
      else
	begin
	  Hashtbl.replace loc_tbl loc_id true;
	  let (t_list,te_list) = (dir loc) in
          let te_list_filtered = Misc.map_partial f' te_list in
          let _ = e_list := te_list_filtered @ !e_list in
	    List.iter adder t_list
	end
    in
      adder sloc;
      (* List.sort compare *) !e_list
  in
  let edges_into_loc2 =
    let pre loc' = 
      let te_list = get_ingoing_edges loc' in
      let t_list = List.map get_source te_list in
	(t_list,te_list)
    in
      edges loc2 pre
  in
  let edges_from_loc1 = 
    let post loc' = 
      let te_list = get_outgoing_edges loc' in
      let t_list = List.map get_target te_list in
	(t_list,te_list)
    in
      edges loc1 post
  in
    Misc.nonempty_intersect compare edges_from_loc1 edges_into_loc2


(* ASSUME that lv_list is closed under sub_lvals *)    
let may_be_modified lv_list loc1 loc2 = 
  Message.msg_string Message.Normal "In may_be_modified";
  let f e = 
    match call_on_edge e with
        Some (fn,_) ->
        let e_id = get_edge_coords e in
        let scc_id = this_fname_scc fn in
        let checker lv = 
          (Hashtbl.mem calledge_lv_effect_table (e_id,lv) ||
           Hashtbl.mem global_lv_effect_table (scc_id,lv))
        in
            List.exists checker lv_list
    |   None ->
        let dw = get_direct_writes e in
        Misc.nonempty_intersect compare  lv_list dw 
  in (* TBD:ALIAS. NOT ENTIRELY precise. we DONT take aliases *)  
        is_relevant_path f loc1 loc2

(*
  let mods_on_path_table : ((int * int) * (int * int), 'a) Hashtbl.t = Hashtbl.create 1009
  let mods_on_path loc1 loc2 =
    let loc1_id = location_coords loc1 in
    let loc2_id = location_coords loc2 in
      try Hashtbl.find mods_on_path_table (loc1_id,loc2_id)
      with Not_found ->
	begin
	  let elist = edges_on_path loc1_id loc2_id in
	  let mods = List.flatten
		       (List.map (fun e -> (get_command e).C_Command.written) elist )
	  in
	    Hashtbl.add mods_on_path_table (loc1_id,loc2_id) mods;
	    mods 
	end
*)
	
(* BEGIN COMMENT 
	 (*** Fixpoints over CFA: to compute R \subseteq V \times V. R is a hashtable. ***)
    let loc_pair_fixpoint base_rel f r_table =
    let intra_cfa_fixpoint r_table fname =
    let vertices = map_nodes_fname (fun x -> x) fname in 
       let n = List.length vertices in
       let _ = Message.msg_string Message.Normal ("CFA for "^fname^" size: "^(string_of_int n))
       in
       let allpairs = Misc.cross_product vertices vertices in
       let rlookup key =
       try Hashtbl.find r_table (location_coords (fst key), location_coords (snd key))
       with Not_found -> base_rel key
	 (*
       let rv = base_rel key in
       Hashtbl.add r_table key rv;
    rv *)
    in let _iter (u,v) =
	 (* let _ = Message.msg_string Message.Debug
   (Printf.sprintf "ITER proc %s %s"
    (location_to_string u) (location_to_string v))
    in *)
    let edges = get_outgoing_edges u in
    let arg = List.map
    (fun e -> (e,rlookup (get_target e,v)))
    edges
    in
    List.iter
 	    (Misc.hashtbl_check_update r_table (location_coords u,location_coords v))
    (f (u,v) (rlookup (u,v)) arg)
    in
    for i = 0 to n + 1 do  
    List.iter _iter allpairs
    done
    in
    List.iter (intra_cfa_fixpoint r_table) (list_of_functions () )  
    
    (* now, we use the above function to compute
    1. always_reach : loc -> loc -> bool 	
    2. mods_on_path : loc -> loc -> lval_list
    For each function, design the base_rel, and the fixpoint function f,
    and invoke loc_pair_fixpoint on it *)
    let always_reach_computed_flag = ref false
     let always_reach_table : ((int * int) * (int * int) , 'a) Hashtbl.t  = Hashtbl.create 1009
   

 	(* 1. always_reach: loc -> loc -> bool  *) 
    let build_always_reach () =
 	(* this is a greatest fixpoint. nu R. (I \/ (R(u,v) =>forall u -> u'.R(u',v)). *)
    assert (!always_reach_computed_flag = false);
    let _ = Message.msg_string Message.Debug "Building Always Reach Table" in
    let base_rel = fun (x,y) -> [] (* if x <> y then [true] else [] *) in 
    let f =
    fun (u,v) r_uv e_r_list ->
    let _ = Message.msg_string Message.Debug "called f" in 
    let _ = Message.msg_string Message.Debug (Printf.sprintf "r_uv = %b" (r_uv <> [false])) in
         (*
 	  let myiter (e,r_e) =
    Message.msg_string Message.Debug
    (Printf.sprintf "%s : %b"
    (location_to_string (get_target e))
    (r_e <> [false]))
    in
    List.iter myiter e_r_list
    in *)
    if ((location_coords u <> location_coords v) && (e_r_list = []
    || (List.exists (fun (_,r_u'v) -> r_u'v = [false]) e_r_list)))
    then
    begin
 	(* Message.msg_string Message.Debug
    (Printf.sprintf "NOT always reach %s --> %s" 
    (location_to_string u) (location_to_string v));*)
    [false]
    end
    else []
     in
    let _ = Message.msg_string Message.Debug "Calling loc_pair_fixpoint " in
    loc_pair_fixpoint base_rel f always_reach_table;
    let _ = Message.msg_string Message.Debug "Done with loc_pair_fixpoint " in
    always_reach_computed_flag := true
    
    let always_reach loc1 loc2 =
    assert (!always_reach_computed_flag = true);
    let rv = 
    try
    (Hashtbl.find always_reach_table (location_coords loc1, location_coords loc2) <> [false])
    with
    Not_found -> true
    in
    Message.msg_string Message.Debug
    (Printf.sprintf "Always Reach %s -> %s = %b"
    (location_to_string loc1) (location_to_string loc2) rv);
    rv
 
    
    (***********************************************************)
   (* 2. mods_on_path: loc -> loc -> lval list *)	  
    let mods_on_path_computed_flag = ref false
    let mods_on_path_table : ((int * int) * (int * int), 'a) Hashtbl.t = Hashtbl.create 1009      
 
   let build_mods_on_path () =
     assert (!mods_on_path_computed_flag = false);
    let base_rel = fun (x,y) -> [] in
     let f =
       let stitch uv (e,e_list) =
	 if (e_list <> [] || (location_coords (fst uv), location_coords (snd uv)) = (location_coords (get_source e), location_coords (get_target e)))
	then Misc.cons_if e e_list
	  else []
        in
 	fun uv r_uv e_r_list ->
 	  Misc.union r_uv (List.flatten (List.map (stitch uv) e_r_list))
     in
     let edges_on_path_table = Hashtbl.create 1009 in
     let _ = loc_pair_fixpoint base_rel f edges_on_path_table in
     let _iter k elist = 
       let mods = List.flatten
 		   (List.map (fun e -> (get_command e).C_Command.written) elist )
       in
 	if mods <> [] then Hashtbl.add mods_on_path_table k mods
     in
       Hashtbl.iter _iter edges_on_path_table;
       mods_on_path_computed_flag := true
 	
   let mods_on_path loc1 loc2 =
     assert (!mods_on_path_computed_flag = true);
     try
       Hashtbl.find mods_on_path_table (location_coords loc1,location_coords loc2) 
     with
 	Not_found -> []
 	  (* assert: this should never happen!
 	     as this function is only called when loc1 has a path to loc2 *)

END COMMENT *)



  (* given a function thisfun, and its cfa,
   build_dummy_cfa builds a cfa for __BLAST_DUMMY_thisfn
   that calls all functions called in thiscfa in arbitrary order
  *)
let build_dummy_fundef thisfun =
  let procname = "__BLAST_DUMMY_" ^ thisfun in
  if (Hashtbl.mem global_function_table thisfun) then
    begin
      let thisfundef = Hashtbl.find global_function_table thisfun in 
	(* assume that this function
	   has already been added to the global_function table! *)
      let get_call_edges thisfun =
       let callcode = ref [] in
       let proc_edge e =
         let code = (get_command e) in
         match code.Command.code with
           Command.FunctionCall (e') ->
             callcode := code :: !callcode
         | _ -> ()
       in
       ignore (map_edges_fname proc_edge thisfun) ;
       !callcode
     in
     let add_fn cmd startlocation automaton =
       let fn_start = cfa_create_location procname automaton Artificial in
       let fn_end = cfa_create_location procname automaton Artificial in
       CFA.add_edge startlocation fn_start C_Command.skip ; (* replace this by havocing all formals *)
       CFA.add_edge fn_start fn_end cmd ;
       CFA.add_edge fn_end startlocation C_Command.skip ;
       ()
     in
     let label_table = Hashtbl.create 1 in
     let automaton = CFA.create_automaton { label_table = label_table } in
     let start_loc = cfa_create_location procname automaton Artificial in
     let end_loc = cfa_create_location procname automaton Artificial in
     CFA.set_start_location automaton start_loc ;
     List.iter (fun fcode -> add_fn fcode start_loc automaton ) (get_call_edges thisfun) ;
     CFA.add_edge start_loc end_loc 
       (C_Command.gather_accesses (C_Command.Block [ C_Command.Return (Expression.Constant (Constant.Int 0)) ]));
     { (* return the created fundef *)
        function_name    = procname ;
        formals          = thisfundef.formals ;
        locals           = thisfundef.locals ;
        cfa              = automaton ;
        fun_src_pos      = thisfundef.fun_src_pos;
        fun_attributes   = thisfundef.fun_attributes ; 
     }
   end
 else (* this function is undefined! *)
   failwith ("build_dummy_fundef: Undefined function "^thisfun)

(* Build the dummy fundefs for each function and populate the dummy function table *)
let build_dummy_fundefs () =
  List.iter (fun f -> let f_fundef = build_dummy_fundef f in Hashtbl.add dummy_function_table f f_fundef ) (list_of_functions () )

(*********************************************************************************************)
(* Function call depth restriction helpers                                                   *)
(*********************************************************************************************)

let functions_of_labels = function [] -> [] | labels -> begin
  let _ = Message.msg_string Message.Debug (Printf.sprintf "Important labels: %s" (String.concat ", " labels)) in
  (* Get all locations the labels point to *)
  let label_locs = List.flatten (List.map get_locations_at_label labels) in
  (* Map the locations to the containing functions *)
  let funcs = Misc.compact (List.map get_location_fname label_locs) in
  let _ = Message.msg_string Message.Debug (Printf.sprintf "Functions with labels: %s" (String.concat ", " funcs)) in
  funcs
end

let functions_of_attrs = function [] -> [] | attrs -> begin
  (* Check if function has one of the attrs *)
  let check_function _ function_def =
    List.fold_left (fun imp attr -> imp || (Cil.hasAttribute attr function_def.fun_attributes)) false attrs
  in
  (* Get all functions that have one of the attributes *)
  let with_attrs = Hashtbl.fold (fun fname fdef accum -> if check_function fname fdef then fname::accum else accum) global_function_table [] in
  let _ = Message.msg_string Message.Debug (Printf.sprintf "Functions with attributes %s: %s" (String.concat ", " attrs) (String.concat ", " with_attrs)) in
  with_attrs
end

(* Closes the set (list) of functions given by callers *)
module FnMap = Map.Make(struct type t = string let compare = compare end)
let close_callers funcs =
  (* First, compute call graph, unless done *)
  if (not !callgraph_built_flag) then compute_callgraph ();
  (* Close the set of functions under "is called by" *)
  let rec more_callers map = function
    | [] -> FnMap.fold (fun k _ accum -> k::accum) map []
    | fn :: more -> if FnMap.mem fn map
      then more_callers map more
      else let callers =  List.map snd (calls_made_into fn) in
	let _ = Message.msg_string Message.Debug (Printf.sprintf "Callers of %s: %s" (fn) (String.concat ", " callers)) in
	more_callers (FnMap.add fn true map) (callers@more)
  in
  more_callers (FnMap.empty) funcs

(* Create a list of important functions, those that contain important labels in their indirect call graph *)
let make_important_functions () =
  let important_functions = close_callers ((functions_of_labels (Options.getValueOfStringList "important-labels")) @ (functions_of_attrs (Options.getValueOfStringList "important-attrs"))) in
  let inline_functions = functions_of_attrs (Options.getValueOfStringList "inline-attrs") in
  let important_functions = inline_functions@important_functions in
  let _ = Message.msg_string Message.Normal (Printf.sprintf "Important functions (%d): %s" (List.length important_functions) (String.concat ", " important_functions)) in
  (* Add the result to the hashtable for it to be accessed later *)
  List.iter (fun fn -> Hashtbl.add important_function_table fn true) important_functions
(* Query the list *)
let is_important fname = Hashtbl.mem important_function_table fname



(*******************************)
(* Initializes CIL representation of all files *)
let initialize_cil c_files =
  let _ = Message.msg_string Message.Debug "Initializing CIL" in
  (* Initialization is done in several stages: get raw cil, modify control flow if necessary, and perform local fixups *)

  (* During the first stage we also gather global information about the whole program *)
  let (raw_cil_files,info) = VampyreStats.time "make_cil_file" (List.fold_left BlastCilInterface.make_cil_file ([],None)) c_files in
  let raw_cil_files = List.rev raw_cil_files in

  (* Change control flow: generate new functions, insert calls to them *)

  let _ = Message.msg_string Message.Debug "Fixing CIL control flow" in
  (* Reroute calls and generate reroute-specific functions *)
  let reflow_cils = raw_cil_files in
  let reflow_cils = List.map (BlastCilInterface.reroute_calls info) reflow_cils in
  (* call initializers from main *)
  let reflow_cils = VampyreStats.time "initializers" (List.map BlastCilInterface.call_initializers) reflow_cils in

  (* Fixups *)
  let _ = Message.msg_string Message.Debug "Final CIL fixups" in
  let fixed_cils = reflow_cils in
  let fixed_cils = List.map BlastCilInterface.fixup_cil fixed_cils in
  fixed_cils

let initialize_blast cil_files =
  let _ = Message.msg_string Message.Debug "Initializing BLAST" in
  Simplify.splitStructs := false;
  global_cil_files := (Some cil_files) ;
  (* then build the CFAs -- the global_cfa_table *)
  let _ = 
    try
      List.iter process_cil_file cil_files;
    with
      DuplicateFunctionName fn_name ->
        failwith ("C_System_Descr.initialize: duplicate function name ("
                  ^ fn_name ^ "): unimplemented")
  in
  ignore(make_important_functions ());
  ignore(output_call_paths ());
  ignore(fill_loop_head_table ());
  ()
    
let initialize c_files =
  let cil_files = initialize_cil c_files in
  initialize_blast cil_files

(* TBD:FPTR. Re-build callgraph with fptr info *)
(* this function is called by main or somesuch (whoever calls initialize_blast)
   AFTER the alias analysis is done, in which case, it will have a function
   called scoped_aliases_fun: string -> lval -> lval list
   where scoped_aliases_fun fname lv returns all the lvals "visible" to fname,
   that can be aliased to lv
*)

let post_alias_analysis scoped_aliases_fun () =
  (* hmm. At this point,  take a pass over the lval tables and "close" under post *)
  let close_lval_table () =
    let clos = ref [] in
    let proc lv =
      let lv_clos' = Misc.list_remove (lvalue_closure lv) lv in
	clos := lv_clos'@ (!clos)
    in
      iterate_all_lvals proc;
      List.iter (fun lv' -> add_lval lv' None) !clos
  in
    (* RJ: hmm. this seems to take a LOOONG while..  *)
       let _ = close_lval_table () in 
  
    (* "mod" is set by default when one does "cf" *)
    if (Options.getValueOfBool "mod") then compute_modifies scoped_aliases_fun ; 
    (*else failwith "boo"; *)
    (* the mod info should be computed BEFORE the symbolic hooks are added *)
    if (Options.getValueOfBool "cf" (* && not (Options.getValueOfBool "incmod")*)) then
      add_symbolic_hooks ();
      Message.msg_string Message.Normal "Done post-alias-analysis"
	

let is_alloc fn =
  List.mem fn ["alloc"; "malloc"; "calloc"; "realloc"; "kmalloc" ]
  ||
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "alloc" function_def.fun_attributes
  with Not_found -> false

let is_dealloc fn =
  List.mem fn ["free" ]
  ||
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "free" function_def.fun_attributes
  with Not_found -> false

let is_interface fn =
  try 
   let function_def = Hashtbl.find global_function_table fn in
   Cil.hasAttribute "public" function_def.fun_attributes
  with Not_found -> false
    
let build_automaton interface_fns =
  let add_fn cmd startlocation automaton =
    let fn_start = cfa_create_location "__BLAST_interface" automaton Artificial in
    let fn_end = cfa_create_location "__BLAST_interface" automaton Artificial in
    CFA.add_edge startlocation fn_start C_Command.skip ; (* replace this by havocing all formals *)
    CFA.add_edge fn_start fn_end cmd ;
    CFA.add_edge fn_end startlocation C_Command.skip ;
    ()
  in
  let label_table = Hashtbl.create 3 in
  let automaton = CFA.create_automaton { label_table = label_table } in
	
  let start_loc = cfa_create_location "__BLAST_interface" automaton Artificial in
  CFA.set_start_location automaton start_loc ;
  let automaton_location = (* automaton location for interface automaton *)
	cfa_create_location "__BLAST_interface" automaton Artificial in
  (* hook up edges in automaton *)
  CFA.add_edge start_loc automaton_location 
	(if is_defined "__BLAST_initialize" then
		(C_Command.gather_accesses 
		(C_Command.FunctionCall (Expression.FunctionCall (Expression.Lval (Expression.Symbol "__BLAST_initialize"), []))) )
	else C_Command.skip) ;
  List.iter (fun fname ->
    let formals = match (lookup_formals fname) with
      Variable l | Fixed l -> l
    in
    let cmd = C_Command.gather_accesses
      (C_Command.FunctionCall 
	 (Expression.FunctionCall 
	    (Expression.Lval (Expression.Symbol fname), 
	     List.map (fun s -> Expression.Lval (Expression.Symbol s)) formals))) in
    add_fn cmd automaton_location automaton ) 
    interface_fns ;
  (start_loc, automaton_location)

(***********************************************************************************)
(* Reaching definitions *)
(* Which definitions of an lvalue (given by edges) reach a certain location        *) 
(* This will be used to merge information in the symbolic store lattice            *)
(***********************************************************************************)

(* The final product *)
let location_to_lval_table : ((int * int), Expression.lval list) Hashtbl.t = Hashtbl.create 101

(** The interface to the reaching analysis.

   Input: a location of the CFA.

   Output: if the location is a join point, then
           a list of all lvals which have multiple definitions that reach this location.
           Otherwise []
*)
let get_all_multiply_defined_lvals l =
  try
	Hashtbl.find location_to_lval_table (location_coords l)
  with Not_found -> []

(* The analysis *)
let compute_reaching_definitions () =
  Message.msg_string Message.Normal "In compute reaching definitions" ;
  let loc_table = Hashtbl.create 101 in (* loc_table maps location's to
				           a hashtable of (lvalue -> location list)
					*)
  let clean_up_loc_table loc_table = (* clean up after the analysis to populate the location_to_lval_table *)
    let _cleanup loc locht =
      if not (is_join (lookup_location_from_loc_coords loc)) then () 
      else 
        begin
          let lval_list = ref [] in
          let _iter lv llist = if List.length llist > 1 then lval_list := lv :: !lval_list 
          in
          Hashtbl.iter _iter locht ;
          if not (!lval_list = []) then Hashtbl.add location_to_lval_table loc !lval_list ;
	  Hashtbl.remove loc_table loc
        end
    in 
    Hashtbl.iter (_cleanup) loc_table ;
    Hashtbl.iter (fun k d -> Message.msg_string Message.Normal (Printf.sprintf "Loc: (%d,%d) -> %s" (fst k) (snd k) (Misc.strList (List.map Expression.lvalToString d)) )) 
	location_to_lval_table
                              
  in
  let gt_hashtbl big small =
     Message.msg_string Message.Normal "In gt_hashtbl" ;
    (try 
      (Hashtbl.iter (fun k d -> ignore (Hashtbl.find big k)) small) ;
    with Not_found -> failwith "The bigger hash table is not really big!" );
    let hashtbl_length ht = (* TBD: USE Hashtbl.length in OCAML 3.08 *)
      Hashtbl.fold (fun k d sofar -> sofar + 1) ht 0
    in
    if (hashtbl_length big > hashtbl_length small) then
       true
    else false
  in
  (* Do an intraprocedural analysis for each function. *)
  (* Start at the head of a function and compute gen sets till fixpoint. *)
  let analyze f =
    Message.msg_string Message.Normal ("Analyzing function "^f) ;
    let compute_gen cmd : Expression.lval list =
      let do_block b =
        if ( (List.exists (function x -> match x with Command.Return e -> true | _ -> false) b)) then []
        else 
            List.fold_left (fun sofar thisassn -> 
              match thisassn with
                Command.Expr (Expression.Assignment (_, target, _)) -> 
                  target :: sofar
	      | _ -> failwith ("Strange statement in compute_gen :")
            ) [] b
      in
      let do_funcall fcexp = (* TBD: Add the lvalues modified by the called function *)
        let fname = get_fname fcexp in
        let modified_lvals = (global_lvals_modified fname ) @ (formal_lvals_modified fname) in
        (match fcexp with
        | Expression.Assignment (_, target, _) ->  target :: modified_lvals
        | Expression.FunctionCall _ ->  modified_lvals 
	| _ -> failwith "modified_lvals: strange function"
        )   
      in
      let code = cmd.Command.code in      
      match code with
        Command.Block b ->
          do_block b
      | Command.Skip -> []
      | Command.FunctionCall fcexp ->
          do_funcall fcexp
      | Command.Pred _ -> []
      | _ -> failwith "compute_gen: Not handled."
    in    
    let transfer (inset: (Expression.lval, edge list) Hashtbl.t) edge =
      let genset : Expression.lval list = compute_gen (get_command edge) in
      let outset = Hashtbl.copy inset in
      List.iter (fun l -> Hashtbl.replace outset l [edge]) genset ;
      outset 
    in
    (* dataflow analysis *)
    let worklist = ref [ lookup_entry_location f ] in (* worklist is a list of locations *)
    while not (!worklist  = []) do
  	Message.msg_string Message.Normal ("One more iteration ") ;
	
      let changed = ref false in
      let join outset cumul =
        Hashtbl.iter (fun key data -> if Hashtbl.mem cumul key then Hashtbl.replace cumul key (Misc.union data (Hashtbl.find cumul key))
  	else Hashtbl.add cumul key data
  	 ) outset ;
        cumul
      in
      let thislocation = List.hd !worklist in
      worklist := List.tl !worklist ;
      let thiscoords = location_coords (thislocation : location)in	
      Message.msg_string Message.Normal 
        (Printf.sprintf "The current node is (%d,%d)" (fst thiscoords) (snd thiscoords)) ;
      let reaching_defs = List.fold_left 
	(fun sofar thisedge -> 
         let inset = try Hashtbl.find loc_table (location_coords (get_source thisedge)) 
                     with Not_found -> Hashtbl.create 7
         in
         join (transfer inset thisedge) sofar)
	(Hashtbl.create 11) (get_ingoing_edges thislocation) in
  	Message.msg_string Message.Normal ("Found reaching defs! ") ;
      (try 
        let old_reaching = Hashtbl.find loc_table thiscoords in
        if gt_hashtbl reaching_defs old_reaching then
          begin
            Hashtbl.replace loc_table thiscoords reaching_defs ;
            changed := true ;
          end
        (* if this location has changed then add all its successors to the worklist *)
      with Not_found -> Hashtbl.add loc_table thiscoords reaching_defs ; changed := true) ;
      if (!changed) then
	worklist := !worklist @ (List.map get_target (get_outgoing_edges thislocation) )  ;
    done ;
  	Message.msg_string Message.Normal ("Cleaning up ") ;
    clean_up_loc_table loc_table ;
    ()
  in 
  (* the main code *)
  List.iter analyze (list_of_functions ()) ;
  ()
  
end
