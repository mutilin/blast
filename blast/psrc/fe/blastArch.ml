(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)



(**
 * This module defines the main signatures for Blast.
 *)


(* DUMMY -- "unit" for functors *)
module type MYUNIT = 
  sig
    val myunit : unit
  end
(**
 * The PRINTABLE signature specifies a type equipped with a print function
 * that allows to pretty print values of this type.
 *)
module type PRINTABLE =
sig
  (* the type *)
  type t

  (** the pretty-printer for this type *)
  val print : Format.formatter -> t -> unit

  (** the string converter for this type *)
  val to_string : t -> string
end


(**
 * The COMMAND signature specifies a set of commands.  These commands
 * typically label the edges of a System description.
 *)
module type COMMAND =
sig
  include PRINTABLE
end

(**
 * The SYSTEM_DESCRIPTION signature specifies how an input system shall be
 * described.  This description is an automaton-like representation suitable
 * for model checking.
 * 
 *)
module type SYSTEM_DESCRIPTION =
sig
  module Command : COMMAND

  (** the type of (control) locations *)
  type location

  (** the type of edges (i.e. control transitions) *)
  type edge

  (** to use to hash edges, its (int*int)*(int*int) *)
  type edge_id_t = ((int * int) * (int * int))
  (** a command that does nothing *)
  val skip : Command.t

  
  (** a location, used to build regions where only the data part is relevant *)
  val fake_location : location

  (** a location, used to build regions where only the data part is relevant *)
  val fake_edge : edge (* fake_location, skip, fake_location *)

  (** get the source position for a location *)
  val get_source_position : location -> (string * int * int) option

  (** get the locations for a source position *)
  val get_position_sources : string * int -> location list

  (** the pretty-printer for locations *)
  val print_location : Format.formatter -> location -> unit

  (** the pretty-printer for edges *)
  val print_edge     : Format.formatter -> edge     -> unit
(*  val print_code     : Format.formatter -> Command.t -> unit *)

  (** the string converter for locations *)
  val location_to_string : location -> string

  val call_depth : location -> int
  val location_coords : location -> int * int

  (** the string converter for edges *)
  (** TBD: This is gratuitous, replace with get_command! *)
  val edge_to_string : edge -> string

  val get_edge_coords : edge -> edge_id_t
 
  val make_edge_coords : location -> location -> edge_id_t

  (** Extracting an edge's associated command *)
  val edge_to_command : edge -> Command.t

  
  (** gives the list of edges originating from a given location *)
  val get_outgoing_edges : location -> edge list

  (** gives the list of edges pointing to a given location *)
  val get_ingoing_edges : location -> edge list

  (** gives the source of an edge *)
  val get_source  : edge -> location

  (** gives the target of an edge *)
  val get_target  : edge -> location

  (** gives the command labeling an edge *)
  val get_command : edge -> Command.t

  (** Create a unique name for the edge *)
  val get_edge_name : edge -> string
    
  val reads_and_writes_at_edge : edge -> Ast.Expression.lval list * Ast.Expression.lval list

  (** make_edge src target cmd : make a new edge with command cmd between src and target,
      but don't add this to the CFA. *)
  val make_edge : location -> location -> Command.t -> edge

  

  (** for an expression x returns *x, **x etc etc ... for now only goes down a depth of 2 *)
  val stats_nb_cached_exp_closure : int ref
  val stats_nb_recomp_exp_closure : int ref

 (** the first argument is true if its to ALL, false for IMAGE *) 
  val add_field_to_unroll : bool -> string -> bool 
 (* was it added (true) or already there (false) *)
 
  val expression_closure : Ast.Expression.expression -> Ast.Expression.expression list
 (** again -- first arg is true if for ALL e.g. trace-project-refine etc. Second for image *)   
  val expression_closure_stamp :  bool -> Ast.Expression.expression -> Ast.Expression.expression list
    (* hide for now:
       val expression_closure_cond : (string -> bool) -> Ast.Expression.expression -> Ast.Expression.expression list *)    

  (** see expression_closure *)
  val lvalue_closure : Ast.Expression.lval ->  Ast.Expression.lval list
  val lvalue_closure_stamp : bool -> Ast.Expression.lval ->  Ast.Expression.lval list
  (* hide for now val lvalue_closure_cond :   (string -> bool) -> Ast.Expression.lval ->  Ast.Expression.lval list *)

  (** incremental closure *)
  type closure_state
  val empty_closurer : closure_state
  val print_closurer : Format.formatter -> closure_state -> unit
  val add_expr_to_closurer : closure_state -> Ast.Expression.expression -> closure_state
  val query_expr_closure : closure_state -> Ast.Expression.expression -> Ast.Expression.expression list


  (** Call a function on every global variable (noit including tid and
      notTid) *)
  val iter_global_variables : (Ast.Expression.lval -> unit) -> unit

  val iterate_all_lvals : (Ast.Expression.lval -> unit) -> unit

  val iterate_all_scope_lvals : string -> (Ast.Expression.lval -> unit) -> unit

  (* Must-aliases utils. *)

  val withdraw_musts : (Ast.Expression.lval, 'a) Hashtbl.t -> unit

  val iterate_all_but_musts_lvals : (Ast.Expression.lval -> unit) -> unit
    
  (** Functions that return the Cil type of an lvalue.
	These are quite fragile. Use at your own risk.
  *)
  
  (** return the type of an lvalue *)
  val get_type_of_lval : Ast.Expression.lval -> Cil.typ

  (** return the type of an expression.
      Right now this only deals with Lvals and Address-es
      *)
  val get_type_of_exp : Ast.Expression.expression -> Cil.typ


  val is_struct : Ast.Expression.lval -> bool
  val is_array : Ast.Expression.lval -> bool
  val is_ptr : Ast.Expression.lval -> bool
  val is_skolem : Ast.Expression.lval -> bool
  
  (** return true if the function name corresponds to an allocation function *)
  val is_alloc : string -> bool
  (** return true if the function name corresponds to a deallocation function *)
  val is_dealloc : string -> bool

  (** return true if the function should not be expanded by Blast *) 
  val is_noexpand : string  -> bool 

  (** return true if the function does not return -- hence no summaries created *) 
  val is_noreturn : string  -> bool 

  (** returns true if the symbol is in the table of global variables *)
  val is_global : Ast.Expression.lval -> bool

  (** returns true if the lvalue can escape. Uses the alias analyzer *)
  val can_escape : Ast.Expression.lval -> bool

  (** returns true if the string is the name of a lock variable *)
  val is_a_lock : Ast.Expression.lval -> bool

  (** returns true if the symbol is a local for the specified function *)
  val is_in_scope : string -> Ast.Expression.lval -> bool

  val globals_of_type : Cil.typ -> Ast.Expression.lval list

(** returns the possible "scopes" of an lval -- should be a single string.
    "" indicates global scope *)
val scope_of_lval : Ast.Expression.lval -> string list
  
    
  (** A variable that contains the id of the running thread. *)
  val tid : string

  (** A variable that is always different from tid *)
  val notTid : string

  (** A variable that refers to a generic memory location *)
  val self : string

  (** gives the entry location of a function *)
  (* Greg: strings for function calls: to change? *)
  exception NoSuchFunctionException of string
  
  (** returns true if the function is defined, false otherwise *)
  val is_defined : string -> bool

  (** always_reach loc1 loc2 returns true iff loc1,loc2 are two locations in the same CFA,
    s.t. all acyclic paths from loc1 (eventually) go through loc2 *)
  val always_reach : location -> location -> bool

  (** mods_on_path loc1 loc2 returns the set of lvals written to along all paths from loc1 to loc2.
    again, the two locations need to be in the same CFA (or you get the empty set) *)
  val mods_on_path : location -> location -> Ast.Expression.lval list

  val may_be_modified : Ast.Expression.lval list -> location -> location -> bool
  val get_fname : Ast.Expression.expression -> string
  val get_name_of_call : Ast.Expression.expression -> string
  val local_mod_on_edge : edge -> Ast.Expression.lval -> bool
  val global_mod_on_call : string -> Ast.Expression.lval -> bool

  (** returns the list of global lvals that can be modified by a function *)
  val global_lvals_modified : string -> Ast.Expression.lval list

  (** returns the list of formal lvals that can be modified by a function *)
  val formal_lvals_modified : string -> Ast.Expression.lval list

  val global_lvals_mod_by : Ast.Expression.lval -> string list

    (* the list of funs that may mod an lv, via copyback effects *)
  val may_mod_by : Ast.Expression.lval -> string list
    
  (** find the entry location for a function *)
  val lookup_entry_location : string -> location

  val is_call_on_edge : edge -> bool
  
  val call_on_edge : edge -> (Ast.Symbol.symbol * Ast.Expression.expression) option
 (** Given some location, return what function it belongs to *)
  val get_location_fname : location -> string
  
 (** Given some fname, returns all the locations for it *)
 val lookup_all_locations : string -> location list

 (** Get the "id" of fname *)
  val get_fname_id : string -> int

  (** return list of functions *)
  val list_of_functions : unit -> string list

  exception FunctionPointerException 

  (** Return target used by deconstructFunCall when function is
      void *)
  val junkRet : Ast.Expression.lval

  (** Name for irrelevant functions *)
  val __SKIPFunctionName : string

  val __NotImplementedFunctionName : string

  val __BLAST_DummyFunctionName : string

  val __BLAST_DispatchFunctionName : string
  (** parse a function expression, returning a triple of
      (function name, (formal name, formal value expr) list, result target lval) *)
  val deconstructFunCall : Ast.Expression.expression ->
       Ast.Symbol.symbol * (string * Ast.Expression.expression) list *
         Ast.Expression.lval

  (** return the location, given the location coordinates *)
  val lookup_location_from_loc_coords : int * int -> location

  (** is a given location a join location ? actually are there 
  multiple edges _into_ that location *)
  val is_join : location -> bool

  (** is a given location l on a loop -- i.e. is l on a cycle in its CFA ?*)
  val is_loopback : location -> bool
  
  (* various boolean utilities *)
  val isReturn : edge -> bool
  
  val returnExp : edge -> Ast.Expression.expression
  
  val isBlock : edge -> bool
  
  val isPredicate : edge -> bool

  val isFunCall : edge -> bool
  
  val isDefinedFunCall : edge -> bool
  (* utilities for async/events *)
 
  val isEventHandler_fname : string -> bool

  val is_dispatch_loc : location -> bool
    
  val isAsyncCall : edge -> bool
  
  val isAsyncExec : edge -> bool
  
  val isDispatchCall : edge -> bool
    
  val acAsyncCall : edge -> edge 
  
  val enter_call : edge -> bool

  val replace_code_in_cfa : edge -> Ast.Expression.t -> unit

  (* val instrument_CFA : unit -> unit *)

  (** Type of formal arguments to a function. *)
  type formals_kind =
      Fixed of string list
    | Variable of string list (** variable arguments *)

  (** gives the locals of a function *)
  val lookup_locals : string -> string list

  (** returns ALL the lvals that may be smashed by fname -- and are in scope of fname *)
  val lookup_local_lvals : string -> Ast.Expression.lval list
	
  (** gives the formals of a function *)
  val lookup_formals : string -> formals_kind

  (** gives the list of global variables *)
  val globals : unit -> string list

	
  (** Used in the parallel model checker. Looks at a location and says whether
      there can be a context switch at that location. *)
  val is_atomic : location -> bool

  val is_event : location -> bool

  val is_task : location -> bool

  val is_spec : location -> bool


  (** the locations corresponding to a label name in the C source *)
  val get_locations_at_label : string -> location list

  (** map_edges f applies f to all the edges in the program *)
  val map_edges : (edge -> 'a) -> 'a list

  (** map_edges f fname applies f to all the edges in the function fname *)
  val map_edges_fname : (edge -> 'a) -> string -> 'a list

  (** call graph related functions *)
  val compute_callgraph : unit -> unit
	
  (* a call node describes the local calling structure for a
 * single function: which functions it calls, and which
 * functions call it *)
(* COMMENT OUT
  type callnode = {
  (* the function this node describes *)
  cnInfo: string;

  (* set of functions this one calls *)
  cnCallees: (string, callnode * int) Hashtbl.t;

  (* set of functions that call this one *)
  cnCallers: (string, callnode * int) Hashtbl.t;
}

(* a call graph is a hashtable, mapping a function name to
 * the node which describes that function's call structure *)
type callgraph =
  (string, callnode) Hashtbl.t



  val get_callgraph : unit -> callgraph
  val get_callers : string -> (string, callnode * int) Hashtbl.t
  val get_callees : string -> (string, callnode * int) Hashtbl.t

  (* update_callgraph caller callee *)
  val update_callgraph : string -> string -> unit
  (* output call graph to file in dot format *)
*)


  val calls_made_by : string -> (Ast.Expression.expression * string) list	
  val calls_made_into : string -> ((edge_id_t *Ast.Expression.expression) * string) list	

  val output_callgraph : string (* filename *) -> unit
  (* val print_callgraph : out_channel -> unit *)
 
  val get_reachable_functions : unit -> string list (* a subset of list_of_functions 
						       that are reachable in the call graph *)

  (** what functions can fname transitively call *)
  val caller_closure : string list -> string list 

  (** what functions transitively call fname *)
  val backwards_caller_closure : string list -> string list
  val bounded_backwards_caller_closure : int -> string list -> string list
  val shortest_path : string -> string ->  string list
  val output_call_paths : unit -> unit
  val scc_caller_closure : string -> string list

(******* Enable the passing of the "error"/"spec" predicate between main and the
modelchecker -- this is a single pred now, but really one should know what all
the spec lvals are and seed skipfun with those *)

val set_error_lvals : Ast.Expression.lval list -> unit
val get_error_lvals : unit -> Ast.Expression.lval list

  
  (**** Reaching definitions *****)

  (** Return a list of lvals which have multiple paths to the specified location *)
  val get_all_multiply_defined_lvals : location -> Ast.Expression.lval list
  (** Do the analysis for reaching definitions *)
  val compute_reaching_definitions : unit -> unit

  (***** Fdepth constraints *****)
  val is_important : string -> bool
end


(**
 * The OPERATION signature specifies a set of operation.  Operations are used
 * in Abstractions whereas Commands are used in System Descriptions.  In
 * pratice, Operations can be e.g. edges of a System Description.
 *
 * @see 'blastCSystemDescr.ml' for operations for C programs
 *)
module type OPERATION =
sig
  include PRINTABLE

  type exp
  type lval

  type info =
    | Normal
    | Call of Ast.Predicate.predicate * lval option * string * exp list
    | Ret of exp

  val get_info : t -> info
  val paren_fun : t -> string
    
end


(**
 * The REGION signature specifies a printable type used to denote sets,
 * equipped with equality, inclusion, (approximate) union, (approximate)
 * intersection, a bottom element for the empty set, and a function to check
 * for emptiness.
 *
 * Regions are an abstract description of sets of states. 
 * We do not make a distinction between "abstract regions" and "concrete
 * regions" at this level, but see {!BlastArch.ABSTRACTION}
 * @see <http://www.eecs.berkeley.edu/~rupak/Papers/a_classification_of_sts.ps>
 *       to see a discussion on regions in model checking
 *)
module type REGION =
sig
  include PRINTABLE

  val log_region : Message.logmsg_level -> Message.logmsg_type -> t -> unit

  (** the equivalence relation corresponding to set equality *)
  val eq : t -> t -> bool

  (** the quasi-ordering corresponding to set inclusion *)
  val leq : t -> t -> bool

  (** the (approximate) union operation *)
  val cup : t -> t -> t

  (** check that the region is Union *)
  val check_union : t -> unit

  (* merge in style of cpa *)
  val cpa_merge_reached: t -> t -> string -> t * t
 
  (** the (approximate) intersection operation *)
  val cap : t -> t -> t

  (** the region representing the emptyset *)
  val bot : t

  (** it is a function because hashtable should be created each time *)
  (** the region representing the emptyset for reached_region*)
  val reached_bot : unit -> t

  (** tests whether a region denotes an empty set *)
  val is_empty : t -> bool

  (** make a region concrete *)
  val concretize : t -> t

  val rename_vars : t -> (Ast.Expression.lval -> Ast.Expression.lval) -> t
  val change_location_and_stack : t -> t -> t
  val call_depth : t -> int
  val get_location_id : t -> (int * int)
  val get_ctrmap : t -> Ast.Counter.counter array

  (* coverers store data structures for quick lookup of a region which covers another *)
  type 'a coverers
  val emptyCoverers : unit -> 'a coverers
  val clearCoverers : 'a coverers -> unit
  val addCoverer : 'a coverers -> t -> 'a -> unit
  val deleteCoverer : 'a coverers -> t -> 'a -> unit
  val deleteCoverers : 'a coverers -> (t * 'a) list -> unit
  (* val rebuildCoverer : 'a coverers -> unit *)
  val findCoverer : 'a coverers -> t -> 'a option
  val findCoverUnion : 'a coverers -> t -> t
  val findExactCoverer : 'a coverers -> t -> 'a option
  val findAllCoverer : 'a coverers -> t -> 'a list  
  val findAllExactCoverer : 'a coverers -> t -> 'a list
  val findCoverCandidates : 'a coverers -> t -> 'a list
  val printCoverer : 'a coverers -> unit

  val project_par_atomic : t -> t


  (* Restricting regions to contain only certain kinds of predicates *)
  type restriction
  val negate_restriction : restriction -> restriction
  val locals_and_globals : string -> restriction
  val only_globals : restriction
  val only_locals : string -> restriction
  val restrict : restriction -> t -> t

  type abstract_data_region

  type data_region (* needed for foci_model_checker *)

  (** get a ``true'' abstract region *)
  val true_adr : abstract_data_region
  (** get a ``false'' abstract region *)
  val false_adr : abstract_data_region
  
  val get_adr : t -> abstract_data_region
  
  (** returns a string from an adr *)
  val abstract_data_region_to_string : abstract_data_region -> string

  val string_of_region_for_dot : t -> string

  val adr_eq : abstract_data_region -> abstract_data_region -> bool


  val adr_leq : abstract_data_region -> abstract_data_region -> bool
  val adr_is_empty : abstract_data_region -> bool
  val adr_cubes : abstract_data_region -> int

  val cartesian_adr : abstract_data_region -> abstract_data_region

  val adr_cap : abstract_data_region -> abstract_data_region -> abstract_data_region
  val adr_cup : abstract_data_region -> abstract_data_region -> abstract_data_region

  (** used to update the lattice part of the region after a join *)
  val update_lattice_in_region : t -> t -> t
  val update_lattice_in_region_coverers_and_delta : t -> 'a coverers -> (t*t)
  val update_lattice_in_region_coverers : t -> 'a coverers -> t
  val union_to_list : t -> t list

end

module type ACCESS_REGION = sig
  module Region : REGION

  type 'a lock_predicate = {
      local_lp_region : Region.t;
      lp_region : Region.t;
      lp_node : 'a;
    }

  (* The type of access regions (\phi's for race condition detection).
     Each component of an access region has a ``node'' information of type
     ['a]. *)
  type 'a t

  (* The empty access region *)
  val empty : 'a t

  (* [add_region n r phi interfering]
     Add a region [r] to the access region [phi]. The node for [r] is [n].
     The region [r] must have been stripped of its location component for
     this function to make sense when analysing a multi-threaded program.
     Return [New_region phi'] if [r] does not interfere with [phi], i.e., 
     if [interfering r ri] is false for every [ri] in that has previously
     been added to [phi]. Otherwise return [Error_at lp] where [lp] contains
     the region and node for the component of [phi] that interferes with
     [r]. *)
  type 'a add_region_outcome =
    | New_region of 'a t
    | Error_at of 'a lock_predicate * Region.t

  val add_region : 'a -> Region.t -> Region.t -> 'a t ->
                   (Region.t -> Region.t -> Region.t option) ->
                   'a add_region_outcome

  val concrete : 'a t -> Region.t

  val to_string : 'a t -> string
end


(**
   Module that implements a complete lattice (with top and bottom elements)
   *)

(**
  Lattice Query Interface
  =======================
  
  The query interface enables lattices to exchange predicates with each other
  and with the abstraction layer (which maintains the predicate abstraction
  state). Here is how the layers fit together:

                       ----------------------------
                       |                          |
                       |    Abstraction Layer     |
                       |                          |
                       ----------------------------
                           |      /|\        | 
                          1|       |        7|
                           |      4|         |
                          \|/      |        \|/
                       ----------------------------
                       |                          |
                       |      Union Lattice       |
                       |                          |
                       ----------------------------
                        |  /|\  |     |    |   |
                       2|  3|  8|    5|   6|  9|
                       \|/  |  \|/   \|/  \|/ \|/
                       -----------   --------------         ---------
                       |         |   |            |         |       |
                       |  Set    |   |  SymExe    |  . . .  |       |
                       | Lattice |   |  Lattice   |         |       |
                       -----------   --------------         ---------

  The query interface is called during a post operation. Here's how it works:
  Step(s)     What Happens
  =======     ==================================================
  1           The abstraction layer calls post on the union lattice. It passes
              in a current lattice element, a command, and a query function.
              The query function, when called, executes the against the
              predicate abstraction state.

  2, 6        The union lattice then executes post on each of the sub-lattices.
              For each sub-lattice, it passes in a different query function -
              each closure includes the identity of the lattice it is passed
              to. This is used by the query function to avoid calling back to
              the same lattice.

  3, 4, 5     During the post function, a lattice can make a call to the
              query function that was passed in. This query will then execute
              queries against the abstraction layer and each of the other
              lattices. A query can return 0, 1, or Top. A contradiction is
              an error. Otherwise, the query returns the most precise value
              returned. Note that queries are executed against the OLD version
              of each lattice, not the new one being computed.

  2, 6, 1     Each lattice's post function returns a new lattice element and
              a predicate. When the union lattice returns from post, it returns
              an array of lattice elements and the conjunction of the
              sub-lattice predictes. Any predicates which are just TRUE may be
              dropped from the conjunction.

  7, 8, 9     The abstraction layer creates an assume command from the returned
              predicate using C_SD.command.pred. This command is first executed
              against the predicate state. The abstraction layer then calls
              the post of the union lattice with the assume command. The union
              lattice, in turn, calls the post of each sublattice.

  The lattice elements and predicate states from the second post are the
  final result used to create the new region.


  Interface Changes
  -----------------
  blastArch.ml will include the following new definitions:
   type lattice_qry_result = Top | True | False
   type lattice_qry_fn =  Ast.predicate -> lattice_qry_result

  The post functions of the set, symbolic execution, and union lattices will
  be changed to have the following signature:
   post: lattice -> edge -> lattice_qry_fun -> (lattice, Ast.prediate)

  Each lattice must also implement a function called "query" which is of
   type lattice_qry_fn.
  

  Design Decisions
  ----------------
  1. When the post function of a lattice is called with an assume command, it
     must not return any new predicates. This ensures termination of our overall
     post.

  2. A query is executed against the old version of each lattice element. This
     is consistent with the semantics of the union lattice -- each sub-lattice
     is an independent representation of the program state. A post operation
     is executed "in parallel" against all sub-lattices.
**)


  
module type LATTICE =
  sig
    module C_SD : SYSTEM_DESCRIPTION
    type lattice
    type edge
    val top : lattice (** No information known *)
    val bottom : lattice (** Empty region -- generally means that state is not reachable *)
    val init : lattice (** initial lattice element *)
    val join : lattice -> lattice -> lattice (** Join is used to take the union of two regions *)
    val join_and_delta : lattice -> lattice -> (lattice*lattice) (** Join is used to take the union of two regions *)
    val meet : lattice -> lattice -> lattice (** Meet is used to take the interection of two regions *)
    val leq : lattice -> lattice -> bool (** Check for containment of regions *)
    val eq : lattice -> lattice -> bool (** Check for equality of regions *)
    val focus : unit -> unit (* for the moment it is unit -> unit *)
    val is_consistent : lattice -> CaddieBdd.bdd -> bool (** Is lattice consistent with predicates? *)
    val post : lattice -> edge -> (Ast.Predicate.predicate -> Ast.Predicate.predicate) ->
                                                                          (Ast.Predicate.predicate list * lattice)
                    (** Compute the lattice which is result after applying computation in edge to initial lattice
		        Also return the predicates that you might have discovered to the mother code *)
    val summary_post : lattice -> lattice -> edge -> edge -> lattice
    val pre : lattice -> edge -> lattice  (** Not currently used *)
    val print : Format.formatter -> lattice -> unit (** Pretty printer for this lattice *)
    (** Called after source code has been initialized *)
    val initialize : unit -> unit
    val query_fn : lattice -> Ast.Predicate.predicate -> Ast.predicateVal
    val enabled_ops : lattice -> C_SD.Command.t list
    val toString : lattice -> string 
  end


  
(* TBD: HIDE most of this stuff!! *)
module type PREDBDD = 
  sig
  module C_SD : SYSTEM_DESCRIPTION
    (* val bddManager : CaddieBdd.ddMgr *)
      val block_a_t_flag : bool ref
      val kill_predbdd : unit -> unit 
      val bddZero : CaddieBdd.bdd
      val bddOne : CaddieBdd.bdd
      val make_varMap : (int * int) list -> CaddieBdd.varMap 
      val bdd_support : CaddieBdd.bdd -> int list
      val blast_bddForeachCube : CaddieBdd.bdd -> (int array -> unit) -> unit
      val bdd_implies : CaddieBdd.bdd -> CaddieBdd.bdd -> bool
      val bdd_cube_cover : CaddieBdd.bdd -> CaddieBdd.bdd
      val bdd_cubes : CaddieBdd.bdd -> int
      val lits_of_cube : CaddieBdd.bdd -> (int * bool) list
      val bdd_cube_literals : CaddieBdd.bdd -> CaddieBdd.bdd list
      val bdd_cartesian_decomposition : CaddieBdd.bdd -> CaddieBdd.bdd list
      val maxPossiblePreds : int
      val tableOfPreds : (int * Ast.Predicate.predicate * CaddieBdd.bdd) list ref
      val hashTableOfPreds : (Ast.Predicate.predicate, int) Hashtbl.t
      val arrayOfPreds : (Ast.Predicate.predicate * CaddieBdd.bdd) array
      val arrayOfCStatePreds : int array
      val arrayOfNStatePreds : int array
      val listOfConstants : string list ref
      val predIndex : int ref
      val getNextIndex : unit -> int
      val getCurrentIndex : unit -> int
      val resetIndex : unit -> unit
      val printTableOfPreds : unit -> unit
      val getPred : int -> Ast.Predicate.predicate
      val getPredIndex :Ast.Predicate.predicate -> int
      val getPredBdd : Ast.Predicate.predicate -> CaddieBdd.bdd
      val getPredIndexBdd : int -> CaddieBdd.bdd
      val getNIndexFromCIndex : int -> int option
      val getCIndexFromNIndex : int -> int option
      val isCurrentIndex : int -> bool
      val isNextIndex : int -> bool
      val getPredIndexNextBdd : int -> CaddieBdd.bdd
      val transrel_bdd_table :
        ((int * int) * (int * int), CaddieBdd.bdd) Hashtbl.t
      val global_invariant_bdd_ref : CaddieBdd.bdd ref
      val get_transrel_bdd : (int * int) * (int * int) -> CaddieBdd.bdd
      val get_eq_transrel_bdd : (int * int) * (int * int) -> CaddieBdd.bdd
      val bddVarMap_list : (int * int) list ref
      val curr_var_map : CaddieBdd.varMap ref
      val curr_var_cube : CaddieBdd.bdd ref
      val next_var_map : CaddieBdd.varMap ref
      val next_var_cube : CaddieBdd.bdd ref
      val update_bdd_info : int * int -> unit
      val make_unprimed_bdd : CaddieBdd.bdd -> CaddieBdd.bdd
      val make_primed_bdd : CaddieBdd.bdd -> CaddieBdd.bdd
      val bdd_postimage : CaddieBdd.bdd -> CaddieBdd.bdd -> CaddieBdd.bdd
      val bdd_preimage : CaddieBdd.bdd -> CaddieBdd.bdd -> CaddieBdd.bdd
      val bdd_exist_quantify : CaddieBdd.bdd -> int list -> CaddieBdd.bdd
      val make_eq_bdd : int list -> CaddieBdd.bdd
      val make_impl_bdd : int list -> CaddieBdd.bdd
      val add_new_pred_to_table : Ast.Predicate.predicate -> int
      val get_all_pred_indexes : unit -> int list
      val extract_full_cube : CaddieBdd.bdd -> CaddieBdd.bdd
      val convertPredToBdd_map :
        (Ast.Predicate.predicate -> CaddieBdd.bdd) ->
        Ast.Predicate.predicate -> CaddieBdd.bdd
      val convertPredToBdd : Ast.Predicate.predicate -> CaddieBdd.bdd
      val ac_convertCubeToPred :
        (int -> Ast.Predicate.predicate) -> int array -> Ast.Predicate.predicate
      val convertCubeToPred : int array -> Ast.Predicate.predicate
      val ac_convertBddToPred :
        (int -> Ast.Predicate.predicate) -> CaddieBdd.bdd -> Ast.Predicate.predicate
      val convertBddToPred : CaddieBdd.bdd -> Ast.Predicate.predicate
      val bdd_to_string : CaddieBdd.bdd -> string
      val bdd_to_string_pretty : CaddieBdd.bdd -> string
      val update_transrel_bdd : (int * int) * (int * int) -> CaddieBdd.bdd -> unit
      val update_eq_transrel_bdd : (int * int) * (int * int) -> int list -> unit
      val update_global_inv_bdd : CaddieBdd.bdd -> unit
      val bdd_node_count : CaddieBdd.bdd -> int
      val bdd_top_var : CaddieBdd.bdd -> int
      val bdd_hash : CaddieBdd.bdd -> int * int
      val string_to_atom : Ast.Symbol.symbol -> Ast.Predicate.predicate
      class ['a] bdd_hash :
        object
          val bdd_table : (int * int, (CaddieBdd.bdd * 'a) list) Hashtbl.t
          method add_new : CaddieBdd.bdd -> 'a -> unit
          method find : CaddieBdd.bdd -> 'a
        end
      val bdd_fold :
        (int -> 'a -> 'a -> 'a) -> (bool -> 'a) -> CaddieBdd.bdd -> 'a
      val bdd_tmp_var_idx : int ref
      val next_bdd_tmp_atom : unit -> Ast.Predicate.predicate
      val bdd_to_pred_map :
        (int -> Ast.Predicate.predicate) -> CaddieBdd.bdd -> Ast.Predicate.predicate
      val bdd_to_predicate : CaddieBdd.bdd -> Ast.Predicate.predicate
  end
  

module type PREDTABLE =
  sig
    include PREDBDD
    (* val post_split_table : (int * int, int list) Hashtbl.t
    val loc_pred_table : (int * int, int list) Hashtbl.t
    val pred_parity_table : (int, bool list) Hashtbl.t
    val fun_pred_table : (int, int list) Hashtbl.t *)
  
    val lookup_pred_parity : int -> bool list
    val add_pred_parity : int -> bool -> unit

    val keep_pred_parity : int * bool -> bool
    val refine_counter : int ref
val refined : unit -> unit
val last_refine_counter : int ref
val refine_check : unit -> bool
val globally_useful_preds : int list ref
val blast_error_lval : Ast.Expression.lval
val globally_important_lvals : Ast.Expression.lval list ref
val globally_important_lvals_table : (Ast.Expression.lval, bool) Hashtbl.t
val all_important_lvals_table : (Ast.Expression.lval, bool) Hashtbl.t
val __SKIPFunctionName : string
val __NotImplementedFunctionName : string
val always_take_fun_table : (string, bool) Hashtbl.t
val always_skip_fun_table : (string, bool) Hashtbl.t
val add_skip_fun : string -> unit
val delete_skip_fun : string -> unit
val stats_nb_take_funs : int ref
val add_take_fun : string -> unit
val always_take : string -> bool
val always_skip : string -> bool
val relevant_globals_mod_table : (string, Ast.Expression.lval list) Hashtbl.t
val relevant_fields_table : (string, bool) Hashtbl.t
val update_global_lvals : Ast.Expression.lval -> unit
val ignorable_globals : Ast.Expression.lval list
val add_relevant_fields : Ast.Expression.lval -> unit
val is_relevant_field_image : string -> bool
val update_imp_lv_table : Ast.Predicate.predicate -> unit
val ac_is_relevant_lval : Ast.Expression.lval -> bool
val is_relevant_lval : Ast.Expression.lval -> bool
val relevant_globals_mod : string -> Ast.Expression.lval list
val pred_affected_table : (C_SD.edge_id_t * int, bool) Hashtbl.t
val update_pred_affected_table : C_SD.edge_id_t -> int -> unit
val is_pred_affected : C_SD.edge_id_t -> int -> bool
val is_pred_global : int -> bool
val isConstant : Ast.Expression.expression -> bool
val lval_constant_table :
  (Ast.Expression.expression, Ast.Expression.expression list) Hashtbl.t
val make_const_table : unit -> unit
val getConstants : Ast.Predicate.predicate -> Ast.Expression.expression list
val constant_preds : Ast.Predicate.predicate -> Ast.Predicate.predicate list
val actual_addPred : Ast.Predicate.predicate -> int
val addPred : Ast.Predicate.predicate -> int
val just_addPred : Ast.Predicate.predicate -> int
val add_pred_to_scope : int -> int option
val getPredsContainingVar : Ast.Symbol.symbol -> Ast.Predicate.predicate list
val theoremProverCache : (string , bool) Hashtbl.t
val statTotalNumberOfQueries : int ref
val statCoveredQueries : int ref
val statTotalCached : int ref
val statPostBddCalls : int ref
val statWorstCaseQueriesForPost : int ref
val statActualQueriesForPost : int ref
val statNonDCQueriesForPost : int ref
val statAssumeQueriesForPost : int ref
val statNumPost : int ref
val statNumAssumePost : int ref
val statReachedCubes : int ref
val statVisitedFunctionTable : (string, bool) Hashtbl.t
val reset_abs_stats : unit -> unit
exception SatisfiableException
val isSatisfiable : CaddieBdd.bdd * Ast.Predicate.predicate list -> bool
val askTheoremProver : Ast.Predicate.predicate -> bool
val askTheoremProverContext :
  Ast.Predicate.predicate -> Ast.Predicate.predicate -> bool
val bat_counter : int ref
val refined_fun_table : (string, bool) Hashtbl.t
val cfb_local_refine_table : (string, bool) Hashtbl.t
val block_a_t_reset : unit -> unit
val is_fun_refined : string -> bool
val add_local_preds : int * int -> int list -> unit
val lookup_location_predicates : int * int -> int list
val print_pred_vector : unit -> unit
val ctrbddManager : CaddieBdd.ddMgr
val ctrbddZero : CaddieBdd.bdd
val ctrbddOne : CaddieBdd.bdd
val freeBddVars : CaddieBdd.bdd list ref
val getBddVar : unit -> CaddieBdd.bdd
val bddMap : (int * CaddieBdd.bdd list) list ref
val allocateCtrBdds : int list -> int -> unit
val destroyCtrBdds : unit -> unit
val getBdd : int -> Ast.Counter.counter -> CaddieBdd.bdd
val ctrmap_to_bdd : Ast.Counter.counter array -> CaddieBdd.bdd
val ctrmap_check_covered : Ast.Counter.counter array -> CaddieBdd.bdd -> bool
val ctrmap_exists_stuck : CaddieBdd.bdd -> bool
type abstraction_info =
    AbsPred of Ast.Predicate.predicate
  | LocPredTuple of ((int * int) * int list)
  | SymLvalTuple of (string * (Ast.Expression.lval * Ast.Expression.lval) list)
val get_AbsPred : abstraction_info -> Ast.Predicate.predicate option
val get_LocPredTuple : abstraction_info -> ((int * int) * int list) option
val get_SymLvalTuple :
  abstraction_info ->
  (string * (Ast.Expression.lval * Ast.Expression.lval) list) option
val relevant_symvar_table : (string, Ast.Expression.lval list) Hashtbl.t
val add_relevant_symvar : string -> Ast.Expression.lval -> unit
val get_relevant_symvar : string -> Ast.Expression.lval list
val initialize_skipfun : unit -> unit
val dump_abs : unit -> unit
val load_abs : unit -> unit
  end



module type EVENTS =
  sig
    module C_SD : SYSTEM_DESCRIPTION
    type eventHandler 
    val eventHandler_to_string : eventHandler -> string
    val eventHandler_to_command : eventHandler -> C_SD.Command.t
    val deconstructAsyncCall : C_SD.edge -> (eventHandler * CaddieBdd.bdd)
    val constructAsyncCall : C_SD.edge * CaddieBdd.bdd -> C_SD.edge
  end
  
(**
 * The ABSTRACTION signature groups an operation module and a region module
 * and specifies abstract post and pre functions.  These two functions are
 * supposed to be conservative with respect to the precise semantics of the
 * operations.  It also specifies the precise and focus functions used for
 * refinement.
 *)
module type ABSTRACTION =
sig
  (** the operation module *)
  module Operation : OPERATION

  type opKind = ProgramOperation of Operation.t | AutomatonOperation of (int * int) 

  (** the region module *)
  module Region : REGION

  module EnvAutomaton :
    sig
      
      (* Invariants: Node 0 is always the stuck node *)      
      type onode_label =
	  {
	    oid : int;
	    oadr : Region.abstract_data_region;
	    oregion : Region.t

	  }

      type anode_label =
	  { id   : int ; (* the id of the abstract automaton node *)
	    root : int ; (* the id of the root node in the cluster *)
	    mutable adr  : Region.abstract_data_region ; (* the abstract data region (is this on globals?) *)
	    mutable atomic : bool; (* are ALL the onodes in this corresp to atomic locations ? *)
	    mutable task : bool;
	    mutable event : bool;
	    mutable modifies : Ast.Expression.lval list ; (* the list of globals that are modified by this transition *)
	  
	  }	
	  
      type aedge_label = 
	  Thread of (Ast.Expression.lval list)             (* Solid edge *) 
	| Env                 (* Dotted edge *)
	    
	    
      type oautomaton_node = (onode_label, opKind) BiDirectionalLabeledGraph.node

      type automaton_node = ((* node label *) anode_label, (* edge label *) aedge_label) BiDirectionalLabeledGraph.node 
	    
      type  ('a, 'b) automaton = {
          
	  autom_root : automaton_node ;
	  number_of_states : int ;
	  
	  id_to_node_ht : (int, automaton_node) Hashtbl.t ;
	            
          o_auto_root : oautomaton_node;
	  
	  cover_set : int Region.coverers;
	  (* other maps *)
	  o_map : (int, oautomaton_node) Hashtbl.t;  (* auto_map *)
	  o_to_a_map : (int, int) Hashtbl.t;         (* auto_shrink_map *)
          a_to_o_map : (int, int list) Hashtbl.t;    (* shrink_elts_map *)
 
(*          	  id_to_bdd : (CaddieBdd.bdd) array ; (* map from state id to bdd encoding *)
	  mutable stVarMap : (int * int) list ;     (* maps between the unprimed (s) and primed (t) *)
	  mutable tsVarMap : (int * int) list ;     (* state variables *)
	  
	  mutable env_transrel    : CaddieBdd.bdd ;
          (* Not required at the moment 
	     mutable thread_transrel : CaddieBdd.bdd ;
	     *)	  
	*)  
	} 
	    
      val getNodeFromId : ('a, 'b) automaton -> int -> automaton_node (* raises Not_found *)
	

      val getEnvEdges : ('a, 'b) automaton -> int -> (anode_label, aedge_label) BiDirectionalLabeledGraph.edge list 
      val getThreadEdges : ('a, 'b) automaton -> int -> (anode_label, aedge_label) BiDirectionalLabeledGraph.edge list

(*
      val encode_bdd_states : int -> CaddieBdd.bdd array * ( (int * int) list ) * ((int * int) list)



	    (* build the dotted transition relation as a BDD. assume that the BDD encoding
	       has been created already 
	       *)
      val build_env_transition_relation : ('a, 'b) automaton -> CaddieBdd.bdd

             i
      val constrain_env_transition_relation_by_region : ('a, 'b) automaton -> Region.abstract_data_region -> CaddieBdd.bdd
*)
    end


  (** check if some other thread is stuck -- used by extended TAR *)
  val exists_stuck : Region.t -> bool
  (** create alias database *) (* Code for David *)
  (* val create_alias_database : unit -> unit  *)

  (** creates a string representation of an (atomic) region -- used by gui *)
  val reg_to_strings : Region.t -> (string list * string )

  (** gets the name of the function for an atomic region *)
  val reg_function_name : Region.t -> string

  (** returns true if the region is in a function that NEVER returns, never becomes a summary *)
  val is_noreturn : Region.t -> bool

  (** gets a string deswcription of a region *)
  val reg_to_string : Region.t -> string

  (** creates a string from an operation *)
  val op_to_string : Operation.t -> (int * int * string)

(*  module AR : ACCESS_REGION with module Region = Region*)
   

val check_races : Ast.Expression.lval option ref
 
 val type_of_edge : opKind -> EnvAutomaton.aedge_label
 

  (** the table mapping variables names to phi regions *) 
  type phi_table = (Ast.Expression.lval, Region.t ref) Hashtbl.t 

  (** the symbolic post function: (approximate) strongest post-condition *)
  val post : Region.t -> Operation.t -> phi_table -> Region.t


  (** post for the product of a program and an automaton *)
  val tarpost : Region.t -> opKind -> ('a, 'b) EnvAutomaton.automaton -> 
    (Region.t * ((int*int*Ast.Counter.counter) list)) list
  
  (** this next thing is a measure to fight the simplify silently dying on us! *) 
  val reset_decision_procedures : unit -> unit 

  (** notify abstraction module of final outcome, for GUI *)
  val notify_result : string -> unit
  
  val create_parallel_atomic_region : Region.t option -> ('a,'b) EnvAutomaton.automaton -> Ast.Counter.counter -> Region.t

  (** the symbolic pre function: (approximate) weakest pre-condition *)
  val pre  : Region.t -> Operation.t -> phi_table -> Region.t

  (** [summary_post caller exit retop ] creates a region to be used after the function call whose
    * post-condition caller represents, where exit is the region of the used summary
    * node's exit
    *)
  val summary_post : Region.t -> Region.t -> Operation.t -> Operation.t -> Region.t


  (** this does pre only it makes syntactic substitutions explicit. This is required
    * in counterexample analysis
    *
    * @see "abstraction.ml" and the operator {!BlastArch.ABSTRACTION.focus}
    *)
  val spre : Region.t -> Operation.t -> phi_table -> Region.t    

  (** forget all data information in a region *)
  val erase_data : Region.t -> Region.t

  (** add the support predicates to the localization table *)
  val save_region_abs : Region.t -> unit
 
  val dump_abs : unit -> unit 
  (** make a region out of a predicate *)
  val predicate_to_region : Ast.Predicate.predicate -> Region.t

  (** create a concrete "true" region with location as the destination of the operation **)
  val create_concrete_true_region : Operation.t -> Region.t

  (** create an abstract "true" region with location as the destination of the operation **)
  val create_abstract_true_region : Operation.t -> Region.t


  (** initial "transition invariant" region *)
  val ti_init_region : unit -> Region.t list 

  (** print computed TI to sicstus prolog *)
  val dump_ti_closure : Region.t -> unit
  
  (** keep only the data part of a region, and concretize it *)
  val trim_region_to_globals : Region.t -> Region.t
  
  (** **)
  (* Why do we need to export these?
  val new_name : string -> Ast.Expression.lval -> Ast.Expression.lval
  val brand_new_suffix : unit -> string
  *)
  val assumed_invariants_region : Region.t ref
  (** the concretization function: take an abstract region and concretize it *)
  val precise  : Region.t -> Region.t

  (** Raised when no new predicates are found after counterexample analysis. (Blast fails) *)
  exception NoNewPredicatesException
  exception RecursionException  
  exception RefineFunctionException of string (* refine from all entries of fname *)
  
  (** the refinement operator *)
  val focus  : Region.t -> Region.t -> Region.t

    (** a dummy refinement operator that really checks if the supplied region is as precise as it can be *)
  val focus_stamp : Region.t -> Region.t 

  (** analyze a trace using uclid *)
  val analyze_trace : Region.t list -> Operation.t list -> unit

  val globally_important_lvals : unit -> Ast.Expression.lval list



  (** was a location belonging to fn refined in the last refine ? *)
  val is_fun_refined : string -> bool

  (** analyze a trace as a bunch of blocks foci *)
  val block_analyze_trace : Region.t list -> Operation.t list -> int list -> (int * Region.t * bool)
  val parallel_block_analyze_trace : ('a,'b) EnvAutomaton.automaton -> ((Region.t list) * (opKind * ((int * int * Ast.Counter.counter) list)) list) -> 
    EnvAutomaton.oautomaton_node list -> Ast.Counter.counter -> unit

    (** analyze all subtraces of a trace to find all possible predicates *)
  val find_all_predicates : Region.t list -> Operation.t list -> unit

    (** have we not discovered new predicates since the last time this function was called ? *)
  val predicate_fixpoint_reached : unit -> bool


  (** check if it is even worthwhile to check if the region is covered *)
  val coverable : Region.t -> bool

  (** the quasi-ordering corresponding to coverability, that is, basically,
     Region.leq modulo "safety" equivalence *)
  val covered : Region.t -> Region.t -> bool

  (** abstracts reg2 wrt preds of reg1 and then takes the intersection *)
  val intersect_with_abstraction : Region.t -> Region.t -> Region.t

  (** allows the printing of various statistics *)
  val print_abs_stats : unit -> unit

  val print_stats : Format.formatter -> unit -> unit
  (** resets statistics ... 
  val reset_abs_stats : unit -> unit
  *)

  (** gives the possibly enabled operations in a given region *)
  val enabled_ops  : ?prepare_alias:(unit -> unit) -> Region.t -> Operation.t list

  (** gives the possibly enabled operations (both program and automaton) in a given region *)
  val tar_enabled_ops  : ('a,'b) EnvAutomaton.automaton -> Region.t -> opKind list
  val get_modifies : ('a,'b) EnvAutomaton.automaton -> Region.t -> 
	(((Ast.Expression.lval list) * (Ast.Expression.lval list)) *((int * Ast.Expression.lval) list))

  val print_debug_info : Operation.t list -> unit


  (** returns the abstract_data_region ... required for building the automaton thing *)
  val extract_abs_data_reg : Region.t -> Region.abstract_data_region
  val replace_abs_data_reg : Region.t -> Region.abstract_data_region -> Region.t 

    
  (** this throws out local state as well *)
  val quantify_and_extract_abs_data_reg : Region.t -> Region.abstract_data_region 

  (** returns true if the operation is an assume predicate *)
  val isPredicate : Operation.t -> bool

  (** tells you whether or not to bother to check for emptiness now *)
  val check_emptiness : Operation.t -> bool

  val reset : unit -> unit
  (* prepare restart the analysis on the same program with possibly
      different havocs. *)
  val exit_abstraction : unit -> unit 

  (** checks if the lvalue can ever be aliased to a global variable. 
      Uses alias information from AliasAnalyzer
      *)
  val can_escape : Ast.Expression.lval -> bool

  val is_lock : Ast.Expression.lval -> bool
  val is_global : Ast.Expression.lval -> bool

  val is_atomic : Region.t -> bool
  val is_event : Region.t -> bool
  val is_task : Region.t -> bool
  val is_spec : Region.t -> bool

(** stuff for fociModelChecker-concrete *)
  type lvmap_t (* =  ((int * Ast.Expression.lval), Ast.Expression.expression) Hashtbl.t *)
  val new_lvmap_t : int -> lvmap_t

  val insert_data_reg : Region.t -> Region.data_region -> Region.t

val negate_region : Region.t -> Region.t
  val is_true_adr : Region.t -> bool
  val is_false_adr : Region.t -> bool


(* Needed by testmodelchecker *)  
  val extract_pred : Region.t -> Ast.Predicate.predicate
      (** {b Data Structures to represent Proofs} *)

  type proof_region
  type lemma_index
  val post_proof: Region.t -> Operation.t -> phi_table -> (proof_region * lemma_index)
  val check_reg_eq: proof_region -> Region.t -> bool
  val extract_proof_region: Region.t -> proof_region
  val print_proof_region : proof_region -> unit
  val print_proofTable : unit -> unit
      
  (** {b Fields related to race condition detection} *)

  (** Invoke a function on each global variable *)
  val iter_global_variables : (Ast.Expression.lval -> unit) -> unit

  (** Check whether an operation reads or writes to a certain variable *)
  type access_type = Anone | Aread | Awrite | Aboth
  
  (** checks how variable string is accessed in Operation.t *)
  val accessing : Operation.t -> Ast.Expression.lval -> access_type
  
  (** returns all strings that are written by this operation *)
  val get_all_writes : Operation.t -> Ast.Expression.lval list
  (** returns all strings that are read by this operation *)
  val get_all_reads : Operation.t -> Ast.Expression.lval list
  
  val reads_and_writes : Operation.t -> (Ast.Expression.lval list * Ast.Expression.lval list)
  val aliasesOf : Ast.Expression.lval -> Ast.Expression.lval list

  (** [multiply_instantiable r r'] is true iff $r[\th\gets\th_1] \wedge
      r'[\th\gets\th_2] \implies \th_1 = \th_2$. *)
  val multiply_instantiable : Region.t -> Region.t -> Ast.Expression.lval -> Ast.Expression.lval -> Region.t option
  val make_multiply_instantiable_region : Region.t -> Region.t -> Ast.Expression.lval -> Ast.Expression.lval -> Region.t

  val genTests : Region.t -> (Ast.Expression.expression * float) list


   (* Interface checking *)
   type spec
   type spec_result =
     | Yes
     | No
     | Maybe of Ast.Predicate.predicate

   (** Prepares to check specifications. Call once after all normal checking is complete. *)
   val init_spec : unit -> unit
   (** Builds a predicate that describes a function's observed input-output behavior,
    *  as shown by a list of function entry points paired with their observed exit points.
    *)
   val build_spec : string -> (Region.t * Region.t list) list -> spec
   (** Generates a predicate equivalent to the implementation of the second parameter spec
    *  by the first parameter spec. (i.e., first parameter => second parameter) *)
   val check_spec : spec -> spec -> spec_result

  val getCurrentPredIndex : unit -> int 
end

module ModelCheckOutcome =
struct
  (* Outcome type polymorphic in error_path type *)
  type 'a outcome =
    | No_error
    | Error_reached of 'a
    | Race_condition of 'a * 'a * Ast.Expression.lval * Ast.Expression.lval
    | Bad_lock_spec of 'a * Ast.Expression.lval
end

(**
 * The LAZY_MODEL_CHECKER specifies the functionality expected from the
 * Lazy Model Checker.
 *)
module type LAZY_MODEL_CHECKER =
sig
  (** the abstraction module used by the model checker *)
  module Abstraction : ABSTRACTION

  (** the type of error paths *)
  type error_path = (Abstraction.Region.t *
                     (Abstraction.Operation.t list) *
                     Abstraction.Region.t)

  type model_check_outcome = error_path ModelCheckOutcome.outcome

  (** the TAR algorithm implementation 
   * {[model_check init error]}
   * searches for a path from {i init} to {i error} in the
   * model and returns an error path or that the system is correct   
  *)
  val model_check_tar : Abstraction.Region.t  ->
                     Abstraction.Region.t option   ->
                     model_check_outcome

  (** the extended TAR algorithm implementation 
   * {[model_check init error]}
   * searches for a path from {i init} to {i error} in the
   * model and returns an error path or that the system is correct   
  *)
  val model_check_extar : Abstraction.Region.t  ->
                     Abstraction.Region.t option   ->
                     model_check_outcome

  (** the lazy model checking algorithm implementation 
   * {[model_check init error]}
   * searches for a path from {i init} to {i error} in the
   * model and returns an error path or that the system is correct   
  *)
  val model_check : ?max_depth:int ->
                     Abstraction.Region.t  ->
                     Abstraction.Region.t option   ->
                     model_check_outcome

end
