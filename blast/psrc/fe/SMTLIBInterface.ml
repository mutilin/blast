(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)


(* Objectives: replace the old simplify SMT solver by an interface for SMT solvers
 * using SMT-Lib format.
 *)

module M = Message

(************** System part: creating and stopping the process ***************)
let smt_solver: (in_channel * out_channel * in_channel) option ref = ref None

(*provide an interface between blast and the solver for solvers that
  cannot handle more than one query in the same execution*)
let solver_cmd = "smtlibServer"

let get_smt_solver () =
  match !smt_solver with
    | None -> 
        M.msg_string M.Normal "Forking SMT sovler process..." ;
        let (ic, oc, ec) = Unix.open_process_full solver_cmd (Unix.environment ()) in
          smt_solver := Some(ic, oc, ec) ;
          M.msg_string M.Normal "done!\n";
          (ic,oc)
    | Some(ic, oc, _) -> (ic, oc)
and stop_smt_solver() = 
  match !smt_solver with
    | None -> ()
    | Some (ic,oc,ec) -> 
        begin
          try
            M.msg_string M.Debug "Stopping SMT solver process...";
            
            begin
              try
                ignore (Unix.close_process_full (ic,oc,ec) );
              with _ ->
                ignore( Sys.command ("killall "^solver_cmd) )
            end;

            smt_solver:= None;
            M.msg_string M.Normal "SMT solver stopped."
          with _ -> failwith "failed to stop SMT solver!"
        end

(***********************   End of system part   ******************************)

open Ast
module Stats = Bstats
module E = Expression
module P = Predicate
module O = Options

(************************  Symbol modification  ******************************)

let symbol_counter = ref 0
let symbol_table = Hashtbl.create 53
let uf = ref [] (*uninterpreted function symbols*)
let chlvalTabl = Hashtbl.create 157

let clear_symbol_table () =
  Hashtbl.clear symbol_table;
  Hashtbl.clear chlvalTabl;
  symbol_counter := 0;
  uf := []

let get_fresh_symbol () = 
  symbol_counter := !symbol_counter + 1;
  let sym = "S" ^ (string_of_int !symbol_counter) in
    sym

let get_quantified_symbol sym = 
  try
    Hashtbl.find symbol_table sym
  with Not_found ->
    symbol_counter := !symbol_counter + 1;
    let new_sym = "?S" ^ (string_of_int !symbol_counter) in
      Hashtbl.add symbol_table sym new_sym;
      new_sym

let get_symbol sym =
  try
    Hashtbl.find symbol_table sym
  with Not_found ->
    let new_sym = get_fresh_symbol () in
      Hashtbl.add symbol_table sym new_sym;
      new_sym
(*
let get_symbol_for_chlval s1 s2 =(*TODO this also define the symbol for s1*)
  let sym = (get_symbol s1)^"v"^s2 in
    Hashtbl.replace symbol_table sym sym;
    sym
*)

let symbol_declaration () =
  let buffer = Buffer.create 1000 in
  let p = Buffer.add_string buffer in
    Hashtbl.iter
      (fun k v ->
        if not (String.get v 0 = '?') then
          (p ":extrafuns(("; p v; p " Int ))\n")
      ) symbol_table;
    Hashtbl.iter
      (fun k v ->
        if not (String.get v 0 = '?') then
          (p ":extrafuns(("; p v; p " Int ))\n")
      ) chlvalTabl;
    Buffer.contents buffer

let declare_fct sym nb_arg =
  let sym2 = "fct_"^sym in
    if not (List.mem (sym2, nb_arg) (!uf)) then
      uf := (sym2, nb_arg):: !uf;
    sym2

let fct_declaration () =
  let rec repeat i str acc  =
    if ( i > 0 ) then repeat (i-1) str (str^acc)
    else acc
  in
    String.concat "\n" (List.map (fun (f,i) -> ":extrafuns (("^f^ " "^(repeat (i+1) "Int " "")^ "))") !uf)

(* HACK transform a string into an integer
 * the SMT solver can handle type, but their typing sytem is stronger than C.
 * So let consider everything as an int...
 *)
let int_size = 1 + snd (frexp (float_of_int max_int)) (*to reduce the hash to an int*)
let sizeof_int = int_size / 8
(*takes only extract  pat of 31/63 bits from the digest*)
let hash_string str = 
  let digest = Digest.string str in
  let hash = ref 0 in
    for i = 0 to sizeof_int do
      let c = (int_of_char (String.get digest i)) in
      let shift = 8*(i mod sizeof_int) in
        hash := !hash lor (c lsl shift)
    done;
  (((!hash) lsl 1) lsr 1)(*makes the integer positive*)

(*
field id: use hash_string
*)

let my_string_of_int i =
  if i >= 0 then string_of_int i
  else Printf.sprintf "(~ %d)" (-i)

(******************   Conversion from Blast to SMT  **************************)
let rec convertLvalForAccessToSMT buf lval =
 let p = Buffer.add_string buf in
 let ce = convertExpToSMT buf in
  match lval with
  | E.Symbol sym -> p (get_symbol sym)
  | E.This -> failwith "this is not supported in convertLvalToSMT"
  | E.Access (ao,exp,sym) ->
    begin
      match ao with
      | E.Dot -> (p "(Select "; ce exp; p (Printf.sprintf " %u )" (hash_string sym)))
      | E.Arrow -> (p "(Select "; ce (E.Lval (E.Dereference exp)); p (Printf.sprintf " %u )" (hash_string sym)))(*arrow is only syntactic sugar*)
    end
  | E.Dereference exp -> (p "(Select "; ce exp ; p "  0)" )
  | E.Indexed (e1, e2) -> (p "(Select "; ce e1 ; p "  "; ce e2 ; p ")" )

and convertChLvalForAccessToSMT buf lval str =
 let p = Buffer.add_string buf in
  if (O.getValueOfBool "clock") then
    (p "(Chlval "; convertLvalForAccessToSMT buf lval; p (Printf.sprintf " %d)" (hash_string str)))
  else
    try
      p (Hashtbl.find chlvalTabl (lval,str))
    with Not_found ->
      let sym = get_fresh_symbol () in
        Hashtbl.replace chlvalTabl (lval,str) sym;
        p sym
  (* NOTE DZ:
    I think it would be better to have somthing like -clock option, but without the Chlval uninterpreted fct.
    But it was done this way in simplify and I don't have the time to do it now.
   *)
  (*
  match lval with
  | E.Symbol sym -> get_symbol_for_chlval sym str
  | E.This -> failwith "this is not supported in convertLvatToSMT"
  | E.Access (ao,exp,sym) ->
    begin
      match ao with
      | E.Dot -> failwith ("ERROR: Chlval are assumed to appears only with symbol: E.Dot "^(convertExpToSMT exp)^" "^(get_symbol sym))
      | E.Arrow -> failwith ("ERROR: Chlval are assumed to appears only with symbol: E.Arrow "^(convertExpToSMT exp)^" "^(get_symbol sym))
    end
  | E.Dereference exp ->  failwith ("ERROR: Chlval are assumed to appears only with symbol: E.Deref"^(convertExpToSMT exp)^" "^(str))
  (*Printf.sprintf "(Select %s  0)" (convertExpToSMT exp)*)
  | E.Indexed (e1, e2) -> failwith ("ERROR: Chlval are assumed to appears only with symbol: E.Indexed "^(convertExpToSMT e1)^" "^(convertExpToSMT e2))
  *)

  (* NOTE DZ:
    It looks that tihs coed is not used ( => not tested ) because the query is a path formula.
    A path fomula is a trace processed and restricted to a subset of all the expressions.
    SMT solvers have the capability to handle directly traces (modulo variable renaming in affectation).
  *)
and convertLvalAndAssignToSMT buf lval expr =
 let p = Buffer.add_string buf in
 let ce = convertExpToSMT buf in
  let expr_str () = ce expr in
    match lval with
    | E.Symbol sym -> (p "(= "; p (get_symbol sym); p " "; expr_str () ; p ")")
    | E.This -> failwith "this is not supported in convertLvalToSMT"
    | E.Access (ao,exp,sym) ->
      begin
        match ao with
        | E.Dot -> ( p "(Store "; ce exp; p" "; p (Printf.sprintf "%u " (hash_string sym)); expr_str(); p" )")
        | E.Arrow -> (p "(Store "; ce (E.Lval (E.Dereference exp)); p (Printf.sprintf " %u " (hash_string sym)); expr_str(); p" )")(*arrow is only syntactic sugar*)
      end
    | E.Dereference exp -> (p "(Store "; ce exp; p " 0 "; expr_str(); p ")")
    | E.Indexed (e1, e2) -> (p "(Store "; ce e1; p " "; ce e2; p " "; expr_str(); p")")

and convertExpToSMT buf exp =
 let p = Buffer.add_string buf in
 let ce = convertExpToSMT buf in
  match exp with
  | E.Lval lval -> convertLvalForAccessToSMT buf lval
  | E.Chlval (lval, str) -> convertChLvalForAccessToSMT buf lval str
  | E.Assignment (ao, lval, exp) -> convertLvalAndAssignToSMT buf lval exp (*is access op needed ?? *)
  (* special handling of left bit shift to match FOCI interface transformations in theoremProver.ml *)
  | E.Binary (E.LShift, e1, (E.Constant (Constant.Int by))) -> ce (E.Binary (E.Mul, e1, E.Constant (Constant.Int (Misc.power 2 by))))
  | E.Binary (op, e1, e2) ->
    begin
      match op with
      | E.Plus -> ( p "(+ "; ce e1; p " "; ce e2; p ")")
      | E.Minus -> ( p "(- "; ce e1; p" "; ce e2; p ")")
      | E.Mul -> ( p "(* "; ce e1; p" "; ce e2; p ")")
      | E.Div -> ( p "(/ "; ce e1; p" "; ce e2; p ")")
      | E.Eq -> ( p "(= "; ce e1; p" "; ce e2; p ")")
      | E.Ge -> ( p "(>= "; ce e1; p" "; ce e2; p ")")
      | E.Gt -> ( p "(> "; ce e1; p" "; ce e2; p ")")
      | E.Le -> ( p "(<= "; ce e1; p" "; ce e2; p ")")
      | E.Lt -> ( p "(< "; ce e1; p" "; ce e2; p ")")
      | E.Ne -> ( p "(not (= "; ce e1; p" "; ce e2; p "))")
      | E.Index -> ( p "(Select "; ce e1; p" "; ce e2; p ")")
      | E.Rem (*-> ( p "(%% "; ce e1; p" "; ce e2; p ")") *)
      | E.BitAnd (*-> ( p "(& "; ce e1; p" "; ce e2; p ")") *)
      | E.BitOr (*-> ( p "(| "; ce e1; p" "; ce e2; p ")") *)
      | E.Xor
      | E.And
      | E.Or
      | E.LShift
      | E.RShift
      | E.Comma
      | E.FieldOffset
      | E.Offset -> (p (Printf.sprintf "(%s " (E.binaryOpToVanillaString op)); ce e1; p " "; ce e2; p ")")
    end
  | E.Cast _ -> failwith "convertExpToSMT: the java part is not integrated"
  | E.CastCil _ -> failwith "convertExpToSMT: C type casting is not supported"
  | E.Constant cst ->
    begin
      match cst with
      | Constant.Int i -> p (my_string_of_int i)
      | Constant.Long i64 -> p (Int64.to_string i64)
      | Constant.Float f ->(* p ((string_of_float f)^"0") (*TODO problem of the unary -*)*)
	    (*maybe it is better to cast everything in int to avoid type conflict (done this way in simplify)*)
        p (my_string_of_int (int_of_float f))
      | Constant.String s -> p (Printf.sprintf "(String %u )" (hash_string s))
      |	Constant.EnumC (i,s) -> p (my_string_of_int i)
      |	Constant.Null -> p "0"
    end
  | E.Constructor _ -> failwith "convertExpToSMT: the java part is not integrated"
  | E.MethodCall _ -> failwith "convertExpToSMT: the java part is not integrated"
  | E.FunctionCall ((E.Lval (E.Symbol f)), args) ->
    let fct_sym = declare_fct f (List.length args) in(*something more to put in extrafuns*)
      (p "("; p fct_sym; p " "; List.iter (fun exp -> ce exp; p " ") args; p ")")
  | E.Select (e1,e2) -> ( p "(Select "; ce e1; p" "; ce e2; p ")")
  | E.Store (e1,e2,e3) -> ( p "(Store "; ce e1; p" "; ce e2; p" "; ce e3; p ")")
  | E.Sizeof i -> p (my_string_of_int i)
  | E.Unary (op, e) ->
    begin
      match op with
      | E.UnaryMinus -> ( p "(~ "; ce e; p")")
      | E.UnaryPlus -> ce e
      | E.Not -> ( p "(not "; ce e; p ")")
      | E.BitNot
      | E.Address -> (p "("; p (E.unaryOpToVanillaString op); p" "; ce e; p " )")
      | E.DestructorCall -> failwith "convertExpToSMT: the java part is not integrated"
      | E.Assume
      | E.Assert
      | E.PreInc
      | E.PreDec
      | E.PostInc
      | E.PostDec -> failwith ("convertExpToSMT: unayOp not suppoted "^E.unaryOpToVanillaString op )
    end
  | E.InstanceOf  _ -> failwith "convertExpToSMT: the java part is not integrated"
  | _ -> failwith ("convertExpToSMT: unsupported expression "^E.toString exp)

and convertPredToSMT buf pred =
 let p = Buffer.add_string buf in
 let r = convertPredToSMT buf in
  match pred with
  | P.True -> p "true "
  | P.False -> p "false "
  | P.And pred_lst -> (p "(and true "; List.iter (fun pred -> r pred; p " ") pred_lst; p ")")
  | P.Or pred_lst ->  (p "(or false "; List.iter (fun pred -> r pred; p " ") pred_lst; p ")")
  | P.Not pred ->     (p "(not "; r pred; p ")")
  | P.Implies (p1, p2) -> (p "(implies "; r p1; p " "; r p2; p ")")
  | P.Iff (p1, p2) ->(p "(iff "; r p1; p " "; r p2; p ")")
  | P.All (sym, pred) ->
    let sym_str = get_quantified_symbol sym in
      (p "(forall ("; p sym_str; p " Int) "; r pred; p ")")
  | P.Exist (sym, pred) ->
    let sym_str = get_quantified_symbol sym in
      (p "(exists ("; p sym_str; p " Int) "; r pred; p ")")
  | P.Atom exp -> convertExpToSMT buf exp
  | _ -> failwith ("convertPredToSMT: unsupported predicate "^P.toString pred)

let convertPredToSMT_string pred =
  let buf = Buffer.create 100 in
  convertPredToSMT buf pred;
  Buffer.contents buf

(*****************************   Axioms   ************************************)
type u_fct_sym = string * int * string (* name + arity + annotations *)

let ufct_sym fct =
  let f,_,_ = fct in f

(*array/pointer declaration: overloaded to avoid type problem with the solvers*)
let uf_select = ("Select", 2, "")
let uf_store = ("Store", 3, "")
(* address operator *)
let uf_address = ("Address", 1, "")
(* ',' return the last value*)
let uf_comma = ("Comma", 2, ":assoc")
(* as uninterpreted function symbols *)
let uf_bitnot = ("BitNot", 1, "")
let uf_bitand = ("BitAnd", 2, ":assoc :comm")
let uf_bitor = ("BitOr", 2, ":assoc :comm")
let uf_and = ("And", 2, ":assoc :comm")
let uf_or = ("Or", 2, ":assoc :comm")
let uf_xor = ("Xor", 2, ":assoc :comm")
let uf_lshift = ("LShift", 2, "")
let uf_rshift = ("RShift", 2, "")
  (*":assumption (forall (?x Int) (?n Int) (= (RShift(LShift ?x ?n) ?n) ?x))";*) (*true only if you don't consider over/underflow*)
let uf_rem = ("Rem", 2, "")
let uf_offset = ("offset", 2, "")
let uf_foffset = ("Foffset", 2, "")
(* string type emulation: take the hash of a string and return an int*)
let uf_string = ("String", 1, "")
let uf_chval = ("Chlval", 2, "")

let predefined_fct = [
uf_select;
uf_store;
uf_address;
uf_comma;
uf_bitnot;
uf_bitand;
uf_bitor;
uf_and;
uf_or;
uf_xor;
uf_lshift;
uf_rshift;
uf_rem;
uf_offset;
uf_foffset;
uf_string;
uf_chval;
]
    

let assumption_select_store = [
  ":assumption (forall (?a Int ) (?i Int) (?x Int) (= (Select (Store ?a ?i ?x) ?i) ?x))";
  ":assumption (forall (?a Int ) (?i Int) (?j Int) (?x Int) (implies (not (= ?i ?j)) (= (Select (Store ?a ?i ?x) ?j) (Select ?a ?j))))";
]
let assumption_select_address = [
  ":assumption (forall (?x Int) (= (Select (Address ?x) 0) ?x))";
  ":assumption (forall (?x Int) (= (Address (Select ?x 0)) ?x))";
  ":assumption (forall (?x Int) (?y Int) (?i Int) (implies (= (Address (Select ?x ?i)) (Address (Select ?y ?i))) (= (Address ?x) (Address ?y)) ))";
  (*":assumption (forall (?x Int) (not (= (Address ?x) 0)))" ;*)(*TODO inconsistent with the rest*)
]
let assumption_comma = [
  ":assumption (forall (?x Int) (?y Int) (= (Comma ?x ?y) ?y))";
]
let assumption_foffset = [
  ":assumption (forall (?x Int) (?y Int) (?d1 Int) (implies (= (Foffset ?x ?d1) (Foffset ?y ?d1)) (= ?x ?y)))" ;
]
let assumption_string = [
  ":assumption (forall (?x Int) (not (= (String ?x) 0)))" ;
]

(******** config **********)
let smtuf = ref false
let ufannot = ref true
let initUF () = smtuf := O.getValueOfBool "smtuf"; ufannot := O.getValueOfBool "ufannot"
(**************************)

let declare_ufct fct =
  match fct with
  | sym, nb, annot ->
    let rec repeat i str acc  =
      if ( i > 0 ) then repeat (i-1) str (str^acc)
      else acc
    in
      (Printf.sprintf ":extrafuns ((%s %s %s ))" sym (repeat (nb+1) "Int " "") (if !ufannot then annot else ""))


(** declare only the assumption that can be used (i.e. the fct symbols appears in the formula )*)
let declarations_axioms_string formula =
  let b = Buffer.create 0 in
    List.iter (
      fun x ->
        if Misc.is_substring formula (ufct_sym x) then
          Buffer.add_string b ((declare_ufct x)^"\n")
      ) predefined_fct; (*declare only the fct that apprears in the formula*)
    let declare_assumptions assumption_list =
      List.iter (fun x -> Buffer.add_string b (x^"\n")) assumption_list
    in
      if not (!smtuf) then
        begin
          if Misc.is_substring formula (ufct_sym uf_select) then
            begin
              if Misc.is_substring formula (ufct_sym uf_store) then
                declare_assumptions assumption_select_store;
              if Misc.is_substring formula (ufct_sym uf_address) then
                declare_assumptions assumption_select_address;
            end;
          if Misc.is_substring formula (ufct_sym uf_comma) then
              declare_assumptions assumption_comma;
          if Misc.is_substring formula (ufct_sym uf_foffset) then
              declare_assumptions assumption_foffset;
          if Misc.is_substring formula (ufct_sym uf_string) then
              declare_assumptions assumption_string;
        end;
      Buffer.contents b

(*****************************   Queries   ***********************************)

let query_counter = ref 0
let incr_counter () =  query_counter := !query_counter + 1
let counter_string () = string_of_int !query_counter
let incr_and_string () = incr_counter(); counter_string ()

(* The stack is used to emulate the context of Simplify.
   It is used for the incremental decision procedure*)
let stack = ref []

(* Stack of converted predicates in current predicate stack *)
let converted_stack = ref []

(* generic push, pop and reset functions *)
let raw_push convert_fn pred =
  stack := pred::(!stack);
  converted_stack := (convert_fn pred)::(!converted_stack)
    (*Misc.append_to_file "smt.log" ("PUSH "^(convertPredToSMT_string pred)^"\n\n")*)

let raw_pop () =
  stack := List.tl !stack;
  converted_stack := List.tl !converted_stack
    (*Misc.append_to_file "smt.log" ("POP\n\n")*)

let raw_reset () =
  stack := [];
  converted_stack := []
    (*Misc.append_to_file "smt.log" ("RESET\n\n")*)




(* Cache of predicates converted to SMT format.
   The cache supports cache keys, and is automatically cleared at block_reset invocation.  There's a special block_reset version that keeps the cache.
   Unlike "converted_stack", this can store converted predicates that have been removed from the trace.
*)
let converted_cache = Hashtbl.create 2003
let get_raw fn (key : string) =
  try Hashtbl.find converted_cache key with Not_found -> let conv = fn () in let _ = Hashtbl.replace converted_cache key conv in conv
	(* using Hashtbl.replace instead of .add is important here: Hashtbl.remove should invalidate the key completely! *)

(* get predicate for cache key or convert from raw predicate *)
let get key raw_pred = get_raw
  (fun () ->
    M.msg_string M.Debug (Printf.sprintf "SMT conv key %s!" key);
    (*let prof_string = if key = "post_1" then "convert 1st" else "convert to SMTlib" in*)
    (*Stats.time prof_string*) convertPredToSMT_string raw_pred
  )
  key

(* Remove cached value from the cache, so that it can be recalculated again *)
let invalidate key =
  M.msg_string M.Debug (Printf.sprintf "SMT invalidate key %s!" key);
  Hashtbl.remove converted_cache key

(* Clear and deactivate cache *)
let drop_cache () =
  Hashtbl.clear converted_cache;
  M.msg_string M.Debug (Printf.sprintf "SMT cache drop!");
  clear_symbol_table()

(* Cache-aware push, pop and reset *)
let push_cache key = raw_push (get key)
let pop_cache = raw_pop
let reset_keep_cache = raw_reset

(* Generic pushs and pops that use only stack of converted preds *)
let height () = List.length !stack
let push = raw_push ((*Stats.time "convert to SMTlib"*) convertPredToSMT_string)
let pop = raw_pop

let reset () =
  reset_keep_cache ();
  clear_symbol_table ();
  drop_cache ()

let convertToSMT () = (*return the new declarations and the formula*)
(*Stats.push_profile "printf conj";*)
  (* excessive "true" is left for comparison with older implementations of this mediator *)
  let formula = Printf.sprintf "(and %s)" (String.concat " " (("true"::(!converted_stack) ))) in
(*Stats.pop_profile "printf conj";*)
  let fct_decl = (*Stats.time "fct"*) fct_declaration () in
  let sym_decl = (*Stats.time "symbol"*) symbol_declaration () in
    ( fct_decl ^ sym_decl,  formula)


(*format the query as a benchmark in the SMT-LIB format*)
let querySMT_cached () =
  let do_work extra formula =
    let buffer = Buffer.create 100 in
      let p = Buffer.add_string buffer in
      p "( benchmark Blast_query_" ; p (incr_and_string()) ; p "\n";
      p ":logic AUFLIA\n";(*TODO simpler as arrays are redefined, and not linear only*)
      p (declarations_axioms_string formula);
      p extra;
      p ":formula ";p formula; p "\n";
      p ")\n";
      (* We don't need a separate string representation of the query, we can pipe it directly from the buffer *)
      if M.get_verbosity () = M.Debugging then begin
        M.msg_string M.Debug ("SMT solver query:");
        M.msg_string M.Debug (Buffer.contents buffer)
      end;
      (*Misc.append_to_file "smt.log" "------------------------------------------------------------\n\n";
	Misc.append_to_file "smt.log" (query^"\n");
	Misc.append_to_file "smt.log" "------------------------------------------------------------\n\n";
	Misc.append_to_file "smt.log" ((P.toString (P.And(pred::!stack)))^"\n\n");
	Misc.append_to_file "smt.log" "------------------------------------------------------------\n\n";*)
      let (ic, oc) = get_smt_solver () in
        Buffer.output_buffer oc buffer;
	flush oc; (*be sure to send the query*)
	match (input_line ic) with
	| "unknown" (*SMT solver says unknown if they cannot disprove *)
	| "sat" -> true
	(*Misc.append_to_file "smt.log" "true\n\n";
	  Misc.append_to_file "smt.log" "============================================================\n\n"; true*)
	| "unsat" -> false
	(*Misc.append_to_file "smt.log" "false\n\n";
	  Misc.append_to_file "smt.log" "============================================================\n\n"; false*)
	| _ as line -> failwith ("SMT sovler answer is strange: "^line)
  in
  let (extra, formula) = (*Stats.time "convert"*) convertToSMT () in
  let _ = M.msg_string M.Normal (Printf.sprintf "Calling SMT solver for conj of %d predicates" (List.length !stack)) in
  (*Stats.time "Pipe the query"*) (do_work extra) formula


(* HACK query ~p to mimic the Simplify semantic.
   Simplify has a Forall semantic and SMT has a Exists semantic:
   Forall x, P(x) = not Exists x, ~P(x)
 *)
let querySMTSolver pred =
  let _ = push (P.Not pred) in
  let res = not (querySMT_cached ()) in
  let _ = pop () in
  res

(* can the solver prove this is unsat (i.e. find a contradicition)*)
let is_contra () = not (querySMT_cached ())

(*****************************************************************************)
let getProofsFromSMT exp = (*really needed ??*)
  failwith "-craig 0 is not supported with SMT-LIB solvers. please use Simplify or Vampyre"
  (*only used with craig 0, and craig 0 in not really used anymore ...*)
