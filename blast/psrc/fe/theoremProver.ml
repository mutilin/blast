(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)


open Ast
module Stats = Bstats
module E = Expression
module P = Predicate
module M = Message
module O = Options
 
let foci_sat_occured = ref false
let ar_foci_query_counter = ref 0
exception AtomInExpressionException

let tp = ref "" (* ref on which theorem prover to use *)
  (* valid theorem provers: "cvc".  Simplify provers are removed ("simplify", "simplify-bits") *)
let init_tp () =
  SMTLIBInterface.initUF ();
  tp := O.getValueOfString "s";
  if not (
       !tp = "smt"
       ) then failwith ("(theoremProver.ml: init_tp) option 's': unknown theorem prover " ^ !tp)


(***********************************************************)
(************ Hashing for string constants *****************)
(***********************************************************)

let string_const_ctr_ref = ref 0
let string_const_table = Hashtbl.create 101
let const_string_table = Hashtbl.create 101

let get_string_constant s =
  try Hashtbl.find string_const_table s 
  with Not_found ->
     string_const_ctr_ref := 1 + !string_const_ctr_ref;
     let c = string_of_int !string_const_ctr_ref in
     Hashtbl.replace string_const_table s c;
     Hashtbl.replace const_string_table c s;
     c
let get_const_string c = 
  try Hashtbl.find const_string_table c 
  with Not_found -> failwith "Unknown string constant c"

(* only use get_string_const, get_const_string *)
(******************************************************************)

(* Interface functions*)

(******************************************************************)
(* Interface to CVC                                               *)
(******************************************************************)
             
(* DZ: Is CVC still used ??*)

(* keep around info: do we have a cvc server (= other process) 
             * running? *)

(*
exception ChannelException

let filed = open_out "blast-trace.cvc" 

let _ = at_exit (fun () -> close_out filed)


let reset_alarm () =
  (* Sys.set_signal Sys.sigalrm (Sys.Signal_ignore) *) ()

let set_alarm () =
  ()
  (*
  ignore (Sys.signal
            Sys.sigalrm
            (Sys.Signal_handle
               (fun i ->
                 M.msg_string M.Error "Caught exception sigalrm in stream!" ;
                  reset_alarm ();
                  raise  ChannelException;
               )) ) ;
  ignore (Unix.alarm (5))
  *)

(*
let simplify_line_tap = ref [] (* string list *)
*)

let secure_output_string oc s = (* CONTRACT: may raise channel exception ... *)
  try
    set_alarm ();
(*
    simplify_line_tap := s::!simplify_line_tap;    
*)
    output_string oc s;
    flush oc;
    reset_alarm ()
  with ChannelException -> 
    
    reset_alarm ();
(*
    dump_line_tap ();
*)
    raise ChannelException

let secure_input_line ic = (* CONTRACT: may raise channel exception *)
  
  set_alarm ();
  try
(*
    simplify_line_tap := ""::!simplify_line_tap;
*)
    let rv = input_line ic in
      reset_alarm ();
      rv
  with ChannelException ->
    reset_alarm () ;
(*
    dump_line_tap ();
*)
    raise ChannelException

let cvcServerWithProof = ref None   
let cvcPushes = ref 0
let clear_varCvcHashtbl_flag = ref true (* the table will get cleared only if this flag is true *)
                    (* moreover -- when simplify_assume is called the flag gets set to false *)
                    (* when pop is called, the flag is again set to true *)

let varCvcHashtbl = Hashtbl.create 53 
let next_cvc_var = ref 0 
let chlvalCvcHashtbl = Hashtbl.create 53 
let unknown_Cvc_var_counter = ref 0 

let getCvcServerWithProof () = 
  match !cvcServerWithProof with
    None -> begin
      M.msg_string M.Normal "Forking CVC process (with proofs)..." ;
      flush stdout ;
      let ic,oc = Unix.open_process "cvcl -sat simple -assump -tcc" (* +proof *)
      in
      cvcServerWithProof := Some(ic,oc) ;
      M.msg_string M.Normal "done!\n";
      flush stdout ;
      at_exit (fun () -> 
        ignore (Unix.close_process (ic,oc) ) ) ;
      (ic,oc)
        end
  | Some(a) -> a

(* RuGang's functions 03/13/2005:
   getCvcServer
   askCvcIfValid *)

let cvcServer = ref None   

let cvclAsserts = "addrOf:[[REAL, REAL] -> REAL];
 addrof:[REAL->REAL];
 foffset:[[REAL, REAL] -> REAL]; 
 select:[[REAL, REAL] -> REAL]; 
 RShift:[[REAL, REAL] -> REAL]; 
 LShift:[[REAL, REAL] -> REAL]; 
And: [[REAL, REAL] -> REAL]; 
Or:  [[REAL, REAL] -> REAL]; 
 ASSERT (FORALL (x, y: REAL): ((select (addrOf (x,y),0)) = x)); 
 ASSERT (FORALL (x, y: REAL): (addrOf (x,y) /= 0)); 
 ASSERT (FORALL (x, y, z: REAL): (foffset(x, z) = foffset(y,z)) => (x = y));"


let cvc_ground_term_list = ref [] 
    

let add_cvc_ground str = 
  if List.mem str (!cvc_ground_term_list) 
  then () 
  else 
    begin
      if (Misc.is_prefix "ASSERT" str) then
        cvc_ground_term_list := (!cvc_ground_term_list) @ [ str ]
      else 
        cvc_ground_term_list := str::(!cvc_ground_term_list)
    end 
      
let reset_cvc_ground () = 
  cvc_ground_term_list := 
   [
   ]


let getCvcServer () = 
  match !cvcServer with
    None -> 
      begin     
        M.msg_string M.Debug "Forking CVC process (no proofs)..." ;
        let ic,oc = Unix.open_process  
          "cvcl +int  -tcc 2>&1" 
        in
          cvcServer := Some(ic,oc) ;
          secure_output_string oc cvclAsserts; 
          flush stdout ;
          at_exit (fun () ->
                     ignore (Unix.close_process (ic,oc) ) ) ;
          (ic,oc)
      end
    | Some(a) -> a

    
(* usually the answer is at the end like
   CVC> CVC> - CVC> - CVC> - CVC> - CVC> - CVC> InValid.
   so using substring 
   other cases might break it? -RuGang, 03-13-05 *)
let rec askCvcIfValid ic =
  M.msg_string M.Debug ("asking cvcl ... \n");
  let line = secure_input_line ic in 
  M.msg_string M.Debug ("got cvcl ");
  let result = if Misc.is_substring line "InValid." then
    begin 
      M.msg_string M.Debug ("CVC: Invalid"^line);
      false
    end

  else if Misc.is_substring line "Valid." then 
    begin
      M.msg_string M.Debug ("CVC: Valid"^line);
      true
    end
  else if Misc.is_substring line "Error" then 
   (print_string ("Error: "^line^"\n");
   print_string ((input_line ic)^"\n");
   print_string ((input_line ic)^"\n");
   flush stdout;
   failwith "Error in CVC output!")
  else (print_string line; askCvcIfValid ic)
  in
  result

    




       
let convertToCvcSyntax exp =
  let get_next_var () = next_cvc_var := !next_cvc_var + 1; !next_cvc_var in
  let get_current_var () = !next_cvc_var in

  if (!clear_varCvcHashtbl_flag) then 
   begin
     Hashtbl.clear chlvalCvcHashtbl;
     next_cvc_var :=0
   end;
  (* I have no idea what the convert recursive crap means? *)
  (* let chlval_convert_recursive (lv,s) =  Printf.sprintf "(chlval %s %s)" (convertLval lv) ("FLD"^ s) in    *)
  let chlval_convert_table e = 
   try (Hashtbl.find chlvalCvcHashtbl e) 
      with 
          Not_found ->
            begin
              let s_e = get_next_var () (* E.toString e*) in
              let new_id = "chlval"^(string_of_int s_e) in
              if (not (Hashtbl.mem varCvcHashtbl new_id)) then
                  begin
                     add_cvc_ground (new_id^": REAL; ");
                      Hashtbl.add varCvcHashtbl new_id s_e
                  end;
              Hashtbl.add chlvalCvcHashtbl e new_id;
              new_id
            end 
  in
  let rec convertExp e =
    match e with
      | E.Lval lv -> convertLval lv
      | E.Chlval (lv,s)  -> 
          if (O.getValueOfBool "clock") then  Printf.sprintf "(chlval %s %s)" (convertLval lv) ("FLD"^ s)  
                                          else chlval_convert_table e   
    | E.Binary (op, e1, e2) -> 
          begin
            match op with 
              E.Minus -> 
                begin
                    match e2 with
                      E.Constant (Constant.Int 0) -> (convertExp e1)
                    | _ -> Printf.sprintf "(%s - %s)" (convertExp e1) (convertExp e2)
                end
              | E.Plus  -> 
                  begin
                    match e2 with
                      E.Constant (Constant.Int 0) -> (convertExp e1)
                    | _ -> Printf.sprintf "(%s + %s)" (convertExp e1) (convertExp e2)
                  end
              | E.Offset -> 
                  Printf.sprintf "(offset (%s,%s))" (convertExp e1) (convertExp e2)
              | E.Mul ->   
                  Printf.sprintf "(%s * %s)" (convertExp e1) (convertExp e2)
              | E.Div ->
                  Printf.sprintf "(%s / %s)" (convertExp e1) (convertExp e2) 
              | E.BitAnd ->
                  Printf.sprintf "And(%s,%s)" (convertExp e1) (convertExp e2)
              | E.BitOr -> 
                  Printf.sprintf "Or(%s,%s)" (convertExp e1) (convertExp e2)
              | E.Rem 
              | E.Xor 
              | E.Comma ->
                  Printf.sprintf "guf(%s,%s)" (convertExp e1) (convertExp e2)
              | E.LShift ->
                  begin
                    match (e2) with
                      E.Constant (Constant.Int k) -> (* multiply by 2^k *)
                        Printf.sprintf "(%s * %d)" (convertExp e1) (Misc.power 2 k)
                    |   _ -> Printf.sprintf "(%s (%s,%s))" (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2)
                  end
              | E.RShift ->
                  begin
                    match (e2) with
                      E.Constant (Constant.Int k) -> (* divide by 2^k *)
                        Printf.sprintf "(%s / %d)" (convertExp e1) (Misc.power 2 k) 
                    |   _ -> Printf.sprintf "(%s (%s,%s))" (E.binaryOpToVanillaString op) (convertExp e1)  (convertExp e2)
                  end    
              | _ -> 
              if E.isRelOp op 
              then 
                raise AtomInExpressionException
              else 
                failwith ("convertExp: (CVC) This operator should not be handled here "^(E.toString e))
          end

    | E.Constant (c) -> 
        begin
          match c with
                Constant.Int i ->  
                  string_of_int i
          | Constant.String s -> 
              if (not (Hashtbl.mem varCvcHashtbl "STRING")) then 
                begin
                  add_cvc_ground ("STRING"^" : REAL ;");
                  Hashtbl.add varCvcHashtbl "STRING"  1
                end;
              ("STRING") 
          | _ -> failwith ("unknown constant type")
        end
    | E.Unary (uop, e) -> 
        begin
          match uop with 
              E.UnaryMinus -> Printf.sprintf ("( - %s)") (convertExp e)
            | E.UnaryPlus -> Printf.sprintf ("( + %s)") (convertExp e)
            | E.Address -> 
              begin
                let e_cvc = convertExp e in
                let expr = Printf.sprintf "(addrof(%s))" e_cvc in
                  (* Also add an assertion relating deref and address for this expression. *)
                  add_cvc_ground (Printf.sprintf "ASSERT (select (%s, 0) = %s);" expr e_cvc) ;
                  expr
              end
          |     _ -> failwith "unknown unop" 
        end
    | E.FunctionCall(E.Lval (E.Symbol f), args) ->
        begin
          Printf.sprintf "(%s ( %s ))" f (Misc.strList (List.map convertExp args))
        end
    | _ -> failwith ("convertExp (Cvc): " ^ (E.toString e) ^ "not handled")
  and convertLval l =
   match l with
   | E.This -> failwith "This not handled in convertLval"
   | E.Symbol(str) -> 
       begin
         let cvc_name = (Str.global_replace (Str.regexp "@") "LOCAL" str) in
           if (Hashtbl.mem varCvcHashtbl cvc_name) then cvc_name
           else 
             begin
               Hashtbl.add varCvcHashtbl cvc_name  1; (* number 1 doesn't matter *)
               add_cvc_ground (cvc_name ^ ": REAL ;") ;
               cvc_name
             end
       end
   | E.Dereference drfe -> "select( "^(convertExp drfe)^ ", 0)"
   | E.Access(E.Dot, e, s) -> 
       begin
          if (not (Hashtbl.mem varCvcHashtbl ("FLD"^s))) then 
            begin
              add_cvc_ground ("FLD"^s^" : REAL;");
              Hashtbl.add varCvcHashtbl ("FLD"^s)  1
            end;
         Printf.sprintf "(select(%s,%s))" (convertExp e) ("FLD"^s)
       end
    | E.Access(E.Arrow, e, s) ->
        begin
          if (not (Hashtbl.mem varCvcHashtbl ("FLD"^s))) then 
            begin
              add_cvc_ground ("FLD"^s^" : REAL;");
              Hashtbl.add varCvcHashtbl ("FLD"^s)  1
            end;
          (Printf.sprintf "(select(%s,%s))" (convertExp e) ("FLD"^s))
        end
    | _ -> failwith ("convertLval (Cvc): " ^ (E.lvalToString l) ^ "not handled")
         
  and convertAtom e =
   flush stdout;
    match e with
      E.Binary (op, e1, e2) -> 
        begin
          let opstring = 
            match op with 
              E.Eq -> "=" | E.Ne -> "/=" | E.Gt  -> ">" | E.Lt  -> "<" | E.Ge  -> ">="
            | E.Le  -> "<=" | _   -> "" in
          if opstring <> "" 
          then
              try Printf.sprintf "(%s %s %s)"  (convertExp e1) opstring (convertExp e2)
              with AtomInExpressionException ->
                  if (opstring = "=" or opstring = "/=") then
                      match e1 with
                        E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
                            let s1 = convertExp e1 in
                            let s2 = convertAtom e2 in
                            if (opstring = "=") then Printf.sprintf "((%s /= 0) <=> (%s))" s1 s2
                            else Printf.sprintf "((%s = 0) <=> (%s))" s1 s2
                      | _ -> 
                            (match e2 with
                              E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
                                  let s1 = convertExp e2 in
                                  let s2 = convertAtom e1 in
                                    if (opstring = "=") then Printf.sprintf "((%s /= 0) <=> (%s))" s1 s2
                                    else Printf.sprintf "((%s = 0) <=> (%s))" s1 s2
                            | _ -> "")
                  else ""
          else (M.msg_string M.Debug "failed opthing";M.msg_string M.Debug (E.toString e); 
               Printf.sprintf "")
        end
    | E.FunctionCall(E.Lval (E.Symbol f), args) ->
        begin
          Printf.sprintf "(%s ( %s ) = 0)" f (Misc.strList (List.map convertExp args))
        end
    | _ -> (M.msg_string M.Debug "failed opthing2 (cvc)";M.msg_string M.Debug (E.toString e);  Printf.sprintf "")
          
  and convertPred e =
   flush stdout;
    match e with
      P.True -> "TRUE"
    | P.False -> "FALSE"
    | P.And plist -> 
        List.fold_left 
          (fun a -> fun b -> Printf.sprintf " (%s AND %s) " (convertPred b) a) 
          "TRUE" (plist)
    | P.Or plist -> 
        List.fold_left 
          (function a -> function b -> Printf.sprintf "(%s OR %s)" (convertPred b) a )
            "FALSE" (plist)
    | P.Implies (e1, e2) -> Printf.sprintf "(%s => %s)" (convertPred e1) (convertPred e2)
          
    | P.Not e1 -> Printf.sprintf "(NOT %s)" (convertPred e1)
    | P.Atom e ->  (convertAtom e)
    | P.All (s,e1) -> Printf.sprintf "(FORALL (%s : REAL): %s)" s (convertPred e1)
    | _ -> failwith ("CVC: Cannot handle this predicate "^(P.toString e))
          

  in
  (convertPred (P.constantfold exp (Hashtbl.create 1)))
    
    
    
let cvcQueryNum = ref 1
let cvcQueryNumPf = ref 1
    
    
let rec cvc_output_list oc lst =
   match lst with 
   [] -> () 
   | a::rest -> 
   begin
   M.msg_string M.Debug ("cvc input: "^a^"\n");
   secure_output_string oc (Printf.sprintf "%s\n" a) ;
   output_string filed  (Printf.sprintf "%s\n" a) ;
   flush oc;
   flush filed;
   cvc_output_list oc rest 
   end 

let queryExpUsingCvc exp = 
  M.msg_string M.Debug ("cvcl: querying exp:");
  M.msg_printer M.Debug P.print exp ;
  let cvcInput = convertToCvcSyntax exp
  in 
  M.msg_string M.Debug ("cvcl: "^cvcInput);
  flush stdout;
  flush stderr;
   let result = 
    try
      let ic,oc = getCvcServer () in
      cvc_output_list  oc !cvc_ground_term_list; 
      secure_output_string oc ("PUSH; QUERY ("^cvcInput^"); POP;\n") ;
      output_string filed ("PUSH; QUERY ("^cvcInput^"); POP;\n") ;
      M.msg_string M.Debug ("cvc input: "^"QUERY ("^cvcInput^");\n");
      reset_cvc_ground ();
      flush oc ; 
      flush filed;
      askCvcIfValid ic
    with e ->
      failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp)^". Check that cvcl is in your path.")
  in
  M.msg_string M.Debug (if result then "true" else "false");
  result

    
let queryExpUsingCvcGreedy exp =
  let _ = M.msg_string M.Debug ("querying exp in queryExpUsingCvcGreedy :"^(P.toString exp)^"\n") in
   (* first we have to convert the formula and collect all the ground terms that appear in it *)

  let fdgreedy = open_out "blast-trace-greedy.cvc" in

  reset_cvc_ground ();
  let formulas = match exp with P.And l -> l | _ -> failwith ("bad call to queryExpUsingCvcGreedy") in
  let all_asserts = List.map (convertToCvcSyntax) formulas
  in
             (* at this point all_asserts is a list of all assertions, and !cvc_ground_term_list is
                the list of all type declarations.
             *)
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest 
        end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest 
        end
  in
  let ic,oc = getCvcServer () in
  let rec getUsefulPredicates usefulPreds  =
    let rec _worker assertlist =
      match assertlist with 
        [] -> failwith "Exception : Formula is not valid!"
      | a :: rest ->
          output_string oc (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\nQUERY FALSE;\n" a (string_of_int !cvcQueryNum)) ;

          output_string fdgreedy (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\n\n" a (string_of_int !cvcQueryNum)) ;

          incr cvcQueryNum ;
          flush oc ;
          flush fdgreedy ;
          let result = 
             try
               askCvcIfValid ic
             with e ->
               failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          if result then a
          else
             _worker rest
    in
    output_string oc "PUSH 1;\n" ;
    output_string fdgreedy "PUSH 1;\n" ;
    output_list oc !cvc_ground_term_list ;
    output_asserts oc usefulPreds ;
    output_list fdgreedy !cvc_ground_term_list ;
    output_asserts fdgreedy usefulPreds ;
    output_string oc (Printf.sprintf "ECHO \"Query %s\" ; \n" (string_of_int !cvcQueryNum)) ;
    incr cvcQueryNum ;
    output_string oc "QUERY FALSE; \n" ;
    flush oc ;
    flush fdgreedy ;
    let result = 
      try
        askCvcIfValid ic
      with e ->
        failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
    in
    if result then usefulPreds 
    else 
      begin
        (* go on adding predicates from assertlist and check for validity.
           When valid, add the last formula to usefulPreds and repeat
           *)
        let nextpred = _worker all_asserts in
        output_string oc "POPTO 1; \n" ;
        output_string fdgreedy "POPTO 1; \n" ;
        getUsefulPredicates (nextpred :: usefulPreds) 
      end
    in
  let listOfPreds = getUsefulPredicates [] 
  in
  output_string oc "POPTO 1;\n" ;
  output_string fdgreedy "POPTO 1;\n" ;
  flush oc ;
  output_string fdgreedy "ECHO\"Predicates are \" ;\n" ;
  output_list fdgreedy (List.map (fun s -> "ECHO \""^s^"\";\n") listOfPreds) ;
  flush fdgreedy ;
  close_out fdgreedy ;
   listOfPreds 

    

    
let queryExpUsingCvcProof exp =

   M.msg_string M.Debug ("CVC querying exp:"^(P.toString (P.convertDNF exp))^"\n");

  reset_cvc_ground ();
  let formulas = exp::[] in 
  let all_asserts = List.map convertToCvcSyntax formulas
  in
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest
        end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest
        end
   in
(*  let ic,oc = getCvcServerWithProof () in - RuGang*)
   (* what are dump assertions? *)
   M.msg_string M.Debug "Asking CVC (Proof)";
   let ic,oc = getCvcServer () in
   output_string oc (Printf.sprintf "ECHO \"Query %s\";\nPUSH 1;\n" (string_of_int !cvcQueryNumPf)) ;
  output_string filed (Printf.sprintf "ECHO \"Query %s\";\nPUSH 1;\n" (string_of_int !cvcQueryNumPf)) ;
  output_list oc !cvc_ground_term_list ;
  output_list filed !cvc_ground_term_list ;
   M.msg_string M.Debug "after cvc ground crap";
   flush oc;
  let rec assert_loop asrtlist =
    match asrtlist with
      [] ->
        begin
          output_string oc "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
          output_string filed "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
          incr cvcQueryNumPf ;
          flush oc ; 
          flush filed ;
          let result = 
             try
               askCvcIfValid ic
             with e ->
              failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          reset_cvc_ground () ;
          flush oc;
          result       
        end
    | a::rest ->
       begin 
         output_string oc (Printf.sprintf "ASSERT %s;\n QUERY FALSE;\n" a);
         output_string filed (Printf.sprintf "ASSERT %s;\n QUERY FALSE;\n" a);
         flush oc ;
         let result = 
             try
               askCvcIfValid ic
             with e ->
               failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
         in
         if result then
           begin
             output_string oc "\nDUMP_ASSUMPTIONS; \nPOPTO 1;\n" ;
             output_string filed "\nDUMP_ASSUMPTIONS; \nPOPTO 1;\n" ;
             incr cvcQueryNumPf ;
             flush oc ; 
             flush filed ;
             reset_cvc_ground () ;
             flush oc;
             result      
           end
         else  
           assert_loop rest
       end
  in
  ignore (assert_loop all_asserts);
  output_asserts oc all_asserts ;
  output_list filed !cvc_ground_term_list ;
  output_asserts filed all_asserts ;
  output_string oc "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
  output_string filed "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
  incr cvcQueryNumPf ;
  flush oc ; 
  flush filed ;
  let result = 
    try
      askCvcIfValid ic
    with e ->
      failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
  in
  reset_cvc_ground () ;
  flush oc;
  result       



(* This implements the greedy algorithm (SLAM, Ruess, ...) to get back a list
   of useful predicates.
*)
exception CVCException 

let getProofsFromCvc exp =
  let _ = M.msg_string M.Debug ("querying exp in getProofsFromCvc :"^(P.toString exp)^"\n") in
   (* first we have to convert the formula and collect all the ground terms that appear in it *)
  try  
  reset_cvc_ground ();
  let formulas = 
    match exp with 
      P.And l -> l 
    | _ -> failwith ("bad call to getProofsFromCvc -- we only handle listd of conjunections") in
  let all_blast_asserts_array = Array.of_list formulas in
  let all_asserts = List.map (convertToCvcSyntax) formulas
  in
             (* at this point all_asserts is a list of all assertions, and !cvc_ground_term_list is
                the list of all type declarations.
             *)
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest 
        end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest 
        end
  in
  let ic,oc = getCvcServer () in
  let rec usefulPredicates usefulPreds  usefulBlastPreds =
    let rec _worker assertlist num =
      match assertlist with 
        [] -> 
          begin
            M.msg_string M.Error "Exception : Formula is not valid! Reverting to Vampyre" ;
            raise CVCException 
          end
      | a :: rest ->
          output_string oc (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\nQUERY FALSE;\n" a (string_of_int !cvcQueryNum)) ;
          incr cvcQueryNum ;
          flush oc ;
          let result = 
             try
               askCvcIfValid ic
             with e ->
               failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          if result then (a, num)
          else
             _worker rest (num+1)
    in
    output_string oc "PUSH 1;\n" ;
    output_list oc !cvc_ground_term_list ;
    output_asserts oc usefulPreds ;
    output_string oc (Printf.sprintf "ECHO \"Query %s\" ; \n" (string_of_int !cvcQueryNum)) ;
    incr cvcQueryNum ;
    output_string oc "QUERY FALSE; \n" ;
    flush oc ;
    let result = 
      try
        askCvcIfValid ic
      with e ->
        failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
    in
    if result then usefulBlastPreds 
    else 
      begin
        (* go on adding predicates from assertlist and check for validity.
           When valid, add the last formula to usefulPreds and repeat
           *)
        let (nextpred, i) = _worker all_asserts 0 in
        output_string oc "POPTO 1; \n" ;
        usefulPredicates (nextpred :: usefulPreds) ((all_blast_asserts_array.(i)) :: usefulBlastPreds)
      end
    in
  let listOfPreds = usefulPredicates [] []
  in
  output_string oc "POPTO 1;\n" ;
  flush oc ;
  List.map 
    (fun a -> match a with P.Not b -> b | _ -> a)
    listOfPreds
with _ ->
            begin
              let _eToCheck = P.implies exp (P.False) in
              let ans = queryExpUsingVampyre _eToCheck
              in
              let _ = M.msg_string M.Debug "Got UsefulPred" in
              if ans = false
              then
                begin
                  failwith "Vampyre could not prove formula valid!" ; []
                end
              else
                begin
                  let _lst = Engine.getUsefulPredicates () in
            (*
              let inferred_atoms = 
              match (!proof) with 
              Some(prf) -> 
              Engine.getInferredAtoms (prf)
              | None -> []
              in
              *)
                  List.map convertToPredicate _lst
                end
            end
*)
module Csisat = CsisatInterface

(*****************************************************************)
(** Implement a simple "predicate flow analysis" on the useful   *)
(*  predicates. This does a congruence closure on the variables  *)
(*  in the set of useful predicates and returns extra predicates *)
(*  that may be useful.                                          *)
(*****************************************************************)

let scope_pred_table = Hashtbl.create 37

let predicate_closure predlist = 
  let cong_classes = ref [] in
  let new_predicates = ref [] in
  let set_of_elt e = (* puts e in a new set if its not anywhere and if it is then removes it*)
    let _temp_set = List.filter (List.mem e) (!cong_classes) in
    let _cong_classes = List.filter (fun x -> not (List.mem e x)) (!cong_classes) in
    cong_classes := _cong_classes;
    if _temp_set = [] then ([e]) else List.hd _temp_set
  in
  
  let process_pred pred = 
    match pred with
      P.Atom (E.Binary (E.Eq,e1,e2)) -> 
        let s1 = set_of_elt e1 in
        if (List.mem e2 s1) then cong_classes := s1:: !cong_classes
        else 
          let u = Misc.union s1 (set_of_elt e2) in
          cong_classes := u::(!cong_classes)
    | _ -> ()
  in
  let _ = List.iter process_pred predlist 
  in
    let scope_of_exp exp =
      match exp with
        E.Lval E.Symbol(s) -> 
          begin
            let names = Misc.chop s "@" in
            if (List.length names = 1) then "__BLAST_GLOBAL" else (List.nth names 1)
          end
      | _ -> "__BLAST_GLOBAL"
    in
    
    let process_cong_class cc = 
      let _ = print_string "cc: \n";List.iter (fun x -> print_string ((E.toString x)^"  \n")) cc in  
  (* this should return a list of predicates *)
      let _ = Hashtbl.clear scope_pred_table in
      let process_exp e = 
        let scp = scope_of_exp e in
        let _ =  M.msg_string M.Debug (Printf.sprintf "exp: %s   scope: %s" (E.toString e) scp) in
        let present_set = try Hashtbl.find scope_pred_table scp with Not_found -> [] in
        Hashtbl.replace scope_pred_table scp (e::present_set)
      in
      let proc_scope_terms globals scp lst =
        if (not (scp = "__BLAST_GLOBAL")) then
          begin
            let pairs = Misc.cross_product lst globals (* Misc.make_all_pairs (lst@globals)*) in
            new_predicates := (List.map (fun (x,y) -> P.Atom (E.Binary(E.Eq,x,y))) pairs )@(!new_predicates)
          end
        else 
          ()
      in
      List.iter process_exp cc;
      let globals_cc = try Hashtbl.find scope_pred_table "__BLAST_GLOBAL" with Not_found -> [] in
      let _ = print_string "globals: \n";List.iter (fun x -> print_string ((E.toString x)^"  \n")) globals_cc    
      in      
      Hashtbl.iter (proc_scope_terms globals_cc) scope_pred_table 
    in  
    let _ = List.iter process_cong_class (!cong_classes) in
    !new_predicates
    (* very inefficient congruence closure algorithm *)
   
(**********************************************************)      
(* Get the list of useful predicates from the last proof. *)
(**********************************************************)
let getUsefulPredicates pred =
  let getPredsFromProof pred =
    if "smt" = O.getValueOfString "s" then
      SMTLIBInterface.getProofsFromSMT pred
    else
      failwith "Only SMT is supported"
  in
  let plist = getPredsFromProof pred in
          M.msg_string M.Debug "Leaf predicates.\n";
          List.iter (fun p -> M.msg_string M.Debug (P.toString p)) (plist); 
      (* let inf_plist = List.map convertToPredicate inferred_atoms in *)
      if(O.getValueOfBool "scope") then
        begin
          let pred_closure_list = predicate_closure plist in
      
          M.msg_string M.Debug "Closure predicates.\n";
          List.iter (fun p -> M.msg_string M.Debug (P.toString p)) (pred_closure_list); 
          plist @ pred_closure_list 
        end
      else
        plist

               
(*************************************************************************************)
(*************************************************************************************)
(*                               Interface to FOCI                                   *)
(*************************************************************************************)
(*************************************************************************************)
let stats_nb_foci_queries = ref 0

exception FociSatException
exception FociSatExceptionAsgn of string list

(* FOR FOCI 2 *) 
module FociPredicate = FociInterface
module FociExpression = FociInterface
module FociConstant = FociInterface
module FociAst = FociInterface


let get_pred_symbol fp = 
  match fp with
  FociPredicate.Atom s -> Some s
  | _ -> None

let itpN itp_fun plist = 
  stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  try
  	Stats.time "foci itpn" itp_fun plist
  with FociInterface.SAT _ -> raise FociSatException
  (*original below*)
  (*
  let p'_list = List.map FociDag.pred_dag_of_tree plist in
   let get_itp_list () = 
    try 
      M.msg_string M.Debug "calling into foci";
      let rv = Stats.time "foci itpn" itp_fun p'_list in
      M.msg_string M.Debug "back from foci";
      rv
    with Foci.SAT (plist',_) (* Not implemented: | FociSplit3.SAT (plist',_)*) ->
      (M.msg_string M.Debug "back from foci - exn SAT";
        if plist' = [] 
        then (M.msg_string M.Normal "Foci SAT: Empty"; 
                foci_sat_occured := true;
                raise FociSatException)
          else 
            let plist'_s = Misc.strList (List.map FociDag.Predicate.toString plist') in 
            let _ = M.msg_string M.Normal ("Foci SAT: "^plist'_s) in
            let plist = List.map (FociDag.pred_tree_of_dag) plist' in
            let pslist = Misc.map_partial get_pred_symbol plist in
              foci_sat_occured := true;
              raise (FociSatExceptionAsgn pslist))
  in
  let itp'_list  = Stats.time "compute itpN" get_itp_list () in
  let itp_list = List.map FociDag.pred_tree_of_dag itp'_list in
  itp_list
  *)

(* let foci_interpolateN = itpN Foci.interpolateN  
 * let foci_interpolateN_symm = itpN Foci.interpolateN_symm 
 * w/o FOCISPLIT *)

(* w/ FOCISPLIT  *)
let csisat_interpolateN plist = Csisat.interpolateN plist
let csisat_interpolate (a,b) = List.hd (Csisat.interpolateN [a;b])

(* Uncomment for Foci 1/2 
let foci_interpolateN_symm _ = failwith "foci_sym_interpolateN not supported in foci 1/2"
let foci_interpolateN_dasdill _ _ = failwith "foci_interpolateN_dasdill not supported in foci1/2"
let foci_theory_clauses () = failwith "foci_theory_clauses not supported in foci 1/2"  
let foci_instance_ref = None 
let foci_inc_pop () = failwith "foci_inc_pop: not implemented"
let foci_inc_push p = failwith "foci_inc_push: not implemented"
let foci_inc_check_sat () = failwith "foci_inc_check_sat: not implemented"
let foci_inc_reset () = failwith "foci_inc_reset: not implemented"
END FOCI 2 *)

(* the next is there only in FOCI 3 *)
(*
let foci_theory_clauses () = 
  let fp_list_list = Foci.theoryClauses () in  
  let tc_size = List.length (List.flatten fp_list_list) in
  M.msg_string M.Normal (Printf.sprintf "Foci says: %d theory clauses" tc_size);
  List.map (List.map (FociDag.pred_tree_of_dag)) fp_list_list

let foci_interpolateN_dasdill t_list c_list = 
   stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  let t'_list = List.map FociDag.pred_dag_of_tree t_list in
  let c'_list = List.map FociDag.pred_dag_of_tree c_list in
  let get_dd_itp_list () =
   try 
     M.msg_string M.Error "Calling dd_itpn";
     Stats.time "foci dd_itpn" (FociDasDill.refine t'_list) c'_list  
    with Foci.SAT (plist',_) ->
        if plist' = [] 
        then (M.msg_string M.Normal "Foci SAT: Empty"; 
              foci_sat_occured := true;
              raise FociSatException)
          else 
            let plist'_s = Misc.strList (List.map FociDag.Predicate.toString plist') in 
            let _ = M.msg_string M.Normal ("Foci SAT: "^plist'_s) in
            let plist = List.map (FociDag.pred_tree_of_dag) plist' in
            let pslist = Misc.map_partial get_pred_symbol plist in
              foci_sat_occured := true;
              raise (FociSatExceptionAsgn pslist)
  in
  let itp'_list  = Stats.time "compute itpN" get_dd_itp_list () in
  let _ = M.msg_string M.Error "done dd_itpn" in
  let itp_list = List.map FociDag.pred_tree_of_dag itp'_list in
  itp_list
*)




  
(* END FOCI 3 *)  
(* FOCI VERSION DIFF -- END *)
(*
let foci_expression_toString = FociAst.Expression.toString
let foci_predicate_toString = FociAst.Predicate.toString
*)
let foci_expression_toString = FociInterface.print_foci_expression
let foci_predicate_toString = FociInterface.print_foci_predicate


let compile_sets (plist : FociPredicate.predicate list) =
  if not (O.getValueOfBool "sets") then plist else 
  let list_of_var = ref [] in
  let rec collect_all_vars (e: FociExpression.expression) =
    match e with
      FociExpression.Constant _ -> ()
    | FociExpression.Variable s -> if List.mem e !list_of_var then () else list_of_var := e :: !list_of_var
    | FociExpression.Application ("sel", [e1; e2]) ->
        collect_all_vars e1
    | FociExpression.Application (s, el) ->
        begin
          List.iter collect_all_vars el
        end
    | FociExpression.Coeff _ -> assert (false)
    | FociExpression.Ite (p, e1, e2) -> trawl_all p ; collect_all_vars e1; collect_all_vars e2
    | _ -> ()
  and
      trawl_all e =
    match e with
      FociPredicate.True | FociPredicate.False -> ()
    | FociPredicate.And plist | FociPredicate.Or plist -> List.iter trawl_all plist
    | FociPredicate.Not  p -> trawl_all p
    | FociPredicate.Implies(p,p') -> trawl_all p; trawl_all p'
    | FociPredicate.Equality (e, e') ->
        begin
          match e' with
          | FociExpression.Application("emptyset",[]) -> ()
          | FociExpression.Application("add", [e1; e2]) ->
              collect_all_vars e2
          | FociExpression.Application("sel", [e1; e2]) ->
              collect_all_vars e; collect_all_vars e1
          | _ -> collect_all_vars e; collect_all_vars e'
        end;
    | FociPredicate.Leq (e, e') -> ()
    | FociPredicate.Atom s -> ()
    | FociPredicate.Forall (slist, p) -> assert (false)
  in
  let emptysets = ref [] in let in_is_called = ref "in" in
  let unions = ref [] in 
  (* trawl over the predicate, populating the above lists *)
  let rec    trawl e =
    match e with
      FociPredicate.True | FociPredicate.False -> ()
    | FociPredicate.And plist | FociPredicate.Or plist -> List.iter trawl plist 
    | FociPredicate.Not  p -> trawl p
    | FociPredicate.Implies(p,p') -> trawl p; trawl p'
    | FociPredicate.Equality (e, e') ->
        begin
          match e' with
          | FociExpression.Application(fnname,e1) ->
              if Misc.is_prefix "emptyset" fnname then
                emptysets := e :: !emptysets ;
              if Misc.is_prefix "add" fnname then begin
                match e1 with [x; t] -> unions := (e, x, t) :: !unions | _ -> assert false 
                end ;
              if Misc.is_prefix "in" fnname then in_is_called := fnname 
          | _ -> () 
        end;
    | FociPredicate.Leq (e, e') -> ()
    | FociPredicate.Atom s -> ()
    | FociPredicate.Forall (slist, p) -> assert (false)
  in
  ignore(List.map trawl_all plist) ;
  List.iter (fun x -> match x with FociExpression.Variable s -> M.msg_string M.Normal (" " ^ s) | _ -> failwith "match failure: compile_sets") !list_of_var ;
  let translate_empty e =
    FociPredicate.And (List.map (fun x -> 
      FociExpression.Equality (FociExpression.Application (!in_is_called, [e ; x]), FociExpression.Constant (FociConstant.Int 0))) !list_of_var)
  in
  let translate_union (e, e1, t) =
    FociPredicate.And (List.map (fun x ->
      let t1 = FociExpression.Equality (FociExpression.Application (!in_is_called, [e; x]), FociExpression.Constant (FociConstant.Int 0)) in
      let t2 = FociExpression.Equality (FociExpression.Application (!in_is_called, [e1; x]), FociExpression.Constant (FociConstant.Int 0)) in
      let t3 = FociExpression.Equality (t, x) in
      let imp1 = FociPredicate.Implies (t1, FociPredicate.Or [ t2; t3]) in
      let imp2 = FociPredicate.Implies (FociPredicate.Or [ t2; t3], t1) in
      FociPredicate.And [ imp1; imp2]) !list_of_var)
  in
  let translate p =
    unions := [] ; emptysets := [] ;
    trawl p;
    let psets = FociPredicate.And ((List.map translate_empty !emptysets) @ (List.map translate_union !unions))
    in
    unions := [] ; emptysets := [];
    M.msg_string M.Debug "compiled sets..." ;
    M.msg_string M.Debug (foci_predicate_toString psets);
    FociPredicate.And [p; psets]
  in
  List.map translate plist 


type foci_return = Sat | Unsat of (P.predicate (* * P.predicate *))

let fctr = ref 0 

(* For the benefit of foci_symm -- we need to rename the UFs used with this suffix so that they are disjoint *)


let foci_make_disjoint_flag = ref false
let set_disjoint_flag () = 
  M.msg_string M.Debug "Set Disjoint flag";
  foci_make_disjoint_flag := true

let unset_disjoint_flag () = 
  M.msg_string M.Debug "Unset Disjoint flag";
  foci_make_disjoint_flag := false

let foci_UF_idx = ref (0)

let foci_UF_idx_tick () = foci_UF_idx := !foci_UF_idx + 1

let foci_UF_idx_reset () =  foci_UF_idx := 0


let foci_UF_string s =
  let rv = 
    if !foci_make_disjoint_flag && not (E.is_interpreted s) 
    then s^"_"^(string_of_int !foci_UF_idx) 
    else s 
  in
  (* M.msg_string M.Debug (Printf.sprintf "foci_UF_string: in = %s, out = %s" s rv); *)
  rv

let clean_foci_UF_string s = 
  if not (!foci_make_disjoint_flag) then s
  else
    let pieces = Misc.chop s "_" in
    if (List.length pieces > 1) then String.concat "_" (Misc.chop_last pieces) 
    else s
 
let new_foci_var () = 
  fctr := !fctr + 1;
  "foci"^(string_of_int !fctr)

let listOfAddrVarsFoci = ref []
let chlval_table = Hashtbl.create 109
let chlval_map_table  = Hashtbl.create 109
let int_table_to = Hashtbl.create 23
let int_table_from = Hashtbl.create 23
let int_table_seen = Hashtbl.create 23

(* This number should be Pervasives.max_int, which is equal to default size.  
   Some of C code generators actually generate very large numbers, 
   up to the native platform unsigned max_int.  
   They aren't really processed by all the tools involved 
   in the checking process (for example, the binary CSIsat 
   distribution is 32-bit).  Therefore, we need a maximum number allowed *)
let max_int = Pervasives.max_int (* some RANDOM number I made up at 1am -- RJ *)
let num_offset = ref 100000
let make_new_num i =
  let s_i = string_of_int i in
  let s_i_c = Misc.k_suffix 2 s_i in
  let rv = int_of_string s_i_c in
    rv
  (*num_offset := !num_offset + 1; 
  !num_offset*)

let sign i = if i > 0 then 1 else if i = 0 then 0 else -1 
 
let int_map_to i =
  (* M.msg_string M.Debug ("int_map_to"^(string_of_int i));*)
  (* add i to the list of constants we've passed to foci *)
  let _ = Hashtbl.replace int_table_seen i true in 
  let abs_i = abs i in
  let ret_val = 
   (sign i) * 
   (if abs_i < max_int then abs_i
    else
      begin
        if (Hashtbl.mem int_table_to abs_i) 
        then 
          (Hashtbl.find int_table_to abs_i)
        else
          let key = make_new_num abs_i in
          let _ = (Hashtbl.add int_table_to abs_i key) in
          let _ = (Hashtbl.add int_table_from key abs_i) in
            key
      end)
 in
   (*M.msg_string M.Debug ("returns: "^(string_of_int ret_val));*)
   
   let _ = if (ret_val <> i) then M.msg_string M.Normal "WARNING :int_maps_to overflow!"
   in
     ret_val

let int_map_from i = 
  let abs_i = abs i in
  let _ =
    let seen = 0::(Misc.hashtbl_keys int_table_seen) in
    let focimax = O.getValueOfInt "focimax"  in
    if false && (focimax <> -1) && 
        (not (Hashtbl.mem int_table_seen i)) && 
        (abs_i > focimax) && 
        List.for_all (fun k -> abs ((abs k) - abs_i) > focimax)  seen (* TBD:SPEED *)
    then 
      (M.msg_string M.Error ("Invalid Interpolant -- large constant:"^(string_of_int i));
       foci_sat_occured := true;
       raise FociSatException)
    (* this will trickle up all the way and be caught by whoever *)
  in
  let ret_val =
    (try
      Hashtbl.find int_table_from abs_i
    with Not_found -> abs_i) * (sign i)
  in
    ret_val
 
(* in theory one could even use simplify or something else to "simplify" *)
let isConstant e = 
  match e with
      E.Constant (Constant.Int i) -> Some i
    | E.Unary (E.UnaryMinus,E.Constant (Constant.Int i)) -> 
        Some (-i)
    | E.Unary (E.UnaryPlus,E.Constant (Constant.Int i)) -> 
        Some (i)
    | _ -> None

let rec convertExpFoci e =
  let convert_times e1 e2 = 
    match (isConstant e1, isConstant e2) with
        (Some i1,Some i2) -> FociExpression.Constant (FociConstant.Int (i1 * i2))
      | (Some i1, None) -> FociExpression.Coeff (FociConstant.Int i1, (convertExpFoci e2))
      | (None, Some i2) -> FociExpression.Coeff (FociConstant.Int i2, (convertExpFoci e1))
      | (None, None) -> 
          begin
            M.msg_string M.Debug (Printf.sprintf "WARNING! convertExpFoci: times of %s * %s  " (E.toString e1) (E.toString e2));
            let ufs = foci_UF_string "times" in
            FociExpression.Application(ufs,List.map convertExpFoci [e1;e2])
          end
  in
  let foci_minus e' =
    match isConstant e' with
        Some i -> FociExpression.Constant (FociConstant.Int (int_map_to (-i)))
      | None -> FociExpression.Coeff (FociConstant.Int (-1),(convertExpFoci e'))
  in
  let chlval_convert_table e = 
    try FociExpression.Variable (Hashtbl.find chlval_map_table e) 
    with 
        Not_found ->
          let s_e = new_foci_var () (* E.toString e*) in
            Hashtbl.add chlval_table s_e e;
            Hashtbl.add chlval_map_table e s_e;
            FociExpression.Variable s_e
  in
  let chlval_convert_recursive (l,s) = 
    let ufs = foci_UF_string "chlval" in
    FociExpression.Application (ufs, [ convertLvalFoci l; FociExpression.Variable ("_"^s) ])
  in
    match e with
        E.Lval lv -> convertLvalFoci lv
      | E.Chlval (l, s) -> 
          if (O.getValueOfBool "fociUF") then chlval_convert_recursive (l,s)
          else chlval_convert_table e
          
      (* So the chlval thing didnt work out so well. look at test/foci/ex3.foci 
         so now we just convert it to string and for reconversion, stuff the result in a hashtbl!
        FociExpression.Application ("chlval", [ convertLvalFoci l; FociExpression.Variable ("_"^s) ])
      *)
  | E.Binary (op, e1, e2) -> 
      begin
        match op with 
            E.Minus -> 
              if (O.getValueOfBool "uif") then
                let ufs = foci_UF_string "UIminus" in
                FociExpression.Application (ufs, [convertExpFoci e1; convertExpFoci e2])
              else
                FociExpression.Sum [convertExpFoci e1; foci_minus e2]
          | E.Plus  -> 
              if (O.getValueOfBool "uif") then
                let ufs = foci_UF_string "UIplus" in
                FociExpression.Application (ufs, [convertExpFoci e1; convertExpFoci e2])
              else
                FociExpression.Sum[convertExpFoci e1; convertExpFoci e2]
          | E.Mul   -> convert_times e1 e2
          | E.Offset
          | E.BitAnd 
          | E.BitOr 
          | E.Div 
          | E.Rem 
          | E.RShift 
          | E.Xor 
          | E.Comma ->
                let ufs = foci_UF_string (E.binaryOpToVanillaString op ) in
                    FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])
          | E.LShift ->
              begin
                match e2 with
                  E.Constant (Constant.Int k) -> convert_times e1 (E.Constant (Constant.Int (Misc.power 2 k)))
                | _ ->    
                    let ufs = foci_UF_string (E.binaryOpToVanillaString op ) in
                    FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])
              end
          | E.FieldOffset ->
             let v = convertExpFoci e1 in
              (* the second argument to field offset is a field id *)
             let s =
               match e2 with
                 E.Lval (E.Symbol s) -> s
               | _ -> failwith ("convertExp : (FOCI) Second operator of field offset not a field id "^
                                 (E.toString e))
             in
             let ufs_fof = foci_UF_string "foffset" in
             let ufs_fld = foci_UF_string ("@field_"^s) in
             FociExpression.Application(ufs_fof, [v; FociExpression.Variable ufs_fld])
             
          |     _ -> 
              if E.isRelOp op then raise AtomInExpressionException
              else failwith ("convertExp (Foci): This operator should not be handled here "^(E.toString e))
      end
  | E.Constant (c) -> 
      begin
        match c with
            Constant.Int i -> FociExpression.Constant (FociConstant.Int (int_map_to i))
          | Constant.String s -> 
              begin
                M.msg_string M.Debug ("convertExpFoci: String Constant! in"^(E.toString e));
                let c = get_string_constant s in
                FociExpression.Variable ("_STRINGCONSTANT"^c)
              end
          | Constant.Float f ->
                    if (f -. (ceil f) = 0.0) then FociExpression.Constant (FociConstant.Int (int_of_float f))
            else FociExpression.Variable "_FLOATINGPTCONSTANT"
          | _ -> failwith ("Cannot handle constant "^(Constant.toString c))
      end
  | E.Unary (uop, e) -> 
      begin
        match uop with 
            E.UnaryMinus -> foci_minus e
          | E.UnaryPlus -> convertExpFoci e
          | E.Address -> 
            let v = convertExpFoci e in
            let ufs = foci_UF_string "addrOf" in
            if (List.mem v (!listOfAddrVarsFoci)) then () 
            else
              listOfAddrVarsFoci := v::(!listOfAddrVarsFoci);
            FociExpression.Application(ufs, [v])
          |     _ -> 
              let ufs = foci_UF_string (E.unaryOpToVanillaString uop)
              in
              FociExpression.Application(ufs, [convertExpFoci e])
      end
  | E.FunctionCall(E.Lval (E.Symbol f), args) ->
      FociExpression.Application(foci_UF_string f, List.map convertExpFoci args)
  | E.Select (e1,e2) -> FociExpression.Application("select",List.map convertExpFoci [e1;e2])
  | E.Store  (e1,e2,e3) -> FociExpression.Application("store",List.map convertExpFoci [e1;e2;e3])
  | _ -> failwith ("convertExpFoci: " ^ (E.toString e) ^ "not handled")
        
  and convertLvalFoci l =
    match l with
    | E.This -> failwith "This not handled in convertLval"
    | E.Symbol(str) -> FociExpression.Variable (str) 
    | E.Access(E.Dot, e, s) -> (* NEEDS DEBUGGING *) 
        let v = convertExpFoci e in (* hack to get around foci bug! *)
        let ufs = foci_UF_string ("sel@field_"^s) in
        FociExpression.Application( ufs, [v(*; FociExpression.Variable ("@field_"^s)*)])
    | E.Access(E.Arrow, e, s) ->
        failwith "foci doesnt like Arrow"
    | E.Dereference e ->
        let ufs = foci_UF_string "sel" in
        FociExpression.Application(ufs, [convertExpFoci e; FociAst.zero])
    | E.Indexed (e1, e2) -> 
        let ufs = foci_UF_string "sel" in
        FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])

  and 
    convertAtomFoci e = (* this returns a focipredicate *)
  let make_eq e1' e2' = 
    try 
      FociPredicate.Equality (convertExpFoci e1', convertExpFoci e2') 
    with AtomInExpressionException ->
      begin
        match e1' with
          E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
            begin
              let s1 = convertExpFoci e1' in
              let s2 = convertAtomFoci e2' in
              FociPredicate.Or [ (FociPredicate.And [s2 ; FociPredicate.Equality (s1, FociExpression.Constant (FociConstant.Int 1)) ] ) ; 
                                 (FociPredicate.And [FociPredicate.Not s2 ; FociPredicate.Equality (s1, FociAst.zero) ] ) ]
            end
        | _ -> 
            begin
              match e2' with
                E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
                  begin
                    let s1 = convertExpFoci e2' in
                    let s2 = convertAtomFoci e1' in
                    FociPredicate.Or [ (FociPredicate.And [s2 ; FociPredicate.Equality (s1, FociExpression.Constant (FociConstant.Int 1)) ] ) ; 
                                       (FociPredicate.And [FociPredicate.Not s2 ; FociPredicate.Equality (s1, FociAst.zero) ] ) ]
                  end
                    
              | _ -> failwith ("convertAtomFoci: Cannot handle this expression: "^(E.toString e))
                    
            end 
      end
  in
  let make_leq e1' e2' = 
    FociPredicate.Leq (convertExpFoci e1', convertExpFoci e2') 
  in
  match e with
    E.Binary (op, e1, e2) -> 
      begin
        match op with 
          E.Eq  -> make_eq e1 e2 
        |       E.Ne  -> FociPredicate.Not (make_eq e1 e2)
        |       E.Le  -> make_leq e1 e2       
        |       E.Lt  -> FociPredicate.Not (make_leq e2 e1)
        |       E.Gt  -> FociPredicate.Not (make_leq e1 e2)
        |       E.Ge  -> make_leq e2 e1 
        |       _   -> failwith ("convertAtomFoci: Cannot handle this expression: "^(E.toString e))
      end       
  | E.FunctionCall(E.Lval (E.Symbol f), args) ->
      make_eq e (E.Constant (Constant.Int 0))
  | E.Lval (E.Symbol s) -> FociPredicate.Atom s
  | _ -> failwith ("convertAtomFoci: "^(E.toString e)^ " Not a binary relation operator")

and convertPredFoci e =
  match e with
    P.True -> FociPredicate.True
  | P.False -> FociPredicate.False
  | P.And plist -> 
      FociPredicate.And (List.map convertPredFoci plist)
  | P.Or plist -> 
      FociPredicate.Or (List.map convertPredFoci plist)
  | P.Implies (e1, e2) -> FociPredicate.Or [FociPredicate.Not (convertPredFoci e1); (convertPredFoci e2)]
  | P.Not e1 -> FociPredicate.Not (convertPredFoci e1)
  
  | P.Atom e ->  (convertAtomFoci e)
  | P.Iff (e1,e2) ->  convertPredFoci 
        (P.Or [(P.And [e1;e2]);(P.And [P.Not e1 ; P.Not e2])])
  | P.All (s,e1) -> FociPredicate.Forall ([s],convertPredFoci e1)  
  | _ -> failwith ("Foci Cannot handle this predicate "^(P.toString e))
        
and convertToFociSyntax (e: P.predicate) =
  listOfAddrVarsFoci := [];
  let v_exp = convertPredFoci (P.constantfold e (Hashtbl.create 1))
  in
  let ufs_sel = foci_UF_string "sel" in
  let ufs_aof = foci_UF_string "addrOf" in
  let genAddrOfEq b =
        FociPredicate.Equality(b, FociExpression.Application(ufs_sel, 
                                                         [FociExpression.Application(ufs_aof, [b]); FociAst.zero]))
  in
  let genAddrPositive b = FociPredicate.Not  (FociPredicate.Leq 
                                ( FociExpression.Application(ufs_aof, [b]), (FociAst.zero))) in
    begin
    let vv = FociPredicate.And ((v_exp 
                      ::(List.map (fun b -> FociPredicate.Not (FociPredicate.Equality ( 
                                                                       (FociExpression.Application(ufs_aof, [b])) ,
                                                                       FociAst.zero)))
                                  !listOfAddrVarsFoci))
                       @ (List.map genAddrOfEq !listOfAddrVarsFoci)
                       @ (List.map genAddrPositive !listOfAddrVarsFoci)
                       ) in
      vv
  end

(* oh no! now we have to write the convert from foci to BLAST. ugh. *)
let rec convertFociLval l = 
  (* this is weird ... since the arg: "l" is supposed to be a foci expression that is really an lval ! *)
  match convertFociExp l with
      E.Lval l' -> l'
    | _ -> failwith ("convertFociLval failed on argument"^(foci_expression_toString l))
    
and make_bin op e1 e2 = 
  E.Binary(op,convertFociExp e1, convertFociExp e2)
and make_eq e1 e2 = make_bin (E.Eq) e1 e2
and make_leq e1 e2 = make_bin (E.Le) e1 e2

and gatherFociSum el = 
  match el with
      [] -> E.Constant (Constant.Int 0)
    | h::tl -> 
                  List.fold_left 
        (fun a b -> E.Binary (E.Plus,a,convertFociExp b)) 
        (convertFociExp h) tl
        
and convertFociExp e = 
  match e with
      FociExpression.Constant (FociConstant.Int i) -> E.Constant (Constant.Int (int_map_from i))
    | FociExpression.Variable v -> 
        begin
          match (Hashtbl.mem chlval_table v) with
              true -> Hashtbl.find chlval_table v 
            | false -> 
                if Misc.is_prefix "_STRINGCONSTANT" v then
                  let c = Misc.chop_after_prefix "_STRINGCONSTANT" v in 
                  let s = get_const_string c in
                  E.Constant (Constant.String s)
                else 
                    E.Lval (E.Symbol v)
        end
    | FociExpression.Application (f',el) ->
        begin
          let f = clean_foci_UF_string f' in
          match f with
            "select" ->
                (try
                  assert (List.length el = 2);
                  let (e1,e2) = (List.nth el 0, List.nth el 1) in
                  E.Select(convertFociExp e1, convertFociExp e2)
                  (* (match e1 with 
                    FociExpression.Variable _ -> E.Lval (E.push_deref (E.Dereference (convertFociExp e2)))
                   | _ -> E.Select(convertFociExp e1, convertFociExp e2))*)
                 with _ -> failwith "exn in convertFociExp @ select")
          | "store" ->
              (try
                assert (List.length el = 3);
                let (e1',e2',e3') = let l' = (List.map convertFociExp el) in (List.nth l' 0, List.nth l' 1, List.nth l' 2) in
                E.Store(e1',e2',e3')
               with _ -> failwith "exn in convertFociExp @ store") 
          | "chlval" -> 
                begin
                    match el with
                      [l;(FociExpression.Variable s)] -> E.Chlval (convertFociLval l, Misc.chop_after_prefix "_" s)
                    | _ -> failwith ("unknown chlval args in convertFociExp! "^(foci_expression_toString e))
                end
          | "*" -> 
                  begin
                    try
                      make_bin E.Mul (List.hd el) (List.nth el 2)
                    with _ ->
                      failwith ("unknown * args in convertFociExp! "^(foci_expression_toString e))
                  end
                  (* Hopefully we wont have to deal with wacko spl cases here! *)
          
          | "addrOf" ->
                  begin
                    try 
                      let [e1] = el in
                        E.Unary(E.Address, convertFociExp e1) 
                    with _ ->
                      failwith ("unknown & args in convertFociExp! "^(foci_expression_toString e))
                  end
          
          | "sel" ->
                  begin
                    try 
                      let [e1;e2] = el in
                        begin
                          match e2 with
                              FociExpression.Variable s ->
                                begin
                                  if (Misc.is_prefix "@field_" s)
                                  then
                                    E.Lval (E.Access(E.Dot, convertFociExp e1, (Misc.chop_after_prefix "@field_" s)))
                                  else
                                    E.Lval (E.Indexed(convertFociExp e1,convertFociExp e2))
                                end

                            | _ -> (* JHALA: flaky! TBD:ARRAYCANON? *(x+i), x[i] *)
                                if (e2 = FociAst.zero) then
                                  E.Lval (
                                  E.Dereference(convertFociExp e1))
                                else 
                                  E.Lval (
                                    E.Indexed(convertFociExp e1,convertFociExp e2))
                        end
                    with _ -> 
                      failwith ("unknown sel args in convertFociExp! "^(foci_expression_toString e))
                                  
                  end
          
          | "+" ->
                  begin
                    M.msg_string M.Normal "foci uses + in app";
                    gatherFociSum el
                  end
              
          | "offset" -> 
              let [e1;e2] = List.map convertFociExp el in
              E.Binary(E.Offset,e1,e2)
          | "UIplus" ->
                  begin
                    M.msg_string M.Normal "foci uses UIplus in app";
                    gatherFociSum el
                  end
          | "UIminus" ->
                  if (List.length el <> 2) then failwith "Bad use of UIminus";
                  let [e1;e2] = List.map convertFociExp el in
                    E.Binary(E.Minus,e1,e2)
          | "foffset" ->
                  begin
                    match el with
                      [e1;(FociExpression.Variable s)] -> 
                        let fld = if Misc.is_prefix "@field_" s then Misc.chop_after_prefix "@field_" s else failwith "bad foffset!" in
                        E.Binary (E.FieldOffset, (convertFociExp e1), (E.Lval (E.Symbol fld)))
                    | _ -> failwith "bad number of args to foffset!"
                  end
              | _ -> 
                  begin
                    if (Misc.is_prefix "sel@field_" f )
                    then
                      (* a field "access" thing *)
                      let field_name = Misc.chop_after_prefix "sel@field_" f in
                        try
                          let [e] = el in
                            E.Lval (E.Access(E.Dot, convertFociExp e, field_name))
                        with _ -> failwith "multiple args to sel@field_ ! ?"
                    else
                      try 
                        let op = E.binaryOpFromVanillaString f in
                        let [e1;e2] = el in
                        E.Binary (op, convertFociExp e1,convertFociExp e2)
                      with _ ->
                        E.FunctionCall(E.Lval (E.Symbol f), List.map convertFociExp el)
                  end
        end
      | FociExpression.Sum el -> gatherFociSum el
      | FociExpression.Coeff ((FociConstant.Int i),e') ->
          if (i < 0) then 
            if (i = (-1)) then
              E.Unary (E.UnaryMinus, (convertFociExp e'))
            else 
              E.Binary(E.Mul, E.Constant (Constant.Int (int_map_from i)),
                                                   convertFociExp e')
          else
            if (i = 1) then convertFociExp e'
            else
              (E.Binary(E.Mul, E.Constant (Constant.Int (int_map_from i)), convertFociExp e'))
              

and convertFociPred p =
  match p with
      FociPredicate.True -> P.True
    | FociPredicate.False -> P.False
    | FociPredicate.And plist -> P.And (List.map convertFociPred plist)
    | FociPredicate.Or plist -> P.Or (List.map convertFociPred plist)
    | FociPredicate.Not p' -> P.Not (convertFociPred p')
    | FociPredicate.Implies (p1,p2) -> P.Implies (convertFociPred p1,convertFociPred p2)
    | FociPredicate.Equality (e1,e2) -> P.Atom (make_eq e1 e2)
    | FociPredicate.Leq (e1,e2) -> P.Atom (make_leq e1 e2)
    | FociPredicate.Atom s -> P.Atom (E.Lval (E.Symbol s))

(* may raise "FociSatException" *)
let foci_plist_toString pl = String.concat "\n ; \n" (List.map foci_predicate_toString pl) 



let big_fq_count_ref = ref 0

let fq_dump num_cons cons_l_string =
  let foci_file_string = Printf.sprintf "\n \n \n %s \n \n \n" (cons_l_string) in
  let _file = 
    if num_cons > 2 then 
      (big_fq_count_ref:= !big_fq_count_ref + 1;
      (string_of_int !big_fq_count_ref)^".fq")
    else "fociqs"
  in
  let file = ((O.getValueOfString "mainsourcename")^"."^_file) in
  let _ = M.msg_string M.Debug ("writing to: "^file) in
  Misc.write_to_file file foci_file_string

(* now the somewhat non-trivial bit *)
(* cons_pre and cons_post are the two sets of constraints. This function returns the predicates
appearing in the fwd interpolant  *)


let check_interpolant 
  (itp : P.predicate) 
  (phi_1: P.predicate) (phi_2 : P.predicate) =  
  (* for now, only check that every symbol occurring in itp is in phi_1, and in phi_2 *)
  let syms_phi_1 = P.allVarExps phi_1 in
  let syms_phi_2 = P.allVarExps phi_2 in
  let syms_itp = P.allVarExps itp in
  let rv = 
    List.filter (fun x -> not ((List.mem x syms_phi_1) && (List.mem x syms_phi_2))) syms_itp
  in
    if rv <> [] then
      begin
        let s = Printf.sprintf "Interpolant issue: %s \n phi1: %s \n phi2: %s \n" 
                  (P.toString itp) (P.toString phi_1) (P.toString phi_1)
        in
          M.msg_string M.Normal s;
          M.msg_string M.Error s;
          List.iter (fun e -> M.msg_string M.Normal (E.toString e)) rv
      end
    
      
(* itp_process_function: P.predicate -> 'a *)

let csisat_extract_and_process_interpolant itp_process_function (cons_pre,cons_post) = 
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let _ = M.msg_string M.Debug "In extract_and_process_interpolant" in
  let cons_l = List.map (fun x -> P.And x) [cons_pre;cons_post] in
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) cons_l  in
  let [foci_c_pre'; foci_c_post'] = List.map convertToFociSyntax cons_l in
  let [foci_c_pre; foci_c_post] = compile_sets [foci_c_pre'; foci_c_post'] in
  let cons_l_string = foci_plist_toString [foci_c_pre;foci_c_post] in
    try
      let _ = 
        M.msg_string M.Debug ">>>>>>>>>>>> calling CSIsat";
        M.msg_string M.Debug (foci_predicate_toString foci_c_pre);
        M.msg_string M.Debug ";";
        M.msg_string M.Debug (foci_predicate_toString foci_c_post);
        M.msg_string M.Debug ">>>>>>>>>>>> CSIsat done" 
      in
        if Options.getValueOfBool "fqdump" then
            Misc.append_to_file "csisatQueries.txt" (cons_l_string ^ "\n\n" );
        let fwd_ip = 
          try 
            Bstats.time "CSIsat time" csisat_interpolate (foci_c_pre,foci_c_post) 
          with e -> 
            (M.msg_string M.Error ("CSIsat FAILS!(eap_itp) raises exception!!"^ (Printexc.to_string e));
             M.msg_string M.Normal ("CSIsat FAILS!(eap_itp) raises exception!!"^ (Printexc.to_string e));
             Misc.append_to_file "csisat_failed_q" cons_l_string;
             FociPredicate.True )
        in
        let _ = M.msg_string M.Debug ("CSIsat yields Interpolant: "^(foci_predicate_toString fwd_ip)) in
        let fwd_ip_b = convertFociPred fwd_ip in
        let _ = M.msg_string M.Debug ("CSIsat yields b_Interpolant: "^(P.toString fwd_ip_b)) in
        (* check if the interpolant is clean *)
        let _ = 
          match cons_l with
            [c1;c2] -> check_interpolant fwd_ip_b c1 c2 
          | _ -> failwith "this cannot happen"
        in
          itp_process_function fwd_ip_b
      with FociSatException -> failwith "CSIsat raised SAT!"
      | e -> raise e

let extract_and_process_interpolant itp_process_function (cons_pre,cons_post) = 
  let csisat_interpolant = lazy (csisat_extract_and_process_interpolant itp_process_function (cons_pre,cons_post) )in
    match O.getValueOfString "itp" with
    | "csisat" -> Lazy.force csisat_interpolant
    | _ -> failwith (Printf.sprintf "interpolant_sat: unknown interpolation procedure %s, only csisat is supported" (O.getValueOfString "itp"))


let extract_foci_preds = extract_and_process_interpolant P.getAtoms

let extract_foci_preds_parity = 
  extract_and_process_interpolant (P.getAtoms_parity true)

let extract_foci_interpolant = extract_and_process_interpolant (fun x -> x)

let csisat_interpolant_sat (cons_pre,cons_post) =
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let cons_l = [cons_pre;cons_post] in
  let _ = M.msg_string M.Debug "In interpolant_sat:" in
  let _ = List.iter (fun x -> M.msg_string M.Debug (P.toString x)) cons_l
  in
  let [foci_c_pre; foci_c_post] = List.map convertToFociSyntax cons_l in
  let foci_file_string = Printf.sprintf "\n \n \n %s \n ; \n  %s \n \n \n" (foci_predicate_toString foci_c_pre) (foci_predicate_toString foci_c_post)
  in
    try
      let fwd_ip = csisat_interpolate (foci_c_pre,foci_c_post) in
      let _ = M.msg_string M.Debug ("CSIsat yields Interpolant: "^(foci_predicate_toString fwd_ip))
      in
      let fwd_ip_b = convertFociPred fwd_ip in
      let _ = M.msg_string M.Debug ("CSIsat yields b_Interpolant: "^(P.toString fwd_ip_b))
      in
        Unsat(fwd_ip_b)
    with 
        FociSatException -> (M.msg_string M.Debug "CSIsat says SAT!"; Sat)
      | FociSatExceptionAsgn _ -> (M.msg_string M.Debug "CSIsat says SATa!"; Sat)
      | e ->
         begin
            M.msg_string M.Error ("CSIsat FAILS!(isat) raises exception!!"^ (Printexc.to_string e));
            M.msg_string M.Error ("CSIsat FAILS!(isat) raises exception!!"^ (Printexc.to_string e));
            Misc.append_to_file "csisat_failed_q" foci_file_string;
            raise e
         end

let interpolant_sat (cons_pre,cons_post) = 
  let csisat_interpolant = lazy (csisat_interpolant_sat (cons_pre,cons_post) )in
    match O.getValueOfString "itp" with
    | "csisat" -> Lazy.force csisat_interpolant
    | _ -> failwith "interpolant_sat: unknown interpolation procedure"
       
let interpolant_implies a b = 
  match interpolant_sat (a, (P.negate b)) with
      Sat -> false
    | Unsat _ -> true
  
let interpolant_unsat a = interpolant_implies a P.False
(* ultimately calls csisat_interpolant_sat (a, true) *)

let extract_and_process_interpolantN itp_fun foci_syn_conv itp_process_function cons_l : P.predicate list = 
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let _ = M.msg_string M.Debug "In extract_and_process_interpolantN:" in
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) cons_l 
  in
  let n_cons = List.length cons_l in
  let foci_cons_l' = List.map foci_syn_conv cons_l in
  let foci_cons_l = compile_sets foci_cons_l' in
  let cons_l_string = foci_plist_toString foci_cons_l in
  let dp = O.getValueOfString "itp" in
  let _ =
    M.msg_string M.Debug (">>>>>>>>>>>> calling "^ dp);
    M.msg_string M.Debug cons_l_string ;
    M.msg_string M.Debug (">>>>>>>>>>>> "^dp^"done" )
  in
  let _ = 
    if (O.getValueOfBool "fqdump") then
      if dp = "csisat" then Misc.append_to_file "csisatQueries.txt" (cons_l_string ^ "\n\n")
      else fq_dump n_cons cons_l_string
  in
  let itp_l = 
    try 
      let rv = itp_fun foci_cons_l in
      M.msg_string M.Debug (dp^" returns");
      rv
    with
      FociSatException -> (M.msg_string M.Normal (dp^" says SAT!"); raise FociSatException)             
    | FociSatExceptionAsgn a -> (M.msg_string M.Normal (dp^" says SATa!"); raise (FociSatExceptionAsgn a))
    | e -> 
        (M.msg_string M.Normal (dp^" FAILS!(eap_itpN) raises exception!!"^ (Printexc.to_string e));
         M.msg_string M.Error (dp^" FAILS!(eap_itpN) raises exception!!"^ (Printexc.to_string e));
         let foci_file_string = Printf.sprintf "\n \n \n %s \n \n \n" (cons_l_string) in
           if dp = "csisat" then Misc.append_to_file "csisat_failed_q" foci_file_string
           else Misc.append_to_file "foci_failed_q" foci_file_string;
           foci_sat_occured := true; 
           raise FociSatException)
    in
    let itp_l_string = foci_plist_toString itp_l in 
    let _ = M.msg_string M.Debug (dp^" yields Interpolants: \n"^itp_l_string) in
    let itp_b_l = List.map convertFociPred itp_l in
    let itp_b_l_string = String.concat "\n ; \n" (List.map P.toString itp_b_l) in
    let _ = M.msg_string M.Debug (dp^" yields b_Interpolant: "^itp_b_l_string) in
        List.map itp_process_function itp_b_l


let conv_fun p = 
  if (O.getValueOfBool "fmc") then
    (foci_UF_idx_tick (); convertToFociSyntax p) 
  else (convertToFociSyntax p)
  
let extract_foci_interpolantN = 
  let itp_fun = match O.getValueOfString "itp" with
    | "csisat" -> csisat_interpolateN
    | _ -> failwith "interpolant_sat: unknown interpolation procedure"
  in
    extract_and_process_interpolantN itp_fun conv_fun  (fun x -> x)

let extract_foci_interpolantN_noconv =
  let itp_fun = match O.getValueOfString "itp" with
    | "csisat" -> csisat_interpolateN
    | _ -> failwith "interpolant_sat: unknown interpolation procedure"
  in
  extract_and_process_interpolantN itp_fun convertToFociSyntax (fun x -> x)
  
(******************************  End Interface to FOCI  ******************************)

(* Incremental DP wrappers for REFINEMENT (not cartesian post, see postBdd) *)

        
let queryExpContext pred query =
  if (!tp="smt") then SMTLIBInterface.querySMTSolver query
  else failwith ("TheoremProver: queryExpContext: Unknown theorem prover "^(!tp)^".")

let rec queryExp exp =
  M.msg_string M.Debug ("Query : ");
  M.msg_printer M.Debug P.print exp; 
  if (!tp="smt") then SMTLIBInterface.querySMTSolver exp
  else failwith ("TheoremProver: queryExp: Unknown theorem prover "^(!tp)^".")


(************************************************************************************)

(*************************************************************************************)

let simp_ctr = ref 0

let block_assert p = 
  M.msg_string M.Debug ("bs_assert: ");
  M.msg_printer M.Debug P.print p;
  let tp = O.getValueOfString "incdp" in
  simp_ctr := !simp_ctr + 1;
  if (tp = "smt") then SMTLIBInterface.push p
  else failwith ("block_assert: unknown incdp: "^tp)

let block_pop () =
  M.msg_string M.Debug  "bs_pop"; 
  assert (!simp_ctr > 0);
  simp_ctr := !simp_ctr - 1;
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.pop ()
  else failwith ("block_pop: unknown incdp "^tp)

    
(* remove all blocks added to the context out of it; uses facilities of this module, not Simplify's *)
let block_reset () = 
  M.msg_string M.Debug "bs_reset";
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.reset ()
  else failwith ("block_reset: Strange theorem prover! "^tp)

(**** Cacheable theorem proving, useful for SMT-useful-blocks algorithm *)

let block_assert_cache key p =
  M.msg_string M.Debug (Printf.sprintf "bs_assert (key %s):" key);
  M.msg_printer M.Debug P.print p;
  let tp = O.getValueOfString "incdp" in
  simp_ctr := !simp_ctr + 1;
  if (tp = "smt") then SMTLIBInterface.push_cache key p
  else failwith ("block_assert_cache: Only SMT solvers are supported! Given: "^tp)

let block_pop_cache () =
  M.msg_string M.Debug  "bs_pop";
  assert (!simp_ctr > 0);
  simp_ctr := !simp_ctr - 1;
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.pop ()
  else failwith ("block_pop_cache: Only SMT solvers are supported! Given: "^tp)

let block_invalidate_cache key =
  M.msg_string M.Debug "bs_invalidate_cache";
  M.msg_string M.Debug key;
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.invalidate key
  else failwith ("block_invalidate_cache: Only SMT solvers are supported! Strange theorem prover: "^tp)

(* remove all blocks added to the context out of it, but keep cache *)
let block_reset_keep_cache () =
  M.msg_string M.Debug "bs_reset (keep cache)";
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.reset_keep_cache ()
  else failwith ("block_reset_keep_cache: Only SMT solvers are supported! Strange theorem prover: "^tp)

let __is_contra () = 
  let tp = O.getValueOfString "incdp" in
  if (tp = "smt") then SMTLIBInterface.is_contra ()
  else failwith ("is_contra: unknown incdp "^tp)

let _is_contra () = Stats.time "contradiction check" __is_contra ()

let check_unsat p =
    let _ = block_assert p in
    let rv = _is_contra () in
    let _ = block_pop () in
      rv

let rec _assume pred =
  let tp = O.getValueOfString "incdp" in 
  if (tp = "smt") then SMTLIBInterface.push pred
  (*else if (tp = "cvc") then cvc_assume pred*)
  (*else if (tp = "foci") then foci_inc_push pred *)
  else failwith ("_assume: unknown dp "^tp)

let _pop () = 
  (*M.msg_string M.Debug  "bs_pop with _"; *)
  let tp = O.getValueOfString "incdp" in 
  if (tp = "smt") then SMTLIBInterface.pop ()
  else failwith ("_pop: unknown dp "^tp) 

(*******************************************************************************)

let generateTest p =
        failwith "Test generation not supported"
(*
  (* Data structures used *)
(* let ac_generateTest p = *)
  let id_to_sym_ht = Hashtbl.create 31 in
  let sym_to_id_ht = Hashtbl.create 31 in
  let number_of_vars = ref 0 in
  let get_next_id () = incr number_of_vars ; !number_of_vars
  in 
  (* Helper functions *)
  let find_variables ap =
    match ap with 
      P.Atom aex | P.Not (P.Atom aex) ->
        let rec _trawl_exp aex =
          match aex with
            E.Lval _ (* add to hash table *)
          | E.Chlval _ -> (* add to hash table *)
              if Hashtbl.mem sym_to_id_ht aex then
                ()
              else 
                begin
                  let n = get_next_id () in
                  Hashtbl.add sym_to_id_ht aex n ;
                  Hashtbl.add id_to_sym_ht n aex ;
                  ()
                end
          | E.Binary (E.FieldOffset, e1, e2) -> failwith "Do not handle field offset"
          | E.Binary (_, e1, e2) ->
              _trawl_exp e1 ; _trawl_exp e2
          | E.Constant _ -> ()
          | E.Unary (_, e) -> _trawl_exp e
        in
        _trawl_exp aex
    | _ -> (* cannot happen *) failwith "In find_variables: atomic predicate expected"
  in
  let get_lp_constraints aex =
    match aex with
      E.Binary (relop, e1, e2) ->
        begin
          let (coeffs : int array) = Array.make (!number_of_vars + 1) 0 in (* coefficients for variables *)
          let const = ref 0 in
          let rec get_coeffs e pos_or_neg =
            match e with 
              E.Constant c ->
                begin
                  match c with Constant.Int i -> const := !const + (if pos_or_neg then i else -i) 
                  | _ -> failwith "add_lp_constraints: Only handle integers"
                end
            | E.Lval _
            | E.Chlval _ -> 
                let id = Hashtbl.find sym_to_id_ht e in
                coeffs.(id) <- coeffs.(id) + (if pos_or_neg then 1 else -1)
            | E.Binary (E.Plus, e1, e2) ->
                get_coeffs e1 pos_or_neg ; get_coeffs e2 pos_or_neg
            | E.Binary (E.Minus, e1, e2) ->
                get_coeffs e1 pos_or_neg ; get_coeffs e2 (not pos_or_neg)
            | E.Binary (E.Minus, e1, e2) ->
                get_coeffs e1 pos_or_neg ; get_coeffs e2 (not pos_or_neg)
            | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Lval l)
            | E.Binary (E.Mul, E.Lval l, E.Constant (Constant.Int i)) ->
                let id = Hashtbl.find sym_to_id_ht (E.Lval l) in
                coeffs.(id) <- coeffs.(id) + (if pos_or_neg then i else -i)
            | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Chlval (l,s))
            | E.Binary (E.Mul, E.Chlval(l,s), E.Constant (Constant.Int i)) ->
                let id = Hashtbl.find sym_to_id_ht (E.Chlval (l,s)) in
                coeffs.(id) <- coeffs.(id) + (if pos_or_neg then i else -i)
            | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Constant (Constant.Int j)) ->
                let ij = i * j in const := !const + (if pos_or_neg then ij else -ij) 
            | E.Unary (E.UnaryPlus, e1) -> get_coeffs e1 pos_or_neg
            | E.Unary (E.UnaryMinus, e1) -> get_coeffs e1 pos_or_neg
            | _ -> failwith ("get_coeffs: failing on "^(E.toString e))
          in
          get_coeffs e1 true ;
          get_coeffs e2 false ;
          (coeffs, !const, relop) 
        end
    | _ -> failwith ("get_lp_constraints: Strange expression "^(E.toString aex))
  in

  (* main function *)
  match p with
    P.And l ->
      begin
        if (List.exists 
             (fun ap -> match ap with 
                P.Atom _ | P.Not (P.Atom _) -> false | _ -> true) l) then
          failwith "generateTest: Assumes conjunction of atomic predicates"
        else 
          begin (* Now start the work *)
            (* first do a trawl and find out the number of variables. In the process, also
               populate the two hash tables name -> id, and id -> name
               *)
            List.iter find_variables l ;
            (* then for each atomic formula, generate the corresponding LP constraint *)
            let get_aexp ap =
              match ap with P.Atom aex -> aex 
              | P.Not (P.Atom aex) -> E.negateRel aex
              | _ -> failwith "Non atomic!"
            in
            let constraints = List.map (fun ap -> get_lp_constraints (get_aexp ap)) l in 
            (* first cut : assume no Neq *)
            let linprog = Lp.lp_make_lp 0 !number_of_vars in
            let add_constraints (coeffs, const, relop) =
              match relop with
              | E.Le -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const)) 
              | E.Ge -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const)) 
              | E.Eq -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 1 (float_of_int (-const)) 
              | E.Gt -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const+ 1)) 
              | E.Lt -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const- 1)) 
              | E.Ne -> if (Random.float 1.0< 0.5) then Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const+ 1)) 
                                else Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const- 1)) 
              | _ -> failwith "add_con: not handled"
            in
            List.iter (fun c -> ignore (add_constraints c)) constraints ;
            (* for each variable, set lower bound to -infinity *)
            Lp.lp_set_obj_fn linprog (Array.make (!number_of_vars + 1) 0.0) ;
            (* if (O.getValueOfBool "quiet") then () else Lp.lp_print_lp linprog ; *)
            let stillnotdone = ref true in

            let set_minus_inf = ref false in
            while !stillnotdone do
            begin 
              stillnotdone := false ;
              M.msg_string M.Normal "Next round of solving Linprog" ;
              (* now solve linprog *)
              let retcode = (Lp.lp_solve linprog) in
              if retcode = 2 then (* infeasible! *) begin
                (if !set_minus_inf then failwith "LP is infeasible!"
                else set_minus_inf := true );
                let (minus_infinity : float) =  -. (Lp.lp_get_infinite (linprog) ) in
                for i = 1 to !number_of_vars do ignore (Lp.lp_set_lowbo linprog i minus_infinity) done ; 
                stillnotdone := true
                end
              else 
              begin (* check that the solution is integral *) 
(*
              if (O.getValueOfBool "quiet") then () else Lp.lp_print_lp linprog ;
              if (O.getValueOfBool "quiet") then () else Lp.lp_print_solution linprog ;
*)
              let sol_array = Lp.lp_get_solution linprog in
              for i = 1 to !number_of_vars do
                if (floor sol_array.(i)  <> sol_array.(i) ) then
                begin
                  M.msg_string M.Normal "Solution not integral!";
                  stillnotdone := true;
                  ignore (Lp.lp_set_int linprog i 1 ) ; 
                end ; 
              done
              end
            end done ;
            (* ok now we have the map from lvalues to values *)
            let sol = Lp.lp_get_solution linprog in
            let make_list ht sol =
              let l = ref [] in
              for i = 1 to !number_of_vars do
                l := (Hashtbl.find ht i, sol.(i)) :: !l ;
              done ;
              !l
            in
            make_list id_to_sym_ht sol
          end
      end
  | _ -> failwith "generateTest: Assumes conjunction of atomic predicates (not a conjunction)"



*)
