(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
type ('a, 'b, 'c) automaton = {
  aut_id : int;
  mutable start_location : ('a, 'b, 'c) location option;
  mutable end_location : ('a, 'b, 'c) location option;
  mutable aut_attributes : 'a;
}
and ('a, 'b, 'c) location = {
  loc_id : int;
  mutable succs : ('c * ('a, 'b, 'c) location) list;
  mutable preds : ('c * ('a, 'b, 'c) location) list;
  automaton : ('a, 'b, 'c) automaton;
  mutable loc_attributes : 'b;
}

val location_function_name : ('a, 'b, 'c) location -> string
val create_location :
  string -> ('a, 'b, 'c) automaton -> 'b -> ('a, 'b, 'c) location
val create_automaton : 'a -> ('a, 'b, 'c) automaton
val get_start_location : ('a, 'b, 'c) automaton -> ('a, 'b, 'c) location
val get_end_location : ('a, 'b, 'c) automaton -> ('a, 'b, 'c) location
val get_automaton_attributes : ('a, 'b, 'c) automaton -> 'a
val set_start_location :
  ('a, 'b, 'c) automaton -> ('a, 'b, 'c) location -> unit
val set_end_location :
  ('a, 'b, 'c) automaton -> ('a, 'b, 'c) location -> unit
val get_succs : ('a, 'b, 'c) location -> ('c * ('a, 'b, 'c) location) list
val get_preds : ('a, 'b, 'c) location -> ('c * ('a, 'b, 'c) location) list
val get_location_attributes : ('a, 'b, 'c) location -> 'b

val add_edge : ('a, 'b, 'c) location -> ('a, 'b, 'c) location -> 'c -> unit

val remove_edge :
  ('a, 'b, 'c) location -> ('a, 'b, 'c) location -> 'c -> unit
val set_location_attributes : ('a, 'b, 'c) location -> 'b -> unit
