(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate

(* Alias old(?) vampyre stats to usual stats *)
module VampyreStats = Stats

(*
module CilExpressionTranslator =
struct
(* Functions to convert from CIL representation to Expressions and Predicates *)
*)

let cilUOptoUnaryOp op =
  match op with
    Cil.Neg -> Expression.UnaryMinus
  | Cil.BNot -> Expression.BitNot
  | Cil.LNot -> Expression.Not

let cilBOptoBinaryOp op =
  match op with
    Cil.PlusA  -> Expression.Plus                              (** arithmetic + *)
  | Cil.PlusPI -> Expression.Plus                             (** pointer + integer *)
  | Cil.IndexPI -> Expression.Offset                          (** pointer[integer]. The difference 
                                                                  form PlusPI is that in this case 
                                                                  the integer is very likely 
                                                                  positive *)
  | Cil.MinusA -> Expression.Minus                             (** arithemtic - *)
  | Cil.MinusPI -> Expression.Minus                            (** pointer - integer *)
  | Cil.MinusPP -> Expression.Minus                            (** pointer - pointer *)
  | Cil.Mult    -> Expression.Mul                            (** * *)
  | Cil.Div     -> Expression.Div                            (** / *)
  | Cil.Mod     -> Expression.Rem                            (** % *)
  | Cil.Shiftlt -> Expression.LShift                            (** shift left *)
  | Cil.Shiftrt -> Expression.RShift                            (** shift right *)

  | Cil.Lt      -> Expression.Lt                            (** <  (arithmetic comparison) *)
  | Cil.Gt      -> Expression.Gt                            (** >  (arithmetic comparison) *)  
  | Cil.Le      -> Expression.Le                            (** <= (arithmetic comparison) *)
  | Cil.Ge      -> Expression.Ge                            (** >  (arithmetic comparison) *)
  | Cil.Eq      -> Expression.Eq                            (** == (arithmetic comparison) *)
  | Cil.Ne      -> Expression.Ne                            (** != (arithmetic comparison) *)            

(* Not supported in CIL anymore 
  | Cil.LtP     -> Expression.Lt                            (** <  (pointer comparison) *)
  | Cil.GtP     -> Expression.Gt                          (** >  (pointer comparison) *)
  | Cil.LeP     -> Expression.Le                            (** <= (pointer comparison) *)
  | Cil.GeP     -> Expression.Ge                            (** >= (pointer comparison) *)
  | Cil.EqP     -> Expression.Eq                            (** == (pointer comparison) *)
  | Cil.NeP     -> Expression.Ne                            (** != (pointer comparison) *)
*)

  | Cil.BAnd    -> Expression.BitAnd                            (** bitwise and *)
  | Cil.BXor    -> Expression.Xor                            (** exclusive-or *)
  | Cil.BOr     -> Expression.BitOr                            (** inclusive-or *)
  | Cil.LOr    
  | Cil.LAnd    -> failwith "Logical Or/And not supported."

let rec convertCilLval (lb, off) =
  let rec genOffset lvalue o =
    match o with
      Cil.NoOffset -> lvalue
    | Cil.Field (fid, o1) ->
	begin
	  (* try *)
	  if Cil.hasAttribute "lock" fid.Cil.fattr then
	    Message.msg_string Message.Normal ("Lock found! "^fid.Cil.fname) ;
	  let ee = Expression.Access (Expression.Dot, Expression.Lval lvalue, fid.Cil.fname) in
	  genOffset ee o1
	end
    | Cil.Index (e, o1) ->
	begin
	  let iexp = convertCilExp e in
	  let ee = Expression.Indexed (Expression.Lval lvalue, iexp) in
	  genOffset ee o1
	end
  in
  
  
  match lb with
    Cil.Var vinfo -> 
      begin
	let m = Expression.Symbol vinfo.Cil.vname in
	genOffset m off
      end
  | Cil.Mem m ->
      begin
	let _lval_mem_worker m off =
	  match off with
	    Cil.NoOffset -> Expression.Dereference m
	  | Cil.Field (finfo, offset) -> 
	      begin
                (* Have only Dot's, no Arrow's *)
	  if Cil.hasAttribute "lock" finfo.Cil.fattr then
	    Message.msg_string Message.Normal ("Lock found! "^finfo.Cil.fname) ;
                let e = Expression.Access 
		    (Expression.Dot, Expression.Lval (Expression.Dereference m), finfo.Cil.fname) in 
		genOffset e offset
	      end
	  | Cil.Index (exp, offset) -> 
	      begin
		let e = (Expression.Indexed (m, convertCilExp exp)) in
		genOffset e offset
	      end
	in
	let m = convertCilExp m
	in
	_lval_mem_worker m off
      end

and convertCilConst c =
  match c with
      Cil.CInt64 (i64, _, _) -> Constant.Int (Int64.to_int i64)
    | Cil.CStr str -> Constant.String str
    | Cil.CChr c   -> Constant.Int (int_of_char c) (* Check : why dont we have char constants? *)
    | Cil.CReal (f,_,_) -> Constant.Float f
    | Cil.CWStr i64list -> failwith "CIL: Wide character constant not handled yet"

and convertCilExp e =
  match e with
    Cil.Const const ->
      Expression.Constant (convertCilConst const)
  | Cil.Lval (lb,loff) ->
      begin
	Expression.Lval (convertCilLval (lb,loff))
      end
  | Cil.SizeOf t -> 
    begin
    try
      Expression.Constant 
	(Constant.Int 
	   (Int32.to_int (Int32.shift_left (Int32.of_int (Cil.bitsSizeOf t))  3))) (* size in bytes *)
    with Cil.SizeOfError(s,_) -> 
      Message.msg_string Message.Error ("Cil SizeofError: "^s); Expression.Constant (Constant.Int 4 )
    end
  | Cil.SizeOfE exp ->
    begin
    try
      Expression.Constant
      (Constant.Int
         (Int32.to_int (Int32.shift_left (Int32.of_int (Cil.bitsSizeOf (Cil.typeOf exp)))  3))) (* size in bytes *)
    with Cil.SizeOfError(s, i) -> failwith ("Cil Sizeoferror: "^s) 
    end
  | Cil.AlignOf t -> 
    begin 
      Message.msg_string Message.Error "convertCilExp: align not handled. Replaced by sizeof";
      (convertCilExp (Cil.SizeOf t)) 
    end
  | Cil.AlignOfE e -> 
    begin 
      Message.msg_string Message.Error "convertCilExp: AlignOfE not handled. Replaced by SizeOfE";
      (convertCilExp (Cil.SizeOfE e)) 
    end
  | Cil.UnOp (op, exp, _) ->
      let uop = cilUOptoUnaryOp op and e = convertCilExp exp in
      Expression.Unary(uop, e)
  | Cil.BinOp (op, e1,e2,_) ->
      let bop = cilBOptoBinaryOp op in
      let (ex1, ex2) = (convertCilExp e1, convertCilExp e2) in
      Expression.Binary(bop, ex1, ex2)
  | Cil.CastE (t, e) -> convertCilExp e (* drop casts for now *)
  | Cil.AddrOf lval -> 
      let l = convertCilLval lval in
      Expression.addressOf (Expression.Lval l)
  | Cil.StartOf lv -> Expression.Lval (convertCilLval lv)
  | Cil.SizeOfStr s -> Expression.Constant (Constant.Int (String.length s))


let convertCilAttrToExpList a : (string * (Expression.expression list)) =
  let rec convertCilAttrparam ap =
    match ap with
      Cil.AInt i -> Expression.Constant (Constant.Int i)
    | Cil.AStr s -> Message.msg_string Message.Debug ("In AStr, string is "^s); (Expression.Lval (Expression.Symbol s))
    | Cil.ACons (s,aplist) -> Message.msg_string Message.Debug ("In ACons, string is "^s); 
	if (aplist = []) then (Expression.Lval (Expression.Symbol s)) else
	begin
	match s with
          "sel" -> Expression.Lval (Expression.Dereference (convertCilAttrparam (List.hd aplist)))
	| "at" -> 
	    begin
	      let s1 = convertCilAttrparam (List.nth aplist 0) and 
		  s2 = convertCilAttrparam (List.nth aplist 1) in
	      match (s1, s2) with
		(Expression.Lval (Expression.Symbol s), Expression.Lval (Expression.Symbol s'))  ->
		  Expression.Lval (Expression.Symbol (s^"@"^s'))
	      |	_ -> failwith "convertCilAttrparam : problem in at"
	    end
	| "arrow" ->
	    begin 
	      let s1 = convertCilAttrparam (List.nth aplist 0) and
		  s2 = convertCilAttrparam (List.nth aplist 1) in
	      match s2 with
		Expression.Lval (Expression.Symbol s) ->
		  Expression.Lval (Expression.Access (Expression.Arrow, s1, s))
	      | _ -> failwith "convertCilAttrparam : problem in arrow"	
	    end
	| "dot" ->
	    begin 
	      let s1 = convertCilAttrparam (List.nth aplist 0) and
		  s2 = convertCilAttrparam (List.nth aplist 1) in
	      match s2 with
		Expression.Lval (Expression.Symbol s) ->
		  Expression.Lval (Expression.Access (Expression.Dot, s1, s))
	      | _ -> failwith "convertCilAttrparam : problem in arrow"	
	    end
          | _ -> 
	      begin
		Message.msg_string Message.Error ("Error in parsing attributes: Strange function call "^s) ;
		Expression.FunctionCall (Expression.Lval (Expression.Symbol s), List.map convertCilAttrparam aplist)
	      end 
	end
    | Cil.ASizeOf _ -> failwith "convertCilAttrparam: SizeOf not handled"
    | Cil.ASizeOfE _ -> failwith "convertCilAttrparam: SizeOfE not handled"
    | Cil.ASizeOfS _ -> failwith "convertCilAttrparam: SizeOfE not handled"
    | Cil.AAlignOf _ | Cil.AAlignOfE _ | Cil.AAlignOfS _ -> failwith "Alignof not handled in attr"
    | Cil.AUnOp (uop, at) -> Expression.Unary (cilUOptoUnaryOp uop, convertCilAttrparam at)
    | Cil.ABinOp (bop, at1, at2) -> Expression.Binary (cilBOptoBinaryOp bop, convertCilAttrparam at1, convertCilAttrparam at2)
    | Cil.ADot (aparam, s) -> failwith "HERE"
  in
  match a with
    Cil.Attr (name, alist) -> 
      Message.msg_string Message.Debug ("Name of attribute is "^name) ; 
      (name, List.map convertCilAttrparam alist)



let rec convertCilExpToPred e =
  (* Greg: I modify this to make use of convertCilExp.
           This way, we just have to make sure convertCilExp works and then
           this one will follow *)     
  let rec convertExpToPred exp =
    match exp with
        Expression.Binary (bop, e1, e2) ->
          if (Expression.isRelOp bop) then
            Predicate.Atom exp
          else 
            Predicate.Atom (Expression.Binary(Expression.Ne,
                                              exp,
                                              Expression.Constant (Constant.Int 0)))
      | Expression.Unary (uop, e1) ->
          begin
            match uop with
                Expression.Not ->
                  Predicate.negate (convertExpToPred e1)
              | _ ->
                  Predicate.Atom (Expression.Binary(Expression.Ne,
                                                    exp,
                                                    Expression.Constant (Constant.Int 0)))
          end
      | _ ->
          Predicate.Atom (Expression.Binary(Expression.Ne,
                                            exp,
                                            Expression.Constant (Constant.Int 0)))
  in
  convertExpToPred (convertCilExp e)

(*******************************************************************************)


class oneret_visitor = object
  inherit Cil.nopCilVisitor 
  method vfunc f =
    Oneret.oneret f ; Cil.SkipChildren
end


(* [Greg] This is very dirty, but it's the only way to know whether Frontc
   encountered an error *)

exception FrontcError of Cil.location

class frontc_error_detection_visitor = object
  inherit Cil.nopCilVisitor 
  method vinst i =
    match i with
        (* Check the Asm to see wether frontc encountered an error *)
        Cil.Asm (_, templates, _, _, _, loc) ->
          begin
            if ((List.length templates) == 1) then
              if (Misc.is_prefix "booo_exp(" (List.hd templates)) then
                raise (FrontcError loc)
          end ;
          Cil.SkipChildren
      | _ ->
          Cil.SkipChildren
end

let rename_locals f =
  Cil.iterGlobals f (fun g -> match g with
    Cil.GFun(fd,_) -> 
  let fnName = fd.Cil.svar.Cil.vname in
  let locals = List.map (fun var -> (var.Cil.vname <- (var.Cil.vname^"@"^fnName));var) fd.Cil.slocals
  and formals = List.map (fun var -> (var.Cil.vname <- (var.Cil.vname^"@"^fnName));var) fd.Cil.sformals
  in
  fd.Cil.slocals <- locals ;
  fd.Cil.sformals <- formals
  | _ -> ())

let make_cfg f =
    Cil.iterGlobals f 
    (fun glob -> match glob with
      Cil.GFun(fd,_) ->
	Cil.prepareCFG fd ; Cil.computeCFGInfo fd false 
    | _ -> ())


let junk_location = {Cil.line = 0; Cil.file = ""; Cil.byte = 0; }
let diverge = Cil.Loop ({Cil.bstmts = []; Cil.battrs = []}, junk_location, None, None)

module VarSet = Set.Make(struct
  type t = Cil.varinfo
  let compare v1 v2 = compare v1.Cil.vid v2.Cil.vid
end)

class add_symbolic_constants file =
(* globals are collected but never used! Commenting out. Rupak.
  let folder globals global =
    match global with
      Cil.GVar (v, _, _) -> VarSet.add v globals
    | Cil.GVarDecl (v, _) -> VarSet.add v globals
    | _ -> globals in
  let globals = Cil.foldGlobals file folder VarSet.empty in
  let globals = VarSet.elements globals in
*)    
  object
    inherit Cil.nopCilVisitor
	
    method vfunc fundec =
      let mk_symbolic formal =
	let name = formal.Cil.vname ^ "#" in
	Cil.makeLocalVar fundec name formal.Cil.vtype in
      Cil.SkipChildren
  end


let inits = ref []
class initGlobals =
  object
    inherit Cil.nopCilVisitor

    method vglob global =
      let rec init_one_offset lvalu offset initval loc =
         match initval with 
           Cil.SingleInit e -> 
		inits := (Cil.Set ((Cil.addOffsetLval offset lvalu), e, loc)) :: !inits
         | Cil.CompoundInit (t, offset_init_list) ->  
             compound_init (Cil.addOffsetLval offset lvalu) t offset_init_list loc
      and
      compound_init lvalu ty offset_init_list loc =
        match ty with
          Cil.TArray _ 
        | Cil.TComp _ -> 
           begin
             List.iter (fun (offset, initval) -> init_one_offset lvalu offset initval loc) 
                offset_init_list 
           end
        | Cil.TNamed (tinfo,_) ->
           begin
             let t = Cil.unrollType ty in compound_init lvalu t offset_init_list loc
           end 
        | Cil.TPtr _ -> Message.msg_string Message.Error "Compound initializer for TPtr not propagated"
        | Cil.TFun _ -> Message.msg_string Message.Error "Compound initializer for TFun not propagated"
        | _ -> Message.msg_string Message.Error "Compound initializer not propagated"
      in
      (match global with
	Cil.GVar (vinfo, init, location) ->
	  begin
           match init.Cil.init with
	    Some (Cil.SingleInit e) -> inits := Cil.Set ((Cil.Var vinfo, Cil.NoOffset), e, location) :: !inits
	  | Some (Cil.CompoundInit (t, offset_init_list)) -> 
              if (Options.getValueOfBool "initialize") then 
                compound_init (Cil.Var vinfo, Cil.NoOffset) t offset_init_list location
	  | None -> ()
          end
      |	_ -> ());
      Cil.SkipChildren
  end


(** add all global initializations as a function __BLAST_initialize () *)

(** if there are several input files, then each file would have
    an initializer. In this case, we generate
    unique names for the initializers by appending the file name.
 
    We maintain all the initializers in a set, and call_initializer
    calls all initializers from main.
	PROBLEM : CAN THERE BE DEPENDENCIES BETWEEN INITIALIZERS?
	We assume there is no dependency between initializers in
	different files... this is sound
*)
let __initializers = ref [] (* VarSet.empty  *)

let call_initializers cil_file = 
  let get_fundec file name =
    let rec search = function
	[] -> raise (Failure "Function declaration not found")
      | (Cil.GFun (fundec, _))::rest ->
	  if compare (fundec.Cil.svar.Cil.vname) name == 0 then
	    fundec
	else
	    search rest
      | _::rest -> search rest
    in search file.Cil.globals
  in
  (try 
    let main = get_fundec cil_file (List.hd (Options.getValueOfStringList "main")) in
    let nullLocation = { Cil.line = 0 ; Cil.file = cil_file.Cil.fileName ; Cil.byte = 0; } in
    let newstmts = List.fold_left (fun listsofar -> fun elt ->
               (Cil.Call (None, Cil.Lval (Cil.Var elt, Cil.NoOffset), [], nullLocation)) :: listsofar)
             [] !__initializers 
    in
    __initializers := [] ;
    main.Cil.sbody <- {main.Cil.sbody with
                       Cil.bstmts = Cil.mkStmt (Cil.Instr newstmts) :: main.Cil.sbody.Cil.bstmts} ;
    Cil.prepareCFG main ; Cil.computeCFGInfo main false 
  with _ -> ()) ;
  cil_file

let put_in_initializer cil_file =
  let nullLocation = { Cil.line = 0 ; Cil.file = cil_file.Cil.fileName ; Cil.byte = 0; } in
  let initer = Cil.emptyFunction ("__BLAST_initialize_"^cil_file.Cil.fileName) in
  cil_file.Cil.globals <- Cil.GVarDecl (initer.Cil.svar, nullLocation) :: 
    (cil_file.Cil.globals @ [ Cil.GFun (initer, nullLocation) ]) ;
  inits := [] ;
  let ig = new initGlobals 
  in
  let cil_file = Cil.visitCilFile ig cil_file in
  initer.Cil.sbody <- {Cil.battrs = []; Cil.bstmts = [Cil.mkStmt (Cil.Instr (List.rev (!inits )))]};
  inits := [] ;
  Message.msg_string Message.Normal ("Putting in initializer "^(initer.Cil.svar.Cil.vname)) ;
  __initializers := initer.Cil.svar :: !__initializers ;
  cil_file 

(* FUNCTION CALL REROUTER *)
(* Reroute calls (in cil_file) of functions from cuts to clones of template, which are made by replacing special template placeholder with keys calculated based on actual call.  The actual function to call is reroute_calls. *)
(* MapCut func - when a function func is called, replace call with func_KEY, where KEY is calculated based on arguments of the call.  For this to work, func_TEMPLATE should present in the source code *)
(* ForeachCut func - when a function func is called, replace it with a sequence of calls func_KEY, for each KEY, which was calculated for map cuts.  For this to work, func_TEMPLATE should present in the source code *)
(* The "TEMPLATE" specifier above is an argument to "reroute_calls", and may be changed. *)


type cut_key_arg_t = All_Args | Specific_Arg of int

type cut_t = MapCut of cut_key_arg_t  | ForeachCut

(* generic utility helpers *)
(* helper functions to add "_TEMPLATE" specifier and remove it or replace it with another string *)
exception NoPlaceholderFound
let templatize plh name = (name^"_"^plh)
let detemplatize plh ?(r = "") name = Str.global_replace (Str.regexp ("_"^plh)) ("_"^r) name
(* utility function for string (how come standard lib doesn't have it?? *)
let str_contains needle haystack =
  try let _ = Str.search_forward (Str.regexp_string needle) haystack 0
    in true
  with Not_found -> false

(* CIL utility helpers *)

(* find global variable in the cil-file specified *)
let get_global_variable file name =
  let rec search glist =
    match glist with
	Cil.GVarDecl(varinfo,_) :: rest when varinfo.Cil.vname = name -> varinfo
      | _ :: rest -> search rest (* tail recursive *)
      | [] -> failwith (Printf.sprintf "Variable %s not found!" name)
  in
  search file.Cil.globals

(* Finds global function in cil-file specified.  Throws if not found *)
let find_function file name =
  let rec search glist =
    match glist with
	Cil.GFun(fundec,_) :: rest when fundec.Cil.svar.Cil.vname = name -> fundec
      | _ :: rest -> search rest (* tail recursive *)
      | [] -> failwith (Printf.sprintf "Function %s not found!" name)
  in
  search file.Cil.globals

(* Converts CIL location to string *)
let string_of_cil_loc loc =
  Printf.sprintf "%s: %i" loc.Cil.file loc.Cil.line



(* Actual cloning/visiting functionality *)

(* CIL visitor (see cil/src/cil.ml) that only gathers information about calls and stores it inside cuts_info mutable object *)
class rerouteInfoVisitor cuts_info = object (self)
  inherit Cil.nopCilVisitor

  (* visits instruction *)
  method vinst = function
    Cil.Call (result, (Cil.Lval (Cil.Var vi,Cil.NoOffset) as name), args, location) ->
      let _ = cuts_info#check vi args in
      Cil.DoChildren
    | _ -> Cil.DoChildren
end

(* CIL visitor that actually reroutes calls based on information in cuts_info (which was gathered during previous visitor work and refining. *)
class rerouteVisitor cuts_info = object (self)
  inherit Cil.nopCilVisitor

  method vinst = function
    Cil.Call (result, (Cil.Lval (Cil.Var vi,Cil.NoOffset) as name), args, location) ->
      let reroute_to = cuts_info#reroute vi args in
      (* just print info about rerouting that happened *)
      let ff = vi.Cil.vname in let tf = reroute_to.Cil.vname in if (ff <> tf) then Message.msg_string Message.Normal (Printf.sprintf "Rerouting %s to %s at %s" ff tf (string_of_cil_loc location) );
      Cil.ChangeTo [(Cil.Call(result, (Cil.Lval (Cil.Var reroute_to,Cil.NoOffset)), args, location))]
    | other -> Cil.DoChildren
  (* methos vinst ends *)

end

(* Exception means that the argument of the map cut specified isn't of form we can extract the key from *)
exception InvalidCutKey

(* given arguments of a function call, provide a key by concatenating keys of each argument (or of the specific one) *)
let make_map_cut_key which args =
  (* Cil has a creepy way of offset representation.  Each address expression and each lval contains the "basic" part and "offset" part, and offsets can be chained.  The creepiness is that they're laid out not in the natural way, but in the reverse way.  In BLAST a natural way is that expression a->b->c is "Field c of (Field b of a)".  In Cil this means "a with offset of (b with offset of (c with NoOffset))".  Hence such a tricky processing *)
  let rec key_of_offset prev = function
     Cil.NoOffset -> (* not a structure *) prev
    |Cil.Field (finfo,more_offset) -> (* this may be another field offset *) key_of_offset finfo.Cil.fname more_offset
    | Cil.Index (exp, offset) -> 
      begin
	(* TODO support array indices*)
        Message.msg_string Message.Normal ("Warning: array index not supported in key_of_offset");
        key_of_offset "array_index_expression" offset 
      end        
  and key_of_lval (lb,off) = match lb with
     Cil.Var vinfo -> key_of_offset vinfo.Cil.vname off
    |Cil.Mem m -> key_of_offset (key_of_exp m) off
  and key_of_exp = function
     Cil.Lval ((_,_) as lv) | Cil.AddrOf( (_,_) as lv) -> key_of_lval lv
    | _ -> 
   raise InvalidCutKey
  in
   let res = match which with
       Specific_Arg(nm) -> key_of_exp (List.nth args nm)
     | All_Args -> String.concat "_" (List.map key_of_exp args)
    in
        Message.msg_string Message.Debug ("Result of make_map_cut_key is "^res);
	res

(* dummy function for making a foreach cut key.  Result is not used, actually *)
let make_foreach_cut_key vi =
  vi.Cil.vname

(* extracts cut key from variable info *)
let make_cut_key cut_type vi args =
  match cut_type with
    MapCut which -> make_map_cut_key which args
    |ForeachCut -> make_foreach_cut_key vi

(* Visits function and replaces references to variables with placeholder in their name with new_varinfo passed to it *)
class templateReplaceVisitor placeholder vi_maker = object (self)
  (* about vi_maker -- see below *)
  inherit Cil.nopCilVisitor
  (* visit lvalue *)
  method vlval (host,off) = match host with
     Cil.Var vinfo when str_contains placeholder vinfo.Cil.vname && vinfo.Cil.vglob -> Cil.ChangeTo (Cil.Var (vi_maker#info_for vinfo),off)
    | _ -> Cil.DoChildren
	(* vlval method ends *)

end

(* returns object that generates new global variables.  For each key supplied, it clones global variable from its template (writter with use of placeholder), or returns an already cloned one, if such thing already exists *)
let get_vi_maker get_global make_global placeholder key =
  object (self)
    val infos = Hashtbl.create 11

    method new_varinfo vinfo =
      let var_name = vinfo.Cil.vname in
      let new_name = detemplatize placeholder ~r:key var_name in
      Message.msg_string Message.Normal (Printf.sprintf "Rerouter: copying variable %s to %s" var_name new_name);
      let new_typ = (get_global var_name).Cil.vtype in
      make_global new_name new_typ

    method info_for vinfo =
      let name = vinfo.Cil.vname in
      try Hashtbl.find infos name
      with Not_found -> let vi = self#new_varinfo vinfo in let _ = Hashtbl.add infos name vi in vi
  end

(* in all identifier names encountered in the function, replace all occurences of plh with key *)
let function_replace_names plh key file new_func =
  (* create structure responsible for creating new global variables referenced from the templated function *)
  let make_global_variable file name typ =
    let new_varinfo = Cil.makeGlobalVar name typ in
    let _ = file.Cil.globals <- Cil.GVarDecl(new_varinfo, Cil.locUnknown) :: file.Cil.globals in
    new_varinfo
  in
  let varinfo_maker = get_vi_maker (get_global_variable file) (make_global_variable file) plh key in
  Cil.visitCilFunction (new templateReplaceVisitor plh varinfo_maker) new_func

(* placeholder that is used for foreach aggregators generation *)
let foreach_ph = "FOREACH"

(* generate foreach aggregator function--i.e. the function that calls all copies of the relevant cut.  The cut is called once for each key specified in keys list.  Note that clonse aren't generated here; instead, if a clone is not found in the cil-filoe specified, the funciton throws. *)
let foreach_for_name cil_file placeholder keys name =
  let new_name = detemplatize placeholder ~r:foreach_ph name in
  (* default location at which new functions will be asigned.  It's not referenced in BLAST CFA anyway *)
  let nullLocation = { Cil.line = 0 ; Cil.file = cil_file.Cil.fileName ; Cil.byte = 0; } in
  (* creates new function, which will be populated with cut clone calls and returned frin foreach_for_name *)
  let new_function = Cil.emptyFunction new_name in
  (* returns statement that calls clone of a specific key *)
  let call_of key =
    let callee_name = detemplatize placeholder ~r:key name in
    Message.msg_string Message.Debug (Printf.sprintf "Looking for %s function" callee_name);
    let callee_varinfo = (find_function cil_file callee_name).Cil.svar in
    (* We imply that argument list is empty, as we don't specify any rules on how it's cloned. *)
    Cil.Call (None, (Cil.Lval (Cil.Var callee_varinfo,Cil.NoOffset)), [], nullLocation)
  in
  Message.msg_string Message.Debug (Printf.sprintf "Rerouter: creating foreach clone for %s as %s" name new_name);
  new_function.Cil.sbody <- {Cil.battrs = []; Cil.bstmts = Cil.compactStmts [Cil.mkStmt (Cil.Instr (List.map call_of keys))] };
  (*new_function.Cil.sbody <- {Cil.battrs = []; Cil.bstmts = [] };*)
  new_function

(* does the actual reroute: scans cil_file for cuts, generates clones and reroutes the relevant calls*)
let gather_reroute_info reroute_info cil_file cuts template placeholder =
  let proper_cut cut_type function_name cut_list =
    List.mem (cut_type, function_name) cut_list
  in
  (* Create a mutable collector of cuts information.  It's passed into collectors of information and into rerouters *)
  (* Of course, if it was already created, just store it *)
  let cuts_info = match reroute_info with Some info -> info | None -> object (self)
    val cuts = cuts

    val mutable map_keys = []
    val mutable foreach_keys = []
    method mpk = map_keys
    method fek = foreach_keys
    val __tmp_hashtbl_size = 11

    val pput = templatize placeholder
    val pget = detemplatize placeholder
    val foreach_name = detemplatize placeholder ~r:foreach_ph
    val fput = templatize foreach_ph

    method get_keys vi args =
      let function_name = vi.Cil.vname in
      try
        let (cut_type,_) = List.find (fun (_,fname_to_cut) -> function_name = fname_to_cut) cuts in
	Some (cut_type, make_cut_key cut_type vi args)
      with Not_found -> None

    (* reroutes function with the relevant varinfo to the proper varinfo *)
    method reroute func_varinfo args =
      (* we need to form new fname, because vname contains "non-template" version of the name (ex. "mutex_lock"), while we should route to a templated one ("mutex_lock_TEMPLATE") *)
      let fname = pput func_varinfo.Cil.vname in
      match (self#get_keys func_varinfo args) with
	None -> func_varinfo (*change nothing*)
	|Some (MapCut(_) as mc,map_key) -> (Cil.findOrCreateFunc cil_file (pget ~r:map_key fname) func_varinfo.Cil.vtype)
	|Some (ForeachCut,foreach_key) -> (Cil.findOrCreateFunc cil_file (foreach_name fname) func_varinfo.Cil.vtype)

    (* check call of function with varinfo vi against the cuts specified, and store information about it *)
    method check vi args =
      match (self#get_keys vi args) with
	None -> ()
	|Some (MapCut(_) as mc,map_key) -> map_keys <- map_key::map_keys (* store with repetition and compact later *)
	|Some (ForeachCut,foreach_key) -> foreach_keys <- foreach_key::foreach_keys

    (* compacts arrays of cut keys (see method check) *)
    method compact () =
      map_keys <- Misc.hashtbl_keys (Misc.hashtbl_of_list __tmp_hashtbl_size map_keys);
      foreach_keys <- Misc.hashtbl_keys (Misc.hashtbl_of_list __tmp_hashtbl_size foreach_keys)

    (* generate clones for the keys accumulated during previous information gathering.  Generated both map clones (one for each key-cut pair) and foreach-clones (one for each pair key-cut).  Both foreach and map clones are generated for map keys. *)
    method generate_functions cil_file =
      (* put and get placeholder *)
      let fname fundec = fundec.Cil.svar.Cil.vname in
      (* this clones the templated function *)
      let copy_function def key =
	Message.msg_string Message.Normal (Printf.sprintf "Rerouter: copying %s with %s to %s" (fname def) key (pget ~r:key (fname def)));
	let new_name = pget ~r:key (fname def) in
	let new_func = Cil.copyFunction def new_name in
	function_replace_names placeholder key cil_file new_func
      in
      let keys = self#mpk in
      let functions_to_copy = List.map (fun x -> pput (snd x) (* I WANT HASKELL'S INFIX COMPOSITION !!! *)) cuts in
      (* Get list of declarations of templates (list of lists to use flatten below) *)
      let all_declarations_to_copy = List.map (fun x -> try [find_function cil_file x] with _ -> []) functions_to_copy in
      (* Get list of functions that actually exist in this file (some of them may be in other files) *)
      let declarations_to_copy = List.flatten all_declarations_to_copy in
      (* actually create cloned declarations *)
      let clones = List.flatten (List.map (fun d -> List.map (copy_function d) keys) declarations_to_copy) in
      (* create a spurious location for all clones
         Note that when we clone functions later, they will use the same locations as the original functions.  This particular location is not used in BLAST *)
      let null_loc = { Cil.line = 0 ; Cil.file = cil_file.Cil.fileName ; Cil.byte = 0; } in
      (* Put clones into the file *)
      cil_file.Cil.globals <- (List.map (fun f -> Cil.GVarDecl(f.Cil.svar,null_loc)) clones) @ cil_file.Cil.globals @ (List.map (fun f -> Cil.GFun(f,null_loc)) clones);
      (* Create "foreach" accumulators and put them into the file *)
      let foreach_names = List.map (fun x -> pput (snd x)) (List.filter (fun (cut_type,_) -> cut_type = ForeachCut) cuts) in
      (* do the same try-flatten trick as above *)
      let foreaches = List.flatten (List.map (fun x -> try [foreach_for_name cil_file placeholder self#mpk x] with _ -> []) foreach_names) in
      (*let foreaches = List.map (fun x -> foreach_for_name cil_file placeholder self#mpk x) foreach_names in*)
      cil_file.Cil.globals <- (List.map (fun f -> Cil.GVarDecl(f.Cil.svar,null_loc)) foreaches) @ cil_file.Cil.globals @ (List.map (fun f -> Cil.GFun(f,null_loc)) foreaches)

  end in
  (* The actual rerouting workflow *)
  Message.msg_string Message.Normal "Gathering reroute info..." ;
  Cil.visitCilFile (new rerouteInfoVisitor cuts_info) cil_file;
  cuts_info#compact ();
  (cil_file,Some cuts_info)

(* based on the cuts_info gathered throughout the first pass, generate clones and reroute calls to them *)
let reroute_calls cuts_info_o cil_file =
  match cuts_info_o with
    None -> let _ = Message.msg_string Message.Normal (Printf.sprintf "No reroute info supplied!") in cil_file
    | Some cuts_info -> begin
      Message.msg_string Message.Normal (Printf.sprintf "Rerouting keys: %s" (String.concat ", " cuts_info#mpk));
      (* generate clones *)
      cuts_info#generate_functions cil_file;
      Cil.visitCilFile (new rerouteVisitor cuts_info) cil_file;
      cil_file
    end


(***************************************************************************)
(* The following visitor and the function simplify changes the use of      *)
(* booleans as integers in C                                               *)
(* programs. For example, it changes code like:                            *)
(* x = (y < 0);                                                            *)
(* to if (y<0) x = 1; else x = 0;                                          *)
(* and if ((a<0) + !f >= ...) ... to                                       *)
(* if .. *)
(* ignoring to ask why anyone would write this?                            *)
(*                                                                         *)
(* CAUTION: This calls CIL's Simplify, don't know what can of worms that   *)
(*          will open!                                                     *)
(***************************************************************************)
(* Commented out because now more thorough transformLExprs visitor is used *)
(* instead.
 
class changeBoolVisitor =
  object (self)
   inherit Cil.nopCilVisitor
   
   method vinst _ = Cil.SkipChildren
   
   method vstmt stmt =
   begin
   let rewrite = function
     | Cil.Set (result, (Cil.UnOp (Cil.LNot, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Lt, _, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Gt, _, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Le, _, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Ge, _, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Eq, _, _, _) as pred), location)
     | Cil.Set (result, (Cil.BinOp (Cil.Ne, _, _, _) as pred), location) ->
       let assign value =
         Cil.mkBlock [Cil.mkStmtOneInstr (Cil.Set (result, value, location))]
       in
       (true, Cil.If (pred, assign Cil.one, assign Cil.zero, location))
     | other -> (false, Cil.Instr [other])
   in
   let rewrite_return e location =
     match e with
     | Cil.UnOp (Cil.LNot, _, _) 
     | Cil.BinOp (Cil.Lt, _, _, _) 
     | Cil.BinOp (Cil.Le, _, _, _) 
     | Cil.BinOp (Cil.Gt, _, _, _) 
     | Cil.BinOp (Cil.Ge, _, _, _) 
     | Cil.BinOp (Cil.Eq, _, _, _) 
     | Cil.BinOp (Cil.Ne, _, _, _) -> 
        let ret value =
         Cil.mkBlock [Cil.mkStmt (Cil.Return (Some value, location))]
       in
       Cil.If (e, ret Cil.one, ret Cil.zero, location)
     | _ -> Cil.Return (Some e, location)
   in
   match stmt.Cil.skind with
   | Cil.Instr [instruction] ->
      stmt.Cil.skind <- snd (rewrite instruction)
   | Cil.Instr instructions ->
      let  flag_stmts = List.map 
        (fun inst -> let (f,s) = rewrite inst in (f, Cil.mkStmt s)) instructions in
      if List.exists (fun (a, _) -> a= true) flag_stmts then
        stmt.Cil.skind <- Cil.Block (Cil.mkBlock (List.map (fun (a,b) -> b) flag_stmts) )
   | Cil.Return (Some e, l) ->
      stmt.Cil.skind <- rewrite_return e l
   | _ -> ()
   end;
   Cil.DoChildren
end

(*********************************************************************)
*)

(**
 * CIL visitor that transforms some `boolean' subexpressions
 * to have explicit integer values 1 or 0. 
 * It's needed because changeBoolVisitor works on rather simple 
 * expressions with one implicit int/boolean conversion only.
 *)

(* Introduces new temporary local variable and makes an `if' statement for explicit conversion *)
let mkExplicit exp vTrue vFalse fundec =
  let v = Cil.var (Cil.makeTempVar fundec Cil.intType) in
  let mkAssignmentBlock e = Cil.mkBlock [(Cil.mkStmtOneInstr (Cil.Set (v, e, Cil.locUnknown)))] in
  (Cil.Lval v, 
   [Cil.mkStmt (Cil.If (exp, mkAssignmentBlock vTrue, mkAssignmentBlock vFalse, Cil.locUnknown))])
 
(* Transforms a whole expression `strictly', i.e converts each suspicious subexpression *)
let rec transformExpression exp fundec =
  match exp with
    Cil.SizeOfE e -> let (e', ss') = transformExpression e fundec in (Cil.SizeOfE e', ss')
  | Cil.AlignOfE e -> let (e', ss') = transformExpression e fundec in (Cil.AlignOfE e', ss')
    (* Double negation elimination *)
  | Cil.UnOp (Cil.LNot, Cil.UnOp (Cil.LNot, e, _), _) -> 
      let (e', ss') = transformExpression e fundec in (e', ss')
    (* Explicit logical negation emulation on integers *)
  | Cil.UnOp (Cil.LNot, e, _) -> 
      let (e', ss') = transformCondition e fundec in
      let (e'', ss'') = mkExplicit e' Cil.zero Cil.one fundec in
      (e'', ss' @ ss'')
    (* Other unary operations just require integers *)
  | Cil.UnOp (unop, e, t) -> let (e', ss') = transformExpression e fundec in (Cil.UnOp (unop, e', t), ss')
  | Cil.BinOp (binop, e1, e2, t) -> (
      let (e1', ss1') = transformExpression e1 fundec and
          (e2', ss2') = transformExpression e2 fundec in
      match binop with
        (* Explicitly convert comparison result to either 1 or 0 *)
        Cil.Lt | Cil.Gt | Cil.Le | Cil.Ge | Cil.Eq | Cil.Ne -> 
          let (e', ss') = mkExplicit (Cil.BinOp (binop, e1', e2', t)) Cil.one Cil.zero fundec in
          (e', ss1' @ ss2' @ ss')
      | _ -> (Cil.BinOp (binop, e1', e2', t), ss1' @ ss2'))
  | Cil.CastE (t, e) -> let (e', ss') = transformExpression e fundec in (Cil.CastE (t, e'), ss')
  | _ -> (exp, []) (* Other cases don't contain subexpressions *)

(* Transforms expression in if statement. Here some cheats are possible due to later *)
(* expression/predicate transformation in convertCilExpToPred. *)
(* For instance, here we can let exprs such as (! (a & b)) remain as they are. *)
and transformCondition exp fundec =
  match exp with
    (* Eliminate double negation *)
	Cil.UnOp (Cil.LNot, Cil.UnOp (Cil.LNot, e, _), _) -> transformCondition e fundec 
  | Cil.UnOp (unop, e, t) -> (
      match unop with
        Cil.Neg | Cil.BNot -> let (e', ss') = transformExpression e fundec in (Cil.UnOp (unop, e', t), ss')
      | Cil.LNot -> let (e', ss') = transformCondition e fundec in (Cil.UnOp (unop, e', t), ss'))
  | Cil.BinOp (binop, e1, e2, t) ->
      let (e1', ss1') = transformExpression e1 fundec and 
          (e2', ss2') = transformExpression e2 fundec in 
      (Cil.BinOp (binop, e1', e2', t), ss1' @ ss2')
  | _ -> transformExpression exp fundec

(* Transforms statement sequence using tail recursion *)
let rec transformStatements src dst fundec =
 match src with
   stmt::stmts -> (
     match stmt.Cil.skind with
       Cil.Instr instrs ->
           (* Transform instruction sequence into *statement sequence* tail recursively *)
           (* Here idst is a buffer list for current instructions that don't require additional statements, *)
           (* and sdst is the resultant statement list *)
           let rec transformInstructions isrc idst sdst =
             match isrc with
               instr::instrs -> (
                 (* Helper function *)
                 (* Transform instrucions in idst to statement if necessary and continue *)
                 let continue instr' ss' = (
                   match ss' with
                     [] -> transformInstructions instrs (idst @ [instr']) sdst (* There were no transformation *)
                   | _  -> (
                       match idst with
                         [] -> transformInstructions instrs [instr'] (sdst @ ss')
                       |  _ -> transformInstructions instrs [instr'] (sdst @ [(Cil.mkStmt (Cil.Instr idst))] @ ss')))
                 in
                 match instr with
                   Cil.Set (l, e, loc) -> 
                     let (e', ss') = transformExpression e fundec in
                     continue (Cil.Set (l, e', loc)) ss' 
                 | Cil.Call (l, e1, es, loc) ->
                     let (e1', ss1') = transformExpression e1 fundec in
                     let (es', ss') = List.fold_left (fun (es, ss) e -> 
                                                        let (e', s') = transformExpression e fundec in
                                                        (es @ [e'], ss @ s'))
                                                     ([], ss1')
                                                     es 
                     in
                     continue (Cil.Call (l, e1', es', loc)) ss'
                 | _ -> transformInstructions instrs (idst @ [instr]) sdst)
             | [] -> sdst @ [(Cil.mkStmt (Cil.Instr idst))] (* Here idst is always not empty *)
           in 
           (* We need to save the labels the source statement had *)
           (* and also preserve `Goto' references to it *)
           let (s'::ss') = transformInstructions instrs [] [] in (* result is always not an empty list *)
           stmt.Cil.skind <- s'.Cil.skind; (* Just update the source statement *) 
           transformStatements stmts (dst @ (stmt::ss')) fundec (* and use it as the head of the resulting list*)
     | Cil.Return (Some e, loc) -> 
          let (e', ss') = transformExpression e fundec in
          stmt.Cil.skind <- Cil.Return (Some e', loc);
          transformStatements stmts (dst @ ss' @ [stmt]) fundec
     | Cil.If (e, b1, b2, loc) ->
          let (e', ss') = transformCondition e fundec in
          b1.Cil.bstmts <- transformStatements b1.Cil.bstmts [] fundec;
          b2.Cil.bstmts <- transformStatements b2.Cil.bstmts [] fundec;
          stmt.Cil.skind <- Cil.If (e', b1, b2, loc);
          transformStatements stmts (dst @ ss' @ [stmt]) fundec
     | Cil.Switch (e, b, _, loc) ->
         let (e', ss') = transformExpression e fundec in
         b.Cil.bstmts <- transformStatements b.Cil.bstmts [] fundec;
         stmt.Cil.skind <- Cil.Switch (e', b, b.Cil.bstmts, loc);
         transformStatements stmts (dst @ ss' @ [stmt]) fundec
     | Cil.Loop (b, _, _, _) | Cil.Block b -> 
         b.Cil.bstmts <- transformStatements b.Cil.bstmts [] fundec;
         transformStatements stmts (dst @ [stmt]) fundec
     | _ ->
         transformStatements stmts (dst @ [stmt]) fundec)
 | [] -> dst

class transformLExprsVisitor = object
  inherit Cil.nopCilVisitor
  
  method vfunc (f: Cil.fundec) : Cil.fundec Cil.visitAction = 
    f.Cil.sbody.Cil.bstmts <- transformStatements f.Cil.sbody.Cil.bstmts [] f;
    (* Cil.dumpBlock Cil.defaultCilPrinter stderr 2 f.Cil.sbody; *)
    Cil.ChangeTo f
end
(*********************************************************************)

let simplify file =
  let visitGlobal = function
   | Cil.GFun _ as fundec ->
     if Options.getValueOfBool "simplify" then begin
       Simplify.doGlobal fundec;
     end else ();
       ignore (Cil.visitCilGlobal (new transformLExprsVisitor) fundec); (* Calling transformLExprs visitor *)
       (* ignore (Cil.visitCilGlobal (new changeBoolVisitor) fundec) *) (* Now transformLExprs does it *)
   | _ -> ()
  in
  Cil.iterGlobals file visitGlobal
;;
(* End of simplification of booleans used as integers               *)
(********************************************************************)

(* Input: file_name and global information gathered through all files.  Appends preprocessed file with the name given to all_files list, and updates the global info *)
let make_cil_file (all_files,global_info) file_name =
  let cil_file = (Frontc.parse file_name) ()
  in
  begin
    try
      let _ = Cil.visitCilFile (new frontc_error_detection_visitor) cil_file in
      ()
    with FrontcError loc ->
      failwith (loc.Cil.file ^ ":" ^ (string_of_int loc.Cil.line) ^
                ": " ^ "error: frontc reported an error --- see messages above.")
  end ;
  let simplified_cil_file = 
    if Options.getValueOfBool "simplemem" then 
      (* "Simplify" memory expressions, such that they do not contain more than one pointer dereference.  This is induced by the requirement for sanity of "pre" operation in presence of pointer expressions.  See the "Lazy Abstraction" paper, sections 5.1 and 5.2.1. *)
      (* We replace the default prefix "mem_" with something less common. *)
      (* We also make CIL ignore the new variables it might want ot introduce outside functions (for instance, in sizeof() expressions, or in __builtin_offsetof in structure definitions, see bug #338). *)
      VampyreStats.time "simplemem" (Simplemem.simplemem_inside_functions ~prefix:"cil_") cil_file
    else cil_file in
   VampyreStats.time "simplify" simplify simplified_cil_file ;
   VampyreStats.time "rmtmps" Rmtmps.removeUnusedTemps simplified_cil_file ;

   if (Options.getValueOfBool "initialize") then
      Stats.time "put in initializer" put_in_initializer simplified_cil_file ;

   let (simplified_cil_file,new_global_info) =
   if (Options.getValueOfBool "reroute") then begin
    let ccc cut_type cut_opt = List.map (fun s -> (cut_type,s)) (Options.getValueOfStringList cut_opt) in
    (* Account for reroute-first *)
    let generic_map_cut_type = if Options.getValueOfBool "reroute-first" then (MapCut (Specific_Arg 0)) else (MapCut All_Args) in
    (* Gather all cuts in one place *)
    let cuts = (ccc generic_map_cut_type "reroute-map")
              @(ccc (MapCut (Specific_Arg 1)) "reroute-map2")
	      @(ccc ForeachCut "reroute-foreach")
    in
    let (s,ngi) = gather_reroute_info global_info simplified_cil_file cuts [] (Options.getValueOfString "reroute-placeholder") in
    (s,ngi)
   end else (simplified_cil_file,global_info) in

   (* NO LONGER SUPPORTED
    VampyreStats.time "make cfg" Cfg.make_cfg simplified_cil_file ;

    if (Options.getValueOfInt "O" >= 1 or Options.getValueOfBool "pe") then begin
      let _ = partiallyEvaluate simplified_cil_file in ()
    end;
*)

    (* Oneret et al were moved to fixup_cil *)

    let simplified_cil_file = reroute_calls global_info simplified_cil_file in

(* NO LONGER SUPPORTED
    if (Options.getValueOfInt "O" >= 2) then ignore (Panalysis.t_simplify simplified_cil_file []);
    if (Options.getValueOfInt "O" >= 1) then ignore (Panalysis.slice simplified_cil_file);
*)

    (simplified_cil_file::all_files,new_global_info)

(* Do fixups that should happen after the control flow is established *)
let fixup_cil cil_file =
  let _ = VampyreStats.time "one ret" (Cil.visitCilFileSameGlobals (new oneret_visitor)) cil_file in
  VampyreStats.time "make cfg" make_cfg cil_file ;
  VampyreStats.time "rename locals" rename_locals cil_file;
  cil_file


let cil_varinfo_to_symbol v = v.Cil.vname


