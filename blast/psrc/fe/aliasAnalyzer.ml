(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(**
   aliasAnalyzer.ml (the engine that produces the alias database
   and answers alias queries).

   Depends on Bddptsto
   *)
  
open Ast
module Stats = Bstats
module E = Expression
module P = Predicate
module M = Message
module O = Options

(* Alias old(?) vampyre stats to usual stats *)
module VampyreStats = Stats


(******************************************************************************************)
(********** Code to "manually override" the alias analysis ********************************)
(******************************************************************************************)
(* SLOW *)
type aliasedge_t = bool
type aliasnode_t = ((int * E.expression),aliasedge_t) BiDirectionalLabeledGraph.node
let exp_node_table = Hashtbl.create 31 

let exp_ctr_ref = ref 0 

let list_of_aliases = ref []

let make_exp_node e =
  try Hashtbl.find exp_node_table e 
  with Not_found ->
    exp_ctr_ref := !exp_ctr_ref + 1;
    let n = BiDirectionalLabeledGraph.create_root (!exp_ctr_ref,e) in
    Message.msg_string Message.Debug ("make_exp_node: adding " ^ (E.toString e)) ;
    Hashtbl.add exp_node_table e n;
    n
  
let add_aliases e_list yesno_flag =
  if yesno_flag then list_of_aliases := (List.nth e_list 0, List.nth e_list 1)::!list_of_aliases;
  let link flg (n1,n2) = 
    BiDirectionalLabeledGraph.hookup_parent_child n1 n2 flg;
    BiDirectionalLabeledGraph.hookup_parent_child n2 n1 flg
  in
  let n_list = List.map make_exp_node e_list in
  let n_pairs_list = Misc.make_all_pairs n_list in
  List.iter (link yesno_flag) n_pairs_list


(* 1 = yes, -1 = no, 0 = don't know, for now, either YES or don't know *)
let override_alias e1 e2 = 
  try
    begin
    let [n1;n2] = List.map (Hashtbl.find exp_node_table) [e1;e2] in
    match (BiDirectionalLabeledGraph.find_edges n1 n2) with
    [] -> 0
    | e_l -> if (List.exists (fun e -> BiDirectionalLabeledGraph.get_edge_label e)) e_l then 1 else (-1)
    end
  with _ -> 0

let override_skolem_alias l1 l2 =
  let r = if List.mem (Expression.Lval l1,Expression.Lval l2) !list_of_aliases then true else 
  if List.mem (Expression.Lval l2,Expression.Lval l1) !list_of_aliases then true else false in
  Message.msg_string Message.Normal ("IN override_skolem_alias: " ^ (E.lvalToString l1) ^ " and " ^ (E.lvalToString l2)^ " returns " ^(if r then "true" else "false"));
  r
  
let add_alias_override aliasPreds =
(*
  Message.msg_string Message.Debug "In add_alias_override";
  Message.msg_string Message.Debug ("Predicate = " ^ (P.toString aliasPreds));
*)
  let proc_atom p = 
	
  Message.msg_string Message.Debug ("proc_atom: Predicate = " ^ (P.toString aliasPreds));
    match p with
    P.Atom(E.Binary (binop,e1,e2)) -> add_aliases [e1;e2] (binop = E.Eq)
    | _ -> failwith ("Unimpl. alias predicate :"^(P.toString p))
  in
  List.iter proc_atom (P.getAtoms aliasPreds) ;
  Message.msg_string Message.Debug "After overriding aliases:" ;
  List.iter (fun (e1,e2) -> Message.msg_string Message.Debug ((E.toString e1) ^ " aliased to " ^ (E.toString e2)) ) !list_of_aliases;
  ()

(* ONLY USE: add_alias_override, is_override_alias *)

(******************************************************************************************)

let listOfAliases = ref []
let pointsToInfo : Bddptsto.pointsToInfo ref = ref (Bddptsto.FlowInsensitivePointsTo Bddptsto.bddZero)

let constructAliasDatabase () =
  let aliasFile = O.getValueOfString "alias" in
  if (aliasFile = "") then
    (* Nothing to do *)
    ()
  else
    if (aliasFile = "bdd") then
    begin
      (* do the bdd based alias analysis *)
	M.msg_string M.Normal "Starting the alias analysis" ;
      pointsToInfo := Bddptsto.do_bdd_alias (O.getValueOfString "pta" = "flow") 
    end
    else 
      begin (* the bdd has been dumped previously *)
	O.setValueOfString "alias" "bdd" ;
        pointsToInfo := Bddptsto.load_FI_points_to_bdd aliasFile 
      end
(***************************
	Commenting out hokey alias file mode -- Rupak
      else
      begin 
    (* add each pair to the alias database *)
       (* we simulate the effect of an alias analysis by letting the user input a file 
	  with alias pairs.
        *)
	let readAliasPairs filename =
	  let doParse inchan =
	    let _ = VampyreErrormsg.startFile filename in
	    let lexbuf = Lexing.from_channel inchan in
	    let _ = VampyreErrormsg.theLexbuf := lexbuf in
	    Inputparse.main Inputflex.token lexbuf 
	  in
	  ignore (M.msg_string M.Normal "Reading in alias pairs...");
	  try
	    let inchan = open_in filename
	    in
	    let a = doParse inchan
	    in
	    ignore (M.msg_string M.Normal "Alias pairs read.\n") ;
	    List.iter (fun a -> M.msg_string M.Normal (P.toString a)) a ;
	    a
	  with
	    Sys_error _ -> (M.msg_string M.Error ("Cannot find alias information in "^filename^".\n"); [])
	  | VampyreErrormsg.Error -> (M.msg_string M.Error ("Error raised in reading alias pairs.\n"); [])
	in
	
	let rec loopOverAliasPairs lst =
	  match lst with
	    [] -> ()
	  | p :: rest ->
	      begin
		match p with
		  P.Atom a ->
		  begin
		    match a with
		      E.Binary (E.Eq, E.Lval l1, E.Lval l2) ->
			listOfAliases := (l1, l2) :: !listOfAliases ; loopOverAliasPairs rest
		    | _ -> failwith ("Strange expression read by readAliasPairs "^(E.toString a))
		  end
		|	_ -> failwith ("Strange predicate read by readAliasPairs "^(P.toString p))
	      end
	in
	loopOverAliasPairs (readAliasPairs aliasFile)(* ;
							transitivelyClose !listOfAliases *)
      end
********************************************)

(* implement -aliasclos -- add more lvalues not encountered in program *)
let refineAliasDatabase () =
  pointsToInfo := Bddptsto.alias_refine pointsToInfo
   
       	
(* memoize get_type_of_lval *)
let lval_types_cache = Hashtbl.create 1003

let gtol lval = 
  let callee = BlastCSystemDescr.C_System_Descr.get_type_of_lval in
  if (Hashtbl.mem lval_types_cache lval)
    then Hashtbl.find lval_types_cache lval
    else begin
      let lval_type = callee lval in
      let _ = Hashtbl.add lval_types_cache lval lval_type in
      lval_type
    end


let askAliasDatabase l1 l2 =
  (* M.msg_string M.Debug ("In askAliasDB "^(E.lvalToString l1)^" and "^(E.lvalToString l2)) ;*)
  let foo () =
  if O.getValueOfString "alias" = "bdd" then
    begin
      let eqt = 
      try 
	let compare_types () = 
	  (*let get_type_function = BlastCSystemDescr.C_System_Descr.get_type_of_lval in*)
	  let get_type_function = gtol in
	  compare 
	    (Stats.time "get_type_of_lval" gtol l1)
	    (Stats.time "get_type_of_lval" gtol l2)
	in
	(Stats.time "comparing types" compare_types ()) = 0
      with e -> 
	  (M.msg_string M.Debug ("get_type fails!"^(Printexc.to_string e));  
          false) (* unsound! *) 
      in
      if eqt then (* only check aliasing between values of the same type *) 
        (* skolems are aliased *)
(*
        if BlastCSystemDescr.C_System_Descr.is_skolem l1 || BlastCSystemDescr.C_System_Descr.is_skolem l2 then begin
	  M.msg_string M.Debug "At least one lvalue is a Skolem: Overriding";
          if O.getValueOfString "aliasfile" <> "" then begin let r = override_skolem_alias l1 l2  in M.msg_string M.Debug ("Answer is "^(if r then "true" else "false")); r end else true
        end
        else
*)
    let do_actual_analysis  () = 
        match !pointsToInfo with
         Bddptsto.FlowSensitivePointsTo locbddhashtbl -> failwith "FlowSensitivePointsTo: Not implemented!"
       | Bddptsto.FlowInsensitivePointsTo ptstobdd -> 
         let field_insensitive_alias = 
           try
             Bddptsto.checkAliasFI (ptstobdd) l1 l2
           with e -> 
              (M.msg_string M.Error ("bdd error!"^(Printexc.to_string e));
              raise e)
         in
         field_insensitive_alias && 
         (match (Bddptsto.fieldmap l1, Bddptsto.fieldmap l2) with
           (Some (e1,f1), Some (e2,f2)) ->
             begin
               try 
		 let falias = Bddptsto.checkAliasFI (ptstobdd) 
		     (Bddptsto.lookup_field_lval e1 f1) (Bddptsto.lookup_field_lval e2 f2) in
		 if falias = false then
		   M.msg_string M.Debug "Aliased without fields, not aliased by field sensitivity!" ;
(*
                 (* force not aliased for different fields *)
                 if f1 <> f2 then false
                 else
*)
		   falias
               with ex -> (M.msg_string M.Error ("bdd error: (field sensitive) "^(Printexc.to_string ex)); 
                           raise ex)
             end
         | _ -> true)
     in Stats.time "analysis of matching types" do_actual_analysis ()
      else
        ((*M.msg_string M.Debug "The types are different. Hence not aliased.";*) 
	      false)
    end
  else
	failwith "No alias information present!" 
  (* foo ends *)
  in Stats.time "analysis of all types" foo ()


let list_of_functions_whose_address_is_taken = ref None

(* given an lvalue, find all functions this lvalue can be pointing to *)
let getFunctionsPointedTo (fcexp : E.expression) =
  M.msg_string M.Error ("getFnPtTo called with "  ^ (E.toString fcexp)) ;
  let get_pts_to l flist = 
      M.msg_string M.Debug ("l is : " ^ (E.lvalToString l)) ;

      match !pointsToInfo with
	Bddptsto.FlowSensitivePointsTo locbddhashtbl ->
	  []
      |	Bddptsto.FlowInsensitivePointsTo ptstobdd ->
          List.filter (fun f -> (* type based disambiguation *)
            Bddptsto.checkPointsToFI ptstobdd l f 
            && 
	    (
            M.msg_string M.Debug ("f is : " ^ (E.lvalToString f)) ;
            let t1 = BlastCSystemDescr.C_System_Descr.get_type_of_lval l 
            and t2 = BlastCSystemDescr.C_System_Descr.get_type_of_lval f in
            M.msg_string M.Debug  
            ("Type t1 is "^(Pretty.sprint ~width:256 (Pretty.dprintf "%a\n"
            Cil.d_type  t1))) ;
            M.msg_string M.Debug
            ("Type t2 is "^(Pretty.sprint ~width:256 (Pretty.dprintf "%a\n"
            Cil.d_type  t2))) ;
            (* t1 = t2 : This isn't good enough --- because of physical subtyping *)
            match (t1, t2) with
              (Cil.TFun (rtyp1, vlist1, _, _), Cil.TFun (rtyp2, vlist2, _, _) ) -> 
                (rtyp1 = rtyp2) && ( match (vlist1, vlist2) with (Some l1, Some l2) -> (List.length l1 = List.length l2) | (None, None) -> true | _ -> false)
            | _ -> false
            ) ) 
          flist 
  in
  let l = (match fcexp with 
             E.Lval lv -> lv 
           | _ -> failwith ("getFunctionsPointedTo: Strange fn ptr call: " ^ (E.toString fcexp))) in
  match !list_of_functions_whose_address_is_taken with
    None ->
     (* filter list of functions *)
      let fl = (List.map (fun s -> E.Symbol s) (BlastCSystemDescr.C_System_Descr.list_of_functions ())) in
      list_of_functions_whose_address_is_taken := Some fl ;
      let fptrs = get_pts_to l fl in
(*
      List.iter (fun f -> M.msg_string M.Error ("Fn ptr: " ^ (E.lvalToString f) )) fptrs ;
*)
      (match fptrs with [] -> failwith ("Function pointer call with no aliased function! " ^ (E.toString fcexp))
      | _ -> fptrs)
  | Some fl ->
      let fptrs = get_pts_to l fl in
(*
      List.iter (fun f -> M.msg_string M.Error ("Fn ptr: " ^ (E.lvalToString f)) ) fptrs ;
*)
      (match fptrs with [] -> failwith ("Function pointer call with no aliased function! " ^ (E.toString fcexp))
      | _ -> fptrs)


let getAllAliases lv =
  if O.getValueOfString "alias" = "bdd" then
    begin
      failwith "getAllAliases : bdds : Not implemented yet" ;
      match !pointsToInfo with
	Bddptsto.FlowSensitivePointsTo locbddhashtbl ->
	  []
      |	Bddptsto.FlowInsensitivePointsTo ptstobdd ->
          (* Look up aliasing relationship from pointsToBdd *)
	  []
    end
  else
    let lst =
      Misc.union 
	(List.map snd (List.filter (fun (l,l') -> l = lv) !listOfAliases))
	(List.map fst (List.filter (fun (l,l') -> l' = lv) !listOfAliases))
    in
    (* M.msg_string M.Debug ("In getAllAliases with "^(E.lvalToString lv)) ;
       M.msg_string M.Debug ("Aliases are ") ; 
    List.iter (fun l -> M.msg_string M.Debug (E.lvalToString l)) lst ; *)
    lst
      
let can_escape lv globals =
  (* M.msg_string M.Debug ("In can_escape with "^E.lvalToString lv) ;*)
  let rec can_escape_worker lv globals seen =
    let rec exp_can_escape e globals =
      match e with
      |	E.Lval l -> can_escape_worker l globals seen
      |	E.Chlval (l,_) -> 
	  begin
	    can_escape_worker l globals seen
	  end
      | E.Binary (_, e1, e2) -> (exp_can_escape e1 globals) || (exp_can_escape e2 globals)
      |	E.Cast (t, e1) -> exp_can_escape e1 globals
      | E.Constant(_) -> false
      |	E.Constructor (_, _) -> failwith "Constructor not handled in can_escape"
      | E.FunctionCall (func, args) ->
	  begin
	    let rec loop lst =
	      match lst with 
		[] -> false
	      | a:: rest -> if exp_can_escape a globals then true else loop rest
	    in
	    exp_can_escape func globals || (loop args) 
	  end
      | E.Unary(_, e) -> 
	  exp_can_escape e globals
      |	_ -> failwith "can_escape : Strange expression"
    in
    if List.mem lv seen then
      false else
      begin
	let escapes_directly = 
	  match lv with
	    E.Symbol s -> Hashtbl.mem globals lv 
	  | E.Dereference e -> exp_can_escape e globals 
	  | E.Access (_, e, _) -> exp_can_escape e globals
	  | E.Indexed (e1, e2) -> failwith "Indexed not handled in can_escape"
	  | E.This -> failwith "This not handled in can_escape"
	in
	escapes_directly || 
	(List.exists (fun l -> 
	  can_escape_worker l globals (lv::seen)) (getAllAliases lv))
      end
  in
  let r = can_escape_worker lv globals []
  in
  (* M.msg_string M.Debug ("can escape returns "^(if r then "true" else
    * "false"));*)
  r
    
let rec generateAliasConstraints (var1 : E.lval) (var2 : E.lval) : P.predicate =
  match (var1, var2) with
  | E.Symbol v, E.Symbol v' -> if askAliasDatabase var1 var2 then P.True else P.False
  | E.Access (E.Dot, E.Lval l, fld1), E.Access (E.Dot, E.Lval l', fld2)
  | E.Access (E.Arrow, E.Lval l, fld1), E.Access (E.Arrow, E.Lval l', fld2)->
      if fld1 = fld2 then
	generateAliasConstraints l l'
      else
	P.False
  | E.Indexed (e1, e2), E.Indexed (e1', e2') ->
      P.conjoinL 
	[ P.Atom (E.Binary (E.Eq, e1, e1')); 
	  P.Atom (E.Binary (E.Eq, e2, e2'))]
  | _ -> 
      if askAliasDatabase var1 var2 then P.True
      else P.False (* not aliased *)



      
  
let  queryAlias (_v1 : E.expression) (_v2 : E.expression) = 
   (*let _ = M.msg_string M.Debug ("Alias query for "^(E.toString _v1)^" and*)
   (*"^(E.toString _v2)) in *)
  (*let override = *)
    (*if (O.getValueOfString "aliasfile" <> "") then override_alias _v1 _v2 else 0*)
  (*in*)
  (*if override <> 0 then *)
    (*begin*)
     (*M.msg_string M.Debug (Printf.sprintf "Overriding: %s %s (%d)" (E.toString _v1) (E.toString _v2) override );*)
    (*override > 0*)
    (*end*)
  (*else*)
  (* now do the real thing *)
  (* symbolic check *)
  let is_sym1 =
    E.is_symbolic (E.lv_of_expr _v1)
    (*try E.is_symbolic (E.lv_of_expr _v1)*)
    (*with Not_found -> false*)
  in
  let is_sym2 =
    E.is_symbolic (E.lv_of_expr _v2)
    (*try E.is_symbolic (E.lv_of_expr _v2)*)
    (*with Not_found -> false*)
  in
    if (is_sym1 || is_sym2) then false (* Nothing can be aliased to a symbolic constant -- note, we are talking about
					  a symbolic constant, NOT derefs etc. of symbolic constants *)
    else
      let (v1,v2) =
	(*if (O.getValueOfBool "cf")*)
	(*then*)
	  (*Stats.time "deep_alpha_convert" (List.map*)
	    (*(E.deep_alpha_convert*)
	       (*(fun lv -> E.Lval (E.peel_symbolic lv))))*)
	    (*[_v1;_v2]*)
	(*else*) (_v1,_v2)
      in
	(* M.msg_string M.Debug ("After peeling: "^(E.toString v1)^" and "^(E.toString v2)) ;*)
	     let lvalOfExp e =
	       match e with 
		   E.Lval l -> l
               | _ -> failwith ("queryAlias: bug in replaceChlvals?" ^ (E.toString e))
	
	     in
	       try
		 let var1 = lvalOfExp (Stats.time "replaceChlvals" E.replaceChlvals v1) 
		 and var2 = lvalOfExp (Stats.time "replaceChlvals" E.replaceChlvals v2) 
		 in
		   match (var1,var2) with
		       (E.Symbol _ , E.Symbol _) -> false
			 (*RJ: two symbols x,y can only be aliased if they are
                         * synt. identical, or identical after "de-symconsting".
                         * In either case, writing to x, either directly affects "y" 
                         * (if they are identical), i.e. is something the caller knows,
			 * or, doesnot affect y -- i.e. they are NOT aliased, and so 
                         * w.log, knowing who is going to call this, we can safely return false.
			 * But really, one should make this function say yes, for the 
                         * same reasons as it should say yes for syntactic identity 
                         * and the case should be filtered out later. *)
		     | _ ->
			 
			 let a' = 
			   if var1 = var2 then
			     (* RJ: silly hack to get around this identity semantics! 
                              * this should say TRUE if =, the function calling it 
                              * should decide what to do if they are the same!!! *)
			     (if  (_v1 <> _v2) then true else false) (* RJ: Is this function even called with Chlvals ? *)
			   else (* RJ: why is this false ? 
				   Answer: If var1 is syntactically equal to var2, then
				   we substitute var2 anyway; and moreover var1 not = var2
				   is false. So in this case we can do without aliasing.
				   RJ: maybe -- but the semantics are wrong. we need a function that says YES
				   if the two lvals are aliased -- without these special cases.
				*)
			     begin
                               let askdb () = askAliasDatabase var1 var2 in
                               let rv = VampyreStats.time "ask aliasdatabase" askdb () in
                               rv
			     end
			 in
                         if a' = true then (); 
			     (* M.msg_string M.Debug ("Got back alias answer
                              * true for "^(E.toString v1)^" and "^(E.toString
                              * v2)) ;*)
			   let a''_3v = (* 1 = TRUE, 0 = Dont know, -1 = FALSE *)
			     match (var1,var2) with
				 (E.Dereference (E.Binary(E.Plus,a1,o1)),E.Dereference (E.Binary(E.Plus,a2,o2))) 
                               | (E.Dereference (E.Binary(E.Offset,a1,o1)),E.Dereference (E.Binary(E.Offset,a2,o2))) 
                                   -> if (a1 = a2) then 1 else 0
			       | (E.Access(_,E.Lval(E.Dereference(E.Binary(E.Plus,a1,o1))),f1),
				  E.Access(_,E.Lval(E.Dereference(E.Binary(E.Plus,a2,o2))), f2))
                               | (E.Access(_,E.Lval(E.Dereference(E.Binary(E.Offset,a1,o1))),f1),
				  E.Access(_,E.Lval(E.Dereference(E.Binary(E.Offset,a2,o2))), f2))
				 -> if (a1=a2 && f1 = f2) then 1 else 0
                                 (* JHALA: pointer arith + field *)
                               | (E.Access(_,E.Lval(E.Indexed(e1,_)),f1),
				  E.Access(_,E.Lval(E.Indexed(e2,_)),f2))
                               -> if (e1=e2 && f1 = f2) then 1 else 0
                                 (* JHALA: index array + field *)
                               | (E.Access(_,E.Lval lv1,f1),E.Access(_,E.Lval lv2,f2)) ->
                                   if (f1<>f2) then 
                                     ((*M.msg_string M.Debug (Printf.sprintf "Diff fields %s %s" f1 f2);*) (-1)) (* RJ: change! *)
                                   else (if (askAliasDatabase lv1 lv2) then 1 else (-1)) 
                                   
                               | (E.Indexed(E.Lval (E.Symbol s1),_),E.Indexed ((E.Lval (E.Symbol s2)),_)) -> 
                                   if (s1 = s2) then 1 else 0
                               (* UNSOUND | (E.Indexed(e1,_),E.Indexed(e1',_)) -> if (e1 = e1') then 1 else 0 *)
		               | _ -> 0
			   in 
			   let a = 
                             let rv = 
                               match a''_3v with
                                 0 -> a'
                               | 1 -> true
                               | (-1) -> false
                               | _ -> a'
                             in 
                             rv && (not (var1 = var2))
                           in a
			     (* M.msg_string M.Debug (Printf.sprintf "QueryAlias %s %s (%b)\n " 
                                (E.lvalToString var1) (E.lvalToString var2) a); *)
		   with _ -> 
		     begin
		       (*M.msg_string M.Debug "queryAlias raises exception for the query" ;*)
		       (*M.msg_string M.Debug ((E.toString v1) ^" and " ^ (E.toString v2)) ;*)
		       (*M.msg_string M.Debug "returning false" ;*)
		       false  
		     end
		     
	       

(********** RJ: this below function is cut out of abstraction.ml because it really should
  be here
*)


 (* this table maps an lval to the set of lvals it may alias... *)
	 (* this table never decreases! Its never cleared! *)
 let lval_alias_table = Hashtbl.create 1009

 (* I tried to be nice about this but circular dependency means I cant be. 
    type iter_key_t = AllLvals | ScopeLvals | TraceLvals
    These are now defined as integers in Misc.ml ...
 *)


 (* this function is ONLY called during REFINE. During image computation,
    the set of relevant preds is culled from the predicates at hand *)
 let stats_nb_alias_query = ref 0
			      
 (* assume that the lval_iterator is CLOSED under closure *)
 let get_lval_aliases_iter iter_key lval_iterator lval = (* TBD:SPEED *)
   let _foo () = 
   if (O.getValueOfString "alias" = "" (*|| O.getValueOfBool
   "incref"*))  then [lval]
   else 
     begin
       try
	 (* M.msg_string M.Debug ("get_lval_aliases_iter: "^(E.lvalToString
          * lval));*)
	 Hashtbl.find lval_alias_table (iter_key,lval) 
       with
	   Not_found ->
	     (* first a rather slow way to do things *)
	     let aliases = ref [] in
	     let _ =
	       Stats.time "Lval iterator" 
		 lval_iterator 
		 (fun x ->
		    let _ = (stats_nb_alias_query := !stats_nb_alias_query + 1) in
		      if Stats.time "queryAlias" (queryAlias
			(E.Lval x))
			(E.Lval lval) 
		      then aliases := x::!aliases;)
	     in
	     let rv = Misc.sort_and_compact (lval::!aliases)
	     in (* NOTE that this always adds lval into the set returned *)
	     let rv_l = List.map E.lvalToString rv in
	       M.msg_string M.Debug (Printf.sprintf "alias set for %s: %s" (E.lvalToString lval) (Misc.strList rv_l));
	       Hashtbl.replace lval_alias_table (iter_key,lval) rv;
	       rv
     end
    in Stats.time "get_lval_aliases_iter" _foo () 

 (* TBD: alias-union-find *)    
 (* SKY: Okay, about union-find.  Union-find is more likely known as "disjoint set structure".  This structure is a representaiton of equivalence classes, where fast operation x~y is available and also you may unite two "classes" making one class out of them. *)
 (* We do not need to iterate over all the lvalues, and check each of them if it aliases the target.  Instead, we could split all lvalues into buckets, and do not check a bucket if we know that no lval there may be aliased with the one given.  This will be implemented in Incremental Alias component--see below *)

 (* THIS IS SLOW! please try not to use it! *)    
 let get_lval_aliases =
   (* Misc.allLvals_t_c is just a constant to distinguish different alias type *)
   (* iterate_all_lvals is a simple Hashtbl iterator that iterates through ALL lvalues in program.  That's the right way, since the lvalues needed for alias analysis may not even encounter in trace. *) 
   (* Possible improvement for llvm: do not iterate ALL lvalues.  Lvalues that are must-aliases of ANYTHING--maybe not even the lvalue in subject!--shouldn't be iterated here.  If they're alias of lvalue being analyzed then they're processed in must-alias processing procedure.  If they're must-aliases of other lval, they shouldn't yield anything for this one.  Hence we should throw them out. 
   	Constants should not be processed as well, because they can't be changed.
   *)
   Stats.time "get_lval_aliases_iter" (get_lval_aliases_iter Misc.allLvals_t_c)
     BlastCSystemDescr.C_System_Descr.iterate_all_lvals
     
 let get_nomust_lval_aliases =
   Stats.time "get_lval_but_must_aliases_iter" (get_lval_aliases_iter Misc.allLvals_t_c)
     BlastCSystemDescr.C_System_Descr.iterate_all_but_musts_lvals

 let get_lval_aliases_scope fname =
   Stats.time "get_lval_aliases_iter[scope]" (get_lval_aliases_iter Misc.scopeLvals_t_c)
     (BlastCSystemDescr.C_System_Descr.iterate_all_scope_lvals fname)

(***********************************************************************************)
(* Incremental Alias Analysis                                                      *)
(***********************************************************************************)
(*
When a trace is analyzed, and a precondition of an operator is computed, we pretend that we assign, conditionally, to each of aliases of the target.  However, there's no point to assign anything to an lvalue the already computed part of the trace does not contain.  This "incremental" interface saves time for the alias analysis on a trace scale by maintaining a set of already encountered lvalues, and then it responds to queries "find all aliases of X among the lvalues you consumed" in an incremental manner.

The lvalues added are split into buckets, such that we can compare X with each bucket, and rule out the whole bucket if we know that nothing there may alias the given value.  The buckets are distinguished by (in this order)

  - "base" lvalue symbol (without field accesses and array indexing)
  - TODO: type of lvalue (optional)

*)

(* Helpers to access aliasing functionality *)

let our_bdd () =
  match !pointsToInfo with
    |Bddptsto.FlowSensitivePointsTo _ -> failwith "FlowSensitivePointsTo: Not implemented!"
    |Bddptsto.FlowInsensitivePointsTo ptstobdd -> ptstobdd

(**** Aliaser buckets.

These are the "buckets" lvals are arranged to.  Axiom: "(may-alias X Y) implies (bucket_alias (bucket_of X) (bucket_of Y))"

Buckets are "simplified lvalues" according to Bddptsto.  When an assignment p=q is processed by the alias analyzer, both sides are "simplified" before their aliasing information is stored.  These simplified values are looked up for in the alias queries.  Therefore, we may use them as "buckets".
*)
type alias_bucket = Expression.lval

module BucketMap = Map.Make(struct
  type t = alias_bucket
  let compare = compare
end)

(* Get "natural" bucket of the lvalue *)
let bucket_of = Bddptsto.simplify_lval
(* Check if this lvalue is a may-alias of a bucket *)
(* NOTE: yes, it could be tempting to discard there x and y, but if we do that, we'll load bdd just once, at the time of construction of the initialization.  However, we need to load it after the alias analysis is performed. *)
let may_alias_bucket x y = let bp = our_bdd() in Bddptsto.checkAliasFI bp x y

let bucket_to_str = Expression.lvalToString

(* Given that the buckets of two lvals may-alias each other, check if the lvalues alias each other.
The code is taken from askAliasDatabase *)
let _may_alias_inbucket l1 l2 =
  match (Bddptsto.fieldmap l1, Bddptsto.fieldmap l2) with
   |(Some (e1,f1), Some (e2,f2)) -> begin
     try
       let falias = Bddptsto.checkAliasFI (our_bdd())
	     (Bddptsto.lookup_field_lval e1 f1) (Bddptsto.lookup_field_lval e2 f2) in
	 if falias = false then
	   M.msg_string M.Debug "SKY: Aliased without fields, not aliased by field sensitivity!" ;
	   falias
       with ex -> (M.msg_string M.Error ("bdd error: (field sensitive) "^(Printexc.to_string ex));
		   raise ex)
     end
   |_->true

let may_alias_inbucket l1 l2 = Stats.time "inbucket" (_may_alias_inbucket l1) l2

let check_type_eq  l1 l2 =
  try
    let compare_types () =
      (*let get_type_function = BlastCSystemDescr.C_System_Descr.get_type_of_lval in*)
      let get_type_function = gtol in
      compare
	(Stats.time "get_type_of_lval" gtol l1)
	(Stats.time "get_type_of_lval" gtol l2)
    in
    (Stats.time "comparing types" compare_types ()) = 0
  with e ->
      (M.msg_string M.Debug ("get_type fails!"^(Printexc.to_string e));
      false) (* unsound! *)

(***** Aliaser cache helpers *)
module LvalMap = Map.Make(struct
  type t = Expression.lval
  let compare = compare
end)
type alias_cache_bucket = (int * Expression.lval list) BucketMap.t


let master_bucket = E.Symbol "BLAST_MASTER"

(* the aliaser type *)
type alias_state = {
  (* simple storage: base lvalue -> list of possible may-aliases.  A special lvalue "master" contains all buckets, and is used to compute what buckets alias the given one *)
  aliaser_storage: (Expression.lval list) BucketMap.t;

  (* auxillary storage: we need to know what lvals are already in the mapping *)
  aliaser_lvals: bool LvalMap.t;

  (* "incremental" cache: for each lvalue, stores list of tuples (bucket,N,l) where l is the list of may-aliases found in bucket when it contained N elements. *)
  mutable aliaser_cache: alias_cache_bucket LvalMap.t;
}

let debug_aliaser_cache = ref false
let debug_aliaser_each_step = ref false

(* Print the debugging info to the printer *)
let print_aliaser fmt a =
  (* helper *)
  let fp = Format.fprintf fmt in
  fp "[Aliaser-->@\n";
  BucketMap.iter (fun b ll ->
    Format.fprintf fmt "%s -> @[" (bucket_to_str b);
    List.iter (fun l -> Format.fprintf fmt "%s@ " (Expression.lvalToString l)) ll;
    fp "@]@\n";
  ) a.aliaser_storage;
  if (!debug_aliaser_cache) then begin
    fp "cache-->@\n";
    LvalMap.iter (fun l bl ->
      Format.fprintf fmt "%s -> @[" (Expression.lvalToString l);
      BucketMap.iter (fun b (n,ll) ->
	Format.fprintf fmt "%s -> @[%d elements@ " (bucket_to_str b) n;
	List.iter (fun l -> Format.fprintf fmt "%s@ " (Expression.lvalToString l)) ll;
	fp "@]@\n";
      ) bl;
      fp "@]@\n";
    ) a.aliaser_cache;
  end;
  fp "<--aliaser]"


(* empty aliaser *)
let empty_aliaser = {
  aliaser_storage = BucketMap.empty;
  aliaser_cache = LvalMap.empty;
  aliaser_lvals = LvalMap.empty;
}

(* add lvalue to the aliaser *)
let _add_lval_to_aliaser a lval =
  let add_to_specific_bucket check storage bucket lval =
    (* check if it's already there and do nothing if it is *)
    if LvalMap.mem lval check then storage else
    let old_contents = try BucketMap.find bucket storage with Not_found -> [] in
    BucketMap.add bucket (lval::old_contents) storage
  in
  let s = a.aliaser_storage in
  let lvs = a.aliaser_lvals in
  let bucket = bucket_of lval in
  (* Add the bucket itself to the special list of buckets *)
  let s = add_to_specific_bucket lvs s master_bucket bucket in
  (* Add the lval to its bucket *)
  let s = add_to_specific_bucket lvs s bucket lval in
  (* NOTE that if an lvalue and its bucket are equal, it shouldn't prevent us from adding the lvalue into its own bucket.  Therefore, we update the lvalue set only here *)
  let lvs = LvalMap.add lval true (LvalMap.add bucket true lvs) in
  (* TODO: check if lval is a must_alias (-nomusts!) *)
  (* update information *)
  {a with aliaser_storage = s ; aliaser_lvals = lvs}

let add_lval_to_aliaser a l =
  let rv = Stats.time "Incremental Alias update" (_add_lval_to_aliaser a) l in
  (* Only print debug info if there's anything new.  Check if the new lval belonged to the OLD aliaser's lvals *)
  if LvalMap.mem l a.aliaser_lvals then rv
  else begin
    M.msg_string M.Debug (Printf.sprintf "add_lval_to_aliaser %s -> " (E.lvalToString l) );
		if (!debug_aliaser_each_step) then M.msg_printer M.Debug print_aliaser rv else ();
    rv
  end


(* Return the list of may-aliases of the lval given, according to the aliaser *)
let _query_aliaser a use_cache lval =
  let s = a.aliaser_storage in

  (* Cache helper functions (not global for inlining) *)
  (* Get the aliases and the size of the bucket at the time of the previous query. *)
  let aliaser_cache_lookup cache bucket lval =
    try BucketMap.find bucket (LvalMap.find lval cache) with Not_found -> (0,[])
  in
  (* Update cache of the aliaser record given.  Updates the mutable field, so ret. unit *)
  let aliaser_cache_update a bucket lval result current_length =
    let c = a.aliaser_cache in
    let lv_old = try LvalMap.find lval c with Not_found -> BucketMap.empty in
    let lv_new = BucketMap.add bucket (current_length,result) lv_old in
    let new_cache = LvalMap.add lval lv_new c in
    a.aliaser_cache <- new_cache
  in

  (* Override of the "main" alias functions.  According to Bddptsto semantics, an lvalue sometimes must-not alias itself (when there is no location on the stack it may-point to).  If it happens, we may end up not adding it to the aliases, if such lvalue was added to the aliaser (we'll find it in the aliaser_lvals, but won't find it in the aliases.  So, we override the main alias function with the one that returns true on equivalence *)
  let may_alias_bucket x y = (x = y) || (may_alias_bucket x y)
  in
  let may_alias_inbucket x y = (x = y) || (may_alias_inbucket x y)
  in

  (* A helper function that finds all may-aliases (according to the given qfn alias relation) of lval in the given bucket *)
  let aliases_in_bucket compare_types qfn bucket lval =
    (* First, we query cache: get the aliases and the size of the bucket at the time of the previous query. *)
    let (was_size,was_result) =
      if use_cache then Stats.time "cache lookup" (aliaser_cache_lookup a.aliaser_cache bucket) lval
      else (0,[])
    in
    let candidates_before_cache = try BucketMap.find bucket s with Not_found -> [] in
    (* Do not consider candidates among the last was_result elements of the list: they're already in the cache *)
    let current_length = List.length candidates_before_cache in
    let (candidates,_) = Misc.cut_list_at_k candidates_before_cache (current_length - was_size) in
    (* Filter new alias candidates by type *)
    (* TODO: make types buckets *)
    M.msg_string M.Debug (Printf.sprintf "in bucket %s query of %s before type -> %s" (E.lvalToString bucket) (E.lvalToString lval) (Misc.strList (List.map E.lvalToString candidates)));
    let type_filtered_candidates = if compare_types then List.filter (check_type_eq lval) candidates else candidates in
    M.msg_string M.Debug (Printf.sprintf "in bucket %s query of %s after type -> %s" (E.lvalToString bucket) (E.lvalToString lval) (Misc.strList (List.map E.lvalToString type_filtered_candidates)));
    (* Filter the remaining candidates by the actual in-bucket may-alias queries *)
    let result = List.rev_append (List.filter (qfn lval) type_filtered_candidates) was_result in
    M.msg_string M.Debug (Printf.sprintf "in bucket %s query of %s after actual aliases -> %s" (E.lvalToString bucket) (E.lvalToString lval) (Misc.strList (List.map E.lvalToString result)));
    (* Save the result to cache, if we use it *)
    let _ = if use_cache then Stats.time "cache update" (aliaser_cache_update a bucket lval result) current_length
    else () in
    result
  in
  (* Get buckets the lvalue may alias *)
  let may_buckets = aliases_in_bucket false may_alias_bucket master_bucket (bucket_of lval) in
  (* get may_aliases in each bucket *)
  let aliases = List.fold_left (fun accum b -> List.rev_append (aliases_in_bucket true may_alias_inbucket b lval) accum) [] may_buckets in
  (* We should also add "self" if the aliases do not contain it *)
  if LvalMap.mem lval a.aliaser_lvals then aliases else lval::aliases

let query_aliaser a l =
  let rv = Stats.time "Incremental Alias query" (_query_aliaser a true) l in
  M.msg_string M.Debug (Printf.sprintf "query_aliaser %s -> %s" (E.lvalToString l) (Misc.strList (List.map E.lvalToString rv)));
  rv

(* Check sanity of the aliaser: if the Bucket Axiom is true *)
let sanity_of_aliaser a = true
    
(***********************************************************************************)
(* Must aliases for temporary mem variables added by Cil                           *)
(***********************************************************************************)

(* is_tempmem is a criterion, based on which must-aliases are chosen.  If is_tempmem lvalue, then lvalue is a must-alias of something. *)
(* This is the potential place to instrument llvm-specific must-alias detection mechanism *)

let rec is_tempmem lv =
  match lv with
  | E.This -> false 
  | E.Symbol s -> 
      ((Misc.is_prefix "cil_" s) || (O.getValueOfBool "blastmust" && Misc.is_prefix "blast_must_" s))
  | E.Dereference e 
  | E.Access (_, e, _) 
  | E.Indexed (e, _) ->
      is_tempmem_exp e
and is_tempmem_exp e =
  match e with 
   E.Lval l -> is_tempmem l
  | E.Chlval _ 
  | E.Constant _ -> false
  | _ -> false (* failwith ("is_tempmem: not handled "^(E.toString e)) *)

(* Must aliases hash: for given lvalue yields list of expressions that are must-aliases *)
let mustAliasesHashtbl = Hashtbl.create 31 

module C_System_Descr = BlastCSystemDescr.C_System_Descr
module CSD = BlastCSystemDescr.C_System_Descr

(* Checks if variable (symbol, actually) is declared as constant.  It is enough to consider symbols only, since constants are only assigned once. *)
(* TODO: this doesn't work for local variables!! Cil just doesn't tell us that they're constant *)
let is_const lval = 
  (*let is_const_sym lval_symbol =*)
    (*let varinfo = CSD.get_varinfo lval_symbol in*)
    (*let var_typ = varinfo.Cil.vtype in*)
    let var_attrs = match CSD.get_type_of_lval lval with
      Cil.TVoid (attributes) -> attributes
    | Cil.TInt ( _ , attributes ) -> attributes
    | Cil.TFloat ( _ , attributes ) -> attributes
    | Cil.TPtr ( _ , attributes) -> attributes
    | Cil.TArray ( _ , _ , attributes) -> attributes
    | Cil.TFun ( _ , _ , _ , attributes) -> attributes
    | Cil.TNamed ( _ , attributes) -> attributes
    | Cil.TComp ( _ , attributes) -> attributes
    | Cil.TEnum ( _ , attributes) -> attributes
    | Cil.TBuiltin_va_list ( attributes) -> attributes
    in
    List.map (fun (Cil.Attr(str,_)) -> M.msg_string M.Debug ("attr: "^(str) )) var_attrs;
    Cil.hasAttribute "const" var_attrs

(* Must aliases are computed only for temporary variables introduced by Cil *)
let constructMustAliasDatabase () = 
  let do_assignment a =
    match a with 
    (* constants are must-aliases too *)
    (* It is enough to consider symbols only, since constants are only assigned once. *)
    CSD.Command.Expr (E.Assignment(_, target, rhs)) when ((O.getValueOfBool "const") && (is_const target)) ->
      M.msg_string M.Debug ("SKY const found: "^(E.lvalToString target) );
      (* check if caonstand was already assigned! *)
      if Hashtbl.mem mustAliasesHashtbl target 
      then failwith ("By specifying -const option, you said that constants would be assigned once, but for "^(E.lvalToString target)^" it is not true!  You filthy liar!")
      else Hashtbl.add mustAliasesHashtbl target [ rhs ]
    | CSD.Command.Expr (E.Assignment(_, target, rhs)) when is_tempmem target ->
      if Hashtbl.mem mustAliasesHashtbl target 
      then failwith ("sanity check: mem_temp recycled? "^(E.lvalToString target))
      else Hashtbl.add mustAliasesHashtbl target [ rhs ]
    (* for debugging purposes -- print if lvalue is not constant *)
    | CSD.Command.Expr (E.Assignment(_, (Expression.Symbol target_sym), rhs)) ->
      M.msg_string M.Debug ("SKY const NOT found: "^(target_sym) )
    | _ -> () in
  let work_edge e =
    (* add the RHS's of any mem_temp lval assignments to the table. *)
    let _ = M.msg_printer M.Debug CSD.print_edge e in
    let c = (CSD.get_command e) in
    let code = c.CSD.Command.code in
    match code with
      CSD.Command.Block l -> List.iter do_assignment l
    | _ -> () (* No other command can cause a must alias *) in
  CSD.map_edges work_edge ;
	let lst = ref [] in
	Hashtbl.iter (fun lval expr_list -> lst := (List.length expr_list)::!lst) mustAliasesHashtbl;
  M.msg_string M.Normal ("Number of Must aliases: "^(string_of_int (List.fold_left (+) 0 !lst)));
  M.msg_string M.Debug "Mustalias database is:" ;
  Hashtbl.iter 
    (fun key data -> 
      M.msg_string M.Debug "Next aliase part, for key ";
      M.msg_string M.Debug ((E.lvalToString key));
      List.iter (fun e -> M.msg_string M.Debug (E.toString e)) data) 
    mustAliasesHashtbl

(* withdraws must aliases from alias processing cycle *)
let withdraw_musts () = 
  CSD.withdraw_musts mustAliasesHashtbl

(* returns list of must-aliases for given expression *)
let get_all_must_aliases lv =
  let convert_to_lval e =
    match e with 
      E.Lval l -> Some l
    | E.Binary (op, E.Lval l1, E.Lval l2) ->
	  (try
	    if CSD.is_ptr l1 then Some l1 
	    else if CSD.is_ptr l2 then Some l2
	    else (M.msg_string M.Debug ("Dropping expr from must aliases (1): "^(E.toString e)); None)
	  with _ -> (M.msg_string M.Debug ("Dropping expr from must aliases (2):"^(E.toString e)); None))
    | E.Binary(_, E.Constant _, E.Lval l1)
    | E.Binary(_, E.Lval l1, E.Constant _) ->
	  (try 
	    if CSD.is_ptr l1 then Some l1 
	    else (M.msg_string M.Debug ("Dropping expr from must aliases (3): "^(E.toString e)); None)
	  with _ -> (M.msg_string M.Debug ("Dropping expr from must aliases (4):"^(E.toString e)); None))
    | _ -> (M.msg_string M.Debug ("Dropping expr from must aliases (4):"^(E.toString e)); None) in
  match lv with E.Symbol _ -> []
  | _ ->
    (* myalias -- aliases that come directly from hash table mustAliasesHashtbl, which is set of right-hand-sides of assignments *)
    let myalias =
      if (Hashtbl.mem mustAliasesHashtbl lv) then
      begin
        M.msg_string M.Debug ("get_all_must_aliases hit table with lvalue "^(E.lvalToString lv)) ;
        let aliases = Hashtbl.find mustAliasesHashtbl lv in
        M.msg_string M.Debug "Aliases are :" ;
        List.iter (fun l -> M.msg_string M.Debug (E.toString l)) aliases ;
        Misc.map_partial convert_to_lval aliases
      end
     else [] in
  (* additional aliases may be found by replacing the must aliases of the variable.
     Example: mem_temp1 = a->field;
              mem_temp1->field2 = foo;
     then the must_aliases of mem_temp1->field2 are foo and (a->field)->field2.
   *)
  (* full simplify *)
   (* basealias -- list of aliases found by doing a substitute described above.  The substitution is only made for the "core" identifier of expression--i.e. the symbol returned by fullsimp_lval *)
   let basealias = begin
       let slv = try E.fullsimp_lval (E.push_deref lv) 
                 with ex -> (* because of substitutions, we get lvals of the form * (0) on which fullsimp fails! *)
                    lv in
       M.msg_string M.Debug ("slv is "^(E.lvalToString slv));
       if Hashtbl.mem mustAliasesHashtbl slv then
        begin
          let slv_list' = Hashtbl.find mustAliasesHashtbl slv in
	  M.msg_apply M.Debug (List.iter (fun e -> M.msg_string M.Debug (E.toString e))) slv_list' ;
	  (* r is list of lv, where its identifier is substituted sequentially with each its must alias *)
          let r = List.map (fun s -> 
	    let t = (E.deep_alpha_convert 
		     (fun l -> if l = slv then s else E.Lval l) (E.Lval lv )) in
	    t ) slv_list' in
	  M.msg_apply M.Debug (List.iter (fun e -> M.msg_string M.Debug (E.toString e))) r ;
          Misc.map_partial convert_to_lval r
        end
       else []
   end
   in
   Misc.union myalias basealias

  
let must_alias_cache = Hashtbl.create 37
let must_alias_pair_cache = Hashtbl.create 37

let _is_must_alias lv e' =
  let slv = E.lvalToString lv in
  try List.mem e' (Hashtbl.find must_alias_cache slv) 
  with Not_found ->
     (let ms = List.map (fun lv'' -> E.Lval lv'') (get_all_must_aliases lv) in
      Hashtbl.replace must_alias_cache slv ms;
      List.mem e' ms)
    
let is_must_alias e e' =
  match e with E.Lval lv ->
    (let slv = E.lvalToString lv in
    let se' = E.toString e' in
    try Hashtbl.find must_alias_pair_cache (slv,se') 
    with Not_found ->
      (let rv = _is_must_alias lv e' in
       Hashtbl.replace must_alias_pair_cache (slv,se') rv;rv))
  | _ -> failwith "Non lval expr in is_must_alias"

  
(***********************************************************************************)
(* End code for must aliases for temporary mem variables added by Cil              *)
(***********************************************************************************)

