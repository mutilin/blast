(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

(* This file contains a modified version of the foci abstract syntax tree
 * and other elements of the fociAst.mli file.
 * Foci copyright belongs to Cadence Design Systems, Inc.
 *)

module CharSet = Set.Make(Char)
module M = Message

type symbol = string

type constant = Int of int

type expression =
    Constant of constant
  | Variable of symbol
  | Application of symbol * expression list
  | Sum of expression list
  | Coeff of constant * expression
  | Ite of predicate * expression * expression

and predicate =
    True
  | False
  | And of predicate list
  | Or of predicate list
  | Not of predicate
  | Implies of predicate * predicate
  | Equality of expression * expression
  | Leq of expression * expression
  | Atom of symbol
  | Forall of (symbol list) * predicate

type concrete =
    Var of symbol
  | Appl of symbol * Num.num list

let zero = Constant( Int 0 )

(*****************************************************************************)
(*******************    TREE PRE- AND POST-PROCESSING    *********************)
(*****************************************************************************)
(*replace and restore the original symbol by new more parsable symbol*)

let symbol_counter = ref 0
let symbol_table = Hashtbl.create 1

let clear_symbol_table () =
  Hashtbl.clear symbol_table;
  symbol_counter := 0

let get_fresh_symbol () = 
  symbol_counter := !symbol_counter + 1;
  "S" ^ (string_of_int !symbol_counter)(*makes the query shorter*)
  (*"Symbol" ^ (string_of_int !symbol_counter)*)

let get_symbol sym =
  try
    Hashtbl.find symbol_table sym
  with Not_found ->
    let new_sym = get_fresh_symbol () in
      Hashtbl.add symbol_table sym new_sym;
      new_sym

let normalize_symbol tree  =
  let rec normalize_symbol_expr tree =
    match tree with
    | Constant _ -> tree
    | Variable var -> Variable (get_symbol var)
    | Application (f, args) -> Application (get_symbol f, List.map (normalize_symbol_expr) args)
    | Sum elements -> Sum (List.map (normalize_symbol_expr) elements)
    | Coeff (cst, expr) -> Coeff (cst, normalize_symbol_expr expr)
    | Ite (p, e1, e2) -> Ite (normalize_symbol_pred p, normalize_symbol_expr e1, normalize_symbol_expr e2)
  and  normalize_symbol_pred tree =
    match tree with
    | True  -> True
    | False -> False
    | And pred_lst -> And (List.map (normalize_symbol_pred) pred_lst)
    | Or pred_lst -> Or (List.map (normalize_symbol_pred) pred_lst)
    | Not pred1 -> Not (normalize_symbol_pred pred1)
    | Implies (pred1, pred2) -> Implies (normalize_symbol_pred pred1, normalize_symbol_pred pred2)
    | Equality (exp1, exp2) -> Equality (normalize_symbol_expr exp1, normalize_symbol_expr exp2)
    | Leq (exp1, exp2) -> Leq (normalize_symbol_expr exp1, normalize_symbol_expr exp2)
    | Atom sym -> Atom (get_symbol sym)
    | Forall (sym_lst, pred1) -> Forall (List.map (get_symbol) sym_lst, normalize_symbol_pred pred1)
  in
    normalize_symbol_pred tree

let restore_symbol tree =
  let reversed_table = Hashtbl.create 101 in
  let get_symbol sym = 
    try
      Hashtbl.find reversed_table sym
    with Not_found -> failwith ("Foci interface: cannot find matching symbol for " ^ sym)
  in
  let rec restore_symbol_expr tree =
    match tree with
    | Constant _ -> tree
    | Variable var -> Variable (get_symbol var)
    | Application (f, args) -> Application (get_symbol f, List.map (restore_symbol_expr) args)
    | Sum elements -> Sum (List.map (restore_symbol_expr) elements)
    | Coeff (cst, expr) -> Coeff (cst, restore_symbol_expr expr)
    | Ite (p, e1, e2) -> Ite (restore_symbol_pred p, restore_symbol_expr e1, restore_symbol_expr e2)
  and restore_symbol_pred tree =
    match tree with
    | True  -> True
    | False -> False
    | And pred_lst -> And (List.map (restore_symbol_pred) pred_lst)
    | Or pred_lst -> Or (List.map (restore_symbol_pred) pred_lst)
    | Not pred1 -> Not (restore_symbol_pred pred1)
    | Implies (pred1, pred2) -> Implies (restore_symbol_pred pred1, restore_symbol_pred pred2)
    | Equality (exp1, exp2) -> Equality (restore_symbol_expr exp1, restore_symbol_expr exp2)
    | Leq (exp1, exp2) -> Leq (restore_symbol_expr exp1, restore_symbol_expr exp2)
    | Atom sym -> Atom (get_symbol sym)
    | Forall (sym_lst, pred1) -> Forall (List.map (get_symbol) sym_lst, restore_symbol_pred pred1)
  in
    (*reverse hash table*)
    Hashtbl.iter (fun a b -> Hashtbl.add reversed_table b a) symbol_table;
    restore_symbol_pred tree

(*****************************************************************************)
(*************************        PRINTER         ****************************)
(*****************************************************************************)

let rec print_foci_expression exp = match exp with
    Constant cst ->
    begin
      match cst with
      | Int i -> (string_of_int i) ^ " "
    end
  | Variable var -> var ^ " "
  | Application (f, args) ->
    begin
      let args_string = List.fold_left (fun acc x -> acc ^ (print_foci_expression x) ) "" args
      in f ^ " [ " ^ args_string ^ "] "
    end
  | Sum elements ->
    begin
      let el_string = List.fold_left (fun acc x -> acc ^ (print_foci_expression x) ) "" elements
      in "+ [ " ^ el_string ^ "] "
    end
  | Coeff (cst, expr) ->
    begin
      match cst with
      | Int i -> "* " ^ (string_of_int i) ^ " " ^ (print_foci_expression expr)
    end
  | Ite (p, e1, e2) -> failwith "Foci interace: what is Ite ??"


let rec print_foci_predicate pred = match pred with
    True  -> "true "
  | False -> "false "
  | And pred_lst ->
    let preds_string = List.fold_left (fun acc x -> acc ^ (print_foci_predicate x) ) "" pred_lst
    in "& [ " ^ preds_string ^ "] "
  | Or pred_lst ->
    let preds_string = List.fold_left (fun acc x -> acc ^ (print_foci_predicate x) ) "" pred_lst
    in "| [ " ^ preds_string ^ "] "
  | Not pred1 -> "~ " ^ (print_foci_predicate pred1)
  | Implies (pred1, pred2) -> (*syntactic sugar ?? ~a | b*)
    "-> " ^ (print_foci_predicate pred1) ^ (print_foci_predicate pred2)
    (*print_foci_predicate ( Or [Not pred1; pred2] )*)
  | Equality (exp1, exp2) -> "= " ^ (print_foci_expression exp1) ^ (print_foci_expression exp2)
  | Leq (exp1, exp2) -> "<= " ^ (print_foci_expression exp1) ^ (print_foci_expression exp2)
  | Atom sym -> sym ^ " "
  | Forall (sym_lst, pred1) -> failwith "Foci interface: how to convert for all ??"


let rec print_foci pred_lst =
  let buffer = Buffer.create 0 in
    List.iter
      (fun x ->
        Buffer.add_string buffer ((print_foci_predicate x) ^ "; " )
      ) pred_lst;
    (*remove the trailling "; "*)
    Buffer.sub buffer 0 ((Buffer.length buffer) -3)


(*****************************************************************************)
(*************************          PARSER          **************************)
(*****************************************************************************)
(*let's go for a simple top down parsing*)

(*control character*)
let _LBRACK = '['
let _RBRACK = ']'
let _LPAREN = '('
let _RPAREN = ')'
let _SEMICOLON = ';'
let _PLUS = '+'
let _TIMES = '*'
let _DASH = '-' (* for -> *)
let _AND = '&'
let _OR = '|'
let _EQ = '='
let _NOT = '~'
let _LEQ = '<' (* for <= *)
let _EOF = char_of_int 0

(*whitespace characters*)
let whitespace = 
  CharSet.add ' ' (
  CharSet.add '\n' (
  CharSet.add '\r' (
  CharSet.add '\t' (
  CharSet.empty ))))

(*control characters*)
let ctrlChar =
  CharSet.add _LBRACK (
  CharSet.add _RBRACK (
  CharSet.add _LPAREN(
  CharSet.add _RPAREN (
  CharSet.add _SEMICOLON (
  CharSet.add _PLUS (
  CharSet.add _TIMES (
  CharSet.add _DASH (
  CharSet.add _AND (
  CharSet.add _OR (
  CharSet.add _EQ (
  CharSet.add _NOT (
  CharSet.add _LEQ (
  CharSet.empty )))))))))))))

(*all is encapsulated in the function => no side effect*)
let parse_foci input_string =
  let in_stream = Stream.of_string input_string in
      
  let current = ref ' ' in

  let expr_table = Hashtbl.create 10 in
  let pred_table = Hashtbl.create 10 in

  let next_char () =
    (*Message.msg_string Message.Debug "next_char";*)
    try
      current := Stream.next in_stream;
    with Stream.Failure -> current := _EOF in

  let next_token () = 
    (*Message.msg_string Message.Debug "next_token";*)
    next_char();
    while CharSet.mem !current whitespace do
      next_char();
    done in

  (*to use after buffer_to_next_ws
    when the current char can already be the next tocken
  *)
  let next_token2 () =
    while CharSet.mem !current whitespace do
      next_char();
    done in

  (*return the chars until the next whitespace or the next control char
    takes at least one char*)
  let buffer_to_next_ws () =
    (*Message.msg_string Message.Debug "buffer_to_next_ws";*)
    let buffer = Buffer.create 0 in
      Buffer.add_char buffer !current;
      next_char();
      while not (CharSet.mem !current whitespace) 
          && not (CharSet.mem !current ctrlChar)
          && not (!current = _EOF) do
        Buffer.add_char buffer !current;
        next_char()
      done;
      Buffer.contents buffer
  in

  (*
  term :: individual_term
        | '(' term ')'
        | '+' '[' term_lst ']'
        | '*' number term
        | uninterpreted_function_symbol '[' term_lst ']'
        | # number term
        | @ number
  *)
  let rec term () =
    match !current with
    | '(' ->
      begin
        next_token();
        let f = term() in
          if !current = ')' then
            begin
              next_token();
              f
            end
          else
            failwith "foci parsing: syntax error (parenthesis mismatch)"
      end
    | '+' ->
      begin
        next_token();
        if !current <> _LBRACK then
          failwith "foci parsing: syntax error (missing '[')";
        next_token();
        let f_lst = term_lst() in
          if !current <> _RBRACK then
            failwith "foci parsing: syntax error (missing ']')";
          next_token();
          Sum f_lst
      end
    | '*' ->
      begin
        next_token();
        let n_str = buffer_to_next_ws () in
        print_endline n_str;
        let n = int_of_string n_str in
          next_token2();
          Coeff ( Int n, term() )
      end
    | '#' ->
      begin
        next_char();
        let n = int_of_string ( buffer_to_next_ws ()) in
          next_token2();
          let t = term () in
            Hashtbl.replace expr_table n t;
            t
      end
    | '@' ->
      begin
        next_char();
        let n = int_of_string ( buffer_to_next_ws ()) in
          next_token2();
          Hashtbl.find expr_table n
      end
    |  _  -> (* uninterpreted fct sym or individual_term or cst *)
      begin
        let sym = buffer_to_next_ws () in
          next_token2();
          if !current <> _LBRACK then
            (*individual_term or cst*)
            try
              Constant (Int (int_of_string sym))
            with _ ->
              Variable sym (* ?? atom or variable*)
          else
            (*uninterpreted_function_symbol*)
            begin
              next_token();
              let args = term_lst() in
                if !current <> _RBRACK then
                  failwith "foci parsing: syntax error (missing ']')";
                next_token();
                Application ( sym, args )
            end
      end
  (*
  term_lst :: term term_lst
            | empty
  *)
  and term_lst () =
    let rec iter acc =
      if !current <> _RBRACK then
        begin
          iter ( (term()) :: acc)
        end
      else
        List.rev acc
    in
      iter []
  in

  (*
  formula :: propositional_variable
           | '(' formula ')'
           | '=' term term
           | '<=' term term
           | '&' '[' formula_lst ']'
           | '|' '[' formula_lst ']'
           | '~' formula
           | '->' formula formula
           | 'true'
           | 'false'
           | # number formula
           | @ number
  *)
  let rec formula () =
    match !current with
    | '(' ->
      begin
        next_token();
        let f = formula() in
          if !current = ')' then
            begin
              next_token();
              f
            end
          else
            failwith "foci parsing: syntax error (parenthesis mismatch)"
      end
    | '=' ->
      begin
        next_token();
        let t1 = term () in
        let t2 = term () in
          Equality (t1, t2)
      end
    | '<' ->
      begin
        next_char();
        if !current <> '=' then
          failwith "foci parsing: syntax error (<=)";
        next_token();
        let t1 = term () in
        let t2 = term () in
          Leq (t1, t2)
      end
    | '&' ->
      begin
        next_token();
        if !current <> _LBRACK then
          failwith "foci parsing: syntax error (missing '[')";
        next_token();
        let f_lst = formula_lst () in
          if !current <> _RBRACK then
            failwith "foci parsing: syntax error (missing ']')";
          next_token();
          And f_lst
      end
    | '|' ->
      begin
        next_token();
        if !current <> _LBRACK then
          failwith "foci parsing: syntax error (missing '[')";
        next_token();
        let f_lst = formula_lst () in
          if !current <> _RBRACK then
            failwith "foci parsing: syntax error (missing ']')";
          next_token();
          Or f_lst
      end
    | '~' ->
      begin
        next_token();
        Not (formula ())
      end
    | '-' ->
      begin
        next_char();
        if !current <> '>' then
          failwith "foci parsing: syntax error (->)";
        next_token();
        let f1 = formula () in
        let f2 = formula () in
          Implies (f1, f2)
      end
    | '#' ->
      begin
        next_char();
        let n = int_of_string ( buffer_to_next_ws ()) in
          next_token2();
          let f = formula () in
            Hashtbl.replace pred_table n f;
            f
      end
    | '@' ->
      begin
        next_char();
        let n = int_of_string ( buffer_to_next_ws ()) in
          next_token2();
          Hashtbl.find pred_table n
      end
    |  _  ->
      begin
        (*propositional_variable true or false*)
        let var = buffer_to_next_ws () in
          next_token2();
          match var with
          | "true" -> True
          | "false" -> False
          | _ -> Atom var (* ?? var or atom *)
      end
  
  (*
  formula_lst :: formula formula_lst
               | empty
  *)
  and formula_lst () =
    let rec iter acc =
      if !current <> _RBRACK then
        begin
          iter ( (formula()) :: acc)
        end
      else
        List.rev acc
    in
      iter []
  in

  (*
  file :: formula [; formula]
  *)
  let rec file () =
    let rec iter acc =
      let acc2 = (formula()) :: acc in
        if !current = _SEMICOLON then
          begin
            next_token();
            iter acc2
          end
        else
          acc2
  in
    iter []

  in
    next_token() ;
    List.rev (file())

(*****************************************************************************)
(*************************        QUERIES        *****************************)
(*****************************************************************************)

exception SAT of predicate list * (concrete * Num.num) list

let foci : (Pervasives.in_channel * Pervasives.out_channel * Pervasives.in_channel) option ref = ref None
let get_foci () =
  match !foci with
    | None -> 
        M.msg_string M.Normal "Forking Foci process..." ;
        let ic, oc, ec = Unix.open_process_full "fociServer" (Unix.environment ()) in
          foci := Some(ic, oc, ec) ;
          print_endline (input_line ic);
          print_endline (input_line ic);
          M.msg_string M.Normal "done!\n";
          (ic,oc)
    | Some(ic, oc, _) -> (ic, oc)
and stop_foci() = 
  match !foci with
    | None -> ()
    | Some (ic,oc,ec) -> 
        begin
          try
            M.msg_string M.Debug "Stopping Foci process...";

            output_string oc "\n";
            flush oc;
            print_endline (input_line ic);

            ignore (Unix.close_process_full (ic,oc,ec) );
            (*ignore( Sys.command "killall -9 fociServer" );*)

            foci := None;
            M.msg_string M.Normal "Foci stopped."
          with _ -> failwith "failed to stop Foci!"
        end

let interpolateN_foci query_type pred_lst =
  (* some IO helper*)
  let line: string option ref = ref None in

  let peek_line in_channel =
    M.msg_string M.Debug "peeking line";
    match !line with
    | Some l -> l
    | None ->
      begin
        let str = input_line in_channel in
          line := Some str;
          str
      end
  in

  let pop_line in_channel =
    M.msg_string M.Debug "poping line";
    match !line with
    | Some l ->
      begin
        line := None;
        l
      end
    | None -> input_line in_channel
  in
  (* end IO helpers *)

  let (foci_out, foci_in) = get_foci () in
    clear_symbol_table();
    let normalize_pred_lst = List.map (normalize_symbol) pred_lst in
    let query = query_type ^ (print_foci normalize_pred_lst) in
      M.msg_string M.Debug ("Foci query: " ^ query);
      output_string foci_in query ;
      output_char foci_in '\n';
      flush foci_in;
      let n = if query_type = "n" then List.tl pred_lst
        else if query_type = "s" then pred_lst
        else failwith "unknown foci query type"
      in (*Number of expected interpolants*)
      M.msg_string M.Debug ("query sent, expecting " ^ string_of_int (List.length n) ^ " interpolants");
      try
          if (peek_line foci_out) = "Satisfiable" then
              raise (SAT ([],[]))
          else
              List.map (fun _ -> restore_symbol (List.hd (parse_foci (pop_line foci_out)))) n
        with Failure e ->
          failwith ("error when reading foci output: " ^ e)


let interpolateN pred_lst =
  interpolateN_foci "n" pred_lst
    
let interpolateN_symm pred_lst =
  interpolateN_foci "s" pred_lst

