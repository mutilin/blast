(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

module Predicate = Ast.Predicate
module Expression = Ast.Expression
module C_System_Descr = BlastCSystemDescr.C_System_Descr
module C_Command = BlastCSystemDescr.C_Command

module IntSet = Set.Make (struct type t = int let compare a b = b - a end )

module Stats = Bstats

open Cil
open BlastArch.ModelCheckOutcome


(********************************************************************)
(*************** Code for query caching move to another file ********)
(********************************************************************)
module GL = Genlex
module ST = Stream

(* type qid_t = (int * int) list
 * type query_t = (qid_t,({-1..unsafe,0..??,+1..safe}, int)) *)

let all_edge_id = ref []

let get_qcfname () =
  let _fn = Options.getValueOfString "qcfile" in
  let _fn' = if _fn = "" then (Options.getValueOfString "mainsourcename") else _fn in
  _fn'^".qc"

let query_cache_table = Hashtbl.create 101

(* db: Make this local! And commented. *)
(* Locations sometimes are recursive, so we compare them with == instead of = *)
(* This should be faulty, but THIS HASH IS USED ONLY IN PRINTING ROUTINES THAT AREN'T CALLED!.
	So, let's hack it through for a while *)
module Location_Compare =
   struct
     type t = C_System_Descr.location
     let equal = (==)
     let hash = Hashtbl.hash
   end ;;
module Location_Hashtbl = Hashtbl.Make (Location_Compare);;
let loc_to_no = Location_Hashtbl.create 40

let get_query_tuples fname =
  let raw_entries = try Misc.string_list_of_file fname with _ -> [] in
  let proc raw =
    try
      let things = Misc.chop raw "," in
      match things with
      [s1;s2;s3] ->
        let ids_l = Misc.chop s1 ";" in
        let ids =
          List.map
          (fun s ->
            match Misc.words_of_string s with [w1;w2] ->(int_of_string w1,int_of_string w2))
          ids_l
        in
        let res = int_of_string (List.hd (Misc.words_of_string s2)) in
        let time = int_of_string (List.hd (Misc.words_of_string s3)) in
        Some(ids,(res,time))
    with e ->
      Message.msg_string Message.Error ("qc line: "^raw);
      Message.msg_string Message.Error ("Raises: "^(Printexc.to_string e));
      None
  in
  Misc.map_partial proc raw_entries

let load_query_cache () =
  let fname = get_qcfname () in
  let qlist = get_query_tuples fname in
  List.iter (fun (k,d) -> Hashtbl.add query_cache_table k d) qlist


let lookup_user_queries () =
  let fname = Options.getValueOfString "qf" in
  List.map fst (get_query_tuples fname)

let query_id_to_string qid =
  let qid' = List.map (fun (x,y) -> Printf.sprintf " %d %d " x y) qid in
  String.concat ";" qid'

let update_query_cache qid (res,time) =
  let fname = get_qcfname () in
  let _ = Hashtbl.add query_cache_table qid (res,time) in
  let qids = query_id_to_string qid in
  let out_string = Printf.sprintf "%s , %d , %d \n\n" qids res time in
  Misc.append_to_file fname out_string

let lookup_query_cache qid =
  try Some (Hashtbl.find query_cache_table qid) with Not_found -> None

let query_id elist =
  let r = List.map (fun e -> C_System_Descr.location_coords (C_System_Descr.get_source e))  elist
  in List.sort compare r


(*******************************************************************)

(*****************************************************************************************)
(* OBSOLETE: NOT SUPPORTED
let inputCustomAbs () =
  let read_abs_info filename =
    let doParse inchan =
      let _ = VampyreErrormsg.startFile filename in
      let lexbuf = Lexing.from_channel inchan in
      let _ = VampyreErrormsg.theLexbuf := lexbuf in
      Absparse.main Absflex.token lexbuf
    in
    ignore (Message.msg_string Message.Normal "Reading in abstraction ...");
    try
      let inchan = open_in filename
      in
      let a = doParse inchan
      in
      ignore (Message.msg_string Message.Normal " Abstraction read.\n") ;
      a
    with
      Sys_error _ -> (Message.msg_string Message.Error ("Cannot find abstraction info in "^filename^".\n"); ([],None,[]))
    | VampyreErrormsg.Error -> (Message.msg_string Message.Error ("Error raised in reading abstraction \n"); ([],None,[]))
  in
  let abs_file = Options.getValueOfString "custom" in
  if (abs_file = "") then
    ([],None,[]) (* no special types nothing at all ... *)
  else
    read_abs_info abs_file
*)

(*****************************************************************************************)
let constructInitialPredicates predFile =
  let readAndAddPredicates filename =
    let doParse inchan =
      (*let _ = VampyreErrormsg.startFile filename in*)
      let lexbuf = Lexing.from_channel inchan in
      (*let _ = VampyreErrormsg.theLexbuf := lexbuf in*)
      Inputparse.main Inputflex.token lexbuf
    in
    ignore (Message.msg_string Message.Normal "Reading in seed predicates...");
    try
      let inchan = open_in filename in
      let a = doParse inchan in
        ignore (Message.msg_string Message.Normal "Seed predicates read.\n") ;
        List.iter (fun a -> Message.msg_string Message.Normal (Predicate.toString a)) a ;
        Message.msg_string Message.Normal ("Read "^(string_of_int (List.length a))^" predicates");
        a
    with Sys_error _ -> (Message.msg_string Message.Error ("Cannot find predicate information in "^filename^".\n"); [])
       | _ -> (Message.msg_string Message.Error ("Error raised in reading seed predicates.\n"); [])
  in
  if (predFile = "") then [] else readAndAddPredicates predFile

let constructInvariants () =
  let readAndAddInvariants filename =
    let doParse inchan =
      (*let _ = VampyreErrormsg.startFile filename in*)
      let lexbuf = Lexing.from_channel inchan in
      (*let _ = VampyreErrormsg.theLexbuf := lexbuf in*)
      Inputparse.main Inputflex.token lexbuf
    in
    ignore (Message.msg_string Message.Normal "Reading in invariants...");
    try
      let inchan = open_in filename
      in
      let a = doParse inchan
      in
      ignore (Message.msg_string Message.Normal "Invariants read.\n") ;
      List.iter (fun a -> Message.msg_string Message.Normal (Predicate.toString a)) a ;
       a
    with
      Sys_error _ -> (Message.msg_string Message.Error ("Cannot find invariant information in "^filename^".\n"); [])
    | _ -> (Message.msg_string Message.Error ("Error raised in reading invariants.\n"); [])
  in
  let invFile = Options.getValueOfString "inv" in
  if (invFile = "") then
    (* Nothing to do *)
    []
  else
    readAndAddInvariants invFile

let constructEnvironmentAssumptions () =
   let readAndAddAssumptions filename =
    let doParse inchan =
      (*let _ = VampyreErrormsg.startFile filename in*)
      let lexbuf = Lexing.from_channel inchan in
      (*let _ = VampyreErrormsg.theLexbuf := lexbuf in*)
      Inputparse.env_assumptions Inputflex.token lexbuf
    in
    ignore (Message.msg_string Message.Normal "Reading in environment assumptions ...");
    try
      let inchan = open_in filename
      in
      let a = doParse inchan
      in
      ignore (Message.msg_string Message.Normal "Assumptions read.\n") ;
      List.iter (fun (l,p) -> Message.msg_string Message.Normal ((Expression.lvalToString l )^" : "^(Predicate.toString p))) a ;
       a
    with
      Sys_error _ -> (Message.msg_string Message.Error ("Cannot find assumption information in "^filename^".\n"); [])
    | _ -> (Message.msg_string Message.Error ("Error raised in reading assumptions.\n"); [])
  in
  let asmFile = Options.getValueOfString "easm" in
  if (asmFile = "") then
    (* Nothing to do *)
    []
  else
    readAndAddAssumptions asmFile

let reachedLoc = ref None
let final_string = ref ""


let make_hash_oplist op_list =
                        let make_hash_op op =
                                let make_hash_loc loc =
                                        if (Location_Hashtbl.mem loc_to_no loc) then
                                        Location_Hashtbl.replace loc_to_no loc ((Location_Hashtbl.find loc_to_no loc) + 1)
                                        else
                                        Location_Hashtbl.add loc_to_no loc 1
                                in
                                let str = C_Command.to_string (C_System_Descr.get_command op) in
                                let src = (C_System_Descr.get_source op) in
                                let trg = C_System_Descr.get_target op in
                        make_hash_loc trg ;
                        in
                        let first_loc = (C_System_Descr.get_source (List.hd op_list)) in
                        Location_Hashtbl.add loc_to_no first_loc ;
                        List.iter make_hash_op op_list;
                        ()

(* db: This is the visitor for printing the program for a counterexample. *)
class test1(op_list) =
  object
    inherit nopCilVisitor
    val working_list = ref (List.tl (List.tl (List.tl (List.tl op_list))))

    method vvdec vinfo=
      if(not (List.length !working_list = 0) ) then
        begin
          let first_elem = (List.hd !working_list) in
          let target = C_System_Descr.get_target first_elem in
          let cmd = C_System_Descr.get_command first_elem in
          let src = C_System_Descr.get_source first_elem in
          let src_pos_in_cil = C_System_Descr.get_source_position src in
          let trg_pos_in_cil = C_System_Descr.get_source_position target in
          let command = C_Command.to_string cmd in
          let vloc = vinfo.vdecl in
          let vfname = vloc.file in
          let vline = vloc.line in
          let _ = (match src_pos_in_cil with

              Some (filename1,lineno1,cno1) ->
                  (match trg_pos_in_cil with
                      Some (filename2,lineno2,cno2) ->
                         print_string ("vardec string1 from " ^ filename1 ^ " " ^ (string_of_int lineno1) ^ " to " ^ filename2 ^ " " ^ (string_of_int lineno2) ^ " " ^ command ^ " " ^ vfname^ " "^ (string_of_int vline) ^"\n") ;
                         if((vline >= lineno1) && (vline < lineno2))  then
                           begin
                             let vname=vinfo.vname in
                             let vtyp = vinfo.vtype in
                             if (vline = lineno2 -1) then
                               begin
                                 working_list := List.tl !working_list ;
                               end
                             else
                               ();
                             (match vtyp with
                               TInt (kind,_) ->
                                 (match (kind) with
                                   IChar -> print_string ("char " ^ vname ^"\n") ;
                                 | ISChar -> print_string ("signed char " ^ vname^"\n") ;
                                 | IUChar -> print_string ("unsigned char " ^ vname^"\n");
                                 | IInt -> print_string ("int " ^ vname^"\n");
                                 | IUInt -> print_string ("unsigned int " ^ vname^"\n");
                                 | IShort -> print_string ("short " ^ vname^"\n");
                                 | IUShort -> print_string ("unsigned short " ^ vname^"\n");
                                 | ILong -> print_string ("long " ^ vname^"\n");
                                 | IULong -> print_string ("unsigned long " ^ vname^"\n");
                                 | ILongLong -> print_string ("long long " ^ vname^"\n");
                                 | IULongLong -> print_string ("unsigned long long " ^ vname^"\n");
                                 | _ -> ();
                                 )
                             | _ -> () ;
                             )
                           end
                         else
                           ();

                    | None -> print_string ("vardec string2 " ^ filename1 ^ " " ^ (string_of_int lineno1) ^ " " ^ command ^ " " ^ vfname^ " "^ (string_of_int vline) ^ "\n");
                      if(vline >= lineno1)  then
                        begin
                          let vname=vinfo.vname in
                          let vtyp = vinfo.vtype in
                            working_list := List.tl !working_list ;
                            (match vtyp with
                              TInt (kind,_) ->
                                (match (kind) with
                                  IChar -> print_string ("char " ^ vname ^"\n") ;
                                | ISChar -> print_string ("signed char " ^ vname^"\n") ;
                                | IUChar -> print_string ("unsigned char " ^ vname^"\n");
                                | IInt -> print_string ("int " ^ vname^"\n");
                                | IUInt -> print_string ("unsigned int " ^ vname^"\n");
                                | IShort -> print_string ("short " ^ vname^"\n");
                                | IUShort -> print_string ("unsigned short " ^ vname^"\n");
                                | ILong -> print_string ("long " ^ vname^"\n");
                                | IULong -> print_string ("unsigned long " ^ vname^"\n");
                                | ILongLong -> print_string ("long long " ^ vname^"\n");
                                | IULongLong -> print_string ("unsigned long long " ^ vname^"\n");
                                | _ -> ();
                                )
                            | _ -> () ;
                            )
                        end
                      else
                        ();

                  )

              | None ->
                (match trg_pos_in_cil with
                  Some (filename2,lineno2,cno2) ->
                    let vloc = vinfo.vdecl in
                    let vfname = vloc.file in
                    let vline = vloc.line in
                    let command = C_Command.to_string cmd in
                      print_string ("vardec string3 " ^ filename2 ^ " " ^ (string_of_int lineno2) ^ " " ^ command ^ " " ^ vfname^ " "^ (string_of_int vline) ^ "\n");
                      if((vline<lineno2) && (vline>0)) then
                        begin
                          let vname=vinfo.vname in
                          let vtyp = vinfo.vtype in
                            if (vline = lineno2-1) then
                              begin
                                working_list := List.tl !working_list ;
                              end
                            else
                              ();
                            (match vtyp with
                              TInt (kind,_) ->
                                (match (kind) with
                                  IChar -> print_string ("char " ^ vname ^"\n") ;
                                | ISChar -> print_string ("signed char " ^ vname^"\n") ;
                                | IUChar -> print_string ("unsigned char " ^ vname^"\n");
                                | IInt -> print_string ("int " ^ vname^"\n");
                                | IUInt -> print_string ("unsigned int " ^ vname^"\n");
                                | IShort -> print_string ("short " ^ vname^"\n");
                                | IUShort -> print_string ("unsigned short " ^ vname^"\n");
                                | ILong -> print_string ("long " ^ vname^"\n");
                                | IULong -> print_string ("unsigned long " ^ vname^"\n");
                                | ILongLong -> print_string ("long long " ^ vname^"\n");
                                | IULongLong -> print_string ("unsigned long long " ^ vname^"\n");
                                | _ -> ();
                                )
                            | _ -> () ;
                            )
                        end
                      else
                        ();

                | None -> print_string "None";
                )
            ) in

            ();
          end
      else
          print_string "oplist empty\n";
          DoChildren
(*              Message.msg_string Message.Normal ("inside a statement " ^ (string_of_int !counter)) ;*)
        (*      counter := !counter + 1;*)


    method vstmt varstmt=
      if(not (List.length !working_list = 0) ) then
        begin
          let first_elem = (List.hd !working_list) in
          let target = C_System_Descr.get_target first_elem in
          let cmd = C_System_Descr.get_command first_elem in
          let src = C_System_Descr.get_source first_elem in
          let src_pos_in_cil = C_System_Descr.get_source_position src in
          let trg_pos_in_cil = C_System_Descr.get_source_position target in
          let command = C_Command.to_string cmd in
          let vskind = varstmt.skind in
            match src_pos_in_cil with
              Some (filename1,lineno1,cno1) ->
                (match trg_pos_in_cil with
                  Some (filename2,lineno2,cno2) ->
                     (match vskind with
                       If (vexp, blk1,blk2,vloc) ->
                         let vline = vloc.line in
                         let vfile = vloc.file in
                           print_string ("stmt string1 from " ^ filename1 ^ " " ^ (string_of_int lineno1) ^ " to " ^ filename2 ^ " " ^ (string_of_int lineno2) ^ " " ^ command ^ " " ^ vfile^ " "^ (string_of_int vline) ^"\n") ;
                           if((vline >= lineno1) && (vline < lineno2))  then
                             begin
                               print_string ("if {\n");
                                 if (vline = lineno2 -1) then
                                   begin
                                     working_list := List.tl !working_list ;
                                   end
                                 else
                                   ();
                             end
                           else
                             ();
                     | Loop (blk,vloc,Some st1, Some st2) ->
                       let vline = vloc.line in
                       let vfile = vloc.file in
                         print_string ("stmt string2 from " ^ filename1 ^ " " ^ (string_of_int lineno1) ^ " to " ^ filename2 ^ " " ^ (string_of_int lineno2) ^ " " ^ command ^ " " ^ vfile^ " "^ (string_of_int vline) ^"\n") ;
                         if((vline >= lineno1) && (vline < lineno2))  then
                           begin
                             print_string ("While {\n");
                               if (vline = lineno2 -1) then
                                 begin
                                   working_list := List.tl !working_list ;
                                 end
                               else
                                 ();
                           end
                         else
                           ();
                     | Instr (inst_list)  ->
                       let proc_inst inst =
                       (match inst with
                         Set (leftval,exp,vloc) ->
                           let vline = vloc.line in
                           let vfile = vloc.file in
                             print_string ("stmt string3 from " ^ filename1 ^ " " ^ (string_of_int lineno1) ^ " to " ^ filename2 ^ " " ^ (string_of_int lineno2) ^ " " ^ command ^ " " ^ vfile^ " "^ (string_of_int vline) ^"\n") ;
                             let (lvar,_) = leftval in
                             let _ = match lvar with
                                 Var (vinfo) -> print_string (vinfo.vname ^ " =" ^ "\n");
                               | _ -> ();
                             in
                               ();
                       | _ -> () ;
                       ) in
                         ();
                     | _ -> ();
                     )
                   | None -> print_string ("stmt string2 \n");
                 )
               | None -> ();
             end
           else
             print_string "oplist empty\n";
             DoChildren




(*  method vblock blck =
      let stmtlist = blck.bstmts in
      let print_loc_stmt stmt =
        let stkind = stmt.skind in
          match stkind with
            Loop (_,loc,bl) ->
              if bl then
                print_string ("string " ^ str ^ " loc " ^ loc.file ^ " " ^ (string_of_int loc.line) ^"\n")
              else
                print_string ("cil made "^ str ^ " loc " ^ loc.file ^ " " ^ (string_of_int loc.line) ^"\n")
          | Default (loc) ->
              print_string ("default " ^ "loc " ^ loc.file ^ " " ^ (string_of_int loc.line) ^"\n")
          | _ -> ()
              List.iter print_loc_label lbl_list;
      in
        List.iter print_loc_stmt stmtlist;
        DoChildren*)

(*  method vglob global =
      print_string ("\n inside a global " ^ (string_of_int !counter)) ;
      counter := !counter + 1;
      DoChildren*)
  end



class test0(counter) =
  object
    inherit nopCilVisitor
    method vvdec vinfo=
      let vname=vinfo.vname in
      let vtyp = vinfo.vtype in
      let _ = match vtyp with
          TInt (kind,_) ->
            (match (kind) with
              IChar -> print_string ("\n char " ^ vname) ;
            | ISChar -> print_string ("\n signed char " ^ vname) ;
            | IUChar -> print_string ("\n unsigned char " ^ vname);
            | IInt -> print_string ("\n int " ^ vname);
            | IUInt -> print_string ("\n unsigned int " ^ vname);
            | IShort -> print_string ("\n short " ^ vname);
            | IUShort -> print_string ("\n unsigned short " ^ vname);
            | ILong -> print_string ("\n long " ^ vname);
            | IULong -> print_string ("\n unsigned long " ^ vname);
            | ILongLong -> print_string ("\n long long " ^ vname);
            | IULongLong -> print_string ("\n unsigned long long " ^ vname);
            | _ -> ();
            )
        | _ -> () ;
      in
(*      Message.msg_string Message.Normal ("inside a statement " ^ (string_of_int !counter)) ;*)
        counter := !counter + 1;
        DoChildren

    method vglob global =
      print_string ("\n inside a global " ^ (string_of_int !counter)) ;
      counter := !counter + 1;
      DoChildren
  end

let teststatement cil_files op_list =
  let counter = ref 0 in
    visitCilFile (new test1(op_list)) cil_files


(*class firsttest(op_list) =
  object
    inherit nopCilVisitor
    method vvdec vinfo=*)

(*let print_dot loc_to_no op_list =
      let print_err_dot ch loc_to_no op_list =
        let known_locs = ref IntSet.empty in
        let known_edges = ref [] in
        let print_one_edge op =
          (*    let ch = open_out fname in *)
          let loc1 = C_System_Descr.get_source op in
          let loc2 =  C_System_Descr.get_target op in
          let cmd =  C_System_Descr.get_command op in
          let rec output_loc loc =
            if (not (IntSet.mem loc.BlastControlFlowAutomaton.loc_id !known_locs)) then
              begin
                if (Location_Hashtbl.mem loc_to_no loc) then
                  begin
                    Printf.fprintf ch "  %d [label = \"%d    %d\"]\n" loc.BlastControlFlowAutomaton.loc_id loc.BlastControlFlowAutomaton.loc_id (Location_Hashtbl.find loc_to_no loc) ;
                    known_locs := IntSet.add loc.BlastControlFlowAutomaton.loc_id !known_locs ;
                  end
                else
                  begin
                    Printf.fprintf ch "  %d [label = \"%d    %d\"]\n" loc.BlastControlFlowAutomaton.loc_id loc.BlastControlFlowAutomaton.loc_id 0 ; (*(Location_Hashtbl.find loc_to_no loc);*)
                    known_locs := IntSet.add loc.BlastControlFlowAutomaton.loc_id !known_locs ;
                  end
              end
            else ();
          in
            output_loc loc1 ;
            output_loc loc2 ;
            if (not (List.mem op !known_edges)) then
              begin
                Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"  loc1.BlastControlFlowAutomaton.loc_id  loc2.BlastControlFlowAutomaton.loc_id  (String.escaped (C_Command.to_string cmd));
                known_edges := (op :: !known_edges) ;
              end
            else
              begin
                ();
              end
        in
          Printf.fprintf ch "digraph {\n" ;
          List.iter print_one_edge op_list;
          Printf.fprintf ch "}\n\n";
      in
        let fname = Options.getValueOfString "trace-dot" in
        if (fname <> "") then
          begin
          let temp = open_out_gen [Open_creat; Open_append; Open_text ] 777 fname in
            print_err_dot temp loc_to_no op_list;
            close_out temp ;
            let _ = ignore (Sys.command ("dot -Tps -o " ^ fname ^ ".ps " ^ fname)) in
              ()
          end
        else
          ()
*)

(******************************************************************************)




(* single query *)
(* cil_files is passed for relating code to counterexample blocks etc. *)
let model_check o_err_locs entries seedPreds cil_files =
  let module This_C_Abstraction = Abstraction.Make_C_Abstraction(C_System_Descr) in
  let module This_Cf_C_Abstraction = CfAbstraction.Make_C_Abstraction(C_System_Descr) in
  let module This_LMC =
    LazyModelChecker.Make_Lazy_Model_Checker(This_C_Abstraction) in
  let module This_LMC_CF =
    CfLazyModelChecker.Make_Lazy_Model_Checker(This_Cf_C_Abstraction) in
  let module This_LMC_CFB =
    CfbLazyModelChecker.Make_Lazy_Model_Checker(This_Cf_C_Abstraction) in
  let module Region = This_C_Abstraction.Region in

  (* Set signals, so that stats are reported at interrupts *)
  let sigfail_handler n =
    Message.msg_string Message.Normal "\n\nModel checker stats:\n" ;
    Message.msg_printer Message.Normal This_LMC.print_stats () ;
    Message.msg_string Message.Normal "\n\nAbstractor stats:\n" ;
    Message.msg_printer Message.Normal This_C_Abstraction.print_stats () ;
    Stats.print stdout "Timings:\n";
    Message.msg_string Message.Error (Printf.sprintf "Interrupted with signal %d" n);
    (* Signal number is negative *)
    exit (n-128)
  in
  List.iter (fun s -> ignore(Sys.signal s (Sys.Signal_handle sigfail_handler))) [Sys.sigint; Sys.sigterm];


  let _ =
    This_C_Abstraction.check_races :=
    let cr = Options.getValueOfString "checkRace" in
      if ( cr = "tar" || cr = "extar") then
        begin
          let rv = (Options.getValueOfString "racevar") in
            Message.msg_string Message.Normal ("Checking for races on: "^rv);
            Some (Ast.Expression.Symbol rv)
        end
      else None
  in
  This_C_Abstraction.assumed_invariants := Stats.time "read invariants" constructInvariants();
  This_C_Abstraction.assumed_invariants_region := This_C_Abstraction.assumed_invariants_list_to_region();
  let _ = This_LMC.process_env_assumptions (constructEnvironmentAssumptions ()) in
  let initStateFile = Options.getValueOfString "init" in
  let initialState = Predicate.conjoinL (Stats.time "read initial state" constructInitialPredicates initStateFile) in
  let mainFunctions = Options.getValueOfStringList "main" in
    (* JHALA: Kill hacks *)
  let killLabel = Options.getValueOfString "killLoc" in
  let kill_location =
    match (Stats.time "Kill init" C_System_Descr.get_locations_at_label killLabel) with
        [l] -> Some(l)
      | []  ->
          if (killLabel = "") then ()
          else
            Message.msg_string Message.Normal ("Kill label '" ^ killLabel ^
                                            "' was not found in the source file(s)") ;
          None
      | _   ->
          Message.msg_string Message.Error ("Kill label '" ^ killLabel ^
                                            "' appears multiple times in the source file(s)") ;
          failwith ("Kill label '" ^ killLabel ^ "' appears multiple times in the source file(s)")
  in
  let _ = This_C_Abstraction.initialize_abstraction () in
  let _ = This_Cf_C_Abstraction.initialize_abstraction () in

  let _ = Message.msg_string Message.Normal "Done adding seed predicates" in
    Message.msg_string Message.Normal "\n\n\n********** Now running the model-checker **********\n\n" ;
    flush stdout;
    begin
      let indent_loc = "                                                " in
        (* This function prints error trace and the end of the file.
           SKY: rt should have correct indentation, but appears to fail to do this.*)
      (* IMPORTANT NOTE!
         The format of the trace printed here is crucial for the trace analysis in integrated LDV verifier
	 If you're going to change anything in this funciton, make SURE that the code changed fits the code that analyses the trace
       *)
      let print_op (indent_op, need_src) op op_next_t =
        let src_loc = C_System_Descr.get_source op
        and target_loc = C_System_Descr.get_target op
        and c_command = C_System_Descr.get_command op
        in
	if (need_src) then
	  Message.msg_string Message.Normal (indent_loc ^ (C_System_Descr.location_to_string src_loc)) ;

        Message.msg_string Message.Normal (indent_op ^ (C_Command.to_string c_command)) ;
	(* We should distinguish function calls for functions with and without bodies.  There are two reasons: first, we want to report it and test if some important functions are skipped; and second, we don't want to indent absent functions, since they don't have a corresponding return operation! *)
	let (is_function,is_function_and_undefined,fname) = match c_command.C_Command.code with
	   C_Command.FunctionCall fcall ->
	    let fname' = Misc.fst3 (C_System_Descr.deconstructFunCall fcall) in
	    (true,not (C_System_Descr.is_defined fname'), fname')
	  | _ -> (false,false, "")
	and is_skipped = match op_next_t with
	    Some op_next -> op <> op_next && target_loc == C_System_Descr.get_source op_next
	  | None -> false
	in

	if (is_function && not is_function_and_undefined && is_skipped) then
	  (* The format of this string is important for report-analyzing tools! *)
	  Message.msg_string Message.Normal ("LDV: skipping call to function due to stack overflow ("^(string_of_int (Options.getValueOfInt "fdepth"))^"): "^fname);

	if (is_function_and_undefined) then
	  (* The format of this string is important for report-analyzing tools! *)
	  Message.msg_string Message.Normal ("LDV: undefined function called: "^fname);

	(* Local variables of function will be printed.
	   Locals that end with # are variables of signature *)
	if (is_function && not is_function_and_undefined) then begin
	  let locals_str = (List.fold_left (fun str local -> (str ^ " " ^ (Expression.lvalToString local))) "" (C_System_Descr.lookup_local_lvals fname)) in
	  let formals_list = let fl = C_System_Descr.lookup_formals fname in match fl with C_System_Descr.Fixed sl | C_System_Descr.Variable sl -> sl in
	  let formals_str = (List.fold_left (fun str formal -> (str ^ " " ^ (formal))) "" formals_list ) in
	  Message.msg_string Message.Normal ("Locals: "^formals_str);
	end;

	(* print target location if necessary *)
        let _ = match c_command.C_Command.code with
	   C_Command.Block ((C_Command.Return _)::_) | C_Command.FunctionCall _ -> ()
	  |_-> Message.msg_string Message.Normal (indent_loc ^ (C_System_Descr.location_to_string target_loc))
	in

        match c_command.C_Command.code with
           C_Command.FunctionCall fcall ->
	    if (is_function && not is_function_and_undefined && not is_skipped)
		    then (indent_op ^ "   ", true)
		    else (indent_op        , true)
          |C_Command.Block l ->
            begin
              match (List.hd (List.rev l)) with
              | C_Command.Return _ ->
                  (String.sub indent_op 0 ((String.length indent_op) - 3), false)
              | _ -> (indent_op, false)
            end
          |_-> (indent_op,false)
      in
      let mc_return_val =
        try
          if (Options.getValueOfBool "cf")
          then
            begin
              let init_region =
                let rec buildloclist lst loclist =
                  match lst with [] -> loclist
                  | a ::rest ->
                      try
                        buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist)
                      with
                        Not_found ->
                          (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
                           failwith ("No function "^(a)^" defined"))
                in
                let loclist = match entries with
                  None -> buildloclist mainFunctions []
                | _ -> failwith "Unimplemented CF multi-entry" in
                This_Cf_C_Abstraction.create_region loclist false seedPreds initialState

              and error_region =
                match o_err_locs with
                  [] -> None
                | _ ->
                    Some (This_Cf_C_Abstraction.create_region o_err_locs true Predicate.True Predicate.True)

              in
              let cfmc i e = match This_LMC_CF.model_check i e with No_error -> 1 | _ -> 0 in
              let cfbmc i (Some e) =
                let start = Unix.times () in
                let rv = This_LMC_CFB.model_check i e in
                  let _ =
                    if (Options.getValueOfBool "reachloc") then
                    let finish = Unix.times () in
                    let time = int_of_float (finish.Unix.tms_utime -. start.Unix.tms_utime) in
                    let qid_all = !all_edge_id in
                    let qid_safe = List.filter (fun id -> not (This_LMC_CFB.is_reachable id)) qid_all in
                    List.iter (fun id -> update_query_cache [id] (1,time)) qid_safe
                  in
                rv
              in
              let model_check = if (Options.getValueOfBool "cfb") then cfbmc else cfmc in
              let mc_return_val = Stats.time "Actual model check[cf]" (model_check init_region) error_region in
              let _ = final_string := "" in
              if mc_return_val = 1 then
                (final_string := "\n\nNo error found.  The system is safe :-)\n";
                Message.msg_string Message.Major !final_string)
              else
                (if !TheoremProver.foci_sat_occured then
                   final_string := "\n\nFOCI couldn't find interpolants for the (probably spurious) counterexample :-(\n"
                 else
                   final_string := "\n\nError found! The system is unsafe :-(\n";
                 Message.msg_string Message.Major !final_string)
            end
          else
            begin
              (* vanilla tree unroll starts here *)
              let init_region =
                let rec buildloclist lst loclist =
                  match lst with [] -> loclist
                  | a ::rest ->
                      try
                        buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist)
                      with
                        Not_found ->
                          (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
                           failwith ("No function "^(a)^" defined"))
                in
                let loclist = match entries with
                  None -> buildloclist mainFunctions []
                | Some entries -> entries in
                This_C_Abstraction.create_region
                  loclist false seedPreds initialState

              and error_region =
                match o_err_locs with
                  [] -> None
                | _ -> Some (This_C_Abstraction.create_region o_err_locs true Predicate.True Predicate.True)
              in

	      (* Fold_left that folds both a[i] and a[i+1] *)
	      let rec fold_left_two f a b lst =
                match lst with
                    [elt] -> f a elt None
                  | hd::tl ->  fold_left_two f (f a hd (Some(List.hd tl))) b tl
	      in
              let mc_return_val =
                match (Options.getValueOfString "checkRace" ) with
                |  "tar" -> (This_LMC.model_check_tar init_region error_region)
                |  "extar" -> (This_LMC.model_check_extar init_region error_region)
                |  _ -> (Stats.time "real model check" (This_LMC.model_check init_region) error_region) in
              match (mc_return_val) with
                No_error ->
                  if not (Options.getValueOfBool "interactive") then
                    let _ = final_string := "\n\nNo error found.  The system is safe :-)\n"
                    in
                      Message.msg_string Message.Major !final_string

              | Error_reached (reg_i, op_list, reg_e) ->
                  if Options.getValueOfBool "interactive" then begin
                    let coords = This_C_Abstraction.Region.get_location_id reg_e in
                    let loc = C_System_Descr.lookup_location_from_loc_coords coords in
                    reachedLoc := Some loc
                  end else begin
                    if !TheoremProver.foci_sat_occured then
                      final_string := "\n\nFOCI couldn't find interpolants for the (probably spurious) counterexample :-(\n\n Error trace:\n"
                    else
		      (* Note that this string is important for external tools that will parse this trace *)
                      final_string := "\n\nError found! The system is unsafe :-(\n\n Error trace: (final)\n";
                    Message.msg_string Message.Major !final_string;
                    Message.msg_string Message.Normal
                      (indent_loc ^ (C_System_Descr.location_to_string
                                       (C_System_Descr.get_source (List.hd op_list)))) ;
                    let _ = make_hash_oplist op_list in
                    let _ = fold_left_two print_op ("",true) None op_list in
		    if (Options.getValueOfBool "vardec") then
		      teststatement (List.hd cil_files) op_list;
		    (* Note that this string is important for external tools that will parse this trace *)
                    Message.msg_string Message.Major "Error trace ends (final)";

                    Message.msg_string Message.Normal "\n\n" ;
                    Message.msg_printer Message.Normal This_C_Abstraction.Region.print reg_e;
                    (try
                      Message.msg_string Message.Normal "Test case:" ;
                      let rec _prtest l = match l with [] -> ()
                      | (sym, v) :: t ->
                        Message.msg_string Message.Normal (Expression.toString sym);
                        Message.msg_string Message.Normal (string_of_float v) ;
                        _prtest t
                      in
                      _prtest (This_C_Abstraction.genTests reg_i) ;
                    with _ -> (Message.msg_string Message.Error "Test generation failed." ; ) ; (* end try *) );
                    (*if (Options.getValueOfBool "xml") then Absutil.dumpXml
                    * op_list *)
                  end
              | Race_condition ((reg1,oplist1, reg1'), (reg2, oplist2, reg2'), lv1, lv2) ->
                  let _ = final_string := "\n\nRace condition found! The system is unsafe :-(\n\n\n"
                     ^(Printf.sprintf "\n On the lvalues %s, %s \n" (Ast.Expression.lvalToString lv1)
                         (Ast.Expression.lvalToString lv2))
                  in
                  Message.msg_string Message.Major "\n\nRace condition found! The system is unsafe :-(\n\n\n";
                  Message.msg_string Message.Normal (Printf.sprintf "\n On the lvalues %s, %s \n"
                                                       (Ast.Expression.lvalToString lv1) (Ast.Expression.lvalToString lv2));
                  Message.msg_string Message.Error (Printf.sprintf "\n On the lvalues %s, %s \n"
                                                      (Ast.Expression.lvalToString lv1) (Ast.Expression.lvalToString lv2));
                  (* print out the first trace *)
                  Message.msg_string Message.Normal "Printing out the first trace.\n" ;
                  Message.msg_printer Message.Normal This_C_Abstraction.Region.print reg1 ;
                  Message.msg_string Message.Normal "\n\n" ;

                  Message.msg_string Message.Normal
                    (indent_loc ^ (C_System_Descr.location_to_string
                                     (C_System_Descr.get_source (List.hd oplist1)))) ;
                  let _ = fold_left_two  print_op ("",true) None oplist1 in
                  Message.msg_string Message.Normal "\n\n" ;
                  Message.msg_printer Message.Normal This_C_Abstraction.Region.print reg1';
                  (* print out the second trace *)
                  Message.msg_string Message.Normal "\n\nPrinting out the second trace.\n" ;
                  Message.msg_printer Message.Normal This_C_Abstraction.Region.print reg2 ;
                  Message.msg_string Message.Normal "\n\n" ;

                  Message.msg_string Message.Normal
                    (indent_loc ^ (C_System_Descr.location_to_string
                                     (C_System_Descr.get_source (List.hd oplist2)))) ;
                  let _ = fold_left_two print_op ("",true) None oplist2 in
                  Message.msg_string Message.Normal "\n\n" ;
                  Message.msg_printer Message.Normal This_C_Abstraction.Region.print reg2';
            end
        with e ->
          begin
            final_string := ("Exception raised :("^(Printexc.to_string e));
            if (Options.getValueOfBool "cf" || Options.getValueOfBool "cfb")
            then (This_Cf_C_Abstraction.notify_result !final_string;
                  This_Cf_C_Abstraction.dump_abs ())
            else (This_C_Abstraction.notify_result !final_string;
                  This_C_Abstraction.dump_abs ());
            Message.msg_string Message.Error !final_string;
            (* Print stats *)
            Message.msg_string Message.Normal "\n\nModel checker stats:\n" ;
            Message.msg_printer Message.Normal This_LMC.print_stats () ;
            Message.msg_string Message.Normal "\n\nAbstractor stats:\n" ;
            Message.msg_printer Message.Normal This_C_Abstraction.print_stats () ;
            raise e
          end
      in
      ()
    end ;
    if (Options.getValueOfBool "cf")
    then
      begin
        let print_stats = if Options.getValueOfBool "cfb" then
          This_LMC_CFB.print_stats else This_LMC_CF.print_stats
        in
        Message.msg_string Message.Normal "\n\nModel checker stats:\n" ;
        Message.msg_printer Message.Normal print_stats () ;
        Message.msg_string Message.Normal "\n\nAbstractor stats:\n" ;
        Message.msg_printer Message.Normal This_Cf_C_Abstraction.print_stats ();
        This_Cf_C_Abstraction.notify_result !final_string;
        This_Cf_C_Abstraction.exit_abstraction ()
    end
    else
      begin
        Message.msg_string Message.Normal "\n\nPhi Regions:\n" ;
        Message.msg_string Message.Normal (This_LMC.print_phis false) ;
        Message.msg_string Message.Normal "\n\nModel checker stats:\n" ;
        Message.msg_printer Message.Normal This_LMC.print_stats () ;
        Message.msg_string Message.Normal "\n\nAbstractor stats:\n" ;
        Message.msg_printer Message.Normal This_C_Abstraction.print_stats () ;
        This_C_Abstraction.notify_result !final_string;
        This_C_Abstraction.exit_abstraction ()
      end
  (* end model_check () *)


(******************************************************************************)
let get_edges funlabel f =
  (* given a function f, find out all edges in its CFA where it   calls the function funlabel *)
   let edgelist = ref [] in
        let _worker e =
          match (C_System_Descr.get_command e).C_Command.code with
            C_Command.FunctionCall ex ->
              begin
                match ex with
                  Expression.Assignment (_, _, Expression.FunctionCall (Expression.Lval (Expression.Symbol fl), _))
                | Expression.FunctionCall (Expression.Lval (Expression.Symbol fl), _) ->
                    if fl = funlabel then edgelist := e :: !edgelist
                | _ -> ()
              end
          | _ -> ()
        in
        C_System_Descr.map_edges_fname _worker f ;
        !edgelist



(******************************************************************************)
(* list of edges -> expression
returns list of lvals that are on edges that are incoming to the beginnings of the edges from the list supplied
*)
let get_lvals_on_guards elist =
  List.fold_left (fun allofthem e ->
	(* the result of fold will be the concatenation of lvs_of_e returns *)
		(* returns list of lvalues on predicate edges going into Node e *)
    let lvs_of_e =
      let src = C_System_Descr.get_source e in
      let incoming = C_System_Descr.get_ingoing_edges src in
      List.fold_left (fun sofar inedge ->
        match (C_System_Descr.get_command inedge).C_Command.code with
        C_Command.Pred p ->
          begin
            let lv_list = Ast.Predicate.lvals_of_predicate p in
            Message.msg_string Message.Normal
            ("The lvalues in this edge are: "^(Misc.strList (List.map Ast.Expression.lvalToString lv_list)));
            lv_list @ sofar
          end
        | _ -> sofar ) [] incoming
    in
    lvs_of_e @ allofthem) [] elist


(******************************************************************************)
 let local_synt_partition edges =
   let fname_edge_table = Hashtbl.create 11 in
   List.iter
   (fun e -> let src = C_System_Descr.get_source e in
   let incoming = C_System_Descr.get_ingoing_edges src in
   List.iter
   (fun inedge ->
     match (C_System_Descr.get_command inedge).C_Command.code with
     C_Command.Pred p ->
       begin
         let lv = Ast.Predicate.varsOfPred p in
         Message.msg_string Message.Normal "The lvalues in this edge are: ";
         List.iter (Message.msg_string Message.Normal ) lv ;
         let thisfp = List.hd lv in
         try let old = Hashtbl.find fname_edge_table thisfp in
             Hashtbl.replace fname_edge_table thisfp (e :: old)
         with Not_found -> Hashtbl.add fname_edge_table thisfp ([ e ])
       end
     | _ -> failwith "Strange incoming edge!")
   incoming)
   edges ;
   let l = ref [] in
   Hashtbl.iter (fun key data -> l := data::!l) fname_edge_table ;
   !l


(******************************************************************************)
(* TBD:SPEED quadratic in elist *)
let aliased_lvalue_cluster elist =
  let cl_tbl = Hashtbl.create 101 in
  let cllv_tbl = Hashtbl.create 101 in
  let keys = ref [] in
  let _proc e =
    let lv_l = get_lvals_on_guards [e] in
    let check idx =
      let lvi_l = Hashtbl.find cllv_tbl idx in
      let hit =
        Misc.list_exists_cross2 (fun l l' -> l=l' ||
        (*AliasAnalyzer.askAliasDatabase l l'*) AliasAnalyzer.queryAlias
        (Expression.Lval l) (Expression.Lval l'))
        lvi_l lv_l
      in
      let _ =
        if hit then
        let _ = Misc.hashtbl_check_update cl_tbl idx e in
        Hashtbl.replace cllv_tbl idx (Misc.sort_and_compact (lv_l@lvi_l))
      in
      hit
    in
    try ignore(List.find check !keys) with Not_found ->
      let newkey = List.length !keys + 1 in
      Hashtbl.replace cllv_tbl newkey lv_l;
      Hashtbl.replace cl_tbl newkey [e];
      keys := newkey ::!keys;
  in
  let output_cluster k =
            let qids =
              let qid = query_id (Hashtbl.find cl_tbl k) in
              (query_id_to_string qid)
            in
            let lvs =
              let ll = List.map Ast.Expression.lvalToString (Hashtbl.find cllv_tbl k) in
              String.concat "," ll
            in
            Message.msg_string Message.Normal
            (Printf.sprintf "[QUERY] id: %s lvals: %s" qids lvs)
  in
    List.iter _proc elist;
    List.iter output_cluster !keys;
    List.map snd (Misc.hashtbl_to_list cl_tbl)



let get_user_query uqid_list all_call_edges =
  let elist = List.flatten all_call_edges in
  let get_query_from_qid uqid =
    let tbl = Hashtbl.create 31 in
    let rec proc uql =
      match uql with [] -> ()
      | (x,y)::t -> Hashtbl.replace tbl (x,y) true;proc t
    in
    let ff e = let [id] = query_id [e] in Hashtbl.mem tbl id in
    proc uqid;
    List.filter ff elist
  in
  List.map get_query_from_qid uqid_list


(******************************************************************************)
let cluster_edges all_call_edges =
  let funlabel = Options.getValueOfString "funlabel" in
  let funmode = Options.getValueOfInt "funmode" in
  let epl =
    if funlabel = "" then [List.flatten all_call_edges]
    else if funmode = 0 then all_call_edges
    else if funmode = 1 then List.map (fun e -> [e]) (List.flatten all_call_edges)
    else if funmode = 2 then List.flatten (List.map local_synt_partition all_call_edges)
    else if funmode = 3 then Stats.time "alias cluster" aliased_lvalue_cluster (List.flatten all_call_edges)
    else if funmode = 4 then get_user_query [Misc.group_pairwise (Options.getValueOfIntList "qid")] all_call_edges
    else if funmode = 5 then get_user_query (lookup_user_queries ()) all_call_edges
    else all_call_edges
  in
  let s1 = ("#Queries : "^(string_of_int (List.length epl))) in
  Message.msg_string Message.Major s1;
  Message.msg_string Message.Error s1;
  List.map (List.sort compare) epl


(* conjunction of all atoms that have lvals from Pred's on edges preceeding ones from elist (or are global if -tg is on) *)
let filter_preds seedPreds elist =
  let lvt = Hashtbl.create 101 in
  let is_relevant p =
    let plvs = Ast.Predicate.lvals_of_predicate p in
    let tg = Options.getValueOfBool "tg" in
    List.exists (fun lv -> Hashtbl.mem lvt lv || (tg &&
    (C_System_Descr.is_global lv))) plvs
  in
	(* place into lvt the lvalues that are on edges that income to tails of the edges from elist *)
  let _ =  List.iter (fun lv -> Hashtbl.replace lvt lv true) (get_lvals_on_guards elist) in
  let atoms = Ast.Predicate.getAtoms seedPreds in
	(* get all atomic predicates that have lvals from the list gotten above (or are global vars if -tg is on *)
  let atoms' = List.filter is_relevant atoms in
  Predicate.conjoinL atoms'



(******************************************************************************)
(* calls model_check with a particular query *)
let modelCheck o_err_locs entries seedPreds cil_files =
  let _ = if (Options.getValueOfBool "qc") then load_query_cache () in
   let funlabel = Options.getValueOfString "funlabel" in
   let replacefunlabel = "__BLAST_"^funlabel in
   let callers = Misc.sort_and_compact
   (List.map (snd) (C_System_Descr.calls_made_into funlabel)) in
   let d = ref 0 in
   let cumulative = ref 0 in
   let successes = ref 0 in
   let failures = ref 0 in
   let no_new = ref [] and
       timeout = ref [] and
       bugs = ref []
   in
   let replace_edges label elist =
     Message.msg_string Message.Error ("Replace Edges with :"^label);
     let rep e =
        match (C_System_Descr.get_command e).C_Command.code with
       C_Command.FunctionCall (Expression.FunctionCall (_,args)) ->
         C_System_Descr.replace_code_in_cfa e
         (Expression.FunctionCall (Expression.Lval (Expression.Symbol label),args))
        | _ -> Message.msg_string Message.Error ("Bad edge in replace_edges!")
     in
       List.iter rep elist
   in
   let run_one_check elist =
     if elist = [] then () else
       begin
         let error_lvs = get_lvals_on_guards elist in
         let _ = C_System_Descr.set_error_lvals error_lvs in
         let edge_fname e =
           C_System_Descr.get_location_fname (C_System_Descr.get_source e)
         in
         let edge_fnames = List.map edge_fname elist in
         let efn = Misc.strList edge_fnames in
         Options.set_errorseed  edge_fnames ;
         Message.msg_string Message.Major (Printf.sprintf "Call %d (cumulative %d) in fun:%s" !d !cumulative efn ) ;
         incr d ;
         cumulative := !cumulative + (List.length elist) ;
         if (!d <= Options.getValueOfInt "skiptest"
             ||
             (Options.getValueOfBool "selecttest") && not (List.mem !d (Options.getValueOfIntList "checktest"))) then
            Message.msg_string Message.Normal ("Skipping query # "^(string_of_int !d))
         else
          begin
          let qid = query_id elist in
          let qids = query_id_to_string qid in
          let ms1 = (Printf.sprintf "Trying error call # %d (cumulative %d) edges: %d, in functions %s, qid: %s"
            !d !cumulative (List.length elist) efn qids)
          in
            Message.msg_string Message.Error ms1;
          Message.msg_string Message.Major ms1 ;
          Message.msg_string Message.Major ("Lvalues on edges in this call: ");
          Message.msg_string Message.Error ("Lvalues on edges in this call: ");
          List.iter (fun lv -> let ls = Ast.Expression.lvalToString lv in
            Message.msg_string Message.Major ls ; Message.msg_string Message.Error ls )
            (get_lvals_on_guards elist) ;
          let start = Unix.times () in
          let (outcome,time) =
            match lookup_query_cache qid with
            Some(o,t) ->
              begin
                Message.msg_string Message.Error  (Printf.sprintf "Query Cache: %d, time = %d" o t);
                (o,t)
              end
            | None ->
                  begin
                    let oref = ref 0 in
                    replace_edges replacefunlabel elist;
                    begin
                    try
                      let seedPreds = filter_preds seedPreds elist in
                      model_check o_err_locs entries seedPreds cil_files ;
                      Message.msg_string Message.Error !final_string;
                      if (Misc.is_substring !final_string ":-(" ) then
                        if (Options.getValueOfBool "norefine") then
                           (no_new := !d :: !no_new)
                        else
                        (bugs := !d :: !bugs;  oref := -1)
                      else ( oref := 1)
                    with
                    Ast.TimeOutException ->
                      (Message.msg_string Message.Normal "This check times out!";
                      timeout:= !d :: !timeout)
                    | e ->
                      (Message.msg_string Message.Error (Printexc.to_string e) ;
                      Message.msg_string Message.Error "Exception raised !" ;
                      no_new := !d :: !no_new)
                    end;
                    replace_edges funlabel elist;
                    let time =
                      let finish = Unix.times () in
                      int_of_float (finish.Unix.tms_utime -. start.Unix.tms_utime)
                      in
                    update_query_cache qid (!oref,time);
                    (!oref,time)
                  end
          in
          let os =
            if outcome = 1 then (incr successes; "SAFE")
            else if outcome = -1 then (incr successes; "UNSAFE")
            else (no_new := !d::!no_new; "FAIL")
          in
          Message.msg_string Message.Major (Printf.sprintf "Query %d: %d s, %s" !d time os) ;
          Message.msg_string Message.Error (Printf.sprintf "Query %d: %d s, %s" !d time os);
         end
    end
  in (* end run_one_check *)

  let run_reachloc_pass edge_partition_l =
    let proc i elist =
      let issafe id = match (lookup_query_cache [id]) with Some(1,_) -> true | _ -> false in
      let qid = query_id elist in
      let (res,rss) =
        if List.for_all issafe qid then (incr successes; (1,"SAFE"))
        else (no_new:=i::!no_new;(0,"FAIL"))
      in
      Message.msg_string Message.Major (Printf.sprintf "Query %d: %d s, %s" i (-2) rss);
      Message.msg_string Message.Error (Printf.sprintf "Query %d: %d s, %s" i (-2) rss);
      update_query_cache qid (res,-2);
      (i+1)
    in
    if not (Options.getValueOfBool "cfb") then failwith "reachloc: only with CFB"
    else
      begin
        let elist = List.flatten edge_partition_l in
        all_edge_id := query_id elist;
        (*replace_edges replacefunlabel elist;*)
        Options.set_errorseed [funlabel];
        let seedPreds = filter_preds seedPreds elist in
        model_check o_err_locs entries seedPreds cil_files;
        (*replace_edges funlabel elist;*)
        ignore(List.fold_left proc 0 edge_partition_l)
      end
  in

  if funlabel = "" then model_check o_err_locs entries seedPreds cil_files
  else
    begin
      Message.msg_string Message.Error
       (Printf.sprintf "Number of modeled functions: %d" (List.length (C_System_Descr.list_of_functions ())));
       let all_call_edges = (List.map (get_edges funlabel) callers) in
       let numcalls = List.length all_call_edges in
       let numinstr = List.fold_left (fun sofar thisone -> sofar + (List.length thisone)) 0 all_call_edges in
       Message.msg_string Message.Major ("#Functions calling error: "^(string_of_int (numcalls))) ;
       Message.msg_string Message.Error ("#Functions calling error: "^(string_of_int (numcalls))) ;
       Message.msg_string Message.Major ("#Callsites calling error: "^(string_of_int (numinstr))) ;
       Message.msg_string Message.Error ("#Callsites calling error: "^(string_of_int (numinstr))) ;
       let qids = query_id_to_string (query_id (List.flatten all_call_edges)) in

       Message.msg_string Message.Major ("All query edges: "^qids);
       Message.msg_string Message.Error ("All query edges: "^qids);
       let edge_partition_l = cluster_edges all_call_edges in
       if (Options.getValueOfBool "cfb" && Options.getValueOfBool "reachloc")
       then (run_reachloc_pass edge_partition_l)
       else (List.iter run_one_check edge_partition_l);
             (* print stats *)
      Message.msg_string Message.Normal ("Number of safe uses is "^(string_of_int (!successes))) ;
      Message.msg_string Message.Error ("Number of safe uses is "^(string_of_int (!successes))) ;
      Message.msg_string Message.Normal ("Number of bugs is "^(string_of_int (!failures))) ;
      Message.msg_string Message.Error ("Number of bugs is "^(string_of_int (!failures))) ;
        List.iter (fun d -> Printf.fprintf Pervasives.stdout "Bug (%d) " d ;
                Printf.fprintf Pervasives.stderr "Bug (%d) " d ;
           ) !bugs ;
      Printf.fprintf Pervasives.stdout "\n" ;
      Printf.fprintf Pervasives.stderr "\n" ;
      Message.msg_string Message.Normal ("Failed : (Blast exception, including no new predicates )") ;
      Message.msg_string Message.Error ("Failed : (Blast exception, including no new predicates)") ;
        List.iter (fun d -> Printf.fprintf Pervasives.stdout "%d " d ;
                Printf.fprintf Pervasives.stderr "%d " d ;
           ) !no_new ;
      Message.msg_string Message.Normal ("Timed out : ") ;
      Message.msg_string Message.Error ("Timed out : ") ;
        List.iter (fun d -> Printf.fprintf Pervasives.stdout "%d " d ;
                Printf.fprintf Pervasives.stderr "%d " d ;
           ) !timeout ;
        Printf.fprintf Pervasives.stdout "\n\n-selecttest " ;
        List.iter (fun d -> Printf.fprintf Pervasives.stdout "-checktest %d " d ;
           ) !timeout ;
        Printf.fprintf Pervasives.stdout "\n\n" ;
        Printf.fprintf Pervasives.stderr "\n\n" ;
    end



(******************************************************************************)
(* main hook into TI_model_checker *)
let ti_modelcheck seedPreds =
   let module This_C_Abstraction =
      Abstraction.Make_C_Abstraction(C_System_Descr) in
  let module Region = This_C_Abstraction.Region in
  let module This_LMC = LazyModelChecker.Make_Lazy_Model_Checker(This_C_Abstraction) in
  This_C_Abstraction.assumed_invariants := Stats.time "read invariants" constructInvariants();
    This_C_Abstraction.assumed_invariants_region := This_C_Abstraction.assumed_invariants_list_to_region();
    let initStateFile = Options.getValueOfString "init" in
  let initialState = Predicate.conjoinL (Stats.time "read initial state" constructInitialPredicates initStateFile) in
  let mainFunctions = Options.getValueOfStringList "main" in
  let _ = This_C_Abstraction.initialize_abstraction () in
  let init_region =
      let rec buildloclist lst loclist =
        match lst with [] -> loclist
          | a ::rest ->
              try
                buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist)
              with
                  Not_found ->
                    (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
                     failwith ("No function "^(a)^" defined"))
      in
      let loclist = buildloclist mainFunctions [] in
        This_C_Abstraction.ac_create_region false loclist false seedPreds  initialState
  in
    Message.msg_string Message.Normal "\n\n\n********** Now running the model-checker **********\n\n" ;
    flush stdout;
   try
        This_LMC.compute_transition_invariant ()
   with e ->
        begin
          Message.msg_string Message.Error ("Exception raised :("^(Printexc.to_string e));
          This_C_Abstraction.dump_abs ();
          Message.msg_printer Message.Normal This_C_Abstraction.print_stats ();
          exit 0
        end


(******************************************************************************)
let cfbmodelcheck seedPreds o_err_loc =
  let module This_Cf_C_Abstraction =
    CfAbstraction.Make_C_Abstraction(C_System_Descr)
  in
  let module This_LMC_CFB =
    CfbLazyModelChecker.Make_Lazy_Model_Checker(This_Cf_C_Abstraction) in
  let module Region = This_Cf_C_Abstraction.Region in
  This_Cf_C_Abstraction.assumed_invariants := Stats.time "read invariants" constructInvariants ();
  This_Cf_C_Abstraction.assumed_invariants_region := This_Cf_C_Abstraction.assumed_invariants_list_to_region();
  let initStateFile = Options.getValueOfString "init" in
  let initialState = Predicate.conjoinL (Stats.time "read initial state" constructInitialPredicates initStateFile) in
  let mainFunctions = Options.getValueOfStringList "main" in
 let _ = This_Cf_C_Abstraction.initialize_abstraction () in
    Message.msg_string Message.Normal "\n\n\n********** Now running the cfb-model-checker **********\n\n" ;
    flush stdout;
     let init_region =
      let rec buildloclist lst loclist =
        match lst with [] -> loclist
          | a ::rest ->
              try
                buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist)
              with
                  Not_found ->
                    (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
                     failwith ("No function "^(a)^" defined"))
      in
      let loclist = buildloclist mainFunctions [] in
        This_Cf_C_Abstraction.create_region loclist false seedPreds  initialState
    and error_region =
      match o_err_loc with
        | Some err_loc ->
            (This_Cf_C_Abstraction.create_region [err_loc] true Predicate.True Predicate.True)
        | None -> failwith "FMC: No error location!"
    in
    try
        This_LMC_CFB.model_check init_region error_region
    with e ->
      begin
        Message.msg_string Message.Error ("Exception raised :("^(Printexc.to_string e));
        raise e
      end






(******************************************************************************)
(* the main hook into the focimodelchecker *)
(* no longer supported (SKY) *)

(******************************************************************************)
(*  TEST MODEL CHECKER NO LONGER SUPPORTED
let testModelCheck i seedPreds =
  let initStateFile = Options.getValueOfString "init" in
  let initialState = Predicate.conjoinL (Stats.time "read initial state" constructInitialPredicates initStateFile) in
  let errorPredFile = Options.getValueOfString "error" in
  let errorPred =
    let plist = (Stats.time "read error state" constructInitialPredicates errorPredFile) in
    if plist = [] then Predicate.True else List.hd plist
  in
  if i = 0 then
    TestModelChecker.test_model_check initialState errorPred seedPreds
  else
    TestModelChecker.ccured_model_check initialState errorPred seedPreds
 *)

(******************************************************************************)
let runquery queryfile seedPreds =
  (*let inchan = open_in queryfile in
  let lexbuf = Qlflex.init inchan in
  Message.msg_string Message.Normal "In runquery -- About to parse" ;
  let stmts = Qlparse.main Qlflex.token lexbuf in
  Message.msg_string Message.Normal "In runquery -- Parsed input, now running statements" ;
  Ql.runStmts stmts*)
  failwith "No queries implemented"

let rec split str delim =
  if(str = "") then
    []
  else
    if (String.contains str delim) then
      let lindex = String.index str delim in
      let first_part= String.sub str 0 lindex  in
      let tail = String.sub str (lindex + 1) ((String.length str) - lindex -1) in
        first_part::(split tail delim)
    else
      [str]


(*
let interactive () =
  (* Shell mode. Read inputs from stdin and execute them. *)

  Message.set_verbosity Message.Quiet;

  let reachLabel = (Options.getValueOfString "L")
  and source_files = (Options.getSourceFiles ()) in
  (* No reachLabel has been specified on the command line *)
  if (source_files = [])
  then
    begin
      Message.msg_string Message.Error ("Error: no input source file specified") ;
      exit 1
    end ;
  Message.msg_string Message.Normal ("Parsing files: " ^ (Misc.strList source_files)) ;

  let cil_files = Stats.time "Parse:" C_System_Descr.initialize_cil source_files in
  (* First do CIL specific optimizations, e.g., slicing. *)

  (* To do the slicing, we have to first read in the instrumentation predicates. *)
  (* Add seed predicates, if any to system *)
  let seedPredsFile = Options.getValueOfString "pred" in
  let seedPreds = Predicate.conjoinL (Stats.time "read seeds" constructInitialPredicates seedPredsFile)
  in
(*
   if (Options.getValueOfInt "O" > 1) then Panalysis.spec_slice cil_files seedPreds ;
 *)
  (* Then build CFA *)
  Stats.time "Build CFA:" C_System_Descr.initialize_blast cil_files;
  List.iter SourceStats.printLines source_files;
  List.iter SourceStats.printStats cil_files;
  Format.printf ".\n";
  Format.print_flush ();

  Message.msg_string Message.Debug "\n\n\n\n\nPrinting System Description\n\n" ;
  (*TODO unbound value C_System_Descr.print_system_dot () ; *)
  Message.msg_printer Message.Debug C_System_Descr.print_system () ;


  (* Do program analysis optimizations at the CFA level. *)
  Stats.time "Alias analysis" AliasAnalyzer.constructAliasDatabase () ;
  AliasAnalyzer.constructMustAliasDatabase () ;

  (* NOW -- we can build the modifies information, and THEN, add the call hooks. *)

  Stats.time "Modifies Database"
    (C_System_Descr.post_alias_analysis AliasAnalyzer.get_lval_aliases_scope) ();

  Message.msg_string Message.Normal "back from post_alias_analysis";
    (*
     (* Construct call graph and check for recursion *)
     Stats.time "Call graph construction" C_System_Descr.compute_callgraph ();
     let callgraph_name = Options.getValueOfString "callgraph" in
     if callgraph_name = "" then () else
     C_System_Descr.output_callgraph callgraph_name ;
     C_System_Descr.output_call_paths ();
   *)


  (* JHALA: what does this do ?
 Rupak: The old modifies info computation. Retired.
  if (Options.getValueOfString "alias" = "bdd") then AliasAnalyzer.constructModifiesInfo () ;
*)

  let prompt () =
    Format.printf "> @?" in
  let parse_error () = Format.eprintf "Syntax Error\n@?" in

  let lexer = GL.make_lexer ["QUIT"; "SOURCE"; "TARGET"; "REACH"] in

  let entry_locs = ref [] in
  let err_locs = ref [] in

  let split_hash fname_line =
                let fname_line_list = split fname_line '#' in            (* have not taken care of exceptions till now:Rajhans PENDING*)
                let first_part = (List.hd fname_line_list) in
                let second_part = (List.hd (List.tl fname_line_list)) in
                let fname = String.sub first_part 0 ((String.length first_part) - 1) in
                let _line = String.sub second_part 1 ((String.length second_part) - 1) in
                let line = int_of_string _line in
                (fname,line) in


  let do_one st =
     match ST.next st with
      GL.Kwd "QUIT" -> raise End_of_file

   | GL.Kwd "SOURCE" ->
        (match ST.next st with
           GL.String fname_line ->
                (match (split_hash fname_line) with
                (fname,line) ->
                        print_string ("hello 1 " ^ fname ^ " " ^ (string_of_int line) ^ "\n") ;
                        let pos = (fname, line) in
                        let locs = C_System_Descr.get_position_sources pos in
                        print_string ("list length " ^ (string_of_int (List.length locs)) ^ "\n");
                (*      Format.fprintf fmt "@ @    @[<v>Location: " ;
                        print_string "it goes\n";
                        List.iter (Message.msg_printer Message.Normal C_System_Descr.print_location) locs ;
                        print_string "it ends\n";
                        let c1 = (List.hd locs) in
                        let c2 = c1.loc_id in
                        let _ = (match c with
                                        (a,_) -> ()
                                        | _ -> ()
                                ) in                *)


(*                      let _ = (match locs with
                                        [] -> _ ;
                                        (_,_,c )::tail ->
                                        let t = c.code in
                                        (match t with
                                                Block (s_list) -> print_int (length s_list)
                                                | _ -> ())
                                        | _ -> ()
                                )
                        in              *)
                        let _ = (match locs with [] -> Format.printf "loc list empty 1\n"
                                                | _ -> print_string ((C_System_Descr.location_to_string (List.hd locs)) ^ "\n")
                                ) in
                entry_locs := locs @ !entry_locs
                | _ -> ()
                )
        | _ -> parse_error ())

    | GL.Kwd "TARGET" ->
        (match ST.next st with
            GL.String fname_line ->
                (match (split_hash fname_line) with
                (fname,line) ->
                        print_string ("hello 2 " ^ fname ^ " " ^ (string_of_int line) ^ "\n") ;
                        let pos = (fname, line) in
                        let locs = C_System_Descr.get_position_sources pos in
                        print_string ("list length " ^ (string_of_int (List.length locs)) ^ "\n");
                        let _ = (match locs with [] -> Format.printf "loc list empty 2\n"
                                                | _ -> print_string ((C_System_Descr.location_to_string (List.hd locs)) ^"\n")
                                ) in
                err_locs := locs @ !err_locs
                | _ -> ()
                )
        | _ -> parse_error ())

(*   | GL.Kwd "SOURCE" ->
        (match ST.next st with
           GL.String fname ->           (*//commented. This part accepts SOURCE fname line*)
            (match ST.next st with
              GL.Int line ->
                let pos = (fname, line) in
                let locs = C_System_Descr.get_position_sources pos in
                let _ = match locs with [] -> Format.printf "loc list empty\n"
                                | _ -> Message.msg_string Message.Normal ("format" ^ (C_System_Descr.location_to_string (List.hd locs))) in
                entry_locs := List.append locs !entry_locs

            | _ -> parse_error ())
        | _ -> parse_error ())
    | GL.Kwd "TARGET" ->
        (match ST.next st with
          GL.String fname ->
            (match ST.next st with
              GL.Int line ->
                let pos = (fname, line) in
                let locs = C_System_Descr.get_position_sources pos in
                err_locs := locs @ !err_locs
            | _ -> parse_error ())
        | _ -> parse_error ())                *)

    | GL.Kwd "REACH" ->
          let reachFrom entry_loc =
          let (srcFname, srcLine) =
           match C_System_Descr.get_source_position entry_loc with
              None -> failwith "Bad REACH source!"
            | Some (fname, line, _) -> fname, line in
          let rec reaches err_loc =
            (*match err_locs with
              [] -> ()
            | _ ->*)
                modelCheck [err_loc] (Some [entry_loc]) seedPreds cil_files;
                print_string ("entrl loc " ^ (C_System_Descr.location_to_string entry_loc) ^ "\n");
                print_string ("err loc " ^ (C_System_Descr.location_to_string err_loc) ^ "\n\n");
(*              Format.printf  (C_System_Descr.location_to_string !reachedLoc);*)
                match !reachedLoc with
                  None -> ()
                | Some loc ->
                   (* let fname, line =*)
                      print_string ("loc " ^ (C_System_Descr.location_to_string loc) ^ "\n");
                      match C_System_Descr.get_source_position loc with
                        None -> failwith "Bad REACH target!"
                      | Some (fname, line, _) ->
                          print_string "sucess\n";
                          Format.printf "REACH \"%s_#_%d\" \"%s_#_%d\"\n" srcFname srcLine fname line;
                        (*  fname, line in*)
                    reachedLoc := None;

                    (*reaches (List.filter
                               (fun x -> match C_System_Descr.get_source_position x with
                                 Some (fname', line', _) -> fname <> fname' || line <> line'
                               | None -> true) err_locs)*)
          in (*reaches !err_locs*)
          List.iter reaches !err_locs
        in
        let _ = match !entry_locs with
          [] ->  Format.printf "loc list empty 3\n"
          | _ -> ()  in
        List.iter reachFrom !entry_locs ;
        entry_locs := [];
        err_locs := [];
        Format.printf ".\n"
    | _ -> parse_error () in

  try
    while true do
      (*prompt ();*)
      (try
        do_one (lexer (Stream.of_string (read_line ())))
      with ST.Failure -> ());
      Format.print_flush ()
    done
  with
  | End_of_file    -> Format.printf "\n@?"; exit 0

*)
(* subgoal manager *)




(******************************************************************************)


(* INTERACTIVE MODE NO LONGER SUPPORTED
let interactive () =
  (* Shell mode. Read inputs from stdin and execute them. *)

  Message.set_verbosity Message.Quiet;

  let reachLabel = (Options.getValueOfString "L")
  and source_files = (Options.getSourceFiles ()) in
  (* No reachLabel has been specified on the command line *)
  if (source_files = [])
  then
    begin
      Message.msg_string Message.Error ("Error: no input source file specified") ;
      exit 1
    end ;
  Message.msg_string Message.Normal ("Parsing files: " ^ (Misc.strList source_files)) ;

  let cil_files = Stats.time "Parse:" C_System_Descr.initialize_cil source_files in
  (* First do CIL specific optimizations, e.g., slicing. *)

  (* To do the slicing, we have to first read in the instrumentation predicates. *)
  (* Add seed predicates, if any to system *)
  let seedPredsFile = Options.getValueOfString "pred" in
  let seedPreds = Predicate.conjoinL (Stats.time "read seeds" constructInitialPredicates seedPredsFile)
  in
(*
   if (Options.getValueOfInt "O" > 1) then Panalysis.spec_slice cil_files seedPreds ;
 *)
  (* Then build CFA *)
  Stats.time "Build CFA:" C_System_Descr.initialize_blast cil_files;
(* OBSOLETE:
  List.iter SourceStats.printLines source_files;
  List.iter SourceStats.printStats cil_files;
  Format.printf ".\n";
*)
  Format.print_flush ();

  Message.msg_string Message.Debug "\n\n\n\n\nPrinting System Description\n\n" ;
  Message.msg_printer Message.Debug C_System_Descr.print_system () ;

  (* Do program analysis optimizations at the CFA level. *)
  Stats.time "Alias analysis" AliasAnalyzer.constructAliasDatabase () ;
  AliasAnalyzer.constructMustAliasDatabase () ;

  (* NOW -- we can build the modifies information, and THEN, add the call hooks. *)

  Stats.time "Modifies Database"
    (C_System_Descr.post_alias_analysis AliasAnalyzer.get_lval_aliases_scope) ();

  Message.msg_string Message.Normal "back from post_alias_analysis";
    (*
     (* Construct call graph and check for recursion *)
     Stats.time "Call graph construction" C_System_Descr.compute_callgraph ();
     let callgraph_name = Options.getValueOfString "callgraph" in
     if callgraph_name = "" then () else
     C_System_Descr.output_callgraph callgraph_name ;
     C_System_Descr.output_call_paths ();
   *)


  (* JHALA: what does this do ?
 Rupak: The old modifies info computation. Retired.
  if (Options.getValueOfString "alias" = "bdd") then AliasAnalyzer.constructModifiesInfo () ;
*)

  let prompt () =
    Format.printf "> @?" in
  let parse_error () = Format.eprintf "Syntax Error\n@?" in

  let lexer = GL.make_lexer ["QUIT"; "SOURCE"; "TARGET"; "REACH"] in

  let entry_locs = ref [] in
  let err_locs = ref [] in

  let do_one st =
    match ST.next st with
      GL.Kwd "QUIT" -> raise End_of_file
    | GL.Kwd "SOURCE" ->
        (match ST.next st with
          GL.String fname ->
            (match ST.next st with
              GL.Int line ->
                let pos = (fname, line) in
                let locs = C_System_Descr.get_position_sources pos in
                entry_locs := locs @ !entry_locs
            | _ -> parse_error ())
        | _ -> parse_error ())
    | GL.Kwd "TARGET" ->
        (match ST.next st with
          GL.String fname ->
            (match ST.next st with
              GL.Int line ->
                let pos = (fname, line) in
                let locs = C_System_Descr.get_position_sources pos in
                err_locs := locs @ !err_locs
            | _ -> parse_error ())
        | _ -> parse_error ())
    | GL.Kwd "REACH" ->
        let reachFrom entry_loc =
          let srcFname, srcLine =
            match C_System_Descr.get_source_position entry_loc with
              None -> failwith "Bad REACH source!"
            | Some (fname, line, _) -> fname, line in
          let rec reaches err_loc =
            (*match err_locs with
              [] -> ()
            | _ ->*)
                modelCheck [err_loc] (Some [entry_loc]) seedPreds cil_fles;
                match !reachedLoc with
                  None -> ()
                | Some loc ->
                    let fname, line =
                      match C_System_Descr.get_source_position loc with
                        None -> failwith "Bad REACH target!"
                      | Some (fname, line, _) ->
                          Format.printf "REACH \"%s\" %d \"%s\" %d\n" srcFname srcLine fname line;
                          fname, line in
                    reachedLoc := None;

                    (*reaches (List.filter
                               (fun x -> match C_System_Descr.get_source_position x with
                                 Some (fname', line', _) -> fname <> fname' || line <> line'
                               | None -> true) err_locs)*)
          in (*reaches !err_locs*)
          List.iter reaches !err_locs
        in List.iter reachFrom !entry_locs;
        entry_locs := [];
        err_locs := [];
        Format.printf ".\n"
    | _ -> parse_error () in

  try
    while true do
      (*prompt ();*)
      (try
        do_one (lexer (Stream.of_string (read_line ())))
      with ST.Failure -> ());
      Format.print_flush ()
    done
  with
  | End_of_file    -> Format.printf "\n@?"; exit 0
*)

(* subgoal manager *)


let print_copyright () =
  Message.msg_string Message.Normal "BLAST 2.7.1, Copyright (c) 2002-2011, ISPRAS, The BLAST Team."


let check_sv_comp_2014_property source_files reachLabel =
  let check_property property_file property_string =
    let fail_parsing () =
      failwith ("Can't parse property in file\n\t" ^ property_file ^ "\n:\n\t" ^ property_string)
    in
    if Str.string_match
         (Str.regexp "CHECK( *init(\\([a-zA-Z_][a-zA-Z0-9_]*\\)()), *LTL(G +\\([! ()a-zA-Z0-9_-]+\\)) *)")
         property_string
         0
    then begin
      try
        let main_function = Str.matched_group 1 property_string in
        let property = Str.matched_group 2 property_string in
        if [main_function] <> Options.getValueOfStringList "main" then
          failwith
            ("Main function specified in command line doesn't match the one specified in the property");
        if Str.string_match
             (Str.regexp
                "! *label(\\([a-zA-Z_][a-zA-Z0-9_]+\\))\\|valid-free\\|valid-deref\\|valid-memtrack")
             property
             0
        then begin
          try
            let label = Str.matched_group 1 property in
            if label <> reachLabel then
              failwith
                ("Error label specified in command line doesn't match the one specified in the property");
            () (* Normal return from the function *)
          with Not_found ->
            Message.msg_string Message.Normal "Unsupported property. Exiting with UNKNOWN verdict...";
            exit 0
        end else
          fail_parsing ()
      with Not_found ->
        fail_parsing ()
    end else
      fail_parsing ()
  in
  if Options.getValueOfBool "sv-comp14-match-prop" then begin
    match source_files with
    | [source_file] ->
      let basename = Filename.basename source_file
      and dirname = Filename.dirname source_file in
      let property_file =
        let prp_file = source_file ^ ".prp" in
        if Sys.file_exists prp_file then
          prp_file
        else
        let all_prp = Filename.concat dirname "ALL.prp" in
        if Sys.file_exists all_prp then
          all_prp
        else
          failwith ("Can't find either\n\t" ^ prp_file ^ "\nor\n\t" ^ all_prp ^
                    "\nrequired for SV-COMP'14 configuration")
      in
      let property_in_channel = open_in property_file in
      let input_length = in_channel_length property_in_channel in
      let properties_string = String.create input_length in
      really_input property_in_channel properties_string 0 input_length;
      close_in property_in_channel;
      List.iter (check_property property_file) (Str.split (Str.regexp "[\r\n]+") properties_string)
    | _  ->
      begin
        Message.msg_string Message.Error "More than one source file specified for sv-comp14 configuration";
        exit 5
      end
  end

(* main function: the glue *)
let ocaml_main () =
  try
    Sys.command "rm -f .newpred";
    Message.set_verbosity Message.Default;
    Options.buildOptionsDatabase ();
    print_copyright ();

    (* Initialize local profiler.  It uses options to set the correct timing function *)
    Stats.reset ();
    Stats.measure_sys_time (Options.getValueOfBool "systime")
                           (Options.getValueOfBool "noprofile")
                           ((float_of_int (Options.getValueOfInt "unaccprof")) /. 100.0);

    (* print in NORMAL mode the options of this run -- because it might be needed *)
    Message.msg_string Message.Normal "Blast was run with the following command line:";
    Message.msg_apply Message.Normal
                      (fun () ->
                         for i = 0 to Array.length (Sys.argv) - 1 do
                           print_string Sys.argv.(i) ; print_string " "
                         done)
                      ();
    print_newline ();

    if (Options.getValueOfBool "msvc") then
      Cil.msvcMode := true;
    Cil.initCIL ();

    Ast.Counter.set_max (Options.getValueOfInt "maxcounter");

    let reachLabel = (Options.getValueOfString "L") in
    let source_files = (Options.getSourceFiles ()) in

    if reachLabel = "" then begin (* No reachLabel has been specified on the command line *)
      Printf.printf "No error label specified!\n";
      Message.msg_string Message.Error ("No error label specified!");
      exit 3
    end;

    if (source_files == []) then begin
      Message.msg_string Message.Error ("Error: no input source file specified");
      exit 5
    end;

    check_sv_comp_2014_property source_files reachLabel;

    Message.msg_string Message.Normal ("Begin Parsing files: " ^ (Misc.strList source_files));
    let cil_files = Stats.time "Parse:" C_System_Descr.initialize_cil source_files in
    Message.msg_string Message.Normal ("Finished Parsing");

    (* First do CIL specific optimizations, e.g., slicing. *)
    (* To do the slicing, we have to first read in the instrumentation predicates. *)
    (* Add seed predicates, if any to system *)
    let seedPredsFile = Options.getValueOfString "pred" in
    let seedPreds = Predicate.conjoinL (Stats.time "read seeds" constructInitialPredicates seedPredsFile) in
    let aliasPreds =
      let aliasPredsFile = Options.getValueOfString "aliasfile" in
      Predicate.conjoinL (Stats.time "read alias pairs" constructInitialPredicates aliasPredsFile)
    in
    (* Eventually slicing was disabled anyway (why?) *)
    (*if (Options.getValueOfInt "O" > 1) then
      Panalysis.spec_slice cil_files seedPreds;*)

    (* Now build CFA *)
    Message.msg_string Message.Normal ("Begin Building CFA");
    Stats.time "Build CFA:" C_System_Descr.initialize_blast cil_files;
    Message.msg_string Message.Normal ("Finished Building CFA");

    Message.msg_string Message.Debug "\n\n\n\n\nPrinting System Description\n\n" ;
    (*TODO unbound value C_System_Descr.print_system_dot () ; *)
    Message.msg_printer Message.Debug C_System_Descr.print_system () ;

    (* CFA optimizations were eventually entirely disabled for some reason (?) *)
    (* THE COMMENT WAS: Do program analysis optimizations at the CFA level. *)
    (*Stats.time "Alias analysis" AliasAnalyzer.constructAliasDatabase ();
      Message.msg_string Message.Normal ("Finished Building Aliases");
      if (Options.getValueOfBool "aliasclos") then begin
      Stats.time "Alias closure" AliasAnalyzer.refineAliasDatabase () ;
      Message.msg_string Message.Normal ("Finished Refinig Aliases");
      end;
      AliasAnalyzer.constructMustAliasDatabase () ;
      Message.msg_string Message.Normal ("Finished Building Must Aliases");
      Stats.time "Modifies Database" (C_System_Descr.post_alias_analysis AliasAnalyzer.get_lval_aliases_scope) ();
      Stats.time "Add alias override"  (AliasAnalyzer.add_alias_override) aliasPreds;
      if (Options.getValueOfBool "nomusts") then begin
      Stats.time "Remove must aliases"  AliasAnalyzer.withdraw_musts ();
      end;
      Message.msg_string Message.Normal "back from post_alias_analysis";*)

    Message.msg_string Message.Normal "No alias analysis done: deferred to the first error found";

    (* Construct call graph and check for recursion *)
    Stats.time "Call graph construction" C_System_Descr.compute_callgraph ();
    let callgraph_name = Options.getValueOfString "callgraph" in
    if callgraph_name <> "" then
      C_System_Descr.output_callgraph callgraph_name ;
    C_System_Descr.output_call_paths ();

    (* TEST MODEL CHECKER NO LONGER SUPPORTED -- RUPAK *)
    (*if (Options.getValueOfBool "test") then begin
      testModelCheck 0  seedPreds;
      exit 0
      end;
      if Options.getValueOfBool "ccured" then begin
      estModelCheck 1  seedPreds; (* here is where the ccured function gets called *)
      exit 0
      end;*)

    TheoremProver.init_tp();
    (* no need to launch the external program now *)
    (*ignore(TheoremProver.get_clp_prover ());
      ignore(FociInterface.get_foci ());
      ignore(SMTLIBInterface.get_smt_solver ());*)


    let queryfile = Options.getValueOfString "query" in
    if queryfile <> "" then begin
      runquery queryfile seedPreds
    end else begin (* Add the sym_var_hooks for CF reachability *)
      let error_location =
        match (C_System_Descr.get_locations_at_label reachLabel) with
        | [l] -> Some l
        | [] when Options.getValueOfString "checkRace" <> "" -> None
        | [] ->
          (* If we didn't find an error, it may be because of its syntactic inreachability. *)
          (* In some modes, this should be SAFE. *)
          if Options.getValueOfBool "nolabelsafe" then begin
            final_string := "\n\nNo error found.  The system is safe :-)\n";
            Message.msg_string Message.Major !final_string;
            exit 0
          end else begin
            Message.msg_string Message.Error ("Error: label '" ^ reachLabel ^
                                              "' was not found in the source file(s)");
            exit 3
          end
        | _   ->
          begin
            Message.msg_string Message.Error ("Error: label '" ^ reachLabel ^
                                              "' appears multiple times in the source file(s)");
            exit 3
          end
      in
      if (Options.getValueOfBool "fmc") then begin
        try
          failwith "foci model check is no longer supported"; ()
        with e -> Message.msg_string Message.Error ("Error: "^(Printexc.to_string e));
      end else if (Options.getValueOfBool "fmcc") then begin
        try
          failwith "foci model check is no longer supported"; ()
        with e -> Message.msg_string Message.Error ("Error: "^(Printexc.to_string e));
      end else if (Options.getValueOfBool "ti") then begin
        let _ =
          try Stats.time "ti_modelcheck" ti_modelcheck seedPreds
          with _ -> ()
        in
        exit 1
      end else
        Stats.time "modelCheck"
                   (modelCheck (Misc.option_to_list error_location) None)
                   seedPreds
                   cil_files;
    end; (* ends if queryfile <> "" *)

    let memstats = Gc.stat () in
    Message.msg_string Message.Normal ("Minor Words : " ^ (string_of_float memstats.Gc.minor_words)) ;
    Message.msg_string Message.Normal ("Major Words : " ^ (string_of_float memstats.Gc.major_words)) ;
    Message.msg_string Message.Normal ("Total size of heap in words : " ^ (string_of_int memstats.Gc.heap_words));
    if not (Options.getValueOfBool "quiet") then
      Stats.print stdout "Timings:\n";

  with a ->
    flush stdout;
    flush stderr;
    raise a
;;


(* [Printexc.catch main ()] is deprecated as of OCaml 3.01: run the bytecode
   with OCAMLRUNPARAM=b to get a diagnostic with backtrace *)
let () =
  try
    ocaml_main ();
    Message.msg_string Message.Normal !final_string;
    Message.msg_string Message.Error !final_string;

    begin try
      CsisatInterface.stop_csisat ()
    with _ ->
      Message.msg_string Message.Error "Error when stopping CSIsat"
    end;
    begin try
      SMTLIBInterface.stop_smt_solver ()
    with _ ->
      Message.msg_string Message.Error "Error when stopping SMT sovler"
    end
  with e -> begin
      begin try
        CsisatInterface.stop_csisat ()
      with _ ->
        Message.msg_string Message.Error "Error when stopping CSIsat"
      end;
      begin try
        SMTLIBInterface.stop_smt_solver ()
      with _ ->
        Message.msg_string Message.Error "Error when stopping SMT sovler"
      end;
      let print_exception_maybe_exit e =
        (* Print nicer exception descriptions *)
        let module TCA = Abstraction.Make_C_Abstraction(C_System_Descr) in
        (* For some reason, NoNewPredicatesException is not detected by
         * matching.  Try substring as well!*)
        let nonew_str = "\n\
          NoNewPredicatesException is raised when BLAST enters an infinite loop.  This should happen sometimes when what\n\
          you solve is an undecidable problem.  This loop is usually related to a certain potential error trace that is\n\
          proven infeasible during its examination, but then re-entered again because BLAST fails to track the status of\n\
          the proving predicates along the trace.  Scroll up until you enconter a program trace; this is the problematic\n\
          one.  The main causes for this error are:\n\
          1) If array operations or 'complex' C operators, such as right shifts, modular arithmetics, etc, significantly\n\
          \tcontribute to the infeasibility of the trace;\n\
          2) This is a bug in BLAST.  Please, report it.\n"
        in
        match e with
          (* return error if main's not found.  WARINING!  Do not change its text,
           * it's read in LDV! *)
        | C_System_Descr.Not_found_main ->
          begin
            Message.msg_string Message.Error "Entry point not found.  Exiting.";
            exit 3
          end
        | FociInterface.SAT _ ->
          Message.msg_string Message.Error "\n\
          FociInterface.SAT exception means that an interpolating prover (which uses FOCI format) found that a formula\n\
          is satisfiable when it's assumed otherwise.  The problematic formula should be printed above, before the\n\
          statistics.  This error usually occurs when: \n\
          1) A solver finds that a formula is unsat in Integer arithmetics, but the interpolating prover works with \n\
          \treal arithmetics, and thinks that the formula is sat (BLAST bug);\n\
          2) Interpolating prover can not process a formula that uses multiplication or division of variables,\n\
          \tand yields inconsistent results.  If reasoning about nonlinear formulas is crucial for your verification\n\
          \ttask, then BLAST may fail to verify it.\n\n"
        | TCA.NoNewPredicatesException -> Message.msg_string Message.Error nonew_str
        | _ when Misc.is_substring (Printexc.to_string e) "NoNewPredicatesException" ->
          Message.msg_string Message.Error nonew_str
        | _ -> ()
      in
      let flag =
        match e with
        | Failure s ->
              (not (Misc.is_substring s "o new pre"))
        | _ -> true
      in
      if true then begin
        let s = Printexc.to_string e in
        Stats.print stdout "Timings:\n";
        flush stdout;
        print_exception_maybe_exit e;
        Message.msg_string Message.Error ("Ack! The gremlins again!: " ^ s);
        Message.msg_string Message.Normal ("Ack! The gremlins again!: " ^ s);
      end;
      raise e;
  end


