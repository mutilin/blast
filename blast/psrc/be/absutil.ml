
(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


open BlastArch

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module E = Ast.Expression
module P = Ast.Predicate
module M = Message
module Stats = Bstats

module Make_Absutil = 
  functor(PredTable : PREDTABLE with module C_SD.Command = BlastCSystemDescr.C_Command) ->
    struct
		 
module C_SD = PredTable.C_SD 
(* BlastCSystemDescr.C_System_Descr *)
(***********************************************************************************)
  (** Code to generate new temporary variable names **)
  let newTmpVarIndex = ref (-1)
  let getNextTmpVarIndex () =
    newTmpVarIndex := !newTmpVarIndex +1; !newTmpVarIndex
	
  (* Check if a symbol is a temporary *)
  (* a string is temporary if it starts with a number (obtained from getNewTmpVar)
     or contains the character '%' (obtained from brand_new_suffix)
     *)
  let isTemporary sym =
    if (Misc.is_digit (String.get sym 0) || String.contains sym '%') then
      true
    else
      false

    
  (* This is used by the strongest postcondition to give names
     to the existentiallly quantified variables.
     This is also used by the wp when substitutions are explicit.
  *)
	 (* jhala -- making this go inside recursively -- ask rupak if this is ok *)
 let getNewTmpVar (t : E.lval) : E.expression =
    let _makeStr s = (string_of_int (getNextTmpVarIndex ()))^"_"^s 
    in
      E.Chlval (t, _makeStr "")

 let clock = ref 0

 let clock_tick () = clock := !clock + 1 ; !clock

 let getNewTmpVar_clock (t : E.lval) : E.expression =
   E.Chlval (t, string_of_int !clock)


  let get_new_unknown fname = 
    getNewTmpVar (E.Symbol ( "BLAST_UNKNOWN@" ^ fname ) )



  (****************************************************************************************)
  (* Code to handle the aliasing between variables in the wp and sp computations. *)

  (* Code to manage hashtable mapping each predicate to its list of variables of the form
     x, x.f, *x, &x, and ( *x ).f *)
  let predTbl = Hashtbl.create 11
  let predTbl_deep = Hashtbl.create 11



  (* get the set of variable expressions in a predicate.  we ask this both for a predicate and its
     negation, so we have to deal with both cases 
     The returned list contains either lvalues or Unary(AddressOf, lvalue).
     *)
  let getVarExps pred  = 
    try Hashtbl.find predTbl pred
    with Not_found -> 
      (try Hashtbl.find predTbl (P.negate pred)
       with Not_found -> 
         let f = P.allVarExps pred
         in
           Hashtbl.add predTbl pred f;
           f)

(* RJ: must generalize this for pointers *)
  let getVarExps_deep pred  = 
      try Hashtbl.find predTbl_deep pred
      with Not_found -> 
	(try Hashtbl.find predTbl_deep (P.negate pred)
	with Not_found -> 
	  let f = P.allVarExps_deep pred in
	  let f' = List.filter 
		     (fun x -> 
			match x with
			    (E.Symbol _) -> true 
			  | _ -> false
		     ) f 
	  in
	  let f''= List.map (fun x ->
			       match x with
				  (E.Symbol y) -> y 
				 | _ -> failwith "should only receive symbols here !"
			    ) f'
	  in
	  Hashtbl.add predTbl_deep pred f'';
	  f'')	   


 (* like getVarExps, but it filters out the expressions of the form &x.  Such expressions are
     unnecessary for alias queries, since they are never aliased to anything.
    This may return structure accesses as well.
  *)
  let getVarsAndDerefs (exp : P.predicate) : E.expression list =
    let filter_addrofs = 
      List.filter 
	(function e -> 
          match e with
            E.Unary(E.Address, _) -> false
          |        _ -> true)
	(getVarExps exp)
    in
    List.map 
      (fun e -> 
	match e with
	  E.Lval _ -> e
	| E.Chlval (_,_) -> e
	| _ -> failwith ("getVarsAndDerefs: Non lval expression! "^(E.toString e)))
      filter_addrofs
      
(* a cheap hack: I want the constituent varsandderefs of an expression -- but dont want to rewrite the function above so... *)
  let getVarsAndDerefs_exp (e : E.expression) : E.expression list =
    let _pred = P.Atom (E.Binary (E.Eq, e, E.Constant (Constant.Int 1))) 
    in
      getVarsAndDerefs _pred 

  (* this function returns a list of expressions (of the form x, *x, x.f, or ( *x ).f) in 
     pred that may be aliased to target. 
     we assume that the given C program 
     is in a normal form such that we only need to do deal with one level of 
     pointers.  for example, a statement of the form **p = 3 will be 
     represented as (tmp = *p; *tmp = 3). *)

	(* this table caches results to getPossibleAliases *)
  let queryAlias_table = Hashtbl.create 1009
  
  let stats_nb_queryAlias_cache_hits = ref 0	
  let stats_nb_queryAlias_cache_called = ref 0
  let queryAlias_cache x y =
    stats_nb_queryAlias_cache_called := !stats_nb_queryAlias_cache_called + 1;
    M.msg_string M.Debug
      (Printf.sprintf "queryAlias_cache %s %s" (E.toString x) (E.toString y));
    let returnval = 
      (* if (Options.getValueOfBool "nocache") then
	AliasAnalyzer.queryAlias x y
      else *)
	try 
          let rv = Hashtbl.find queryAlias_table (x,y) in
          stats_nb_queryAlias_cache_hits := 1 + !stats_nb_queryAlias_cache_hits;
          rv
	with Not_found ->
	  begin
            let get_query_result () = AliasAnalyzer.queryAlias x y in
	    let rv = Stats.time "queryAlias" get_query_result () in
                
	      Hashtbl.add queryAlias_table (x,y) rv;
	      rv
	  end
    in
    M.msg_string M.Debug (Printf.sprintf "alias result: %b" returnval);
    returnval

  module LvalueSet =
      Set.Make (struct
                  type t = E.lval
                  let compare x y =  compare x  y
                end) 
         
  let lv_rd_aliases_table = Hashtbl.create 1001  
  let stats_nb_get_lvrd_aliases_compute = ref 0
  let stats_nb_get_lvrd_aliases_cached = ref 0
  let stats_total_alias_set_size = ref 0 
  let stats_max_alias_set_size = ref 0
  
  let get_scoped_aliases lv =
      let aliasfn = 
        if Options.getValueOfBool "cf" then AliasAnalyzer.get_lval_aliases_scope
        else (fun fn -> AliasAnalyzer.get_lval_aliases)
      in
      let scopes = Misc.sort_and_compact (C_SD.scope_of_lval lv) in
      let raw () = 
              (List.map (fun fn -> aliasfn fn lv)
              scopes)
      in       
      let ac_aliases = Stats.time "inside AliasIter_scope" raw () in
      let rv = Misc.sort_and_compact (List.flatten ac_aliases) in
      let size = List.length rv in
        stats_total_alias_set_size := !stats_total_alias_set_size + size;
        if (!stats_max_alias_set_size < size) then
                stats_max_alias_set_size := size;
        rv
        
   (* a version of the above that uses "alias_iter" instead. *)
  let queryAlias_iter_cache e_wr e_rd =
    match (e_wr,e_rd) with
          (E.Lval lv_wr,E.Lval lv_rd) ->
        (* 1. get the set of lv_rd's aliases *)
          let lv_rd_alias_set () = 
                try 
                        let rv = Hashtbl.find lv_rd_aliases_table lv_rd in
                        stats_nb_get_lvrd_aliases_cached := 1 +
                        !stats_nb_get_lvrd_aliases_cached;
                        rv
                with Not_found -> 
                   begin
                        let _ = stats_nb_get_lvrd_aliases_compute := 1 +
                           !stats_nb_get_lvrd_aliases_compute 
                        in 
                        let lv_rd_aliases : E.lval list = 
                             Stats.time "compute lv_rd_aliases"
                                get_scoped_aliases lv_rd 
                        in
                        let lv_aset : LvalueSet.t = 
                            List.fold_left 
                              (fun _set _elt -> LvalueSet.add _elt _set)
                                 LvalueSet.empty lv_rd_aliases
                        in
                            Hashtbl.replace lv_rd_aliases_table lv_rd lv_aset;
                            lv_aset
                   end
          in    
          let lvaset = Stats.time "get lv_rd_alias_set" lv_rd_alias_set () in
          LvalueSet.mem lv_wr lvaset
          | _ -> false
          

  let _getPredAliases alias_query_fn target pred =
    if (Options.getValueOfString "alias" = "") then []
    else 
      begin
	let predVars = Stats.time "getVarsAndDerefs" getVarsAndDerefs pred in
	let candidates : E.expression list =
	  if E.isDeref (E.Lval target) then predVars
	  else List.filter E.isDeref predVars in
	let _ = M.msg_string M.Debug "Alias candidates" in
        Stats.time "filter alias candidates" (List.filter (alias_query_fn (E.Lval target))) candidates
      end 
      
  let getPossibleAliases target pred =
    let alias_query_fn = 
      if (Options.getValueOfBool "aiter") then queryAlias_iter_cache
      else queryAlias_cache in
    _getPredAliases alias_query_fn target pred
  
  let getMustAliases target pred = _getPredAliases (AliasAnalyzer.is_must_alias) target pred
    
                       
      
(***************************************************************************)
(* Jhala: Misc functions required for the pre of function calls *)
  (* retTarget always points to the last target -- it is set when doing a 
     post from the stack info of the region 
     *)

  let retTarget = ref C_SD.junkRet
  let retCallSubs = ref [] (* the asgns done to pass actuals into formals -- a sin that follows from retTarget *)
  let retCallerName = ref C_SD.__NotImplementedFunctionName
			
  let wpCapRegion = ref P.True

  (* this is the only way to write to retTarget -- it should not be used in any other way! *)		      
  let set_ret_pointers targ subs caller_name =
    retTarget := targ;
    retCallSubs := subs;
    retCallerName := caller_name
  
  let set_ret_pointers_ce ce = 
    let retloc = C_SD.get_target ce in
    let (_,subs,rt) = 
       match C_SD.call_on_edge ce with Some (_,fce) -> C_SD.deconstructFunCall fce 
          | None -> failwith "block_pred_upd_cons : error" in
    set_ret_pointers rt subs (C_SD.get_location_fname retloc)
    
    
  let get_ret_pointers () = (!retTarget,!retCallSubs,retCallerName)
  let reset_ret_pointers () = set_ret_pointers C_SD.junkRet [] C_SD.__NotImplementedFunctionName
			      
  let sat_filter cubes_disjl cap_reg =
     let print_info_flag = (cap_reg <> P.True) in
     let _ = if print_info_flag then M.msg_string M.Debug ("Cap Region: "^(P.toString cap_reg)) in
     let _candidate_terms = P.to_dnf (cubes_disjl) in
     let is_term_satisfiable term = 
       if (print_info_flag) 
       then
	 begin
	   Printf.printf (".");
	   not ((Options.getValueOfBool "wpsat") && (PredTable.askTheoremProver (P.implies (P.conjoinL [term;!wpCapRegion]) P.False)))
	 end
       else
	 true
     in
     let _surviving_terms = List.filter is_term_satisfiable _candidate_terms in
     if (true) then
       begin
	 M.msg_string M.Debug ("Original cubes: "^(string_of_int (List.length _candidate_terms)));
	 M.msg_string M.Debug ("Remaining cubes: "^(string_of_int (List.length _surviving_terms)))
       end;
     P.disjoinL _surviving_terms



(* Misc functions to play with parameters etc. *)

let _get_param exp = match exp with
  E.Lval (E.Symbol(x)) -> x
| _ -> failwith ("Invalid function Parameter " ^ (E.toString exp))
(* All function call params should be variables ... or you need
   unification ... ! *)

let listToMap l = function x -> (try List.assoc x l with _ -> x)

let real_fun_name fcexp = 
   match fcexp with 
     E.FunctionCall(E.Lval (E.Symbol(fname)), elist) -> fname
   | E.Assignment (E.Assign,_ , 
			   E.FunctionCall(E.Lval (E.Symbol(fname)), _)) ->
			     fname
   | _ -> failwith "bad call to real_fun_name"


  (**
   * This module defines the operations of this abstraction.
   *)


      
	
module Make_Operation =
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
struct
  type t = C_SD.edge
  let print = C_SD.print_edge
  let to_string = Misc.to_string_from_printer print

    (* presumably, these are written here solely for brevity in the remainder of the module ? *)
type exp = E.expression
type lval = E.lval
	      
type info =
  | Normal
  | Call of P.predicate (* the guard, for guarded calls *) * lval option * string * exp list
  | Ret of exp
	      
  open BlastCSystemDescr.C_Command

  let get_info edge =
      let _get_info_fce (p,fce) =
        if not (C_SD.enter_call edge) then Normal 
        else
        match fce with
	  E.FunctionCall (E.Lval (E.Symbol n), el) -> Call (p, None, n, el) 
        | E.Assignment (E.Assign, targ, E.FunctionCall (E.Lval (E.Symbol n), el)) -> Call (P.True, Some targ, n, el) 
        | _ -> assert false in 
      match (C_SD.edge_to_command edge).code with
                Block [Return e] -> Ret e
        |	FunctionCall e -> _get_info_fce (P.True,e) 
        |       GuardedFunctionCall (p,e) -> _get_info_fce (p,e) 
        |	_ -> Normal
	  
   let paren_fun op = 
      match get_info op with
	  Call _ -> "("
	| Ret _  -> ")"
	| _ -> ""
 end

    
      

let funCallSubstitute l = P.substitute 
      (List.map (function (x,y) -> (E.Lval (E.Symbol(x)), E.Lval (E.Symbol(y)))) l)

(*  removeFunPreds fname predList = (preds that remain ,delPreds) 
    the ones that remain are the ones that do not contain any
    variables  local to that function *)

  let removeFunPreds fname predList = 
    let formalList = C_SD.lookup_formals fname in
    let formals = 
      match formalList with
	C_SD.Variable l -> l
      |	C_SD.Fixed l -> l
    in
    let _keepPred i = 
      let pred_i = PredTable.getPred i in
      let vars_pred_i = getVarExps_deep pred_i in
      if ( List.exists (function elt -> List.mem elt formals) vars_pred_i) then false else true 
    in
    let remain_preds = List.filter _keepPred predList in
    let delPreds = List.filter (function x -> (not (_keepPred x))) predList in
    (remain_preds,delPreds) 

				
let asgn_of_funcall (targ_sym_flag,exp_sym_flag) glvm_filter_o fcallExp = 
  let glv_filter = match glvm_filter_o with Some f -> f | None -> (fun lv -> true) in 
  let (fname,_subs,target) = (C_SD.deconstructFunCall fcallExp) in
  let exists_fun = C_SD.is_defined fname in 
    (* try 
      let _ = C_SD.lookup_entry_location fname 
      in true 
    with C_SD.NoSuchFunctionException _ -> false *)
    M.msg_string M.Debug "Funcall assignment";
    List.iter (fun (x,e) -> M.msg_string M.Debug (Printf.sprintf "%s := %s" x (E.toString e))) _subs;
    let asgn_list = 
      if (exists_fun || (target = C_SD.junkRet)) then
	(List.map (fun (x,e) -> C_SD.Command.Expr( E.Assignment (E.Assign,E.Symbol(x),e))) _subs ) @
        (if C_SD.is_noexpand fname then [C_SD.Command.Expr( fcallExp )] else [])
      else
	[C_SD.Command.Expr( E.Assignment (E.Assign, target, get_new_unknown fname ))] 
    (*TBD:SECURITY --  should also smash all the pointers passed into function *)
    in
    let mk_sym flag symv = if flag then symv else (E.peel_symbolic symv) 
    in
    let lhs symv = mk_sym (true) (*targ_sym_flag*) symv in
    let rhs symv = E.Lval (mk_sym false (* exp_sym_flag *) symv) in
    let sym_const_hooks =
      if (Options.getValueOfBool "cf" && Options.getValueOfBool "incmod") then
	begin
	  (* Get the relevant symvars *)
	  let rel_sym_vars = 
            if targ_sym_flag then (PredTable.get_relevant_symvar fname)
            else 
              let glv_mod = List.filter glv_filter (C_SD.global_lvals_modified fname) in
              List.map (E.make_symvar fname) glv_mod        
          in
          let _ = M.msg_string M.Debug 
          ("Symvars: "^(Misc.strList (List.map E.lvalToString rel_sym_vars)))
          in
          let make_asgn symv =
	    C_SD.Command.Expr ( E.Assignment
				  (E.Assign,(lhs symv),(rhs symv)))
	  in
	    List.map make_asgn rel_sym_vars
	end
      else []
    in
      (* PREV: List.fold_right (wp explicit_flag) asgn_list pred *)
      (* WITH ANNOT: NOT SURE THIS IS CORRECT: check with Jhala *)
      asgn_list @ sym_const_hooks (* NOTE: the sym_const_hooks come AFTER the asgn_list *)
  
  
(* should only be called with x_lv which can possible be modified by the call,
    i.e. x_lv is either a global or a formal lvalue *)
	(* says if a DEREF of x_lv is modified -- not if x_lv itself is!
	   -- useful only if a local parameter has been passed to it *)
(* only used below *)
	
let is_smashed_fname callee_name =
  let smashed =
    let formals_smashed = (C_SD.formal_lvals_modified callee_name) in
    let globals_smashed = (C_SD.global_lvals_modified callee_name) in
      formals_smashed @ globals_smashed
  in
    fun lv_l -> Misc.nonempty_intersect compare lv_l (smashed)
            
let return_tweak callee_name is_smashed sym_flag x_lv =
  let modopt = (Options.getValueOfBool "modopt" ) in
    if (not modopt) || (is_smashed [x_lv] && sym_flag) then
      E.make_symvar callee_name x_lv
    else x_lv
      

let modified_by_call_table = Hashtbl.create 101

let modified_by_call callee_name sym_flag x_lv =
  M.msg_string M.Debug ("In modified_by_call: "^(E.lvalToString x_lv));
  try
    Hashtbl.find modified_by_call_table (sym_flag,x_lv)
  with Not_found ->
    let is_smashed = is_smashed_fname callee_name in
    let modopt = (Options.getValueOfBool "modopt" ) in
    let is_modified =
      if not (C_SD.is_ptr x_lv) then false
      else
	if modopt then
	  let deref_x_lv = (E.Dereference (E.Lval x_lv)) in
            is_smashed (C_SD.lvalue_closure deref_x_lv) 
	else true
    in
    let tweaked = return_tweak callee_name is_smashed sym_flag x_lv in
      Hashtbl.replace modified_by_call_table (sym_flag,x_lv) (is_modified,tweaked);
      (is_modified,tweaked)
    

(* the above functions are atrociously slow! we need a new modified_by_call -- or judicious use of caching -- as done above.*)
(* x_lv is unique to each callee_name, so we can hash on it *)
let modified_by_call2 callee_name sym_flag x_lv = 
  (* 1. do we need to copy back.
     2. if so, is x_lv itself modified ? -- *)
  try 
    Hashtbl.find modified_by_call_table (sym_flag,x_lv)
  with Not_found ->
    begin
      let all_smashed = (C_SD.formal_lvals_modified callee_name) in
      let x_smashed = List.mem x_lv all_smashed in
      let x_cl_smashed = 
	let x_deref = (E.Dereference (E.Lval x_lv)) in
	  List.exists (fun lv' -> 
			 E.occurs_check 
			 (E.Lval x_deref) (E.Lval lv')) all_smashed
      in
      let tweaked = 
	if x_smashed && sym_flag then E.make_symvar callee_name x_lv else x_lv
      in
      	Hashtbl.replace modified_by_call_table (sym_flag,x_lv) (x_cl_smashed,tweaked);
	(x_cl_smashed,tweaked)
    end 

(* only when sym_flag is on do we bother to plug in the symbolic names *)
let copy_back_asgns sym_flag param_subs callee_name  =
  let _ = M.msg_string M.Debug "In copy_back_asgns" in
  let tweaker = modified_by_call2  callee_name sym_flag in
  let copy_back_asgn (x,e) =
    let x_lv = (E.Symbol x)  in
    let (is_modified,tweaked_x_lv) = Stats.time "tweaker" tweaker x_lv in
      if is_modified
      then 
        Some 
	  (E.push_deref(E.Dereference e), 
	   E.Lval
	     (E.Dereference (E.Lval (tweaked_x_lv))))
      else None
  in
   if Options.getValueOfBool "cf" then Misc.map_partial copy_back_asgn param_subs else []
  
    
let asgn_of_return callee_name  =
  M.msg_string M.Debug "In asgn_of_return";
  let (ret_target,param_subs,_) = get_ret_pointers () in
  let cb_asgns = copy_back_asgns true (* MAKE FLEXIBLE ? *) param_subs callee_name in
  let ret_pair_option = if ret_target = C_SD.junkRet then None else Some (ret_target) in
    (ret_pair_option,cb_asgns)

(******************************************************************************************************)
(******************************************************************************************************)
(* Useful utilities for concurrent Blast *) 
(*****************************************************************************************************)
(*****************************************************************************************************)
  let is_local v =
    M.msg_string M.Debug ("[Abstraction.is_local " ^(E.lvalToString v) ^ "]");
    let rec _is_local lv =
      match lv with
	E.Symbol s ->
	  String.contains s '@'
      | E.This -> failwith "is this local?"
      | E.Access (_, e, _) -> _is_local_e e
      | E.Dereference e -> _is_local_e e
      | E.Indexed (e1, e2) -> _is_local_e e1 || _is_local_e e2
    and
	_is_local_e e =
      match e with
	E.Lval l -> _is_local l
      |	E.Chlval (l, _) -> _is_local l
      |	E.Binary (_, e1, e2) -> _is_local_e e1 || _is_local_e e2
      |	E.Unary (_, e) -> _is_local_e e
      |	E.Constant _ -> false
      |	_ -> failwith ("is_local : Strange expression "^(E.toString e))
    in
    _is_local v
      
  (* function to substitute tid -> notTid and locals with fresh copies.
     Uses the global phi_com_counter to get new names for locals 
     this is used to give new names to all the local variables inside the phi *)
 
  let new_name suffix (lv : E.lval) = 
    let rec _new_name lv =
      match lv with
	E.Symbol v ->
	  E.Symbol 
	    (if (Symbol.compare v "tid") = 0 
	    then "notTid"
	    else
	      if (Symbol.compare v "notTid") = 0 then "tid" else
	      if (Symbol.compare v "self") = 0 then "self" else
	      if String.contains v '@' 
	      then (v^suffix)
	      else v)
      | E.This -> failwith "this : in new_name" 
      | E.Access (aop, e, fld) -> 
	  E.Access(aop, _new_name_e e, fld)
      |	E.Dereference e -> E.Dereference (_new_name_e e)
      |	E.Indexed (e1, e2) -> E.Indexed (_new_name_e e1, _new_name_e e2)
    and
	_new_name_e e =
      match e with
	E.Lval l -> E.Lval (_new_name l)
      |	E.Chlval (l, suff) -> 
	  begin
	    let l' = 
	      match l with
		E.Symbol _ -> l
              | E.This -> l
	      | E.Dereference e -> E.Dereference (_new_name_e e)
	      | E.Access (op, e1, fld) -> E.Access (op, _new_name_e e1, fld)
	      | E.Indexed (e1, e2) -> failwith "_new_name : array not handled"
	    in
	    E.Chlval (l', suff)
	  end
      |	E.Binary (op, e1, e2) -> E.Binary (op, _new_name_e e1, _new_name_e e2)
      |	E.Unary (op, e) -> E.Unary (op, _new_name_e e)
      |	_ -> e
    in
    _new_name lv
      


  (******************************************************************************)
	    
      (* every so often we need fresh names for variables. The
	 suffix_counter variable, and the brand_new_suffix
	 function provides the utility: essentially these
	 provide a "brand new suffix" that is tagged on to the current
	 id / lvalue.

	 Sometimes, we need to know the current suffix. get_current_suffix returns
	 the current suffix.
	 *)

  let suffix_counter = ref 0 
  let brand_new_suffix () = 
    suffix_counter := !suffix_counter + 1;
    "%_bsuf_"^(string_of_int !suffix_counter)
		 
  let get_current_suffix () =
    "%_bsuf_"^(string_of_int !suffix_counter)

  let expression_mentions_locals e =
    try
      let reject_local (name : E.lval) = if is_local name then raise Exit; name in
      let _ = E.alpha_convert reject_local e in false
    with Exit -> true

  let predicate_mentions_locals p = 
    let atoms = P.getAtoms p in
    let xtract_xps p' = 
      match p' with 
	  P.Atom x -> x
	| _ -> failwith "bad call to xtract_xps"
    in
    let exps = List.map xtract_xps atoms in (* this should not fail due to the above ! *)
      List.exists expression_mentions_locals exps

(******************************************************************************************)
(******************************************************************************************)
(******************************************************************************************)
(* Utilities related to focus *)
(******************************************************************************************)
(******************************************************************************************)

 (******** extracting new predicates from **********)
(* from a cube *)

let getNewPreds orlist =
    let newp = 
      try 
	List.fold_left 
	  (function prlst -> function p -> prlst @ (Stats.time "getUsefulPreds" TheoremProver.getUsefulPredicates p)) [] orlist 
      with ((Failure "simplify") as e) -> (M.msg_string M.Normal "Simplify exception!"; raise e)
    in
      match newp with 
	  [] ->
            begin
              M.msg_string M.Error  "\n do_focus: Vampyre bug: cannot prove region to be empty!\n" ;
              M.msg_string M.Error "Taking all predicates!\n" ;
              M.msg_string M.Error ("Vampyre failed on query:\n "^(P.toString (P.Or (orlist))));
	      List.fold_left (function plist -> function p -> plist @ (P.getAtoms p)) [] orlist
            end
	| _ -> newp             


(* tweaks a given pred to drop primes and so on ... *)

let _newTemporariesAdded = ref []

let _massage symbol = (* get back original varname from temporary name *)
   let drop_initial_number =
      if (Misc.is_digit (String.get symbol 0)) then
        let _ind = String.index symbol '_' in
        let len = String.length symbol in
          (String.sub symbol (_ind+1) (len - _ind - 1))
      else symbol in
   if (String.contains drop_initial_number '%') then
     let _ind = String.index symbol '%' in
	      String.sub drop_initial_number 0 _ind
   else drop_initial_number

(* TBD: REFACTOR -- can't this be done with E.deep_transform ? *)   
let rec massage_lval fs l =
  let me = massage fs in
  match l with
      | E.Symbol s -> E.Symbol (fs s)
      | E.This -> l
      | E.Access(op, e1, s) -> E.Access(op, me e1, s)
      |	E.Dereference e -> E.push_deref (E.Dereference (me e))
      |	E.Indexed (e1, e2) -> E.Indexed (me e1, me e2)

and massage fs e =
    let me = massage fs in
    match e with
	E.Lval l -> E.Lval (massage_lval fs l)
      |	E.Chlval (l,_) -> E.Lval (massage_lval fs l)
      |	E.Binary(op, e1, e2) -> E.Binary(op, me e1, me e2)
      | E.Unary(E.Address, e) -> E.addressOf (me e)
      | E.Unary(op, e) -> E.Unary(op, me e)
      | E.Constant _   -> e
      | E.Select(e1,e2) -> E.Select(me e1,me e2)
      | E.Store(e1,e2,e3) -> E.Store(me e1,me e2, me e3)
      | E.FunctionCall (E.Lval (E.Symbol _fnNm), args) -> 
          begin
            match _fnNm with
              "BitAnd" -> E.Binary(E.BitAnd, me (List.hd args), me (List.nth args 1))
            | "BitOr" -> E.Binary(E.BitOr, me (List.hd args),me (List.nth args 1))
            | "Xor" -> E.Binary(E.Xor, me (List.hd args), me (List.nth args 1))
            | "Div" -> E.Binary(E.Div, me (List.hd args), me (List.nth args 1))
            | "LShift" -> E.Binary(E.LShift, me (List.hd args), me (List.nth args 1))
            | "RShift" -> E.Binary(E.RShift, me (List.hd args), me (List.nth args 1))
            | "Rem" -> E.Binary(E.Rem, me (List.hd args), me (List.nth args 1))
            | "BitNot" -> E.Unary(E.BitNot, me (List.hd args))
            | "chlval" -> me (List.hd args)
            | "foffset" -> E.Binary(E.FieldOffset, me (List.hd args),me (List.nth args 1))
            | "sel4" ->
                 (* Rename sel4 to select, no? sel4 is a remnant of Necula's sel4 -- 
                  * but there we were keeping track of bytes for memory safety *)
                begin
                  if (List.length args <> 2) then 
                    failwith ("focus: sel4 with more than two args."^(E.toString e))
                  else
                    let _var = me (List.hd args) and _deref = List.nth args 1 in
                      match _deref with
                            E.Constant i -> E.Lval (E.Dereference _var)
                          | E.Lval (E.Symbol s) -> E.Lval (E.Access (E.Arrow, _var, _massage s))
                          | _ -> failwith "focus: unexpected use of sel4! Strange arguments."
                end
            
            | "select" -> (* case Dot *)
                begin
                  if (List.length args <> 2) then 
                    failwith ("focus: select with more than two args. "^(E.toString e))
                  else
                    let _var = (me (List.hd args)) and _deref = List.nth args 1 in
                          match _deref with
                            | E.Lval (E.Symbol s) -> 
				E.Lval (E.Access (E.Dot, _var, _massage s))
			    | E.Constant (Constant.Int _) ->
				E.Lval (E.Dereference _var)
                            | _ -> failwith ("focus: select used with strange args. (deref)"^(E.toString e))
                end
            
            | "addrOf" ->
                  if (List.length args <> 1) then
                    failwith ("focus: addrOf with more than one argument."^(E.toString e))
                  else
                    let _var = me (List.hd args) in 
                    E.Unary(E.Address, _var)
            | _ -> 
                if E.is_interpreted _fnNm then 
                  E.FunctionCall (E.Lval (E.Symbol _fnNm), List.map me args)
                else failwith ("focus: unexpected function name "^_fnNm)
          end
      | _ -> failwith "focus: Expression type not handled"
   
let check_fs orig_s str = 
  let ss = (_massage str) in
  if (ss = orig_s) 
  then (* need annotations *)
    (let tempVarName = "$tmp_" in
     if not (List.mem (tempVarName^ss, ss) (!_newTemporariesAdded))
     then _newTemporariesAdded := (("$tmp_"^ss, ss) :: !_newTemporariesAdded));
  ss

let _substOriginalVars pred =
  (* substitute original variables for temporaries *)
      match pred with
        P.Atom (E.Binary(E.Eq, E.Lval (E.Symbol s), e)) ->
           let (s,m) = if isTemporary s then let orig_s = _massage s in (orig_s, massage (check_fs orig_s))
                                         else (s, massage _massage) in
           P.Atom (E.Binary(E.Eq, E.Lval (E.Symbol s), m e))
      | P.Atom e -> P.Atom (massage _massage e)
      | _  -> failwith "do_focus: obtained non-atomic predicate: unexpected"
	  
let callsToFocus = ref 0

let clean_new_predicates newpreds = 
  let newpreds = List.map P.normalize_interpreted newpreds in
  let _ = _newTemporariesAdded := [] in
  let _substnewpreds = 
    try List.map _substOriginalVars newpreds 
    with e -> (M.msg_string M.Error ("_substOrigVars :"^(Printexc.to_string e));[]) in
  let rv = 
    List.fold_left
    (fun curlist -> fun p -> 
      match p with
	P.Atom e ->
	  begin
	    match e with
	      E.Binary(E.Eq, lhs, (E.Binary(op, _, _) as rhs)) when E.isRelOp op ->
		(P.Atom (E.Binary(E.Eq, lhs, E.Constant (Constant.Int 0))) ::
		(P.Atom (E.Binary(E.Eq, lhs, E.Constant (Constant.Int 1))) ::
		 (P.Atom rhs :: curlist)
		 ))
	    | _ -> p :: curlist
	  end
      | _ -> failwith "clean_new_predicates: Nonatomic predicate!"
	    )
    []
    _substnewpreds 
  in
    if (Options.getValueOfBool "cf") then
      (* deal with symbolic constants *)
      let get_syms pred = 
	let extract_sym_pred lv =
	  let lv' = E.peel_symbolic lv in
	    if lv' <> lv then (* lv is a symbolic constant *)
	      let p_eq = (P.expr_equate (E.Lval lv) (E.Lval lv')) in
	      let p_sub = P.substitute [(E.Lval lv),(E.Lval lv')] pred in
		Some ([p_eq;p_sub]) (* all this overkill -- ! *)
	    else None
	in
	let sym_preds = List.flatten (Misc.map_partial extract_sym_pred (P.allVarExps_deep pred))
	in
	  (pred::sym_preds)
      in
	List.flatten (List.map get_syms rv)
    else rv
      
    
let extractUsefulPredicates emptyPred = 
  let _ = incr callsToFocus in
    (* let newpreds = Stats.time "getUsefulPreds" TheoremProver.getUsefulPredicates emptyPred *)
    (* while the above should have been suficient, Vampyre seems to have trouble with Or's.
       So we convert to DNF, and check if each conjunct is satisfiable, collecting useful predicates. *)
  let emptyPredDNF_form = P.convertDNF emptyPred in
    (* Printf.printf "Converting to DNF and printing...\n%s\n" (P.toString emptyPredDNF_form);*)
  let orlist = 
     match emptyPredDNF_form with
	 P.Or plist -> plist
       | a -> [a]
  in
  let newpreds = getNewPreds orlist in
  let l = clean_new_predicates newpreds in
  l

    
(* Hacks to identify predicates from for loops.
   No longer used. Remove?
*)
let predsFromForLoops b =
  (* Hack for 'for' loops 
     Here is what we do: for each temporary variable added, we find lower and upper bounds for
               the real variable "b" associated with the temporary. Then we add all predicates real = i
     for i in (lower, upper). This is a hack, so we do not try to be very clever about this.
     The hope is that simple cases will be caught.
  *)
  M.msg_string M.Error "";
  M.msg_string M.Error "";
  M.msg_string M.Error ("Warning: Adding predicates for a possible for loop involving index variable "^b);
  M.msg_string M.Error ("Warning: The current handling of such for loops in BLAST is extremely elementary");
  M.msg_string M.Error ("Warning: and is a major cause for state explosion.");
  M.msg_string M.Error ("Warning: We recommend that you manually remove the for loop from your program");
  M.msg_string M.Error ("Warning: by unrolling, or by replacing the concrete upper bound by an uninitialized variable.");
  M.msg_string M.Error ("Blast shall continue in the meantime.");
  M.msg_string M.Error "The Control key is at the bottom left of your keyboard, and C is in the third line of the alphabet, between X and V.";
  M.msg_string M.Error "";
  M.msg_string M.Error "";
  let find_bounds str = (* lower <= upper *)
    let preds_with_str = PredTable.getPredsContainingVar str in
    let rec upper_lower str predList (u, l) =
      match predList with 
          [] -> (u,l)
        | p :: rest ->
            begin
              match p with
                  P.Atom (E.Binary (E.Eq, E.Lval (E.Symbol s), E.Constant (Constant.Int a))) ->
                    if (s = str) then 
                      let newu = if (a > u) then a else u and newl = if (a < l) then a else l in
			upper_lower str rest (newu, newl)
                    else upper_lower str rest (u,l)
		| P.Atom (E.Binary (E.Lt, E.Lval (E.Symbol s), E.Constant (Constant.Int a)))
		| P.Atom (E.Binary (E.Gt, E.Lval (E.Symbol s), E.Constant (Constant.Int a)))
		| P.Atom (E.Binary (E.Le, E.Lval (E.Symbol s), E.Constant (Constant.Int a)))
		| P.Atom (E.Binary (E.Ge, E.Lval (E.Symbol s), E.Constant (Constant.Int a))) 
		| P.Atom (E.Binary (E.Lt, E.Constant (Constant.Int a), E.Lval (E.Symbol s)))
		| P.Atom (E.Binary (E.Gt, E.Constant (Constant.Int a), E.Lval (E.Symbol s)))
		| P.Atom (E.Binary (E.Le, E.Constant (Constant.Int a), E.Lval (E.Symbol s)))
		| P.Atom (E.Binary (E.Ge, E.Constant (Constant.Int a), E.Lval (E.Symbol s))) ->
                    if (s = str) then 
                      let newu = if (a > u) then a else u and newl = if (a < l) then a else l in
			upper_lower str rest (newu, newl)
                    else upper_lower str rest (u,l)
		| _ -> upper_lower str rest (u,l)
            end
    in
      upper_lower str preds_with_str (min_int, max_int)
  in
  let (u, l) = find_bounds b in
    if (l = min_int || u = max_int) then [] (* Uh oh -- trouble! *)
    else
      (let localPredList = ref [] in
       for i = l to u do
         localPredList := 
         (P.Atom (E.Binary (E.Eq, 
                                             E.Lval (E.Symbol b), 
                                             E.Constant (Constant.Int i)))) :: !localPredList     
       done;
	 !localPredList)
      
      
      
(*************************************************************************************)
(*************************************************************************************)
(*************************************************************************************)
(* Utilities for interpolants and interpolant based predicate discovery *)

(* note that the function below changes the PredTable.loc_pred_table *)
let process_raw_foci_preds post_process raw_preds = 
  begin
  M.msg_string M.Debug "raw foci preds";
  List.iter (fun x -> M.msg_string M.Debug (P.toString x)) raw_preds;
  let less_raw_preds = post_process raw_preds in
  M.msg_string M.Debug "post-proc foci preds";
  List.iter (fun x -> M.msg_string M.Debug (P.toString x)) less_raw_preds;
  let cleaned_preds = (clean_new_predicates less_raw_preds) in
  M.msg_string M.Debug "cleaned foci preds";
  List.iter (fun x -> M.msg_string M.Debug (P.toString x)) cleaned_preds;
  (* let _ = -- this is done in PredTable.addPred --> upd_imp_lv_table -- 
    let globvars = List.filter C_SD.is_global 
		     (List.map (fun s -> E.Symbol s)
			(List.flatten (List.map (fun p -> getVarExps_deep p) cleaned_preds))) in
      List.iter PredTable.update_global_lvals globvars in *)	
  let pred_ids = List.map PredTable.addPred cleaned_preds in
    List.filter (fun x -> x <> -1) pred_ids
  end

let _extract_foci_preds post_process arg = 
  let raw_preds = TheoremProver.extract_foci_preds arg in
  process_raw_foci_preds post_process raw_preds
  
let extract_foci_preds = _extract_foci_preds (fun x -> x)

let extract_foci_preds_cf (callee_name, pre_call_cons) =
  let pre_call_atoms = P.getAtoms pre_call_cons in
  let is_chlv e =
    match e with
	E.Chlval (_) -> true
      | _ -> false in
  let is_symv e =
    (is_chlv e) &&
    List.exists
      (fun x ->
	 match x with
	     P.Atom e_atom -> E.occurs_check e e_atom
	   | _ -> false)
      pre_call_atoms in
  let transformer e =
    if is_symv e then
	let lv = E.lv_of_expr (E.replaceChlvals e) in
	let symlv = (E.make_symvar callee_name lv) in
	  (PredTable.add_relevant_symvar callee_name symlv; E.Lval symlv)
    else e in
  let pproc preds =
    let proc_atom atm =
      match atm with
	  P.Atom e ->
	    P.Atom (E.deep_transform_top transformer e)
	| _ -> atm in
      List.map proc_atom (List.flatten (List.map P.getAtoms preds)) in
    _extract_foci_preds pproc


(* THIS DOESNOT WORK. DELETE       
let extract_foci_preds_incref_cf (callee_name, _ ) =
  let is_chlv e =
    match e with
	E.Chlval (_) -> true
      | _ -> false
  in
  let pproc preds =
    (* TBD:ARRAYS *)
    (* let us first find the symbolic variables:
         An lv is a symbolic variable if in the preds,
         there are two distinctly chlvalled occurrences of it. *)
    let plist_chlv_table = Hashtbl.create 31 in (* map lv -> chlvs found for it *)
    let gather_chlv e =
      if (is_chlv e) then
	begin
	  let lv_e = E.lv_of_expr (E.replaceChlvals e) in
	    Misc.hashtbl_check_update plist_chlv_table lv_e e;
	    ()
	end;
      e
    in
    let all_atoms = (List.flatten (List.map P.getAtoms preds)) in
    let extract_atom_lv atm =
      match atm with
	  P.Atom e -> (E.deep_transform gather_chlv e);()
	| _ -> ()
    in
    let _ = List.iter extract_atom_lv all_atoms in
      (* we now know all the symvars *)
    let is_symv e =
      (is_chlv e) &&
      try
	let lv_e = E.lv_of_expr (E.replaceChlvals e) in
	let chlv = Hashtbl.find plist_chlv_table lv_e in
	  (List.length chlv) > 1
      with _ -> false
    in
    let transformer e =
      if is_symv e then
	let lv = E.lv_of_expr (E.replaceChlvals e) in
	let symlv = (E.make_symvar callee_name lv) in
	  add_relevant_symvar callee_name symlv;
	  E.Lval symlv
      else e
    in
    let proc_atom atm =
      match atm with
	  P.Atom e ->
	    P.Atom (E.deep_transform_top transformer e)
	| _ -> atm
    in
      List.map proc_atom
	(List.flatten (List.map P.getAtoms preds))
  in
    _extract_foci_preds pproc
*)
      
let extract_foci_preds_parity arg = 
  let blist_to_string bl = Misc.strList (List.map string_of_bool bl) in
  let raw_preds = TheoremProver.extract_foci_preds_parity arg in
  M.msg_string M.Debug "raw foci preds (parity)"; 
  List.iter  (fun (x,p) -> M.msg_string M.Debug ((P.toString x)^(string_of_bool p))) raw_preds;
  let (predlist,parity_list) = Misc.unzip raw_preds in
  let  cleaned_preds =   (List.map (fun p -> clean_new_predicates [p]) predlist) in
  let  cleaned_parity_list = List.combine cleaned_preds parity_list in
  M.msg_string M.Debug "cleaned foci preds"; 
  List.iter (fun x -> M.msg_string M.Debug  (Misc.strList (List.map P.toString x))) cleaned_preds; 
   let _ = 
     if (Options.getValueOfInt "craig" >= 2 && Options.getValueOfString "checkRace" <> "") 
     then
       let globvars = List.filter C_SD.is_global 
       (List.map (fun s -> E.Symbol s) 
       (List.flatten (List.map (fun p -> getVarExps_deep p) (List.flatten cleaned_preds)))) in
       List.iter PredTable.update_global_lvals globvars in	
   let ppl = List.combine cleaned_preds parity_list in
   let pred_ids = List.map ( fun (pl,par) -> ((List.map PredTable.addPred pl), par))  cleaned_parity_list in
   let proc (pl,par) = List.iter (fun p -> PredTable.add_pred_parity p par) pl in
   List.iter proc pred_ids; 
   List.filter (fun x -> x <> -1) (List.flatten (List.map fst pred_ids)) 
  
      
(* KILL VARIABLES *)
let killPredTable : (P.predicate, bool) Hashtbl.t = Hashtbl.create 37
let kill_filter_fun x  = Hashtbl.mem killPredTable (PredTable.getPred x) 
			   
let hg_exists kill_ff bdd = 
  let soc_bdd = ref PredTable.bddZero in
  let make_cube_bdd cube = 
    let tmp_cube_bdd = ref PredTable.bddOne in
      for i = 0 to (Array.length cube - 1) do
	if (cube.(i)==2 || (kill_ff i) ) 
        then 
	  begin
	    if (not (cube.(i)==2)) then M.msg_string M.Debug ("killing pred: "^(P.toString (PredTable.getPred i))^"\n")
          end
        else 
	  begin
   	    if (cube.(i)==1) then 
	      tmp_cube_bdd := CaddieBdd.bddAnd (!tmp_cube_bdd) (PredTable.getPredIndexBdd i)
	    else
	      tmp_cube_bdd := CaddieBdd.bddAnd (!tmp_cube_bdd) (CaddieBdd.bddNot (PredTable.getPredIndexBdd i))
	  end
      done;
      soc_bdd := CaddieBdd.bddOr (!soc_bdd) (!tmp_cube_bdd)
  in
    (M.msg_string M.Debug ("Calling hg_exists = "^(P.toString (PredTable.convertBddToPred bdd))));
    CaddieBdd.bddForeachCube bdd make_cube_bdd;
    (M.msg_string M.Debug ("Bye bye hg_exists = "^(P.toString (PredTable.convertBddToPred !soc_bdd))));
    !soc_bdd


(* random utils common to abstraction.ml.m4 and cfAbstraction.ml.m4 *)




    
end






















    
