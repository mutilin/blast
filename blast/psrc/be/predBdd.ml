(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

open BlastArch
module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module Counter = Ast.Counter
module Stats = Bstats




module Make_PredBdd = 
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
    struct
 (** Code relating to initializing the BDD package.
      Defines the manager, true, and false -BDDs.
      *)
  module C_SD = C_SD

  let block_a_t_flag = ref false	
  let bddManager = CaddieBdd.init 0 32 256 512 (* Find out what these numbers mean  *)
  let bddZero = CaddieBdd.bddFalse bddManager
  let bddOne = CaddieBdd.bddTrue bddManager
  let make_varMap x = CaddieBdd.toMap bddManager x
  let kill_predbdd () = () (*CaddieBdd.exit bddManager*)

  let bdd_support bdd = 
    let take a_i = (a_i <> 2) in
    let __l = ref [] in
    let _get_v a = __l := (Misc.array_filter take a) @ !__l in
    let _ = CaddieBdd.bddForeachCube (CaddieBdd.bddSupport bdd) (_get_v) in
    Misc.sort_and_compact (!__l) 

    
  let blast_bddForeachCube bdd f = 
    let cc = ref 0 in
    let f' a = (cc := !cc + 1; f a) in
    CaddieBdd.bddForeachCube bdd f';
    if (!cc > 150) then 
      begin
        Message.msg_string Message.Error ("Warning foreachCube #cubes = "^(string_of_int !cc));
        Message.msg_string Message.Normal ("Warning foreachCube #cubes = "^(string_of_int !cc));
      end;
    ()
    
  let bdd_implies b1 b2 = CaddieBdd.bddEqual (CaddieBdd.bddImp b1 b2) bddOne 
    
  let bdd_cube_cover bdd = 
    let check_upd in_bdd b' = if bdd_implies bdd b' then (CaddieBdd.bddAnd in_bdd b') else in_bdd
    in  
    let grow_cube cube i = (* check if bdd implies bdd_i *)
        let bdd_i_pos = CaddieBdd.bddIthVar bddManager i in
        let bdd_i_neg = CaddieBdd.bddNot bdd_i_pos in
        check_upd (check_upd cube bdd_i_pos) bdd_i_neg 
    in
    let support = bdd_support bdd in
    List.fold_left grow_cube bddOne support
      
  let bdd_cubes bdd = 
    let ctr = ref 0 in
    let cnt _ = ctr := !ctr + 1 in
    if CaddieBdd.bddEqual bdd bddZero || CaddieBdd.bddEqual bdd bddOne then 1
    else (CaddieBdd.bddForeachCube bdd cnt; !ctr)


  let lits_of_cube bdd =
    assert (bdd_cubes bdd = 1);
    let abl = ref [] in
    let d cube = 
      for i = 0 to (Array.length cube -1) do
        match cube.(i) with
          0 ->  abl:= (i,false)::!abl
        | 1 ->  abl:= (i,true)::!abl
        | v ->  assert (v=2)
      done; in
    blast_bddForeachCube bdd d;
    !abl

    (* this really only makes sense if the bdd is a cube *)
  let bdd_cube_literals bdd = 
    let abl = ref [] in
    let de_cube cube = 
        for i = 0 to (Array.length cube -1) do
           match cube.(i) with
               0 ->  abl:= (i,false)::!abl
             | 1 ->  abl:= (i,true)::!abl
             | v ->  assert (v=2)
         done;
    in
    blast_bddForeachCube bdd de_cube;
      List.map 
      (fun (i,b) -> 
        let bdd_i = CaddieBdd.bddIthVar bddManager i in
        if b then bdd_i else CaddieBdd.bddNot bdd_i)
        !abl
        
  (* factor the bdd into cube_cover * rest *)  
  let bdd_cartesian_decomposition bdd = 
    let _ = Message.msg_string Message.Debug "in cart_decomposition" in
    let ccov = bdd_cube_cover bdd in
    let _ = Message.msg_string Message.Debug "Done cube cover" in 
    let qcube = 
      let support = bdd_support ccov in
       Misc.accumulate (CaddieBdd.bddIthVar bddManager) CaddieBdd.bddAnd bddOne
        support
    in
    let rest = CaddieBdd.bddAndAbstract bdd bddOne qcube in
    let _ = Message.msg_string Message.Debug "Done bddaa" in
        rest::(bdd_cube_literals ccov)
       
      (******************************************************************)
  (* Code relating to the management of global list of predicates *)

  (* Global table storing atomic predicate, index, and bdd corresponding
     to this predicate *)
  
  let maxPossiblePreds = 1300 
  let tableOfPreds = ref ([] : (int * Predicate.predicate * CaddieBdd.bdd) list)
  
  let hashTableOfPreds = Hashtbl.create 109

  let arrayOfPreds = Array.create maxPossiblePreds (Predicate.True,bddOne) 
  (* CS[i] = if i is a CS pred, then the next state version of i else -1 *)
  (* NS[i] = if i is a NS pred, then the current state version of i else -1 *)
  let arrayOfCStatePreds = Array.create maxPossiblePreds (-1)
  let arrayOfNStatePreds = Array.create maxPossiblePreds (-1)

  let listOfConstants = ref ( [] : string list) 
  let predIndex = ref (-1)

  let getNextIndex () = 
    predIndex := !predIndex + 1; 
    if !predIndex >= maxPossiblePreds then
      failwith "Bound on the maximum number of predicates exceeded!"
    else !predIndex

  let getCurrentIndex () = !predIndex
 
  let resetIndex () = predIndex := -1

     (** Stats and debug routine *)
  let printTableOfPreds () =
    let rec loop lst i =
      match lst with
        [] -> i
      | (a,b,_) :: rest ->
         begin
           (* Message.msg_string Message.Normal ("//Index = "^(string_of_int a)^" Predicate: \n"^(Predicate.toString b)^";\n"); *)
           loop rest (i+1)
	 end
    in
    let len = loop (!tableOfPreds) 0 in
    Message.msg_string Message.Normal ("\nNumber of predicates ="^(string_of_int len)^" ")


         (* Given an index, find predicate corresponding to that index;
           raises Not_found if no such index 
        *)
  let getPred index = 
    if (index > !predIndex)
      then
	begin
	  Message.msg_string Message.Debug "Looking for unknown predicate!";
	  raise Not_found
	end
      else
	if (index < 0)
	  then
	    begin
	      Message.msg_string Message.Debug (Printf.sprintf "Request for predicate number %d" index);
	      (* This error may happen if interpolation failed at the previous step *)
	      raise (Invalid_argument "Predicate index is less than zero")
	    end
	  else
	    fst (arrayOfPreds.(index))

  (** Given a predicate, find the index in the global table corresponding to that predicate;
     raises Not_found if no such index 
  *)
  let getPredIndex pred = 
    let rec _searchPred lst pr =
      match lst with
          [] -> raise Not_found
        | (j,p,_)::rest -> if (p=pr) then j else _searchPred rest pr
    in
      (Hashtbl.find hashTableOfPreds pred)
      (* _searchPred (!tableOfPreds) pred *)

  (** Given a predicate, find the BDD node corresponding to that predicate *)
  let getPredBdd pred = 
    let rec _searchPred lst pr =
      match lst with
          [] -> raise Not_found
        | (_,p,b)::rest -> if (p=pr) then b else _searchPred rest pr
    in
    let i =  getPredIndex pred in
      snd (arrayOfPreds.(i))
      (*_searchPred (!tableOfPreds) pred*)

 
  (** Given an index, find bdd corresponding to that index;
     raises Not_found if no such index *)
  let getPredIndexBdd index = 
      if (index > !predIndex) then
        raise Not_found
      else
	snd (arrayOfPreds.(index))


 (* BDDIMAGE: Code for BDD based image computation -- BDD variables for the "next" value of predicates *)       
 (** Given a pred, returns the bdd corresponding to the "next" value of pred *)
  
  (** To toggle between Curr/Next Indices *)   
  let getNIndexFromCIndex i = 
    let i' = arrayOfCStatePreds.(i) in
    if i' = -1 then None else Some i'

  let getCIndexFromNIndex i' = 
    let i = arrayOfNStatePreds.(i') in
    if i = -1 then None else Some i

  let isCurrentIndex i = (getNIndexFromCIndex i <> None)

  let isNextIndex i' = (getCIndexFromNIndex i' <> None)
  
  let getPredIndexNextBdd idx =
     assert (isCurrentIndex idx);
     let i' = arrayOfCStatePreds.(idx) in
     snd (arrayOfPreds.(i'))

  let transrel_bdd_table : ((int*int) * (int*int),CaddieBdd.bdd) Hashtbl.t = Hashtbl.create 109
  
  let eq_transrel_bdd_table : ((int*int) * (int*int),CaddieBdd.bdd) Hashtbl.t = Hashtbl.create 109
  
  let global_invariant_bdd_ref = ref bddOne
 
  let get_eq_transrel_bdd key = 
    try Hashtbl.find eq_transrel_bdd_table key with Not_found -> bddOne

  (** may return Not_found *)
  let get_transrel_bdd key = 
    try Hashtbl.find transrel_bdd_table key 
    with Not_found -> bddOne
    (*
      Message.msg_string Message.Debug "Not_found raised in get_transrel_bdd";
      raise Not_found *)

   let bddVarMap_list = ref [] 
  let curr_var_map = ref (CaddieBdd.toMap bddManager [])
  let curr_var_cube = ref bddOne
  let next_var_map = ref (CaddieBdd.toMap bddManager [])
  let next_var_cube = ref bddOne
  
  let update_bdd_info (i,i') =
    bddVarMap_list := (i',i)::!bddVarMap_list;
    curr_var_map := CaddieBdd.toMap bddManager (!bddVarMap_list);
    next_var_map := CaddieBdd.toMap bddManager (List.map (fun (x,y) -> (y,x)) !bddVarMap_list);
    curr_var_cube := CaddieBdd.bddAnd !curr_var_cube (getPredIndexBdd i);
    next_var_cube := CaddieBdd.bddAnd !next_var_cube (getPredIndexNextBdd i);
    ()
  
  let make_unprimed_bdd bdd' = CaddieBdd.replace bdd' (!curr_var_map)
  let make_primed_bdd bdd = CaddieBdd.replace bdd (!next_var_map)
  
  let bdd_postimage curr_bdd trel_bdd = 
    let next_bdd' = CaddieBdd.bddAndAbstract curr_bdd trel_bdd (!curr_var_cube) in
    let next_bdd = make_unprimed_bdd next_bdd' in
    if (Options.getValueOfBool "invbdd") then (CaddieBdd.bddAnd next_bdd !global_invariant_bdd_ref)
    else next_bdd

    
  let bdd_preimage next_bdd trel_bdd = 
    let next_bdd' = make_primed_bdd next_bdd in
    let curr_bdd = CaddieBdd.bddAndAbstract trel_bdd next_bdd' (!next_var_cube) in
    if (Options.getValueOfBool "invbdd") then (CaddieBdd.bddAnd curr_bdd !global_invariant_bdd_ref)
    else curr_bdd

  let bdd_exist_quantify bdd idx_list = 
   let q_cube = Misc.accumulate getPredIndexBdd CaddieBdd.bddAnd bddOne idx_list in
   CaddieBdd.bddAndAbstract bdd bddOne q_cube
  
  let make_eq_bdd cpred_idx_list = 
    let eq_bddi i = CaddieBdd.bddBiimp (getPredIndexBdd i) (getPredIndexNextBdd i) in
    let folder i bdd = CaddieBdd.bddAnd (eq_bddi i) bdd in
    List.fold_right folder cpred_idx_list bddOne
   

  let make_impl_bdd cpred_idx_list = 
    let eq_bddi i = CaddieBdd.bddImp (getPredIndexBdd i) (getPredIndexNextBdd i) in
    let folder i bdd = CaddieBdd.bddAnd (eq_bddi i) bdd in
    List.fold_right folder cpred_idx_list bddOne

      
  let add_new_pred_to_table (pred : Predicate.predicate) = 
    let _add () = 
      let i = getNextIndex () in
      let bddnd = CaddieBdd.bddNewVar bddManager in   
      tableOfPreds := (i, pred, bddnd) :: (!tableOfPreds); 
      Array.set arrayOfPreds i (pred,bddnd); 
      i
    in
    let i = _add () in
    Hashtbl.add hashTableOfPreds pred i;
    if (Options.getValueOfBool "bddpost") then
        begin (* BDDIMAGE: BDD based image computation *)
          let i' = _add () in
          Array.set arrayOfCStatePreds i i';
          Array.set arrayOfNStatePreds i' i;
          update_bdd_info (i,i')
        end;
    i
 
   (* returns indices for real predicates, not the next-state versions *) 
  let get_all_pred_indexes () = 
   if (Options.getValueOfBool "bddpost") then 
     Misc.map_partial (fun (i,_,_) -> if isCurrentIndex i then Some i else None) (!tableOfPreds)
   else List.map (Misc.fst3) (!tableOfPreds)

(* bdd: STATE - bdd, not TREL *)
  let extract_full_cube bdd = 
    if (CaddieBdd.bddEqual bdd bddZero) then failwith "empty bdd to extract_full_cube!";
    let proc int_bdd idx = 
      let idx_bdd = getPredIndexBdd idx in
      let conj_bdd =
        if not (CaddieBdd.bddEqual (CaddieBdd.bddAnd int_bdd idx_bdd) bddZero) then
          idx_bdd 
        else (CaddieBdd.bddNot idx_bdd) 
      in
      CaddieBdd.bddAnd int_bdd conj_bdd
    in
    List.fold_left proc bdd (get_all_pred_indexes ())
   
  (* END code for BDD based image computation *)


  (******************************************************************)
  (* Code to convert Predicates to BDD's *)

  let rec convertPredToBdd_map atomicpred_to_bdd pred =
    let convertPredToBdd = convertPredToBdd_map atomicpred_to_bdd in
    match pred with
      Predicate.Atom a -> 
        begin
         try atomicpred_to_bdd pred 
         with e -> 
           Message.msg_string Message.Error ("convertPredToBdd_map: Couldnt convert pred "^(Predicate.toString pred));
           Message.msg_string Message.Error ("raises :"^(Printexc.to_string e));
           failwith "error in convertPredToBdd_map"
        end
    | Predicate.And plist -> List.fold_left (fun b -> fun p -> CaddieBdd.bddAnd b (convertPredToBdd p)) bddOne plist
    | Predicate.Or plist ->  List.fold_left (fun b -> fun p -> CaddieBdd.bddOr b (convertPredToBdd p)) bddZero plist
    | Predicate.Not p -> CaddieBdd.bddNot (convertPredToBdd p)
    | Predicate.Implies (p1, p2) -> CaddieBdd.bddImp (convertPredToBdd p1) (convertPredToBdd p2)
    | Predicate.True -> bddOne
    | Predicate.False -> bddZero
    | _ -> failwith "convertPredToBdd: cannot handle quantified predicates"
  
  
  let rec convertPredToBdd pred =
    let atom_to_bdd p = 
      match p with
      Predicate.Atom a -> 
        begin
         try getPredBdd p 
         with Not_found -> 
           begin
             try CaddieBdd.bddNot (getPredBdd (Predicate.negateAtom p)) 
             with Not_found ->  failwith ("Did not find predicate "^(Predicate.toString pred)) 
           end
        end     
      | _ -> failwith "match fail in atom_to_bdd!"
    in
    convertPredToBdd_map atom_to_bdd pred

  (** 
    Code to convert a cube from a BDD (given as an int array) to
    the corresponding predicate
  *)
  let ac_convertCubeToPred map_idx cube =
    let _convertCubeWorker cube = 
      let predlst = ref [Predicate.True] 
       in
         for i = 0 to (Array.length cube -1) do
           match cube.(i) with
               0 ->  predlst := (Predicate.negate (map_idx i))::!predlst
             | 1 ->  predlst := (map_idx i)::!predlst
             | v ->  assert (v=2)
         done;
         !predlst
    in
    let pl = _convertCubeWorker cube in
      if (Array.length cube = 1) then (List.hd pl) else (Predicate.conjoinL pl)
          

let convertCubeToPred = ac_convertCubeToPred getPred

  (* Code to convert a BDD to a Predicate (sum-of-products form) *)
  let ac_convertBddToPred map_idx bdd =
    let list_of_minterms = ref [] in
    let _worker cube =
      list_of_minterms := (ac_convertCubeToPred map_idx cube)::(!list_of_minterms)
    in
      if (CaddieBdd.bddEqual bddOne bdd) then
        Predicate.True
      else 
        begin
          if (CaddieBdd.bddEqual bddZero bdd) then
            Predicate.False
          else
            begin
              (* CaddieBdd.bddForeachCube *)
              blast_bddForeachCube bdd _worker;
              if (List.length (!list_of_minterms) == 1) 
              then (List.hd !list_of_minterms)
              else Predicate.disjoinL (!list_of_minterms)
            end
        end
 
   let convertBddToPred = ac_convertBddToPred getPred
     
 (* TBD: MOVE THIS BACK UP, to the rest of the trel stuff!! once you have a reasonable print routine -- the current
 one is garbage! *)

 let bdd_to_string_pretty bdd =
   if (Options.getValueOfBool "bddprint") then 
     let gp i = if isNextIndex i then Predicate.Next (getPred i) else getPred i in
     Predicate.toString (ac_convertBddToPred gp bdd)
   else "XXX"
 
 let bdd_to_string bdd = "XXX"

 let update_transrel_bdd = 
   let tr_size_ref = ref 0 in
   fun key bdd -> 
     let ((l1,l2),(l1',l2')) = key in
     let oldbdd = try get_transrel_bdd key with Not_found -> bddOne in
     let newbdd = CaddieBdd.bddAnd oldbdd bdd in
     let size = List.length (bdd_support newbdd) in
     (if size > !tr_size_ref then 
       (Message.msg_string Message.Normal ("max tr: "^(string_of_int size)); 
       tr_size_ref := size));
     Message.msg_string Message.Debug 
     (Printf.sprintf "Update trel with: (%d,%d) ---> (%d,%d): %s " l1 l2 l1' l2' 
     (bdd_to_string_pretty bdd));
     Hashtbl.replace transrel_bdd_table key newbdd;
     if not (CaddieBdd.bddEqual newbdd oldbdd) then block_a_t_flag := true;
     ()

 let update_eq_transrel_bdd k is = 
   let b' = CaddieBdd.bddAnd (get_eq_transrel_bdd k) (make_eq_bdd is) in
   Hashtbl.replace eq_transrel_bdd_table k b'

 let update_global_inv_bdd bdd = 
   global_invariant_bdd_ref := CaddieBdd.bddAnd bdd (!global_invariant_bdd_ref)
   
 let bdd_node_count bdd = 
   let ctr = ref 0 in
   let f _ = ctr := !ctr + 1 in
   CaddieBdd.bddForEachNode  bdd f;
   !ctr

 let bdd_top_var bdd = 
   match bdd_support (CaddieBdd.bddTopVar bdd) with
   [h] -> h
   | _ -> failwith "error in bdd_top_var"
   
 let bdd_hash bdd =    
   let top_idx = bdd_top_var bdd in 
    let bdd_size = bdd_node_count bdd in
        (top_idx,bdd_size)

        
(** Code to convert BDDs into more reasonable formulas **)

(* code to convert a bdd into a predicate -- adds new temporary variables *)
let string_to_atom s = Predicate.Atom (Expression.Lval (Expression.Symbol s))    
   
    

class ['a] bdd_hash = 
  object 
    val bdd_table : ((int*int),(CaddieBdd.bdd * 'a) list) Hashtbl.t = Hashtbl.create 109
    
    method add_new bdd data = 
        Misc.hashtbl_update bdd_table (bdd_hash bdd) (bdd,data); 
            
    method find bdd = 
          let key = bdd_hash bdd in 
          let rv = 
            let cand =  Hashtbl.find bdd_table key in (* EXC: Not_found *)
            let flt (b,data) = if CaddieBdd.bddEqual b bdd then Some data else None in
                Misc.get_first flt cand
          in
          match rv with
            None -> raise Not_found
          | Some data -> data
  end

 (* this function traverses the bdd node like so: 
   f : int -> 'a -> 'a -> 'a
   base : bool -> 'a
   and takes a bdd and returns 'a 
   by recursively computing the result for the then-bdd and else-bdd and
   applying that to the id of the node to get the result of the toplevel bdd *)
   
 let bdd_fold f b bdd =
   let bddh = new bdd_hash in
   let rec _ac bdd =
     if (CaddieBdd.bddEqual bdd bddOne) then (b true)
     else if (CaddieBdd.bddEqual bdd bddZero) then (b false)
     else
       try bddh#find bdd 
       with Not_found ->
         begin
         let rpos = _ac (CaddieBdd.bddThen bdd) in
         let rneg = _ac (CaddieBdd.bddThen bdd) in
         let r = f (bdd_top_var bdd) rpos rneg in
           bddh#add_new bdd r; 
           r
         end
   in
   _ac bdd
   
   
(* Aggresive use of the above to turn a bdd into Predicate *)   
   
let bdd_tmp_var_idx = ref 0 
   
let next_bdd_tmp_atom () = 
  bdd_tmp_var_idx := !bdd_tmp_var_idx + 1; 
  string_to_atom ("t_"^(string_of_int !bdd_tmp_var_idx))
  
  
let bdd_to_pred_map mapper bdd = 
  let constr_list_ref = ref [] in
  let f idx then_atom else_atom =
    let vpos = mapper idx in
    let vneg = Predicate.negate vpos in
    let at = next_bdd_tmp_atom () in
    let new_cons =
      Predicate.Iff (at,
      Predicate.Or [Predicate.And [vpos;then_atom]; Predicate.And [vneg;else_atom]])
    in
    let _ = constr_list_ref := new_cons :: !constr_list_ref in
    at
  in
  let base_case b = if b then Predicate.True else Predicate.False in
  let top_atom = bdd_fold f base_case bdd in
  Predicate.And (top_atom::!constr_list_ref)

  
let bdd_to_predicate bdd = bdd_to_pred_map (getPred) bdd

end
  (**************************************************************************)
