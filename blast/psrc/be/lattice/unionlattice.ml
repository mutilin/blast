(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


(** Module for lattice implementations *)

open BlastArch

open Lattice

module Make_UnionLattice =
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
    functor (Op : OPERATION ) ->
      functor (Events : EVENTS with module C_SD = C_SD) ->
struct

  module Command = C_SD.Command
  (* Whenever you write a new lattice, add an entry here *)
  module TrivialLattice = (Lattice.Make_TrivialLattice (C_SD)) (Op)
  module SymbolicStoreLattice = (Lattice.Make_SymbolicStoreLattice (C_SD)) (Op)
  module ListLattice = (ListLattice.Make_ListLattice (C_SD)) (Op)
  module EC = ((EventCounterLattice.Make_EventCounterLattice (C_SD)) (Op)) (Events)
  
  (* This code assumes that all lattices have the same edge type *)
  type edge = C_SD.edge
      
  (* Whenever you write a new lattice, add an entry here *)
  type elements = 
      Trivial of TrivialLattice.lattice
    | Symb of SymbolicStoreLattice.lattice
    | LList of ListLattice.lattice
    | Ec of EC.lattice 

  type lattice = elements list

  (* We need to maintain flags to indicate whether each lattice type is enabled. This is used
     by post exclude disabled lattices. Originally, this was done in the definitions for
     top and bottom. However, this doesn't work because command line options are processed
     after this file is initialized. JF 3/13/05 *)
  let const_enabled = ref false
  let trivial_enabled = ref true
  let symb_enabled = ref false
  let list_enabled = ref false
  let ec_enabled = ref false 

  let filter l =
    List.filter (function
		     Trivial _ -> !trivial_enabled
		   | Symb _ -> !symb_enabled
		   | LList _ -> !list_enabled
		   | Ec _ -> !ec_enabled
		   ) l
  
  let print (f: Format.formatter)  (l:lattice) = (** Pretty printer for this lattice *)
    let f a = 
      match a with
          Trivial t1 -> if !trivial_enabled then (TrivialLattice.print f t1; Format.pp_force_newline f ())
        | Symb s1 -> if !symb_enabled then (SymbolicStoreLattice.print f s1; Format.pp_force_newline f ())
        | LList l1 -> if !list_enabled then (ListLattice.print f l1; Format.pp_force_newline f ())
        | Ec l1 -> if !ec_enabled then (EC.print f l1; Format.pp_force_newline f ())
      in
    List.iter f l 

  let toString = Misc.to_string_from_printer print

  let checklattice l =
    match l with
        Trivial t -> "trivial"
      | Symb s -> "symbolic"
      | LList ll -> "list"
      | Ec _ -> "ec"
      
  let top = [LList ListLattice.top; 
             Symb SymbolicStoreLattice.top; 
             Trivial TrivialLattice.top; 
             Ec EC.top
			 ]

  let bottom = [LList ListLattice.bottom; 
                Symb SymbolicStoreLattice.bottom; 
                Trivial TrivialLattice.bottom; 
                Ec EC.bottom
				]
		 
  let init  = [LList ListLattice.init; 
               Symb SymbolicStoreLattice.init ; 
               Trivial TrivialLattice.init ; 
               Ec EC.init
			   ]
  
  let sane a =
    let is_bottom l = 
      match l with
          Trivial t -> if t = TrivialLattice.bottom then true else false
        | Symb s -> if s = SymbolicStoreLattice.bottom then true else false
        | LList ll -> if ll = ListLattice.bottom then true else false
        | Ec l -> if l = EC.bottom then true else false
    in
      if List.exists is_bottom a then bottom else a 

  let predlist_bottom = 
    List.map (fun x -> ([], x)) bottom      
  (*
  let predlist_bottom = 
      ([([], LList ListLattice.bottom) ]) @
      ([([], Symb SymbolicStoreLattice.bottom) ]) @
      [ ([], Trivial TrivialLattice.bottom) ] @
      [ ([], Ec EC.bottom)]
  *)
	
  let aux_sane (a:(Ast.Predicate.predicate list * elements) list) =
    let is_bottom l = 
      match l with
          (_,Trivial t) -> if t = TrivialLattice.bottom then true else false
	| (_,Symb s) -> if s = SymbolicStoreLattice.bottom then true else false
	| (_,LList ll) -> if ll = ListLattice.bottom then true else false
	| (_,Ec l) -> if l = EC.bottom then true else false
    in
      if List.exists is_bottom a then predlist_bottom else a 
	
  let rec conjoin_results results cumval=
    match results with
      | [] ->
	  if (cumval = 0) then Ast.False
	  else if(cumval = 1) then Ast.True
	  else Ast.Dontknow
      | Ast.True::tail ->
	  if (cumval = 0) then failwith "fatal contradiction - true : conjoin_results@unionlattice.ml"
	  else conjoin_results tail 1
      | Ast.False::tail ->
	  if (cumval = 1) then failwith "fatal contradiction - false : conjoin_results@unionlattice.ml"
	  else conjoin_results tail 0
      | Ast.Dontknow::tail ->
	  conjoin_results tail cumval

  let aux_query_fn intval l pred  =
    let f a  =
      match a with
	  Trivial t1 ->
	    if intval = 1 then Ast.Dontknow
	    else TrivialLattice.query_fn t1 pred
	| Symb s1 ->
	    if intval = 2 then Ast.Dontknow
	    else SymbolicStoreLattice.query_fn s1 pred
	| LList l1 ->
	    if intval = 3 then Ast.Dontknow
	    else ListLattice.query_fn l1 pred
    | Ec _ -> Ast.Dontknow
    in
      List.map f l
	
  let query_fn (id:int) l (mother_query_fn:Ast.Predicate.predicate->Ast.predicateVal) (pred:Ast.Predicate.predicate) =
    let qresults:Ast.predicateVal list = aux_query_fn id l pred in 
    let aug_results = (qresults @ [mother_query_fn pred]) in 
    let cumval = -1 in 
      conjoin_results aug_results cumval

  let join (l1:lattice) (l2:lattice) =  
    let f a b =
      match (a,b) with
          (Trivial t1, Trivial t2) -> Trivial (TrivialLattice.join t1 t2)
        | (Symb s1, Symb s2) -> Symb (SymbolicStoreLattice.join s1 s2)
        | (LList ll1, LList ll2) -> LList (ListLattice.join ll1 ll2)
        | (Ec l1, Ec l2) -> Ec (EC.join l1 l2)
        | _ -> failwith "UnionLattice join: Mismatched or unknown lattice elements"
    in
      sane (List.map2 f (filter l1) (filter l2))
      (*sane (List.map2 f l1 l2)*)

(* Gregory's version *)
  let join_and_delta (l1:lattice) (l2:lattice) =  
    let f a b =
      match (a,b) with
          (Trivial t1, Trivial t2) -> let (x,y) = (TrivialLattice.join_and_delta t1 t2) in (Trivial x, Trivial y)
        | (Symb s1, Symb s2) -> let (x,y) = (SymbolicStoreLattice.join_and_delta s1 s2) in (Symb x, Symb y)
        | (LList ll1, LList ll2) -> let (x,y) = (ListLattice.join_and_delta ll1 ll2) in (LList x, LList y)
        | (Ec l1, Ec l2) -> let (x,y) = (EC.join_and_delta l1 l2) in (Ec x, Ec y)
        | _ -> failwith "UnionLattice join_and_delta: Mismatched or unknown lattice elements"
    in
    let (jl1,jl2) = List.split (List.map2 f (filter l1) (filter l2)) in
      (sane jl1, sane jl2)

  let meet (l1:lattice) (l2:lattice) =
    let f a b =
      match (a,b) with
          (Trivial t1, Trivial t2) -> Trivial (TrivialLattice.meet t1 t2)
        | (Symb s1, Symb s2) -> Symb (SymbolicStoreLattice.meet s1 s2)
        | (LList ll1, LList ll2) -> LList (ListLattice.meet ll1 ll2)
        | (Ec l1, Ec l2) -> Ec (EC.meet l1 l2)
        | _ -> let [s1;s2] = List.map checklattice [a;b] in
	       failwith (Printf.sprintf "UnionLattice meet: (%s,%s)" s1 s2)
    in
      sane (List.map2 f (filter l1) (filter l2))
      (*sane (List.map2 f l1 l2)*)

  let leq  (l1: lattice) (l2: lattice) =
    let f a b =
      match (a,b) with
          (Trivial t1, Trivial t2) -> (TrivialLattice.leq t1 t2)
        | (Symb s1, Symb s2) -> (SymbolicStoreLattice.leq s1 s2)
        | (LList ll1, LList ll2) -> (ListLattice.leq ll1 ll2)
        | (Ec l1, Ec l2) -> (EC.leq l1 l2)
        | _ -> failwith "UnionLattice leq: Mismatched or unknown lattice elements"
    in
      List.for_all2 f (filter l1) (filter l2) 
      (*List.for_all2 f l1 l2 *)
	
  let eq  (l1: lattice) (l2: lattice) =
    let f a b =
      match (a,b) with
          (Trivial t1, Trivial t2) -> (TrivialLattice.eq t1 t2)
        | (Symb s1, Symb s2) -> (SymbolicStoreLattice.eq s1 s2)
        | (LList ll1, LList ll2) -> (ListLattice.eq ll1 ll2)
        | (Ec l1, Ec l2) -> (EC.eq l1 l2)
        | _ -> failwith "UnionLattice eq: Mismatched or unknown lattice elements"
    in
      List.for_all2 f (filter l1) (filter l2) 
      (*List.for_all2 f l1 l2 *)

  let focus () = () (* Unimplemented *) 

  (** This needs to be reimplemented. 
    Right now it checks if the bdd is consistent with each
    individual lattice. How can we be sure that the lattice
    states themselves are consistent?
  *)
  let is_consistent  (l: lattice)  (bdd: CaddieBdd.bdd) =
    let f a =
      match a with
          Trivial t1 -> (TrivialLattice.is_consistent t1 bdd)
        | Symb s1 -> (SymbolicStoreLattice.is_consistent s1 bdd)
        | LList l1 -> (ListLattice.is_consistent l1 bdd)
        | Ec l -> (EC.is_consistent l bdd)
    in
      List.for_all f l 

  let rec aux_clean_post_output in_list out_list =
    if ((List.length in_list) = 0) then out_list
    else
      begin
	let head = List.hd in_list in
	let (predhead, lathead) = head in
	let (predlist, latlist) = out_list in
	let npredlist = predhead @ predlist in
	let nlatlist =  latlist @ [lathead] in
	let nout_list = (npredlist, nlatlist) in
	let tail = List.tl in_list in
	  if ((List.length tail) = 0) then nout_list
	  else aux_clean_post_output tail nout_list
      end
    
	
  let clean_post_output in_list =
    aux_clean_post_output in_list ([],[])

  (* JF 3/13/05
     TODO: need to improve performance - either use a static structure for union lattice or define a
     global top and bottom which can be expanded to the set of lattices enabled. *)      
  (** Compute the lattice which is result after applying
    computation in command to initial lattice *)
  let post (l: lattice) (e: edge) (fn:Ast.Predicate.predicate->Ast.predicateVal) = 
    Message.log_string_norm Message.UnionLat "Running post on command:";
    Message.log_print_norm Message.UnionLat C_SD.Command.print (C_SD.get_command e);
    let f a = 
      match a with
        | Trivial t1 ->
	    if !trivial_enabled then
	      let (predlist, lelement) = (TrivialLattice.post t1 e (query_fn 1 l fn))
	      in (predlist, Trivial lelement)
	    else ([], Trivial TrivialLattice.top)
	| Symb s1 ->
	    if !symb_enabled then
	      let (predlist, lelement) = (SymbolicStoreLattice.post s1 e (query_fn 2 l fn))
	      in (predlist, Symb lelement)
	    else ([], Symb SymbolicStoreLattice.top)
	| LList l1 ->
	    if !list_enabled then
	      let (predlist, lelement) = (ListLattice.post l1 e (query_fn 3 l fn))
	      in (predlist, LList lelement)
	    else ([], LList ListLattice.top)
        | Ec l' ->
            if !ec_enabled then 
              let (predlist, lelement) = (EC.post l' e (query_fn 4 l fn)) in
              (predlist, Ec lelement)
            else ([], Ec EC.top)
    in
    let (pred_list, out_lattice) = clean_post_output (aux_sane (List.map f l))
    in
      Message.log_string_norm Message.UnionLat "Post returning predicates:";
      Message.log_print_norm Message.UnionLat Ast.Predicate.print (Ast.Predicate.And pred_list);
      (pred_list, out_lattice)
      
 
  let summary_post l l' e e' =
    Message.log_string_norm Message.UnionLat "summary_post :";
    Message.log_print_norm Message.UnionLat print l;
    Message.log_print_norm Message.UnionLat print l';
    try 
    let f a b = 
      match (a,b) with 
          (Trivial l, Trivial l') -> Trivial (TrivialLattice.summary_post l l' e e')
        | (Symb l, Symb l') -> Symb (SymbolicStoreLattice.summary_post l l' e e')
        | (LList l, LList l') -> LList (ListLattice.summary_post l l' e e')
        | (Ec l,Ec l') -> Ec (EC.summary_post l l' e e')
    in
    List.map2 f l l'
    with _ -> failwith "error: unionLattice summary_post"

  (** Compute the lattice which is result after applying computation in command to initial lattice *)
  let pre  (l: lattice) e =     
    let f a = 
      match a with
          Trivial t1 -> Trivial (TrivialLattice.pre t1 e)
        | Symb s1 -> Symb (SymbolicStoreLattice.pre s1 e)
        | LList l1 -> LList (ListLattice.pre l1 e)
        | Ec l1 -> Ec (EC.pre l1 e)
    in
      sane (List.map f l )

  
  (* Call the initialize function of each enabled lattice and set its enabled flag *)
  let initialize () =
    if (Options.getValueOfBool "events") then (ec_enabled := true;EC.initialize ());
    let includes = Options.getValueOfStringList "include-lattice" in
      begin
	if not (Options.getValueOfBool "nolattice") then
	  begin
	    if List.mem "symb" includes then
	      (SymbolicStoreLattice.initialize ();
	       symb_enabled := true;
	       Message.log_string_always Message.UnionLat "Symbolic Store Lattice enabled");
	    if List.mem "list" includes then
	      (ListLattice.initialize ();
	       list_enabled := true;
	       Message.log_string_always Message.UnionLat "List Lattice enabled");
	  end;
	if !symb_enabled || !list_enabled || !ec_enabled then
	  trivial_enabled := false
	else
	  Message.log_string_always Message.UnionLat "Only trivial lattice enabled"
      end

   let enabled_ops l = 
     let f a = 
       match a with 
         Trivial l -> TrivialLattice.enabled_ops l
       | Symb l -> SymbolicStoreLattice.enabled_ops l
       | LList l -> ListLattice.enabled_ops l
       | Ec l -> EC.enabled_ops l
     in
     List.flatten (List.map f l)
   
  
end

      
