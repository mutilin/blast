(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module C = Ast.Counter
module M = Message

open BlastArch

module Make_EventCounterLattice = 
functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
  functor (Op : OPERATION ) ->
    functor (Ev : EVENTS with module C_SD = C_SD) ->
  struct
    
    (* for now, events are names of handlers *)
    module EventMap = Map.Make(struct
      type t = Ev.eventHandler
      let compare = compare
    end)
    
    type lattice = Emap of C.counter EventMap.t | Top
    type edge = C_SD.edge
    let bottom = Emap EventMap.empty 
    let top = Top (* Top has no other purpose *)
    let init = bottom
   
   
    let print fmt l =
      match l with
	  Top -> Format.fprintf fmt "ETop"
        | Emap e -> 
            let f k d s = s^(Printf.sprintf " : %s -> %s "
              (Ev.eventHandler_to_string k) (C.to_string d)) in
            let s = EventMap.fold f e "" in
            Format.fprintf fmt "@ Emap = %s" s

    
    let toString l = 
      match l with Top -> "ETop"
      | Emap e -> 
          let f k d s = s^(Printf.sprintf " : %s -> %s "
              (Ev.eventHandler_to_string k) (C.to_string d)) in
          EventMap.fold f e ""
    
    (* type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal*)
    let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) = Ast.Dontknow

    let lookup flag ec key = 
      try EventMap.find key ec 
      with Not_found -> if flag then C.omega () else (C.make_counter 0)
    
    let join l1 l2 =
      match (l1, l2) with
          (Top, _) | (_, Top) -> Top 
        | (Emap ec1, Emap ec2) ->
          let f k c m = EventMap.add k (C.plus c (lookup false m k)) m in
          Emap (EventMap.fold f ec2 ec1)
	
	let join_and_delta l1 l2 = let x =  join l1 l2 in (x, x)
          
    let meet l1 l2 =
      match (l1, l2) with
        | (Top,l)  | (l,Top) -> l
        | (Emap ec1, Emap ec2) ->
            let f ec k c m = 
              try 
                let c' = EventMap.find k ec in 
                EventMap.add k (C.ctrmin c c') m
              with Not_found -> m in
            Emap (EventMap.fold (f ec1) ec2 EventMap.empty) 
      
    (* TBD:SPEED *)        
    let leq a b =
      M.msg_string M.Debug (Printf.sprintf "EC.leq : %s : %s : \n" (toString a) (toString b)); 
        match (a,b) with 
        ( _ , Top) -> true 
      | (Top,_) -> false
      | (Emap ec1, Emap ec2) -> 
          let f k c b = 
            let c' = lookup false ec2 k in
            b && (C.leq c c') in
          EventMap.fold f ec1 true
      
    let eq a b = (leq a b && leq b a)
      
    let focus () = ()

    let is_consistent a b = true 

    let upd dir ec e = 
      M.msg_string M.Debug (Printf.sprintf "Event upd: e = %s : dir = %b" (Ev.eventHandler_to_string e) dir);
      let c = lookup false ec e in
      let c' = (if dir then C.plus else C.minus) c (C.make_counter 1) in
        EventMap.add e c' ec
   
  let post (a : lattice) op _ =
    M.msg_string M.Debug ("EC.post: "^(C_SD.get_edge_name op));
    M.msg_printer M.Debug print a;
    let a' = 
      match a with 
        Top -> a
      | Emap ec ->
          (if C_SD.isAsyncCall op then let (e,_) = Ev.deconstructAsyncCall op in Emap (upd true ec e)
          else if (C_SD.isDefinedFunCall op && Options.getValueOfBool "cf" && not (C_SD.isDispatchCall op)) 
               then bottom else a) in
    ([],a')

    let summary_post caller exit callop inbdd =
      M.msg_string M.Debug "in EC.summary_post";
      let rv =
        match (caller, C_SD.isAsyncExec callop) with 
          (Emap ec, true) -> 
            let (e,_) = Ev.deconstructAsyncCall callop in join (Emap (upd false ec e)) exit
        | _ -> join caller exit in
      M.msg_string M.Debug "done EC.summary_post";
      rv

    let pre a _ = a

    let initialize () = ()
    
    let enabled_ops l =
      M.msg_string M.Debug (Printf.sprintf "Event enabled_ops: %s" (toString l));
      let es = ref [] in
      let f k d = if not (C.is_zero d) then es := k ::!es in
      match l with
        Top -> failwith "EventCounterLattice: enabled_ops with Top"
      | Emap em ->  (EventMap.iter f em; List.map Ev.eventHandler_to_command !es)
 end


