(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open BlastArch

(** Module that performs Lvalue renaming *)
module LvalNumbering =
struct

  module Expression = Ast.Expression

  type elval_map = (Expression.lval, int list) Hashtbl.t
  type t = elval_map

  (** Initial, empty map *)
  let map0 = Hashtbl.create 109

  (** Remove the numbering of an expression *)
  let clean lvc = Expression.unprime lvc

  (** Get all the value that have been associated to a given map entry (excluding 0) *) 
  let assigned_values map = fun lv ->
    try
      let lst = Hashtbl.find map lv
      in if not (List.mem 0 lst) then lst else List.filter (fun x -> x <> 0) lst
    with Not_found -> []

  (** Lookup function in the map *)
  let theta map = fun lv -> (match assigned_values map lv with [] -> 0 | x :: _ -> x)

  let theta_str map = fun lv -> string_of_int (theta map lv)
      
  (** Provide the lvalue constant corresponding to the given lvalue *)
  let rec subst_lval map lv = match lv with
      Expression.Symbol s ->
	Expression.Chlval (lv, theta_str map lv)
    | Expression.Access (op, expr, lab) ->
	let sub = subst_expr map expr in
	let sub' = Expression.Access (op, sub, lab) in
	  Expression.Chlval (sub', theta_str map sub')
    | Expression.Dereference e ->
	let sub = subst_expr map e in
	let sub' = Expression.Dereference sub in
	  Expression.Chlval (sub', theta_str map sub')
    | Expression.Indexed (base, idx) ->
	let sub_b = subst_expr map base in
	let sub_i = subst_expr map idx in
	let sub' = Expression.Indexed (sub_b, sub_i) in
	  Expression.Chlval (sub', theta_str map sub')
    | _ -> failwith("subst_lval: unhandled construct"^(Expression.lvalToString lv))
  and subst_expr map e = match e with
      Expression.Lval lv -> (subst_lval map lv)
    | Expression.Chlval (lv, id) -> e
    | Expression.Binary (op, e1, e2) -> Expression.Binary (op, (subst_expr map e1), (subst_expr map e2))
    | Expression.CastCil (typ, e) -> Expression.CastCil (typ, (subst_expr map e))
    | Expression.Unary (Expression.Address, _) -> e (* no numbering under & *)
    | Expression.Unary (op, e) -> Expression.Unary (op, (subst_expr map e))
    | Expression.Sizeof s -> Expression.Constant (Ast.Constant.Int s)
    | _ -> e

  let rec _all_subst_lval min map lv = 
    let all_chlval_of_lval = function (elv, min) ->
      let indices = assigned_values map elv in
	List.map (fun x -> (Expression.Chlval (elv, (string_of_int x)), x)) (List.filter (fun x -> x >= min) indices)
    in match lv with
	Expression.Symbol s ->
	  all_chlval_of_lval (lv, 0)
      | Expression.Access (op, expr, lab) ->
	  let sub = _all_subst_expr min map expr in
	  let sub' = List.map (function (x,y) -> (Expression.Access (op, x, lab), y)) sub in
	    List.flatten (List.map all_chlval_of_lval sub')
      | Expression.Dereference e ->
	  let sub = _all_subst_expr min map e in
	  let sub' = List.map (function (x,y) -> (Expression.Dereference x, y)) sub in
	    List.flatten (List.map all_chlval_of_lval sub')
      | _ -> failwith("all_subst_lval: unhandled construct"^(Expression.lvalToString lv))
  and _all_subst_expr min map e = match e with
      Expression.Lval lv -> (_all_subst_lval min map lv)
    | Expression.Chlval (lv, id) -> [(e, int_of_string id)]
    | _ -> [(e, 0)]

  (** Provide all the lvalue constant corresponding to the given lvalue *)
  let all_subst_lval map lv = List.map (function (x, _) -> x) (_all_subst_lval 0 map lv)
  let all_subst_expr map lv = List.map (function (x, _) -> x) (_all_subst_expr 0 map lv)
			      
  (** update map written newnum returns a copy of map where the numbering
      is updated supposing that the lvalues in the (closed-under-aliasing)
      set of lvalues written may be written by setting the Chlval index to
      the newnum value *)
  let update map written newnum =
    let map' = Hashtbl.copy map in
    let _update_one_lval lv =
      let clv = subst_lval map' lv in
	match clv with
	    Expression.Chlval (elv, old) ->
	      let _ = Message.msg_string Message.Debug ("LvalNumbering: update("^(Expression.lvalToString elv)^") -> "^(string_of_int newnum)
							^" (old was "^old^")") in
	      let oldlst = assigned_values map' elv in
		(match oldlst with
		     x :: _ when x = newnum -> ()
		   | _ -> Hashtbl.replace map' elv (newnum :: oldlst))
	  | _ -> failwith ("update: _update_one_lval: Unexpected lvalue constant "^(Expression.toString clv))
    in
    let sorted_written =
      List.sort (fun a b -> compare (String.length (Expression.lvalToString a)) (String.length (Expression.lvalToString b))) written in
    let _ = List.iter _update_one_lval sorted_written in
      map'

  (** Do the same as update but take as argument a list of expressions.
      Non-Lval expressions are simply ignored *)
  let update_expr map written newnum =
    update map (Misc.map_partial (function Expression.Lval lv -> Some lv | _ -> None) written) newnum

  (** Print a string representation of a map *)
  let string_of_map map =
    Hashtbl.fold (fun elv i str -> str^(Expression.lvalToString elv)^" -> "^(theta_str map elv)^"; ") map "MAP: "

  (** Get the set of values for which a mapping exists *)
  let stored_lvals map = Hashtbl.fold (fun k _ l -> k :: l) map []

  (** Get the  list of extended lvalue that have different numberings in
      the two provided maps *)
  let diff map1 map2 =
    let all_keys = Misc.compact ((stored_lvals map1) @ (stored_lvals map2)) in
      List.filter (fun x -> (theta map1 x) <> (theta map2 x)) all_keys

  (** Get the largest label to which a value is mapped *)
  let max_lab map =
    let max_idx = Hashtbl.fold (fun _ kl old_max -> let kl_max = match kl with [] -> 0 | _ -> Misc.list_max kl in max old_max kl_max) map 0 in
      string_of_int max_idx

  (** Replace the out-most numbering by the older value in the table *)
  let one_step_older map = function
      Expression.Chlval (lv, _) | Expression.Lval lv -> 
	let vals = assigned_values map lv in
	  (match vals with
	       _ :: second :: _ -> Expression.Chlval (lv, string_of_int second)
	     | onlyone :: [] -> Expression.Chlval (lv, string_of_int onlyone)
	     | _ -> failwith "Corrupted map")
    | cv -> failwith ("one_step_older expects a Chlval or a Lval expression, but got "^(Expression.toString cv))
	
end
