(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)
     (**********************************************************************************)
     (******* the procedure below generates a boolean cfa ******************************)
     (**********************************************************************************)

(* This is the main function that generates a constraint formula for an edge and a bunch of predicates *)
(* The boolean variable for a particular predicate p = __BS__i where i is the predIndex of the pred *)
(* The value of the variable at the next time is __BS__i'*)
  
  let bp_prefix = "b_p__BS__" 
  let bp_next_prefix = bp_prefix^"_n"  
  
  let state_vars_table = Hashtbl.create 1009
  let comb_vars_table = Hashtbl.create 1009
  let bs_table = Hashtbl.create 1009
  let dumpBoolFlag = ref true 

  let edge_constraint_table = Hashtbl.create 1009
  let edge_constraint_in_table = Hashtbl.create 1009
  let edge_constraint_out_table = Hashtbl.create 1009

  let make_boolean_state_var pred_index = 
      let _ = M.msg_string M.Debug "back from getPredIndex" in
      let pre_var = bp_prefix^(string_of_int pred_index) in
      let post_var = bp_next_prefix^(string_of_int pred_index) in
      let _ = Hashtbl.replace state_vars_table pre_var true in
      (P.Atom (E.Lval (E.Symbol pre_var)), 
       P.Atom (E.Lval (E.Symbol post_var)))

  let copy_update_constraints pred_index_list =
    let copy_pred pred_index = 
      let (pre_var,post_var) = make_boolean_state_var pred_index in
        P.Iff (pre_var,post_var)
    in
    P.conjoinL (List.map copy_pred pred_index_list)
 

    
  let constraint_for_edge edge e_id ce_o =
    try Hashtbl.find edge_constraint_table e_id 
    with Not_found ->
      (lvmap_reset ();
      let (s_id,t_id) = e_id in
      let fname = C_SD.get_location_fname (C_SD.lookup_location_from_loc_coords s_id) in
      let postloc = C_SD.lookup_location_from_loc_coords t_id in
      let rv1 = block_concrete_data_pre fname 0 edge ce_o in
      let rv2 = copy_lvmap () in
      lvmap_reset ();
      Hashtbl.replace edge_constraint_table e_id (rv1,rv2);
      (rv1,rv2))

  let block_constraint_for_pred_in lvmap_in e_id pred_index =
    try Hashtbl.find edge_constraint_in_table (e_id,pred_index) 
    with Not_found ->
      (let rv = 
        let (pre_var,_) = make_boolean_state_var pred_index in
        let pred = pred_to_mccarthy (PredTable.getPred pred_index) in
          P.iff pre_var (pred_lval_map_a lvmap_in 0 pred) in
      Hashtbl.replace edge_constraint_in_table (e_id,pred_index) rv;
      rv)
          
  let block_constraint_for_pred_out (cmd,edge) e_id pred_index =
    try Hashtbl.find edge_constraint_out_table (e_id,pred_index) 
    with Not_found ->
    (let rv = 
       let (pre_var,post_var) = make_boolean_state_var pred_index in
       let pred = pred_to_mccarthy (PredTable.getPred pred_index) in
       let pred' = pred_lval_map 0 pred in
       let _ = 
         let wpp =  (concrete_data_pre false false {Region.pred = pred;} cmd edge (Hashtbl.create 2)).Region.pred in
         if (wpp <> pred) then PredTable.update_pred_affected_table e_id pred_index in
         P.iff pred' post_var in
    Hashtbl.replace edge_constraint_out_table (e_id,pred_index) rv;
    rv)
      
  let block_predicate_update_constraints edge (postloc,ce_o) (in_pred_index_list,out_pred_index_list) = 
    if (C_SD.isReturn edge) then  
       (match ce_o with None -> failwith "block_pred_upd_cons: no matching caller!"
                      | Some ce -> Absutil.set_ret_pointers_ce ce);
    let e_id = (fst (C_SD.get_edge_coords edge), C_SD.location_coords postloc) in
    let (econs,lvmap_r) = constraint_for_edge edge e_id ce_o in
    let chain = List.map (block_constraint_for_pred_in lvmap_r e_id) in_pred_index_list in
    lvmap_reset ();
    let wpc = List.map (block_constraint_for_pred_out (C_SD.get_command edge,edge) e_id) out_pred_index_list in
    let _ =  Absutil.reset_ret_pointers () in
    (chain,wpc,[econs])
   
  let predicate_update_constraints (cmd,edge) postloc (in_pred_index_list,out_pred_index_list) = 
    let _ = M.msg_string M.Debug "In predicate_update_constraints" in
    let _assume_predicate_list = 
      match cmd.C_SD.Command.code with 
          C_SD.Command.Pred e -> [(pred_to_mccarthy e)] 
	| _ -> []
    in
    let e_id = (fst (C_SD.get_edge_coords edge), C_SD.location_coords postloc) in
    let constraint_for_pred_in pred_index = 
      let _ = M.msg_string M.Debug "In constraints_for_pred_in" in
      let (pre_var,_) = make_boolean_state_var pred_index in
      P.iff pre_var (pred_to_mccarthy (PredTable.getPred pred_index))
    in
    let constraint_for_pred_out pred_index =
      let _ = M.msg_string M.Debug "In constraint_for_pred_out" in
      let (pre_var,post_var) = make_boolean_state_var pred_index in
      let emptytable = Hashtbl.create 2 in
      let pred = pred_to_mccarthy (PredTable.getPred pred_index) in
      let wp_pred = 
        match _assume_predicate_list with
        [] -> (* not an assume *)
        let rv = (concrete_data_pre false false { Region.pred = pred ; } cmd edge emptytable).Region.pred in
        if (rv <> pred) then PredTable.update_pred_affected_table e_id pred_index;
        rv
        | [e] -> P.Implies ((pred_to_mccarthy e),pred)
        | _ -> failwith "bad _assume_predicate_list in constraint_for_pred"
      in 
      if (wp_pred = pred && _assume_predicate_list = []) then 
        let p' = if (Options.getValueOfInt "craig" = 2) then P.iff  pred post_var else P.True in
        P.And [p' ;(P.iff pre_var post_var)]
      else (P.iff wp_pred post_var)
	(* I deliberately made it wp <--> x_p' *)
    in
    let _ = M.msg_string M.Debug "Leaving predicate_update_constraints" in
    let _ =  if (C_SD.isReturn edge) then  
                  (let callLoc = (C_SD.get_source (Misc.list_filter_unique C_SD.isFunCall (C_SD.get_ingoing_edges postloc))) in
		  let _ = last_loc_pointer := Some(callLoc) in
		  let (rt,_,retloc ,subs) = lastTarget_loc (Some (C_SD.get_source edge)) in
	          Absutil.set_ret_pointers rt subs (C_SD.get_location_fname retloc);
	          last_loc_pointer := None)
    in
    let chain = List.map constraint_for_pred_in in_pred_index_list in
    let wpc = List.map constraint_for_pred_out out_pred_index_list in
    let _ =  Absutil.reset_ret_pointers () in
    (chain,wpc,_assume_predicate_list)

(* this takes a predicate in a rich logic and then employs uclid etc to get a predicate (P.predicate)
which is totally boolean ... it also adds to the comb_var_table ...*)
  let pred_to_bool_counter = ref 0

let predicate_to_boolean pred = 
   let _ = M.msg_string M.Debug "In predicate_to_boolean" in
   let _ = (pred_to_bool_counter := !pred_to_bool_counter + 1) in
   let int_to_atom _s = 
     let _ = print_string "." in
     let neg = Misc.is_prefix "-" _s in
     let s = List.hd (Misc.chop _s "-") in
     let sym = 
       if Hashtbl.mem bs_table s 
       then Hashtbl.find bs_table s 
       else 
	 begin
	   Hashtbl.replace  comb_vars_table ("b_p_x_"^s) true;
	   ("b_p_x_"^s) 
	 end
     in 
       match neg with
	   true -> P.negate (P.Atom (E.Lval (E.Symbol sym)))
	 | false -> (P.Atom (E.Lval (E.Symbol sym)))
   in
    
   let _ = Hashtbl.clear bs_table in
   let modelname = "tmp_"^(string_of_int !pred_to_bool_counter) in
   (*let _ = TheoremProver.uclid_output modelname pred "" in*)
	 let _ = "wanted to call uclid_output, but it was removed" in
   let _ = Sys.command ("rm "^modelname^".cnf") in   
   let _ = M.msg_string M.Debug "Now calling Uclid" in
   let uclidret = Sys.command ("runuclid "^modelname^".ucl cnf") in
   let _ = M.msg_string M.Debug "Back from Uclid" in

     (* now we have to read back in tmp.cnf and make sense of it ...*)
   let _ = M.msg_string M.Debug "Now parsing cnf file" in

   let string_clause_of_string x = List.tl (List.rev (Misc.words_of_string x)) in
   let list_of_clauses = 
     try 
       List.tl (List.map string_clause_of_string (Misc.string_list_of_file (modelname^".cnf"))) 
     with _ -> [] (* this exception happens if uclid doesnt generate blasttrace.cnf which is when either the 
		     thing is always true ? or its always false ? let us simply say [] now *)
   in
   let _ = M.msg_string M.Debug "Done parsing cnf file" in
   (* drop the first line which is a bunch of whooey anyways ...*)
     (* now we must make up a map so as to know which are the fun variables (ie. of interest ...*)
   let _ = Sys.command ("fgrep \"b_p\" "^modelname^".map > BS.map") in
   let _ = M.msg_string M.Debug "Reading in BS map" in
   let var_map = List.map 
		   (fun l -> Hashtbl.replace bs_table (List.hd (List.tl 
l)) (List.hd l))
		   (List.map Misc.words_of_string (Misc.string_list_of_file "BS.map"))
   in
   let _ = M.msg_string M.Debug "Done reading BS map" in
     (* now all we need is judicious use of map ! *)
   let retval = P.And (List.map (fun l -> P.Or (List.map int_to_atom l)) list_of_clauses)
   in
     M.msg_string M.Debug "Returning from predicate_to_boolean";
     retval

  let generate_boolean_cfa predlist start_loc err_loc outfile =
    let module LocationSet =
      Set.Make (struct
                  type t = int 
                  let compare x y =  compare x  y
                end) in
    let pred_i_list = List.map (PredTable.addPred) predlist in 
    let cfa_chan = open_out ".trans.cfa" in
    let bcfa_chan = open_out ".trans.bcfa" in
    
    let sprint_edge e = 
      let (l1,c,l2) = (C_SD.get_source e, C_SD.get_command e, C_SD.get_target e) in 
      let (_,y1) = C_SD.location_coords l1 in
      let (_,y2) = C_SD.location_coords l2 in
      let blast_pred_e = 
        let (chain,wpc,asm) = (predicate_update_constraints (c,e) l2 (pred_i_list,pred_i_list)) in
        P.conjoinL (chain @ wpc @ asm) 
      in
      let _ = M.msg_string M.Debug "Back from pred_upd_const" in
      let _ = flush stdout in
	match (!dumpBoolFlag) with
	    true -> 
	      begin
		output_string bcfa_chan 
		  (Printf.sprintf "[ %d ]  ->  [ %d ] : %s ; \n \n" y1 y2 
		     (P.toString (predicate_to_boolean blast_pred_e)));
		M.msg_string M.Debug "Dumped edge to file"
	      end
	  | false -> 
	      begin
		output_string cfa_chan
		  (Printf.sprintf "[ %d ]  ->  [ %d ] : %s ; \n \n" y1 y2 
		     ( P.toString blast_pred_e ));
		print_string "dumped edge to file"
	      end
    in

    let sprint_function fname =
      let already_visited_source_locations = ref LocationSet.empty in
      let rec sprint_location_and_edges loc =
	let (_,loc_id) = C_SD.location_coords loc in
        if (LocationSet.mem loc_id !already_visited_source_locations) then
          ()
        else
          begin
            let outgoing_edges = List.rev (C_SD.get_outgoing_edges loc) in
	    let _ = List.iter sprint_edge outgoing_edges in
	      already_visited_source_locations := (LocationSet.add (snd (C_SD.location_coords loc)) !already_visited_source_locations) ;
		(List.iter (fun e -> sprint_location_and_edges (C_SD.get_target e)) outgoing_edges)          end 
      in
        sprint_location_and_edges (C_SD.lookup_entry_location fname)
    in
    let _ = dumpBoolFlag := true in
      (* this thing below will have to change when we add support for function calls *)
    let (* trans_string *) _ = 
       (* String.concat "\n" *)
	 (List.iter sprint_function (C_SD.list_of_functions ()))
    in
    let _ = dumpBoolFlag := false in
    let _ = (List.iter sprint_function (C_SD.list_of_functions ())) in
    let (_,_) = (close_out cfa_chan, close_out bcfa_chan) in
    let reach_string = Printf.sprintf "[ %d ] -> [ %d ];" (snd (C_SD.location_coords start_loc)) (snd (C_SD.location_coords err_loc)) 
    in
    let state_string = (String.concat "," (Misc.run_on_table (fun x _ -> x) state_vars_table))^";" in
    let comb_string = (String.concat "," (Misc.run_on_table (fun x _ -> x) comb_vars_table))^";" in
    let boolean_cfa_string = 
      Printf.sprintf "STATEVAR \n %s \n COMBVAR \n %s \n TRANS \n"
	state_string comb_string
    in
    let fo_cfa_string = 
      Printf.sprintf "STATEVAR \n %s \n COMBVAR \n %s \n TRANS \n"
	state_string "" (* comb_string fo_trans_string reach_string *)
    in
    let _ = Misc.write_to_file ".temp1" (Printf.sprintf "REACH \n %s \n" reach_string) in
      Misc.write_to_file (".temp") boolean_cfa_string;
      ignore (Sys.command ("cat .temp .trans.bcfa .temp1 > "^outfile^".bcfa"));
      ignore (Sys.command ("cat .temp .trans.cfa .temp1 > "^outfile^".cfa"));
      ignore (Sys.command ("rm .temp")); 
      ignore (Sys.command ("rm .temp1"));
      ignore (Sys.command "rm .trans.bcfa"); 
      ignore (Sys.command "rm .trans.cfa") ;
      ()
      
(************************* END BOOLEAN CFA DUMP routines ****************************)
(************************* START the uclidsat cref stuff ****************************)

  (* this next function will produce the huge formula that we will send off to UCLID *)
  (* region0 -> op1 -> region 1 -> ...-> op n -> region n *)
  (* as of now this just dumps the trace to a file and then *)

  let rename_with_suffix pred suffix_string = 
    let new_name l =
      match l with 
	E.Symbol s -> E.Symbol (s^suffix_string)
      |	_ -> failwith "rename_with_suffix"
    in
    P.alpha_convert new_name pred 
   
  let trace_map_table = Hashtbl.create 1009 
  let comp_tm_table = Hashtbl.create 1009
 

  let got_new_split = ref false 


  let sign x = if x > 0 then 1 else -1

  let var_of_string x = 
    let frags = 
      List.filter (fun y -> y <> "") (Misc.chop x "_") 
    in
      (int_of_string (List.nth frags 3), int_of_string (List.nth frags 4),(List.length frags = 6))
  

  let process_block_constraints cons_list =
    let _ = Sys.command "rm -f t-*.cnf" in
    let clauses = ref 0 in
    let pred_array = Array.create_matrix (!predIndex + 1) (List.length cons_list + 1) 0 in
    let pred_prime_array = Array.create_matrix (!predIndex + 1) (List.length cons_list + 1) 0 in
    let _ = Hashtbl.clear comp_tm_table in

    let rec _process_block_constraints i offset cons_list =
      let replace_var s = 
	let var = int_of_string s in
	  (* the branch prediction is getting messed up here *)
	  if (var = 0) then "0" else string_of_int (var + ((sign var)*offset))
      in
      let convert_cnf_file () = 
	let h::t = List.map Misc.words_of_string (Misc.string_list_of_file "tmp.cnf") in
	let new_lines = (List.map (fun l -> ((String.concat "    " (List.map replace_var l))^"\n")  ) t) in
	  Misc.write_list_to_file ("t-"^(string_of_int i)^".cnf") new_lines;
	  clauses := !clauses + (int_of_string (List.nth h 3));
	  int_of_string (List.nth h 2) (* this returns the number of vars in this CNF file *)
      in
      let process_map () =
	let map_items = List.map Misc.words_of_string (Misc.string_list_of_file ("BS-"^(string_of_int i)^".map")) in
	let proc_item lst =
	  match lst with
	   (x::y::_) ->
	     begin
	       let (pred_id,time,prime) = var_of_string x in 
	         match prime with
		  true -> pred_prime_array.(pred_id).(time) <- (int_of_string y + offset)
	         | false -> 
		   begin
		     Hashtbl.add comp_tm_table (int_of_string y + offset) (pred_id,time);
		     pred_array.(pred_id).(time) <- (int_of_string y + offset)
		   end
             end
          | _ -> failwith "proc_iterm"
	in
	  List.iter proc_item map_items
      in
	match cons_list with
	    [] -> offset
	  | hd::tl -> 
	      begin
		(*TheoremProver.uclid_output "tmp" hd "";*)
	 let _ = "wanted to call uclid_output, but it was removed" in
		Sys.command "runuclid tmp.ucl";
		Sys.command ("fgrep \"b_p\" tmp.map > BS-"^(string_of_int i)^".map");
		process_map ();
		let num_new_vars = convert_cnf_file () in
		  _process_block_constraints (i+1) (offset + num_new_vars) tl
		    (* recall that we must end up recurring with (i+1) (offset + vars) tl *)	
	      end
    in
    let variables = _process_block_constraints 1 0 cons_list in
      (* now we have to cat the files together and add to it the tying together constraints *)
    let connect_constraint = 
      let s = ref "" in
	for j = 0 to (!predIndex) do
	  for k = 2 to (List.length cons_list) do
	    let v1 = pred_array.(j).(k) in
	  let v2 = pred_prime_array.(j).(k-1) in
	    s := !s^(Printf.sprintf "%d    -%d    0 \n -%d   %d   0\n" v1 v2 v1 v2);
	  done;
	done;
	clauses := !clauses + 2*(1 + !predIndex)*(List.length cons_list - 1);
	!s
    in
      Misc.write_to_file "sat-info" (Printf.sprintf "p cnf %d %d \n %s" variables !clauses connect_constraint);
      ignore (
	Sys.command "cat sat-info t-*.cnf > trace.cnf"; 
	Sys.command "rm ztrace.*";
	Sys.command "smvsat trace.cnf satout";);
      try
	let vars_in_proof =  
	  List.map (fun x -> int_of_string (List.hd x)) 
	    (List.map Misc.words_of_string (Misc.string_list_of_file "ztrace.variables")) 
	in
	  List.filter (Hashtbl.mem comp_tm_table) vars_in_proof
      with _ -> failwith "Trace is satisfiable, apparently!"
      
	
	
   let analyze_trace region_list op_list = 
     let _ = Hashtbl.clear trace_map_table in
     let pipe_counter = ref 0 in
     let next_pipe_var pred i = 
       Printf.sprintf "%s%d_%d" bp_prefix (PredTable.getPredIndex pred) i 
     in
     let predicate_list = List.map Misc.snd3 !PredTable.tableOfPreds in
	let constraint_to_boolean_with_renaming cons i =
	let boolean_cons = predicate_to_boolean cons in
	let rename_function l = 
	match l with
	E.Symbol x ->
	if (Misc.is_prefix "b_p_x_" x) then E.Symbol (x^("_t_"^(string_of_int i))) 
	else E.Symbol x
	| _ -> failwith "constraint_to_boolean_with_renaming"
       in
	 P.alpha_convert rename_function boolean_cons
     in
     let process_op (reg,op,i) =
       let rename_with_suffix pred suffix_string = 
         let vars_in_pred = 
           let _var_string_list = ref [] in
             List.iter 
               (fun s -> if (not (List.mem s (!listOfConstants))) 
                then _var_string_list := s::(!_var_string_list)
                else  ()
               )
               ( Absutil.getVarExps_deep pred ); !_var_string_list 
         in
         let subs_pairs = 
           List.map 
	     (fun x -> (E.Lval (E.Symbol (x)), E.Lval (E.Symbol(x^suffix_string)))) 
	     vars_in_pred
         in
           P.substitute  subs_pairs pred
       in
       let _assume_predicate_list = 
         let cmd = (C_SD.get_command op).C_SD.Command.code in
           match cmd with 
               C_SD.Command.Pred e -> [(rename_with_suffix e ("_"^(string_of_int i)))] 
	     | _ -> []
       in
       let process_pred pred =
	 let emptytable = Hashtbl.create 2 in
	 M.msg_string M.Debug "Creating empty table in process_pred: will probably break.";
         let _phi = spre (replace_data_region reg (Region.Concrete {Region.pred = pred})) op emptytable in
         let phi_p = 
              if (_assume_predicate_list <> []) then pred 
              else 
              begin
                match _phi with 
                Region.Atomic foo -> 
                  begin
                    match foo.Region.data with 
                        Region.Concrete x -> x.Region.pred
                      | _ -> failwith("bad return from pre !")
                  end
               |  _ -> failwith ("bad region in analyze trace")
	      end
         in
	 let p_i_minus_1_prime = rename_with_suffix pred ("_"^(string_of_int (i-1))^("_p")) in
         let p_i = rename_with_suffix pred ("_"^(string_of_int i)) in
         let phi_p_i = rename_with_suffix phi_p ("_"^(string_of_int i)) in
         let p_i_prime = rename_with_suffix pred ("_"^(string_of_int (i))^("_p")) in
          (* a simple iff *)
         let pipe = next_pipe_var pred i in
	 let pipe_prime = pipe^"_p" in
         let _pipe_pred = P.Atom (E.Lval (E.Symbol pipe)) in
	 let _pipe_pred_prime = P.Atom (E.Lval (E.Symbol pipe_prime)) in
	 let _last_pipe_pred_prime = P.Atom (E.Lval (E.Symbol (next_pipe_var pred (i-1)))) in 
	   
	 let left_cons = [(P.iff _pipe_pred p_i)] 
         in
         let right_cons = P.iff phi_p_i _pipe_pred_prime in
           right_cons::left_cons (* avoiding the use of conjoinL *)
       in
       let op_constraints = List.flatten (List.map process_pred predicate_list) in
	 (P.And (_assume_predicate_list @ op_constraints)) 
	 (*
	   let retval = 
	   in
	   M.msg_string M.Normal ("Done booleanifying block"^(string_of_int i));
	   retval
	 *)
   in
       (* the above is what you get for (reg,op,i)-now make a list of those and hit it with List.map*)
     let rec make_op_reg_i_triple rl ol i = 
       match (rl,ol) with 
	   (h::t,h1::t1) -> (h,h1,i)::(make_op_reg_i_triple t t1 (i+1)) 
	 | ([],[]) -> []
	 | _ -> failwith ("poor args to make_op_reg_i_triple")
     in
     let op_reg_i_list = make_op_reg_i_triple  (List.tl region_list) op_list 1 in
       (* at this point things start to get a little messy ... *)
     

     
     let uclid_constraint_list = List.map process_op op_reg_i_list in
     let useful_vars = process_block_constraints uclid_constraint_list in
     (*	 
     let uclid_constraints = P.And((List.map process_op op_reg_i_list)) in
       M.msg_string M.Normal "Querying Uclid";
       let useful_vars = TheoremProver.uclidSat uclid_constraints in
	 begin
	   match useful_vars with 
	       [] -> failwith "UCLID says trace is satisfiable!"
	     | l -> List.map (fun x -> print_string (x^" ")) useful_vars
	 end;
	 
	*)
	  (* toggle comment*)
     let process_useful_var var =
       try
	 let (index,i) = Hashtbl.find comp_tm_table var in (* toggle comments *)
	 (* let (index,i,_) = var_of_string var in *)            

	 let (reg,_,_) = List.find (fun (_,_,j) -> j=i-1) op_reg_i_list in
	   
	 let location_of_pred = location_coords_of_region reg in
	 let lc = C_SD.location_coords location_of_pred in
	 let l = try Hashtbl.find post_split_table lc with _ -> [] in
	 let newl = 
	   if (List.mem index l) then l else 
	     begin
	       M.msg_string 
		 M.Debug (Printf.sprintf "%s : (%d , %d);" 
				   (P.toString (PredTable.getPred index)) (fst lc) (snd lc) (* (C_SD.location_to_string location_of_pred) *));
	       
	       got_new_split := true;
	       (index::l)
	     end
	 in
	   Hashtbl.replace post_split_table lc newl
       with 
	   Not_found -> (M.msg_string M.Debug ("Warning: BOGUS
           USEFUL VAR!!Not splitting on pipe-var"^(string_of_int var));failwith
           "Bogus useful var!")
     in
       got_new_split := false;
       List.iter process_useful_var useful_vars;
       if (!got_new_split) then M.msg_string M.Debug "Learnt new split predicate"
       else failwith ("No new split information !!")
       ;
       M.msg_string M.Debug "Restarting MC with split information"
	 
(*
     if (trace_is_feasible) then failwith("uclid: COUNTEREXAMPLE IS VALID !") else failwith("uclid: INVALID COUNTEREXAMPLE!")
      *)
(**************************************************************************************)


