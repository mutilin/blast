(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)



(**
  * This module implements the abstraction interface connecting the model
  * checker to the concrete transition system.  It defines a Region module
  * based on Predicate Abstraction and BDDs, the symbolic pre and post,
  * and routines for refinment used by the model checker.
*) 


open BlastArch

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module Counter = Ast.Counter
module Stats = Bstats
module M = Message
module E = Expression
module P = Predicate
module AA = AliasAnalyzer
module O = Options

module Make_C_Abstraction =
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
struct

  module PredBdd =  PredBdd.Make_PredBdd(C_SD) 
  module PredTable = PredTable.Make_PredTable(PredBdd)
  module Absutil = Absutil.Make_Absutil(PredTable)
  module Events = Events.Make_Events(PredTable)
  module Cmd = C_SD.Command

  open PredTable
  open Absutil

  let arg_subst _ = failwith "Not implememted"

  let check_races = ref None (* this will now be an lval *)

		      
  let assumed_invariants = ref []


  let maxStackLen = ref 0 
  let getCurrentPredIndex () = PredTable.getCurrentIndex ()


				 

  let dump_abs () = PredTable.dump_abs ()
  let is_fun_refined fn = PredTable.is_fun_refined fn 
		      


  (*********************************************************************************************)
  (*********************************************************************************************)
  (* The operation, DataLattice and Region modules  *)
				 include(abs-region.ml)
  (*********************************************************************************************)
  (*********************************************************************************************)
				 
 				 

  (**********************************************************************************************)
  (**********************************************************************************************)
  (* The environment automaton model in the TAR algorithm *)
  (**********************************************************************************************)
  (**********************************************************************************************)
  type opKind = ProgramOperation of Operation.t | AutomatonOperation of (int * int) 

  module EnvAutomaton =
  struct
    
    type onode_label =
	{
	  oid : int;
	  oadr : Region.abstract_data_region;
	  oregion : Region.t
	}
	  
    (* Invariants: Node 0 is always the stuck node *)      
	  
    type anode_label =
	{ id   : int ; (* the id of the abstract automaton node *)
	  root : int ; (* the id of the root node in the cluster *)
	  mutable adr  : Region.abstract_data_region ; (* the abstract data region (is this on globals?) *)
          mutable atomic : bool ;
	  mutable task : bool;
	  mutable event : bool;
	  mutable modifies : E.lval list ; (* the list of globals that are modified by this transition *)

	}	
	  
    type aedge_label = 
	Thread of E.lval list              (* Solid edge *) 
      | Env                 (* Dotted edge *)
	  
    type oautomaton_node = (onode_label, opKind) BiDirectionalLabeledGraph.node
	
    type automaton_node = ((* node label *) anode_label, (* edge label *) aedge_label) BiDirectionalLabeledGraph.node 
	
    type  ('a, 'b) automaton = {
      
      autom_root : automaton_node ;
      
      
      number_of_states : int ;
      
      id_to_node_ht : (int, automaton_node) Hashtbl.t ;
      
      o_auto_root : oautomaton_node;
      
      cover_set : int Region.coverers;
      (* other maps *)
      o_map : (int, oautomaton_node) Hashtbl.t;  (* auto_map *)
      o_to_a_map : (int, int) Hashtbl.t;         (* auto_shrink_map *)
      a_to_o_map : (int, int list) Hashtbl.t;    (* shrink_elts_map *)
      
     } 

    let getNodeFromId autom i = (* raises Not_found *)
      Hashtbl.find autom.id_to_node_ht i

    let getAdrFromId autom i = (* raises Not_found *)
      let nd = getNodeFromId autom i in
	(BiDirectionalLabeledGraph.get_node_label nd).adr

    let getIdFromNode autom nd = 
      (BiDirectionalLabeledGraph.get_node_label nd).id

    let getModifiesFromNode autom nd = 
      (BiDirectionalLabeledGraph.get_node_label nd).modifies

    let getModifiesFromId autom id = 
      let nd = getNodeFromId autom id in
	(BiDirectionalLabeledGraph.get_node_label nd).modifies

    let getModifiesOnEdge autom id = 
      let nd = getNodeFromId autom id in
      List.fold_left (fun sofar e -> match (BiDirectionalLabeledGraph.get_edge_label e) with 
	Thread lv -> Misc.union lv sofar | Env -> sofar)
	[] (BiDirectionalLabeledGraph.get_out_edges nd)
	
    let getAdrFromNode autom nd = 
      (BiDirectionalLabeledGraph.get_node_label nd).adr

    let getEnvEdges autom i =
      let thisnode = getNodeFromId autom i in
      let outgoing = BiDirectionalLabeledGraph.get_out_edges thisnode in
      List.filter (fun e -> match (BiDirectionalLabeledGraph.get_edge_label e) with 
	Env -> true | Thread _ -> false) outgoing

    let getThreadEdges autom i =
      let thisnode = getNodeFromId autom i in
      let outgoing = BiDirectionalLabeledGraph.get_out_edges thisnode in
      List.filter 
	(fun e -> match (BiDirectionalLabeledGraph.get_edge_label e) with 
	  Env -> false | Thread _ -> true) outgoing
	  



  end


    (**********************************************************************************************)
    (**********************************************************************************************)
    (* End of The environment automaton model in the TAR algorithm *)
    (**********************************************************************************************)
    (**********************************************************************************************)




    (*******************************************************************************************)
    (* for m4 *)
    (* GUI functions *)
    include(abs-gui.ml)
    (* CONCRETE PRE/POST CONDITIONS + lvalue_maps *)
    include(abs-wpsp.ml)
    (* ABSTRACT POST *)
    include(abs-adp.ml)
    (*******************************************************************************************)			  
    (* Starting the definition of PRE *)
  
  let setWPCapRegion reg =
    let conc_reg = Region.concretize reg in
      wpCapRegion :=
      match conc_reg with 
	  Region.Empty -> P.False
	| Region.Atomic atomic_reg -> 
	    begin
	      match atomic_reg.Region.data with
		  Region.Concrete foo -> foo.Region.pred
		| _ -> failwith ("bad call to setWPCapRegion")
	    end
	| Region.ParallelAtomic p_atomic_reg ->
	    begin
	      match p_atomic_reg.Region.p_data with
		  Region.Concrete foo -> foo.Region.pred
		| _ -> failwith ("bad call to setWPCapRegion")
	    end
	| _ -> failwith ("bad call to setWPCapRegion: Strange region type")

  let resetWPCapRegion () = 
    wpCapRegion := P.True

  (* the symbolic pre function: (approximate) weakest pre-condition *)
  let pre_template subs_flag sat_flag reg op phi_table =
    M.msg_string M.Debug "In pre -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg ;
    M.msg_printer M.Debug Operation.print op ;
    M.msg_string M.Debug (if subs_flag then "Substitute \n" else "Explicit (no substitute) \n");
    M.msg_string M.Debug (if sat_flag then "sat-filter-on \n" else "sat-filter-off \n");
    match reg with
	Region.Empty ->
	  Region.bot
      | Region.Atomic atomic_reg ->
	  if (((C_SD.location_coords atomic_reg.Region.location) <> C_SD.location_coords (C_SD.get_target op)) 
		&& (not (C_SD.isReturn op)) && (not (C_SD.isFunCall op))) then
	    (* Jhala: This is only if this is not a return op *)
	    begin
	      M.msg_string M.Error "WARNING: pre: blast internal error suspected" ;
	      M.msg_string M.Error "              operation do not match this region's location" ;
	      failwith "location mismatch";
	      Region.bot
	    end
	  else
	    begin
	      let new_stack =
		if (C_SD.isFunCall op) then
		  begin 
		    match (C_SD.get_command op).Cmd.code with
			Cmd.FunctionCall fcexp -> 
			  let function_is_defined fcexp = 
			    match fcexp with
				E.FunctionCall(E.Lval (E.Symbol(fname)), _) -> 
				  C_SD.is_defined fname
			      | E.Assignment (_, _, E.FunctionCall (E.Lval (E.Symbol fname), _)) -> 
				  C_SD.is_defined fname
			      | _ -> 
				  (* Rupak: this case happens if there is a function pointer call.
				     Previously we failed here with:
				     failwith ("Bad function call :"^(E.toString fcexp))
				     However we can punt this by assuming that the function pointer call
				     does not call a known function, and continue.
				     Do this only if nofp is set.
				  *)
				  if (Options.getValueOfBool "nofp") then
				    false
				  else
				    failwith ("pre_template: Bad function call (Possible function pointer?):"^(E.toString fcexp))
			  in
			    if (not (function_is_defined fcexp)) then
			      atomic_reg.Region.stack
			    else 
			      begin
				let (_fname,_,target) = C_SD.deconstructFunCall fcexp in
				if (_fname = PredTable.__NotImplementedFunctionName || C_SD.is_noexpand _fname) 
				then 
				  atomic_reg.Region.stack
				else   
				  if (C_SD.is_defined _fname ) then 
				    try (Region.CallStack (List.tl (Region.list_of_stack atomic_reg.Region.stack))) 
				    with e -> (print_string (Printexc.to_string e);failwith "Incorrect callstack type !!")
				  else 
				    atomic_reg.Region.stack (* was a skip *)
			      end
		    |    _ -> failwith "This can never happen: isFuncall checked that this was a function call!"
		  end
		else
		  if (C_SD.isReturn op)
		  then
		    begin
		      M.msg_string M.Debug "Pre: Hit isReturn\n";
		      (* recall that we put the call points in the stack *)
		      (* we assume that a function call edge is the only edge
			 going to its target location *)
		      M.msg_string M.Debug "Ingoing edges : ";
		      List.iter (fun a -> M.msg_string M.Debug (Operation.to_string a)) (C_SD.get_ingoing_edges atomic_reg.Region.location);
		      M.msg_string M.Debug "\nNow matching...\n";
                      let callLoc = 
			(C_SD.get_source (Misc.list_filter_unique C_SD.isFunCall (C_SD.get_ingoing_edges atomic_reg.Region.location))) in
			last_loc_pointer := Some(callLoc);
		      let (rt,_,retloc ,subs) = lastTarget_loc (Some(C_SD.get_source op)) in
		      Absutil.set_ret_pointers rt subs (C_SD.get_location_fname retloc);
		      last_loc_pointer := None;
		      ( Region.CallStack (callLoc::(Region.list_of_stack atomic_reg.Region.stack)))
		    end
		  else
		    atomic_reg.Region.stack
	      in
	      let new_location = (C_SD.get_source op)
	      and new_data =
		match atomic_reg.Region.data with
		  Region.Abstract abst_data_reg ->
		    Region.Abstract (abstract_data_pre abst_data_reg (C_SD.get_command op) op atomic_reg.Region.location phi_table)
		| Region.Concrete conc_data_reg ->
		    Region.Concrete (concrete_data_pre subs_flag sat_flag conc_data_reg (C_SD.get_command op) (op) phi_table)
	      in
	      Absutil.reset_ret_pointers (); 
	      Region.Atomic { Region.location = new_location ;
			      Region.data = new_data ;
			      Region.stack = new_stack ; }
		
	    end
      |	Region.ParallelAtomic p_atomic_reg ->
	  Region.bot
	    (* i am here *)
      | Region.Union _ ->
	  failwith "pre: argument is a union: unimplemented"
      | Region.ParallelUnion _ ->
	  failwith "pre: argument is a parallel union: unimplemented"


  (* Jhala: am setting sat flag to false for now ... *)
  let pre (* reg op phi_table *) = pre_template true false (* reg op phi_table *)
  let spre (* reg op phi_table *) = pre_template false false (* reg op phi_table *)


  let abstract_data_post abst_data_reg  cmd edge post_location phi_table =
    (abstract_data_post_graf_saidi abst_data_reg  cmd) edge post_location phi_table 
      
      
  let abstract_predicate pred pred_list = 
    M.msg_string M.Debug "abstract_predicate called" ;
    assume_pred_ref := Some(pred);
    let abstract_bdd = postBdd bddOne (List.map (function i -> (i,getPred i,P.negate (getPred i),true)) pred_list) [] 
    in
      assume_pred_ref := None;
      Region.Abstract { Region.lelement = DataLattice.top (* TBD: call DataLattice.abstract ... *) ; 
			Region.bdd = abstract_bdd; Region.preds = pred_list}

  let scope_kill data_region fname = 
    M.msg_string M.Debug "Inside scope_kill:\n";
    match data_region with
	Region.Abstract(abst_data_reg) ->
	  begin
	    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg;
	    let _kill_ff x =  
	      let predVars = Absutil.getVarExps_deep (PredTable.getPred x) in
		List.exists (fun x -> Misc.is_substring x ("@"^fname)) predVars
	    in
	    let new_bdd = Absutil.hg_exists _kill_ff abst_data_reg.Region.bdd in
	    let ret_abst_data_reg  = { Region.bdd = new_bdd; 
	                               Region.lelement = abst_data_reg.Region.lelement;
                                       Region.preds = abst_data_reg.Region.preds} in
	      M.msg_string M.Debug "Returning from kill_data_region with :\n";
	      M.msg_printer M.Debug Region.print_abstract_data_region ret_abst_data_reg;
	      Region.Abstract(ret_abst_data_reg)
	  end
      | _ -> 
	  begin
	    M.msg_string M.Debug "bad call to kill"; 
	    data_region
	  end


  (*************************************************************************************)
  (* Automaton post : 
     1. dotted post along Env edges
     2. solid post along Thread edges
  *)
  (*************************************************************************************)

  (* check if the region contains some stuck state *)
  let exists_stuck reg =
    match reg with
      | Region.Empty -> false
      | Region.ParallelAtomic p_at ->
	  let ctrmap = p_at.Region.e_control in
	    not (Counter.eq ctrmap.( (* STUCK STATE *) 0) (Counter.make_counter 0)) && not (Region.is_empty reg)
      | Region.ParallelUnion punion ->
	  Region.AtomicRegionTable.fold
	    (fun (l,s) datalist flag -> flag || (List.exists (fun (ec,_) -> not (Counter.eq ec.((* stuck state *)0) (Counter.make_counter 0))) datalist)) 
	    punion false
      | _ -> failwith "exists_stuck is only used with (unions of) ParallelAtomic regions"

  let cluster_ctrmaps lmaps = (* cluster ctrmaps clusters the maps according to nonzero entries in the map,
                                 and gives back one map for each cluster which is the maximum pointwise map of all maps in the cluster *)
    let _ = M.msg_string M.Normal ("In cluster with: "^(string_of_int (List.length lmaps))^" : maps" ) in
      
    let get_nonzero_indices ctrmap =
      let indices = ref [] in for i = 0 to Array.length ctrmap -1 do if not (Counter.is_zero ctrmap.(i)) then indices := i :: !indices done ; !indices in
    let cluster_ht = Hashtbl.create 31 in (* make a hash table for the clusters *)
      List.iter (fun (thismap, _) -> 
		   let poss = get_nonzero_indices thismap in
		     if Hashtbl.mem cluster_ht poss then Hashtbl.replace cluster_ht poss 
		       (Counter.array_max (Hashtbl.find cluster_ht poss) thismap) 
		     else Hashtbl.add cluster_ht poss thismap) lmaps ;
      let rv = Hashtbl.fold (fun a b thislist -> (b,[])::thislist) cluster_ht [] in
      let _ = M.msg_string M.Normal ("Leaving cluster with: "^(string_of_int (List.length rv))^" : maps" ) in
	rv
	  


  (*  This part of the post deals with the updates to the counters as the automaton
      moves along dotted edges.
      
  *)

  let envPost automaton ctrmap ( adr : Region.abstract_data_region ) =
    (* for each location, get the dotted successors to locations 
       that contain adr 
    *)
    let allzeros ctrmap =
      let len = Array.length ctrmap in let i = ref 0 in let flag = ref false in
	while (not !flag) && !i < len do (if ctrmap.(!i) = Counter.make_counter 0 then () else flag := true); incr i done ; not !flag 
    in
    let numnodes = automaton.EnvAutomaton.number_of_states in
    let update_array = Array.make numnodes [] in
      if (allzeros ctrmap) then [ (ctrmap, []) ] else
	begin
	  M.msg_string M.Debug "In envPost: generating possible updates" ;
	  (* first generate an array of possible updates *)
	  for i = 0 to ((Array.length ctrmap) - 1) do 
	    if (ctrmap.(i) = (Counter.make_counter 0)) then 
	      ()
		
	    else (* three cases: ctrmap.(i) is 1 or 2 or omega *)
	      begin
		M.msg_string M.Debug ("Location "^(string_of_int i)^" is not empty");
		let ith_node = EnvAutomaton.getNodeFromId automaton i in
		let dotted_succs = List.filter (fun e -> match (BiDirectionalLabeledGraph.get_edge_label e) with
						    EnvAutomaton.Env -> true | _ -> false) (BiDirectionalLabeledGraph.get_out_edges ith_node)
		in
		let dotted_succ_nodes_containing_adr = (* myself, if I intersect adr *)
		  (if not (Region.adr_is_empty (Region.adr_cap adr (EnvAutomaton.getAdrFromId automaton i))) then [ ith_node ] else [])
		  @
 		  (* together with all my successors (except myself) that intersect adr *)
		  (List.map (BiDirectionalLabeledGraph.get_target) 
		     (List.filter 
			(fun e -> let target = BiDirectionalLabeledGraph.get_target e in
			   (EnvAutomaton.getIdFromNode automaton target <> i)&& not (Region.adr_is_empty (Region.adr_cap adr (EnvAutomaton.getAdrFromNode automaton target))) 
			) dotted_succs))
		in
		  List.iter (fun nd -> M.msg_string M.Debug ("Successor node : "^(string_of_int (EnvAutomaton.getIdFromNode automaton nd))) ) dotted_succ_nodes_containing_adr ; 
		  if dotted_succ_nodes_containing_adr = [] then
		    begin
                      (* stuck! *)
                      M.msg_string M.Debug "The automaton gets stuck. Do something!" ;
	              (* go to stuck, which is node 0 *)
		      update_array.(i) <- [(0 (* 0 is the stuck state *), ctrmap.(i), (i,0,ctrmap.(i)) )] :: update_array.(i)
		    end
		  else	  (* update maps *)
		    begin
	              M.msg_string M.Debug "In envPost: dotted succs not empty" ;	
                      if List.length dotted_succ_nodes_containing_adr = 1 then  (* this is the common case in the -dsq option *)
			update_array.(i) <- List.map (fun s -> let sid = EnvAutomaton.getIdFromNode automaton s in 
							[(sid, ctrmap.(i), (i,sid,(ctrmap.(i))) )]) dotted_succ_nodes_containing_adr
		      else
			if (Counter.eq ctrmap.(i) (Counter.make_counter 1)) then
			  (* the next states are where this counter goes to one of the next states *)
			  update_array.(i) <- List.map (fun s -> let sid = EnvAutomaton.getIdFromNode automaton s in 
							  [(sid, Counter.make_counter 1, (i,sid,(Counter.make_counter 1)) )]) dotted_succ_nodes_containing_adr
			else if (Counter.eq ctrmap.(i) (Counter.make_counter 2)) then
			  (* the next states are (1) both counters go to one state, or (2) one counter each goes to two states *)
			  begin
			    let pairs = Misc.cartesianProduct [ dotted_succ_nodes_containing_adr ; dotted_succ_nodes_containing_adr ] in
			      update_array.(i) <- List.map 
				(fun s -> let sids = List.map (EnvAutomaton.getIdFromNode automaton) s in 
				   match sids with [a;b] -> if a = b then [(a,Counter.make_counter 2, (i,a,(Counter.make_counter 2)))]
				   else 
				     [(a, Counter.make_counter 1, (i,a,(Counter.make_counter 1)) ) ; 
				      (b, Counter.make_counter 1, (i,b,(Counter.make_counter 1)) )  ]) pairs 
			  end
			else (* ctrmap.(i) = omega *)
			  (* the next states are where the omega many counters go to some next states *)
			  (* we approximate by putting omega fellows at each successor *)
			  begin
			    M.msg_string M.Debug "In envPost: case omega " ;	
			    let is_consistent ctrlist =
			      let r = List.fold_left (fun sofar (s, _, _) -> Region.adr_cap (EnvAutomaton.getAdrFromId automaton s) sofar) Region.true_adr ctrlist in
				not (Region.adr_is_empty (r)) in
			      if (List.length dotted_succ_nodes_containing_adr > 2) then failwith "envPost: omega and >2 succ"  else  ();
			      let all_omegas = 
				List.map (fun s -> let sid = EnvAutomaton.getIdFromNode automaton s in
					    (sid, Counter.omega (), (i,sid, Counter.omega ()) )) dotted_succ_nodes_containing_adr in 
				if is_consistent all_omegas then
				  update_array.(i) <- [ all_omegas ]
				else begin M.msg_string M.Debug "all omegas inconsistent" ; 
				  update_array.(i) <- List.map (fun s -> let sid = EnvAutomaton.getIdFromNode automaton s in 
								  [(sid, Counter.omega (), (i,sid,(Counter.omega() )) )]) dotted_succ_nodes_containing_adr
				end
			  end
		    end
	      end
	  done ;
	  let reduce_ctrmaps_by_dropping_inconsistent_maps ctrmap_list = 
	    List.filter (fun (ctrmap,_) ->
			   M.msg_string M.Debug "In drop_inconsistent. Ctrmap is" ;
			   for i = 0 to Array.length ctrmap - 1 do 
			     if not (Counter.is_zero ctrmap.(i)) then 
			       Printf.printf "ctrmap.(%d) = %s " i (Counter.toString ctrmap.(i)) 
			   done; 
			   Printf.printf "\n";
			   M.msg_string M.Debug ("Region is "^(Region.abstract_data_region_to_string adr));
			   let reg = ref adr in
			     for i = 1 to Array.length ctrmap - 1 do 
			       if (Counter.eq ctrmap.(i) (Counter.make_counter 0)) then () else
				 reg := Region.adr_cap (EnvAutomaton.getAdrFromId automaton i) !reg 
			     done ;
			     let a = not (Region.adr_is_empty !reg) in (*if a then Printf.printf "Not Dropping this one!\n" else () ;*) a) ctrmap_list
	  in
	    (* then do a Cartesian product of the update array to get all possible updated maps *)
	  let cp = List.map List.flatten (Misc.cartesianProduct (List.filter (fun s -> not (s = [])) (Array.to_list update_array))) in
	    (* each element in cp is a (counter map, list of transitions), where a transition (i,j,n) says n fellows went from i to j *)
	  let ctrmap_list : (Counter.counter array * ((int*int*Counter.counter) list)) list = List.map (fun m -> 
													  let a = Array.make numnodes (Counter.make_counter 0) in let who_goes_where = ref [] in
													  let rec _work lst =
													    match lst with [] -> (a, !who_goes_where)
													      | (s,ctr, (i,t,n) )::rest -> a.(s) <- Counter.plus a.(s) ctr ; if (i<>t) then (who_goes_where := (i,t,n):: !who_goes_where) ; _work rest  in
													    _work m) cp
	  in
	  let no_inconsistent = (reduce_ctrmaps_by_dropping_inconsistent_maps  ctrmap_list) in
            (* if the model checker is run with omega counters then cluster and drop the who went where info *)
	  let exists_omega (cmap,_) = let flag = ref true in let i = ref 0 in 
            while (!flag && !i< Array.length cmap) do if (Counter.is_omega cmap.(!i)) then flag := false else () ; incr i done ; not !flag in
	    if (exists_omega (List.hd no_inconsistent)) then
	      cluster_ctrmaps no_inconsistent else no_inconsistent 
	end

  let havoc_post datareg lvallist postloc =
    let foo_ht = Hashtbl.create 3 in (* fake hash table *)
    let op = C_SD.fake_edge  in
      List.fold_left (fun reg lval -> 
			let havoc_cmd = { Cmd.read = [] ; Cmd.written = [] ; Cmd.code = Cmd.Havoc lval } in
			  abstract_data_post reg havoc_cmd op postloc foo_ht)  datareg lvallist
	

  (* Solid post for an automaton *)
  let threadPost automaton ctrmap i j datareg postloc =
    (* automaton moves from location i to location j along a solid edge *)
    let inode = BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i) and
      jnode = BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton j) in
      
      (* sanity checks *)
      if (i >= automaton.EnvAutomaton.number_of_states || j >= automaton.EnvAutomaton.number_of_states) then failwith "Out of bound in threadpost" ;
      if (Counter.is_zero ctrmap.(i)) then failwith "Error in threadpost 1" ;
      let ijedges = List.filter (fun e -> (BiDirectionalLabeledGraph.get_node_label (BiDirectionalLabeledGraph.get_target e)).EnvAutomaton.id = j ) 
		      (EnvAutomaton.getThreadEdges automaton i) in
	if ijedges = [] then       
	  failwith "no edge between i and j" ;
	(* it may prove a burden to separate these edges later ... *)
	let edge_havocs = 
	  List.fold_left 
	    (fun sofar thisedge -> 
	       match (BiDirectionalLabeledGraph.get_edge_label thisedge) with 
		   EnvAutomaton.Thread lvl -> Misc.union lvl sofar) [] ijedges 
	in
	  if (Region.adr_is_empty 
		(Region.adr_cap datareg (inode).EnvAutomaton.adr)) 
	  then
	    failwith "region at i does not intersect datareg " ;
	  let _get_modifies automaton ctrmap =
	    let lvlist = ref [] in
	      for i = 0 to Array.length ctrmap -1 do
		if (not (Counter.eq ctrmap.(i) (Counter.make_counter 0))) then
		  lvlist := Misc.union (EnvAutomaton.getModifiesFromId automaton i) !lvlist
	      done;
	      !lvlist
	  in

	  (* ok now do the update *)
	  (* we havoc all variables that can be modified by the environment by looking at the modifies lists of all populated states *)
	  let havoced_data = havoc_post datareg (Misc.union edge_havocs (_get_modifies automaton ctrmap)) postloc in
	  let newdatareg = Region.adr_cap havoced_data jnode.EnvAutomaton.adr in 
	    (*
	      M.msg_string M.Normal ("Havoced post is " );
	      M.msg_string M.Normal (Region.abstract_data_region_to_string havoced_data) ;
	      M.msg_string M.Normal ("new data reg is " );
	      M.msg_string M.Normal (Region.abstract_data_region_to_string newdatareg) ;
	    *)
	    (* update ctrmap by moving one guy from i to j *)
	    ctrmap.(i) <- Counter.minus ctrmap.(i) (Counter.make_counter 1) ;
	    (* now do dotted post
	    *)
	    let updctrmaps : (Counter.counter array * ((int*int*Counter.counter) list)) list = if Region.adr_leq newdatareg Region.false_adr then 
              [ Array.copy ctrmap,[] ]
            else envPost automaton ctrmap newdatareg(* jnode.EnvAutomaton.adr *) in
	      (*
		Note that envPost must be done after i has been incremented, but before
		j has been incremented, in order not to move the guy
		who just moved to j
	      *)
	      (* finally put the guy back in j *)
	      List.map (fun (updctrmap, who_goes_where) -> updctrmap.(j) <- Counter.plus updctrmap.(j) (Counter.make_counter 1)) updctrmaps ;
	      (newdatareg, updctrmaps)

  (*************************************************************************************)
  (* Done Automaton post :                                                             *)
  (*************************************************************************************)

  exception RecursionException
  exception StackOverflowException of string
    
  (* the symbolic post function: (approximate) strongest post-condition *)
  (* TODO: if checking races, add havocs *)
  let post reg op phi_table =
    M.msg_string M.Debug "In post -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg ;
    M.msg_printer M.Debug Operation.print op ;
    Hashtbl.replace statVisitedFunctionTable (C_SD.get_location_fname (C_SD.get_target op)) true  ;
    let post_atomic_reg atomic_reg = 
      try
	if not (Region.check_locations_eq atomic_reg.Region.location (C_SD.get_source op)) then
            failwith "ERROR: post: blast internal error"
	else
	  begin
            try 
	      let callStack = Region.list_of_stack atomic_reg.Region.stack in
	      let tos_fname = 
		match callStack with
		| [] -> (last_loc_pointer := None; C_SD.__BLAST_DummyFunctionName)
		| _  -> (last_loc_pointer := Some (List.hd callStack);C_SD.get_location_fname (C_SD.get_target op)) 
	      in
	      let tos_2_fname = 
		if (List.length callStack >= 1) then (C_SD.get_location_fname (List.hd callStack)) else C_SD.__BLAST_DummyFunctionName 
	      in
	      let _ = M.msg_string M.Debug (Printf.sprintf "tos_fname: %s, tos_2_fname: %s" tos_fname tos_2_fname) in
		(* So last_loc_pointer points to the top of the stack at all times or to errorloc Rif the stack is empty *)
	      let (new_location, new_stack) = 
                if (C_SD.isFunCall op) then
                  begin
                    match (C_SD.get_command op).Cmd.code with
			(* GuardedFunctionCall is a function pointer call disambiguated via alias analysis.  We only take its target location here, and the condition it's followed under (address equality) is handled further *)
			Cmd.GuardedFunctionCall (_, fcexp) |
                        Cmd.FunctionCall fcexp -> 
                          let (_fname,_,target) = C_SD.deconstructFunCall fcexp in
	                  let _ = stackInfo := Some (_fname,tos_fname,true) in
                            if not (C_SD.enter_call op) then (C_SD.get_target op, atomic_reg.Region.stack)
                            else   
			      begin
                                assert (C_SD.is_defined _fname);
				  begin
                                    let number_of_occurrences s stk =
                                      let rec _worker s lst n =
                                      match lst with [] -> n
                                      | a :: rest ->
                                        _worker s rest 
                                          (if compare s a = 0 then (n+1) else n)
                                      in
                                      _worker s stk 0
                                    in
				    let src = C_SD.get_source op in
				      (* Treat call as skip if preset maximal recursion is reached *)
				      if number_of_occurrences src callStack > 2 then 
                                        raise RecursionException
				      (* Treat call as skip if preset maximal stack depth is reached *)
				      else
					let fdepth_reached = ( O.getValueOfInt "fdepth" > 0 ) && (List.length callStack >= O.getValueOfInt "fdepth" ) in
					(* This ugly, old fdepth condition will be removed in the future releases *)
					let old_fdepth_condition = (String.length(O.getValueOfString "ignoredepthfor") > 0 ) && (String.length _fname >= String.length(O.getValueOfString "ignoredepthfor")) && (String.sub _fname 0 (String.length(O.getValueOfString "ignoredepthfor")) = O.getValueOfString "ignoredepthfor") in
					(* Skip not important functions on fdepth violation *)
					if fdepth_reached && not (old_fdepth_condition || (C_SD.is_important _fname))
					then raise (StackOverflowException _fname)
					(* Ok, normal function call.  Proceed. *)
					else begin
					  if (List.length callStack = !maxStackLen) then 
                                            (maxStackLen := !maxStackLen + 1;);
				          let newStack = src:: callStack in
				            ((C_SD.lookup_entry_location _fname),
				             Region.CallStack newStack)
					end
				  end
                                (* else 
                                  begin
				    M.msg_string M.Normal ("WARNING: Undeclared Important Function"^_fname);
				    ((C_SD.get_target op),atomic_reg.Region.stack)
                                  end *)
			      end
                      | _ -> failwith "what the (post) !"
			  
                  end
                else 
                  try 
		    if (C_SD.isReturn op) then 
		      begin
			let _ = stackInfo := Some (tos_fname,tos_2_fname,true) in
			let (ret,_,nextLoc,subs) = lastTarget_loc ~real_fn_o:(Some tos_fname) (Some (C_SD.get_source op)) in 
			Absutil.set_ret_pointers ret subs (C_SD.get_location_fname nextLoc);
			let newStack = List.tl callStack in
			  (nextLoc,(Region.CallStack newStack)) 
		      end
		    else 
		      begin
			let _ = stackInfo := Some (tos_fname,tos_2_fname,false) in
			  (C_SD.get_target op,atomic_reg.Region.stack) 
		      end
                  with Failure ("tl") -> 
		    begin
		      let _ = stackInfo := Some (tos_fname,tos_2_fname,false) in
			(C_SD.get_target op, atomic_reg.Region.stack ) 
		    end
	      in 
	      (* ok, we have now location C_SD.get_target op and stack stack *)
	      let get_new_data () () =
		let _new_data =
		  try 
		    match atomic_reg.Region.data with
			Region.Abstract abst_data_reg ->
			  begin
			  M.msg_string M.Debug "SKY start abstract_data_post";
			  let yyy=
			  Stats.time "abstract_data_post" (abstract_data_post abst_data_reg (C_SD.get_command op) op new_location) phi_table
			  in begin 
			  M.msg_string M.Debug "SKY end abstract_data_post";
			  Region.Abstract yyy
			  end end
		      | Region.Concrete conc_data_reg ->
			  Region.Concrete 
			    (Stats.time "concrete_data_post" (concrete_data_post conc_data_reg (C_SD.get_command op) op) phi_table)
		  with C_SD.NoSuchFunctionException f -> failwith "Foo"
		in
		  (* Adding scoping hack *)
		  (* Adding kill *)
		  match ((Options.getValueOfBool "scope") && (C_SD.isReturn op) (*&& false*) ) with
		      true  -> 
			begin
			  let (_,arg,_,_) = lastTarget_loc (Some (C_SD.get_source op)) in
			    Stats.time "scope_kill" (scope_kill _new_data) arg
			end
		    | false -> _new_data
	      in
                (* set retTarget back to junk *)
	      let new_data = Stats.time "get_new_data" (get_new_data ()) () in
	      let _ = Absutil.reset_ret_pointers () in
		begin
		  Region.Atomic { Region.location = new_location ;
				  Region.data = new_data ;
				  Region.stack = new_stack ; }
		end
 	    with
		Failure("Empty Call Stack") -> Region.bot
	  end
      with 
          C_SD.NoSuchFunctionException f -> (* treat it as a skip *)
            M.msg_string M.Minor ("Unexpected NoSuchFunctionException "^f);
            (* THIS SHOULD BE COMMENTED OUT IF UNDEFINED FUNCTIONS ARE TO BE AVOIDED *)
            (* failwith ("Unexpected NoSuchFunctionException "^f); *)
            Region.Atomic
              { Region.location = C_SD.get_target op;
                Region.data = atomic_reg.Region.data;
                Region.stack = atomic_reg.Region.stack;
	      } 
              (* COMMENT OUT UPTO HERE *)
        | StackOverflowException f -> (* treat it as a skip *)
            M.msg_string M.Normal ("Skipping call to "^f^" due to stack overflow");
            Region.Atomic
              { Region.location = C_SD.get_target op;
                Region.data = atomic_reg.Region.data;
                Region.stack = atomic_reg.Region.stack;
	      }
	      
        | RecursionException ->
            if (Options.getValueOfBool "enablerecursion") then
              begin
                (* treat this as a skip *)
		M.msg_string M.Normal "WARNING: Skipping recursive function call";
		M.msg_string M.Normal (Operation.to_string op) ; 
		M.msg_string M.Normal "For region " ;
		M.msg_printer M.Normal Region.print reg ; 

                Region.Atomic {
                  Region.location = C_SD.get_target op ;
                  Region.data = atomic_reg.Region.data ;
                  Region.stack = atomic_reg.Region.stack ;
                }
              end
            else raise RecursionException
    in
      (* M.msg_string M.Normal "checking region type"; *)
      match reg with	
	  Region.Empty -> Region.bot
	| Region.Atomic atr -> post_atomic_reg atr
        | Region.AtomicTI atrTI ->
            begin
              let prev_loc = atrTI.Region.ti_location_prev in
              let atr = 
                {Region.location = atrTI.Region.ti_location;
                 Region.data = atrTI.Region.ti_data;
                 Region.stack = atrTI.Region.ti_stack}
              in 
		match (post_atomic_reg atr) with
                    Region.Atomic atr' ->
                      Region.AtomicTI
                      {Region.ti_location_prev = prev_loc;
                       Region.ti_location = atr'.Region.location;
                       Region.ti_stack = atr'.Region.stack;
                       Region.ti_data = atr'.Region.data}
		  | Region.Empty -> Region.Empty
		  | _ -> failwith "bad return from post_atomic_reg" 
            end
	| Region.ParallelAtomic p_atomic_reg ->
            failwith "post: argument is parallel atomic: unimplemented"
	| Region.Union _ ->
            failwith "post: argument is a union: unimplemented"
	| Region.ParallelUnion _ ->
            failwith "post: argument is a parallel union: unimplemented"







  
  (** Tar post : post for automaton as well as thread *)
  let tarpost reg op automaton =
    (* JHALA: adding this function to maintain the invariant mentioned below *)

    let ctrmap_conjoin adr ctrmap =
      let conj_bdd = ref adr.Region.bdd in
      let conj_i i i_ctr =
	if Counter.is_zero i_ctr then ()
	else conj_bdd := 
	  CaddieBdd.bddAnd !conj_bdd
	    (( EnvAutomaton.getAdrFromId automaton i).Region.bdd)
      in
	Array.iteri conj_i ctrmap;
	{ Region.bdd = !conj_bdd;
          Region.lelement = DataLattice.top ;
	  Region.preds = []
	}
    in
      (* RJ: END *)
    let phi_table = Hashtbl.create 1 in
      M.msg_string M.Debug "In tarpost ; region is" ;
      M.msg_printer M.Debug Region.print reg ;
      (match op with 
	   ProgramOperation opn->
	     M.msg_printer M.Debug Operation.print opn ;
	     Hashtbl.replace statVisitedFunctionTable (C_SD.get_location_fname (C_SD.get_target opn)) true  
	 | AutomatonOperation (i,j) ->
	     M.msg_string M.Debug (Printf.sprintf "Automaton movement (%d, %d)" i j);
      )	 ;     
      match op with
	  ProgramOperation pmove ->
	    begin
              match reg with
		  Region.Empty -> [ (Region.Empty, []) ]
		| Region.ParallelAtomic p_atomic_reg ->
		    begin
		      try
			let newreg = Region.Atomic { Region.location = p_atomic_reg.Region.p_control.Region.p_location ;
						     Region.stack = p_atomic_reg.Region.p_control.Region.p_stack ;
						     Region.data = p_atomic_reg.Region.p_data ;
						   } in
			let nextreg = post newreg pmove phi_table in (* do the program move *)
			let datareg = (extract_abs_data_reg nextreg) in 
			  (* Notice that datareg is over locals and globals, and the automaton adr is over globals.
			     But we do not need to explicitly quantify out the locals from datareg since
			     we shall simply intersect with the adr and check emptiness.
			     This uses the following theorem:
			     R_1(\vec{x},\vec{y}) \cap R_2(\vec{x}) \not = \emptyset 
			     iff
			     \exists \vec{y}. R_1(\vec{x},\vec{y}) \cap R_2(\vec{x}) \not = \emptyset
			  *)
			let newctrmaps = if Region.adr_leq datareg Region.false_adr then [ (Array.copy p_atomic_reg.Region.e_control,[])]
			else envPost automaton (Array.copy p_atomic_reg.Region.e_control) datareg in (* update counter map *)
			  (*
			    M.msg_string M.Debug "Returned from envPost in tarpost (program move); printing new maps ";
			    List.iter (fun (nm, _) -> for i = 0 to Array.length nm -1 do 
			    M.msg_string M.Debug (Printf.sprintf "(%d, %s)" i (Counter.toString nm.(i))) done ) newctrmaps; 
			    M.msg_string M.Debug "Done printing envmaps" ;
			  *)
			  (* RJ: CRUCIAL INVARIANT !
			     in any region r in the reachability tree: "adr.r <= auto.i" if ctrmap.r.i <> 0 
			     we must conjoin the env regions which are afterall invariants as it were,
			     with the datareg -- it is the state of the system that the envautomaton is 
			     guaranteeing... the feasibility of newctrmap ensures the conjunction is satisfiable
			  *)
			let cmc = ctrmap_conjoin datareg in
			  List.map 
			    (fun (newctrmap,who_goes_where) ->
			       let (loc, stk) = Region.getPcStack nextreg in
			       let new_control = { Region.p_location = loc ;  
						   Region.p_stack = stk ; } in
			       let new_adr = cmc newctrmap in
		       		 (Region.ParallelAtomic 
				    { Region.p_control = new_control; 
				      Region.e_control =  newctrmap;
				      Region.p_data = Region.Abstract new_adr;}
				    , who_goes_where))
			    newctrmaps 
		      with Region.Empty_region -> [ (Region.bot, []) ]
		    end
		| _ -> failwith "tarpost expects a parallel atomic region"
            end
	| AutomatonOperation (i,j) ->
	    begin
	      match reg with
		  Region.Empty -> [ (Region.Empty, []) ]
		| Region.ParallelAtomic p_atomic_reg ->
		    (* update the automaton *)
		    (* recall that threadPost updates the dotted transitions of the automaton as well *)
		    let (postloc,_) = Region.getPcStack reg in
		    let (newdatareg, newctrmaps) = threadPost automaton (Array.copy p_atomic_reg.Region.e_control) i j 
						     (extract_abs_data_reg reg ) postloc in
		    let cmc = ctrmap_conjoin newdatareg in
		      List.map (fun (newctrmap,who_goes_where) -> (Region.ParallelAtomic 
								     { Region.p_control =  p_atomic_reg.Region.p_control;
								       Region.e_control =  newctrmap;
								       Region.p_data = Region.Abstract (cmc newctrmap); 
								       (* RJ: doing the env constraining here as well *)
								     } , who_goes_where) ) newctrmaps
		| Region.Atomic ar -> (* basically compute a thread post without the environment moves *)
		    let (ind, jnd) = (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i),
				      BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton j)) 
		    in
		      if (i >= automaton.EnvAutomaton.number_of_states || j >= automaton.EnvAutomaton.number_of_states) then failwith "Out of bound in tarpost" ;
		      let ijedges = List.filter (fun e -> (BiDirectionalLabeledGraph.get_node_label (BiDirectionalLabeledGraph.get_target e)).EnvAutomaton.id = j ) 
				      (EnvAutomaton.getThreadEdges automaton i) in
			if ijedges = [] then       
			  failwith "no edge between i and j" ;
			(* it may prove a burden to separate these edges later ... *)
			let all_havocs = 
			  Misc.union (EnvAutomaton.getModifiesFromId automaton i)
			    (List.fold_left 
			       (fun sofar thisedge -> 
				  match (BiDirectionalLabeledGraph.get_edge_label thisedge) with 
				      EnvAutomaton.Thread lvl -> Misc.union lvl sofar) [] ijedges) 
			in
			let datareg = extract_abs_data_reg reg in
			let datareg' = (Region.adr_cap datareg (ind).EnvAutomaton.adr) in
			  if (Region.adr_is_empty datareg') then ([ (Region.bot, []) ]) 
			  else
			    begin
	      		      (* ok now do the update *)
			      (* we havoc all variables that can be modified by the environment 
				 by looking at the modifies lists of all populated states *)
			      let havoced_data = havoc_post datareg' (all_havocs) ar.Region.location in
			      let newdatareg = Region.adr_cap havoced_data jnd.EnvAutomaton.adr in 
			      [ ( Region.Atomic { Region.location = ar.Region.location ; 
						  Region.stack = ar.Region.stack ; 
						  Region.data = Region.Abstract newdatareg; }, 
				 []) ]
			    end
		| _ -> failwith "tarpost expects parallel atomic region"
	    end
	      

  (***************************************************************************************************)



  let statsMaxNumberOfPredsActive = ref 0
				      (* Keeps track of the number of predicates active
					 at any point, discounting scope
				      *)

  (* do_focus : get new predicates to add to the current set *)
  exception NoNewPredicatesException



  (* this is a highly stateful function! *)
  let last_no_of_preds = ref 0
  
  let predicate_fixpoint_reached () = 
    if (!last_no_of_preds = List.length !PredTable.tableOfPreds) then true
    else (last_no_of_preds := List.length !PredTable.tableOfPreds;false)


  let do_focus abst_data_reg empty_conc_data_reg =
    M.msg_string M.Debug "In do_focus -- arguments are:" ;
    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg ;
    M.msg_printer M.Debug Region.print_concrete_data_region empty_conc_data_reg ;
    let emptyPred = empty_conc_data_reg.Region.pred in
    let allPreds = try extractUsefulPredicates emptyPred with Failure "simplify" -> raise NoNewPredicatesException in
    let oldnum = getCurrentIndex () in
    let added_preds = List.map addPred allPreds in
    let newnum = getCurrentIndex () in
      if (oldnum = newnum) then
	begin
	  M.msg_string M.Debug "Restarting..." ;
	  Options.setValueOfBool "localrestart" true ;
	end
      else
	begin
	  M.msg_string M.Debug "At least one predicate added ..." ;
	  Options.setValueOfBool "localrestart" false ;
	end ;

      let newpredlist = Misc.compact (Misc.difference (added_preds) (-1 :: abst_data_reg.Region.preds)) 
			  (* This is not correct ... as there may be a descendant along the error path 
			     that does not have a certain pred that is in this list which the 
			     ancestor ie the current data region has... if you are doing PATH that is ...*)
      in
	if (newpredlist = []) then (M.msg_string M.Error "No new predicates ! Unexpected ...";
				    printTableOfPreds () ;
				    if (Options.getValueOfInt "predH" < 6) then
				      raise NoNewPredicatesException;
				   );
	begin  
	  (* RJ: leave the next line as NORMAL -- reqd. for demo mode in gui *)
	  (* let _ =  M.msg_string M.Normal ("NEW PREDICATES FOUND \n") in *)
	  let _listOfAllActivePreds = abst_data_reg.Region.preds @ newpredlist in
	    ignore (if (List.length _listOfAllActivePreds) > !statsMaxNumberOfPredsActive then 
		      begin
			statsMaxNumberOfPredsActive := List.length _listOfAllActivePreds;
			M.msg_string M.Debug ("Size of pred set: "^(string_of_int (List.length !PredTable.tableOfPreds)) );
			M.msg_string M.Debug "Active Predicates"; 
			List.iter (function i -> M.msg_string M.Debug  (P.toString (PredTable.getPred i)))  
			  _listOfAllActivePreds
		      end);
	    { Region.bdd = abst_data_reg.Region.bdd ;
              Region.lelement = abst_data_reg.Region.lelement ;
	      Region.preds = _listOfAllActivePreds ; }
	end


  let replace_data_region reg new_data_reg =
    match reg with 
	Region.Atomic _reg ->
	  Region.Atomic 
	  { Region.location = _reg.Region.location;
	    Region.data = new_data_reg;
	    Region.stack = _reg.Region.stack;
	  }
      | _ -> failwith ("bad call to replace_data_region")


  (* ******************* ERROR TRACE FILTER ****************************** *)

  (* the following procedure takes a trace and spits out that subtrace of it which is used to 
     get a contradiction -- it is to help in "manual" counterexample analysis *)

  let  trace_filter region_list op_list = 
    (* the above is the unsatisfiable list of regions and operations -- arranged in "past --> present" order*)
    let reg_list_r = List.rev region_list in
    let op_list_r = List.rev op_list in
    let process_op_reg (phi , map) (op, reg) = () in
      ()

  (* ******************* find_all_preds ********************************** *)
  (* this function will look at ALL subtraces of a given trace and find all predicates coming from it *)





  let quantify_and_extract_abs_data_reg reg =
    let _ = M.msg_string M.Debug "In qae:" in

    let quant_fn = fun i -> predicate_mentions_locals (getPred i) in
    let abst_data_reg = extract_abs_data_reg reg in
    let __l = ref [] in
    let _get_v a = 
      for i = 0 to Array.length a - 1 do if a.(i) = 2 then () else __l := i::!__l ; done in
    let _preds_in_bdd = let _ = CaddieBdd.bddForeachCube (CaddieBdd.bddSupport abst_data_reg.Region.bdd) (_get_v) in !__l in
    let _preds_to_quantify = List.filter quant_fn _preds_in_bdd in
    (* BDDFIX:let _varset = CaddieBdd.fromList bddManager _preds_to_quantify in 
    let new_bdd = CaddieBdd.exists _varset abst_data_reg.Region.bdd in *)
    let new_bdd = bdd_exist_quantify (abst_data_reg.Region.bdd) _preds_to_quantify in
      {Region.bdd = new_bdd;
       Region.lelement = abst_data_reg.Region.lelement ;
       Region.preds = []
      }


  let insert_data_reg reg dr = 
    match reg with
	Region.Atomic atomic_reg ->
	  begin
	    Region.Atomic 
	      { Region.data = dr;
		Region.location = atomic_reg.Region.location;
		Region.stack = atomic_reg.Region.stack;
	      }
	  end
      | _ -> failwith "bad call to insert_data_reg"


  (* this is called only in "block" mode ... and with noforget *)
  let focus_stamp reg =
    let abst_data_reg = extract_abs_data_reg reg in
    let cur = getCurrentIndex () in
    let present_prec = 
      try List.hd abst_data_reg.Region.preds 
      with _ -> -1 
    in
    let new_reg = 
      insert_data_reg reg 
	(Region.Abstract 
	   { Region.bdd = abst_data_reg.Region.bdd;
             Region.lelement = abst_data_reg.Region.lelement;
	     Region.preds = [cur];
	   })
    in
    if (present_prec = cur && not ((Options.getValueOfInt "craig" >= 2))) then
      begin
	dump_abs ();
	raise NoNewPredicatesException;
      end
    else new_reg




  (* Cheap hack: go through all the predicates along oplist and add them to the list of predicates. *)
  (* again oplist is in increasing order past --> present *)
  (* r1 -> o1 -> r2 -> o2 -> ... -> on -> rn *)

  let find_all_predicates_cheap oplist =
    let rec _more lst accum =
      match lst with
	  [] -> accum
	| a :: rest ->
            begin
              match (C_SD.get_command a).Cmd.code with
		  Cmd.Pred e ->
                    if e = P.True then _more rest accum
                    else 
                      begin
			let ee = match e with 
			  P.Not e' -> e' | P.Atom _ -> e 
			| _ -> failwith "nonatomic predicate on path!" 
			in
			_more rest (ee :: accum)
                      end
		| _ -> _more rest accum
            end
    in
    let all_preds_in_path = _more oplist [] in
    let newpredlist = Misc.compact 
			(Misc.difference 
                           (List.map addPred all_preds_in_path) 
                           (-1 :: ( PredTable.get_all_pred_indexes ()))) in 
    let number = List.length newpredlist in
      M.msg_string M.Debug ("find_all_predicates_cheap found "^
					(string_of_int number)^" predicates.") ;
      number


  let find_all_predicates reglist oplist = 
    let list_of_predicates = ref [] in
    let oplist_r = List.rev oplist in
    let reglist_r = List.rev reglist in

    let rec _proc_sub seed ol = 
      (* take wp of seed wrt (ol.head,rl.head) -- query for unsat if unsat take preds... quit and break out ..
	 else if sat, then take region and recur on it with ol.tail, rl.tail *)
      match ol with
	  op::t -> 
	    begin 
	      let emptytable = Hashtbl.create 2 in
	      let pre_reg = pre seed op emptytable in
		if (Region.is_empty pre_reg) then 
		  begin
		    let allpreds = 
		      try extractUsefulPredicates (extract_pred pre_reg) 
		      with Failure "simplify"-> raise NoNewPredicatesException 
		    in
		    M.msg_string M.Debug ("Learning new predicates: ");
		    List.iter (fun x -> list_of_predicates := x::(!list_of_predicates); 
		      M.msg_string M.Debug ((P.toString x)^" , ")) allpreds;
		    M.msg_string M.Debug ("From region: "^(P.toString (extract_pred pre_reg)))
			(* do we or do we not recur on pre_reg ? as of now, NO *)
		  end
		else 
		  _proc_sub pre_reg t
	    end
	| [] -> ()
    in

    let rec process_subtrace ol_r rl_r = 
      match rl_r with
	  h::h1::t ->
	    let seed_reg = (replace_data_region h (Region.Concrete {Region.pred = P.True})) in
	      _proc_sub seed_reg ol_r; 
	      process_subtrace (List.tl ol_r) (h1::t)
	| _ -> ()
    in
      (* this is the point where the main function is called ! *)
      (*  The stuff in this needs to be worked on ... its *very* slow ... 
	  let _ = process_subtrace oplist_r reglist_r in
	  let newpredlist = Misc.compact (Misc.difference (List.map addPred !list_of_predicates) (-1 :: ( List.map Misc.fst3 !PredTable.tableOfPreds))) in
	  M.msg_string M.Normal "find_all_predicates found: \n";
	  List.iter (fun x -> M.msg_string M.Normal (P.toString x)) !list_of_predicates;
	  M.msg_string M.Normal ("# new preds = "^(string_of_int (List.length newpredlist)));
	  if (newpredlist = [] ) then failwith ("find_all_predicates also fails to get new predicates !!") *)

    if find_all_predicates_cheap oplist == 0 then
      begin
	let _ = process_subtrace oplist_r reglist_r in
	let newpredlist = Misc.compact 
	    (Misc.difference (List.map addPred !list_of_predicates) (-1 :: (PredTable.get_all_pred_indexes () ))) 
	in
	M.msg_string M.Debug "find_all_predicates found: \n";
	List.iter (fun x -> M.msg_string M.Debug (P.toString x)) !list_of_predicates;
	M.msg_string M.Debug ("# new preds = "^(string_of_int (List.length newpredlist)));
	if (newpredlist = [] ) then failwith ("find_all_predicates also fails to get new predicates !!")
      end




	  
  (*******************************************************************************************************) 
	  (* this is for m4 *)
	  include(abs-boolean.ml)
	  include(abs-include.ml)
  (*******************************************************************************************************) 
	  


  (*******************************************************************************************************) 
  (****this function takes the various arrays and a list of useful blocks and gets and adds new preds ****)
  (*******************************************************************************************************) 
  (*******************************************************************************************************)


  let parallel_interpolant_refine 
    extract_block conj_block_array reg_array post_reg_array
    tid_list  tid_array
    (block_l : int list) = 
    
    M.msg_string M.Debug "Inside parallel interpolant refine";
    (* assert (not (block_l = [])); *)
    let ord_block_l = List.sort compare block_l in
    let _ = M.msg_string M.Normal ("Block set: "^(Misc.strList (List.map string_of_int ord_block_l)) )
    in
    let start = List.hd ord_block_l in
    let stop = List.hd (List.rev ord_block_l) in
    let cons_l = List.map extract_block  ord_block_l in
    let start_cons = 
      if block_check_unsat (P.And cons_l) then P.True
      else conj_block_array.(start) 
    in	
      
    let start_tid = tid_array.(start) in
      (* for debugging purposes, we shall dump the interesting blocks *)
    let _ = 
      begin
	M.msg_string M.Normal (Printf.sprintf "num blocks = %d" (List.length block_l));
	M.msg_string M.Normal (Printf.sprintf "start cons = \n %s" (P.toString start_cons));
	M.msg_string M.Normal ("Constraints");
	List.iter (fun p -> M.msg_string M.Debug (P.toString p) ) cons_l
      end
    in
      (* RJ: on the ken-wishlist: let proof = getFociProof (start_cons::cons_l) in *)
      (* new method. Lets not add these global things everywhere.... only:
	 1. add the preds from the tids interpolant
	 2. add them from: min (the min block in interesting blocks / tid
	 3. add them to: ...
	 let k = max interesting block.
	 case: k \not \in tid -> till the max block in tid <= k
      *)

    let cut tid =
      let (this_l,env_l) = List.partition (fun i -> tid_array.(i) = tid) block_l in
	if not (this_l <> [] && env_l <> []) then
	  M.msg_string M.Normal "All cons in one tid"
	else
	  let (s_c,e_c) = if tid = start_tid then ([start_cons],[]) else ([],[start_cons]) 
	  in
	  let cons_l_tid = s_c @ (List.map extract_block this_l) 
	  and cons_l_env = e_c @ List.map extract_block env_l
	  in
	  let foci_preds = 
	    (* JHALA: temporary patch for segfaults! *)
	    match (block_check_unsat (P.And cons_l_tid)) || (block_check_unsat (P.And cons_l_env)) with
		true -> []
	      | false -> extract_foci_preds (cons_l_tid,cons_l_env) 
	  in 
	  let _ = 
	    M.msg_string M.Normal "extraction done";
	    List.iter (fun i -> M.msg_string M.Normal (string_of_int i)) foci_preds
	  in 
	  let _ = 
	    if (Options.getValueOfInt "craig" >= 2) then 
	      begin
		(* now this maneuver where one only adds the preds to the relevant location *)
  		let ftid tid i = if tid_array.(i) = tid then Some (i) else None in 
		let (tid_min) =  (Misc.get_first (ftid tid) ord_block_l) in
		  match (tid_min) with
		      (None) -> ()  (* this tid does not contribute to unsat *)
		    | (Some this_tid_start) ->
			begin
			  for i = this_tid_start to stop do
			    if (tid_array.(i) = tid) then
			      begin  
				M.msg_string M.Debug ("Accessing i= "^(string_of_int i));
				
				let pre_loc = C_SD.location_coords (location_coords_of_region (reg_array.(i))) in
				let post_loc = C_SD.location_coords (location_coords_of_region (post_reg_array.(i))) in
				  PredTable.add_local_preds pre_loc foci_preds;
				  PredTable.add_local_preds post_loc foci_preds
			      end
			  done;
			end
	      end
	  in
	  let _ =
	    if (Options.getValueOfInt "craig" < 2 || Options.getValueOfBool "gpred") then
	      begin
		let to_add = 
		  List.filter
		    (fun p_i -> not ( List.mem p_i !PredTable.globally_useful_preds)) foci_preds 
		in 
		  M.msg_string M.Debug "Adding to gpredtable "; 
		  List.iter 
		    (fun p_i -> 
		       PredTable.block_a_t_flag := true; 
		       M.msg_string M.Debug (string_of_int p_i);
		       PredTable.globally_useful_preds := p_i :: !PredTable.globally_useful_preds) 
		    to_add  
	      end
	  in ()

    in
      List.iter cut tid_list




  (********************************* THE E-TAR refinement procedures *******************************)
  (******************************* Begin the e-tar refinement procedures ***************************)
  (* copying a bunch of functions from the lazymodelchecker file to here *)


  (* sq_loc tells you the position at some time and sq_edge, the last edge traversed to get to that squished loc
     the option is None when the move is a stutter *)

  type sq_edge_t = {sq_loc : int ; sq_edge : (int * int * EnvAutomaton.aedge_label) option }
      (* an invariant about the above -- if sq_edge is not None, then the snd is the same as sq_loc *)

  let get_shrink_node_id sn = 
    (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.id

  let concretize_shrink_node env_auto sn_id = 
    (*let sn_id = get_shrink_node_id sn in *)
    let onode_id_list = 
      try Hashtbl.find env_auto.EnvAutomaton.a_to_o_map sn_id 
      with Not_found -> failwith "bad env_auto -- cannot concretize"
    in
      (* List.map (Hashtbl.find env_auto.EnvAutomaton.o_map) *) onode_id_list
      (* for now, return only the ids, not the actual nodes *)

  let race_lval () = 
    match !check_races with
	None -> failwith "No race variable!!"
      | Some x -> x 

  let type_of_edge e_label =  
    let gvars = 
      (race_lval ())::(!PredTable.globally_important_lvals) 
    in
      match e_label with
	  ProgramOperation op -> 
	    let this_lvals = (snd (C_SD.reads_and_writes_at_edge op)) in
	      EnvAutomaton.Thread 
		(List.filter (fun l -> List.mem l this_lvals) gvars)
	| AutomatonOperation _ -> EnvAutomaton.Env
	    

  let get_auto_node_id an = 
    (BiDirectionalLabeledGraph.get_node_label an).EnvAutomaton.oid


  let abstract_auto_node env_auto an_id = 
    try
      Hashtbl.find env_auto.EnvAutomaton.o_to_a_map an_id
    with Not_found -> failwith "bad o_to_a_map !"


  (* I shall assume this to be the number of threads ... *)
  let num_threads = ref 1 


  (* a couple of type transducers *)

  let reggify (l) =
    let make_tuple e =
      let (n,op,n') = BiDirectionalLabeledGraph.deconstruct_edge e in
	match op with
	    ProgramOperation op' -> 
	      let n_label = BiDirectionalLabeledGraph.get_node_label n in
	      let n'_label = BiDirectionalLabeledGraph.get_node_label n' in
		Some (n_label.EnvAutomaton.oregion, op', n'_label.EnvAutomaton.oregion)
	  | _ -> None
    in
      Misc.map_partial make_tuple l

  (*************************************************************************)

  (* this next function will return "ct_l" list of sequences of ops for each thread *) 

  let env_consistency_check thread_posn_array env_map = 
    let loc_table = Hashtbl.create 17 in
    let proc_ctr = ref 0 in
    let check_eq loc num = 
      proc_ctr := !proc_ctr + num;
      env_map loc = num 
    in
    let gather i {sq_loc = loc} =
      if i <> 0 then
	if Hashtbl.mem loc_table loc 
	then
	  let present_count = Hashtbl.find loc_table loc in
	    Hashtbl.replace loc_table loc (present_count + 1)
	else
	  Hashtbl.replace loc_table loc 1
    in
      Array.iteri gather thread_posn_array;
      try
	let rv = Misc.hashtbl_forall check_eq loc_table in
	  rv && (!proc_ctr = !num_threads)
	    
      with e -> failwith "env_consistency raises exception" 
	
  (* this below returns an array such that tau.i.j tells you where process j was at time i,
     as well as the last operation it took to get there ... *)

  let compute_tau env_aut trace target_list = 
    (* trace will be used to build ct 
       first, we build trace', the sequence of automaton states *)
    M.msg_string M.Normal "Building Tau";
    let deconstruct_p_trace trace = 
      let extract_env_map reg = 
	let env_arr = 
	  match reg with
	      Region.ParallelAtomic pat -> pat.Region.e_control
	    | _ -> failwith "bad call to extract_env_map"
	in
	  (* RJ: Warning -- arrays are updated in place so this function could do funky things if one isn't careful ...
	     ie. if somehow the underlying array has changed for some reason ... *)
	  (fun i -> try Counter.to_int (env_arr.(i)) with _ -> failwith "bad counter map in decon_pt")
      in
      let extract_edge_map (opk,eml) = 
	let et = Hashtbl.create 37 in
	let _ = List.iter 
		  (fun (j1,j2,c) -> Hashtbl.replace et (j1,j2,EnvAutomaton.Env) 
		     (Counter.to_int c))
		  eml
	in
	let _ =
	  match opk with
	      ProgramOperation _ -> ()
	    | AutomatonOperation (j1,j2) -> 
		Hashtbl.replace et (j1,j2,EnvAutomaton.Thread []) 1 (* dont look at the actual
								       lvals modified *)
	in
	  et
      in
	(List.map extract_env_map (fst trace), List.map extract_edge_map (snd trace))
    in

    let (eloc_map_l, eedge_map_l ) = deconstruct_p_trace trace in 

    let trace_length = List.length eedge_map_l in 
      (* RJ: Is this the num of ops or regs which is the former + 1 ? 
	 -- say ops -- then its perfect -- so assume that trace_length is the number of ops *)
    let shrink_root = env_aut.EnvAutomaton.autom_root in
    let shrink_root_id = get_shrink_node_id shrink_root in
      
    let init_loc = {sq_loc = shrink_root_id; sq_edge = None} in
    let tau = Array.make_matrix (trace_length + 1) (!num_threads) init_loc in
      (* building target information *)
    let tids_at_shrink_loc l = 
      Misc.array_filter (fun a -> a.sq_loc = l) tau.(trace_length) 
    in

      
    (* this is the array that we shall fill up. tau.i.j contains the squished state that thread j is in 
       at time i. That squished state is of course, merely an int *)
      

    (* RJ: can do a sanity check here that in the first elt of the trace, the region has all the
       env threads at the start position *)
    let _ =  assert (env_consistency_check (tau.(0)) (List.hd eloc_map_l)) in
    let proc_step i (loc_map,edge_map) =
      
      M.msg_string M.Normal ("Proc step "^(string_of_int i));
      Array.iteri (fun i a -> Printf.printf "Tid: %d  SQ_loc: %d \n" i a.sq_loc) tau.(i-1);
      let edge_print (j1,j2,e) num = 
	let s =
	  match e with
	      EnvAutomaton.Thread [] -> "thread"
	    | _ -> "Env"
	in      
      	  Printf.printf "Edge (%d, %d, %s) : %d \n" j1 j2 s num
      in 
	Hashtbl.iter edge_print edge_map;
	let p_table = Hashtbl.create 37 in
	let _ = assert (env_consistency_check (tau.(i-1)) (loc_map)) in
	  Array.iteri (fun j loc -> Misc.hashtbl_update p_table (loc.sq_loc) j) tau.(i-1);
	  
	  let mop_up loc plist = 
	    List.iter (fun j -> tau.(i).(j) <- {sq_loc = loc; sq_edge = None}) plist
	  in
	  let proc_edge (j1,j2,etype) num_proc_using_edge = 
	    (* let num_proc_using_edge = try Hashtbl.find edge_map (j1,j2,etype) with Not_found -> 0 in *)
	    let proc_at_source = try Hashtbl.find p_table j1 with Not_found -> [] in
	    let (go,stay) = 
	      try
		Misc.cut_list_at_k proc_at_source num_proc_using_edge 
	      with _ -> failwith "Not enough threads at squished state !"
	    in
	      (* after the step i we are at the ith point in the trace -- the trace starts at t=0, steps are 1,2,3 ... *)
	      (* so we shall just say the threads that "go"ed end up at j2 at the i'th step *)
	      List.iter (fun tid -> tau.(i).(tid) <- {sq_loc = j2; sq_edge = Some (j1,j2,etype)}) go;
	      Hashtbl.replace p_table j1 stay 
		(* only these are left, the rest have been assigned already *)
	  in
	    
	    Hashtbl.iter proc_edge edge_map;
	    (* the processes that are still at a location are presumably stuttering ... 
	       so we put that into their info like so ...*)
	    Hashtbl.iter mop_up p_table;
	    (i+1) (* for fold_left purposes *)
    in
    let lmem_l = List.combine (List.tl eloc_map_l) (eedge_map_l) in
    let _ =   List.fold_left proc_step 1  lmem_l in
      (* now the tau is loaded *)
    let target_map = 
      match target_list with
	  [] -> -1
	| [h] -> 
	    let tasl = (tids_at_shrink_loc (abstract_auto_node env_aut (get_auto_node_id h))) in
	      if tasl = [] then failwith "no tid at loc -- error in tau-generation! "
	      else 
		let i = List.hd tasl in
		  i
	| _ -> failwith "bad target_list"
    in
      (tau,target_map)
	(* the concrete trace for each thread is a list of (c_edge * region),
	   where type c_edge = Move of Opkind | Stutter
	   and region is, well, region -- though with the envmaps projected 
	   away, since the whole thing is constructed as a path through the orig auto
	   which is a projection of the reachability tree 
	*) 

  type visited_type = In | Out
      
  let sq_loc_to_string sq_l = 
    Printf.sprintf "[ sq_l = %d ]" sq_l

  let sq_edge_to_string sq_e = 
    match sq_e with 
	None -> ""
      | Some (j1,j2,e) ->
	  let s = 
	    match e with
		EnvAutomaton.Thread _ -> "thread" 
	      | _ -> "env" 
	  in
	    Printf.sprintf "Edge (%d, %d, %s)" j1 j2 s

  let  sq_to_string sq = 
    (sq_loc_to_string sq.sq_loc)^"  "^(sq_edge_to_string sq.sq_edge)

  let print_shrink_path a_ =
    M.msg_string M.Normal "Shrink_path:";
    let pr sq = 
      M.msg_string M.Normal (sq_to_string sq)
    in List.iter pr a_

  let oauto_node_to_string onode = 
    string_of_int ((BiDirectionalLabeledGraph.get_node_label onode).EnvAutomaton.oid)

  let oauto_edge_to_string oedge = 
    let (s,_,t) = BiDirectionalLabeledGraph.deconstruct_edge oedge in
      Printf.sprintf "%s --> %s" (oauto_node_to_string s) (oauto_node_to_string t)

  let findpath env_auto a_ start target = 
    let abstract_auto_node n = abstract_auto_node env_auto (get_auto_node_id n) in
    let target_id = match target with Some targ -> get_auto_node_id targ | None -> -1 in
    let stamped_table = Hashtbl.create 1009 in (* this is the stamped table *)
      (* here a is a squished node and s is a concrete node *)
    let local_out_edges s = 
      let a_id = abstract_auto_node s in
	List.filter 
	  (fun e' -> abstract_auto_node ( (BiDirectionalLabeledGraph.get_target e')) = a_id)
	  (BiDirectionalLabeledGraph.get_out_edges s)
    in
    let local_successors s = 
      List.map BiDirectionalLabeledGraph.get_target (local_out_edges s)
    in

    let local_connect = BiDirectionalLabeledGraph.local_connect local_out_edges in
      

    let get_succ etyp a_id s = 
      let is_solid etyp = 
	match etyp with
	    EnvAutomaton.Thread _ -> true
	  | _ -> false
      in
	(* here etyp is either Thread [] or Env ... -- lval mod info is NOT there ..*)
	List.filter 
	  (fun e -> 
	     (abstract_auto_node (BiDirectionalLabeledGraph.get_target e) = a_id )	   && 
	     (is_solid (type_of_edge 
			  (BiDirectionalLabeledGraph.get_edge_label e))    
	      = (is_solid etyp)))
	  (BiDirectionalLabeledGraph.get_out_edges s)
    in
    let frontier s a =
      let a_id = a.sq_loc in
      let fnodes = BiDirectionalLabeledGraph.collect_component local_successors [s] in
	(* now take only the fellows that have an edge leaving the component *)
      let is_frontier_node s' = 
	(List.exists (fun s'' -> abstract_auto_node s'' <> a_id) 
	   (BiDirectionalLabeledGraph.get_children s'))
	|| (get_auto_node_id s' = target_id)
      in (* the final target is always a frontier -- should you wish to come there and wait *)
      let fnodes = List.filter is_frontier_node fnodes in
      let forced_local_connect s n = 
	match local_connect s n with
	    Some el -> el
	  | None -> failwith "forced_local_connect fails! bad source-target ?"
      in
	List.map (fun n -> ((forced_local_connect s n),n)) fnodes

    in
    let pathlen = List.length a_ in

    let rec _findpath a_ i s = 
      let s_id = get_auto_node_id s in 
	(* here s is an auto node *)
	if Hashtbl.mem stamped_table (s_id,i,In) then None (* no path from here ... *)
	else 
	  begin
	    Hashtbl.replace stamped_table (s_id,i,In) true;
	    match a_ with
		[] -> failwith "THIS SHOULD NOT HAPPEN -- bad a_"
	      | [a] -> 
		  begin
		    match target with
			Some targ ->
			  begin
			    M.msg_string M.Normal ("findpath--lasttarget: "^(string_of_int ((BiDirectionalLabeledGraph.get_node_label targ).EnvAutomaton.oid) ));
			    match local_connect s targ with
				None -> None
			      | Some el -> 
				  begin
				    let ps = Misc.strList (List.map (oauto_edge_to_string) el) in
				      M.msg_string M.Normal ("connectpath: "^ps^":");
				      Some [el]
				  end
			  end
		      | None -> 
			  begin
			    M.msg_string M.Normal "findpath--no last target";
			    Some [[]] (* It needs to do nothing *)
			  end
		  end
	      | a::a'::_ -> 
		  let _ = assert (a.sq_loc = 0 || (Hashtbl.find env_auto.EnvAutomaton.o_to_a_map s_id) = a.sq_loc) in
		  let continue_path (squiggle, n') = 
		    let n'_id = get_auto_node_id n' in
		      if Hashtbl.mem stamped_table (n'_id,i,Out) then None
		      else
			begin
			  Hashtbl.replace stamped_table (n'_id,i,Out) true;
			  let rest_a_ = List.tl a_ in
			    match a'.sq_edge with
				None -> (* a stutter edge *) 
				  begin
				    match _findpath (rest_a_) (i+1) n' with
					None -> None
				      | Some t' -> Some (squiggle::t')
				  end
			      | Some (j1,j2,ety) ->
				  begin
				    let e_l = get_succ ety j2 n' in
				    let targs = List.map BiDirectionalLabeledGraph.get_target e_l in
				      match Misc.get_first (_findpath rest_a_ (i+1)) targs with
					  None -> None
					    
					| Some [] -> failwith "assert: this cannot happen in continue path"
					| Some [[]] ->
					    begin
					      (* this happens if:
						 1. a' is the last fellow in the sq_trace AND
						 2a. there is no target 
						 || 2b. there is a target and there is an edge in e_l 
						 whose succ is target *)
					      if (List.length rest_a_ <> 1)
					      then 
						begin
						  print_shrink_path rest_a_;
						  failwith "I was wrong. RJ." 
						end

					      else
						let e' = 
						  match target with
						      Some targ ->
							begin
							  try
							    List.find (fun e -> (BiDirectionalLabeledGraph.get_target e = targ)) e_l with _ -> failwith "I was really wrong. RJ"
					    		end
						    | None -> 
							begin
							  try List.hd e_l
							  with _ -> failwith "but but ..."
							end
						in
						  Some ((squiggle@[e'])::[[]])
					    end

					| Some (t') ->  
					    let e' = 
					      match Misc.hd_ll t' with
						  Some e'' ->
						    begin
						      try
							List.find 
							  (fun e -> BiDirectionalLabeledGraph.get_target e = BiDirectionalLabeledGraph.get_source e'') 
							  e_l 
							  (* as an abstract edge was taken -- it means you have to 
							     find a successor here *)
						      with Not_found -> failwith "list_find_fails!"
						    end
						| None -> List.hd e_l 
					    in
					      Some ((squiggle@[e'])::t')
				  end
			end
		  in
		    if a'.sq_loc = 0 then 
		      (* i.e. the next state is the stuck state -- we've done i-1 moves so we need n-(i-1) more *)
		      begin
			let rec bl k = 
			  if k = 0 then [] else []::(bl (k-1))
			in
			  Some (bl (pathlen - i + 1))
		      end
		    else
		      let outl = frontier s a in
			Misc.get_first continue_path outl
	  end
    in
      _findpath a_ 1 start
	
  let get_thread_trace trace = 
    let (r_l,e_l) = trace in
    let reg_pairs_l = Misc.make_list_pairs (List.map Region.project_par_atomic r_l) in
      List.map2 (fun (r1,r2) (opk,_) -> 
		   match opk with
		       ProgramOperation op -> (0,[(r1,op,r2)])
		     | _ -> (0,[])
		)
	reg_pairs_l e_l
	

  let auto_node_to_string {EnvAutomaton.oid = i ; EnvAutomaton.oadr = adr} =
    Printf.sprintf "%d $$ %s" i (Region.abstract_data_region_to_string adr)

  let auto_edge_to_string op =
    match op with
	ProgramOperation op -> Operation.to_string op
      | AutomatonOperation (i,j) -> (Printf.sprintf "AutomatonMove(%d,%d) " i j)





  let dumpauto env_auto j a_ = 
    M.msg_string M.Normal "Dumpauto: Printing Automaton:";
    let init_node = env_auto.EnvAutomaton.o_auto_root in
    let _  =  BiDirectionalLabeledGraph.output_graph_dot 
		auto_node_to_string 
		auto_edge_to_string 
		(Options.getValueOfString "auto") [init_node] in
    let _ = M.msg_string M.Normal "Sh_el_info:" in
    let il_to_s il = Misc.strList (List.map string_of_int il) in
      Hashtbl.iter (fun i elts_l -> M.msg_string M.Normal
		      (Printf.sprintf "%d : %s" i (il_to_s elts_l))) 
	env_auto.EnvAutomaton.a_to_o_map

	

  let build_g_trace env_auto trace target_list = 
    let time_list = 0::(Misc.make_list (List.length (snd trace))) in
    let oroot = env_auto.EnvAutomaton.o_auto_root in
      
    let target_map i tid = 
      if tid <> i then None 
      else
	if target_list = [] then None
	else Some (List.hd target_list) 
    in
      
    let get_ethread_ct tau target_tid j =
      let a_ = try List.map (fun i -> tau.(i).(j)) time_list with _ -> failwith "offbyone?" in
	match (findpath env_auto a_ oroot (target_map target_tid j)) with
	    Some l -> 
	      let ps = Misc.strList (List.map (fun p -> Misc.strList (List.map oauto_edge_to_string p)) l )  in
	      let _ = M.msg_string M.Normal ("Path: tid "^(string_of_int j)^" : "^ps) in
		List.map (fun a -> (j+1,(reggify a))) l (* to deal with the start by zero issue *)
		  (* RJ: I leave b as a list as well -- a real edge will be a list of length 1,
		     but for stuttering you put an empty list -- I'm getting tired of them options *)
	  | None -> 
	      begin
		dumpauto env_auto j a_ ;
		failwith ("Cannot find witness trace for thread "^(string_of_int j))
	      end
    in
    let (tau,target_tid) = compute_tau env_auto trace target_list in
    let ct0 = get_thread_trace trace in 
    let ct_env = List.map (get_ethread_ct tau target_tid) (0::(Misc.make_list (!num_threads -1))) 
    in (* now ct0 may have length 1 less than each list in ct_env as the env can do one last squiggle ... so make kcombine robust! *)
    let serial_trace = Misc.kcombine (ct0::ct_env) in (*   (int * (reg,op,reg) list) list *)
      List.flatten (List.map (fun (i,l) -> List.map (fun x -> (i,x)) l) serial_trace) 
	
  (* RJ:  what does each "edge" have above ? Since we have to homogenize it ... make an edge a (reg,op,reg) ... 
     where by "reg" I mean an Atomic region *)



  (********************************)

  let parallel_block_analyze_trace env_auto trace target_list number_of_threads =
    let _ = M.msg_string M.Normal "In par_block_analyze_trace" in
    let _ = num_threads := Counter.to_int number_of_threads in (* should be a finite number *)
    let _ = (PredTable.block_a_t_flag := false) in 
      (* to check if any new info has been added *)
    let serial_trace  = build_g_trace env_auto trace target_list in 
      (* the above is now an (int * (r,op,r')) list *)  
      (* Phase 1: make the giant arrays corresponding to the whole trace               *)
      (* here "g" stands for "giant" or globbed *)
    let len_op_l = List.length serial_trace in
    let g_op_l = List.map (fun (_,(_,op,_)) -> op) serial_trace in
    let g_reg_l = List.map (fun (_,(r,_,_)) -> r) serial_trace in
    let g_post_reg_l = List.map (fun (_,(_,_,r)) -> r) serial_trace in
    let g_tid_l = List.map (fun (tid,_) -> tid) serial_trace in 
    let print_strace (tid,(_,op,_)) =
      M.msg_string M.Normal ("Thread"^(string_of_int tid));
      M.msg_string M.Normal (Operation.to_string op);
    in
    let _ = List.iter print_strace serial_trace in
      if (Options.getValueOfBool "stop") then failwith "FALSE ERROR CHOKE" ;
      let g_tid_array = Array.of_list g_tid_l in
      let g_op_array = Array.of_list g_op_l in
      let g_reg_array = Array.of_list g_reg_l in
      let g_post_reg_array = Array.of_list g_post_reg_l in
      let g_conj_block_array = Array.create (len_op_l) P.True in
      let g_block_array = Array.create len_op_l P.True in
	(* INV: all the above arrays have length len_op_l *)
      let _ = M.msg_string M.Debug "pblock_a_t: building blocks start" in  
      let unsat_flag = ref false in
      let _ = block_reset () in

      let _ = M.msg_string M.Normal ("par_trace length = "^(string_of_int len_op_l)) in
      let posn = ref (len_op_l - 1) in 
      let unsat_somewhere = ref false in
      let parallel_cons_folder (tid,(pre_reg,op,post_reg)) () = 
	M.msg_string M.Debug ("par_cons_folder !posn = "^(string_of_int !posn));
	M.msg_string M.Debug ("par_cons_folder tid = "^(string_of_int tid));
	if (not (!unsat_flag)) then
	  begin
	    let atomic_post_reg = 
	      match post_reg with
		  Region.Atomic atr -> atr
		| _ -> failwith "bad region given to cons_folder!"
	    in
	    let fname = reg_function_name pre_reg in
            let ce_o = 
                if C_SD.isReturn op then Some(get_callEdge_from_postloc atomic_post_reg.Region.location) 
                else None in
	    let block_cons = block_concrete_data_pre fname tid op ce_o in
	    let conj_block_cons = pred_lval_map tid (extract_pred pre_reg) in
	    let _ = M.msg_string M.Normal ("posn:"^(string_of_int !posn)) in
	    let _ = M.msg_string M.Normal ("bcons"^(P.toString block_cons)) in
	    let _ = M.msg_string M.Normal ("cbcons"^(P.toString conj_block_cons)) in

	    let is_unsat = 
	      block_assert block_cons;
	      block_assert conj_block_cons;
	      let rv = TheoremProver._is_contra () in
		block_pop ();
		rv
	    in
	      Array.set g_block_array !posn block_cons;
	      Array.set g_conj_block_array !posn conj_block_cons;
	      Array.set g_tid_array !posn tid; (* so we know which thread moved at this timestep *)
	      posn := !posn - 1;
	      if (is_unsat) then
		M.msg_string M.Debug 
		  ("Found unsat at level"^(string_of_int (!posn +1)));
	      if (false && is_unsat && not (Options.getValueOfInt "predH" >= 6))
	      then unsat_flag := true;
	      unsat_somewhere := !unsat_somewhere || is_unsat
	  end
      in
	(* let _ = if (Options.getValueOfBool "talias") then load_lv_table g_op_l in *)
	
      let _ = List.fold_right parallel_cons_folder serial_trace ();block_reset ()
      in
	
      let _ = M.msg_string M.Debug "pblock_a_t: building blocks end" in
	
      (* Phase 2: Partition giant array into arrays for each thread                    *)
	
      let tid_list = (0::(Misc.make_list !num_threads)) in
      let step_list = 0::(Misc.make_list len_op_l) in  
      let partition_steps = 
	List.map 
	  (fun tid -> Misc.array_filter (fun j -> j = tid) g_tid_array) 
	  tid_list
      in 
	
      let partition_array arr = 
	Misc.array_of_list2 (Misc.array_select2 arr partition_steps) 
      in

      let par_op_array = partition_array g_op_array in
      let par_conj_block_array = partition_array g_conj_block_array in
      let par_block_array = partition_array g_block_array in
      let par_reg_array = partition_array g_reg_array in
	(* Phase 3: For each thread, call get_useful_blocks and then interpolant_refine  *)
	
      let local_refine tid =
	let _ = M.msg_string M.Normal ("In local_refine tid = "^(string_of_int tid)) in
	let (_ , interesting_block_sets, suppress_stuff) =
	  get_useful_blocks par_op_array.(tid) par_block_array.(tid) par_conj_block_array.(tid) None
	in 
	let _ = M.msg_string M.Normal ("found "^(string_of_int (List.length interesting_block_sets))^" useful block sets") 
	in
	let (suppress_array, 
	     extract_block, 
	     drop_constraint, 
	     restore_constraint, 
	     restore_all_constraints) =  suppress_stuff 
	in
	let _ = restore_all_constraints () in
	  (match (Options.getValueOfInt "craig" <> 0) with
	       true -> Misc.ignore_list_iter (interpolant_refine None None extract_block par_conj_block_array.(tid) par_reg_array.(tid))
	     | false -> Misc.ignore_list_iter (focus_refine extract_block par_conj_block_array.(tid) par_reg_array.(tid))
	  )  interesting_block_sets
      in
      let _ = print_string "tid_list: "; List.iter print_int tid_list in
      let _ = List.iter local_refine tid_list in
      let _ = M.msg_string M.Normal "finished local ref" in
	(* Phase 4: Call get_useful_blocks on the giant array *)
      let (_, interesting_block_sets, suppress_stuff) =
	get_useful_blocks g_op_array g_block_array g_conj_block_array None
      in 
      let _ = M.msg_string M.Normal ("Globally found "^(string_of_int (List.length interesting_block_sets))^" useful block sets") 
      in
      let (suppress_array, 
	   extract_block, 
	   drop_constraint, 
	   restore_constraint, 
	   restore_all_constraints) =  suppress_stuff 
      in
	(* Phase 5: Call parallel_interpolant_refine on the result of the above *)
      let _ = M.msg_string M.Normal "Now calling par_int_ref" in
      let _ = restore_all_constraints () in
      let _  = List.iter 
		 (parallel_interpolant_refine 
		    extract_block g_conj_block_array g_reg_array g_post_reg_array tid_list g_tid_array) 
		 interesting_block_sets
      in
	if (not !unsat_somewhere) then 
	  begin
	    List.iter print_strace serial_trace;
	    failwith "real error!"
	  end
	else 
	  if (not !PredTable.block_a_t_flag) then failwith "Par: No new predicates found ...!"
	  else ()

  (* TBD:
     1. "target" for each thread
     2. get_thread_trace
     3. the actual predicate extraction from the combined trace 
     4. hookup the max_count with num_threads 
     5. The aliasing thing above search for TBDPLDI *)


  (************************* End the e-tar refinement procedures  *************************)
  (****************************************************************************************)

  (* the refinment operator required by the ABSTRACTION signature *)
  let focus reg_to_refine empty_reg  =
    M.msg_string M.Debug "In focus -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg_to_refine ;
    M.msg_printer M.Debug Region.print empty_reg ;

    match (reg_to_refine, empty_reg) with
        (Region.Atomic abst_atomic_reg, Region.Atomic empty_conc_atomic_reg) ->
          begin
            try 
              match (abst_atomic_reg.Region.data, empty_conc_atomic_reg.Region.data) with
                  (Region.Abstract abst_data_reg, Region.Concrete empty_conc_data_reg) ->
                    Region.Atomic { Region.location = abst_atomic_reg.Region.location ;
                                    Region.data = Region. Abstract (Stats.time "do_focus" (do_focus abst_data_reg) empty_conc_data_reg) ;
                                    Region.stack = abst_atomic_reg.Region.stack ; }
		| _ -> failwith "focus: input regions should be abstract and concrete in this order"
            with NoNewPredicatesException ->
              ( M.msg_string M.Debug "In focus -- arguments are:" ;
                M.msg_printer M.Debug Region.print reg_to_refine ;
                M.msg_printer M.Debug Region.print empty_reg ;
                M.msg_string M.Debug "No new predicates found. Aborting." ;
                raise NoNewPredicatesException
              )
          end
      | _ -> begin
          Region.print (Format.std_formatter) reg_to_refine ;
          Region.print (Format.std_formatter) empty_reg ;
          failwith "focus: arguments should be atomic regions"
        end


  (* the following function takes a concrete data region and a list of predicates and returns an 
     abstract region corresponding to it. Cousot would call it alpha *)

  let abstract atomic_conc_reg pred_list =
    let assumption  =
      match atomic_conc_reg.Region.data with
          Region.Concrete cr -> cr.Region.pred
        | _ -> failwith ("bad call to atomic_conc_reg")
    in
      assume_pred_ref := Some(assumption);
      let abstract_bdd = postBdd PredTable.bddOne (List.map (function i -> (i,PredTable.getPred i,P.negate (PredTable.getPred i),true)) pred_list) [] 
      in
        assume_pred_ref := None;
        Region.Abstract { Region.bdd = abstract_bdd; Region.lelement = DataLattice.top; Region.preds = pred_list}


  let intersect_with_abstraction reg1 reg2 =
    (* reg1 : abstract, reg2 : concrete or abstract, returns reg1 cap reg2 : abstract *)
    let reg2' = precise reg2 in
      match (reg1,reg2') with
          (Region.Atomic atr1, Region.Atomic atr2) ->
            begin
	      M.msg_string M.Debug ("intersect_with_abstraction:\n" ^ 
						"    reg1 = " ^ Region.to_string reg1 ^
						"    reg2 = " ^ Region.to_string reg2);
              match (atr1.Region.data,atr2.Region.data) with
		  (Region.Abstract abst_1,Region.Concrete conc_2) ->
                    let preds = abst_1.Region.preds in
                    let abst_2 = abstract atr2 preds in
                    let newreg2 = Region.Atomic { Region.location = atr2.Region.location;
                                                  Region.data = abst_2;
                                                  Region.stack = atr2.Region.stack; }
                    in
                      Region.cap reg1 newreg2
		| _ -> failwith "bad call to intersect_with_abstraction"
            end
        | _ -> failwith "bad call to intersect_with_abstraction 2"

  

  let _tar_enabled_ops automaton reg =
    M.msg_string M.Debug "In tar_enabled_ops" ;
    match reg with
	Region.ParallelAtomic p_atomic_reg ->
	  begin
	    let get_autom_actions automaton ctrmap =
              let succs = ref [] in
		for i = 0 to (Array.length ctrmap -1) do
                  if not (Counter.eq ctrmap.(i) (Counter.make_counter 0)) then
                    let thread_succs = List.map (fun e -> AutomatonOperation (i,EnvAutomaton.getIdFromNode automaton (BiDirectionalLabeledGraph.get_target e))) 
					 (EnvAutomaton.getThreadEdges automaton i) in
                      succs := if (Counter.is_omega ctrmap.(i)) then !succs @ thread_succs else thread_succs @ !succs
                  else () 
		done ;
		!succs
            in
	    let progops = List.map (fun o -> ProgramOperation o) (C_SD.get_outgoing_edges p_atomic_reg.Region.p_control.Region.p_location) in
              if (is_atomic reg (* p_atomic_reg.Region.p_control.Region.p_location*)) then progops 
              else
		begin
		  let get_atomics = 
                    let atomic_ones = ref [] in
                      for i = 0 to Array.length p_atomic_reg.Region.e_control -1 do
			let cnt_i = p_atomic_reg.Region.e_control.(i) in
			  if (not (Counter.eq cnt_i (Counter.make_counter 0)) && 
			      (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i)).EnvAutomaton.atomic = true) then
			    begin
			      atomic_ones := i :: !atomic_ones;
			      if Counter.geq cnt_i (Counter.make_counter 2) then 
				failwith "more than one atomic!"
			    end

			  else ()
                      done ;
                      if List.length !atomic_ones > 1 then failwith "More than one atomic operation in tar_enabled_ops!" ;
                      !atomic_ones
		  in
		    match get_atomics with 
			[] ->
			  let automops = (* for each location i in the automaton with counter value > 0, return 
					    (i,j) where j is a solid neighbor of i *)
			    (get_autom_actions automaton p_atomic_reg.Region.e_control)
			  in
			    progops @ automops
		      | [ a ] -> List.map (fun e -> AutomatonOperation (a,EnvAutomaton.getIdFromNode automaton (BiDirectionalLabeledGraph.get_target e))) 
	                  (EnvAutomaton.getThreadEdges automaton a) 
		      | _ -> failwith "cannot happen"
		end 
	  end
      | _ -> List.map (fun o -> ProgramOperation o) (enabled_ops reg) (* no env I suppose *)

  let exists_atomic automaton reg =
    match reg with
	Region.ParallelAtomic p_atomic_reg ->
	  let env_has_atomic = 
	    let atomic_flag = ref false in
	      for i = 0 to Array.length p_atomic_reg.Region.e_control -1 do
		if (not (Counter.eq p_atomic_reg.Region.e_control.(i) 
			   (Counter.make_counter 0)) && 
		    (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i)).EnvAutomaton.atomic = true) 
		then
		  atomic_flag := true;
              done ;
	      !atomic_flag
	  in
            (C_SD.is_atomic p_atomic_reg.Region.p_control.Region.p_location) || env_has_atomic
      | _ -> false
	  

  let exists_multiple_tasks automaton reg = 
    match reg with
	Region.ParallelAtomic p_atomic_reg -> 
	  begin
	    let task_ctr = 
	      if (is_task reg) 
	      then ref (Counter.make_counter 1) 
	      else 
		ref (Counter.make_counter 0) 
	    in
	      for i = 0 to Array.length p_atomic_reg.Region.e_control -1 do
		if (not (Counter.eq p_atomic_reg.Region.e_control.(i) (Counter.make_counter 0)) && 
		    (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i)).EnvAutomaton.task = true) 
		then
		  task_ctr := Counter.plus !task_ctr p_atomic_reg.Region.e_control.(i)
	      done ;
	      Counter.geq (!task_ctr) (Counter.make_counter 2)
	  end
      | _ -> false

  let exists_event automaton reg = 
    match reg with
	Region.ParallelAtomic p_atomic_reg -> 
	  begin
	    let event_flag = 
	      if (is_event reg) 
	      then ref true
	      else 
		ref false
	    in
	      for i = 0 to Array.length p_atomic_reg.Region.e_control -1 do
		if (not (Counter.eq p_atomic_reg.Region.e_control.(i) (Counter.make_counter 0)) && 
		    (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i)).EnvAutomaton.event = true) 
		then
		  event_flag := true
	      done ;
	      !event_flag
	  end
      | _ -> false

  let op_is_task_op auto reg op = 
    match op with
	ProgramOperation _ -> is_task reg
      | AutomatonOperation (i,_) ->
	  begin
	    match reg with
		Region.ParallelAtomic p_atomic_reg -> 
		  begin
		    let i_node = EnvAutomaton.getNodeFromId auto i in
		      (BiDirectionalLabeledGraph.get_node_label i_node).EnvAutomaton.task = true
		  end
	      | _ -> failwith "this cannot happen in op_is_task_op!"
	  end
	    
  let tar_enabled_ops automaton reg = 
    let rv = _tar_enabled_ops automaton reg in
      if (Options.getValueOfBool "alt" = false || (exists_atomic automaton reg) ) then rv
      else
	(* ASSERT : no one is atomic ... *)
	(* rules: in decreasing priority 
	   1. multiple tasks -> []
	   2. >1 event -> all nothings, events
	   3. True -> everything
	*)
	begin
	  match reg with
	      Region.ParallelAtomic p_atomic_reg ->
		begin
		  if (exists_multiple_tasks automaton reg) 
		  then []
		  else 
		    if (exists_event automaton reg) 
		    then 
		      List.filter (fun op -> not (op_is_task_op automaton reg op)) rv
		    else
		      rv
		end
	    | _ -> rv     
	end

  let initialize_abstraction () =
    PredTable.load_abs ();
    if (Options.getValueOfBool "cf") = false then DataLattice.initialize ()

  (* Saving final abs from tree *)
  let save_region_abs reg = 
    try 
      let l_id = C_SD.location_coords (location_coords_of_region reg) in
      let preds = PredTable.bdd_support (extract_abs_data_reg reg).Region.bdd in
	PredTable.add_local_preds l_id preds
    with _ -> M.msg_string M.Normal "Warning: Save failed!"


  (* if abs_conc_flag then create an abstract region else concrete 
   * if err_init_bool then EveryStack (error region) else emptyStack (init region) *)
  let ac_create_region abs_conc_flag loclist err_init_bool seedPreds initialStatePredicate =
    let _ = 
      if (Options.getValueOfBool "skipfun") then 
	match !check_races with
	    Some lv -> 
	      begin
		Hashtbl.replace PredTable.all_important_lvals_table lv true;
		PredTable.globally_important_lvals := lv::!PredTable.globally_important_lvals
	      end
	  | _ -> ()
    in
    (* seedPreds and initialStatePredicate are disassembled into the set of atomic predicates 
    and then inserted into globally_used_preds array*)
    let atom_list = (P.getAtoms seedPreds) @ (P.getAtoms initialStatePredicate) in
    let ind_list = List.map (fun x -> addPred x) atom_list in
    let pred_indexes = List.filter ((<) (-1)) ind_list in
    let _ = 
      let is = Misc.sort_and_compact (!PredTable.globally_useful_preds @ pred_indexes) in
      PredTable.globally_useful_preds := is in
    let _ = List.iter (fun i -> Hashtbl.add PredTable.pred_parity_table i [true]) pred_indexes in
    let (our_stk, our_le) = 
      if err_init_bool then (Region.EveryStack,DataLattice.top) else (Region.CallStack [],DataLattice.init) in
    let data_region = 
      if abs_conc_flag then
	    let our_bdd =
	      try convertPredToBdd initialStatePredicate
	      with Not_found -> failwith "create_region: init state pred not in the pred table" in
	      Region.Abstract { Region.bdd = our_bdd ; Region.lelement = our_le; Region.preds = pred_indexes ; }
      else
              Region.Concrete {Region.pred = initialStatePredicate}
    in
      assert (not (O.getValueOfBool "par"));
      if (Options.getValueOfBool "par") then failwith "ParallelAtomic not implemented" 
      else
	Region.Atomic {
	  Region.location = List.hd loclist ;
	  Region.data = data_region;
	  Region.stack = our_stk;
        }
  (* calls ac_create_region but creates abstract region always.*)
  let create_region = ac_create_region true

			
  (* initreg is an atomic region with abstract data region *)
  let create_parallel_atomic_region initreg automaton initctr =
    let ctrmap = Array.make (automaton.EnvAutomaton.number_of_states) (Counter.make_counter 0) in
      ctrmap.(EnvAutomaton.getIdFromNode automaton automaton.EnvAutomaton.autom_root) <- initctr ;
      let ir = match initreg with Some i -> i 
        | None -> (* fake initial region for program *) Region.Atomic {Region.location = C_SD.fake_location ; 
								       Region.stack = Region.CallStack [] ; 
								       Region.data =  Region.Abstract Region.true_adr; } 
      in
	match ir with
	    Region.Empty -> failwith "create_parallel_atomic_region called with empty region!"
	  | Region.Atomic atreg ->
	      Region.ParallelAtomic { Region.p_control = { Region.p_location = atreg.Region.location ; Region.p_stack = atreg.Region.stack ; } ;
	                              Region.e_control = ctrmap ;
                                      Region.p_data = atreg.Region.data ; }
	  | _ -> failwith "create_parallel_atomic_region called with strange region"
	      
      
      
  let ti_init_region () = 
    let main_fn = try List.hd (Options.getValueOfStringList "main") with _ ->
      failwith "Empty main fn list!" 
    in
    let abs_data_reg =
      let lv_list = ref [] in
      let f lv = lv_list := lv::!lv_list in
	C_SD.iterate_all_scope_lvals main_fn f;
	let constr = 
          let e_sym_l = List.map (fun lv -> E.Lval (E.make_symvar main_fn lv)) !lv_list in
          let e_lv_l = List.map (fun lv -> E.Lval lv) !lv_list in
            P.equate e_lv_l e_sym_l
	in
	  
	let pred_i_list =  (PredTable.get_all_pred_indexes ()) in
          abstract_predicate constr pred_i_list        
    in     
    let make_reg loc = 
      Region.AtomicTI 
	{ Region.ti_location_prev = loc; 
          Region.ti_location = loc;  
          Region.ti_stack = Region.CallStack [];
          Region.ti_data = abs_data_reg;}
    in
    let all_locations = C_SD.lookup_all_locations main_fn in
      List.map make_reg all_locations
        


  let print_stats fmt () =
    M.msg_string M.Normal ("Total number of non-post queries = "^(string_of_int (!statCoveredQueries))^" ");
    M.msg_string M.Normal ("Total number of cached queries = "^(string_of_int (!statTotalCached))^" ");
    M.msg_string M.Normal ("Total number of queries = "^(string_of_int (!statTotalNumberOfQueries))^" ");
    M.msg_string M.Normal ("Worst-case number of post queries = "^(string_of_int (!statWorstCaseQueriesForPost))^" ");
    M.msg_string M.Normal ("Actual number of post queries = "^(string_of_int (!statActualQueriesForPost))^" ");
    M.msg_string M.Normal ("NonDC number of post queries = "^(string_of_int (!statNonDCQueriesForPost))^" ");
    M.msg_string M.Normal ("Assume post queries = "^(string_of_int (!statAssumeQueriesForPost))^" ");
    M.msg_string M.Normal ("Total posts = "^(string_of_int (!statNumPost))^" ");
    M.msg_string M.Normal ("Total assume posts = "^(string_of_int (!statNumAssumePost))^" ");

      M.msg_string M.Normal ("Total foci queries = "^(string_of_int (!TheoremProver.stats_nb_foci_queries))^" ");
   
    M.msg_string M.Normal ("List of predicates: ");
    printTableOfPreds ();
    M.msg_string M.Normal 
      ("Maximum number of predicates active together (discounting scope) = "^(string_of_int (!statsMaxNumberOfPredsActive))^"\n");
    M.msg_string M.Normal ("Functions visited:");
    Hashtbl.iter (fun x y -> print_string (x^" , ")) statVisitedFunctionTable;
    dump_abs (); ()

  let print_abs_stats () = 
    M.msg_string M.Normal ("TP Queries:" ^ (string_of_int !statActualQueriesForPost));
    M.msg_string M.Normal ("Reached Cubes:" ^ (string_of_int !statReachedCubes));
    dump_abs ();
    ()



  (* **************** CONCURRENCY **************** *)

  (* this function will reset all the appropriate global data structures ... *)

  let reset () = 
    Hashtbl.clear theoremProverCache;
    Hashtbl.clear predTbl;
    Hashtbl.clear predTbl_deep; 
    Hashtbl.clear unchangedPredTable;
    Hashtbl.clear iasm_table;
    (* [RUPAK] Do not reset the predicates
       PredTable.tableOfPreds := [];
       predIndex := -1;
    *)
    Absutil.reset_ret_pointers ();
    last_loc_pointer := (None);
    assume_pred_ref := None;
    ()

  let exit_abstraction () = PredTable.kill_predbdd ()

  (*  let iter_global_variables = C_SD.iter_global_variables
  *)
  let iter_global_variables = C_SD.iterate_all_lvals



  type access_type = Anone | Aread | Awrite | Aboth
  let accessing op v =
    let reads, writes = C_SD.reads_and_writes_at_edge op in
      match (List.mem v reads, List.mem v writes) with
	| false, false -> Anone
	| false, true -> Awrite
	| true, false -> Aread
	| true, true -> Aboth

  let get_all_writes op =
    snd (C_SD.reads_and_writes_at_edge op)

  let get_all_reads op =
    fst (C_SD.reads_and_writes_at_edge op)

  let reads_and_writes op = C_SD.reads_and_writes_at_edge op 

  let globally_important_lvals () = !PredTable.globally_important_lvals 

  let aliasesOf lval = if can_escape lval then ( lval::(AliasAnalyzer.getAllAliases lval)) else []

  let eq_addr_constraint_region lval1 lval2 = 
    let p = 
      P.Atom 
	(E.Binary 
	   (E.Eq, 
	    E.addressOf (E.Lval lval1),
	    E.addressOf (E.Lval lval2)))
    in
      predicate_to_region p

  (* this should return the list of possible aliases in the whole program, of some lval 
     maybe we should keep a Hashtbl or something and query that every time 
  *)

  let make_multiply_instantiable_region r1 r2 lval1 lval2 = 
    M.msg_string M.Debug "In make_multiply instantiable\n\n" ;
    M.msg_string M.Debug ("r1 is"^(Region.to_string r1)) ;
    M.msg_string M.Debug ("r2 is"^(Region.to_string r2)) ;
    let rename_fun = new_name (Absutil.brand_new_suffix ()) in
    let eq_addr_const_reg = eq_addr_constraint_region lval1 (E.alpha_convert_lval rename_fun lval2) in 
      Region.cap 
	(Region.cap !assumed_invariants_region eq_addr_const_reg)
	(Region.cap r1 (Region.rename_vars r2 rename_fun))
	
	
  (* the below function should ONLY be called on two regions known to be such that:
     1. lval_1, lval_2 are such that they are accessed in region r1 r2 respectively
     2. neither of the lvals is a lock.
  *)
  let multiply_instantiable r1 r2 lval1 lval2 =
    let r = make_multiply_instantiable_region r1 r2 lval1 lval2 
    in
      if Region.is_empty r then None else Some r

  let get_modifies automaton reg =
    match reg with
      | Region.Empty -> (([(* program read *)], [(*program write*)]), [])
      | Region.ParallelAtomic p_at ->
	  begin
	    let rec exists_atomic auto ctrmap i =
	      if i = Array.length ctrmap then false 
	      else
		if (not (Counter.eq ctrmap.(i) (Counter.make_counter 0) ) 
		    && (BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId auto i)).EnvAutomaton.atomic) then
		  true
		else
		  exists_atomic auto ctrmap (i+1)
	    in
            let is_atomic = exists_atomic automaton p_at.Region.e_control 0 in
	    let progreads_and_writes =
	      if (is_atomic || C_SD.is_atomic p_at.Region.p_control.Region.p_location) then ([],[]) 
              else (* if the current location is atomic, then don't consider its reads/writes *)
		List.fold_left
		  (fun (lvr, lvw) edge -> 
		     let (r, w) = C_SD.reads_and_writes_at_edge edge in (Misc.union r lvr, Misc.union w lvw)) ([],[])
		  ((* find all the enabled program moves at this program location *)enabled_ops reg) 
	    in
	    let lvlist = ref [] in
              if is_atomic then () 
              else begin
		for i = 0 to automaton.EnvAutomaton.number_of_states -1 do
		  if (not (Counter.eq p_at.Region.e_control.(i) (Counter.make_counter 0)) && 
		      (not ((BiDirectionalLabeledGraph.get_node_label (EnvAutomaton.getNodeFromId automaton i)).EnvAutomaton.atomic))) then
		    let autom_modifies = 
		      (List.map (fun l -> (i,l))
			 (Misc.union 
			    (EnvAutomaton.getModifiesFromId automaton i)
			    (EnvAutomaton.getModifiesOnEdge automaton i)))
		    in
		      
		      lvlist := Misc.union autom_modifies !lvlist
		done;
	      end ;
	      (progreads_and_writes, !lvlist)
	  end
      | _ -> failwith "get_modifies: requires ParallelAtomic"
	  
	  
	  
	  (* **************** END concurrency **************** *)
	  (************* FOR BLASTPCC ************************)
	  (* for m4 -- include blast-pcc.ml *)
	  include(blast-pcc.ml)

  (************* START test generation  ************************)
  let genTests reg =
    let formals = match (C_SD.lookup_formals (List.hd (Options.getValueOfStringList "main"))) with
        C_SD.Fixed f -> f | C_SD.Variable f -> f
    in
    let is_arg a =
      let _is_arg_lval l = 
        match l with
            E.Symbol sym -> List.mem sym formals
          | _ -> false
      in
	match a with E.Lval l -> _is_arg_lval l
          | E.Chlval (l,s) -> (* _is_arg_lval l *) false
    in
    let is_unknown a =
      let _is_unknown_lval l = 
        match l with
            E.Symbol sym ->  Misc.is_prefix "BLAST_UNKNOWN@" sym 
          | _ -> false
      in
	match a with E.Lval l -> _is_unknown_lval l
          | E.Chlval (l,s) -> _is_unknown_lval l
    in

    let pred = extract_pred reg in
    let sym_to_val_list = TheoremProver.generateTest pred in
      (* sort the unknowns *)
    let unknowns = List.sort (fun (a,b) (c,d) ->
                                match (a,c) with
                                    (E.Chlval (l,s1) , E.Chlval (l', s2)) -> - (String.compare s1 s2) (* sort in decreasing order, since the
                                                                                                                           ctrex analysis goes backwards *)
                                  | _ -> failwith ("Trouble sorting unknowns! "^(E.toString a)^(E.toString c))
                             )
	             (List.filter (fun (a,b) -> is_unknown a) sym_to_val_list) in
    let args = List.filter (fun (a,b) -> is_arg a) sym_to_val_list  in
      (List.map (fun a -> let ea = E.Lval (E.Symbol a) in (ea, try List.assoc ea args with Not_found -> 0.0 (* any value *))) formals  ) @ unknowns 
	

  (* END test generation stuff *)


  type spec = unit
  type spec_result =
    | Yes
    | No
    | Maybe of P.predicate
  let summary_post _ = failwith "summary_post not implemented"
  let init_spec = summary_post
  let build_spec = summary_post
  let check_spec = summary_post

end

