(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open BlastArch
open Ast

(** Module for simplifying formula based on a set of known (non-)equalities,
    which can be extracted from a predicate which is assumed to be true.
    Contains in addition a simplification function that performs elementary
    simplifications *)
module Constraint_Simplifier =
struct
  
  type e = Expression.t

  (** Type to represent a set of known facts (facts that are
      assumed to be true *)
  type knowledge = {
    eq_classes : (e, e) Hashtbl.t ;
    mutable ne_facts : (e * e) list ;
    mutable ne_pairs : (e * e) list
  }

  (** Type to represent a fact *)
  type fact =
      Eq of (e * e)
    | Ne of (e * e)

  (** Answer given when querying a knowledge *)
  type answ = Known_Eq | Known_Ne | Unknown

  (** Create a new, fresh set of known facts *)
  let new_knowledge () = { eq_classes = Hashtbl.create 101 ;
			   ne_facts = [] ;
			   ne_pairs = [] }

  let eq_class_of_expr kn = fun e ->
    try Hashtbl.find kn.eq_classes e
    with Not_found -> e

  let all_eq_of_expr kn = fun a ->
    a :: (Hashtbl.fold (fun x c' l -> if c' = a then x :: l else l) kn.eq_classes [])

  let set_class_for_expr kn = fun a -> fun c ->
    let old_a = all_eq_of_expr kn a in
    let old_c = all_eq_of_expr kn c in
    let ne_a = Misc.map_partial (function (x,y) -> if x = a then Some y else if y = a then Some x else None) kn.ne_facts in
    let ne_c = Misc.map_partial (function (x,y) -> if x = c then Some y else if y = c then Some x else None) kn.ne_facts in
    let new_ne_pairs_for_c = List.flatten (List.map (fun c' -> List.flatten (List.map (function x -> List.map (fun x' -> if (x'<c') then (x',c') else (c',x')) (all_eq_of_expr kn x)) ne_a)) old_c) in
    let new_ne_pairs_for_a = List.flatten (List.map (fun a' -> List.flatten (List.map (function x -> List.map (fun x' -> if (x'<a') then (x',a') else (a',x')) (all_eq_of_expr kn x)) ne_c)) old_a) in
    let _ = kn.ne_pairs <- Misc.compact (kn.ne_pairs @ new_ne_pairs_for_c @ new_ne_pairs_for_a) in
    let _ = List.iter (fun x -> Hashtbl.replace kn.eq_classes x c) old_a in
      ()

  (** Add the provided facts to the set of known facts *)
  let learn kn facts =
    let (eq_facts, ne_facts) = List.partition (function Eq _ -> true | Ne _ -> false) facts in
    let handle_eq_fact = function Eq (a, b) ->
      let _ = Message.msg_string Message.Debug ("learn_facts: Learn "^(Expression.toString a)^" == "^(Expression.toString b)) in
      let a_class = eq_class_of_expr kn a in
      let b_class = eq_class_of_expr kn b in
	(if a_class <> b_class then set_class_for_expr kn b a) | _ -> () in
    let _ = List.iter handle_eq_fact eq_facts in
    let rec handle_ne_fact = function Ne (a, b) ->
      if a > b then
	handle_ne_fact (Ne (b,a))
      else if not (List.mem (a,b) kn.ne_facts) then
	let _ = Message.msg_string Message.Debug ("learn_facts: Learn "^(Expression.toString a)^" != "^(Expression.toString b)) in
	let _ = kn.ne_facts <- (a,b) :: kn.ne_facts in
	let all_a = all_eq_of_expr kn a in
	let all_b = all_eq_of_expr kn b in
	let new_pairs = List.map (function (x,y) -> if (x < y) then (x,y) else (y,x)) (Misc.cross_product all_a all_b) in
	let _ = kn.ne_pairs <- Misc.union new_pairs kn.ne_pairs in
	  ()  | _ -> () in
    let _ = List.iter handle_ne_fact ne_facts in
      ()

  (** Give a string representation of a set of known facts *)
  let string_of_knowledge kn =
    let ts = Expression.toString in
      "Known Equalities:\n" ^
	begin
	  Hashtbl.fold
	    (fun a b s -> s^"   "^(ts a)^" == "^(ts b)^"\n")
	    kn.eq_classes ""
	end ^
	"Ne facts:\n" ^
	begin
	  List.fold_left (fun s -> function (a,b) -> s^"   "^(ts a)^" != "^(ts b)^"\n") "" kn.ne_facts
	end ^
	"Known non-equalities:\n" ^
	begin
	  List.fold_left (fun s -> function (a,b) -> s^"   "^(ts a)^" != "^(ts b)^"\n") "" kn.ne_pairs
	end

  (** Query the set of known facts to know what we know about two values *)
  let what_about kn = fun a b ->
    let a_class = eq_class_of_expr kn a in
    let b_class = eq_class_of_expr kn b in
      if a_class = b_class then
	let _ = Message.msg_string Message.Debug ("what_about: It is known that "^(Expression.toString a)^" == "^(Expression.toString b)) in
	  Known_Eq
      else
	if List.exists (fun (x,y) -> ((x,y) = (a,b)) || ((x,y) = (b,a))) kn.ne_pairs then
	  let _ = Message.msg_string Message.Debug ("what_about: It is known that "^(Expression.toString a)^" != "^(Expression.toString b)) in
	    Known_Ne
	else
	  Unknown

  (** Remove implications that can be deduced from known facts *)
  let solve_facts kn =
    let rec _solve p = match p with
	Predicate.And pl -> Predicate.And (List.map _solve pl)
      | Predicate.Or pl -> Predicate.Or (List.map _solve pl)
      | Predicate.Not p' -> Predicate.Not (_solve p')
      | Predicate.Implies ((Predicate.Atom e) as p1, p2) ->
	  begin
	    match e with
		Expression.Binary (Expression.Eq, a, b) ->
		  (match what_about kn a b with
		       Known_Eq -> _solve p2
		     | Known_Ne -> Predicate.True
		     | Unknown -> Predicate.Implies (p1, _solve p2))
	      | Expression.Binary (Expression.Ne, a, b) ->
		  (match what_about kn a b with
		       Known_Eq -> Predicate.True
		     | Known_Ne -> _solve p2
		     | Unknown -> Predicate.Implies (p1, _solve p2))
	      | _ -> Predicate.Implies (p1, _solve p2)
	  end
      | Predicate.Implies (p1, p2) -> Predicate.Implies ((_solve p1), (_solve p2))
      | Predicate.Iff (p1, p2) -> Predicate.Iff ((_solve p1), (_solve p2))
      | Predicate.Next p' -> Predicate.Next (_solve p')
      | Predicate.Atom e -> p
      | _ -> p
    in
      _solve

  (** Return the list of facts that are implied by the given prediate *)
  let extract_facts p =
    let rec _extract positive p = match p with
	Predicate.And pl -> if positive then List.flatten (List.map (_extract positive) pl) else []
      | Predicate.Or pl -> if not positive then List.flatten (List.map (_extract positive) pl) else []
      | Predicate.Not p' -> _extract (not positive) p'
      | Predicate.Atom e ->
	  begin
	    match e with
		Expression.Binary (Expression.Eq, a, b) -> if positive then [Eq (a,b)] else [Ne (a,b)]
	      | Expression.Binary (Expression.Ne, a, b) -> if positive then [Ne (a,b)] else [Eq (a,b)]
	      | _ -> []
	  end
      | _ -> []
    in
      _extract true p

  (** Simplify a formula by extracting predicates, and then removing useless implications *)
  let extract_and_solve p =
    let kn = new_knowledge () in
    let _ = learn kn (extract_facts p) in
    let _ = Message.msg_string Message.Debug ("extract_and_solve: Hereafter is what we learned:"^(string_of_knowledge kn)) in
      solve_facts kn p

  (** Simplify a formula *)
  let rec simple_prune cstr = match cstr with
      Predicate.Implies (p1, p2) ->
	let (p1', p2') = ((simple_prune p1), (simple_prune p2)) in
	  (match (p1', p2') with
	       (Predicate.False, _) | (_, Predicate.True) -> Predicate.True
	     | (Predicate.True, _) -> p2'
	     | _ -> Predicate.Implies (p1', p2'))
    | Predicate.Not p ->
	let p' = (simple_prune p) in
	  (match p' with Predicate.True -> Predicate.False | Predicate.False -> Predicate.True | _ -> Predicate.Not p')
    | Predicate.And pl -> 
	let pl' = List.filter (fun x -> x <> Predicate.True) (List.map simple_prune pl) in
	  (match pl' with
	       [] -> Predicate.True
	     | _ -> if List.exists (fun x -> x = Predicate.False) pl' then Predicate.False else Predicate.And pl')
    | Predicate.Or pl -> 
	let pl' = List.filter (fun x -> x <> Predicate.False) (List.map simple_prune pl) in
	  (match pl' with
	       [] -> Predicate.False
	     | _ -> if List.exists (fun x -> x = Predicate.True) pl' then Predicate.True else Predicate.Or pl')
    | Predicate.Iff (p1, p2) ->
	let (p1', p2') = ((simple_prune p1), (simple_prune p2)) in
	  (match (p1', p2') with
	       (l, r) when l = r -> Predicate.True
	     | (Predicate.False, Predicate.True) | (Predicate.True, Predicate.False) -> Predicate.False
	     | (Predicate.False, p) | (p, Predicate.False) -> Predicate.Implies (p, Predicate.False)
	     | (Predicate.True, p) | (p, Predicate.True) -> p
	     | _ -> Predicate.Iff (p1', p2'))
    | Predicate.Atom ex -> begin
	match ex with
	    Expression.Binary (Expression.Eq, l, r) when l = r -> Predicate.True
	  | Expression.Binary (Expression.Ne, l, r) when l = r -> Predicate.False
	  | _ -> Predicate.Atom ex
      end
    | Predicate.Next p -> Predicate.Next (simple_prune p)
    | Predicate.All (s, p) -> 
	let p' = simple_prune p in
	  (match p' with Predicate.True -> Predicate.True | Predicate.False -> Predicate.False | _ -> Predicate.All (s,p'))
    | Predicate.Exist (s, p) -> 
	let p' = simple_prune p in
	  (match p' with Predicate.True -> Predicate.True | Predicate.False -> Predicate.False | _ -> Predicate.Exist (s,p'))
    | Predicate.False -> Predicate.False
    | Predicate.True -> Predicate.True

end
