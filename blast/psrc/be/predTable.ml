(*  BLAST is a tool for software model checking.
This file is part of BLAST.
Software model checking tool BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

open BlastArch
module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module Counter = Ast.Counter
module Stats = Bstats
module M = Message
module O = Options

 (* include PredBdd *)

(* let x = 22 *)

module Make_PredTable = 
  functor(PredBdd : PREDBDD with module C_SD.Command = BlastCSystemDescr.C_Command) ->
struct
  
(* module C_SD = BlastCSystemDescr.C_System_Descr*)

include PredBdd
(* [Greg] Very dirty hack because we renamed Stats to VampyreStats in
   vampyre...  We should clean this stats business anyway. *)
  
let post_split_table : ((int * int) , int list) Hashtbl.t = Hashtbl.create 109
let loc_pred_table : ((int * int) , int list) Hashtbl.t  = Hashtbl.create 1009
let pred_dep_table : (int, int list) Hashtbl.t = Hashtbl.create 1009
let pred_parity_table : (int, bool list) Hashtbl.t = Hashtbl.create 109
let fun_pred_table : (int, int list) Hashtbl.t = Hashtbl.create 1009
let bat_counter = ref 0


let lookup_pred_parity p = try Hashtbl.find pred_parity_table p with Not_found -> []

let add_pred_parity p par = 
  let pp = lookup_pred_parity p in
  if not (List.mem par pp) then 
    (M.msg_string M.Debug "Adding new parity ...";
     block_a_t_flag := true;
     Hashtbl.replace pred_parity_table p (par::pp))

  

let update_pred_dep_table prev_preds p = 
  let c = try Hashtbl.find pred_dep_table p with Not_found -> [] in
  let c' = Misc.sort_and_compact (prev_preds@c) in
  Hashtbl.replace pred_dep_table p c'

let lookup_pred_dep_table p = 
  try Hashtbl.find pred_dep_table p with Not_found -> []
  (* tells whether we're interested in a pred being true/false or both of a variable*)
    (* this variable tracks the number of times some interesting refinement fact has been added ...
       so if it changes after a call to focus, then we know that we need to keep going,
       if it doesnt, then we know that we are stuck *)

    (* this next function will be called if craig=3, just before calling make_conjunct_bdd in postbdd *)

  let keep_pred_parity (i,b) =
    let rv =
      try
	let pl = Hashtbl.find pred_parity_table i in
	  List.mem b pl 
      with Not_found -> false
    in

      if not rv then M.msg_string M.Debug (Printf.sprintf "delete fact: %d %b" i b);
      rv

  let refine_counter = ref 0

  let refined () = 
    refine_counter := !refine_counter + 1

  let last_refine_counter = ref 0
  let refine_check () = 
    if !last_refine_counter = !refine_counter 
    then
      false
    else
      begin
	last_refine_counter := !refine_counter;
	true
      end

  let globally_useful_preds = ref ([] : int list)

  let blast_error_lval = Expression.Symbol (O.getValueOfString "errorvar")
		     (* Expression.Symbol "__BLAST_error" *)
			   
  let globally_important_lvals = ref ([blast_error_lval] : Expression.lval list)
  let globally_important_lvals_table:(Expression.lval, bool) Hashtbl.t = Hashtbl.create 109
  let _ = Hashtbl.replace globally_important_lvals_table blast_error_lval true

  let ( all_important_lvals_table :(Expression.lval, bool) Hashtbl.t)
    = Hashtbl.create 109 

(**********************************************************************************************************)

(* the main thing that deals with whether or not we should actually take an edge or not.
   ADD notes about various invariants here.
   Theorem: It is sound to skip a call "lv = f(elist)" if SP.\varphi.mods = \varphi.
   By sound I mean treating such a statement as "skip" ...as you can see, this is trivially true.
   we shall skip such calls if f modifies NOTHING that can possibly be in \varphi
*)   
   
	
  let __SKIPFunctionName =
    BlastCSystemDescr.C_System_Descr.__SKIPFunctionName
  let __NotImplementedFunctionName =
    BlastCSystemDescr.C_System_Descr.__NotImplementedFunctionName
  let __BLAST_DispatchFunctionName = 
    BlastCSystemDescr.C_System_Descr.__BLAST_DispatchFunctionName

  (* tables for skipping functions: both are sets of fnames: i.e. fname -> bool Hashtbl *)
  let always_take_fun_table = Hashtbl.create 101
  let always_skip_fun_table = Hashtbl.create 101

  let add_skip_fun fname =
    M.msg_string M.Debug ("Adding skip_fun: "^fname);
    Hashtbl.replace always_skip_fun_table fname true
	
  let delete_skip_fun fname =
    try
      M.msg_string M.Debug ("Removing skip_fun: "^fname);
      Hashtbl.remove always_skip_fun_table fname
    with _ -> ()
let stats_nb_take_funs = ref 0
  let add_take_fun fname =
    if not (Hashtbl.mem always_take_fun_table fname) then
       begin
        stats_nb_take_funs := 1 + !stats_nb_take_funs;
        M.msg_string M.Debug ("Adding take_fun: "^fname);
        Hashtbl.replace always_take_fun_table fname true;
        delete_skip_fun fname
       end   

  let always_take fname = Hashtbl.mem always_take_fun_table fname
  let always_skip fname = Hashtbl.mem always_skip_fun_table fname
      
  (* ONLY USE: delete_skip_fun, add_skip_fun, always_take, always_skip *)
      

	
(*********************************************************************************************************)    
  (* which globals are modified by a function: fname -> lval list table *)	
  let relevant_globals_mod_table = Hashtbl.create 101

  let relevant_fields_table = Hashtbl.create 37
				     
  let update_global_lvals lv = 
    if C_SD.is_global lv && not (Hashtbl.mem globally_important_lvals_table lv)
    then 
      (M.msg_string M.Normal ("Adding glob_imp_lval: "^(Expression.lvalToString lv));
       globally_important_lvals := lv::!globally_important_lvals;
       Hashtbl.replace globally_important_lvals_table lv true) 
	
let ignorable_globals = (* JHALA TBPLDI *)
  List.map (fun s -> Expression.Symbol s) ["__BLAST_interrupt_enabled";"__BLAST_task_enabled"] 

(* THIS FUNCTION IS CALLED WHEN lv belongs to a PREDICATE being added.
   In this case, the field is added permanently -- not flushed like during tproj/block_at *)
let add_relevant_fields lv =
  let lv_fields = (Expression.fields_of_lval lv) in
    List.iter (fun fld -> Hashtbl.replace relevant_fields_table fld true) lv_fields;
    ignore (List.map (C_SD.add_field_to_unroll false) lv_fields)

let is_relevant_field_image fld =
  Hashtbl.mem relevant_fields_table fld
    
(* we shall add all aliases of lvals in predicates -- i.e. the set of "relevant" lvals is closed under aliasing.
   Hence, if an assignment to an lval is such that the lval is irrelevant, then its really irrelevant *)
      

let update_imp_lv_table pred =
  let closure_fun = 
     if (O.getValueOfBool "fldunroll") then (C_SD.lvalue_closure_stamp false)
     else (C_SD.lvalue_closure) in
  let add_mod lv =
    (* let lv_aliases = [] AliasAnalyzer.get_lval_aliases lv in*)
    let proc lv' =
      if C_SD.is_global lv' then
	begin
	  (* 1. add to copy back/hooks etc etc *)
	  let f_list = C_SD.global_lvals_mod_by lv' in 
	  (* As this function is used for image-computation ONLY,
           * we don't have to take closure, something is "relevant"
           * iff it appears in a predicate, and otherwise its NOT 
           * relevant 
          let lv'_clos = closure_fun lv' in *)
	  let _ =
	    List.iter
	      (fun lv'' ->
		 List.iter (fun fname ->
			      ignore (Misc.hashtbl_check_update
					relevant_globals_mod_table
					fname
					lv''))
		 f_list)
	      [lv'] (* _clos *)
	  in
	  (*2. skipfun processing *)
	  let _ = List.iter add_take_fun f_list in
          let _ = M.msg_string M.Normal ("Now take_fun size: "^(string_of_int !stats_nb_take_funs)) in  
          ()
	end
      else (* lv' is local *)
	begin
	  let f_list = C_SD.may_mod_by lv' in
	    List.iter delete_skip_fun f_list
	(* make sure we don't skip functions that may modify this lv (as a copyback "effect") *)
	end
    in 
    (* RJ: no need to close under aliases as mod_by table is ALREADY closed 
     * under aliases. i.e. if fn mods lv' which is aliased to lv, then the mod
     * by for lv includes fn -- and so we shall add lv to the
     * relevant_globals_mod_table for fn *)
      List.iter proc [lv] (* (lv::lv_aliases) *)
  in
  let add_lv lv = 
    if (Hashtbl.mem all_important_lvals_table lv) then ()
    else 
      begin
	M.msg_string M.Debug ("Adding imp_lval:"^(Expression.lvalToString lv)); 
	Hashtbl.replace all_important_lvals_table lv true;
	add_relevant_fields lv;
	add_mod lv;
	if (C_SD.is_global lv) then 
	  if (Hashtbl.mem globally_important_lvals_table lv) 
	  then ()
	  else update_global_lvals lv 
      end;
  in
  List.iter add_lv (Predicate.allVarExps_deep pred)
            
    
  let ac_is_relevant_lval lv =
    if (List.mem lv ignorable_globals) then false
    else
      (if (O.getValueOfString "checkRace" <> "") then 
	 Hashtbl.mem globally_important_lvals_table lv
       else
	 Hashtbl.mem all_important_lvals_table lv)

  let is_relevant_lval lv =
        let slvs = Expression.lvals_of_expression (Expression.Lval lv) in
        let rv = List.exists ac_is_relevant_lval (lv::slvs) in
        rv
      
      
  let relevant_globals_mod fname =
    try
      Hashtbl.find relevant_globals_mod_table fname
    with Not_found -> []
	
      

  (* this will map locations to the predicates that are useful there 
      which is info gleaned from the interpolants *)
   (* This list doesn't seem to be used anywhere :
      let globally_useful_pred_list :  list ref  = ref []
     predicates who's value we want to know pretty much everywhere ... 
     e.g. spec predicates *)

 

    
(**************Tables for checking where predicates are modified******************)
let pred_affected_table :((C_SD.edge_id_t * int),bool) Hashtbl.t = Hashtbl.create 101
let update_pred_affected_table e_id p_idx = Hashtbl.replace pred_affected_table (e_id,p_idx) true
let is_pred_affected e_id p_idx = Hashtbl.mem pred_affected_table (e_id,p_idx)

(*********************************************************************************)
let pred_global_table = Hashtbl.create 101

let is_pred_global i = 
  try Hashtbl.find pred_global_table i with Not_found ->
    let p = getPred i in
    let rv = List.exists C_SD.is_global (Predicate.allVarExps_deep p) in
    Hashtbl.replace pred_global_table i rv;
    rv

(*********************************************************************************)

(* the following function scans the code and fills up a the constant table which is used in predH > 0 *)
  
let isConstant e = 
  match e with 
      Expression.Constant c -> true
    | _ -> false

let lval_constant_table = Hashtbl.create 1009

let make_const_table () = 
  (* we trawl over the whole CFA and learn for every lval expression all the constant exp *)
  let module LocationSet =
    Set.Make (struct
                type t = int 
                let compare x y =  compare x  y
              end) in
    
  let add_to_table (x,y) = 
    if (isConstant y && (not (isConstant x))) then
      begin
	let l = try Hashtbl.find lval_constant_table x with Not_found -> [] in
	  if (List.mem y l) then () else Hashtbl.replace lval_constant_table x (y::l)
	end
    else ()
  in
  let process_statement s = 
    match s with
	C_SD.Command.Expr e -> 
	  List.iter (fun p -> add_to_table p) (Expression.assignments_of_expression e)
      | _ -> ()
  in
  let sprint_edge e = 
    let process_atom p = 
      match p with
	  Predicate.Atom a ->
	    begin
	      match a with
		  Expression.Binary (op,e1,e2) -> (List.iter add_to_table [(e1,e2);(e2,e1)])
		| _ -> ()
	    end
	| _ -> ()
    in
    let (l1,c,l2) = (C_SD.get_source e, C_SD.get_command e, C_SD.get_target e) in 
      match c.C_SD.Command.code with
	  C_SD.Command.Block l -> List.iter process_statement l
	| C_SD.Command.Pred p ->  
	    if (O.getValueOfInt "predH" > 1) 
	    then 
	      List.iter (process_atom) (Predicate.getAtoms p)
	    else ()
	| _ -> ()
  in 
  let sprint_function fname =
    let already_visited_source_locations = ref LocationSet.empty in
    let rec sprint_location_and_edges loc =
      let (_,loc_id) = C_SD.location_coords loc in
        if (LocationSet.mem loc_id !already_visited_source_locations) 
	  then
            ()
        else
          begin
            let outgoing_edges = List.rev (C_SD.get_outgoing_edges loc) in
	    let _ = List.iter sprint_edge outgoing_edges in
	      already_visited_source_locations := (LocationSet.add (snd (C_SD.location_coords loc)) !already_visited_source_locations) ;
	      (List.iter (fun e -> sprint_location_and_edges (C_SD.get_target e)) outgoing_edges)          end 
    in
      sprint_location_and_edges (C_SD.lookup_entry_location fname)
  in
    List.iter sprint_function (C_SD.list_of_functions ());
    let print_entry lv constlist = 
      M.msg_string M.Debug (Printf.sprintf " %s : " (Expression.toString lv));
      List.iter (fun x -> M.msg_string M.Debug (Printf.sprintf " %s,"(Expression.toString x))) constlist;
      M.msg_string M.Debug "\n"; ()
    in
      Hashtbl.iter print_entry lval_constant_table 




(* total hack -- this should be replaced with the 
   function that pulls lvals out of bigger expressions from the psrc directory ! *)
      

let rec getConstants p = 
  let rec getConstants_exp e = 
    match e with
	Expression.Binary(_,e1,e2) -> List.flatten (List.map getConstants_exp [e1;e2])
      | Expression.Unary(Expression.UnaryMinus,Expression.Constant (Constant.Int i)) -> 
	  [e]
      | Expression.Constant(Constant.Int i) -> [e]
      | _ -> []
  in
    match p with
	Predicate.True -> []
      | Predicate.False -> []
      | Predicate.And l -> List.flatten (List.map getConstants l)
      | Predicate.Or l -> List.flatten (List.map getConstants l)
      | Predicate.Atom e -> getConstants_exp e
      | _ -> []

 

let constant_preds p = 
  let _ = M.msg_string M.Debug ("Finding Constant Preds for: "^(Predicate.toString p)^"\n") 
  in
 let lvals_of_predicate p = 
    match p with
	Predicate.Atom e -> 
	  begin
	    match e with
		Expression.Binary(op,e1,e2) -> [e1;e2]
	      | _ -> []
	  end
      | _ -> []
 in
  let current_pred_lvals = (lvals_of_predicate p) in
  let make_predicate v c = Predicate.Atom (Expression.Binary (Expression.Eq,v,c)) in
  let process_lval v = 
    try 
      let rv =  List.map (make_predicate v) (Hashtbl.find lval_constant_table v) in
	List.iter (fun x -> M.msg_string M.Debug ((Predicate.toString x)^"\n")) rv;
	rv
    with
	Not_found -> 
	  M.msg_string M.Debug ("lval not in lval_constant_table: "^(Expression.toString v));
	  []
  in
  let rv1 =  List.flatten (List.map process_lval (lvals_of_predicate p )) in
    (* in more generality *)
  (*let rv2 =
    let mk_pred c c' = Predicate.substitute [(c,c')] p in
    let pred_lv = getVarExps p in
      match pred_lv with
	  [h] -> 
	    begie
	      let pred_consts = getConstants p in
	      if (List.length pred_consts <> 1) then []
	      else 
		List.map (mk_pred (List.head pred_consts)) (Hashtbl.find lval_constant_table h)
	    end
  in    *)
    rv1(* @rv2 *)


 
    
 (** Add a new atomic predicate to the global table of
    predicates and introduce a new BDD variable corresponding
    to the index, if the predicate or its negation is not present in 
    the table already. Returns the index of the pred added == bdd variable
    corresponding to the predicate.
  *)
  let actual_addPred pred =
    let add_to_table pred =
    try Hashtbl.find hashTableOfPreds pred 
    with Not_found ->        
      begin
        let _negPred = Predicate.negate pred in
          try  Hashtbl.find hashTableOfPreds _negPred
          with Not_found ->
		  let rv = add_new_pred_to_table pred in  
		  let _ = update_imp_lv_table pred in
		  let ps = Predicate.toString pred in
		  M.msg_string M.Debug 
		    (Printf.sprintf "//addPred:%d: adding predicate \n %s;" rv ps);
		  M.msg_string M.Normal 
		    (Printf.sprintf "addPred: %d: (gui) adding predicate %s to the system" rv ps);
		  M.msg_string M.Error 
		    (Printf.sprintf "addPred: %d: (gui) adding predicate %s to the system" rv ps);
                  rv 
(*
	      | _ -> failwith "addPred: Attempt to add non-atomic predicate to predicate table"
*)
      end
    in
    let reflexcheck e = 
      match e with
	  Expression.Binary(_,e1,e2) -> e1 = e2 
	| _ -> false in
    match pred with
      Predicate.Atom (Expression.Lval (Expression.Symbol s)) when Misc.is_prefix "b_p__BS__" s -> -1 
    (* | Predicate.Atom e ->
        if ((reflexcheck e) || (Expression.allVarExps e = [])) then -1 
	else add_to_table pred *)
    | _ -> add_to_table pred

        
let addPred p = 
  let p' = Predicate.canonicize p in
  let _ = if (O.getValueOfInt "predH" <> 0 && O.getValueOfBool "constpred") 
          then ignore(List.map actual_addPred (constant_preds p')); in
  actual_addPred p'
    
let just_addPred p = 
  actual_addPred (Predicate.canonicize p) 

let add_pred_to_scope i =
  let p = getPred i in
  let p_lvals = Predicate.lvals_of_predicate p in
  let scopes = Misc.sort_and_compact (List.flatten (List.map C_SD.scope_of_lval p_lvals)) in
  let id_scopes = Misc.map_partial (fun fname -> (try Some (C_SD.get_fname_id fname) with _ -> None)) scopes in
  ignore(List.map (fun scp_id -> Misc.hashtbl_check_update fun_pred_table scp_id i) id_scopes);
  if List.mem "" scopes then Some(i) else None
      
  let getPredsContainingVar str =
    let containsVar p str =
      let e_str = Expression.Lval (Expression.Symbol str) in
      match p with 
        Predicate.Atom expr -> (List.mem e_str (Expression.allVarExps expr))
      |	_ -> false
    in
    let rec _go_through_list lst str accum =
      match lst with
        [] -> accum
      |	(_, p, _) :: rest -> if (containsVar p str) then _go_through_list rest str (p::accum) else
        _go_through_list rest str accum
    in
    _go_through_list (!tableOfPreds) str []

 
  (******************************************************************)

        

  (* Code to ask theorem prover queries and check satisfiability *)        
  let theoremProverCache : (string , bool) Hashtbl.t = Hashtbl.create 49033
                             
  (* Statistics: Number of queries to the Theorem Prover *)
  let statTotalNumberOfQueries = ref 1
  let statCoveredQueries = ref 1
  let statTotalCached = ref 0
  let statPostBddCalls = ref 0
  let statWorstCaseQueriesForPost = ref 0
  let statActualQueriesForPost = ref 0
  let statNonDCQueriesForPost = ref 0
  let statAssumeQueriesForPost = ref 0
  let statNumPost = ref 0
  let statNumAssumePost = ref 0
  let statReachedCubes = ref 0
  let statVisitedFunctionTable : (string, bool) Hashtbl.t  = Hashtbl.create 109

  let reset_abs_stats () = 
    statTotalCached := 0;
    statTotalNumberOfQueries := 1;
    statWorstCaseQueriesForPost := 0;
    statReachedCubes := 0;
    statActualQueriesForPost := 0;
    statAssumeQueriesForPost := 0;
    statNumPost := 0;
    statNumAssumePost := 0;
    statCoveredQueries := 0; 
    ()
  
  (****************************************************************************)
      
      
  exception SatisfiableException
    
  (* This is the main interface to the theorem prover. This takes
     a BDD and a list of predicates, and checks, for each cube of the BDD if
     the conjunction of the cube and the list of predicates is satisfiable.
  *)

  let isSatisfiable (bdd, plist) =
    try 
    begin
    M.msg_string M.Normal "In isSatisfiable..\n";
    M.msg_string M.Debug ("Bdd = "^(Predicate.toString (convertBddToPred bdd)));
    M.msg_string M.Debug "\nList of predicates = ";
    let rec loop lst = 
      match lst with [] -> () | a::rest -> (ignore (M.msg_string M.Debug (Predicate.toString a)); loop rest) 
    in
      loop plist;
      let rec _syntaxCheck cube alist =
        begin
	  try
            (match alist with
              [] -> false
            | (i, j) :: rest ->
                if ((cube.(j) <> 2) && (cube.(j) <> i)) then
                  true (* Unsatisfiable -- Easy win! *)
                else
                  _syntaxCheck cube rest)
with Invalid_argument s -> failwith ("Invalid_argument "^s^" here")
        end
      in
        
      let modeledPredIndexList = (* List of modeled predicates or their negations from plist *)
        let rec _origPreds plist orig = (* List of atomic predicates *)
          match plist with
              [] -> orig
            | a::rest ->
                begin
                  try
                    let i = getPredIndex a
                    in
                      _origPreds rest ((1,i) :: orig)
                  with
                      Not_found -> 
                        begin
                          try
                            let i = getPredIndex (Predicate.negate a)
                            in
                              _origPreds rest ((0,i) :: orig)
                          with 
                              Not_found ->
                                _origPreds rest orig
                        end
                end
        in
          _origPreds plist []
      in
      let _satworker plist cube =
        (* check if this cube conjoined with plist is satisfiable *)
        (* Note: The "cube" may be fake, so don't print it or try to convert
	   it to a predicate. Ugly hack, but works. So far.
	   *)
        (* Syntactic heuristic *)
        if (_syntaxCheck cube modeledPredIndexList) then
          ()
        else
          begin
            let _make_Pred (cube : int array) (plist : Predicate.predicate list) =
              let _ = M.msg_string M.Debug "HITHER" in
	      
	      let pref = ref [] in
                begin
                  for i = 0 to (Array.length cube - 1) do
                    if (cube.(i)==2) 
                    then () 
                    else 
                      begin
                         if (cube.(i)==1) then 
                           pref := (getPred i)::(!pref)
                         else
                           pref := (Predicate.negate (getPred i))::(!pref)
                      end
                   done;
                   Predicate.implies (Predicate.conjoinL ((!pref) @ plist)) (Predicate.False)
                end 
            in
            begin
                (*(ignore (Printf.printf "Asking query...%d\n" !statTotalNumberOfQueries));*)
              statTotalNumberOfQueries := !statTotalNumberOfQueries + 1;
              statCoveredQueries := !statCoveredQueries + 1;
		let eToCheck = _make_Pred cube plist
                in
                let _ = M.msg_string M.Debug "query-start" in
                    let ans = 
		      if (O.getValueOfBool "nocache") then 
			TheoremProver.queryExp eToCheck
		      else
			begin
			  statTotalCached := !statTotalCached + 1;
                          let es = Predicate.toString eToCheck in
                          try
                            
			    Hashtbl.find theoremProverCache (* eToCheck *)  es  
			  with Not_found -> 
			    let _ans = TheoremProver.queryExp  eToCheck in
			    Hashtbl.add theoremProverCache (*eToCheck*)  es  _ans;
			    statTotalCached := !statTotalCached - 1;
			    _ans
			end
		    in
		    let _ = M.msg_string M.Debug "query-end" in
		    if ans = false
                    then
                      raise SatisfiableException
                    else
                      ()
            end
          end
      in
        try
          if (CaddieBdd.bddEqual bdd bddZero) then
            false
          else 
            begin
              if (CaddieBdd.bddEqual bdd bddOne) 
              then
                let _tmpcube = (* build a temporary cube with nothing in it *)
                  Array.make (*size *)(getCurrentIndex ()+2) (* content *)2 (* Note: The size
									     is certainly a vast
									     overestimate, but this
									     is the easiest way 
									     The "+2" removes additional
									       complications relating
									       to index being -1 initially.
									       Note that this is a fake
									       cube -- we should neve
									       try to print it or convert it
									       to a predicate!

									       Ugly hack, but works. So far.
									       *)
                in
                  _satworker plist _tmpcube;
                  false
              else
                begin
                  ignore (CaddieBdd.bddForeachCube bdd (_satworker plist));
                  false
                end
            end
        with SatisfiableException ->
          true
       end
       with Out_of_memory -> failwith "isSatisfiable raises out of mem" 

let post_prover_cache = Hashtbl.create 49033

let askTheoremProver pred =
  statTotalNumberOfQueries := !statTotalNumberOfQueries + 1;
  if (O.getValueOfBool "nocache") then 
    TheoremProver.queryExp pred
  else
    (* It's tempting to replace the conversion, and just add predicates to the hash directly, but the experiments prove that the overhead of the string conversion makes it faster overall. *)
    let ps = Stats.time "toString" Predicate.toString pred in
    try
      let rv = Stats.time "lookup" Hashtbl.find post_prover_cache ps (* pred *) in
      statTotalCached := !statTotalCached + 1;
      rv
    with Not_found ->
      begin
	let t2 = TheoremProver.queryExp pred in
	Stats.time "add" Hashtbl.add post_prover_cache ps (* pred *) t2;
	t2
      end


   
(* Same as ask thm prover but assumes that you have told  the thm
  prover your  assumptions pred is the total predicate ie asm -> foo
  and  query is the foo ... theoretically you could pass just one arg 
 and pattern match but ...*)

let askTheoremProverContext pred query = 
  statTotalNumberOfQueries := !statTotalNumberOfQueries + 1;
  if (O.getValueOfBool "nocache") then 
    begin
      M.msg_string M.Debug (Predicate.toString query);
      TheoremProver.queryExpContext pred query
    end (* TBD: querycache : the caching is hideously broken! *)
    else
    (* It's tempting to replace the conversion, and just add predicates to the hash directly, but the experiments prove that the overhead of the string conversion makes it faster overall. *)
      let ps = Stats.time "toString" Predicate.toString pred in
      try 
        let rv = (Stats.time "lookup_cache" (Hashtbl.find post_prover_cache)
        ps (* pred *) ) in
        let _  = statTotalCached := !statTotalCached + 1 in
        rv
      with Not_found -> 
	begin
          let t2 = TheoremProver.queryExpContext pred query in
          Stats.time "lookup_cache_add" (Hashtbl.add post_prover_cache ps (*
          pred *) ) t2;
          t2
	end

  (******************************************************************************)
let statTotalNumberOfQueries_proof = ref 1
let statTotalCached_proof = ref 0
let statTotalMismatches = ref 0
let vampyre_lemma_counter = ref 1
  (******************************************************************************)


let refined_fun_table = Hashtbl.create 31
(* used to check if predicates were added to a particular function during last refinement *)


let cfb_local_refine_table : (string,bool) Hashtbl.t = Hashtbl.create 31
(* used to store functions containing pivot node -- reset when a bonafide refine
 * takes place, o.w. keep adding to this table until you add a function thats
 * already in it! *)

let block_a_t_reset () = 
  block_a_t_flag := false;
  Hashtbl.clear refined_fun_table

let is_fun_refined fn = Hashtbl.mem refined_fun_table fn

let add_local_preds l_id preds = 
  M.msg_string M.Debug (Printf.sprintf "Add local pred: loc_id (%d, %d): %s" (fst l_id) (snd l_id)(Misc.string_of_int_list preds)); 
  let pres_p_l_loc = try Hashtbl.find loc_pred_table l_id with Not_found -> [] in
  let pres_p_l_fun = try Hashtbl.find fun_pred_table (fst l_id) with Not_found -> [] in
  let which_to_add ppl = List.filter (fun p_i -> not(List.mem p_i ppl)) preds in
  let to_add_loc = which_to_add pres_p_l_loc in
  let to_add_fun = which_to_add pres_p_l_fun in
  M.msg_string M.Debug "Adding pred "; 
  List.iter (fun p_i -> block_a_t_flag := true; 
	       M.msg_string M.Debug (string_of_int p_i)) to_add_loc;
  Hashtbl.replace loc_pred_table l_id (to_add_loc@pres_p_l_loc);
  let nfp = (to_add_fun@pres_p_l_fun) in
  Hashtbl.replace fun_pred_table (fst l_id) (to_add_fun@pres_p_l_fun);
  if to_add_fun <> [] then 
    (let fn = C_SD.get_location_fname (C_SD.lookup_location_from_loc_coords l_id) in
    Hashtbl.replace refined_fun_table fn true)
  

(* TBD:Simplify this hideous mess! *)  
let lookup_location_predicates loc = 
  let craig_preds () =
     (!globally_useful_preds) @
      try 
        if O.getValueOfInt "craig" >= 2 then Hashtbl.find loc_pred_table loc 
        else Hashtbl.find fun_pred_table (fst loc) 
      with Not_found -> [] in
  let rv = if O.getValueOfInt "craig" >= 1 then craig_preds () else (get_all_pred_indexes ()) in
  M.msg_string M.Debug 
   (Printf.sprintf "location-predicates (%d,%d) : %s" (fst loc) (snd loc) (Misc.string_of_int_list rv));
    rv
  
let print_pred_vector () = 
  let psize_l = List.map (fun (_,plist) -> List.length plist) (Misc.hashtbl_to_list loc_pred_table) in 
  M.msg_string M.Normal ("Pred-Vector: "^(Misc.string_of_int_list psize_l))
(** Environment Counter map BDDs *)

  let ctrbddManager = CaddieBdd.init 0 32 256 512 (* Find out what these numbers mean  *)
  let ctrbddZero = CaddieBdd.bddFalse ctrbddManager
  let ctrbddOne = CaddieBdd.bddTrue ctrbddManager

let freeBddVars: CaddieBdd.bdd list ref = ref []
let getBddVar () = 
  match !freeBddVars with (* try the free list, if there are no bdds there, allocate a new variable *)
    [] -> CaddieBdd.bddNewVar ctrbddManager
  | a::rest -> freeBddVars := rest ; a

let bddMap: (int * (CaddieBdd.bdd list)) list ref = ref [] (* (id, log k -bdd vars) list where id is the node id, and there are k possible counter values *)


let allocateCtrBdds state_id_list maxctrval = (* allocate log maxctrval bdd's for each id, and update bddMap *)
  M.msg_string M.Normal (Printf.sprintf "Allocating BDD variables for %d nodes and %d counters" (List.length state_id_list) (maxctrval+2));
  let numbdds = int_of_float (ceil (( log (float_of_int (maxctrval+2))) /. (log 2. ))) in (* ceil ( log_2 (maxctrval+2)  ), plus 2 for 0 and omega *) 
  (* for each id, allocate numbdds bdd nodes *)
  let rec _w l = match l with [] -> () 
  | id :: rest -> let bdds = ref [] in for i=1 to numbdds do bdds := (getBddVar ())::!bdds done ; bddMap := (id,!bdds)::!bddMap ; _w rest in
  _w state_id_list 
    

let destroyCtrBdds () = (* destroy bddMap and add all the bdd variables to the free list *)
  let rec _w l = match l with [] -> () | (i,bddlist):: rest -> freeBddVars := bddlist @ !freeBddVars; _w rest in
  _w !bddMap ; bddMap := []

let getBdd (id : int) ctrval =
  M.msg_string M.Normal ("In getBdd with id " ^ (string_of_int id) ^ " and ctrval "^(Counter.toString ctrval)) ;
  try let (_, bddlist) = (List.find (fun (i, b) -> i=id) !bddMap) in
  let n = List.length bddlist in
  let rec n_ones n = if n<= 0 then [] else 1::(n_ones (n-1)) in
  let binrep = if (Counter.is_omega ctrval) then n_ones n else Misc.get_binary (Counter.to_int ctrval) n in
  let bdd_ = List.fold_left2 (fun bddsofar thisbit thisnode -> CaddieBdd.bddAnd bddsofar (if thisbit=1 then thisnode else CaddieBdd.bddNot thisnode))
      ctrbddOne binrep bddlist in 
  M.msg_string M.Normal "Returning from getBdd with BDD" ;
  CaddieBdd.bddPrint bdd_ ;
  bdd_
  with Not_found ->   M.msg_string M.Normal ("In getBdd id not found" ^ (string_of_int id)) ;ctrbddOne (* this is a filter to catch ids that don;t have automaton states *)

let ctrmap_to_bdd ctrmap =
  M.msg_string M.Normal "In ctrmap_to_bdd" ;
  let bdd = ref ctrbddOne in
  for i=0 to Array.length ctrmap -1 do bdd := let b = getBdd i ctrmap.(i) in CaddieBdd.bddAnd b !bdd done ;
  M.msg_string M.Normal "Returning from ctrmap_to_bdd" ;
  CaddieBdd.bddPrint !bdd ;
  !bdd

let ctrmap_check_covered (ctrmap: Counter.counter array) cupmap =
  (* I shall take the "upward closure" of map1 and intersect it with cupmap. *)
  let bdd = ref ctrbddOne in
  for i=0 to Array.length ctrmap -1 do
    let rec _b i bdd cval =
      if Counter.eq cval (Counter.omega ()) then CaddieBdd.bddOr bdd (getBdd i cval)
      else
	let b = CaddieBdd.bddOr bdd (getBdd i cval) in
        let c' = Counter.plus cval (Counter.make_counter 1) in
	_b i b c'
    in
    bdd := CaddieBdd.bddAnd !bdd (if CaddieBdd.bddEqual ctrbddOne (getBdd i ctrmap.(i)) then ctrbddOne else _b i ctrbddZero ctrmap.(i)) ;
  done;
  let b = CaddieBdd.bddAnd !bdd cupmap in
  not (CaddieBdd.bddEqual b ctrbddZero)

let ctrmap_exists_stuck b =
  let stuck = CaddieBdd.bddNot (getBdd 0 (Counter.make_counter 0)) in
  let s_and_b = (CaddieBdd.bddAnd stuck b) in
  not (CaddieBdd.bddEqual s_and_b ctrbddZero)




(*****************************************************************************)
(************* Types/Structures etc. for loading/saving abstractions**********)
(*****************************************************************************)

(* we need a new data type for random operations pertaining to the loading/
  * saving of abstractions. With such a type in place, we can keep 
  * changing the facts that we load/save. 
  * The .abs file is just a abstraction_info list *)

type abstraction_info = 
  AbsPred of Predicate.predicate
| LocPredTuple of ((int*int) * int list)
| SymLvalTuple of (string * (Expression.lval * Expression.lval) list)
                                (*(fname,[...(lv,sym_lv)...] *)

let get_AbsPred ai = 
  match ai with
        AbsPred p -> Some p
  |     _       -> None

        
        
let get_LocPredTuple ai = 
  match ai with
       LocPredTuple t -> Some t
  |     _ -> None
  
let get_SymLvalTuple ai =
  match ai with
        SymLvalTuple t -> Some t
  |     _ -> None

(* this table has a map from fname -> relevant symconsts for that fname. 
 * Used to construct assignments corresponding to function calls etc *)
  let relevant_symvar_table = Hashtbl.create 101

  (* storing the symvar suffices -- we can recompute the lv from it *)
  let add_relevant_symvar fname symvar =
    let lv = (Expression.peel_symbolic symvar) in
    let str = C_SD.is_struct lv in
    M.msg_string M.Debug
    (Printf.sprintf "add_rel_symvar: %s : %s : %s : %b" fname
    (Expression.lvalToString symvar) (Expression.lvalToString lv) str);
    (if not (str) then ignore(Misc.hashtbl_check_update relevant_symvar_table fname symvar))

  let get_relevant_symvar fname = 
    try Hashtbl.find relevant_symvar_table fname with Not_found -> []
   


let initialize_skipfun () =
  if (O.getValueOfBool "skipfun") then
    begin
      let all_funs = C_SD.get_reachable_functions () in
      let _ = List.iter add_skip_fun (all_funs) in
      let dont_skip_funs = 
        let seed =
           if (O.getValueOfString "funlabel" <> "") 
           then O.get_errorseed ()
           else 
             let error_label = O.getValueOfString "L" in
             let error_locs = C_SD.get_locations_at_label error_label in
                Misc.sort_and_compact (List.map C_SD.get_location_fname error_locs) 
        in
        let _  = M.msg_string M.Normal 
                ("seed error funs: "^(Misc.strList seed)) in
        if (O.getValueOfBool "bmc") then
        begin      
          let main = List.hd (O.getValueOfStringList "main") in   
          let e_depths = 
            List.map (fun f' -> List.length (C_SD.shortest_path main f')) seed 
          in
          let max_depth = Misc.list_max e_depths in
          let _ = M.msg_string M.Normal 
          ("Shortest Path to err: "^(string_of_int max_depth)) in
          let offset = O.getValueOfInt "depthoffset" in
          let bcc = 
            C_SD.bounded_backwards_caller_closure (max_depth + offset) seed
          in
          let _ = M.msg_string M.Normal 
          ("back call clos size:"^(string_of_int (List.length bcc))) 
          in
          (* now put in the functions that modify the "important predicates" *)
          (* we could achieve this by "adding" a predicate corresponding to the
           * target *)
                bcc 
        end     
        else
            begin
               let bcc = 
                 if (O.getValueOfBool "bccscc") then 
                 C_SD.scc_caller_closure (List.hd seed) 
                 else 
                   C_SD.backwards_caller_closure seed 
               in
               M.msg_string M.Normal 
                ("seed error funs: "^(Misc.strList seed));
                M.msg_string M.Normal ("back call clos size: "^(string_of_int (List.length bcc)));
                bcc
            end         
      in
      let exit_funs = 
                let sink_fnames = ["exit";"abort";"__error__"] in
                let exitdepth = O.getValueOfInt "exitDepth" in
                C_SD.bounded_backwards_caller_closure exitdepth sink_fnames
      in
      let _ = M.msg_string M.Normal 
        (Printf.sprintf "exit funs: %d :%s" (List.length exit_funs)
        (Misc.strList exit_funs))
      
      in
      let modify_err_lval_funs =
        if (O.getValueOfBool "errlv") then
          begin
            let error_lvals = C_SD.get_error_lvals () in
            let _ = M.msg_string M.Normal ("Error Lvalues: "^(Misc.strList (List.map Expression.lvalToString error_lvals)))
            in
            let f_list = Misc.sort_and_compact (List.flatten (List.map C_SD.may_mod_by error_lvals)) in
            let _ = M.msg_string M.Normal 
            (Printf.sprintf "Error lv modified by: %d : %s" (List.length f_list)
            (Misc.strList f_list))
            in f_list 
          end
        else  []
      in
      let dont_skip_funs = modify_err_lval_funs @ dont_skip_funs in
       if O.getValueOfBool "events" then add_take_fun __BLAST_DispatchFunctionName;
       List.iter add_take_fun (dont_skip_funs @ exit_funs);
      M.msg_string M.Normal ("Start take_fun size:"^(string_of_int !stats_nb_take_funs))
    end 
    


    
(* Saving abstractions *)
let dump_abs () =
  let _ = M.msg_string M.Debug "Dumping Abstraction" in
  let max = ref 0 in
  let total = ref 0 in
  let predi_to_string i = Predicate.toString (getPred i) in
  (* 0. pred_dep_table *)
  M.msg_string M.Debug "Predicate Dependancy Table";
  let print_pd i = 
    let idep = lookup_pred_dep_table i in
    let s = Printf.sprintf "%d : %s --> %s" i 
      (predi_to_string i) 
      (Misc.string_of_int_list idep)
    in
    M.msg_string M.Debug s
  in
  let _ = List.iter print_pd (Misc.hashtbl_keys pred_dep_table) in 
  (* 1. Elements corresponding to Abstraction Predicates *)
  let predicate_list = Misc.sort_and_compact (List.map (Misc.snd3) !tableOfPreds) in
  let plist = List.map (fun x -> AbsPred x) predicate_list in
  (* Need to make a map from pred -> integer thus *)
  let pmap_table = Hashtbl.create 101 in
  let rec _filler i _plist  = 
    match _plist with 
    [] -> ()
    | h::t -> (Hashtbl.replace pmap_table h i; _filler (i+1) t)
  in
  let _ = _filler 0 predicate_list in
  let translate_idx idx = 
    let p_idx = getPred idx in
    try Hashtbl.find pmap_table p_idx with Not_found -> failwith "bug in dump_abs!"
  in (* TBD! *)
  (* 2. Elements corresponding to Location Predicate Tuples (map) *)
  let loc_info = 
	  begin
	    let compair (x1,y1) (x2,y2) =
	      if (x1,y1) = (x2,y2) then 0
	      else if ((x1 > x2) || (x1 = x2 && y1 > y2)) then 1
	      else -1
	    in
	    let loc_l = List.sort compair (Misc.hashtbl_keys loc_pred_table) in
	    let get_pred (l_1,l_2) =
	      let rv =
		try Hashtbl.find loc_pred_table (l_1,l_2) with Not_found -> [] 
	      in
              let _ = 
                let size = List.length rv in
                total := !total + size;
                if (size > !max) then max := size
              in
              let pflag = if (O.getValueOfBool "bddpost") then
                M.Normal else M.Debug 
              in
		M.msg_string pflag
		  (Printf.sprintf "(%d,%d) |-> %s" l_1 l_2
		     (Misc.strList (List.map predi_to_string rv)));
	        List.map translate_idx rv
	    in	    	     
            List.map (fun loc -> LocPredTuple (loc, get_pred loc)) loc_l
	  end
  in
  let sym_var_info = 
    let svl = Misc.hashtbl_to_list relevant_symvar_table in 
    let proc sym_lv = (Expression.peel_symbolic sym_lv,sym_lv) in
      List.map 
        (fun (fn,sv_list) -> SymLvalTuple (fn, List.map proc sv_list)) 
        svl  
  in
      (* Now, write the file *)
  let fname = (O.getValueOfString "mainsourcename")^".abs" in
  let _ = M.msg_string M.Normal ("Writing out .abs file: "^fname) in    let oc = open_out fname in
    Marshal.to_channel oc (plist @ loc_info @ sym_var_info) []; close_out oc;     
    let locs = 1 + List.length (Misc.hashtbl_keys loc_pred_table) in
    M.msg_string M.Normal ("Maximum #preds/loc: "^(string_of_int !max));
    M.msg_string M.Normal ("Average #preds/loc: "^(string_of_int (!total/locs)));
    M.msg_string M.Normal ("Done writing .abs file")
        
let load_async_preds () = 
  let proc (fn,ps) = 
    let l = C_SD.location_coords (C_SD.lookup_entry_location fn) in
    let is = List.map just_addPred ps in
    add_local_preds l is in
  let doParse filename =
    let inchan = open_in filename in
    let lexbuf = Lexing.from_channel inchan in
    Inputparse.fun_main Inputflex.token lexbuf in
  ignore (M.msg_string M.Normal "Reading in async predicates...");
  let filename = (O.getValueOfString "mainsourcename")^".apred" in
  try
    let a = doParse filename in
    ignore (M.msg_string M.Normal "Async predicates read.\n") ; 
    List.iter proc a
  with _ -> (M.msg_string M.Error ("Cannot find async predicate information in "^filename^".\n"))
    
(* Loading Abstractions *)
let load_abs () = 
  let compute_max_and_average linfolist =
    let (max, sum) = 
      List.fold_left (fun (m,s) -> fun ((lc1,lc2), lst) -> 
                        let l = List.length lst in 
                        ((if l > m then l else m), s+l))  (0,0) linfolist in
    let avg = 
      let l = List.length linfolist in 
      if l = 0 then 0.0 else (float_of_int sum) /. (float_of_int l) in
    M.msg_string M.Normal ("Maximum predicates active = "^(string_of_int max)) ;
    M.msg_string M.Error ("Maximum predicates active = "^(string_of_int max)) ;
    M.msg_string M.Normal ("Average predicates active = "^(string_of_float avg)) ;
    M.msg_string M.Error ("Average predicates active = "^(string_of_float avg)) in 
  if (O.getValueOfBool "events") then load_async_preds ();
  M.msg_string M.Error "here now back from load_async";
  if (O.getValueOfInt "predH" > 0) then make_const_table () ;
  (* Next, initialize skip things *)
  let _ = Stats.time "initialize skipfun" initialize_skipfun () in 
    (* now, deal with loadabs *)
  let fname = 
    let raw = (O.getValueOfString "loadabs") in
    if raw = "self" then ((O.getValueOfString "mainsourcename")^".abs") else raw in
  if fname <> "" then 
    begin
      let _ = M.msg_string M.Normal "initialize abstraction" in
      let ic = open_in fname in
        (* 1. load all abstraction elements *)
      let abs_info = (Marshal.from_channel ic : abstraction_info list) in
        (* 2. deconstruct list *)
      let sym_var_info_list = Misc.map_partial get_SymLvalTuple abs_info in
      let pred_list = Misc.map_partial get_AbsPred abs_info in
      let loc_info_list = Misc.map_partial get_LocPredTuple abs_info in
      let _ = compute_max_and_average loc_info_list in
        (* Add components to wherever *)
        (* this creates appropriate symvars, adds them to table, creates
         * list of subs-pairs with which to "transform" predicates *)
      let sym_subs =     
         let proc_svt (fname,lvlist) = 
           List.map 
            (fun (lv,sym_lv) -> 
              let new_symvar = Expression.make_symvar fname lv in
              add_relevant_symvar fname new_symvar;
              (Expression.Lval sym_lv,Expression.Lval new_symvar)) 
            lvlist in 
         List.flatten (List.map proc_svt sym_var_info_list) in 
        (* now, just substitute the preds with the new symbolic names,
         * and then add them to the table *)
      let _ = M.msg_string M.Normal "Init: Added symvars" in
      let _ =
          let pl_s = String.concat "\n" (List.map Predicate.toString pred_list) in
          M.msg_string M.Debug ("Preds: "^pl_s) in
      let pred_list = List.map (Predicate.substitute sym_subs) pred_list in
      let _ =
          let pl_s = String.concat "\n" (List.map Predicate.toString pred_list) in
          M.msg_string M.Debug ("Preds: "^pl_s) in
      let _ = M.msg_string M.Normal "Init: Preds substituted" in
      let idx_map_table = Hashtbl.create 101 in
      let idx_map i = 
          try Hashtbl.find idx_map_table i 
          with Not_found -> failwith ("bug in idx_map in loadabs: "^(string_of_int i)) in
      let rec _addp i plist = 
          match plist with
          h::t ->
            begin
              let i' = just_addPred h in 
              Hashtbl.add idx_map_table i i'; _addp (i+1) t
            end
          | [] -> () in  
      let _ = _addp 0 (pred_list) in
      let add_tuple (l,p_list) = add_local_preds l (List.map idx_map p_list) in
      List.iter add_tuple loc_info_list;
      M.msg_string M.Normal "done initializing"    
    end

end


