(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(* This file has code for abstract_data_post / postBdd etc. *)




(* Abstract pre operator. Keep track of predicates mentioned in reg.preds *)
  (* Type: Region.abstract_data_region -> Cmd.t -> Region.abstract_data_region *)
  let abstract_data_pre abst_data_reg cmd edge post_location phi_table =
    M.msg_string M.Debug "In abstract_data_pre -- arguments are:" ;
    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg ;
    M.msg_printer M.Debug Cmd.print cmd ;
    (* Implementation of the H function from DasDillPark-CAV99 *)
    let rec _H (bdd, p) predlist =
      (* optimization: check if (abst_data_reg, p) is satisfiable? *)
      if (PredTable.isSatisfiable (bdd, p)) then
	begin
	  match predlist with
	      [] -> if (PredTable.isSatisfiable (bdd, p)) then PredTable.bddOne else PredTable.bddZero
	    | a:: rest ->
		let pr = PredTable.getPred a in
		let cr = concrete_data_post { Region.pred = pr ; } cmd edge phi_table in
		let wppos = cr.Region.pred in
		let wpneg = P.negate wppos in
		  CaddieBdd.bddOr 
		    (CaddieBdd.bddAnd (PredTable.getPredIndexBdd a) (_H (bdd, wppos :: p) rest))
		    (CaddieBdd.bddAnd (CaddieBdd.bddNot (PredTable.getPredIndexBdd a)) (_H (bdd, wpneg :: p) rest))
	end
      else
	PredTable.bddZero
    in
    let pre_bdd = 
      if Options.getValueOfBool "bddpost" then
        let src_loc = C_SD.location_coords (C_SD.get_source edge) in
        let dst_loc = C_SD.location_coords post_location in
        let trel_bdd = PredTable.get_transrel_bdd (src_loc,dst_loc) in
          PredTable.bdd_preimage abst_data_reg.Region.bdd trel_bdd
      else
        _H (abst_data_reg.Region.bdd, []) abst_data_reg.Region.preds
    in
      { Region.lelement = DataLattice.pre abst_data_reg.Region.lelement cmd ;
        Region.bdd = pre_bdd ;
	Region.preds = abst_data_reg.Region.preds ; }

      

  (* -----  JHALA: The new abstract_data_post : SLAM_POST *)
  let assume_pred_ref = ref None 
			  (* We use this to check if a given predicate is unchanged in this post ie if its wp = p 
			     ... which also means <?> wpnp = np this is cleared every time postBdd is called *)

    let iasm_table = Hashtbl.create 37
    let unchangedPredTable = Hashtbl.create 37 
    let dontcareArray = Array.create PredTable.maxPossiblePreds false 
    let outOfScopeArray = Array.create PredTable.maxPossiblePreds false 


  let extract_scope var = 
    let names = Misc.chop var "@" in
      try List.nth names 1 with _ -> "__BLAST_GLOBAL" 


  let postBdd bdd _pred_list _post_split_preds =
    statPostBddCalls := !statPostBddCalls + 1;
    M.msg_string M.Debug "In postBdd..\n";
    M.msg_string M.Debug ("post of assume"^(match !assume_pred_ref with 
      Some(e) -> ("YES:"^(P.toString e)) | _ -> "NO")^"\n");
    M.msg_string M.Debug "\nList of predicates = ";

    let pred_list = List.map (fun (a,b,c,d) -> (a,b,c)) _pred_list in
    let post_split_preds = List.map (fun (a,b,c,d) -> (a,b,c)) _post_split_preds in
    let craig_flag = (Options.getValueOfInt "craig" >= 2 ) in
    let craig_flag1 = (Options.getValueOfInt "craig" = 1) in
    let dc_flag = (Options.getValueOfBool "dc") in
    if (Options.getValueOfBool "scope") then 
      begin
	Misc.array_reset outOfScopeArray false;
	List.iter (fun (a,_,_,d) -> Array.set outOfScopeArray a d) _pred_list 
      end;
    let rec loop lst = 
      match lst with 
	[] ->  () 
      | (a,b,c,d)::rest -> 
	  begin
	    ignore (M.msg_string M.Debug (P.toString (PredTable.getPred a)));
	    ignore (M.msg_string M.Debug (P.toString b));
	    ignore (M.msg_string M.Debug (P.toString c)); 
	    ignore (M.msg_string M.Debug (string_of_bool d));
	    loop rest
	  end
    in
    M.msg_apply M.Debug loop _pred_list;
    M.msg_string M.Debug ("Will split on: \n");
    M.msg_apply M.Debug loop  _post_split_preds ;

    (* assuming that if p = wp, then np = wpnp ...*)
    let wp_different_filter (p,wp,_) = 
      if craig_flag then true 
      else
	match Options.getValueOfString "post" with
	    "slam" -> true
	  | "slam-dumb" -> not (PredTable.getPred p = wp)
	  | "slam1" -> not ((PredTable.getPred p = wp) && (!assume_pred_ref = None))
	  | "slam2" -> let p_pred = PredTable.getPred p in 
	      (not (p_pred = wp) || 
	       (match !assume_pred_ref with 
		    None -> false
		  |	Some(asm) -> not (Misc.intersection (Absutil.getVarExps_deep p_pred) (Absutil.getVarExps_deep asm) = [])))  
		(* JHALA: Instead of this intersection check I would like to say 
		   if the two preds are textually the same or imply 
		   each other or some such ...*)
	  |  a -> failwith ("Unknown post algorithm "^a)
    in
    (* TBD: merge! *)
    let (_changed_pred_list,unchanged_pred_list) =
      if (O.getValueOfBool "cf") then 
        Misc.filter_cut wp_different_filter pred_list 
      else
        (List.filter wp_different_filter pred_list, 
         List.filter (function (p,wp,_) -> PredTable.getPred p = wp) pred_list) in
    let cpl_addon = if craig_flag1 then post_split_preds else [] in
    let changed_pred_list = _changed_pred_list @ cpl_addon in
    
    let _ = Hashtbl.clear iasm_table in
    let _ = Hashtbl.clear unchangedPredTable in
    let _ = List.iter (fun x -> Hashtbl.add unchangedPredTable (Misc.fst3 x) true) unchanged_pred_list in
    let sumofcubesBdd = ref PredTable.bddZero in
    let num_cubes = ref 0 in

    let conjunctL = ref [] in
      (* conjunct that the present cube produces post-command c *)
    let _ = Misc.array_reset dontcareArray false in

    (* START TIME HOLE *)
    let _check_Pred_list_simplify assumptions pred_triple_list conjunct_pointer =
      let asm_conj = P.conjoinL assumptions in
      let _ = M.msg_string M.Debug ("Assuming pre-state: "^(P.toString asm_conj)) in
      let _ = Stats.time "_assume" (TheoremProver._assume ) asm_conj in 
      let _check_pred (*_asm_conj*) (p,wpp,wpnp) = 
	try Hashtbl.find iasm_table p
        with Not_found ->
        if ( dontcareArray.(p) && dc_flag) then false 
	  (* this tuple will never be called if I dont care about it if craig is true *)
	else 
	  let eToCheck1 = P.And [asm_conj;wpnp] in (* wpnp = \neg wpp *)
	  let eToCheck2 = P.And [asm_conj;wpp] in
	  let _ = M.msg_string M.Debug ("Post pred: "^(P.toString (PredTable.getPred p))) in
	  let _ = M.msg_string M.Debug ("Post query+: "^(P.toString wpp)) in
	  let _ = M.msg_string M.Debug ("Post query-: "^(P.toString wpnp)) in
	  let ans1 = Stats.time "simp-a1:" (PredTable.askTheoremProverContext eToCheck1) wpp in
	  let ans2 = 
            if (Options.getValueOfBool "posp") then false
            else
              Stats.time "simp-a2:" (PredTable.askTheoremProverContext eToCheck2) wpnp 
          in
	  if ans1 then statNonDCQueriesForPost := !statNonDCQueriesForPost + 1;
          if ans2 then statNonDCQueriesForPost := !statNonDCQueriesForPost + 1;
          match ans1 with
	    true ->  
	      if  (ans2 = true) then 
		begin
		  M.msg_string M.Debug ("Cube implies both wp(p) and wp(neg p): "^(P.toString (PredTable.getPred p))); 
		  conjunct_pointer := (p,true)::(p,false)::(!conjunct_pointer);
		  true
		end
	      else
		begin 
		  conjunct_pointer := (p,true)::(!conjunct_pointer);
		  true               
		end
	  | false ->  
	      if (ans2 = true) then  
		begin
		  conjunct_pointer := (p,false)::(! conjunct_pointer);
		  true
		end
	      else 
		false
      in
      let _ = Stats.time "_check_pred" (List.map (_check_pred (* asm_conj *))) pred_triple_list in
	statWorstCaseQueriesForPost :=  2*(List.length pred_list) + !statWorstCaseQueriesForPost;
	statActualQueriesForPost := 2*(List.length pred_triple_list) + (!statActualQueriesForPost);
	statNumPost := !statNumPost + 1;
	(match (!assume_pred_ref) with
	     Some _ -> 
	       (statAssumeQueriesForPost := 2*(List.length pred_triple_list) + (!statAssumeQueriesForPost); statNumAssumePost := !statNumAssumePost + 1)
	   | None -> ())
	;
	TheoremProver._pop ();
	true
    in
      (* END TIME HOLE *)
    let _check_Pred_list =  _check_Pred_list_simplify in
    let rec make_conjunct_bdd conjl = 
      match conjl with
	  [] -> PredTable.bddOne 
	| (p,true)::tl ->  CaddieBdd.bddAnd (PredTable.getPredIndexBdd p) (make_conjunct_bdd tl) 
	| (p,false)::tl -> CaddieBdd.bddAnd (CaddieBdd.bddNot (PredTable.getPredIndexBdd p))  (make_conjunct_bdd tl) in
       
    let _check_Cube _pred_list (cube : int array)  =
      (M.log_string_dbg M.BddPost " In _check_Cube");
      statReachedCubes := !statReachedCubes + 1;
      conjunctL := [];   
      let _ = 
	if (dc_flag) then Misc.array_reset dontcareArray false;
	if (craig_flag) then 
	  begin
	    Misc.array_reset dontcareArray true; 
	    List.iter (fun (i,_,_) -> Array.set dontcareArray i false) pred_list
	  end
      in
      let cubePostIsEmpty = 
	match (!assume_pred_ref) with 
	  Some(e) -> (PredTable.askTheoremProver 
			(P.implies (P.conjoinL [(convertCubeToPred cube);e]) P.False))
	| None -> false
      in 
      let pre_filter_query (p,_,_) = 
        let f i = (p <> i) && (cube.(i) = 1 || cube.(i) = 0) in
        let pp = PredTable.lookup_pred_dep_table p in
        if not ((List.mem (-1) pp) || (List.exists f pp)) then
          Hashtbl.replace iasm_table p false
      in
      if not (cubePostIsEmpty) then
	  begin   
	    let pref = match (!assume_pred_ref) with None -> ref [] | Some(e) -> ref [e] in
	    let _plist = ref _pred_list in
	      begin
                Hashtbl.clear iasm_table;
		for i = 0 to (Array.length cube - 1) do
                  if (cube.(i)==2) 
		  then 
		    begin
		      try
			if ((dc_flag) && (outOfScopeArray.(i)))  then
			  begin
			    M.log_string_dbg M.BddPost ("DC about: "^(P.toString (PredTable.getPred i))); 
			    Array.set dontcareArray i true
			  end
		      with _ -> ()
		    end
		  else 
		    begin
		      let i_pred = PredTable.getPred i in
			(* Assume the invariant that i_pred : P.Atom *)
                      let i_unchanged = (Hashtbl.mem unchangedPredTable i)  
		      in
			if (cube.(i)==1) then
			  begin
			    pref := i_pred::(!pref);
			    if (i_unchanged && not(dontcareArray.(i))) 
			    then (Hashtbl.replace iasm_table i true; 
                                  conjunctL := (i,true)::(!conjunctL));
			  end
			else
			  begin
			    pref := (P.negate i_pred)::(!pref);
			    if (i_unchanged && not(dontcareArray.(i))) 
			    then (Hashtbl.replace iasm_table i true;
                                  conjunctL := (i,false)::(!conjunctL));
			  end
		    end
		done;
                if (craig_flag && Options.getValueOfBool "preqfilter") 
                then List.iter pre_filter_query changed_pred_list;  
		(* Now ask the thmprover if pref -> each fellow in predlist *)
		if (Stats.time "_check_Pred_list" (_check_Pred_list !pref)  changed_pred_list conjunctL) 
		then 
		  begin
		    (* Jhala: I'm being paranoid about the soundness and aliasing. something to assuage me *)
		    if (Options.getValueOfBool "scheck") then
		      begin
			let cube_l = List.map 
			    (fun (i,b) -> if b then (PredTable.getPred i) else P.Not(PredTable.getPred i))
			    !conjunctL 
			in
			if (TheoremProver.queryExp (P.Implies ((P.And cube_l),(P.False))) && (!assume_pred_ref = None)) 
			then 
			  begin
			    M.log_string_always M.BddPost 
			      "SCHECK: Post of block is FALSE !!"
			  end
			else 
			  ()
		      end ;
		      
		    (*  this is where we plug in the post_split_cube *)
		    let _ = 
		      if (Options.getValueOfInt "craig" >= 3) 
		      then conjunctL := List.filter (PredTable.keep_pred_parity) !conjunctL 
		    in
		    let splitup_bdd = make_conjunct_bdd !conjunctL in
		    sumofcubesBdd := CaddieBdd.bddOr (splitup_bdd) (!sumofcubesBdd);
                    num_cubes := 1 + !num_cubes;
                    
		  end; () 
	      end;
	  end
    in
    begin
      if (CaddieBdd.bddEqual bdd PredTable.bddZero) then
	sumofcubesBdd := PredTable.bddZero
      else 
	begin
	  if (CaddieBdd.bddEqual bdd PredTable.bddOne) 
	  then
	    let _tmpcube = (* build a temporary cube with nothing in it *)
	      Array.make (*size *)(getCurrentIndex ()+2) (* content *)2 
		
	      (* Note: The size is certainly a vast	overestimate, but this
		 is the easiest way. The "+2" removes additional complications relating
		 to index being -1 initially. Note that this is a fake cube -- we should
		 never try to print it or convert it to a predicate!Ugly hack, but 
		 works. So far *)
	    in
	    _check_Cube changed_pred_list _tmpcube
	  else
	    ignore (Stats.time "bddForEachCube-post" (CaddieBdd.bddForeachCube bdd) (_check_Cube changed_pred_list));
	end;
      M.log_string_norm M.BddPost
	("Returning from post-bdd: number of cubes = "^(string_of_int !num_cubes));
      !sumofcubesBdd 
    end
      
  (** BDD-based image computation takes input bdd, *)   
  let postBdd_bdd bdd (src_id,dst_id) preserved_preds = 
    let _ = M.msg_string M.Normal "postBdd_bdd:begin" in
    let rv = 
      let tr = PredTable.get_transrel_bdd (src_id,dst_id) in
      let etr = 
        if Options.getValueOfInt "craig" < 2 || CaddieBdd.bddEqual tr bddOne 
        then PredTable.get_eq_transrel_bdd (src_id,dst_id) else bddOne in 
      let tr = CaddieBdd.bddAnd tr etr in
      PredTable.bdd_postimage bdd tr in
    let _ = M.msg_string M.Normal "postBdd_bdd:end" in
    rv
  (*
  (** Implementation of the H function from DasDillPark-CAV99 *)
  let postBdd_dasdillpark bdd assume_pred_opt pred_triple_list =
    let pre_pred = 
      let bdd_pred = PredTable.convertBddToPred bdd in (* use bdd_to_predicate, after it works! *)
	match assume_pred_opt with
	    Some p' -> P.And [p';bdd_pred]
	  | None -> bdd_pred
    in
    let sat_check p = 
      if (true) then (TheoremProver.interpolant_sat (pre_pred, P.And p) = TheoremProver.Sat)
      else PredTable.isSatisfiable (bdd,p)
    in
    let rec _H p _pred_triple_list =
      M.log_string_norm M.BddPost ("_H depth : "^(string_of_int (List.length p)));
      M.log_string_dbg M.BddPost ("pred = "^(match p with [] -> "" | _ -> P.toString (List.hd p)));
      if (sat_check p) then
	begin
	  match _pred_triple_list with
	      [] -> PredTable.bddOne 
	    | (a,wppos,wpneg,_):: rest ->
                let bdd_a = (PredTable.getPredIndexBdd a) in
		  CaddieBdd.bddOr 
		    (CaddieBdd.bddAnd bdd_a (_H (wppos :: p) rest))
		    (CaddieBdd.bddAnd (CaddieBdd.bddNot bdd_a) (_H (wpneg :: p) rest))
	end
      else
	PredTable.bddZero
    in
      M.log_string_norm M.BddPost "begin ddp _H";
      let rv = _H [] pred_triple_list in
	M.log_string_norm M.BddPost "end ddp _H";
	rv

	  
  let postBdd_dasdillpark_inc (assert_fn,pop_fn,issat_fn,reset_fn)
    bdd assume_pred_opt pred_triple_list = 
    let pre_pred = 
      let bdd_pred = PredTable.convertBddToPred bdd in (* use bdd_to_predicate, after it works! *)
	match assume_pred_opt with
	    Some p' -> P.And [p';bdd_pred]
	  | None -> bdd_pred
    in
    let rec _H depth _pred_triple_list =
      M.log_string_norm M.BddPost ("_H depth : "^(string_of_int depth));
      if not (issat_fn ()) then
	(M.log_string_norm M.BddPost "ddpinc:unsat";PredTable.bddZero)
      else
	begin
          match _pred_triple_list with
              [] -> PredTable.bddOne 
            | (a,wppos,wpneg,_)::rest ->
		let recur p = 
		  assert_fn p;
		  let rv = _H (depth + 1) rest in pop_fn (); rv
		in
		let bdd_pos = recur wppos in
		let bdd_neg = recur wpneg in
		let bdd_a = (PredTable.getPredIndexBdd a) in
		  CaddieBdd.bddOr 
		    (CaddieBdd.bddAnd bdd_a bdd_pos) 
		    (CaddieBdd.bddAnd (CaddieBdd.bddNot bdd_a) bdd_neg)
	end
    in
      M.log_string_norm M.BddPost "begin ddp-inc _H";
      assert_fn pre_pred;
      let rv = _H 0 pred_triple_list in
	reset_fn ();
	M.log_string_norm M.BddPost "end ddp-inc _H";
	rv


  (*
  (** uses incremental foci interface *)
  let postBdd_ddp_inc_foci = 
    postBdd_dasdillpark_inc
      (TheoremProver.foci_inc_push,
       TheoremProver.foci_inc_pop,
       TheoremProver.foci_inc_check_sat, 
       TheoremProver.foci_inc_reset)
  *)
      
  (** uses incremental simplify interface *)
  let postBdd_ddp_inc_simplify = 
    let issat_fn () = not (TheoremProver._is_contra ()) in
      postBdd_dasdillpark_inc
	(TheoremProver.block_assert,
	 TheoremProver.block_pop,
	 issat_fn, 
	 TheoremProver.block_reset)
*)
        
        
(* CF-postBdd, merge ? or is the non-CF one good enough... 
  let postBdd bdd _pred_list _post_split_preds =
    statPostBddCalls := !statPostBddCalls + 1;
    M.msg_string M.Debug "In postBdd..\n";
    M.msg_string M.Debug ("post of assume"^(match !assume_pred_ref with Some(e) -> ("YES:"^(P.toString e)) | _ -> "NO")^"\n");
    M.msg_string M.Debug ("Bdd = "^(P.toString (convertBddToPred bdd)));
    M.msg_string M.Debug "\nList of predicates = ";
    let pred_list = List.map (fun (a,b,c,d) -> (a,b,c)) _pred_list in
    let post_split_preds = List.map (fun (a,b,c,d) -> (a,b,c)) _post_split_preds in
    let craig_flag = (Options.getValueOfInt "craig" = 2) in
    let craig_flag1 = (Options.getValueOfInt "craig" = 1) in
    let dc_flag = (Options.getValueOfBool "dc") in
    let _ = 
      if (Options.getValueOfBool "scope") then 
	begin
	  Misc.array_reset outOfScopeArray false;
	  List.iter (fun (a,_,_,d) -> Array.set outOfScopeArray a d) _pred_list 
	end
    in
    let rec loop lst = 
      match lst with 
	  [] ->  () 
	| (a,b,c,d)::rest -> 
	    begin
	      ignore (M.msg_string M.Debug (P.toString (getPred a)));
	      ignore (M.msg_string M.Debug (P.toString b));
	      ignore (M.msg_string M.Debug (P.toString c)); 
	      ignore (M.msg_string M.Debug (string_of_bool d));
	      loop rest
	    end
    in
    let _ = loop _pred_list in
    let _ =  M.msg_string M.Debug ("Will split on: \n");
      loop  _post_split_preds
    in

    (* assuming that if p = wp, then np = wpnp ...*)
    let wp_different_filter (p,wp,_) = 
      if craig_flag && (!assume_pred_ref <> None ) then true 
      else
	match Options.getValueOfString "post" with
	    "slam" -> true
	  | "slam-dumb" -> not (getPred p = wp)
	  | "slam1" -> not ((getPred p = wp) && (!assume_pred_ref = None))
	  | "slam2" -> let p_pred = getPred p in 
	      (not (p_pred = wp) || 
	       (match !assume_pred_ref with 
		    None -> false
		  |	Some(asm) -> not (Misc.intersection (getVarExps_deep p_pred) (getVarExps_deep asm) = [])))  
		(* JHALA: Instead of this intersection check I would like to say 
		   if the two preds are textually the same or imply 
		   each other or some such ...*)
	  |  a -> failwith ("Unknown post algorithm "^a)
    in
      (* I dont have a good idea of when an "if" affects/doesnt affect a predicate, so for now, leave them all in ... there should be some cone-check here ...*)
    let (_changed_pred_list, unchanged_pred_list) = Stats.time "wp_diff_filter" (Misc.filter_cut wp_different_filter) pred_list in
    let cpl_addon = if craig_flag1 then post_split_preds else [] in
    let changed_pred_list = _changed_pred_list @ cpl_addon in
      
    let _ = Hashtbl.clear unchangedPredTable in
    let _ = List.iter (function x -> Hashtbl.add unchangedPredTable (Misc.fst3 x) true) unchanged_pred_list in
      (* all the unchanged preds are dumped into this table *)
    let sumofcubesBdd = ref bddZero in
      
    let conjunctL = ref [] in
      (* conjunct that the present cube produces post-command c *)
    let _ = Misc.array_reset dontcareArray false in
      
    (* START TIME HOLE *)
    let _check_Pred_list_simplify assumptions pred_triple_list conjunct_pointer =
      let asm_conj = P.conjoinL assumptions in
      let _ = Stats.time "_assume" (TheoremProver._assume ) asm_conj in 
      let _check_pred (*_asm_conj*) (p,wpp,wpnp) = 
	if ( dontcareArray.(p) && dc_flag) then false 
	  (* this tuple will never be called if I dont care about it if craig is true *)
	else 
          let eToCheck1 = P.And [asm_conj; wpnp] in
          let eToCheck2 = P.And [asm_conj;wpp] in
          let ans1 = Stats.time "simp-a1:" (askTheoremProverContext eToCheck1) wpp in
          let ans2 = Stats.time "simp-a2:" (askTheoremProverContext eToCheck2) wpnp in
            match ans1 with
		true ->  
		  if  (ans2 = true) then 
		    begin
		      M.msg_string M.Debug ("Cube implies both wp(p) and wp(neg p): "^(P.toString (getPred p))); 
		      conjunct_pointer := (p,true)::(p,false)::(!conjunct_pointer);
		      true
		    end
		  else
		    begin 
	              conjunct_pointer := (p,true)::(!conjunct_pointer);
	              true               
		    end
              | false ->  
		  if (ans2 = true) then  
		    begin
	              conjunct_pointer := (p,false)::(! conjunct_pointer);
	              true
		    end
		  else 
		    false
      in
      let _ = Stats.time "_check_pred" (List.map (_check_pred (* asm_conj *))) pred_triple_list in
	statWorstCaseQueriesForPost :=  2*(List.length pred_list) + !statWorstCaseQueriesForPost;
	statActualQueriesForPost := 2*(List.length pred_triple_list) + (!statActualQueriesForPost);
	statNumPost := !statNumPost + 1;
	(match (!assume_pred_ref) with
	     Some _ -> 
	       (statAssumeQueriesForPost := 2*(List.length pred_triple_list) + (!statAssumeQueriesForPost); statNumAssumePost := !statNumAssumePost + 1)
	   | None -> ())
	;
	TheoremProver._pop ();
	true
    in
      (* END TIME HOLE *)

    let _check_Pred_list_vampyre assumptions pred_triple_list conjunct_pointer = 
      let is_cube_consistent  = TheoremProver.assume assumptions in
      let asm_conj = P.conjoinL assumptions in
      let _check_pred assumptions (p,wpp,wpnp) = 
	if ((dontcareArray.(p)) && dc_flag) then false
	else 
	  let eToCheck1 = P.implies asm_conj wpp in
	  let eToCheck2 = P.implies asm_conj wpnp in
	  let ans1 = (*Stats.time "ask1:"*) (askTheoremProverContext eToCheck1) wpp in
	  let ans2 = (*Stats.time "ask2:"*) (askTheoremProverContext eToCheck2) wpnp in
	    match ans1 with
		true ->  
		  if  (ans2 = true) then 
		    begin
		      M.msg_string M.Debug ("Cube implies both wp(p) and wp(neg p): "^(P.toString (getPred p))); 
		      conjunct_pointer := (p,true)::(p,false)::(!conjunct_pointer);
		      true
		    end
		      
		  else
		    begin 
		      conjunct_pointer := (p,true)::(!conjunct_pointer);
		      true               
		    end
	      | false -> 
		  if (ans2 = true) then  
		    begin
		      conjunct_pointer := (p,false)::(!conjunct_pointer);
		      true
		    end
		  else 
		    false
      in
	
	if (is_cube_consistent) then 
	  begin 
            let _ = List.map (_check_pred asm_conj) pred_triple_list in
              true
	  end 
	else false
    in
      
    let _check_Pred_list = 
      match (Options.getValueOfString "s" ) with
	  "vampyre" -> _check_Pred_list_vampyre
	| _ -> _check_Pred_list_simplify
    in
      (* the preds below are the integer identifiers *)
    let rec make_conjunct_bdd conjl = 
      match conjl with
	  [] -> bddOne 
	| (p,true)::tl ->  CaddieBdd.bddAnd (getPredIndexBdd p) (make_conjunct_bdd tl) 
	| (p,false)::tl -> CaddieBdd.bddAnd (CaddieBdd.bddNot (getPredIndexBdd p))  (make_conjunct_bdd tl) 
    in
      
    let rec refine_cube pre_reg cube_list split_preds = 
      (* Jhala: this next line is very slow and should be optimized *)
      let has_no_value (x,wp,wpn) = List.for_all (fun y -> x <> fst y) cube_list in
      let preds_to_split_on = List.filter (has_no_value) split_preds in 
	match preds_to_split_on with
	    [] -> make_conjunct_bdd cube_list
	  | (h,wp,wpneg)::t -> 
	      begin
		let cube_plus = (h,true)::cube_list in
		let pre_reg_plus = wp::pre_reg in
		let conj_plus_ptr = ref cube_plus in
		let _ = _check_Pred_list pre_reg_plus preds_to_split_on conj_plus_ptr in
		let plus_bdd = refine_cube pre_reg_plus !conj_plus_ptr preds_to_split_on in
		  
		let cube_minus = (h,false)::cube_list in
		let pre_reg_minus = wpneg::pre_reg in
		let conj_minus_ptr = ref cube_minus in
		let _ = _check_Pred_list pre_reg_minus preds_to_split_on conj_minus_ptr in
		let minus_bdd = refine_cube pre_reg_minus !conj_minus_ptr preds_to_split_on in
		  
		  CaddieBdd.bddOr plus_bdd minus_bdd
              end
    in
      (* let custom_post_cube assumptions  =
	 M.msg_string M.Debug "In custom_post_cube";
	 let asm_conj = P.conjoinL assumptions in
	 let _ = Stats.time "_assume" (TheoremProver._assume) asm_conj in
	 let is_sat (pi,wp,wpnp) = 
      (* check if this pred is satifiable in the post - returns a boolean *)
	 let query = P.implies (P.conjoinL (wp::assumptions)) P.False in
	 let ac_query = P.implies wp P.False in 
         not (askTheoremProverContext query ac_query) 
      (* if the predicate is NOT valid iff fmla is satisfiable *) 
	 in
	 let _process_var_preds l = 
	 List.fold_left (fun x y -> CaddieBdd.bddOr x (getPredIndexBdd (Misc.fst3 y))) bddZero (List.filter is_sat l)
	 in
	 let rv =  List.fold_left (fun x y -> CaddieBdd.bddAnd (_process_var_preds y) x) bddOne (!custom_preds_wp)
	 in
	 TheoremProver._pop (); 
	 M.msg_string M.Debug "Leaving custom_post_cube";
	 rv
      (* Jhala: ideally i should only fire queries for those variables that are affected by the cmd *)
	 in
      *)

    let _check_Cube _pred_list (cube : int array)  =
      (M.msg_string M.Debug (" In _check_Cube"));
      statReachedCubes := !statReachedCubes + 1;
      conjunctL := [];   
      let _ = 
	if (dc_flag) then Misc.array_reset dontcareArray false;
	if (craig_flag) then 
	  begin
	    Misc.array_reset dontcareArray true; (* RJ: whoa! *)
	    List.iter (fun (i,_,_) -> Array.set dontcareArray i false) pred_list
	      (* make sure we care about the predicates that we care about *)
	  end
      in
	(* if (_syntaxCheck cube modeledPredIndexList) then 
	   () else --syntactic heuristic *)
      let cubePostIsEmpty = 
	match (!assume_pred_ref) with 
            Some(e) -> (askTheoremProver (P.implies (P.conjoinL [(convertCubeToPred cube);e]) P.False))
          | None -> false
      in 
	if (cubePostIsEmpty) then ()
	else 
          begin   
            let pref = match (!assume_pred_ref) with None -> ref [] | Some(e) -> ref [e] in
            let _plist = ref _pred_list in
	      begin
		for i = 0 to (Array.length cube - 1) do
		  if (cube.(i)==2) 
		  then 
		    begin
		      try
			if ((dc_flag) && (outOfScopeArray.(i)))  then
			  begin
			    M.msg_string M.Debug ("DC about: "^(P.toString (getPred i))^" \n"); 
			    Array.set dontcareArray i true
			  end
		      with _ -> ()
		    end
		  else 
		    begin
		      let i_pred = getPred i in
			(* Assume the invariant that i_pred : P.Atom *)
                      let i_unchanged = (Hashtbl.mem unchangedPredTable i)  
		      in
			if (cube.(i)==1) then
			  begin
			    pref := i_pred::(!pref);
			    if (i_unchanged && not(dontcareArray.(i))) 
			    then conjunctL := (i,true)::(!conjunctL);
			  end
			else
			  begin
			    pref := (P.negate i_pred)::(!pref);
			    if (i_unchanged && not(dontcareArray.(i))) 
			    then conjunctL := (i,false)::(!conjunctL);
			  end
		    end
		done;
		
		(* Now ask the thmprover if pref -> each fellow in predlist *)
		if (Stats.time "_check_Pred_list" (_check_Pred_list !pref)  changed_pred_list conjunctL) 
		then 
		  begin
		    (* Jhala: I'm being paranoid about the soundness and aliasing. something to assuage me *)
		    let _ =
		      match (Options.getValueOfBool "scheck") with
			  false -> ()
			| true ->
			    begin
			      let cube_l = List.map 
					     (fun (i,b) -> if b then (getPred i) else P.Not(getPred i))
					     !conjunctL 
			      in
				if (TheoremProver.queryExp (P.Implies ((P.And cube_l),(P.False))) && (!assume_pred_ref = None)) 
				then 
				  begin
				    M.msg_string M.Normal "SCHECK: Post of block is FALSE !!";
				    M.msg_string M.Error  "SCHECK: Post of block is FALSE !!";
				    ()
				  end
				else 
				  ()
			    end
		    in
		      (*  this is where we plug in the post_split_cube *)
		    let splitup_bdd = 
		      if (false (*Options.getValueOfBool "cref"*)) then (refine_cube !pref !conjunctL post_split_preds)
		      else (make_conjunct_bdd !conjunctL) 
		    in
		      sumofcubesBdd := CaddieBdd.bddOr (splitup_bdd) (!sumofcubesBdd)                
			(*
			  let os_succ_cube = (make_conjunct_bdd !conjunctL) in
			  if (Options.getValueOfString "custom" = "") 
			  then 
			  sumofcubesBdd := CaddieBdd.bddOr (make_conjunct_bdd !conjunctL) (!sumofcubesBdd)                
			  else
			  begin
			  let custom_succ_bdd = (custom_post_cube !pref) in
			  sumofcubesBdd := CaddieBdd.bddOr 
			  (CaddieBdd.bddAnd custom_succ_bdd (make_conjunct_bdd !conjunctL)) 
			  (!sumofcubesBdd); 
			  end
			*)
  		  end; () 
		    (* !conjunctL *)
              end;
	  end
    in
      begin
	
	if (CaddieBdd.bddEqual bdd bddZero) then
	  sumofcubesBdd := bddZero
	else 
	  begin
	    if (CaddieBdd.bddEqual bdd bddOne) 
	    then
              let _tmpcube = (* build a temporary cube with nothing in it *)
		Array.make (*size *)(getCurrentIndex ()+2) (* content *)2 
		  
	      (* Note: The size is certainly a vast	overestimate, but this
		 is the easiest way. The "+2" removes additional complications relating
		 to index being -1 initially. Note that this is a fake cube -- we should
		 never try to print it or convert it to a predicate!Ugly hack, but 
		 works. So far *)
              in
		_check_Cube changed_pred_list _tmpcube
	    else
              ignore (Stats.time "bddForEachCube-post" (CaddieBdd.bddForeachCube bdd) (_check_Cube changed_pred_list));
	  end;
	(M.msg_string M.Debug ("Returning from Post-Bdd = "^(P.toString (convertBddToPred !sumofcubesBdd))));
	!sumofcubesBdd 
      end
END CF-postBdd
*)
	
  let wp_table = Hashtbl.create 1009 
  let stackInfo = ref None

  let query_fn bdd (qpred:P.predicate)  =
    let trueval = not (PredTable.isSatisfiable (bdd, [ P.Not qpred ]))
    in let falseval = not (PredTable.isSatisfiable (bdd, [ qpred]))
    in
      if (trueval = falseval) then Ast.Dontknow
      else if trueval then  Ast.True
      else Ast.False

  let sane_pred_list ps = List.filter ((<>) P.True) ps

  let pred_to_assume_cmd _pred =
    {Cmd.code = Cmd.Pred _pred;
     Cmd.read = [];
     Cmd.written = []}

      
  let rec get_bdd_for_pred_list lpredlist __H_cmd tmpbdd regpreds =
    match lpredlist with
      | [] -> tmpbdd
      | hd::tail ->
	  let newtmpbdd = __H_cmd (pred_to_assume_cmd hd) (tmpbdd, []) regpreds
	  in
	  get_bdd_for_pred_list tail __H_cmd newtmpbdd regpreds

  let post_preds abst_data_reg target_loc =
    let tpp = PredTable.lookup_location_predicates target_loc in
    if (tpp =[] && (* O.getValueOfInt "craig" = 2 && *) not (O.getValueOfBool "cf") && O.getValueOfBool "prop") 
    then abst_data_reg.Region.preds 
    else tpp 

  let adp_nextreg adr edge preds bdd' f =
      M.msg_string M.Debug ("in adp_nextreg: "^(Cmd.to_string (C_SD.get_command edge)));
      let (_lpredlist, rlelement) = Stats.time "lattice post" (DataLattice.post adr.Region.lelement edge) (query_fn adr.Region.bdd) in 
      let lpredlist = sane_pred_list _lpredlist in
      let nextbdd = if lpredlist = [] then bdd' else f adr.Region.bdd bdd' preds lpredlist in 
      { Region.lelement = rlelement;
        Region.preds = preds;
        Region.bdd = nextbdd;} 
  
  let rec s_get_bdd_for_pred_list lpredlist __H_cmd tmpbdd regpreds =
    match lpredlist with
      | [] -> tmpbdd
      | hd::tail ->
	  let assume_cmd = pred_to_assume_cmd hd in
	  let str = Cmd.to_string assume_cmd(*.Cmd.code*)
	  in
	  assume_pred_ref := Some (hd) ;
	  let newtmpbdd = __H_cmd (pred_to_assume_cmd hd) tmpbdd regpreds
	  in
	  assume_pred_ref := None ;
	  s_get_bdd_for_pred_list tail __H_cmd newtmpbdd regpreds
 
          
  let out_of_scope pred = 
      let (tos,tos2,call) = match (!stackInfo) with
	  Some (t,t2,c) -> (t,t2,c) 
	| None -> failwith "scope info NOT supplied !" in
      M.log_string_dbg M.BddPost (Printf.sprintf "tos=%s , tos2=%s , call=%s" tos tos2 (string_of_bool call));
      if (Options.getValueOfBool "scope") then
	  let _stay_ff x = 
	    let scope_x = extract_scope x in
	      ((tos = scope_x) || (call && tos2 = scope_x) || (scope_x = "__BLAST_GLOBAL")) in
	  let predVars = Absutil.getVarExps_deep pred in
	    List.exists (fun x -> not (_stay_ff x)) predVars
      else true 
    
  (* Self-recursion. See test/cftest/rec4.c for an example.
   * If there is a call to f() inside f... then we rename:
     * 1. the "Formals" of f, 
     * 2. the symlvals of f,
     * 3. the predicates inside f
     So that they don't clash with the caller region.
     Note that the funcall assignments are: fml = actual and sym_fml = fml.
     For the former, we rename only the fml, in the latter we rename both
     sym_fml and fml as they both correspond to the callee region.
   *)
  let rename_locals lv =
    match lv with
    E.Symbol s ->
      if (String.contains s '@') then (E.Symbol (s^"BLAST"))
      else lv
      | _ -> lv

  let self_rename_pred p = P.alpha_convert rename_locals p
 
  let self_rename_lval lv = E.alpha_convert_lval rename_locals lv
  
  let self_rename_expr e = E.alpha_convert rename_locals e
  
  let self_rename_cmd cmd =
    let self_rename_stm stmt =
      match stmt with
      C_SD.Command.Expr (E.Assignment (E.Assign,lv,e)) ->
        let (lv',e') = 
          if E.is_symbolic lv then (self_rename_lval lv, self_rename_expr e)
          else (self_rename_lval lv, e)
        in C_SD.Command.Expr (E.Assignment (E.Assign, lv',e'))
      | _ -> stmt
    in
    let new_code = 
      match cmd.C_SD.Command.code with
      C_SD.Command.Block el -> C_SD.Command.Block (List.map self_rename_stm el)
      | _ -> cmd.C_SD.Command.code
    in
    {cmd with C_SD.Command.code = new_code}
  
  let is_self_recursion op =
     let src = C_SD.get_source op in
     match Operation.get_info op with
      Operation.Call (_,_,callee_name,_) -> callee_name = C_SD.get_location_fname src
      | _ -> false 
      

  let _H_cmd_gs edge target_loc predlist self_recursion_flag phi_table _cmd bdd actual_predlist =
    let _cmd = if (self_recursion_flag) then self_rename_cmd _cmd else _cmd in
    let src_loc = C_SD.location_coords (C_SD.get_source edge) in
    let e_id = (src_loc,target_loc) in
    let prop_eq_bdd a =
      if Options.getValueOfBool "bddpost" then
        PredTable.update_eq_transrel_bdd e_id [a] in
    let make_pred_triple_cached () a =
	try Hashtbl.find wp_table (a,e_id) with Not_found ->
        let rv = 
          let pr = PredTable.getPred a in
          let pr = if (self_recursion_flag) then self_rename_pred pr else pr in
          match (!assume_pred_ref) with 
            None ->
              let cr = concrete_data_pre false false { Region.pred = pr ; } _cmd edge phi_table in
              let wppos = cr.Region.pred in
              let _ = 
                if (wppos <> pr) then PredTable.update_pred_affected_table e_id a 
                else (prop_eq_bdd a) in
              let ncr = concrete_data_pre false false { Region.pred = P.negate pr ; } _cmd edge phi_table in
              let wpneg = ncr.Region.pred in
              (a,wppos,wpneg, (out_of_scope pr))
          | Some(e) -> (prop_eq_bdd a; (a,pr,P.negate pr,(out_of_scope pr))) in 
        (Hashtbl.replace wp_table (a,e_id) rv; rv) in
    let post_split_preds = if (Options.getValueOfInt "craig" = 1) then predlist else [] in
    let pred_triple_ac = Stats.time "mk_pred_triple ac" (List.map (make_pred_triple_cached ())) actual_predlist in
    let pred_triple_sp = Stats.time "mk_pred_triple sp" (List.map (make_pred_triple_cached ())) post_split_preds in
	(* {{{ options begin -- bddpost,hybrid,dasdillpark *)
	(** this is code for exact predicate abstraction using various dec. procs  
	  if (Options.getValueOfString "post" = "Hsimplify") then
	  Stats.time "postBdd_ddp" (postBdd_ddp_inc_simplify bdd !assume_pred_ref) pred_triple_ac
	  else if (Options.getValueOfString "post" = "Hfoci") then
	  Stats.time "postBdd_ddp" (postBdd_ddp_inc_foci bdd !assume_pred_ref) pred_triple_ac
	  else if (Options.getValueOfString "post" = "H") then
	  let b1 = Stats.time "postBdd_ddp_foci" (postBdd_ddp_inc_foci bdd !assume_pred_ref) pred_triple_ac in
	  let b2 = Stats.time "postBdd_ddp_simp" (postBdd_ddp_inc_simplify bdd !assume_pred_ref) pred_triple_ac
	  in
	  if not (CaddieBdd.bddEqual b1 b2) then
          begin
          M.msg_string M.Error "foci,simplify disagree!";
          M.msg_string M.Normal "foci says:";
          M.msg_string M.Normal (P.toString(PredTable.convertBddToPred b1));
          M.msg_string M.Normal "simplify says:";
          M.msg_string M.Normal (P.toString(PredTable.convertBddToPred b2));
          exit 0
          end
	  else b1
      else }}} *) 
      if (Options.getValueOfBool "bddpost") then  
          let _ = Stats.time "mk_pred_triple cp" (List.map (make_pred_triple_cached ())) (PredTable.bdd_support bdd) in
	  let b2 = Stats.time "postBdd-bdd" (postBdd_bdd bdd (src_loc,target_loc)) [] in     
	  let b1 = 
            if (O.getValueOfBool "hybrid") then
              let bdd_cov = Stats.time "bdd_cube_cov" PredTable.bdd_cube_cover bdd in
		Stats.time "postBdd" (postBdd bdd_cov pred_triple_ac) pred_triple_sp
            else PredTable.bddOne   
	  in (CaddieBdd.bddAnd b1 b2) 
      else Stats.time "postBdd" (postBdd bdd pred_triple_ac) pred_triple_sp 
       
  
  let make_async_op op adr = 
    assert (C_SD.isAsyncCall op);
    let cmd = C_SD.get_command op in
    let cmds = Cmd.to_string cmd in 
    M.msg_string M.Debug ("in make_async_op: "^cmds);
    match cmd.Cmd.code with Cmd.FunctionCall fcallExp ->
      let (fname,_subs,_) = C_SD.deconstructFunCall fcallExp in
      let tloc = (C_SD.location_coords (C_SD.lookup_entry_location fname)) in
      let ps = post_preds adr tloc in
      M.msg_string M.Normal (Printf.sprintf "make_async_op preds: %s : %s" cmds (Misc.string_of_int_list ps));
      let asgns = 
          {cmd with Cmd.code = Cmd.Block (List.map (fun (x,e) -> Cmd.Expr(E.Assignment (E.Assign,E.Symbol(x),e))) _subs)} in
      let bdd' = _H_cmd_gs op tloc ps (is_self_recursion op) (Hashtbl.create 2) asgns adr.Region.bdd ps in
      let rv = Events.constructAsyncCall (op,bdd') in
      M.msg_string M.Normal (Printf.sprintf "make_async_op returns: %s" (Cmd.to_string (C_SD.get_command rv)));
      rv
    | _ -> (assert false; failwith "err: make_async_op")

  let adp_async op adr =
    M.msg_string M.Debug ("in adp_async:"^(C_SD.edge_to_string op));
    assert (C_SD.isAsyncCall op || C_SD.isAsyncExec op);
    let f _ b' _ _ = b' in 
    let cmd = C_SD.get_command op in
    match cmd.Cmd.code with Cmd.FunctionCall fcallExp ->
      if C_SD.isAsyncCall op then
        let _ = M.msg_string M.Debug "is async call" in 
        let op' = make_async_op op adr in
          adp_nextreg adr op' adr.Region.preds adr.Region.bdd f 
      else 
       (assert (C_SD.isAsyncExec op);
        let _ = M.msg_string M.Debug "is async exec" in 
        let (_,bdd') = Events.deconstructAsyncCall op in
        let (fname,_,_) = C_SD.deconstructFunCall fcallExp in
        let ps = post_preds adr (C_SD.location_coords (C_SD.lookup_entry_location fname)) in
        adp_nextreg adr op ps (CaddieBdd.bddAnd bdd' adr.Region.bdd) f)
    | _ -> (assert false; failwith "non-funcall to adp_async")
          
      
  let rec abstract_data_post_graf_saidi abst_data_reg cmd edge post_location phi_table =
    M.log_string_norm M.BddPost "In abstract_data_post GRAF_SAIDI_POST -- arguments are:" ;
    M.log_print_norm M.BddPost Region.print_abstract_data_region abst_data_reg ;
    M.log_print_norm M.BddPost Cmd.print cmd ;
    let src = C_SD.get_source edge in
    let src_loc = C_SD.location_coords src in
    let target_loc = C_SD.location_coords post_location in
    let e_id = (src_loc,target_loc) in
    M.log_string_norm M.BddPost (Printf.sprintf "absdatapost preloc: (%d,%d)" (fst src_loc)  (snd src_loc));
    M.log_string_norm M.BddPost (Printf.sprintf "absdatapost postloc: (%d,%d)" (fst target_loc)  (snd target_loc)); 
    (* SKY: in predlist predicates in target locations are stored *)
    let predlist = Stats.time "post_preds" (post_preds abst_data_reg) target_loc in
    let _H_cmd = _H_cmd_gs edge target_loc predlist (is_self_recursion edge) phi_table in
    let _H = _H_cmd cmd in
    if (*CF*)((not (O.getValueOfBool "events")) && predlist = [] && (CaddieBdd.bddEqual abst_data_reg.Region.bdd PredTable.bddOne))
    then
      adp_nextreg abst_data_reg edge predlist abst_data_reg.Region.bdd 
        (fun b _ p p' -> s_get_bdd_for_pred_list p' _H_cmd b p)
    else 
      (* This function is detached to share code between guarded and usual function calls *)
      let handle_funciton_call fcallExp =
	if O.getValueOfBool "events" && (C_SD.isAsyncExec edge || C_SD.isAsyncCall edge)
	then adp_async edge abst_data_reg
	else
	  let (fname,_subs,target) = (C_SD.deconstructFunCall fcallExp) in
	  let asgn_block = {cmd with Cmd.code = Cmd.Block (Absutil.asgn_of_funcall (true,false) None fcallExp)} in
	  let bdd' = _H_cmd asgn_block (abst_data_reg.Region.bdd) predlist in
	  let r' = adp_nextreg abst_data_reg edge predlist bdd'
		    (fun b b' p p' -> s_get_bdd_for_pred_list p' _H_cmd b' p) in
	  if (O.getValueOfBool "cf" && C_SD.is_defined fname) then
	    Region.restrictAbstractData (Region.locals_and_globals fname) r'
	  else r'
      in
      match cmd.Cmd.code with 
          Cmd.Skip 
	| Cmd.SymbolicHook _
	| Cmd.Block [] 
	| Cmd.Pred (P.True) -> 
            begin
              M.log_string_dbg M.BddPost "Treats as skip";
              if (Options.getValueOfBool "bddpost") then
		{abst_data_reg with 
                 Region.bdd = _H (abst_data_reg.Region.bdd) predlist;
                 Region.preds = predlist;}
              else abst_data_reg
            end
	| Cmd.FunctionCall fcallExp ->
            M.log_string_dbg M.BddPost ("In the function call case: "^(E.toString fcallExp));
	    handle_funciton_call fcallExp
              
	(* GuardedFunctionCall is a call guarded with predicate; generated from function pointer call by alias analysis algorithm *)
	| Cmd.GuardedFunctionCall (pred, fcallExp) -> begin
            M.log_string_dbg M.BddPost (Printf.sprintf "Guarded Function Call: calling %s if %s" (E.toString fcallExp) (P.toString pred));
	    assume_pred_ref := Some(pred);
	    let retReg = handle_funciton_call fcallExp in
	    assume_pred_ref := None;
	    retReg
	end
        | Cmd.Block l  -> 
	    if ( not (List.exists (function x -> match x with Cmd.Return e -> true | _ -> false) l))
	    then 
	      let _ = Absutil.reset_ret_pointers () in
              let bdd' = _H  (abst_data_reg.Region.bdd) predlist in
              adp_nextreg abst_data_reg edge predlist bdd' 
                (fun b b' p p' -> s_get_bdd_for_pred_list p' _H_cmd b' p)
	    else 
	      let (targ, fname, retloc , subs) = lastTarget_loc (Some src) in
	      let _ = Absutil.set_ret_pointers targ subs (C_SD.get_location_fname retloc) in 
	      let (newPreds,delPreds) = Absutil.removeFunPreds fname abst_data_reg.Region.preds in
              let new_preds =
                let tp = PredTable.lookup_location_predicates target_loc in
                if tp = [] then newPreds@delPreds else tp in
              let newBdd = (_H  (abst_data_reg.Region.bdd) predlist) in
	      let _ = Absutil.reset_ret_pointers () in
	      adp_nextreg abst_data_reg edge new_preds newBdd 
                 (fun b b' p p' -> s_get_bdd_for_pred_list p' _H_cmd b' p)
        | Cmd.Pred e -> 
              if (e = P.True) then ( M.msg_string M.Debug "thinks its true!";abst_data_reg)
	      else 
                begin
                  assume_pred_ref := Some(e);
                  let bdd' =  _H (abst_data_reg.Region.bdd) predlist in
	          let retReg = 
                  adp_nextreg abst_data_reg edge predlist bdd' 
                    (fun b b' p p' -> s_get_bdd_for_pred_list p' _H_cmd b' p) in  
                  assume_pred_ref := None;          
                  retReg
	        end
	| Cmd.Phi (sym, flag) -> 
	    let e = phi_command_to_pred sym flag phi_table in
              (* Now treat just like Cmd.Pred e *)
	      if (e = P.True) then abst_data_reg
	      else 
		begin
		  assume_pred_ref := Some(e);
		  let retReg = 
		    {Region.bdd = _H (abst_data_reg.Region.bdd) predlist ;
		     Region.preds = predlist ;
		     Region.lelement = abst_data_reg.Region.lelement;}
		  in  
		    assume_pred_ref := None;          
		    retReg
		end
	| Cmd.Havoc sym -> 
	    let new_var = 
	      match Absutil.getNewTmpVar (E.Symbol "__foo") with 
		  E.Chlval (E.Symbol x, s) -> E.Lval (E.Symbol (s^x))
		| _ -> failwith "Abstraction.concrete_data_pre: Havoc: TBD" in
	    let assign = E.Assignment(E.Assign, sym, new_var) in
	    let stmt = Cmd.Expr assign in
	    let cmd' = {cmd with Cmd.code = Cmd.Block [stmt]} in
	      abstract_data_post_graf_saidi abst_data_reg cmd' edge post_location phi_table
	| Cmd.HavocAll -> failwith "Abstraction.abstract_data_post_graf_Saidi: HavocAll: TBD"       

  (* CF  
  let rec abstract_data_post_graf_saidi abst_data_reg cmd edge post_location phi_table =
    M.msg_string M.Debug "In abstract_data_post GRAF_SAIDI_POST -- arguments are:" ;
    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg ;
    M.msg_printer M.Debug C_SD.Command.print cmd ;
    let src = C_SD.get_source edge in
    let self_recursion_flag =
      match Operation.get_info edge with
      Operation.Call (_,_,callee_name,_) -> callee_name = C_SD.get_location_fname src
      | _ -> false in
    let target_loc  = C_SD.location_coords post_location (* (C_SD.get_target edge) *) in
    let _ = M.msg_string M.Debug
	      (Printf.sprintf "absdatapost postloc: (%d,%d)" (fst target_loc)  (snd target_loc)) 
    in
    let craig_flag = Options.getValueOfInt "craig" = 2 in
    let craig_flag1 = Options.getValueOfInt "craig" = 1 in
    let craig_preds () = 
      try Hashtbl.find loc_pred_table target_loc with Not_found -> [] in
    let predlist =
      (!globally_useful_preds 
      @ (try Hashtbl.find fun_pred_table (fst target_loc) with Not_found -> []))
    in
    let _H_cmd _cmd bdd actual_predlist =
      M.msg_string M.Debug ("In _H of abstract_data_post, bdd = "^(P.toString (convertBddToPred bdd)));
      let (tos,tos2,call) = match (!stackInfo) with
	  Some (t,t2,c) -> (t,t2,c) 
	| None -> failwith "scope info NOT supplied !"
      in
      let _ = M.msg_string M.Debug
		(Printf.sprintf "tos=%s , tos2=%s , call=%s" tos tos2 (string_of_bool call)) in
      let out_of_scope pred = 
	if (Options.getValueOfBool "scope") then
	  let _stay_ff x = 
	    let scope_x = (extract_scope x) in
	      ((tos = scope_x) || (call && tos2 = scope_x) || (scope_x = "__BLAST_GLOBAL"))
	  in
	  let predVars = getVarExps_deep pred in
	    List.exists (fun x -> not (_stay_ff x)) predVars
	else 
	  true
      in
      let _cmd = if (self_recursion_flag) then self_rename_cmd _cmd else _cmd in
      let make_pred_triple () (a : int) = 
	let pr = getPred a in
        let pr = if (self_recursion_flag) then self_rename_pred pr else pr in
          match (!assume_pred_ref) with 
	      None ->
		let cr = concrete_data_pre false false { Region.pred = pr ; } _cmd edge phi_table in
		let wppos = cr.Region.pred in
		let ncr = concrete_data_pre false false { Region.pred = P.negate pr ; } _cmd edge phi_table in
		let wpneg = ncr.Region.pred in
                  (*if (wppos,wpneg) = (_check_wppos,_check_wpneg) then*)
                  (a,wppos,wpneg, (out_of_scope pr))
                    (* else failwith ("bad wp_table")*)
            | Some(e) -> (a,pr,P.negate pr,(out_of_scope pr) )
      in
      let post_split_preds = if craig_flag1 then craig_preds () else [] in
	Stats.time "postBdd" (postBdd bdd  (List.map ( Stats.time "mk_pred_triple" make_pred_triple ()) actual_predlist)) (List.map (make_pred_triple ()) post_split_preds)
    in
    let _H = _H_cmd cmd in
      if (predlist = [] && (CaddieBdd.bddEqual abst_data_reg.Region.bdd PredTable.bddOne))
      then
        adp_nextreg abst_data_reg edge predlist abst_data_reg.Region.bdd 
          (fun b _ p p' -> s_get_bdd_for_pred_list p' _H_cmd b p)
      else
	match cmd.C_SD.Command.code with 
	    C_SD.Command.FunctionCall fcallExp 
	  | C_SD.Command.GuardedFunctionCall (_, fcallExp) -> (* TBD: RUPAK: treat as an assume followed by a functioncall *)
	      let _ = M.msg_string M.Debug "NEW CDP" in
	      let caller = abst_data_reg in
	      let (fname, _ , _ ) = (C_SD.deconstructFunCall fcallExp) in
	      let asgn_block =
		{cmd with
		   C_SD.Command.code = C_SD.Command.Block (Absutil.asgn_of_funcall
							     (true,false) None fcallExp)}
	      in
	      let (_lpredlist, rlelement) = DataLattice.post caller.Region.lelement edge (query_fn caller.Region.bdd) in
	      let lpredlist = sane_pred_list _lpredlist in
	      let region =
		  (if (List.length lpredlist = 0)
		   then 
		     {
		       Region.lelement = rlelement;
      		       Region.preds = caller.Region.preds;
      		       Region.bdd = _H_cmd asgn_block (caller.Region.bdd) predlist;
		     }
		   else
		     let tmppreds = caller.Region.preds
		     in let tmpbdd = _H_cmd asgn_block (caller.Region.bdd) predlist
		     in let tmpbdd2 = s_get_bdd_for_pred_list lpredlist _H_cmd tmpbdd predlist
		     in
		       {
			 Region.lelement = rlelement;
			 Region.preds = tmppreds;
			 Region.bdd = tmpbdd2
		       }
		  )
	      in
		if (C_SD.isAsyncCall edge || not (C_SD.is_defined fname)) then region
		else
		  begin
		    M.msg_string M.Debug ("Restricting region across call to: "^fname);
		    M.msg_printer M.Debug Region.print_abstract_data_region region;
		    let region = Region.restrictAbstractData (Region.locals_and_globals fname) region in
		      M.msg_string M.Debug "-->";
		      M.msg_printer M.Debug Region.print_abstract_data_region  region;
		      region
		  end
          | C_SD.Command.Skip ->  abst_data_reg
	  | C_SD.Command.SymbolicHook _ -> abst_data_reg 
	  | C_SD.Command.Block [] ->  abst_data_reg
	  | C_SD.Command.Block l  -> 
	      if ( not (List.exists (function x -> match x with C_SD.Command.Return e -> true | _ -> false) l))
	      then 
		let _ = (Absutil.reset_ret_pointers ()) in
		let (_lpredlist, rlelement) = DataLattice.post abst_data_reg.Region.lelement edge (query_fn abst_data_reg.Region.bdd)
		in let lpredlist = sane_pred_list _lpredlist
		in if (List.length lpredlist = 0)
		  then 
		    { 
	              Region.lelement = rlelement;
      		      Region.preds = abst_data_reg.Region.preds;
      		      Region.bdd =   _H  (abst_data_reg.Region.bdd) predlist (*abst_data_reg.Region.preds*);
		    }
		  else
		    let tmppreds = abst_data_reg.Region.preds
		    in let tmpbdd =  _H  (abst_data_reg.Region.bdd) predlist
		    in let tmpbdd2 = s_get_bdd_for_pred_list lpredlist _H_cmd  tmpbdd  predlist
		    in 
		    {
		      Region.lelement = rlelement;
		      Region.preds = tmppreds;
		      Region.bdd = tmpbdd2
		    }
	      else 
		let (targ, fname, retloc ,subs) = lastTarget () in
		  (* Jhala: the t.o.s. is passed using last_loc_pointer  reg.Region.stack *) 
		let _ = Absutil.set_ret_pointers targ subs (C_SD.get_location_fname retloc) in
		let (newPreds,delPreds) = removeFunPreds fname abst_data_reg.Region.preds in
		let _ = M.msg_string M.Debug "Return: Calling _H" in
		let newBdd = (_H  (abst_data_reg.Region.bdd) predlist) in
		let _ = M.msg_string M.Debug "Return: Back from _H" in
		let ex_newBdd = newBdd in
		let _ = Absutil.reset_ret_pointers () in
		let (_lpredlist, rlelement) = DataLattice.post abst_data_reg.Region.lelement edge (query_fn ex_newBdd)
		in let lpredlist = sane_pred_list _lpredlist
		in if (List.length lpredlist = 0) 
		  then
		    { 
		      Region.lelement = rlelement;
      		      Region.preds = newPreds@delPreds;
      		      Region.bdd =  ex_newBdd;
		    }
		  else
		    let tmppreds = newPreds@delPreds
		    in let tmpbdd = ex_newBdd
		    in let tmpbdd2 = s_get_bdd_for_pred_list lpredlist _H_cmd tmpbdd (newPreds@delPreds)
		    in 
		    {
		      Region.lelement = rlelement;
		      Region.preds = tmppreds;
		      Region.bdd = tmpbdd2
		    }
	  | C_SD.Command.Pred e -> 
	      if (e = P.True) then ( M.msg_string M.Debug "thinks its true!";abst_data_reg)
	      else 
		begin
		  assume_pred_ref := Some(e);
		  M.msg_string M.Debug "calling _H";
		  let (_lpredlist, rlelement) = DataLattice.post abst_data_reg.Region.lelement edge (query_fn abst_data_reg.Region.bdd)
		  in let lpredlist = sane_pred_list _lpredlist
		  in 
		  let retReg =
		    (if (List.length lpredlist = 0)
		     then
		       { 
			 Region.lelement = rlelement;
			 Region.bdd = _H (abst_data_reg.Region.bdd) predlist (*abst_data_reg.Region.preds*) ;
			 Region.preds = abst_data_reg.Region.preds ;
		       }
		     else
		       let tmppreds = abst_data_reg.Region.preds
		       in let tmpbdd = _H (abst_data_reg.Region.bdd) predlist (*abst_data_reg.Region.preds*)
		       in let tmpbdd2 = s_get_bdd_for_pred_list lpredlist _H_cmd tmpbdd predlist
		       in 
		       {
			 Region.lelement = rlelement;
			 Region.preds = tmppreds;
			 Region.bdd = tmpbdd2
		       }
		    )
		  in  
		    assume_pred_ref := None;          
		    retReg
		end
	  | C_SD.Command.Phi (sym, flag) -> 
	      let e = phi_command_to_pred sym flag phi_table in
		(* Now treat just like C_SD.Command.Pred e *)
		if (e = P.True) then abst_data_reg
		else 
		  begin
		    assume_pred_ref := Some(e);
		    let retReg = 
		      {
			Region.lelement = failwith "Do not handle Phi" ;
			Region.bdd = _H (abst_data_reg.Region.bdd) predlist (* abst_data_reg.Region.preds*) ;
			Region.preds = abst_data_reg.Region.preds ;
		      }
		    in  
		      assume_pred_ref := None;          
		      retReg
		  end
	  | C_SD.Command.Havoc sym -> 
	      let new_var = 
		match getNewTmpVar (E.Symbol "__foo") with 
		    E.Chlval (E.Symbol x, s) -> E.Lval (E.Symbol (s^x))
		  | _ -> failwith "Abstraction.concrete_data_pre: Havoc: TBD"
	      in
	      let assign = E.Assignment(E.Assign, sym, new_var) in
	      let stmt = C_SD.Command.Expr assign in
	      let cmd' = {cmd with C_SD.Command.code = C_SD.Command.Block [stmt]} in
		abstract_data_post_graf_saidi abst_data_reg cmd' edge post_location phi_table
	  | C_SD.Command.HavocAll -> failwith "Abstraction.abstract_data_post_graf_Saidi: HavocAll: TBD"       
	     
  (* END CF *)
*) 


  (* TBD:OBSOLETE!*)
  let abstract_data_post_das_dill abst_data_reg cmd edge post_location phi_table =
    failwith "abstract_data_post_das_dill is obsolete";
    M.msg_string M.Debug "In abstract_data_post -- arguments are:" ;
    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg ;
    M.msg_printer M.Debug Cmd.print cmd ;
    (* Implementation of the H function from DasDillPark-CAV99 *)
    let rec _H_cmd _cmd (bdd,p) predlist =
      M.msg_string M.Debug ("pred = "^(match p with [] -> "" | _ -> P.toString (List.hd p)));
      (* optimization: check if (abst_data_reg, p) is satisfiable? *)
      if (PredTable.isSatisfiable (bdd,p)) then
	begin
	  match predlist with
	      [] -> if (PredTable.isSatisfiable (bdd,p)) then PredTable.bddOne else PredTable.bddZero
	    | a:: rest ->
		let pr = PredTable.getPred a in
		let cr = concrete_data_pre false false { Region.pred = pr ; } _cmd edge phi_table in
		let wppos = cr.Region.pred in
		let ncr = concrete_data_pre false false { Region.pred = P.negate pr ; } _cmd edge phi_table in
		let wpneg = ncr.Region.pred
		in
		  CaddieBdd.bddOr 
		    (CaddieBdd.bddAnd (PredTable.getPredIndexBdd a) (_H_cmd _cmd (bdd, wppos :: p) rest))
		    (CaddieBdd.bddAnd (CaddieBdd.bddNot (PredTable.getPredIndexBdd a)) (_H_cmd _cmd (bdd, wpneg :: p) rest))
	end
      else PredTable.bddZero in
    let _H = _H_cmd cmd in
        match cmd.Cmd.code with 
	  Cmd.FunctionCall fcallExp ->
	    let (fname,_subs,_) = (C_SD.deconstructFunCall fcallExp) in 
	    let asgn_block =
	      { cmd with Cmd.code = Cmd.Block (List.map (fun (x,e) -> Cmd.Expr( E.Assignment (E.Assign,E.Symbol(x), e))) _subs) }
	    in
            let bdd' = _H_cmd asgn_block (abst_data_reg.Region.bdd, []) abst_data_reg.Region.preds in
            adp_nextreg abst_data_reg edge abst_data_reg.Region.preds bdd' 
               (fun b _ p _ -> _H_cmd asgn_block (b,[]) p) 
            		  
	(*let subs = List.map Misc.swap _subs in
	  let rec _make_copy_list lst c_lst pBdd = 
	  match lst with
	  [] -> (c_lst,pBdd)
	  | h::t -> 
	  let pred_h = PredTable.getPred h in
	  let cpred_h = funCallSubstitute subs pred_h in
	  if (cpred_h = pred_h) 
	  then (_make_copy_list t c_lst pBdd)  (* no copy made *)
	  else (
	  let cpred_i = addPred cpred_h in
	  let nextPBdd = CaddieBdd.bddAnd (CaddieBdd.bddBiimp
	  (PredTable.getPredIndexBdd h)
	  (PredTable.getPredIndexBdd
	  cpred_i)) pBdd in
	  _make_copy_list t (cpred_i::c_lst) nextPBdd) (* copy made *)
	  in 
	  let (subsPreds, newBdd) = _make_copy_list abst_data_reg.Region.preds [] abst_data_reg.Region.bdd
	  in
	  {
	  Region.lelement = DataLattice.... ;
	  Region.bdd = newBdd ;
	  Region.preds = abst_data_reg.Region.preds} *)
	(* What was just done for the function call ... *) 
	(* Push stuff onto the stack: ... in the rest of post 
	   1. edge, on return, the location will be
	   the other end of the edge ...
	   2. if edge was a y = f(x) then post on return reflects this *)
	(* Go through list of predicates and compute "copies" *)
	(* Compute BDD of reg ^_i (pred_i = copy_pred_i) *)
	(* Start location = start location of function *)
	|	Cmd.Block l  -> 
		  if ( not (List.exists (function x -> match x with Cmd.Return e -> true | _ -> false) l))
		  then 
		    let _ = Absutil.reset_ret_pointers () in
                    let bdd' = _H (abst_data_reg.Region.bdd, []) abst_data_reg.Region.preds in
                    adp_nextreg abst_data_reg edge abst_data_reg.Region.preds bdd' 
                      (fun _ b' p p' -> get_bdd_for_pred_list p' _H_cmd b' p)
		  else 
		    let (targ, fname, retloc ,subs) = lastTarget_loc (Some(C_SD.get_source edge)) in
		      (* Jhala: the t.o.s. is passed using last_loc_pointer  reg.Region.stack *) 
		    let _ = Absutil.set_ret_pointers targ subs (C_SD.get_location_fname retloc) in
		    let (newPreds,delPreds) = Absutil.removeFunPreds fname abst_data_reg.Region.preds in
		    let _ = M.msg_string M.Debug "Return: Calling _H" in
		    let bdd' = (_H  (abst_data_reg.Region.bdd, []) abst_data_reg.Region.preds) in
		    let _ = M.msg_string M.Debug "Return: Back from _H" in
		    let _predToVar i =  -1 in (* JHALA: To be done ... ??? *)
		    let _ = Absutil.reset_ret_pointers () in
                    let tmppreds = newPreds @ delPreds in
                    adp_nextreg abst_data_reg edge tmppreds bdd' 
                      (fun b b' p p' -> get_bdd_for_pred_list p' _H_cmd b' p)
	| _ ->
            let bdd' = _H (abst_data_reg.Region.bdd, []) abst_data_reg.Region.preds in 
	    adp_nextreg abst_data_reg edge abst_data_reg.Region.preds bdd' 
               (fun b b' p p' -> get_bdd_for_pred_list p' _H_cmd b' p)

