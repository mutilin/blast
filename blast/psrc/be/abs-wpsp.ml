(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(* This file has code for lvmaps, block_wp, block_sp, wp, sp,
   block_sp is only used by foci_model_checker, which also uses several lvmaps.
   Hence, only block_sp takes an lvmap parameter.
   The others just use the default lvmap.
*)

   (***************************************************************************)
   (*         weakest preconditions and strongest postconditions              *)
   (***************************************************************************)

(* A function to "simplify" address equality predicates *)
(* TBD: generalize this to a "unification" style procedure that spits out the
 * list outstanding obligations *)
let rec simplify_address_equality (a,a') =
  match (a,a') with
    (E.Binary (op1,e1,e1'), E.Binary (op2,e2,e2')) when op1 = op2 && e1 = e2 -> 
       simplify_address_equality (e1',e2')
  | (E.Binary (op1,e1,e1'), E.Binary (op2,e2,e2')) when op1 = op2 && e1' = e2' ->
       simplify_address_equality (e1,e2)
  | _ -> (a,a')
  
  
let strip_quantifier_prefix qp =
  let rec strip_worker qlist qp =
    match qp with
      P.All (s, qp') -> strip_worker ((true, s)::qlist) qp' 
    | P.Exist (s, qp') ->  strip_worker ((false, s)::qlist) qp'
    | _ -> (qlist, qp)
  in
  strip_worker [] qp

let reattach_quantifier_prefix qlist qp =
  let rec attacher qlist qp =
    match qlist with
      [] -> qp
    | (qf, qvar)::rest -> attacher rest (if qf then P.All(qvar, qp) else P.Exist(qvar,qp))
  in
  attacher qlist qp


   (* compute WP for expressions involving pointers.  in particular, we have
      to consider aliasing if we are given an assignment statement.
      otherwise, we do the standard WP computation. 
      possibleAliases is the set of alias pairs that need to be considered for an
     assignment statement *)

  (* It does explicit substitutions when the flag is true.
     *)
 (* RJ: my head hangs in shame re: the compress-flag *)
 let compress_flag = ref false
 (* note that when wp is called with compress_flag set to true, it must be that
    explicit_flag is also true *)
 (* this is really only to be called when doing the interpolant thing *)

 (* pred_l is a list of predicates -- the first of which is the one where we will add stuff 
 while the others correspond to constraints further down the trace and so we merely substitute
 and so on in them. It returns a pred_list as well ... *)
 let make_exp_lval e = 
   match e with
       E.Lval l -> l
     | _ -> failwith ("bad call to make_exp_lval: "^(E.toString e))


(* Incremental aliases (-ialias) *)

let aliaser = ref AliasAnalyzer.empty_aliaser

let initialize_aliaser () = aliaser := AliasAnalyzer.empty_aliaser

let lval_to_aliaser lval = aliaser := (AliasAnalyzer.add_lval_to_aliaser !aliaser lval)

let query_aliaser lval = AliasAnalyzer.query_aliaser !aliaser lval

let debug_print_aliaser () = M.msg_printer M.Debug AliasAnalyzer.print_aliaser !aliaser


 
(* THE NEXT TWO FUNCTIONS ARE NOT USED ANYWHERE! *)
 (* VVVVIMP : TBD make this trace check faster ! -- RJ: this is NOT used ANYWHERE right now *)
 let trace_lv_table = Hashtbl.create 109  (* this is actually a table of expressions *)
(* WARNING! this table is not actually READ anywhere ... *)
(* SKY: omg it's so true :-), unless '-talias' option is in use *)

 let trace_field_table = Hashtbl.create 17 (* just stores the fields of lvs in the trace *)

 let is_relevant_field_trace fld =
   Hashtbl.mem trace_field_table fld
		
 let this_trace_field_table = Hashtbl.create 17 (* store the fields added BY THIS TRACE, FLUSHED at the end of the refinement *)

 
 (* lval list -> unit *) 
 (* This initializes lv_table AND adds fileds to the hash table in C_SD, such that these fields only are considered relevant in the subsequent closures computed for weakest precondition *)
 let add_to_lv_table candidates = 
    (*let candidates = Absutil.getVarsAndDerefs pred in*)
    M.msg_string M.Debug "Adding to lv_table:";
      List.iter (fun x ->
		   M.msg_string M.Debug (E.toString x);
		   Hashtbl.replace trace_lv_table x true) candidates;
      let c_fields = List.flatten (List.map (E.fields_of_expr) candidates) in
	M.msg_string M.Debug ("Adding Fields "^(Misc.strList c_fields));
	List.iter 
	  (fun fld ->
	     Hashtbl.replace trace_field_table fld true;
	     let added = C_SD.add_field_to_unroll true fld in
             if added then Hashtbl.replace this_trace_field_table fld true
	  ) c_fields

	
let lv_of_expr = E.lv_of_expr

let trace_lv_table_get_lval_aliases =
  let trace_lv_table_iter f =
    let _iter e _ =
      try
	let lv = lv_of_expr e in
	  f lv
      with _ -> M.msg_string M.Major
	("Warning: tlv_get_alias skipping "^(E.toString e))
    in
    Hashtbl.iter _iter trace_lv_table
  in
    AliasAnalyzer.get_lval_aliases_iter Misc.traceLvals_t_c trace_lv_table_iter 
(* this function has been moved to aliasAnalyzer.ml *)
let ac_get_lval_aliases a_flag lv =
  if a_flag then 
    let _ = Message.msg_string Message.Debug "SKY in ac_get_lval_aliases" in
    (* NOMUSTS: with this option turned on, we first calculate may-aliases of variable and then mix must aliases in. Otherwise, we insert must aliases and calculate may aliases of them as well *)
    let must_aliases = AliasAnalyzer.get_all_must_aliases lv in
    let lv_l = lv::(if (not (O.getValueOfBool "nomusts")) then must_aliases else []) in 
    (* this functions gets may-aliases that are succesfully re-used in path formula later *)
    let alias_getter = if (Options.getValueOfBool "nomusts") then AliasAnalyzer.get_nomust_lval_aliases else AliasAnalyzer.get_lval_aliases in
    let after_may = if (O.getValueOfBool "nomusts") then must_aliases else [] in
    (* Compute the aliases from the usual alias engine, unless we only use the incremental ones *)
    let use_ialias = O.getValueOfBool "use-ialias" in
    let do_ialias = O.getValueOfBool "ialias" in
    let usual_aliases = if not use_ialias then List.fold_left (fun acc lv -> List.rev_append (alias_getter lv) acc) [] lv_l else [] in
    (* Compute the aliases from the incremental alias engine, if necessary *)
    let incremental_aliases = if do_ialias then List.fold_left (fun acc lv -> List.rev_append (query_aliaser lv) acc) [] lv_l else [] in
    (* Report the aliasing result *)
    let _ = if (not use_ialias) && do_ialias then M.msg_string M.Normal (Printf.sprintf "Usual  aliases of %s: %s\nIncrem aliases of %s: %s" (E.lvalToString lv) (String.concat ", " (List.map E.lvalToString usual_aliases)) (E.lvalToString lv) (String.concat ", " (List.map E.lvalToString incremental_aliases))) else () in
    let rv = Misc.sort_and_compact (List.rev_append (if use_ialias then incremental_aliases else usual_aliases) after_may) in
    rv
  else [lv]

(* temporal solution for suppressing alias analysis for initialization *)
let suppress_aliases = ref false

let get_lval_aliases lv = ac_get_lval_aliases (not (O.getValueOfBool "incref") && not (!suppress_aliases)) lv

 
(* TBD:ARRAYS *)
let aliases_in_scope fname target alias_candidates =
  let relevant_scopes = (* TBD:CF+pointer?CHECK for now, lets just keep it simple *)
    Misc.cons_if fname
      (List.filter (fun s -> s <> "") (C_SD.scope_of_lval target))
  in
  let formals = match C_SD.lookup_formals fname with
      C_SD.Fixed args -> args
    | C_SD.Variable args -> args
  in
   let fml_table = Hashtbl.create 17 in
   let _ = List.iter (fun x -> Hashtbl.replace fml_table x true) formals in
   let _ = M.msg_string M.Debug ("Formals : "^(Misc.strList formals)) in
   let f_conv lv =
     match lv with
	 E.Symbol x ->
	   let lv' =
	     if (Hashtbl.mem fml_table x) && not (O.getValueOfBool "incref")
	     then (E.make_symvar fname lv) else lv in
	     E.Lval(lv')
       | _ -> E.Lval lv
   in
   let proc_cand lv = 
     if not (C_SD.is_in_scope fname lv)
       (*(List.exists (fun fn -> C_SD.is_in_scope fn lv) relevant_scopes) *)
     then []
     else
       let lv' = lv_of_expr (E.deep_alpha_convert f_conv (E.Lval lv)) in
	 if lv' = lv then [lv]
	 else [lv;lv'] (* RJ:CF+pointer -- this isn't quite whats in the popl-paper.. or isit ? *)
   in
   let rv = List.flatten (List.map proc_cand alias_candidates) in
     M.msg_string M.Debug
       (Printf.sprintf "Aliases in scope of %s : %s"
	  (E.lvalToString target) (Misc.strList (List.map (E.lvalToString) rv)));
	rv


(*****************************************************************************)
(************************ McCarthy Code **************************************)
(*****************************************************************************)

let mc_var = E.mc_var get_lval_aliases
let to_mccarthy = E.to_mccarthy get_lval_aliases
let pred_to_mccarthy p = 
  if O.getValueOfBool "mccarthy" then P.to_mccarthy get_lval_aliases p else p
	  
(***************************************************************************)
(************************* LVALUE MAPS *************************************)
(***************************************************************************)
			 
module LvalMap = Map.Make(struct
  type t = int * E.lval
  let compare (i1,lv1) (i2,lv2) =
    if (compare i1 i2) = 0
    then
      compare (E.lvalToString lv1) (E.lvalToString lv2)
    else compare i1 i2
end)


type base_lvmap_t = E.expression LvalMap.t

type lvmap_record = {
  lvmap_curr  : base_lvmap_t;
  lvmap_hist  : base_lvmap_t list;
  lvmap_frames: int list;
  lvmap_locals: E.lval list list
}
type lvmap_t = lvmap_record ref

let lvmap_empty = {lvmap_curr = LvalMap.empty; lvmap_hist = []; lvmap_frames = [0] ; lvmap_locals = [] }

(* Function for convenient conversion from the older code.  Since we expect lvmap to be expandable, we add old_lvmap here *)
let lvmap_from_triple old_lvmap (cur,hist,frames) = {old_lvmap with lvmap_curr = cur; lvmap_hist = hist; lvmap_frames = frames }

let frame = ref 0
let get_new_frame () =
  frame := !frame + 1;
  !frame

let make_lval_tuple tid lval = 
   if C_SD.is_global lval then (0,lval) else (tid,lval)
(* RJ: I shall nevertheless call them lvmap_ref to rub in the fact that they are pointers *)		    
let update_map_a_b new_var_fun lvmap_ref tid lval =
  let cur = !lvmap_ref.lvmap_curr in
  let lval_tup = make_lval_tuple tid lval in
  let new_lval = new_var_fun lval in
    lvmap_ref := { !lvmap_ref with lvmap_curr = LvalMap.add lval_tup new_lval cur }
      
      
let update_map_a = update_map_a_b Absutil.getNewTmpVar
let update_map_clock_a = update_map_a_b Absutil.getNewTmpVar_clock


let in_lval_map_a lvmap_ref tid lval = 
  let cur = !lvmap_ref.lvmap_curr in
  let lval_tup = make_lval_tuple tid lval in
  LvalMap.mem lval_tup cur

let is_interpreted_lval lv = match lv with E.Symbol s -> E.is_interpreted s | _ -> false
  
let _lval_map_a lvmap_ref tid lval =
  if is_interpreted_lval lval then E.Lval lval else        
  let cur = !lvmap_ref.lvmap_curr in
  let frames = !lvmap_ref.lvmap_frames in
  let lval_tup = make_lval_tuple tid lval in
  try
      LvalMap.find lval_tup cur
  with Not_found -> 
      begin (* TBD:WHY? we need this to be deterministic -- WHY ? -- is it to do with "clock" ? *)
        let id =
	  if (O.getValueOfBool "lvframe") then
	    try (string_of_int (List.hd (!lvmap_ref.lvmap_frames))) with _ -> failwith "BAD FRAMES!"
	  else "0"
	in
        let (id,cand) = 
          if (O.getValueOfBool "timezero") then (id,[lval])
	  (* SKY: Why do we get aliases here always, even in incref? *)
	  (* SKY: Noone knows.  Ruling them out, ok. *)
	  (* It only makes the difference if the first occurence of the value (from the end) in the trace was due to the alias of something *)
	  else ((E.lvalToString lval)^id,[lval]) (*::(ac_get_lval_aliases ( not (Options.getValueOfBool "incref")) lval))*)
        in
        let lval_l = List.filter (fun lv -> not (in_lval_map_a lvmap_ref tid lv)) cand in
        let key_l = List.map (make_lval_tuple tid) lval_l in
        let data_l = List.map (fun lv -> E.Chlval (lv,id)) lval_l in
        let cur' = List.fold_right2 LvalMap.add key_l data_l cur in
	  lvmap_ref := {!lvmap_ref with lvmap_curr = cur'};
          E.Chlval (lval,id) 
      end

let lval_map_a lvmap_ref tid e =
  E.address_normalize 
    (E.deep_alpha_convert (_lval_map_a lvmap_ref tid) e)


let pred_lval_map_a lvmap_ref tid pred =
  let p' = (P.deep_alpha_convert (_lval_map_a lvmap_ref tid ) pred) in
  M.msg_string M.Debug "plvmap: now normalize";
  P.address_normalize p'
    
let new_lvmap_t tid = 
  let rv = ref lvmap_empty in
  let upd_f = if O.getValueOfBool "clock" then update_map_clock_a rv tid else update_map_a rv tid in
  let dummy_lookup lv = ignore (_lval_map_a rv tid lv) in
  C_SD.iterate_all_lvals dummy_lookup; (* adds all the lvals of the program to this table *)
    Absutil.clock_tick ();
    rv

let default_lvmap_ref = ref lvmap_empty

let lvmap_reset () = default_lvmap_ref := lvmap_empty
			  

let update_map = update_map_a default_lvmap_ref
let update_map_clock = update_map_clock_a default_lvmap_ref
			  
let lval_map = lval_map_a default_lvmap_ref 
			
let in_lval_map = in_lval_map_a default_lvmap_ref

let pred_lval_map = pred_lval_map_a default_lvmap_ref

let copy_lvmap () = ref (!default_lvmap_ref)

(* Creates mappings in destination map for the vars in source map that satisfy the check *)
let copy_vars_if check map_src map_dst =
  let folder lv _ m' =
    (* INV: lv in map_src *)
    try
      if check (snd lv) then
	LvalMap.add lv (LvalMap.find lv map_src) m'
      else m'
    with _ -> failwith "INV smashed in copy_vars_if"
  in
    LvalMap.fold folder map_src map_dst

(** copy_globals m_s m_d returns m' s.t. m'.lv = (lv \in (m_s \cap globals)) ? m_s.lv : m_d.lv *)
let copy_globals = copy_vars_if (C_SD.is_global)

(* This used to look like this:

let _lvm_cf_flag () = O.getValueOfBool "cf" || O.getValueOfBool "enablerecursion"

and it indeed needs to look like this if "enablerecursion" does not just treat recursive calls as skips (see the handler of RecursionException), but tries to allocate new values on stack for each recursive call.  Yes, keeping several lval_maps seems like a solution here.

However, before we implement this, this would consider _each_ call (except some mysterious "asynchronous calls" (is it -events?)) as a recursive one, which leads to false unsafes: parts of the trace get disconnected, and this could be crucial for proving unsatisfiability, see bug #1684.  So, the enablerecursion won't trigger this for now. *)
let _lvm_cf_flag () = O.getValueOfBool "cf"

(* Filter_o is a function that, if present, constrains the lvalues that will be restored at lvmap_pop.  The values that do not satisfy the filter, will get the new mapping between push and pop, and after pop the original mapping will be restored.  *)
let lvmap_push_a lvmap_ref filter_o =
  let curr = !lvmap_ref.lvmap_curr in
  match filter_o with
    | Some filter -> begin
      (* In the new map only filtered variables will get their new mapping *)
      let new_map = copy_vars_if filter curr LvalMap.empty in
      (* Save the "locals"--the variables, to which we assign new mappings *)
      let locals = LvalMap.fold (fun (_,lv) _ acc -> (if filter lv then [] else [lv])@acc) curr [] in
      M.msg_string M.Debug ("Lvmap filtered locals: " ^ (Misc.strList (List.map E.lvalToString locals)));
      (* Update lvmap storage *)
      lvmap_ref := {!lvmap_ref with lvmap_curr = new_map; lvmap_hist = curr::(!lvmap_ref.lvmap_hist); lvmap_frames = ((get_new_frame ())::(!lvmap_ref.lvmap_frames)); lvmap_locals = locals::(!lvmap_ref.lvmap_locals)}
    end
    | _ -> begin
      M.msg_string M.Debug ("SKY: Non-filtered lvmap_push!");
      let new_map = if (_lvm_cf_flag () )
	then copy_globals curr LvalMap.empty
	else curr
      in lvmap_ref := {!lvmap_ref with lvmap_curr = new_map; lvmap_hist = curr::(!lvmap_ref.lvmap_hist); lvmap_frames = ((get_new_frame ())::(!lvmap_ref.lvmap_frames))}
    end


let lvmap_pop_a lvmap_ref =
  match !lvmap_ref.lvmap_locals with
    | last_push_loc :: rest_loc -> begin
      (* We have some local lvals, therefore we should merge maps *)
      (* Copy all mappings except for those lvals menitioned in locals of last push *)
      (* TODO: maybe, just store the function instead of the list? *)
      let restored_filter lv = not (List.mem lv last_push_loc) in
      M.msg_string M.Debug ("Lvmap will be restored: " ^ (Misc.strList (List.map E.lvalToString last_push_loc)));
      match (!lvmap_ref.lvmap_curr, !lvmap_ref.lvmap_hist, !lvmap_ref.lvmap_frames)  with
	  (curr, top::rest, _ ::rest_fr) ->
	    let new_curr = copy_vars_if restored_filter curr top in
	      lvmap_ref := {!lvmap_ref with lvmap_curr = new_curr; lvmap_hist = rest; lvmap_frames = rest_fr; lvmap_locals = rest_loc}
	| _ -> failwith "Empty lvmap history in lvmap_pop"
    end
    | _ -> begin
      if (_lvm_cf_flag ())
      then
	match (!lvmap_ref.lvmap_curr, !lvmap_ref.lvmap_hist, !lvmap_ref.lvmap_frames)  with
	    (curr, top::rest, _ ::rest_fr) ->
	      let new_curr = copy_globals curr top in
		lvmap_ref := lvmap_from_triple !lvmap_ref (new_curr,rest,rest_fr)
	  | _ -> failwith "Empty lvmap history in lvmap_pop"
    end

let lvmap_push () = lvmap_push_a default_lvmap_ref None
let lvmap_push_filter filter () = lvmap_push_a default_lvmap_ref filter

let print_frames () =
  let frames = !default_lvmap_ref.lvmap_frames in
    M.msg_string M.Debug
      ("Frames: "^(Misc.strList (List.map (string_of_int) frames)))



let lvmap_pop () = lvmap_pop_a default_lvmap_ref
		     
let lvmap_copy lvmap_ref = ref (!lvmap_ref)

    
(* TBD:CF+POINTERS -- this whole business about lvmap push/pop rename needs to be looked at *)
let lvmap_rename_locals first_push_flag upd fname  =
  if (_lvm_cf_flag () || O.getValueOfBool "events")
  then
      begin
	M.msg_string M.Debug (Printf.sprintf "Tweak locals of %s.@." fname);
	let all_local_lvs = Stats.time "lookup locals" C_SD.lookup_local_lvals fname in
	let _ = M.msg_string M.Debug
		  ("lvmap_rename_locals: "^
		   (Misc.strList (List.map E.lvalToString all_local_lvs)))
	in
	  if first_push_flag then
	    begin
	      lvmap_push ();
	    List.iter upd all_local_lvs
	    end
	  else
	    begin
	      List.iter upd all_local_lvs;
	      lvmap_push ()
	    end
      end

(** here f: ( int * lval) -> expr -> unit *)    
let lvmap_iter f lvmap_ref =
  LvalMap.iter f (!default_lvmap_ref.lvmap_curr)

let print_lvmap () =
  let _pr (_,lv) e =
    M.msg_string M.Debug
      (Printf.sprintf "%s ----> %s" (E.lvalToString lv) (E.toString e))
  in
    lvmap_iter _pr default_lvmap_ref


    (* Find the stack state at the error to prepare the lvmap correctly with
     * initial renamings of local variables. *)
let make_lv_stack start_fname op_l =
  let stack_fold stack op =
    match Operation.get_info op with
	Operation.Ret _ -> (try List.tl stack with _ -> failwith "make_lv_stack: mismatched call/ret")
      | Operation.Normal -> stack 
      | Operation.Call (_, _, fname,_) when (C_SD.isAsyncCall op) -> stack
      | Operation.Call (_, _, fname,_) -> fname :: stack in
  let stack = List.fold_left stack_fold [start_fname] op_l in
  let update_map = 
    if (O.getValueOfBool "clock") then update_map_clock 0 else update_map 0 in
    lvmap_reset ();
    List.fold_right (fun fn () -> lvmap_rename_locals false update_map fn) stack () 

      
(* only functions used are:
   ...
1. lvmap_push
   2. lvmap_pop
   3. lvmap_rename_locals
   4. update_map/_clock
   5. (pred_)lval_map
   6. lvmap_empty
   7. lvmap_copy
   8. lvmap_rename_locals -- NOT REALLY used -- should be suppressed.
   9. lvmap_iter
   10. make_lv_stack
*)
    
(********************************* END OF LVMAP MODULE ***********************)
(*****************************************************************************)
  







(* Misc. predicate helpers for block_wp/sp -- see popl04 paper *)

let ite (cond,t1,t2) = 
  P.disjoinL 
    [ Stats.time "conjoinL" P.conjoinL [ cond;t1] ; 
      Stats.time "conjoinL" P.conjoinL [P.Not cond; t2]]
    

       
(* RJ: THERE CAN BE BUGS WITH THE LVAL_MAP INTERFACE !! PLEASE CHECK IF YOU GET WIERD ANSWERS THAT THE CONSTRAINTS GENERATED 
   ARE GOOD ! -- better still PROVE this interface correct. PROVE ???!!! HA HA.*)
       
    
(* creates list of (alias,valu) pairs, for each alias, that is a must-alias for target.
	Aliases are fetched via AliasAnalyzer module *)
let must_close (target,valu) =
  let must_aliases = AA.get_all_must_aliases target in
  let _ = M.msg_string M.Debug (Printf.sprintf "Must aliases of %s are %s" 
    (E.lvalToString target) 
    (Misc.strList (List.map E.lvalToString must_aliases)));
  in 
    List.map (fun lv -> (lv,valu)) (target::must_aliases)

let wp_get_lval_aliases =
  if (O.getValueOfBool "talias") then trace_lv_table_get_lval_aliases 
  (* occurring in trace *)
  else get_lval_aliases (* in the whole program *)

(* Incremental lvalue closure (-iclos) *)

let closurer = ref C_SD.empty_closurer

let initialize_closurer () = closurer := C_SD.empty_closurer

let lval_to_closurer e = closurer := (C_SD.add_expr_to_closurer !closurer (E.Lval e))

let query_closurer e = C_SD.query_expr_closure !closurer e

let debug_print_closurer () = M.msg_printer M.Debug C_SD.print_closurer !closurer


let wp_closure_fun =
  if O.getValueOfBool "use-iclos" then query_closurer
  else
  if (O.getValueOfBool "fldunroll") then (* always true *)
    C_SD.expression_closure_stamp true (* (is_relevant_field_trace) *)
  else C_SD.expression_closure

let aliasesOf targets_fname (target,value) =
  if (O.getValueOfString "alias" = "") then [target]
  else
    (* ac_get_lval_aliases is called through wp_get_lval_aliases *)
    (* Cool, but it actually just gathers may-aliases and compacts redundant dereferences.  So we have candidates. *)
    let _ = if (O.getValueOfBool "devdebug" ) && (O.getValueOfBool "cref") && (O.getValueOfString "alias" <> "")  then M.msg_string M.Normal ("Getting aliases for block...") else () in
    let lval_aliases = wp_get_lval_aliases target in
    let _ = if (O.getValueOfBool "devdebug" ) && (O.getValueOfBool "cref") && (O.getValueOfString "alias" <> "") then M.msg_string M.Normal ("Aliases done.") else () in
    let candidates = Stats.time "push_deref in aliasesOf" (List.map E.push_deref) lval_aliases in
    (*let _ = M.msg_string M.Debug ("Alias pushed candidates: "^(Misc.strList (List.map E.lvalToString candidates))) in*)
    (* only lvals in the same "scope"  ?? *)
    if (O.getValueOfBool "cf") then
      Misc.cons_if target (aliases_in_scope targets_fname target candidates)
    else candidates

(* constraints for a single assignment *)
let asgn_cons lval_map (target,valu) lv_triple_list =
  (* In "usual" (non-incremental) closure mode, all the closures are essentially the same, so we calculate it once *)
  let common_valu_closure_vals = if O.getValueOfBool "use-iclos" then [] else
    let valu_closure () =
      let target_closure = Stats.time "wp_closure_fun" wp_closure_fun (E.Lval target) in
      List.map
      (fun x ->
	let vx = (E.substituteExpression [(E.Lval target, valu)] x)
	in
	try E.Lval (E.push_deref (lv_of_expr vx)) with _ -> vx)
      target_closure
    in
    let vc = Stats.time "valu_closure" valu_closure () in
    (* Update aliaser with target and its closure.  We do not traverse the aliases (only the target) as the lvals in the aliases are already in the aliaser: they came from it *)
    (* TODO: move it to the proper place after the closure implementation *)
    (* NOTE: we apply it before lval_map, as we need the actual Lvals, not ChLvals.  However, there _may_ be some chlvals here that come from get_new_unknown in Absutil. So, we replace them beforehand. *)
    let lvalues_here = List.map (fun e -> E.lvals_of_expression (E.replaceChlvals e)) vc in
    let _ = List.iter (fun lvs -> List.iter lval_to_aliaser lvs) lvalues_here in
    List.map lval_map vc
  in
  (* SKY: Ok, here valu_closure_vals is a list of values the corresponding closures of the target (in the list (wp_closure_fun target)) get at the assignment *)
  let target_addr = E.addressOf (lval_map (E.Lval target)) in
  (* RJ: this order is VERY important. First substitute, THEN take addressOf *)
  (* SKY: target_addr is an address of target after assignment *)
  let process_alias (lv,lv_closure,new_vals) = 
    let valu_closure_vals =
    (* The closed new values were already computed, if in "usual" mode *)
    if not (O.getValueOfBool "use-iclos") then common_valu_closure_vals else begin
      (* With incremental closure, the closures of target and of its alias are no longer structurally equivalent wrt variable substitution.  Each element of the closure of the alias lv should get the value, and we should take the closure of lv as a base, not the closure of the target! *)
      let valu_closure_vals =
	List.map
	(fun x ->
	  let vx = (E.substituteExpression [(E.Lval lv, valu)] x)
	  in
	  try E.Lval (E.push_deref (lv_of_expr vx)) with _ -> vx)
	lv_closure
      in
      M.msg_string M.Debug (Printf.sprintf "real valu clos for alias %s=%s : %s" (E.lvalToString lv) (E.toString valu) (Misc.strList (List.map E.toString valu_closure_vals)));
      (* Update aliaser with target and its closure.  We do not traverse the aliases (only the target) as the lvals in the aliases are already in the aliaser: they came from it *)
      (* TODO: move it to the proper place after the closure implementation *)
      (* NOTE: we apply it before lval_map, as we need the actual Lvals, not ChLvals.  However, there _may_ be some chlvals here that come from get_new_unknown in Absutil. So, we replace them beforehand. *)
      let lvalues_here = List.map (fun e -> E.lvals_of_expression (E.replaceChlvals e)) valu_closure_vals in
      let _ = List.iter (fun lvs -> List.iter lval_to_aliaser lvs) lvalues_here in
      (* update closurer in the same manner *)
      let _ = List.iter (fun lvs -> List.iter lval_to_closurer lvs) lvalues_here in
      List.map lval_map valu_closure_vals
      (* note: all of the above is done _AFTER_ the lval_map itself has been updated *)
    end in

    if lv = target then
      try P.min_equate new_vals valu_closure_vals with excn -> failwith ("eq1: "^(Printexc.to_string excn))
    else
      begin
        let old_vals = List.map lval_map lv_closure in
        let eq_new = 
          try P.min_equate new_vals valu_closure_vals with excn -> failwith ("eq2: "^(Printexc.to_string excn))
        in
        let eq_old = try P.min_equate new_vals old_vals with excn ->
          failwith ("eq3: "^(Printexc.to_string excn))
        in
        let lv_addr = E.addressOf (lval_map (E.Lval lv)) in
 	(* condition of equivalence of target's addres after assignemnt and alias' address (lv_addr) *)
        let cond =
          let (a,a') = simplify_address_equality (target_addr,lv_addr) in
          P.Atom (E.Binary(E.Eq,a,a')) 	    
        in
	  (* SKY: this function makes conditions in form ( (cond AND eq_new) OR (!cond AND eq_old) ). *)
	  (* SKY: That's the same as  ( (cond IMPLIES eq_new) AND (!cond IMPLIES eq_old) ), provided eq_new and eq_hold are unsat together *)
	  (* SKY: The meaning of this is such: ``If at the time of assignment the may-alias in subject is an alias indeed, then the value of that alias will be updated at the point of assignment.  And if this was not true, then the value of the aliased memory remains the same.'' *)
	  (* SKY:  You will find such phrases in debug trace.  That's how may-aliases are turned into real aliases.  And it's cool *)
          ite (cond,eq_new,eq_old)
      end
  in
      List.map process_alias lv_triple_list

(* Debug print function for printing closures of aliases.  Turned on only in incremental closure mode, as in usual mode they do not differ *)
let print_triples =
  if not (O.getValueOfBool "use-iclos") then ignore else
  let p_triple_list tl =
    let p_triple (lv,ecl,mapped_ecl) =
      M.msg_string M.Debug (Printf.sprintf "lv=%s; clos=%s; eclos=%s" (E.lvalToString lv) (Misc.strList (List.map E.toString ecl)) (Misc.strList (List.map E.toString mapped_ecl)));
    in
    M.msg_string M.Debug ("Next triple.");
    List.iter p_triple tl;
  in
  M.msg_string M.Debug ("Now printing triple lists");
  List.iter p_triple_list

let doAssign_list lval_map update_map lvmap_operation_option targets_fname (target_valu_list) = 
  let _ = Absutil.clock_tick () in
  (*let num = List.length target_valu_list in*)
  (* for every target add its __must__ aliases to the list, thus pretending that value is assigned to them also *)
	(* It adds must aliases based on the database created at the beginning of the program.  So this procedure should be fast *)
  let target_valu_list = if (O.getValueOfBool "nomusts") then target_valu_list else
    List.map 
    (fun (target,valu) -> (E.push_deref target, valu)) 
    (List.flatten (List.map (Stats.time "must_close" must_close) target_valu_list)) 
  in  
  (* SKY: for every lv in t_v_list (a list like the one computed above)...*)
  (* SKY: Form a list of (alias of that lval, expression_closure of alias, exp closure with lvals mapped to Chlvals ) *)
  (* SKY: expression_closure is a list of lvalues we can access if we are given the expression, constrained with "-cldepth" *)
  let lv_triple_list_list = 
    let make_triple lv = 
      let eclos = Stats.time "wp_closure_fun" wp_closure_fun (E.Lval lv) in
      (lv,eclos,List.map (Stats.time "lval_map" lval_map) eclos)
      (* RJ: #3 is names of lvalues _AFTER_ assignment *) in
    List.map (List.map make_triple) (List.map (Stats.time "aliasesOf" (aliasesOf targets_fname)) target_valu_list) in

  (* Print triples (useful for incremental alias debug) *)
  print_triples lv_triple_list_list;

  (* SKY: Relevant lvals are all lvalues that will be assigned to in the assignment in subject *)
  let relevant_lvals =
      List.flatten
	(List.map (fun l -> List.flatten (List.map Misc.snd3 l)) lv_triple_list_list) in
  (* SKY: the following operator deprecates Chlvals that were mapping the lvals in this assignment.  It gives them new temporary variables, thus itilizing the SSA-for-trace concept *)
  (* In incremental closure mode, repeated lvalues may appear here.  We should not update mapping for them twice! *)
  let relevant_lvals = Misc.compact relevant_lvals in
  M.msg_string M.Debug ("relevant_lvals (compacted): "^(String.concat ";" (List.map E.toString relevant_lvals)));
  let _ = Stats.time "updating map" (List.iter (fun x -> update_map (lv_of_expr x))) relevant_lvals in
  let _ = match lvmap_operation_option with None -> () | Some (f) -> f () in    
  (* after this we are using the updated lval_map *) 
  (* asgn_cons is `assignment constraints' *)
Stats.push_profile "convert asgns to preds" ;
  let final_list = List.flatten (List.map2 (asgn_cons (Stats.time "lval_map" lval_map)) target_valu_list lv_triple_list_list) in
Stats.pop_profile "convert asgns to preds";
  M.msg_string M.Debug "Constraints of the block (without chlvals) are: ";
  let noprimed_list = List.map (P.deep_transform (function P.Atom(exp) -> (P.Atom(E.replaceChlvals exp)) | x -> x)) final_list in
  let _ = List.iter (fun pr -> M.msg_string M.Debug (P.toString pr)) noprimed_list in
  final_list


let doAssign_list_mccarthy lval_map update_map lvmap_op_opt fname t_v_list = 
  let _ = Absutil.clock_tick () in 
  let d (lv,_) = match mc_var lv with None -> (E.Lval lv, lval_map (E.Lval lv))
                                    | Some m -> (m, lval_map m) in
  let t_tpost_list = List.map d t_v_list in
  let _ = List.iter (fun (e,_) -> update_map (lv_of_expr e)) t_tpost_list in
  let _ = match lvmap_op_opt with None -> () | Some (f) -> f () in    
  (* after this we are using the updated lval_map *) 
  let make_cons (t,tpost) (lv,e) = 
    let y = 
      match mc_var lv with None -> to_mccarthy e 
      | Some _ -> E.Store(t, to_mccarthy(E.mc_addressOf (E.Lval lv)), (to_mccarthy e))
    in P.Atom (E.Binary(E.Eq,tpost,lval_map y))
  in
  List.map2 make_cons t_tpost_list t_v_list
  
 
  
(* Notes about block_wp:
 * implicit INV:possibleAliases have the same closure set as the target 
 * FALSE!!! The aliases may have a "smaller" closure, due to heuristics in
 * closure that prevent unrolling a field if it already is in the prefix!!
 * now updating the map so that all the things that might possibly have 
 * changed have new values for _BEFORE_ the operation
 *
 * lvmap_operation_option can be:
 * lvmap_push -- if you are processing a return,
 * lvmap_pop -- if these are assignments of a funcall,
 * (fun () -> ()) otherwise 
 * push indicates that this assignment is saving a return value.
 * We'll save the renaming AFTER changing names for aliases of the target but
 * BEFORE renaming local variables of the function we return from, to make
 * sure that these local variable renamings go out of scope outside this
 * function call. TBD:SOUNDNESS 
 * copy_back_asgns is [] if not "cf"  
 * TBD:ARRAYS
 * actually, you have to make a pass over the code and see what cells may have been modified.
 * if this includes things like arrays, this DOES NOT WORK...
 * unless, you repace the arrays wholesale... but this is not sound, as you may create
 * sharing where there is none... sigh. Ideally, you have to know which lvals have been "hit"
 * inside the function call, and copyback all those lvals at the return site. 
 * For now, we assume we only have to copy back the lvals passed in as args and their 
 * transitive derefs. One has to use trace_lv_table -- to copy back all the fellows 
 * that may be looked at in the future. One has to think about this...*)


(* TBD:SOUNDNESS -- in cf mode, only use lvals IN SCOPE! not all lvals *)
(* fname = name of function to which e belongs *)         
let block_wp lvmap_operation_option fname tid e_list pred =
  let _ = Stats.time "print_fra" print_frames () in
  let doAssign_list = 
    let update_map = 
      (*if (O.getValueOfBool "clock") then update_map_clock tid else*) Stats.time "update_map" update_map tid
    in 
    let lval_map =  lval_map tid in (* i.e. we use the default lvmap *)
    (*if (O.getValueOfBool "mccarthy") then *)
      (*doAssign_list_mccarthy lval_map update_map lvmap_operation_option*)
    (*else*)
      doAssign_list lval_map update_map lvmap_operation_option 
  in 
  let doReturn retexp =
    (*M.msg_string M.Debug "WP of return";*)
    let (return_asgn_option,copy_back_asgns) = Absutil.asgn_of_return fname  in
        let all_ret_asgn =
      match return_asgn_option with
	  Some (return_target) ->
	    (*M.msg_string M.Debug*)
		(*(Printf.sprintf "Return: %s <- %s"*)
		   (*(E.lvalToString return_target) (E.toString retexp));*)
	    (return_target,retexp)::copy_back_asgns
	| None -> copy_back_asgns
    in
      Stats.time "doAssign_list" (doAssign_list  (!Absutil.retCallerName)) all_ret_asgn
  in
  let new_cons =
    match e_list with
	[C_SD.Command.Expr (E.Assignment(E.Assign, target, valu))] ->
	  (* calls doAssign_list (lval_map tid) (update_map) lvmap_operation_option fname [(t,v)] *)
	  Stats.time "doAssign_list" (doAssign_list fname) [(target,valu)] (* target "belongs" to fname *)
	  (* INV: in this case the lvmap_oper_option = None *)
      | [C_SD.Command.Return exp] -> doReturn exp
	  (* INV: in this case the lvmap_oper_option = Some(lvmap_push) *)
      | _ ->
	  (* INV: in this case the lvmap_oper_option = Some(lvmap_pop) *)
	  begin
	  try
	    let extract_tv e =
	      match e with
		  C_SD.Command.Expr (E.Assignment(E.Assign, target, valu))
		    -> (target,valu)
		| _ -> failwith "bad call to block_wp --extract_tv"
	    in
	    let t_v_list = List.map extract_tv e_list in
	      Stats.time "doAssign_list" (doAssign_list fname) t_v_list
	  with ex -> failwith("bad call to block_wp: "^(Printexc.to_string ex))
	  end
  in
  (*suppress_aliases := false ;*)
  debug_print_aliaser ();
  debug_print_closurer ();
  Stats.time "conjoinL" P.conjoinL (new_cons@[pred]) 


let make_sub mcflag lv (e,v) =
  if not mcflag then (e,v) 
  else 
    match mc_var lv with 
      None -> (e,(to_mccarthy v)) 
    | Some m -> (m,E.Store(m,to_mccarthy (E.mc_addressOf (E.Lval lv)), (to_mccarthy v)))
  
let wp_one eflag pred t v mustAliases aliasSet =
  let mks = make_sub (O.getValueOfBool "mccarthy") t in
	(* make list of constraints that describe the alias set supplied *)
  let aliasExps =
    List.map
      (function (b,e) -> let tAddr = E.addressOf (E.Lval t) and eAddr = E.addressOf e in 
       let (a,a') = simplify_address_equality (tAddr,eAddr) in
       if b then P.Atom (E.Binary(E.Eq,a ,a')) else P.Atom (E.Binary(E.Ne,a ,a'))) 
    aliasSet in
  (* make the list of (true,x) for each x that has an equal address with target t *)
  let ac_aliases = (true, E.Lval t) ::(List.filter fst aliasSet) in
  (* whether value is a boolean expression *)
  let boolexp = match v with E.Binary (op, _, _) when E.isRelOp op -> true | _ -> false in
  (* new value for v: if we're assigning "boolean", then we need a tmp var to stope boolean result, and the final predicate would be mocked *)
  let vsub = if eflag || boolexp then Absutil.getNewTmpVar t else v in
  (* put v instead of each thing that has an equal address with v, according to the ac_aliases and must_aliases computed above *)
  let p1 =
    let subs = mustAliases @ (List.map snd ac_aliases) in
    let substSet = List.map (fun e -> mks (e,vsub)) subs in 
    P.substitute substSet pred
  in
  (* If no bool-to-int casting happens, is P.True.  Otherwise, is a starightforward disjunction ( (target -> value = 1) or (!target -> value = 0)) *)
  let p2 = 
    if boolexp then 
      let [v0;v1] = List.map (fun n -> E.Constant (Constant.Int n)) [0;1] in 
      P.disjoinL [P.conjoinL [P.Atom (to_mccarthy v); (P.expr_equate vsub v1)];
                  P.conjoinL [P.Atom (E.negateRel (to_mccarthy v)); (P.expr_equate vsub v0)]]
    else if eflag then P.expr_equate vsub v
    else P.True
  in
  (* result is a conjunction of
   p1 - predicate with substitutions to value wrt aliasSet
   p2 - bool-to-int casting
   aliasExp - current state of aliased variables
  *)
    (P.conjoinL (p1::p2:: aliasExps)) 
  
  
 (* Weakest Preconditions *)   
 let rec wp explicit_flag sat_flag stmt qpred =
   let qpred = pred_to_mccarthy qpred in
   match stmt with
     C_SD.Command.Expr e->
     begin
       let (quantifier_prefix, pred) = strip_quantifier_prefix qpred in
       match e with
         E.Assignment(E.Assign, target, valu) -> (* operation target = value *)
           let aliasSets = 
             if O.getValueOfBool "mccarthy" then [[]] else
             let possibleAliases = Stats.time "getPossibleAliases" (Absutil.getPossibleAliases target) pred in
             Misc.powerSet (List.filter (function al -> al <> (E.Lval target)) possibleAliases) in
		       let _ = M.msg_string M.Debug "Possibles done, making must" in
           let mustAliases = Stats.time "getMustAliases" (Absutil.getMustAliases target) pred in
		       let _ = M.msg_string M.Debug "musts done, calling wp_one" in
           let actual_pre = (P.disjoinL (List.map (wp_one explicit_flag pred target valu mustAliases) aliasSets)) in
           let _ = M.msg_string M.Debug (Printf.sprintf "wp result is %s" (P.toString actual_pre)) in
           let ap = if sat_flag then sat_filter (actual_pre) P.True else actual_pre in
           reattach_quantifier_prefix quantifier_prefix  ap
       | E.Unary(E.Assume, e) -> reattach_quantifier_prefix quantifier_prefix (P.conjoinL [(P.Atom e); pred])
       | _ -> failwith "wp: not implemented"
     end
   | C_SD.Command.Return exp ->
       M.msg_string M.Debug "WP of return";
       if ((!retTarget) = C_SD.junkRet) then qpred 
       else wp explicit_flag sat_flag (C_SD.Command.Expr(E.Assignment(E.Assign, !retTarget, exp))) qpred 


 (* OLD Weakest Preconditions, both explicit and implicit    
 let rec wp explicit_flag sat_flag stmt quantified_pred =
   match stmt with
       C_SD.Command.Expr e->
	 begin
	   let (quantifier_prefix, pred) = strip_quantifier_prefix quantified_pred in
	   match e with
	       E.Assignment(E.Assign, target, valu) -> (* operation target = value *)
		 let assigned_value = 
		   if explicit_flag then 
		     begin
		       let newvar = (Absutil.getNewTmpVar target) in
			 newvar
		     end
		   else (* IMPLICIT *)
		     begin
		       valu
		     end
		 in
		 (* is the value assigned a boolean expression? *)
		 let is_boolexp = 
		   match valu with E.Binary (op, _, _) when E.isRelOp op -> true | _ -> false
		 in
		 let _print_info_flag = (!wpCapRegion = P.True) in
		 let possibleAliases = Stats.time "getPossibleAliases" (Absutil.getPossibleAliases target) pred in
		 let aliasSets = Misc.powerSet 
		     (List.filter (function al -> al <> (E.Lval target)) possibleAliases)
		 in
		 let wp_one aliasSet =
		   (* In the implicit version, we would construct substitutions here.
		      But here we should not substitute: the aliasExps will take care of that.
		    *)
		   begin
		     let aliasExps = List.map 
			 (function (b,e) ->
			   let targetAddr = E.addressOf (E.Lval target)
			   and eAddr = E.addressOf e in 
			   let (a,a') = simplify_address_equality (targetAddr,eAddr) in
			   match b with 
			     true -> P.Atom (E.Binary(E.Eq,a ,a'))
			   | false -> P.Atom (E.Binary(E.Ne,a ,a'))) 
			 aliasSet 
		     in
		     (* without pointers, the following line would be the entire
			weakest precondition computation, with an empty substSet *)
		     if is_boolexp then 
		       begin
			 if explicit_flag then 
			   begin
			     let substSet = 
			       List.map (function (_,e) -> (e,assigned_value))
				 (List.filter (function (b,e) -> b=true) aliasSet) in
			     
			     let _s1 =
			       P.substitute ( (E.Lval target, 
						       assigned_value )
						      :: substSet) pred in
			     M.msg_string M.Normal (P.toString _s1) ;
			     (P.conjoinL (
			      (P.Or
				 [(P.conjoinL [ P.Atom valu; 
							P.Atom (E.Binary 
									  (E.Eq, assigned_value, 
									   E.Constant (Constant.Int 1)))]
				  );
				  (P.conjoinL [ P.Atom (E.negateRel valu); 
							P.Atom (E.Binary 
									  (E.Eq, assigned_value, 
									   E.Constant (Constant.Int 0)))])
				]
			      ) :: (_s1 :: aliasExps)))
			   end
			 else
			   begin
			     let substSet1 = 
			       List.map (function (_,e) -> (e, E.Constant (Constant.Int 1)))
				 (List.filter fst aliasSet) in
			     let substSet0 = 
			       List.map (function (_,e) -> (e,E.Constant (Constant.Int 0)))
				 (List.filter fst aliasSet) in
			     
			     let _s1 =
			       P.substitute ( (E.Lval target, 
						       E.Constant (Constant.Int 1)) 
						      :: substSet1) pred in
			     
			     let _s0 =
			       P.substitute ( (E.Lval target, 
						       E.Constant (Constant.Int 0)) 
						      :: substSet0) pred in
			     
			     (P.conjoinL ( 
			      (P.disjoinL
				 [
				  P.conjoinL [P.Atom valu ; _s1 ] ;
				  P.conjoinL [P.Atom (E.negateRel valu); _s0 ]
				] )
			      :: aliasExps))
			   end
			     
		       end
		     else 
		       let substSet = 
			 List.map (function (_,e) -> (e,assigned_value))
			   (List.filter (function (b,e) -> b=true) aliasSet) in
		       
		       let _s1 = 
			 P.substitute ((E.Lval target, assigned_value) :: substSet) pred in
		     		       (* conjoin the weakest precondition result to the expressions
			  representing this particular aliasing scenario *)
		       (P.conjoinL (List.append (_s1:: aliasExps) 
					      (if explicit_flag then
						[P.Atom (E.Binary
								   (E.Eq,
								    assigned_value, valu))]
					      else [])
					   ))
		   end
		 in
		 let actual_pre = (P.disjoinL (List.map wp_one aliasSets)) in
		 reattach_quantifier_prefix quantifier_prefix 
		   (if sat_flag then 
		     sat_filter (actual_pre) P.True
		   else 
		     actual_pre)
		   
	   |        E.Unary(E.Assume, e) -> reattach_quantifier_prefix quantifier_prefix
		 (P.conjoinL [(P.Atom e); pred])
	   |        _ -> (Message.msg_string Message.Normal (E.toString e ) ); failwith "wp: this is not implemented"
	 end
   | C_SD.Command.Return exp ->
       M.msg_string M.Debug "WP of return";
       if ((!retTarget) <> C_SD.junkRet) then
	 begin
	   M.msg_printer M.Debug C_SD.Command.print 
	     { C_SD.Command.code = 
	       C_SD.Command.Block [(C_SD.Command.Expr(E.Assignment(E.Assign, !retTarget, exp)))];
	       C_SD.Command.read = []; (* printing ignores .read and .written *)
	       C_SD.Command.written = []}; (* ditto *)
	   (wp 
	      explicit_flag sat_flag 
	      (C_SD.Command.Expr(E.Assignment(E.Assign, !retTarget, exp))) 
	      quantified_pred)
	 end
       else quantified_pred
	   (* Jhala: wp RETURN, here. Devilish hack. Sorry ! *)
*)	

   
   (*
     Strongest postcondition
   *)
	   (* Jhala: Need to fold in rupaks changes to SP *)
   (*
     Strongest postcondition
     We do not need an explicit flag because sp is always explicit
   *)
   let rec sp sat_flag stmt pred =
     match stmt with
	 C_SD.Command.Expr e ->
	   begin
	     match e with  
		 E.Assignment(E.Assign, target, value) ->
		   begin 
		     let targetExp = E.Lval target 
		     in
		     let possibleAliases = Absutil.getPossibleAliases target pred in
		       (* compute all possible alias scenarios.  we can remove the case when the target
			  appears in the pred, because these are always aliased (and there is no point of
			  doing the usual wp substitution in this case). *)
		     let aliasSets = Misc.powerSet (List.filter (function al -> al <> (E.Lval target)) possibleAliases) in
		     let newvar = (Absutil.getNewTmpVar target) in
		     let sp_one aliasSet =
		       (* create the pairs for substituting expressions in pred for
			  value, when pred and target are aliased *)
		       let substSet = List.map (function (_,e) -> (e,newvar))
					(List.filter (function (b,e) -> b=true) aliasSet) in
			 (* create expressions representing the alias pairs *)
		       let aliasExps = List.map 
					 (function (b,e) ->
					    let targetAddr = E.addressOf (E.Lval target)
					    and eAddr = E.addressOf e in
                                            let (targetAddr,eAddr) =
                                              simplify_address_equality
                                              (targetAddr,eAddr)
                                            in
					      match b with 
						  true -> P.Atom (E.Binary(E.Eq,targetAddr,eAddr))
						| false -> P.Atom (E.Binary(E.Ne,targetAddr,eAddr))) aliasSet 
		       in
			 (* compute the strongest postcondition *)
			 (* without pointers, the following line would be the entire
			    strongest postcondition computation, with an empty substSet *)
		       let s1 = P.substitute ((targetExp, newvar) :: substSet) pred in
		       let s2 = E.substituteExpression [(targetExp, newvar)] value in
			 P.conjoinL (List.append 
					       [s1; P.Atom (E.Binary (E.Eq, targetExp, s2))] 
					       aliasExps)
		     in
		     let actual_sp =  P.disjoinL (List.map sp_one aliasSets) in
		     if sat_flag then
		       sat_filter (actual_sp) P.True else actual_sp
		   end
	       |        E.Unary (E.Assume, e) -> (* P.normalize *)
		     (P.And [(P.Atom e); pred])
	       |        _ -> failwith "sp: not implemented"
	   end
       |  C_SD.Command.Return exp ->
	   if ((!retTarget) <> C_SD.junkRet) then
	     begin
	       sp sat_flag (C_SD.Command.Expr (E.Assignment (E.Assign, !retTarget, exp))) pred
	     end
	   else
	     pred

(************************************************************************************************************************)
(***********************************  block_sp, for the foci_model_checker **********************************************)
(************************************************************************************************************************)


(* Calling this changes the lvmap *)
(* the 
* infirst param is a function that should return an overapproximaiton of the aliases of an lv, w.r.t. whatever
   is of interest -- you could return, for ex. either ALL aliases of lv, or those aliases that occur in the future,
   or those that occur in a predicate etc etc.
*)

let rec ac_block_sp closure_fun aliases_of_fun lvmap_ref retTarget_o e pred =
  let update_map = if (O.getValueOfBool "clock") then update_map_clock_a lvmap_ref 0 else update_map_a lvmap_ref 0
  in (* RJ: this used to be: let update_map = update_map_a lvmap_ref 0 in *)
  let lval_map =  lval_map_a lvmap_ref 0 in
    match e with
	 C_SD.Command.Expr (E.Assignment(E.Assign, target, valu)) ->
	   begin
	     let _ = Absutil.clock_tick () in
	     (* i'm doing this again for no good reason *)
	     let target_closure = closure_fun (E.Lval target) in
	     let valu_closure = 
	       List.map 
		 (
		   fun x -> 
		     let vx = 
		       (E.substituteExpression [(E.Lval target, valu)] x)
		     in 
		       try E.Lval (E.push_deref (lv_of_expr vx))
		       with _ -> vx
		 )
		 target_closure
	     in
	       (* shouldnt really need to do this latter operation as its subsumed by the one later but ... *)

	     let possibleAliases = List.map E.push_deref (aliases_of_fun target) in
	       (* 1. store the old NAMES *)
	     let lv_triple_list = 
	       let make_triple lv = 
		 let eclos = closure_fun (E.Lval lv) in
		 let lv_addr = lval_map (E.addressOf (E.Lval lv)) in
		   ((lv,lv_addr),eclos,List.map lval_map eclos) (* these are names BEFORE the operation *)
	       in
		 List.map make_triple possibleAliases
	     in
	       (* 2. store the old VALUES *)
	     let valu_closure_vals = List.map lval_map valu_closure in 
	     let target' = lval_map (E.Lval target) in
	     let target_addr = E.addressOf target' in
	       (* this is the OLD name of target -- i.e. BEFORE the operation *)
	       (* implicit INVARIANT: the possibleAliases have the same closure set as the target *)

	       (* 3. now updating the map for things that may have changed *)
	     let relevant_lvals = List.flatten (List.map Misc.snd3 lv_triple_list) in
	     let _ = List.iter (fun x -> update_map (lv_of_expr x)) relevant_lvals in
	       
	     (* after this we are using the updated lval_map *)
	     (* implicit INVARIANT: the closures come out in the right order ... *)

	     let process_alias ((lv,lv_addr),lv_closure,old_vals) = 
	       let new_vals = List.map lval_map lv_closure in
               if lv = target then
		   try P.equate new_vals valu_closure_vals with _ -> failwith "pa 1" 
               else
		 begin
		   let eq_new = try P.equate new_vals valu_closure_vals with _ -> failwith "pa 2" in
		   let eq_old = try P.equate new_vals old_vals with _ -> failwith "pa 3" in
		   let lv_addr = E.addressOf  (lval_map (E.Lval lv)) in
		   let cond =
                     let target_addr,lv_addr = simplify_address_equality (target_addr,lv_addr) in
		     P.Atom (E.Binary(E.Eq,target_addr,lv_addr))
		   in
		     ite (cond,eq_new,eq_old)
		 end
	     in
	     let new_cons = 
	       List.map process_alias lv_triple_list 
	     in 
(*
	       M.msg_string M.Debug
		 ("bsp: "^(P.toString (P.conjoinL new_cons)));
*)
	       P.conjoinL (new_cons@[pred])
	   end
       | C_SD.Command.Return exp ->
          begin
	   M.msg_string M.Debug "bSP of return";
	   match retTarget_o with
	       None -> pred
	     | Some retTarget ->
		 begin
		   M.msg_printer 
		     M.Debug C_SD.Command.print 
		     { C_SD.Command.code = 
			 C_SD.Command.Block 
			   [(C_SD.Command.Expr(E.Assignment
						 (E.Assign, retTarget, exp)))];
		       C_SD.Command.read = []; (* printing ignores .read and .written *)
		       C_SD.Command.written = []}; 
		   (ac_block_sp closure_fun aliases_of_fun lvmap_ref None 
		      (C_SD.Command.Expr
			 (E.Assignment(E.Assign, retTarget, exp))) 
		      pred)
		 end
        end	
     | _ -> failwith("bad call to block_sp!")


 let block_sp = ac_block_sp (C_SD.expression_closure) get_lval_aliases 
		  
		  
(* a new implementation of strongest postcondition that uses the above -- the regular one
   is flaky in the presence of pointers *)
(* for now, we only do it for blocks of assignments *)
	 (* asgn list : (lval * expression) list *)

 let sp_via_block closure_fun (relevant_future_lvals) pred asgn_list  =
   (* 1. make new lvmap
      2. p = lval_map lvmap pred
      3. p' = block_sp my_alias_fun lvmap pred asgn_list
      4. p'' = P.deep_transform_top f p'
      where
      f (Chlv_exp) =
      if is_latest chlv_exp then Exp.Lval (removechlv chlvexp)
      else chlv_exp
      is_latest chlv_exp = ((lval_map lvmap (removeChlval chlvexp)) = chlvexp)
   *)
 let _ = 
                begin
                let asgn_string = Misc.strList
                        (List.map
			  (fun (lv,e) -> Printf.sprintf "%s := %s " (E.lvalToString lv) (E.toString e))
			  asgn_list
		       )
                in
                M.msg_string M.Debug ("sp_via_block called with: ") ;
                M.msg_printer M.Debug  P.print pred ;
                M.msg_string M.Debug ("asgns: "^asgn_string)
                end
 in
   let relevant_future_lvals = List.map E.push_deref
   relevant_future_lvals 
 in 
   (* 1. make a new lvmap *)
   let sp_lvmap_ref = ref lvmap_empty in
     (* now, lets fill the lvmap with the relevant_future_lvals *)
   (* 2. compute p *)
   let p =
     let updater =
       if (O.getValueOfBool "clock") then Stats.time "update_map_clock" (update_map_clock_a sp_lvmap_ref) 0
       else Stats.time "update_map" (update_map_a sp_lvmap_ref) 0
     in
       List.iter updater relevant_future_lvals;
       Stats.time "pred_lval_map_a" pred_lval_map_a sp_lvmap_ref 0 pred
   in
   (* 3. compute p' *)
   let p' =
     let sp_aliases_fun lvmap_ref lv =
       (* find all the aliases of lv, in the present lvmap *)
       let cand_ref = ref [] in
       let f (_,lv') _ =
	 if Absutil.queryAlias_cache (E.Lval lv) (E.Lval lv')
	 then cand_ref := lv'::(!cand_ref)
       in
	 lvmap_iter f lvmap_ref;
	 (lv::!cand_ref)
     in
     let this_sp pred (target,valu) =
       ac_block_sp closure_fun (sp_aliases_fun sp_lvmap_ref) sp_lvmap_ref None
	   (C_SD.Command.Expr (E.Assignment(E.Assign, target, valu)))
	   pred
     in
       P.conjoinL [p;(List.fold_left this_sp (P.True) asgn_list)]
   in
     (* 4. finally,  compute p'' *)
   let p'' =
     let transformer  = 
       let subs e' =
	 match e' with
	     E.Chlval _ ->
	       let dechlv = E.replaceChlvals e' in
		 if (lval_map_a sp_lvmap_ref 0 dechlv) = e' (* i.e. e' is the latest name of dechlv *)
		 then dechlv else e'
	   | _ -> e' (* this is strictly an optimization (?) ...
			the above would ensure that if e' was not a chlv, then the lhs of the if above IS a chlv,
			hence NOT eq the rhs, and so you get e' anyway ... *)
       in
	 E.deep_transform_top subs
     in
       P.atom_transform transformer p'
   in
(*
     M.msg_string M.Debug ("sp_via returns: "^(P.toString p''));
*)
     p''
	 
   (*******************************************************************************************)      

		   (** block-concrete_data_pre *)
 (* to do block_concrete_data_pre we need to know all the lvals that appear in a trace -- so we define load_lv_table here *)
 (* RJ: changing this to only track the values READ along a trace -- the other lvalues are useless to us CHECK!*)
let load_lv_table op_l =
  M.msg_string M.Debug "In load_lv_table";
  (* This closure stuff is commented out later (see the RJ's comment above).  Disregard it. *)
  let closure_fun =
    if (O.getValueOfBool "fldunroll") then
      C_SD.expression_closure_stamp true (* is_relevant_field_trace *)
    else C_SD.expression_closure
  in
    
  let add_write e = 
    let clos  = closure_fun e in (*TBD:CLOSURE*)
      add_to_lv_table (List.flatten (List.map Absutil.getVarsAndDerefs_exp clos))
  in
let proc_asgn asgn = 
  match asgn with
      C_SD.Command.Expr (E.Assignment(E.Assign, target, valu)) ->
	add_to_lv_table (Absutil.getVarsAndDerefs_exp valu)
	  (*
	let target_closure = closure_fun (E.Lval target) in
	let valu_closure = 
	  List.map 
	    (
	      fun x -> 
		let vx = 
		  (E.substituteExpression [(E.Lval target, valu)] x)
		in 
		  try E.Lval (E.push_deref (lv_of_expr vx))
		  with _ -> vx
	    )
	    target_closure
	in
	  add_to_lv_table (List.flatten (List.map Absutil.getVarsAndDerefs_exp (target_closure@valu_closure))) *)
| C_SD.Command.Return e -> (add_to_lv_table (Absutil.getVarsAndDerefs_exp e))
    | _ -> ()
 in
let process_op op = 
     match (C_SD.get_command op).C_SD.Command.code with
	 C_SD.Command.Skip -> ()
       | C_SD.Command.Pred e ->  (add_to_lv_table (Absutil.getVarsAndDerefs e))
       | C_SD.Command.Block el -> List.iter proc_asgn el
       | C_SD.Command.FunctionCall fcallExp -> 
	   let (_,_,target) = C_SD.deconstructFunCall fcallExp in
	   let asgn_list = Absutil.asgn_of_funcall (true,false) None fcallExp in
	   (*let _ =  if (target <> C_SD.junkRet) then add_write (E.Lval target) in*)
	     List.iter proc_asgn asgn_list
       | C_SD.Command.GuardedFunctionCall (p, fcallExp) ->  (* treat as a Pred followed by a function call *)
           (add_to_lv_table (Absutil.getVarsAndDerefs p)) ;
	   let (_,_,target) = C_SD.deconstructFunCall fcallExp in
	   let asgn_list = Absutil.asgn_of_funcall (true,false) None fcallExp in
	   List.iter proc_asgn asgn_list
       | _ -> ()
   in
  Hashtbl.clear trace_lv_table;
  Hashtbl.clear trace_field_table;
  List.iter process_op op_l

       
 let blocks_of_pred p = 
   match p with 
       P.And l -> l
     | _ -> failwith "bad call to blocks_of_pred"



	 

   (* Pre and Post *)

 (* concrete Pre for a function call *)
 (* take the region, and replace all the variables that are the formals
    of the function you just called with the expressions that were used
    to call it *)	

  let disambiguate_fnptr_call a =
    let fname = C_SD.get_location_fname (C_SD.get_source a) in 
      (* if this is a function pointer call, disambiguate it:
      disambiguation is done by looking at the fname passed in *)
      match ((C_SD.get_command a).C_SD.Command.code) with
        C_SD.Command.FunctionCall (E.FunctionCall(E.Lval (E.Symbol _), _)) 
      | C_SD.Command.FunctionCall (E.Assignment(_, _, E.FunctionCall(E.Lval (E.Symbol _), _)))  ->
        a
      | C_SD.Command.FunctionCall (E.FunctionCall(_, args)) ->
        C_SD.make_edge (C_SD.get_source a) (C_SD.get_target a) 
		(C_SD.Command.gather_accesses 
                  (C_SD.Command.FunctionCall(E.FunctionCall(E.Lval (E.Symbol fname), args))))
      | C_SD.Command.FunctionCall (E.Assignment(aop, t, E.FunctionCall(_, args)))  ->
         C_SD.make_edge (C_SD.get_source a) (C_SD.get_target a) 
	   (C_SD.Command.gather_accesses 
              (C_SD.Command.FunctionCall(E.Assignment(aop, t, E.FunctionCall(E.Lval (E.Symbol fname), args)))))
      | _ -> failwith "disambiguate_fnptr_call is called with something that is not a function call!"

 (* lastTarget looks at the stack of call points and returns the
    following info: (target := if the call was x = f(y), f, the successor location) *)

(* RJ: REMOVE ALL OCCURENCES OF LAST_LOC_POINTER !!! ITS VILE AND DANGEROUS *)
 let last_loc_pointer = ref (None)

 
 (* real_fn_o is an option of the actual function traversed if it was a function pointer call *)
 let lastTarget_loc ?(real_fn_o = None) cur_loc_o =
   match !last_loc_pointer with 
       None -> (M.msg_string M.Debug "lastTarget called with errorLocation  as top of stack !"); failwith "Empty Call Stack"
     | Some(callLoc) ->
	 let outEdges = C_SD.get_outgoing_edges callLoc in
	 let _ = if (List.length outEdges <> 1) then (M.msg_string M.Error "Call point with bad successor !!") in
	 let callEdge = (List.hd outEdges) in
	 let retLoc = C_SD.get_target callEdge in
	 let (fname,subs,target) = 
           match C_SD.call_on_edge callEdge with 
              None -> 
                (match cur_loc_o with None -> failwith "bad lastTarget 1!"
                 | Some loc -> 
                     if C_SD.is_dispatch_loc callLoc && O.getValueOfBool "events" 
                     then (C_SD.get_location_fname loc,[],C_SD.junkRet) else failwith "bad lastTarget 2!") 
           |  Some(_,e) -> begin
	     let e' = if O.getValueOfBool "nofp" then e else begin

	       (* If we handle function pointers, check if the call is a pointer.  If it is, create a fake call expression to deconstruct it afterwards. *)

	       (* A function to check for pointer call *)
	       let is_fnptr_call fcexp = match fcexp with
	       |  E.FunctionCall(E.Lval (E.Symbol _),_) | E.Assignment(_,_,E.FunctionCall(E.Lval (E.Symbol _),_)) -> false
	       | _ -> true
	       in
	       if is_fnptr_call e then
		 (* To make a fake call expression, we need arguments of the original call.  They will be matched with formals in deconstructFunCall that will be invoked later *)
		 let args = match e with
		 | E.FunctionCall(_, args)
		 | E.Assignment(_,_,E.FunctionCall(_, args)) -> args
		 | _ -> failwith "lastTargetLoc e is not a call!"
		 in
		 (* Read the real name, which is supplied from the main "post" function *)
		 let real_fname = match real_fn_o with Some fn -> fn | None -> failwith (Printf.sprintf "Real fname should be supplied for call %s!" (E.toString e)) in
		 (* print and return the fake call *)
		 let result = E.FunctionCall(E.Lval (E.Symbol real_fname), args) in
		 let _ = M.msg_string M.Debug (Printf.sprintf "in lastTargetLoc generated a fake call: %s" (E.toString result)) in
		 result
	       else e
	     end in
	     C_SD.deconstructFunCall e'
	   end
	 in
	 (target,fname,retLoc,subs)

 let lastTarget () = lastTarget_loc None 
         
(* Jhala: Misc functions required for the pre of function calls *)
 let getRetexp op =
   let cmd = C_SD.get_command op in
     match cmd.C_SD.Command.code with
	 C_SD.Command.Block l ->
	   begin
	     try
	       begin
		 let last_stm = Misc.list_last l in
		   match last_stm with
		       C_SD.Command.Return e -> e
		     | _ -> failwith "Bad call to getRetexp 1"
	       end
	     with _ -> failwith "Bad call to getRetexp 2"
	   end
       | _ -> failwith "Bad call to getRetexp3"

	   
let isReturn op = 
   let cmd = C_SD.get_command op in
   match cmd.C_SD.Command.code with
       C_SD.Command.Block l -> (List.exists (function x -> match x with C_SD.Command.Return e -> true | _ -> false) l)
   | _ -> false

let isBlock op =
   let cmd = C_SD.get_command op in
   match cmd.C_SD.Command.code with
          C_SD.Command.Block _ -> true
      | _ -> false

let isPredicate op =
   let cmd = C_SD.get_command op in
   match cmd.C_SD.Command.code with
          C_SD.Command.Pred _ -> true
      | _ -> false

let isFunCall op = 
   let cmd = C_SD.get_command op in
   match cmd.C_SD.Command.code with
       C_SD.Command.FunctionCall _ -> true 
   |   C_SD.Command.GuardedFunctionCall _ -> true 
   | _ -> false

	   
 let opFunName op = 
    let cmd = C_SD.get_command op in
    match cmd.C_SD.Command.code with 
    C_SD.Command.FunctionCall fcallExp -> Misc.fst3 (C_SD.deconstructFunCall fcallExp)
    | _ -> M.msg_string M.Error "opFunName called with bad arg !"; failwith "opFunName called with bad arg !"

	
 (* INV: p is of the form Forall s. p' *)
 let rec instantiate_quantifiers p = 
   let tf p s lv' = 
     let p' = P.substitute [(E.Lval (E.Symbol s)),(E.Lval lv')] p 
     in
     instantiate_quantifiers p'
   in
   match p with
   P.All (s,p') ->
     let lv'_l = 
       List.filter C_SD.is_skolem
       (C_SD.globals_of_type (C_SD.get_type_of_lval (E.Symbol s))) 
     in
     M.msg_string M.Normal (Printf.sprintf "[QI]: %s --> %s" s
     (Misc.strList (List.map E.lvalToString lv'_l)));
     P.And (List.map (tf p' s) lv'_l)
   | _ -> p
 
   
   (* this function expects the conc_data_reg to look like And ( pred_list) and returns something with that
    form as well *)

 (* this function pretends he's called with the predicate true -- its the callers job to collect 
    all the constraints! 
 *)


(* concrete Pre for a function call *)
(* take the region, and replace all the variables that are the formals
   of the function you just called with the expressions that were used
   to call it *)	

let get_callEdge_from_postloc postloc = 
  let rec get_fn_call_edge lst =
  match lst with
    a::rest -> 
      if (C_SD.isFunCall a) then
        if (List.exists C_SD.isFunCall rest) then failwith "multiple incoming function calls!"
        else disambiguate_fnptr_call a 
      else get_fn_call_edge rest
  | [] -> failwith ("no matching function call !") in
  get_fn_call_edge (C_SD.get_ingoing_edges postloc)

let get_callEdge_o op_array = 
  let (left,_) = Misc.paren_match Operation.paren_fun op_array in
  let f i postloc = 
    if O.getValueOfBool "events" then 
      let ci = left i in
      (assert (0 <= ci && ci < Array.length op_array);
      Some (op_array.(ci))) 
    else try Some (get_callEdge_from_postloc postloc) with _ -> None in
  f

let rec block_concrete_data_pre fname tid edge callEdge_opt =
   M.msg_string M.Debug "In block_concrete_data_pre -- arguments are:" ;
   M.msg_string M.Debug ("fname is " ^ fname) ;
   let edge = if O.getValueOfBool "events" && C_SD.isAsyncCall edge then C_SD.acAsyncCall edge else edge in
   let cmd = C_SD.get_command edge in
   M.msg_printer M.Debug C_SD.Command.print cmd ;
   M.msg_printer M.Debug C_SD.print_edge edge ;
   let lvmap_operation_option = 
     if (C_SD.isReturn edge) then 
        match callEdge_opt with 
          None -> failwith "block_CDP: return w.o. callEdge!" 
        | Some ce -> begin
	  (* When we're handling a return, we should update function maps, so that new maps for the local vars in the current function are created.  If we do not do this, functions like int f() { int x; return x; } will not be handled, because there's no assignment to distinguish x's between two calls *)
	  (* TODO: add _ in GuardedFunctionCall at merge! *)
	  let e = match (C_SD.edge_to_command ce).C_SD.Command.code with C_SD.Command.FunctionCall e | C_SD.Command.GuardedFunctionCall (_,e) -> e | _ -> failwith ("call edge is not a call edge?") in
	  (* Get local vars of the function we call *)
	  let locals = List.map (fun str -> (E.Symbol str)) (C_SD.lookup_locals (C_SD.get_name_of_call e)) in
	  (* Filter means that the values that satisfy it are remembered after we get out of the function.  So, only local vars should satisfy it. *)
	  (Absutil.set_ret_pointers_ce ce;Some(lvmap_push_filter (Some(fun lv -> not (List.mem lv locals)))))
	end
     else None in
       (* this should be a list of blocks of constraints *)
   let newpred =
     let rec _doBlock stmtlst pred  =
       (* INV: if lvmap_operation_option <> None then stmtlist = [Return] *)
       match stmtlst with
	   [] -> pred (*if (O.getValueOfBool "wpsat" && sat_flag) then sat_filter pred (!wpCapRegion) else*) 
	 | stmt::rest -> 
	       let wp_pred = Stats.time "block_wp" (block_wp lvmap_operation_option fname tid  [stmt]) pred in
		 _doBlock rest wp_pred in
     let doPred e =
               let qf_e = instantiate_quantifiers e in
               let qf_e = pred_to_mccarthy qf_e in
               let lvals_here = P.lvals_of_predicate e in
               let _ = List.iter lval_to_aliaser lvals_here in
               let _ = List.iter lval_to_closurer lvals_here in
               pred_lval_map tid qf_e 
     in
     let doFunCall fcallExp =
	     let callee_name = Misc.fst3 (C_SD.deconstructFunCall fcallExp) in
             let asgn_list = Absutil.asgn_of_funcall (false,false) (Some (in_lval_map tid)) fcallExp in
             (*let _ =   M.msg_string M.Normal "asgn_list is:"; *)
                       (*List.iter (M.msg_printer M.Normal C_SD.Command.print_statement) (asgn_list) in*)
	     let (targ_fname,lvmap_operation_option) = 
                 if (C_SD.isAsyncCall edge) then (callee_name,None)
                 else if (C_SD.is_defined callee_name) then (callee_name,Some(lvmap_pop))
                 else (fname,None) in
	     let rv = Stats.time "block_wp" (block_wp lvmap_operation_option targ_fname tid asgn_list) P.True in
	       (* RJ: pop only if its a real function that we entered ... *)
             (*let _ = M.msg_string M.Debug ("bwpred is : "^(P.toString rv)) in *)
	       rv
     in
     match cmd.C_SD.Command.code with
	   C_SD.Command.Block el -> begin
	   let tvl = _doBlock (List.rev el) (P.True) in
	   tvl; end
	 | C_SD.Command.Pred e ->
	     doPred e
	 | C_SD.Command.FunctionCall fcallExp ->
	     doFunCall fcallExp
	 (* a conditional function call a call-by-pointer was converted to *)
         | C_SD.Command.GuardedFunctionCall(pred, fcallExp) ->
	     (* NOTE: we should first convert call, then predicate, as precondition traverses the chain of events from the bottom *)
	     let lower_call = doFunCall fcallExp in
	     let upper_assume = doPred pred in
	     P.conjoinL [lower_call; upper_assume]
	 | C_SD.Command.Skip -> P.True
	 | C_SD.Command.Havoc lv -> 
             let block = [C_SD.Command.Expr(E.Assignment
             (E.Assign, lv, Absutil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)
	 | C_SD.Command.HavocAll -> failwith "Abstraction.block_concrete_data_pre: FunctionCall: TBD"
	 | C_SD.Command.Phi (sym, flag) ->
	     failwith "block_cdp: Phi unimplemented"
	     (*
	       let e = phi_command_to_pred sym flag phi_table in
	       let cmd' = {cmd with C_SD.Command.code = C_SD.Command.Pred e} in
	       let res = concrete_data_pre explicit_flag sat_flag conc_data_reg cmd' edge phi_table in
	       res.Region.pred
	     *)
         | C_SD.Command.SymbolicHook fname ->
                (* RJ: i believe it suffices to just plug these in for
                 * the lvals that are "relevant" i.e. appear in preds,
                 * not for every bloody lvalue mod. 
                 * Reason: if its mod in the body of this trace, then no need.
                 * if its not mod in the body of this trace, then WAIT
                 * first iter -- pred gets added -- lv becomes relevant.
                 * second iter -- after mod-smashing in summary_post gives a
                 * ctrex, you will add this constraint and thus the symvar gets
                 * added... *)
              let make_asgn lv = C_SD.Command.Expr (E.Assignment (E.Assign, lv, E.Lval lv)) in
              let globs_smashed = (PredTable.relevant_globals_mod fname) in
              M.msg_string M.Debug ("Hook-plug: "^(Misc.strList (List.map E.lvalToString globs_smashed)));
              let fmls =
                let acf = match C_SD.lookup_formals fname with C_SD.Fixed fml -> fml |
                C_SD.Variable fml -> fml in
                List.map (fun s ->  E.Symbol s) acf in
              let asgn_list = List.map make_asgn (globs_smashed @ fmls) in
                _doBlock (asgn_list) (P.True) 
   in
   let _ = Absutil.reset_ret_pointers () in
   newpred

   let make_formal_pred cname exec_i = 
    let ps =
      let eloc = C_SD.location_coords (C_SD.lookup_entry_location cname) in
      let allps = (PredTable.lookup_location_predicates eloc) in
      List.filter (fun i -> not (PredTable.is_pred_global i)) allps in
    let f idx = 
      let si = Printf.sprintf "b_p__BS__%d_%d" idx exec_i in
      let pidx = pred_lval_map 0 (PredTable.getPred idx) in
      P.Iff (P.Atom (E.Lval (E.Symbol si)), pidx) in
    P.And (List.map f ps)

   
   (*******************************************************************************************)
       
   let isPredicate op = C_SD.isPredicate op

   let can_escape lv = C_SD.can_escape lv

   let is_lock v = C_SD.is_a_lock v

   let is_global = C_SD.is_global 

   let is_foo foo_check r = 
     try
       let (loc,stack) = Region.getPcStack r in
       let stack_locs = loc::(locations_of_stack stack) in
	 List.exists foo_check stack_locs 
     with _ -> false (* failwith "is_foo fails!"*)

   let is_atomic = is_foo C_SD.is_atomic

   let is_event = is_foo C_SD.is_event

   let is_task = is_foo C_SD.is_task

   let is_spec = is_foo C_SD.is_spec

		   
   (*******************************************************************************************)

		   
   (* function to substitute tid -> notTid and locals with fresh copies.
      Uses the global phi_com_counter to get new names for locals 
      *)

   (******************************************************************************)
   (* new : with aliasing *) (** HEREHERHEREHEREHERE *)
   let phi_command_to_pred sym flag phi_table =
     assert (flag == true) ;

     let phi_pred newname sym1 sym2 =
       let phi = 
	 try
	   Region.rename_vars 
	     !(Hashtbl.find phi_table sym2) 
	     (newname) 
	 with Not_found ->

	   begin
	     (* RUPAK: check that this is sound.
		If the name sym2 is not in the phi_table, this means 
		that the syntactic lvalue name sym2 is never used in
		the program to read an address or to update an address.
		Hence we can assume that the phi is false.
		*)
	     M.msg_string M.Error ("phi_command_to_pred raised not found on lvalue "^(E.lvalToString sym2)) ;
	     Region.bot
	   end
       in
       begin
	 match phi with
	   Region.Empty -> P.False
	 |	_ ->
	     begin
	       if (sym1 = sym2) then Region.region_to_predicate phi
	       else
		 begin
		   P.And 
		     [ P.Atom (E.Binary(E.Eq, 
							 E.addressOf (E.Lval sym1),
							 E.addressOf (E.Lval sym2)));
		       Region.region_to_predicate phi ] 
		 end
	     end
       end
     in
     if can_escape sym then
       begin
	 if flag then 
	   begin
	     let aliases = AliasAnalyzer.getAllAliases sym in
	     let newname = new_name (Absutil.brand_new_suffix ()) in
	     let p = P.normalize (P.Or (List.map (phi_pred newname sym) (sym :: aliases)))
	     in
	     M.msg_string M.Normal ("Phi for "^(E.lvalToString sym)^" is "^(P.toString p)) ;
	     p
	   end
	 else 
	   begin
	     failwith "phi_command_to_pred : flag should not be false "
	   end
       end
     else
       P.False

 (* Jhala: End of misc functions ... *)




       
   (* Type : Region.concrete_data_region -> C_SD.Command.t -> C_SD.edge -> Region.concrete_data_region *)
   let rec concrete_data_pre explicit_flag sat_flag conc_data_reg cmd edge phi_table =
     M.msg_string M.Debug "In concrete_data_pre -- arguments are:" ;
     M.msg_printer M.Debug Region.print_concrete_data_region conc_data_reg ;
     M.msg_printer M.Debug C_SD.Command.print cmd ;
     let pred = conc_data_reg.Region.pred in
     let newpred =
       let rec _doBlock stmtlst pred i =
		 match stmtlst with
		   [] -> if (O.getValueOfBool "wpsat" && sat_flag) then sat_filter pred (!wpCapRegion) else pred
		 | stmt::rest -> 
		     begin
		       let wp_pred = wp explicit_flag sat_flag stmt pred in
		       if (rest = []) then 
			  if (O.getValueOfBool "wpsat" && sat_flag) then 
				  sat_filter wp_pred (!wpCapRegion)
			       else 
				  wp_pred
		       else
			  _doBlock rest wp_pred (i+1)
		     end
       in
       match cmd.C_SD.Command.code with
	   C_SD.Command.Block el -> _doBlock (List.rev el) pred 0 
	 | C_SD.Command.Pred e -> 
             let e = pred_to_mccarthy e in
             P.conjoinL  [e; pred]
	 | C_SD.Command.FunctionCall fcallExp ->
	     let asgn_list = Absutil.asgn_of_funcall (true,false) None fcallExp in
	     let _ =
	       M.msg_string M.Debug "asgn_list is:";
	       List.iter (M.msg_printer M.Debug C_SD.Command.print_statement)
		 (asgn_list)
	     in
               _doBlock asgn_list pred 0
	 | C_SD.Command.Skip -> pred
	 | C_SD.Command.Havoc lv ->
	     let new_var = 
	       match Absutil.getNewTmpVar (E.Symbol "__foo_havoc") with 
		 E.Chlval (E.Symbol x, s) -> s^x
	       | _ -> failwith "Abstraction.concrete_data_pre: Havoc: TBD"
	     in
	     let new_name v = 
	       if (v = lv) 
	       then E.Symbol new_var
	       else v
	     in 
	     P.alpha_convert new_name pred 
	 | C_SD.Command.HavocAll -> failwith "Abstraction.concrete_data_pre: FunctionCall: TBD"
	 | C_SD.Command.Phi (sym, flag) -> 
	     let e = phi_command_to_pred sym flag phi_table in
	     let cmd' = {cmd with C_SD.Command.code = C_SD.Command.Pred e} in
	     let res = concrete_data_pre explicit_flag sat_flag conc_data_reg cmd' edge phi_table in
	     res.Region.pred
         | C_SD.Command.SymbolicHook _ -> pred
     in
     { Region.pred = newpred ; }

  
 (** block-wp end *)

 let rec concrete_data_post conc_data_reg cmd edge phi_table =
     M.msg_string M.Debug "In concrete_data_post -- arguments are:" ;
     M.msg_printer M.Debug Region.print_concrete_data_region conc_data_reg ;
     M.msg_printer M.Debug C_SD.Command.print cmd ;

     let pred = conc_data_reg.Region.pred in
     let flag = O.getValueOfBool "wpsat" in
     let newpred =
       match cmd.C_SD.Command.code with
	   C_SD.Command.Block el -> List.fold_left (fun a -> fun b -> sp flag b a) pred el
	 | C_SD.Command.Pred e -> P.normalize (P.And [e; pred])
	 | C_SD.Command.FunctionCall fcallExp -> 
	     begin
	       let (_, _subs, _) = C_SD.deconstructFunCall fcallExp in
	       let asgn_list = List.map (fun (x,e) -> 
		 C_SD.Command.Expr(E.Assignment 
				     (E.Assign,E.Symbol(x),e))) _subs 
	       in         
	       List.fold_left (fun a -> fun b -> sp flag b a) pred asgn_list
	     end
	 | C_SD.Command.Skip -> pred
	 | C_SD.Command.Havoc sym -> 	    
	     let new_var = (* sym should be assigned a fresh variable *)
	       match Absutil.getNewTmpVar (E.Symbol "__foo") with 
		 E.Chlval (E.Symbol x, s) -> E.Lval (E.Symbol (s^x))
	       | _ -> failwith "Abstraction.concrete_data_post: Havoc of lval: TBD"
	     in
	     let assign = E.Assignment(E.Assign, sym, new_var) in
	     let stmt = C_SD.Command.Expr assign in
	     sp flag stmt pred
	 | C_SD.Command.HavocAll -> failwith "Abstraction.concrete_data_post: HavocAll: TBD"
	 | C_SD.Command.Phi (sym, flag) -> 
	     let e = phi_command_to_pred sym flag phi_table in
	     P.normalize (P.And [e; pred])
         | C_SD.Command.SymbolicHook _ -> pred
       in
     { Region.pred = newpred ; }




