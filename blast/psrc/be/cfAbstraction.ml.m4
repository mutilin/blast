(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)



(**
 * This module implements the abstraction interface connecting the model
 * checker to the concrete transition system.  It defines a Region module
 * based on Predicate Abstraction and BDDs, the symbolic pre and post,
 * and routines for refinment used by the model checker.
 *) 


open BlastArch

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Counter = Ast.Counter
module Stats = Bstats
module M = Message
module E = Ast.Expression
module Expression = Ast.Expression
module P = Ast.Predicate
module Predicate = Ast.Predicate
module AA = AliasAnalyzer
module O = Options

let valOf = function
    None -> failwith "valOf"
  | Some v -> v

module Make_C_Abstraction =
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
struct

  module PredBdd =  PredBdd.Make_PredBdd(C_SD) 
  module PredTable = PredTable.Make_PredTable(PredBdd)
  module Absutil = Absutil.Make_Absutil(PredTable)
  module Events = Events.Make_Events(PredTable)
  module Cmd = C_SD.Command

  open PredTable
  open Absutil
	
  let fprintf = Format.fprintf Format.std_formatter

  let check_races = ref None

  let assumed_invariants = ref []
  let maxStackLen = ref 0

  let getCurrentPredIndex () = PredTable.getCurrentIndex ()


				 
  let dump_abs () = PredTable.dump_abs ()
  let is_fun_refined fn = PredTable.is_fun_refined fn
		      

  let type_of_edge e = failwith "unimp cf"



  (**************************************************************************)
  (*********************************************************************************************)
  (*********************************************************************************************)
  (* The operation, DataLattice and Region modules  *)
				  include(abs-region.ml)
  (*********************************************************************************************)
  (*********************************************************************************************)


  (*******************************************************************************************)
				  (* for m4 *)
				  (* GUI functions *)
				  include(abs-gui.ml)
				  (* PRE/POST CONDITIONS + lvalue_maps *)
				  include(abs-wpsp.ml)
				  include(abs-adp.ml)(* TBD:ABS-ADP.ML *)
  (*******************************************************************************************)			 
  (* Starting the definition of PRE *)

  let setWPCapRegion reg =
    let conc_reg = Region.concretize reg in
      wpCapRegion :=
      match conc_reg with 
          Region.Empty -> P.False
        | Region.Atomic atomic_reg -> 
            begin
	      match atomic_reg.Region.data with
                  Region.Concrete foo -> foo.Region.pred
                | _ -> failwith ("bad call to setWPCapRegion")
	    end
	| Region.ParallelAtomic p_atomic_reg ->
	    begin
	      match p_atomic_reg.Region.p_data with
		  Region.Concrete foo -> foo.Region.pred
		| _ -> failwith ("bad call to setWPCapRegion")
	    end
	| _ -> failwith ("bad call to setWPCapRegion: Strange region type")

  let resetWPCapRegion () = 
    wpCapRegion := P.True
      
  (* the symbolic pre function: (approximate) weakest pre-condition *)
  (* RJ: WARNING -- this function is obsolete should not be used any more. Who uses this anyway ? *)
  let pre_template subs_flag sat_flag reg op phi_table =
    M.msg_string M.Error "WARNING:[OBSOLETE] In pre -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg ;
    M.msg_printer M.Debug Operation.print op ;
    M.msg_string M.Debug (if subs_flag then "Substitute \n" else "Explicit (no substitute) \n");
    M.msg_string M.Debug (if sat_flag then "sat-filter-on \n" else "sat-filter-off \n");
    match reg with
        Region.Empty ->
          Region.bot
      | Region.Atomic atomic_reg ->
          if ((C_SD.location_coords atomic_reg.Region.location <> C_SD.location_coords (C_SD.get_target op)) 
		&& (not (isReturn op)) && (not (isFunCall op))) then
            (* Jhala: This is only if this is not a return op *)
            begin
              M.msg_string M.Error "WARNING: pre: blast internal error suspected" ;
              M.msg_string M.Error "              operation do not match this region's location" ;
	      failwith "location mismatch";
              Region.bot
            end
          else
            begin
              let new_stack =
                if (isFunCall op) then
                  begin 
                    match (C_SD.get_command op).C_SD.Command.code with
			C_SD.Command.FunctionCall fcexp -> 
			  let function_is_defined fcexp = 
			    match fcexp with
				E.FunctionCall(E.Lval (E.Symbol(fname)), _) -> 
				  C_SD.is_defined fname
			      | E.Assignment (_, _, E.FunctionCall (E.Lval (E.Symbol fname), _)) -> 
				  C_SD.is_defined fname
			      | _ -> 
				  if (Options.getValueOfBool "nofp") then false
				  else failwith ("pre_template: Bad function call (Possible function pointer?):"^(E.toString fcexp))
			  in
			    if (not (function_is_defined fcexp)) then
			      atomic_reg.Region.stack
			    else 
			      begin
				let (_fname,_,target) = C_SD.deconstructFunCall fcexp in
				  if (_fname = C_SD.__NotImplementedFunctionName) 
				  then 
				    atomic_reg.Region.stack
				  else   
				    if (_fname <> C_SD.__BLAST_DummyFunctionName) then 
				      try (Region.CallStack (List.tl (Region.list_of_stack atomic_reg.Region.stack))) 
				      with e -> (print_string (Printexc.to_string e);failwith "Incorrect callstack type !!")
				    else 
				      atomic_reg.Region.stack (* was a skip *)
					
                              end
                      |    _ -> failwith "This can never happen: isFuncall checked that this was a function call!"
                  end
                else
                  if (isReturn op) then
                    let ce = get_callEdge_from_postloc atomic_reg.Region.location in
                    let callLoc = C_SD.get_source ce in
                      (Absutil.set_ret_pointers_ce ;
                       Region.CallStack (callLoc::(Region.list_of_stack atomic_reg.Region.stack)))
                  else atomic_reg.Region.stack
              in
              let new_location = (C_SD.get_source op)
              and new_data =
                match atomic_reg.Region.data with
                    Region.Abstract abst_data_reg ->
                      Region.Abstract (abstract_data_pre abst_data_reg (C_SD.get_command op) op atomic_reg.Region.location phi_table)
                  | Region.Concrete conc_data_reg ->
                      Region.Concrete (concrete_data_pre subs_flag sat_flag conc_data_reg (C_SD.get_command op) (op) phi_table)
              in
              Absutil.reset_ret_pointers ();
              Region.Atomic { Region.location = new_location ;
                              Region.data = new_data ;
                              Region.stack = new_stack ; }
		  
            end
      |	Region.ParallelAtomic p_atomic_reg ->
	  Region.bot
	    (* i am here *)
      | Region.Union _ ->
          failwith "pre: argument is a union: unimplemented"
      | Region.ParallelUnion _ ->
          failwith "pre: argument is a parallel union: unimplemented"


  (* Jhala: am setting sat flag to false for now ... *)
  let pre (* reg op phi_table *) = pre_template true false (* reg op phi_table *)
  let spre (* reg op phi_table *) = pre_template false false (* reg op phi_table *)
				      
				      
  let abstract_data_post abst_data_reg  cmd edge post_location phi_table = 
    match (Options.getValueOfString "post") with
        "H" -> (* Stats.time "Post H: " *) (abstract_data_post_das_dill abst_data_reg cmd) edge post_location phi_table
      | _  -> (* Stats.time "Post Graf-Saidi"*) (abstract_data_post_graf_saidi abst_data_reg  cmd) edge post_location phi_table 


  let abstract_predicate pred pred_list = 
    M.msg_string M.Debug "abstract_predicate called" ;
    assume_pred_ref := Some(pred);
    let abstract_bdd = postBdd bddOne (List.map (function i -> (i,getPred i,P.negate (getPred i),true)) pred_list) [] 
    in
      assume_pred_ref := None;
      Region.Abstract { Region.lelement = DataLattice.top; 
			Region.bdd = abstract_bdd; 
                        Region.preds = pred_list}


  let scope_kill data_region fname = 
    M.msg_string M.Debug "Inside scope_kill:\n";
    match data_region with
	Region.Abstract(abst_data_reg) ->
	  begin
	    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg;
	    let _kill_ff x =  
	      let predVars = getVarExps_deep (getPred x) in
		List.exists (fun x -> Misc.is_substring x ("@"^fname)) predVars
	    in
	    let new_bdd = hg_exists _kill_ff abst_data_reg.Region.bdd in
	    let ret_abst_data_reg  = { Region.lelement = abst_data_reg.Region.lelement ;
				       Region.bdd = new_bdd; Region.preds = abst_data_reg.Region.preds} in
	      M.msg_string M.Debug "Returning from kill_data_region with :\n";
	      M.msg_printer M.Debug Region.print_abstract_data_region ret_abst_data_reg;
	      Region.Abstract(ret_abst_data_reg)
          end
      | _ -> 
	  begin
	    M.msg_string M.Debug "bad call to kill"; 
	    data_region
	  end

  let get_new_data atomic_reg op new_location phi_table  =
    let _new_data =
      try 
        match atomic_reg.Region.data with
        Region.Abstract abst_data_reg ->
          Region.Abstract (abstract_data_post abst_data_reg (C_SD.get_command op) op new_location phi_table)
      | Region.Concrete conc_data_reg ->
          Region.Concrete 
          (concrete_data_post conc_data_reg (C_SD.get_command op) op phi_table)
      with C_SD.NoSuchFunctionException f -> failwith "error in get_new_data" in
    if ((Options.getValueOfBool "scope") && (isReturn op)) 
    then
      (let src = C_SD.get_source op in
       M.msg_printer M.Debug C_SD.print_location src;
       M.msg_printer M.Debug C_SD.print_location new_location;
       let (_,arg,_,_) = lastTarget_loc (Some src) in
       Stats.time "scope_kill" (scope_kill _new_data) arg) 
    else _new_data

  
  let post reg op phi_table =
    M.msg_string M.Debug "In post -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg ;
    M.msg_printer M.Debug Operation.print op ;
    Hashtbl.replace statVisitedFunctionTable (C_SD.get_location_fname (C_SD.get_target op)) true; 
    match reg with
      Region.Empty -> Region.bot
    | Region.Atomic atomic_reg ->
        begin
          try 
            if (not (Region.check_locations_eq atomic_reg.Region.location  (C_SD.get_source op))) 
            then failwith "post:operation do not match this region's location" ;
            let callStack = Region.list_of_stack atomic_reg.Region.stack in
            let tos_fname = 
                if (not (callStack = [])) 
                then (last_loc_pointer := Some (List.hd callStack);C_SD.get_location_fname (C_SD.get_target op)) 
                else (last_loc_pointer := None; C_SD.__BLAST_DummyFunctionName) in
            let tos_2_fname = 
                if (List.length callStack >= 1) then (C_SD.get_location_fname (List.hd callStack)) 
                else C_SD.__BLAST_DummyFunctionName in
            M.msg_string M.Debug (Printf.sprintf "tos_fname: %s, tos_2_fname: %s" tos_fname tos_2_fname);
            let (new_location, new_stack) = 
              if (isFunCall op) then
                match (C_SD.get_command op).C_SD.Command.code with
                  C_SD.Command.FunctionCall fcexp  
                | C_SD.Command.GuardedFunctionCall (_, fcexp) ->
                    begin
                      try
                        let (_fname,_,target) = C_SD.deconstructFunCall fcexp in
                        let _ = stackInfo := Some (_fname,tos_fname,true) in
                        if not (C_SD.enter_call op) then ((C_SD.get_target op), atomic_reg.Region.stack)
                        else   
                          (assert (C_SD.is_defined _fname); 
                           let newStack = (C_SD.get_source op) :: callStack in
                           (C_SD.lookup_entry_location _fname, Region.CallStack newStack))
		          (* else (C_SD.get_target op, atomic_reg.Region.stack)  *)
	              with C_SD.NoSuchFunctionException _ -> failwith "AAHA"
		    end (*try*)
                | _ -> failwith "error in post"
              else
                    try 
		      if (isReturn op) then 
                        let _ = stackInfo := Some (tos_fname,tos_2_fname,true) in
                        let (ret,_,nextLoc,subs) = lastTarget_loc (Some (C_SD.get_source op)) in
                        let _ = Absutil.set_ret_pointers ret subs (C_SD.get_location_fname nextLoc) in
                        let newStack = List.tl callStack in
                        (nextLoc,Region.CallStack newStack) 
                      else 
                        let _ = stackInfo := Some (tos_fname,tos_2_fname,false) in
                        (C_SD.get_target op,atomic_reg.Region.stack) 
                    with Failure ("tl") -> 
			  let _ = stackInfo := Some (tos_fname,tos_2_fname,false) in
                          (C_SD.get_target op, atomic_reg.Region.stack ) 
            in 
            let new_data = Stats.time "get_new_data" (get_new_data atomic_reg op new_location) phi_table in
            Absutil.reset_ret_pointers (); 
            M.msg_string M.Debug "Writing back post region" ;
            Region.Atomic { Region.location = new_location ;
                            Region.data = new_data ;
                            Region.stack = new_stack ; }
	      
       with Failure("Empty Call Stack") -> Region.bot
	        |  C_SD.NoSuchFunctionException f -> (* treat it as a skip *)
		    M.msg_string M.Minor ("Unexpected NoSuchFunctionException "^f);
		    (* THIS SHOULD BE COMMENTED OUT IF UNDEFINED FUNCTIONS ARE TO BE AVOIDED *)
		    (* failwith ("Unexpected NoSuchFunctionException "^f); *)
		    Region.Atomic
                      { Region.location = C_SD.get_target op;
			Region.data = (failwith "Here" ;atomic_reg.Region.data) ;
			Region.stack = atomic_reg.Region.stack;
		      } 
		      (* COMMENT OUT UPTO HERE *)

     end
        | Region.ParallelAtomic _  
	| Region.Union _ 
	| Region.ParallelUnion _  -> failwith "post: unimplemented"




  let extract_pred reg = 
    match reg with
	Region.Atomic r -> 
	  begin
	    match r.Region.data with 
		Region.Concrete x -> x.Region.pred 
	      | Region.Abstract a -> convertBddToPred a.Region.bdd
		  (*
		    (M.msg_string M.Error "Bad call to extract_pred"; failwith ("Bad call to extract pred"))*)
	  end
      | _ -> (M.msg_string M.Error "Bad call to extract_pred"; failwith ("Bad call to extract pred"))

  let join_abstract_data d1 d2 =
    { Region.lelement = DataLattice.meet d1.Region.lelement d2.Region.lelement ;
      Region.bdd = CaddieBdd.bddAnd d1.Region.bdd d2.Region.bdd ;
      Region.preds = Misc.union d1.Region.preds d2.Region.preds }

      
  let modvar_table = Hashtbl.create 31 (* filled with the globals that the callee may change *)




  (*******************************************************************************************)

  let statsMaxNumberOfPredsActive = ref 0
				      (* Keeps track of the number of predicates active
                                         at any point, discounting scope
                                      *)

  (* do_focus : get new predicates to add to the current set *)
  exception AnnotationException  
  exception NoNewPredicatesException
  exception RecursionException
    

  (* this is a highly stateful function! *)
  let last_no_of_preds = ref 0
  let predicate_fixpoint_reached () = 
    if (!last_no_of_preds = List.length !tableOfPreds) then 
      begin
	(* printTableOfPreds (); *)
	true
      end
    else
      begin
	last_no_of_preds := List.length !tableOfPreds;
	false
      end

	
  let do_focus abst_data_reg empty_conc_data_reg =
    M.msg_string M.Debug "In do_focus -- arguments are:" ;
    M.msg_printer M.Debug Region.print_abstract_data_region abst_data_reg ;
    M.msg_printer M.Debug Region.print_concrete_data_region empty_conc_data_reg ;
    let emptyPred = empty_conc_data_reg.Region.pred in
    let allPreds = extractUsefulPredicates emptyPred in
      (*let _= 
	begin
	M.msg_string M.Error "Candidate preds";
	List.iter (fun p -> M.msg_string M.Error (P.toString p) ) allPreds
	end
	in*)
      (* if this round did not add any new predicates, then we should restart *)
      (*
	let _ = Misc.write_to_file ".newpred" " " in     
      *)
    let oldnum = getCurrentIndex () in
    let added_preds = List.map addPred allPreds in
    let newnum = getCurrentIndex () in
      if (oldnum = newnum) then
	begin
          M.msg_string M.Debug "Restarting..." ;
	  Options.setValueOfBool "localrestart" true ;
        end
      else
        begin
          M.msg_string M.Debug "At least one predicate added ..." ;
	  Options.setValueOfBool "localrestart" false ;
        end ;
      
      let newpredlist = Misc.compact (Misc.difference (added_preds) (-1 :: abst_data_reg.Region.preds)) 
                          (* This is not correct ... as there may be a descendant along the error path 
                             that does not have a certain pred that is in this list which the 
                             ancestor ie the current data region has... if you are doing PATH that is ...*)
      in
        if (newpredlist = []) then (M.msg_string M.Error "No new predicates ! Unexpected ...";
                                    printTableOfPreds () ;
				    if (Options.getValueOfInt "predH" < 6) then
        			      raise NoNewPredicatesException;
				   );
        begin  
	  (* let _ =  M.msg_string M.Debug ("NEW PREDICATES FOUND \n") in *)
          let _listOfAllActivePreds = abst_data_reg.Region.preds @ newpredlist in
            ignore (if (List.length _listOfAllActivePreds) > !statsMaxNumberOfPredsActive then 
                      begin
			statsMaxNumberOfPredsActive := List.length _listOfAllActivePreds;
			M.msg_string M.Debug ("Size of pred set: "^(string_of_int (List.length !tableOfPreds)) );
			M.msg_string M.Debug "Active Preds"; 
			List.iter (function i -> M.msg_string M.Debug  (P.toString (getPred i)))  
                          _listOfAllActivePreds
                      end);
            { Region.lelement = abst_data_reg.Region.lelement ;
	      Region.bdd = abst_data_reg.Region.bdd ;
              Region.preds = _listOfAllActivePreds ; }
        end


  let replace_data_region reg new_data_reg =
    match reg with 
	Region.Atomic _reg ->
          Region.Atomic 
	  { Region.location = _reg.Region.location;
            Region.data = new_data_reg;
            Region.stack = _reg.Region.stack;
          }
      | _ -> failwith ("bad call to replace_data_region")


  (* ******************* ERROR TRACE FILTER ****************************** *)

  (* the following procedure takes a trace and spits out that subtrace of it which is used to 
     get a contradiction -- it is to help in "manual" counterexample analysis *)

  let  trace_filter region_list op_list = 
    (* the above is the unsatisfiable list of regions and operations -- arranged in "past --> present" order*)
    let reg_list_r = List.rev region_list in
    let op_list_r = List.rev op_list in
    let process_op_reg (phi , map) (op, reg) = () in
      ()
	
  (* ******************* find_all_preds ********************************** *)


  (* this function will look at ALL subtraces of a given trace and find all predicates coming from it *)



  (* again oplist is in increasing order past --> present *)
  (* r1 -> o1 -> r2 -> o2 -> ... -> on -> rn *)

  let extract_abs_data_reg reg = 
    match reg with
	Region.Atomic atomic_reg ->
	  begin
	    match atomic_reg.Region.data with
		Region.Abstract ar -> ar
	      | _ -> failwith "conc_dr given to extract_abs_data_reg"
	  end
      | _ -> failwith "union region given to extract_abs_data_reg"

  let quantify_and_extract_abs_data_reg = extract_abs_data_reg (* RJ: In short, I'm being lazy i.e. indolent *)


  let insert_data_reg reg dr = 
    match reg with
	Region.Atomic atomic_reg ->
	  begin
	    Region.Atomic 
	      { Region.data = dr;
		Region.location = atomic_reg.Region.location;
		Region.stack = atomic_reg.Region.stack;
	      }
	  end
      | _ -> failwith "bad call to insert_data_reg"


  (* this is called only in "block" mode ... and with noforget *)
  let focus_stamp reg =
    let abst_data_reg = extract_abs_data_reg reg in
    let cur = getCurrentIndex () in
    let present_prec = 
      try List.hd abst_data_reg.Region.preds 
      with _ -> -1 
    in
      if (present_prec = cur && not (refine_check ())) then
	begin
	  dump_abs ();
	  raise NoNewPredicatesException;
	end
      else
	begin
	  insert_data_reg reg 
	    (Region.Abstract 
	       { Region.lelement = abst_data_reg.Region.lelement; 
		 Region.bdd = abst_data_reg.Region.bdd;
		 Region.preds = cur :: abst_data_reg.Region.preds (*[cur]*);
	       })
	end




  (* Cheap hack: go through all the predicates along oplist and add them to the list of predicates. *)

  let find_all_predicates_cheap oplist =
    let rec _more lst accum =
      match lst with
	  [] -> accum
	| a :: rest ->
            begin
              match (C_SD.get_command a).C_SD.Command.code with
		  C_SD.Command.Pred e ->
                    if e = P.True then _more rest accum
                    else 
                      begin
			let ee = match e with P.Not e' -> e' | P.Atom _ -> e | _ -> failwith "nonatomic predicate on path!" in
			  _more rest (ee :: accum)
                      end
		| _ -> _more rest accum
            end
    in
    let all_preds_in_path = _more oplist [] in
    let newpredlist = Misc.compact 
			(Misc.difference 
                           (List.map addPred all_preds_in_path) 
                           (-1 :: ( PredTable.get_all_pred_indexes ()))) 
    in 
    let number = List.length newpredlist in
      M.msg_string M.Debug ("find_all_predicates_cheap found "^
					(string_of_int number)^" predicates.") ;
      number

  (* RJ: OBSOLETE ? *)
  let find_all_predicates reglist oplist = 
    let list_of_predicates = ref [] in
    let oplist_r = List.rev oplist in
    let reglist_r = List.rev reglist in

    let rec _proc_sub seed ol = 
      (* take wp of seed wrt (ol.head,rl.head) -- query for unsat if unsat take preds... quit and break out ..
	 else if sat, then take region and recur on it with ol.tail, rl.tail *)
      match ol with
	  op::t -> 
	    begin 
	      let emptytable = Hashtbl.create 2 in
	      let pre_reg = pre seed op emptytable in
		if (Region.is_empty pre_reg) then 
		  begin
		    let allpreds = extractUsefulPredicates (extract_pred pre_reg) in
		      M.msg_string M.Debug ("Learning new predicates: ");
		      List.iter (fun x -> list_of_predicates := x::(!list_of_predicates); 
				   M.msg_string M.Debug ((P.toString x)^" , ")) allpreds;
		      M.msg_string M.Debug ("From region: "^(P.toString (extract_pred pre_reg)))
			(* do we or do we not recur on pre_reg ? as of now, NO *)
		  end
		else 
		  _proc_sub pre_reg t
	    end
	| [] -> ()
    in

    let rec process_subtrace ol_r rl_r = 
      match rl_r with
	  h::h1::t ->
	    let seed_reg = (replace_data_region h (Region.Concrete {Region.pred = P.True})) in
	      _proc_sub seed_reg ol_r; 
	      process_subtrace (List.tl ol_r) (h1::t)
	| _ -> ()
    in
      (*
	let current_preds = !tableOfPreds in
	let current_pred_vars = 
	List.map (fun x -> E.Symbol x)
	(Misc.compact (List.flatten (List.map (fun x -> getVarExps_deep (Misc.snd3 x)) current_preds)))
	in
	let make_predicate v c = P.Atom (E.Binary (E.Eq,v,c)) in
	let process_var v = 
	try 
	List.map (make_predicate v) (Hashtbl.find lval_constant_table v)
	with
	Not_found -> failwith ("lval not in lval_constant_table: "^(E.toString v))
	in
	let new_const_preds = List.flatten (List.map process_var current_pred_vars) in
	let _ = List.map addPred new_const_preds in
	let _ = 
	if (List.length !tableOfPreds = List.length current_preds) then
	failwith (" No new constant preds learnt! ")
	else 
	begin
	M.msg_string M.Debug "new_const_predicates found: \n";
	List.iter (fun x -> M.msg_string M.Normal (P.toString x)) new_const_preds;
	M.msg_string M.Normal ("# new preds = "^(string_of_int (List.length new_const_preds )));
	end
      *)  
      (* this is the point where the main function is called ! *)
      (*  The stuff in this needs to be worked on ... its *very* slow ... 
	  let _ = process_subtrace oplist_r reglist_r in
	  let newpredlist = Misc.compact (Misc.difference (List.map addPred !list_of_predicates) (-1 :: ( List.map Misc.fst3 !tableOfPreds))) in
	  M.msg_string M.Normal "find_all_predicates found: \n";
	  List.iter (fun x -> M.msg_string M.Normal (P.toString x)) !list_of_predicates;
	  M.msg_string M.Normal ("# new preds = "^(string_of_int (List.length newpredlist)));
	  if (newpredlist = [] ) then failwith ("find_all_predicates also fails to get new predicates !!") *)

      if find_all_predicates_cheap oplist == 0 then
	begin
	  let _ = process_subtrace oplist_r reglist_r in
	  let newpredlist = Misc.compact (Misc.difference (List.map addPred !list_of_predicates) (-1 :: ( PredTable.get_all_pred_indexes ()))) in
	    M.msg_string M.Debug "find_all_predicates found: \n";
	    List.iter (fun x -> M.msg_string M.Debug (P.toString x)) !list_of_predicates;
	    M.msg_string M.Debug ("# new preds = "^(string_of_int (List.length newpredlist)));
	    if (newpredlist = [] ) then failwith ("find_all_predicates also fails to get new predicates !!")
	end


	  (**************************************************************************************)

	  (* this is for m4 *)
	  include(abs-boolean.ml)
	  include(abs-include.ml)
	  (**************************************************************************************)

  (* the refinment operator required by the ABSTRACTION signature *)
  let focus reg_to_refine empty_reg  =
    M.msg_string M.Debug "In focus -- arguments are:" ;
    M.msg_printer M.Debug Region.print reg_to_refine ;
    M.msg_printer M.Debug Region.print empty_reg ;

    match (reg_to_refine, empty_reg) with
        (Region.Atomic abst_atomic_reg, Region.Atomic empty_conc_atomic_reg) ->
          begin
            try 
              match (abst_atomic_reg.Region.data, empty_conc_atomic_reg.Region.data) with
                  (Region.Abstract abst_data_reg, Region.Concrete empty_conc_data_reg) ->
                    Region.Atomic { Region.location = abst_atomic_reg.Region.location ;
                                    Region.data = Region. Abstract (Stats.time "do_focus" (do_focus abst_data_reg) empty_conc_data_reg) ;
                                    Region.stack = abst_atomic_reg.Region.stack ; }
		| _ -> failwith "focus: input regions should be abstract and concrete in this order"
            with NoNewPredicatesException ->
              ( M.msg_string M.Debug "In focus -- arguments are:" ;
                M.msg_printer M.Debug Region.print reg_to_refine ;
                M.msg_printer M.Debug Region.print empty_reg ;
                M.msg_string M.Error "No new predicates found. Aborting." ;
                raise NoNewPredicatesException
              )
          end
      | _ -> failwith "focus: arguments should be atomic regions"


  (* the following function takes a concrete data region and a list of predicates and returns an 
     abstract region corresponding to it. Cousot would call it alpha *)


  let abstract atomic_conc_reg pred_list =
    let assumption  =
      match atomic_conc_reg.Region.data with
	  Region.Concrete cr -> cr.Region.pred
	| _ -> failwith ("bad call to atomic_conc_reg")
    in
      assume_pred_ref := Some(assumption);
      let abstract_bdd = postBdd bddOne (List.map (function i -> (i,getPred i,P.negate (getPred i),true)) pred_list) [] 
      in
	assume_pred_ref := None;
	Region.Abstract { Region.lelement = DataLattice.top ; (* I assume I know nothing about the lattice state
								 Each lattice should implement an
								 alpha that takes a concrete predicate and
								 returns a lattice element
							      *) 
			  Region.bdd = abstract_bdd; Region.preds = pred_list}
	  


  let intersect_with_abstraction reg1 reg2 =
    (* reg1 : abstract, reg2 : concrete or abstract, returns reg1 cap reg2 : abstract *)
    let reg2' = precise reg2 in
      match (reg1,reg2') with
	  (Region.Atomic atr1, Region.Atomic atr2) ->
            begin
	      M.msg_string M.Debug ("intersect_with_abstraction:\n" ^ 
						"    reg1 = " ^ Region.to_string reg1 ^
						"    reg2 = " ^ Region.to_string reg2);
              match (atr1.Region.data,atr2.Region.data) with
		  (Region.Abstract abst_1,Region.Concrete conc_2) ->
		    let preds = abst_1.Region.preds in
		    let abst_2 = abstract atr2 preds in
                    let newreg2 = Region.Atomic { Region.location = atr2.Region.location;
                                                  Region.data = abst_2;
                                                  Region.stack = atr2.Region.stack; }
                    in
                      Region.cap reg1 newreg2
		| _ -> failwith "bad call to intersect_with_abstraction"
            end
	| _ -> failwith "bad call to intersect_with_abstraction 2"
	    
  let print_abs_stats () = 
    M.msg_string M.Normal ("TP Queries:" ^ (string_of_int !statActualQueriesForPost));
    M.msg_string M.Normal ("Reached Cubes:" ^ (string_of_int !statReachedCubes));
    ()
      

      
  (** this function is the heart of cfAbs. Here's the deal:
    Given:
    1. p_in (the region for the caller -- at the callsite) -- encoded here inside "caller"
    2. p_out (the region at the callee's return site -- encoded here inside "exit"
    we want the post. w.r.t the call of p_in, where the effect of the call is encoded inside p_out.
    -- let p_in' = p_in \cap_{relevant_sym_consts}(c = ac.c)
    where ac.c = (peel c) -- if its a global then, thats that, else, replace all occurrences of formal x, with actual e
    let p_out' = SP.p_in'.(asgn_of_return)
    output: pred_abs.(p_out \cap p_out')
  *)
   
  let statTotalNumberOfSummaryPosts = ref 0

  let summary_post_data caller exit call_op ret_op =
    let _ = statTotalNumberOfSummaryPosts := 1 + !statTotalNumberOfSummaryPosts in
    (* used to rename the callee_exit in case of clash *)
    let (closure_fun,closure_fun_lv) = 
      if (Options.getValueOfBool "fldunroll") then
	(C_SD.expression_closure_stamp false (* (PredTable.is_relevant_field_image) *),
	 C_SD.lvalue_closure_stamp false (*  (PredTable.is_relevant_field_image) *))
      else (C_SD.expression_closure,C_SD.lvalue_closure)
    in
    let _ = M.msg_string M.Debug "Summary Post" in
      match (Region.advance_location caller, exit) with
	  (Region.Atomic ({Region.data = Region.Abstract d1} as at1),
	   Region.Atomic ({Region.data = Region.Abstract d2} as at2)) ->
	    begin (* TBD: RUPAK: I NEED TO LOOK AT THE PRED HERE! *)
	      M.msg_string M.Debug "Printing abstract data regions: ";
	      M.msg_printer M.Debug Region.print (Region.Atomic at1);
	      M.msg_printer M.Debug Region.print (Region.Atomic at2);
	      let pred_i_list = 
		!globally_useful_preds @
		(try 
		   Hashtbl.find PredTable.fun_pred_table 
		     (fst (C_SD.location_coords (at1.Region.location)))
		 with Not_found -> [])
	      in
	      let get_fcexp edge = 
		match (C_SD.get_command edge).C_SD.Command.code with
		    C_SD.Command.FunctionCall fcexp -> (P.True, fcexp )
		  | C_SD.Command.GuardedFunctionCall (p, fcexp) -> (p, fcexp)  
		  | _ -> failwith "Bad op/edge arg: summary_post"
	      in
	      let (edge, guard, (callee_name, subs, target)) =  
                let (grd, fce) = get_fcexp call_op in 
                (call_op, grd, C_SD.deconstructFunCall fce) in 
	      let _ = M.msg_string M.Debug ("Target: "^(E.lvalToString target)) in
	      let retres = getRetexp ret_op in
		if (CaddieBdd.bddEqual d1.Region.bdd PredTable.bddOne &&
		    CaddieBdd.bddEqual d2.Region.bdd PredTable.bddOne &&
		    DataLattice.leq DataLattice.top d1.Region.lelement &&
		    DataLattice.leq DataLattice.top d2.Region.lelement && 
		    (not (PredTable.is_relevant_lval target)
		     || (pred_i_list = []) 
		     || (target = C_SD.junkRet) 
		     || (E.is_lval retres)
		    )
		   )
		then
		  begin
		    M.msg_string M.Debug "Trivial post";
		    Region.Atomic {at1 with Region.data = at2.Region.data}
		  end
		else
		  let creg_caller = extract_pred caller in (* p_in *)
		  let ac_creg_callee_exit = extract_pred exit in (* p_out *)
		  let caller_name = reg_function_name caller in
		  let name_clash = (caller_name = callee_name) in
		  		  let (retres,creg_callee_exit) = 
	    	    if not name_clash then (retres, ac_creg_callee_exit) 
		    else 
                      ((E.alpha_convert rename_locals retres),(P.alpha_convert rename_locals ac_creg_callee_exit))
		  in
		  let _ = Absutil.set_ret_pointers target subs caller_name in 
		    (* let callee_name = reg_function_name exit in *)
		  let _ =
		    M.msg_string M.Debug
		      ("Caller (input) Region:"^(P.toString creg_caller));
		    M.msg_string M.Debug
		      ("Callee (output) Region:"^(P.toString creg_callee_exit))
		  in
		  let rel_globs_mod =
		    Stats.time "relevent_globals_mod"
		      ((fun () -> PredTable.relevant_globals_mod) ()) callee_name
		  in
		  let _ = M.msg_string M.Debug 
			    ("rel_globs_mod: "^(Misc.strList (List.map E.lvalToString rel_globs_mod)))
			    
		  in
		  let symvar_fun = 
		    if not name_clash then E.make_symvar callee_name
		    else
		      (fun lv -> 
			 match E.make_symvar callee_name lv with
			     E.Symbol s -> E.Symbol (s^"BLAST")
			   | _ -> failwith "bad return from make_symvar")
		  in	
		  let get_sym_preds () = 
		    let make_eq replace_fn lv =
		      let lv_clos = Stats.time "lv-closure" closure_fun_lv lv in 
		      let _ = M.msg_string M.Debug 
				(Printf.sprintf "lv (%s)--- clos (%s)" (E.lvalToString lv)
				   (Misc.strList (List.map E.lvalToString lv_clos)))
		      in
			P.equate
			  (List.map (fun lv -> E.Lval (symvar_fun lv)) lv_clos)
			  (List.map (fun lv -> replace_fn (E.Lval lv)) lv_clos) in
		    let g_pred =
		      P.conjoinL
			(List.map (make_eq (fun x -> x)) rel_globs_mod) in
		    let l_pred = 
                      P.conjoinL
                        (List.map (fun (x,e) -> make_eq (E.substituteExpression [(E.Lval (E.Symbol x), e)]) (E.Symbol x))
                         subs) in
		    (g_pred,l_pred)
		  in
		  let creg_callee_exit' = (* p_out' *)
                  begin
		      let (g_sym_pred,l_sym_pred) = Stats.time "get sym preds" get_sym_preds () in
		      let creg_caller' = P.conjoinL [creg_caller;g_sym_pred;l_sym_pred] in (* p_in' *)
		      let get_asgn_list () =
			let params_list = (* could do an optimization similar to the global situation here *)
			  let _ = M.msg_string M.Debug "Getting copy_back asgns" in
			  let (_,copy_back_asgns) = Stats.time "asgn of ret" Absutil.asgn_of_return callee_name in
			  let _cba = 
			    if target = C_SD.junkRet then copy_back_asgns
			    else
			      (target,retres)::copy_back_asgns
			  in 
			    List.filter 
                              (fun (l,_) -> 
				 let rv = PredTable.is_relevant_lval l in                                         
                                 M.msg_string M.Debug (Printf.sprintf "Check relevant cb: %s : %b" (E.lvalToString l) rv);
				 rv) _cba
			in
			let globals_list =
			  let glvs = rel_globs_mod in 
			    (* earlier:   C_SD.global_lvals_modified callee_name   in *) 
			    List.map (fun glv -> (glv,Absutil.get_new_unknown callee_name)) glvs
			in
			  globals_list@params_list
		      in
		      let asgn_list = Stats.time "get asgn list" get_asgn_list () in
		      let dump_asgn (lv,e) = M.msg_string M.Debug
					       (Printf.sprintf "%s := %s" (E.lvalToString lv) (E.toString e))
		      in
		      let _ = List.iter dump_asgn asgn_list in
			(* now do the "return" using the following trick. the copy backs, i.e the WP step *)
		      let pred_list = List.map PredTable.getPred pred_i_list in
		      let relevant_future_lvals : (E.lval list) =
			let cands = List.flatten (List.map getVarsAndDerefs (creg_caller'::pred_list)) in
			let cand_lvs =
			  Misc.map_partial
			    (fun e -> try Some (E.lv_of_expr e) with _ -> None)
			    cands
			in
			  Misc.sort_and_compact cand_lvs
		      in
			
		      let _ = M.msg_string M.Debug "Now building creg_callee_exit'" in
			
			Stats.time "sp_via_block" (sp_via_block closure_fun relevant_future_lvals creg_caller') asgn_list
		    end
		  in 
		  let _ = M.msg_string M.Debug ("callee_exit' = "^(P.toString creg_callee_exit'))
		  in
		  let lift_conj () = (abstract_predicate
					(P.conjoinL [creg_callee_exit';creg_callee_exit])
					pred_i_list) 
		  in
		  let _ = M.msg_string M.Debug ("Using preds:"^(Misc.strList (List.map (fun i -> P.toString (PredTable.getPred i)) pred_i_list)))
		  in
		  let new_data = Stats.time "lift conj" lift_conj () in
		  let reg = Region.Atomic {at1 with Region.data = new_data} (* abs.Gamma.(p_out' \cap p_out) *)
		  in 
		    M.msg_string M.Debug "Summary Post: Region is ";
		    M.msg_printer M.Debug  Region.print  reg;
		    M.msg_string M.Debug (Printf.sprintf "@.After restrict to %s: " caller_name);
		    (* this restrict business should not be needed if craig is being used *)
		    let reg' = Stats.time "Region.restrict"
				 (Region.restrict (Region.locals_and_globals caller_name)) reg
		    in
		      M.msg_printer M.Debug Region.print reg';
		      reg'
            end
	| _ -> failwith "One or both regions passed to summary_post are not Abstract-Atomic"
	    
  let summary_post caller exit callop retop = 
    let reg' = summary_post_data caller exit callop retop in
    match (reg', caller, exit) with
      (Region.Atomic({Region.data = Region.Abstract adr_reg'} as at'),
       Region.Atomic({Region.data = Region.Abstract adr_caller}),
       Region.Atomic({Region.data = Region.Abstract adr_exit})) -> 
         let l' = DataLattice.summary_post adr_caller.Region.lelement
         adr_exit.Region.lelement callop retop in
         let adr_reg'' = {adr_reg' with Region.lelement = l'} in 
         Region.Atomic {at' with Region.data = Region.Abstract adr_reg''} 
    | _ -> failwith "error in summary_post"
    
  let initialize_abstraction () =
    PredTable.load_abs ();
    M.msg_string M.Normal "Initialized Abstraction";
    if (Options.getValueOfBool "cf") then DataLattice.initialize ()
      
  (* Saving final abs from tree *)
  let save_region_abs reg = 
    let l_id = C_SD.location_coords (location_coords_of_region reg) in
    let preds = PredTable.bdd_support (extract_abs_data_reg reg).Region.bdd in
      PredTable.add_local_preds l_id preds

	
  (* this function will allow us to create the initial and error regions
     to give to the model checking function *)

  let ac_create_region abs_conc_flag loclist stack_bool seedPreds initialStateP = 
    failwith "unimplemented: cfAbs ac_create_region"
  let add_alias_override _ = failwith "CF: alias override unimplemented"

  let create_region loclist err_init_bool (* stack_bool *) seedPreds initialStateP =
    let atom_list = 
      (P.getAtoms seedPreds) @ (P.getAtoms initialStateP)
    in
    let ind_list = List.map (fun x -> addPred x) atom_list in 
    let pred_indexes = List.filter (fun a -> if (a> -1) then true else false) ind_list in
    let gpred_indexes = 
      if (Options.getValueOfBool "scope" || Options.getValueOfBool "cf") then
    	Misc.map_partial (PredTable.add_pred_to_scope) pred_indexes
      else pred_indexes
    in
    let _ = M.msg_string M.Normal "Glob useful preds" in
    let _ = M.msg_string M.Normal 
	      (Misc.strList (List.map (fun i ->
					 P.toString
					 (PredTable.getPred i)) gpred_indexes)) in
    let _ =	globally_useful_preds := gpred_indexes @ !globally_useful_preds in
      (*let our_bdd = convertPredToBdd p in *)
    let our_bdd =
      try
	convertPredToBdd initialStateP
      with Not_found -> failwith "create_region: Some predicate in the initial state not found in the predicate table!"
    in
    let (our_stk,our_le) = 
      if err_init_bool then (Region.EveryStack,DataLattice.top) else (Region.CallStack [], DataLattice.init) in
    Region.Atomic {
	Region.location = List.hd loclist ;
	Region.data = Region.Abstract { Region.lelement = our_le ; Region.bdd = our_bdd ; Region.preds = pred_indexes ; } ;
        Region.stack = our_stk; 
      }


	
  let print_stats fmt () =
    M.msg_string M.Normal ("CF Abstraction Statistics");
    M.msg_string M.Normal ("Total number of summary-posts = "^(string_of_int (!statTotalNumberOfSummaryPosts)));
    M.msg_string M.Normal ("Total number of queries = "^(string_of_int (!statTotalNumberOfQueries))^"\n");
    M.msg_string M.Normal ("Total number of non-post queries = "^(string_of_int (!statCoveredQueries))^"\n");
    M.msg_string M.Normal ("PostBdd Calls = "^(string_of_int
    !statPostBddCalls));
    M.msg_string M.Normal ("Worst-case number of post queries = "^(string_of_int (!statWorstCaseQueriesForPost))^"\n");
    M.msg_string M.Normal ("Actual number of post queries = "^(string_of_int (!statActualQueriesForPost))^"\n");
    M.msg_string M.Normal ("Assume post queries = "^(string_of_int (!statAssumeQueriesForPost))^"\n");
    M.msg_string M.Normal ("Total posts = "^(string_of_int (!statNumPost))^"\n");
    M.msg_string M.Normal ("Total assume posts = "^(string_of_int (!statNumAssumePost))^"\n");

    M.msg_string M.Normal ("Total cached = "^(string_of_int (!statTotalCached))^"\n\n");
    M.msg_string M.Normal ("Total foci queries = "^(string_of_int (!TheoremProver.stats_nb_foci_queries))^" ");

    M.msg_string M.Normal ("Total number of alias queries = "^(string_of_int !AliasAnalyzer.stats_nb_alias_query));
    M.msg_string M.Normal ("Total number of cached exp_closures ="^(string_of_int !C_SD.stats_nb_cached_exp_closure));
    M.msg_string M.Normal ("Total number of queryAlias_cache calls = "^(string_of_int !Absutil.stats_nb_queryAlias_cache_called ));
    M.msg_string M.Normal ("Total number of queryAlias_cache hits = "^(string_of_int !Absutil.stats_nb_queryAlias_cache_hits ));
    M.msg_string M.Normal ("Total number of tproj alias queries = "^(string_of_int !stats_nb_tproj_alias_queries));
    M.msg_string M.Normal ("Total number of lv_rd alias queries (computed) = "^(string_of_int !stats_nb_get_lvrd_aliases_compute));
    M.msg_string M.Normal ("Total number of lv_rd alias queries (cached) = "^(string_of_int !stats_nb_get_lvrd_aliases_cached));
    if !stats_nb_get_lvrd_aliases_compute > 0 then
      M.msg_string M.Normal ("Average Alias Set size number of lv_rd alias queries (computed) = "^(string_of_int (!stats_total_alias_set_size / !stats_nb_get_lvrd_aliases_compute)));
    M.msg_string M.Normal ("Max Alias Set size = "^(string_of_int !stats_max_alias_set_size));
    M.msg_string M.Normal ("Total number of recomp exp_closures ="^(string_of_int !C_SD.stats_nb_recomp_exp_closure));
    M.msg_string M.Normal ("The List of predicates:\n");
    printTableOfPreds ();
    M.msg_string M.Normal 
      ("Maximum number of predicates active together (discounting scope) = "^(string_of_int (!statsMaxNumberOfPredsActive))^"\n");
    M.msg_string M.Normal ("Functions visited:");
    let fvcnt = ref 0 in
      Hashtbl.iter (fun x y -> fvcnt := !fvcnt +1;
		      print_string (x^" , ")) statVisitedFunctionTable;
      
    M.msg_string M.Normal (Printf.sprintf "\nNumber of visited functions:  %d" !fvcnt);
    dump_abs (); ()




  (* **************** CONCURRENCY **************** *)

  (* this function will reset all the appropriate global data structures ... *)

  let reset () = 
    Hashtbl.clear theoremProverCache;
    Hashtbl.clear predTbl;
    Hashtbl.clear predTbl_deep; 
    Hashtbl.clear unchangedPredTable; 
    (* [RUPAK] Do not reset the predicates
       tableOfPreds := [];
       predIndex := -1;
    *)
    Absutil.reset_ret_pointers ();
    last_loc_pointer := (None);
    assume_pred_ref := None;
    ()

  let exit_abstraction ()  = PredTable.kill_predbdd () 

  (*  let iter_global_variables = C_SD.iter_global_variables
  *)
  let iter_global_variables = C_SD.iterate_all_lvals


  let expression_mentions_locals e =
    try
      let reject_local (name : E.lval) = if is_local name then raise Exit; name in
      let _ = E.alpha_convert reject_local e in false
    with Exit -> true


  type access_type = Anone | Aread | Awrite | Aboth
  let accessing op v =
    let reads, writes = C_SD.reads_and_writes_at_edge op in
      match (List.mem v reads, List.mem v writes) with
	| false, false -> Anone
	| false, true -> Awrite
	| true, false -> Aread
	| true, true -> Aboth

  let get_all_writes op =
    snd (C_SD.reads_and_writes_at_edge op)

  let get_all_reads op =
    fst (C_SD.reads_and_writes_at_edge op)

  let reads_and_writes op = C_SD.reads_and_writes_at_edge op 


  let globally_important_lvals () = !PredTable.globally_important_lvals

  let aliasesOf lval = if can_escape lval then ( lval::(AliasAnalyzer.getAllAliases lval)) else []

  let eq_addr_constraint_region lval1 lval2 = 
    let p = 
      P.Atom 
	(E.Binary 
	   (E.Eq, 
	    E.addressOf (E.Lval lval1),
	    E.addressOf (E.Lval lval2)))
    in
      predicate_to_region p

  (* this should return the list of possible aliases in the whole program, of some lval 
     maybe we should keep a Hashtbl or something and query that every time 
  *)

  let make_multiply_instantiable_region r1 r2 lval1 lval2 = 
    M.msg_string M.Debug "In make_multiply instantiable\n\n" ;
    M.msg_string M.Debug ("r1 is"^(Region.to_string r1)) ;
    M.msg_string M.Debug ("r2 is"^(Region.to_string r2)) ;
    let rename_fun = new_name (brand_new_suffix ()) in
    let eq_addr_const_reg = eq_addr_constraint_region lval1 (E.alpha_convert_lval rename_fun lval2) in 
      Region.cap 
	(Region.cap !assumed_invariants_region eq_addr_const_reg)
	(Region.cap r1 (Region.rename_vars r2 rename_fun))
	
	
  (* the below function should ONLY be called on two regions known to be such that:
     1. lval_1, lval_2 are such that they are accessed in region r1 r2 respectively
     2. neither of the lvals is a lock.
  *)
  let multiply_instantiable r1 r2 lval1 lval2 =
    let r = make_multiply_instantiable_region r1 r2 lval1 lval2 
    in
      if Region.is_empty r then None else Some r

	
	
	
        (* **************** END concurrency **************** *)
	(************* FOR BLASTPCC ************************)
	(* for m4 -- include blast-pcc.ml *)
	include(blast-pcc.ml)


  (************************************ MAP functions **************************************) 





  let get_region_bdd reg =
    match reg with
	Region.Empty -> bddZero
      | Region.Atomic {Region.data = Region.Abstract adr} -> adr.Region.bdd
      | _ -> failwith "Bad region to get_region_bdd"

	  
  type spec = string * (CaddieBdd.bdd * CaddieBdd.bdd) list * CaddieBdd.bdd

  let replace = ref None

  let init_spec () =
    (* First, designate the current predicates as relative to "preconditions" and create a
     * corresponding "postcondition" version of each predicate. *)
    let preds = Hashtbl.fold (fun p n l -> (p, n)::l) hashTableOfPreds [] in
    let prime_lval lv =
      match lv with
	  E.Symbol v ->
	    (try
	       let pos = String.index v '@' in
		 E.Symbol
		   (String.sub v 0 pos ^ "'" ^ String.sub v pos (String.length v - pos))
	     with Not_found -> lv)
	| _ -> lv in
    let prime_pred = P.alpha_convert prime_lval in
      
    let add_post replace (p, n) =
      let p' = prime_pred p in
      let n' = actual_addPred p' in
	(n, n') :: replace in
      replace := Some ((* BDDFIX: CaddieBdd.toMap bddManager*) make_varMap (List.fold_left add_post [] preds))

  module StringSet = Set.Make(struct
				type t = string
				let compare = compare
			      end)

  let build_spec fname summaries =
    let formals = match C_SD.lookup_formals fname with
	C_SD.Fixed args -> args
      | C_SD.Variable args -> args in
    let formals = List.map (function s -> String.sub s 0 (String.index s '@')) formals in
    let allowed = List.fold_right StringSet.add formals
		    (StringSet.add "__retres" StringSet.empty) in
      print_string "ALLOWED: ";
      StringSet.iter (fun s -> print_string s; print_string "; ") allowed;
      print_char '\n';
      let isInternal sym =
	try
	  let idx = String.index sym '@' in
	    not (StringSet.mem (String.sub sym 0 idx) allowed)
	with Not_found -> false in
      let restrictInternal = Region.restrict isInternal in
      let isNonoutput sym =
	try
	  let idx = String.index sym '@' in
	    String.sub sym 0 idx <> "__retres"
	with Not_found -> false in
      let restrictNonoutput = Region.restrict isNonoutput in

      let replace = valOf (!replace) in
	(* This function transforms a "precondition" bdd into the corresponding "postcondition." *) 
      let make_post bdd = CaddieBdd.replace bdd replace in

      (* Now we build a formula describing function behavior per observed entry point *)
      let map_entry (entry, exits) =
	let pre_bdd = get_region_bdd (restrictInternal entry) in
	let post_bdds = List.map (fun r -> make_post
				    (get_region_bdd (restrictNonoutput r))) exits in
	let post_bdd = List.fold_left CaddieBdd.bddOr bddZero post_bdds in
	  ((pre_bdd, post_bdd), CaddieBdd.bddImp pre_bdd post_bdd) in
      let (casePairs, cases) = List.split (List.map map_entry summaries) in
	fprintf "@.Cases:@.";
	List.iter (fun bdd ->
		     (*CaddieBdd.bddPrint bdd;*)
		     P.print Format.std_formatter (convertBddToPred bdd);
		     fprintf "@.") cases;

	(* Finally, a conjunction of the entry point formulae describes all behavior. *)
	(fname, casePairs, List.fold_left CaddieBdd.bddAnd bddOne cases)


  let genTests reg = []

  type spec_result =
    | Yes
    | No
    | Maybe of P.predicate

  let check_spec (f1, c1, s1) (f2, c2, s2) =
    let replace_lval lv =
      match lv with
	  E.Symbol x ->
	    (try
	       let idx = String.index x '@' in
		 if String.sub x (idx+1) (String.length x - idx-1) = f1 then
		   E.Symbol (String.sub x 0 (idx+1) ^ f2)
		 else
		   lv
	     with Not_found -> lv)
	|	_ -> lv in
    let preds = Hashtbl.fold (fun p n l -> (p, n)::l) hashTableOfPreds [] in
    let change_pred = P.alpha_convert replace_lval in
      
    let add_changed replace (p, n) =
      let p' = change_pred p in
      let n' = actual_addPred p' in
	(n, n') :: replace in
    let replace = (* BDDFIX CaddieBdd.toMap bddManager*) make_varMap (List.fold_left add_changed [] preds) in

    let s1 = CaddieBdd.replace s1 replace in

    let caseBdd (pre, post) =
      fprintf "@.  Pre: ";
      P.print Format.std_formatter (convertBddToPred pre);
      fprintf "@. Post: ";
      P.print Format.std_formatter (convertBddToPred post);
      fprintf "@.Known: ";
      P.print Format.std_formatter (convertBddToPred s1);
      let bdd = CaddieBdd.bddImp (CaddieBdd.bddAnd pre s1) post in
	fprintf "@. Case: ";
	P.print Format.std_formatter (convertBddToPred bdd);
	fprintf "@.";
	bdd in
    let caseBdds = List.map caseBdd c2 in
    let casesBdd = List.fold_left CaddieBdd.bddAnd bddOne caseBdds in

    let pred = convertBddToPred casesBdd in
      fprintf "All cases: ";
      P.print Format.std_formatter pred;
      fprintf "@.";

      block_reset ();
      block_assert pred;
      if TheoremProver._is_contra () then
	(block_reset ();
	 No)
      else
	(block_reset ();
	 block_assert (P.negate pred);
	 if TheoremProver._is_contra () then
	   (block_reset ();
	    Yes)
	 else
	   (block_reset ();
	    Maybe pred))

  (* Extended TAR Stuff -- Not implemented *)
  (**********************************************************************************************)
  (**********************************************************************************************)
  (* The environment automaton model in the TAR algorithm *)
  (**********************************************************************************************)
  (**********************************************************************************************)
  type opKind = ProgramOperation of Operation.t | AutomatonOperation of (int * int) 

  module EnvAutomaton =
  struct
    type onode_label =
	{
	  oid : int;
	  oadr : Region.abstract_data_region;
	  oregion : Region.t
	}
	  (* Invariants: Node 0 is always the stuck node *)      
	  
    type anode_label =
	{ id   : int ; (* the id of the abstract automaton node *)
	  root : int ; (* the id of the root node in the cluster *)
	  mutable adr  : Region.abstract_data_region ; (* the abstract data region (is this on globals?) *)
          mutable atomic : bool ;
	  mutable task : bool ;
	  mutable event : bool ;
	  mutable modifies : E.lval list ; (* the list of globals that are modified by this transition *)

	}	
	  
    type aedge_label = 
	Thread of E.lval list              (* Solid edge *) 
      | Env                 (* Dotted edge *)
	  
    type oautomaton_node = (onode_label, opKind) BiDirectionalLabeledGraph.node
  	
    type automaton_node = ((* node label *) anode_label, (* edge label *) aedge_label) BiDirectionalLabeledGraph.node 
	
    type  ('a, 'b) automaton = {
      
      autom_root : automaton_node ;
      number_of_states : int ;
      
      id_to_node_ht : (int, automaton_node) Hashtbl.t ;
      
      o_auto_root : oautomaton_node;
      cover_set : int Region.coverers;
      (* other maps *)
      o_map : (int, oautomaton_node) Hashtbl.t;  (* auto_map *)
      o_to_a_map : (int, int) Hashtbl.t;         (* auto_shrink_map *)
      a_to_o_map : (int, int list) Hashtbl.t;    (* shrink_elts_map *)
      (*  
	  id_to_bdd : (CaddieBdd.bdd) array ; (* map from state id to bdd encoding *)
	  mutable stVarMap : (int * int) list ;     (* maps between the unprimed (s) and primed (t) *)
	  mutable tsVarMap : (int * int) list ;     (* state variables *)

	  mutable env_transrel    : CaddieBdd.bdd ;
      (* Not required at the moment 
	  mutable thread_transrel : CaddieBdd.bdd ;
      *)
      *)  
      
    } 

    let getNodeFromId autom i = (* raises Not_found *)
      Hashtbl.find autom.id_to_node_ht i

    let getEnvEdges autom i =
      let thisnode = getNodeFromId autom i in
      let outgoing = BiDirectionalLabeledGraph.get_out_edges thisnode in
	List.filter (fun e -> match (BiDirectionalLabeledGraph.get_edge_label e) with Env -> true | Thread _ -> false) outgoing

    let getThreadEdges autom i =
      let thisnode = getNodeFromId autom i in
      let outgoing = BiDirectionalLabeledGraph.get_out_edges thisnode in
	List.filter (fun e -> match (BiDirectionalLabeledGraph.get_edge_label e) with Env -> false | Thread _ -> true) outgoing
	
  end 

  (**********************************************************************************************)
  (**********************************************************************************************)
  (* End of The environment automaton model in the TAR algorithm *)
  (**********************************************************************************************)
  (**********************************************************************************************)

  let tar_enabled_ops _ = failwith "tar_enabled_ops unimplemented"      
  let tarpost reg op automaton = failwith "tarpost not implemented"
  let exists_stuck reg = failwith "exists stuck not implemented"
  let get_modifies autom reg = failwith "get_modifies not implemented"
  let create_parallel_atomic_region autom reg ctr = failwith "Not implemented"
      

  let parallel_block_analyze_trace autom a b c = ()

  (* random things put in so that the typechecker doesnt complain *)

  (* focimodelchecker *)
  let foci_post (reg,orig_lvmap) op = failwith "unimplemented foci_post"
  let pred_to_data_region pred = Region.Concrete {Region.pred = pred}
  let fociModelChecker_refine_trace reg_array op_array block_array lvmap_array = 
    failwith "unimplemented: fmc refine_trace"
  let ti_init_region _ = failwith "unimp:cf:ti_init"
end
