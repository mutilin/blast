(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
(** Module for simplifying formula based on a set of known (non-)equalities,
    which can be extracted from a predicate which is assumed to be true.
    Contains in addition a simplification function that performs elementary
    simplifications *)
module Constraint_Simplifier :
sig
  
  type e = Ast.Expression.t

  (** Type to represent a set of known facts (facts that are
      assumed to be true *)
  type knowledge

  (** Type to represent a fact *)
  type fact =
      Eq of (e * e)
    | Ne of (e * e)

  (** Answer given when querying a knowledge *)
  type answ = Known_Eq | Known_Ne | Unknown

  (** Create a new, fresh set of known facts *)
  val new_knowledge : unit -> knowledge

  (** Add the provided facts to the set of known facts *)
  val learn : knowledge -> fact list -> unit

  (** Give a string representation of a set of known facts *)
  val string_of_knowledge : knowledge -> string

  (** Query the set of known facts to know what we know about two values *)
  val what_about : knowledge -> e -> e -> answ

  (** Remove implications that can be deduced from known facts *)
  val solve_facts : knowledge -> Ast.Predicate.predicate -> Ast.Predicate.predicate

  (** Return the list of facts that are implied by the given prediate *)
  val extract_facts : Ast.Predicate.predicate -> fact list

  (** Simplify a formula by extracting predicates, and then removing useless implications *)
  val extract_and_solve : Ast.Predicate.predicate -> Ast.Predicate.predicate

  (** Simplify a formula *)
  val simple_prune : Ast.Predicate.predicate -> Ast.Predicate.predicate

end
