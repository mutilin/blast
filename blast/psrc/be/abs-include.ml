(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)


open LvalUtils
open ConstraintUtil

(*********************************************************************************************************)
(***************************** SKIP FUN Code -- skipping useless functions********************************)
(*********************************************************************************************************)
(* NOTE: some of this code is in PredTable -- where we store what lvals are relevant etc. *)

(* if fname is a skipped function, then returns the original function skipped, otherwise None *)    
  let unskip fname =
    try
      Some(Misc.chop_after_prefix __SKIPFunctionName fname) 
    with _ -> None
      
  let skip_fun_process csd_edge =
  assert (O.getValueOfBool "skipfun");
    let subst_fname fcexp fname =
      match fcexp with
	  E.FunctionCall(E.Lval (E.Symbol(_)), elist)
	  -> E.FunctionCall(E.Lval (E.Symbol(fname)), elist)
	| E.Assignment (E.Assign,target, E.FunctionCall(E.Lval (E.Symbol(_)), elist)) ->
	    E.Assignment (E.Assign,target, E.FunctionCall(E.Lval (E.Symbol(fname)), elist)) in
    let cmd = C_SD.get_command csd_edge in
      match cmd.C_SD.Command.code with
       	  C_SD.Command.FunctionCall fcallExp ->
	    begin
	      let (fname,args) =
		match fcallExp with
		    E.FunctionCall(E.Lval (E.Symbol(fn)), elist) -> (fn,elist)
		| E.Assignment (E.Assign,target, E.FunctionCall(E.Lval (E.Symbol(fn)), elist)) -> (fn,elist)
		| _ -> if (O.getValueOfBool "nofp") then (__NotImplementedFunctionName, [])
		       else failwith ("Strange fcallexp!"^(E.toString fcallExp)) in
	    let skip = 
	      if (PredTable.always_take fname) then false 
              else if (PredTable.always_skip fname) then true 
              else
		  (* longer case, when we have to check if the "effects" at this callsite are relevant *)
		  (* TBD *)
		  false
		    (*
		      TBD:FULL-BLOWN-SKIP
		      let actuals = List.map snd _subs in
	      	      let globs_mod = C_SD.global_lvals_modified fname in 
		      let influences = targ :: globs_mod in
		      List.exists PredTable.is_relevant_lval influences
			*)
	    in
	      if skip then
		begin
		  M.msg_string M.Normal ("Skipping function: "^fname);
		  let new_fname = __SKIPFunctionName^fname in
		  let new_fcexp = subst_fname fcallExp new_fname in
		  let new_cmd = {cmd with C_SD.Command.code = C_SD.Command.FunctionCall new_fcexp} in
                  let src = C_SD.get_source csd_edge in
                  let dst = C_SD.get_target csd_edge in
		    C_SD.make_edge src dst new_cmd
		end
	      else csd_edge
	  end
      | _ -> csd_edge

	  
(* This file has various functions used by abstraction/cfAbstraction,
   most of the functions (as of now) pertain to refinement *)

  
(***********************************************************************************)
(***********************************************************************************)
(*****************     Interpolation based predicate discovery    ******************)
(***********************************************************************************)
(***********************************************************************************)

(**
 * This "module" implements the stuff done for learning predicates by interpolating
 * The main function craig_preds: trace -> list of preds for now
 * Actually we should return a list of locations where the preds should be used
 * but first we shall just return the list of predicates
 *) 

(* the general plan:
   1. set up arrays, reg_array, op_array 
   2. construct constraints using block_concrete_data_pre, put in array
   3. create Atom -> block map 
   4. find interesting block sets -- i.e. those which are unsatisfiable, corresponding to 
      unsatisfiable trace fragments (type: int list list)
   5. List.iter block_refine (4.) ...
   6. return the min of the numbers seen in (4.) as the point from which to refine.
*)



let pred_block_table = Hashtbl.create 109

let foci_pred_table = Hashtbl.create 31


let print_lp_table () = 
  let print_loc l = 
      M.msg_string M.Debug (Printf.sprintf "Location (%d,%d)" (fst l) (snd l));
    let p_list = Hashtbl.find PredTable.loc_pred_table l in
      List.iter (fun p -> M.msg_string M.Debug (string_of_int p)) p_list
  in
  let print_fun f = 
    M.msg_string M.Debug (Printf.sprintf "Function (%d)" f);
    let p_list = Hashtbl.find fun_pred_table f in
      List.iter (fun p -> M.msg_string M.Debug (string_of_int p)) p_list
  in
    List.iter print_loc (Misc.hashtbl_keys loc_pred_table);
    List.iter print_fun (Misc.hashtbl_keys fun_pred_table)
    



(* funky stuff you can do with functional programming! *)
let next_pos dir x = x + dir

let toggle_next_pos npf =
  let dir = npf 0 in
    next_pos (-dir)

let next_back = next_pos (-1) 
(* end funky stuff *)

(* incremental DP interface for refinement *)
let (block_assert,block_pop,block_reset,block_check_unsat,block_is_contra) = 
  (* foci_inc is very flaky 
   *  if (O.getValueOfString "incdp" = "foci") then 
    (TheoremProver.foci_inc_push, 
     TheoremProver.foci_inc_pop, 
     TheoremProver.foci_inc_reset, 
     TheoremProver.foci_inc_check_unsat,
     TheoremProver.foci_inc_is_contra)
  else *)
    (TheoremProver.block_assert,
     TheoremProver.block_pop,
     TheoremProver.block_reset,
     TheoremProver.check_unsat,
     TheoremProver._is_contra)
(* end incremental DP interface for refinement *)

	   (* this returns an atomic_region *)
let insert_pred_in_reg reg pred =
  match reg with
      Region.Atomic atr ->
	
	{ Region.location = atr.Region.location;
	  Region.data = Region.Concrete {Region.pred = pred};
	  Region.stack = atr.Region.stack;
	}
    | _ -> failwith "bad argument to insert_pred_in_reg!"



let min_reg_id = ref (-1) 
let min_adr = ref None 
  (* this is used by predH 7 *)
let dom_rel l1 l2 = 
  (* true if l1 > l2, false otherwise *)
  (* l1 > l2 iff blocks go like that (in merged and sorted list):  L1 L1 L1 L2 L2 L1 L1 *)
  (* i.e. all L2's lay between two elements of L1 *)
  (* no elements in l1 and l2 coincide by construction *)
  if l2 = [] then true
  else
    let (sorted_l1,sorted_l2) = (List.sort compare l1,List.sort compare l2) in
    let (min,max) = (List.hd sorted_l2, Misc.list_last sorted_l2)  in
    let rec dom_check l =
      match l with
	  h1::h2::tl -> if (h1 < min && max < h2) then true else dom_check (h2::tl)
	| [h] -> false
	| [] -> false
    in
      dom_check l1

let lv_temp_table = Hashtbl.create 109 

		      

let extract_foci_preds c = 
  if (O.getValueOfInt "craig" <= 2) then Absutil.extract_foci_preds c 
  else Absutil.extract_foci_preds_parity c 

let list_max l =
  List.fold_left (fun a b -> if (a > b) then a else b) 0 l




(*******************************************************************************************************)
(******************Vanilla "proof-based" refinement, not using interpolants ***************************)
(******************************************************************************************************)
       
let focus_refine extract_block conj_block_array reg_array (block_l : int list) = 
    (* essentially extract the predicates appearing in those blocks *)
    let _ = PredTable.block_a_t_flag := true in
    let head = Misc.min block_l in
    let head_reg = reg_array.(head) in
    let abst_data_reg = extract_abs_data_reg (head_reg) in
    let trace_pred_l = List.map (fun x -> extract_block x) block_l  in
    let all_trace_pred_l = (conj_block_array.(head))::trace_pred_l in
    let t_empty = block_check_unsat (P.And trace_pred_l) in
    let _ = block_reset () in    
      begin
	let old_pred_list_len = List.length (abst_data_reg.Region.preds) in 
	let new_adr = 
      do_focus abst_data_reg { Region.pred = 
				 match t_empty with
				     true -> P.And trace_pred_l
					 | false -> P.And (all_trace_pred_l)
			     }
        in
        let new_pred_list_len = List.length (new_adr.Region.preds) in
        if (new_pred_list_len >= old_pred_list_len) then PredTable.block_a_t_flag := true;
        if (head <= !min_reg_id || !min_reg_id = -1) then
          begin
	    min_reg_id := head;
	    Array.set reg_array head (insert_data_reg head_reg (Region.Abstract new_adr)) 
          end;
        ()
      end
  

(***************************************************************************************)
(****************GET USEFUL BLOCKS i.e. inconsistent sets of instructions **************)
(***************************************************************************************)
	  
(*** 1. Exposing functions, not clear that this is really required! *)
let suppress_functions op_array block_array =
  let len_op_l = Array.length op_array in
  let suppress_array = Array.make len_op_l false in (* used to "drop assumes" *)
  let extract_block i = 
    match suppress_array.(i) with
	true -> P.True
      | false -> block_array.(i) in
  let drop_constraint i = 
    let is_assume_constraint i = 
      match (C_SD.get_command (op_array.(i))).C_SD.Command.code with C_SD.Command.Pred _ -> true 
      | _ -> false in
    if is_assume_constraint i then Array.set suppress_array i true in
  let restore_constraint i = Array.set suppress_array i false in 
  let restore_all_constraints () = 
    Array.iteri (fun i a -> Array.set suppress_array i false) suppress_array  in
    (suppress_array, extract_block, drop_constraint, restore_constraint, restore_all_constraints)

(* 2. the main function, that uses the above *)
let print_op (x : Operation.t) =
  let (l1,l2,rv) = op_to_string x in
  M.msg_string M.Debug (Printf.sprintf "%d :: %s :: %d \n\n" l1 rv l2)

exception BisectionFailed
exception NonMonotonic
exception OutOfRange

(* The arrays provided contain operations, postconditions and preconditions (regions).  In (predH < 7) mode, the two latter are not complete (see cons folder), only their last cells (starting from trace_end to the end of arrays) are filled with non-true constraints *)
let get_useful_blocks_inc op_array block_array conj_block_array (Some trace_end) =
  let len_op_l = Array.length op_array in
  let _ = M.msg_string M.Normal ("get_useful_blocks: size ="^(string_of_int len_op_l)) in
  let _ = Array.iter print_op op_array in
    (* end_cons is needed by FOCIMODELCHECKER to check for coverability *)
    (* SKY: otherwise it's true *)
  let end_cons =
   if (O.getValueOfBool "fmcc")
   then conj_block_array.((Array.length conj_block_array) - 1)
   else P.True in
  (* some kind of set of switches whether we `turn off' blocks *)
  let suppress_stuff = suppress_functions op_array block_array in
  if len_op_l = 0 then (-1,[],suppress_stuff) else
  let (suppress_array,  (*SKY an array that contains whether we suppress i-th block (i-th in sence of below) *)
       extract_block,   (* get block for i-th element of that arr unless it's set to true *)
       drop_constraint, (* set i-th to true if its block is a predicate *)
       restore_constraint, (* set i-th (for *all - set all to - false *)
       restore_all_constraints) =  suppress_stuff in
  let foci_check_unsat is =
  (* called with is = list of useful blocks if TheoremProver returns `false' interpolant  *)
   let head = Misc.min is in
   (* gets list of conj_block for the first interestingblock and conjuncts it with all interesting blocks *)
   let q = P.And ((conj_block_array.(head)) :: (List.map extract_block is)) in
   let unsat_set = try TheoremProver.interpolant_unsat q
     with e -> let _ = M.msg_string M.Normal "Testing if useful blocks are contrad" in
               let _ = List.iter (M.msg_printer M.Normal P.print) ((conj_block_array.(head)) :: (List.map extract_block is)) in
               raise e
   in
   if unsat_set then is else [] in
  let check_block_unsat block_l =
  (* called with the list of blocks, ordered by number ascending - first called - first in list *)
    (* this function is used as a sanity check *)
    (* SKY: YES, it's  JUST a sanity check.  Nothing more *)
    let _ = M.msg_string M.Debug "Checking block unsat" in
    let head = Misc.min block_l in
    let trace_pred_l = List.map extract_block block_l  in
    let all_trace_pred_l = (conj_block_array.(head))::trace_pred_l in
    let _ = block_reset () in
    let all_together = 
      let t_empty = block_check_unsat (P.And trace_pred_l) in
      let all_empty = block_check_unsat (P.And all_trace_pred_l) in 
      (t_empty || all_empty) in
    let one_by_one = 
      block_reset (); block_assert (conj_block_array.(head));
      List.iter (fun i -> block_assert (extract_block i)) block_l;
      block_is_contra () in
    block_reset ();
    if one_by_one && not (all_together) then M.msg_string M.Debug "simplify inconsistency!";
    if (all_together) then M.msg_string M.Normal "blockset unsat as expected"
    else M.msg_string M.Normal "WARNING: blockset SATISFIABLE!!"; all_together in
  
  let (ref_start_pos, interesting_block_sets) = 
    begin (* find_interesting_blocks *)
      (* Current ewrfiqo *)
      let pos = ref (len_op_l - 1) in
      (* procedure toget useful blocks *)
      (* To get "useful blocks" means to select only those blocks from path formula, which affect interpolation results.  The result is several sets of blocks,  each of which is an unsat conjunction of blocks. *)
      let rec getUsefulBlocks mustSet dir =
	match mustSet with 
	 [] -> (
		(* Traverse from end of the trace to the beginning until we find that conjunciton of "precondition" region and the subsequent postcondition expressions is UNSAT *)
	        pos := len_op_l - 1;
		block_assert (end_cons);
		(* first we assert the block `as is' *)
		block_assert (conj_block_array.(!pos));
		(* block_is_contra is _is_contra proc from fe/theoremProver *)
		(* it checks whether current context is contradictory *)
		(* the context is conjunction of all blocks so far, starting from the last  *)
		let contr = ref false in
		while (not ( !contr )  && !pos >= 0) do
		  M.msg_string M.Debug ("useful-block pos = "^(string_of_int !pos));
		  (* instead of a block, assert every other block and check context *)
		  (* pop pops the conj block from previous iteration *)
		  block_pop ();
		  block_assert (extract_block !pos);
		  (* push also the predicate *)
		  (try block_assert conj_block_array.(!pos) with e ->
                    print_string "crash here"; raise e);
		  (* dir is a function that shifts  !pos.  Why's it needed?  loop condition states it's to decrease, and no sense to decrease for a quantity other than one *)
		  pos := dir (!pos);
		  contr := block_is_contra ()
		done;
                M.msg_string M.Normal "done loop";
		(* exit if the path is not contradictory (i.e. we've checked the whole trace) *)
                if (not (!contr)) then (block_reset (); [])
		else
		    let rev_dir = toggle_next_pos dir in
		                                        (* is always empty string *)
		    let dir' = match O.getValueOfString "tsd" with "zig" -> (rev_dir) | _ -> (next_back) in
		      (block_reset ();
		      M.msg_string M.Normal ("adding useful block: "^(string_of_int (rev_dir !pos)));
		      (* so, the pos+1 is the highest block number, after what the chain's contradictory *)
		      (* recursively call useful blocks with [last_pushed_block] (dir+1) *)
		      (* (because mustSet is []) *)
		      getUsefulBlocks ((rev_dir !pos)::mustSet) dir')) 
	| h::t -> 
		(* h is the last block (rev_dir !pos) just descibed there ^^^^^^^^^^ *)
		(* in this part of match we again traverse the trace from its end.  The thing it that we know that the conjunction of blocks from "h" to the end of the trace is UNSAT.  We're to select only those blocks which are enough to make it unsat. *)
		(* *)
	       ((* report iteration number *)
		M.msg_string M.Debug ("useful-block pos = "^(string_of_int !pos));
	        (* assert some foci-stuff, "true" in simplify's case *)
                block_assert (end_cons);
		(* head is the last `useful' block found so far *)
		let head = Misc.list_last (h::t) in
		(* push the conj block -- "precondition" region predicate of the first useful block *)
		block_assert (conj_block_array.(head));
		(* push interesting blocks recognized so far *)
		List.iter (fun i -> block_assert (extract_block i)) mustSet;
		(* if these `interesting blocks' form contradictory set, then return them *)
		if (block_is_contra ()) then (block_reset (); mustSet)
		(* otherwise -- do the same as in the `switch' above but startig from block previous to the lowest in mustSet so far *)
		else
		  (let dir' = match O.getValueOfString "tsd" with "zig" -> (pos := dir h; toggle_next_pos dir) 
			      | _ -> (pos := len_op_l - 1;next_back) in
                   while (not (block_is_contra ()) && !pos >= 0) do
                     M.msg_string M.Debug  ("Now asserting block: "^(string_of_int !pos));
                     block_assert (extract_block !pos);
                     pos := dir !pos
                   done; 
		   assert (block_is_contra ()); 
		   (* if assertion checking was on this fails if !pos = 0 *)
		   if (!pos < 0) then failwith "Error in getUsefulBlocks ... Check Simplify behaviour ...";
		   let rev_dir = toggle_next_pos dir in
		     block_reset ();
		     M.msg_string M.Normal ("adding useful block: "^(string_of_int (rev_dir !pos)));
	             getUsefulBlocks ((rev_dir !pos)::mustSet) dir'))
      in
      (* Useful-block algorithm for SMT solvers.
         Given an error trace, with pre- and postconditions, detect a set of useful blocks.
	 Block set is useful if:
	   - it's a subset of the trace
	   - conjunction of all blocks (with precondition (region) of the first one) is UNSAT
	   - it's a minimal set

         Due to the nature of these solvers, pushing predicates one-by-one and performing a contradiction check until we encounter an UNSAT conjunction is suboptimal.  When the trace has, say, a thousand of blocks, and 500th and 501th are "contradictory", it will perform about 1000 calls to SMT solvers (500 calls to find the first one, and the other 500 to find the 2nd one).

	 Instead, we use binary search.  In case of a false error, conjunction of zero blocks from the trace is SAT, but conjunction of the whole trace is UNSAT.  The satisfiability of the trace conjunction is a monotonic function, and it can be bisected.  The algorithm implemented here is basically a bisection (see "bisect") of a trace satisfiablilty function (see "target_function").
       *)
      (***** CACHE *)
      (* Cache key for postcondition of i-th predicate (cons) *)
      let postkey i = Printf.sprintf "post_%d" i in
      (* Cache key for precondition of i-th predicate (conj) *)
      let prekey i = Printf.sprintf "pre_%d" i in
      (* Cache key for i-th block if it's useful*)
      let ubkey i = Printf.sprintf "ub_%d" i in
      (* Cache assertion procedure *)
      let block_assert_cache = TheoremProver.block_assert_cache in
      let block_reset_keep_cache = TheoremProver.block_reset_keep_cache in

      (* We ignore "direction" attribute because it is not generic enough, and is not utilized anyway *)
      (* Blocks should be reset after this! *)
      let gub_smt a _ =
	(* Given that f is a monotonic growing function that takes ane argument, and has values equal to 0 and 1 only, find the largest i between st and en, such that (f i) is 0.  If there's no such i, throw a BisectionFailed exception.  If f is detected to be non-monotonic, a NonMonotonic exception is thrown.  *)
	(* (fst, fen) are values of (f st, f en).  Sometimes they're already computed *)
        let rec bisect f st en (fst, fen) =
	  let get_mid a b =
	    let (fa,fb) = Misc.map2 float_of_int (a,b) in
	    int_of_float ((fa +. fb) /. 2.0)
	  in
	  M.msg_string M.Debug (Printf.sprintf "Bisection step (%d %d)" st en);
	  if (st > en) then raise BisectionFailed else
	  let fst = match fst with None -> f st | Some a -> a in
	  let fen = match fen with None -> f en | Some a -> a in
	  (* An exuberant, but error-prone implementation of bisection step *)
	  match (fst, fen) with
	     (0,0) -> en
	    |(0,1) when (st+1>=en) -> st
	    |(0,1) ->(let mid = get_mid st en in
	              let fmid = f mid in
		      match fmid with
			 0 -> bisect f mid en (Some fmid, Some fen)
			|1 -> bisect f st mid (Some fst, Some fmid)
		     )
	    |(1,0) -> raise NonMonotonic
	    |(1,1) -> raise BisectionFailed
	    |(_,_) -> raise OutOfRange

	in
	let pr_int i = M.msg_string M.Debug (string_of_int i) in
        M.msg_string M.Debug "Chose SMT useful blocks algorithm";
	(* function we try to bisect *)
	(* Assumes blocks in the "assume_some" array, and uses trace access functions, thinking that end of trace is at length, and start of the trace of interest is at n.
	   when end of trace is beyond the length, assume only the assume_some blocks, if any, and precondition (region) of first of them, if any
	 *)
	let target_function assume_some length n =
	  let dpr s i = M.msg_string M.Debug (s^(string_of_int i)) in
	  M.msg_string M.Debug (Printf.sprintf "calling target_function with n=%d and length=%d" n length);
	  block_reset_keep_cache ();

	  (* Assert "precondition" region of the very first assumed block--it's either the minimal from assume_some, or the n-th one *)
	  let very_first_block = List.hd ((List.rev (assume_some))@[n]) in
	  if not (very_first_block > length && n>length) then begin
	    (* we skip this if the block we assert is out of bound AND if it's a special mode when the last trace block to assume is beyond the bound *)
	    M.msg_string M.Debug (Printf.sprintf "Region of the first block %d:" very_first_block);
	    block_assert_cache (prekey very_first_block) (conj_block_array.(very_first_block))
	  end;

	  M.msg_string M.Debug (Printf.sprintf "Blocks fetched so far:");
	  List.iter (fun i -> block_assert_cache (ubkey i) (extract_block i)) assume_some;

	  (* Add end_cons *)
	  M.msg_string M.Debug (Printf.sprintf "Adding end_cons:");
	  block_assert (end_cons);

	  (* Add all blocks in the trace *)
	  M.msg_string M.Debug (Printf.sprintf "Asserting trace up to %d:" n);
	  let rec add_blk_until n = function
	      i when i<n      -> (* special case, do not assert any trace blocks *) ()
	    | i when i=n      -> block_assert_cache (postkey i) (extract_block i)
	    | i               -> let _ = block_assert_cache (postkey i) (extract_block i) in add_blk_until n (i-1)
	  in
	  add_blk_until n length;

	  (* Check the asserted blocks for contradiction *)
	  let rslt = match block_is_contra () with true (*unsat*) -> 0 | false (*sat*) -> 1 in
	  M.msg_string M.Debug (Printf.sprintf "target_function %d (%d) returns %d\n" n length rslt);
	  rslt
	in
	let rec gub_smt_rec useful_blocks_so_far =
	  (* some useful blocks found, head of the list denotes the bound up to which the next block should be searched *)
	  try
	    let len = len_op_l in
	    let top = match useful_blocks_so_far with [] -> trace_end | h::t -> h in
	    (* top stores the index of the last block, we don't need to assert it (it's in "so far" list), hence (top+1) *)
	    (* top may be equal to end of the trace.  If it's the case, the bisection will immediately throw BisectionFailed Exception, and we'll exit *)
	    (* But before we start bisection, we should check if the set of useful blocks so far is already UNSAT.  To do it, we just calculate and check initial bisection value for one end of the trace.
	       (That's not just a performance optimization, not doing this will result in an excessive "useful" block.  If blockset is unsat, then the target function is zero at both ends of the interval, which makes bisection think that the end is the answer).
	     *)
	    let b_st = top + 1 in
	    let b_en = len in (* b_en is off-by-one: it's beyond the trace end, and it means that no trace blocks should be assumed *)
	    let last_in_trace = len-1 in
	    let tf = target_function useful_blocks_so_far last_in_trace in
	    let blockset_sat = tf b_en in
	    match blockset_sat with
	       0 (*unsat*) -> (* blockset found, exit *) raise BisectionFailed 
	      |_ ->( let next_useful_block = bisect tf b_st b_en (None,Some blockset_sat) in
	             M.msg_string M.Debug (Printf.sprintf "New block is %d." next_useful_block);
	             gub_smt_rec (next_useful_block::useful_blocks_so_far)
		   )
	  with
	    BisectionFailed -> (block_reset_keep_cache(); useful_blocks_so_far)
	in
	gub_smt_rec a
      in
      (* in SMT solver we use caching of blocks, and we need to invalidate useful-blocks that are removed from trace *)
      let invalidate_block i =
        let tp = O.getValueOfString "incdp" in
	if (tp = "smt") then ( SMTLIBInterface.invalidate (ubkey i) ;
	                       SMTLIBInterface.invalidate (postkey i)
			     )
	                else ()
      in
      (* strange: always called with a = [] *)
      let gub a b =
        M.msg_string M.Debug "calling gub";
	let getUsefulBlocks = if (O.getValueOfString "s" = "smt") && not(O.getValueOfBool "nosmtgub") then gub_smt else getUsefulBlocks in
        let is = getUsefulBlocks a b in
	(*foci_check_unsat just returns empty list if interpolant is found.  Otherwise reurns list of useful blocks *)
	(* TODO: wtf? *)
        let rv = if is = [] then [] else foci_check_unsat is in
        (* List.iter drop_constraint is;*)
        rv
      in
	(* this seemingly excessive block_reset is necessary to drop cache for SMT useful-blocks *)
	block_reset ();
	(* predH = 6 trawls the trace and drops assumes in the middle to find "better" blocks *)
	(* predH = 7 drops the blocks that are "dominated" *)
      match (O.getValueOfInt "predH" >= 6) with
        true ->
	  begin
	    (* SKY: We start here *)
            let ubl = ref [] in
            let next_useful_blocks = ref [] in
            next_useful_blocks := gub [] next_back;
	    (* Where do we go if we found an interpolant? *)
	    (* if interpolant is found, the next_useful_blocks Array will be empty and the cycle won't be iterated *)
	    (*    then the return of this function will be (-1, []) *)
	    (* the cycle below drops all useful blocks from array of constraints and re-runs getUsefulBlocks with the new set of constrains switches *)
	    (* ubl is the list of lists of useful blocks calculated on each iteration so far *)
            while (not (!next_useful_blocks = []) && (List.length !ubl < O.getValueOfInt "bset")) do
              M.msg_string M.Debug "Another u.b. found";

	      (* next call doesn't do anything useful: just an assertion *)
	      (* We comment it out as it's  a) slow;  b) interferes with cache badly;  c) satisfiable blockset will end up with FociInterface:SAT exception anyway *)
              (*check_block_unsat !next_useful_blocks;*)

              List.iter drop_constraint !next_useful_blocks;
	      (* Invalidate cache (if used) *)
              List.iter invalidate_block !next_useful_blocks;
              ubl := (!next_useful_blocks)::!ubl;
              pos := 0; 
              next_useful_blocks := gub [] next_back
            done;
	    (* Now !ubl is a list of lists of useful blocks *)
            match (!ubl) with [] -> (-1, [])
            | l -> let head = Misc.min (List.flatten l) in
                 let l' = if (O.getValueOfInt "predH" <> 7) then l else  
                     let max_l = Misc.maximal dom_rel l in
                     (M.msg_string M.Debug (Printf.sprintf "Maximal/Orig blocks: %d/%d "(List.length max_l) (List.length l));
                      max_l) in
                 (head, l')
	      
          end
	| false ->
            let useful_blocks = gub [] next_back in
            (match useful_blocks with [] -> M.msg_string M.Debug "No useful blocks found!";(-1, [])
             | l -> let head = Misc.min l in (head, [l]))
      (* if the trace is feasible then ref_start_pos = -1 and ref_start_reg is the 
	 region for the root from which the trace happens *)
    end
  in
    restore_all_constraints ();
    M.msg_string M.Normal "get_useful_blocks done.";
    (ref_start_pos,  interesting_block_sets, suppress_stuff)


let next_block_foci len_op_l end_cons extract_block =
  let rec fld cur (itp',i) itpl = 
    match itpl with [] -> List.rev cur
    | h::t -> let cur' = if h <> itp' || O.getValueOfBool "mccarthy" then i::cur else cur in 
              fld cur' (h,i+1) t in
  let cons_l = end_cons::(List.map extract_block (Misc.range (0,len_op_l,1))) in 
  try
    let blocks = 
      (* TBD: if O.getValueOfBool "fociminp" then 
        TheoremProver.extract_minp_interpolantN 
      else *)
         let itp_l = List.tl (TheoremProver.extract_foci_interpolantN cons_l) in
        fld [] (P.True,0) (itp_l@[P.False]) in 
    match blocks with [] -> failwith "inc_block_foci:unsat but empty blocks!"
    | h::t -> (Misc.min blocks,blocks) 
  with e -> (M.msg_string M.Normal ("Exception: "^(Printexc.to_string e));raise e)

let print_contra_block op_array idx b_l =
  let proc i =
    let op_i = op_array.(i) in
    let (l1,l2,rv) = op_to_string op_i in
    M.msg_string M.Normal (Printf.sprintf "[INF%d] %d :: %s :: %d \n" idx l1 rv l2)
  in
  List.iter proc b_l;
  M.msg_string M.Normal (Printf.sprintf "[INF%d] \n\n\n" idx)
  
let get_useful_blocks_foci op_array block_array conj_block_array = 
  let len_op_l = Array.length op_array in
  let _ = M.msg_string M.Normal ("get_useful_blocks_foci: size ="^(string_of_int len_op_l)) in
  let _ = Array.iter print_op op_array in
  let end_cons =
    if (O.getValueOfBool "fmcc") then conj_block_array.((Array.length conj_block_array) - 1)
    else P.True in
  let suppress_stuff = suppress_functions op_array block_array in
  let (suppress_array, extract_block, drop_constraint, restore_constraint, restore_all_constraints) =  
    suppress_stuff in
  if len_op_l = 0 then (-1,[],suppress_stuff)
  else
    begin
     let min_rsp = ref (-1) in
     let ibs_l_ref = ref [] in
     let unsat_ctr = ref 0 in
     let _ = 
      try
        while (true) do
          M.msg_string M.Debug "calling next_block_foci";
          let (rsp,inc_op_l) = next_block_foci len_op_l end_cons extract_block in
          if (!min_rsp = -1) || (rsp < !min_rsp) then min_rsp := rsp;
          ibs_l_ref := inc_op_l :: !ibs_l_ref; 
          List.iter drop_constraint inc_op_l;
          print_contra_block op_array !unsat_ctr inc_op_l 
        done;
      with TheoremProver.FociSatException | TheoremProver.FociSatExceptionAsgn _ -> () in
     restore_all_constraints();
     (!min_rsp,!ibs_l_ref,suppress_stuff) 
    end

let get_useful_blocks a b c d = 
  if Options.getValueOfString "ubdp" = "foci" then get_useful_blocks_foci a b c
  else get_useful_blocks_inc a b c d
 
(*********************************************************************************)
(********************** TRACE PROJECTION *****************************************)
(*********************************************************************************)

(* We attempt to find a subset of the operations of the trace that actually matter 
 * in some way. Clearly, if the resulting TF clauses are UNSAT, then the whole TF 
 * is unsat. Dually, we require that if the TF clauses are SAT, then, modulo 
 * termination assumptions, the whole thing is SAT. [Path Slicing : PLDI 05]
 *)

(* Expected use: block_analyze_trace --> to minimize counterexamples *)


exception UnskipException of int
exception RefineFunctionException of string

(* The relevant set of LVALS *)
(* this is used as a global and is an implicit parameter to many of the functions below *)


let error_variable = E.Symbol (O.getValueOfString "errorvar")
let exit_variable = E.Symbol (O.getValueOfString "exitvar")

let rel_field_table : (string,bool) Hashtbl.t = Hashtbl.create 37
				
		  
let rel_variable_table : (E.lval, bool) Hashtbl.t = Hashtbl.create 37
let rel_variable_size = ref 0


let add_rel_fields l = 
  (* INV: see invariant for flush_rel_fields *)
  let l_fields = (E.fields_of_lval l) in
    List.iter (fun fld -> 
		 let added = C_SD.add_field_to_unroll true fld in
                 if added then Hashtbl.replace rel_field_table fld true) l_fields
      (* TBD:FIELDS -- mixing tproj/image relevant fields *) 
 

let add_rel_variable l =
  if (Hashtbl.mem rel_variable_table l) then ()
  else
    begin
      M.msg_string M.Debug
	(Printf.sprintf "Adding relevant lval %s" (E.lvalToString l));
      Hashtbl.replace rel_variable_table l true;
      rel_variable_size := 1 + !rel_variable_size;
      add_rel_fields l
    end
  
let del_rel_variable l =
  if (Hashtbl.mem rel_variable_table l) then 
    begin
      M.msg_string M.Debug
	(Printf.sprintf "Removing relevant lval %s" (E.lvalToString l));
      Hashtbl.remove rel_variable_table l;
      rel_variable_size := !rel_variable_size - 1 
    end

let flush_relevant_locals fname = 
  let del lv _ = 
        let sc = C_SD.scope_of_lval lv in
        if List.mem fname sc then (del_rel_variable lv)
  in
  Hashtbl.iter del rel_variable_table

let rel_variable_reset () =
  Hashtbl.clear rel_variable_table;
  Hashtbl.clear rel_field_table;
  rel_variable_size := 0;
  add_rel_variable error_variable;
  if (O.getValueOfBool "exit") then add_rel_variable exit_variable;
  ()
  (* TBD:EXIT *)


let hit_check_cache = Hashtbl.create 1001
let sub_lval_cache = Hashtbl.create 1001

(* alas, we have to take closure here. 
   take the closure of lv_wr ... for each item, 
   check if it is aliased to lv_rd -- 
   such a fellow is lv_rd' == lv_wr^i s.t. lv_rd' aliases lv_rd *)

(* CHECK: hmm is caching ok in the presence of changing closures ? 
   I believe so because if you cached with lv_rd, 
   then the result computed at that point contains all relevant fields of lv_rd.
   I can construct pathological situations where the aliasing happens via 
   some field NOT in lv_rd in which case you get the wrong
   answer -- but that can happen without caching as well :) *)
let stats_nb_tproj_alias_queries = ref 0
        
let hit_check aliasfn lv_wr lv_rd =
  M.msg_string M.Debug 
        (Printf.sprintf "In Hit check (lv_wr = %s ,lv_rd = %s)"        
        (E.lvalToString lv_wr) (E.lvalToString lv_rd));
  let in_cache = Hashtbl.mem hit_check_cache (lv_wr,lv_rd) in
  if (in_cache) then Hashtbl.find hit_check_cache (lv_wr,lv_rd)
  else
    begin
      let clos = C_SD.lvalue_closure_stamp true lv_wr in (* caching done inside C_SD *)
      M.msg_string M.Debug (Printf.sprintf "Closure lv_wr = %s"
      (Misc.strList (List.map E.lvalToString clos)));
      let get_res () = 
	try
	  Some
	    ((List.find 
		(fun lvwri -> 
		   let (e1,e2) = ((E.Lval lvwri),(E.Lval lv_rd)) in
		     if e1 = e2 then true else
		       begin 
                               stats_nb_tproj_alias_queries := 1 +
                               !stats_nb_tproj_alias_queries;
                               Absutil.queryAlias_cache e1 e2
                       end      
                   ))
	       clos)
	with Not_found ->
          if E.occurs_check (E.Lval lv_wr) (E.Lval lv_rd) 
          then Some lv_rd else None
      in
      let get_res_aliasfn () = (* throwing this back in for time comparisons *)
        let get_aliases () = aliasfn lv_rd in
        let rd_aliases = Stats.time "get aliases aliasfn" get_aliases () in
        (* RJ: this is cached inside aliasfn -- so we don't cache again *)
        let a_table = Misc.hashtbl_of_list  7 rd_aliases in
        try 
           Some (List.find (Hashtbl.mem a_table) clos)
        with Not_found -> None
      in                
      let res = 
              if (O.getValueOfBool "aiter") then 
                      Stats.time "get res aliasfn" get_res_aliasfn () 
              else    
                      Stats.time "get res" get_res () 
      in
	Hashtbl.replace hit_check_cache (lv_wr,lv_rd) res;
        let lvos lvo = match lvo with None -> "None" | Some lv ->
          E.lvalToString lv in
         M.msg_string M.Debug 
        (Printf.sprintf "Hit check (lv_wr = %s ,lv_rd = %s,res = %s)"        
        (E.lvalToString lv_wr) (E.lvalToString lv_rd) (lvos
        res));
       res
    end

let gen_is_relevant aliasfn l = 
  let _mapper lv_read _ = 
    match hit_check aliasfn l lv_read with
	None -> None
      | Some lv_read' -> Some (lv_read,lv_read')
  in
    Misc.hashtbl_map_partial _mapper rel_variable_table

let max_relevant_size = ref 0

let dump_relevant () =
  let lvs = (Misc.hashtbl_keys rel_variable_table) in
  let slvs = List.map (fun lv -> E.lvals_of_expression (E.Lval
  lv)) lvs
  in 
  let rv = List.flatten slvs in
  if (List.length rv > !max_relevant_size) then
    begin
      max_relevant_size := List.length rv;
      M.msg_string M.Normal ("Max rel-cone size"^(string_of_int
      !max_relevant_size))
    end; 
  rv
        
  
let mods_on_path_table = Hashtbl.create 101
let mods_on_path_reset () = Hashtbl.clear mods_on_path_table


(* when you add a new "relevant" lv means you are taking the op, 
 * which means you are resetting the mods_on_path table. 
 * Hence, you ONLY do the add_and_hit_check when you are 
 * adding a mod_on_path *)

let add_and_hit_check aliasfn mod_lv =
  if Hashtbl.mem mods_on_path_table mod_lv then false
  else
    begin
      Hashtbl.replace mods_on_path_table mod_lv true;
      let find_rel = Stats.time "gen is relevant" (gen_is_relevant aliasfn) mod_lv in 
      (find_rel <> [])
    end	


(* NOTES on proc_write:
   when  you do an assign lv = e,
   then we cal gen_is_relevant to get a list:
   [ ... (lv_rd,lv_rd')...] where lv_rd is "read" in the future and thus  
   is relevant now, and lv_rd' == lv^i (in the closure of lv) such that lv_rd' aliases lv_rd.
   then, corresponding to this "hit", the new "read" value (add) is e^i,
   which is lv^i[e/lv] which is exactly e^i ... which is the new "value" of lv_rd,
   and "lv_rd" is itself irrelevant now (killed) and hence put in del.
   if lv_rd' == lv_rd then we can definitely delete lv_rd from the relevant set, 
   and replace with e^i otherwise not. We must do the above for all lv' that are 
   must aliases of lv (including lv)
*)
		       
let rec process_asgn_block aliasfn fname a_block add_c del_c take =
  (* first, lets put add_c/del_c in the table etc -- so we dont have to fuss with them when checking relevant etc. *)
  let aliasfn_f = aliasfn fname in    
  let _ = 
  List.iter del_rel_variable del_c; 
    List.iter add_rel_variable add_c 
in      
  let proc_write (lv,e) =
    (* returns a 4-tuple. (must_add, must_del, may_add,may_del) *)
    let (must_add,must_del,may_add,may_del) = 
      (ref [],ref [],ref [],ref []) 
    in
    let must_aliases_lv = lv::(AA.get_all_must_aliases lv) in    
    let get_hit_list () = 
      Misc.map_partial 
	(fun lv' -> 
	   let lv'_hits = gen_is_relevant aliasfn_f lv' in
	     if lv'_hits = [] then None else Some (lv',lv'_hits))
	must_aliases_lv
    in
    let hit_list = Stats.time "get hit list" get_hit_list () in
    let proc_hit lv' (lv_rd,lv_rd') = 
      let spairs = [((E.Lval lv'),e)] in
      let update al_ref dl_ref =
	let subs_lvals = 
	  E.lvals_of_expression 
	    (E.substituteExpression spairs (E.Lval lv_rd')) 
	    (* we compute e^i by subst. lv_rd' == lv' ^i using [e/lv'] *) 
	in
        M.msg_string M.Debug (Printf.sprintf "Subs (lv' , e) in lv_rd' : %s , %s, %s " 
        (E.lvalToString lv') (E.toString e)
        (E.lvalToString lv_rd'));
        M.msg_string M.Debug (Printf.sprintf "subs_lvals: %s"
        (Misc.strList (List.map E.lvalToString subs_lvals)));
	  al_ref :=  subs_lvals @ !al_ref;
	  dl_ref := lv_rd :: !dl_ref
      in
	(* INV: its a MUST hit if lv_rd == lv_rd', o.w. its a may hit *)
	if (lv_rd' = lv_rd) then
	  update must_add must_del
	else update may_add may_del
    in
    let process_all_hits () = 
      List.iter (fun (lv',hits) -> List.iter (proc_hit lv') hits) hit_list
    in
    let _ = Stats.time "process all hits" process_all_hits () in
      (!must_add,!must_del,!may_add,!may_del)
  in
    match a_block with
	(target,value)::tl ->
	  begin
	    let _ = M.msg_string M.Debug
		      (Printf.sprintf "Hit assign %s := %s"
			 (E.lvalToString target)
			 (E.toString value))
	    in
	    let (tadd,tdel,alias_add,alias_del) = 
	      Stats.time "proc write targ" proc_write (target,value) 
	    in
	      (* (alias_del) = [] empty in the worst case, use it only to see if this op was relevant,ie to compute "take" *)
	    let read = Misc.sort_and_compact (tadd @ alias_add) in
	    let written = (target::tdel) in
	    let take' = ((tdel@alias_del) <> []) in 
	      (* take' == true IFF some update made was relevant. if some update was relevant,
		then tdel@alias_del <> [], as every relevant update sends something into the del bin *)
	      (* OLD CODE: let written = [target] (* TBD:SOUNDNESS *) in
	            let read = E.allVarExps_deep value in
	         TBD:TPROJ -- do we really need allVarExps_deep ? *)
	    let (add_c',del_c') =
	      if take' then
		(Misc.union (Misc.difference add_c  written)  read,
		 Misc.difference (Misc.union del_c  written)  read)
	      else
		(add_c,del_c)
	    in
	      process_asgn_block aliasfn fname tl add_c' del_c' (take || take')
	  end
      | [] -> ((add_c,del_c),take)


	        
(* unsat_flag == true means we just want to gather the "residual" funcalls *)	  
let process_trace_op unsat_flag aliasfn op_array left last (i : int) =
  (* ABC INV: 0 < i < Array.length op_array *)
  (* INV: the two locations MUST be in the same CFA *)
  let level = M.Debug in
  let _ = M.msg_string M.Normal ("TPROJ: at op "^(string_of_int i)) in
  let op = try op_array.(i) with _ -> failwith "HERE1" in
  let call_i = left i (* may be -1: if the current loc i is in main *) in
  let op_fname = C_SD.get_location_fname (C_SD.get_source op) in
  (*let this_lvalue_closure =
     if (O.getValueOfBool "fldunroll") then
     C_SD.lvalue_closure_stamp 
     else C_SD.lvalue_closure
     in
     let lvalue_hits lv =
     List.flatten (List.map (aliasfn op_fname) (this_lvalue_closure lv))
     in -- used to "close" mods_on_path w.r.t. aliasing etc *)
  let last_op = op_array.(last) in
  M.msg_string M.Debug ("proc_trace_op:"^(C_SD.Command.to_string (C_SD.get_command op))); 
  let process_return callsite_op call_triple caller_name retexp = 
    let (targ_option,fname,args) = call_triple in
    let cb_asgns =
      let fmls =
	match C_SD.lookup_formals fname with
	    C_SD.Variable l -> l
	  | C_SD.Fixed l -> l in
      copy_back_asgns (O.getValueOfBool "incref") (Misc.partial_combine fmls args) fname in
    let all_asgns = 
      match targ_option with
	  None -> cb_asgns
	| Some(targ) -> (targ,retexp)::cb_asgns in
    let ((add,del),ret_take) = process_asgn_block aliasfn caller_name all_asgns [] [] false in
    let _ = M.msg_string M.Debug "Now check glob_take" in
    let get_glob_take () = 
      if (O.getValueOfBool "dmod") 
      then 
        let (glob_lvals,ng_lvals) = Misc.filter_cut C_SD.is_global (dump_relevant ()) in
        let _ = M.msg_string M.Normal "check mod_on_edge" in
        (List.exists (C_SD.local_mod_on_edge callsite_op) ng_lvals
        || List.exists (C_SD.global_mod_on_call fname) glob_lvals)
      else
          (let globals_mod = (C_SD.global_lvals_modified fname) in (* closed under alias *) 
           List.exists (fun lv -> (gen_is_relevant (aliasfn "") lv) <> []) globals_mod) in
    let _ = M.msg_string M.Debug "done checking glob_take" in
    let glob_take = Stats.time "get glob take" get_glob_take () in
    let take = glob_take || ret_take in
    let i' = if take then i-1 else (call_i - 1) in	      
    ((add,del),take,i') in
    
  let skipcheck (targ_option,fname,args) =
    M.msg_string M.Debug "in skipcheck" ;
    if (not (O.getValueOfBool "skipfun") || (O.getValueOfBool "skipcheck" = false)) then ()
    else
	match unskip fname with None -> ()
        | Some(orig_fname) ->
            if (not (C_SD.is_defined orig_fname)) then ()
            else
	      let dummyretexp = Absutil.get_new_unknown orig_fname in
	      let (_,take,_) = process_return op  (targ_option,orig_fname,args) op_fname dummyretexp in
	      if take then 
                 (M.msg_string M.Normal ("Whoops: skipped --need to take: "^orig_fname);
                  if (Misc.is_substring orig_fname "__initialize__" && not (O.getValueOfBool "initialize")) 
                  then raise (UnskipException (-1));
                  PredTable.add_take_fun orig_fname; 
                  raise (UnskipException (i-1)))
              else (M.msg_string M.Debug ("Can still skip"^orig_fname)) in
   match Operation.get_info op with
     Operation.Ret retexp ->
	  begin
	    if (unsat_flag) then (([],[]),false,call_i - 1)
	    else
	      let call_op = op_array.(call_i) in
  	      match Operation.get_info call_op with
		  Operation.Call (_, targ_option,fname,args) -> 
		    process_return call_op (targ_option,fname,args) (C_SD.get_location_fname (C_SD.get_source call_op))
		    retexp
		| _ -> failwith "bad call-op in process_trace_op"
	  end
      | Operation.Call (_, targ,fname,args) ->
	  (* note: as we are actually processing the call, we have to take it *)
	  begin
            if (unsat_flag) then (([],[]),true,i-1)
	    else
	    let ac_formals =
	      List.map (fun s -> (E.Symbol s))
		(match C_SD.lookup_formals fname with
		     C_SD.Variable fml -> fml
		   | C_SD.Fixed fml -> fml)
	    in
	    let asgn_list = Misc.partial_combine ac_formals args in
	      (* INV: List.length.formals = List.length args *)
	    let ((add,del),_) = process_asgn_block aliasfn fname asgn_list [] [] false in
	      (* NOTE: that we have used fname -- the callee_name NOT the op_fname here *)
            let _ = flush_relevant_locals fname in
            if (add = [] && !rel_variable_size = 0) then
	      begin
		M.msg_string M.Error ("CONE EMPTY! -- probably an ERROR:"^fname);
		M.msg_string M.Normal ("CONE EMPTY! -- probably an ERROR:"^fname);
		if (O.getValueOfBool "tpbug") then (failwith "CONE EMPTY UNSAFE ? :-(")
	      end;
            let ((add,del), take, next_i) =
              let ac_call_i = left (i-1) in  
              let current_loc = C_SD.get_source op in
              let current_fname = C_SD.get_location_fname current_loc in
              if (O.getValueOfBool "projfun" && ac_call_i <> -1  && not (O.is_errorseed current_fname))
              then
              begin
                let _ = 
                  List.iter (del_rel_variable) del;
                  List.iter (add_rel_variable) add;
                in      
                let last_loc = current_loc in 
                let this_loc = C_SD.lookup_entry_location current_fname in  
                let rel_lvals = dump_relevant () in
                let _ = M.msg_string M.Normal 
                "dmod: projf: check may_be_modif" in
                let cant_jump = Stats.time "projf : may be modified" (C_SD.may_be_modified rel_lvals this_loc) last_loc  in
                if (not cant_jump) then (M.msg_string M.Normal
                ("TP: Jumping!:"^(string_of_int i)));
                (([],[]),true,if cant_jump then (i-1) else ac_call_i)
              end
            else ((add,del), true, i-1) in
            ((add,del),take,next_i)
	  end
      | Operation.Normal -> (* split cases on whether its an assign or an assume *)
	  begin
	    if (unsat_flag) then (([],[]),false,i - 1)
	    else
	      match (C_SD.get_command op).C_SD.Command.code with
		C_SD.Command.Block l ->
		  let peel_stm stm =
		    match stm with
			C_SD.Command.Expr (E.Assignment
			  (E.Assign, targ,valu)) -> (targ,valu)
		      | _ -> failwith "Bad stm in peel_stm!"
		  in
		  let ((add,del),take) =
		    process_asgn_block aliasfn op_fname
		      (List.rev (List.map peel_stm l)) [] [] false
		  in
		    ((add,del),take,i-1)
	      | C_SD.Command.Pred p ->
		  begin
		    let this_loc = C_SD.get_source op in
		    let last_loc = C_SD.get_source last_op in
                    let hit_checker = (add_and_hit_check (aliasfn op_fname)) in
                    let this_pred_lvals = P.lvals_of_predicate p in
                    let this_pred_relevant = List.exists hit_checker this_pred_lvals in
                    let path_mods_relevant = 
                      if (O.getValueOfBool "dmod") 
                      then
                         begin
                          let rel_lvals = dump_relevant () in
                          let _ = M.msg_string M.Normal 
                          "dmod: check may_be_modif" in
                          Stats.time "may be modified" (C_SD.may_be_modified rel_lvals this_loc) last_loc                         
                        end
                      else
                        let mods_between =  Stats.time "mods_on_path" (C_SD.mods_on_path this_loc) last_loc in
                        Stats.time "add and check hits" (List.exists hit_checker) mods_between in 
                    (* note: ok to STOP at first "hit", ie use List.exists, because the set will be reset 
                     * if the exists is true *)
                    let not_always_reach = (not (Stats.time "Always Reach" (C_SD.always_reach this_loc) last_loc)) in 
                    let take = (i+1 = Array.length op_array) 
                               || not_always_reach || this_pred_relevant || path_mods_relevant in
		    let (add,del) = ((if take then this_pred_lvals else []), []) in
		      ((add,del),take,i-1)
		  end
	      | C_SD.Command.Skip -> (([],[]),false,i-1)
              | C_SD.Command.SymbolicHook _ -> (([],[]),(not unsat_flag),i-1)
              | C_SD.Command.FunctionCall e -> 
                  begin
		    match e with
			E.FunctionCall (fn, el) ->
			  let fname = C_SD.get_fname fn in
                          (skipcheck  (None,fname,el)); 
			    (* skipcheck will throw an exception to be caught in block_analyze_trace if reqd.,
			       AFTER changing the various tables etc.*)
			  M.msg_string M.Normal ("Unknown fun "^fname^" in trace_project : optimistic");
			    (([],[]),false,i-1)
		      | E.Assignment
			  (E.Assign, targ, E.FunctionCall(fn, el)) ->
			  let fname = C_SD.get_fname fn in
			  let _ = skipcheck  (Some(targ),fname,el) in
                          let _ = M.msg_string M.Debug "after skch2" in
                          M.msg_string M.Normal ("Unknown fun "^fname^" in trace_project : optimistic");
			  let asgn_block = [(targ,Absutil.get_new_unknown fname)] in
			  let ((_,del),take) = process_asgn_block aliasfn op_fname asgn_block [] [] false in
			    (([],del),take,i-1)
		  end
	      | _ -> failwith ("unexpected value in process_trace_op")
	  end

let print_trace op_l =
  let print_op op = 
    let (l1,l2,rv) = op_to_string op in
    M.msg_string M.Normal (Printf.sprintf "%d :: %s :: %d" l1 rv l2) in
  List.iter print_op op_l

let print_trace_reg level op_l reg_l = 
  let print_op_reg i (op,reg) = 
    let (l1,l2,rv) = op_to_string op in
    let d = if O.getValueOfBool "bddprint" then P.toString (extract_pred reg) else "" in
    let s = if C_SD.isAsyncCall op then ":ACall" else if C_SD.isAsyncExec op then ":AExec" else "" in
    M.msg_string level (Printf.sprintf "Op: %d %s :%d :: %s :: %d" i s l1 rv l2);
    M.msg_string level ("post-reg: "^d);
    i+1 in
  let zipped = try List.combine op_l reg_l with _ -> failwith "combine error in print_trace_reg" in
  ignore(List.fold_left print_op_reg 0 zipped)
 
let add_unsat_check_op op_array reg_array cefn i =
    M.msg_string M.Debug ("Tproj: asserts op "^(string_of_int i));
    let op = op_array.(i) in
    let pre_reg = reg_array.(i) in
    let post_reg = reg_array.(i+1) in
    let pre_pred = extract_pred pre_reg in
    let atomic_post_reg = 
      match post_reg with
        Region.Atomic atr -> atr
      | _ -> failwith "bad region given to add_unsat_check_op!" in
    let fname = reg_function_name pre_reg in
    let ce_o = if C_SD.isReturn op then (cefn i atomic_post_reg.Region.location) else None in
    let block_cons = block_concrete_data_pre fname 0 op ce_o in
    let conj_block_cons = pred_lval_map 0 pre_pred in
    let is_unsat = 
      block_assert block_cons;
      block_assert conj_block_cons;
      let rv = block_is_contra () in
      block_pop ();
      rv in
    if (is_unsat) then M.msg_string M.Debug  ("Found unsat at op"^(string_of_int i));
    is_unsat 


  
let trace_project op_l reg_l =
  let aliasfn =
    if (O.getValueOfBool "cf") then AA.get_lval_aliases_scope
    else if (O.getValueOfBool "talias") then  (fun _ -> trace_lv_table_get_lval_aliases)
    else (fun _ -> get_lval_aliases) in
  let trace_size = List.length op_l in
  M.msg_string M.Major ("Trace Project: IN size "^(string_of_int trace_size));
  M.msg_string M.Error ("Trace Project: IN size "^(string_of_int trace_size));
  print_trace_reg M.Normal op_l (List.tl reg_l) ;
  let op_array = Array.of_list op_l in
  let reg_array = Array.of_list reg_l in
  let cefn = get_callEdge_o op_array in
  let auco = add_unsat_check_op op_array reg_array cefn in
  let _ = M.msg_string M.Debug "Building tp_array" in
  let _ = block_reset () in
  let _ = Stats.time "make_lv_stack [tproj]" (make_lv_stack (reg_function_name (List.hd reg_l))) op_l in 
    let (left,_) = Misc.paren_match Operation.paren_fun op_array in
  let tp_array = Array.make trace_size false in
  let _ = rel_variable_reset () in
  let last = ref (trace_size - 1) in	      
  let unsat_flag = ref false in
  (* now a strange way to do a decreasing for-loop in OCAML *)
  let i = ref (trace_size - 1) in
  let _ =
    while (!i >= 0) do
      Ast.check_time_out () ;
      let ((add,del),take,i') =
        Stats.time "process_trace_op" (process_trace_op !unsat_flag aliasfn op_array left (!last)) (!i) in
      Array.set tp_array !i take;
      if (take) then (last := !i; mods_on_path_reset ());
      i := i'; 
      List.iter del_rel_variable del;
      List.iter add_rel_variable add; 
      if take && (not !unsat_flag) && not (O.getValueOfInt "predH" >= 6) 
      then unsat_flag := auco !last
    done in
  let _ = block_reset () in (* dont forget to reset the thmprover *)
  let _ = M.msg_string M.Debug "tp_array built" in
  let proj_size = Misc.array_counti (fun i a -> a) tp_array in
  let _ = M.msg_string M.Error ("Trace Project: OUT size "^(string_of_int (proj_size))) in
  let _ = M.msg_string M.Major ("Trace Project: OUT size "^(string_of_int (proj_size))) in
  let fwd_map_array = Array.create trace_size (-1) in
  let rev_map_array = Array.create proj_size (-1) in
  let idx = ref 0 in
  for i = 0 to trace_size - 1 do
    if tp_array.(i) then
      (Array.set fwd_map_array i (!idx);
       Array.set rev_map_array (!idx) i;
       idx := !idx + 1);
  done;
  assert (!idx = proj_size);
  let revmap = (fun i -> if (0 <= i) && (i < proj_size) then rev_map_array.(i) else -1) in
  let fwdmap = (fun i -> if (0 <= i) && (i < trace_size) then fwd_map_array.(i) else -1) in
  let mask_fn = fun i -> if 0 <= i && i < trace_size then tp_array.(i) else false in
  let op_l' = Misc.list_mask mask_fn op_l in
  let pre_reg_l = Misc.list_mask mask_fn reg_l in
  let post_reg_l = Misc.list_mask (fun i -> mask_fn (i-1)) reg_l in
  M.msg_string M.Debug "Out Trace";
  print_trace op_l';
  (pre_reg_l,post_reg_l,op_l',revmap,fwdmap)


(*******************************************************************************************************)
(******** CF INTERPOLANT REFINE -- takes useful blocks, cuts the trace, adds new predicates ************)
(*******************************************************************************************************)
let stats_nb_non_trivial_functions = ref 0

let rec jump_paren (left,right) i =
  try
  let (li,ri) = (left i, right i) in
  if i = ri 
  then 
    if i+1 = left (i+1) then jump_paren (left,right) (right (i+1))
    else (left (i+1),right (i+1))
  else (li,ri) 
  with e -> failwith ("Error in jump_paren! "^(Printexc.to_string e))
(* (..(((.(.))).)....((..)).(.).(.).().(.(.()).) *)

  
let cf_interpolant_refine (left,right) extract_block conj_block_array reg_array
  (block_l : int list) = 
  M.msg_string M.Debug ("Inside cf interpolant refine:"^(string_of_int (List.length block_l)));
  (* assert (not (block_l = [])); *)
  let jpf = jump_paren (left,right) in
  let len_op_l = Array.length conj_block_array in
  let ord_block_l = List.sort compare block_l in
  let start = List.hd ord_block_l in
  let cons_l = List.map extract_block  ord_block_l in
  let (start_cons,start_cons_pos) = 
    if block_check_unsat (P.And cons_l) then (P.True,-1)
    else (conj_block_array.(start),start) 
  in
      (* for debugging purposes, we shall dump the interesting blocks *)
  let _ =
          let level = M.Debug in
          if (level = M.Normal) then
    begin
      M.msg_string level (Printf.sprintf "num blocks = %d" (List.length block_l));
      M.msg_string level  (Printf.sprintf "start cons = [%d] \n "  start_cons_pos );
      M.msg_printer level  P.print (start_cons);
      M.msg_string level ("Constraints");
      List.iter2 (fun p i -> M.msg_string level  ((string_of_int i)^" :  "^(P.toString p)) ) 
	cons_l ord_block_l
    end
  in
  let cut ls i = 
    let l_i = left i in
    let r_i = right i in
    let (l_i,r_i) = if (r_i = i) then jpf i else (l_i,r_i) in
    let (context,hole) = 
      if (l_i = -1) then ([],ls) 
      else
	List.partition (fun x -> (x <= l_i || (x >= r_i && r_i <> -1))) ls 
    in
    let pre_context = List.filter (fun x -> x <= l_i) context
    in (* to get the symbolic constants *)
    let (phi_minus,phi_plus) = List.partition (fun x -> x <= i) hole in
    let b = (l_i < start_cons_pos) && (start_cons_pos <= i) in 
      (* true means the "start_cons" goes to the past, false means it goes into the future *)
      
    (* RJ:
       Q: can you have simultaneously :
       1. a nontrivial start_cons in the past
       2. a "closed" trace ie all calls return
       3. a context containing constraints from before the call at i ?
       A: I really dont think so !
    *)
    let rv =
      if r_i <> -1 then 
	(i,pre_context,phi_minus,context@phi_plus,b) 
      else 
	(* RJ: can you have a r_i = -1 AND the start_cons in the future ... NO. return true for the b coordinate ... *)
	(i,pre_context,context@phi_minus, phi_plus,true) 
    in
    let b = b || (r_i = -1) in
      M.msg_string M.Debug (Printf.sprintf "scp, i, l_i, r_i, b = %d %d %d %d %b" start_cons_pos i l_i r_i b);
      rv
  in
  let trace_cuts = Misc.list_tabulate (cut ord_block_l) 0 (len_op_l) in
    let _ = Hashtbl.clear foci_pred_table in
      (* now for each trace cut, get the predicates and add them to their resp. functions. for now only craig 1 *)
    let process_cut (i,call_past,past,future,b) =

      let _ = M.msg_string M.Debug ("In cf process cut at:"^(string_of_int i)) in
      let _ = 
	M.msg_string M.Debug ("b : "^(string_of_bool b));
	M.msg_string M.Debug ("start_cons : "^(P.toString start_cons))
      in
      let _ = 
	M.msg_string M.Debug ("Past Constraints:");
	List.iter (fun p -> M.msg_string M.Debug (P.toString (extract_block p))) past
      in
      let _ = 
	M.msg_string M.Debug ("Future Constraints:");
	List.iter (fun p -> M.msg_string M.Debug (P.toString (extract_block p))) future
      in
      let (cons_l_pre,cons_l_post) = 
	match b with
	    false -> (List.map extract_block past, start_cons :: (List.map extract_block future))
	  | true -> (start_cons::(List.map extract_block past),List.map extract_block future)
      in
      let l_id = C_SD.location_coords (location_coords_of_region (reg_array.(i+1))) in (*RJ : check*)
      let callee_name = C_SD.get_location_fname (C_SD.lookup_location_from_loc_coords l_id) in
      let foci_preds = try Hashtbl.find foci_pred_table (past) with Not_found ->
	      let rv = 
		(* JHALA: temporary patch for segfaults! *)
		match (block_check_unsat (P.And cons_l_pre)) || (block_check_unsat (P.And cons_l_post)) with
		    true -> []
		  | false ->
		      let pre_call_cons = P.conjoinL (List.map extract_block call_past) in
			Stats.time "extract foci preds (cf)"
			  (Absutil.extract_foci_preds_cf (callee_name,pre_call_cons)) (cons_l_pre,cons_l_post)
	      in 
		Hashtbl.add foci_pred_table past rv;
		rv
      in
      let _ = M.msg_string M.Debug "extraction done" in
      let _ = List.iter (fun i -> M.msg_string M.Debug (string_of_int i)) foci_preds
      in
	let present_p_l = try Hashtbl.find loc_pred_table l_id with Not_found -> [] in
	let present_fun_p_l = try Hashtbl.find fun_pred_table (fst l_id) with Not_found -> [] in
	let to_add = List.filter (fun p_i -> not( List.mem p_i present_p_l)) foci_preds 
in 
  M.msg_string M.Debug ("#Added preds " ^ string_of_int (List.length to_add)); 
  List.iter (fun p_i -> refined ();block_a_t_flag := true; M.msg_string M.Debug (string_of_int p_i)) to_add;
  PredTable.add_local_preds l_id to_add;
  if (present_fun_p_l = [] && to_add <> []) then 
      stats_nb_non_trivial_functions := !stats_nb_non_trivial_functions + 1
in
  List.iter process_cut trace_cuts



(**************************************************************************)
(**INTERPOLANT REFINE:take useful blk, cuts trace, adds preds *************)
(**************************************************************************)



let gen_itps_from_cuts extract_block start_cons end_cons = 
  (* called like that: (p_l_l,itp_l) = gen_itps_from_cuts extract_block scons econs ([],[]) ([],op_id_l) *)
  (* The _ac procedure moves the ``cutting line'' downwards through the trace.  The conjunction of blocks before and after the trace are two parts the formula is divided to make a craig interpolant.  Blocks before the line are in before_l list and after the line -- in after_l.  Each recursive call has one item transferred from the latter to the former one. *)
  let rec _ac (pred_l_l,itp_l) (before_l,after_l) =  
    let cons_l_pre = start_cons::(List.map extract_block before_l) 
    and cons_l_post = end_cons ::(List.map extract_block after_l) in
    let (foci_preds) = 
      if (O.getValueOfBool "fmcc") then 
  	let _ = M.msg_string M.Debug "SKY point 1" in
	[]
      else
	(* we enter here dafaultly *)
  	let _ = M.msg_string M.Debug "SKY point 2" in
	(* before cut-point is phi- formula, and after --- phi+ *)
        match (block_check_unsat (P.And cons_l_pre)) || 
        (block_check_unsat (P.And cons_l_post)) with
        true -> let _ = M.msg_string M.Debug "SKY point 3"  in
	[]
	(* the next line triggers when (phi- && phi+) is unsatisfiable, and, therefore, a superset of phi- is craig interpolant *)
	(* extract_foci_preds looks for it even in non-foci case *)
        | false -> let _ = M.msg_string M.Debug "SKY point 4" in extract_foci_preds (cons_l_pre,cons_l_post) 
    in 
    let (foci_interpolant) = 
      if (O.getValueOfBool "fmcc") then
        match (block_check_unsat (P.And cons_l_pre)) 
        || (block_check_unsat (P.And cons_l_post)) with true -> let _ = M.msg_string M.Debug "SKY point 5" in P.True 
        | false -> let _ = M.msg_string M.Debug "SKY point 6" in TheoremProver.extract_foci_interpolant (cons_l_pre,cons_l_post)
      else let _ = M.msg_string M.Debug "SKY point 7" in P.True
    in
    (* with default settings, foci_interpolant is always P.True *)
    let (fpl',fil') = (foci_preds::pred_l_l,foci_interpolant::itp_l) in
    match after_l with [] -> (List.rev fpl',List.rev fil')
    | h::t -> _ac (fpl',fil') (h::before_l,t)
  in
  _ac

  
let proc_itps_from_cuts start_pos end_pos reg_array itp_array tps_o =  
  let rec _ac prev_preds (pred_l_l,itp_l) (before_l,after_l) = 
    let prev_preds = if prev_preds = [] then [-1] else prev_preds in
    let (refine_begin,refine_end) =
      match after_l with [] -> (List.hd before_l + 1, end_pos)
      | end_pos::tl -> 
        if (before_l = []) then (start_pos,start_pos) 
        else (List.hd before_l + 1, end_pos)
    in
    try
      begin
      assert (pred_l_l <> [] && itp_l <> []);
      let (foci_preds::fpl',foci_interpolant::fil') = (pred_l_l,itp_l) in
      List.iter (PredTable.update_pred_dep_table prev_preds) foci_preds;
      for i = refine_begin to refine_end do
        M.msg_string M.Debug ("Accessing i= "^(string_of_int i));
        (match tps_o with Some a -> Array.set a i (foci_preds@ (a.(i))) | None -> ());
        Array.set itp_array i foci_interpolant; 
        let l_id = C_SD.location_coords (location_coords_of_region (reg_array.(i))) in
        List.iter (PredTable.update_pred_dep_table foci_preds) foci_preds;
        PredTable.add_local_preds l_id foci_preds
        (*M.msg_string M.Debug (Printf.sprintf "loc_id (%d, %d)" (fst l_id) (snd l_id));	
        let present_p_l = try Hashtbl.find PredTable.loc_pred_table l_id with Not_found -> [] in
        let to_add = List.filter (fun p_i -> not( List.mem p_i present_p_l)) foci_preds in
        M.msg_string M.Debug "Adding pred "; 
        List.iter (fun p_i -> PredTable.block_a_t_flag := true; 
        M.msg_string M.Debug (string_of_int p_i)) to_add;
        Hashtbl.replace PredTable.loc_pred_table l_id (to_add@present_p_l)*)
      done;
      match after_l with [] -> () | h::t -> _ac foci_preds (fpl',fil') (h::before_l,t)
      end
    with e -> 
    (M.msg_string M.Error ("proc_itps_from_cuts: "^(Printexc.to_string e)); raise e)
  in _ac []

let cut_itpN tps_o extract_block spos epos scons econs reg_array itp_array op_id_l = 
  let cons_l = scons :: (List.map extract_block op_id_l) @ [econs] in
  let itp_l = TheoremProver.extract_foci_interpolantN cons_l in
  let f itp = Absutil.process_raw_foci_preds (fun x -> x) (P.getAtoms itp) in
  let p_l_l = List.map f itp_l in
  proc_itps_from_cuts spos epos reg_array itp_array tps_o (p_l_l,itp_l) ([], op_id_l);
  List.flatten p_l_l

let cut_itp extract_block spos epos scons econs reg_array itp_array op_id_l =
  let (p_l_l,itp_l) = gen_itps_from_cuts extract_block scons econs ([],[]) ([],op_id_l) in
  let _ = M.msg_string M.Debug "SKY dumping itp_l" in
  let _ = List.iter (fun x -> M.msg_string M.Debug (P.toString x)) itp_l in
  let _ = M.msg_string M.Debug "SKY dumping p_l_l" in
  (* let _ = List.iter (fun x -> M.msg_string M.Debug (P.toString x)) (List.flatten p_l_l) in *)
  let _ = List.iter (fun x -> M.msg_string M.Debug (string_of_int x)) (List.flatten p_l_l) in
  (* now p_l_l contains list of interpolants and itp_l contains list of true preds (default settings!) *)
  let _ = M.msg_string M.Debug "SKY calling proc_itps" in
  proc_itps_from_cuts spos epos reg_array itp_array None (p_l_l,itp_l) ([], op_id_l);
  List.flatten p_l_l
  
    
(* OLD: to be deleted once the new version is stable  
let rec cut_itp extract_block start endp start_cons end_cons reg_array itp_array (* fixed *)
        accum_pred before_l after_l =
      (* INV: before_l + after_l = block_l *)
      let cut = cut_itp extract_block start endp start_cons end_cons reg_array itp_array in
      if (after_l = [] && end_cons = P.True) then accum_pred 
      else
        let cons_l_pre = start_cons::(List.map extract_block before_l) 
        and cons_l_post = end_cons ::(List.map extract_block after_l) in
        let (foci_preds) = 
          if (O.getValueOfBool "fmcc") then []
          else
            match (block_check_unsat (P.And cons_l_pre)) || 
            (block_check_unsat (P.And cons_l_post)) with
              true -> []
            | false -> extract_foci_preds (cons_l_pre,cons_l_post) 
        in 
        let (foci_interpolant) = 
          if (O.getValueOfBool "fmcc") then
            match (block_check_unsat (P.And cons_l_pre)) || 
            (block_check_unsat (P.And cons_l_post)) with
            true -> P.True 
            | false -> TheoremProver.extract_foci_interpolant (cons_l_pre,cons_l_post)
          else P.True
        in
        M.msg_string M.Debug "extraction done";	
        List.iter (fun i -> M.msg_string M.Debug (string_of_int i)) foci_preds;
	let (refine_begin,refine_end) =
	  match after_l with
	      [] -> (List.hd before_l + 1, endp)
	    | end_pos::tl -> if (before_l = []) then (start,start) else (List.hd before_l + 1, end_pos)
	in
        for i = refine_begin to refine_end do
          M.msg_string M.Debug ("Accessing i= "^(string_of_int i));
          Array.set itp_array i foci_interpolant; 
          let l_id = C_SD.location_coords (location_coords_of_region (reg_array.(i))) in
          M.msg_string M.Debug (Printf.sprintf "loc_id (%d, %d)" (fst l_id) (snd l_id));	
          let present_p_l = try Hashtbl.find PredTable.loc_pred_table l_id with Not_found -> [] in
          let to_add = List.filter (fun p_i -> not( List.mem p_i present_p_l)) foci_preds in
          M.msg_string M.Debug "Adding pred "; 
          List.iter (fun p_i -> PredTable.block_a_t_flag := true; 
          M.msg_string M.Debug (string_of_int p_i)) to_add;
          Hashtbl.replace PredTable.loc_pred_table l_id (to_add@present_p_l)
        done; 
        match after_l with
          [] -> accum_pred 
        | end_pos :: tl -> cut (foci_preds @ accum_pred) (end_pos::before_l) tl
*)

(* for default tps_o and acc_itp_o are equal to None *)
(* extract_block is a function that gets block *)
let interpolant_refine tps_o acc_itp_o extract_block conj_block_array reg_array (block_l : int list) = 
  M.msg_string M.Debug (Printf.sprintf "Inside interpolant refine: _%s _"
  (Misc.string_of_int_list block_l)) ;
  (*b assert (not (block_l = [])); *)
  (* focimodelchecker -- piggybacking on this ! *)
  let len_reg_array = Array.length reg_array in
  let itp_array = Array.make len_reg_array P.True in (* set the default ITP to be true *)
  let last_block = list_max block_l in
  let _ = Array.iteri (fun i _ -> if i > last_block then itp_array.(i) <- P.False else ()) itp_array in
    (* end focimodelchecker *)
  (* itp_array contains interponalts so far: true so far, false after the last block *)
  let ord_block_l = List.sort compare block_l in
  let start = List.hd ord_block_l in
  let endp = Array.length conj_block_array - 1 in
  let cons_l = List.map extract_block ord_block_l in
  let end_cons = if (O.getValueOfBool "fmcc") then conj_block_array.(endp) else P.True in    
  (* if trace is contradictory, then starting constraint is P.True *)
  let start_cons = 
      if block_check_unsat (P.And (cons_l@[end_cons])) then P.True
      else conj_block_array.(start) 
  in	
  let accum_pred = ref [] in
  (* the cutting function --- intrepolation is HERE *)
	(* Ususally cut_itp is called *)
  let cutter = if (O.getValueOfBool "itpn") then cut_itpN tps_o else cut_itp in
  let _ = M.msg_string M.Debug (if (O.getValueOfBool "itpn") then "SKY call cut_itpN tps_o" else "SKY call cut_itp") in
  let accum_pred = cutter extract_block start endp start_cons end_cons
    reg_array itp_array (* [] []*) ord_block_l in    
  let _ = M.msg_string M.Debug "SKY cutter quit!" in
  (* if (O.getValueOfString "checkRace" <> "") then *)
  (* remove redundant interpolants *)
  let accum_pred = Misc.compact accum_pred in
  (* determine the part of the trace where we're applying these *)
  let (min,max) = (List.hd ord_block_l, List.hd (List.rev ord_block_l)) in
  M.msg_string M.Normal "Adding all preds now...";
  for i = min to max do
    M.msg_string M.Debug ("Accessing i= "^(string_of_int i));
    let l_id = C_SD.location_coords (location_coords_of_region (reg_array.(i))) in
    PredTable.add_local_preds l_id accum_pred;
    (* "head" of the list is the min of the list, should also be the last in the list *)
  done;
  match acc_itp_o with None -> ()
  | Some a -> Array.iteri (fun i v -> a.(i) <- Predicate.And [v;itp_array.(i)]) a
  (* if (O.getValueOfBool "fmcc") then Some (itp_array) else None *)

(****** DEBUG TMP ***********)
let string_of_str_lst name l =
  name ^ ": " ^ (List.fold_left (fun s x -> s ^ (if s <> "" then ", " else "") ^ x) "" l)
let string_of_expr_lst name l =
  string_of_str_lst name (List.map Expression.toString l)
let string_of_lval_lst name l =
  string_of_str_lst name (List.map Expression.lvalToString l)

(********************************************************************************************************)
(******************* The main refinement function for seq programs called by lmc*************************)
(********************************************************************************************************)

exception IncreaseEventCounter

let async_matcher ops regs = 
  assert (List.length ops = List.length regs - 1);
  M.msg_string M.Debug "in async_matcher";
  let regs' = Misc.chop_last regs in
  (* let f op r = if C_SD.isAsyncCall op then make_async_op op (Region.get_adr r) else op in
  let ops = List.map2 f ops regs' in *)
  let op_to_k op = Events.eventHandler_to_string (fst (Events.deconstructAsyncCall op)) in
  let fp op = 
    if C_SD.isAsyncCall op then ("(",op_to_k op) 
    else if C_SD.isAsyncExec op then (")",op_to_k op) 
    else ("","") in
  let rv = 
    match Misc.gen_paren_match fp (Array.of_list ops) with
      None -> raise IncreaseEventCounter 
    | Some (_,r) -> r in 
  M.msg_string M.Debug "done async_matcher";
  rv

let refined_ops ops = 
  let proc op = 
    let sloc = C_SD.get_source op in
    let fn = C_SD.get_location_fname sloc in
    Hashtbl.replace refined_fun_table fn true
  in
  Hashtbl.clear refined_fun_table;
  List.iter proc ops


let async_block_concrete_data_pre fname op ce_o i = 
     assert (C_SD.isAsyncCall op || C_SD.isAsyncExec op);
     match C_SD.call_on_edge op with None -> failwith "async_bcdp: no fname!" 
     | Some(cname,_) ->
         let formalp = make_formal_pred cname i in
         if (C_SD.isAsyncCall op) then
           let actualp = block_concrete_data_pre fname 0 op ce_o in 
           P.And [actualp;formalp]
         else ((* smash the formals *) lvmap_pop (); formalp)
  
let block_at_cdp fname op ce_o i_exec = 
  M.msg_string M.Debug "block_at_cdp start";
  let rv = 
    if (O.getValueOfBool "events" && (C_SD.isAsyncCall op || C_SD.isAsyncExec op)) 
    then async_block_concrete_data_pre fname op ce_o i_exec 
    else block_concrete_data_pre fname 0 op ce_o  in
  M.msg_string M.Debug "block_at_cdp done";
  rv


(* returns block_array, conj_block_array *)
let build_block_at_constraint_arrays op_l reg_l pre_reg_l post_reg_l =
  M.msg_string M.Normal "build_block_at_constraint_arrays: start";
  print_trace_reg M.Normal op_l post_reg_l;
  (* false by default *)
  let foci_mode = O.getValueOfString "ubdp" = "foci" || O.getValueOfBool "fmc" in
  let ptrace_size = List.length op_l in
  let op_array = Array.of_list op_l in
  (* initially conj_block_array consists of true preds *)
  let conj_block_array = Array.make ptrace_size P.True in
  (* so does block_array *)
  let block_array = Array.make ptrace_size P.True in
  (* by default - constant zero *)
  let fmatch = if (O.getValueOfBool "events") then async_matcher op_l reg_l else fun _ -> 0 in
  let fexec i = max i (fmatch i) in
  (* function of int -> op -> op gets some kind of disamiguated edge if we're calling a function via pointer *)
  let cefn = get_callEdge_o op_array in
  (* current position *)
  let posn = ref (ptrace_size - 1) in 
  let _ = M.msg_string M.Debug "block_a_t: building blocks start 2" in  
  let unsat_flag = ref false in
  (* prepare empty theorem prover *)
  let _ = block_reset () in
  (* Initialize the aliaser *)
  let _ = initialize_aliaser () in
  (* Initialize the incremental closurer *)
  let _ = initialize_closurer () in
  (* this function gets operations, regions-after-operation and predicates.  It iterates them simultaneously *)
  let rec cons_folder unsat_at opRegs preds =
    match unsat_at with
      None -> begin
        match (opRegs, preds) with
	  (((op, Region.Atomic atomic_post_reg) :: opRest), (pre_pred :: predRest)) -> 
	    begin
	      M.msg_string M.Normal ("cons_folder !posn = "^(string_of_int !posn));
	      (* the function we're in *)
              let fname = match opRest with (_, entry_reg)::_ -> reg_function_name entry_reg
		                             | _ -> atomic_reg_function_name atomic_post_reg in
	      (* refined edge *)
              let ce_o = 
                if C_SD.isReturn op then (cefn !posn atomic_post_reg.Region.location) 
                else None in
	      (* call with function name, operation, edge to this operation,  posn *)
	      (* calls block_concrete_data_pre fname 0 op ce_o *)
	      let block_cons = Stats.time "block_at_cdp" (block_at_cdp fname op ce_o) (fexec !posn) in
	      let conj_block_cons = pred_lval_map 0 pre_pred in
	      Array.set block_array !posn block_cons;
              Array.set conj_block_array !posn conj_block_cons;
              posn := !posn - 1;
              let is_unsat = 
                if foci_mode || (Options.getValueOfInt "predH" >= 6) then false
                else
                    (block_assert block_cons; block_assert conj_block_cons;
                    let rv = Stats.time "contra_check" block_is_contra () in
                    block_pop (); rv) in
              if (is_unsat) then M.msg_string M.Debug ("Unsat at op: "^(string_of_int (!posn +1)));
              let new_unsat_at = if (is_unsat && not (O.getValueOfInt "predH" >= 6)) then Some (!posn+1) else unsat_at in
              cons_folder new_unsat_at opRest predRest 
	    end
	| _ -> 0
      end
      | Some n -> n
    in
    (* whenever recursion is enabled, we need to preprocess the lvmap appropriately *)
  (* make call stack from first region's function to error operation *)
  let _ = Stats.time "make lv_stack" (make_lv_stack (reg_function_name (List.hd reg_l))) op_l in
  (* just array of preds from blocks, converted to Predicate *)
  let conj_l = List.map (fun r -> if foci_mode then P.True else extract_pred r) pre_reg_l in
  M.msg_string M.Debug (string_of_int (List.length pre_reg_l));
  (* SKY let _ = List.iter (M.msg_printer M.Debug P.print) conj_l in *)
  (* list of pairs: (operation, resulting region *)
  let op_reg_l = try List.combine op_l (post_reg_l) with _ -> failwith "lists done have the same length" in
     (* RJ: lv_table is not used anywhere ..! *)
  (* populate the lv_table with lvalues READ throughout the trace *)
  (* Also, gather the fields accessed in the trace; will be used when generating lvalue closures in constraint generation *)
  M.msg_string M.Normal ("Loading LV table...");
  if (O.getValueOfBool "talias" || O.getValueOfBool "fldunroll") then (Stats.time "load lv table" load_lv_table op_l);
  M.msg_string M.Debug "BEFORE CONS\n";
  Array.iter (M.msg_printer M.Debug P.print) block_array;
  M.msg_string M.Debug  "END BEFORE CONS \n";
  let trace_ends_at = Stats.time "Cons folder" (cons_folder None (List.rev op_reg_l)) (List.rev conj_l) in
  M.msg_string M.Debug "AFTER CONS\n";
  Array.iter (M.msg_printer M.Debug P.print) block_array;
  M.msg_string M.Debug  "END AFTER CONS \n";
  block_reset ();
  M.msg_string M.Debug "THE CONSTRAINTS ARE:\n";
  Array.iter (M.msg_printer M.Debug P.print) block_array;
  M.msg_string M.Debug  "END CONSTRAINTS \n";
  M.msg_string M.Debug "CONJS ARE:\n";
  Array.iter (M.msg_printer M.Debug P.print) conj_block_array;
  M.msg_string M.Debug  "CONJS \n";
  M.msg_string M.Debug "block_a_t: building blocks end"; 
  (block_array,conj_block_array,trace_ends_at)
  
(*let rec print_trace preregl opl postregl ctr = print_trace_reg M.Debug opl postregl
  
  let regs r = 
    let (ps,l) = reg_to_strings r in 
    (String.concat ";" ps)^" @ "^l in
  match (preregl,opl,postregl) with
    (prereg::pre_tl,op::op_tl,postreg::post_tl) ->
      M.msg_string M.Debug (Printf.sprintf "BNum: PRE  : %d : %s" ctr (regs prereg)) ;
      M.msg_string M.Debug (Printf.sprintf "BNum: OP   : %d : %s" ctr (op_to_string op)) ;
      M.msg_string M.Debug (Printf.sprintf "BNum: POST : %d : %s" ctr (regs postreg)) ;
      print_trace pre_tl op_tl post_tl (ctr+1)
    | ([],[],[]) -> ()
    | _ -> failwith "Bad list lengths in block_at" *)

      
let ac_block_analyze_trace reg_l op_l =
  assert(List.length op_l > 0);
  print_string "block_analyze_trace\n";
  let trace_size = List.length op_l in
  M.msg_string M.Normal ("In block_a_t:"^(string_of_int trace_size));
  try
    (* Phase 1: Load trace information into arrays *) 
    (* TRACE PROJECTION *)
    let orig_op_l = op_l in
    	(* original list of operations *)
    let orig_op_array = Array.of_list orig_op_l in (* needed in CF for left,right *)
    (*  discard last region ; discard first reg, op_l, E, E *)
    let (pre_reg_l,post_reg_l,op_l,revmap,fwdmap) =
      if (O.getValueOfBool "tproj") then Stats.time "Trace Project" (trace_project op_l) reg_l 
      else (Misc.chop_last reg_l, List.tl reg_l, op_l,(fun x -> x),(fun x -> x))
    in
      assert (List.length pre_reg_l = List.length post_reg_l);
      assert (List.length post_reg_l = List.length op_l);
      M.msg_string M.Debug (Printf.sprintf "Projected lengths: pre_reg_l %d post_reg_l %d op_l %d "
        (List.length pre_reg_l)(List.length post_reg_l)(List.length op_l));
      print_trace_reg M.Normal op_l post_reg_l;
      (* clear refined_fun table *)
      PredTable.block_a_t_reset ();
      let op_array = Array.of_list op_l in
        (* Phase 2: we've set up the various arrays ... now to get the block constraints *)
        M.msg_string M.Normal "\n\nStarting phase 2" ;
        M.msg_string M.Normal ("Cldepth is"^(string_of_int (O.getValueOfInt "cldepth"))^"\n") ;
        let (block_array,conj_block_array,trace_end) = build_block_at_constraint_arrays op_l reg_l pre_reg_l post_reg_l in
	(* block_array are postconditions imposed by each block.
	   conj_block_array are predicates from regions. Last cell of conj_block_array is always true *)
          (* Phase 3: Constraints set up, the various arrays are loaded! Compute the useful ie contradictory blocks *)
          M.msg_string M.Normal "\n\nStarting phase 3" ;
	  (* get sets of interesting blocks:  the ones that make the trace contradictory, discarding other useless ones.  Nice heuristics actually! *)
	  (* Note that all constraints are RESTORED when the function returns!  Later we will see that only `extract block' is used. *)
          let (_ref_start_pos, _interesting_block_sets, suppress_stuff) =
            Stats.time "get useful blocks" (get_useful_blocks op_array block_array conj_block_array) (Some trace_end) in 
          let _ = if (O.getValueOfBool "traces") then 
            let pr_s_l = List.map (fun r -> String.concat "," (fst (reg_to_strings r))) post_reg_l in
              htmlize_trace op_l pr_s_l _interesting_block_sets "" in
            let (_, _extract_block,_,_,_) =  suppress_stuff in
            (* TRACE PROJECT : now we shall go back into the "real" trace *)
            let interesting_block_sets = List.map (List.map revmap) _interesting_block_sets in 
            let _ =
              let print_bs (idx,l) = 
                let printop i =
                  let a = orig_op_array.(i) in
                  let (_,_,op_s) = op_to_string a in
                    M.msg_string M.Normal (Printf.sprintf "[INF%d] %d : %s" idx i op_s)
                in
                  M.msg_string M.Normal "Conflicting Blocks";
                  List.iter printop (List.sort compare l)
              in
                List.iter print_bs (List.combine (Misc.range (0,List.length interesting_block_sets,1)) interesting_block_sets)
            in
              let ref_start_pos = revmap _ref_start_pos in
              let extract_block = fun i -> if fwdmap i = -1 then P.True else (_extract_block (fwdmap i)) in
              let _conj_block_array =
                if (O.getValueOfBool "tproj") then
	                let arr = Array.make trace_size P.True in
                    (Array.iteri (fun i a -> Array.set arr (revmap i) a) conj_block_array ; arr)
                else conj_block_array
              in
                let conj_block_array = _conj_block_array in
                let reg_array = Array.of_list reg_l in
                (* RJ: this is reqd for 1) oldschool focus, 2) to know what loc *)
                (* DONE setting up arrays for cutting *)
                let _ = M.msg_string M.Normal "[BAT] Done setting arrays for cutting" in
                (* more vicious hacks *)
                let _ = min_reg_id := (-1) in
                let _ = min_adr := None in
                let pre_flag = predicate_fixpoint_reached () in
                let _ = M.msg_string M.Debug ("#block sets = "^(string_of_int (List.length interesting_block_sets))) in
                (* Phase 4: refine using the blocks *)
                  M.msg_string M.Normal "\n\nStarting phase 4" ;
                  let tps_o = if O.getValueOfBool "mccarthy" then Some (Array.make (List.length reg_l) []) else None in
                  let acc_itp_o = if O.getValueOfBool "fmc" then Some (Array.make (List.length reg_l) P.True) else None in
                  let refine_fun =
                    if (O.getValueOfBool "cf") then cf_interpolant_refine (Misc.paren_match Operation.paren_fun orig_op_array)
		    (* SKY:  INTERPOLANTS TRIGGER HERE!!! *)
		    (* for default tps_o and acc_itp_o are equal to None *)
                    else fun a b c d -> (interpolant_refine tps_o acc_itp_o a b c d; ()) in
                  let refiner =   
                    (match (O.getValueOfInt "craig" <> 0)  with
			    (* The extract_block here returns block; all constrains are set to ok! *)
			    (* The reg_array is an array of regions *)
	                    true -> Misc.ignore_list_iter (refine_fun extract_block conj_block_array reg_array)
                    | false ->  Misc.ignore_list_iter (focus_refine extract_block conj_block_array reg_array))
                  in
                  let _ = M.msg_string M.Normal "[BAT] Calling refiner" in
                  let _ = Stats.time "Interpolant Refine" refiner interesting_block_sets in
                  let _ = M.msg_string M.Normal "[BAT] Done refiner" in
                  let _ = M.msg_string M.Debug (Printf.sprintf "Leaving block_a_t refine from %d" ref_start_pos) in
                  let _ = block_reset () in 
                  (* Phase 5: Check if we added something new *)
                  let _ = bat_counter := !bat_counter + 1 in
                  let _ = M.msg_string M.Normal ("Non-trivial functions "^(string_of_int !stats_nb_non_trivial_functions)) in
                  let cleanup_and_go () = 
                    if (ref_start_pos = -1 || !PredTable.block_a_t_flag || O.getValueOfBool "lazysymm" (* cycle caught in modelchecker *)) 
                    then (* bonafide refine or error *)
                      ( M.msg_string M.Debug "bonafide refine or error";
                      M.msg_string M.Debug (string_of_int ref_start_pos);
											Hashtbl.clear cfb_local_refine_table; 
                      let r' = if ref_start_pos < 0 then Region.bot else reg_array.(ref_start_pos) in
                        (ref_start_pos, r', true, tps_o,acc_itp_o))
                    else if (O.getValueOfInt "craig" >= 2) then
                      ( M.msg_string M.Debug "here where craig >= 2";
                      if (O.getValueOfBool "localrestart") 
                      then (print_lp_table ();printTableOfPreds (); dump_abs ();raise NoNewPredicatesException) 
                      else (O.setValueOfBool "localrestart" true; 
											M.msg_string M.Debug "before dump_abs SKY";
											dump_abs (); 
                        (ref_start_pos, reg_array.(ref_start_pos),true,tps_o,acc_itp_o)))
                    else if (O.getValueOfBool "cfb") then
                      let fname = reg_function_name (reg_array.(ref_start_pos)) in
                        if Hashtbl.mem cfb_local_refine_table fname then raise NoNewPredicatesException
                        else (Hashtbl.replace cfb_local_refine_table fname true; raise (RefineFunctionException fname))
                    else if (O.getValueOfBool "cf") then 
                      (print_lp_table ();printTableOfPreds (); dump_abs ();
                      raise NoNewPredicatesException)
                    (* TBD:COMPLETENESS this is v. premature -- but for dfs its ok *)
                    else
                      (O.setValueOfBool "localrestart" false;dump_abs ();
                      let r' = if ref_start_pos < 0 then Region.bot else reg_array.(ref_start_pos) in
                      (ref_start_pos,r',true,tps_o,acc_itp_o))
                  in
                    Stats.time "cleanup and go" cleanup_and_go ()
  with
    UnskipException ref_start_pos ->
      (* skipped an important function! *)
      (block_reset (); O.setValueOfBool "localrestart" false; dump_abs ();
      let r' = if ref_start_pos < 0 then Region.bot else List.nth reg_l ref_start_pos in
        ((ref_start_pos),r', true,None,None))
  | IncreaseEventCounter -> (block_reset (); failwith "TBD: raise counter!";(0,List.hd reg_l,true,None,None))
  | e -> (M.msg_string M.Error ("block_at hits exception"^(Printexc.to_string e));
      block_reset ();raise e)

let block_analyze_trace x y _ =  
  let (a,b,c,_,_) = ac_block_analyze_trace x y in
 (a,b,c)



(*** interpolation stuff end ****)    


(* Misc *)

  (* this next function is duplicated in cfAbstraction! *)

  let print_debug_info oplist = 
    let rec get_funs ops lst = 
      match ops with 
	  [] -> lst
	| op::rest ->
	    begin
              match (C_SD.get_command op).C_SD.Command.code with
		  C_SD.Command.FunctionCall fcexp -> 
		    begin
		      match fcexp with
			  E.FunctionCall(E.Lval (E.Symbol(fname)), _) -> 
			    if (C_SD.is_defined fname) then get_funs rest lst 
			    else get_funs rest (fname::lst)
			| E.Assignment (_, _, E.FunctionCall (E.Lval (E.Symbol fname), _)) -> 
			    if (C_SD.is_defined fname) then get_funs rest lst 
			    else get_funs rest (fname::lst)
			| _ -> get_funs rest (("FnPtr::"^(E.toString fcexp)) :: lst)
		    end
		| _ -> get_funs rest lst
	    end
    in
    let get_skip op = 
      match (C_SD.get_command op).C_SD.Command.code with
          C_SD.Command.FunctionCall fcexp -> 
	    begin
	      match fcexp with
		  E.FunctionCall(E.Lval (E.Symbol(fname)), _) 
		| E.Assignment (_, _, E.FunctionCall
					   (E.Lval (E.Symbol fname), _)) -> 
		    let (n,_,_) = C_SD.deconstructFunCall fcexp in
		      if n = __SKIPFunctionName then Some fname
		      else None
		| _ -> (*TBD: function pointer call *) None 
	    end
	| _ -> None
    in
      
    let impfun = get_funs oplist [] in
    let skipped_funs = Misc.compact (Misc.map_partial get_skip oplist) in
      if (List.length impfun > 0) then 
	begin
	  M.msg_string M.Normal "The following are suspect funs:\n";
	  M.msg_string M.Normal (Misc.strList impfun); 
	  M.msg_string M.Normal "The following are skipped funs:\n";
	  M.msg_string M.Normal (Misc.strList skipped_funs) 
	end

  (*******************************************************************************************)

let reset_decision_procedures () = SMTLIBInterface.reset ()

let notify_result result_s = htmlize_trace_final result_s 
  
(* Prepare_alias is a function to prepare may-alias information unless it has already been prepared *)
let disambiguate_function_pointers prepare_alias edge =
  let (srcloc,op, targetloc) = (C_SD.get_source edge, C_SD.get_command edge, C_SD.get_target edge) in 
    match op.C_SD.Command.code with
      C_SD.Command.FunctionCall(e) ->
        begin
          let is_fnptr_call fcexp = match fcexp with E.Lval (E.Symbol _) -> false | _ -> true in
          let gen_eq fname fcexp = P.Atom (E.Binary (E.Eq, E.Lval fname, fcexp)) in
	  (* Function to check for pointers in alias analyzer.
	     The alias analysis function throws an exception if it can't find any aliases of the function, or even if alias analysis is turned off.  This should be intercepted. *)
	  let get_fn_pointers fcexp =
	    try
	      prepare_alias ();
	      AA.getFunctionsPointedTo fcexp
	    with e ->
	      let s = Printexc.to_string e in
	      let _ = Message.msg_string Message.Normal (Printf.sprintf "Function pointer alias analyzer says: %s.\nSkipping this call-by-pointer." s) in
	      []
	  in
          (* if this function call is a function pointer call, disambiguate it *)
          match e with
              E.FunctionCall(fcexp, args) -> 
              if is_fnptr_call fcexp then
                begin
                  M.msg_string M.Normal ("Disambiguating " ^ (E.toString fcexp)) ;
		  match get_fn_pointers fcexp with
		  (* No function pointers found.  Even if the analysis failed, we still should keep going, so we return a fake edge *)
		  | [] -> [C_SD.make_edge srcloc targetloc (C_SD.Command.gather_accesses (C_SD.Command.FunctionCall (
		    E.FunctionCall ((E.Lval (E.Symbol "__BLAST_Couldnt_Disambiguate_Fptr")),args))
		  ))]
		  | _ as fps -> (* edge :: *) (List.map (fun fname ->
                    C_SD.make_edge srcloc targetloc
                      (C_SD.Command.gather_accesses 
                        (C_SD.Command.GuardedFunctionCall (gen_eq fname fcexp, E.FunctionCall(E.Lval fname, args)) )  ) )
                         fps)
                end
              else [ edge ]
            | E.Assignment(aop, target, E.FunctionCall(fcexp, args)) ->
                if is_fnptr_call fcexp then
                 begin
                  M.msg_string M.Normal ("Disambiguating " ^ (E.toString fcexp)) ;
		  match get_fn_pointers fcexp with
		  (* No function pointers found.  Even if the analysis failed, we still should keep going, so we return a fake edge *)
		  | [] -> [C_SD.make_edge srcloc targetloc (C_SD.Command.gather_accesses (C_SD.Command.FunctionCall (
		    E.FunctionCall ((E.Lval (E.Symbol "__BLAST_Couldnt_Disambiguate_Fptr")),args))
		  ))]
                  | _ as fps -> (* edge :: *) ( List.map (fun fname ->
                    C_SD.make_edge srcloc targetloc
                            (C_SD.Command.gather_accesses 
                               (C_SD.Command.GuardedFunctionCall (gen_eq fname fcexp, 
                                           E.Assignment(aop, target, E.FunctionCall(E.Lval fname, args)) ) )))
                         fps )
                 end
                else [ edge ]
            | _ -> failwith "enabled_ops: Internal invariant broken! Strange function call!" 
          end
      | _ ->  [ edge ]

      
let lattice_enabled_ops reg =
  match reg with
    Region.Atomic atomic_reg ->
      if O.getValueOfBool "events" && C_SD.is_dispatch_loc (location_coords_of_region reg) then 
        let loc = location_coords_of_region reg in
        List.map (C_SD.make_edge loc loc) (DataLattice.enabled_ops (lattice_of_region reg)) 
      else []
  | _ -> []


let async_proc reg op = 
  if O.getValueOfBool "events" && C_SD.isAsyncCall op 
  then make_async_op op (Region.get_adr reg) else op
    
let enabled_ops ?(prepare_alias = (fun () -> ())) reg =
    let ops = 
      match reg with
          Region.Empty -> []
	| Region.Atomic atomic_reg -> C_SD.get_outgoing_edges atomic_reg.Region.location
        | Region.AtomicTI atomicTI_reg -> C_SD.get_outgoing_edges atomicTI_reg.Region.ti_location
	| Region.ParallelAtomic _ -> failwith "enabled_ops: par atomic unimplemented"
	| Region.Union _ -> failwith "enabled_ops: union unimplemented"
	| Region.ParallelUnion _ -> failwith "enabled_ops: parallel union unimplemented" in
    let ops = ops @ (lattice_enabled_ops reg) in 
    let ops = if (O.getValueOfBool "nofp") then ops
             else List.fold_right (fun e acc -> (disambiguate_function_pointers prepare_alias e @ acc)) ops [] in
    let ops = if (O.getValueOfBool "skipfun") then List.map skip_fun_process ops else ops in
    let ops = if (O.getValueOfBool "events") then List.map (async_proc reg) ops else ops in
    ops
