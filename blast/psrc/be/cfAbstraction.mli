(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * This module implements the abstraction interface
 * connecting the model checker to the concrete transition system.
 * It defines the abstract pre and post, and various routines for
 * focus, shrink, etc used by the model checker.
 *) 


open BlastArch


module Make_C_Abstraction :
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
  sig
    include ABSTRACTION
    (* val build_custom_abstraction : (((string * ((Ast.Expression.expression * bool) list)) list) 
				      * ((Ast.Expression.expression * (Ast.Expression.expression * bool) list) list option) 
				      * ((Ast.Expression.expression*string) list))-> unit
    val initKill : C_SD.location option -> Ast.Predicate.predicate list -> unit
*)	
    (* create a region: takes a location, a flag that sets the stack (flag = true -> EveryStack, flag = false -> [])
	the set of seed predicates, the initial state and returns a region
	*)
      (** As the name suggests, this sets up the abstraction by loading in predicates and such *)
    val initialize_abstraction : unit -> unit
    val save_region_abs : Region.t -> unit

    val dump_abs : unit -> unit
    val create_region : C_SD.location list -> bool -> Ast.Predicate.predicate -> Ast.Predicate.predicate -> Region.t
    val print_stats : Format.formatter -> unit -> unit
    val check_races : Ast.Expression.lval option ref
    val assumed_invariants : Ast.Predicate.predicate list ref
    val assumed_invariants_region : Region.t ref
    val assumed_invariants_list_to_region : unit -> Region.t
  end with type Operation.t = C_SD.edge
       and type Operation.exp = Ast.Expression.expression


