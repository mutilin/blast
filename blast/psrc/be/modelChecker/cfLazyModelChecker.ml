(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)



(**
 * This module implements the lazy model checking algorithm.
 *)

open Ast
open BlastArch

(* [Greg] Very dirty hack because we renamed Stats to VampyreStats in
   vampyre...  We should clean this stats business anyway. *)
module Stats = Bstats

module Make_Lazy_Model_Checker =
  functor(Abstraction : ABSTRACTION with type Operation.exp = Expression.expression) ->
struct
  module Abstraction = Abstraction

  module Region = Abstraction.Region
(*  module AR = Abstraction.AR *)
  module Operation = Abstraction.Operation

  module Tree = BiDirectionalLabeledTree

  type error_path = (Region.t * (Operation.t list) * Region.t)

  let fprintf = Format.fprintf Format.std_formatter

  let valOf = function
      None -> failwith "valOf"
    | Some v -> v

  module IntMap = Map.Make(struct
    type t = int
    let compare = compare
  end)

  type node_set = tree_node IntMap.t

  and 'a node_map = (tree_node * 'a) IntMap.t

  and summary_path =
    | ListPath of tree_node list
    | ConsPath of summary_path * summary_path

  and pt_data = summary_path

  (* Information on a function entry point *)
  and entry_node = {
      first_call_site : tree_node option  ;
      mutable call_sites : Region.t node_map; (* Possible call sites that lead to this
                                               * entry state, mapped to their entry regions *)
      mutable exit_pts : pt_data node_map (* Known reachable exit points from this
					   * entry point, mapped to the paths to them *)
    }

  (* Information on a function return point *)
  and exit_node = {
      mutable entry_pts : pt_data node_map (* Known reverse-reachable entry points from
					    * this point, mapped to the paths to them *)
    } 

  (* Distinctions between nodes, to be used in keeping function summaries *)
  and node_kind =
    | Node                (* Node unrelated to function entry or exit *)
    | Entry of entry_node (* A possible beginning of a function body execution *)
    | Exit of exit_node   (* A possible return point from a function *)

  (**
   * Information that interests us about a dag node.
   * The time stamp ts of a processed covered node indicates that this node
   * is covered by processed uncovered nodes of time stamp lesser than ts.
   * Conversely, the time stamp ts of processed uncovered node indicates
   * that this node may partially cover any processed covered of time stamp
   * greater than ts.
   *)
  and marking_data = {
      mutable time_stamp      : int ;
      mutable region          : Region.t ;
      mutable call_time_stamp : int option        (* Time stamp of entry point in any
					           * summary edged child of this node,
              					   * or 0 otherwise *)
    }

  and marking =
      (* for leaves that have never been processed *)
      Unprocessed
      (* for (already processed) error-free leaves that are covered by
         previously processed (cf. time stamps) nodes *)
    | Processed_Covered of marking_data
      (* for (already processed) error-free nodes that were not covered
         by previously processed nodes at the time they were last
         processed *)
    | Processed_Uncovered of marking_data
      (* for (already processed) error-free leaves that were covered but
         by previously processed nodes that have been refined, and hence
      have to be re-processed *)
    | Processed_Was_Covered_To_Reprocess of marking_data

  and node_data = {
    id           : int ;
    mutable mark : marking ;
    kind         : node_kind
  }

  and summary_data = {
      path : summary_path ;
      n_in : tree_node ;
      n_out : tree_node
    }

  and edge =
    | SummaryEdge of summary_data
    | PathEdge of Operation.t

  (* The complete type of a dag node *)
  and tree_node = (node_data, edge) Tree.node

  and flat_tree_node = (node_data, Operation.t) Tree.node

  let print_marking_data fmt md =
    Format.fprintf fmt "@[time_stamp=%d;@ region=" md.time_stamp ;
    Region.print fmt md.region ;
    Format.fprintf fmt "@]"

  let rec print_marking fmt = function
      Unprocessed ->
        Format.fprintf fmt "@[Unprocessed@]"
    | Processed_Covered md ->
        Format.fprintf fmt "@[Processed_Covered(" ;
        print_marking_data fmt md ;
        Format.fprintf fmt ")@]" ;
    | Processed_Uncovered md ->
        Format.fprintf fmt "@[Processed_Uncovered(" ;
        print_marking_data fmt md ;
        Format.fprintf fmt ")@]" ;
    | Processed_Was_Covered_To_Reprocess md ->
        Format.fprintf fmt "@[Processed_Was_Covered_To_Reprocess(" ;
        print_marking_data fmt md ;
        Format.fprintf fmt ")@]"

  let rec print_marking_short fmt = function
      Unprocessed ->
        Format.fprintf fmt "@[Unprocessed@]"
    | Processed_Covered md
    | Processed_Uncovered md
    | Processed_Was_Covered_To_Reprocess md ->
        Format.fprintf fmt "@[" ;
        Region.print fmt md.region ;
        Format.fprintf fmt "@]"
  
  and print_node_data fmt nd =
    Format.fprintf fmt "@[Data(@[id=%d;@ kind=" nd.id ;
    (match nd.kind with
      Node -> Format.fprintf fmt "Node"
    | Entry _ -> Format.fprintf fmt "Entry"
    | Exit _ -> Format.fprintf fmt "Exit");
    Format.fprintf fmt ";@ mark=";
    print_marking fmt nd.mark;
    (*print_marking fmt nd.mark ;*)
    Format.fprintf fmt "@])@]"

  and print_option f fmt n =
    match n with
      None -> Format.fprintf fmt "NONE"
    | Some v -> f v

  and print_node fmt n =
    Format.fprintf fmt "@[Node(@[edge=";
    (match Tree.get_parent_edge n with
      None -> Format.fprintf fmt "NONE"
    | Some eg ->
	(match Tree.get_edge_label eg with
	| PathEdge v -> Operation.print fmt v
	| SummaryEdge {n_out = n_out} ->
	    Format.fprintf fmt "SUMMARY ";
	    print_node fmt n_out));
    Format.fprintf fmt ",@,data=";
    print_node_data fmt (Tree.get_node_label n);
    Format.fprintf fmt "@])@]"

(************** PRINTING SEQUENCES OF REGIONS/OPERATIONS *****************)

let vanilla_print_op_l op_l = 
  let print_op op = 
    let (l1,l2,rv) = Abstraction.op_to_string op in
    Message.msg_string Message.Debug (Printf.sprintf "%d :: %s :: %d \n\n" l1 rv l2) 
 in
  Message.msg_string Message.Debug "Printing Trace:";
  List.iter print_op op_l

let digest_trace (b,op_l,reg_l) = 
  let creg_l = List.map Region.concretize reg_l in
  let check_o2s x = 
    let (l1,l2,rv) = Abstraction.op_to_string x in
    let s = Printf.sprintf "%d :: %s :: %d \n\n" l1 rv l2
    in
         (s,(l1,l2,rv))
 in
let _l = List.map check_o2s op_l in
let op_string = String.concat "\n ----> \n" (List.map fst _l) in
let llv_l = List.map snd _l in
let string_creg_l = List.map Abstraction.reg_to_strings creg_l in
let trace_string_1 = ("Start trace \n "^(op_string)^"End trace\n") in
let trace_string_2 = ("Start Regions \n"^
		      (String.concat "\n ---r---> \n"
			 (List.map (fun (x,_) -> Misc.strList x) string_creg_l))
		      ^"End Regions \n")
in
let trace_string = trace_string_1^"\n"^trace_string_2 in
  Message.msg_string Message.Debug trace_string;
    (trace_string , (b,llv_l,string_creg_l))

let ac_dumptrace tracefile textfile (trace_string,output_trace_triple) =
  let trace_chan = open_out_bin tracefile in
    Marshal.to_channel trace_chan output_trace_triple [];
 close_out trace_chan;
 Misc.append_to_file textfile trace_string
  
 
(* trace_triple = (b,op_l,reg_l) *)
let dumptrace trace_triple = 
  ac_dumptrace 
    ((Options.getValueOfString "mainsourcename")^".btr")
    ((Options.getValueOfString "mainsourcename")^".traces")
    (digest_trace trace_triple)

(*****************************************************************************************)
    
(******************************* Data Structures for TREE ********************************)
      
  let tree_node_table = Hashtbl.create 1009

  (* helper functions to access a tree node's data *)
  let get_node_data n = Tree.get_node_label n

  let edge_to_op e =
    match e with
      PathEdge op -> op
    | _ -> failwith "Summary edge in edge_to_op/get_op"

  let get_edge_op e = edge_to_op (Tree.get_edge_label e)

  let get_op n =
    match (Tree.get_parent_edge n) with
        Some e -> get_edge_op e
      | None ->
          invalid_arg "get_op: input node has no parent edge"

  let get_parent n =
    match (Tree.get_parent_edge n) with
        Some(e) ->
          (Tree.get_source e)
      | None ->
          invalid_arg "get_parent: input node has no parent edge"

  let get_parent_edge n =
    match (Tree.get_parent_edge n) with
        Some e ->
          Tree.get_edge_label e
      | None ->
          invalid_arg "get_parent_edge: input node has no parent edge"    

  let get_children n = Tree.get_children n

  let get_id n = (get_node_data n).id

  let get_marking n = (get_node_data n).mark

  let get_marking_data n =
    match (get_marking n) with
        Unprocessed ->
          invalid_arg "get_marking: input node is unprocessed"
      | Processed_Covered md
      | Processed_Uncovered md
      | Processed_Was_Covered_To_Reprocess md ->
          md

  let get_time_stamp n =
    try (get_marking_data n).time_stamp
    with Invalid_argument "get_marking: input node is unprocessed" ->
      invalid_arg "get_time_stamp: input node is unprocessed"

  let min n1 n2 =
    if n1 < n2 then
      n1
    else
      n2

  let max n1 n2 =
    if n1 > n2 then
      n1
    else
      n2

  let update_caller n_call ts =
    let md = get_marking_data n_call in
    md.call_time_stamp <- ts

  let called_after md ts =
   match md.call_time_stamp with
     None -> true
   | Some cts -> cts >= ts

  (*let print_tree_node fmt n =
    Format.fprintf fmt "@[TreeNode(@[label=" ;
    print_node_data fmt (get_node_data n) ;
    begin
      match (Tree.get_parent_edge n) with
          None ->
            Format.fprintf fmt ";@ no parent"
        | Some(e) ->
            Format.fprintf fmt ";@ incoming_op=" ;
            Operation.print fmt (Tree.get_edge_label e) ;
            Format.fprintf fmt ";@ parent_id=%d"
              (get_id (Tree.get_source e))
    end ;
    let children_ids = List.map get_id (Tree.get_children n)
    in
      Format.fprintf fmt ";@ children_ids=" ;
      Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
      Format.fprintf fmt "@])@]"*)

  let print_tree_node = print_node

  let node_equal n1 n2 = get_id n1 = get_id n2

  let get_kind n = (Tree.get_node_label n).kind

  let get_entry n =
    match get_kind n with
      Entry en -> en
    | _ -> failwith "Non-Entry in get_entry"

  let get_exit n =
    match get_kind n with
      Exit ex -> ex
    | _ -> failwith "Non-Exit in get_entry"

  let get_region n =
    try (get_marking_data n).region
    with Invalid_argument "get_marking: input node is unprocessed" ->
      invalid_arg ("get_region: input node is unprocessed")

  let get_region_opt n =
    match (get_marking n) with
        Unprocessed -> None
      | Processed_Covered md
      | Processed_Uncovered md
      | Processed_Was_Covered_To_Reprocess md ->
          Some md.region

  (* Sets of tree_node's *)

  type nset = tree_node IntMap.t

  let nset_empty = IntMap.empty

  let nset_add set n = IntMap.add (get_id n) n set

  let nset_mem set n = try let _ = IntMap.find (get_id n) set in true with Not_found -> false

  let nset_remove set n = IntMap.remove (get_id n) set

  let nset_iter f set = IntMap.iter (fun _ n -> f n) set

  let nset_map f set = IntMap.map (fun _ n -> f n) set

  let nset_fold f s set = IntMap.fold (fun _ n s -> f s n) set s

  let nset_choose set = nset_fold (fun s n ->
    match s with
      None -> Some n
    | s -> s) None set

  let nset_filter f set = nset_fold (fun set n ->
    if f n then
      nset_add set n
    else
      set) nset_empty set

  let nset_join s1 s2 = nset_fold nset_add s1 s2

  let nset_to_list = nset_fold (fun t h -> h::t) []

  let nset_size set = nset_fold (fun n _ -> n+1) 0 set

  (* Maps from tree_node's *)

  let nmap_empty = IntMap.empty

  let nmap_add map n v = IntMap.add (get_id n) (n, v) map

  let nmap_mem map n = try let _ = IntMap.find (get_id n) map in true with Not_found -> false

  let nmap_remove map n = IntMap.remove (get_id n) map

  let nmap_iter f map = IntMap.iter (fun _ (n, v) -> f n v) map

  let nmap_map f map = IntMap.map (fun _ (n, v) -> (n, f v)) map

  let nmap_fold f s map = IntMap.fold (fun _ (n, v) s -> f s n v) map s

  let nmap_filter f map = nmap_fold (fun map n v ->
    if f n v then
      nmap_add map n v
    else
      map) nmap_empty map

  let nmap_join s1 s2 = nmap_fold nmap_add s1 s2

  let nmap_to_list = nmap_fold (fun t n v -> (n, v)::t) []

  let nmap_size map = nmap_fold (fun n _ _ -> n+1) 0 map

  let nmap_isEmpty map =
    try
      nmap_fold (fun _ -> raise Not_found) () map;
      true
    with Not_found -> false

  let nmap_choose f map = nmap_fold (fun s n v ->
    match s with
      None -> if f v then Some n else None
    | s -> s) None map

  (* helper class to create nodes with appropriate ids *)
  class tree_node_creator =
  object
    val mutable next_id = 0
    method create_root m =
      let this_id = next_id in
        next_id <- next_id + 1 ;
	(* RJ: this is the only entry node where the first call site = None *) 
        let entry = {first_call_site = None; call_sites = nset_empty; exit_pts = nset_empty} in
        let rv = ((Tree.create_root { id  = this_id ;
                             mark = m; kind = Entry entry}) : tree_node) 
	in
	  Hashtbl.add tree_node_table this_id (ref rv);
	  rv

    method create_child k m op par =
      let this_id = next_id in
        next_id <- next_id + 1 ;
	let rv = 
          ((Tree.create_child { id  = this_id ;
                              mark = m; kind = k } op par) : tree_node)
	in
	  Hashtbl.add tree_node_table this_id (ref rv);
	  rv

	(* Jhala: It would be nice to have a destroy as well -- or are we relying on some sort of Garbage collection to handle that ? *)
    method delete_children par = 
      Tree.delete_children (par : tree_node)

  end

  (* Reachable entry & exit point helpers for summary edge creation *)

  (*let entry_pts_node n =
    let rec backup stack_depth n path =
      (*fprintf "Consider parent ";
      print_node Format.std_formatter n;
      fprintf "@.";*)

      match Tree.get_parent_edge n with
	None -> [(n, n::path)]
      |	Some _ ->
	  let parent = Tree.get_parent n in
	  (match get_kind n with
	    Node -> backup stack_depth parent (n::path)
	  | Entry _ ->
	      if stack_depth >= -1 then
		[(n, n::path)]
	      else
		backup (stack_depth+1) parent (n::path)
	  | Exit _ -> backup (stack_depth-1) parent (n::path))
    in backup 0 n []*)

  let entry_pts_node n =
    let rec backup n path =
      match Tree.get_parent_edge n with
	None -> [(n, ListPath (n::path))]
      |	Some _ ->
	  let parent = Tree.get_parent n in
	  (*fprintf "Consider: ";
	  print_node Format.std_formatter parent;
	  fprintf "@.";*)
	  (match get_kind n with
	    Entry _ -> [(n, ListPath (n::path))]
	  | _ -> backup parent (n::path))
    in backup n []

  let single_child n =
    (match Tree.get_children n with
      [ch] -> ch
    | _ -> failwith "More or less than one child unexpected")

  let single_child_edge n =
    (match Tree.get_parent_edge (single_child n) with
      None -> failwith "No edge for single_child_edge"
    | Some ed -> Tree.get_edge_label ed)

  let parent_edge n =
    (match Tree.get_parent_edge n with
      None -> failwith "No parent edge in parent_edge"
    | Some ed -> Tree.get_edge_label ed)

  (*let exit_pts_node n = 
    let rec search stack_depth path acc n =
      match (Tree.get_node_label n).kind with
	Node -> List.fold_left (search stack_depth (n::path)) acc (Tree.get_children n)
      | Entry _ -> List.fold_left (search (stack_depth+1) (n::path)) acc (Tree.get_children n)
      | Exit _ ->
	  if stack_depth <= 0 then
	    (n, List.tl (List.rev (n::path)))::acc
	  else
	    List.fold_left (search (stack_depth-1) (n::path)) acc (Tree.get_children n)
    in search 0 [] [] n*)

  let tlSafe = function
      [] -> []
    | _::t -> t

  let exit_pts_node n =
    let rec search path acc n =
      match get_kind n with
	Exit _ -> (n, ListPath (tlSafe (List.rev path)))::acc
      |	_ -> List.fold_left (search (n::path)) acc (Tree.get_children n)
    in search [] [] n

  exception RestartException

  (* statistic variables *)
  let stats_nb_iterations = ref 0
  let stats_nb_iterations_of_outer_loop = ref 0 (* added for races *)
  let stats_nb_created_nodes = ref 0
  let stats_nb_refinment_processes = ref 0
  let stats_nb_refined_nodes = ref 0
  let stats_nb_proof_tree_nodes = ref 0
  let stats_nb_proof_tree_covered_nodes = ref 0
  let stats_nb_deleted_nodes = ref 0
  let stats_nb_funs_entered = ref 0
				
  let reset_stats =
    stats_nb_iterations := 0 ;
    stats_nb_refinment_processes := 0 ;
    stats_nb_created_nodes := 0 ;
    stats_nb_refined_nodes := 0 ;
    stats_nb_proof_tree_nodes := 0;
    stats_nb_proof_tree_covered_nodes := 0

  let print_stats fmt () =
    Format.fprintf fmt "@[<v>@[Nb iterations of outer while loop:@ %d@]" !stats_nb_iterations_of_outer_loop ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[<v>@[Nb iterations of reachability:@ %d@]" !stats_nb_iterations ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb created nodes:@ %d@]" !stats_nb_created_nodes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb refinment processes:@ %d@]" !stats_nb_refinment_processes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb refined nodes:@ %d@]" !stats_nb_refined_nodes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb proof tree nodes:@ %d@]" !stats_nb_proof_tree_nodes;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb proof tree covered nodes:@ %d@]@]" !stats_nb_proof_tree_covered_nodes;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb deleted nodes:@ %d@]@]" !stats_nb_deleted_nodes;
    ()

  (************************************************************************************************)
  (** Model checker data types **)
  (*type model_check_outcome =
    | No_error
    | Error_reached of error_path
    | Race_condition of error_path * error_path * Ast.Expression.lval * Ast.Expression.lval
    | Bad_lock_spec of error_path * Ast.Expression.lval*)

  include BlastArch.ModelCheckOutcome
  type model_check_outcome = error_path outcome

  exception Exit_from_check_concurrent_access of model_check_outcome
	  
  type 'a currentApproximation = 
      { 
      read_whole : Region.t ;
      write_whole : Region.t ;
      read_accesses : (('a * Region.t) list) ; (* the 'a is a tree node *)
      write_accesses : (('a * Region.t) list) (* the 'a is a tree node *)
      } 


  type raceKind =      Read | Write | Both | NoDistinction

  exception RaceConditionException of model_check_outcome 


  (******************************************************)

  (**************************************************************************************************)
  (** Data structures to hold the phi's for lvalues                                                **)

  let global_variable_table = Hashtbl.create 31
  let iter_global_variables f = Hashtbl.iter f global_variable_table 
  let get_global_var_phi sym = 
    try
      Hashtbl.find global_variable_table sym
    with Not_found ->
      (* this lvalue has not been seen so far. So return an empty phi. *)
      ref Region.bot

  let global_lock_asm_table = Hashtbl.create 31
  (* the last is because the global_variable_table gets messed with too often *)
				 

  (**************************************************************************************************)

  (* the following takes a list of (lval,predicate) and adds it to the above table *)

  let process_env_assumptions ll =
    (* initialize all environment assumptions to false *)
    let _ = Abstraction.iter_global_variables 
	      (fun name -> 
		 Hashtbl.add global_variable_table name (ref Region.bot)
	      ) 
    in  
      (* now add the stuff about the interesting locks ... *)                     
    let add (l,p) = 
      Hashtbl.replace global_variable_table l (ref (Abstraction.predicate_to_region p));
      Hashtbl.replace global_lock_asm_table l (Abstraction.predicate_to_region p)
    in
      List.iter add ll

  let print_phis verbose_flag = 
    let phi_print flag name ref_phi = 
      if (flag) then
	begin
	  Message.msg_string Message.Debug 
	    ("Variable " ^ (Ast.Expression.lvalToString name) ^ 
	     "\nPhi =\n" ^ (Region.to_string !ref_phi) ^ "\n\n"); 
	  ()
	end
      else
	begin
	  if !ref_phi = Region.bot 
	  then () 
	  else
	    (Message.msg_string Message.Debug 
	      ("Variable " ^ (Ast.Expression.lvalToString name) ^ 
	       "\nPhi =\n" ^ (Region.to_string !ref_phi) ^ "\n\n"); 
	     ())
	end
    in
    iter_global_variables (phi_print verbose_flag);
    "Done"



  type error_check =
      EmptyErrorAtNode of Region.t * tree_node * (tree_node list) * (tree_node list)
    | NonEmptyErrorAtRoot of Region.t * tree_node * (tree_node list)

  (**
   * Checks whether a reached error region corresponds to a real error
   * path.  This function takes a node and an error region contained in
   * the node's region as arguments, and does a backward pre computation
   * from that node, along the branch from the root to that node, until
   * it gets an empty region or reaches the root.  It returns the obtained
   * region at the reached ancestor along with the path from this ancestor
   * to the initial node.
   * 
   * @param n     a node of the reachability tree
   * @param err_n a non-empty error region contained in n.region
   * @return a value of type error_check
   *)

  let is_entry node =
    match get_kind node with
      Entry _ -> true
    | _ -> false

  let get_outgoing node =
    match get_children node with
      [ch] ->
	(match Tree.get_edge_label (valOf (Tree.get_parent_edge ch)) with
	  PathEdge op -> op
	| _ -> failwith "Summary edge in get_outgoing")
    | _ -> failwith "Multiple children in get_outgoing"
	    
  (* do the block-trace analysis, this will also add predicates wherever (locations) required 
     of course we want this function to return the same sort of thing that the original (i.e.
     below function returns :: EmptyErrorAtNode(reg, *)
  let rec get_incoming_operation n =
    match Tree.get_parent_edge n with
	None -> failwith "Bad call to get_incoming_operation"
      | Some e ->
	  begin
	    match Tree.get_edge_label e with
		PathEdge op -> op
	      | SummaryEdge _ ->
		     (* INV: this shouldnt happen -- the last node in a summarypath should be a return node,
		        i.e. its parent should be a pathedge labelled return *)
		  failwith ("Last node is SummarySuccessor!")
	  end
    
	    
  let rec expand_summary_path = function
      ListPath l -> l
    | ConsPath (l1, l2) -> (expand_summary_path l1) @ (expand_summary_path l2)

  let expand_path_with_old path =
    let rec expand_path par path acc =
      match path with
	[] -> (acc, par)
      |	node::rest ->
	  (match par with
	    None ->
	      let node = Tree.create_root node in
	      expand_path (Some node) rest (node::acc)
	  | Some par ->
	      (match Tree.get_parent_edge node with
		None -> failwith "Expected incoming edge in expand_path"
	      |	Some ed ->
		  (match Tree.get_edge_label ed with
		    PathEdge _ as ed ->
		      let node = Tree.create_child node ed par in
		      expand_path (Some node) rest (node::acc)
		  | SummaryEdge {path = spath; n_out = n_out; n_in = n_in} ->
		      Message.msg_string Message.Debug "Expanding a summary edge";
		      let spath = expand_summary_path spath in
			List.iter (fun s -> 
				     Message.msg_string Message.Debug "Node: ";
				     Message.msg_printer Message.Debug print_node s;
				  ) 
			  spath;
		      let (acc, par) = expand_path (Some par) spath acc in
		      (*let acc = List.tl acc in*)
		      let node = Tree.create_child node (get_parent_edge n_out) (valOf par) in
			expand_path (Some node) rest (node::acc)))) in
    let (path, _) = expand_path None path [] in
      List.rev path

  let expand_path path =
    let oldRoot = List.hd path in
    let path = expand_path_with_old path in
    let (root, rest) = (List.hd path, List.tl path) in
    let get_label n = Tree.get_node_label (Tree.get_node_label n) in
    let root =
      match Tree.get_parent_edge oldRoot with
	None -> Tree.create_root (get_label root)
      |	Some ed ->
	  let root' = Tree.create_root (get_label root) in
	  Tree.create_child (get_label root) (Tree.get_edge_label ed) root' in
    let rec expand last path acc =
      match path with
	[] -> List.rev acc
      |	first::rest ->
	  let n = Tree.create_child (get_label first) (parent_edge first) last in
	  expand n rest (n::acc) in
    expand root rest [root]

  let path_to_root n =
    let rec backup n acc =
      Message.msg_string Message.Debug "path_to_root: ";
      Message.msg_printer Message.Debug print_node n;
      
      match get_kind n with
	Entry en ->
	  let reg = get_region n in
	    (match en.first_call_site with
		 None ->
		   Message.msg_string Message.Debug "path_to_root terminates; no matching call site";
		   n::acc
	       | Some n_call -> backup n_call (n::acc))
	|	_ -> backup (Tree.get_parent n) (n::acc) in
      backup n []

  let check_error (node : tree_node) error_at_node =
    let r_path = path_to_root node in

    List.iter (fun n -> Message.msg_printer Message.Debug  print_node n) r_path;
    (* let _ = Message.msg_string Message.Error ("counterex. size:"^(string_of_int (List.length r_path))) in
    print_string "Expanding....\n"; *)
    let r_path = expand_path_with_old r_path in
    Message.msg_string Message.Debug "Done expanding.\n";

    List.iter 
      (fun n -> Message.msg_printer Message.Debug print_node (Tree.get_node_label n)) 
      r_path;

    (*print_string "COUNTEREX:\n";
    List.iter (print_node Format.std_formatter) r_path;*)
    (* let  _ = Message.msg_string Message.Error ("counterex. size:"^(string_of_int (List.length r_path))) in *)
  
    match Options.getValueOfBool "block" with
	true -> 
	  begin
	    let r_path_flat = List.map Tree.get_node_label r_path in

            let reg_l = ((List.map (get_region) (Misc.chop_last r_path_flat))@[error_at_node] )
	    in

	    let id_l = List.map get_id r_path_flat in 

	    let rec get_ops ns acc =
	      match ns with
		[] -> List.rev acc
	      |	[n] -> List.rev (get_op n :: acc)
	      |	n1::((n2::rest) as rest') ->
		  match Operation.get_info (get_op n2) with
		    Operation.Call _ ->
		      let caller_region = get_region (Tree.get_node_label n1) in
		      let call_op = List.hd (Abstraction.enabled_ops caller_region) in
		      get_ops rest (call_op :: get_op n1 :: acc)
		  | _ -> get_ops rest' (get_op n1 :: acc) in

	    Message.msg_string Message.Debug  "r_path";
	    List.iter 
	      (fun n -> 
		 (Message.msg_printer Message.Debug print_node (Tree.get_node_label n))) 
	      r_path;
	    let op_l = get_ops (List.tl r_path) [] in
	    (*Format.printf "op_l =@.";
	      List.iter (fun op -> (Operation.print Format.std_formatter op;
				  Format.printf "@.")) op_l;*)
	    let index = Abstraction.getCurrentPredIndex () in
	      match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l op_l) id_l) with
		  (-1,a,_) -> 
		    (* the assumption is that -1 indicates it goes all the way to the top *)
		    let (root::path) = r_path_flat in (* should never fail ! *)
		      NonEmptyErrorAtRoot(a,root,path)
		| (i,a,b) ->
		    (* "i" is the number of hops from the root the falsehood comes *)
		    (* no no no ... we may have added some predicate to some other function *)
		    if (not b) (* index = Abstraction.getCurrentPredIndex () *) then
		      begin
			Message.msg_string Message.Major "No new predicates for this trace!";
			Message.msg_string Message.Major
			  ("See last trace in "^(Options.getValueOfString "mainsourcename")^".traces");
			dumptrace (false,op_l,reg_l);
			failwith "No new predicates found."
		      end;
		    let (err_path,n',path) = Misc.list_cut' i (r_path_flat) in
		      EmptyErrorAtNode(a,n',path,err_path)
          end
      | false ->
	  (*begin
	    let rec backward_pre n (err_n,subs_err_n) path =
	      match (Tree.get_parent_edge n) with
		  Some(e) ->
		    (* f is n's father and op labels the edge f --> n *)
		    let f = Tree.get_source e
		    and op = Tree.get_edge_label e in
		    let f_reg = get_region f in
		      (* the error subregion at f *)
		      (* JHALA: this subregion business is rubbish. It only blows up the
			  formulas. Not required *)
		      (* JHALA: MAJOR CHANGE! dropping the "capping" in the recursive call *)
	
		    (* JHALA: temporarily comment out!::  let _ = Abstraction.setWPCapRegion f_reg in *)
		    let pre_err_f = (Abstraction.pre err_n op global_variable_table) in
		    let err_f = Region.cap f_reg pre_err_f in
		    let pre_subs_err_f = (Abstraction.spre subs_err_n op global_variable_table) 
		    in

		    let subs_err_f = Region.cap f_reg pre_subs_err_f in
		    (* JHALA: temporarily comment out!:: let _ = Abstraction.resetWPCapRegion () in *)
		      (*let check_emptiness_flag = Abstraction.check_emptiness op in*)
		      if ((*check_emptiness_flag &&*) Stats.time "Emptiness Check:" Region.is_empty subs_err_f)   
		      then
			begin
			  (*Message.msg_printer Message.Error print_tree_node f;*)
			EmptyErrorAtNode(err_f, f, n::path, n::path)
			end
		      else
			backward_pre f (pre_err_f,pre_subs_err_f) (n::path)
		| None ->
		    (* n is the root *)
		    NonEmptyErrorAtRoot(err_n, n, path)
	    in
	    let prec_err_n = Abstraction.precise error_at_node in
	    let rv = backward_pre node (prec_err_n,prec_err_n) [] in
	      (* double checking *)
	      (*
	    let _ = 
	      begin
		let reg_l = 
		  ((List.map (get_region)(Misc.chop_last r_path))@[error_at_node] )
		in
		let op_l = List.map (get_op) ( List.tl r_path) in
		  match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l) op_l) with
		      (-1,a) -> 
			(* the assumption is that -1 indicates it goes all the way to the top *)
			let (root::path) = r_path in (* should never fail ! *)
			  NonEmptyErrorAtRoot(a,root,path)
		    | (i,a) ->
			(* "i" is the number of hops from the root the falsehood comes *)		     
			let (n',path) = Misc.list_cut i (r_path) in
			let _ = Message.msg_printer Message.Error print_tree_node n' in
			  EmptyErrorAtNode(a,n',path,path)
              end
	    in *)
	      rv
	  end*) failwith "block = false not implemented"

  (*let check_error_fwd node error_at_node  =
    let rec get_path node path proj_path =
      match (Tree.get_parent_edge node) with
        Some (e) ->
            (* f is node's father and op labels the edge f --> node *)
          let f = Tree.get_source e and op = Tree.get_edge_label e
          in 
          get_path f ((f, Some op):: path) (f::proj_path)
      | None -> (path, proj_path)
    in
    let (error_path, path_projected_on_tree_nodes) = get_path node [ (node, None) ] [ node ]
    in
    Message.msg_string Message.Debug "Check error forward called with" ;
    Message.msg_string Message.Debug "Path = ";
    List.iter (function node -> Message.msg_printer Message.Debug print_tree_node node) path_projected_on_tree_nodes ;
    Message.msg_string Message.Debug "end of path\n\n\n" ;
    let root = match path_projected_on_tree_nodes with
      h :: _ -> h
    | _ -> failwith "Empty error path in check_error_fwd"
    in
    let reg_true = Abstraction.precise (get_region root)
    in
    (* now do a concrete post on the error path *)
    let rec forward_post current_reg path =
      (*Message.msg_string Message.Debug ("\n\nforward post called with region "^(Region.to_string current_reg));
	*)
      match path with
        [] -> (* Valid error *) 
	  let r = (Region.cap (Abstraction.trim_region_to_globals current_reg) error_at_node) in
	  if (Region.is_empty r) then
	    failwith "Rupak: this case not handled yet"
	  else
	    NonEmptyErrorAtRoot (r , root, List.tl path_projected_on_tree_nodes)
      | (f, op) :: rest ->
            begin
              match op with
		None -> (* reached the node where the error is without trouble *)
		  (* check if satisfiable *)
		  let err_f = Region.cap (Abstraction.trim_region_to_globals error_at_node) 
					 (Abstraction.trim_region_to_globals current_reg) in
		  if (Stats.time "Emptiness Check (Forward):" Region.is_empty err_f) then
		    begin
		      Message.msg_string Message.Debug "Emptiness check succeeds at end of path: this is a false error";
		      check_error f error_at_node
		    end
		  else
		    begin
		      Message.msg_string Message.Debug "Emptiness check fails at end of path: this is a true error";
		      NonEmptyErrorAtRoot ( err_f , root, List.tl path_projected_on_tree_nodes)
		    end
	      |	Some oper ->
		  begin
		    (* do a strongest postcondition computation.
		       if oper is an assume, do a satisfiability check
		       (find predicates if unsatisfiable).
		       otherwise recursively call forward_post on the rest of the path
		       *)
		    Message.msg_string Message.Debug ("Now doing post of "^(Operation.to_string oper)) ;
		    let next_reg = Abstraction.post current_reg oper global_variable_table 
		    in
(*
		    if (Abstraction.check_emptiness oper) then 
*)
		    if (true) then
		      if (Stats.time "Emptiness Check (Forward):" Region.is_empty next_reg) then
			begin
			  Message.msg_string Message.Debug "Emptiness check succeeds: this is a false error";
			  let nd = match rest with
			    (h, _) :: _ -> h
			  | _ -> failwith "strange error path"
			  in
			  try
			    check_error nd (get_region nd)
			  with Invalid_argument _ -> (* This node was marked Unprocessed *)
			    begin (* get region in the tedious way *)
			      let reg = Abstraction.post (get_region (get_parent nd)) (get_op nd) global_variable_table in
			      (get_node_data nd).mark <- 
				 Processed_Was_Covered_To_Reprocess { time_stamp = 0; region = reg } ;
			      check_error nd (reg)
			    end
			end
		      else
			forward_post next_reg rest
		    else
		      forward_post next_reg rest
		  end
            end
    in
    (* forward simulate starting from root and True *)
    forward_post reg_true error_path*)


  (* Greg: I don't like this get_op... try to add the operations in the path? *)



  let rec refine_path anc path =
    match path with
        []   -> ()
      | n::p ->
          stats_nb_refined_nodes := !stats_nb_refined_nodes + 1 ;

          Message.msg_string Message.Debug "Refining node:" ;
          Message.msg_printer Message.Debug print_tree_node n ;
          Message.msg_string Message.Debug "whose ancestor is:" ;
          Message.msg_printer Message.Debug print_tree_node anc ;

          let anc_reg = get_region anc
          and n_marking_data = get_marking_data n in

            Message.msg_string Message.Debug "calling Abstraction.post with region:" ;
            Message.msg_string Message.Debug ("    region    : " ^ (Region.to_string anc_reg)) ;
            Message.msg_string Message.Debug ("    operation : " ^ (Operation.to_string (get_op n))) ;

            n_marking_data.region <- Abstraction.post anc_reg (get_op n) global_variable_table;

            Message.msg_string Message.Debug "The refined node looks like:" ;
            Message.msg_printer Message.Debug print_tree_node n ;

            refine_path n p


  let add_pt pts fname n =
    try
      let current = Hashtbl.find pts fname in
      Hashtbl.replace pts fname (nset_add current n)
    with Not_found -> Hashtbl.add pts fname (nset_add nset_empty n)

  let has_pt pts fname n =
    try
      let current = Hashtbl.find pts fname in
      nset_mem current n
    with Not_found -> false

  let remove_pt pts fname n =
    try
      let current = Hashtbl.find pts fname in
      Hashtbl.replace pts fname (nset_remove current n)
    with Not_found -> ()
  
  let clear_pts pts = Hashtbl.clear pts

  let get_pts pts fname =
    try Hashtbl.find pts fname
    with Not_found -> nset_empty
  
  let iter_pts = Hashtbl.iter

  let print_summaries fmt epts =
    let funcs fname entries =
      Format.fprintf fmt "@.--- Summaries for %s ---@." fname;
      let sums entry =
	Format.fprintf fmt "@.FROM@.";
	print_node fmt entry;
	let exits = (get_entry entry).exit_pts in
	nmap_iter (fun exit _ ->
	  Format.fprintf fmt "@.TO@.";
	  print_node fmt exit) exits in
      nset_iter sums entries in
    iter_pts funcs epts

  (*let print_summaries fmt epts =
    Abstraction.init_spec ();
    let f1 = ref None in
    let f2 = ref None in
    let funcs fname entries =
      let folder sums entry =
	let reg_in = get_region entry in
	let exits = (get_entry entry).exit_pts in
	let exits = nmap_fold (fun exits exit _ -> (get_region exit) :: exits) [] exits in
	(reg_in, exits) :: sums in
      let summaries = nset_fold folder [] entries in
      Format.fprintf fmt "@.--- Summaries for %s ---@." fname;
      (*List.iter (fun (reg_in, reg_outs) ->
	Format.fprintf fmt "@.FROM: ";
	Region.print fmt reg_in;
	List.iter (fun reg_out ->
	  Format.fprintf fmt "@.  TO: ";
	  Region.print fmt reg_out;
	  Format.fprintf fmt "@.") reg_outs) summaries;*)
      let spec = Abstraction.build_spec fname summaries in
      (*Format.fprintf fmt "@.Spec: ";
      Predicate.print fmt spec;
      Format.fprintf fmt "@.";*)
      if fname = "sum" then f1 := Some spec
      else if fname = "sum2" then f2 := Some spec in
    iter_pts funcs epts;
    fprintf "@.@.Correct? ";
    (match Abstraction.check_spec (valOf !f1) (valOf !f2) with
      Abstraction.Yes -> fprintf "YES!@.";
    | Abstraction.No -> fprintf "NO!@.";
    | Abstraction.Maybe pred ->
	fprintf "Only if ";
	Predicate.print Format.std_formatter pred;
	fprintf "@.")*)

  let update_tree_after_refinment entry_pts unprocessed_nodes ts coverers =
    let rec update_and_compute n =
      Format.printf "@.Found node:@.";
      print_node Format.std_formatter n;
      Format.printf "@.with %d children@." (List.length (Tree.get_children n));
      let n_data = get_node_data n in
      (match n_data.mark with
        Unprocessed -> 
	  assert (not (Tree.has_child n)) ;
	  begin
	    match (Options.getValueOfString "comp") with
	      "path" -> ();
	    | _ -> 	
		unprocessed_nodes#add_element n ;
		Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		Message.msg_printer Message.Debug print_tree_node n ;
	  end	    
      | Processed_Was_Covered_To_Reprocess md ->
              (* --- for debugging --- *)
          assert (not (Tree.has_child n)) ;
	  begin
	    match (Options.getValueOfString "comp") with
	      "path" -> ()
	    | _ -> 	
		Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		Message.msg_printer Message.Debug print_tree_node n ;
		unprocessed_nodes#add_element n ;
		Region.addCoverer coverers md.region n
	  end
      | Processed_Covered md ->
              (* --- for debugging --- *)
          assert (not (Tree.has_child n)) ;
          if md.time_stamp >= ts || called_after md ts then
            begin
              Message.msg_string Message.Debug "Redo covered node!\n";
              n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
              Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element n
            end
          else
	    Region.addCoverer coverers md.region n
      | Processed_Uncovered md ->
          (*Format.printf "call_time_stamp = %d@." md.call_time_stamp;*)
          if called_after md ts then
            begin
              Message.msg_string Message.Debug "Redo uncovered node because of call!\n";
              Tree.delete_children n;
              n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
              Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element n
            end
          else
            begin
	      Format.printf "This node's function target was called early enough: %d vs. " ts;
	      (match md.call_time_stamp with
		None -> Format.printf "NONE@."
	      | Some t -> Format.printf "%d@." t);
              Region.addCoverer coverers md.region n
            end)
    (*in List.iter update_and_compute (get_children n)*)
    in let funcs fname entries =
      let update_entry entry =
	let stamp = get_time_stamp entry in
	Format.printf "Entry for %s: %d vs. %d@." fname stamp ts;
	if stamp >= ts then
	  remove_pt entry_pts fname entry
	else
	  update_and_compute entry
      in nset_iter update_entry entries
    in
      Message.msg_string Message.Debug "Updating tree after refinement....\n";
      Region.clearCoverers coverers;
      if ts = 0 then
        begin
          (* We must start all over again. We can't do this the normal way, since that
           * would remove the main program entry point for being in the error time range. *)
          Format.printf "Start over again!@.";
          let mainfn = List.hd (Options.getValueOfStringList "main") in
          let tree_root = !(Hashtbl.find tree_node_table 0) in
          clear_pts entry_pts;
          add_pt entry_pts mainfn tree_root;
          Tree.delete_children tree_root;
          (get_node_data tree_root).mark <-
            Processed_Was_Covered_To_Reprocess (get_marking_data tree_root);
          unprocessed_nodes#add_element tree_root
        end
      else
        Hashtbl.iter funcs entry_pts


(* ********************************************************************** *) 
  (* Jhala: Adding proof tree data structures *)
    (* module BlastProof = Abstraction.BlastProof *)
			  
    type proof_tree_node_data = {
      pt_id       : int; 
      pt_region   : Abstraction.proof_region;
      covering_nodes :  (Abstraction.lemma_index * (proof_tree_node ref list)) option; 
     (* Jhala: Right now, just dumping the tree, 
	the last field will say which nodes cover the present node *)
    }
    and proof_tree_edge = (Operation.t * Abstraction.lemma_index) 
    and proof_tree_node = (proof_tree_node_data, proof_tree_edge) Tree.node(* Jhala: not storing the operation on the edge *)
  (* Jhala: end proof tree data structures -- 
     rest are in the module "BlastProof" in abstraction.ml 
     -- Abstraction.lemma_index is just "int" *)


let print_proof_tree (root: proof_tree_node) =
  let print_pt_id n1 = print_int (Tree.get_node_label n1).pt_id;print_string " ; "
  in
  let print_pt_children_ids n1 = 
    print_string "Children of ";
    print_pt_id n1;print_string ": \n"; 
    print_string "["; 
    List.iter print_pt_id (Tree.get_children n1);
    print_string "]\n"
  in
  let print_pt_edge pt_e = 
    match pt_e with
	(op,i) ->  
	  begin 
	    print_string "op: ";
	    Message.msg_printer Message.Debug Operation.print op ; 
	    print_string "\n proof: " (* URGENT ! ;print_int i --TYPE ISSUES *)
	  end
  in
  let print_pt_node_data n_data = 
    print_int n_data.pt_id;print_string "\n";
    Abstraction.print_proof_region n_data.pt_region;
    match n_data.covering_nodes with
	None -> print_string "Not covered \n"
      | Some(i,l) -> print_string "Covered \n"
  in

  let rec print_pt_node n =
    let _ = 
      match (Tree.get_parent_edge n) with
	  None -> print_string "ROOT NODE Id: ";
	| Some(e) ->
	    let par = Tree.get_source e in
	      begin
		print_string "----par: ";print_pt_id par;print_string "\n";
		print_pt_edge (Tree.get_edge_label e);
		print_string "---->\n";
		print_string "INT NODE Id: "
	      end 
    in
      print_pt_node_data (Tree.get_node_label n);
      print_pt_children_ids n;
      List.iter print_pt_node (Tree.get_children n)
  in
    print_pt_node root
      
(* count the size of the proof tree *)
let count_tree_nodes root = 
  Message.msg_string Message.Debug "Counting tree nodes";
  let update_counter n = 
    (* let us just count how many covered nodes there are *)
    if ((List.length (get_children n ) = 0)) then stats_nb_proof_tree_covered_nodes := !stats_nb_proof_tree_covered_nodes + 1;
    stats_nb_proof_tree_nodes := !stats_nb_proof_tree_nodes + 1; ()
  in
  let leaf_check_fn n = (get_children n) = [] in
    stats_nb_proof_tree_nodes := Tree.count_nodes_descendants  root;
    stats_nb_proof_tree_covered_nodes := Tree.count_f_descendants leaf_check_fn root

(****************************************************************************)
(****************************************************************************)

(** LMCUTIL start *)

(* GUI hooks *)
(* some functions that dumps the counterexample data structure to a file *)
(* the type of a trace is a (bool * Operation.t list * Region.t list) *)

(* returns a string,trace object pair which are used to output a trace*)


let dumppath node region_at_node =
    let _ = Message.msg_string Message.Error "in dumppath" in
    let p_to_root = path_to_root node in
    let list_of_regs = (List.map (get_region)  
 			  (Misc.chop_last p_to_root))@[region_at_node] in
    let list_of_ops = List.map (get_op) ( List.tl p_to_root) in
    dumptrace (false, list_of_ops, list_of_regs)


let dump_trace_to_node n n_region genuine =
   let n_data   = (Tree.get_node_label n) in
   let path_to_root = path_to_root n in
   let list_of_regs = (List.map (get_region)  
			 (Misc.chop_last path_to_root))@[n_region] in
   let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
     dumptrace (genuine, list_of_ops,list_of_regs)

let demo_process n n_region genuine =

 match (Options.getValueOfBool "demo" (*|| genuine *)) with
      true ->
	begin
	  let s = if genuine then "Genuine" else "Spurious" in
	  let outs = Printf.sprintf "*** %s Counterexample Found!***" s in
	    Message.msg_string Message.Debug outs;
	    dump_trace_to_node n n_region genuine;
	Message.msg_string Message.Error "waiting to hear from gui";
	let _ = input_line stdin in
	  Message.msg_string Message.Error "heard from gui!";
	  ()
 end 
| false -> ()


(* definitely should be in LMC.ml *)
(* this next function is the same as in the regular lazymodelchecker.ml *)

let real_error_counter = ref 0 
let real_error_table = Hashtbl.create 109
let real_error_array = Array.create (Options.getValueOfInt "multi") None
let reaches_error_table = Hashtbl.create 109 
(* keeps those nodes that have a path to error *)
(* we shall deem a node to be a non-coverer if it is in this table ... *)

let reaches_error n = 
  Hashtbl.mem reaches_error_table (get_id n)

let update_reaches_error_table n = 
  let id = get_id n in
    Message.msg_string Message.Debug 
      ("Adding to reaches_error_table: "^(string_of_int id));
    Hashtbl.add reaches_error_table id ()


(* TBD *)
let output_all_errors () = ()
  
(* for now I shall ignore the stack *)
let path_signature path = 
  let op_l = List.map get_op (List.tl path) in
  let reg_l = Misc.chop_last (List.map get_region path) in
  (* signature is the sequence of event locations ... *)
  let comb = List.combine op_l reg_l in
  let ff (o,r) = if Options.getValueOfBool "efilter" then Abstraction.is_spec r else true in
  let event_l = List.filter ff comb in
  let _ = Message.msg_string Message.Error
	    ("Path Sig Size "^(string_of_int (List.length event_l)))
  in
  let op_triple_list = List.map (fun (o,_) -> Abstraction.op_to_string o) event_l in
  let psig = Misc.strList 
	       (List.map 
		  (fun (x,y,_) -> ((string_of_int x)^"#"^(string_of_int y)^"#"))
		  op_triple_list)
  in
    (psig, (List.map fst event_l,op_l))


(* We'd like to hash a trace using its "spec" signature *)
let accumulate_real_error path =
  (* path is a "feasible" error trace *)
  let _ = Message.msg_string Message.Normal "Another real error found." in
  let _ = Message.msg_string Message.Error "Another real error found." in
  let (psig, (event_op_l,op_l)) = path_signature path in
    if (Hashtbl.mem real_error_table psig) then 
      (* begin -- we've seen this path before *)
      () (* this SHOULD treat the error location as a HALT. *)
    else
      let reg_l = List.map get_region path in
	(* bookkeeping begin *)
      let op_array = Array.of_list op_l in
      let paren_fun op = 
	match Operation.get_info op with
	    Operation.Call _ -> "("
	  | Operation.Ret _  -> ")"
	  | _ -> ""
      in
      let (left ,right) = Misc.paren_match paren_fun op_array in
	(* now to gather all the "tainted" nodes *)
      let rec gather node_list i =
	match node_list with
	    [] -> []
	  | h::t ->
	      if right i = -1 then h::(gather t (i+1))
	      else gather t (i+1)
      in
      let tainted = gather (Misc.chop_last path) 0 in
      let _ = List.iter 
		update_reaches_error_table
		tainted
      in
	(* bookkeeping end *)
	Message.msg_string Message.Normal 
	  ("Found real error "^(string_of_int  !real_error_counter));
	Hashtbl.add real_error_table psig (!real_error_counter,event_op_l);
	Array.set real_error_array !real_error_counter 
	  (Some (digest_trace (true,op_l,reg_l)));
	real_error_counter := !real_error_counter + 1;
	
	(* print the event sequence *)
	Message.msg_string Message.Normal 
	  ("Event Sequence: size = "^(string_of_int (List.length event_op_l)));
	vanilla_print_op_l event_op_l;
	if !real_error_counter = (Options.getValueOfInt "multi") then
	  begin
	    (* we've seen enough errors, just dump the lot and quit *)
	    output_all_errors (); 
	    (* we may instead want to make a pass after the mc is done to report still more errors *)
	      Message.msg_string Message.Normal "Found MAX real errors. Quitting!";
	      Message.msg_string Message.Error "Found MAX real errors. Quitting!";
	      exit 1
	  end
 
      

(* LMCUTIL end *)

(*******************************************************************************)
(*******************************************************************************)
  (* Parallel model checker.
     Instead of assume guarantee checking, just check all interleavings.

     

     Need to synchronize this with the TAR algorithm.
     *)
  let model_check init o_err =
    let interface = Options.getValueOfString "interface" in
    let component = Options.getValueOfString "component" in

    Message.msg_string Message.Debug "Entering parallel_model_check" ;
    Message.msg_string Message.Debug ("   init region: " ^ (Region.to_string init)) ;
    Message.msg_string Message.Debug 
      (match o_err with None -> "No error region" | Some e -> "   error region: " ^ (Region.to_string e)) ;
    let err = 
      match o_err with
	None -> failwith "parallel_model_check: error region not specified."
      | Some e -> e
    in 
    (* the final result of the search will be stored here *)
    let resulting_error_path = ref None
    and offending_error_node = ref None
      
    (* this boolean variable allows to stop the while-loop *)
    and continue = ref true

    (* The structure to look up which node covers another begins as empty *)
    and coverers = Region.emptyCoverers ()
    and (cover_fun,all_coverer_fun) = 
            if (Options.getValueOfBool "ecov") then
                    (Region.findExactCoverer,Region.findAllExactCoverer)
            else (* for various subtle issues, exact coverer is 
		    required. The trouble is that we want to get the exit points
		    from a node. If the node has an exact coverer, I can just point
		    the parent of this node to the coverer, and thus inherit all
		    exit points of the coverers. Instead, if I keep the coverers
		    separate, then I may miss exit points.
                  *)

(*
		    (Region.findCoverer,Region.findAllCoverer) 
*)
	    failwith "cf requires -ecov!"

    (* Record of which entry points a function has *)
    and entry_pts : (string, node_set) Hashtbl.t = Hashtbl.create 101

   (* Record of which exit points a function has *)
    and exit_pts : (string, node_set) Hashtbl.t = Hashtbl.create 100

    (* helper object to create nodes *)
    and tnc = new tree_node_creator

   
    (* the set of pending nodes to be processed *)
    and unprocessed_nodes = (* RJ: TBD -- bfs -- cf reach *)
      if (* Options.getValueOfBool "bfs" *) false then new Search.bfsiterator
      else new Search.dfsiterator

    (* the global time used for time stamps *)
    and time = ref 0 in

    let entered_fname_table = Hashtbl.create 31 in
      
    let summary_edges_enode n =
      match get_kind n with
	Entry en -> List.map (fun (n_out, path) -> (n, n_out, path)) (nmap_to_list en.exit_pts)
      |	_ -> raise (Failure "Entry node expected in summary_edges_enode") in

    let summary_edges fname =
      let epts = get_pts entry_pts fname in
      List.fold_left (@) [] (List.map summary_edges_enode
			       (nset_to_list epts)) in

    let lift_and_propagate sum_es call_n reg_in =
      (*let reg = get_region call_n in
      let op =
	match Abstraction.enabled_ops reg with
	  [op] -> op
	| _ -> failwith "Non-1 out edge count from call node in lift_and_propagate" in
      let reg_in = Abstraction.post reg op global_variable_table in*)
      Message.msg_string Message.Debug "lift_and_propagate\n";
      let lift_one (n_in, n_out, (path : summary_path)) =
	(*fprintf "Match ";
	(*Region.print Format.std_formatter reg_in;*)
	print_node Format.std_formatter call_n;
	fprintf " vs. ";
	(*Region.print Format.std_formatter (get_region n_in);*)
	print_node Format.std_formatter n_in;
	fprintf "@.";*)
	if Region.leq reg_in (get_region n_in) then
	  (match get_kind n_in with
	    Entry en ->
	      Message.msg_string Message.Debug "A call site matched in lift_and_propagate.";
	      Message.msg_printer Message.Debug print_node call_n;
	      let summary_already_exists n_child =
		let edge = get_parent_edge n_child in
		match edge with
		  SummaryEdge {n_out = n_out'} -> node_equal n_out n_out'
		| _ -> false in
	      if List.exists summary_already_exists (Tree.get_children call_n) then
		Message.msg_string Message.Debug 
		  "I would have propagated this edge, but it already exists!"
	      else
		(update_caller call_n (Some (get_time_stamp n_out));
		 let retOp = SummaryEdge {path = path; n_in = n_in; n_out = n_out} in
		 let child = tnc#create_child Node Unprocessed retOp call_n in
		 Message.msg_string Message.Debug 
		   (Printf.sprintf "Now caller has %d children.@." (List.length (Tree.get_children call_n)));
		 unprocessed_nodes#add_element child)
	  | _ -> failwith "Entry node expected in lift_and_propagate")
	(*else
	  print_string "Didn't match.\n"*)
      in List.iter lift_one sum_es in

    let new_summary_edge (n_in, path_in) (n_out, path_out) =
      Message.msg_string Message.Debug "New summary edge!";
      Message.msg_printer Message.Debug print_node n_in;
      Message.msg_string Message.Debug " --> ";
      Message.msg_printer Message.Debug print_node n_out;
      Message.msg_string Message.Debug "by way of ";
     
	 let path = ConsPath (path_in, path_out) in
 
      (match (get_kind n_in, get_kind n_out) with
	(Entry en, Exit ex) ->
	  en.exit_pts <- nmap_add en.exit_pts n_out path;
	  ex.entry_pts <- nmap_add ex.entry_pts n_in path;
	  (*Format.fprintf Format.std_formatter "Entry has %d call sites.@."
	    (nset_size en.call_sites);
	  print_node Format.std_formatter n_in;
	  fprintf "@.";*)
	  nmap_iter (lift_and_propagate [(n_in, n_out, path)])
	    en.call_sites
      | _ ->
	  (match (get_kind n_in, get_kind n_out) with
	       (Exit _, _) -> Message.msg_string Message.Debug "Came from Exit\n";
	     | (_, Entry _) -> Message.msg_string Message.Debug "Went to Entry\n";
	     | (Node, _) -> Message.msg_string Message.Debug "Came from Node\n";
	     | (_, Node) -> Message.msg_string Message.Debug "Went to Node\n";
	     | _ -> Message.msg_string Message.Debug "Huh?\n");
	  failwith "Expecting edge from Entry to Exit in new_summary_edge");
    Message.msg_string Message.Debug "Summary edge added\n" in
	  
    let new_summary_edges ns n_out =
      List.iter (fun n_in -> new_summary_edge n_in n_out) ns in
      
    (* The function for constructing children .. and adding them to the tree *)
    let construct_children node accu op =
      (tnc#create_child Node Unprocessed op node)::accu
    in				 
						 
 (* the root of the reachability tree *)
    let tree_root =
      tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init ;
                                                            call_time_stamp = Some 0 }) in
    add_pt entry_pts (Abstraction.reg_function_name init) tree_root;

    let new_entry mark n op fname =
      (* created new entry. *)
      let _ = Message.msg_string Message.Debug ("cfLMC enters: "^fname) in
      let _ =
	if (not (Hashtbl.mem entered_fname_table fname))
	then
	  begin
	    Hashtbl.replace entered_fname_table fname true;
	      stats_nb_funs_entered := !stats_nb_funs_entered + 1
	  end
      in
      let n_region = get_region n in
      let n_root = tnc#create_root Unprocessed in
      let entry_region = Abstraction.post n_region op global_variable_table in
      let call_sites = nmap_add nmap_empty n entry_region in
      let entry = 
	Entry {
	  first_call_site = Some n;
	  call_sites = call_sites;
	  exit_pts = nset_empty} 
      in
      let md = { time_stamp = (time := !time + 1; !time);
		 region = entry_region;
                 call_time_stamp = Some 0} in
      let n_entry = tnc#create_child entry (mark md) (PathEdge op) n_root in
      update_caller n None; (* None indicates an in-progress function call.
			     * The stamp will be updated with an exit node's time
			     * stamp if the call terminates. *)
      (*unprocessed_nodes#add_element n_entry;*)
      n_entry in

    let new_exit n op =
      let n_region = get_region n in
      let exit = Exit {entry_pts = nset_empty} in
      let region = n_region (*Abstraction.post n_region op global_variable_table*) in
      let marking_data = { region = region;
			   time_stamp = (time := !time + 1 ; !time);
                           call_time_stamp = Some 0 } in
      let child = tnc#create_child exit
	  (Processed_Uncovered marking_data)
	  (PathEdge op) n in
      
      let fname = Abstraction.reg_function_name n_region in
      Message.msg_string Message.Debug "It's a new exit point for ";
      Message.msg_string Message.Debug fname;
      Message.msg_string Message.Debug "!\n";
      add_pt exit_pts fname child;
      child in

    let do_subtree_cut_refinement anc new_anc_region err_path err_n =
      let use_bfs = Options.getValueOfBool "bfs" in

      Message.msg_string Message.Debug "Subtree cut refinement starting at:";
      Message.msg_printer Message.Debug print_node anc;
            
      let new_anc_region =
	match get_kind anc with
	  Entry en ->
	    Message.msg_string Message.Debug "Entry found!\n";
	    (* We must treat Entry nodes specially to assure that new predicates are
	     * inferred correctly. *)
	    Message.msg_string Message.Debug 
	      (Printf.sprintf "Choose 1 of %d call sites." (nset_size en.call_sites));
	    let reg = get_region anc in
	      begin	      
		match en.first_call_site with
		    None -> new_anc_region
		  | Some caller ->
		      let caller = get_region caller in
		      let caller = Abstraction.focus_stamp caller in
			Abstraction.post caller (List.hd (Abstraction.enabled_ops caller))
			  global_variable_table
	      end
	  | Exit ex -> failwith "Exit found!"
	  | _ -> new_anc_region in

      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      (*Message.msg_printer Message.Debug print_tree_node anc ;*)
        (* Note that here there is no path to update as the entire subtree is going to be chopped off... *)

      Message.msg_string Message.Debug "Now deleting subtree:" ;

      (*let old_children = Tree.get_children anc in*)

      (* Invalidate summary edges for error nodes, finding the earliest timestamp along
       * the path in the process *)

      iter_pts (fun fname pts ->
		  Message.msg_string Message.Debug ("Entry points to %s:"^fname);
		  nset_iter (fun n -> 
			       Message.msg_printer Message.Debug print_node n) 
		    pts)
	entry_pts;

      let clear_summary n_in en =
	en.exit_pts <- nmap_empty;
	fprintf "clear_summary ";
	Message.msg_printer Message.Debug print_node  n_in
      in

      let rec backup n stamp =
	Message.msg_string Message.Debug "backup: ";
	Message.msg_printer  Message.Debug print_node n;
	if node_equal n anc then
	  (min stamp (get_time_stamp n), true)
	else
	  (match get_kind n with
	    Entry en ->
	      clear_summary n en;
	      (min stamp (get_time_stamp n), false)
	  | _ ->
	      let (stamp, stop) =
		(match get_parent_edge n with
		  SummaryEdge {n_in = n_in; n_out = n_out} ->
		    (*let stamp = clear_summary n_in (get_entry n_in) stamp in*)
		    Message.msg_string Message.Debug "<RETURN>";
		    let ret = backup n_out stamp in
		    Message.msg_string Message.Debug "<CALL>";
		    ret
		| _ -> (stamp, false)) in
	      if stop then
		(stamp, true)
	      else
		backup (Tree.get_parent n) stamp) in

      let invalidate stamp n =
	let parent = Tree.get_parent n in
	(match get_region_opt parent with
	  None -> ()
	| Some reg ->
	    (match Abstraction.enabled_ops reg with
	      [op] ->
		(match Operation.get_info op with
		  Operation.Call (_, _, fname, _) ->
		    let checkEntry n_in =
		      Message.msg_string Message.Debug "checkEntry";
		      let en = get_entry n_in in
		      if nset_mem en.call_sites parent then
			clear_summary n_in en
		    in nset_iter checkEntry (get_pts entry_pts fname)
		| _ -> ())
	    | _ -> ()));
	min stamp (get_time_stamp n)

      in let first_stamp = List.fold_left invalidate (get_time_stamp err_n) err_path in
      let (first_stamp, _) = backup err_n first_stamp in
      let first_stamp = 0 in
      Message.msg_string Message.Debug (Printf.sprintf "first_stamp = %d\n" first_stamp);

      (* Jhala: I'm assuming that its ok to just empty this out and fill it back up when we are traversing the tree later ... this whole traversal business is not good by the way, there should be a much more incremental way to do it *)
      unprocessed_nodes#empty_out ();

      let data = Tree.get_node_label anc in
      let marking_data = get_marking_data anc in
      (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      marking_data.region <- new_anc_region ;

      fprintf "Delete children of ";
      Message.msg_printer Message.Debug print_node anc;
      tnc#delete_children anc;

      Message.msg_string Message.Debug "Now re-adding children:" ;

      let add_children ops =
	Message.msg_string Message.Debug  "add_children\n";
	List.fold_left (construct_children anc) [] 
	  (List.map (fun s -> PathEdge s) ops) in

      let children : tree_node list =
          let process_one_op (accum : tree_node list) op = 
	    (match Operation.get_info op with
	      Operation.Call (_, _, fname, _) -> accum 
				(* [new_entry (fun s -> Processed_Covered s) anc op fname] *)
	    | Operation.Ret _ ->
		Message.msg_string Message.Debug "Anchor is a return\n";
		let n_out = new_exit anc op in

		(* Just adding an unprocessed node for a particular return point is not
		 * good enough, because its parent may have been deleted due to summary
		 * edges that appear in the "OK" part of the trace having been deleted
		 * because they also appear in the "error" part. *)
		let entries = entry_pts_node (Tree.get_parent n_out) in
		let entries =
		  List.fold_left (fun m (n, p) -> nmap_add m n p) nmap_empty entries in
		let lift n_in path =
		  let en = get_entry n_in in
		  let create_summary call_n _ =
		    Message.msg_string Message.Debug  "Refining summary edge for:";
		    
		    let retOp = SummaryEdge {path = path; n_in = n_in; n_out = n_out} in
		    let _ = tnc#create_child Node Unprocessed retOp call_n in
		    () in
		  nmap_iter create_summary en.call_sites in
		nmap_iter lift entries;
		n_out :: accum
	    | _ -> (add_children [op] ) @ accum )
         in
         List.fold_left process_one_op [] (Abstraction.enabled_ops new_anc_region)
      in
      Message.msg_string Message.Debug "The children are:" ;
      List.iter (fun ch -> (Message.msg_printer Message.Debug print_tree_node ch)) children;

      Message.msg_string Message.Debug  "Anc:";
      Message.msg_printer Message.Debug print_node anc;
      
      
      (* add the children to the set of pending nodes *)
      (* Jhala: This should all be done by the the subtree cut 
	 Message.msg_string Message.Debug "Adding the children to the set of pending nodes" ;
	 unprocessed_nodes#add_list children ;
       *)

      (* Recalculate reached region from all entry points that remain valid *)
      Message.msg_string Message.Debug "Updating the currently reached region" ;
      Stats.time "update_tree_after_refinement[CUT]" 
	(update_tree_after_refinment entry_pts unprocessed_nodes
	   first_stamp) coverers;
       Message.msg_string Message.Debug  "Refinement complete.\n"
    in


    (* the refinment process *)
    let do_refinment anc path new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      anc_marking_data.region <- new_anc_region ;
      
      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
      
        (* update the path *)
      Message.msg_string Message.Debug "Refining the nodes along the path..." ;
      refine_path anc path ;
      
        (* update the tree and the reached region *)
      Message.msg_string Message.Debug "Updating the markings and the reached region..." ;
      Stats.time "update after refine [PATH]"
	  (update_tree_after_refinment entry_pts unprocessed_nodes
	     anc_marking_data.time_stamp) coverers
    in

    (* this function deletes covered nodes from the tree *)
    let rec reclaim n = 
      if (Tree.has_child n || (not (Tree.has_parent n))) then ()
      else 
	begin
	  let par = get_parent n in
	  Tree.delete_child n; 
	  reclaim par
	end
    in
      (* add the root to the pending unprocessed nodes *)
      unprocessed_nodes#add_element (tree_root) ;


    Ast.set_time_out_signal () ;

    while (!continue && unprocessed_nodes#has_next ()) do
      stats_nb_iterations := !stats_nb_iterations + 1;
      
      Ast.check_time_out () ;
      
        (* the search is not finished yet *)
	if (!stats_nb_iterations mod 100 == 0) then
	  begin
            Message.msg_string Message.Normal
	      (Printf.sprintf
		 "While loop tick: %d : Funcs: %d\n"
		 (!stats_nb_iterations)
	    	 (!stats_nb_funs_entered));
	    flush stdout; flush stderr;()
	    (*Abstraction.print_abs_stats () *)
	  end ;
	Message.msg_string Message.Debug "\n\n****************************************************************************\n" ;
        Message.msg_string Message.Debug "Next iteration of model-check's big while-loop" ;
	Message.msg_printer Message.Debug Format.pp_print_int (!stats_nb_iterations);
        (* Message.msg_string Message.Debug "Currently reached region:" ;
           Message.msg_printer Message.Debug Region.print !reached_region ;*)

        (* let's process the next pending node *)
        let n = unprocessed_nodes#next () in
          Message.msg_string Message.Debug (*Normal*) "Now processing tree node:" ;
          Message.msg_printer Message.Debug (*Debug Normal*) print_tree_node n ;

          let n_data   = (Tree.get_node_label n) in

          let n_marking_data = 
            match n_data.mark with
              Unprocessed ->
                let region = match get_parent_edge n with
		  PathEdge op ->
		    Stats.time "main post" (Abstraction.post (get_region (get_parent n)) op)  global_variable_table
		| SummaryEdge {n_out = n_out} ->
		    let reg = get_region (get_parent n_out) in
		    let parent_reg = get_region (get_parent n) in
                    (* TBD:printsumedge *)
                    failwith "cfLMC NOT Supported"
		    (* Stats.time "summary post" (Abstraction.summary_post parent_reg reg)
			(get_incoming_operation n_out) *)
		in
		{ region = region;
                  time_stamp = (time := !time + 1 ; !time);
                  call_time_stamp = Some 0 };
            | Processed_Was_Covered_To_Reprocess md -> md
            | _ -> failwith "LazyModelChecker.model_check: unexpected marking data"
          in
            (* --- for debugging --- *)
            (* assert (n_data.mark == Unprocessed) ; *)

          let n_region = n_marking_data.region in
	  let _ = 
	    if (!stats_nb_iterations mod 1000 = 0) 
	    then
              (Message.log_string_always Message.Rgn "Processed node's region:" ;
	       Region.log_region Message.LogAlways Message.Rgn n_region)
	    else
              (Message.log_string_norm Message.Rgn "Processed node's region:" ;
	       Region.log_region Message.LogNormal Message.Rgn n_region);
	  in
          (* the error region at this node *)
          let err_n = Stats.time "cap" (Region.cap n_region) err in
            Message.msg_string Message.Debug ("Error region at this node: " ^ (Region.to_string err_n)) ;

            if (Stats.time "is_empty" Region.is_empty err_n)
            then
              begin
                (* no error found at this node *)
                Message.msg_string Message.Debug "No error found at this node" ;

                (* let's test whether this node is covered *)
                Message.msg_string Message.Debug "Let's test whether this node is covered" ;
		let coverable =  
		  if (Options.getValueOfBool "cov") 
		  then Abstraction.coverable n_region
		  else true
		in
		  (* RJ: patch for multiple errors *)
		let covered = 
		  if not coverable then None 
		  else 
		    if (Options.getValueOfInt "multi" = 0) then 
		      (Stats.time "Covered check:" (cover_fun coverers) n_region)
		    else
		      let candidates = Stats.time "(Multi) Covered Check:" 
					 (all_coverer_fun coverers) n_region
		      in
		      let f n' = 
			if reaches_error n'
			then None else Some n'
		      in Misc.get_first f candidates
		in
		  (* we shall not consider nodes that have a path to error to be "real coverers" -- so 
		      we end up duplicating such nodes... *)

		  (* RJ: end patch for multiple errors *) 
		  match (covered, Region.is_empty n_region) with
		      (_, true) -> ()
		    | (Some n', _) ->
			begin
			  (* this node is covered *)
			  Message.msg_string Message.Debug "This node is covered" ;
			  
			  Message.msg_string Message.Debug "*** COVERED ";
			  Message.msg_string Message.Debug (Region.to_string n_region);
			  Message.msg_string Message.Debug  " BY: ";
			  Message.msg_printer Message.Debug print_node n';
			  flush stdout;
			  
			  let entries = entry_pts_node n in
			  let exits = exit_pts_node n' in
			    
			  Message.msg_string Message.Debug ("NEW SUMMARIES " ^ string_of_int (List.length entries) ^ " vs. " ^ string_of_int (List.length exits) ^ "\n");
			    (*print_string "New summary edges around covered node.\n";*)
			    List.iter (new_summary_edges entries) exits;
			    (*print_string "MADE THEM\n"*)
			    
			    (* mark n as processed and covered *)
			    n_data.mark <- Processed_Covered n_marking_data;
			    if (Options.getValueOfBool "reclaim") 
			    then
			      reclaim n;
			    ()
			end
		    | (None, _) ->
			begin
			  (* this node is not covered *)
			  Message.msg_string Message.Debug "This node is not covered" ;
			  Message.msg_string Message.Debug "Constructing its successor children..." ;
			  
			  (* we construct its successor children *)
			  
			  (* Adamc to-do:
			   * If n is a function call to f:
			   *    Create an Entry node for this function call
			   *    Add it to the worklist
			   *    Lift f's summary edges to this call site
			   * If n is a function exit from f:
			   *    Record n as a reachable exit point from f
			   *    Final all function entry pts EN that reach n
			   *    Create summary edges between EN x {n}
			   * Otherwise, n is a boring node, so:
			   *    Carry on as usual, letting Abstraction do the work
			   *)
			  
		    let enabled_ops = List.map (fun s -> PathEdge s)
			(Stats.time "enabled_ops" Abstraction.enabled_ops n_region) in
		    
		    (*Message.msg_string Message.Debug (*Normal*) "The children are:" ;
		    List.iter (function child -> Message.msg_printer Message.Debug (*Normal*) print_tree_node child) children ;*)
		    
                    (* add the children to the set of pending nodes *)
		    let addChildren ops =
                      let children = List.fold_left (construct_children n) [] ops in
		      Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
		      unprocessed_nodes#add_list children ;
		      Message.msg_string Message.Debug ("Remaining nodes:"^(string_of_int (unprocessed_nodes#sizeof ()))); 
		      Message.msg_apply Message.Debug unprocessed_nodes#iter (fun x -> Message.msg_printer Message.Debug print_tree_node x) 
		    in

		    Message.msg_string Message.Debug "This node looks like: (before updating)" ;
		    Message.msg_printer Message.Debug print_tree_node n ;

                      (* mark n as processed and uncovered *)

		    Message.msg_string Message.Debug "Updating the node's marking" ;
                        (* Update the lattice of the current region *)

		    
		    let updated_region = Region.update_lattice_in_region_coverers n_region coverers in

		    let updated_marking_data = { n_marking_data with region = updated_region} in
		    n_data.mark <- Processed_Uncovered updated_marking_data ;
		   
		    Message.msg_string Message.Debug "This node now looks like:" ;
		    Message.msg_printer Message.Debug print_tree_node n ;

(*
                    (match enabled_ops with
                      [op] ->
			let op = edge_to_op op in
			(match Operation.get_info op with
			  Operation.Call (_, _, fname, _) ->
			    let n_entry = Stats.time "new_entry" (new_entry
				(fun s -> Processed_Was_Covered_To_Reprocess s)
				n op) fname in
			    
			    let in_region = get_region n_entry in
			    
			    let found_one = ref false in
			    
			    (* Tell any matching function entry points that they have
			       * a new caller. *)
			    let checkEntry n_in = 
			      let c1  = Region.eq in_region (get_region n_in) in
			      let c2 = (Options.getValueOfInt "multi" = 0) in
			      let c3 = reaches_error n_in in
			      if (c1 && (c2 || (not c3))) then
				(let en = get_entry n_in in
				en.call_sites <- nmap_add en.call_sites n in_region;
				Message.msg_string Message.Debug "Matched a previous call!\n";
				found_one := true)
			      else
				( Message.msg_string Message.Debug "Did not match ";
				 Message.msg_printer Message.Debug print_node  n_in;
				 )
			    in nset_iter checkEntry (get_pts entry_pts fname);
			    
			    if not !found_one then
			      (add_pt entry_pts fname n_entry;
			       Message.msg_string Message.Debug ("New entry point for "^fname);
			       unprocessed_nodes#add_element n_entry);
                            (* Propagate any summary edges for matching calls that have
			       * already been explored to exit points. *)
			    lift_and_propagate (summary_edges fname) n in_region
			| Operation.Ret exp ->
			    let child = new_exit n op in
			    let entries = entry_pts_node n in
			    Message.msg_string Message.Debug 
			      (Printf.sprintf "|entries| = %d" (List.length entries));
			    Message.msg_string Message.Debug 
			      "New summary edges to exit node.\n";
			    new_summary_edges entries (child, ListPath [])
			| Operation.Normal -> addChildren ())
		    | _ -> addChildren ());
*)

                    let process_op op' =
			let op = edge_to_op op' in
			(match Operation.get_info op with
			  Operation.Call (_, _, fname, _) ->
			    let n_entry = Stats.time "new_entry" (new_entry
				(fun s -> Processed_Was_Covered_To_Reprocess s)
				n op) fname in
			    
			    let in_region = get_region n_entry in
			    
			    let found_one = ref false in
			    
			    (* Tell any matching function entry points that they have
			       * a new caller. *)
			    let checkEntry n_in = 
			      let c1  = Region.eq in_region (get_region n_in) in
			      let c2 = (Options.getValueOfInt "multi" = 0) in
			      let c3 = reaches_error n_in in
			      if (c1 && (c2 || (not c3))) then
				(let en = get_entry n_in in
				en.call_sites <- nmap_add en.call_sites n in_region;
				Message.msg_string Message.Debug "Matched a previous call!\n";
				found_one := true)
			      else
				( Message.msg_string Message.Debug "Did not match ";
				 Message.msg_printer Message.Debug print_node  n_in;
				 )
			    in nset_iter checkEntry (get_pts entry_pts fname);
			    
			    if not !found_one then
			      (add_pt entry_pts fname n_entry;
			       Message.msg_string Message.Debug ("New entry point for "^fname);
			       unprocessed_nodes#add_element n_entry);
                            (* Propagate any summary edges for matching calls that have
			       * already been explored to exit points. *)
			    lift_and_propagate (summary_edges fname) n in_region
			| Operation.Ret exp ->
			    let child = new_exit n op in
			    let entries = entry_pts_node n in
			    Message.msg_string Message.Debug 
			      (Printf.sprintf "|entries| = %d" (List.length entries));
			    Message.msg_string Message.Debug 
			      "New summary edges to exit node.\n";
			    new_summary_edges entries (child, ListPath [])
			| Operation.Normal -> addChildren [op'] )
                    in
                    List.iter (process_op) enabled_ops ;



                      (* add n's region to the reached region *)
		    if (coverable) then 
		      begin
			Message.msg_string Message.Debug "Updating the currently reached region" ;
                        Message.msg_string Message.Debug "Here: " ;
                        Region.addCoverer coverers updated_region n;
			
                        (*print_string "*** ADDED COVERED\n"*)
		      end ;
		    


		  end
              end
            else
              begin
                (* error found *)
                Message.msg_string Message.Debug "Error found : checking validity." ;
		Message.msg_string Message.Debug ("Error at depth"^(string_of_int (Tree.node_depth n))); 

		Message.msg_string Message.Debug ("Error node: "^(string_of_int (get_id n)));
                  begin
                    Message.msg_string Message.Debug "Error found at this node" ;
		    
                    (* this node will have to be reprocessed after refinment *)
		    (* JHALA: why will this node have to be reprocessed after refinement ? *) 
                    (* now, the error node has been added to the set and so you're screwed *)
		    (* if the error is real then it doesnt need to be added as:
		       1. multi means you DONT want to add it,
		       2. non-multi means that you have a real error, stop.
		       if the error is bogus then anyway that node will be eschewed by the
		       subtree-cut-refine business -- so why add that node to the set ?
		       Hence, I am commenting out the next few lines ... *)
		   
                    n_data.mark <- Processed_Was_Covered_To_Reprocess n_marking_data ;
		    (*
		      Message.msg_string Message.Debug "Re-adding this node to the set of pending unprocessed nodes" ;
		      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		      Message.msg_printer Message.Debug print_tree_node n ;
                      unprocessed_nodes#add_element n ;
		    *)
		    
                    (* let's now test whether this error is a real one *)
                    Message.msg_string Message.Debug "Testing whether this error is a real one" ; 
		    let check_refinement = 
			check_error
		    in
		      try 
			match (Stats.time "check_error" (check_refinement n) err_n) with
			    EmptyErrorAtNode(err_anc, anc, path, err_path) ->
				(* this is a false error *)
			      begin
				Message.msg_string Message.Normal "This is a false error (seq)" ;
				Message.msg_string Message.Debug "The check_error function ended up with" ;
				Message.msg_string Message.Debug "     at ancestor: " ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
				Stats.time "demo process" (demo_process n n_region) false;
				
				if (Options.getValueOfBool "stop") then
				  failwith ("FALSE ERROR CHOKE");

				(* print_string "FALSE ERROR:\n";
				List.iter (print_node Format.std_formatter) path;*)

				(* let's refine *)
				Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path" ;
				if (Options.getValueOfBool "traces") then dumppath n n_region;
				
			      	let anc_region = (get_region anc) in
				Message.msg_string Message.Debug  "anc_region is: ";
				Message.msg_printer Message.Debug  Region.print  anc_region;
				
				let focused_anc_region = 
				  try
				    let rv = 
				      match Options.getValueOfBool "block" with
					  false ->
					    Stats.time "focus" (Abstraction.focus anc_region) err_anc 
					| true -> Abstraction.focus_stamp anc_region
					    (* analysis is already done! *) 
				    in
				      if (Options.getValueOfBool "restart" || Options.getValueOfBool "localrestart") then 
					begin
					  Message.msg_string Message.Error "About to Restart";
					  
					  raise RestartException
					    
					end
					else rv
				  with Abstraction.NoNewPredicatesException ->
				    begin
                                      Message.msg_string Message.Normal "NO NEW PREDS!";
                                      (* Adamc: Ignoring predH for now *)
 				      if (Options.getValueOfInt "predH" = 3) then
					begin
					  let path_to_root = path_to_root n in
					  let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					  let list_of_ops = List.map get_op ( List.tl path_to_root) in
					  Message.msg_string Message.Debug 
					    ("\n No of regs: "^(string_of_int (List.length list_of_regs)));
					  Message.msg_string Message.Debug 
					    ("\n No of ops : "^(string_of_int (List.length list_of_ops)));
					  try 
					    (Abstraction.find_all_predicates list_of_regs list_of_ops; 
					     raise RestartException
					       )
					  with e -> 
					    
					    begin
					      if (e = RestartException) then raise e 
					      else 
						begin
						  Message.msg_string Message.Minor "This is a false error" ;
						  Message.msg_string Message.Minor "The check_error function ended up with" ;
						  Message.msg_string Message.Minor ("     an empty error: " ^ (Region.to_string err_anc)) ;
						  Message.msg_string Message.Minor "     at ancestor: " ;
						  Message.msg_printer Message.Minor print_tree_node anc ;
						  Message.msg_string Message.Minor "     along the path (from the ancestor to the current node):" ;
						  Message.msg_printer Message.Minor  print_tree_node anc ;
						  List.iter (function x -> Message.msg_printer Message.Minor print_tree_node x) path ;
						  Message.msg_string Message.Debug ("find_all_predicates raises : "^(Printexc.to_string e)) ; 
						  dumptrace (false, list_of_ops, list_of_regs);
						  failwith ("problem in find_all_predicates !") 
						end
					    end
					end
				      else 
					if (Options.getValueOfInt "predH" = 1 && 
					    (not (Abstraction.predicate_fixpoint_reached ()))) 
					then (Message.msg_string Message.Debug "Raising Restart"; raise RestartException)
					else
                                          (* Adamc: Only treating this case (i.e., predH not set) *)
					  begin
                                            (*let path_to_root = path_to_root n in
                                              List.iter (fun n -> fprintf "On real path: ";print_node Format.std_formatter n; fprintf "@.") path_to_root;
					    let path_to_root = expand_path path_to_root in
                                            List.iter (fun n -> fprintf "On path: ";print_node Format.std_formatter n; fprintf "@.") path_to_root;
					    let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					    let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
					    dumptrace (false, list_of_ops, list_of_regs);*)
					      failwith ("No new preds found !-- and not running allPreds ...")
					  end
				    end
				in (* for the focused_anc_region *)
				Message.msg_string Message.Debug "Done calling Focus";
				Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
		
				stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;

				Message.msg_string Message.Debug "REFINING with region";
				Message.msg_printer Message.Debug Region.print focused_anc_region;

				begin
				match (Options.getValueOfString "comp") with
				    "path" -> 
				      begin
					Stats.time "do_ref [PATH]" (do_refinment anc path) focused_anc_region ;
					Message.msg_string Message.Error "PATH"; 
					Message.msg_string Message.Debug "The refined path looks like:" ;
					Message.msg_printer Message.Debug print_tree_node anc ;
					List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
				      end
				  | _ ->
				      Stats.time "subtree_cut_refinement" (do_subtree_cut_refinement anc focused_anc_region err_path) n
			      end
 end
		| NonEmptyErrorAtRoot(err_root, root, path) ->
			    (* --- for debugging --- *)
			    begin
			    assert (root == tree_root) ;
			    (* this is a real error *)
			    (* we put the error path in the appropriate variable *)
			
			      let path = Stats.time "expand_path" expand_path path in
                              List.iter (fun n -> 
					   Message.msg_string Message.Debug "Path: "; 
					   Message.msg_printer Message.Debug print_node n; 
					) path;
				if (Options.getValueOfInt "multi" = 0) then
				  begin
				    (* single error will do thank you *)
				    resulting_error_path := Some(err_root,
								 List.map (get_op) path,
								 err_n) ;
				    offending_error_node := Some (Misc.list_last path) ;
				    (* print the unknown functions  on the path *)
				    Abstraction.print_debug_info (List.map (get_op) path);
				    demo_process n n_region true;
				    (* and break the while-loop *)
				    continue := false
				  end
				else 
				  (* find multiple errors -- so just keep going till a fixpoint is found *)
				  accumulate_real_error path;
			
				
			    end
		      with RestartException -> 
			begin
			  failwith "Restart not supported yet"
			  (*stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
			  print_string "CC\n";
			  do_subtree_cut_refinement tree_root (get_region tree_root) n*)
			end
		  end
	      end
      done ;
    Message.msg_string Message.Debug ("Depth of tree: "^(string_of_int (Tree.subtree_depth tree_root)));
    Message.msg_string Message.Debug  "Function summaries:";
    Message.msg_printer Message.Debug print_summaries entry_pts;

    if interface <> "" && component <> "" && !resulting_error_path = None then
      begin
	let get_spec fname =
	  let folder sums entry =
	    let reg_in = get_region entry in
	    let exits = (get_entry entry).exit_pts in
	    let exits = nmap_fold (fun exits exit _ -> (get_region exit) :: exits) [] exits in
	    (reg_in, exits) :: sums in
	  let summaries = nset_fold folder [] (get_pts entry_pts fname) in
	    Message.msg_string Message.Debug (Printf.sprintf "--- Summaries for %s ---" fname);

	  Stats.time "build_spec" (Abstraction.build_spec fname) summaries in
	Stats.time "init_spec" Abstraction.init_spec ();
	let sInterface = get_spec interface in
	let sComponent = get_spec component in
	let res = Abstraction.check_spec sComponent sInterface in
	Message.msg_string Message.Normal "Correct? ";
	match res with
	  Abstraction.Yes -> Message.msg_string Message.Normal "YES!";
	| Abstraction.No -> Message.msg_string Message.Normal "NO!";
	| Abstraction.Maybe pred ->
	    Message.msg_string Message.Normal "Only if ";
	    Message.msg_printer Message.Normal Predicate.print pred;
	          end;

    match (!resulting_error_path) with 
      Some ((_,_,err_n) as e_path) -> 
	begin
	  let err_node = 
	    match !offending_error_node with
		    Some n -> n
	    | None -> failwith "Quit while w/ error loop w/o setting offending_error_node!"
	  in
	  let path_to_root = Tree.path_to_root err_node in
	  let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[get_region err_node] in
	  let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
	  dumptrace (true,list_of_ops,list_of_regs);
	  Error_reached e_path
	    
	end
    | None -> (* print the tree *) 
	begin
	      (count_tree_nodes tree_root);
	  (* !resulting_error_path *)
	      No_error
	end
	 

end 
