(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * This module implements the lazy model checking algorithm.
 *)


open BlastArch


module Make_Lazy_Model_Checker :
  functor(Abstraction : ABSTRACTION) ->
  sig
    include LAZY_MODEL_CHECKER
    val print_stats : Format.formatter -> unit -> unit
    val print_phis : bool -> string
    val process_env_assumptions : (Ast.Expression.lval * Ast.Predicate.predicate) list -> unit
    val compute_transition_invariant : unit -> unit

  end with module Abstraction = Abstraction
