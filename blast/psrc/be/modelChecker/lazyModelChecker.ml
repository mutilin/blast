(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)



(**
 * This module implements the lazy model checking algorithm.
 *)

open Ast
open BlastArch


(* [Greg] Very dirty hack because we renamed Stats to VampyreStats in
   vampyre...  We should clean this stats business anyway. *)
module Stats = Bstats

module Make_Lazy_Model_Checker =
  functor(Abstraction : ABSTRACTION) ->
struct
  module Abstraction = Abstraction

  module Region = Abstraction.Region

  module EnvAutomaton = Abstraction.EnvAutomaton

  module Operation = Abstraction.Operation

  module Tree = BiDirectionalLabeledTree

  module C_System_Descr = BlastCSystemDescr.C_System_Descr

  module ModelChecker = ModelChecker.Make_Model_Checker (Abstraction)
  open ModelChecker

  type error_path = ModelChecker.error_path

  exception RestartException

  let initial_dfs_bound = 5 (* initial bound used for bounded dfs option *)

  (* statistic variables *)
  let stats_nb_iterations = ref 0
  let stats_nb_iterations_of_outer_loop = ref 0 (* added for races *)
  let stats_nb_created_nodes = ref 0
  let stats_nb_refinment_processes = ref 0
  let stats_nb_refined_nodes = ref 0
  let stats_nb_proof_tree_nodes = ref 0
  let stats_nb_proof_tree_covered_nodes = ref 0
  let stats_nb_deleted_nodes = ref 0
  let local_restart_counter = ref 1

  let reset_stats =
    stats_nb_iterations := 0 ;
    stats_nb_refinment_processes := 0 ;
    stats_nb_created_nodes := 0 ;
    stats_nb_refined_nodes := 0 ;
    stats_nb_proof_tree_nodes := 0;
    stats_nb_proof_tree_covered_nodes := 0
      
  let print_stats fmt () =
    Format.fprintf fmt "@[<v>@[Nb iterations of outer while loop:@ %d@]" !stats_nb_iterations_of_outer_loop ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[<v>@[Nb iterations of reachability:@ %d@]" !stats_nb_iterations ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb created nodes:@ %d@]" !stats_nb_created_nodes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb refinment processes:@ %d@]" !stats_nb_refinment_processes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb refined nodes:@ %d@]" !stats_nb_refined_nodes ;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb proof tree nodes:@ %d@]" !stats_nb_proof_tree_nodes;
    Format.fprintf fmt "@ " ;
    Format.fprintf fmt "@[Nb proof tree covered nodes:@ %d@]@]" !stats_nb_proof_tree_covered_nodes;
    Format.fprintf fmt "@[Nb deleted nodes:@ %d@]@]" !stats_nb_deleted_nodes;
    ()

  (************************************************************************************************)
  (** Model checker data types **)
  (*type model_check_outcome =
    | No_error
    | Error_reached of error_path
    | Race_condition of error_path * error_path * Ast.Expression.lval * Ast.Expression.lval
    | Bad_lock_spec of error_path * Ast.Expression.lval*)

  include BlastArch.ModelCheckOutcome
  type model_check_outcome = error_path outcome

  exception Exit_from_check_concurrent_access of model_check_outcome
          
  type 'a currentApproximation = 
      { 
      read_whole : Region.t ;
      write_whole : Region.t ;
      read_accesses : (('a * Region.t) list) ; (* the 'a is a tree node *)
      write_accesses : (('a * Region.t) list) (* the 'a is a tree node *)
    } 


  type raceKind =      Read | Write | Both | NoDistinction

  exception RaceConditionException of model_check_outcome 

  (**************************************************************************************************)
  (** Data structures to hold the phi's for lvalues                                                **)

  let global_variable_table = Hashtbl.create 31
  let iter_global_variables f = Hashtbl.iter f global_variable_table 
  let get_global_var_phi sym = 
    try
      Hashtbl.find global_variable_table sym
    with Not_found ->
      (* this lvalue has not been seen so far. So return an empty phi. *)
      ref Region.bot

  let global_lock_asm_table = Hashtbl.create 31
  (* the last is because the global_variable_table gets messed with too often *)
                                 

  (**************************************************************************************************)

  (* the following takes a list of (lval,predicate) and adds it to the above table *)

  let process_env_assumptions ll =
    (* initialize all environment assumptions to false *)
    let _ = Abstraction.iter_global_variables 
              (fun name -> 
                 Hashtbl.add global_variable_table name (ref Region.bot)
              ) 
    in  
      (* now add the stuff about the interesting locks ... *)                     
    let add (l,p) = 
      Hashtbl.replace global_variable_table l (ref (Abstraction.predicate_to_region p));
      Hashtbl.replace global_lock_asm_table l (Abstraction.predicate_to_region p)
    in
      List.iter add ll

  let print_phis verbose_flag = 
    let phi_print flag name ref_phi = 
      if (flag) then
        begin
          Message.msg_string Message.Debug 
            ("Variable " ^ (Ast.Expression.lvalToString name) ^ 
             "\nPhi =\n" ^ (Region.to_string !ref_phi) ^ "\n\n"); 
          ()
        end
      else
        begin
          if !ref_phi = Region.bot 
          then () 
          else
            (Message.msg_string Message.Debug 
              ("Variable " ^ (Ast.Expression.lvalToString name) ^ 
               "\nPhi =\n" ^ (Region.to_string !ref_phi) ^ "\n\n"); 
             ())
        end
    in
    iter_global_variables (phi_print verbose_flag);
    "Done"



  type error_check =
      EmptyErrorAtNode of Region.t * tree_node * (tree_node list)
    | NonEmptyErrorAtRoot of Region.t * tree_node * (tree_node list)

  (**
     * Checks whether a reached error region corresponds to a real error
     * path.  This function takes a node and an error region contained in
     * the node's region as arguments, and does a backward pre computation
     * from that node, along the branch from the root to that node, until
     * it gets an empty region or reaches the root.  It returns the obtained
     * region at the reached ancestor along with the path from this ancestor
     * to the initial node.
     * 
     * @param n     a node of the reachability tree
     * @param err_n a non-empty error region contained in n.region
     * @return a value of type error_check
     *)

  (* do the block-trace analysis, this will also add predicates wherever (locations) required 
     of course we want this function to return the same sort of thing that the original (i.e.
     below function returns :: EmptyErrorAtNode(reg, ) *)
     
   let check_error node error_at_node =
     let r_path = Tree.path_to_root node in
     let  _ = Message.msg_string Message.Normal ("counterex. size:"^(string_of_int (List.length r_path))) in
     
    match Options.getValueOfBool "block" with
        true -> 
	  (* this branch works *)
          begin
            let reg_l = 
              ((List.map (get_region)(Misc.chop_last r_path))@[error_at_node] )
            in
            let id_l = List.map get_id r_path in 
            let op_l = List.map (get_op) ( List.tl r_path) in
              match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l op_l) id_l) with
                  (-1,a,_) -> 
                    (* the assumption is that -1 indicates it goes all the way to the top *)
                    let (root::path) = r_path in (* should never fail ! *)
                      NonEmptyErrorAtRoot(a,root,path)
                | (i,a,_) ->
                  begin
                    (* "i" is the number of hops from the root the falsehood comes *)                
                    let (n',path) = Misc.list_cut i (r_path) in
                      EmptyErrorAtNode(a,n',path)
                  end
          end
      | false ->
          begin
            let rec backward_pre n (err_n,subs_err_n) path =
              match (Tree.get_parent_edge n) with
                  Some(e) ->
                    (* f is n's father and op labels the edge f --> n *)
                    let f = Tree.get_source e
                    and op = Tree.get_edge_label e in
                    let f_reg = get_region f in
                      (* the error subregion at f *)
                      (* JHALA: this subregion business is rubbish. It only blows up the
                          formulas. Not required *)
                      (* JHALA: MAJOR CHANGE! dropping the "capping" in the recursive call *)
        
                    (* JHALA: temporarily comment out!::  let _ = Abstraction.setWPCapRegion f_reg in *)
                    let pre_err_f = (Abstraction.pre err_n op global_variable_table) in
                    let err_f = Region.cap f_reg pre_err_f in
                    let pre_subs_err_f = (Abstraction.spre subs_err_n op global_variable_table) 
                    in

                    let subs_err_f = Region.cap f_reg pre_subs_err_f in
                    (* JHALA: temporarily comment out!:: let _ = Abstraction.resetWPCapRegion () in *)
                      (*let check_emptiness_flag = Abstraction.check_emptiness op in*)
                      if ((*check_emptiness_flag &&*) Stats.time "Emptiness Check:" Region.is_empty subs_err_f)   
                      then
                        begin
                          (*Message.msg_printer Message.Error print_tree_node f;*)
                        EmptyErrorAtNode(err_f, f, n::path)
                        end
                      else
                        backward_pre f (pre_err_f,pre_subs_err_f) (n::path)
                | None ->
                    (* n is the root *)
                    NonEmptyErrorAtRoot(err_n, n, path)
            in
            let prec_err_n = Abstraction.precise error_at_node in
            let rv = backward_pre node (prec_err_n,prec_err_n) [] in
              (* double checking *)
              (*
            let _ = 
              begin
                let reg_l = 
                  ((List.map (get_region)(Misc.chop_last r_path))@[error_at_node] )
                in
                let op_l = List.map (get_op) ( List.tl r_path) in
                  match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l) op_l) with
                      (-1,a) -> 
                        (* the assumption is that -1 indicates it goes all the way to the top *)
                        let (root::path) = r_path in (* should never fail ! *)
                          NonEmptyErrorAtRoot(a,root,path)
                    | (i,a) ->
                        (* "i" is the number of hops from the root the falsehood comes *)                    
                        let (n',path) = Misc.list_cut i (r_path) in
                        let _ = Message.msg_printer Message.Error print_tree_node n' in
                          EmptyErrorAtNode(a,n',path)
              end
            in *)
              rv
          end

  (* SKY: is not called from model_check algorithm *)
  let check_error_fwd node error_at_node  =
    let rec get_path node path proj_path =
      match (Tree.get_parent_edge node) with
        Some (e) ->
            (* f is node's father and op labels the edge f --> node *)
          let f = Tree.get_source e and op = Tree.get_edge_label e
          in 
          get_path f ((f, Some op):: path) (f::proj_path)
      | None -> (path, proj_path)
    in
    let (error_path, path_projected_on_tree_nodes) = get_path node [ (node, None) ] [ node ]
    in
    Message.msg_string Message.Debug "Check error forward called with" ;
    Message.msg_string Message.Debug "Path = ";
    List.iter (function node -> Message.msg_printer Message.Debug print_tree_node node) path_projected_on_tree_nodes ;
    Message.msg_string Message.Debug "end of path\n\n\n" ;
    let root = match path_projected_on_tree_nodes with
      h :: _ -> h
    | _ -> failwith "Empty error path in check_error_fwd"
    in
    let reg_true = Abstraction.precise (get_region root)
    in
    (* now do a concrete post on the error path *)
    let rec forward_post current_reg path =
      (*Message.msg_string Message.Debug ("\n\nforward post called with region "^(Region.to_string current_reg));
        *)
      match path with
        [] -> (* Valid error *) 
          let r = (Region.cap (Abstraction.trim_region_to_globals current_reg) error_at_node) in
          if (Region.is_empty r) then
            failwith "Rupak: this case not handled yet"
          else
            NonEmptyErrorAtRoot (r , root, List.tl path_projected_on_tree_nodes)
      | (f, op) :: rest ->
            begin
              match op with
                None -> (* reached the node where the error is without trouble *)
                  (* check if satisfiable *)
                  let err_f = Region.cap (Abstraction.trim_region_to_globals error_at_node) 
                                         (Abstraction.trim_region_to_globals current_reg) in
                  if (Stats.time "Emptiness Check (Forward):" Region.is_empty err_f) then
                    begin
                      Message.msg_string Message.Debug "Emptiness check succeeds at end of path: this is a false error";
                      check_error f error_at_node
                    end
                  else
                    begin
                      Message.msg_string Message.Debug "Emptiness check fails at end of path: this is a true error";
                      NonEmptyErrorAtRoot ( err_f , root, List.tl path_projected_on_tree_nodes)
                    end
              | Some oper ->
                  begin
                    (* do a strongest postcondition computation.
                       if oper is an assume, do a satisfiability check
                       (find predicates if unsatisfiable).
                       otherwise recursively call forward_post on the rest of the path
                       *)
                    Message.msg_string Message.Debug ("Now doing post of "^(Operation.to_string oper)) ;
                    let next_reg = Abstraction.post current_reg oper global_variable_table 
                    in
(*
                    if (Abstraction.check_emptiness oper) then 
*)
                    if (true) then
                      if (Stats.time "Emptiness Check (Forward):" Region.is_empty next_reg) then
                        begin
                          Message.msg_string Message.Debug "Emptiness check succeeds: this is a false error";
                          let nd = match rest with
                            (h, _) :: _ -> h
                          | _ -> failwith "strange error path"
                          in
                          try
                            check_error nd (get_region nd)
                          with Invalid_argument _ -> (* This node was marked Unprocessed *)
                            begin (* get region in the tedious way *)
                              let reg = Abstraction.post (get_region (get_parent nd)) (get_op nd) global_variable_table in
                              (get_node_data nd).mark <- 
                                 Processed_Was_Covered_To_Reprocess { time_stamp = 0; region = reg } ;
                              check_error nd (reg)
                            end
                        end
                      else
                        forward_post next_reg rest
                    else
                      forward_post next_reg rest
                  end
            end
    in
    (* forward simulate starting from root and True *)
    forward_post reg_true error_path


  (* Greg: I don't like this get_op... try to add the operations in the path? *)



  let rec refine_path anc path =
    match path with
        []   -> ()
      | n::p ->
          stats_nb_refined_nodes := !stats_nb_refined_nodes + 1 ;

          Message.msg_string Message.Debug "Refining node:" ;
          Message.msg_printer Message.Debug print_tree_node n ;
          Message.msg_string Message.Debug "whose ancestor is:" ;
          Message.msg_printer Message.Debug print_tree_node anc ;

          let anc_reg = get_region anc
          and n_marking_data = get_marking_data n in

            Message.msg_string Message.Debug "calling Abstraction.post with region:" ;
            Message.msg_string Message.Debug ("    region    : " ^ (Region.to_string anc_reg)) ;
            Message.msg_string Message.Debug ("    operation : " ^ (Operation.to_string (get_op n))) ;

            n_marking_data.region <- Abstraction.post anc_reg (get_op n) global_variable_table;

            Message.msg_string Message.Debug "The refined node looks like:" ;
            Message.msg_printer Message.Debug print_tree_node n ;

            refine_path n p


  (* TODO from node root to unprocessed_nodes (ts is unused in DFS) *)
  let update_tree_after_refinment root unprocessed_nodes ts =
    (* does the work with n=root *)
    let update_and_compute_bfs n = 
      (* internal nodes, ... *)
      let nl_int = ref [] in
      (* ... leaf nodes, ... *)
      let nl_leaf = ref [] in
      let reached_reg = ref (if not (Options.getValueOfBool "stop-sep") then Region.bot else Region.reached_bot ()) in 
      (* ... and their iterators *)
      (* join regions in internal nodes with reached_reg *)
      let f_int = (fun n -> 
           Message.msg_string Message.Debug "cpa-fix: f_int cup node" ;
           Message.msg_printer Message.Debug print_tree_node n ;
           reached_reg := Region.cup !reached_reg (get_region n)
      ) in
      let f_leaf = (fun n -> nl_leaf := n::(!nl_leaf)) in
      (* traverse subtree starting from root and apply functions to them *)
      let _ = Tree.traverse_bfs f_leaf f_int n in
      let nl_leaf = List.rev !nl_leaf in
      let _ = Message.msg_string Message.Debug "Leaf Nodes" in
      let _ = Message.msg_string Message.Debug ("SKY: total "^(string_of_int (List.length nl_leaf))) in
      let _ = List.iter 
                (fun n -> Message.msg_printer Message.Debug  print_tree_node n) 
                nl_leaf
      in
      (* all leaves are iterated with this function *)
      let process n = 
        let n_data = get_node_data n in 
          match n_data.mark with
              Unprocessed ->
                assert (not (Tree.has_child n)) ;
                begin
                  match (Options.getValueOfString "comp") with
		      (* this is branch obsolete *)
                      "path" -> ();
		    (* this branch works *)
                    | _ ->      
                        unprocessed_nodes#add_element n ;
                        Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                        Message.msg_printer Message.Debug print_tree_node n ;
                end
            |  Processed_Was_Covered_To_Reprocess md ->
                 (* --- for debugging --- *)
                 assert (not (Tree.has_child n)) ;
                 begin
                   match (Options.getValueOfString "comp") with
		      (* this is branch obsolete *)
                       "path" -> ()
		    (* this branch works *)
                     | _ ->     
                         Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                         Message.msg_printer Message.Debug print_tree_node n ;
                         unprocessed_nodes#add_element n ;
                         (* Region.cup reached_region md.region  JHALA CHECK CHECK CHECK *)
                 end
            | Processed_Covered md ->
                (* --- for debugging --- *)
                assert (not (Tree.has_child n));
                if (not (Abstraction.covered md.region !reached_reg)) then
                  begin
                    n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
                    Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                    Message.msg_printer Message.Debug print_tree_node n ;
                    unprocessed_nodes#add_element n 
                  end
                else
                  ()
            | Processed_Uncovered md ->
                begin
                  (* it should be the case that this node, oddly enough, has NO children, i.e. this pc has no children,
                     so it doesnt matter if I add it to cover or not. Indeed I shouldnt, and I shouldnt try to add it to 
                     the list of things to process either -- RJ. *)
                  if (Abstraction.enabled_ops (get_region n) <> []) then
                    begin
                      Message.msg_string Message.Normal "About to die";
                      Message.msg_printer Message.Normal print_tree_node n ;
                      failwith "ugh! process_n_leaf gets an internal node!"
                    end
                end
     in
     let _ = List.iter process nl_leaf in
     !reached_reg
    in
    (* only in DFS. *)
    let rec update_and_compute reached_region n =
      let n_data = get_node_data n in
        match n_data.mark with
            Unprocessed -> 
              assert (not (Tree.has_child n)) ;
              begin
                match (Options.getValueOfString "comp") with
                    "path" -> ();
                  | _ ->        
                      unprocessed_nodes#add_element n ;
                      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                      Message.msg_printer Message.Debug print_tree_node n ;
              end;
              reached_region
                
          | Processed_Was_Covered_To_Reprocess md ->
              (* --- for debugging --- *)
              assert (not (Tree.has_child n)) ;
              begin
                match (Options.getValueOfString "comp") with
                    "path" -> reached_region
                  | _ ->        
                      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                      Message.msg_printer Message.Debug print_tree_node n ;
                      unprocessed_nodes#add_element n ;
                      
                      Region.cup reached_region md.region
              end
          | Processed_Covered md ->
              (* --- for debugging --- *)
              assert (not (Tree.has_child n)) ;
              if (md.time_stamp >= ts) then
                begin
                  n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
                  Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                  Message.msg_printer Message.Debug print_tree_node n ;
                  unprocessed_nodes#add_element n ;
                  reached_region
                end
              else
                Region.cup reached_region md.region
          | Processed_Uncovered md ->
              let new_reached_region = Region.cup reached_region md.region in
                List.fold_left update_and_compute new_reached_region (get_children n)
    in
      match (Options.getValueOfBool "bfs") with
          true -> update_and_compute_bfs root
        | false -> update_and_compute (if not (Options.getValueOfBool "stop-sep") then Region.bot else Region.reached_bot ()) root


(* ********************************************************************** *) 
  (* Jhala: Adding proof tree data structures *)
    (* module BlastProof = Abstraction.BlastProof *)
                          
    type proof_tree_node_data = {
      pt_id       : int; 
      pt_region   : Abstraction.proof_region;
      covering_nodes :  (Abstraction.lemma_index * (proof_tree_node ref list)) option; 
     (* Jhala: Right now, just dumping the tree, 
        the last field will say which nodes cover the present node *)
    }
    and proof_tree_edge = (Operation.t * Abstraction.lemma_index) 
    and proof_tree_node = (proof_tree_node_data, proof_tree_edge) Tree.node(* Jhala: not storing the operation on the edge *)
  (* Jhala: end proof tree data structures -- 
     rest are in the module "BlastProof" in abstraction.ml 
     -- Abstraction.lemma_index is just "int" *)


let print_proof_tree (root: proof_tree_node) =
  let print_pt_id n1 = print_int (Tree.get_node_label n1).pt_id;print_string " ; "
  in
  let print_pt_children_ids n1 = 
    print_string "Children of ";
    print_pt_id n1;print_string ": \n"; 
    print_string "["; 
    List.iter print_pt_id (Tree.get_children n1);
    print_string "]\n"
  in
  let print_pt_edge pt_e = 
    match pt_e with
        (op,i) ->  
          begin 
            print_string "op: ";
            Message.msg_printer Message.Debug Operation.print op ; 
            print_string "\n proof: " (* URGENT ! ;print_int i --TYPE ISSUES *)
          end
  in
  let print_pt_node_data n_data = 
    print_int n_data.pt_id;print_string "\n";
    Abstraction.print_proof_region n_data.pt_region;
    match n_data.covering_nodes with
        None -> print_string "Not covered \n"
      | Some(i,l) -> print_string "Covered \n"
  in

  let rec print_pt_node n =
    let _ = 
      match (Tree.get_parent_edge n) with
          None -> print_string "ROOT NODE Id: ";
        | Some(e) ->
            let par = Tree.get_source e in
              begin
                print_string "----par: ";print_pt_id par;print_string "\n";
                print_pt_edge (Tree.get_edge_label e);
                print_string "---->\n";
                print_string "INT NODE Id: "
              end 
    in
      print_pt_node_data (Tree.get_node_label n);
      print_pt_children_ids n;
      List.iter print_pt_node (Tree.get_children n)
  in
    print_pt_node root
      
(* count the size of the proof tree *)
let count_tree_nodes root = 
  Message.msg_string Message.Debug "Counting tree nodes";
  let update_counter n = 
    (* let us just count how many covered nodes there are *)
    if ((List.length (get_children n ) = 0)) then stats_nb_proof_tree_covered_nodes := !stats_nb_proof_tree_covered_nodes + 1;
    stats_nb_proof_tree_nodes := !stats_nb_proof_tree_nodes + 1; ()
  in
  let leaf_check_fn n = (get_children n) = [] in
    stats_nb_proof_tree_nodes := Tree.count_nodes_descendants  root;
    stats_nb_proof_tree_covered_nodes := Tree.count_f_descendants leaf_check_fn root

  (* Jhala: Functions to construct the proof tree *)
	(* --discarded (SKY) *)

(****************************************************************************)
(* auto here *)

(*********************************************************************************************************************)
(***********     Tree --> Automaton Conversion stuff    ***************************************************************)
(*********************************************************************************************************************)

(* given a complete reachability tree, will 
1. make a pass and find the node that covers each leaf
2. fold the edges back ie if n' covers n then when n'' --> n then will make n'' -> n' 
3. Quantify away local state
4. Minimize automaton
*)


  type auto_node_label = int * Region.abstract_data_region
  type auto_edge_label = Operation.t


let auto_node_to_string {EnvAutomaton.oid = i ; EnvAutomaton.oadr = adr} =
  Printf.sprintf "%d $$ %s" i (Region.abstract_data_region_to_string adr)

let auto_edge_to_string op =
  match op with
      Abstraction.ProgramOperation op -> Operation.to_string op
    | Abstraction.AutomatonOperation (i,j) -> (Printf.sprintf "AutomatonMove(%d,%d) " i j)


(*type shrink_node_label = EnvAutomaton.anode_label
    
type edge_type = EnvAutomaton.aedge_label*)


let shrink_node_to_string (s_l : EnvAutomaton.anode_label) = 
   let ats = (if s_l.EnvAutomaton.atomic then "*" else "")  in
   let tas = (if s_l.EnvAutomaton.task then "T" else "")  in
   let evs = (if s_l.EnvAutomaton.event then "E" else "")  in
   Printf.sprintf "%d $$ %d %s %s" s_l.EnvAutomaton.id s_l.EnvAutomaton.id
     (ats^tas^evs)
     (Region.abstract_data_region_to_string s_l.EnvAutomaton.adr)
(*
   Printf.sprintf "(%d , %d) : (%s, %s, [%s])" s_l.EnvAutomaton.id s_l.EnvAutomaton.root (Region.abstract_data_region_to_string s_l.EnvAutomaton.adr)
     (if s_l.EnvAutomaton.atomic then "atomic" else "") (Misc.strList (List.map Expression.lvalToString s_l.EnvAutomaton.modifies))
*)
let shrink_edge_to_string e_l = 
  match e_l with
      EnvAutomaton.Thread modl -> 
        "T: "^(Misc.strList (List.map Expression.lvalToString modl))
    | EnvAutomaton.Env -> "E"
     
  let print_graph filename root = 
        BiDirectionalLabeledGraph.output_graph_dot 
          shrink_node_to_string 
            shrink_edge_to_string filename
              [root]
  
let get_auto_node_id auto_node = 
  (BiDirectionalLabeledGraph.get_node_label auto_node).EnvAutomaton.oid

let get_modified_globals auto_node =
     let nlabel = BiDirectionalLabeledGraph.get_node_label auto_node in
     let outgoing_progops = List.fold_left 
       (fun sofar thisone -> match BiDirectionalLabeledGraph.get_edge_label thisone with 
          Abstraction.ProgramOperation op -> op::sofar | _ -> sofar) [] (BiDirectionalLabeledGraph.get_out_edges auto_node) in
     let gwrites = List.map (fun op -> List.filter (fun l -> (Abstraction.can_escape l) )
       (snd (Abstraction.reads_and_writes op))) outgoing_progops in
     List.fold_left (fun sofar thislist -> Misc.union thislist sofar) [] gwrites

let get_shrink_node_id sn = 
  (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.id
    
let quantify_env_states rt = rt (* TO DO *)

(* RJ: wrapping everything inside ONE GIANT function *)
 

 let race_lval () = 
   match !Abstraction.check_races with
       None -> failwith "No race variable!!"
     | Some x -> x 
let oe_is_dot e = 
    match BiDirectionalLabeledGraph.get_edge_label e with 
        Abstraction.AutomatonOperation _ -> true 
      | _ -> false 
  
  
let dotted_neighbours an =
    let dot_out_succ = List.map (BiDirectionalLabeledGraph.get_target) 
                          (List.filter oe_is_dot (BiDirectionalLabeledGraph.get_out_edges an))
    in
    let dot_in_succ = List.map (BiDirectionalLabeledGraph.get_source) 
                         (List.filter oe_is_dot (BiDirectionalLabeledGraph.get_in_edges an))
    in
      dot_in_succ @ dot_out_succ
  
let make_shrink_auto full_tree_root =
  Message.msg_string Message.Normal "In make_shrink_auto" ;
  let tree_root = quantify_env_states full_tree_root in
  (* this tells you the automaton node corresponding to a tree node *)
  let tree_auto_map = Hashtbl.create 1009 in

  (* this tells you the list of tree nodes corresponding to an auto node *)
  let auto_tree_map = Hashtbl.create 1009 in

  (* this map tells you whether or not a node is a back node *)
  let back_node_map = Hashtbl.create 1009 in
    
  (* this map is from int --> auto node, should one wish to directly access a node *)
  let auto_map = Hashtbl.create 1009 in

  (* returns the root of the constructed automaton which is a 
     (auto_node_label, auto_edge_label) BiDirectionalLabeledGraph.node *)
   let cov = Region.emptyCoverers () in (* this is now needed outside for the cover_set *)
     
   let tree_close root =  
    let unprocessed_nodes = 
      if (Options.getValueOfBool "bfs") then new Search.bfsiterator
      else new Search.dfsiterator
    in  
    let tree_size_count = ref 0 in
     
    let connect auto_node tree_node = 
      let a_id = get_auto_node_id auto_node in
      let t_id = get_id tree_node in
        Hashtbl.replace tree_auto_map t_id auto_node;
        Misc.hashtbl_update auto_tree_map a_id tree_node
    in
        (* function for phase 1: add tree nodes if required to the set of coverers *)
    let collect_coverers n = 
      let id = get_id n in
      let _ = Message.msg_string Message.Debug ("Collect coverers:"^(string_of_int id)) in
      let n_actual_region = get_region n in
      if (Region.is_empty n_actual_region) then 
        Message.msg_string Message.Debug ("Tree node empty")
      else
        let n_reg = Region.project_par_atomic (get_region n) in                     
        let cov_fun =
          if (Options.getValueOfBool "ecov") 
          then Region.findExactCoverer 
          else Region.findCoverer
        in
        let (coverer_id,is_covered) = 
          match cov_fun cov n_reg with
              Some i -> 
                begin
                  Message.msg_string Message.Debug ("Covered by: "^(string_of_int i));
                  (i,true)
                end
            | None -> 
                begin
                  if (Tree.get_children n = []) then
                    Message.msg_string Message.Debug
                      ("failed findcov: "^(string_of_int id)^": "^ (Region.to_string ( get_region n))) ;
                  
                  
                  let _ = Message.msg_string Message.Debug ("New auto node: "^(string_of_int id)) 
                  in
                  let new_auto_node = 
                    BiDirectionalLabeledGraph.create_root 
                      { EnvAutomaton.oid = id;
                        EnvAutomaton.oadr = (Abstraction.quantify_and_extract_abs_data_reg n_reg);
                        EnvAutomaton.oregion = n_reg
                      }
                      (* RJ: at this point we can quantify out local predicates as well *)
                  in
                    Hashtbl.add auto_map id new_auto_node;
                    Region.addCoverer cov n_reg id;
                    (id,false)
                end
        in
        let auto_node = 
          try Hashtbl.find auto_map coverer_id 
          with Not_found -> failwith "hashtbl fails in get_auto_node"
        in
          (* isnt tree_auto_map same as auto_map ? no ... *)
          (*  let auto_node_id = get_auto_node_id auto_node in *) (* isnt this always coverer_id ? RJ *)
          (* Hashtbl.replace back_node_map coverer_id true; -- this is never used *)
          connect auto_node n;
          ()
    in

    let get_auto_node n = 
      let id = get_id n in
        try
          Hashtbl.find tree_auto_map id 
        with Not_found -> failwith ("didnt see this node in collect_coverers"^(string_of_int id))
    in
      (* Another function for phase 1: add the tree edges ... *)
    let add_tree_edge (n_parent,tree_edge_label,n) =
      tree_size_count := !tree_size_count + 1;
      let n_actual_region = get_region n in
      let n_region = Region.project_par_atomic n_actual_region in
        if not (Region.is_empty n_actual_region) (* check for emptiness of the tree's actual region! *) 
        then
          begin
            let (n_auto) = get_auto_node n in (* this will also update the maps if needed *)
            
            let np_id = get_id n_parent in
            let n_id = get_id n in
              

            let n_parent_auto = get_auto_node n_parent in
            let npa_id = get_auto_node_id n_parent_auto in
            let na_id = get_auto_node_id n_auto in
            let _ =   Message.msg_string Message.Debug 
                (Printf.sprintf "Autoedge (%d,%d) from tree edge (%d,%d)" 
                   npa_id na_id np_id n_id)
            in
              BiDirectionalLabeledGraph.hookup_parent_child 
                n_parent_auto n_auto 
                (tree_edge_label).op
          end
    in
      (* first, get all the states *)
      BiDirectionalLabeledTree.iter_descendants_nodes collect_coverers root;
      (* second, add all the edges *)
      BiDirectionalLabeledTree.iter_descendants_edges add_tree_edge root;
      (* the "orig_auto" is now built! *)
      Message.msg_string Message.Normal "Main loop of make_shrink_auto done" ;
      try
        let init_node = Hashtbl.find tree_auto_map (get_id root) in
        let _  =  BiDirectionalLabeledGraph.output_graph_dot 
            auto_node_to_string 
              auto_edge_to_string 
                (Options.getValueOfString "auto") [init_node] in
  init_node 
    (* return the start node of the auto *)
 with
     Not_found ->
       failwith "No tree root ! Revolting !"
       
 in

 (** now we'd like to minimize the automaton to get the shrink auto which is finally what is needed *)
 (** a map from id --> shrink_node *)
 let shrink_map = Hashtbl.create 1009 in

 (** this maps a shrink_id -> auto_id list. ie this is the inverse of the next map *)
 let shrink_elts_map = Hashtbl.create 1009 in

 (** auto_node_id -> shrink_node id *)
 let auto_shrink_map = Hashtbl.create 1009 in

 let auto_shrink_node an =
   let (a_id) = get_auto_node_id an in
     Hashtbl.find shrink_map (Hashtbl.find auto_shrink_map a_id)
 in

 let shrink_counter = ref 0 in
     
 let new_shrink_id () = 
   shrink_counter := !shrink_counter + 1;
   !shrink_counter
 in

 let gvars = (race_lval ()) :: (Abstraction.globally_important_lvals ()) in

 let _ = Message.msg_string Message.Normal "Glob Imp lvals are:" in
 let _ = Message.msg_string Message.Normal (Misc.strList (List.map Expression.lvalToString gvars)) in

 let irrelev_shrink_edge e = 
   match (BiDirectionalLabeledGraph.get_edge_label e) with
       EnvAutomaton.Thread l -> List.for_all (fun lv -> not (List.mem lv gvars)) l 
         |  _ ->  true
 in
         
       let similar_edges e = 
         let (sn,_,sn') = BiDirectionalLabeledGraph.deconstruct_edge e in
         let oe = BiDirectionalLabeledGraph.get_out_edges sn in
List.filter (fun e' -> BiDirectionalLabeledGraph.get_target e' = sn') oe
in
 let multiple_par a_id par_id = 
   let an = try Hashtbl.find auto_map a_id with Not_found -> failwith "unknown autonode!" in
   let pars = BiDirectionalLabeledGraph.get_parents an in
   List.exists (fun n -> 
                  let n_id = get_auto_node_id n in 
                    n_id <> par_id 
                    && n_id <> a_id 
                    && par_id <> a_id  (* self-loops dont count !*)
               ) pars 
   
 in
 let cluster_edges end_pt_fun e_list = 
   let nt = Hashtbl.create 7 in
   let proce e = 
     if Hashtbl.mem nt (end_pt_fun e) then ()
     else Hashtbl.replace nt (end_pt_fun e) e
   in
     List.iter proce e_list;
     List.map snd (Misc.hashtbl_to_list nt)
 in

  (** this function is used to filter nodes where we stop growing, args are auto_nodes  *)
 let stop_growing auto_root parent out_edge =
   let elabel = BiDirectionalLabeledGraph.get_edge_label out_edge in
     if (Options.getValueOfBool "dotsep" && (Abstraction.type_of_edge elabel  = EnvAutomaton.Env))
     then true
     else
   let child = BiDirectionalLabeledGraph.get_target out_edge in
   let {EnvAutomaton.oid = c_id;
        EnvAutomaton.oadr = c_adr;
        EnvAutomaton.oregion = c_reg;
       } = 
     BiDirectionalLabeledGraph.get_node_label child
   in
   let par_id = get_auto_node_id parent in
     if (* non unique parent should suffice: Hashtbl.mem back_node_map c_id*) (* RJ: check this it gives garbage! *)
       (((*not*) (multiple_par c_id par_id)) || c_id = (get_auto_node_id auto_root))
     then 
       (Message.msg_string Message.Normal ("non-unique par"^(string_of_int c_id));
       true)
     else
       let { EnvAutomaton.oadr = p_adr;
             EnvAutomaton.oregion = p_reg
           } = (BiDirectionalLabeledGraph.get_node_label parent)
       in
       let rv = 
         not (Region.adr_eq c_adr p_adr 
              && ((* Options.getValueOfBool "atc" = false 
                  ||*)  (Abstraction.is_atomic c_reg = Abstraction.is_atomic p_reg)))
       in
         if rv then 
           Message.msg_string Message.Normal ("adrs differ"^(string_of_int c_id));
         rv
           (* no need to do the modifies  check here .. because we're still in oauto land ... 
              and all the shrink nodes have a single entry point ... 
           *)
 in   

(* TBD: supply the list of vars modified at this node -- can be post-computed looking at the tree map *)

 let new_shrink_node auto_node = 
   let new_id = new_shrink_id () in
   let { EnvAutomaton.oid = root_id; 
         EnvAutomaton.oadr = an_adr;
         EnvAutomaton.oregion = oreg
       }
     = BiDirectionalLabeledGraph.get_node_label auto_node 
   in
   let label = { EnvAutomaton.id = new_id; 
                 EnvAutomaton.root = root_id; 
                 EnvAutomaton.adr = an_adr ; 
                 EnvAutomaton.atomic = Abstraction.is_atomic oreg;
                 EnvAutomaton.task = Abstraction.is_task oreg;
                 EnvAutomaton.event = Abstraction.is_event oreg;
                 EnvAutomaton.modifies = [] (* get_modified_globals auto_node *) 
               } 
   in
   let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map new_id new_sn;
     new_sn
 in       

 let shrink_stuck_node = 
     let label = { EnvAutomaton.id = 0; 
                   EnvAutomaton.root = 0 (* junk *); 
                   EnvAutomaton.atomic = false ;
                   EnvAutomaton.task = false ;
                   EnvAutomaton.event = false ;
                   EnvAutomaton.adr = Region.true_adr ; 
                   EnvAutomaton.modifies = []} in
     let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map 0 new_sn;
     new_sn
 in
 let edge_list = ref [] in

 let add_edges () = 
   let add_edge e =
     let (an1,a_edge,an2) = BiDirectionalLabeledGraph.deconstruct_edge e in
       (* an1 should be  either a back node or should have a different adr than an2 *)
       (* we should actually trawl the auto to find all the global lvals that may have been modified
          but for now I shall postpone that VVVVVIMP. TBD *)
       (* RJ: never add a dotted edge to itself ! *)

     let shrink_par = auto_shrink_node an1 in
     let shrink_child =  auto_shrink_node an2 in
     let es = Printf.sprintf " (%d, %d)  ... (%d,%d) " (get_auto_node_id an1) (get_auto_node_id an2)(get_shrink_node_id  shrink_par) (get_shrink_node_id shrink_child) in
     let el = Abstraction.type_of_edge a_edge  in
       if (shrink_child = shrink_par && el = EnvAutomaton.Env) then
         Message.msg_string Message.Normal "Self dotted edge ... eschew!"
       else
         let _ = Message.msg_string Message.Normal ("Adding edge: "^es) in 
           
           (* if a similar edge exists dont re-add it ... just update ... *)
           BiDirectionalLabeledGraph.hookup_parent_child shrink_par shrink_child el
   in
     List.iter add_edge !edge_list
 in      
   (* connect the auto_node and the shrink node *)    
 let connect_shrink_auto sn an =
   let snlabel =BiDirectionalLabeledGraph.get_node_label sn in 
   let s_id = snlabel.EnvAutomaton.id in
   let a_id = get_auto_node_id an in
     Misc.hashtbl_update shrink_elts_map s_id a_id;
     (* 
        let modified_globals = get_modified_globals an in
        snlabel.EnvAutomaton.modifies <- Misc.union modified_globals snlabel.EnvAutomaton.modifies ;*)
     Hashtbl.replace auto_shrink_map a_id s_id
 in 
 
 let shrink_auto auto_root = 
   let _stop_growing = stop_growing auto_root in
  (* should we clear the hashtbls ? *)
  let visited_table = Hashtbl.create 1009 in
  let unprocessed_nodes = 
    if (Options.getValueOfBool "bfs") then new Search.bfsiterator
    else new Search.dfsiterator
  in
  let _ = edge_list := [] in
  let shroot = ref None in
    (* Phase 1 shrinking functions *)
  let rec grow shrink_node auto_node =
    (* a check to make sure *)
    if (Hashtbl.mem visited_table (get_auto_node_id auto_node))
    then ()
    else
      begin
        Hashtbl.add visited_table (get_auto_node_id auto_node) true;
        connect_shrink_auto shrink_node auto_node;
        let _l =  BiDirectionalLabeledGraph.get_out_edges auto_node in
        let l = 
          List.filter (fun e -> (BiDirectionalLabeledGraph.get_target e) <> auto_node) _l 
        in
        let (l1,l2) =  Misc.filter_cut (_stop_growing  auto_node) l  in
          List.iter (fun e -> unprocessed_nodes#add_element 
                       (BiDirectionalLabeledGraph.get_target e)) l1; 
          (* actually should only add the unprocessed ones but... *)
          List.iter (fun e -> edge_list := e::!edge_list) l1;
          List.iter (grow shrink_node) (List.map BiDirectionalLabeledGraph.get_target l2) 
            
(* INV: no  nodes in l2 is a back node i.e. no node in l2 is a coverer ..
   hence no node in l2 has been processed *)
      end
  in
  let process_auto_node node = 
    match (Hashtbl.mem visited_table (get_auto_node_id node)) with
        true -> ()
      | false -> 
          let sn = new_shrink_node node in
            grow sn node
              
  in
  let gather_modifies_info () = 
    let gather_sn_info sn_id sn = 
      let mynodes = 
        List.map 
          (fun a_id -> try Hashtbl.find auto_map a_id with Not_found -> failwith "grumble") 
          (try 
            (Hashtbl.find shrink_elts_map sn_id) 
          with Not_found -> (assert (sn_id = 0) (* the stuck node... *); [])) 
      in
        (* for each node, on each edge gather successors *)
      let edge_is_local e = 
        try
          sn_id = 
        Hashtbl.find auto_shrink_map 
          (get_auto_node_id (BiDirectionalLabeledGraph.get_target e))
        with  Not_found -> failwith "NO HERE"
      in
      let globs_on_edge e = 
        match (BiDirectionalLabeledGraph.get_edge_label e) with
            Abstraction.ProgramOperation op -> 
              List.filter (fun l -> (Abstraction.can_escape l))
              (snd (Abstraction.reads_and_writes op))
          | _ -> []
      in
      let internal_mods an = 
        let local_e = List.filter  edge_is_local (BiDirectionalLabeledGraph.get_out_edges an) 
        in
          Misc.compact (List.flatten (List.map globs_on_edge local_e))
      in
      let local_mods = Misc.compact (List.flatten (List.map internal_mods mynodes)) in
        Message.msg_string Message.Normal ("adding mods to : "^(string_of_int sn_id));
        print_string (Misc.strList (List.map Expression.lvalToString local_mods)); 
        (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.modifies <- local_mods
    in
      Hashtbl.iter gather_sn_info shrink_map
  in
  let acc_at_e e = 
      match BiDirectionalLabeledGraph.get_edge_label e with
          EnvAutomaton.Thread l -> l
        | _ -> []
  in      
    (* Phase 2 shrinking functions *)
  let all_accesses_at_shrink_node sn = 
    (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.modifies
    @
    (List.flatten (List.map  acc_at_e (BiDirectionalLabeledGraph.get_out_edges sn)))
  in
  let shrink_visited_table = Hashtbl.create 1009 in
 
 let unique_parent sn = 
    let pars = List.filter (fun n' -> n' <> sn) 
                 (BiDirectionalLabeledGraph.get_parents sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) pars in
      List.length (Misc.compact ids) = 1
  in
  let unique_child sn = 
    let kids = List.filter (fun n' -> n' <> sn) 
                 (BiDirectionalLabeledGraph.get_children sn) 
    in
    let ids = List.map (fun n -> get_shrink_node_id n) kids in
      List.length (Misc.compact ids) = 1
  in
 let really_unique_parent sn = 
    let pars = (BiDirectionalLabeledGraph.get_parents sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) pars in
      List.length (Misc.compact ids) = 1
  in
  let really_unique_child sn = 
    let kids =  (BiDirectionalLabeledGraph.get_children sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) kids in
      List.length (Misc.compact ids) = 1
  in

    (* this checks if sn2 can be merged into sn1 *)
  let sn_adr_eq dir sn1 sn2 =
    let target_check_flag = true in
    let 
      { EnvAutomaton.adr = sn1_adr;
        EnvAutomaton.atomic = sn1_atomic;
        EnvAutomaton.task = sn1_task;
        EnvAutomaton.event = sn1_event;

      } = (BiDirectionalLabeledGraph.get_node_label sn1)
    in
    let
      { EnvAutomaton.adr = sn2_adr;
        EnvAutomaton.atomic = sn2_atomic;
        EnvAutomaton.task = sn2_task;
        EnvAutomaton.event = sn2_event;
      } = (BiDirectionalLabeledGraph.get_node_label sn2)
    in
  
    let lm mods = List.map (fun lv -> List.mem lv mods) gvars in
    let [sn1_mods;sn2_mods] = List.map all_accesses_at_shrink_node [sn1;sn2] in
 
    let (rlv1,rlv2) = (lm sn1_mods, lm sn2_mods) in
    let lv_check = 
      match Options.getValueOfBool "lvc" with
          false -> (if dir then Misc.boolean_list_leq rlv1 rlv2
                    else Misc.boolean_list_leq rlv2 rlv1)
        | true ->  (rlv1 = rlv2)
    in
    let sn1a_tup = (sn1_atomic,sn1_task,sn1_event) in    
    let sn2a_tup = (sn2_atomic,sn2_task,sn2_event) in
    let atcheck = 
      if (Options.getValueOfBool "alt") then (sn1a_tup =sn2a_tup)
      else (sn1_atomic = sn2_atomic)
    in
      (Region.adr_eq sn1_adr sn2_adr) 
      && ( (* Options.getValueOfBool "atc" = false || *) (atcheck))
        && (lv_check)  (* modify the same set of globally important lvals *)
      in 
  let merge_and_delete sn sn' = 
    (* sn' is going to get obliterated eventually *)

    let sn_id = get_shrink_node_id sn in
    let sn'_id = (get_shrink_node_id sn') in
    let ds = Printf.sprintf "Merging %d with %d" sn'_id sn_id in
    let _ = Message.msg_string Message.Debug ds in
    let _ = if (sn'_id = get_shrink_node_id sn) then failwith "deleting self!" in
    let an_l = List.map (Hashtbl.find auto_map) (Hashtbl.find shrink_elts_map sn'_id) in
    let _ = List.iter (connect_shrink_auto sn) an_l in 
    let _ = Hashtbl.remove shrink_map sn'_id in (* what if sn = sn' ????? *)
    let _ = Hashtbl.remove shrink_elts_map sn'_id in
    let _ = BiDirectionalLabeledGraph.delete_node sn' in
      if !shroot = Some sn'_id then shroot := Some (sn_id);
      ()
  in
 (* let merge_edge e = (* the merged node is sn, the source of the edge *)
    let (sn,e_l,sn') = BiDirectionalLabeledGraph.deconstruct_edge e in
    let sn'_id = (get_shrink_node_id sn') in
   let an_l = List.map (Hashtbl.find auto_map) (Hashtbl.find shrink_elts_map sn'_id) in
   let _ = List.iter (connect_shrink_auto sn) an_l in
    let _ = Hashtbl.remove shrink_map sn'_id in
   l et out_edges = BiDirectionalLabeledGraph.get_out_edges sn' in
    let in_edges = Misc.list_remove (BiDirectionalLabeledGraph.get_in_edges sn') e in
    let  _ = List.iter (BiDirectionalLabeledGraph.copy_edge_source sn) out_edges in
    let _  = List.iter (BiDirectionalLabeledGraph.copy_edge_target sn) in_edges in
      if !shroot = Some sn' then shroot := Some sn;
      BiDirectionalLabeledGraph.delete_node sn' 
     in *)
  
  (* this next function cannot loop forever as you dont grow if the target has multiple parents and
     in a cycle some node must have multiple parents *)
  let grow_component target_check_flag dir sn = 
    let node_l = ref [] in
    let edge_l = ref [] in
    let (endpt_fn,succ_fn,grow_fn) = 
      if dir 
      then (BiDirectionalLabeledGraph.get_target,
            BiDirectionalLabeledGraph.get_out_edges,unique_parent)
      else (BiDirectionalLabeledGraph.get_source,
            BiDirectionalLabeledGraph.get_in_edges,unique_child)
    in 
    let visited_table = Hashtbl.create 37 in
    let is_solid e = 
      match (BiDirectionalLabeledGraph.get_edge_label e) with
          EnvAutomaton.Thread _ -> true
        | _ -> false
    in
    let rec _ac_grow_comp e =
      let _sn = endpt_fn e in
      let _sn_id = get_shrink_node_id _sn in
      let e_dot_ok = (Options.getValueOfBool "dotsep" = false || is_solid e) in
      let is_dot_edge = (Options.getValueOfBool "dotsep" = false && not (is_solid e)) in
      let edge_check_ok = 
        if (Options.getValueOfBool "lvc") then true (* that will deal with it ... *)
        else 
          let all_e = BiDirectionalLabeledGraph.get_all_edges_like e in
          let mods_imp e = (Misc.intersection (acc_at_e e) gvars) <> [] in
            List.exists mods_imp all_e 
      in (* if some edge between these nodes changes something imp, then please dont grow... *)
      let _sn_not_root = (not dir || (Some _sn_id  <> !shroot))  in
      let grow_cond = _sn_not_root && edge_check_ok && e_dot_ok && grow_fn _sn && (sn_adr_eq dir  sn _sn) in
        if (grow_cond ) then 
          (* maintains atomicity *)
          begin
            if (Hashtbl.mem visited_table (_sn_id)) then ()
            else
              begin
                Hashtbl.replace visited_table _sn_id true;
                node_l := _sn::!node_l;
                List.iter _ac_grow_comp (succ_fn _sn)
              end
          end
        else
          edge_l := e :: !edge_l
    in
      (* this makes sure you dont add sn to the node_l which will get it deleted!! *)
      Hashtbl.replace visited_table (get_shrink_node_id sn) true;
      List.iter _ac_grow_comp (succ_fn sn);
      (!node_l,!edge_l) 
      
  (* the first is the nodes that should be merged with sn, the second, the outgoing edges of this comp*)
  in
    
  let rec dshrink target_check_flag dir sn = 
    let (succ_fn, copy_edge) = 
      if dir 
      then (BiDirectionalLabeledGraph.get_target, BiDirectionalLabeledGraph.copy_edge_source)
      else (BiDirectionalLabeledGraph.get_source, BiDirectionalLabeledGraph.copy_edge_target)
    in
    let sn_id = (get_shrink_node_id sn) in
    let _ = Message.msg_string Message.Normal ("In dshrink: "^(string_of_int sn_id)) in
    if (Hashtbl.mem shrink_visited_table sn_id) then 
      Message.msg_string Message.Normal ("In dshrink: Visited already "^(string_of_int sn_id))
    else
      begin
        let _ = Hashtbl.add shrink_visited_table sn_id true in
    Message.msg_string Message.Normal ("In dshrink: Added this nodeto the hashtbl") ;
    (*BiDirectionalLabeledGraph.output_graph_dot shrink_node_to_string shrink_edge_to_string "" [sn] ;*)
        let (nl,el) = grow_component target_check_flag dir sn in        
    Message.msg_string Message.Normal ("In dshrink: components grown") ;
        let _ = List.iter (merge_and_delete sn) nl in
    Message.msg_string Message.Normal ("In dshrink: merged and deleted ") ;
        let _ = List.iter (copy_edge sn) el in
          List.iter (dshrink target_check_flag dir) (List.map succ_fn el) (* used to be get_target *)
      end
 in

let deatomize shrink_root = 
  (* will visit each node, and see if we really need it to be atomic *)
  (* a node sn is atomic if exists a path sn -> .... sn_k -- write x ---> ... 
     where x is an imp var, and each of sn, sn1, ..., sn_k is atomic 
  
     THIS IS A HEURISTIC (no!), but sound.
  *)
  let ignorable_globals = (* JHALA TBPLDI *)
    List.map (fun s -> Expression.Symbol s) ["__BLAST_interrupt_enabled";"__BLAST_task_enabled"] 
  in
  let nice_gvars = Misc.difference gvars ignorable_globals in
  let _ = Message.msg_string Message.Normal 
            ("Nice gvars:"^(Misc.strList (List.map Expression.lvalToString nice_gvars)))
  in
  let irrelev_atomic e = 
    match (BiDirectionalLabeledGraph.get_edge_label e) with
        EnvAutomaton.Thread l -> 
          List.for_all (fun lv -> not (List.mem lv nice_gvars)) l 
            |  _ ->  true
          in
            
  let shrink_node_is_atomic sn = 
    let sn_label = (BiDirectionalLabeledGraph.get_node_label sn) in
      sn_label.EnvAutomaton.atomic
  in

  let atomic_desc sn = 
    if not (shrink_node_is_atomic sn) then []
    else 
      List.filter shrink_node_is_atomic (BiDirectionalLabeledGraph.get_children sn)
  in

  let proc_node sn =
    if (not (shrink_node_is_atomic sn)) then ()
    else
      let sn_atomic_comp = BiDirectionalLabeledGraph.collect_component atomic_desc [sn] in
      let sn_ac_edges = List.flatten 
                          (List.map BiDirectionalLabeledGraph.get_out_edges sn_atomic_comp)
      in
        
      let new_atomic = List.exists (fun e -> not (irrelev_atomic  e)) sn_ac_edges
      in
        (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.atomic <- new_atomic
  in

    List.iter proc_node (BiDirectionalLabeledGraph.descendants [shrink_root])

in
  (* this keeps hitting the graph with "rewrites" until no change is possible ... *) 
let iterate_all_shrinks shrink_root = 
  let sn_adr_eq = sn_adr_eq true in

  let change_flag = ref true in

  let shrinkable_then_shrink sn = 

   let self_loop_shrink sn =
     (* remove all irrelevant self loop edges *)
     List.iter ( fun e -> 
                   if (irrelev_shrink_edge e && BiDirectionalLabeledGraph.get_target e = sn)
                   then 
                     begin
                       change_flag := true;
                       BiDirectionalLabeledGraph.delete_edge e
                     end
               ) (BiDirectionalLabeledGraph.get_out_edges sn)
   in
   let pullin_child_shrink sn = 
     (* if there is a child with:
        1. same reg etc etc, 
        2. has a unique parent, 
        3. w/ an irrelevant edge ...
        4. is not the root
        5. all the other edges are irrelevant ...
        then delete it and copy its successors to me...
     *)
     let _ = Message.msg_string Message.Debug "In pullin_child" in
     let suck_succ e = 
       let sn' = BiDirectionalLabeledGraph.get_target e in
       let sim_edges = similar_edges e in
       if ((List.for_all irrelev_shrink_edge sim_edges) 
           && sn_adr_eq sn sn' 
           && (really_unique_parent sn') (* so no self loop *)
           && (Some (get_shrink_node_id sn') <> !shroot)
           && (sn <> sn'))
       then
         begin
           change_flag := true;
           List.iter (BiDirectionalLabeledGraph.copy_edge_source sn) 
             (BiDirectionalLabeledGraph.get_out_edges sn');
           merge_and_delete sn sn'
         end
    in 
    let oute = BiDirectionalLabeledGraph.get_out_edges sn in
       List.iter suck_succ (cluster_edges BiDirectionalLabeledGraph.get_target oute)
  in
  let pullin_parent_shrink sn =   
    let _ = Message.msg_string Message.Debug "In pullin_parent" in
    
     (* reverse of the above -- the above is dshrink true -- this is dshrink false *)
    let suck_pred e = 
       let sn' = BiDirectionalLabeledGraph.get_source e in
       let sn = BiDirectionalLabeledGraph.get_target e in
       let sim_edges = similar_edges e in
       if ((List.for_all irrelev_shrink_edge sim_edges) 
           && sn_adr_eq sn sn' 
           && (really_unique_child sn') (* so no self loop *)
           && (Some (get_shrink_node_id sn') <> !shroot)
           && (sn <> sn'))
       then
         begin
           change_flag := true;
           List.iter (BiDirectionalLabeledGraph.copy_edge_target sn) 
             (BiDirectionalLabeledGraph.get_in_edges sn');
           merge_and_delete sn sn'
         end
    in 
    (* if (Some (get_shrink_node_id sn) = !shroot) then ()
    else *) 
      let ine = BiDirectionalLabeledGraph.get_in_edges sn in
        List.iter suck_pred (cluster_edges BiDirectionalLabeledGraph.get_source ine)
  in

  let merge_children_shrink sn =  
    let _ = Message.msg_string Message.Debug "In merge_children_shrink" in
    let merge_children sn1 sn2 = 
      (* copy sn2's out edges to sn1, merge_and_delete sn1 sn2 *)
      let _ = List.iter (BiDirectionalLabeledGraph.copy_edge_source sn1) 
                (BiDirectionalLabeledGraph.get_out_edges sn2)
      in
      let _ = 
        List.iter 
          (BiDirectionalLabeledGraph.hookup_wo_duplicate sn sn1) 
          (List.map BiDirectionalLabeledGraph.get_edge_label
             (BiDirectionalLabeledGraph.find_edges sn sn2))
      in (* this copies all edges from sn --> sn2 to sn --> sn1 *)
        merge_and_delete sn1 sn2;
          change_flag := true
    in
    let candidates = Misc.compact (BiDirectionalLabeledGraph.get_children sn) in
    let find_appt_sibling sn1 =
      if (not (really_unique_parent sn1)) then None
      else 
        let good_sibling sn' = 
          if (sn_adr_eq sn1 sn') && (sn' <> sn1) && (really_unique_parent sn')
          then Some (sn') else None
        in
          match (Misc.get_first good_sibling candidates) with
              None -> None
            | Some (sn') -> Some (sn1,sn')
    in
      match Misc.get_first find_appt_sibling candidates with
          None -> ()
        | Some (sn1,sn2) -> merge_children sn1 sn2
  in

  let merge_parents_shrink sn = 
   let _ = Message.msg_string Message.Debug "In merge_parents_shrink" in
    let merge_parents sn1 sn2 = 
      (* copy sn2's in  edges to sn1, merge_and_delete sn1 sn2 *)
      let _ = List.iter (BiDirectionalLabeledGraph.copy_edge_target sn1) 
                (BiDirectionalLabeledGraph.get_in_edges sn2)
      in
      let _ = 
        List.iter 
          (BiDirectionalLabeledGraph.hookup_wo_duplicate sn1 sn) 
          (List.map BiDirectionalLabeledGraph.get_edge_label
             (BiDirectionalLabeledGraph.find_edges sn2 sn))
      in (* this copies all edges from sn --> sn2 to sn --> sn1 *) 
        merge_and_delete sn1 sn2;
          change_flag := true
    in
    let candidates = Misc.compact (BiDirectionalLabeledGraph.get_parents sn) in
    let find_appt_psibling sn1 =
      if (not (really_unique_child sn1)) then None
      else 
        let good_psibling sn' = 
          if (sn_adr_eq sn1 sn') && (sn' <> sn1) && (really_unique_child sn')
          then Some (sn')
          else None
        in
          match (Misc.get_first good_psibling candidates) with
              None -> None
            | Some (sn') -> Some (sn1,sn')
    in
      match Misc.get_first find_appt_psibling candidates with
          None -> ()
        | Some (sn1,sn2) -> merge_parents sn1 sn2
  in
   (* begin the main thing in shrinkable_then_shrink *)
    self_loop_shrink sn;
    pullin_child_shrink sn;
    pullin_parent_shrink sn;
    merge_children_shrink sn;
    if (Options.getValueOfBool "nomp") then () else (merge_parents_shrink sn)
   (* end main thing in shrinkable_then_shrink *)
in
  while !change_flag do
    let all_nodes = BiDirectionalLabeledGraph.descendants [shrink_root] in
      change_flag := false;    
      Message.msg_string Message.Debug 
        ("shrink_hammer: size = "^(string_of_int (List.length all_nodes)));
      List.iter shrinkable_then_shrink all_nodes
  done;
()
in                                
     
(* 1. fuse the nodes in nl to sn
   2. copy the edges in el to sn
*)

    (* phase 1: grow components till a back node or different adr is hit *)
let phase1 () = 
    Message.msg_string Message.Normal "Phase 1" ;
    unprocessed_nodes#add_element auto_root;
    while (unprocessed_nodes#has_next ()) do
      process_auto_node (unprocessed_nodes#next ())
    done;
  
 in

let dsquish_phase1 () = 
  Message.msg_string Message.Normal "Phase 1: dsq";
  let dsq_visit_table = Hashtbl.create 37 in
  
  let proc_an an = 
    if (Hashtbl.mem dsq_visit_table an) then ()
    else
      let _ = Hashtbl.replace dsq_visit_table an true in
      let shrink_node = new_shrink_node an in
      let _ = Message.msg_string Message.Normal ("dsquish: auto node:"^
              (string_of_int (get_auto_node_id an)))
      in
      let an_cc = BiDirectionalLabeledGraph.collect_component dotted_neighbours [an] in
      let _ = List.iter (fun a -> Hashtbl.replace dsq_visit_table a true) an_cc in

      let _ = Message.msg_string Message.Normal ("dcomponent:"^(Misc.string_of_int_list (List.map get_auto_node_id an_cc))) in
      let _adr_cc = List.fold_left 
                     (fun curr b -> Region.adr_cup curr (BiDirectionalLabeledGraph.get_node_label b).EnvAutomaton.oadr )
                     ((BiDirectionalLabeledGraph.get_node_label an).EnvAutomaton.oadr) 
                     an_cc
      in
      let adr_cc = 
        if (Options.getValueOfBool "csq") 
        then Region.cartesian_adr _adr_cc else _adr_cc
      in
      let edge_leaving an' = 
        List.filter (fun e -> not (oe_is_dot e))
          (
            (BiDirectionalLabeledGraph.get_out_edges an') 
            @ (BiDirectionalLabeledGraph.get_in_edges an'))
      in
      let _ = edge_list := (List.flatten (List.map edge_leaving an_cc)) @ !edge_list in
      let _ = List.iter (connect_shrink_auto shrink_node) an_cc in
      (BiDirectionalLabeledGraph.get_node_label shrink_node).EnvAutomaton.adr <- adr_cc 
        
  in
    List.iter proc_an (BiDirectionalLabeledGraph.descendants [auto_root])
in

let _ = if (Options.getValueOfBool "dsq") then dsquish_phase1 () else phase1 () in
  Message.msg_string Message.Normal "Shrink elts info";
    let il_to_s il = Misc.strList (List.map string_of_int il) in
    Hashtbl.iter (fun i elts_l -> Message.msg_string Message.Normal
                    (Printf.sprintf "%d : %s" i (il_to_s elts_l))) shrink_elts_map;
  Message.msg_string Message.Normal "Auto info";

    gather_modifies_info (); (* first, collect the modifies info for each shrink node created thus far *)
    add_edges ();
    (* phase 2: if a node has a single parent with the same adr then merge *)
    Message.msg_string Message.Normal "Phase 2" ;
    let shrink_root = auto_shrink_node auto_root in
    let _ = shroot := Some (get_shrink_node_id shrink_root) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr1" else fn) shrink_root in
    Message.msg_string Message.Normal "Phase 2.2" ;
    let _  = dshrink true true shrink_root in 
    Message.msg_string Message.Normal "Phase 2.5" ;
    let all_shrunk_nodes = BiDirectionalLabeledGraph.descendants [shrink_root] in
    let _ = Hashtbl.clear shrink_visited_table in
    Message.msg_string Message.Normal "Phase 2.6" ;
    let _ = List.iter (dshrink true false) all_shrunk_nodes in
      (* how many times shall I iterate ? *)
    let _ = Hashtbl.clear shrink_visited_table in
    let _ = Message.msg_string Message.Normal "Phase 2.8" in
    
    let _ = Message.msg_string Message.Normal "Phase 3" in
    let sid = 
      match !shroot with Some i -> i | None -> failwith "Unknown shroot" in
    let shrink_root = try Hashtbl.find shrink_map sid with _ -> failwith ("shrink_root: "^(string_of_int sid)) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr2" else fn) shrink_root  
    in
    let _ = 
      if (Options.getValueOfBool "atc") then
        begin
          Message.msg_string Message.Normal "Phase 2.9 : De-atomize";
          deatomize shrink_root
        end
    in
    let _ = iterate_all_shrinks shrink_root in
    let sid = 
      match !shroot with Some i -> i | None -> failwith "Unknown shroot" in
    let shrink_root = try Hashtbl.find shrink_map sid with _ -> failwith ("shrink_root: "^(string_of_int sid)) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr2" else fn) shrink_root  
    in
    (* update modifies information *)
(*    Hashtbl.iter 
     (fun key nd -> let list_of_real_nodes = List.map (fun d -> Hashtbl.find auto_map d) (Hashtbl.find shrink_elts_map key) in
          let this_modifies = 
            List.fold_left (fun modlist nextnode -> 
              let outgoing_ops = List.fold_left 
                  (fun sofar thisone -> match BiDirectionalLabeledGraph.get_edge_label thisone with 
                    Abstraction.ProgramOperation op -> op::sofar | _ -> sofar) [] (BiDirectionalLabeledGraph.get_out_edges nextnode) in
              let (_, writes) = Abstraction.reads_and_writes op in 
              Misc.union writes modlist
              ) [] list_of_real_nodes in
            (BiDirectionalLabeledGraph.get_node_label nd).modifies <- this_modifies
                 ) shrink_map ;
*)
(*
    let some_bdd = PredTable.bddOne in
    let dummy_id_to_bdd = Array.create 0 some_bdd in
*)
    Message.msg_string Message.Normal "Phase 4" ;
Message.msg_string Message.Normal "Shrink elts info";
    let il_to_s il = Misc.strList (List.map string_of_int il) in
    Hashtbl.iter (fun i elts_l -> Message.msg_string Message.Normal
                    (Printf.sprintf "%d : %s" i (il_to_s elts_l))) shrink_elts_map;
 
    let size = !shrink_counter (*List.length (BiDirectionalLabeledGraph.descendants [shrink_root])*)  + (*stuck node *)1 in
    let _ = gather_modifies_info () in
      {
    EnvAutomaton.autom_root = shrink_root;
    EnvAutomaton.o_auto_root = auto_root;
    EnvAutomaton.number_of_states = size;
    EnvAutomaton.id_to_node_ht = shrink_map;
    EnvAutomaton.o_map = auto_map ;
    EnvAutomaton.cover_set = cov;
    EnvAutomaton.o_to_a_map = auto_shrink_map;
    EnvAutomaton.a_to_o_map = shrink_elts_map;
(*   
    EnvAutomaton.id_to_bdd = dummy_id_to_bdd;
    EnvAutomaton.stVarMap = [] ;
    EnvAutomaton.tsVarMap = [];
    EnvAutomaton.env_transrel = some_bdd;
*)
  }
 in
let s_auto = shrink_auto (tree_close tree_root) in
  Message.msg_string Message.Normal "Leaving mk_s_a";
  s_auto

let get_auto_size auto = 
  let nodes = BiDirectionalLabeledGraph.descendants [auto.EnvAutomaton.autom_root] in
    List.length nodes

  

exception TreeIncompleteException

(* this raises TreeIncompleteException if the check fails -- or shall we make it return a boolean ? *)
(* Q: Theorem that needs to be shown : can check that possible dotted succs of a node are contained in the 
   region of its dotted_component *)

(* requires enabled_edge_fun : onode -> (int,int) -> bool *)
exception EmptyTarPost  (* trapped inside *)

let omega_completeness_check env_auto enabled_edge_fun =
  let _ = Message.msg_string Message.Normal "Omega Completeness Check" in 
  let sn_reg_array = Array.create (env_auto.EnvAutomaton.number_of_states) Region.bot in
  let fill_array sn_id regi = 
    if (Hashtbl.mem env_auto.EnvAutomaton.id_to_node_ht sn_id
          && Hashtbl.mem env_auto.EnvAutomaton.a_to_o_map sn_id) 
      (* 0 is a case where only the former is true. INV: the latter condition implies the former *)
    then
      let sn_comp = 
        List.map (Hashtbl.find env_auto.EnvAutomaton.o_map) 
          (try
             (Hashtbl.find env_auto.EnvAutomaton.a_to_o_map sn_id) 
           with Not_found -> failwith ("Ugh: "^(string_of_int sn_id)))
           in
      let sn_cluster_reg = 
        List.fold_left
          (fun curr on' -> 
             Region.cup curr (BiDirectionalLabeledGraph.get_node_label on').EnvAutomaton.oregion)          sn_reg_array.(sn_id) sn_comp
      in
      sn_reg_array.(sn_id) <- sn_cluster_reg
    else ()
  in
  let _ = Array.iteri fill_array sn_reg_array in
    

  let edge_list = 
     let snodes = BiDirectionalLabeledGraph.descendants [env_auto.EnvAutomaton.autom_root] in
     let get_sn_edges sn = 
       let succs = 
         Misc.compact
           (List.map
              (fun e -> 
                 get_shrink_node_id
                ( BiDirectionalLabeledGraph.get_target e)) 
             (BiDirectionalLabeledGraph.get_out_edges sn))
       in 
      let  sn_id = get_shrink_node_id sn in
        List.map (fun j -> (sn_id, j)) succs
     in 
      List.flatten (List.map get_sn_edges snodes)
  in 
  let check_node on =
    if (Abstraction.is_atomic (BiDirectionalLabeledGraph.get_node_label on).EnvAutomaton.oregion)
    then true
    else
      let on_sn_id = Hashtbl.find env_auto.EnvAutomaton.o_to_a_map (get_auto_node_id on) in
      let on_comp = (* BiDirectionalLabeledGraph.collect_component dotted_neighbours [on]*) 
        try
          List.map (Hashtbl.find env_auto.EnvAutomaton.o_map) 
          (Hashtbl.find env_auto.EnvAutomaton.a_to_o_map on_sn_id) 
          (* i.e. all the fellows with the same sn_id *)
        with Not_found -> failwith "inconsistent maps!"
      in
      (*  first, get the existing i.e. "seen" states after env moves *)
        (* JHALA: VVVVIMP --- prove this formally in Coq, after you get tenure ... 
           [Theorem] 
           it suffices to show that all the possible successors of a state, wrt the env post, are in the same cluster
          as the state... i.e. all successors are covered by the cup of all the nodes in the shrink_node of o_node !
          surprised ? You better be ! *)
      let on_reg  = (BiDirectionalLabeledGraph.get_node_label on).EnvAutomaton.oregion in
        
      let on_cluster_reg = sn_reg_array.(on_sn_id) in
        (* now get the list of possible dotted succs with this auto *)
      let enabled_edges = List.filter (fun (i,j) -> enabled_edge_fun on (i,j)) edge_list in
      let mytarpost reg op auto = 
        try
          let mp = (Abstraction.tarpost reg op auto) in
            if mp = [] then raise EmptyTarPost 
            else
              fst (List.hd mp) (* this should be a list of length one ? *)
        with e -> 
          begin
            Message.msg_string Message.Error ("Tarpost Fails!"^ (Printexc.to_string e));
            raise EmptyTarPost;
          end
      in
        List.for_all 
          (fun (i,j) -> 
             let rv = 
               try (* JHALA: does this conjoin with the region of i ? *)
                 let postreg = mytarpost on_reg (Abstraction.AutomatonOperation (i,j)) env_auto in
                   Region.leq postreg on_cluster_reg 
               with EmptyTarPost -> true
             in
               if (not rv) then 
                 begin
                   Message.msg_string Message.Error "Errant node:";
                   Message.msg_string Message.Error 
                     (auto_node_to_string (BiDirectionalLabeledGraph.get_node_label on));
                   Message.msg_string Message.Error (Printf.sprintf "Errant edge: (%d,%d)" i j);
                   Message.msg_string Message.Error (Printf.sprintf "My id: %d" on_sn_id);
                   Message.msg_string Message.Error (Printf.sprintf "sn reg: %s" (Region.to_string on_cluster_reg));
                   failwith "died here!"
                 end
               else rv
           (* with _ -> 
               begin
                 Message.msg_string Message.Error "tarpost failed Errant node:";
                 Message.msg_string Message.Error 
                   (auto_node_to_string (BiDirectionalLabeledGraph.get_node_label on));
                 Message.msg_string Message.Error (Printf.sprintf "Errant edge: (%d,%d)" i j);
                 Message.msg_string Message.Error (Printf.sprintf "My id: %d" on_sn_id);
                 failwith "no no died here!"
               end *)
               
          )
          enabled_edges
        in
        let all_o_nodes = BiDirectionalLabeledGraph.descendants 
                            [env_auto.EnvAutomaton.o_auto_root] 
        in
          List.for_all check_node all_o_nodes



(* To check if a tree is complete -- will rip out a part of the above actually... *)

let tree_is_complete env_auto tree_root =
  (* we shall trawl the whole tree to see if:
     1. every node is covered by someone in the env_auto
     2. if n1 --- env move ---> n2 then cov.n1 -- env move --> cov.n2
  *)
  let _ = Message.msg_string Message.Normal "Checking if tree is complete" in
  let e_good an2 e =
    (BiDirectionalLabeledGraph.get_target e = an2 ) &&
    (Abstraction.type_of_edge (BiDirectionalLabeledGraph.get_edge_label e) = EnvAutomaton.Env) 
  in 
  let proc_tree_edge (n1,e_label,n2) = 
    let (n1_actual,n2_actual) = (get_region n1,get_region n2) in
      if (Region.is_empty n1_actual || Region.is_empty n2_actual) then ()
      else
        let n1_reg = Region.project_par_atomic n1_actual in
        let n2_reg = Region.project_par_atomic n2_actual in
        let coverfn = if (Options.getValueOfBool "ecov") then Region.findAllExactCoverer 
                          else Region.findAllCoverer 
        in
        let n1_id_list = coverfn env_auto.EnvAutomaton.cover_set n1_reg in
        let n2_id_list = coverfn env_auto.EnvAutomaton.cover_set n2_reg in
          (* either of the above could be none ...*)
          match (n1_id_list,n2_id_list) with
              ([],_) -> 
                begin
                  Message.msg_string Message.Normal "Local state not seen before (n1)!";
                  Message.msg_string Message.Normal ("tree id ="^(string_of_int (get_id n1)));
                  Message.msg_string Message.Normal (Region.to_string n1_reg);
                  Message.msg_string Message.Normal "Actual region";
                  Message.msg_string Message.Normal (Region.to_string (get_region n1));
                  raise TreeIncompleteException 
                end
            | (_,[]) -> 
                begin
                  Message.msg_string Message.Normal "Local state not seen before (n2)!";
                  Message.msg_string Message.Normal ("tree id ="^(string_of_int (get_id n2)));
                  Message.msg_string Message.Normal (Region.to_string n2_reg);
                  Message.msg_string Message.Normal "Actual region";
                  Message.msg_string Message.Normal (Region.to_string (get_region n2));
                  raise TreeIncompleteException 
                end
            | (l1, l2) -> 
                match  Abstraction.type_of_edge (e_label.op) with
                    EnvAutomaton.Thread _ -> ()
                  | _ ->
                      begin
                        let all_pairs = Misc.cross_product l1 l2 in
                        let happy_pair (i1,i2) = 
                          try 
                            let an1 = Hashtbl.find env_auto.EnvAutomaton.o_map i1 in
                            let an2 = Hashtbl.find env_auto.EnvAutomaton.o_map i2 in
                            let rv = 
                              (i1 = i2) (* RJ: is this check really sound ... hmmm. A theorem is called for.. *)
                              (* certainly seems so if one drops the middle clause ... but how can that hurt ? *)
                              ||
                              ((Hashtbl.find env_auto.EnvAutomaton.o_to_a_map i1) = 
                                 (Hashtbl.find env_auto.EnvAutomaton.o_to_a_map i2))
                              || 
                              (List.exists (e_good an2) (BiDirectionalLabeledGraph.get_out_edges an1))
                            in 
                              if rv then Message.msg_string Message.Debug 
                                (Printf.sprintf  "Witness for %d  --> %d is %d..%d" 
                                   (get_id n1) (get_id n2) i1 i2)
                              ;
                              rv
                          with Not_found -> 
                            failwith (Printf.sprintf "bogus automaton! %d %d" i1 i2)
                        in
                          if List.exists happy_pair all_pairs then () 
                          else 
                            begin
                              let spit_pair (i1,i2) = 
                                Message.msg_string Message.Normal "Lacks dotted edge from:";
                                Message.msg_printer Message.Normal Region.print (get_region n1) ;
                                Message.msg_string Message.Normal "To:";
                                Message.msg_printer Message.Normal Region.print (get_region n2) ;
                                Message.msg_string Message.Normal ("auto_nodes"^"  "^(string_of_int i1)^"  "^(string_of_int i2))
                              in
                                List.iter spit_pair all_pairs;
                                Message.msg_string Message.Normal "Tree incomplete";
                                raise TreeIncompleteException
                            end
                      end
  in
    try 
      BiDirectionalLabeledTree.iter_descendants_edges proc_tree_edge tree_root;
      Message.msg_string Message.Normal "Tree is good";
      true
    with TreeIncompleteException -> 
      Message.msg_string Message.Normal "Tree is not complete";
      false
      
(* an alternate -- hopefully more forgiving, check ... *)
let tree_is_complete2  env_auto tree_root = 
  let _ = Message.msg_string Message.Normal "Checking if tree is complete" in
  let sn_reg_array = Array.create (env_auto.EnvAutomaton.number_of_states) Region.bot in
  let fill_array sn_id regi = 
    if (Hashtbl.mem env_auto.EnvAutomaton.id_to_node_ht sn_id
          && Hashtbl.mem env_auto.EnvAutomaton.a_to_o_map sn_id) 
      (* 0 is a case where only the former is true. INV: the latter condition implies the former *)
    then
      let sn_comp = 
        List.map (Hashtbl.find env_auto.EnvAutomaton.o_map) 
          (try
             (Hashtbl.find env_auto.EnvAutomaton.a_to_o_map sn_id) 
           with Not_found -> failwith ("Ugh: "^(string_of_int sn_id)))
           in
      let sn_cluster_reg = 
        List.fold_left
          (fun curr on' -> 
             Region.cup curr (BiDirectionalLabeledGraph.get_node_label on').EnvAutomaton.oregion)          sn_reg_array.(sn_id) sn_comp
      in
      sn_reg_array.(sn_id) <- sn_cluster_reg
    else ()
  in
  let _ = Array.iteri fill_array sn_reg_array in

  let proc_tree_node n = 
    let _ = Message.msg_string Message.Normal "in ctn" in
    let n_reg = Region.project_par_atomic (get_region n) in
    let dotted_child_regions = 
      let take_child n' =
        let Some(pe) = BiDirectionalLabeledTree.get_parent_edge n' in
          (* this cant fail as n' is always a child not the root! *)
          match (BiDirectionalLabeledTree.get_edge_label pe).op with
              Abstraction.AutomatonOperation _ -> Some (Region.project_par_atomic (get_region n'))
            | _ -> None
      in 
        Misc.map_partial take_child (BiDirectionalLabeledTree.get_children n)
    in
        
  let proc_cand on_id =
    let _ = Message.msg_string Message.Normal "in proc_cand" in
    let on_sn_id = Hashtbl.find env_auto.EnvAutomaton.o_to_a_map on_id in

    let on_cluster_reg = sn_reg_array.(on_sn_id) in
    let rv =
      List.for_all (fun r -> Region.leq r on_cluster_reg) dotted_child_regions
      in
        if rv then
          Message.msg_string Message.Debug ("Tree node: "^(string_of_int (get_id n)));
          Message.msg_string Message.Debug ("Cand is"^(string_of_int on_id))
          ;
          rv
        (*let rv = Region.leq r on_cluster_reg in
          if (not rv) then 
          begin
          Message.msg_string Message.Error "ic2 Errant node:";
          Message.msg_string Message.Error 
          (auto_node_to_string (BiDirectionalLabeledGraph.get_node_label on));
          end;
          rv ) *)    
in
        if (Abstraction.is_atomic n_reg || Region.is_empty (get_region n)) then ()
        else
          let candidates = Region.findAllCoverer (* JHALA: findAllExactCoverer *)
                             env_auto.EnvAutomaton.cover_set n_reg in
          let rv = List.exists proc_cand candidates in
            if rv then ()
            else 
              begin
                Message.msg_string Message.Normal "ic2 Errant node";
                Message.msg_string Message.Normal ("tree id ="^(string_of_int (get_id n)));
                Message.msg_string Message.Normal (Region.to_string n_reg);
                Message.msg_string Message.Normal "Actual region";
                Message.msg_string Message.Normal (Region.to_string (get_region n));
                raise TreeIncompleteException 
              end
 in 
  try
    BiDirectionalLabeledTree.iter_descendants_nodes proc_tree_node tree_root;
    Message.msg_string Message.Normal "Tree IS complete";
    true
  with TreeIncompleteException -> 
    Message.msg_string Message.Normal "Tree is not complete";
      false
    


(* return the start node of the auto *)
(****************************************** AUTO finished *********************************************)

 let make_initial_auto tree_root =
  let shrink_map = Hashtbl.create 3 in
  let stuck_node = 
     let label = { EnvAutomaton.id = 0; 
                   EnvAutomaton.root = 0 (* junk *); 
                   EnvAutomaton.atomic = false ; 
                   EnvAutomaton.task = false ; 
                   EnvAutomaton.event = false ; 

                   EnvAutomaton.adr = Region.true_adr (* RJ: Region.false_adr *) ; 
                   EnvAutomaton.modifies = []} in
     let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map 0 new_sn;
     new_sn
 in
  let shrink_root = 
     let label = { EnvAutomaton.id = 1; EnvAutomaton.root = 0 (* junk *); EnvAutomaton.atomic= false ;EnvAutomaton.task= false ;EnvAutomaton.event= false ;   EnvAutomaton.adr = Region.false_adr ; EnvAutomaton.modifies = []} in
     let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map 1 new_sn;
     new_sn
   in
   let auto_root = BiDirectionalLabeledGraph.create_root { EnvAutomaton.oid =0 ; EnvAutomaton.oadr = Region.true_adr; EnvAutomaton.oregion =  (get_region tree_root) } in
   let auto_map = Hashtbl.create 2 in let auto_shrink_map = Hashtbl.create 2 in let shrink_elts_map = Hashtbl.create 2 in
  {
    EnvAutomaton.autom_root = shrink_root ;
    EnvAutomaton.o_auto_root = auto_root;
    EnvAutomaton.number_of_states = 2;
    EnvAutomaton.id_to_node_ht = shrink_map;
    EnvAutomaton.o_map = auto_map ;
    EnvAutomaton.cover_set = Region.emptyCoverers (); 
    EnvAutomaton.o_to_a_map = auto_shrink_map;
    EnvAutomaton.a_to_o_map = shrink_elts_map;
   
(*
    EnvAutomaton.id_to_bdd = Array.make 0 PredTable.bddZero;
    EnvAutomaton.stVarMap = [] ;
    EnvAutomaton.tsVarMap = [];
    EnvAutomaton.env_transrel = PredTable.bddZero;
*)
  }


(****************************************************************************)
(* GUI hooks *)
(* some functions that dumps the counterexample data structure to a file *)
(* the type of a trace is a (bool * Operation.t list * Region.t list) *)

(* returns a string,trace object pair which are used to output a trace*)

let digest_trace (b,op_l,reg_l) = 
  let creg_l = List.map Region.concretize reg_l in
  let op_string = ref "" in
  let check_o2s x = 
    let (l1,l2,rv) = Abstraction.op_to_string x in
    let s = Printf.sprintf "%d :: %s :: %d \n\n" l1 rv l2 in
      op_string := s^(!op_string);
      Printf.printf "%d :: %s :: %d \n" l1 rv l2 ; 
      (l1,l2,rv) 
 in
let output_trace_triple = 
  (b,
   List.map check_o2s op_l,
   List.map Abstraction.reg_to_strings creg_l)
in

let str_of_reg r =
  match Abstraction.reg_to_strings r with
    | (ps :: _, loc) ->
	Printf.sprintf "      %s\n" ps
in
let str_of_op o =
  let (l1,l2,rv) = Abstraction.op_to_string o in
    Printf.sprintf "%d :: %s :: %d \n" l1 rv l2
in
let comb_lst = List.combine op_l (List.tl reg_l) in

let trace_string =
  (List.fold_left
     (fun acc (o, r) ->
	let os = str_of_op o in
	let rs = str_of_reg r in
	  acc^os^rs)
     ("Start trace \n "^(str_of_reg (List.hd reg_l))) comb_lst)
  ^"End trace\n\n"
in
  (trace_string , output_trace_triple)


let ac_dumptrace tracefile textfile (trace_string,output_trace_triple) =
  let trace_chan = open_out_bin tracefile in
      Marshal.to_channel trace_chan output_trace_triple [];
      close_out trace_chan;
      Misc.append_to_file textfile trace_string

 
(* trace_triple = (b,op_l,reg_l) *)
let dumptrace trace_triple = 
  ac_dumptrace 
    ((Options.getValueOfString "mainsourcename")^".btr")
    ((Options.getValueOfString "mainsourcename")^".traces")
    (digest_trace trace_triple)


let dumppath node region_at_node =
  let _ = Message.msg_string Message.Error "in dumppath" in
  let p_to_root = Tree.path_to_root node in
  let list_of_regs = (List.map (get_region)  
                        (Misc.chop_last p_to_root))@[region_at_node] in
  let list_of_ops = List.map (get_op) ( List.tl p_to_root) in
    dumptrace (false, list_of_ops, list_of_regs)
      

let dump_trace_to_node n n_region genuine =
   let n_data   = (Tree.get_node_label n) in
   let path_to_root = Tree.path_to_root n in
   let list_of_regs = (List.map (get_region)  
                         (Misc.chop_last path_to_root))@[n_region] in
   let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
     dumptrace (genuine, list_of_ops,list_of_regs)


let print_whole_tree root = 
  Message.msg_string Message.Debug "Printing Reachability Tree";
  let _ = print_tree_dot root in
  let f n = Message.msg_printer Message.Debug print_tree_node n
  in
  let rec _ac_print n = 
    f n; 
    List.iter _ac_print (Tree.get_children n)
  in
    _ac_print root
      

let demo_process n n_region genuine =

 match (Options.getValueOfBool "demo" (*|| genuine *)) with
      true ->
        begin
          let s = if genuine then "Genuine" else "Spurious" in
          let outs = Printf.sprintf "*** %s Counterexample Found!***" s in
            Message.msg_string Message.Normal outs;
            dump_trace_to_node n n_region genuine;
        Message.msg_string Message.Error "waiting to hear from gui";
        let _ = input_line stdin in
          Message.msg_string Message.Error "heard from gui!";
          ()
 end 
| false -> ()


(****************************************************************************)
(* TAR algorithm *)
(****************************************************************************)
        
  let model_check_tar init o_err = 
    Message.msg_string Message.Debug "Entering model_check" ;
    Message.msg_string Message.Debug ("   init region: " ^ (Region.to_string init)) ;
    Message.msg_string Message.Debug 
      (match o_err with None -> "No error region" | Some e -> "   error region: " ^ (Region.to_string e)) ;
    try   
     let flag = ref true 
     in
     Ast.set_time_out_signal () ;
     while (!flag) do
      begin
        Message.msg_string Message.Debug "\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n" ;
        Message.msg_string Message.Debug "Next iteration of model-check's outer while-loop\n\n\n" ;
        Message.msg_string Message.Debug "Printing Assumptions\n" ;
        (* debug *)
        print_phis true;
        
        stats_nb_iterations_of_outer_loop := !stats_nb_iterations_of_outer_loop + 1;
        flag := false;
        
        (* Check if time out has occurred *)
        Ast.check_time_out (); 
        
        (* Routines to check for concurrent access and to check for valid errors / do refinement *)
        let do_focus_and_refine err_anc anc path =
          Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path 1" ;
          let anc_region = (get_region anc) in
          let _ = Message.msg_string Message.Debug (* Normal*) "About to call focus" in
          try
            let focused_anc_region = 
              Stats.time "focus" (Abstraction.focus anc_region) err_anc
            in
            Message.msg_string Message.Debug "Done calling Focus";
            Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
            stats_nb_refinment_processes := !stats_nb_refinment_processes + 1
          with Abstraction.NoNewPredicatesException ->
            Message.msg_string Message.Debug (* Normal *)"do_focus_and_refine: This is a false error" ;
            Message.msg_string Message.Debug "The check_error function ended up with" ;
            Message.msg_string Message.Debug ("     an empty error: " ^ (Region.to_string err_anc)) ;
            Message.msg_string Message.Debug "     at ancestor: " ;
            Message.msg_printer Message.Debug print_tree_node anc ;
            Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
            Message.msg_printer Message.Debug  print_tree_node anc ;
(*          let path_to_root = Tree.path_to_root n in 
            let list_of_regs = (List.map (get_region)  (Misc.chop_last path))@[n_region] in
*)
            let list_of_regs = (List.map (get_region)  path) in
            let list_of_ops = List.map (get_op) ( List.tl path) in
	    print_tree_dot (List.hd path);
            dumptrace (false, list_of_ops, list_of_regs) ;
            failwith "No new predicates" 
(*
            List.iter (function x -> Message.msg_printer Message.Normal print_tree_node x) path
*)
        in
        let focus_and_refine args =
          let n_refinements = !stats_nb_refinment_processes in
          List.iter (function (err_anc, anc, path) ->
            do_focus_and_refine err_anc anc path)
            args;
          if n_refinements = !stats_nb_refinment_processes
          then failwith "No new predicates found by focus!"
        in
        (* raise exception if there is a possible race
           return true if the command on edge accesses global_var and false otherwise *)
        (* 
           rkind -> kind of race to check for. rkind is Read, Write, Both, or NoDistinction
           operation -> the edge label of the cfa that needs to be checked for races
           node -> the current node in the reachability tree
           region_at_node -> R /\ wp (op, true) , where R is the region at the node computed in the reachability tree
           global_var -> the variable being checked
           phi_list -> the current assumptions about the global variable
           *)

        let check_concurrent_access rkind operation node region_at_node global_var gv_alias phi_list  =
          (* the phi_list given above is gv_alias's phi_list *)
          (* this function should only be called with global_var/gv_alias such that the pair of lvals is
             such that they might be aliased ... if it is known that they are NOT ever aliased then they should not 
             be sent to this function ... 
          *)
          Message.msg_string Message.Debug "Checking concurrent access." ;
          Message.msg_string Message.Debug ("Region at node: "^(Region.to_string region_at_node)) ;
          Message.msg_string Message.Debug ("Main lval = "^(Expression.lvalToString global_var)) ;
          Message.msg_string Message.Debug ("Alias lval = "^(Expression.lvalToString gv_alias));
          if (Abstraction.is_lock global_var || Abstraction.is_lock gv_alias) then
            begin
              (* JHALA: TO BE DONE TBD *)
              (* we have to insert the checking for locks -- i.e. that region is contained inside the predicate
                 business 
              *)
              (* 1. is global_var \in the global_lock_asm_table -- if so then check: leq region_at_node spec
                 if not -- then do counterexample guided refinement !! see if present path is feasible if so is sp contained inside the spec -- if so learn new preds and restart -- if not -- ERROR !
                 else gv_alias is in the damn table --> do the check with (cap region_at_node eq_addr_const) spec
              *)
              Message.msg_string Message.Debug "This is a lock variable. Need not check this." ;
              ()
            end
          else
            (* check for race condition *)
          begin
            let (* the trim_region_to_globals simply projects away all control info -- ie pc, stack *)
                r = Abstraction.trim_region_to_globals region_at_node 
            in
              begin
                let check_for_race (othernode, otherregion) =
                  (* check for race does the sat. check for (node, region_at_node) (othernode, otherregion) *)
                  (* raise an exception if a possible error is found *)
                  (* construct the formula and check satisfiability *)
                  (* this function returns the following: 
                     either it raises no exceptions -- 
                     meaning all the intersections in the present phi_tables are empty ...
                     or it raises  No_error -- which means that there WAS some non-empty intersection but it was
                     bogus because either a. at least one of the 2 offending paths is infeasible or b. the conjunction
                     of the 2 sp is infeasible ... in either case No_error is caught later and the model checker 
                     "restarted" appropriately 
                     Question: what if the address constraint predicate is returned by focus ? 
                     ... where will we add it ? will we need to add it at all ? 
                  *)
                  begin
                    match 
                      Abstraction.multiply_instantiable 
                        r (Abstraction.trim_region_to_globals otherregion) global_var gv_alias
                    with
                      None -> (* Good, not satisfiable, do nothing, 
                                 means the present abs is enough to say that the regions dont intersect *) ()
                    | Some reg -> (* the region is satisfiable: potential race! *)
                        begin
                          Message.msg_string Message.Normal "Found a possible race! Checking feasibility." ;
                          Message.msg_string Message.Error "Found a possible race! Checking feasibility." ;
                          Message.msg_printer Message.Debug print_tree_node node ;
                          Message.msg_string Message.Debug ("This region is "^(Region.to_string region_at_node)) ;
                          Message.msg_string Message.Debug "\n\nOther node :" ;
                          Message.msg_printer Message.Debug print_tree_node othernode ;
                          Message.msg_string Message.Debug ("Other region is "^(Region.to_string otherregion)) ;
                                            (* satisfiable, check feasibility of each error *)
                                 (* DEBUG *)
                          
                          Message.msg_string Message.Normal "\n\nTrace 1:" ;
                          dumppath node region_at_node ;
                          
                          Message.msg_string Message.Normal "\n\nTrace 2:" ;
                          dumppath othernode otherregion ;


                          (* first I need to put back the control info (pc, stack) in otherregion *)
                          (* RUPAK: this does not work now, I simply replace otherregion with the region in othernode to test
                           *)
                          begin
                            match (check_error_fwd node region_at_node, check_error_fwd othernode otherregion) with
                              (NonEmptyErrorAtRoot (err_root1, root1, path1), NonEmptyErrorAtRoot (err_root2, root2, path2)) ->
                                begin
                                Message.msg_string Message.Normal "\nBoth paths are feasible! But is their intersection feasible?" ;
                                Message.msg_string Message.Error "\nBoth paths are feasible! But is their intersection feasible?" ;
                                let intersection =
                                  Abstraction.make_multiply_instantiable_region
                                    (Abstraction.trim_region_to_globals err_root1)
                                    (Abstraction.trim_region_to_globals err_root2)
                                    global_var 
                                    gv_alias
                                in
                                  match Region.is_empty intersection with
                                      true ->
                                        begin
                                          (* the paths are not feasible together *)
                                          (* in this case we take the empty intersection and focus an arbitrary
                                             node with respect to this empty region.
                                             We ignore the result returned by focus, because we are only interested
                                             in the side effect: some new predicates get added.
                                             Not functional. But functional. (Bad pun here).
                                             
                                          *)
                                          Message.msg_string Message.Normal "The intersection is empty. Refining." ;
                                          try
                                            ignore (Stats.time "focus" 
                                                      (Abstraction.focus 
                                                       (Abstraction.create_abstract_true_region operation) )
                                                      intersection)
                                          with Abstraction.NoNewPredicatesException ->
                                            begin
                                              Message.msg_string Message.Normal "Intersection: This is a false error" ;
                                              Message.msg_string Message.Normal "The check_error function ended up with" ;
                                              Message.msg_string Message.Normal ("     an empty intersection: " ^ 
                                                                                 (Region.to_string
                                                                                    intersection)) ;
                                              print_phis true ;
                                              Message.msg_string Message.Normal "\n\nTrace 1:" ;
                                              dumppath node region_at_node ;
                                              Message.msg_string Message.Normal "\n\nTrace 2:" ;
                                              dumppath othernode otherregion ;

                                              
                                              failwith "No new predicates."
                                            end
                                        end
                                    | false ->
                                        begin
                                          Message.msg_string Message.Debug "The intersection is feasible. Real race." ;
                                          raise (Exit_from_check_concurrent_access 
                                                   (Race_condition ((err_root1,
                                                                     List.map get_op path1,
                                                                     region_at_node), 
                                                                    (err_root2,
                                                                     List.map get_op path2,
                                                                     otherregion),global_var , gv_alias ) ))
                                        end
                                end
                            | (NonEmptyErrorAtRoot (_, _, _),
                               EmptyErrorAtNode (err_anc2, anc2, path2)) ->
                              (* find more predicates and restart *)
                                Message.msg_string Message.Normal "\nSecond path is infeasible! " ;
                                Message.msg_string Message.Error "\nSecond path is infeasible! " ;

                                 focus_and_refine [(err_anc2, anc2, path2)]
                            | (EmptyErrorAtNode (err_anc1, anc1, path1),
                               NonEmptyErrorAtRoot (_, _, _)) ->
                                Message.msg_string Message.Normal "\n1st path is infeasible! " ;
                                Message.msg_string Message.Error "\n1st path is infeasible! " ;
                             (* find more predicates and restart *)
                                 focus_and_refine [(err_anc1, anc1, path1)]
                            | (EmptyErrorAtNode (err_anc1, anc1, path1), EmptyErrorAtNode (err_anc2, anc2, path2))
                              ->
                                Message.msg_string Message.Normal "\nBoth paths is infeasible! " ;
                                Message.msg_string Message.Error "\nBoth paths is infeasible! " ;
                                focus_and_refine [(err_anc1, anc1, path1);
                                                    (err_anc2, anc2, path2)]
                          end;
                          raise (Exit_from_check_concurrent_access No_error)            
                        end;
                
                  end
                in
                match rkind with
                  NoDistinction ->
                    (* first check if node_region is included in phi_list -- this is the easy case *)
                    if (Region.leq region_at_node (phi_list.write_whole)) 
                    then ()
                    else
                      begin
                        List.iter check_for_race ((node, region_at_node) :: phi_list.write_accesses) ; 
                        (* if I am here, then there was no error *)
                        ()
                      end
                | Read -> 
                    (* first check if node_region is included in phi_list -- this is the easy case *)
                    if (Region.leq region_at_node (phi_list.read_whole)) 
                    then ()
                    else
                      begin
                       (* check for conflicts with write accesses. Since this node was a read, we
                          need not check for errors against this node. *)
                        List.iter check_for_race (phi_list.write_accesses) ;
                        (* if I am here, then there was no error *)
                        ()
                      end
                | Write ->
                                    (* first check if node_region is included in phi_list -- this is the easy case *)
                    if (Region.leq region_at_node (phi_list.write_whole)) 
                    then ()
                    else
                      begin
                       (* check for conflicts with write accesses. *)
                        List.iter check_for_race ((node, region_at_node) :: phi_list.write_accesses) ;
                       (* check for conflicts with read accesses. *)
                        List.iter check_for_race (phi_list.read_accesses) ;
                        (* if I am here, then there was no error *)
                        ()
                      end
                | Both ->
                                    (* first check if node_region is included in the write phi_list  *)
                    if (Region.leq region_at_node (phi_list.write_whole) ) 
                    then (* no new bugs can occur due to this write, so check for read bugs *)
                      begin
                        (* this could be done by a recursive call to check_concurrent_access Read .. *)
                        if (Region.leq region_at_node (phi_list.read_whole)) 
                        then ()
                        else
                          begin
                            (* check for conflicts with write accesses. Since the write region at this node
                               was included in write_whole, we
                               need not check for errors against this node. *)
                            List.iter check_for_race (phi_list.write_accesses) ;
                            (* if I am here, then there was no error *)
                            ()
                          end
                      end
                    else
                      begin
                       (* check for conflicts with write accesses. *)
                        (* step 1*) List.iter check_for_race ((node, region_at_node) :: phi_list.write_accesses) ;
                       (* check for conflicts with read accesses. *)
                        (* step 2*) List.iter check_for_race ((node, region_at_node) :: phi_list.read_accesses) ;
                        (* if I am here, then there was no write error *)
                        (* also, the possible read errors due to this region have already been
                           checked in step 1. So I need not check for read errors again.
                           RUPAK: check this!
                           *)
                        ()
                      end
              end
          end
        (* end check_concurrent_access *) 
        in
        (* initialize current approximations to environment assumptions to empty list *)
        (* the current approximations are a map Global Variables -> List of tree nodes *)
        (* the environment assumption for x is \/ { n_reg | n\in accNodes(x) } *)
        let accNodes = Hashtbl.create 31 
        in
        (* initialize the hash table to [] for each global *)
        Abstraction.iter_global_variables 
          (fun name -> Hashtbl.add accNodes name { read_whole = Region.bot  ; read_accesses = [] ;
                                                   write_whole = Region.bot ; write_accesses = []
                                                 } 
              ) ;

        (* set up the reachability search *)
        (* the reached region is initially empty *)
        let reached_region = ref Region.bot
            
        (* helper object to create nodes *)
        and tnc = new tree_node_creator
            
            
        (* the set of pending nodes to be processed *)
        and unprocessed_nodes = 
          if (Options.getValueOfBool "bfs") 
          then new Search.bfsiterator
          else new Search.dfsiterator

        (* the global time used for time stamps *)
        and time = ref 0 in
        
        (* The function for constructing children .. and adding them to the tree *)
        let construct_children node accu op =
          (tnc#create_child Unprocessed op node)::accu
                                                     
        in                               
        
        (* the root of the reachability tree *)
        let tree_root =
          tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                                region = init ; })
        in
        let do_subtree_cut_refinement anc new_anc_region =
          let _ = 
            stats_nb_deleted_nodes := !stats_nb_deleted_nodes + 
            (Tree.count_nodes_descendants anc) 
          in
          let anc_data = Tree.get_node_label anc in
          let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
          Message.msg_string Message.Debug "Updating the ancestor's region..." ;
          anc_marking_data.region <- new_anc_region ;
          
          Message.msg_string Message.Debug "This ancestor now looks like:" ;
          Message.msg_printer Message.Debug print_tree_node anc ;
        (* Note that here there is no path to update as the entire subtree is going to be chopped off... *)
          Message.msg_string Message.Debug "Now deleting subtree:" ;
          tnc#delete_children anc;
          unprocessed_nodes#empty_out ();
      (* Jhala: I'm assuming that its ok to just empty this out and fill it back up when we are traversing the tree later ... this whole traversal business is not good by the way, there should be a much more incremental way to do it *)
          Message.msg_string Message.Debug "Now re-adding children:" ;
          let children = List.fold_left (construct_children anc) [] 
              (Abstraction.enabled_ops new_anc_region) in
          Message.msg_string Message.Debug "The children are:" ;
          List.iter (function child -> Message.msg_printer Message.Debug print_tree_node child) children ;
          
       (* add the children to the set of pending nodes *)
      (* Jhala: This should all be done by the the subtree cut 
         Message.msg_string Message.Debug "Adding the children to the set of pending nodes" ;
         unprocessed_nodes#add_list children ;
         *)
      (* mark n as processed and uncovered *)
          Message.msg_string Message.Debug "Updating the node's marking" ;
          
          anc_data.mark <- Processed_Uncovered anc_marking_data ;
          Message.msg_string Message.Debug "This node now looks like:" ;
          Message.msg_printer Message.Debug print_tree_node anc;
      (* add n's region to the reached region *)
          Message.msg_string Message.Debug "Updating the currently reached region 1" ;
          reached_region := 
             Stats.time "update_tree_after_refinement[CUT]" 
               (update_tree_after_refinment tree_root unprocessed_nodes) anc_marking_data.time_stamp
        in
        
        let no_error n n_data n_marking_data =
          (* no error found at this node *)
          Message.msg_string Message.Debug "No error found at this node" ;
          
          (* let's test whether this node is covered *)
          Message.msg_string Message.Debug "Let's test whether this node is covered" ;
          let coverable = 
            if (Options.getValueOfBool "cov") then Abstraction.coverable n_marking_data.region else true
          in
          let covered = 
          if coverable then 
            (Stats.time "Covered check:"
               (Abstraction.covered n_marking_data.region) !reached_region)
          else false in
          if covered
          then
            begin
              (* this node is covered *)
              Message.msg_string Message.Normal "This node is covered" ;
              
              (* mark n as processed and covered *)
              n_data.mark <- Processed_Covered n_marking_data
            end
          else
            begin
              (* this node is not covered *)
              Message.msg_string Message.Normal "This node is not covered" ;
              Message.msg_string Message.Debug "Constructing its successor children..." ;
              
              (* we construct its successor children *)
              
              let children = List.fold_left (construct_children n) []
                  (Abstraction.enabled_ops n_marking_data.region) in
              
              Message.msg_string Message.Debug "The children are:" ;
              List.iter (function child -> Message.msg_printer Message.Debug print_tree_node child) children ;
              
              (* add the children to the set of pending nodes *)
              Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
              unprocessed_nodes#add_list children ;
              
              (* mark n as processed and uncovered *)
              Message.msg_string Message.Debug "Updating the node's marking" ;
              n_data.mark <- Processed_Uncovered n_marking_data ;
              
              Message.msg_string Message.Debug "This node now looks like:" ;
              Message.msg_printer Message.Debug print_tree_node n ;
              
              (* add n's region to the reached region *)
              Message.msg_string Message.Debug "Updating the currently reached region 2" ;
              reached_region := Region.cup !reached_region n_marking_data.region
            end
        in            

        (* This function checks if an error region reached in the thread is a real
           error trace (given the current assumption about the other thread).
           *)
        let error_reached n err_n n_marking_data =
          begin
            let n_data = (Tree.get_node_label n) in
            let n_region = n_marking_data.region in
                    (* this node will have to be reprocessed after refinment *)
                    (* JHALA: why will this node have to be reprocessed after refinement ? *) 
            Message.msg_string Message.Debug "Re-adding this node to the set of pending unprocessed nodes NO CF" ;
            n_data.mark <- Processed_Was_Covered_To_Reprocess n_marking_data ;
            Message.msg_string Message.Debug "Adding to unprocessed#nodes";
            Message.msg_printer Message.Debug print_tree_node n ;
            unprocessed_nodes#add_element n ;
            
                (* let's now test whether this error is a real one *)
            Message.msg_string Message.Normal "Testing whether this (single threaded) error is a real one" ; 
            Message.msg_string Message.Debug "Testing whether this (single threaded) error is a real one" ; 
            let check_refinement = 
(*            if (Options.getValueOfString "refine" = "fwd") then
                check_error_fwd 
              else *) check_error
            in
            match (Stats.time "check_error" (check_refinement n) err_n) with
              EmptyErrorAtNode(err_anc, anc, path) ->
                                (* this is a false error *)
                begin
                  Message.msg_string Message.Normal "This is a false (single threaded) error" ;
                  Message.msg_string Message.Debug "The check_error function ended up with" ;
                  Message.msg_string Message.Debug "     at ancestor: " ;
                  Message.msg_printer Message.Debug print_tree_node anc ;
                  Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
                  Message.msg_printer Message.Debug print_tree_node anc ;
                  List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
                  demo_process n n_region false;
                  
                  if (Options.getValueOfBool "stop") then
                    begin
                      dumppath n n_region;
                      failwith ("FALSE ERROR CHOKE");
                    end
                                (* let's refine *)
                
                  Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path 2" ;
                  let anc_region = (get_region anc) in
                  let focused_anc_region = 
                    try
                      let rv = Stats.time "focus" (Abstraction.focus anc_region) err_anc in
                      rv
                    with Abstraction.NoNewPredicatesException ->
                      begin
                        Message.msg_string Message.Normal "No new predicates raised for single-threaded error." ;
                        let path_to_root = Tree.path_to_root n in
                        let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
                        let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
			print_tree_dot (List.hd path_to_root);
                        dumptrace (false, list_of_ops, list_of_regs);
                        Message.msg_string Message.Normal "No new predicates found!";
                        failwith ("No new preds found !-- and not running find_all_preds ...")
                      end
                  in (* for the focused_anc_region *)
                  Message.msg_string Message.Debug "Done calling Focus";
                  Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
                  
                  stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;
                  Stats.time "subtree_cut_refinement" (do_subtree_cut_refinement anc) focused_anc_region;
                  end
            | NonEmptyErrorAtRoot(err_root, root, path) ->
                            (* --- for debugging --- *)
                begin
                  assert (root == tree_root) ;
                            (* this is a real error *)
                            (* we put the error path in the appropriate variable *)

                              (* print the unknown functions on the path *)
                  Abstraction.print_debug_info (List.map get_op path);
                  
                  raise (RaceConditionException (Error_reached 
                                                  (err_root,
                                                   List.map get_op path,
                                                   err_n) ) )

                end
          end
        in
        (* continue_reachability says if there was an error *)
        let continue_reachability = ref true in
        stats_nb_iterations := 0; 
        (* add the root to the pending unprocessed nodes *)
        unprocessed_nodes#add_element (tree_root) ;
        let itl = Options.getValueOfInt "iter-bound" in
        while (!continue_reachability && unprocessed_nodes#has_next ()) do
          stats_nb_iterations := !stats_nb_iterations + 1;
          (if itl > 0 then
             if !stats_nb_iterations > itl then raise Ast.TimeOutException);
          Message.msg_string Message.Debug "\n\n************************************************************************\n" ;
          Message.msg_string Message.Debug "Next iteration of model-check's inner reachability loop" ;
          Message.msg_printer Message.Debug Format.pp_print_int (!stats_nb_iterations);
          Message.msg_string Message.Debug "Currently reached region:" ;
          (* Message.msg_printer Message.Debug Region.print !reached_region ; *)
          
        (* let's process the next pending node *)
          let n        = unprocessed_nodes#next () in
          Message.msg_string Message.Debug (*Normal*)  "Now processing tree node:" ;
          Message.msg_printer Message.Debug (*Normal*) print_tree_node n ;
          
          let n_data   = (Tree.get_node_label n) in
          
          let n_marking_data = 
            match n_data.mark with
              Unprocessed ->
                let op = (get_op n) in
                { time_stamp = (time := !time + 1 ; !time) ;
                  region = Abstraction.post (get_region (get_parent n)) op global_variable_table; }
            | Processed_Was_Covered_To_Reprocess md ->
                md
            | _ -> failwith "LazyModelChecker.model_check: unexpected marking data"
                  
          in
            (* --- for debugging --- *)
            (* assert (n_data.mark == Unprocessed) ; *)
          
          let n_region = n_marking_data.region in
          (** look for bugs **)
          (* continue is set to false if there was a bug **)
          begin
            Message.msg_string Message.Debug "Checking for race conditions." ;
            (** Helper functions *)

            (* construct_enabled_region returns region /\ wp(action on edge, true) *)
            let construct_enabled_region n_region edge =
              if (Abstraction.isPredicate edge) then
                begin
                  let true_reg = Abstraction.create_concrete_true_region edge in (* region true *)
                  let wp = Abstraction.spre true_reg edge global_variable_table in
                  Message.msg_string Message.Debug ("wp created, it is "^(Region.to_string wp)) ;
                  Abstraction.intersect_with_abstraction (n_region) wp
                end
              else
                n_region
            in
            try 
            (* for each enabled successor check if there is a potential race condition *)
              (* RUPAK: this is not the most efficient. need to optimize this. e.g., only
                 a few variables are touched by the edge, so need not iterate over all global vars *)
              
              (* JHALA: add other parameters to check_concurrent_access lval_now aliased_lval *)

              (* JHALA: for each edge: 
                 1. find the lvals written to/ read from for that edge and update
                 their assumption tables appropriately... or do we do the updating AFTER the check ?
                 2. then do the checking i.e. for each lval that may be aliased
                 to something in the lvals_written \cup lvals_read *)
              
              let update_read_assumptions trimmed_reg lval = 
                Message.msg_string Message.Debug ("Updating the current read assumption on "^(Expression.lvalToString lval));
                try
                  let phi_list = Hashtbl.find accNodes lval in
                  Hashtbl.replace 
                    accNodes 
                    lval 
                    { read_whole = Region.cup phi_list.read_whole trimmed_reg ; 
                      read_accesses = (n, trimmed_reg) :: phi_list.read_accesses ;
                      write_whole = phi_list.write_whole ; 
                      write_accesses = phi_list.write_accesses 
                    }
                with Not_found -> (* if accNodes does not find lval, this means that there is no
                                     write in the program through this lvalue. 
                                     *)
                  begin
                    Message.msg_string Message.Debug ("This lvalue was not found in accNodes. Not updating read assumption.");
                    ()
                  end
              in
              let update_write_assumptions trimmed_reg lval =
                Message.msg_string Message.Debug ("Updating the current write assumption on "^(Expression.lvalToString lval));
                try
                  let phi_list = Hashtbl.find accNodes lval in
                  Hashtbl.replace 
                    accNodes 
                    lval
                    { read_whole = phi_list.read_whole ; 
                      read_accesses = phi_list.read_accesses ;
                      write_whole =  Region.cup phi_list.write_whole trimmed_reg; 
                      write_accesses = (n, trimmed_reg) ::phi_list.write_accesses 
                    }
                with Not_found ->
                  (* Something wrong! *)
                  failwith ("Lvalue being written to not found in accNodes! "^(Expression.lvalToString lval))
              in
              let strong_src_reg = n_region (* construct_enabled_region n_region edge *) in
              let trimmed_reg = Abstraction.trim_region_to_globals strong_src_reg in
              let checking_access_and_updating edge race_kind lval =
                (* first find the list of all aliases of this lval *)
                let list_of_aliases = Abstraction.aliasesOf lval in
                let race_string = match race_kind with Read -> "read" | Write -> "write" in
                  Message.msg_string Message.Normal 
                    ("Checking for races on a " ^ race_string ^" to variable "^(Expression.lvalToString lval)) ;
                  Message.msg_string Message.Normal ("for the operation "^(Operation.to_string edge)) ;
                let checkfn l = 
                  let l_phi_list = 
                    try Hashtbl.find accNodes l 
                    with Not_found ->
                      (* RUPAK: check that this is correct.
                         If phi is not found for an lvalue l in the accNodes table, this
                         means that the lvalue l never occurs syntactically in the program.
                         Hence, this means that no address is ever updated in the program
                         through the lvalue l, nor is the lvalue l ever read.
                         So we can return Region.Bot for any such l
                         *)
                      begin
                        Message.msg_string Message.Debug ("phi not found for "^(Expression.lvalToString l));
                        {
                        read_whole = Region.bot ; read_accesses = [] ;
                        write_whole = Region.bot ; write_accesses = [] ;
                        }       
                      end
                  in 
                  Message.msg_string Message.Debug ("\n\nwith the possibly aliased "^(Expression.lvalToString l));
                  Message.msg_string Message.Debug "Now calling check_concurrent_access" ;
                  Message.msg_string Message.Debug "Phi list is : " ;
                  Message.msg_string Message.Debug ("Read = "^(Region.to_string l_phi_list.read_whole)) ;
                  Message.msg_string Message.Debug ("Write = "^(Region.to_string l_phi_list.write_whole)) ;
                  
                  check_concurrent_access race_kind edge n strong_src_reg lval l l_phi_list; 
                  Message.msg_string Message.Debug "Check concurrent access returned without error, start updating phi" 
                in
                let _  = List.iter checkfn list_of_aliases in
                  (* if we get this far then there are no races between lval and all his list_of_aliases *)
                  match race_kind with
                      Read -> update_read_assumptions trimmed_reg lval
                    | Write -> update_write_assumptions trimmed_reg lval
              in
                List.iter 
                  (fun edge -> 
                     let (lvals_read, lvals_written) = Abstraction.reads_and_writes edge in
                       List.iter (checking_access_and_updating edge Read) lvals_read;
                       List.iter (checking_access_and_updating edge Write) lvals_written)
                  (Abstraction.enabled_ops n_region);

            with Exit_from_check_concurrent_access a ->
              begin
                match a with
                  No_error ->
                   (* found an infeasible race *)
                    Message.msg_string Message.Debug "Found an infeasible race. Setting continue to false." ;
                    continue_reachability := false 
                  | Race_condition _ -> raise (RaceConditionException a) 
                      (* send back error condition to caller *)
                  | _ -> failwith "model_check: Strange return value!"
              end;
          end;
                    (** finished looking for bugs **)
          (** do the following only if continue remains true **)
          if (!continue_reachability = true) then
            (* continue the reachability *)
            begin
              (* check for location errors *)
              match o_err with
                None ->  no_error n n_data n_marking_data (* no error to check: pure race detection *)
              | Some err ->
                  (* check for error location reachability *)
                  begin
                    let err_n = Stats.time "cap" (Region.cap n_region) err in
                    if (Region.is_empty err_n) then
                      no_error n n_data n_marking_data
                    else
                      begin
                        (* error found. Check if real error *)
                        Message.msg_string Message.Debug "Error found : checking validity." ;
                        error_reached n err_n n_marking_data
                      end   
                  end
                    
            end ;
        done; (* inner reachability loop *)

      (* outer loop: if continue is false, then there was an error, so reset environment assumptions loop back *)
      if (!continue_reachability = false) then
        begin
          (* reset all environment assumptions to false *)
          let _ = Hashtbl.iter (fun a b -> Hashtbl.replace global_variable_table a (ref Region.bot)) 
                               global_variable_table in
          Message.msg_string Message.Debug "Resetting all assumptions in the global_variable_table and continuing reachability" ;
          (* note that refine already adds predicates *)
          flag := true (*;
          Abstraction.reset ()  do we need this? *)
        end
      else
        begin
          let _checkEntailmentAndUpdate name currAppx =
            let ax = !(get_global_var_phi name) in
            let curr = (* current approximation to the environment assumption *)
              currAppx.write_whole
            in
            if (not (Region.leq curr ax)) then
              begin
                (* update global environment assumptions *)
                let newreg = (Region.cup ax curr) in
                Message.msg_string Message.Debug ("Updating global invariant about "^(Expression.lvalToString name)) ;
                Message.msg_string Message.Debug ("The new environment assumption is "^(Region.to_string newreg)) ;
                Hashtbl.replace global_variable_table name (ref newreg) ;
                flag := true
              end
          in
          (* check for each global if the current environment assumptions are subsumed *)
          Hashtbl.iter (_checkEntailmentAndUpdate) accNodes
        end
      end
    done;
    (* send back "correct program" to caller *)
    No_error
    with RaceConditionException a -> 
      begin
(*
        match a with
          Race_condition (p1, p2, _, _) ->
            dumptrace p1 ;
            dumptrace p2
        | Error_reached p1 -> dumptrace p1
        | _ -> failwith "Race condition exception raised with strange model check outcome" ;
*)
        a
      end
        
(*******************************************************************************)
(*******************************************************************************)
(*******************************************************************************)
(* vanilla *)


(***************************
  Alias analysis.  Deferred to the first error region *)

let constructInitialPredicates predFile =
  let readAndAddPredicates filename =
    let doParse inchan =
      (*let _ = VampyreErrormsg.startFile filename in*)
      let lexbuf = Lexing.from_channel inchan in
      (*let _ = VampyreErrormsg.theLexbuf := lexbuf in*)
      Inputparse.main Inputflex.token lexbuf
    in
    ignore (Message.msg_string Message.Normal "Reading in seed predicates...");
    try
      let inchan = open_in filename in
      let a = doParse inchan in
        ignore (Message.msg_string Message.Normal "Seed predicates read.\n") ;  
        List.iter (fun a -> Message.msg_string Message.Normal (Predicate.toString a)) a ;
        Message.msg_string Message.Normal ("Read "^(string_of_int (List.length a))^" predicates");
        a
    with Sys_error _ -> (Message.msg_string Message.Error ("Cannot find predicate information in "^filename^".\n"); [])
       | _ -> (Message.msg_string Message.Error ("Error raised in reading seed predicates.\n"); [])
  in
  if (predFile = "") then [] else readAndAddPredicates predFile


let alias_not_done_flag = ref true

let alias_not_done () =
  let oldv = !alias_not_done_flag in
  let _ = alias_not_done_flag := false in
  oldv
 

let perform_alias_analysis () =
   let aliasPreds = 
     let aliasPredsFile = Options.getValueOfString "aliasfile" in
     Predicate.conjoinL (Stats.time "read alias pairs" constructInitialPredicates aliasPredsFile)
   in
    (* Do program analysis optimizations at the CFA level. *)
    Stats.time "Alias analysis" AliasAnalyzer.constructAliasDatabase () ; 
       Message.msg_string Message.Normal ("Finished Building Aliases");
    if (Options.getValueOfBool "aliasclos") then begin
      Stats.time "Alias closure" AliasAnalyzer.refineAliasDatabase () ; 
      Message.msg_string Message.Normal ("Finished Refinig Aliases");
    end;
    AliasAnalyzer.constructMustAliasDatabase () ; 
       Message.msg_string Message.Normal ("Finished Building Must Aliases");
    Stats.time "Modifies Database"
      (C_System_Descr.post_alias_analysis AliasAnalyzer.get_lval_aliases_scope) ();
    Stats.time "Add alias override"  (AliasAnalyzer.add_alias_override) aliasPreds;
    if (Options.getValueOfBool "nomusts") then begin
      Stats.time "Remove must aliases"  AliasAnalyzer.withdraw_musts ();
    end;
  Message.msg_string Message.Normal "back from post_alias_analysis"


(*******************************************************************************)
(*******************************************************************************)
  (* Parallel model checker.
     Instead of assume guarantee checking, just check all interleavings.

     

     Need to synchronize this with the TAR algorithm.
     *)
  let rec model_check ?(max_depth=initial_dfs_bound) init o_err =
    (* Parallel is written here?  well, you know, with number of processors == 1, parallel becomes sequental ;-) *)
    Message.msg_string Message.Debug "Entering parallel_model_check" ;
    Message.msg_string Message.Debug ("   init region: " ^ (Region.to_string init)) ;
    Message.msg_string Message.Debug 
      (match o_err with None -> "No error region" | Some e -> "   error region: " ^ (Region.to_string e)) ;
    let err = 
      match o_err with
        None -> failwith "parallel_model_check: error region not specified."
      | Some e -> e
    in 
    (* the final result of the search will be stored here *)
    let resulting_error_path = ref None
    and offending_error_node = ref None
      
    (* this boolean variable allows to stop the while-loop *)
    and continue = ref true

    (* the reached region is initially empty *)
    and reached_region = ref (if not (Options.getValueOfBool "stop-sep") then Region.bot else Region.reached_bot ())

    (* helper object to create nodes *)
    and tnc = new tree_node_creator

   
    (* the set of pending nodes to be processed *)
    (* There are three search strategies -
       dfs, bfs, and bounded dfs *)
    and unprocessed_nodes =
      if (Options.getValueOfBool "dfs") then new Search.dfsiterator
      else if (Options.getValueOfBool "bnddfs") 
           then new Search.bounded_dfs_iterator max_depth (fun n -> (Tree.get_node_label n).depth)
           else new Search.bfsiterator (* bfs is default *)

    (* the global time used for time stamps *)
    and time = ref 0 in

    (* The function for constructing children .. and adding them to the tree *)
    (* construct children of node node along the operation op and appends it to the beginning of accu list *)
    (* the argument order is tied to folding *)
    let construct_children node accu op = (tnc#create_child Unprocessed op node)::accu in                                
 (* the root of the reachability tree *)
    let tree_root =
      tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init ; })
    in
    (* "refine" abstraction: recreate unprocessed_nodes and update reached_region
    	it just updates marking, actual refinement happens in next main loop iterations.
	anc is the first node, to which a new interpolant is assigned.
	new_anc_region is its new region *)
    let do_subtree_cut_refinement anc new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      anc_marking_data.region <- new_anc_region ;
      
      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
        (* Note that here there is no path to update as the entire subtree is going to be chopped off... *)
      Message.msg_string Message.Debug "Now deleting subtree:" ;
      tnc#delete_children anc;
      Message.msg_string Message.Debug "SKY: About to clean unprocessed..." ;
      unprocessed_nodes#empty_out ();
      (* Jhala: I'm assuming that its ok to just empty this out and fill it back up when we are traversing the tree later ... this whole traversal business is not good by the way, there should be a much more incremental way to do it *)
      (* SKY: You were wrong, Ranjit, so wrong...  For breadth-first (i.e. default) traversal in the unprocessed_nodes array there may be the nodes that are on the other execution branch and post may not reach them.  With nondeterministic choice it's crucial. *)
      Message.msg_string Message.Debug "Now re-adding children:" ;
      let children = List.fold_left (construct_children anc) [] 
          (Abstraction.enabled_ops new_anc_region) in
      Message.msg_string Message.Debug "The children are:" ;
      List.iter (function child -> Message.msg_printer Message.Debug print_tree_node child) children ;

       (* add the children to the set of pending nodes *)
      (* Jhala: This should all be done by the the subtree cut 
         Message.msg_string Message.Debug "Adding the children to the set of pending nodes" ;
         unprocessed_nodes#add_list children ;
         *)
      (* mark n as processed and uncovered *)
      Message.msg_string Message.Debug "Updating the node's marking" ;
      
      anc_data.mark <- Processed_Uncovered anc_marking_data ;
      Message.msg_string Message.Debug "This node now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc;
      (* add n's region to the reached region *)
      Message.msg_string Message.Debug "Updating the currently reached region 3" ;
      (* This procedure updates markings of leaves of current abstraction and adds them to the unprocessed_nodes *)
      (* It also updates current region reached so far *)
      (* for BFS time_stamp doesn't matter*)
      reached_region := Stats.time "update_tree_after_refinement[CUT]" 
           (update_tree_after_refinment tree_root unprocessed_nodes) anc_marking_data.time_stamp
    in


    (* the refinment process *)
    (* this function is obsolete and is not called by default *)
    let do_refinment anc path new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      anc_marking_data.region <- new_anc_region ;
      
      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
      
        (* update the path *)
      Message.msg_string Message.Debug "Refining the nodes along the path..." ;
      refine_path anc path ;
      
        (* update the tree and the reached region *)
      Message.msg_string Message.Debug "Updating the markings and the reached region..." ;
      reached_region := Stats.time "update after refine [PATH]" (update_tree_after_refinment tree_root
                                                                   unprocessed_nodes) anc_marking_data.time_stamp

    in

    (* this function deletes covered nodes from the tree *)
    let rec reclaim n = 
      if (Tree.has_child n || (not (Tree.has_parent n))) then ()
      else 
        begin
          let par = get_parent n in
          Tree.delete_child n; 
          reclaim par
        end
    in
      (* add the root to the pending unprocessed nodes *)
    unprocessed_nodes#add_element (tree_root) ;

    Ast.set_time_out_signal () ;
    let itl = Options.getValueOfInt "iter-bound" in
    while (!continue && unprocessed_nodes#has_next ()) do
      stats_nb_iterations := !stats_nb_iterations + 1;
      
      try
        begin
          Ast.check_time_out () ;
          (if itl > 0 then
             if !stats_nb_iterations > itl then raise Ast.TimeOutException);
          
          (* the search is not finished yet *)
          (* Dump intermediate abstraction info each 100 iterations*)
          if (!stats_nb_iterations mod 100 == 0) then
            begin
              Message.msg_string Message.Major ("While loop tick:"^(string_of_int !stats_nb_iterations));
	      Stats.push_profile "flush";
	      flush stdout; flush stderr;
	      Stats.pop_profile "flush";
	      if (Options.getValueOfBool "do-dumps") then
		Stats.time "dump abs" Abstraction.print_abs_stats ()
            end ;
          Message.msg_string Message.Normal "\n\n****************************************************************************\n" ;
          Message.msg_string Message.Normal "Next iteration of model-check's big while-loop" ;
          Message.msg_printer Message.Normal Format.pp_print_int !stats_nb_iterations;
          (* Message.msg_string Message.Debug "Currently reached region:" ;
             Message.msg_printer Message.Debug Region.print !reached_region ;*)

          (* let's process the next pending node *)
          let n : tree_node = unprocessed_nodes#next () in
            if (!stats_nb_iterations mod 100 == 0) then
              begin
		Message.msg_string Message.Normal ("Node Depth: "^(string_of_int (BiDirectionalLabeledTree.node_depth n)));
		Stats.push_profile "flush";
		flush stdout; flush stderr;
		Stats.pop_profile "flush";
              end ;
	    Stats.push_profile "logging node";
            Message.msg_string Message.Normal (*Debug Normal*) "Now processing tree node:" ;
	    Message.msg_printer (if Options.getValueOfBool "devdebug" then Message.Normal else Message.Debug) print_tree_node n;
	    Stats.pop_profile "logging node";
            
            let n_data : node_data  = (Tree.get_node_label n) in
              
	    (* update node's region if it's Unprocessed.
	      If it's Processed_Was_Covered_To_Reprocess, it has a correct region within itself; and we should construct its children only and not touch the reg. *)
            let n_marking_data = 
              match n_data.mark with
                  Unprocessed ->
                    begin
                      try
                        let op : Operation.t = (get_op n) in
			let rerere = Stats.time "get new reg" (fun nn -> get_region (get_parent nn)) n in
                          { time_stamp = (time := !time + 1 ; !time) ;
                            region = Stats.time "main post" (Abstraction.post rerere op)  global_variable_table; }
                      with
                          Abstraction.RecursionException ->
                            begin
                              let par = get_parent n in
                                dumppath par (get_region par); (*  n_marking_data.region; *)
                                failwith "Program is recursive!"
                            end
                    end
                | Processed_Was_Covered_To_Reprocess md ->
                    md
		(* It found a Processed_Covered node! *)
                | _ -> failwith "LazyModelChecker.model_check: unexpected marking data"

            in
              (* --- for debugging --- *)
              (* assert (n_data.mark == Unprocessed) ; *)

            let n_region = n_marking_data.region in
            let print_region () =
              if (!stats_nb_iterations mod 1000 = 0 || Options.getValueOfBool "devdebug") 
              then
                (Message.log_string_always Message.Rgn "Processed node's region:" ;
                 Region.log_region Message.LogAlways Message.Rgn n_region)
              else
                (Message.log_string_norm Message.Rgn "Processed node's region:" ;
                 Region.log_region Message.LogNormal Message.Rgn n_region);
            in
	    let _ = Stats.time "Printing region info" print_region () in
              (* the error region at this node is the intersection of overall error region (location and predicate) and current node's region *)
            let err_n = Stats.time "cap" (Region.cap n_region) err in
              Message.msg_string Message.Debug ("Error region at this node: " ^ (Stats.time "logging region" Region.to_string err_n)) ;

              if (Region.is_empty err_n)
              then
                begin
                  (* no error found at this node *)
                  Message.msg_string Message.Normal "No error found at this node" ;
  
                  (* let's test whether this node is covered *)
                  Message.msg_string Message.Debug "Let's test whether this node is covered" ;
                  let coverable =  
                    if (Options.getValueOfBool "cov") 
                    then Stats.time "coverable" Abstraction.coverable n_region
                    else true
                  in
                  let covered = 
                    if coverable
                    then (Stats.time "Covered check:" (Abstraction.covered n_region) !reached_region)
                    else false
                  in
                    if (covered)
                    then
                      begin
                        (* this node is covered *)
                        Message.msg_string Message.Normal "This node is covered" ;

			(*j
			(* why's it covered *)
			Message.msg_string Message.Debug "*** COVERED ";
			Message.msg_string Message.Debug (Region.to_string n_region);
			Message.msg_string Message.Debug  " BY: ";
			Message.msg_printer Message.Debug print_node n';
			*)
                        
                        (* mark n as processed and covered *)
                        n_data.mark <- Processed_Covered n_marking_data;
                        if (Options.getValueOfBool "reclaim") 
                        then
                          reclaim n;
                        ()
                      end
                    else (* node's not covered *)
                      begin
                        (* this node is not covered *)
                        Message.msg_string Message.Debug "This node is not covered" ;
                        Message.msg_string Message.Debug "Constructing its successor children..." ;
                        
                        (* we construct its successor children *)
Stats.push_profile "second half 2 half 1";

			(* We may need alias analysis here if we're processing function pointers to disambiguate the call! *)
			let alias_fn () =
			  if alias_not_done() then begin
			    Message.msg_string Message.Normal ("Function pointer found, performing alias analysis...") ;
			    perform_alias_analysis();
			    Message.msg_string Message.Normal ("Alias analysis completed") ;
			  end else
			    ()
			in
              
                        
                        let children = List.fold_left (construct_children n) []
                          (Abstraction.enabled_ops ~prepare_alias:alias_fn n_region)
                        in
                          
                          Message.msg_string Message.Debug (*Normal*) "The children are:" ;
                          List.iter (function child -> Message.msg_printer Message.Debug (*Normal*) print_tree_node child) children ;
                          
                          (* add the children to the set of pending nodes *)
                          Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
                          unprocessed_nodes#add_list children ;
                          Message.msg_string Message.Debug ("Remaining nodes:"^(string_of_int (unprocessed_nodes#sizeof ()))); 
                          unprocessed_nodes#iter (fun x -> Message.msg_printer Message.Debug print_tree_node x);
Stats.pop_profile "second half 2 half 1";

                          if not (Options.getValueOfBool "stop-sep") then 
                            begin
                              (* add n's region to the reached region *)
                              if (coverable) then
                                begin
                                  Message.msg_string Message.Debug "Updating the currently reached region 4" ;
                                  reached_region := Stats.time "Region.cup" (Region.cup !reached_region) n_region
                                end
                              else () ;
                              (* mark n as processed and uncovered *)
                              Message.msg_string Message.Debug "Updating the node's marking" ;
                          
                              (*
                                Should update the lattice state at this point.
                              *)
                              Message.msg_string Message.Debug "Now updating the current region by joining lattice part";
                              let updated_marking_data = {n_marking_data with region = Stats.time "update marking" (Region.update_lattice_in_region n_region) !reached_region} in
                                (*
                                  Message.msg_string Message.Normal "Old region is ";
                                  Message.msg_printer Message.Debug Region.print n_marking_data.region ; 
                                  Message.msg_string Message.Normal "New region is ";
                                  Message.msg_printer Message.Debug Region.print updated_marking_data.region ; 
                                *)
                                n_data.mark <- Processed_Uncovered updated_marking_data ;
                             end
                          else
                             begin
		 		Message.msg_string Message.Debug "cpa-fix: In model_check" ;
	  		        Region.check_union !reached_region;
				let (new_reached, new_region) = 
					Region.cpa_merge_reached !reached_region n_region (Options.getValueOfString "merge")
				in
				  begin 
                                	Message.msg_string Message.Debug "cpa-fix";
					reached_region := new_reached;
                                	Message.msg_string Message.Debug "Don't change current region after cup";
                                	let updated_marking_data = {n_marking_data with region = new_region} in 
                                  	  n_data.mark <- Processed_Uncovered updated_marking_data ;
				  end
                             end;
                            
                            Message.msg_string Message.Debug "This node now looks like:" ;
                            Message.msg_printer Message.Debug print_tree_node n ;
                    

                      end
                end
              else (* error region of current node is not empty *)
                begin
		  if ((alias_not_done()) && true) then begin
		    Message.msg_string Message.Normal ("Error candidate found, performing alias analysis...") ;
		    perform_alias_analysis();
		    Message.msg_string Message.Normal ("Alias analysis completed") ;
		  end;

                  (* error found *)
                  Message.msg_string Message.Normal "Error found : checking validity." ;
		  (* outputting the tree *)
		  print_whole_tree tree_root ; 
                  Message.msg_string Message.Debug ("Error at depth"^(string_of_int (Tree.node_depth n))); 
                  (*    List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) (Tree.path_to_root n) ; *)
                  if (Options.getValueOfBool "cref") 
		    (* SKY: We never traverse to this branch, because if we would like to, we should use
		    not (Options.getValueOfBool "incref").  This branch doesn't work anyway, so we just
		    forget about it. *)
                  then 
                    begin
                      let path_to_root = Tree.path_to_root n in
                      let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
                      let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
                        (* note that list_of_regs should be 1 longer than list of ops *)
                      let _ = 
                        try 
                          Message.msg_string Message.Debug ("Found error length: "^(string_of_int (List.length list_of_regs)));
                          Abstraction.analyze_trace list_of_regs list_of_ops
                        with fail_exception ->  
                          begin
                            List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x)
                              path_to_root;
                            print_string "Uh Oh! Error in analyze_trace!";
                            raise fail_exception
                          end 
                      in
                        stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
                        do_subtree_cut_refinement tree_root (get_region tree_root)
                          
                    end
                  else  (* this branch works *)
                    begin
                      Message.msg_string Message.Debug "Error found at this node" ;
                      
                      (* this node will have to be reprocessed after refinment *)
                      (* JHALA: why will this node have to be reprocessed after refinement ? *) 
                      Message.msg_string Message.Debug "Re-adding this node to the set of pending unprocessed nodes" ;
                      n_data.mark <- Processed_Was_Covered_To_Reprocess n_marking_data ;
                      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
                      Message.msg_printer Message.Debug print_tree_node n ;
                      unprocessed_nodes#add_element n ;
                      
                      (* let's now test whether this error is a real one *)
                      Message.msg_string Message.Debug "Testing whether this error is a real one" ; 
                      let check_refinement = 
                        (*
                          if (Options.getValueOfString "refine" = "fwd") then
                          check_error_fwd 
                          else *)
                        check_error
                      in
                        try 
                          match (Stats.time "check_error" (check_refinement n) err_n) with
                              EmptyErrorAtNode(err_anc, anc, path) ->
                                (* this is a false error *)
                                begin
                                  Message.msg_string Message.Debug "This is a false error (seq)" ;
                                  Message.msg_string Message.Debug "The check_error function ended up with" ;
                                  Message.msg_string Message.Debug "     at ancestor: " ;
                                  Stats.time "logging region"  (Message.msg_printer Message.Debug print_tree_node) anc ;
                                  Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
                                  Stats.time "logging region" (Message.msg_printer Message.Debug print_tree_node) anc ;
                                  List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
                                  demo_process n n_region false;
                                  
                                  if (Options.getValueOfBool "stop") then
                                    failwith ("FALSE ERROR CHOKE");
                                  (* let's refine *)
                                  Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path" ;
                                  if (Options.getValueOfBool "traces") then dumppath n n_region;
                                  
                                  let anc_region = (get_region anc) in
                                  let focused_anc_region = 
                                    try
                                      let rv = 
                                        match Options.getValueOfBool "block" with
                                            false ->
                                              Stats.time "focus" (Abstraction.focus anc_region) err_anc 
					  (* this branch works *)
					  (* and actullay does nothing if craig >= 2 *)
                                          | true -> Abstraction.focus_stamp anc_region 
                                              (* analysis is already done! *) 
                                      in
                                        if (Options.getValueOfBool "localrestart") then
                                          local_restart_counter := !local_restart_counter + 1;
                                        if (Options.getValueOfBool "restart" || ( Options.getValueOfBool "localrestart" && !local_restart_counter mod 15 == 0)) then 
                                          begin
                                            Message.msg_string Message.Error "About to Restart";
                                                                                  
                                            raise RestartException
                                            
                                          end
                                        else 
                                          begin
                                            Options.setValueOfBool "localrestart" false;
                                            rv
                                          end
                                    with Abstraction.NoNewPredicatesException ->
                                      begin
                                        if (Options.getValueOfInt "predH" = 3) then
                                          begin
                                            let path_to_root = Tree.path_to_root n in
                                            let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
                                            let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
                                              Message.msg_string Message.Debug 
                                                ("\n No of regs: "^(string_of_int (List.length list_of_regs)));
                                              Message.msg_string Message.Debug 
                                                ("\n No of ops : "^(string_of_int (List.length list_of_ops)));
                                              try 
                                                (Abstraction.find_all_predicates list_of_regs list_of_ops; 
                                                 raise RestartException
                                                )
                                              with e -> 
                                                
                                                begin
                                                  if (e = RestartException) then raise e 
                                                  else 
                                                    begin
                                                      Message.msg_string Message.Minor "This is a false error" ;
                                                      Message.msg_string Message.Minor "The check_error function ended up with" ;
                                                      Message.msg_string Message.Minor ("     an empty error: " ^ (Region.to_string err_anc)) ;
                                                      Message.msg_string Message.Minor "     at ancestor: " ;
                                                      Message.msg_printer Message.Minor print_tree_node anc ;
                                                      Message.msg_string Message.Minor "     along the path (from the ancestor to the current node):" ;
                                                      Message.msg_printer Message.Minor  print_tree_node anc ;
                                                      List.iter (function x -> Message.msg_printer Message.Minor print_tree_node x) path ;
                                                      Message.msg_string Message.Debug ("find_all_predicates raises : "^(Printexc.to_string e)) ; 
						      print_tree_dot (List.hd path_to_root);
                                                      dumptrace (false, list_of_ops, list_of_regs);
                                                      failwith ("problem in find_all_predicates !") 
                                                    end
                                                end
                                          end
                                        else 
                                          if (Options.getValueOfInt "predH" = 1 &&
                                              (not (Abstraction.predicate_fixpoint_reached ()))) 
                                          then (Message.msg_string Message.Debug "Raising Restart"; raise RestartException)
                                          else
                                            begin
                                              let path_to_root = Tree.path_to_root n in
                                              let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
                                              let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
						print_tree_dot (List.hd path_to_root);
                                                dumptrace (false, list_of_ops, list_of_regs); Message.msg_string Message.Normal "No new predicates found!";
                                                failwith ("No new preds found !-- and not running allPreds ...")
                                            end
                                      end
                                  in (* for the focused_anc_region *)
                                    Message.msg_string Message.Debug "Done calling Focus";
                                    Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
                                    
                                    stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;
                                    begin
                                      match (Options.getValueOfString "comp") with
                                          "path" -> 
					    (* is obsolete and not called *)
                                            begin
                                              Stats.time "do_ref [PATH]" (do_refinment anc path) focused_anc_region ;
                                              Message.msg_string Message.Error "PATH"; 
                                              Message.msg_string Message.Debug "The refined path looks like:" ;
                                              Message.msg_printer Message.Debug print_tree_node anc ;
                                              List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
                                            end
					(* this branch always works *)
                                        | _ -> Stats.time "subtree_cut_refinement" (do_subtree_cut_refinement anc) focused_anc_region;
                                    end
                                end
                            | NonEmptyErrorAtRoot(err_root, root, path) ->
                                (* --- for debugging --- *)
                                begin
                                  assert (root == tree_root) ;
                                  (* this is a real error *)
                                  (* we put the error path in the appropriate variable *)
                                  
                                  resulting_error_path := Some(err_root,
                                                               List.map get_op path,
                                                               err_n) ;
                                  offending_error_node :=
                                    Some (match path with
                                              [] -> root
                                            | _ -> Misc.list_last path);
                                  (* print the unknown functions on the path *)
                                  Abstraction.print_debug_info (List.map get_op path);
                                  demo_process n n_region true;
                                  
                                  (* and break the while-loop *)
                                  continue := false
                                end
                        with RestartException -> 
                          begin
                            stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
                            do_subtree_cut_refinement tree_root (get_region tree_root)
                          end
                    end
                end;
        end
      with
        | Ast.TimeOutException as e ->
            (*print_whole_tree tree_root ;*) raise e
        | e -> raise e
    done ;
    
    Message.msg_string Message.Normal ("Depth of tree: "^(string_of_int (Tree.subtree_depth tree_root)));
    Stats.time "print reach tree" print_whole_tree tree_root;
    
    match (!resulting_error_path) with 
      Some ((_,_,err_n) as e_path) -> 
        begin
          let err_node = 
            match !offending_error_node with
                    Some n -> n
            | None -> failwith "Quit while w/ error loop w/o setting offending_error_node!"
          in
	  Message.msg_string Message.Normal "Final error trace:";
          let path_to_root = Tree.path_to_root err_node in
          let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[get_region err_node] in
          let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
          if not (Options.getValueOfBool "interactive") then
	    print_tree_dot (List.hd path_to_root);
            dumptrace (true,list_of_ops,list_of_regs);
          Error_reached e_path
            
        end
    | None -> (* print the tree *) 
          if unprocessed_nodes#was_bound_exceeded ()=true then
            begin
              Message.msg_string Message.Debug
                ("Bound of " ^ (string_of_int max_depth) ^
                 " was reached with no error, trying again with max_depth = " ^
                 (string_of_int (2*max_depth)));
              model_check ~max_depth:(2 * max_depth) init o_err
            end
          else
            begin
                (count_tree_nodes tree_root);
              (* !resulting_error_path *)
              No_error
            end
         

(****************************************************************************)
(* Extended TAR algorithm *)
(****************************************************************************)

  exception StuckOrErrorException 
     
  

  let model_check_extar init o_err = 
    Message.msg_string Message.Debug "Entering model_check (Extended Tar)" ;
    Message.msg_string Message.Debug ("   init region: " ^ (Region.to_string init)) ;
    Message.msg_string Message.Debug (match o_err with None -> "No error region" | Some e -> "   error region: " ^ (Region.to_string e)) ;
    
    let initial_automaton = 
      let m = Processed_Uncovered { time_stamp = 0; region = init ; } 
      in
        make_initial_auto ((Tree.create_root { id  = 0 ; mark = m ; depth = 1}) : tar_tree_node) in 
      (* initial automaton: two states: INIT and STUCK *)
    let current_automaton = Array.make (*List.length initlist*)1 initial_automaton in
    
    let current_ctr = ref 1 in (* current value of counter : start with one other thread *)
    Ast.Counter.set_counter !current_ctr ; (* set counter limit to !current_ctr *)
    try   
      let flag = ref true 
      in
      Ast.set_time_out_signal () ;

      while (!flag) do
        begin
          Message.msg_string Message.Debug "\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n" ;
          Message.msg_string Message.Debug "Next iteration of model-check's outer while-loop\n\n\n" ;
          Message.msg_string Message.Debug ("Current counter value is "^(string_of_int !current_ctr)) ;
          Message.msg_string Message.Debug "Printing Automaton\n" ;
          for i = 0 to Array.length current_automaton -1 do
            print_graph "" current_automaton.(i).EnvAutomaton.autom_root ;
            Message.msg_string Message.Debug (Printf.sprintf "Automaton %d has %d states " i (current_automaton.(i).EnvAutomaton.number_of_states));      
          done ;

          Ast.check_time_out () ;
          stats_nb_iterations_of_outer_loop := !stats_nb_iterations_of_outer_loop + 1;
          flag := false;
          
          let do_reachability init_reg break_on_error_flag =
            begin
              (* set up the reachability search *)
              Message.msg_string Message.Debug "In do_reachability" ; 
              (* the reached region is initially empty *)
              let reached_region = ref (if not (Options.getValueOfBool "stop-sep") then Region.bot else Region.reached_bot ())
              and reached_region_cov = Region.emptyCoverers ()
              (* helper object to create nodes *)
              and tnc = new tar_tree_node_creator
              (* the set of pending nodes to be processed *)
              and unprocessed_nodes = if (Options.getValueOfBool "bfs") then new Search.bfsiterator else new Search.dfsiterator
               (* the global time used for time stamps *)
              and time = ref 0 in
               (* The function for constructing children .. and adding them to the tree *)
              let construct_children node accu op =
                (tnc#create_child Unprocessed ({ op = op ; movement_map = [] }) node)::accu in                           
               (* the root of the reachability tree *)
              let tree_root =
                tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                                      region = init_reg ; })
              in
              let no_error n n_data n_marking_data = (* no error found at this node *)
                Message.msg_string Message.Debug "No error found at this node" ;
                Message.msg_string Message.Debug "Let's test whether this node is covered" ;
                Message.msg_string Message.Debug "Currently reached region:" ;
                Message.msg_printer Message.Debug Region.print !reached_region ;
                Message.msg_string Message.Debug "Current region:" ;
                Message.msg_printer Message.Debug Region.print n_marking_data.region ;
                let coverable = 
                  if (Options.getValueOfBool "cov") 
                  then Abstraction.coverable n_marking_data.region else true
                in
                let covered = 
                  if (Options.getValueOfBool "ecov") then
                    if (Region.is_empty n_marking_data.region) then true
                    else
                      match Region.findExactCoverer reached_region_cov n_marking_data.region 
                      with
                          Some i -> 
                            begin
                              Message.msg_string Message.Normal ("Covered by tree node: "^(string_of_int i));
                              true
                            end
                        | None -> false
                  else
                    if coverable then 
                      (Stats.time "Covered check:"
                         (Abstraction.covered n_marking_data.region) !reached_region)
                    else false 
                in
                if covered
                then
                  begin                 (* this node is covered *)
                    Message.msg_string Message.Normal "This node is covered" ;
                                        (* mark n as processed and covered *)
                    n_data.mark <- Processed_Covered n_marking_data
                  end
                else
                  begin                 (* this node is not covered *)
                    Message.msg_string Message.Debug "This node is not covered" ;
                    Message.msg_string Message.Debug "Constructing its successor children..." ;
                    
                    (* we construct its successor children *)
                    let children = let _chld = ref [] in
                      for i = 0 to Array.length current_automaton -1 do 
                        _chld := List.fold_left (construct_children n) !_chld
                           (Abstraction.tar_enabled_ops current_automaton.(i) n_marking_data.region) done ; !_chld in (* FIX THIS TO TAKE AUTO_ID*)
                    Message.msg_string Message.Debug "The children are:" ;
                    List.iter (function child -> Message.msg_printer Message.Debug print_tar_tree_node child) children ;
                    (* add the children to the set of pending nodes *)
                    Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
                    unprocessed_nodes#add_list children ;
                    (* mark n as processed and uncovered *)
                    Message.msg_string Message.Debug "Updating the node's marking" ;
                    n_data.mark <- Processed_Uncovered n_marking_data ;
                    Message.msg_string Message.Debug "This node now looks like:" ;
                    Message.msg_printer Message.Debug print_tar_tree_node n ;
                    (* add n's region to the reached region *)
                    Message.msg_string Message.Debug "Updating the currently reached region 5" ;
                    reached_region := Region.cup !reached_region n_marking_data.region;
                    if (Options.getValueOfBool "ecov") then
                      Region.addCoverer reached_region_cov n_marking_data.region (get_id n)
                  end
              in              
            
              (* This function checks if an error region reached in the thread is a real
                 error trace (given the current assumption about the other thread).
                 *)
              let error_reached (n : tar_tree_node) n_marking_data automaton (env_info_list : ((int * Expression.lval) list list)) =
                begin
                  let n_data = (Tree.get_node_label n) in
                  let n_region = n_marking_data.region in
                    (* let's now test whether this error is a real one *)
                  Message.msg_string Message.Normal "Testing whether this error is a real one" ; 
                  Message.msg_string Message.Debug "Testing whether this error is a real one" ; 
                  stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;

                  let rec print_path n =
                    match (Tree.get_parent_edge n) with
                      None -> Message.msg_printer Message.Normal print_tar_tree_node (n) 
                    | Some e -> let n' = Tree.get_source e  in print_path n' ; 
                        let elabel = Tree.get_edge_label e in
                        (match elabel.op with
                          Abstraction.ProgramOperation op -> Message.msg_string Message.Normal "Program moves" ; Message.msg_printer Message.Normal Operation.print op 
                        | Abstraction.AutomatonOperation (i,j) -> Message.msg_string Message.Normal ("\nAutomaton moves from state "^(string_of_int i)^" to state "^(string_of_int j)) ) ;
                        Message.msg_printer Message.Normal print_tar_tree_node (n)
                  in
                  print_path n  ;
                  (*if (Options.getValueOfBool "stop") then
                    begin
                      failwith ("FALSE ERROR CHOKE");
                    end ;
                  *)

                  Message.msg_string Message.Debug "Now getting regions and operations" ;
                  let rec get_regs_and_ops n regs ops =
                    match (Tree.get_parent_edge n) with
                      None -> (* reached root *) (regs, ops)
                      | Some e -> let n' = Tree.get_source e  in 
                        let elabel = Tree.get_edge_label e in
                        get_regs_and_ops n' ((get_region n')::regs) ((elabel.op, elabel.movement_map) :: ops)
                  in
                  let (reglist, op_and_map_list) = get_regs_and_ops n [ n_region ] [] in
                  let env_list = match env_info_list with 
                    [] -> []
                  | [(i,x); (_, y)] :: more_races -> (* find a node in state i that modifies x *)
                     begin
                       let xmodifier = let allnodes = List.map (fun k -> Hashtbl.find automaton.EnvAutomaton.o_map k) (Hashtbl.find automaton.EnvAutomaton.a_to_o_map i) in
                         List.find (fun nd -> let oedges = List.map BiDirectionalLabeledGraph.get_edge_label (BiDirectionalLabeledGraph.get_out_edges nd) in
                           List.exists (fun eop -> match eop with Abstraction.ProgramOperation op -> let (_,w) = Abstraction.reads_and_writes op in
                                          List.mem x w
                                        | _ -> false ) oedges)    allnodes
                       in [ xmodifier ] 
                     end
                   | _ -> failwith "Ouch"
                  in
                  Abstraction.parallel_block_analyze_trace automaton (reglist, op_and_map_list) 
                                          (* where the automata are *)env_list (* current value of counter *)(Ast.Counter.make_counter !current_ctr) ;
                  No_error

                end
              in
            (* continue_reachability says if there was an error *)
              let continue_reachability = ref true in
              stats_nb_iterations := 0; 
              
            (* add the root to the pending unprocessed nodes *)
              unprocessed_nodes#add_element (tree_root) ;
              while (!continue_reachability && unprocessed_nodes#has_next ()) do
                stats_nb_iterations := !stats_nb_iterations + 1;
                Message.msg_string Message.Debug "\n\n************************************************************************\n" ;
                Message.msg_string Message.Debug "Next iteration of model-check's inner reachability loop" ;
                if (!stats_nb_iterations mod 100 = 0) then
                  Message.msg_string Message.Normal ("While loop tick:"^(string_of_int !stats_nb_iterations));

                        let dp_reset = Options.getValueOfInt "simpres" in
                let _ = 
                  if dp_reset = 0 then () else
                    if (!stats_nb_iterations mod dp_reset = 0) then 
                      Abstraction.reset_decision_procedures ()
                in
        Message.msg_string Message.Debug "Currently reached region:" ;
                Message.msg_printer Message.Debug Region.print !reached_region ;
                
                (* let's process the next pending node *)
                let n        = unprocessed_nodes#next () in
                Message.msg_string Message.Debug   "Now processing tree node:" ;
                Message.msg_printer Message.Debug  print_tar_tree_node n ;
                
                let n_data   = (Tree.get_node_label n) in
                
                let n_marking_data = 
                  match n_data.mark with
                    Unprocessed ->
                      let op = (get_op n) in let parent = get_parent n in
                      (* compute the post to get a list of regions differing only in the counter maps (e_control fields) *)
                      Message.msg_string Message.Debug "Computing tarpost" ;
                      let reglist = Abstraction.tarpost (get_region parent) op.op current_automaton.(0) in
                      Message.msg_string Message.Debug "Printing tarpost" ; 
                      List.iter (fun (r,_) -> Message.msg_printer Message.Debug Region.print r) reglist ;
                      Message.msg_string Message.Debug "Done Computing tarpost" ; 

                      (* for each region except the first, create a new child in the tree, fill the marking data of
                         that child and add the child to the list of unprocessed nodes; explore the first region now *)
                      if (List.length reglist > 1) then
                        begin
                          let children_regs = List.tl reglist in
                          let children = List.map (fun (reg,who_goes_where) ->  let eop = { op = op.op ; movement_map = who_goes_where } in
                          tnc#create_child (Processed_Was_Covered_To_Reprocess { time_stamp = (incr time; !time) ; region = reg;}) eop parent) children_regs
                          in
                          unprocessed_nodes#add_list children ; (* add all the other children to the set of unprocessed nodes *)
                        end ;
                      (match reglist with
                        [] -> op.movement_map <- [] ; { time_stamp = (time := !time + 1 ; !time) ; region = Region.bot ; }
                      | (r,whogoeswhere):: _ -> 
                       
                        op.movement_map <- whogoeswhere ;
                      (* return the first child *)
                      { time_stamp = (time := !time + 1 ; !time) ;
                        region = r ; } )
                  | Processed_Was_Covered_To_Reprocess md ->
                      md
                  | _ -> failwith "LazyModelChecker.model_check: unexpected marking data"
                        
                in
                
                let n_region = n_marking_data.region in
                Message.msg_string Message.Debug "Currently reached region:" ;
                Message.msg_printer Message.Debug Region.print !reached_region ;
                (** look for bugs **)
                (* continue is set to false if there was a bug **)
                begin
                  Message.msg_string Message.Debug "Checking for race conditions." ;
                  (* for each access to a global variable in the program successors check if there
                     is a race with the globals modified by the current automaton states.
                     The automaton states of interest are those for which the environment_region is not empty. *)
                    (* for each enabled successor check if there is a potential race condition *)
                  let ((lvals_read, lvals_written), list_of_automaton_havocs) =
                     (Abstraction.get_modifies current_automaton.(0) n_region) in
                  Message.msg_string Message.Debug "Program modifies : "; 
                  Message.msg_string Message.Debug (Misc.strList (List.map Expression.lvalToString (lvals_written @ lvals_read))) ;

                  Message.msg_string Message.Debug "Automaton modifies : "; 
                  Message.msg_string Message.Debug (Misc.strList (List.map (fun (i,x) -> Expression.lvalToString x) list_of_automaton_havocs)) ;
                  (* there is a race if someone in the list of automaton havocs is aliased to someone in lvals_read or lvals_written *)
                  let possible_races =
                    match !Abstraction.check_races with
                        None -> []
                      | Some race_lval ->
                          begin
(*
                            if (Region.get_location_id n_region = (-1, -1)) then (* B^omega : the program location is fake *)
                              [] (* TODO *)
                            else
*)
                              List.filter (fun p -> match p with [(i,a);(_,b)] -> 
                                           (* RJ: add condition that this is what I am checking for *)
                                           (a = race_lval || b = race_lval) &&
                                           (not (Abstraction.is_lock a)) && (not (Abstraction.is_lock b)) && (Abstraction.can_escape a) && (Abstraction.can_escape b) && ((a=b)|| (AliasAnalyzer.queryAlias (Expression.Lval a) (Expression.Lval b))) 
                                           | _ -> failwith "error in possible races") 
                              (Misc.cartesianProduct [ list_of_automaton_havocs ; List.map (fun l -> (-1,l)) (lvals_written @ lvals_read) ] )
                          end
                  in
                  if (not (possible_races = []) && (not (Region.is_empty n_region))) 
                    (* RJ: and the region is nonempty! Check here with RUPAK -- TBDPLDI*)
                  then 
                    begin (* Found a possible race *)
                      Message.msg_string Message.Normal "Found a possible race in this region." ;
                      if (break_on_error_flag) then raise StuckOrErrorException else () ;
                      let a = error_reached n n_marking_data (current_automaton.(0)) possible_races in
                      match a with
                        No_error ->
                        (* found an infeasible race *)
                          Message.msg_string Message.Debug "Found an infeasible race. Setting continue to false." ;
                          continue_reachability := false 
                      | Race_condition _ -> raise (RaceConditionException a) 
                      (* send back error condition to caller *)
                      | _ -> failwith "model_check: Strange return value!"
                            
                    end
                  else 
                    () (* Message.msg_string Message.Normal "No race found. Continuing." *)
                end;
                    (** finished looking for race bugs **)
                    (** do the following only if continue remains true **)
                if (!continue_reachability = true) then
                (* continue the reachability *)
                  begin
                  (* check for location errors *)
                    match o_err with
                      None ->    no_error n n_data n_marking_data (* no error to check: pure race detection *)
                    |   Some err ->
                      (* check for error location reachability *)
                        begin
                          if Region.is_empty n_region || Region.get_location_id n_region = (-1,-1) then
                            no_error n n_data n_marking_data
                          else
                            let err_n = Stats.time "cap" (Region.cap n_region) err in
                            if (Region.is_empty err_n) then
                              no_error n n_data n_marking_data
                            else
                              begin
                              (* error found. Check if real error *)
                                Message.msg_string Message.Normal "Error found : checking validity." ;
                                if (break_on_error_flag) then raise StuckOrErrorException ;
                                let a = (error_reached n n_marking_data (current_automaton.(0)) []) in
                                match a with
                                  No_error ->
                                   (* found an infeasible error *)
                                    Message.msg_string Message.Debug "Found an infeasible race. Setting continue to false." ;
                                    continue_reachability := false 
                                | Race_condition _ -> raise (RaceConditionException a) 
                                   (* send back error condition to caller *)
                                | _ -> failwith "model_check: Strange return value!"
                              end   
                        end
                          
                  end ;
              done; (* inner reachability loop *)
              (!continue_reachability, tree_root, !reached_region)
            end
            (* Things to return : continue_reachability, tree_root, reached_reg *)
          in
          let init_region = Abstraction.create_parallel_atomic_region (Some init) current_automaton.(0) (Ast.Counter.make_counter !current_ctr) in
          let (continue_reachability, tree_root, reached_region) = do_reachability init_region (* do not break on error or stuck *)false in

          (* outer loop: if continue is false, then there was an error, so reset environment assumptions loop back *)
          if (continue_reachability = false) then
            begin
              (* reset all environment assumptions to false *)
              let _ = Hashtbl.iter (fun a b -> Hashtbl.replace global_variable_table a (ref Region.bot)) 
                  global_variable_table in
              Message.msg_string Message.Debug "Resetting all assumptions in the global_variable_table and continuing reachability" ;
              (* note that refine already adds predicates *)
              for i = 0 to Array.length current_automaton -1 do
                current_automaton.(i) <- initial_automaton ; (* reset the automaton to the initial INIT, STUCK automaton *)
              done ;
              flag := true 
            end
          else
            begin
              let tree_complete_check =
                if (Options.getValueOfBool "tc2") then tree_is_complete2 
                else tree_is_complete
              in
              if ((*Abstraction.exists_stuck reached_region 
                 ||*) (not (tree_complete_check current_automaton.(0) tree_root)) 
                 ) 
              then
                begin
                  Message.msg_string Message.Normal "The environment gets stuck! Rerun loop." ;
                  Message.msg_string Message.Error "The environment gets stuck! Rerun loop." ;
                  (* take the reachability tree and make an automaton *)
                  let this_autom = make_shrink_auto tree_root in
                  current_automaton.(0) <- this_autom ;
                    
                  Message.msg_string Message.Normal "Current automaton is set to the following automaton." ;
                  print_graph "" this_autom.EnvAutomaton.autom_root ;
                  Message.msg_string Message.Normal "Let us now press on ...";
                  flag := true
                end
              else 
                begin (* check if we have reached a fixed point *) 
                  (* first do a reachability with omega many counters *)
                  (* this reachability breaks as soon as some guy gets stuck or an error is found. In that case,
                     bump up the current counter and try again.
                     *)
                  try
                    Message.msg_string Message.Debug "Running reachability with omega counters" ;
                    Message.msg_string Message.Normal "Calling reachability with B^omega" ;Message.msg_string Message.Error "Calling reachability with B^omega" ;
                    
                    let om_start = 
                      if (Options.getValueOfInt "omega" = 0) then
                        (Ast.Counter.omega ())
                      else
                        begin
                          let thisc = Options.getValueOfInt "omega" in
                            Ast.Counter.set_counter (thisc + 1);
                            Counter.make_counter (thisc)
                        end
                          (* first, bump up the counter, then ... *)
                    in
                    let omega_init_reg = Abstraction.create_parallel_atomic_region (None) current_automaton.(0) om_start (* (Ast.Counter.omega ()) *) in
                    let (_, bomega_tree_root, bomega_reached_reg) = Stats.time "do_reach B^omega" (do_reachability omega_init_reg) true in
                    Message.msg_string Message.Normal "Finished reachability with B^omega" ;
                    Message.msg_string Message.Error "Finished reachability with B^omega" ;
                    let actual_auto_size = get_auto_size current_automaton.(0) in
                    let _ = Message.msg_string Message.Normal ("STAT: Final Automaton size:"^(string_of_int actual_auto_size)) 
                    in
                    let _ = Message.msg_string Message.Error ("Final Automaton size:"^(string_of_int actual_auto_size)) 
                    in
                    (* cluster the B^omega tree by edges *)
                    let edge_to_maxctr : (int * int * int, Ast.Counter.counter) Hashtbl.t = Hashtbl.create 31 in
                    let _get_maxctrs (src_node, elabel, target_node) =
                      let emove = elabel.op in
                      let (i,j) = match emove with Abstraction.AutomatonOperation (_i,_j) -> (_i,_j) | _ -> failwith "Edge in Bomega tree marked with program move!" in
                      let src_reg = get_region src_node in
                      if Region.is_empty src_reg then () else
                        let ctrmap = Region.get_ctrmap src_reg in (* src_reg here should always be a ParallelAtomic region *)
                        for k = 0 to current_automaton.(0).EnvAutomaton.number_of_states - 1 do
                          if not (Ast.Counter.eq ctrmap.(k) (Ast.Counter.make_counter 0)) then 
                            if Hashtbl.mem edge_to_maxctr (i,j,k) then
                              Hashtbl.replace edge_to_maxctr (i,j,k) (Ast.Counter.ctrmax ctrmap.(i) (Hashtbl.find edge_to_maxctr (i,j,k)))
                            else Hashtbl.add edge_to_maxctr (i,j,k) ctrmap.(i)
                          else () ;
                        done 
                    in
                    Tree.iter_descendants_edges (_get_maxctrs) bomega_tree_root ;
                    
                    (* At this point, we have constructed a hashtbl keeping, for each edge (i,j) and state k, the counter value
                         max { sigma.i | sigma \in Reach(B^omega) and sigma has an (i,j) edge enabled and sigma.k>0} *)
                    let is_allowed s (i,j) =
                      let autom_map_id = 
                        try
                          Hashtbl.find current_automaton.(0).EnvAutomaton.o_to_a_map 
                            (get_auto_node_id s) 
                            with Not_found ->
                              begin
                                Message.msg_string Message.Error ("Cannot find:" ^(auto_node_to_string (BiDirectionalLabeledGraph.get_node_label s)));
                                failwith "is_allowed cannot find autonode!"
                              end
                      in
                        try
                          let maxct = Hashtbl.find edge_to_maxctr (i,j,autom_map_id) in
                          if autom_map_id = i 
                          then Ast.Counter.geq maxct (Ast.Counter.make_counter 2)  
                          else Ast.Counter.geq maxct (Ast.Counter.make_counter 1)
                        with Not_found -> false 
                          (* no such edge in the tree ! i.e. no edge i-->j when the k component is nonzero *)
                    in              
                    (* check if the old automaton is good *)

                    let omega_check = omega_completeness_check current_automaton.(0) is_allowed in
                    if not omega_check then 
                      begin
                        Message.msg_string Message.Error "Unpleasant! omega makes tree bad!";
                        failwith "die die die"
                      end;
                      
   (*
                    let omega_init_reg = Abstraction.create_parallel_atomic_region (Some init) current_automaton.(0) (Ast.Counter.omega ()) in
                    let (_, tree_root, _) = do_reachability omega_init_reg true in
                    if (not (tree_is_complete current_automaton.(0) tree_root)) 
                    then
                      begin
                        Message.msg_string Message.Normal "Unpleasant! omega makes tree bad!";
                        raise StuckOrErrorException
                      end;
  *)

                    Message.msg_string Message.Normal "Fixpoint reached." ;
                    print_graph "" (current_automaton.(0)).EnvAutomaton.autom_root ;
                    Message.msg_string Message.Normal ("Fixpoint reached with "^(string_of_int !current_ctr)^" counters") ;
                    ()
                  with StuckOrErrorException ->
                    begin
                      Message.msg_string Message.Normal "Reachability with omega threads found error/ got stuck. Bumping up counter!" ;
                      (* bump up counter *) incr current_ctr ;
                      Ast.Counter.set_counter !current_ctr ;
                      if !current_ctr > 3 then failwith "Counter value more than 3!" ;
                      flag := true (* try again *)
                    end
                end
            end
        end
      done; (* outer while loop *)
       (* send back "correct program" to caller *)
      No_error
    with RaceConditionException a -> 
      begin
        a
      end

(* Compute transition invariants for liveness checking *)
let compute_transition_invariant () =
  let worklist = new Search.bfsiterator in      
  let tclosure = ref Region.bot  in
  let compute_post_and_add current_transition =  
    (* what are all the enabled operations from this atomic region? *)
    let enabled_ops = Abstraction.enabled_ops current_transition in
    (* for each operation, compute the post, update the tclosure, and add to the worklist *)
    let process_one_op current op =
      let next = Abstraction.post current op global_variable_table in
      let covered = (Region.leq next !tclosure) in
      if not covered then 
        begin
          worklist#add_element next ;
          tclosure := Region.cup !tclosure next 
        end;
       Message.msg_string Message.Debug ("TI: curr = "^(Region.to_string current));
      Message.msg_string Message.Debug ("TI: next = "^(Region.to_string next));
      Message.msg_string Message.Debug (Printf.sprintf "TI: covered = %b" covered)

    in
    List.iter (process_one_op current_transition) enabled_ops ;
  in
        (* initialize the worklist: for each location l, put in (l,l, \phi) where
           \phi is the predicate abstraction of (x_old = x_new)
         *)
  let _ = 
    (* for every location l, add the region (l,l,initregion) to the worklist *)
    let initregion = Abstraction.ti_init_region () in
         List.iter compute_post_and_add initregion 
  in
  (* fixpoint loop *)
  while (worklist#has_next () ) do
    stats_nb_iterations := !stats_nb_iterations + 1;

    Message.msg_string Message.Debug "\n\n************************************************************************\n" ;
    Message.msg_string Message.Debug "Next iteration of compute_transition_invariant's reachability loop" ;
    Message.msg_printer Message.Debug Format.pp_print_int (!stats_nb_iterations);

        (* let's process the next pending node *)
    let (current_transition) = worklist#next () in
          Message.msg_string Message.Debug ("Now processing transition:") ;
          Message.msg_string Message.Debug (Region.to_string current_transition) ;
    compute_post_and_add current_transition 
  done ;
  (* at this point, tclosure has all the required transitive closure information *)
  (* for each location l, write out (l,l,reached_pred) as a Sicstus prolog constraint *)
  Abstraction.dump_ti_closure !tclosure ;
  ()

(****************************************************************************)
end 
