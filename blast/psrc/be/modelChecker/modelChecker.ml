(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)

(*
 * modelChecker.ml
 * $Id: modelChecker.ml,v 1.6 2007/08/29 23:56:05 beyer Exp $
 *)

open Ast
open BlastArch

module Make_Model_Checker =
  functor (Abstraction : ABSTRACTION) ->
  struct
    module Abstraction = Abstraction 
    module Region = Abstraction.Region
	
    module EnvAutomaton = Abstraction.EnvAutomaton
	
    module Operation = Abstraction.Operation
	
    module Tree = BiDirectionalLabeledTree
	
    type error_path = (Region.t * (Operation.t list) * Region.t)
	  
	  
    (**
       * The time stamp ts of a processed covered node indicates that this node
       * is covered by processed uncovered nodes of time stamp lesser than ts.
       * Conversely, the time stamp ts of processed uncovered node indicates
       * that this node may partially cover any processed covered of time stamp
       * greater than ts.
       *)
    type marking_data = {
	mutable time_stamp : int ;
	mutable region     : Region.t ;
      }
	  
    let print_marking_data fmt md =
      Format.fprintf fmt "@[time_stamp=%d;@ region=" md.time_stamp ;
      Region.print fmt md.region ;
      Format.fprintf fmt "@]"
	
    type marking =
      (* for leaves that have never been processed *)
	Unprocessed
      (* for (already processed) error-free leaves that are covered by
         previously processed (cf. time stamps) nodes *)
      | Processed_Covered of marking_data
      (* for (already processed) error-free nodes that were not covered
         by previously processed nodes at the time they were last
         processed *)
      | Processed_Uncovered of marking_data
      (* for (already processed) error-free leaves that were covered but
         by previously processed nodes that have been refined, and hence
	 have to be re-processed *)
      | Processed_Was_Covered_To_Reprocess of marking_data
	    
    let print_marking fmt = function
	Unprocessed ->
          Format.fprintf fmt "@[Unprocessed@]"
      | Processed_Covered md ->
          Format.fprintf fmt "@[Processed_Covered(" ;
          print_marking_data fmt md ;
          Format.fprintf fmt ")@]" ;
      | Processed_Uncovered md ->
          Format.fprintf fmt "@[Processed_Uncovered(" ;
          print_marking_data fmt md ;
          Format.fprintf fmt ")@]" ;
      | Processed_Was_Covered_To_Reprocess md ->
          Format.fprintf fmt "@[Processed_Was_Covered_To_Reprocess(" ;
          print_marking_data fmt md ;
          Format.fprintf fmt ")@]" ;
	  
    type node_data = {
	id           : int ;
	mutable mark : marking ;
	depth        : int;
      }
	  
	  
    let print_node_data fmt nd =
      Format.fprintf fmt "@[Data(@[id=%d;@ mark=" nd.id ;
      print_marking fmt nd.mark ;
      Format.fprintf fmt "@])@]"
	
    type tree_node = (node_data, Operation.t) Tree.node
	  
    type tar_edge = { op : Abstraction.opKind ; mutable movement_map : (int*int* Ast.Counter.counter) list }
    type tar_tree_node = (node_data, tar_edge) Tree.node
	  
  (* let tree_node_table = Hashtbl.create 1009 *)
	  
  (* this hashtbl is used to remember which leaf is covered by which *)
  (* helper functions to access a tree node's data *)
    let get_node_data n = Tree.get_node_label n
	
    let get_op n =
      match (Tree.get_parent_edge n) with
	Some(e) ->
          (Tree.get_edge_label e)
      | None ->
          invalid_arg "get_op: input node has no parent edge"
	    
    let get_parent n =
      match (Tree.get_parent_edge n) with
        Some(e) ->
          (Tree.get_source e)
      | None ->
          invalid_arg "get_parent: input node has no parent edge"
	    
    let get_children n = Tree.get_children n
	
    let get_id n = (get_node_data n).id
	
    let get_marking n = (get_node_data n).mark
	
    let get_marking_data n =
      match (get_marking n) with
        Unprocessed ->
          invalid_arg "get_marking: input node is unprocessed"
      | Processed_Covered md
      | Processed_Uncovered md
      | Processed_Was_Covered_To_Reprocess md ->
          md
	    
    let get_time_stamp n =
      try (get_marking_data n).time_stamp
      with Invalid_argument "get_marking: input node is unprocessed" ->
	invalid_arg "get_time_stamp: input node is unprocessed"
	  
    let print_tree_node fmt n =
      Format.fprintf fmt "@[TreeNode(@[label=" ;
      print_node_data fmt (get_node_data n) ;
      begin
	match (Tree.get_parent_edge n) with
          None ->
            Format.fprintf fmt ";@ no parent"
        | Some(e) ->
            Format.fprintf fmt ";@ incoming_op=" ;
            Operation.print fmt (Tree.get_edge_label e) ;
            Format.fprintf fmt ";@ parent_id=%d"
              (get_id (Tree.get_source e))
      end ;
      let children_ids = List.map get_id (Tree.get_children n)
      in
      Format.fprintf fmt ";@ children_ids=" ;
      Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
      Format.fprintf fmt "@])@]"
	
    let print_tar_tree_node fmt n =
      Format.fprintf fmt "@[TreeNode(@[label=" ;
      print_node_data fmt (get_node_data n) ;
      begin
	match (Tree.get_parent_edge n) with
          None ->
            Format.fprintf fmt ";@ no parent"
        | Some(e) ->
            Format.fprintf fmt ";@ incoming_op=" ;
	    (match (Tree.get_edge_label e).op with
	      Abstraction.ProgramOperation op ->
		Operation.print fmt op ;
	    | Abstraction.AutomatonOperation (i,j) ->
		Format.fprintf fmt "AutomatonMove(%d,%d) " i j);
            Format.fprintf fmt ";@ parent_id=%d"
              (get_id (Tree.get_source e))
      end ;
      let children_ids = List.map get_id (Tree.get_children n)
      in
      Format.fprintf fmt ";@ children_ids=" ;
      Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
      Format.fprintf fmt "@])@]"
	
	
    let get_region n =
      try (get_marking_data n).region
      with Invalid_argument "get_marking: input node is unprocessed" ->
	invalid_arg "get_region: input node is unprocessed"
	  
  (* helper class to create nodes with appropriate ids *)
    class tree_node_creator =
      object
      val mutable next_id = 0
      method create_root m =
	let this_id = next_id in
        next_id <- next_id + 1 ;
        let rv = ((Tree.create_root { id  = this_id ;
				      mark = m; depth = 1 }) : tree_node) 
	in
	(*  Hashtbl.add tree_node_table this_id (ref rv); *)
	rv
	  
      (* creates child of par along the operation op with the marking m *)
      method create_child m op par =
      let this_id = next_id in
      next_id <- next_id + 1 ;
      let rv = 
        ((Tree.create_child { id  = this_id ;
                              mark = m; depth = (Tree.get_node_label par).depth + 1 } op par) : tree_node)
      in
        (* Hashtbl.add tree_node_table this_id (ref rv); *)
      rv
	
	(* Jhala: It would be nice to have a destroy as well -- or are we relying on some sort of Garbage collection to handle that ? *)
      (* VERIFY: deletes children of node par *)
      method delete_children par = 
	Tree.delete_children (par : tree_node)
	  
    end
	  
  (* helper class to create tar tree nodes with appropriate ids *)
    class tar_tree_node_creator =
      object
      val mutable next_id = 0
      method create_root m =
	let this_id = next_id in
        next_id <- next_id + 1 ;
        let rv = ((Tree.create_root { id  = this_id ;
				      mark = m; depth = 1 }) : tar_tree_node) 
	in
	(*  Hashtbl.add tree_node_table this_id (ref rv); *)
	rv
	  
      method create_child m op par =
	let this_id = next_id in
        next_id <- next_id + 1 ;
	let rv = 
          ((Tree.create_child { id  = this_id ;
				mark = m; depth = (Tree.get_node_label par).depth + 1 } op par) : tar_tree_node)
	in
        (* Hashtbl.add tree_node_table this_id (ref rv); *)
	rv
	  
	(* Jhala: It would be nice to have a destroy as well -- or are we relying on some sort of Garbage collection to handle that ? *)
      method delete_children par = 
	Tree.delete_children (par : tar_tree_node)
	  
    end

  let print_tree_dot root =
    let string_of_node { id = id ; mark = mark } =
      let string_of_marking_data { region = r } =
	Abstraction.Region.string_of_region_for_dot r
      in
      let string_of_mark m = match m with
	  Unprocessed -> "[Unprocessed]"
	| Processed_Covered md ->
	    "[C] " ^ (string_of_marking_data md)
	| Processed_Uncovered md ->
	    "[U] " ^ (string_of_marking_data md)
	| Processed_Was_Covered_To_Reprocess md ->
	    "[R] " ^ (string_of_marking_data md)
      in
	(string_of_int id) ^ "$$" ^ (string_of_mark mark)
    in
    let string_of_edge e =
      String.escaped ""
	(* (Abstraction.Operation.to_string e) *)
    in
    let tree_file = Options.getValueOfString "tree-dot" in
      if tree_file <> "" then
	let chan = open_out_gen [Open_append; Open_creat; Open_text] 420 tree_file in
	  Tree.output_tree_dot string_of_node string_of_edge chan [root];
	  close_out chan
      else
	()
	    
end
    
