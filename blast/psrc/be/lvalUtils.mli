(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
(** Module that performs Lvalue renaming *)
module LvalNumbering :
sig
  type t

  (** Initial, empty map *)
  val map0 : t

  (** Remove the numbering of a lvalue *)
  val clean : Ast.Expression.expression -> Ast.Expression.expression

  (** Get all the value that have been associated to a given map entry (including 0) *) 
  val assigned_values : t -> Ast.Expression.lval -> int list

  (** Lookup function in the map *)
  val theta : t -> Ast.Expression.lval -> int

  (** Provide the lvalue constant corresponding to the given lvalue *)
  val subst_lval : t -> Ast.Expression.lval -> Ast.Expression.expression
  val subst_expr : t -> Ast.Expression.expression -> Ast.Expression.expression

  (** Provide all the lvalue constant corresponding to the given lvalue *)
  val all_subst_lval : t -> Ast.Expression.lval -> Ast.Expression.expression list
  val all_subst_expr : t -> Ast.Expression.expression -> Ast.Expression.expression list


  (** update map written newnum returns a copy of map where the numbering
      is updated supposing that the lvalues in the (closed-under-aliasing)
      set of lvalues written may be written by setting the Chlval index to
      the newnum value *)
  val update : t -> Ast.Expression.lval list -> int -> t

  (** Do the same as update but take as argument a list of expressions.
      Non-Lval expressions are simply ignored *)
  val update_expr : t -> Ast.Expression.expression list -> int -> t

  (** Print a string representation of a map *)
  val string_of_map : t -> string   

  (** Get the set of values for which a mapping exists *)
  val stored_lvals : t -> Ast.Expression.lval list

  (** Get the  list of extended lvalue that have different numberings in
      the two provided maps *)
  val diff : t -> t -> Ast.Expression.lval list

  (** Get the largest label to which a value is mapped *)
  val max_lab : t -> string

  (** Replace the out-most numbering by the older value in the table *)
  val one_step_older : t -> Ast.Expression.expression -> Ast.Expression.expression

end
