(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)
(**********************************************************************************************)

module Operation = Absutil.Make_Operation(C_SD) 

module DataLattice = 
  (* (Lattice.Make_TrivialLattice (C_SD)) (Operation) *)
  ((Unionlattice.Make_UnionLattice (C_SD)) (Operation)) (Events)
  (* ((EventCounterLattice.Make_EventCounterLattice (C_SD)) (Operation)) (Events) *)

(**********************************************************************************************)
(*
 * This module defines a symbolic representation for C programs, equipped
 * with boolean operations.
 *)
module Region =
struct

  (* list of integers corresponding to the predicates used in this region *)
  type predList = int list

  type abstract_data_region = {
    lelement : DataLattice.lattice ;
    bdd   : CaddieBdd.bdd ;
    preds : predList ;
  }

  let true_adr = { lelement = DataLattice.top ; bdd = PredTable.bddOne ; preds = [] }
  let false_adr = { lelement = DataLattice.bottom ; bdd = PredTable.bddZero ; preds = [] }


  type concrete_data_region = {
    pred : Predicate.predicate ;
  }

  (* the type of data regions *)
  type data_region =
      Abstract of abstract_data_region
    | Concrete of concrete_data_region

  (* the type of atomic regions *)
  type stack_contents =
      CallStack of C_SD.location list
    | EveryStack



  let list_of_stack stack = 
    match stack with
	EveryStack -> []
      |	CallStack s -> s


  type atomic_region = {
    location : C_SD.location ;
    data     : data_region ;
    stack    : stack_contents;
  }

  type atomic_ti_region = {
    ti_location_prev : C_SD.location;  
    ti_location : C_SD.location;
    ti_data : data_region;
    ti_stack : stack_contents;  
  }

  let get_stack_opt reg =
    match reg.stack with
	CallStack (h::_) -> Some h
      |	_ -> None

  let get_reg_key reg = reg.location

    
  (* davare: added code to define a control region *)
  type control_region = {
    p_location : C_SD.location;
    p_stack    : stack_contents;
  }
      
  (* the state of the environment is a map from the 
     environment automaton states to counters
  *)
  type environment_region = Counter.counter array (* (int, Counter.counter) Hashtbl.t *)

  let check_locations_eq location1 location2 =
    let (l1, l1') = C_SD.location_coords (location1) and
	(l2, l2') = C_SD.location_coords (location2) in
      (l1 = l2) && (l1' = l2')

  let stack_to_string s =
    match s with
	EveryStack -> "EveryStack@@"
      |	CallStack(_s) -> List.fold_right (function x -> function y -> ((C_SD.location_to_string x)^"@@"^y)) _s ""

  let loc_to_string_id loc = let (a,b) = C_SD.location_coords loc in Printf.sprintf "(%d,%d)" a b

  let check_stack_eq stk1 stk2 =
    if (Options.getValueOfBool "cf") then true (* RJ: CRUCIAL in merge ... sigh. *)
    else begin
      match (stk1, stk2) with
          (EveryStack, EveryStack) -> true
	| (CallStack _s1, CallStack _s2) -> (try List.for_all2 (fun l1 l2 -> check_locations_eq l1 l2) _s1 _s2 with _ -> false)
	| _ -> false
    end

  (* an utility we seem to use quite often.
     Given two environment regions, check if they represent the same counter maps
  *)
  let check_environment_region_leq er1 er2 =
    let l1 = Array.length er1 and l2 = Array.length er2 in
      if l1 <> l2 then failwith "Counter regions with different lengths!" ;
      let flag = ref true in let i = ref 0 in
        while !flag && (!i < l1 ) do
          (if (Counter.leq er1.(!i) er2.(!i)) then () else flag := false ) ; incr i
	done ;
        !flag

  let check_environment_region_eq er1 er2 =
    let l1 = Array.length er1 and l2 = Array.length er2 in
      if l1 <> l2 then failwith "Counter regions with different lengths!" ;
      let flag = ref true in let i = ref 0 in
        while !flag && (!i < l1 ) do
          (if (Counter.eq er1.(!i) er2.(!i)) then () else flag := false ) ; incr i
	done ;
        !flag
	  
  (* this module allows to manipulate maps of pairs (location, stack) to
     data regions *)
  module AtomicRegionTable =
    BatHashtbl.Make (
      struct
        type t = C_SD.location * stack_contents 
	let hash (loc,stack) = 
	  let stack_to_string s =
	    match s with
		EveryStack -> "ES@@"
	      |	CallStack(_s) -> List.fold_right (function x -> function y -> ((C_SD.location_to_string x)^"@@"^y)) _s ""
	  in
	  let stack_to_coords_list s =
	    match s with EveryStack -> [ (-100, -100) ]
	      | CallStack _s -> List.map (function l -> C_SD.location_coords l) _s
          in
	    if not (Options.getValueOfBool "cf") then 
	      (* Instead of mere Hashtbl.hash, we use hash_param here.  In my implementation of OCaml, hash = hash_param 10 100, which is not enough for successfully breaking down the stack in different buckets. *)
	      Hashtbl.hash_param 500 1000 ((C_SD.location_coords loc),(stack_to_coords_list stack))
            else Hashtbl.hash_param 500 1000 (C_SD.location_coords loc)
	      
        let equal (l1, s1) (l2, s2) = 
          let stack_eq stack1 stack2 = 
	    match (stack1,stack2) with
		(CallStack(_s1),CallStack(_s2)) ->
		  (try List.for_all2 (fun l1 l2 -> check_locations_eq l1 l2) _s1 _s2 with _ -> false)
	      | (EveryStack,EveryStack) -> true
	      | _ -> false
	  in
	    (* Message.msg_string Message.Debug ("COMPARE args: "^l1s^" "^l2s); *)
	    if ((check_locations_eq l1  l2) && (Options.getValueOfBool "cf" || stack_eq s1 s2)) then true else false 
      end)

  module AtomicTIRegionTable =
    Hashtbl.Make (
      struct
        type t = (C_SD.location * C_SD.location * stack_contents)  
	let hash (loc,loc',stack) = 
	  let stack_to_string s =
	    match s with
		EveryStack -> "ES@@"
	      |	CallStack(_s) -> List.fold_right (function x -> function y -> ((C_SD.location_to_string x)^"@@"^y)) _s ""
	  in
	  let stack_to_coords_list s =
	    match s with EveryStack -> [ (-100, -100) ]
	      | CallStack _s -> List.map (function l -> C_SD.location_coords l) _s
          in
	    if not (Options.getValueOfBool "cf") then 
	      Hashtbl.hash ((C_SD.location_coords loc),(C_SD.location_coords loc'),(stack_to_coords_list stack))
            else Hashtbl.hash (C_SD.location_coords loc)
	      
        let equal (l1,l1', s1) (l2, l2', s2) = 
          let stack_eq stack1 stack2 = 
	    match (stack1,stack2) with
		(CallStack(_s1),CallStack(_s2)) ->
		  List.for_all2 (fun l1 l2 -> check_locations_eq l1 l2) _s1 _s2 
	      | (EveryStack,EveryStack) -> true
	      | _ -> false
	  in
	    (* Message.msg_string Message.Debug ("COMPARE args: "^l1s^" "^l2s); *)
	    ((check_locations_eq l1 l2) && (check_locations_eq l1' l2')  
             && (Options.getValueOfBool "cf" || stack_eq s1 s2))  
      end)


  type parallel_atomic_region = {
    p_control    : control_region ;
    e_control    : environment_region ;
    p_data       : data_region ;
  }
      
  (* the type of regions *)
  type t =
      Empty
    | Atomic of atomic_region
    | Union of data_region AtomicRegionTable.t
    | ParallelAtomic of parallel_atomic_region
    | ParallelUnion of ((environment_region * data_region) list) AtomicRegionTable.t  
    | AtomicTI of atomic_ti_region
    | UnionTI of data_region AtomicTIRegionTable.t 
	
  let atomic_region_parameter = 100001
    (* the size of the Hashtbl for a union region *)
    (* It should be large enough because the remove_all function that removes all bindings requires some stack, and the more buckets the hash table has the less it is likely to overflow.  101 was certainly not enough. *)

    (* the pretty-printer for abstract data regions *)
    
  let _print_abstract_data_region (detailed:bool) fmt abst_data_reg =
    Format.fprintf fmt "@[AbstrDatRgn:@\n";
    if detailed then 
      (Format.fprintf fmt "Bdd:@\n";
       Predicate.print fmt (PredTable.convertBddToPred abst_data_reg.bdd);
       Format.pp_force_newline fmt ())
    else (Format.fprintf fmt "Predicates: [...]@\n");
    Format.fprintf fmt "Lattice:@\n";
    DataLattice.print fmt abst_data_reg.lelement ;
    Format.pp_force_newline fmt ();
    Format.pp_close_box fmt ()


  let print_abstract_data_region fmt abst_data_reg =
    _print_abstract_data_region (Options.getValueOfBool "bddprint") fmt
      abst_data_reg

  (* JF Old ugly version. Keeping around in case there is code that
     expects this specific format
     let print_abstract_data_region fmt abst_data_reg =
     Format.fprintf fmt "@[Abstract(@[bdd=" ;
     if (Options.getValueOfBool "bddprint") then 
     Predicate.print fmt (PredTable.convertBddToPred abst_data_reg.bdd)
     else ();
     Format.fprintf fmt ";@ preds= [...]" ;
     Format.fprintf fmt ";@[lattice =" ;
     DataLattice.print fmt abst_data_reg.lelement ;
     Format.fprintf fmt "@]@])@]"*)

  let get_adr reg = 
    match reg with Atomic {data = Abstract adr} -> adr
      | _ -> (assert false; failwith "error: get_adr")

  let abstract_data_region_to_string adr = PredTable.bdd_to_string_pretty adr.bdd 

  let string_of_region_for_dot r = match r with
      Atomic a ->
	let (cfa_id, loc_id) = C_SD.location_coords a.location in
	let data_reg = match a.data with
	    Abstract adr ->
	      abstract_data_region_to_string adr
	  | _ -> "-"
	in
	  String.escaped (Printf.sprintf "%d#%d\n%s" cfa_id loc_id data_reg)
    | Empty -> "(empty)"
    | _ -> "?"
	
  let adr_cap adr1 adr2 =
    { lelement = DataLattice.meet adr1.lelement adr2.lelement;
      bdd = CaddieBdd.bddAnd adr1.bdd adr2.bdd ; 
      preds = Misc.union adr1.preds adr2.preds ; }
      
  let adr_cup adr1 adr2 =
    { lelement = DataLattice.join adr1.lelement adr2.lelement;
      bdd = CaddieBdd.bddOr adr1.bdd adr2.bdd ; preds = Misc.union adr1.preds adr2.preds ; }
      

  let bdd_leq bdd1 bdd2 = 
    let union_bdd = CaddieBdd.bddOr bdd1 bdd2 in
      CaddieBdd.bddEqual bdd2 union_bdd

  let adr_bdd_leq adr1 adr2 = 
     (if (Options.getValueOfBool "bddcov") then
	 Stats.time "bdd_leq" (bdd_leq adr1.bdd) adr2.bdd
       else
         let b = Stats.time "adr1 && !adr2" (CaddieBdd.bddAnd adr1.bdd) (CaddieBdd.bddNot adr2.bdd)
	 in
           not (Stats.time "is pred table sat" PredTable.isSatisfiable (b,[])))

  let adr_leq adr1 adr2 =
    let bl = adr_bdd_leq adr1 adr2 
     in
      (* SKY: This shows that lattice coverage is only taken into account when predicate regions cover each other. *)
      if Options.getValueOfBool "devnocov" then false
	else 
         (if bl then Stats.time "lattice.leq" (DataLattice.leq adr1.lelement) adr2.lelement 
                else false)
	
  let adr_leq_cov adr1 adr2 =
    ((adr1.lelement = DataLattice.bottom) || (adr2.lelement = DataLattice.top))
    && 
      (if (Options.getValueOfBool "bddcov") then
         bdd_leq adr1.bdd adr2.bdd
       else
	 let b = CaddieBdd.bddAnd adr1.bdd (CaddieBdd.bddNot adr2.bdd)
	 in
	   not (PredTable.isSatisfiable (b,[])))


  let adr_cubes adr =
    PredTable.bdd_cubes adr.bdd

  let adr_bdd_eq adr1 adr2 = 
     if (Options.getValueOfBool "bddmin") then
	 Stats.time "bdd_equiv" (CaddieBdd.bddEqual adr1.bdd) adr2.bdd
     else (adr_bdd_leq adr1 adr2) && (adr_bdd_leq adr2 adr1)

  let adr_eq adr1 adr2 = 
    if (Options.getValueOfBool "bddmin") then
      CaddieBdd.bddEqual adr1.bdd adr2.bdd && (DataLattice.eq adr1.lelement adr2.lelement)
    else (adr_leq adr1 adr2) && (adr_leq adr2 adr1)
      
  let adr_is_empty adr = 
    (not (DataLattice.is_consistent adr.lelement adr.bdd)) ||  
      (if (Options.getValueOfBool "bddcov") then (CaddieBdd.bddEqual adr.bdd bddZero)
       else (not (PredTable.isSatisfiable (adr.bdd, [] ))))

  let cartesian_adr abst_data_reg = 
    let __l = ref [] in
    let _get_v a = 
      for i = 0 to Array.length a - 1 do if a.(i) = 2 then () else __l := i::!__l ; done 
    in
    let _preds_in_bdd = 
      let _ = CaddieBdd.bddForeachCube (CaddieBdd.bddSupport abst_data_reg.bdd) (_get_v) in 
	!__l 
    in
    let process b i =
      let true_i = (PredTable.getPredIndexBdd i) in
      let false_i = CaddieBdd.bddNot (PredTable.getPredIndexBdd i) in
	match ((bdd_leq abst_data_reg.bdd true_i), (bdd_leq abst_data_reg.bdd false_i)) 
	with
	    (true,true) -> (PredTable.bddZero) 
	  | (false,true) -> CaddieBdd.bddAnd (false_i) b
	  | (true,false) -> CaddieBdd.bddAnd (true_i) b
	  | (false,false) -> b
    in
    let new_bdd = List.fold_left process PredTable.bddOne _preds_in_bdd  in
      { lelement = abst_data_reg.lelement; 
        bdd = new_bdd;
	preds = abst_data_reg.preds
      }
	

  (* the pretty-printer for concrete data regions *)
  let print_concrete_data_region fmt conc_data_reg =
    Format.fprintf fmt "@[ConcrDatRgn:@\n" ;
    Format.fprintf fmt "Predicates:@\n";
    Predicate.print fmt conc_data_reg.pred ;
    Format.fprintf fmt "@\n@]"

  (* the pretty-printer for data regions *)
  let print_data_region (detailed:bool) fmt = function
      Abstract abst_data_reg ->
        _print_abstract_data_region detailed fmt abst_data_reg
    | Concrete conc_data_reg ->
        print_concrete_data_region fmt conc_data_reg

  let my_print_data_region fmt (data_reg : data_region) = 
     print_data_region (Options.getValueOfBool "bddprint") fmt data_reg
 
	  
  (* the pretty-printer for atomic regions *)
  let print_atomic_region (detailed:bool) fmt (atomic_reg : atomic_region) =
    Format.fprintf fmt "@[[AtomicRegion:@ " ;
    C_SD.print_location fmt atomic_reg.location ;
    Format.pp_force_newline fmt ();
    print_data_region detailed fmt atomic_reg.data;
    (match atomic_reg.stack with
	 EveryStack -> Format.fprintf fmt "Stack: Everystack@\n"
       | _ -> 
	   (Format.fprintf fmt "Stack:@\n";
	    Misc.list_printer_from_printer
	      C_SD.print_location fmt (list_of_stack atomic_reg.stack)));
    Format.fprintf fmt "]@]@\n"

      
  (* the pretty-printer for atomic regions *)
  let print_atomicTI_region (detailed:bool) fmt
      (atomicTI_reg : atomic_ti_region) =
    Format.fprintf fmt "@[Atomic(@[prev " ;
    C_SD.print_location fmt atomicTI_reg.ti_location_prev ;
    Format.fprintf fmt "@[Atomic(@[" ;
    C_SD.print_location fmt atomicTI_reg.ti_location ;
    Format.fprintf fmt ";@ data=" ;
    print_data_region detailed fmt atomicTI_reg.ti_data;
    Format.fprintf fmt ";@ stack=" ;
    (match atomicTI_reg.ti_stack with
	 EveryStack -> Format.fprintf fmt "Everystack"
       |	_ -> Misc.list_printer_from_printer C_SD.print_location fmt (list_of_stack atomicTI_reg.ti_stack));
    Format.fprintf fmt "@])@]"


      

  (* the pretty-printer for parallel atomic regions *)
  let print_parallel_atomic_region (detailed:bool) fmt p_atomic_reg =
    Format.fprintf fmt "@[ParallelAtomic";
    (* print program control region *)
    Format.fprintf fmt "(@[" ;
    C_SD.print_location fmt p_atomic_reg.p_control.p_location ;
    Format.fprintf fmt "@]" ;
    Format.fprintf fmt ";@[stack=" ;         
    (match p_atomic_reg.p_control.p_stack with
	 EveryStack -> Format.fprintf fmt "Everystack"
       | _ -> Misc.list_printer_from_printer C_SD.print_location fmt (list_of_stack p_atomic_reg.p_control.p_stack));
    Format.fprintf fmt "@]" ;
    Format.fprintf fmt ";@[environment map=" ;
    for i = 0 to Array.length p_atomic_reg.e_control - 1 do
      if (not (Counter.is_zero p_atomic_reg.e_control.(i)))
      then
        Format.fprintf fmt "(%d, %s)" i (Counter.toString p_atomic_reg.e_control.(i)) 
    done ;
    Format.fprintf fmt "@]" ;
    Format.fprintf fmt ";@[data=" ;
    print_data_region detailed fmt p_atomic_reg.p_data;
    Format.fprintf fmt ")@]"



  (* the pretty-printer for regions *)
  let _print (detailed:bool) fmt reg =
    match reg with
        Empty ->
          Format.fprintf fmt "@[Bottom@]"
      | Atomic atomic_reg ->
          print_atomic_region detailed fmt atomic_reg
      | AtomicTI atomicTI_reg ->
	  print_atomicTI_region detailed fmt atomicTI_reg
      | Union atomic_reg_table ->
	  let reglist = ref [] in
	  let tbl_to_list (l, s) d =
	    reglist := ( { location = l ; data = d ;stack = s ;})::(!reglist);
          in
            Format.fprintf fmt "@[Union " ;
            (AtomicRegionTable.iter tbl_to_list atomic_reg_table) ; 
            Misc.list_printer_from_printer (print_atomic_region detailed)
	      fmt (!reglist);
            Format.fprintf fmt "@]"
      | UnionTI atomicTI_reg_table ->
	  let reglist = ref [] in
	  let tbl_to_list (l, l', s) d =
	    reglist := ( { ti_location_prev = l ; ti_location = l'; ti_data = d ;ti_stack = s ;})::(!reglist);
          in
            Format.fprintf fmt "@[UnionTI " ;
            (AtomicTIRegionTable.iter tbl_to_list atomicTI_reg_table) ; 
            Misc.list_printer_from_printer (print_atomicTI_region detailed)
              fmt (!reglist);
            Format.fprintf fmt "@]"
              
      | ParallelAtomic p_atomic_reg ->
	  print_parallel_atomic_region detailed fmt p_atomic_reg
      | ParallelUnion p_atomic_reg_table ->
	  Message.msg_string Message.Debug "RJ: par union region print suppressed";
	  
          Format.fprintf fmt "@[ParallelUnion " (* ;
						   (AtomicRegionTable.iter (fun (l,s) dlist ->
						   let pc = { p_location = l ; p_stack = s ; } in
						   List.iter (fun (e,d) -> print_parallel_atomic_region fmt ({ p_control = pc ; e_control = e ; p_data = d ; })) dlist)
						   p_atomic_reg_table) ;
						   Format.fprintf fmt "@]" *)


  let print fmt reg =
    _print (Options.getValueOfBool "bddprint") fmt reg

  let log_region (loglevel:Message.logmsg_level) (logtype:Message.logmsg_type) reg :unit =
    Message.log_print loglevel logtype
      (_print ((Message.is_dbg_logging_enabled logtype) || (Options.getValueOfBool "bddprint"))) reg

  (* the string converter for regions *)
  let to_string = Misc.to_string_from_printer print

    
  (* the empty region *)
  let bot = Empty

  (* the empty reached region *)
  let reached_bot = (fun () -> Union (AtomicRegionTable.create atomic_region_parameter))

  (* make a region precise *)
  (* This is the function written at Berkeley *)
  let concretize reg =
    Message.msg_string Message.Debug "In Region.concretize -- argument is:" ;
    Message.msg_printer Message.Debug print reg ;

    let concretize_data_region = function
        Abstract abst_data_reg ->
          Concrete { pred = PredTable.convertBddToPred abst_data_reg.bdd ; }
      | Concrete conc_data_reg ->
          Concrete conc_data_reg in
    let concretize_atomic_region atomic_reg =
      { location = atomic_reg.location ;
        data = concretize_data_region atomic_reg.data ;
        stack = atomic_reg.stack ; }
    in
    let concretize_atomicTI_region atomicTI_reg =
      { ti_location_prev = atomicTI_reg.ti_location_prev ;
        ti_location = atomicTI_reg.ti_location ;
        ti_data = concretize_data_region atomicTI_reg.ti_data ;
        ti_stack = atomicTI_reg.ti_stack ; }
    in
    let concretize_p_atomic_region p_atomic_reg =
      { p_control = p_atomic_reg.p_control;
	e_control = p_atomic_reg.e_control;
        p_data = concretize_data_region p_atomic_reg.p_data ;}
    in
      match reg with
          Empty ->
            Empty
        | Atomic atomic_reg ->
            Atomic (concretize_atomic_region atomic_reg)
        | AtomicTI atomicTi_reg ->
            AtomicTI (concretize_atomicTI_region atomicTi_reg)
        | Union atomic_reg_table ->
            failwith "Region.concretize: argument is a union: unimplemented"
        | UnionTI _ -> failwith "concretize: UNION TI_reg unimplemented"
	| ParallelAtomic p_atomic_reg ->
	    ParallelAtomic (concretize_p_atomic_region p_atomic_reg)
        | ParallelUnion p_atomic_reg_table ->
            failwith "Region.concretize: argument is a parallel union: unimplemented"

	      
	      
  (* This is the function written by Gilles *)
  let concretize_map f_location f_stack f_predicate reg =
    Message.msg_string Message.Debug "In Region.concretize -- argument is:" ;
    Message.msg_printer Message.Debug print reg ;
    let concretize_data_region = function
	Abstract abst_data_reg ->
          Concrete { pred = f_predicate (PredTable.convertBddToPred abst_data_reg.bdd) }
      | Concrete conc_data_reg ->
          Concrete { pred = f_predicate conc_data_reg.pred } in
    let concretize_control_region p_control =
      { p_location = f_location p_control.p_location ;
	p_stack = f_stack p_control.p_stack;
      } 
    in
    let concretize_atomic_region atomic_reg =
      { location = f_location atomic_reg.location;
        data = concretize_data_region atomic_reg.data;
        stack = f_stack atomic_reg.stack; }
    in
    let concretize_p_atomic_region p_atomic_reg =
      { p_control = concretize_control_region p_atomic_reg.p_control;
	e_control = p_atomic_reg.e_control;
        p_data = concretize_data_region p_atomic_reg.p_data ;}
    in
      match reg with
          Empty ->
            Empty
        | Atomic atomic_reg ->
            Atomic (concretize_atomic_region atomic_reg)
        | Union atomic_reg_table ->
            failwith "Region.concretize: argument is a union: unimplemented"
	| ParallelAtomic p_atomic_reg ->
	    ParallelAtomic (concretize_p_atomic_region p_atomic_reg)
	| ParallelUnion _ ->
	    failwith "Region.concretize_map: parallel union not implemented"
        | _ -> failwith "Region.concretize_map is not implemented for this type of region"


  (* returns true if reg denotes an empty set, false otherwise *)
  let is_empty reg =
    Message.msg_string Message.Debug "In Region.is_empty -- argument is: suppressed" ;
    (* Message.msg_printer Message.Debug print reg ; *)
    match reg with
        Empty ->
          true
      | Atomic atomic_reg ->
          begin
	    (*SKY beg*)
	    Message.msg_string Message.Debug "We're in Atomic, is_empty reg";
	    (*SKY end*)
            match atomic_reg.data with
                Abstract abst_data_reg -> begin 
		  (*SKY beg*)
		  Message.msg_string Message.Debug "We're in Abstract, is_empty reg";
		  Message.msg_printer Message.Debug print_abstract_data_region abst_data_reg;
		  (*SKY end*)
		  adr_is_empty abst_data_reg;
		end
              | Concrete conc_data_reg -> begin
		  (*SKY beg*)
		  Message.msg_string Message.Debug "We're in Concrete, is_empty reg";
		  (*Message.msg_string C_Command.to_string conc_data_reg.pred ;*)
		  (*SKY end*)
		  not (PredTable.isSatisfiable (PredTable.bddOne, [conc_data_reg.pred]))
		end
          end
      | AtomicTI atomicTI_reg ->
          begin
            match atomicTI_reg.ti_data with
                Abstract abst_data_reg -> adr_is_empty abst_data_reg
              | Concrete conc_data_reg ->
                  not (PredTable.isSatisfiable (PredTable.bddOne, [conc_data_reg.pred]))
          end

      | ParallelAtomic p_atomic_reg ->
	  begin
	    match p_atomic_reg.p_data with 
                Abstract abst_data_reg ->  adr_is_empty abst_data_reg
              | Concrete conc_data_reg -> not (PredTable.isSatisfiable (PredTable.bddOne, [conc_data_reg.pred]))
	  end
      | Union _ ->
          failwith "Region.is_empty: argument is a union: unimplemented"
      | UnionTI _ -> failwith "Region.is_empty: argument UNIONTI : unimp"
      | ParallelUnion _ ->
          failwith "Region.is_empty: argument is a parallel union: unimplemented"

  let leq_abstract_data_region a1 a2 adr_leq_param =
	if a1.lelement = DataLattice.bottom then true
	else Stats.time "adr_leq_param" (adr_leq_param a1) a2

  let my_leq_data_region data_reg1 data_reg2 adr_leq_param =
	match (data_reg1, data_reg2) with
            (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
              leq_abstract_data_region abst_data_reg1  abst_data_reg2 adr_leq_param
	  | _ -> failwith "my_leq_data_region: Works only for abstract data_regions"

  let my_equiv_data_region data_reg1 data_reg2 adr_eq_param =
	match (data_reg1, data_reg2) with
            (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
              adr_eq_param abst_data_reg1  abst_data_reg2
	  | _ -> failwith "my_equiv_data_region: Works only for abstract data_regions"
	    
  (* returns true if reg1 is included in reg2, false otherwise *)
  let _leq_proto adr_leq_param =
    let rec _f reg1 reg2 =
      Message.msg_string Message.Debug "In Region.leq -- arguments are: (suppressed-RJ)" ;
			(* SKY I also suppress.  That increases time to produce trace for too much *)
      (*Message.msg_printer Message.Debug print reg1 ;
      Message.msg_printer Message.Debug print reg2 ;*)
      let leq_data_region data_reg1 data_reg2 =
	match (data_reg1, data_reg2) with
            (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
	    (* Most likely, this branch works *)
	      (* TODO check lattice leq *)
              leq_abstract_data_region abst_data_reg1  abst_data_reg2 adr_leq_param
          | (Concrete conc_data_reg1, Concrete conc_data_reg2) ->
              let notreg2 = Predicate.negate conc_data_reg2.pred in
              let pr = Predicate.conjoinL [conc_data_reg1.pred; notreg2] in
                not (PredTable.isSatisfiable (PredTable.bddOne, [pr]))
          | (Abstract abst_data_reg, Concrete conc_data_reg) ->
	      let conc_of_abst_data_reg = concretize reg1 in
		_f conc_of_abst_data_reg reg2
          | (Concrete conc_data_reg, Abstract abst_data_reg) ->
	      let conc_of_abst_data_reg = concretize reg2 in
		_f reg1 conc_of_abst_data_reg
      in
	match (reg1, reg2) with
            (Empty, _) ->
              true
          | (_, Empty) ->
              (* note that (Empty, Empty) is caught above *)
              false
          | (Atomic atomic_reg1, Atomic atomic_reg2) ->
              if (((check_locations_eq atomic_reg1.location atomic_reg2.location) 
                   || (check_locations_eq atomic_reg2.location  C_SD.fake_location)) &&
                    (Options.getValueOfBool "cf" || atomic_reg2.stack = EveryStack ||
			check_stack_eq atomic_reg1.stack  atomic_reg2.stack )) 
              then
                (M.msg_string M.Debug ("Region.leq : loc_id : "^(loc_to_string_id atomic_reg1.location));
                 leq_data_region atomic_reg1.data atomic_reg2.data)
	      else
		false
          | (AtomicTI atomicTI_reg1, AtomicTI atomicTI_reg2) ->
              if (
                ((check_locations_eq atomicTI_reg1.ti_location atomicTI_reg2.ti_location) 
                 || (check_locations_eq atomicTI_reg2.ti_location  C_SD.fake_location)) 
                &&
                  ((check_locations_eq atomicTI_reg1.ti_location atomicTI_reg2.ti_location) 
                   || (check_locations_eq atomicTI_reg2.ti_location C_SD.fake_location))
                &&
                  (Options.getValueOfBool "cf" || atomicTI_reg2.ti_stack = EveryStack ||
                      check_stack_eq atomicTI_reg1.ti_stack  atomicTI_reg2.ti_stack )) 
              then
		leq_data_region atomicTI_reg1.ti_data atomicTI_reg2.ti_data
	      else
		false
		  
	  | (ParallelAtomic p_atomic_reg1, ParallelAtomic p_atomic_reg2) ->
	      (* first check if the program locations and stack match *)
	      if (((check_locations_eq p_atomic_reg1.p_control.p_location p_atomic_reg2.p_control.p_location) || 			
	             (p_atomic_reg2.p_control.p_location = C_SD.fake_location) )  &&
		    (p_atomic_reg2.p_control.p_stack = EveryStack || check_stack_eq p_atomic_reg1.p_control.p_stack p_atomic_reg2.p_control.p_stack))
	      then
		(* then check if the counter maps match *)
		if (check_environment_region_leq p_atomic_reg1.e_control p_atomic_reg2.e_control ) then
		  (* then check if the data regions are contained *)
		  leq_data_region p_atomic_reg1.p_data p_atomic_reg2.p_data
		else false
	      else
		false
          | (Atomic atomic_reg, Union atomic_reg_table) ->
	      begin
		(*SKY beg*)
		Message.msg_string Message.Debug "We're in (Atomic,Union)";
		(*SKY end*)
		if not (Options.getValueOfBool "stop-sep") then 
		  try
                    let retval = leq_data_region atomic_reg.data
                      (AtomicRegionTable.find atomic_reg_table (atomic_reg.location,atomic_reg.stack))
		    in
                      retval
		  with 
		      Out_of_memory -> failwith "ahaha"
		    | Not_found -> (Message.msg_string Message.Debug "Atregtable: failed to hit table"; is_empty reg1) 
		else
		  try
		    begin 
		      Message.msg_string Message.Debug "cpa-fix: stop sep";
		      Message.msg_string Message.Debug "At location ";
		      Message.msg_printer Message.Debug C_SD.print_location atomic_reg.location;
		      Stats.time "find" (AtomicRegionTable.find atomic_reg_table) (atomic_reg.location,atomic_reg.stack);
		      Message.msg_string Message.Debug "Found at least one region for the given location and stack";
                      (*let vals = Stats.time "find_all" (AtomicRegionTable.find_all atomic_reg_table) (atomic_reg.location,atomic_reg.stack) in*)
                        (*begin*)
                          (*Message.msg_string Message.Debug ("Size of vals in stop is "^(string_of_int (List.length vals)));*)
			  Message.msg_string Message.Debug ("Size of vals in stop is "^"suppressed PS");
                          try 
                            let ge = Stats.time "find leq" (AtomicRegionTable.find_if atomic_reg_table (Stats.time "leq_data_region" (leq_data_region atomic_reg.data))) (atomic_reg.location,atomic_reg.stack)
		            in
                              begin
                                Message.msg_string Message.Debug "region ";
                                Message.msg_printer Message.Debug my_print_data_region atomic_reg.data;
                                Message.msg_string Message.Debug " is covered by ";                                 
                                Message.msg_printer Message.Debug my_print_data_region ge;
                                true
                              end
                          with
		           Not_found -> (Message.msg_string Message.Debug "not covered"; false) 			
                        (*end*)
		    end
		  with 
		      Out_of_memory -> failwith "ohoho"
		    | Not_found -> (Message.msg_string Message.Debug "Atregtable: failed to hit table"; is_empty reg1) 			
	      end
          | (AtomicTI atomicTI_reg, UnionTI atomicTI_reg_table) -> 
              begin
		try
                  let curr = 
                    let key = (atomicTI_reg.ti_location_prev,atomicTI_reg.ti_location,atomicTI_reg.ti_stack) in
                      AtomicTIRegionTable.find atomicTI_reg_table key 
                  in
                  let retval = leq_data_region atomicTI_reg.ti_data curr in
                    retval
		with 
		    Out_of_memory -> failwith "ahaha"
		  |   Not_found -> (Message.msg_string Message.Debug "Atregtable: failed to hit table"; is_empty reg1) 
              end
          | (ParallelAtomic p_atomic_reg, ParallelUnion p_atomic_reg_table) ->
	      begin
		try
		  let dlist = AtomicRegionTable.find p_atomic_reg_table (p_atomic_reg.p_control.p_location, p_atomic_reg.p_control.p_stack) in
		  let retval = (is_empty reg1) || (List.exists 
						     (fun (ec, dr) -> (check_environment_region_leq p_atomic_reg.e_control ec) && (leq_data_region p_atomic_reg.p_data dr)) dlist) in
                    Message.msg_string Message.Debug ("Answer is "^(if retval then "true" else "false")) ;
		    retval
		with 
		    Out_of_memory -> failwith "ahaha"
		  |   Not_found -> (Message.msg_string Message.Debug "Atregtable: failed to hit table"; is_empty reg1) 
	      end 
          | (Union _, _) ->
              failwith "Region.leq: first argument is a union: unimplemented"
          | (ParallelUnion _, _) ->
              failwith "Region.leq: first argument is a parallel union: unimplemented"
	  | (Atomic _, ParallelAtomic _) -> failwith "Region.leq cannot mix atomic /parallelatomic"
	  | (ParallelAtomic _, Atomic _) -> failwith "Region.leq cannot mix parallelatomic/ atomic"
	  | _ -> failwith "Region.leq: Unknown constant"
    in _f

  let leq =
    _leq_proto adr_leq
      
  let leq_cov =
      leq
	
  (* returns true if r1 is equivalent to r2, false if not *)
  let eq reg1 reg2 = (leq reg1 reg2) && (leq reg2 reg1)

  let cup_data_region data_reg1 data_reg2 =
      match (data_reg1, data_reg2) with
          (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
	    (* SKY BUG:  If we join lattices, the requltant region is bigger than the region we want to get here for coverage checks.  Assume that we have reached lattices [a = 1   b = 2] and [a = 1  b = 3].  The join may be [a = 1] (because there's no element defined for "b is 2 or 3"), however, this join will cover the [a = 1  b = 5] element, which is covered by neither of the original nodes.
	    So, this approach should be fixed. *)
            Abstract { lelement = Stats.time "join lattice" (DataLattice.join abst_data_reg1.lelement) abst_data_reg2.lelement ;
                       bdd = Stats.time "`or` regions" (CaddieBdd.bddOr abst_data_reg1.bdd) abst_data_reg2.bdd ;
                       preds = Misc.union abst_data_reg1.preds abst_data_reg2.preds ; }
        | (Concrete conc_data_reg1, Concrete conc_data_reg2) ->
            Concrete { pred = Predicate.disjoinL [conc_data_reg1.pred;
                                                  conc_data_reg2.pred] ; }
        | (Abstract abst_data_reg, Concrete conc_data_reg)
        | (Concrete conc_data_reg, Abstract abst_data_reg) ->
            failwith "Region.cup: concrete and abstract region types mixed: unimplemented"

  let cup_lattice_in_data_region data_reg1 data_reg2 =
	match (data_reg1, data_reg2) with
            (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
	        Abstract { lelement = Stats.time "join lattice" (DataLattice.join abst_data_reg1.lelement) abst_data_reg2.lelement ;
                           bdd = abst_data_reg1.bdd;
                           preds = Misc.union abst_data_reg1.preds abst_data_reg2.preds ; }
	  | _ -> failwith "cup_lattice_in_data_region: support only abstract,abstract: unimplemented"


  let check_union reg =    
       match reg with 
	Union reg_table -> 
	    Message.msg_string Message.Debug "ok"
	| _ -> failwith "reached region is not a union"

  let cpa_merge atomic_reg_table atomic_reg mode = 
      let rec merge_list_with tochange merged_data = 
        match tochange with
	| [] -> merged_data
        | h :: tl -> begin
	  let new_merged = 
	    match mode with 
	        "bdd" -> cup_lattice_in_data_region h merged_data
	      | "location" -> cup_data_region h merged_data
	  in merge_list_with tl new_merged
	end
      in
      Message.msg_string Message.Debug "cpa-fix: At location ";
      Message.msg_printer Message.Debug C_SD.print_location atomic_reg.location;

      if  mode = "no" then begin
	let vals = Stats.time "find_all" (AtomicRegionTable.find_all atomic_reg_table) (atomic_reg.location,atomic_reg.stack) in
	Message.msg_string Message.Debug ("Size of vals in merge is "^(string_of_int (List.length vals)));
	try
	 let ge = Stats.time "find adr_leq" (List.find (fun x -> my_leq_data_region atomic_reg.data x adr_leq)) vals
	 in
	   begin
	     (* failwith "cov should be checked before merge" *)
	     Stats.time "exists leq element" (Message.msg_string Message.Debug) "Exists element which covers the node";
	     Message.msg_string Message.Debug "original region is ";
	     Message.msg_printer Message.Debug my_print_data_region atomic_reg.data;
	     Message.msg_string Message.Debug "found adr_leq data_region is ";
	     Message.msg_printer Message.Debug my_print_data_region ge;
	     (Union atomic_reg_table, Atomic atomic_reg)
	  end
	 with Not_found ->
	   begin
	     Message.msg_string Message.Debug "add new region ";
	     Message.msg_printer Message.Debug my_print_data_region atomic_reg.data;
	     Stats.time "add" (AtomicRegionTable.add atomic_reg_table (atomic_reg.location, atomic_reg.stack)) atomic_reg.data;
	     (Union atomic_reg_table, Atomic atomic_reg)
	   end
      end else begin
	(* The algorithm here is to remove all regions that match this location only or bdd and location from the hash table keeping the rest there (if we remove by locaiton only, we will thus remove all matching values).  Then the values removed are merged and the merged region is re-added back. *)
	(* Instead of find-partition-remove, we use a faster version, find_and_remove_if, which will do this in one pass over a hash bucket *)

	(* Get the function that returns the matching values only *)
	let check_if_merge = match mode with
	  | "bdd" -> (fun x -> my_equiv_data_region atomic_reg.data x adr_bdd_eq)
	  | "location" -> (fun x -> true)
	in
	let tochange = Stats.time "find_and_rm_all" (AtomicRegionTable.find_and_remove_all_if atomic_reg_table check_if_merge) (atomic_reg.location,atomic_reg.stack) in
	Message.msg_string Message.Debug (Printf.sprintf "Size of vals to merge (without unchanged) is %d" (List.length tochange));
	match tochange with
	 | [] (* no regions to change *) -> begin
	    Message.msg_string Message.Debug ("no changes, simply add new region.");
	    Stats.time "no changes, simply add new region"
	      (AtomicRegionTable.add atomic_reg_table (atomic_reg.location, atomic_reg.stack)) atomic_reg.data;
	    (Union atomic_reg_table, Atomic atomic_reg)
	  end
	 | _ (* some changes *) -> begin
	   let changed = merge_list_with tochange atomic_reg.data in
	   List.iter (fun x -> Message.msg_printer Message.Debug my_print_data_region x) tochange;
	   Message.msg_string Message.Debug ("Merged "^(string_of_int (List.length tochange))^" elements");
	   Message.msg_string Message.Debug "original region is ";
	   Message.msg_printer Message.Debug my_print_data_region atomic_reg.data;
	   Message.msg_string Message.Debug "merged data_region is ";
	   Message.msg_printer Message.Debug my_print_data_region changed;

	   (*Stats.time "remove all vals" (AtomicRegionTable.remove_all atomic_reg_table) (atomic_reg.location, atomic_reg.stack);*)
	   (*Stats.time "add unchanged vals" *)
	     (*(List.iter (fun x -> AtomicRegionTable.add atomic_reg_table (atomic_reg.location, atomic_reg.stack) x)) unchanged;*)

	   Stats.time "add changed val"
	     (AtomicRegionTable.add atomic_reg_table (atomic_reg.location, atomic_reg.stack)) changed;
	   (Union atomic_reg_table, Atomic {atomic_reg with data = changed})
	 end
       end

  let cpa_merge_reached union_reg reg mode = 
       match (union_reg, reg) with 
	 (Union reg_table, Atomic atomic_reg) -> 
		cpa_merge reg_table atomic_reg mode
	 | (Union reg_table, Empty) -> 
	 	failwith "reg is empty, only atomic supported"
	 | (Union reg_table, _) -> 
	 	failwith "unknown reg"
	 | _ -> failwith "reached region is not a union"


 (* union of two regions *)
  let cup reg1 reg2 =
    Message.msg_string Message.Debug "In Region.cup -- arguments are: args suppressed" ;
    (*Message.msg_printer Message.Debug print reg1 ;
      Message.msg_printer Message.Debug print reg2 ;*)

    let min_stack stack1 stack2 = 
      match stack1 with
	  EveryStack -> stack2
	| foo -> (match stack2 with EveryStack -> foo | bar -> failwith "Incomparable args to min_stack")
    in
      match (reg1, reg2) with
          (Empty, _) ->
            reg2
	| (_, Empty) ->
            reg1
	| (Atomic atomic_reg1, Atomic atomic_reg2) ->
            if (check_locations_eq atomic_reg1.location atomic_reg2.location)
            then
	      begin
		if (check_stack_eq atomic_reg1.stack  atomic_reg2.stack)
		then Atomic { location = atomic_reg1.location ;
			      data = cup_data_region atomic_reg1.data atomic_reg2.data ;
			      stack = atomic_reg1.stack; (* same as the other stack *)}
		else 
		  if (atomic_reg1.stack = EveryStack || atomic_reg1.stack = EveryStack)
		  then Atomic { location = atomic_reg1.location ;
				data = cup_data_region atomic_reg1.data atomic_reg2.data ;
				stack = min_stack atomic_reg1.stack atomic_reg2.stack; }
		    (* This should not be EveryStack as there is a 
		       connection between the data and stack -- 
		       so for correctness, it should be the min of the two stacks *)
		  else
		    begin
		      let newTable = (AtomicRegionTable.create atomic_region_parameter) in
			AtomicRegionTable.add newTable (atomic_reg1.location, atomic_reg1.stack) atomic_reg1.data;
			AtomicRegionTable.add newTable (atomic_reg2.location, atomic_reg2.stack) atomic_reg2.data;
			Union newTable
		    end
		      
	      end
	    else 
	      begin
		let newTable = (AtomicRegionTable.create atomic_region_parameter) in
		  AtomicRegionTable.add newTable (atomic_reg1.location, atomic_reg1.stack) atomic_reg1.data;
		  AtomicRegionTable.add newTable (atomic_reg2.location, atomic_reg2.stack) atomic_reg2.data;
		  Union newTable
	      end
	| (ParallelAtomic p_atomic_reg1, ParallelAtomic p_atomic_reg2) ->
	    let locs_equal = check_locations_eq p_atomic_reg1.p_control.p_location p_atomic_reg2.p_control.p_location
	    in
	    let stack_equal =
	      let stk1 = p_atomic_reg1.p_control.p_stack and
	          stk2 = p_atomic_reg2.p_control.p_stack in
		(check_stack_eq stk1 stk2) || stk1 = EveryStack || stk2 = EveryStack
	    in
	    let envmap_equal =
	      check_environment_region_eq p_atomic_reg1.e_control p_atomic_reg2.e_control
	    in
	      if (locs_equal && stack_equal && envmap_equal) then
		begin
		  ParallelAtomic 
		    { p_control = { p_location = p_atomic_reg1.p_control.p_location ;
				    p_stack    = min_stack p_atomic_reg1.p_control.p_stack p_atomic_reg2.p_control.p_stack ;
				  }  ;
	              e_control = p_atomic_reg1.e_control ;
	              p_data = cup_data_region p_atomic_reg1.p_data p_atomic_reg2.p_data
	            } 
		end
	      else
		begin
		  let newTable = (AtomicRegionTable.create atomic_region_parameter) in
		    AtomicRegionTable.add newTable (p_atomic_reg1.p_control.p_location, p_atomic_reg1.p_control.p_stack) [(p_atomic_reg1.e_control,p_atomic_reg1.p_data)];
		    if AtomicRegionTable.mem newTable (p_atomic_reg2.p_control.p_location, p_atomic_reg2.p_control.p_stack) then
		      AtomicRegionTable.replace newTable (p_atomic_reg2.p_control.p_location, p_atomic_reg2.p_control.p_stack) 
			((p_atomic_reg2.e_control, p_atomic_reg2.p_data) :: (AtomicRegionTable.find newTable (p_atomic_reg2.p_control.p_location, p_atomic_reg2.p_control.p_stack)))
		    else
		      AtomicRegionTable.add newTable (p_atomic_reg2.p_control.p_location, p_atomic_reg2.p_control.p_stack) [(p_atomic_reg2.e_control,p_atomic_reg2.p_data)];
		    ParallelUnion newTable			
		end
	| (Atomic atomic_reg, Union atomic_reg_table)
	| (Union atomic_reg_table, Atomic atomic_reg) ->
            begin
		if not (Options.getValueOfBool "stop-sep") then 
		  try (* JHALA: ARM.find *)
		    let data_reg_in_table = AtomicRegionTable.find atomic_reg_table (atomic_reg.location,atomic_reg.stack)
		    in
		      begin
		        AtomicRegionTable.replace atomic_reg_table (atomic_reg.location, atomic_reg.stack)
                          (cup_data_region data_reg_in_table atomic_reg.data);
		        Union atomic_reg_table
                      end
		  with Not_found ->
		     begin
		       AtomicRegionTable.add atomic_reg_table (atomic_reg.location, atomic_reg.stack) atomic_reg.data;
		       Union atomic_reg_table
		     end
		else 
		  begin		         
		    (* TODO check equality of values for equal keys *)
		    Message.msg_string Message.Debug "cpa-fix: merge in cup =no (do not merge)";
		    let (res_table, changed) = cpa_merge atomic_reg_table atomic_reg "no" 
		    in
		     res_table
		  end
            end
              
	| (AtomicTI atomicTI_reg1, AtomicTI atomicTI_reg2) ->
            if (check_locations_eq atomicTI_reg1.ti_location atomicTI_reg2.ti_location &&
		  check_locations_eq atomicTI_reg1.ti_location_prev atomicTI_reg2.ti_location_prev) 
            then
	      begin
		if (check_stack_eq atomicTI_reg1.ti_stack  atomicTI_reg2.ti_stack)
		then AtomicTI { 
                  ti_location_prev = atomicTI_reg1.ti_location_prev;
                  ti_location = atomicTI_reg1.ti_location ;
		  ti_data = cup_data_region atomicTI_reg1.ti_data atomicTI_reg2.ti_data ;
		  ti_stack = atomicTI_reg1.ti_stack; (* same as the other stack *)}
		else 
		  if (atomicTI_reg1.ti_stack = EveryStack || atomicTI_reg1.ti_stack = EveryStack)
		  then AtomicTI { 
                    ti_location_prev = atomicTI_reg1.ti_location_prev;
                    ti_location = atomicTI_reg1.ti_location ;
                    ti_data = cup_data_region atomicTI_reg1.ti_data atomicTI_reg2.ti_data ;
                    ti_stack = min_stack atomicTI_reg1.ti_stack atomicTI_reg2.ti_stack; }
		    (* This should not be EveryStack as there is a 
		       connection between the data and stack -- 
		       so for correctness, it should be the min of the two stacks *)
		  else
		    begin
		      let newTable = (AtomicTIRegionTable.create atomic_region_parameter) in
			AtomicTIRegionTable.add newTable (atomicTI_reg1.ti_location_prev, atomicTI_reg1.ti_location, atomicTI_reg1.ti_stack) atomicTI_reg1.ti_data;
			AtomicTIRegionTable.add newTable (atomicTI_reg2.ti_location_prev, atomicTI_reg2.ti_location, atomicTI_reg2.ti_stack) atomicTI_reg2.ti_data;
			UnionTI newTable
		    end
		      
	      end
	    else 
	      begin
		let newTable = (AtomicTIRegionTable.create atomic_region_parameter) in
		  AtomicTIRegionTable.add newTable (atomicTI_reg1.ti_location_prev, atomicTI_reg1.ti_location, atomicTI_reg1.ti_stack) atomicTI_reg1.ti_data;
		  AtomicTIRegionTable.add newTable (atomicTI_reg2.ti_location_prev, atomicTI_reg2.ti_location, atomicTI_reg2.ti_stack) atomicTI_reg2.ti_data;
		  UnionTI newTable
              end

		
	| (AtomicTI atomicTI_reg, UnionTI atomicTI_reg_table)
	| (UnionTI atomicTI_reg_table, AtomicTI atomicTI_reg) ->
            begin
              let key =  (atomicTI_reg.ti_location_prev,atomicTI_reg.ti_location,atomicTI_reg.ti_stack) in
		try
		  let data_reg_in_table = AtomicTIRegionTable.find atomicTI_reg_table key in 
		    begin
		      (AtomicTIRegionTable.replace atomicTI_reg_table key 
			 (cup_data_region data_reg_in_table atomicTI_reg.ti_data));
		      UnionTI atomicTI_reg_table
		    end
		with Not_found ->
		  begin
		    (AtomicTIRegionTable.add atomicTI_reg_table key atomicTI_reg.ti_data);
		    UnionTI atomicTI_reg_table
		  end
            end
	      
	| (ParallelAtomic p_atomic_reg, ParallelUnion p_atomic_reg_table)
	| (ParallelUnion p_atomic_reg_table, ParallelAtomic p_atomic_reg) ->
            begin
              try 
		let data_reg_in_table = AtomicRegionTable.find p_atomic_reg_table (p_atomic_reg.p_control.p_location, p_atomic_reg.p_control.p_stack)
		in
		  begin (* see if the list data_reg_in_table has an entry with the same e_control *)
		    let (eyes, eno) = List.partition (fun (ec,_) -> check_environment_region_eq ec p_atomic_reg.e_control) data_reg_in_table in
		      (match eyes with
			   [] -> 
			     AtomicRegionTable.replace p_atomic_reg_table (p_atomic_reg.p_control.p_location, p_atomic_reg.p_control.p_stack)
			       ((p_atomic_reg.e_control, p_atomic_reg.p_data) :: data_reg_in_table) 
			 | [(e,d)] -> 
			     (AtomicRegionTable.replace p_atomic_reg_table (p_atomic_reg.p_control.p_location, p_atomic_reg.p_control.p_stack)
				((p_atomic_reg.e_control , cup_data_region d p_atomic_reg.p_data) :: eno))
			 | _ -> failwith "Error in cup punion, patomic" );
		      ParallelUnion p_atomic_reg_table
		  end
              with Not_found -> (* raised by AtomicRegionTable.find *)
		begin
		  (AtomicRegionTable.add p_atomic_reg_table (p_atomic_reg.p_control.p_location, p_atomic_reg.p_control.p_stack) [(p_atomic_reg.e_control,p_atomic_reg.p_data)]);
		  ParallelUnion p_atomic_reg_table
		end
            end
	| (Union _, Union _) ->
            failwith "Region.cup: both arguments are unions: unimplemented"
	| (ParallelUnion _, ParallelUnion _) ->
            failwith "Region.cup: both arguments are parallel unions: unimplemented"
	| ( (ParallelAtomic _ | ParallelUnion _), (Atomic _ | Union _ ))
	| ( (Atomic _ | Union _ ), (ParallelAtomic _ | ParallelUnion _)) ->
	    failwith "Region.cup: single and parallel regions cannot be mixed" 
	| _ -> failwith "unknown pattern in Region.cup"

	    
  exception EmptyStack

  (* intersection of two regions *)
  let rec cap reg1 reg2 =
    Message.msg_string Message.Debug "In Region.cap -- arguments are: Supressed VM" ;
    (* Message.msg_printer Message.Debug print reg1 ;
    Message.msg_printer Message.Debug print reg2 ; *)
    let cap_data_region data_reg1 data_reg2 =
      match (data_reg1, data_reg2) with
          (Abstract abst_data_reg1, Abstract abst_data_reg2) ->
            Abstract { lelement = Stats.time "meet" (DataLattice.meet abst_data_reg1.lelement) abst_data_reg2.lelement ;
		       bdd = Stats.time "bddAnd" (CaddieBdd.bddAnd abst_data_reg1.bdd) abst_data_reg2.bdd ;
                       preds = Stats.time "union" (Misc.union abst_data_reg1.preds) abst_data_reg2.preds ; }
        | (Concrete conc_data_reg1, Concrete conc_data_reg2) ->
            Concrete { pred = Predicate.conjoinL [conc_data_reg1.pred;
                                                  conc_data_reg2.pred] ; }
        | (Abstract abst_data_reg, Concrete conc_data_reg)
        | (Concrete conc_data_reg, Abstract abst_data_reg) ->
            Concrete { pred = Predicate.conjoinL [conc_data_reg.pred;
                                                  PredTable.convertBddToPred abst_data_reg.bdd] ; }
    in
    let stack_cap stack1 stack2 = 
      match stack1 with 
	  CallStack s -> if (check_stack_eq stack1 stack2 || check_stack_eq stack2 EveryStack) then stack1 else raise EmptyStack
	| EveryStack -> stack2
    in
    let location_cap loc1 loc2 = 
      let (i1, i2) = C_SD.location_coords loc1 and
	  (j1, j2) = C_SD.location_coords loc2 in
      let (fake1, fake2) = C_SD.location_coords C_SD.fake_location in
	if ((i1 = fake1) && (i2 = fake2)) then loc2
	else if ((j1 = fake1) && (j2 = fake2)) then loc1 
	else if ((i1 = j1) && (i2 = j2)) then loc1 
        else raise EmptyStack
    in
      match (reg1, reg2) with
          (Empty, _) ->
            Empty
        | (_, Empty) ->
            Empty
        | (Atomic atomic_reg1, Atomic atomic_reg2) ->
            begin
	      try 
                Atomic { location = (location_cap atomic_reg1.location atomic_reg2.location);
                         data = (cap_data_region atomic_reg1.data atomic_reg2.data);
                         stack = (stack_cap atomic_reg1.stack atomic_reg2.stack); 
		       }
	      with EmptyStack -> Empty
	    end
        | (Atomic atomic_reg1, ParallelAtomic patomic_reg2)  (* this is used to intersect
                                                                the error region while model checking *)
        | (ParallelAtomic patomic_reg2, Atomic atomic_reg1) -> 
            begin
              let atomic_reg_from_patomic =
                Atomic { location = patomic_reg2.p_control.p_location ; stack = patomic_reg2.p_control.p_stack ;
	                 data = patomic_reg2.p_data ; } in
		cap atomic_reg_from_patomic reg2
            end
	| (ParallelAtomic p_atomic_reg1, ParallelAtomic p_atomic_reg2) ->
	    let environment_cap e1 e2 =
	      check_environment_region_eq e1 e2 
	    in
	      begin
		try
		  ParallelAtomic { p_control = { p_location = location_cap p_atomic_reg1.p_control.p_location p_atomic_reg2.p_control.p_location ;
						 p_stack = stack_cap p_atomic_reg1.p_control.p_stack p_atomic_reg2.p_control.p_stack ;
					       } ;
				   e_control = if environment_cap p_atomic_reg1.e_control p_atomic_reg2.e_control then 
				     p_atomic_reg1.e_control 
				   else
				     raise EmptyStack ;
				   p_data    = cap_data_region p_atomic_reg1.p_data p_atomic_reg2.p_data ;
				 } 
		with
		    EmptyStack -> Empty
	      end

	| (Atomic atomic_reg, Union atomic_reg_table)
        | (Union atomic_reg_table, Atomic atomic_reg) ->
            begin
              try (* JHALA : ARM.find *)
                let data_reg_in_table = Stats.time "find" (AtomicRegionTable.find atomic_reg_table) (atomic_reg.location,atomic_reg.stack)
                in
                  Atomic { location = atomic_reg.location ;
                           data = Stats.time "cap_data_region" (cap_data_region data_reg_in_table) atomic_reg.data ;
                           stack = atomic_reg.stack ; }
              with Not_found ->
                Empty
            end
        | (ParallelAtomic p_atomic_reg, ParallelUnion p_atomic_reg_table)
        | (ParallelUnion p_atomic_reg_table, ParallelAtomic p_atomic_reg) ->
            begin
	      failwith "this case punion patomic not implemented"
            end
        | (Union _, Union _) ->
            failwith "Region.cap: both arguments are unions: unimplemented"
        | (ParallelUnion _, ParallelUnion _) ->
            failwith "Region.cap: both arguments are unions: unimplemented"
        | ( (ParallelAtomic _ | ParallelUnion _), (Atomic _ | Union _ ))
        | ( (Atomic _ | Union _ ), (ParallelAtomic _ | ParallelUnion _)) ->
	    failwith "Region.cap: single and parallel regions cannot be mixed"
        | _ -> failwith "Unknown pattern in Region.cap"       
            
  let rename_vars r new_name =
    let do_predicate p = Predicate.alpha_convert new_name p
    in
    let do_atomic_region = function
      | Concrete c ->
          Concrete {pred = do_predicate c.pred}
      | Abstract a ->
          Concrete ({pred = do_predicate (PredTable.convertBddToPred a.bdd)})
    in
      match r with
	| Empty -> Empty
	| Atomic ar ->
            Message.msg_string Message.Debug "cpa-fix: in rename_vars Atomic";
            Atomic {ar with data = do_atomic_region ar.data}
	| Union h as r ->
            Message.msg_string Message.Debug "cpa-fix: in rename_vars Union";
            AtomicRegionTable.iter (fun k data -> AtomicRegionTable.replace h k
				      (do_atomic_region data)) h;
            r
	| AtomicTI arTI ->  AtomicTI {arTI with ti_data = do_atomic_region arTI.ti_data}
	| UnionTI h as r ->  AtomicTIRegionTable.iter (fun k data -> AtomicTIRegionTable.replace h k
							 (do_atomic_region data)) h;
            r
	| ParallelAtomic par ->
            ParallelAtomic {par with p_data = do_atomic_region par.p_data}
	| ParallelUnion p ->
            AtomicRegionTable.iter (fun k datalist -> 
				      AtomicRegionTable.replace p k (List.map (fun (e,d) -> (e,do_atomic_region d)) datalist)) p;
            r

  (* region to predicate takes a region and returns a predicate corresponding to the data region
     of the region
  *)
  let region_to_predicate r =
    let do_atomic_region = function
      | Concrete c -> c.pred
      | Abstract a -> PredTable.convertBddToPred a.bdd
    in
      match r with
	| Empty -> Predicate.False
	| Atomic ar -> do_atomic_region ar.data
	| AtomicTI arTI -> do_atomic_region arTI.ti_data
	| ParallelAtomic par -> do_atomic_region par.p_data
	| Union atomic_region_table ->
            begin
              Message.msg_string Message.Debug "cpa-fix: in region_to_predicate";
              AtomicRegionTable.fold
		( fun control data_region curr_pred -> Predicate.disjoinL [ curr_pred ;
									    (do_atomic_region data_region) ] )
		atomic_region_table
		Predicate.False
            end
	| UnionTI atomicTI_region_table ->
            begin
              AtomicTIRegionTable.fold
		( fun control data_region curr_pred -> Predicate.disjoinL [ curr_pred ;
									    (do_atomic_region data_region) ] )
		atomicTI_region_table
		Predicate.False
            end
	| ParallelUnion p_atomic_region_table ->
            begin
	      failwith "Not implemented !!"
		(*
		  AtomicRegionTable.fold
		  ( fun control data_region curr_pred -> Predicate.disjoinL [ curr_pred ;
		  (do_atomic_region data_region) ] )
		  p_atomic_region_table
		  Predicate.False
		*)
            end


  let change_location_and_stack r1 r2 =
    match (r1, r2) with 
	(Empty, _) -> Empty
      | (Atomic a1, Empty) -> Atomic a1
      | (Atomic a1, Atomic a2) -> Atomic { a1 with stack = a2.stack;
					     location = a2.location; }
      | (_,_) -> failwith "change_location_and_stack: unimplemented"


  let union_to_list reg =
    match reg with
	UnionTI atomicTI_reg_table -> 
	  let at_l = ref [] in
	  let gather  (l,l',s) d = 
	    let this_at = 
              {ti_location_prev = l; 
               ti_location = l'; 
               ti_stack = s;
               ti_data = d}
	    in 
	      at_l := (AtomicTI this_at)::!at_l
	  in
	    AtomicTIRegionTable.iter gather atomicTI_reg_table;
	    !at_l
      | _ -> failwith "unimp: union to atomic list"
	  
  exception Empty_region
  exception Early_stop

    
  let stack_to_string_id stk =
    match stk with EveryStack -> "EveryStack@@"
      | CallStack s -> String.concat ";" (List.map loc_to_string_id s)
	  
  let print_pcStack (l,s) =
    M.msg_string M.Debug (Printf.sprintf "pc: %s stack: %s : " (loc_to_string_id l) (stack_to_string_id s))
      
  let getPcStack r =
    let rv = 
      match r with
	  Atomic a -> (a.location,a.stack)
	| Empty -> raise Empty_region
	| Union _ -> failwith "Atomic region expected : got Union"
	| ParallelAtomic p -> (p.p_control.p_location, p.p_control.p_stack)
	| ParallelUnion _ -> failwith "Atomic region expected : got ParallelUnion"
        | _ -> failwith "getPcStack pattern match failure" in
    let rv' = 
      if (Options.getValueOfBool "cf") then (fst rv, EveryStack)
      else rv in
      rv'
	
	
  let get_location_id r = C_SD.location_coords (fst (getPcStack r))

  let call_depth r =  C_SD.call_depth (fst (getPcStack r))

  let get_ctrmap r =
    match r with
        ParallelAtomic p_at -> p_at.e_control
      |	_ -> Message.msg_string Message.Error "get_ctrmap : Not a parallel atomic region!" ; raise Empty_region

  (********************************* coverers data structure ********************************)
	  
  (* Our coverage lookup structure will map pc's to regions that have them.
   * This will only work with atomic regions!!
   * A faster method would be nice if we can come up with one. *)
	  
  (** Gregory: coverers will store list of nodes so as to be
      able to handle join for covering *)
  (*
    type 'a coverers = (t * ('a list)) list AtomicRegionTable.t 
    type 'a coverers = (t * (t * 'a) list) AtomicRegionTable.t
    type 'a coverers = (t * (t * ('a list)) list) AtomicRegionTable.t
  *)
  (*TODO *)
  type 'a coverers = ( (t * ((t * 'a) list)) ) list AtomicRegionTable.t
      
  let emptyCoverers () = AtomicRegionTable.create atomic_region_parameter
    
  let clearCoverers = AtomicRegionTable.clear

  let get_bdd r = 
    match r with
        Empty -> bddZero 
      | Atomic ({data = Abstract adr}) -> adr.bdd
      | _ -> bddZero  

  (* let get_bdd_union rs = 
     List.fold_left (fun b r -> CaddieBdd.bddOr b (get_bdd r)) bddZero rs
  *)
  let getPcStacks r = 
    let (l,s) = getPcStack r in
      (loc_to_string_id l)^":"^(stack_to_string_id s)

  (** TODO: Do a full printing *)
  let printCoverer cov =
    M.msg_string M.Debug "Printing cov:";
    AtomicRegionTable.iter
      (fun pcs l ->
	 List.iter
	   (fun (r, _) ->
	      print_pcStack pcs;
	      M.msg_string M.Debug (PredTable.bdd_to_string_pretty (get_bdd r))
	   ) l
      ) cov
	
  let print_cov cov = printCoverer cov
    
  let addCoverer cov reg data =
    Stats.time "addCoverer" (Message.msg_string Message.Debug) "addCov";
    try
      let pc = getPcStack reg in
	(** Gregory: should we join for coverage check? (note: work only in the
	    case of relational join) *)
      let cov_list =
	try
	  AtomicRegionTable.find cov pc
	with Not_found -> []
      in
	match Options.getValueOfString "covercheck" with
	  | "all" ->
	      begin
		match cov_list with
		  | [(creg, pl)] ->
		      AtomicRegionTable.replace cov pc [((cup creg reg), ((reg, data)::pl))]
		  | [] ->
		      AtomicRegionTable.replace cov pc [(reg, [(reg, data)])]
		  | _ ->
		      failwith "addCoverer: there are more than one element in the coverers list when covercheck is set to all"
	      end
	  | ("predicated" as cc)
	  | ("predicated-delta" as cc) ->
	      begin
		(* Find a region with the same predicate abstraction *)
		match reg with
		    Atomic { location = loc ;
			     data = Abstract { lelement = lelement ; bdd = bdd ; preds = preds } ;
			     stack = stack } ->
		      let rec join_if_same_nullary rl = match rl with
			| [] -> [(reg, [(reg,data)])]
			| ((Atomic { data = Abstract { lelement = lelement' ;
						       bdd = bdd' ;
						       preds = preds' } }) as reg', pl') :: rest ->
			    if CaddieBdd.bddEqual bdd bdd' then
			      (* for predicated-delta (TVLA emulation):
				 -> the region is left unchanged in the coverage list (reg') because
				    it has been updated in findCoverer *)
			      let reg_join = if cc = "predicated-delta" then reg' else (cup reg reg') in
				(reg_join, (reg, data) :: pl') :: rest
			    else
			      (reg', pl') :: (join_if_same_nullary rest)
		      in
			AtomicRegionTable.replace cov pc (join_if_same_nullary cov_list)
	      end
	  | "single" ->
	      AtomicRegionTable.replace cov pc ((reg, [(reg,data)])::cov_list)
	  | _ ->
	      failwith "addCoverer: Unexpected value for the option covercheck"
    with Empty_region -> Message.msg_string Message.Normal "adding empty region to cov!"
      
  (* deletes and rebuilds *)
  let deleteCoverers cov rds = 
    let pct = Hashtbl.create 251 in
    let pcst = Hashtbl.create 251 in
    let _ = 
      try
        List.iter
          (fun (r,d) -> 
             try 
               let pcs = getPcStacks r in
		 Hashtbl.add pct pcs d;
		 Hashtbl.add pcst pcs (getPcStack r) 
             with Empty_region -> ()) rds
      with ex -> (M.msg_string M.Error ("ERROR: deleteCoverers: "^(Printexc.to_string ex)); raise ex) in
    let upd pcs pc =
      try
	let cov_list = AtomicRegionTable.find cov pc in
	let dels = Misc.do_catch "deleteCoverers:cup:2-3" (Hashtbl.find_all pct) pcs in
	let cov_list' =
	  List.fold_left
	    (fun acc (join_reg, pl) ->
	       let pl' = Misc.delete_assoc_list pl dels in
		 match pl' with
		   | [] -> acc
		   | x when List.length x < List.length pl ->
		       let join_reg' = List.fold_left cup bot (List.map fst pl') in
			 (join_reg', pl') :: acc
		   | _ ->
		       (join_reg, pl) :: acc) [] cov_list
	in
	  AtomicRegionTable.replace cov pc cov_list'
      with Not_found -> ()
    in
      Hashtbl.iter upd pcst
	
  let deleteCoverer cov reg data = 
    deleteCoverers cov [(reg, data)]

  (* TODO: Check when this function is used *)
  let findCoverUnion cov reg = 
    try
      let reg_lst = List.map fst (AtomicRegionTable.find cov (getPcStack reg)) in
	List.fold_left cup bot reg_lst
    with Not_found -> bot

  let findCoverer cov reg =
    Message.msg_string Message.Debug "findCov";
    try
      let pc = Stats.time "findCoverer: getPcStack" getPcStack reg in
	try
	  let cov_list = Stats.time "findCov: Hash lookup" AtomicRegionTable.find cov pc in
	    match Options.getValueOfString "covercheck" with
		(* for predicated-delta (TVLA emulation):
		   -> the region is updated in this function instead of in addCoverer *)
	      | "predicated-delta" ->
		  begin
		    match reg with
		      | Atomic { location = loc ;
				 data = Abstract { lelement = lelement ; bdd = bdd ; preds = preds } ;
				 stack = stack } ->
			  let rec upd_if_same_nullary rl pre = match rl with
			    | [] -> None
			    | ((Atomic { data = Abstract { lelement = lelement' ; bdd = bdd' ; preds = preds' } }) as reg', datal') :: rest ->
				if CaddieBdd.bddEqual bdd bdd' then
				  (* Do the join *)
				  let (ljoin, ldelta) = DataLattice.join_and_delta lelement' lelement in
				    if ldelta = DataLattice.bottom then
				      Some (snd (List.hd datal'))
				    else
				      let reg'' =
					Atomic { location = loc ;
						 data = Abstract { lelement = ljoin ; bdd = bdd ; preds = preds } ;
						 stack = stack }
				      in
					
				      (** datal' is left unchanged since it's updated in addCoverer *)
				      let _ = AtomicRegionTable.replace cov pc (pre @ ((reg'', datal') :: rest))
				      in None
				else
				  (upd_if_same_nullary rest (pre @ [(reg', datal')]))
			  in
			    upd_if_same_nullary cov_list []
		  end
	      | _ ->
		  (* TODO: Return List.map snd (snd retval) (list of nodes instead of a single node) *)
		  let (_, (_, data) :: _) = Stats.time "findCov: find" (List.find (fun (reg', _) -> leq_cov reg reg') ) cov_list in
		    Some data
	with Not_found -> (Message.msg_string Message.Debug "found no coverer";None)
    with Empty_region -> None  (* getAnyFromCoverer cov *)

  let findExactCoverer cov reg = 
    match Options.getValueOfString "covercheck" with
      | "predicated"
      | "predicated-delta" ->
	  (* TODO: find out what findExactCoverer is actually used for *)
	  begin
	    (*Message.msg_string Message.Normal "Exact coverers & join for cover shouldn't be used together ; using non-exact coverer instead" ;*)
	    findCoverer cov reg
	  end
      | _ ->
	  begin
	    Message.msg_string Message.Debug "findExactCov";
	    let pc = getPcStack reg in
	      (* let dloc = C_SD.is_dispatch_loc (fst pc) in
		 if dloc && O.getValueOfBool "events" then findCoverer cov reg else *)
              try
		let cov_list = AtomicRegionTable.find cov pc in
		let merged_lists = List.flatten (List.map snd cov_list) in
		let _, data = Stats.time "findCov: find" (List.find (fun (reg', _) -> eq reg reg') ) merged_lists in
		  Some data
              with
		| Not_found -> (M.msg_string M.Debug (Printf.sprintf "found no exact coverer" );None)
		| Empty_region -> None  (* getAnyFromCoverer cov *) 
	  end
	    
  (* TODO: incompatible with predicated(-delta) cover check *)
  let findAllCoverer cov reg = 
    try
      let pc = getPcStack reg in
	try
	  let cov_list = AtomicRegionTable.find cov pc in
	    List.map snd (List.flatten (List.map snd (List.filter (fun (reg', _) -> leq_cov reg reg') cov_list)))
	with Not_found -> (Message.msg_string Message.Debug "found no coverer at all";[])
    with Empty_region ->  [] (* getAnyFromCoverer cov *)
      
  (* TODO: incompatible with predicated(-delta) cover check *)
  let findAllExactCoverer cov reg =
    try
      let pc = getPcStack reg in
	try
	  let cov_list = AtomicRegionTable.find cov pc in
	  let merged_lists = List.flatten (List.map snd cov_list) in
	    List.map snd (List.filter (fun (reg', _) -> eq reg reg') merged_lists)
	with Not_found -> (Message.msg_string Message.Debug "found no exact coverer at all";[])
    with Empty_region ->  [] (* getAnyFromCoverer cov *)


  let findCoverCandidates cov reg = 
    try
      let pc = getPcStack reg in
	try
	  let cov_list = AtomicRegionTable.find cov pc in
	    List.map snd (List.flatten (List.map snd cov_list))
	with Not_found -> (Message.msg_string Message.Debug "found no cov candidates ";[])
    with Empty_region ->  [] (* getAnyFromCoverer cov *)

  let findCoverCandidatesAtLoc cov reg =
    try
      let pc = getPcStack reg in
	try
	  let cov_list = AtomicRegionTable.find cov pc in
	    List.flatten (List.map snd cov_list)
	with Not_found -> []
    with Empty_region -> []
  (************************************************************************************************)
      
  (************************************************************************************************)

  let project_par_atomic r = 
    let quantify_useless reg =
      Message.msg_string Message.Debug "In quantify_useless";
      Message.msg_printer Message.Debug print reg;
      
      if Options.getValueOfInt "craig" < 2 then reg
      else 
	match reg with
	    Atomic {location = loc;
		    data = Abstract abst_data_reg;
		    stack = stk
		   } -> 
	      begin
		let new_adr = 
		  let l_id = C_SD.location_coords loc in
		  let keep_preds =
		    try Hashtbl.find PredTable.loc_pred_table l_id with Not_found -> []
		  in
		  let keep_preds = 
		    (if (Options.getValueOfInt "craig" >= 2) 
		     then
		       !PredTable.globally_useful_preds
		     else [] ) @ keep_preds 
		      (* else: the stuff is already there .. *)
		  in
                  let new_bdd = 
                    let quant_fn = fun i -> not (List.mem i keep_preds) in 
                    let _preds_in_bdd = PredTable.bdd_support abst_data_reg.bdd in
                    let _preds_to_quantify = List.filter quant_fn _preds_in_bdd in
                      bdd_exist_quantify (abst_data_reg.bdd) _preds_to_quantify
                        (* BDDFIX: let _varset = CaddieBdd.fromList bddManager _preds_to_quantify in 
                           CaddieBdd.exists _varset abst_data_reg.bdd *)
                  in 
		    (*let _ = Message.msg_string Message.Normal "Leaving q-u" in *)
		    { 
                      lelement = abst_data_reg.lelement ;
                      bdd = new_bdd;
		      preds = []
		    }
		in 
		  Atomic {location = loc;
			  data = Abstract new_adr;
			  stack = stk
			 }
		    
	      end
	  | _ -> failwith "unpleasant region sent to quantify_useless"
    in
      match r with
	|  Empty -> Empty
	|  ParallelAtomic par_atm_reg ->
	     quantify_useless 
	       (Atomic {
		  location = par_atm_reg.p_control.p_location;
		  data = par_atm_reg.p_data;
		  stack = par_atm_reg.p_control.p_stack;
		})
	| Atomic _ -> quantify_useless r
	| _ -> failwith ("unimplemented in project_par_atomic "^(to_string r))
	    

  (* reg_lastTarget : a civilized way to do lastTarget using present reg as an argument,
     instead of that funny pointer which is just not on! -- but of course in some cases we 
     dont have the region. *)

  let reg_lastTarget reg op = 
    let (_,stack) = getPcStack (reg) in
      match stack with
	  EveryStack -> failwith "Everystack in top_of_stack"
	| CallStack [] -> 
	    begin
	      Message.msg_string Message.Debug 
		"reg_lastTarget called with errorLocation  as top of stack !"; 
	      ( C_SD.junkRet, [], C_SD.get_target op)
	    end
	| CallStack (callLoc::_) -> 
	    let outEdges = C_SD.get_outgoing_edges callLoc in
	    let _ = if (List.length outEdges <> 1) then 
	      (Message.msg_string Message.Error "Call point with bad successor !!") 
	    in
	    let callEdge = (List.hd outEdges) in
	    let fcallExp = 
	      match (C_SD.get_command callEdge) with	
		  {C_SD.Command.code = C_SD.Command.FunctionCall fcexp} -> fcexp 
		| _ -> failwith "bad command at funcall point !"
	    in
	    let (fname,subs,target) = C_SD.deconstructFunCall fcallExp in
	    let retLoc = C_SD.get_target (List.hd outEdges) in
	      (target, subs, retLoc)
		
  let update_lattice_in_region reg reached_reg =
    if (Options.getValueOfBool "nolattice") then reg
    else
      match reg with
	  Atomic atomic_reg ->
	    begin
	      let r = 
		match atomic_reg.data with
		    Abstract adr ->
		      Atomic { data = Abstract { lelement = DataLattice.top ; bdd = adr.bdd; preds = adr.preds; } ;
			       location = atomic_reg.location ;
			       stack = atomic_reg.stack ;
			     }  
		  | _ -> failwith "update lattice_in_region: bad call: region is not Abstract Atomic!"
	      in
		Stats.time "cap" (cap r) reached_reg
	    end
	| Empty -> Empty
	| _ -> failwith "bad call to update_lattice_in_region"  


  (* TODO DZ ?and_delta? *)
  let update_lattice_in_region_coverers_and_delta reg coverers =
    if Options.getValueOfBool "lattice-no-join" then
      (reg, reg)
    else
      match reg with
	  Atomic atomic_reg ->
	    begin
	      match atomic_reg.data with
	          Abstract adr ->
		    let cov_list  = findCoverCandidatesAtLoc coverers reg in
                      (* find all nodes with the same location and take the join of all the
			 lattice elements 
		      *)
		    let get_lattice r =
                      match r with 
			  Empty -> DataLattice.bottom
			| Atomic areg ->
			    (match areg.data with Abstract adr -> adr.lelement | _ -> DataLattice.bottom)
			| _ -> DataLattice.bottom
		    in
		    let get_bdd r =
		      match r with
			  Empty -> bddZero
			| Atomic areg -> (match areg.data with Abstract adr -> adr.bdd | _ -> bddOne)
			| _ -> bddOne
		    in
		    let join_existing = 
		      (*
			List.fold_left
			(if Options.getValueOfBool "lattice-join-all" then
			(fun sofar thisone -> lattice_join_only (get_lattice (fst thisone)) sofar)
			else
			(fun sofar thisone -> 
			if (CaddieBdd.bddEqual (get_bdd (fst thisone) ) adr.bdd) then
			lattice_join_only (get_lattice (fst thisone)) sofar
			else
			sofar))
			DataLattice.bottom cov_list in
		      *)
		      try
			get_lattice (fst (List.find
					    (if Options.getValueOfBool "lattice-join-all" then
					       (fun x -> true)
					     else
					       (fun x -> (CaddieBdd.bddEqual (get_bdd (fst x) ) adr.bdd)))
					    cov_list))
		      with Not_found -> DataLattice.bottom in

		    let (join_all, join_delta) = DataLattice.join_and_delta join_existing adr.lelement in
		    let (jcov,jcont) =
		      (join_all, join_all)
		    in
		      (Atomic { data = Abstract { lelement = jcov ; bdd = adr.bdd ; preds = adr.preds; } ;
				location = atomic_reg.location; 
				stack = atomic_reg.stack ;
			      },
		       Atomic { data = Abstract { lelement = jcont ; bdd = adr.bdd ; preds = adr.preds; } ;
				location = atomic_reg.location; 
				stack = atomic_reg.stack ;
			      })
		| _ -> failwith "update lattice_in_region_coverers: bad call: region is not Abstract Atomic!"
	    end
	| Empty -> (Empty, Empty)
	| _ -> failwith "bad call to update_lattice_in_region_coverers"  
	    
  let update_lattice_in_region_coverers reg coverers =
    fst ( update_lattice_in_region_coverers_and_delta reg coverers )

  type restriction = Symbol.symbol -> bool

  let restrictAbstractData f abstract_data_region =
    let preds = abstract_data_region.preds in
    let allPreds = Hashtbl.fold (fun _ p l -> p::l) hashTableOfPreds [] in
      (*print_string ("Restricting " ^ string_of_int (List.length preds) ^ " preds\n");*)
    let outOfScope pred =
      let pr = getPred pred in
	(* let vars = Predicate.allVarExps_deep pr in *)
      let syms = Predicate.varsOfPred pr in
	(* let isOut = function
	   Expression.Symbol sym -> f sym
	   | l -> failwith ("Unexpected lval in restrict_predicate"^(Expression.lvalToString l))
	   in *)
	List.exists f syms (* isOut vars *) in
    let (dropPreds, keepPreds) = List.partition outOfScope allPreds in
    let bdd = bdd_exist_quantify (abstract_data_region.bdd) dropPreds in
      (* BDDFIX: CaddieBdd.exists (CaddieBdd.fromList bddManager
	 dropPreds) abstract_data_region.bdd *)
      {lelement = abstract_data_region.lelement ; preds = preds; bdd = bdd}
	
  let restrict f region =
    match region with
	Empty -> Empty
      |	Atomic ({data = Abstract adr} as atomic) -> Atomic {atomic with data =
	    Abstract (restrictAbstractData f adr)}                                                      
      |	_ -> failwith "Region.restrict called on a region that is not atomic or not abstract"

  let negate_restriction r sym = not (r sym)

  let locals_and_globals fname sym =
    try
      let idx = String.index sym '@' in
	fname <> String.sub sym (idx+1) (String.length sym - idx-1)
    with Not_found -> false
      
  let only_globals sym =
    try
      let idx = String.index sym '@' in
	true
    with Not_found -> false

  let only_locals fname sym =
    try
      let idx = String.index sym '@' in
	fname <> String.sub sym (idx+1) (String.length sym - idx-1)
    with Not_found -> true

  let advance_location reg =
    match reg with
	Atomic ({location = loc} as atomic) ->
	  (match C_SD.get_outgoing_edges loc with
	       [skip] -> Atomic {atomic with location = C_SD.get_target skip}
	     | _ -> failwith "Location has zero or more than one outgoing edges in advance_location")
      | _ -> failwith "advance_location called on non-Atomic region"

end


let extract_pred reg = 
  match reg with
      Region.Atomic r -> 
	begin
	  match r.Region.data with 
	      Region.Concrete x -> x.Region.pred 
	    | Region.Abstract a -> PredTable.convertBddToPred a.Region.bdd
	end
    |  Region.AtomicTI r -> 
	 begin
	   match r.Region.ti_data with 
	       Region.Concrete x -> x.Region.pred 
	     | Region.Abstract a -> PredTable.convertBddToPred a.Region.bdd
	 end     
    | Region.Empty -> Predicate.False
    | _ -> (Message.msg_string Message.Error ("Bad call to extract_pred "^(Region.to_string reg)); failwith ("Bad call to extract pred"))

let extract_abs_data_reg reg = 
  match reg with
      Region.Empty ->
	{ Region.lelement = DataLattice.bottom ; Region.bdd = PredTable.bddZero ; Region.preds = [] }
    |  Region.Atomic atomic_reg ->
	 begin
	   match atomic_reg.Region.data with
	       Region.Abstract ar -> ar
	     | _ -> failwith "conc_dr given to extract_abs_data_reg"
	 end
    |  Region.AtomicTI atomicTI_reg ->
	 begin
	   match atomicTI_reg.Region.ti_data with
	       Region.Abstract ar -> ar
	     | _ -> failwith "conc_dr given to extract_abs_data_reg"
	 end
	   
    | Region.ParallelAtomic p_atomic_reg ->
	begin
	  match p_atomic_reg.Region.p_data with
	      Region.Abstract ar -> ar
	end
    | _ -> failwith "union region given to extract_abs_data_reg"

let replace_abs_data_reg reg areg = 
  match reg with
      Region.Empty -> 
	reg
    |  Region.Atomic { Region.location = l ;
		       Region.data = d ;
		       Region.stack = s } ->
	 begin
	   match d with
	       Region.Abstract ar -> Region.Atomic { Region.location = l ;
						     Region.data = Region.Abstract areg ; 
						     Region.stack = s }
	     | _ -> failwith "conc_dr given to replace_abs_data_reg"
	 end
    |  Region.AtomicTI { Region.ti_location_prev = p ;
			 Region.ti_location = l ;
			 Region.ti_data = d ;
			 Region.ti_stack = s } ->
	 begin
	   match d with
	       Region.Abstract ar -> Region.AtomicTI { Region.ti_location_prev = p ;
						       Region.ti_location = l ;
						       Region.ti_data = Region.Abstract areg ;
						       Region.ti_stack = s }
	     | _ -> failwith "conc_dr given to extract_abs_data_reg"
	 end
	   
    | Region.ParallelAtomic { Region.p_control = c ;
			      Region.e_control = e ;
			      Region.p_data = d } ->
	begin
	  match d with
	      Region.Abstract ar -> Region.ParallelAtomic { Region.p_control = c ;
							    Region.e_control = e ;
							    Region.p_data = Region.Abstract areg }
	end
    | _ -> failwith "union region given to extract_abs_data_reg"

let is_x_adr f reg = 
  let adr = extract_abs_data_reg reg in
    f adr

let is_true_adr reg =
  let f adr = CaddieBdd.bddEqual (adr.Region.bdd) PredTable.bddOne in
    is_x_adr f reg

let is_false_adr reg =
  let f adr = CaddieBdd.bddEqual (adr.Region.bdd) PredTable.bddZero in
    is_x_adr f reg


      
let extract_data_reg reg = 
  match reg with
      Region.Atomic atomic_reg -> atomic_reg.Region.data
    | _ -> failwith "Non atomic reg in extract_data_reg!"

        
let insert_data_reg reg dr = 
  match reg with
      Region.Atomic atomic_reg ->
	begin
	  Region.Atomic 
	    { Region.data = dr;
	      Region.location = atomic_reg.Region.location;
	      Region.stack = atomic_reg.Region.stack;
	    }
	end
    | _ -> failwith "bad call to insert_data_reg"

	
let insert_bdd_reg reg bdd =
  match reg with
      Region.Atomic ({Region.data = Region.Abstract d} as at) ->
	let d_new = {d with Region.bdd = bdd} in
	  Region.Atomic {at with Region.data = Region.Abstract d_new}
    | _ -> failwith "Bad region in insert_bdd_reg"
	
(*
  module AR = AccessRegion.Make(Region)
  type 'a phi_table = (Ast.Symbol.symbol, 'a AR.t ref) Hashtbl.t 
*)
type phi_table = (Ast.Expression.lval, Region.t ref) Hashtbl.t 

let assumed_invariants_region = 
  ref (Region.Atomic { Region.location = C_SD.fake_location ;
		       Region.data = Region.Concrete { Region.pred = Predicate.True; } ;
		       Region.stack = Region.EveryStack ; })

let assumed_invariants_list_to_region () =
  let data = Region.Concrete { Region.pred = Predicate.conjoinL !assumed_invariants ; } in
    Region.Atomic { Region.location = C_SD.fake_location ;
                    Region.data = data ;
                    Region.stack = Region.EveryStack ; }


let rec trim_predicate_to_globals = function
  | Predicate.True -> Predicate.True
  | Predicate.False -> Predicate.False
  | Predicate.And preds -> Predicate.And (List.map trim_predicate_to_globals preds)
  | Predicate.Or preds -> failwith "Abstraction.trim_predicate_to_globals: Predicate.Or"
  | Predicate.Not (Predicate.Atom e) as pred -> if expression_mentions_locals e then Predicate.True else pred
  | Predicate.Not _ -> failwith "Abstraction.trim_predicate_to_globals: Predicate.Not"
  | Predicate.Implies (p, q) -> failwith "Abstraction.trim_predicate_to_globals: Predicate.Implies"
  | Predicate.Iff (p, q) -> failwith "Abstraction.trim_predicate_to_globals: Predicate.Iff"
  | Predicate.All (sym, pred) -> failwith "Abstraction.trim_predicate_to_globals: Predicate.All"
  | Predicate.Exist (sym, pred) -> failwith "Abstraction.trim_predicate_to_globals: Predicate.Exists"
      (*
	| Predicate.All (sym, pred) -> Predicate.All (sym, trim_predicate_to_globals pred)
	| Predicate.Exist (sym, pred) -> Predicate.Exist (sym, trim_predicate_to_globals pred)
      *)
  | Predicate.Atom e as pred -> if expression_mentions_locals e then Predicate.True else pred

let trim_region_to_globals =    
  Region.concretize_map
    (fun l -> C_SD.fake_location) (fun s -> Region.EveryStack)
    (fun p -> Message.msg_string Message.Debug ("trim_region_to_globals:" ^ 
						  Predicate.toString p); 
       (* trim_predicate_to_globals*) p)

let erase_data = function
  | Region.Empty -> Region.Empty
  | Region.Atomic a ->
      let new_data = match a.Region.data with
	| Region.Concrete _ -> Region.Concrete {Region.pred = Predicate.True}
	| Region.Abstract adr ->
	    Region.Abstract {adr with Region.bdd = bddOne}
      in
	Region.Atomic {a with Region.data = new_data}
  | Region.ParallelAtomic a ->
      let new_data = match a.Region.p_data with
	| Region.Concrete _ -> Region.Concrete {Region.pred = Predicate.True}
	| Region.Abstract adr ->
	    Region.Abstract {adr with Region.bdd = bddOne }
      in
	Region.ParallelAtomic {a with Region.p_data = new_data}
  | Region.Union _ -> failwith "Abstraction.erase_data"
  | Region.ParallelUnion _ -> failwith "Abstraction.erase_data parallel union unimplemented"
  | _ -> failwith "erase_data pattern match fail"
      
(* this takes a predicate formula and returns a region out of it *)
let predicate_to_region p = 
  Region.Atomic 
    { Region.location = C_SD.fake_location;
      Region.data = Region.Concrete { Region.pred = p} ;
      Region.stack = Region.EveryStack;
    }


let create_concrete_true_region edge =
  Region.Atomic 
    { Region.location = (C_SD.get_target edge) ;
      Region.data = Region.Concrete { Region.pred = Predicate.True } ;
      Region.stack = Region.EveryStack
    } 

let create_abstract_true_region edge =
  Region.Atomic 
    { Region.location = (C_SD.get_target edge) ;
      Region.data = Region.Abstract { Region.bdd = PredTable.bddOne ; 
                                      Region.lelement = DataLattice.top ;
				      Region.preds = List.map Misc.fst3 (!PredTable.tableOfPreds)
				    } ;
      Region.stack = Region.EveryStack
    } 
    
let negate_data_reg dr =
  match dr with
      Region.Concrete cdr -> Region.Concrete {Region.pred = Predicate.negate (cdr.Region.pred)}
    |   Region.Abstract adr -> Region.Abstract {adr with Region.bdd = CaddieBdd.bddNot adr.Region.bdd }

let negate_region reg = 
  match reg with
      Region.Atomic atr -> Region.Atomic {atr with Region.data = negate_data_reg (atr.Region.data)}
    |   Region.AtomicTI atrTI -> Region.AtomicTI {atrTI with Region.ti_data = negate_data_reg (atrTI.Region.ti_data)}
    |   _ -> failwith ("Non-atomic region in negate-region!"^(Region.to_string reg))

	  
(* Accelerate and focus *)

(* the precise function required by the ABSTRACTION signature *)
let precise = Region.concretize (* GILLES stuff : Region.concretize_map (fun l -> l) (fun s -> s) (fun p -> p) *)


let check_emptiness op =
  if (Options.getValueOfString "refine" = "fwd") then
    C_SD.isPredicate op
  else
    match (C_SD.get_command op).C_SD.Command.code with
	C_SD.Command.Pred Predicate.True 
      | C_SD.Command.Skip 
      | C_SD.Command.Block [] -> false
      | _ -> true
	  

(* check if cover check will be useful *)
let cover_check_useful reg =
  if (Options.getValueOfString "cover" = "join") then
    (* return true if reg is a join point *)
    begin
      match reg with
          Region.Empty -> true
        | Region.Atomic ar -> 
            begin (* check coverability only if more than one edge comes into this location *)
              let incoming_edges =  C_SD.get_ingoing_edges ar.Region.location in
		(List.length (incoming_edges) > 1) ||
		  List.mem ar.Region.location (List.map (C_SD.get_source) incoming_edges) (* to handle things like ERROR: goto ERROR; *)
            end
        | _ -> true
    end
  else
    true



      
(* returns true if reg1 is covered by reg2, false otherwise *)
let covered reg1 reg2 =
  (* This is currently buggy...  Let's just call leq for now *)
  Region.leq reg1 reg2
    

let coverable reg =
  match reg with
      Region.Empty -> true
    | Region.Atomic a -> 
        begin
          let co = Options.getValueOfString "cover" in
            match co with
		"join" -> C_SD.is_join a.Region.location
              | "loop" -> C_SD.is_loopback a.Region.location
              | _ -> true
        end
    | _ -> false


let location_coords_of_region reg =
  match reg with
      Region.Atomic _reg ->  (_reg.Region.location)
    | Region.ParallelAtomic _reg -> _reg.Region.p_control.Region.p_location
    | _ -> failwith ("bad call to location_coords_of_region:"^(Region.to_string reg))

let lattice_of_region reg = 
  match reg with 
      Region.Atomic ({Region.data = Region.Abstract adr}) -> (adr.Region.lelement) 
    | _ -> failwith "bad call to lattice_of_region" 
	
(***************** Transition Invariant Stuff (for Andrey Rybalchenko) *****************) 

let dump_ti_closure r =
  let _ = Message.msg_string Message.Normal "DONE COMPUTING TI" in
  let ctr = ref 0 in
  let inc_ctr () = ctr := !ctr + 1; !ctr in
    (* write out Sicstus Prolog for the constraints for Andrey's magic *)
  let convertToSicstusProlog p = 
    let rec convertPred p =
      Message.msg_string Message.Debug ("conv-pred-sic: "^(Predicate.toString p));
      match p with 
          Predicate.True -> "1=1"
	| Predicate.False -> "1=0"
	| Predicate.Atom at -> convertExp at
	| Predicate.And plist -> List.fold_left (fun sofar thisone -> (convertPred thisone) ^ "," ^ sofar) "]" plist
	| Predicate.Not p' -> 
            begin
              let negp' = Predicate.negate p' in
		match negp' with
		    Predicate.Not _ -> failwith "negation!!"
		  | _ -> convertPred negp'
            end
    and convertExp e =
      Message.msg_string Message.Debug ("conv-exp-sic: "^(Expression.toString e));

      match e with
          Expression.Constant (Constant.Int i) -> string_of_int i
	| Expression.Lval l -> convertLval l
	| Expression.Binary (op, e1, e2) -> 
            begin
              let se1 = convertExp e1 and se2 = convertExp e2 in 
              let ops = match op with
		  Expression.Eq -> " = "
		| Expression.Ne -> " =\\= "
		| Expression.Ge -> " >= "
		| Expression.Gt -> " > "
		| Expression.Le -> " =< "
		| Expression.Lt -> " < "
		| Expression.Plus -> " + "
		| Expression.Minus -> " - "
		| _ -> failwith ("convertToSicstusProlog: This expression (binary op) not handled: " ^ (Expression.toString e))
              in
		se1 ^ ops ^ se2
            end
	| Expression.Unary (op, e1) -> 
            begin
	      let ops = match op with Expression.UnaryMinus -> " - " | Expression.UnaryPlus -> " + " 
	        | _ -> failwith ("convertToSicstusProlog: This expression (unary op) not handled: " ^ (Expression.toString e))
              in
		ops ^ (convertExp e1)
            end
	| _ -> failwith ("convertToSicstusProlog: This expression not handled: " ^ (Expression.toString e))
    and convertLval l =
      match l with
          Expression.Symbol s -> let _s = "_"^s in _s
	| _ -> failwith ("convertToSicstusProlog: This lval not handled: "^(Expression.lvalToString l)) 
    in     
      
    let (allsymlvals, alllvals) = List.partition (fun s -> match s with l -> Expression.is_symbolic l) (Predicate.lvals_of_predicate p) 
    in
    let all_lvals = Misc.sort_and_compact (alllvals @ (List.map Expression.peel_symbolic allsymlvals)) in
    let all_scopes = Misc.sort_and_compact (List.flatten (List.map (C_SD.scope_of_lval) all_lvals)) in
    let scope = 
      try List.hd ( List.filter (fun s -> s <> "") all_scopes)
      with _ -> "main" 
    in 
    let all_sym_lvals = List.map (Expression.make_symvar scope) all_lvals in
    let all_sym_lvs_string = Printf.sprintf "( %s )" 
      (String.concat "," (List.map convertLval all_sym_lvals))
    in
    let all_lvs_string = Printf.sprintf "( %s )" 
      (String.concat "," (List.map convertLval all_lvals))
    in
    let ps = convertPred p in
      ( Printf.sprintf "r( %s , %s , %s , %d)." all_sym_lvs_string all_lvs_string ps (inc_ctr ()))
  in
  let all_str_l = ref [] in 
  let print_sic_string ss =
    Message.msg_string Message.Normal ">>>>>>>>>>>>>Sicstus>>>>>>>>>>>>>";
    Message.msg_string Message.Normal ss;
    Message.msg_string Message.Normal ">>>>>>>>>>>>>endSicstus>>>>>>>>>>>>>";
    all_str_l := ss::!all_str_l
  in
    
  let dumpfun atomic_ti_reg = 
    let l = atomic_ti_reg.Region.ti_location_prev in
    let l' = atomic_ti_reg.Region.ti_location in
    let _ = atomic_ti_reg.Region.ti_stack in
    let datareg = atomic_ti_reg.Region.ti_data in
      if (C_SD.location_coords l = C_SD.location_coords l') then 
	let concretedatareg  =
          match datareg with
              Region.Concrete x -> x.Region.pred
            | Region.Abstract a -> PredTable.convertBddToPred a.Region.bdd
	in
	let dnfForm = Predicate.convertDNF concretedatareg in
	  (* for each conjunct, write out sicstus prolog *)
	let _ = 
          Message.msg_string Message.Normal ("TI Constraint: "^(Predicate.toString dnfForm))
	in
	  match dnfForm with
              Predicate.Or orlist -> List.iter (fun s -> let ss = convertToSicstusProlog s in print_sic_string ss ) orlist 
	    | Predicate.True -> print_sic_string "1=1"
	    | Predicate.False -> print_sic_string "1=0"
	    | Predicate.Atom _ -> 
		let s = convertToSicstusProlog dnfForm in
		  print_sic_string s
	    | Predicate.And _ ->  let ss = convertToSicstusProlog dnfForm in print_sic_string ss 
	    | _ -> failwith ("bad predicate in dump etc: "^(Predicate.toString dnfForm))
      else ()
  in
  let atTI_reg_list = Region.union_to_list r in
  let dump r' = 
    match r' with
	Region.AtomicTI atTI -> dumpfun atTI
      | _ -> failwith "bad result from union_to_list!"
  in
    List.iter dump atTI_reg_list;
    Misc.write_to_file "transinv.pl" (String.concat "\n\n" !all_str_l)
