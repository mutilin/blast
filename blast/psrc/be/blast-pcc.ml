(*  BLAST is a tool for software model checking.
This file is part of BLAST.

 Copyright 2009-2011 Institute for System Programming of Russian
                     Academy of Sciences.
 Copyright 2002-2008 The BLAST Team.

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Refer to README file for the full list of maintainers and contributors.

*)
(********************************************************************************)
(* Jhala: Adding blast-proof.ml stuff over here ... 
   we'll let Gregoire fuss with the modules etc *)
(***** BLAST-PROOF *****)
(* module BlastProof =
   struct *)
  (* Data Structures for Proofs *)  
   type  data_literal = (int * bool) 
   type  data_cube =  data_literal list 
   type   data_region =  data_cube list
   
   type atomic_proof_region = {
	   prog_counter             : C_SD.location;
	   call_stack               : Region.stack_contents;
	   data_reg_cubes           :  data_region;
          (* The above is the list of cubes --before-- we turned it into bdds the invariant on the proof tree is that the two data regs should in fact be identical logically. We shall check this when constructing the proof -- indeed, it should be possible to do the model checking only on these lists *)
     (*data_reg_preds           : int list;  list of relevant predicates -- not needed, due to the check that we shall perform when making the proof *)
     (* data_reg_bdd             : CaddieBdd.bdd ; should come from bddfying the data_reg_preds *)
	 }
   type  proof_region = 
       Empty_pr
     | Atomic_pr of atomic_proof_region
	 
   type lemma_index = int 
   (* The proof tree will have edges labelled with "lemmas" and a separate (lemma_index, blast_lemma) Hashtbl *)
   type vampyre_lemma_index =  int 
   type cube_post_lemma = 
       Empty_post of lemma_index
     | Nonempty_post of ( data_literal * vampyre_lemma_index) list

   type blast_lemma = 
     Post_Edge_Lemma of ( data_cube * cube_post_lemma) list
(* [(cube,[...(l,prf)...])...] : prf is a proof of cube -> wp(l,edge) *)
(* proofs labelling the edges of the tree *)
(* Note that for post of assume, the assume pred is not going to be in the hyp: data_cube as given above -- that must come from the op labelling the edge (PROOF-CHECKER)*)
     | Covering_Lemma (* TBD: Still have to figure out the covering *)
     | Skip_Lemma (* for SKIPs and so on whenever the post is the same *)





(* This is the (lemma_index,blast_lemma) table *) 
let lemmaTable = Hashtbl.create 53
let lemma_counter = ref 0 

let insert_lemma lem = 
  (* Message.msg_string Message.Normal "Adding to lemmaTable";*)
  let lc = !lemma_counter in
    Hashtbl.add lemmaTable lc lem;
    lemma_counter := (!lemma_counter)+1;
    lc

let skip_lemma_index = insert_lemma Skip_Lemma (* 0 is the default skip lemma *)

let clear_lemma_table () = 
  Hashtbl.clear lemmaTable; lemma_counter := 0


let print_proofTable () = 
  Message.msg_string Message.Normal ("Total number of lemmas: "^(string_of_int !lemma_counter)^"\n");
  Message.msg_string Message.Normal ("Total number of vampyre obligations: "^(string_of_int !vampyre_lemma_counter)^"\n");
  Message.msg_string Message.Normal ("Total number of REGION MISMATCHES: "^(string_of_int !statTotalMismatches)^"\n");
 

(*  let table_chan = open_out_bin "foo.table" in
    Marshal.to_channel table_chan lemmaTable [] ;
    close_out table_chan; *)
    (*close_out (!VampyreErrormsg.proofChannel);*)
    ()

(* Need the following functions ... *)
   (* done ..print_proof_region : *)
   (* done ..check_reg_eq : proof_region -> Region.t -> bool *)
   (* done ..extract_proof_region : Region.t -> proof_region *)

   let print_proof_region preg_formal =
     let print_loc loc = 
       print_string ((C_SD.location_to_string loc )^";") 
     in
     let _ = print_string "Proof Region: \n" in
       match preg_formal with
	   Empty_pr -> print_string "Empty \n"
	 | Atomic_pr (preg) -> 
	     begin
	       let _ = print_string ("Location: "^(C_SD.location_to_string preg.prog_counter)^"\n"); print_string "Stack : [ " in
	       let _ = 
		 match preg.call_stack with
		     Region.EveryStack -> print_string "EveryStack"
		   | Region.CallStack s -> List.iter print_loc s 
	       in
		 print_string "] \n";
		 let print_literal (i,b) = 
		   match b with
		       true -> print_string (Predicate.toString (PredTable.getPred i));print_string "."
		     | false -> print_string "~"; print_string (Predicate.toString (PredTable.getPred i)); print_string "."
		 in
		 let print_cube l = print_string "+ "; List.iter print_literal l; print_string "\n" 
		 in
		   List.iter print_cube preg.data_reg_cubes;
		   print_string "End proof region \n"
	     end

   exception DifferentRegions 

   let make_data_region_bdd data_reg = 
     let _bdd = ref PredTable.bddZero in
     let rec make_cube_bdd cube = 
       match cube with
	   [] -> PredTable.bddOne 
	 | (p,true)::tl ->  CaddieBdd.bddAnd (PredTable.getPredIndexBdd p) (make_cube_bdd tl) 
	 | (p,false)::tl -> CaddieBdd.bddAnd (CaddieBdd.bddNot (PredTable.getPredIndexBdd p))  (make_cube_bdd tl) 
     in
     let add_cube c = 
       _bdd := CaddieBdd.bddOr (make_cube_bdd c) (!_bdd)
     in
       List.iter add_cube data_reg; !_bdd
       
   let check_reg_eq preg_formal reg = 
      (* We shall only check the equivalence of the data regions ... *)
     let retval = 
       try
	 match preg_formal with 
	     Empty_pr -> (reg = Region.bot)
	   | Atomic_pr preg -> 
	       begin
		 let reg_data_bdd = 
		 match reg with 
		     Region.Atomic a -> 
		       begin
			 match a.Region.data with
			     Region.Abstract ab -> ab.Region.bdd
			   | _ -> raise DifferentRegions
		       end
		   | _ -> raise DifferentRegions
	       in
		 let preg_data_bdd = make_data_region_bdd preg.data_reg_cubes 
		 in
		   CaddieBdd.bddEqual preg_data_bdd reg_data_bdd
	       end
       with
	   DifferentRegions -> false
     in
       if ( not retval) then 
	 begin
	   (* Message.msg_string Message.Error "REGION MISMATCH !"; *)
	   statTotalMismatches := !statTotalMismatches + 1
	 end;
       retval

   let extract_data_proof_region data_reg = 
     let rip_bdd b = 
       let cube_list = ref [] in
       let rip_cube (cube : int array) =
	 let conjl = ref [] in
	   for i = 0 to (Array.length cube - 1) do
             if (cube.(i)==2) 
             then () 
             else  conjl := (i,(cube.(i)==1))::(!conjl)
	   done;
	   cube_list := (!conjl)::(!cube_list)
	 in
	 if (CaddieBdd.bddEqual b PredTable.bddOne) then [[]] 
	 else ( CaddieBdd.bddForeachCube b rip_cube; !cube_list)
     in
       rip_bdd data_reg.Region.bdd

   let extract_proof_region reg = 
       let rip_bdd b = 
	 let cube_list = ref [] in
	 let rip_cube (cube : int array) =
	   let conjl = ref [] in
	     for i = 0 to (Array.length cube - 1) do
               if (cube.(i)==2) 
               then () 
               else  conjl := (i,(cube.(i)==1))::(!conjl)
	     done;
	     cube_list := (!conjl)::(!cube_list)
	 in
	   if (CaddieBdd.bddEqual b  PredTable.bddOne) then [[]] 
	   else ( CaddieBdd.bddForeachCube b rip_cube; !cube_list)
       in
	 match reg with 
             Region.Atomic a -> 
	       begin
		 match a.Region.data with
		     Region.Abstract ab -> 
		       Atomic_pr ( 
			 { 
			   prog_counter = a.Region.location;
			   call_stack = a.Region.stack;
			   data_reg_cubes = Stats.time "rip_bdd" rip_bdd ab.Region.bdd;
			 })
			 
		   | _ -> failwith "extract_proof_region: bad region !"
	       end
	   | Region.Empty -> Empty_pr
	   | _ -> failwith "extract_proof_region: bad region !"
		 
		 
(* print_proofTable *)   
(* post_proof : Region.t -> Operation.t -> (proof_region * lemma_index) *)
(* the next 4 functions culminate in post_proof *)
(* postBdd_proof, abstract_data_post_proof_graf_saidi,abstract_data_post_Proof, post_proof *)

   let assume_pred_ref_proof = ref None 
   let postBdd_proof (bdd, pred_list) =
     Message.msg_string Message.Debug "In postBdd_proof..\n";
     Message.msg_string Message.Debug ("post of assume"^(match !assume_pred_ref_proof with Some(_) -> "YES" | _ -> "NO")^"\n");
     Message.msg_string Message.Debug ("Bdd = "^(Predicate.toString (PredTable.convertBddToPred bdd)));
     Message.msg_string Message.Debug "\nList of predicates = ";
     let rec loop lst = 
       match lst with 
	   [] ->  () 
	 | (a,b,c)::rest -> (ignore (Message.msg_string Message.Debug (Predicate.toString (PredTable.getPred a)));ignore (Message.msg_string Message.Debug (Predicate.toString b));ignore (Message.msg_string Message.Debug (Predicate.toString c));  loop rest) 
     in
     let _ = loop pred_list 
     in
  (* let (changed_pred_list , unchanged_pred_list ) =  pred_list in*) 
  (* pretend that everyone has changed *)  
  (* we shall not use wp_different_filter as its an optimization *)
     let sumofcubesList = ref [] in 
     let conjunctL = ref [] in
     let asm_cubeL = ref [] in
	    (* conjunct that the present cube produces post-command c *)
    (* deleting _check_Pred_list_simplify as here we must use vampyre *)
     let _check_Pred_list assumptions pred_triple_list = 
       Message.msg_string Message.Debug "in _check_Pred_list_pr";


       (* URGENT: VAMPYRE BUG! let is_cube_consistent  = TheoremProver.assume assumptions in *)
       let quickCheck wpx eToCheck =
         let rec quick_canonicize pred =
           match pred with
             Predicate.Atom e ->
	       begin
        	  let atm = 
	           match e with
	             Expression.Binary (Expression.Ne, Expression.Lval (Expression.Symbol sym1), Expression.Lval (Expression.Symbol sym2)) ->
		       if (sym1 < sym2) then 
		          Expression.Binary (Expression.Ne, Expression.Lval (Expression.Symbol sym1), Expression.Lval (Expression.Symbol sym2))
		       else
		          Expression.Binary (Expression.Ne, Expression.Lval (Expression.Symbol sym2), Expression.Lval (Expression.Symbol sym1))
	           | Expression.Binary (Expression.Eq, Expression.Lval (Expression.Symbol sym1), Expression.Lval (Expression.Symbol sym2)) ->
		       if (sym1 < sym2) then 
		          Expression.Binary (Expression.Eq, Expression.Lval (Expression.Symbol sym1), Expression.Lval (Expression.Symbol sym2))
		       else
		          Expression.Binary (Expression.Eq, Expression.Lval (Expression.Symbol sym2), Expression.Lval (Expression.Symbol sym1))
		    
	           |	Expression.Binary (Expression.Ne, ee, Expression.Lval (Expression.Symbol sym2)) ->
		          Expression.Binary (Expression.Ne, Expression.Lval (Expression.Symbol sym2), ee) 
	           |	Expression.Binary (Expression.Eq, ee, Expression.Lval (Expression.Symbol sym2)) ->
		          Expression.Binary (Expression.Eq, Expression.Lval (Expression.Symbol sym2), ee)
	           |	Expression.Binary (Expression.Ge, e1, e2) -> Expression.Binary(Expression.Le, e2, e1)
	           |	Expression.Binary (Expression.Gt, e1, e2) -> Expression.Binary(Expression.Lt, e2, e1)
		  
	           |	_ -> e
	       in
	       Predicate.Atom atm
	     end
          |  Predicate.Not ap -> quick_canonicize (Predicate.negateAtom ap) 
          |  Predicate.True -> Predicate.True
          |  Predicate.False -> Predicate.False
          |	_ -> failwith ("quick_canonicize: Predicate "^(Predicate.toString pred)^" is not atomic")
         in
	 let wpx_canon = quick_canonicize wpx in
	 let finder e = (quick_canonicize e) = wpx_canon in
	   if (List.exists finder assumptions) then (Some(0)) else 
	     
			 (*(PredTable.askTheoremProver_proof eToCheck)*)
			 failwith "wanted to call askTheoremProver_proof, but it's no longer supported!"

       in
	 (* check if wpx follows trivially from the assuptions if so return Some(trivialProof) else return (PredTable.askTheoremProver_proof eToCheck) 
*)
       let _check_pred assumptions (p,wpp,wpnp) = 
	 let eToCheck1 = List.fold_right (fun a -> fun b -> Predicate.implies a b) assumptions wpp in
	 let eToCheck2 = List.fold_right (fun a -> fun b -> Predicate.implies a b) assumptions wpnp in
	 let (ans1,ans2) = 
	   if (Options.getValueOfBool "pfgen") then 
	     ((quickCheck wpp eToCheck1),(quickCheck wpnp eToCheck2))
	   else
	     begin
	       let asm =  Predicate.conjoinL assumptions in
	       let conj_e1 = Predicate.implies asm wpp in
	       let conj_e2 = Predicate.implies asm wpnp in
				 failwith "wanted to call askTheoremProver_noproof, but it's no longer supported";
				 (None,None)
		 (*((PredTable.askTheoremProver_noproof conj_e1 eToCheck1),(PredTable.askTheoremProver_noproof conj_e2 eToCheck2))*)
	     end
	 in
	   match (ans1,ans2) with
               ((Some(i1)),(Some(i2))) ->
		 begin
		   Message.msg_string Message.Debug ("Cube implies both wp(p) and wp(neg p): "^(Predicate.toString (PredTable.getPred p))); 
		   conjunctL := ((p,true),i1)::((p,false),i2)::(!conjunctL);
		   true
		 end
	     | (Some(i1),_) -> 
		 begin 
                   conjunctL := ((p,true),i1)::(!conjunctL);
		   true               
		 end
		 
	     | (_,Some(i2)) -> 
		 begin 
		   conjunctL := ((p,false),i2)::(!conjunctL);
		   true               
		 end
	     | (_,_) -> false
       in 
       let _ = List.map (_check_pred assumptions) pred_triple_list in
         ()
	 (* URGENT: fix the is_cube_consistent in the original post -- it must be consistent as we should have checked earler *)
     in
     let _check_Cube _pred_list (cube : int array)  =
       (Message.msg_string Message.Debug (" In _check_Cube"));
       conjunctL := []; asm_cubeL := [];   
       let cubePostIsEmpty = 
	 (Message.msg_string Message.Debug (" In cubePostIsEmpty"));
         match (!assume_pred_ref_proof) with 
             Some(e) -> let _ = failwith "Wanted to call askTheoremProver_proof, but it's no longer supported" in (*PredTable.askTheoremProver_proof ( List.fold_right (fun a -> fun b -> Predicate.implies a b) [(convertCubeToPred cube);e]*) Some(0)
           | None -> let _ = failwith "Wanted to call askTheoremProver_proof, but it's no longer supported" in (*PredTable.askTheoremProver_proof ( List.fold_right (fun a -> fun b -> Predicate.implies a b) [(convertCubeToPred cube)]*) Some(0)
 (* URGENT ! Jhala: fix this in the original post! it could ? still be false ... *)
       in 
       let pref = match (!assume_pred_ref_proof) with None -> ref [] | Some(e) -> ref [e] 
       in
	 for i = 0 to (Array.length cube - 1) do
	   (Message.msg_string Message.Debug (" In do_loop"));
	   if (cube.(i)==2) 
	   then () 
	   else 
	     begin
	       let i_pred = PredTable.getPred i in
(* Assume the invariant that i_pred : Predicate.Atom *)
		 if (cube.(i)==1) then
		   begin
		     pref := i_pred::(!pref);
		     asm_cubeL := (i,true)::(!asm_cubeL);
		   end
		 else
		       begin
			 pref := (Predicate.negate i_pred)::(!pref);
			 asm_cubeL := (i,false)::(!asm_cubeL);
		       end
	     end
	 done;
	 
	 match cubePostIsEmpty with 
	     Some(prf) -> (* inconsistent, empty *)
	       sumofcubesList := (!asm_cubeL ,Empty_post (prf))::(!sumofcubesList)
		 
	   | None -> (* consistent, nonempty *) 
	       begin  
	 (* Now ask the thmprover if pref -> each fellow in predlist *)
		 (_check_Pred_list !pref  pred_list); 
		 sumofcubesList := (!asm_cubeL , Nonempty_post (!conjunctL))::(!sumofcubesList) 
	       end
     in
       begin
	 if (CaddieBdd.bddEqual bdd PredTable.bddZero) then
	   sumofcubesList := []
	 else 
	   begin
	     if (CaddieBdd.bddEqual bdd PredTable.bddOne) 
	     then
               let _tmpcube = (* build a temporary cube with nothing in it *)
		 Array.make (*size *)(getCurrentIndex ()+2) (* content *)2 
		   
(* Note: The size is certainly a vast	overestimate, but this
   is the easiest way. The "+2" removes additional complications relating
   to index being -1 initially. Note that this is a fake cube -- we should
   never try to print it or convert it to a predicate!Ugly hack, but 
   works. So far *)
               in
		 _check_Cube pred_list _tmpcube
	     else
               ignore (CaddieBdd.bddForeachCube bdd (_check_Cube pred_list));
	   end;
	 (Message.msg_string Message.Debug ("Returning from Post-Bdd_proof"));
	 
         let extract_reg_from_pep pep = 
	   let take (dcube,cpl) = 
	    match cpl with
		Empty_post _ -> None 
	      | Nonempty_post l -> Some (List.map fst l)
	   in
	     Misc.list_gather_option (List.map take pep)
	 in
	 let post_data_cubeList = extract_reg_from_pep (!sumofcubesList) in
	 let post_lemma_index = insert_lemma ( Post_Edge_Lemma !sumofcubesList) in
	   (post_data_cubeList,post_lemma_index)
       end
       
 let abstract_data_post_proof_graf_saidi abst_data_reg cmd edge phi_table : (data_literal list list * lemma_index) =
     Message.msg_string Message.Debug "In abstract_data_post_proof SLAM_POST -- arguments are:" ;
     Message.msg_printer Message.Debug Region.print_abstract_data_region abst_data_reg ;
     Message.msg_printer Message.Debug C_SD.Command.print cmd ;
     let _H_cmd_proof _cmd bdd predlist =
       Message.msg_string Message.Debug ("In _H of abstract_data_post_Proof, bdd = "^(Predicate.toString (PredTable.convertBddToPred bdd)));
       let make_pred_triple a = 
         let pr = PredTable.getPred a in
           match (!assume_pred_ref_proof) with 
               None -> 
                 let cr = concrete_data_pre false false { Region.pred = pr ; } _cmd edge phi_table in
                 let wppos = cr.Region.pred in
                 let ncr = concrete_data_pre false false { Region.pred = Predicate.negate pr ; } _cmd edge phi_table in
                 let wpneg = ncr.Region.pred in
                   (a,wppos,wpneg)
             | Some(e) -> (a,pr,Predicate.negate pr)
       in
         postBdd_proof (bdd,(List.map make_pred_triple predlist))
     in
     let (_H_proof : CaddieBdd.bdd -> Region.predList -> ( data_literal list list * lemma_index)) = _H_cmd_proof cmd in
     match cmd.C_SD.Command.code with 
           C_SD.Command.FunctionCall fcallExp ->
	     let (fname,_subs,_) = (C_SD.deconstructFunCall fcallExp) in
               if (fname = C_SD.__NotImplementedFunctionName || not (C_SD.is_defined fname)) then (extract_data_proof_region abst_data_reg, skip_lemma_index)
               else 
		 let asgn_block =
                   { cmd with C_SD.Command.code = C_SD.Command.Block (List.map (fun (x,e) -> C_SD.Command.Expr( Expression.Assignment (Expression.Assign,Expression.Symbol(x), e ))) _subs) }
		 in
		   (* Region.preds = abst_data_reg.Region.preds;*)
      	 (_H_cmd_proof asgn_block (abst_data_reg.Region.bdd) abst_data_reg.Region.preds) (* {Region.bdd = PredTable.bddZero;Region.preds = []}*)
		   
	 | C_SD.Command.Skip ->  (extract_data_proof_region abst_data_reg, skip_lemma_index)
             (* let _ = (retTarget := Absutil.junkRet) in
		_H_proof  (abst_data_reg.Region.bdd) abst_data_reg.Region.preds*)
		 
     (* Stats.time "rip_bdd" rip_bdd abst_data_reg.Region.bdd *)
	 | C_SD.Command.Block [] ->  (extract_data_proof_region abst_data_reg, skip_lemma_index)
	 | C_SD.Command.Block l  -> 
             if ( not (List.exists (function x -> match x with C_SD.Command.Return e -> true | _ -> false) l))
	     then 
	       let _ = Absutil.reset_ret_pointers () in
                 _H_proof  (abst_data_reg.Region.bdd) abst_data_reg.Region.preds
		   
	     else 
	       let (targ, fname, retloc,subs) = lastTarget () in
 (* Jhala: the t.o.s. is passed using last_loc_pointer  reg.Region.stack *) 
	       let _ = Absutil.set_ret_pointers targ subs (C_SD.get_location_fname retloc)
	       in 
	       let (newPreds,delPreds) = Absutil.removeFunPreds fname abst_data_reg.Region.preds in
               let _ = Message.msg_string Message.Debug "Return: Calling _H_proof" in
	       let (newCubes,index) = (_H_proof  (abst_data_reg.Region.bdd) abst_data_reg.Region.preds) in
               let _ = Message.msg_string Message.Debug "Return: Back from _H_pr" in
	       let _predToVar i =  -1 in (* JHALA: To be done ... ??? *)
	       let _ = Absutil.reset_ret_pointers () in
                (* Region.preds = newPreds@delPreds;*)
      		 (newCubes,index)
		 
	 | C_SD.Command.Pred e -> 
             (* if (e = Predicate.True) then abst_data_reg else : OPT*) 
               begin
		 assume_pred_ref_proof := Some(e);
		 let (newCubes,index) = _H_proof (abst_data_reg.Region.bdd) abst_data_reg.Region.preds 
		 (* Region.preds = abst_data_reg.Region.preds ; *)
		 in  
		   assume_pred_ref_proof := None;          
		   (newCubes,index)
               end
	 | C_SD.Command.Phi (_,_) 
	 | C_SD.Command.Havoc _ 
	 | C_SD.Command.HavocAll -> failwith "abstract_data_post_proof_graf_saidi: Havoc and HavocAll : TBD"
	       
   let abstract_data_post_proof abst_data_reg  cmd edge phi_table = 
     match (Options.getValueOfString "post") with
         "H" -> 
	   begin
	     Message.msg_string Message.Error "ERROR!: Proof Generation with H-post is not yet implemented"; 
	     failwith "Unimplemented post-proof with H"
	   end
       | _  -> 
	     let (rr : (data_literal list list * lemma_index)) = 
	       (* Stats.time "Post-proof Graf-Saidi"*) (abstract_data_post_proof_graf_saidi abst_data_reg  cmd edge) phi_table 
	     in
	       rr

   let post_proof reg op phi_table =
     Message.msg_string Message.Debug "In post_proof -- arguments are:" ;
     Message.msg_printer Message.Debug Region.print reg ;
     Message.msg_printer Message.Debug Operation.print op ;
     
     let preds_of_region reg = 
       match reg with
	   Region.Atomic atr -> 
             begin     
               match atr.Region.data with 
		   Region.Abstract abst_data_reg -> List.map PredTable.getPred abst_data_reg.Region.preds
		 | _ -> failwith "post_proof: Bad call to preds_of_region "
             end
	 | _ -> failwith "post_proof: Bad call to preds_of_region "
     in
       match reg with
           Region.Empty -> 
	     begin
	       Message.msg_string Message.Error "WARNING: post_proof called with empty region";
	       (Empty_pr,skip_lemma_index) (* failwith "post_proof: arg is Empty Region !" *)
	     end
	 | Region.Atomic atomic_reg ->
	     begin
               try 
		 if (atomic_reg.Region.location <> (C_SD.get_source op)) then
		   begin
                     Message.msg_string Message.Error "WARNING: post: blast internal error suspected" ;
                     Message.msg_string Message.Error "               operation do not match this region's location" ;
                     failwith "post_proof: args mismatch !"
                   (* Region.bot *) 
		   end
               else
		 begin
                   try 
                     let callStack = Region.list_of_stack atomic_reg.Region.stack in
	             let _ = 
		       if (callStack <> []) 
                       then (last_loc_pointer := Some (List.hd callStack)) 
                       else ( last_loc_pointer := None)
		     in
	          (* So last_loc_pointer points to the top of the stack at all times or to errorloc Rif the stack is empty *)
                     let (new_location, new_stack) = 
		       
			 if (C_SD.isFunCall op) then
			   begin
                             match (C_SD.get_command op).C_SD.Command.code with
				 C_SD.Command.FunctionCall fcexp -> 
				   let (_fname,_,target) = C_SD.deconstructFunCall fcexp in
				     if (_fname = C_SD.__NotImplementedFunctionName || not (C_SD.is_defined _fname)) 
				     then 
				       ((C_SD.get_target op), atomic_reg.Region.stack)
				     else   
				       begin
					 if (C_SD.is_defined _fname) then
					   ((C_SD.lookup_entry_location _fname),(Region.CallStack ((C_SD.get_source op):: callStack)))  
					 else 
					   begin
                                             let _wrap_getvars _pred = List.map (function x -> Expression.Symbol x) (Absutil.getVarExps_deep _pred) in 
                                             let vars_in_support = List.flatten (List.map _wrap_getvars (preds_of_region reg)) in   
                                             if (not (List.mem target vars_in_support)) then 
                                               ((C_SD.get_target op),atomic_reg.Region.stack) (* is a skip *)
                                             else failwith ("Undeclared important function"^_fname)
				           end
			                end
                                  | _ -> failwith "what the (post) !"
		             end
                        else 
	
			   try
			     let (_,_,arg,_) = lastTarget () in
			     if (C_SD.isReturn op) then ( arg,(Region.CallStack (List.tl callStack)) ) 
			     else (C_SD.get_target op,atomic_reg.Region.stack) 
			   with Failure ("tl") -> (C_SD.get_target op, atomic_reg.Region.stack ) 
	
       
                     and (new_data,p_index) =
                       try 
			 match atomic_reg.Region.data with
			     Region.Abstract abst_data_reg ->
                               ((abstract_data_post_proof abst_data_reg (C_SD.get_command op)) op phi_table)
			   | Region.Concrete conc_data_reg ->
			       begin
				 Message.msg_string Message.Error "ERROR!: post_proof given concrete region"; failwith "post_proof: arg concrete region"
			       end
                       with C_SD.NoSuchFunctionException f -> failwith ("post_proof: Unexpected NoSuchFunctionException "^f)
                     in
                      ( Atomic_pr { prog_counter = new_location ;
                                   call_stack = new_stack ; 
				   data_reg_cubes = new_data;
				 }, p_index)
		   with
		       Failure("Empty Call Stack") -> (Empty_pr,skip_lemma_index)
		 end
               with 
		   C_SD.NoSuchFunctionException f -> (* treat it as a skip *)
		     failwith ("post_proof: Unexpected NoSuchFunctionException "^f)
		     (*Message.msg_string Message.Minor ("Unexpected NoSuchFunctionException "^f);*)
		     (* THIS SHOULD BE COMMENTED OUT IF UNDEFINED FUNCTIONS ARE TO BE AVOIDED *)
		     (* failwith ("Unexpected NoSuchFunctionException "^f); *)
		     (*
		       Atomic_pr { prog_counter = C_SD.get_target op old_location;
				   call_stack = atomic_reg.Region.stack old_stack;
				   data_reg_cubes = old_data;
				 },  skip_lemma_index*)
		       
       	     end
	 | Region.Union _ ->
             failwith "post_proof: arg is a union !"   
	 | _ -> failwith "post_proof: parallel regions not supported"   

let print_stats_proof fmt () =
  Message.msg_string Message.Normal ("Proof Statistics:");
   Message.msg_string Message.Normal ("Total number of queries = "^(string_of_int (!statTotalNumberOfQueries))^"\n");
   Message.msg_string Message.Normal ("Total cached = "^(string_of_int (!statTotalCached))^"\n\n");     
   ()

(* ********** END BlastProof ************ *)
