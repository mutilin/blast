(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
	 
  (************************************************************************* *)
  (******** Functions required by the GUI for reading data structures *******)
  (**************************************************************************)

let stack_to_string stack = 
  match stack with
      Region.EveryStack -> "[_]"
    | Region.CallStack stack_list -> 
	begin
	  let stack_string = 
	    List.fold_left 
	      (fun a -> fun b -> (a^(C_SD.get_location_fname b)^";\n"))
	      "" stack_list
	  in
	    "[ "^stack_string^" ]"
	end


let locations_of_stack stack =
  match stack with
      Region.EveryStack -> []
    | Region.CallStack stack_list -> stack_list

	
let data_to_string_list data =
  let pred_is_atomic p = 
    match p with
      Predicate.Atom _ -> true
    | Predicate.Not (Predicate.Atom _) -> true
    | Predicate.True | Predicate.False -> true 
    | _ -> false
  in
    match data with
	Region.Concrete p ->
	  begin
	    match p.Region.pred with
		Predicate.And l -> 
		  let _ = 
		    if (List.exists (fun x -> not (pred_is_atomic x)) l) 
		    then ( 
		      Message.msg_string 
			Message.Debug 
			("Warning: region has non-atomic conjuncts "^(Predicate.toString p.Region.pred)))
		  in
		    List.map Predicate.toString l
	      | Predicate.True -> []
	      |	Predicate.Atom a -> [ Predicate.toString p.Region.pred ]
	      |	Predicate.Not (Predicate.Atom _) -> [ Predicate.toString p.Region.pred ]
	      | _ -> [(Predicate.toString p.Region.pred)]
	end
    | Region.Abstract adr ->
        let s = PredTable.bdd_to_string_pretty adr.Region.bdd in
        let s' = DataLattice.toString adr.Region.lelement in
        [s;s']


let reg_to_strings reg = 
  match reg with
      Region.Union _ -> failwith "Bad trace -- union region"
    | Region.Atomic r -> 
	begin
	  let loc = r.Region.location in
	  let data = r.Region.data in
	  let stack = r.Region.stack in
          let s2 = 
            if (Options.getValueOfBool "fmc" 
                || Options.getValueOfBool "fmcc" 
                || Options.getValueOfBool "cf") 
            then 
              let (x,y) = C_SD.location_coords loc in
              (string_of_int x)^"#"^(string_of_int y)
            else
            stack_to_string stack
          in
	   (data_to_string_list data,s2)
	end
    | Region.ParallelAtomic _
    | Region.ParallelUnion _ ->failwith "reg_to_strings: bad region"
    | Region.Empty -> (["empty"],"")

let atomic_reg_function_name r =
  begin
    let loc = r.Region.location in
    C_SD.get_location_fname loc
  end
  
let reg_function_name reg = 
  match reg with
      Region.Union _ -> failwith "Bad trace -- union region"
    | Region.Atomic r -> atomic_reg_function_name r
    | Region.ParallelAtomic _
    | Region.ParallelUnion _ 
    | Region.Empty -> failwith "reg_function_name: bad region"

let is_noreturn reg = C_SD.is_noreturn (reg_function_name reg)
   
let reg_to_string reg = 
  match reg with
      Region.Union _ -> failwith "Bad trace -- union region"
    | Region.Atomic r -> 
	begin
	  let loc = r.Region.location in
	  C_SD.location_to_string loc
	end
    | Region.ParallelAtomic _
    | Region.ParallelUnion _ 
    | Region.Empty ->
	failwith "reg_function_name: bad region"

let loc_to_string loc =
  match C_SD.get_source_position loc with
      Some(file,line,col) -> (string_of_int line^":")
    | None -> "-1:"

let loc_to_int loc =
  match C_SD.get_source_position loc with
      Some(file,line,col) -> line (* (string_of_int line^":")*)
    | None -> -1

let op_to_string op =
  let print_fncall fcallexp = (* when printing the function calls, also print the formal argument names *)
    let fncall_to_string (fname, args) =
      let argstring = 
        match fname with Expression.Lval (Expression.Symbol s) -> 
          begin
            try let formals = C_SD.lookup_formals s in
            match formals with 
              C_SD.Fixed l ->  List.fold_right2 
               (fun thisformal thisactual sofar -> (thisformal ^ " = " ^ (Expression.toString thisactual) ^"," ^ sofar))
               l args ""
            | C_SD.Variable l -> List.fold_right (fun thisone sofar -> (Expression.toString thisone) ^" , " ^ sofar) args ""
            with _ -> List.fold_right (fun thisone sofar -> (Expression.toString thisone) ^" , " ^ sofar) args ""
          end
        | _ -> List.fold_right (fun thisone sofar -> (Expression.toString thisone) ^" , " ^ sofar) args ""
      in
      (Expression.toString fname) ^ "("  ^ argstring ^ ")"
    in
    match fcallexp with
      Expression.FunctionCall(fname, args) ->
       fncall_to_string (fname, args)
    | Expression.Assignment (_, t, Expression.FunctionCall (fname, args)) ->
       (Expression.lvalToString t) ^ " = " ^ (fncall_to_string (fname, args))
    | _ -> failwith "Internal error in op_to_string" 
  in
  let s_loc = C_SD.get_source op in
  let x = C_SD.get_command op in
  let s1_loc_line = 
    match x.C_SD.Command.code with C_SD.Command.Pred _ -> -1
    | _ -> loc_to_int (C_SD.get_target op) 
  in
  let cmd_string = 
    match x.C_SD.Command.code with 
      C_SD.Command.FunctionCall f -> "FunctionCall("^ (print_fncall f ) ^ ")"
    | _ -> C_SD.Command.to_string x 
  in
  (loc_to_int s_loc, s1_loc_line, (Printf.sprintf "%s \t %s" (loc_to_string s_loc) (cmd_string)))
 
  
(**********************End GUI functions *********************************)

(************* Code for generating HTML versions of traces ***************)
(* Files:
 * static: trace.js,trace.css
 * gen (fixed): code.html, index.html, result.html
 * gen (for each trace i): index.i.html,header.i.html,trace.i.html
 *)

let trace_count_ref = ref 0
let src_ref = ref ""
let html_dirname () = "trace_"^(!src_ref)

let head_string = 
  "<html><head><title>BLAST Counterexample Viewer</title>
   <style type=\"text/css\" media=\"screen\">@import \"trace.css\";</style>
   <script src=\"trace.js\" type=\"text/javascript\"></script></head>"
   
let index0_string = 
  "<html><head><meta http-equiv=\"Refresh\" content=\"0; URL=index.1.html\"></head></html>"

let result_string = 
  "<html><head><meta http-equiv=\"Refresh\" content=\"0; URL=result.html\"></head></html>"

let html_make_index_string i = 
  Printf.sprintf 
  "%s \n 
   <frameset rows=\"170,*\">
   <frame name=\"header\" scrolling=\"no\" src=\"header.%d.html\">
   <frameset cols=\"267,*\">
   <frame name=\"trace\" src=\"trace.%d.html\" scrolling=\"auto\">
   <frame name=\"code\" src=\"code.html\" scrolling=\"auto\">
   </frameset>
   <noframes>
   <body>
   <p>This page uses frames, but your browser doesn't support them.</p>
   </body>
   </noframes>
   </frameset>
   </html>" head_string i i


let html_make_header_string idx srcfile comment_s infblock_s pre_idx post_idx = 
  Printf.sprintf 
  "%s \n <body align=center>
  <div align=center> BLAST Counterexample %d for %s </div>
  <table align=center>
  <tr><td>%s</td></tr>
  <tr> 
  <td>Infeasible Blocks:</td>
  <td><a href=\"javascript://\" onclick=\"top.trace.colorRed()\";>-</a></td>
  %s 
  </tr></table>
  <table border align=center>
    <tr>
      <td><a href=\"javascript://\" onclick=\"top.trace.showfootnote()\";>Toggle State</a></td>
      <td><a href=\"index.%d.html\" target=\"_top\">Previous</a></td>
      <td><a href=\"index.%d.html\" target=\"_top\">Next</a></td>
      <td><a href=\"result.html\" target=\"_top\">Result</a></td>
  </tr>
  </table>
</body></html>" head_string idx srcfile comment_s infblock_s pre_idx post_idx 

let html_make_result_string res_s = 
  let img_s = 
    if Misc.is_substring res_s ":-)" then "safe"
    else if Misc.is_substring res_s ":-(" then "unsafe" 
    else "dontknow"
  in
    Printf.sprintf "%s \n <body>
  <table align=center> 
  <tr><td align=center> <img src=\"%s.gif\" height=30> </td></tr>
  <tr><td> %s <td></tr>
  </table></body> </html>" head_string img_s res_s

let html_make_trace_string s = 
  Printf.sprintf "%s \n <body><ol> %s </ol></body></html>" head_string s

let html_make_op_string idx start stop op_s reg_s = 
  Printf.sprintf 
  "<li onclick=\"colorme(%d)\";> 
  <div
    onMouseOver=\"this.className='highlight'\"
    onMouseOut=\"this.className='normal'\"
    onclick=\"top.code.colorCode('td',%d,%d)\";> %s </a><br><br>
    <div class=\"Desc\"> %s </div></div></li><br />"
  idx start stop op_s reg_s

let html_make_ibs i ib =
  Printf.sprintf 
  "<td><a href=\"javascript://\" onclick=\"top.trace.colorRed(%s)\";>%d</a></td>\n"
  (String.concat "," (List.map string_of_int ib)) i
let html_feasible_trace = "<td><font color=\"#ff0000\"> Feasible Trace </font></td>"
  
let htmlize_trace_init () =  (* TBD:HTMLCOPY *)
 let _ = 
     src_ref := try List.hd (Options.getSourceFiles ()) with _ -> failwith "imp"
   in
   Sys.command (Printf.sprintf "mkdir %s" (html_dirname()));
   Sys.command (Printf.sprintf "rm %s/*.html" (html_dirname ()));
   Sys.command (Printf.sprintf "cp $BLASTHOME/doc/html/* %s/" (html_dirname ()));
   Sys.command (Printf.sprintf "$BLASTHOME/bin/htmlize %s > %s/code.html" !src_ref (html_dirname ()));
   Misc.write_to_file (Printf.sprintf "%s/index.html" (html_dirname ())) index0_string;
   trace_count_ref := 1


let fixloc (i,j) = 
  match (i,j) with  
      (-1,-1) -> (i,j)
    | (-1,j) -> (j-1,j) 
    | (i,-1) -> (i,i+1) 
    | _ -> if (i>=j) then (i,i+1) else (i,j)

let htmlize_make_index i = 
  let istring = html_make_index_string i in
  let ifile = Printf.sprintf "%s/index.%d.html" (html_dirname ()) i in
  Misc.write_to_file ifile istring

let htmlize_make_infbs infb_l = 
  let rec folder i s ibl = 
    match ibl with [] -> s 
    | h::t -> let s = s^(html_make_ibs i h) in 
              folder (i+1) s t
  in
  if infb_l = [] then html_feasible_trace else folder 1 "" infb_l

let htmlize_make_header i comment_s infb_l = 
  let i_prev = if i>1 then i-1 else 1 in
  let infb_s = htmlize_make_infbs infb_l in
  let hstring = html_make_header_string i (!src_ref) comment_s infb_s i_prev (i+1) in
  let hfile = Printf.sprintf "%s/header.%d.html" (html_dirname ()) i in
  Misc.write_to_file hfile hstring


let htmlize_make_trace i op_l post_reg_s_l =
  let clean op_s = 
    match Misc.chop op_s "\t" with (h::h'::t) -> String.concat "" (h'::t) | _ -> op_s
  in
  let make_op_string i op reg_s =
    let (l1,l2,op_s) = op_to_string op in
    let (l1,l2) = fixloc (l1,l2) in
    html_make_op_string i l1 l2 (clean op_s) reg_s 
  in
  let rec folder i s _op_l _p_reg_l = 
    match (_op_l,_p_reg_l) with 
      ([],[]) -> s
    | (op::opl',reg::regl') -> 
        let s' = s^(make_op_string i op reg)^"\n\n" in 
        folder (i+1) s' opl' regl'
    | _ -> failwith "error in make_trace"
  in
  let op_s = folder 0 "" op_l post_reg_s_l in
  let tstring = html_make_trace_string op_s in
  let tfile = Printf.sprintf "%s/trace.%d.html" (html_dirname ()) i in
  Misc.write_to_file tfile tstring
  
(************* API functions **************)

let htmlize_trace ops regs' infbs comment = 
  if (Options.getValueOfBool "traces") then
  begin
    let regs' = List.map (Misc.substitute_substrings [("<"," &lt; ");(">"," &gt; ")]) regs' in
    if !trace_count_ref = 0 then htmlize_trace_init ();
    htmlize_make_index !trace_count_ref;
    htmlize_make_header !trace_count_ref comment infbs;
    htmlize_make_trace !trace_count_ref ops regs';
    trace_count_ref := !trace_count_ref + 1
  end

let htmlize_trace_final  res_s =
  if (Options.getValueOfBool "traces") then
    begin
  let fs = html_make_result_string res_s in
  let finaltfile = Printf.sprintf "%s/index.%d.html" (html_dirname ()) !trace_count_ref in
  Misc.write_to_file (Printf.sprintf "%s/result.html" (html_dirname ())) fs;
  Misc.write_to_file finaltfile result_string
    end
