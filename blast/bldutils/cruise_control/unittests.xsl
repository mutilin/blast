<?xml version="1.0"?>
<!--********************************************************************************
 * CruiseControl, a Continuous Integration Toolkit
 * Copyright (c) 2001, ThoughtWorks, Inc.
 * 651 W Washington Ave. Suite 600
 * Chicago, IL 60661 USA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     + Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     + Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     + Neither the name of ThoughtWorks, Inc., CruiseControl, nor the
 *       names of its contributors may be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:lxslt="http://xml.apache.org/xslt">

    <xsl:output method="html"/>

    <xsl:variable name="testsuite.list" select="//testsuite"/>
    <xsl:variable name="testsuite.error.count" select="count($testsuite.list/error)"/>
    <xsl:variable name="testcase.list" select="$testsuite.list/testcase"/>
    <xsl:variable name="testcase.error.list" select="$testcase.list/error"/>
    <xsl:variable name="testcase.failure.list" select="$testcase.list/failure"/>
    <xsl:variable name="totalErrorsAndFailures" select="count($testcase.error.list) + count($testcase.failure.list)"/>
    <xsl:variable name="totalTestsPassed" select="count($testcase.list) - $totalErrorsAndFailures"/>
    <xsl:variable name="project" select="/cruisecontrol/info/property[@name='projectname']/@value"/>
    <xsl:variable name="buildno" select="/cruisecontrol/info/property[@name='lastbuild']/@value"/>

    <xsl:template match="/" mode="unittests">
        <table cellpadding="2" cellspacing="0" border="1">

            <!-- Unit Tests -->
            <tr>
                <td class="unittests-sectionheader" colspan="4">
                   &#160;<xsl:value-of select="count($testcase.list)"/> Unit Tests: (<xsl:value-of select="$totalTestsPassed"/> passed, <xsl:value-of select="count($testcase.error.list)"/> aborted, <xsl:value-of select="count($testcase.failure.list)"/> failed)
                </td>
            </tr>
              <!-- JF 9/26/05: Add link to regression test report -->
              <tr>
                <td><a><xsl:attribute name="href">/cruisecontrol/artifacts/<xsl:value-of select="$project"/>/<xsl:value-of select="$buildno"/>/regrtest.html</xsl:attribute>Full Test Report</a></td>
              </tr>

            <xsl:choose>
                <xsl:when test="count($testsuite.list) = 0">
                    <tr>
                        <td colspan="2" class="unittests-data">
                            No Tests Run
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="unittests-error">
                            This project doesn't have any tests
                        </td>
                    </tr>
                </xsl:when>

                <xsl:when test="$totalErrorsAndFailures = 0">
                    <tr>
                        <td colspan="2" class="unittests-data">
                            All Tests Passed
                        </td>
                    </tr>
                </xsl:when>
            </xsl:choose>
            <tr>
              <td>
                 <table cellpadding="4" cellspacing="0" border="0" >
                    <xsl:apply-templates select="$testcase.error.list" mode="unittests"/>
                    <xsl:apply-templates select="$testcase.failure.list" mode="unittests"/>
                 </table>
              </td>
            </tr>
            <tr/>
            <tr><td colspan="2">&#160;</td></tr>
        </table>
    </xsl:template>

    <!-- UnitTest Errors -->
    <xsl:template match="error" mode="unittests">
        <tr>
            <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="class">unittests-oddrow</xsl:attribute>
            </xsl:if>

            <td class="unittests-data" align="right">
                <xsl:value-of select="../@name"/>
            </td>
            <td class="unittests-data">
                aborted
            </td>
            <td class="unittests-data">
                <xsl:value-of select="../TestDesc"/>
            </td>
        </tr>
    </xsl:template>

    <!-- UnitTest Failures -->
    <xsl:template match="failure" mode="unittests">
        <tr>
            <xsl:if test="($testsuite.error.count + position()) mod 2 = 0">
                <xsl:attribute name="class">unittests-oddrow</xsl:attribute>
            </xsl:if>

            <td class="unittests-data" align="right">
                <xsl:value-of select="../@name"/>
            </td>
            <td class="unittests-data">
                failed
            </td>
            <td class="unittests-data">
                <xsl:value-of select="../TestDesc"/>
            </td>
        </tr>
    </xsl:template>


    <xsl:template match="/">
        <xsl:apply-templates select="." mode="unittests"/>
    </xsl:template>
</xsl:stylesheet>
