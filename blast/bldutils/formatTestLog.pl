#!/usr/bin/perl
# formatTestLog.pl
# Generate xml and html reports from regrtest.log
# Run this program in the $BLASTHOME/test directory.

#statistics
$passed = 0;
$failed = 0;
$aborted = 0;


open LOGFILE, "regrtest.log" or die "Unable to open regrtest.log";
open XMLFILE, ">regrtest.xml" or die "Unable to open output file regrtest.xml";
open HTMFILE, 
      ">regrtest.html" or die "Unable to open output file regrtest.html";

print XMLFILE "<testsuite>";
print HTMFILE "<html><head><title>BLAST regression test results</title>\n";
print HTMFILE "<h1 align=\"center\">BLAST regression test results</h1></title><body>\n";
print HTMFILE "<table align=\"center\" cellpadding=\"2\" cellspacing=\"0\"";
print HTMFILE " border=\"1\" width=\"85%\">\n";
print HTMFILE "<tr><td width=30><b>Test No.</b></td><td width=50><b>Status</b></td><td><b>Reason<b></td><td><b>Command</b></td></tr>\n";

while (<LOGFILE>) {
    # first, extract test number
    if (/^\[(\d+)\]/) {
	$testno = $1;
	s/^\[(\d+)\] //;
    } else {
	print "formatTestLog.pl ignoring line: $_\n";
	next;
    }

    # next, determine status
    if (/(.+) succeeds$/) {
	$command = $1;
	++$passed;
	print XMLFILE "<testcase\n name=\"$testno\" status=\"passed\">";
        print XMLFILE "<TestDesc>$command</TestDesc>";
	print XMLFILE
            "<TestStatusDetail>Blast run successful.</TestStatusDetail>";
	print XMLFILE "</testcase>";
	print HTMFILE "<tr><td width=30>$testno</td>";
	print HTMFILE "<td width=60 bgcolor=\"lightgreen\">Passed</td>";
	print HTMFILE "<td>Blast run successful.</td><td>$command</td></tr>\n";
    }
    elsif (/(.+) fails as expected$/) {
	$command = $1;
	++$passed;
	print XMLFILE "<testcase\n name=\"$testno\" status=\"passed\">";
        print XMLFILE "<TestDesc>$command</TestDesc>";
	print XMLFILE
            "<TestStatusDetail>Failed as expected.</TestStatusDetail>";
	print XMLFILE "</testcase>";
	print HTMFILE "<tr><td width=30>$testno</td>";
	print HTMFILE "<td width=60 bgcolor=\"lightgreen\">Passed</td>";
	print HTMFILE "<td>Failed as expected.</td><td>$command</td></tr>\n";
    }
    elsif (/(.+) Unexpected error/) {
	$command = $1;
	++$aborted;
	print XMLFILE "<testcase\n name=\"$testno\" status=\"aborted\">";
        print XMLFILE "<TestDesc>$command</TestDesc>";
	print XMLFILE
            "<error>Blast run did not complete.</error>";
	print XMLFILE "</testcase>";
	print HTMFILE "<tr><td width=30>$testno</td>";
	print HTMFILE "<td width=60 bgcolor=\"orange\">Aborted</td>";
	print HTMFILE "<td>Blast run did not complete. See <a href=\"test_$testno.log\">";
	print HTMFILE "test_$testno.log</a> for details.</td><td>$command</td></tr>\n";
    }
    elsif (/^A regression test command failed:$/) {
	$command = <LOGFILE>;
	chomp $command;
	++$failed;
	print XMLFILE "<testcase\n name=\"$testno\" status=\"failed\">";
        print XMLFILE "<TestDesc>$command</TestDesc>";
	print XMLFILE
            "<failure>BLAST run found an error that was not expected.</failure>";
	print XMLFILE "</testcase>";
	print HTMFILE "<tr><td width=30>$testno</td>";
	print HTMFILE "<td width=60 bgcolor=\"red\">Failed</td>";
	print HTMFILE "<td>Blast run found an unexpected error. See <a href=\"test_$testno.log\"";
	print HTMFILE ">test_$testno.log</a> for details.</td>";
	print HTMFILE "<td>$command</td></tr>\n";
    }
    elsif (/BAD NEWS\: A regression test that should fail \(.*\) now succeeds:$/)
    {
	$command = <LOGFILE>;
	chomp $command;
	++$failed;
	print XMLFILE "<testcase\n name=\"$testno\" status=\"failed\">";
        print XMLFILE "<TestDesc>$command</TestDesc>";
	print XMLFILE
          "<failure>BLAST passes but should fail.</failure>";
	print XMLFILE "</testcase>";
	print HTMFILE "<tr><td width=30>$testno</td>";
	print HTMFILE "<td width=60 bgcolor=\"red\">Failed</td>";
	print HTMFILE "<td><b>Blast passes but should fail!</b> See <a href=\"test_$testno.log\"";
        print HTMFILE ">test_$testno.log</a> for details.</td>";
	print HTMFILE "<td>$command</td></tr>\n";
    }
    elsif (/skipping/) {
	print "$testno skipped\n";
    }
    else {
	print "formatTestLog.pl unable to parse line: $_\n";
	exit 1;
    }
}

print XMLFILE
    "<TestSummary passed=\"$passed\" failed=\"$failed\" aborted=\"$aborted\" />\n";
print "Passed:  $passed\n";
print "Failed:  $failed\n";
print "Aborted: $aborted\n";

print XMLFILE "</testsuite>";
close XMLFILE;

print HTMFILE "</table></body>\n</html>";
close HTMFILE;
exit 0;
