(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

  
module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
    
module C_SD = BlastCSystemDescr.C_System_Descr

(* debug flag: I want to debug this module separately *)
let debug_bdd_pts_to = ref false
(** Code relating to initializing the BDD package.
    Defines the manager, true, and false -BDDs.
    *)
let bddManager = CaddieBdd.init 0 64 256 512 
let bddZero = CaddieBdd.bddFalse bddManager
let bddOne = CaddieBdd.bddTrue bddManager
let allocFuns = ["alloc"; "malloc"; "calloc"; "realloc"; "kmalloc" ]  @ [ "fopen"; "fdopen" ] 

let rec fullsimp_lval lv =
  try
    match lv with
      Expression.Symbol s -> lv
    | Expression.Access(_, e,_) ->
        fullsimp_exp e
    | Expression.Dereference e ->
        fullsimp_exp e
    | Expression.Indexed(e,_) ->
        fullsimp_exp e
  with excn -> 
    failwith ("fullsimp-lval: value "^(Expression.lvalToString lv)^" raises exception:"^(Printexc.to_string excn))
and fullsimp_exp e =
  match e with
    Expression.Lval l -> fullsimp_lval l
  | Expression.Unary (Expression.Address, e') -> fullsimp_exp e'
  | _ -> failwith ("bddptsto[fullsimp_lval]: Strange expression "^(Expression.toString e))




(** This hash table contains the map from locations to their bdd functions *)
let location_to_bdd_map = Hashtbl.create 31

let lookup_location loc =
  try
    let (a,b) = C_SD.location_coords loc in
    Hashtbl.find location_to_bdd_map (a,b)
  with Not_found ->
    begin
      Hashtbl.add location_to_bdd_map (C_SD.location_coords loc) bddZero ;
      bddZero
    end
  | _ -> if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug "lookup fails" ; 
      if (!debug_bdd_pts_to) then 
	Message.msg_string  Message.Debug 
	  ("Length = "^(string_of_int 
			  (List.length 
			     (Hashtbl.fold 
				(fun key data curlist -> key :: curlist) location_to_bdd_map []))))  ; 
      failwith "Here"

(** This hash table will contain a map from lvalues to their bdd encoding. *)
let lvalue_to_bdd_map = Hashtbl.create 31
    
(** This hash table will contain a map from edge to its allocated memory *)
let edge_to_mem_map = Hashtbl.create 31

(** This hash table will contain a map from function calls to the target locations of their edges *)
(*
let fcn_to_target_map = Hashtbl.create 31
*)  
  
(** This hash table will contain a map from function name to the list of lvalues in them *)
(*
   let fcn_to_lvalue_map = Hashtbl.create 31
   let fcn_to_lvalbdd_map = Hashtbl.create 31
 *)

(** Field sensitivity *)
let type_and_field_name_to_lval = Hashtbl.create 31

let lookup_field_lval e fldname =
  let t = C_SD.get_type_of_exp e in
  match t with
    Cil.TComp _ -> 
      begin
	try Hashtbl.find type_and_field_name_to_lval (t,fldname)
	with Not_found -> failwith ("lookup_field_lval: Not found for "^
				    (Expression.toString e) ^ 
				    " " ^ fldname)
      end
  | _ -> failwith ("lookup_field_lval: strange lval "^(Expression.toString e))

let add_field_lval t f =
  try Hashtbl.find type_and_field_name_to_lval (t,f)
  with Not_found ->
    begin
      match t with
	Cil.TComp (c, _) ->
	  let lvname = (Cil.compFullName c)^"//" ^ f in
	  let l = Expression.Symbol lvname in
	  Hashtbl.add type_and_field_name_to_lval (t,f) l ;
	  l
      | _ -> failwith "add_field_lval: Trying to add non structure type"
    end

let rec fieldmap (l : Expression.lval) = 
  match l with
    Expression.Access(Expression.Dot, e, f) -> Some (e,f)
  | Expression.Access(Expression.Arrow, e, f) -> Some (Expression.Lval (Expression.Dereference e),f)
  | Expression.Indexed(e', _) -> fieldmapexp e'
  | _ -> None
and
  fieldmapexp (e : Expression.expression) =
  match e with
    Expression.Lval l -> fieldmap l
  | _ -> None

(*****************************************************************************)
(** Code to generate new temporary variables *)

let bddPTSTONAME = "__bddptsto_mem"

let tmpVarIndex = ref (-1)

let getNextTmpVarIndex () =
  incr tmpVarIndex ; !tmpVarIndex

let resetTmpVarIndex () = tmpVarIndex := -1

let is_malloc_location key = 
  match key with
    Expression.Symbol s -> Misc.is_prefix bddPTSTONAME s
  | _ -> false


(*********************************************)
    
(** worklist contains all locations that changed in last analysis *)

(*************************************************************************)
    
(** this function initialize location_to_bdd_map and worklist I use bfs search *)
let map_nodes_fname f fname  = 
  let module LocationSet =
    Set.Make (struct
      type t = int 
      let compare x y =  compare x  y
    end) 
      in
      let already_visited_source_locations = ref LocationSet.empty in
      let rec map_loc_and_edges loc_list =
	match loc_list with
	  [] -> []
	| loc::t -> 
	    begin
	      let (_,loc_id) = C_SD.location_coords loc in
	      if (LocationSet.mem loc_id !already_visited_source_locations) 
	      then 
		(map_loc_and_edges t)
              else
		begin
		  let _ = 
		    (already_visited_source_locations := LocationSet.add loc_id !already_visited_source_locations) 
		  in
		  let children_h = List.map C_SD.get_target (C_SD.get_outgoing_edges loc) in
		  (* fixed by li. !!! if we write (f loc) :: (...) the last function will be executed first *)
		  let f_list = f loc in
		  f_list::(map_loc_and_edges (t @ children_h))
		end
	    end
      in
      map_loc_and_edges [(C_SD.lookup_entry_location fname)]

let init_bdd_location worklist =
  let node_map node =
    worklist#add_element node ;
    ()
  in
  ignore (List.map (fun fn -> map_nodes_fname (node_map) fn) (C_SD.list_of_functions ()))


let collapse_expression e =
  let rec get_ptr_lval l =
    begin
      try
	print_string "In collapse expression\n" ;
	match (C_SD.get_type_of_lval l) with
	  Cil.TPtr _ | Cil.TArray _ -> [l]
	| _ -> []
      with _ -> []
    end
  and get_ptr ex =
    match ex with
      Expression.Lval l -> get_ptr_lval l
    | Expression.Cast (_, e') -> get_ptr e'
    | Expression.Binary (Expression.FieldOffset, e1, e2) ->
	begin
	  match e1 with
	    Expression.Lval l -> get_ptr_lval l
	  | _ -> []
	end
    | Expression.Binary (op, e1, e2) -> 
	begin
	match op with
          Expression.Plus | Expression.Minus | Expression.Offset ->
          begin
	  let p1 = get_ptr e1
	  and p2 = get_ptr e2 in
	  match (p1,p2) with
	    ([], []) -> []
	  | ([a], []) -> [a]
	  | ([], [b]) -> [b]
	  | _ -> if (!debug_bdd_pts_to) then 
	      Message.msg_string  Message.Debug ("get_ptr: Too many pointers! " ^ (Expression.toString ex)); []
          end
          | _ -> []
	end
    | Expression.Unary (Expression.Address, e1) ->
	[]
    | Expression.Unary (op, e1) ->
	get_ptr e1
    | Expression.Constant _ -> []
    | Expression.Assignment _ 
    | Expression.FunctionCall _ 
    | _ -> failwith ("get_ptr : "^(Expression.toString e))
  in
  let lst = get_ptr e in
  match lst with
    [] -> e
  | [a] -> Expression.Lval a
  | _ -> failwith ("collapse expression returns too many pointers for "^(Expression.toString e))

(* this function simplify the lvalues. Structures that we can handle:
   A[i], p.f, *(&a).
   *(p + i) reduced to *p.
   Only field and index are removed. dereference info is kept. *)
let rec simplify_lval lv = 
  match lv with
    Expression.Symbol s -> lv 			
  | Expression.Access(_, e,_) -> 
	simplify_exp e
  | Expression.Dereference e ->
      (* we still need to keep the dereference info *)
      begin 
	match e with
	  Expression.Unary(Expression.Address, Expression.Lval lval) -> simplify_lval lval (* *(&e) is simplified as e *)
	| Expression.Lval lval -> Expression.Dereference (Expression.Lval (simplify_lval lval))
	| _ -> 
	    begin
	      if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug "Interesting case for dereference" ;
	      if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("Expression is "^(Expression.lvalToString lv)) ;
	      let e_ = (collapse_expression e) 
	      in
	      if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("We return * "^(Expression.toString e_)) ;
	      Expression.Dereference (e_)
	    end
      end
  | Expression.Indexed(e, _) -> 
      begin
	match e with 
	  Expression.Lval e_lv -> Expression.Dereference (Expression.Lval (simplify_lval e_lv))
		(* sth should be wrong here *)
	| _ -> lv
      end		      
 and simplify_exp e =
  match e with
   Expression.Lval l -> simplify_lval l
  | _ -> failwith ("simplify_exp: Not handled " ^ (Expression.toString e))	

(** debug functions *)
let debug_printedge source target = 
  match (C_SD.get_source_position source) with
    Some (filename, row, _) ->
      begin
	Printf.fprintf stdout "source loc at (%s:%d)\n" filename row ;
	match (C_SD.get_source_position target) with
	  Some (filename1, row1, _) ->
	    Printf.fprintf stdout "target loc at (%s:%d)\n" filename1 row1 
	| None -> () 
      end
  | None -> ()


let get_addrofs code =
  let rec get_addrofs_lval l =
    match l with
      Expression.Symbol _ -> []
    | Expression.Dereference e -> get_addrofs_exp e
    | Expression.Access (_, e, _) -> get_addrofs_exp e
    | Expression.Indexed (e1, e2) -> (get_addrofs_exp e1) @ (get_addrofs_exp e2)
  and
   get_addrofs_exp e =
    match e with
      Expression.Lval lv -> get_addrofs_lval lv
    | Expression.Chlval _ -> []
    | Expression.Assignment (_, target, exp) -> (get_addrofs_lval target) @ (get_addrofs_exp exp)
    | Expression.Binary (_, e1, e2) -> (get_addrofs_exp e1) @ (get_addrofs_exp e2)
    | Expression.Cast (_, e1) -> get_addrofs_exp e1
    | Expression.Constant _ -> []
    | Expression.Constructor _ -> []
    | Expression.FunctionCall (f1, flist) -> (get_addrofs_exp f1) @(List.flatten (List.map get_addrofs_exp flist))
    | Expression.Sizeof _ -> []
    | Expression.Unary (Expression.Address, Expression.Lval (Expression.Symbol s)) -> [ Expression.Symbol s ]
    | Expression.Unary (Expression.Address, _) -> failwith "Strange address operator"
    | Expression.Unary (_, e1) -> get_addrofs_exp e1
    | _ -> failwith "get_addrofs_exp match fails"
  in
  let rec get_addrofs_pred p =
    match p with
      Predicate.And plist 
    | Predicate.Or plist ->
	List.flatten (List.map get_addrofs_pred plist)
    | Predicate.Not notp -> get_addrofs_pred notp
    | Predicate.Atom a -> get_addrofs_exp a
    | _ -> []
  in
  let rec get_addrofs_stmt s =
    match s with 
      C_SD.Command.Expr e -> get_addrofs_exp e
    | C_SD.Command.Return _ -> [] (* there are no address-of's in returns *)
  in
  match code with
    C_SD.Command.Skip -> []
  | C_SD.Command.Block stmts -> List.flatten (List.map get_addrofs_stmt stmts)
  | C_SD.Command.Pred p -> get_addrofs_pred p
  | C_SD.Command.FunctionCall (e) -> get_addrofs_exp e
  | C_SD.Command.GuardedFunctionCall (p,e) -> (get_addrofs_pred p) @ (get_addrofs_exp e) 
  | C_SD.Command.Havoc lv -> get_addrofs_lval lv
  | _ -> failwith ("get_addrofs : code not handled") 

(** Go through all edges and put all different lvalues to lval_list. 
    For each memory allocation, we assign a new lvalue to that memory. 
    A[i] is simplied as A
    q.f ( *q).f is simplied as q
    PLUS: we keep the map from each function call to its target locations.
    *)
let fread_lvals = function edge1 -> 
  let cmd = C_SD.get_command edge1 in
  begin
		(* we read all lvalues used in this command and simplify them *)
    let tmp1 = List.append (cmd.C_SD.Command.written) (cmd.C_SD.Command.read) 
    in
    let tmp2 = get_addrofs cmd.C_SD.Command.code in 
                                   (* get_addrofs gets the lvals whose address has been taken in the code.
				      We need this to catch those variables y which only appear as x= &y
				      in the code. (note that such y's are not read :-) *)
    let all_lvals = tmp2 @ tmp1 in 
    let result_list = List.map (fun lv ->
      (* if lv is a field access, add special field lval *)
      (match lv with 
	Expression.Access(_, e', fld) -> 
	  begin
	    try
	      let t = C_SD.get_type_of_exp e' in
	      ignore (add_field_lval t fld)
	    with exn -> Message.msg_string Message.Error ("Exception: "^(Printexc.to_string exn)) ; ()
	  end
      | _ -> () ) ; 
      fullsimp_lval lv) all_lvals
    in
    (* we give each memory block a name, according to their locations *)
    match cmd.C_SD.Command.code with 
      C_SD.Command.FunctionCall(e)  -> 
	begin
	  match e with
	    Expression.FunctionCall (fnname, arglist) 
	  | Expression.Cast(_, Expression.FunctionCall (fnname, arglist))
	  | Expression.Assignment (Expression.Assign, _, Expression.FunctionCall (fnname, arglist)) 
	  | Expression.Assignment (Expression.Assign, _, Expression.Cast(_, Expression.FunctionCall (fnname, arglist))) ->
	      begin
		(* we keep a map from function call to their targets *)
		let target = C_SD.get_target edge1 in 
		match fnname with
		  Expression.Lval (Expression.Symbol s) ->
		  begin
(*
		    Hashtbl.add fcn_to_target_map s target;
*)
		    if (List.mem s allocFuns) then 
						(* we apply a new variable for this memory block *)
		      let getNewVar s =
			let i = getNextTmpVarIndex()
			in 
			Expression.Symbol( s^( string_of_int i ))
		      in 
		      let newMem = getNewVar bddPTSTONAME
		      in
		      Hashtbl.add edge_to_mem_map edge1 newMem ;
		      (newMem :: result_list)
		    else result_list 
		  end
		| _ -> result_list
	      end
	  | _ -> failwith "Strange function call"
	end
    | _ -> result_list
  end

let get_all_lvals () =
  resetTmpVarIndex () ;
  let unique_globals = Hashtbl.create 31 in
  let update_list curlist key = 
    if (not (is_malloc_location key)) && C_SD.is_array key then 
    begin
      match key with Expression.Symbol s -> key :: ((Expression.Symbol (s^"#aloc")) :: curlist) 
      | _ -> Message.msg_string Message.Debug ("Strange lvalue : " ^ (Expression.lvalToString key)); failwith "oh"
    end
    else (key :: curlist) 
  in
  let process_single_routine fnname = 
    let tmplevel2_list = C_SD.map_edges_fname fread_lvals fnname in
    let tmp_list = List.flatten tmplevel2_list in
    let get_formals fnname = 
      match (C_SD.lookup_formals fnname) with
	C_SD.Fixed flist -> List.map (fun s -> Expression.Symbol s) flist
      | C_SD.Variable vlist -> List.map (fun s -> Expression.Symbol s) vlist
    in
    let formal_list = get_formals fnname in
    let unique_lvals = Hashtbl.create 31 in


    let rec remove_dup_lvals tmp_list =     
      match tmp_list with
	[] -> ()
      | cur_lval :: rest ->
	  begin
	    if C_SD.is_global cur_lval then
	      begin
		try 
		  let _ = Hashtbl.find unique_globals cur_lval in
		  remove_dup_lvals rest
		with Not_found ->
		  Hashtbl.add unique_globals cur_lval 0;
		  remove_dup_lvals rest
	      end
	    else
	      begin
		try
		  let _ = Hashtbl.find unique_lvals cur_lval in
		  remove_dup_lvals rest
		with Not_found ->
			(* we add this to lval_list and hash table *)
		  Hashtbl.add unique_lvals cur_lval 0;
		  remove_dup_lvals rest
	      end	    
	  end
    in
    remove_dup_lvals (tmp_list @ formal_list );
    (* for each variable of type array, add the additional array location aloc *)
    let result_list = Hashtbl.fold 
      (fun key data curlist -> update_list curlist key)
      unique_lvals []
    in
(*
    Hashtbl.add fcn_to_lvalue_map fnname result_list;
*)
    result_list
  in
  let local_list = List.flatten (List.map process_single_routine (C_SD.list_of_functions ())) 
  in
  let global_list = Hashtbl.fold (fun key data curlist -> update_list curlist key) unique_globals []
  in
  let field_lval_list = Misc.hashtbl_data type_and_field_name_to_lval 
  in
  (field_lval_list, global_list, local_list, (List.map (fun s -> Expression.Symbol s) (C_SD.list_of_functions ())  ))

(*****************************************************************************)    


(*** code to map variables to bdd variables *)    

(** make source to target/target to source maps we will use later
    they should be global *)
let stvarMap = ref []
let tsvarMap = ref []
let num_bddVar = ref 0 

(** the encoding can be kept an array of bdd variable nodes. initially ^x1^x2...^xn
    Note we need two sets of bdd variables since we represent p->r 
    their indexes are kept in code1_index and code2_index*)
let encode_variables num_bddvars lval_list =   
    let cur_code = Array.make num_bddvars 0 in
    let cur_encode = Array.make num_bddvars bddZero in
    let code1_index = Array.make num_bddvars 0 in
  
  let rec go_through_list l_list =
    match l_list with
      [] -> ()
    | lvalu :: rest ->
                    (* encode this variable *)
                    (* a tutorial for me. how to use pointers in ocaml *)
	let cur_encoding = ref (Array.get cur_encode 0) in
	for i = 1 to num_bddvars - 1 do
	  cur_encoding := CaddieBdd.bddAnd !cur_encoding cur_encode.(i)
	done ;
	(* debug 
	if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug (
	Printf.sprintf "lvalue %s is encoded as \n"  
	(Expression.lvalToString lvalu));
	CaddieBdd.bddPrint !cur_encoding; *)
	Hashtbl.add lvalue_to_bdd_map lvalu !cur_encoding ;	
	            (* so we don't have break in ocaml *)
	let rec update_current_encoding i =
	  if(i>=0) then
            begin
	      cur_code.(i) <- 1 - cur_code.(i);
	      cur_encode.(i) <- CaddieBdd.bddNot(cur_encode.(i));
	      if cur_code.(i)==1 then
		()
	      else update_current_encoding (i-1)
	    end
	in			
	update_current_encoding (num_bddvars - 1) ;
		     (* after that, continue with the rest of the list *)
	go_through_list rest
  in 
  begin 
	(* initialize lvalues encoding *)
    for i = 0 to num_bddvars - 1 do
      cur_code.(i) <- 0;
      let tmpbdd =  CaddieBdd.bddNot (CaddieBdd.bddNewVar bddManager) in
      cur_encode.(i) <- tmpbdd;
      code1_index.(i) <- CaddieBdd.bddIndex tmpbdd
    done;
    go_through_list lval_list;
	
        (* encode the second set of bdd variables, they will be used for q in p->q *)
    let code2_index = Array.make num_bddvars 0 in
    for i =0 to num_bddvars - 1 do
      let tmpbdd = CaddieBdd.bddNewVar bddManager in
      code2_index.(i) <- CaddieBdd.bddIndex tmpbdd
    done;

        (* build up two mapping lists *)
    for i = 0 to num_bddvars -1 do
      stvarMap := (code1_index.(i), code2_index.(i)) :: !stvarMap;
      tsvarMap := (code2_index.(i), code1_index.(i)) :: !tsvarMap
    done
  end

(**********************************************************************************************)

(** point to analysis *)

    (* Find the bdd corresponding to the lvalue lv in the BDD bdd_loc *)
let rec get_bdd_expensive bdd_loc lv =
  match lv with 
    Expression.Symbol _ -> 
      let b = Hashtbl.find lvalue_to_bdd_map lv in
      b
  |	Expression.Dereference e -> 
      let lv_pts_to = CaddieBdd.bddCofactor bdd_loc (get_bdd_e_expensive bdd_loc e)
      in
      if (!debug_bdd_pts_to) then 
	begin
	  Message.msg_string  Message.Debug ("Points to set:" );
	  CaddieBdd.bddPrint lv_pts_to 
	end;
      let b = CaddieBdd.replace lv_pts_to (CaddieBdd.toMap bddManager !stvarMap) in
      if (!debug_bdd_pts_to) then 
	begin
	  Message.msg_string  Message.Debug ("In terms of original vars:" );
	  CaddieBdd.bddPrint b 
	end;
      b
  |	Expression.Access(Expression.Dot, e, _) ->
      get_bdd_e_expensive bdd_loc e
  |	Expression.Indexed (e1, _) ->
      get_bdd_e_expensive bdd_loc e1
and
    get_bdd_e_expensive bdd_loc e =
  match e with
    Expression.Lval l -> get_bdd_expensive bdd_loc l
  |  Expression.Binary (Expression.Plus, e1, e2)
  |  Expression.Binary (Expression.Minus, e1, e2) 
  |  Expression.Binary (Expression.Offset, e1, e2) ->
	begin
      (* RUPAK: TBD: Unsound *)
	  get_bdd_e_expensive bdd_loc e1
	end
  |	Expression.Unary (Expression.Address, _)
  |	_ -> failwith ("checkAliasFI: cannot handle "^(Expression.toString e))

	

(** This function Or the bdd function at source to the bdd function at target *)
    (* Find the bdd corresponding to the lvalue lv in the BDD bdd_loc *)
let get_bdd bdd_loc lv =
  if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("In get_bdd : lv is "^(Expression.lvalToString lv)) ;
  match lv with 
    Expression.Symbol _ -> 
      let b = Hashtbl.find lvalue_to_bdd_map lv in
      b
  |	Expression.Dereference e -> 
     begin
      if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("Dereference" );
      let bdd_e = match e with
        Expression.Lval (Expression.Symbol s) -> Hashtbl.find lvalue_to_bdd_map (Expression.Symbol s)
      | _ -> get_bdd_e_expensive bdd_loc e
      in
      let lv_pts_to = CaddieBdd.bddCofactor bdd_loc (bdd_e)
      in
      if (!debug_bdd_pts_to) then 
	begin
	  Message.msg_string  Message.Debug ("Points to set:" );
	  CaddieBdd.bddPrint lv_pts_to 
	end;
      let b = CaddieBdd.replace lv_pts_to (CaddieBdd.toMap bddManager !stvarMap) in
      if (!debug_bdd_pts_to) then 
	begin
	  Message.msg_string  Message.Debug ("In terms of original vars:" );
	  CaddieBdd.bddPrint b 
	end;
      b
    end
  |	Expression.Access(Expression.Dot, Expression.Lval (Expression.Symbol s), _) ->
      Hashtbl.find lvalue_to_bdd_map (Expression.Symbol s)
  |	Expression.Access(Expression.Dot, Expression.Lval (Expression.Dereference (Expression.Lval (Expression.Symbol s))), _) ->
      let b = Hashtbl.find lvalue_to_bdd_map (Expression.Symbol s) in
      let lv_pts_to = CaddieBdd.bddCofactor bdd_loc b in
      let b' = CaddieBdd.replace lv_pts_to (CaddieBdd.toMap bddManager !stvarMap) in
      b'
  |	Expression.Access(Expression.Dot, e, _) ->
      get_bdd_e_expensive bdd_loc e
  |	Expression.Indexed (e1, _) ->
      get_bdd_e_expensive bdd_loc e1
	

(** This function Or the bdd function at source to the bdd function at target *)
let inherit_st target new_bdd_at_target worklist =
  try
    let bdd_at_target = lookup_location target in
    if (CaddieBdd.bddEqual (CaddieBdd.bddImp new_bdd_at_target bdd_at_target) bddOne) then
      ()
    else
      begin
	(* if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug "BDD Changed!" ; *)
	let target_bdd = CaddieBdd.bddOr bdd_at_target new_bdd_at_target in
	Hashtbl.remove location_to_bdd_map (C_SD.location_coords target) ;
	Hashtbl.replace location_to_bdd_map (C_SD.location_coords target) target_bdd ;
	worklist#add_element target;
	()
      end
  with Not_found ->
    (* something is wrong *)
    failwith ( "location not in hash table ")
      
      
(** This function is for p= (arbitrary) lval statement. Note bdd_target will possibly be changed in this function *)
let anyp_anyq monotonic bdd_source p q =
    try
      let bdd_pcode = get_bdd bdd_source p in 
      let bdd_qcode = get_bdd bdd_source q in
		(* get rid of p->y *)
      let elim_py = if monotonic then bdd_source else
                    CaddieBdd.bddAnd bdd_source (CaddieBdd.bddNot bdd_pcode) in
		(* get x *)
      let qx = CaddieBdd.bddCofactor elim_py bdd_qcode in
		(* get p->x *)
      let px = CaddieBdd.bddAnd bdd_pcode qx in
Message.msg_string Message.Debug "new result. adds" ;
CaddieBdd.bddPrint px ;
      let new_result = CaddieBdd.bddOr elim_py px in
      new_result
    with Not_found ->
      failwith ( " sth not found in any_p=any_q " ^ (Expression.lvalToString p) ^ (Expression.lvalToString q))
    | e -> begin print_string (Printexc.to_string e) ; failwith "p=q aha" end

	
(** This function is for p = &q statement. Note bdd_target will possibly be changed in this function *)
let anyp_addr_anyq monotonic bdd_source p q =
	(* p=&q. Get rid of any p->y. p->q *)
    try 
      let bdd_pcode = get_bdd bdd_source p in
      let bdd_qcode = get_bdd bdd_source q in
		(* get rid of p->y *)
      let elim_py = if monotonic then bdd_source else CaddieBdd.bddAnd bdd_source (CaddieBdd.bddNot bdd_pcode) in
		(* map q to the target set of variables *)
      let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
      let pq = CaddieBdd.bddAnd bdd_pcode qtarget in
      let new_result = CaddieBdd.bddOr elim_py pq in
      new_result
    with Not_found ->
      failwith ( " sth not found in p=&q") 
    | e -> begin print_string (Printexc.to_string e) ; failwith "p = &q aha" end

(** This function is for p = &q statement. Note bdd_target will possibly be changed in this function *)
let p_addrq monotonic bdd_source p q =
	(* p=&q. Get rid of any p->y. p->q *)
    try 
      let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
      let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in
		(* get rid of p->y *)
      let elim_py = if monotonic then bdd_source else CaddieBdd.bddAnd bdd_source (CaddieBdd.bddNot bdd_pcode) in
		(* map q to the target set of variables *)
      let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
      let pq = CaddieBdd.bddAnd bdd_pcode qtarget in
      let new_result = CaddieBdd.bddOr elim_py pq in
      new_result
    with Not_found ->
      failwith ( " sth not found in p=&q") 
    | e -> begin print_string (Printexc.to_string e) ; failwith "p = &q aha" end
	
	
(** This function is for *p = &q statement. Note bdd_target will possibly be changed in this function *)
let derefp_addrq monotonic bdd_source p q = 
	(* *p=&q. if p->y, then check if y is unique. if so, get rid of y->z. add y->q *)
    try 
      if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("Now performing assignment *"^(Expression.lvalToString p)^" = &"^(Expression.lvalToString q)) ;
      let bdd_pcode = try Hashtbl.find lvalue_to_bdd_map p with Not_found -> print_string (Expression.lvalToString p) ; raise Not_found in
      let bdd_qcode = try Hashtbl.find lvalue_to_bdd_map q with Not_found -> print_string (Expression.lvalToString q) ; raise Not_found in 
      let bdd_py = CaddieBdd.bddAnd bdd_source bdd_pcode in 
		(* get y *)
      let y = CaddieBdd.bddCofactor bdd_py bdd_pcode in
      let ysource = CaddieBdd.replace y (CaddieBdd.toMap bddManager !tsvarMap) in
		(* map q to target set of variables *)
      let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
		(* compute y->q *)
      let yq = CaddieBdd.bddAnd ysource qtarget in
      let py_num = if monotonic then 0 else int_of_float(CaddieBdd.bddCountMinterm bddManager bdd_py (2*(!num_bddVar))) in
      if (py_num = 1) then
	begin
		(* get rid of y->z *)
	  let elim_yz = CaddieBdd.bddAnd bdd_source (CaddieBdd.bddNot ysource) in
	  let new_result = CaddieBdd.bddOr elim_yz yq in
	  new_result
	end
      else
	begin 
	  let new_result = CaddieBdd.bddOr bdd_source yq in
	  new_result
	end
    with Not_found ->
	  failwith ( " sth not found in *p=&q") 
    | e -> begin print_string (Printexc.to_string e) ; failwith "*p = &q aha" end
	    
(** This function is for p (or *p) = alloc(...). Note bdd_target will possibly be changed in this function *)
let assign_alloc monotonic bdd_source ptarget qalloc = 

    Message.msg_string Message.Debug "In assign_alloc " ;
    Message.msg_string Message.Debug ("ptarget is "^(Expression.lvalToString ptarget)) ;
    
    try 
      let bdd_pcode = get_bdd bdd_source ptarget (* Hashtbl.find lvalue_to_bdd_map (ptarget) *) in
      let bdd_qcode = Hashtbl.find lvalue_to_bdd_map qalloc in
      match (simplify_lval ptarget) with
      Expression.Symbol p  -> 	
      (* p = alloc(..), then get rid of p->x. add p->qalloc *)
	begin
	  if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug "p = alloc ()" ; 
		(* get rid of p->x *)
	  let elim_px =  if monotonic then bdd_source else CaddieBdd.bddAnd bdd_source (CaddieBdd.bddNot bdd_pcode) in 
		(* map qalloc to target variables *)
	  let qtarget =  
	    CaddieBdd.replace 
	      bdd_qcode 
	      (CaddieBdd.toMap bddManager !stvarMap) 
	  
	  in
		(* compute p->qalloc *)
	  let pq = CaddieBdd.bddAnd bdd_pcode qtarget in
	  let new_result = CaddieBdd.bddOr elim_px pq in
	  new_result
	end
      | Expression.Dereference (Expression.Lval (Expression.Symbol p)) -> 
      (* *p=alloc(...), then if p->x, then check if x is unique. if so, get rid of x->r. add x->qalloc *)
	begin
	  if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug "*p = alloc ()" ; 
          CaddieBdd.bddPrint bdd_pcode;
		(* map qalloc to target variables *)
	  let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
          if (!debug_bdd_pts_to) then CaddieBdd.bddPrint qtarget;
          let xq = CaddieBdd.bddAnd bdd_pcode qtarget in
          if (!debug_bdd_pts_to) then CaddieBdd.bddPrint xq;
	  begin
	    let new_result = CaddieBdd.bddOr bdd_source xq in
	    new_result
	  end
	end
      | _ -> Message.msg_string Message.Debug 
	    ("Do not handle this yet. To do." ^ (Expression.lvalToString ptarget))  ; failwith "alloc"
    with Not_found ->
      Message.msg_string Message.Debug 
	( (" sth not found in alloc "^ (Expression.lvalToString ptarget) ^ (Expression.lvalToString qalloc))  ) ; 
      failwith "alloc'"
	

let rec do_assignment monotonic bdd ptarget qvalue =
    (* Statement: ptarget = qvalue *)
  
			(* we have  types of assignments: p=q(or q[i]); p=*q; p=&q; *p=q(or q[i]); *p=*q; *p=&x; *)
			(* first match lhs *)
  let p_lv = simplify_lval ptarget in
  if !debug_bdd_pts_to then begin
    Message.msg_string Message.Debug ("ptarget is "^(Expression.lvalToString ptarget)) ;
    Message.msg_string Message.Debug ("p_lv is "^(Expression.lvalToString p_lv)) ;
  end ;
  let newbdd =   
    match p_lv with
      (* ptarget *) Expression.Symbol p  ->
	begin
	  match qvalue with
	  |  (* qvalue *) Expression.Lval lv 
	  |  (* qvalue *) (Expression.Cast (_, Expression.Lval lv)) ->
	      begin
		Message.msg_string Message.Debug "This case." ;
		let q_lv = simplify_lval lv in
		anyp_anyq monotonic bdd p_lv q_lv
	      end
	  | (* qvalue *) Expression.Unary (Expression.Address, e) 
	  | (* qvalue *) Expression.Cast (_, Expression.Unary (Expression.Address, e)) ->
	      begin
		match e with 
		  Expression.Lval lv -> (* case p = &q *)
		    let s_lv = simplify_lval lv in
		    begin
		      match s_lv with
			Expression.Symbol _ ->
			  Message.msg_string Message.Debug ("p = & q with q : " ^ (Expression.lvalToString s_lv));
		          p_addrq monotonic bdd p_lv s_lv 
		      | Expression.Dereference e -> 
                          (* case p = & ( *e) I have simplified ptarget once, no need to do it again*)
			  do_assignment monotonic bdd p_lv e
                      | _ -> failwith "do_assignment: address_of : Strange expression"
		    end
		| _ -> failwith ("do_assignment: Not handled q = "^(Expression.toString e))
	      end
	  | (* qvalue *) Expression.Binary (Expression.FieldOffset, e1, e2) -> (* assume no pointer arithmetic *)
	      begin
		do_assignment monotonic bdd p_lv e1
	      end
	  | (* qvalue *) Expression.Binary (op, e1, e2) (* assume no pointer arithmetic *)
	  | (* qvalue *) Expression.Cast(_, Expression.Binary (op, e1, e2)) -> 
	      begin
		if (!debug_bdd_pts_to) then 
		  Message.msg_string  Message.Debug ("qvalue Binary "^(Expression.toString qvalue)) ;
		let simp_e = collapse_expression qvalue in
		if simp_e = qvalue then bdd else 
		begin
		  if (!debug_bdd_pts_to) then 
		    Message.msg_string  Message.Debug ("Doing assignment on "^(Expression.toString simp_e)) ;
		  do_assignment monotonic bdd p_lv simp_e
		end
	      end
	  | (* qvalue *) Expression.Constant _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Sizeof _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Unary _ -> bdd
	  | _ -> failwith ("do_assignment : do not handle" ^ (Expression.toString qvalue))
	end
    | (* ptarget *) Expression.Dereference (Expression.Lval (Expression.Symbol p)) -> (* LHS is *p *)
	begin
	  match qvalue with
	  |   Expression.Lval lv 
	  |   (Expression.Cast(_, Expression.Lval lv)) ->
	      begin
		let q_lv = simplify_lval lv in
		anyp_anyq monotonic bdd p_lv q_lv
	      end
	  | Expression.Unary (Expression.Address, e) 
	  | Expression.Cast(_, Expression.Unary (Expression.Address, e)) ->
	      begin
		match e with 
		  Expression.Lval lv -> (* case p = &q *)
		    let s_lv = simplify_lval lv in
		    begin
		      match s_lv with
			Expression.Symbol _ ->
		          derefp_addrq monotonic bdd (Expression.Symbol p) s_lv 
		      | Expression.Dereference e -> 
                          (* case *p = & ( *e) I have simplified ptarget once, no need to do it again*)
			  do_assignment monotonic bdd p_lv e
                      | _ -> failwith "do_assignment: address_of : Strange expression"
		    end
		| _ -> failwith ("do_assignment: Not handled q = "^(Expression.toString e))
	      end
	  | (* qvalue *) Expression.Binary (Expression.FieldOffset, e1, e2) -> (* assume no pointer arithmetic *)
	      begin
	      do_assignment monotonic bdd p_lv e1
	      end
	  | (* qvalue *) Expression.Binary (op, e1, e2) -> (* assume no pointer arithmetic *)
	      begin
		if (!debug_bdd_pts_to) then 
		  Message.msg_string  Message.Debug ("qvalue Binary "^(Expression.toString qvalue)) ;
		let simp_e = collapse_expression qvalue in
		if simp_e = qvalue then bdd else 
		begin
		  do_assignment monotonic bdd p_lv simp_e
		end
	      end
	  | (* qvalue *) Expression.Constant _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Sizeof _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Unary _ -> bdd 
	  | _ -> failwith ("do_assignment : do not handle"^(Expression.toString qvalue))
	end
    | (* ptarget *) _ -> 
	begin
	  match qvalue with
	  |   Expression.Lval lv 
	  |   (Expression.Cast(_, Expression.Lval lv)) ->
	      begin
		let q_lv = simplify_lval lv in
		anyp_anyq monotonic bdd p_lv q_lv
	      end
	  | Expression.Unary (Expression.Address, e) 
	  | Expression.Cast(_, Expression.Unary (Expression.Address, e)) ->
	      begin
		match e with 
		  Expression.Lval lv -> (* case p = &q *)
		    anyp_addr_anyq monotonic bdd p_lv lv
		| _ -> failwith ("do_assignment: Not handled q = "^(Expression.toString e))
	      end
	  | (* qvalue *) Expression.Binary (Expression.FieldOffset, e1, e2) -> (* assume no pointer arithmetic *)
	      begin
		do_assignment monotonic bdd p_lv e1
	      end
	  | (* qvalue *) Expression.Binary (op, e1, e2) -> (* assume no pointer arithmetic *)
	      begin
		if (!debug_bdd_pts_to) then 
		  Message.msg_string  Message.Debug ("qvalue Binary "^(Expression.toString qvalue)) ;
		let simp_e = collapse_expression qvalue in
		if simp_e = qvalue then bdd else 
		begin
		  if (!debug_bdd_pts_to) then 
		    Message.msg_string  Message.Debug ("Doing assignment on "^(Expression.toString simp_e)) ;
		  do_assignment monotonic bdd p_lv simp_e
		end
	      end
		
	  | (* qvalue *) Expression.Constant _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Sizeof _ -> (* do nothing *)
	      bdd
	  | (* qvalue *) Expression.Unary _ -> bdd 
	  | _ -> failwith ("do_assignment : do not handle"^(Expression.toString qvalue))
	end 
  in
  (* now add field sensitivity *)
  match (fieldmap ptarget, fieldmapexp qvalue) with
    (Some (e,f), Some (e', f')) ->
      begin
	let l1 = lookup_field_lval e f in
	let l2 = lookup_field_lval e' f' in
	let newbdd' =  do_assignment monotonic newbdd l1 qvalue in
	do_assignment monotonic newbdd' l1 (Expression.Lval l2)
      end
  | (Some (e,f), None) ->
      let l1 = lookup_field_lval e f in
      do_assignment monotonic newbdd l1 qvalue
  | _ -> newbdd
	
	  

(****************************************************************************)
(****************************************************************************)
(****************************************************************************)
(** A flow insensitive version of points to analysis *)
(****************************************************************************)
(****************************************************************************)

let bdd_pts_to_flow_insensitive () =
  let (flval_list, global_list, local_list, fn_list) = get_all_lvals () in 
  let all_but_field_list = global_list @ local_list @ fn_list in
  let lval_list = flval_list @ all_but_field_list in
  Message.msg_string Message.Debug "Got all Lvals" ;
  List.iter (fun a -> Message.msg_string Message.Debug ("Fields " ^(Expression.lvalToString a)) ) flval_list ;
(*
  List.iter (fun a -> print_string ("global " ^(Expression.lvalToString a)) ) global_list ;
  List.iter (fun a -> print_string ("local " ^(Expression.lvalToString a)) ) local_list ;
  print_string "funs\n" ;
  List.iter (fun a -> print_string (Expression.lvalToString a)) fn_list ;
*)
  (* how many bdd variables we need to encode all lvalues *)
  let num_bddvars_lval lval_list = int_of_float (ceil ((log (float_of_int (List.length lval_list))) /. (log 2.)))
  in
  let num_bddvars = num_bddvars_lval lval_list in
  Message.msg_string Message.Debug ("Number of BDD variables = "^(string_of_int num_bddvars)) ;
  encode_variables num_bddvars lval_list ;
    Hashtbl.iter (fun key b -> Message.msg_string Message.Debug (Expression.lvalToString key) ;    
      if (!debug_bdd_pts_to) then (CaddieBdd.bddPrint b)) lvalue_to_bdd_map ;
  num_bddVar := num_bddvars;
  let initialize_arrays lvals =
	(* for each array variable a, set the mapping a -> a#aloc *)
     let rec __loop l bdd = match l with [] -> bdd
     | a :: rest -> 
        match a with Expression.Symbol s ->
          if (not (Misc.is_suffix "#aloc" s) && (not (is_malloc_location a)) && (C_SD.is_array a)) then 
          begin
                let bdd_pcode = Hashtbl.find lvalue_to_bdd_map a in
                let bdd_qcode = Hashtbl.find lvalue_to_bdd_map (Expression.Symbol (s^"#aloc") ) in
                let qtarget =  CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap)
                in
                (* compute p->qalloc *)
                let pq = CaddieBdd.bddAnd bdd_pcode qtarget in
                let new_result = CaddieBdd.bddOr bdd pq in
                __loop rest new_result
          end
          else __loop rest bdd
        | _ -> Message.msg_string Message.Debug 
	      ("Strange lvalue in initialize_array"^(Expression.lvalToString a)) ; 
	    failwith "ooh" 
     in
     __loop lvals bddZero
  in
  let initialize_fns b = 
    (* for each function, put a self loop *)
    List.fold_left (fun bdd thisfn -> 
      let bdd_pcode = Hashtbl.find lvalue_to_bdd_map thisfn in
      let bdd_target = CaddieBdd.replace bdd_pcode (CaddieBdd.toMap bddManager !stvarMap) in
      CaddieBdd.bddOr bdd (CaddieBdd.bddAnd bdd_pcode bdd_target)
      ) b fn_list 
  in
  let initialize_arrays_and_fns lval_list =
    let b1 = initialize_arrays lval_list in
    initialize_fns b1 
  in
  let initial_bdd = initialize_arrays_and_fns all_but_field_list in
  let ptstobdd = ref initial_bdd in
  let newptstobdd = ref initial_bdd in
  Message.msg_string Message.Debug "bdd_pts_to: Starting iteration.\n";
  
  (* do alias analysis on all branches until there is no change *)
  let changed = ref true in 
  while !changed do
    ptstobdd := !newptstobdd ;

    (* Process one edge *)
    let process_one_edge bdd_at_cur_loc edge  = 
      let bdd_at_target = bdd_at_cur_loc in
      let cmd = C_SD.get_command edge in
      match cmd.C_SD.Command.code with
      | C_SD.Command.Block l ->
	(* this is the interesting case -- *)
	  begin
             (* analyze_statement takes a points to bdd and a statement (assignment or return)
		and updates the points to set for the effect of the statement 
		*)
	    let analyze_statement bdd stmt = 
	      match stmt with
		C_SD.Command.Expr e ->
		  begin
		    match e with
		      Expression.Assignment(Expression.Assign, ptarget, qvalue) ->
			begin
			  do_assignment true bdd ptarget qvalue
			end
		    | _ -> failwith "Analyze_statement: this case not handled"
		  end
	      | C_SD.Command.Return _ -> 
		  bdd
	    in
	    List.fold_left (analyze_statement) bdd_at_cur_loc l
	      
	  end
      | C_SD.Command.FunctionCall fexp ->
	  begin
	    match fexp with
	      Expression.FunctionCall (Expression.Lval (Expression.Symbol fname), arglist) 
	    | Expression.Cast (_, Expression.FunctionCall(Expression.Lval (Expression.Symbol fname), arglist)) ->
		begin
		  try
		    match (C_SD.lookup_formals fname) with
		      C_SD.Fixed formals ->
			begin
			  let nameAssocList = 
			    try
			      List.combine 
				(List.map (function s -> Expression.Symbol s) formals)
				arglist
			    with _ -> failwith "Incorrect number of parameters in call"
			  in
			  let bdd_at_entry_loc = bdd_at_cur_loc
			  in
			  let increase_bdd_at_entry_loc = 
			    List.fold_left (function bdd -> function (lv, e) -> do_assignment true bdd lv e) 
			      bdd_at_cur_loc nameAssocList 
			  in
			  let new_bdd_at_entry_loc = CaddieBdd.bddOr increase_bdd_at_entry_loc bdd_at_entry_loc in
			  new_bdd_at_entry_loc
			end
		    | _ -> 
			bdd_at_cur_loc 
			  
		(* the following exception deals with functions that are not found *)
		  with _ -> bdd_at_cur_loc 
		end
	    | Expression.Assignment 
		(Expression.Assign, retval, 
		 Expression.FunctionCall(Expression.Lval (Expression.Symbol fname), arglist)) 
	    | Expression.Assignment 
		(Expression.Assign, retval, 
		 (Expression.Cast (_, Expression.FunctionCall(Expression.Lval (Expression.Symbol fname), arglist)))) ->
		   
		   begin
		     try
		       if (List.mem fname allocFuns) then
	                 (* alloc functions are handled specially *)
			 begin
			   let qalloc = Hashtbl.find edge_to_mem_map edge in
			   let new_bdd_at_target = assign_alloc true bdd_at_cur_loc retval qalloc in
			   new_bdd_at_target 
			 end
		       else
			 begin
			   match (C_SD.lookup_formals fname) with
			     C_SD.Fixed formals ->
			       begin
				 let nameAssocList = 
				   try
				     List.combine 
				       (List.map (function s -> Expression.Symbol s) formals)
				       arglist
				   with _ -> failwith "Incorrect number of parameters in call"
				 in
				 let bdd_at_entry_loc = bdd_at_cur_loc
				 in
				 let increase_bdd_at_entry_loc = 
				   List.fold_left (function bdd -> function (lv, e) -> do_assignment true bdd lv e) 
				     bdd_at_cur_loc nameAssocList 
				 in		
				 let new_bdd_at_entry_loc = CaddieBdd.bddOr increase_bdd_at_entry_loc bdd_at_entry_loc in 
				 
				 
	                         (* get the assignment for retval *)
 				 let returnlvalue = Expression.Lval(Expression.Symbol("__retres@"^fname)) in
				 let new_bdd_at_target = do_assignment true new_bdd_at_entry_loc retval returnlvalue in
				 new_bdd_at_target
			       end
			   | _ -> failwith "Do not handle variable parameters"
			 end
			   
		     with C_SD.NoSuchFunctionException f -> 
		       if !debug_bdd_pts_to then 
			 Message.msg_string Message.Debug ("Warning: Function "^f^" not found") ; 
		       bdd_at_cur_loc 
		     | _ -> bdd_at_cur_loc (* THIS IS WRONG *)
		   end
	    | _ -> bdd_at_cur_loc  
	  end
      | _ -> (* other things are uninteresting *)
	  bdd_at_cur_loc 
    in
    (* now do the points to analysis by trawling all edges and applying the
       appropriate rules
       *)
    newptstobdd := List.fold_left (fun b -> fun c -> CaddieBdd.bddOr b c) 
	(!ptstobdd) (C_SD.map_edges (process_one_edge (!ptstobdd) )) ;
    changed := not (CaddieBdd.bddEqual (!ptstobdd) (!newptstobdd) ) 
  done ;
  (* at the end of the loop, ptstobdd contains (a reference to) the points to relation *)
  if (!debug_bdd_pts_to) then 
    begin
      CaddieBdd.bddPrint !ptstobdd ;
      Hashtbl.iter (fun key b -> Message.msg_string Message.Debug (Expression.lvalToString key) ; 
	CaddieBdd.bddPrint b) lvalue_to_bdd_map
    end ;
  !ptstobdd

(****************************************************************************************)
(* Lookup functions                                                                     *)
(* and query engine                                                                     *)
(*                                                                                      *)
(* Flow insensitive versions:
   checkAliasFI bdd lval1 lval2
   checkPointsToFI   bdd lval1 lval2
   getAllAliasesFI lval

   Flow sensitive versions:
   checkAliasFS loctbl loc lval1 lval2
   checkPointsToFS   loctbl loc lval1 lval2
   getAllAliasesFS lval
*)
(****************************************************************************************)
let rec get_pts_to bdd_loc (p : Expression.lval) =
  if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("In get_pts_to : p is "^(Expression.lvalToString p)) ;
  let addr_p =  (Expression.addressOf (Expression.Lval p)) in
  if (!debug_bdd_pts_to) then Message.msg_string  Message.Debug ("In get_pts_to : &p is "^(Expression.toString (addr_p))) ;
  match addr_p with
    Expression.Lval l -> 
      let bdd_pcode = get_bdd bdd_loc l in
		(* get y in p->y *)
      let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
      y
  |	Expression.Unary (Expression.Address, ae) ->
      begin
	match ae with
	  Expression.Lval l ->
	    begin
	      let bdd_pcode = get_bdd bdd_loc l in
	      CaddieBdd.replace bdd_pcode (CaddieBdd.toMap bddManager !stvarMap)
	    end
	| _ -> failwith ("get_pts_to: Unary Address of strange expression "^(Expression.toString ae))
      end
  |	Expression.Binary (Expression.FieldOffset, e1, e2) ->
      begin
	match e1 with
	  Expression.Lval l1 ->
	    let bdd_l = get_bdd bdd_loc l1 in
	    CaddieBdd.bddCofactor bdd_loc bdd_l
	| Expression.Unary (Expression.Address, Expression.Lval l) ->
	    let bdd_pcode = get_bdd bdd_loc l in
	    CaddieBdd.replace bdd_pcode (CaddieBdd.toMap bddManager !stvarMap)
        | Expression.Binary (Expression.Plus, _ , _) 
        | Expression.Binary (Expression.Minus, _ , _)
        | Expression.Binary (Expression.Offset, _ , _) ->
            begin
                 let l = collapse_expression e1 in 
                 match l with Expression.Lval ll ->
                   let bdd_pcode = get_bdd bdd_loc ll in
                   let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
                   y
                 | _ -> Message.msg_string Message.Error ("Binary case inside foffset fails! "^(Expression.toString addr_p)) ; failwith "binary"
               end
              
	| _ -> get_pts_to bdd_loc (Expression.Dereference e1) 
      end
  
  | Expression.Binary (Expression.Plus, e1, e2) 
  | Expression.Binary (Expression.Minus, e1, e2) 
  | Expression.Binary (Expression.Offset, e1, e2) -> 
      begin
(*
  Message.msg_string Message.Debug "Case: binary" ;
  Message.msg_string Message.Debug ("addr_p : " ^ (Expression.toString addr_p)) ;
  *)
	let l = collapse_expression addr_p in
        match l with Expression.Lval ll ->
          let bdd_pcode = get_bdd bdd_loc ll in
          let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
          y
        | Expression.Binary (op, _, _) 
            when (op = Expression.Plus || op = Expression.Minus || op = Expression.Offset ) ->
            begin
	      Message.msg_string Message.Debug ("Binary case fails with binop! " ^(Expression.toString l)) ; failwith "binary"
            end
        | _ -> Message.msg_string Message.Error ("Binary case fails! " ^(Expression.toString l)) ; failwith "binary"
	      
      end
  |	_ -> Message.msg_string Message.Debug ("get_pts_to : Strange address of expression: "^(Expression.lvalToString p)) ; failwith "bin"
	
	
(****************************************************************************************)

let checkAliasFI bdd_loc (p : Expression.lval) (q : Expression.lval) =
  try
    if (!debug_bdd_pts_to) then
      begin
	Message.msg_string Message.Debug ("checking Alias : "^(Expression.lvalToString p)^ " " ^(Expression.lvalToString q)) 
      end;
    let y = get_pts_to bdd_loc p and
		(* get x in q->x *)
	x = get_pts_to bdd_loc q 
    in
    if !debug_bdd_pts_to then 
      begin
	Message.msg_string  Message.Debug "x is " ;
	CaddieBdd.bddPrint x ;
	Message.msg_string  Message.Debug "y is " ;
	CaddieBdd.bddPrint y 
      end ;
    
		(* get intersection of x and y *)
    let xy = CaddieBdd.bddAnd y x in
    if (!debug_bdd_pts_to) then 
      begin
	Message.msg_string Message.Debug "x & y is " ;
	CaddieBdd.bddPrint xy ;
      end; 
    if (CaddieBdd.bddEqual xy (bddZero )) then
      false
    else
      true
  with
    excn -> if !debug_bdd_pts_to then 
      Message.msg_string Message.Error 
	("Warning:Error raised in checkAliasFI!:"^(Printexc.to_string excn) ^ " for inputs " 
	 ^ (Expression.lvalToString p) ^ " and " ^ (Expression.lvalToString q) ) ; 
      false
	
let checkPointsToFI bdd_loc (p : Expression.lval) (q : Expression.lval) = 
  begin
      try 
	let bdd_pcode = get_pts_to bdd_loc p in
	let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in
	let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
    if !debug_bdd_pts_to then 
      begin
	Message.msg_string  Message.Debug "p is " ;
	CaddieBdd.bddPrint bdd_pcode ;
        Message.msg_string Message.Debug ("q: " ^ (Expression.lvalToString q));
	Message.msg_string  Message.Debug "q is " ;
	CaddieBdd.bddPrint bdd_qcode ; 
	Message.msg_string  Message.Debug "q (primed) is " ;
	CaddieBdd.bddPrint qtarget ; 
      end ;
        let xy = (CaddieBdd.bddAnd bdd_pcode qtarget) in 
    if (!debug_bdd_pts_to) then 
      begin
	Message.msg_string Message.Debug "x & y is " ;
	CaddieBdd.bddPrint xy ;
      end; 
	begin
	  if (CaddieBdd.bddEqual xy bddZero) then
	    false
	  else
	    true
	end
      with Not_found ->
	failwith ( "sth not found in checkPointsToFI")
    end

let getAllAliasesFI bdd_loc p =
  begin
    let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
    let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in (* this is the set of lvals
							  that p points to 
							 *)
    let allaliases = CaddieBdd.bddCofactor bdd_loc y in
    (* now iterate over all lvalues. Is there a better way? *)
    [] (* TO DO *)
  end

(**********************************************************************)
(* Various querying and printing utilities                            *)
(**********************************************************************)

let alias_pointsto_flow_insensitive opfile1 opfile2 bdd_loc p q = 
  if (p = q) then
    ()
  else
    begin
      try 
	let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
	let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in
	let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
	let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
	begin
	  if (CaddieBdd.bddEqual (CaddieBdd.bddAnd y qtarget) bddZero) then
	    ()
	  else
	    Printf.fprintf opfile1 "%s -> %s \n" 
	      (Expression.lvalToString p) 
	      (Expression.lvalToString q) 
	      ;
	  let x = CaddieBdd.bddCofactor bdd_loc bdd_qcode in
	  if (CaddieBdd.bddEqual (CaddieBdd.bddAnd x y) bddZero) then
	    ()
	  else
	    Printf.fprintf opfile2 "%s ==  %s ; \n" 
	      (Expression.lvalToString p) 
	      (Expression.lvalToString q) 
	      
	end
      with Not_found ->
	failwith ( "sth not found in print")
    end




(* is_alias : location -> Expression.Lval -> Expression.Lval -> bool *) 
let is_alias opfile loc p q =
  (* check if the set pointed to by p intersects the set pointed to by q at location loc*)
  try
    if (p = q) then true
    else
      let bdd_loc = lookup_location loc in
      let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
      let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in 
		(* get y in p->y *)
      let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
		(* get x in q->x *)
      let x = CaddieBdd.bddCofactor bdd_loc bdd_qcode in
		(* get intersection of x and y *)
      let xy = CaddieBdd.bddAnd y x in
      if (CaddieBdd.bddEqual xy (bddZero )) then
	false
      else
	begin
      (* print out p is alias of q *)
	  Printf.fprintf opfile "%s <-> %s %s\n" 
		(Expression.lvalToString (simplify_lval p)) 
		(Expression.lvalToString (simplify_lval q)) 
		(C_SD.location_to_string loc) ; 
	      true
	end
  with Not_found ->
    failwith ( " sth not found in is_alias") 

(* is_pointsto : location -> Expression.lval -> Expression.lval -> bool *)
let is_pointsto loc p q =
  if (p=q) then
    false
  else
    begin
  try 
    let bdd_loc = lookup_location loc in
    let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
    let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in
               (* get q to target set of variables *)
    let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
    let pq = CaddieBdd.bddAnd bdd_pcode qtarget in
    if (CaddieBdd.bddEqual (CaddieBdd.bddAnd bdd_loc pq) bddZero) then 
      false
    else
      (*
      begin
	Printf.fprintf opfile "%s -> %s at %s\n" 
	  (Expression.lvalToString p) 
	  (Expression.lvalToString q) 
	  (C_SD.location_to_string loc) ; 
	true
      end
	*)
      true
  with Not_found ->
    failwith ( " sth not found in is_pointsto")
    end

let alias_pointsto opfile1 opfile2 loc p q = 
  if (p = q) then
    ()
  else
    begin
      try 
	let bdd_loc = lookup_location loc in
	let bdd_pcode = Hashtbl.find lvalue_to_bdd_map p in
	let bdd_qcode = Hashtbl.find lvalue_to_bdd_map q in
	let qtarget = CaddieBdd.replace bdd_qcode (CaddieBdd.toMap bddManager !stvarMap) in
	let y = CaddieBdd.bddCofactor bdd_loc bdd_pcode in
	begin
	  if (CaddieBdd.bddEqual (CaddieBdd.bddAnd y qtarget) bddZero) then
	    ()
	  else
	    Printf.fprintf opfile1 "%s -> %s ; // at  %s\n" 
	      (Expression.lvalToString p) 
	      (Expression.lvalToString q) 
	      (C_SD.location_to_string loc) ;
	  let x = CaddieBdd.bddCofactor bdd_loc bdd_qcode in
	  if (CaddieBdd.bddEqual (CaddieBdd.bddAnd x y) bddZero) then
	    ()
	  else
	    Printf.fprintf opfile2 "%s == %s; // at  %s\n" 
	      (Expression.lvalToString p) 
	      (Expression.lvalToString q) 
	      (C_SD.location_to_string loc)
	end
      with Not_found ->
	failwith ( "sth not found in print")
    end


(* this function directly prints out all p->q at some location *)
let do_bdd_pointsto_flow_sensitive () = 

  Message.msg_string Message.Debug "Points to print start.\n" ;

  let printlist = new Search.bfsiterator in
  init_bdd_location printlist;

  Message.msg_string Message.Debug "get all locations\n" ;

  (* print out all the lvalues *)
  (*
  let printlval lval dummy_bdd = 
    Printf.fprintf opfile "%s\n" (Expression.lvalToString lval)
  in Hashtbl.iter printlval lvalue_to_bdd_map;
    *)
  (* print out all locations *)
  (*
  let printloc (a,b) dummy_bdd = 
    Printf.fprintf opfile "location (%d,%d) \n" a b 
  in
  Hashtbl.iter printloc location_to_bdd_map;
    *)
  let i = ref 0.0 in
  while (printlist#has_next () ) do
    let cur_loc = printlist#next () in
    let bdd_loc = lookup_location cur_loc in
    i := !i +. (CaddieBdd.bddCountMinterm bddManager bdd_loc (2*(!num_bddVar)))
  done;

  Message.msg_string Message.Debug (Printf.sprintf "number of edges %f\n" !i) 






(* End of various printing and querying utilities                                 *)
(**********************************************************************************)
(**********************************************************************************)
(* Main routines to call the analyses     *)
(**********************************************************************************)





type pointsToInfo =
    FlowSensitivePointsTo of ((int * int, CaddieBdd.bdd) Hashtbl.t)
  | FlowInsensitivePointsTo of CaddieBdd.bdd

(* Store the points to information *)
let store_FI_points_to_bdd bdd fname =
  begin
    CaddieBdd.bddStore bddManager bdd fname 
  end

(* Load a previously computed BDD from a file. *)
let load_FI_points_to_bdd fname =
  begin
    (* first set up the bdd manager and the var maps *)
  let lval_list = let (flval, a,b, c) = get_all_lvals () in flval @ a @ b @ c in
  Message.msg_string Message.Debug "Got all Lvals" ;
  (* how many bdd variables we need to encode all lvalues *)
  let num_bddvars_lval lval_list = int_of_float (ceil ((log (float_of_int (List.length lval_list))) /. (log 2.)))
  in
  let num_bddvars = num_bddvars_lval lval_list in
  Message.msg_string Message.Debug ("Number of BDD variables = "^(string_of_int num_bddvars)) ;
  encode_variables num_bddvars lval_list ;
  num_bddVar := num_bddvars; 

  (* then invoke the bddLoad routine *)
  let bdd = try (CaddieBdd.bddLoad bddManager fname)
  with _ -> 
   begin
     Message.msg_string Message.Error "Loading BDD failed: Rebuilding BDD." ;
     let flow_insensitive_bdd =
	bdd_pts_to_flow_insensitive () 
     in 
     store_FI_points_to_bdd flow_insensitive_bdd ((Options.getValueOfString "mainsourcename")^".bdd") ;
     Message.msg_string Message.Debug (Printf.sprintf "Number of edges (flow insensitive) %f\n" 
					   (CaddieBdd.bddCountMinterm bddManager flow_insensitive_bdd (2*(!num_bddVar)))) ;
     flow_insensitive_bdd
   end
  in
  () ;
  FlowInsensitivePointsTo bdd 
  end


let bdd_pts_to_flow_sensitive () =
  failwith "Flow sensitive BDD aliasing not supported any more!"

(* The main routine.
   flow_flag = true --> do a flow sensitive analysis.
             = false --> do a flow insensitive analysis.
*)
let do_bdd_alias flow_flag = 
  Message.msg_string Message.Debug "In do_bdd_alias\n" ;
  (* call the analysis *)
  if (flow_flag) then
    begin
      bdd_pts_to_flow_sensitive () ;

      (* print out points to and alias information *)
      (* return a hashtable of loc -> bdds *)
      FlowSensitivePointsTo location_to_bdd_map

    end 
  else 
    begin
      let flow_insensitive_bdd = bdd_pts_to_flow_insensitive () in 
      store_FI_points_to_bdd flow_insensitive_bdd ((Options.getValueOfString "mainsourcename")^".bdd") ;
      Message.msg_string Message.Debug 
	(Printf.sprintf "Number of edges (flow insensitive) %f\n" 
	   (CaddieBdd.bddCountMinterm bddManager flow_insensitive_bdd (2*(!num_bddVar)))) ;
      (* return the bdd *)
      FlowInsensitivePointsTo flow_insensitive_bdd
    end







