(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


module Command =
struct
  type statement =
    | Expr of Ast.Expression.expression
    | Assignment of Ast.Expression.lval * Ast.Expression.expression
    | Identity of Ast.Expression.lval * Ast.Expression.expression
    | Return of Ast.Expression.expression
    | ReturnVoid
    
  type command =
      Skip
    | Block of statement list
    | Pred of Ast.Predicate.predicate
    | Throw of Ast.Expression.expression
    | MonitorEnter
    | MonitorExit
    | InvokeStmt of Ast.Symbol.symbol  * (Ast.Expression.expression list)
    | MethodCall of Ast.Expression.expression
    | FunctionCall of Ast.Expression.expression
    | GuardedFunctionCall of (Ast.Predicate.predicate * Ast.Expression.expression)
	(* guarded function calls disambiguate function pointer
           calls. A function pointer call *f() translates to
	   GuardedFunctionCall( *f==foo, foo()) 
	   for each function foo that can be aliased to *f 
           More notes: function pointer calls are disambiguated
	   to GuardedFunctionCall in enabled_ops
	*)
    | Havoc of Ast.Expression.lval
    | HavocAll 
    | Phi of Ast.Expression.lval * bool
    | SymbolicHook of string
	
  type t = { code : command ; read : Ast.Expression.lval list ; written : Ast.Expression.lval list ; }
      

  let local_cmd code = {code = code; read = []; written = []}
  let skip = local_cmd Skip
  let cmd_true = local_cmd (Pred Ast.Predicate.True)
	    
  let gather_accesses code =
    let module S = Set.Make(
      struct type t = Ast.Expression.lval
	     let compare = Pervasives.compare
      end
    ) 
    in let is_global sym = not (String.contains sym '@')
    in let rec lvalue_w (r, w as rw) l = 
	let (r', w') = 
	  match l with
	    | Ast.Expression.Symbol s -> rw
	    | Ast.Expression.Access (op,e,s) -> expr rw e
	    | Ast.Expression.Dereference e -> expr rw e 
	    | Ast.Expression.Indexed (e1, e2) -> 
		begin 
		  let rw' = expr rw e1 in expr rw' e2
		end
	    | _ -> failwith "Unexpected error_6 : gather_accesses @ blastSystemDescr.ml"
	in (r', S.add l w')	  
       and lvalue (r, w as rw) l = 
        let (r',w') = 
	  match l with
	    | Ast.Expression.Symbol s -> rw
	    | Ast.Expression.Access (op,e,s) -> expr rw e
	    | Ast.Expression.Dereference e -> expr rw e 
	    | Ast.Expression.Indexed (e1, e2) -> 
		begin
		  let rw' = expr rw e1 in
		    expr rw' e2
		end
	    | _ -> failwith "Unexpected error_5 : gather_accesses @ blastSystemDescr.ml"
	in (S.add l r', w')
       and expr (r, w as rw) = function
	 | Ast.Expression.Lval lv -> lvalue rw lv
	 | Ast.Expression.Chlval _ -> failwith "gather_accesses : Chlval unexpected!"
	 | Ast.Expression.Assignment (op, lv, e2) ->
             let rw' = expr rw e2 in
  	       lvalue_w rw' lv 
	 | Ast.Expression.Binary (Ast.Expression.FieldOffset, e1, e2) -> expr rw e1 
	 | Ast.Expression.Binary (op, e1, e2) -> let rw' = expr rw e1 in expr rw' e2
	 | Ast.Expression.Cast (ty, e) -> expr rw e
	 | Ast.Expression.Constant _ -> rw
	 | Ast.Expression.Constructor _ -> rw
	 | Ast.Expression.MethodCall (e1, e2, e3, e4, es) -> List.fold_left expr rw (es)
	 | Ast.Expression.FunctionCall (e, es) -> List.fold_left expr rw ((* e ::*) es)
	     (* when the day dawns where we deal with function pointers we shall throw the "e" back in  
		but for now it puts the function names in which is not so nice... *)
	 | Ast.Expression.Sizeof _ -> rw
	 | Ast.Expression.Unary (Ast.Expression.Address, e) -> rw (* don't go below address-ofs *)
	 | Ast.Expression.Unary (op, e) -> expr rw e
	 | _ -> failwith "Unexpected error_4 : gather_accesses @ blastSystemDescr.ml"
    in let rec predicate rw = function
      | Ast.Predicate.True | Ast.Predicate.False -> rw
      | Ast.Predicate.And ps -> List.fold_left predicate rw ps
      | Ast.Predicate.Or ps -> List.fold_left predicate rw ps
      | Ast.Predicate.Not p -> predicate rw p
      | Ast.Predicate.Implies (p1, p2) -> predicate (predicate rw p1) p2
      | Ast.Predicate.Iff (p1, p2) -> predicate (predicate rw p1) p2
      | Ast.Predicate.All (sym, p) -> predicate rw p
      | Ast.Predicate.Exist (sym, p) -> predicate rw p
      | Ast.Predicate.Atom e -> expr rw e
      | _ -> failwith "Unexpected error_3 : gather_accesses @ blastSystemDescr.ml"
    in let collect (r, w) =
	{ code = code;
	  read = S.fold (fun v vs -> v :: vs) r [];
	  written = S.fold (fun v vs -> v :: vs) w [] }
    in
      match code with
	| Skip -> skip
	| Block statements ->
	    collect (List.fold_left
		       (fun rw block ->
			  expr rw (match block with
				     | Expr e -> e
				     | Return e -> e
				     | _ -> failwith "Unexpected error_2 : gather_accesses @ blastSystemDescr.ml"))
		       (S.empty, S.empty) statements)
	| Pred p -> collect (predicate (S.empty, S.empty) p)
	| MethodCall e -> collect (expr (S.empty, S.empty) e)
	| FunctionCall e -> collect (expr (S.empty, S.empty) e)
	| Havoc _ | HavocAll -> assert false
	| Phi _ -> assert false
        | SymbolicHook _ -> skip
	| _ -> failwith "Unexpected error : gather_accesses @ blastSystemDescr.ml"

  let print_statement fmt stm =
    match stm with
	Assignment(l,e) | Identity (l,e) ->
	  Format.fprintf fmt "@[" ;
	  Ast.Expression.print_lval fmt l ;
	  Format.fprintf fmt " = " ;
          Ast.Expression.print fmt e ;
          Format.fprintf fmt ";" ;
	  Format.fprintf fmt "@]"
      | Expr(e) ->
          Ast.Expression.print fmt e ;
          Format.fprintf fmt ";"
      | Return(e) ->
          Format.fprintf fmt "Return(" ;
          Ast.Expression.print fmt e ;
          Format.fprintf fmt ");"
      | ReturnVoid -> 
	  Format.fprintf fmt "return;"

	    

  (* the pretty-printer for this type *)
  let print fmt cmd =
    Format.fprintf fmt "@[" ;
    begin
      match cmd.code with
          Skip ->
            Format.fprintf fmt "Skip"
        | Block(l) ->
            Format.fprintf fmt "Block(" ;
            List.iter (print_statement fmt) l ;
            Format.fprintf fmt ")"
        | Pred(p) ->
            Format.fprintf fmt "Pred(" ;
            Ast.Predicate.print fmt p ;
            Format.fprintf fmt ")"
(*	| ReturnVoid -> 
	    Format.fprintf fmt "return;"*)
(*	| Return e -> 
	    Format.fprintf fmt "return " ;
	    Expression.print fmt e *)
	| Throw e ->
	    Format.fprintf fmt "Throw(" ;
	    Ast.Expression.print fmt e ;
	    Format.fprintf fmt ")" 
	| MonitorEnter -> 
	    Format.fprintf fmt "MonitorEnter" ;
	| MonitorExit -> 
	    Format.fprintf fmt "MonitorExit" ;
	| InvokeStmt (s, elist) ->
	    Format.fprintf fmt "Invoke (" ;
	    Format.fprintf fmt "%s" s ;
	    List.iter (fun e -> Ast.Expression.print fmt e ) elist ;
	    
	    Format.fprintf fmt ")"
	| MethodCall (e) ->
	    Format.fprintf fmt "MethodCall(";
	    Ast.Expression.print fmt e ;
	    Format.fprintf fmt ")"
        | FunctionCall(e)  ->
            Format.fprintf fmt "FunctionCall(" ;
            Ast.Expression.print fmt e ;
            Format.fprintf fmt ")"
        | Havoc v ->
            Format.fprintf fmt "Havoc (%s)" (Ast.Expression.lvalToString v)
        | HavocAll ->
            Format.fprintf fmt "HavocAll"
        | Phi (v, sign) ->
            Format.fprintf fmt "Pred(%s\\phi(%s))"
                               (if sign then "" else "\\not") (Ast.Expression.lvalToString v)
	| SymbolicHook fname -> Format.fprintf fmt "SymHook(%s)" fname
	| _ -> failwith "Unexpected error : print @ blastSystemDescr"
    end ;
    Format.fprintf fmt "@]"

  (* the string converter for this type *)
  let to_string x = 
    let cmd_string = Misc.to_string_from_printer print x in
    cmd_string

end
