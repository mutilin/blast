(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

%{
module E = VampyreErrormsg
open Ast

let  parse_error msg =
    E.s (E.error "Parse error at %s: %s" 
           (E.getLineCol !E.current (symbol_start ())) msg)

let predicateList = ref []
let resetPredicateParser () = predicateList := []
let pushPredicate  pred =
  predicateList := pred :: !predicateList
%}

%token <string> Id Const
%token <int> Num
%token GLOBAL LOCAL ENV STORE PROC 
%token PREDICATES ANNOTATIONS SEMICOLON COLON
%token EOF
%token PROC LPAREN  RPAREN LINDEX RINDEX QUESTION  DOT  ARROW 
%token  BITAND  BITOR  DIV  LSHIFT  MINUS  MUL 
%token  PLUS  REM  RSHIFT LSHIFT XOR  EQ  GE  GT  LE  LT 
%token  NE  INDEX  AND  OR  COMMA  NOT  BITNOT  DEREF 
%token  ADDRESS  SYMBOL 
%token UMINUS UPLUS DOTSTAR ARROWSTAR
%token ICR DECR SIZEOF
%token ASSIGN MULTassign DIVassign MODassign PLUSassign MINUSassign LSassign RSassign ANDassign XORassign ORassign
%token INT CHAR FLOAT DOUBLE LONG VOID
%token CONSTANT

%start fun_main
%start main
%start env_assumptions
%start lval_list
%start modifies_info

%type <((string * Ast.Predicate.predicate list) list)> fun_main
%type <(Ast.Predicate.predicate list)> main
%type <((Ast.Expression.lval * (Ast.Predicate.predicate)) list)> env_assumptions
%type <(Ast.Expression.lval list)> lval_list
%type <Ast.Predicate.predicate> predicate
%type <Ast.Expression.expression> equality_expression
%type <((int * (Ast.Expression.lval list)) list ) * ((string * (Ast.Expression.lval list)) list) * ((Ast.Expression.lval * (string list)) list) * (( (int * int * int * int) * Ast.Expression.lval ) list)> modifies_info


%%


env_assumptions:
 EOF  { [] }
| unary_expression COLON assumption SEMICOLON env_assumptions 
     { 
       match $1 with
	   Expression.Lval l -> (l,$3) :: $5
	 | _ -> failwith "Inputparse : lhs of env_asm is not an lvalue !"
     }
  ;

modifies_info: 
  int_to_lval_list
  string_to_lval_list
  lval_to_string_list
  four_int_to_lval_list EOF { ($1, $2, $3, $4) }

int_to_lval_list : DOT { [] }
| Num COLON list_of_lvals COMMA int_to_lval_list { ($1, $3) :: $5 }

string_to_lval_list : DOT { [] }
| Id COLON list_of_lvals COMMA string_to_lval_list { ($1, $3) :: $5 }

lval_to_string_list : DOT { [] }
| unary_expression COLON string_list COMMA lval_to_string_list 
   { match $1 with Expression.Lval l -> (l, $3) :: $5
     | _ -> failwith ("Inputparse: lhs of lval_to_string_list is not an lvalue")
   }

four_int_to_lval_list : DOT { [] }
| LPAREN Num COMMA Num COMMA Num COMMA Num RPAREN COLON unary_expression COMMA four_int_to_lval_list
  { (($2, $4, $6, $8), match $11 with Expression.Lval l -> l | _ -> failwith "Inputparse: four_int_to_lval_list: Not an lvalue") :: $13 }

fun_main:                   {[]} 
| fun_predicates DOT fun_main   {$1::$3}
;

fun_predicates:  Id COLON predicates { let x = !predicateList in (resetPredicateParser (); ($1,x)) }
;

main:
  predicates EOF   { (let x = !predicateList in let _ = (predicateList := []) in x )}
  ;

lval_list: list_of_lvals EOF { $1 }

list_of_lvals:
   { [] }
| unary_expression SEMICOLON list_of_lvals 
	{ match $1 with
		Expression.Lval l -> (l) :: $3
	  |     _                 -> failwith ("Inputparse: Expected lvalues, got "^(Expression.toString $1))
	}
	;


predicates:
  {}
| predicate SEMICOLON predicates         {pushPredicate $1}
    ;


predicate:
  equality_expression { Predicate.Atom ($1) }
  ;

assumption:
  predicate { $1 }
| assumption AND assumption {Predicate.And [$1;$3]}
| assumption OR  assumption {Predicate.Or [$1;$3]}
| NOT assumption {Predicate.Not $2}
| LPAREN assumption RPAREN {$2}
;

primary_expression:           
  identifier            { Expression.Lval (Expression.Symbol ($1)) }
| constant              { Expression.Constant ($1) }
| LPAREN expression RPAREN    { $2  }
    ;
  
  postfix_expression:           
    primary_expression { $1 }
| postfix_expression LINDEX expression RINDEX
    {  
       Expression.Lval (Expression.Indexed ($1, $3))
    
    }
    
| postfix_expression LPAREN RPAREN
    { Expression.FunctionCall ($1, []) }
    
| postfix_expression LPAREN argument_expression_list RPAREN
    { Expression.FunctionCall ($1, $3) }
    
| postfix_expression DOT identifier
    { Expression.Lval (Expression.Access(Expression.Dot, $1, $3)) }
    
| postfix_expression ARROW identifier
    { Expression.Lval (Expression.Access(Expression.Dot, Expression.Lval (Expression.Dereference ($1)), $3)) }
    
| postfix_expression ICR
    { Expression.Unary(Expression.PostInc, $1) }
    
| postfix_expression DECR
            { Expression.Unary(Expression.PostDec, $1) }
    ;
  
argument_expression_list:       
    assignment_expression
    { [ $1 ] }
    
| argument_expression_list COMMA assignment_expression
    { $1 @ [ $3 ] } 
    ;
  
  unary_expression:               
    postfix_expression
    { $1 }
    
| ICR unary_expression
            { Expression.Unary(Expression.PreInc, $2) }
    
| DECR unary_expression
    { Expression.Unary(Expression.PreDec, $2) }
    
| unary_operator cast_expression
    { 
  match $1 with
    Expression.Address -> Expression.addressOf $2
  | _ -> Expression.Unary($1, $2)
    }
| MUL cast_expression 
	{ Expression.Lval (Expression.Dereference $2)    }
| SIZEOF unary_expression
    { failwith "Sizeof with expression not handled" }
    
| SIZEOF LPAREN identifier RPAREN
    { failwith "Sizeof not handled" }
    ;
  
  unary_operator:                 
          BITAND     { Expression.Address }
        | PLUS     { Expression.UnaryPlus }
        | MINUS     { Expression.UnaryMinus }
        | BITNOT     { Expression.BitNot }
        | NOT     { Expression.Not }
        ;

cast_expression:               
          unary_expression { $1 }
        | LPAREN type_name RPAREN cast_expression
            { Expression.Cast ($2, $4) }
        ;

multiplicative_expression:     
          cast_expression { $1 }
        | multiplicative_expression MUL cast_expression
            { Expression.Binary(Expression.Mul, $1, $3) }

        | multiplicative_expression DIV cast_expression
            { Expression.Binary(Expression.Div, $1, $3) }

        | multiplicative_expression REM cast_expression
            { Expression.Binary(Expression.Rem, $1, $3) }
        ;

additive_expression:           
          multiplicative_expression { $1 }
        | additive_expression PLUS multiplicative_expression
            { Expression.Binary(Expression.Plus, $1, $3) }

        | additive_expression MINUS multiplicative_expression
            { Expression.Binary(Expression.Minus, $1, $3) }
        ;

shift_expression:              
          additive_expression { $1 }
        | shift_expression LSHIFT additive_expression
	    { Expression.Binary(Expression.LShift, $1, $3) }

        | shift_expression RSHIFT additive_expression
	    { Expression.Binary(Expression.RShift, $1, $3) }
        ;

relational_expression:         
          shift_expression { $1 }
        | relational_expression LT shift_expression
	    { Expression.Binary(Expression.Lt, $1, $3) }

        | relational_expression GT shift_expression
	    { Expression.Binary(Expression.Gt, $1, $3) }

        | relational_expression LE shift_expression
	    { Expression.Binary(Expression.Le, $1, $3) }

        | relational_expression GE shift_expression
	    { Expression.Binary(Expression.Ge, $1, $3) }
        ;

equality_expression:           
          relational_expression {$1}
        | equality_expression EQ relational_expression
            { Expression.Binary(Expression.Eq, $1, $3) }

        | equality_expression NE relational_expression
            { Expression.Binary(Expression.Ne, $1, $3) }
        ;

AND_expression:                
          equality_expression { $1 }
        | AND_expression BITAND equality_expression
            { Expression.Binary(Expression.BitAnd, $1, $3) }
        ;

exclusive_OR_expression:       
          AND_expression { $1 }
        | exclusive_OR_expression XOR AND_expression
            { Expression.Binary (Expression.Xor, $1, $3) }
        ;

inclusive_OR_expression:       
          exclusive_OR_expression { $1 }
        | inclusive_OR_expression BITOR exclusive_OR_expression
            { Expression.Binary (Expression.Or, $1, $3) }
        ;

logical_AND_expression:        
          inclusive_OR_expression { $1 }
        | logical_AND_expression AND inclusive_OR_expression
            { Expression.Binary(Expression.And, $1, $3) }
        ;

logical_OR_expression:         
          logical_AND_expression { $1 }
        | logical_OR_expression OR logical_AND_expression
            { Expression.Binary(Expression.Or, $1, $3) }
        ;

conditional_expression:        
          logical_OR_expression { $1 }
        | logical_OR_expression QUESTION expression COLON conditional_expression
            { failwith "Question mark not handled" }
        ;

assignment_expression:         
          conditional_expression {$1}
        | unary_expression assignment_operator assignment_expression
            {
	  match $1 with
	    Expression.Lval l ->
	      Expression.Assignment ($2, l, $3 )
	  | _ -> failwith "Inputparse : left hand side of assingment not an lvalue" 
	    }
        ;

assignment_operator:           
          ASSIGN
            { Expression.Assign }

        | MULTassign
            { Expression.AssignMul }

        | DIVassign
            { Expression.AssignDiv }

        | MODassign
            { Expression.AssignRem }

        | PLUSassign
            { Expression.AssignPlus }

        | MINUSassign
            { Expression.AssignMinus }

        | LSassign
            { Expression.AssignLShift }

        | RSassign
            { Expression.AssignRShift }

        | ANDassign
            { Expression.AssignBitAnd }

        | XORassign
            { Expression.AssignXor }

        | ORassign
            { Expression.AssignBitOr }
        ;

expression:                    
          assignment_expression
            { $1 }
        | expression ',' assignment_expression
            {  failwith "COmma not handled"
	    }
        ;

constant: 
          Num       { Constant.Int ($1) }
        ;


identifier:
          Id { $1 }
        ;

string_list :  { [] }
| Id COMMA string_list { $1 :: $3 } 

type_name: 
    INT { (JavaType.Primitive (JavaType.IntType)) }
	|   CHAR { (JavaType.Primitive (JavaType.CharType)) } 

