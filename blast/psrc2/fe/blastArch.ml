(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * This module defines the main signatures for Blast.
 *)

(* DUMMY -- "unit" for functors *)
module type MYUNIT = sig val myunit : unit end

(**
 * The PRINTABLE signature specifies a type equipped with a print function
 * that allows to pretty print values of this type.
 *)
module type PRINTABLE =
sig
  type t
  val print : Format.formatter -> t -> unit
  val to_string : t -> string
end

(**
 * The COMMAND signature specifies a set of commands.  These commands
 * typically label the edges of a System description.
 *)
module type COMMAND =
sig
  include PRINTABLE
end

(**
 * The SYSTEM_DESCRIPTION signature specifies how an input system shall be
 * described.  This description is an automaton-like representation suitable
 * for model checking.
 * 
 *)
module type SYSTEM_DESCRIPTION =
sig
  module Command : COMMAND

  type location
  
  type edge

  (** to use to hash edges, its (int*int)*(int*int) *)
  type edge_id_t = ((int * int) * (int * int))
  (** a command that does nothing *)
  val skip : Command.t
  
  (** a location, used to build regions where only the data part is relevant *)
  val fake_location : location

  (** a location, used to build regions where only the data part is relevant *)
  val fake_edge : edge (* fake_location, skip, fake_location *)

  (** get the source position for a location *)
  val get_source_position : location -> (string * int * int) option

  (** get the locations for a source position *)
  val get_position_sources : string * int -> location list

  (** the pretty-printer for locations *)
  val print_location : Format.formatter -> location -> unit

  (** the pretty-printer for edges *)
  val print_edge     : Format.formatter -> edge     -> unit
(*  val print_code     : Format.formatter -> Command.t -> unit *)

  (** the string converter for locations *)
  val location_to_string : location -> string

  val call_depth : location -> int
  val location_coords : location -> int * int

  (** the string converter for edges *)
  (** TBD: This is gratuitous, replace with get_command! *)
  val edge_to_string : edge -> string

  val get_edge_coords : edge -> edge_id_t
 
  val make_edge_coords : location -> location -> edge_id_t

  (** Extracting an edge's associated command *)
  val edge_to_command : edge -> Command.t
  
  (** gives the list of edges originating from a given location *)
  val get_outgoing_edges : location -> edge list

  (** gives the list of edges pointing to a given location *)
  val get_ingoing_edges : location -> edge list

  (** gives the source of an edge *)
  val get_source  : edge -> location

  (** gives the target of an edge *)
  val get_target  : edge -> location

  (** gives the command labeling an edge *)
  val get_command : edge -> Command.t

  (** Create a unique name for the edge *)
  val get_edge_name : edge -> string
    
  val reads_and_writes_at_edge : edge -> Ast.Expression.lval list * Ast.Expression.lval list

  (** make_edge src target cmd : make a new edge with command cmd between src and target,
      but don't add this to the CFA. *)
  val make_edge : location -> location -> Command.t -> edge

  (** for an expression x returns *x, **x etc etc ... for now only goes down a depth of 2 *)
  val stats_nb_cached_exp_closure : int ref
  val stats_nb_recomp_exp_closure : int ref

 (** the first argument is true if its to ALL, false for IMAGE *) 
  val add_field_to_unroll : bool -> string -> bool 
 (* was it added (true) or already there (false) *)
 
  val expression_closure : Ast.Expression.expression -> Ast.Expression.expression list
 (** again -- first arg is true if for ALL e.g. trace-project-refine etc. Second for image *)   
  val expression_closure_stamp :  bool -> Ast.Expression.expression -> Ast.Expression.expression list

  val lvalue_closure : Ast.Expression.lval ->  Ast.Expression.lval list
  val lvalue_closure_stamp : bool -> Ast.Expression.lval ->  Ast.Expression.lval list
  val iter_global_variables : (Ast.Expression.lval -> unit) -> unit

  val iterate_all_lvals : (Ast.Expression.lval -> unit) -> unit

  val iterate_all_scope_lvals : string -> (Ast.Expression.lval -> unit) -> unit
    
  (** Functions that return the Cil type of an lvalue. Fragile. Use at your own risk. *)
  
  (** return the type of an lvalue *)
  val get_type_of_lval : Ast.Expression.lval -> Cil.typ

  (** return the type of an expression. Right now this only deals with Lvals and Address-es*)
  val get_type_of_exp : Ast.Expression.expression -> Cil.typ

  val is_struct : Ast.Expression.lval -> bool
  val is_array : Ast.Expression.lval -> bool
  val is_ptr : Ast.Expression.lval -> bool
  val is_skolem : Ast.Expression.lval -> bool
  
  (** return true if the function name corresponds to an allocation function *)
  val is_alloc : string -> bool
  (** return true if the function name corresponds to a deallocation function *)
  val is_dealloc : string -> bool

  (** return true if the function should not be expanded by Blast *) 
  val is_noexpand : string  -> bool 

  (** return true if the function does not return -- hence no summaries created *) 
  val is_noreturn : string  -> bool 

  (** returns true if the symbol is in the table of global variables *)
  val is_global : Ast.Expression.lval -> bool

  (** returns true if the lvalue can escape. Uses the alias analyzer *)
  val can_escape : Ast.Expression.lval -> bool

  (** returns true if the string is the name of a lock variable *)
  val is_a_lock : Ast.Expression.lval -> bool

  (** returns true if the symbol is a local for the specified function *)
  val is_in_scope : string -> Ast.Expression.lval -> bool

  val globals_of_type : Cil.typ -> Ast.Expression.lval list

(** returns the possible "scopes" of an lval -- should be a single string.
    "" indicates global scope *)
val scope_of_lval : Ast.Expression.lval -> string list
  
    
  (** A variable that contains the id of the running thread. *)
  val tid : string

  (** A variable that is always different from tid *)
  val notTid : string

  (** A variable that refers to a generic memory location *)
  val self : string

  (** gives the entry location of a function *)
  (* Greg: strings for function calls: to change? *)
  exception NoSuchFunctionException of string
  
  (** returns true if the function is defined, false otherwise *)
  val is_defined : string -> bool

  (** always_reach loc1 loc2 returns true iff loc1,loc2 are two locations in the same CFA,
    s.t. all acyclic paths from loc1 (eventually) go through loc2 *)
  val always_reach : location -> location -> bool

  (** mods_on_path loc1 loc2 returns the set of lvals written to along all paths from loc1 to loc2.
    again, the two locations need to be in the same CFA (or you get the empty set) *)
  val mods_on_path : location -> location -> Ast.Expression.lval list

  val may_be_modified : Ast.Expression.lval list -> location -> location -> bool
  val get_fname : Ast.Expression.expression -> string
  val local_mod_on_edge : edge -> Ast.Expression.lval -> bool
  val global_mod_on_call : string -> Ast.Expression.lval -> bool

  (** returns the list of global lvals that can be modified by a function *)
  val global_lvals_modified : string -> Ast.Expression.lval list

  (** returns the list of formal lvals that can be modified by a function *)
  val formal_lvals_modified : string -> Ast.Expression.lval list

  val global_lvals_mod_by : Ast.Expression.lval -> string list

    (* the list of funs that may mod an lv, via copyback effects *)
  val may_mod_by : Ast.Expression.lval -> string list
    
  (** find the entry location for a function *)
  val lookup_entry_location : string -> location

  val is_call_on_edge : edge -> bool
  
  val get_next_location : location -> location

  val call_on_edge : edge -> (Ast.Symbol.symbol * Ast.Expression.expression) option
 (** Given some location, return what function it belongs to *)
  val get_location_fname : location -> string
  
 (** Given some fname, returns all the locations for it *)
 val lookup_all_locations : string -> location list

 (** Get the "id" of fname *)
  val get_fname_id : string -> int

  (** return list of functions *)
  val list_of_functions : unit -> string list

  exception FunctionPointerException 

  (** Return target used by deconstructFunCall when function is
      void *)
  val junkRet : Ast.Expression.lval

  (** Name for irrelevant functions *)
  val __SKIPFunctionName : string

  val __NotImplementedFunctionName : string

  val __BLAST_DummyFunctionName : string

  val __BLAST_DispatchFunctionName : string
  (** parse a function expression, returning a triple of
      (function name, (formal name, formal value expr) list, result target lval) *)
  val deconstructFunCall : Ast.Expression.expression ->
       Ast.Symbol.symbol * (string * Ast.Expression.expression) list *
         Ast.Expression.lval

  (** return the location, given the location coordinates *)
  val lookup_location_from_loc_coords : int * int -> location

  (** is a given location a join location ? actually are there 
  multiple edges _into_ that location *)
  val is_join : location -> bool

  (** is a given location l on a loop -- i.e. is l on a cycle in its CFA ?*)
  val is_loopback : location -> bool
  
  (* various boolean utilities *)
  val isReturn : edge -> bool
  
  val returnExp : edge -> Ast.Expression.expression
  
  val isBlock : edge -> bool
  
  val isPredicate : edge -> bool

  val isFunCall : edge -> bool
  
  val isDefinedFunCall : edge -> bool
  (* utilities for async/events *)
 
  val isEventHandler_fname : string -> bool

  val is_dispatch_loc : location -> bool
    
  val isAsyncCall : edge -> bool
  
  val isAsyncExec : edge -> bool
  
  val isDispatchCall : edge -> bool
    
  val acAsyncCall : edge -> edge 
  
  val enter_call : edge -> bool

  val replace_code_in_cfa : edge -> Ast.Expression.t -> unit

  (* val instrument_CFA : unit -> unit *)

  (** Type of formal arguments to a function. *)
  type formals_kind =
      Fixed of string list
    | Variable of string list (** variable arguments *)

  (** gives the locals of a function *)
  val lookup_locals : string -> string list

  (** returns ALL the lvals that may be smashed by fname -- and are in scope of fname *)
  val lookup_local_lvals : string -> Ast.Expression.lval list
	
  (** gives the formals of a function *)
  val lookup_formals : string -> formals_kind

  (** gives the list of global variables *)
  val globals : unit -> string list

	
  (** Used in the parallel model checker. Looks at a location and says whether
      there can be a context switch at that location. *)
  val is_atomic : location -> bool

  val is_event : location -> bool

  val is_task : location -> bool

  val is_spec : location -> bool

  (** the locations corresponding to a label name in the C source *)
  val get_locations_at_label : string -> location list

  (** map_edges f applies f to all the edges in the program *)
  val map_edges : (edge -> 'a) -> 'a list

  (** map_edges f fname applies f to all the edges in the function fname *)
  val map_edges_fname : (edge -> 'a) -> string -> 'a list

  (** call graph related functions *)
  val compute_callgraph : unit -> unit
	
  val calls_made_by : string -> (Ast.Expression.expression * string) list	
  val calls_made_into : string -> ((edge_id_t *Ast.Expression.expression) * string) list	

  val output_callgraph : string (* filename *) -> unit
  (* val print_callgraph : out_channel -> unit *)
 
  val get_reachable_functions : unit -> string list (* a subset of list_of_functions 
						       that are reachable in the call graph *)

  (** what functions can fname transitively call *)
  val caller_closure : string list -> string list 

  (** what functions transitively call fname *)
  val backwards_caller_closure : string list -> string list
  val bounded_backwards_caller_closure : int -> string list -> string list
  val shortest_path : string -> string ->  string list
  val output_call_paths : unit -> unit
  val scc_caller_closure : string -> string list

(******* Enable the passing of the "error"/"spec" predicate between main and the
modelchecker -- this is a single pred now, but really one should know what all
the spec lvals are and seed skipfun with those *)

val set_error_lvals : Ast.Expression.lval list -> unit
val get_error_lvals : unit -> Ast.Expression.lval list
  
  (**** Reaching definitions *****)

  (** Return a list of lvals which have multiple paths to the specified location *)
  val get_all_multiply_defined_lvals : location -> Ast.Expression.lval list
  (** Do the analysis for reaching definitions *)
  val compute_reaching_definitions : unit -> unit
end

module type OPERATION =
sig
  include PRINTABLE
  type info =
    | Normal
    | Call of Ast.Predicate.predicate * 
              Ast.Expression.lval option * 
              string * Ast.Expression.expression list
    | Ret of Ast.Expression.expression 
  val get_info : t -> info
  val paren_fun : t -> string
  val to_string : t -> string
end

module type REGION =
sig
  include PRINTABLE
  val log_region : Message.logmsg_level -> Message.logmsg_type -> t -> unit
  
  val eq : t -> t -> bool
  val leq : t -> t -> bool
  val cup : t -> t -> t
  val cap : t -> t -> t
  val negate : t -> t
  val is_empty : t -> bool
  val to_strings : t -> (string list * string)
  val to_predicate: t -> Ast.Predicate.predicate
  val get_call_depth : t -> int
  val get_function_name : t -> string
  val get_location_id : t -> (int * int)
  val is_noreturn : t -> bool
  val is_coverable : t -> bool
  val erase_data : t -> t
  val cubes : t -> int 
  (* coverers store data structures for quick lookup of a region which covers another *)
  type 'a coverers
  val emptyCoverers : unit -> 'a coverers
  val addCoverer : 'a coverers -> t -> 'a -> unit
  val deleteCoverer : 'a coverers -> t -> 'a -> unit
  val deleteCoverers : 'a coverers -> (t * 'a) list -> unit
  val findCoverer : 'a coverers -> t -> 'a option
  val findCoverUnion : 'a coverers -> t -> t
  val findExactCoverer : 'a coverers -> t -> 'a option
  val printCoverer : 'a coverers -> unit

  (* type data_region  *)

end


(**
   Module that implements a complete lattice (with top and bottom elements)
   *)

(**
  Lattice Query Interface
  =======================
  
  The query interface enables lattices to exchange predicates with each other
  and with the abstraction layer (which maintains the predicate abstraction
  state). Here is how the layers fit together:

                       ----------------------------
                       |                          |
                       |    Abstraction Layer     |
                       |                          |
                       ----------------------------
                           |      /|\        | 
                          1|       |        7|
                           |      4|         |
                          \|/      |        \|/
                       ----------------------------
                       |                          |
                       |      Union Lattice       |
                       |                          |
                       ----------------------------
                        |  /|\  |     |    |   |
                       2|  3|  8|    5|   6|  9|
                       \|/  |  \|/   \|/  \|/ \|/
                       -----------   --------------         ---------
                       |         |   |            |         |       |
                       |  Set    |   |  SymExe    |  . . .  |       |
                       | Lattice |   |  Lattice   |         |       |
                       -----------   --------------         ---------

  The query interface is called during a post operation. Here's how it works:
  Step(s)     What Happens
  =======     ==================================================
  1           The abstraction layer calls post on the union lattice. It passes
              in a current lattice element, a command, and a query function.
              The query function, when called, executes the against the
              predicate abstraction state.

  2, 6        The union lattice then executes post on each of the sub-lattices.
              For each sub-lattice, it passes in a different query function -
              each closure includes the identity of the lattice it is passed
              to. This is used by the query function to avoid calling back to
              the same lattice.

  3, 4, 5     During the post function, a lattice can make a call to the
              query function that was passed in. This query will then execute
              queries against the abstraction layer and each of the other
              lattices. A query can return 0, 1, or Top. A contradiction is
              an error. Otherwise, the query returns the most precise value
              returned. Note that queries are executed against the OLD version
              of each lattice, not the new one being computed.

  2, 6, 1     Each lattice's post function returns a new lattice element and
              a predicate. When the union lattice returns from post, it returns
              an array of lattice elements and the conjunction of the
              sub-lattice predictes. Any predicates which are just TRUE may be
              dropped from the conjunction.

  7, 8, 9     The abstraction layer creates an assume command from the returned
              predicate using C_SD.command.pred. This command is first executed
              against the predicate state. The abstraction layer then calls
              the post of the union lattice with the assume command. The union
              lattice, in turn, calls the post of each sublattice.

  The lattice elements and predicate states from the second post are the
  final result used to create the new region.


  Interface Changes
  -----------------
  blastArch.ml will include the following new definitions:
   type lattice_qry_result = Top | True | False
   type lattice_qry_fn =  Ast.predicate -> lattice_qry_result

  The post functions of the set, symbolic execution, and union lattices will
  be changed to have the following signature:
   post: lattice -> edge -> lattice_qry_fun -> (lattice, Ast.prediate)

  Each lattice must also implement a function called "query" which is of
   type lattice_qry_fn.
  

  Design Decisions
  ----------------
  1. When the post function of a lattice is called with an assume command, it
     must not return any new predicates. This ensures termination of our overall
     post.

  2. A query is executed against the old version of each lattice element. This
     is consistent with the semantics of the union lattice -- each sub-lattice
     is an independent representation of the program state. A post operation
     is executed "in parallel" against all sub-lattices.
**)


  
module type LATTICE =
  sig
    module C_SD : SYSTEM_DESCRIPTION
    type lattice
    type edge
    val top : lattice (** No information known *)
    val bottom : lattice (** Empty region -- generally means that state is not reachable *)
    val init : lattice (** initial lattice element *)
    val join : lattice -> lattice -> lattice (** Join is used to take the union of two regions *)
    val meet : lattice -> lattice -> lattice (** Meet is used to take the interection of two regions *)
    val leq : lattice -> lattice -> bool (** Check for containment of regions *)
    val eq : lattice -> lattice -> bool (** Check for equality of regions *)
    val focus : unit -> unit (* for the moment it is unit -> unit *)
    val is_consistent : lattice -> CaddieBdd.bdd -> bool (** Is lattice consistent with predicates? *)
    val post : lattice -> edge -> (Ast.Predicate.predicate -> Ast.Predicate.predicate) ->
                                                                          (Ast.Predicate.predicate list * lattice)
                    (** Compute the lattice which is result after applying computation in edge to initial lattice
		        Also return the predicates that you might have discovered to the mother code *)
    val summary_post : lattice -> lattice -> edge -> edge -> lattice
    val pre : lattice -> edge -> lattice  (** Not currently used *)
    val print : Format.formatter -> lattice -> unit (** Pretty printer for this lattice *)
    (** Called after source code has been initialized *)
    val initialize : unit -> unit
    val query_fn : lattice -> Ast.Predicate.predicate -> Ast.predicateVal
    val enabled_ops : lattice -> C_SD.Command.t list
    val toString : lattice -> string 
  end

module type PREDTABLE =
  sig
    module C_SD : SYSTEM_DESCRIPTION
    val refined_flag : bool ref
    val bddZero : CaddieBdd.bdd
    val bddOne : CaddieBdd.bdd
    val bdd_support : CaddieBdd.bdd -> int list
    val bdd_cube_cover : CaddieBdd.bdd -> CaddieBdd.bdd
    val lits_of_cube : CaddieBdd.bdd -> (int * bool) list
      
    val tableOfPreds : (int * Ast.Predicate.predicate * CaddieBdd.bdd) list ref
    val hashTableOfPreds : (Ast.Predicate.predicate, int) Hashtbl.t
      
    val getCurrentIndex : unit -> int
    val printTableOfPreds : unit -> unit
      
    val getPred : int -> Ast.Predicate.predicate
    val getPredIndex :Ast.Predicate.predicate -> int
    val getPredBdd : Ast.Predicate.predicate -> CaddieBdd.bdd
    val getPredIndexBdd : int -> CaddieBdd.bdd
     
    val getNIndexFromCIndex : int -> int option
    val getCIndexFromNIndex : int -> int option
      
    val isCurrentIndex : int -> bool
      
    val getPredIndexNextBdd : int -> CaddieBdd.bdd
    val transrel_bdd_table : ((int * int) * (int * int), CaddieBdd.bdd) Hashtbl.t
    val get_transrel_bdd : (int * int) * (int * int) -> CaddieBdd.bdd
    val bddVarMap_list : (int * int) list ref
    val curr_var_map : CaddieBdd.varMap ref
    val curr_var_cube : CaddieBdd.bdd ref
    val next_var_map : CaddieBdd.varMap ref
    val next_var_cube : CaddieBdd.bdd ref
    val update_bdd_info : int * int -> unit
    val make_unprimed_bdd : CaddieBdd.bdd -> CaddieBdd.bdd
    val make_primed_bdd : CaddieBdd.bdd -> CaddieBdd.bdd
    val bdd_postimage : CaddieBdd.bdd -> CaddieBdd.bdd -> CaddieBdd.bdd
    val bdd_preimage : CaddieBdd.bdd -> CaddieBdd.bdd -> CaddieBdd.bdd
    val bdd_exist_quantify : CaddieBdd.bdd -> int list -> CaddieBdd.bdd
    val make_eq_bdd : int list -> CaddieBdd.bdd
    val make_impl_bdd : int list -> CaddieBdd.bdd
    val add_new_pred_to_table : Ast.Predicate.predicate -> int
    val get_all_pred_indexes : unit -> int list
    val extract_full_cube : CaddieBdd.bdd -> CaddieBdd.bdd
    val convertPredToBdd_map :
      (Ast.Predicate.predicate -> CaddieBdd.bdd) ->
        Ast.Predicate.predicate -> CaddieBdd.bdd
    val convertPredToBdd : Ast.Predicate.predicate -> CaddieBdd.bdd
    val ac_convertCubeToPred :
        (int -> Ast.Predicate.predicate) -> int array -> Ast.Predicate.predicate
    val convertCubeToPred : int array -> Ast.Predicate.predicate
    val ac_convertBddToPred :
        (int -> Ast.Predicate.predicate) -> CaddieBdd.bdd -> Ast.Predicate.predicate
      val convertBddToPred : CaddieBdd.bdd -> Ast.Predicate.predicate
      val bdd_to_string : CaddieBdd.bdd -> string
      val bdd_to_string_pretty : CaddieBdd.bdd -> string
      val update_transrel_bdd :
        (int * int) * (int * int) -> CaddieBdd.bdd -> unit
      val update_global_inv_bdd : CaddieBdd.bdd -> unit
      val bdd_node_count : CaddieBdd.bdd -> int
      val bdd_top_var : CaddieBdd.bdd -> int
      val bdd_hash : CaddieBdd.bdd -> int * int
      val string_to_atom : Ast.Symbol.symbol -> Ast.Predicate.predicate
      class ['a] bdd_hash :
        object
          val bdd_table : (int * int, (CaddieBdd.bdd * 'a) list) Hashtbl.t
          method add_new : CaddieBdd.bdd -> 'a -> unit
          method find : CaddieBdd.bdd -> 'a
        end
      val bdd_fold :
        (int -> 'a -> 'a -> 'a) -> (bool -> 'a) -> CaddieBdd.bdd -> 'a
      val bdd_tmp_var_idx : int ref
      val next_bdd_tmp_atom : unit -> Ast.Predicate.predicate
      val bdd_to_pred_map :
        (int -> Ast.Predicate.predicate) -> CaddieBdd.bdd -> Ast.Predicate.predicate
      val bdd_to_predicate : CaddieBdd.bdd -> Ast.Predicate.predicate
    val refine_counter : int ref
val refined : unit -> unit
val last_refine_counter : int ref
val refine_check : unit -> bool
val globally_useful_preds : int list ref
val blast_error_lval : Ast.Expression.lval
val globally_important_lvals : Ast.Expression.lval list ref
val globally_important_lvals_table : (Ast.Expression.lval, bool) Hashtbl.t
val all_important_lvals_table : (Ast.Expression.lval, bool) Hashtbl.t
val __SKIPFunctionName : string
val __NotImplementedFunctionName : string
val always_take_fun_table : (string, bool) Hashtbl.t
val always_skip_fun_table : (string, bool) Hashtbl.t
val add_skip_fun : string -> unit
val delete_skip_fun : string -> unit
val stats_nb_take_funs : int ref
val add_take_fun : string -> unit
val always_take : string -> bool
val always_skip : string -> bool
val relevant_globals_mod_table : (string, Ast.Expression.lval list) Hashtbl.t
val relevant_fields_table : (string, bool) Hashtbl.t
val update_global_lvals : Ast.Expression.lval -> unit
val ignorable_globals : Ast.Expression.lval list
val add_relevant_fields : Ast.Expression.lval -> unit
val is_relevant_field_image : string -> bool
val update_imp_lv_table : Ast.Predicate.predicate -> unit
val ac_is_relevant_lval : Ast.Expression.lval -> bool
val is_relevant_lval : Ast.Expression.lval -> bool
val relevant_globals_mod : string -> Ast.Expression.lval list
val pred_affected_table : (C_SD.edge_id_t * int, bool) Hashtbl.t
val update_pred_affected_table : C_SD.edge_id_t -> int -> unit
val is_pred_affected : C_SD.edge_id_t -> int -> bool
val is_pred_global : int -> bool
val isConstant : Ast.Expression.expression -> bool
val lval_constant_table :
  (Ast.Expression.expression, Ast.Expression.expression list) Hashtbl.t
val make_const_table : unit -> unit
val getConstants : Ast.Predicate.predicate -> Ast.Expression.expression list
val constant_preds : Ast.Predicate.predicate -> Ast.Predicate.predicate list
val actual_addPred : Ast.Predicate.predicate -> int
val addPred : Ast.Predicate.predicate -> int
val just_addPred : Ast.Predicate.predicate -> int
val add_pred_to_scope : int -> int option
val getPredsContainingVar : Ast.Symbol.symbol -> Ast.Predicate.predicate list
val theoremProverCache : (string , bool) Hashtbl.t
val statTotalNumberOfQueries : int ref
val statCoveredQueries : int ref
val statTotalCached : int ref
val statPostBddCalls : int ref
val statWorstCaseQueriesForPost : int ref
val statActualQueriesForPost : int ref
val statNonDCQueriesForPost : int ref
val statAssumeQueriesForPost : int ref
val statNumPost : int ref
val statNumAssumePost : int ref
val statReachedCubes : int ref
val statVisitedFunctionTable : (string, bool) Hashtbl.t
val reset_abs_stats : unit -> unit
exception SatisfiableException
val isSatisfiable : CaddieBdd.bdd * Ast.Predicate.predicate list -> bool
val askTheoremProver : Ast.Predicate.predicate -> bool
val askTheoremProverContext :
  Ast.Predicate.predicate -> Ast.Predicate.predicate -> bool
val theoremProverCache_proof :
  (Ast.Predicate.predicate, Proof.proof * int) Hashtbl.t
val trivialProof : Proof.proof
val statTotalNumberOfQueries_proof : int ref
val statTotalCached_proof : int ref
val statTotalMismatches : int ref
val vampyre_lemma_counter : int ref
val generateLfOutput : Ast.Predicate.predicate -> Proof.proof -> unit
val generateLfOutput_noproof : Ast.Predicate.predicate -> unit
val askTheoremProver_proof : Ast.Predicate.predicate -> int option
val askTheoremProver_noproof :
  Ast.Predicate.predicate -> Ast.Predicate.predicate -> int option
val bat_counter : int ref
val refined_fun_table : (string, bool) Hashtbl.t
val cfb_local_refine_table : (string, bool) Hashtbl.t
val block_a_t_reset : unit -> unit
val is_fun_refined : string -> bool
val add_local_preds : int * int -> int list -> unit
val lookup_location_predicates : int * int -> int list
val print_pred_vector : unit -> unit
val ctrbddManager : CaddieBdd.ddMgr
val ctrbddZero : CaddieBdd.bdd
val ctrbddOne : CaddieBdd.bdd
val freeBddVars : CaddieBdd.bdd list ref
val getBddVar : unit -> CaddieBdd.bdd
val bddMap : (int * CaddieBdd.bdd list) list ref
val allocateCtrBdds : int list -> int -> unit
val destroyCtrBdds : unit -> unit
val getBdd : int -> Ast.Counter.counter -> CaddieBdd.bdd
val ctrmap_to_bdd : Ast.Counter.counter array -> CaddieBdd.bdd
val ctrmap_check_covered : Ast.Counter.counter array -> CaddieBdd.bdd -> bool
val ctrmap_exists_stuck : CaddieBdd.bdd -> bool
type abstraction_info =
    AbsPred of Ast.Predicate.predicate
  | LocPredTuple of ((int * int) * int list)
  | SymLvalTuple of (string * (Ast.Expression.lval * Ast.Expression.lval) list)
val get_AbsPred : abstraction_info -> Ast.Predicate.predicate option
val get_LocPredTuple : abstraction_info -> ((int * int) * int list) option
val get_SymLvalTuple :
  abstraction_info ->
  (string * (Ast.Expression.lval * Ast.Expression.lval) list) option
val relevant_symvar_table : (string, Ast.Expression.lval list) Hashtbl.t
val add_relevant_symvar : string -> Ast.Expression.lval -> unit
val get_relevant_symvar : string -> Ast.Expression.lval list
val initialize_skipfun : unit -> unit
val dump_abs : unit -> unit
val load_abs : unit -> unit
  end


module type EVENTS =
  sig
    module C_SD : SYSTEM_DESCRIPTION
    type eventHandler 
    val eventHandler_to_string : eventHandler -> string
    val eventHandler_to_command : eventHandler -> C_SD.Command.t
    val deconstructAsyncCall : C_SD.edge -> (eventHandler * CaddieBdd.bdd)
    val constructAsyncCall : C_SD.edge * CaddieBdd.bdd -> C_SD.edge
  end

(**
 * The ABSTRACTION signature groups an operation module and a region module
 * and specifies abstract post and pre functions.  These two functions are
 * supposed to be conservative with respect to the precise semantics of the
 * operations.  It also specifies the precise and focus functions used for
 * refinement.
 *)
module type ABSTRACTION =
sig
  module Operation : OPERATION
  module Region : REGION

  val create_region : bool -> Ast.Predicate.predicate -> Ast.Predicate.predicate -> Region.t

  val initialize_abstraction : unit -> unit 

  val post : Region.t -> Operation.t -> Region.t

  (** [summary_post caller exit retop ] creates a region to be used 
   * after the function call whose post-condition caller represents, 
   * where exit is the region of the used summary node's exit *)
  val summary_post : Region.t -> Region.t -> Operation.t -> Operation.t -> Region.t

  (** check if a tree edge is sound check_abstract_transition reg op reg' checks 
  if post.(reg,op) \subseteq reg' *)
  val check_abstract_transition : Region.t -> Operation.t -> Region.t -> bool
  
  (** this next thing is a measure to fight the simplify silently dying on us! *) 
  val reset_decision_procedures : unit -> unit 

  (** notify abstraction module of final outcome, for GUI *)
  val notify_result : string -> unit
  
  val dump_abs : unit -> unit 

  (** Raised when no new predicates are found after counterexample analysis. (Blast fails) *)
  exception NoNewPredicatesException
  exception RecursionException  
  exception RefineFunctionException of string (* refine from all entries of fname *)

  (** was a location belonging to fn refined in the last refine ? *)
  val is_fun_refined : string -> bool

  (** analyze a trace as a bunch of blocks foci *)
  val block_analyze_trace : Region.t list -> Operation.t list -> int 

  val print_stats : unit -> unit

  (** gives the possibly enabled operations in a given region *)
  val enabled_ops  : Region.t -> Operation.t list

  val reset : unit -> unit

  val fociModelChecker_refine_trace_abstract : 
    Region.t array -> Operation.t array -> (int *  Region.t list)

end

module ModelCheckOutcome =
struct
  (* Outcome type polymorphic in error_path type *)
  type 'a outcome =
    | No_error
    | Error_reached of 'a
    | Race_condition of 'a * 'a * Ast.Expression.lval * Ast.Expression.lval
    | Bad_lock_spec of 'a * Ast.Expression.lval
end

module type MAKE_LMC = 
  functor(A : ABSTRACTION) ->
  sig
    val model_check : A.Region.t  -> A.Region.t -> int 
  end 

