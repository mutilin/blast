(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
module E :
  sig
    type binary_op
    type lval
    type expression
  end
module P :
  sig
    type predicate = Ast.Predicate.predicate 
  end
val convertToVampyreSyntax : P.predicate -> Input.pred 
val set_simplify_query_flag : unit -> unit
val kill_simplify_server : unit -> unit
val ques_flag : bool ref
val queryExp_proof : P.predicate -> Proof.proof option
val uclidSat : P.predicate -> string list
val uclid_output : string -> P.predicate -> string -> unit
val getUsefulPredicates : P.predicate -> P.predicate list
val stats_nb_foci_queries : int ref
exception FociSatException
exception FociSatExceptionAsgn of string list
type foci_return = Sat | Unsat of P.predicate
val extract_foci_preds :
  P.predicate list * P.predicate list -> P.predicate list
val extract_foci_preds_parity :
  P.predicate list * P.predicate list -> (P.predicate * bool) list
val extract_foci_interpolant :
  P.predicate list * P.predicate list -> P.predicate
val interpolant_sat : P.predicate * P.predicate -> foci_return
val interpolant_unsat : P.predicate -> bool
val extract_foci_interpolantN : P.predicate list -> P.predicate list
val extract_foci_interpolantN_noconv : P.predicate list -> P.predicate list
val extract_foci_interpolantN_symm :
  P.predicate list -> P.predicate list * 'a list
val extract_minp_interpolantN : P.predicate list -> P.predicate list
val extract_foci_interpolantN_dasdill :
  P.predicate list -> P.predicate list -> P.predicate list
val foci_inc_pop : unit -> unit
val foci_inc_push : P.predicate -> unit
val foci_inc_check_sat : unit -> bool
val foci_inc_check_unsat : P.predicate -> bool
val foci_inc_is_contra : unit -> bool
val foci_inc_reset : unit -> unit
val queryExpContext : P.predicate -> P.predicate -> bool
val queryExp : P.predicate -> bool
val block_assert : P.predicate -> unit
val block_pop : unit -> unit
val block_reset : unit -> unit
val _is_contra : unit -> bool
val sat : P.predicate -> bool
val reset : unit -> unit
val block_check_unsat : P.predicate -> bool
val _assume : P.predicate -> unit
val _pop : unit -> unit
val generateTest : 'a -> 'b
