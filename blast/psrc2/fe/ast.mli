(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * This module defines the internal representation used
 * by BLAST to represent expressions and predicates.
 *)
module Symbol : 
sig 
  type symbol = string (* change later *)
  type t = symbol
  val compare : t -> t -> int (* So that Symbol : Set.OrderedType *)
  val toString : symbol -> string 
  val print : Format.formatter -> symbol -> unit 
end

type modifier = Static | Abstract | Final | Public | Protected | Private
  | Strictfp (* what is Strictfp? *)
  | Native | Synchronized | Transient | Volatile
  | Interface (* Wes did not have interface as a modifier, but Soot has it *)
module Constant :
sig
  type constant = 
      Int     of int		(* SLAM uses int32?? so that we get all 32 bits *)
    | Long    of int64
    | Float   of float
    | String  of string
    | EnumC   of int * string (* SLAM uses int32 *)
    | Null

  val toString : constant -> string
end

module JavaType :
sig
type type_or_void = (* method result type *) 
  | Type of java_type
  | Void

and java_type = 
    Primitive of primitive_type
  | Reference of reference_type

and primitive_type = BooleanType | CharType | ByteType | ShortType | IntType | LongType 
  | FloatType | DoubleType 
  | VoidType (* I keep Void here to please the C internal representation.
                This will not occur for Java.
		*)

and reference_type = 
        | ClassOrInterfaceType of Symbol.symbol
        | ArrayType of java_type (* base element type *) * int32 (* number of dimensions *)
	| NullType (* Soot defines a type for Java's null *)

val toString : java_type -> string

val print : Format.formatter -> java_type -> unit

end

module Expression : 
sig
  type access_op = Dot | Arrow
      
  type assign_op = 
      Assign | AssignBitAnd | AssignBitOr | AssignDiv | AssignLShift | AssignMinus
    | AssignMul | AssignPlus | AssignRem | AssignRShift | AssignXor
	  
  type unary_op  = 
      UnaryMinus | Not | BitNot | Address | UnaryPlus | DestructorCall | Assume | Assert
    | PreInc | PreDec | PostInc | PostDec
	  
  type binary_op = 
      BitAnd | BitOr | Div | LShift | Minus | Mul | Plus | Rem | RShift | Xor 
    | Eq | Ge | Gt | Le | Lt | Ne | Index | And | Or | Comma 
    | FieldOffset | Offset 

  type lval =	
      Symbol of Symbol.symbol
    | This
    | Access of access_op * expression * Symbol.symbol
    | Dereference of expression
    | Indexed of expression * expression
  and
	expression =
      Lval of lval 
    | Chlval of lval * string
    | Assignment of assign_op * lval * expression (* op, target, value *)
    | Binary of binary_op * expression * expression
    | Cast of JavaType.java_type * expression	(* only deal with cast (T)e *)
    | CastCil of Cil.typ * expression	(* only deal with cast (T)e *)
    | Constant of Constant.constant
    | Constructor of Symbol.symbol * JavaType.java_type
    | MethodCall of expression * expression * expression * expression * (expression list)
    | FunctionCall of expression * expression list
    | Select of expression * expression
    | Store of expression * expression * expression
    | Sizeof of int
    | Unary of unary_op * expression
    | InstanceOf of expression * JavaType.java_type

  (** t and compare are added to make Expression a Set.OrderedType.
      With these, we can put expressions in sets.
      The type t is of course the same as the type expression,
      and compare is the system compare
      *)
  type t = expression
  val compare : t -> t -> int

  val unaryOpToString : unary_op -> string 
  
  (** a vanilla string is a string over the alphabet 
     \[a-zA-Z0-9\] ; it is useful because the theorem provers/other backend tools dislike
     expressions like "|". Vanilla string for this is BitOr.
  *)
  val unaryOpToVanillaString : unary_op -> string 

  val assignOpToString : assign_op -> string

  val accessOpToString : access_op -> string 
  (* val uf_select : expression
  val uf_store : expression *)
  val binaryOpToString : binary_op -> string
  val binaryOpToVanillaString : binary_op -> string
  val binaryOpFromVanillaString : string -> binary_op
  (** {b Printers.} *)

  val print : Format.formatter -> expression -> unit
  val print_lval : Format.formatter -> lval -> unit
  val toString : expression -> string
  val lvalToString : lval -> string

(** to construct and deconstruct symbolic constants *)
  val make_symvar : string -> lval -> lval
  val peel_symbolic : lval -> lval
  val is_symbolic : lval -> bool

  val is_interpreted_lval : lval -> bool
  val is_interpreted : string -> bool
  val is_interpreted_renamable : string -> bool

  (** {b Miscellaneous utilities.} *)

 (** the name says it all *)
  val lv_of_expr : expression -> lval 

	
  (** Push all address operators to the variables.
      See the memory model discussions.
      *)
  val addressOf : expression -> expression

  (** like above, but strips down address so MCVAR+address uniquely determine
   * lval. mc_addressOf a[i] is "i", not a+i or a offset i similarly for fields. *)
  val mc_addressOf : expression -> expression
  
  (** replace all occurrences of a Chlval by corresponding Lval *)
  val replaceChlvals : expression -> expression

  val isRelOp : binary_op -> bool  
  (* check if an Expression is of the form *p or ( *p ).f or p->f *)
  val isDeref : expression -> bool
  
  (* push dereferences below address of operations *)
  val push_deref : lval -> lval
 
  (* given a deref expression *p or ( *p ).f or p->f, return the derefed symbol p *)
  val derefedVar : lval -> expression
  (* get the variables p s.t. *p or ( *p ).f or p->f appear in varExps *)
  val getParents : lval list -> expression list
  (* return the expressions of the form x, x.f, *x, &x, x->f, and ( *x ).f in the 
     given expression.  we assume those are the only forms. 
     also, because of our assumption that whole-struct assignments do not occur, we
     do not go "inside" an expression of the form x->f or ( *x ).f .  that is, we return it, but not
     *x as a variable in the expression. *)
  val allVarExps : expression -> expression list
  val allVarExps_deep : expression -> lval list
  val varsOfExp : expression -> string list 

  val fields_of_expr : expression -> string list
  val fields_of_lval : lval -> string list
  
  
  val is_chlval : expression -> bool	
  val is_lval : expression -> bool	
	(* is this irrelevant ? see allVarExps  *)
  val lvals_of_expression : expression -> lval list	

  (* Negate a relational expression. *)
  val negateRel : expression -> expression

  (** returns a chlval that is essentially a "Fresh" version of that lval *)
  val fresh_lval : lval -> expression

  (* substitute *)
  val alpha_convert : (lval -> lval) -> expression -> expression
  val symbol_alpha_convert : (lval -> lval) -> expression -> expression

  val alpha_convert_lval : (lval -> lval) -> lval -> lval
 
  (** deep_alpha_convert f: lval -> expr 
    where f: lval -> expr essentially f has the chlval information ... this substitutes the lval at the
    top level and then goes inside and recursively goes in and substitutes
    *)
  val deep_alpha_convert : (lval -> expression) -> expression -> expression
  val deep_alpha_convert_lval : (lval -> expression) -> lval -> lval

  val substituteExpression : (expression * expression) list -> expression -> expression
  val subsExpInLval : (expression * expression) list -> lval -> lval
	
  val assignments_of_expression : expression -> (expression * expression) list

  (** how "deep" inside lv does lv' occur .. -1 if it doesnt occur at all*)
  (* actually, this is a hack right now -- hey! -- and doesnt work if lval
   * lv' occurs twice inside lv *)
  val depth_of_lval : lval -> lval -> int
  
  (** occurs_check e1 e2 is true iff e1 occurs inside e2 *)
  val occurs_check : expression -> expression -> bool

  (** deep_transform f e -- recursively goes inside e doing surgery on subexpressions in a bottom up way *)
  val deep_transform : (expression -> expression) -> expression -> expression

	(** a variant on the above that is top down -- WARNING -- if f is not non-decreasing, will diverge! *)
  val deep_transform_top : (expression -> expression) -> expression -> expression

  val deep_transform_top_cond : (expression -> (expression * bool)) -> expression -> expression
 	
  (** changes chlval back to lval -- used for "cleaning" interpolants *)
  val unprime : expression -> expression

  (** Normalizes expressions containing & INCLUDING Chlvals. Recursively replaces occ of Unary(Address e) with addressOf e *)
  val address_normalize : expression -> expression
	
  (** get the symbol from an lval *)
  val fullsimp_lval : lval -> lval

  val constantfold : expression -> (lval, expression) Hashtbl.t -> expression

  val is_mc_var : lval -> bool

  val mc_var : (lval -> lval list) -> lval -> expression option

  val to_mccarthy : (lval -> lval list) -> expression -> expression
  

end

type predicateVal = True | False | Dontknow

(**
 * A predicate is a boolean combination of atomic expressions.
 * The signature allows All and Exist quantifiers, but we never
 * use them. 
 *)
module Predicate :
sig
  type predicate =
      True
    | False
    | And of predicate list
    | Or of predicate list
    | Not of predicate
    | Implies of predicate * predicate
    | Iff of predicate * predicate
    | All of Symbol.symbol * predicate (* Forall quantifier -- symbol is the variable 
					  being quantified *)
    | Exist of Symbol.symbol * predicate (* Exists quantifier -- symbol is the variable 
					    being quantified *)
    | Atom of Expression.expression
    | Next of predicate 

  (** Various useful normalizations : e.g., And \[...; False; ...\] -> False etc *)
  val normalize : predicate -> predicate 

  val conjoinL : predicate list -> predicate (* And of predicate list *)
  val disjoinL : predicate list -> predicate (* Or of predicate list *)
  val implies : predicate -> predicate -> predicate
  val iff : predicate -> predicate -> predicate
  val ite : predicate -> predicate -> predicate -> predicate 
  (* Negate a predicate: does not go inside Atoms *)
  val negate : predicate -> predicate
  (* Negate an atomic predicate deeply *)
  val negateAtom : predicate -> predicate

  (** Convert a boolean expression to DNF form. *)
  val convertDNF : predicate -> predicate
  val to_dnf : predicate -> predicate list
  
  val toString : predicate -> string
  val print : Format.formatter -> predicate -> unit


  val allVarExps : predicate -> Expression.expression list
  val allVarExps_deep : predicate -> Expression.lval list
  val varsOfPred : predicate -> string list
  
  val getAtoms : predicate -> predicate list
  val getAtoms_parity : bool -> predicate -> (predicate * bool) list 

      (* is this redundant? see allVarExps_deep *)
  val lvals_of_predicate : predicate -> Expression.lval list
     

  (** [map sym_f expr_f p] ==> [p']
      Return a predicate with the same structure, and [sym_f] applied to each
      *bound* symbol at the place where it is bound, and [expr_f] applied to
      each expression. The first argument passed to [expr_f] is a function
      that tests whether its argument is *bound*. *)
  val map : (Symbol.symbol -> Symbol.symbol) ->
            ((Expression.lval -> bool) ->
              Expression.expression -> Expression.expression) ->
            predicate -> predicate
  val alpha_convert : (Expression.lval -> Expression.lval) -> predicate -> predicate
  val symbol_alpha_convert : (Expression.lval -> Expression.lval) -> predicate -> predicate

  val deep_alpha_convert : (Expression.lval -> Expression.expression) -> predicate -> predicate
    
  val deep_transform : (predicate -> predicate) -> predicate -> predicate

  (** atom_transform f p = p[Atom (f e)/ Atom e] i.e replaces each Atom e with Atom (f e) *)   
  val atom_transform : (Expression.expression -> Expression.expression) -> predicate -> predicate
    
  val expr_equate : Expression.expression -> Expression.expression -> predicate
    
   
  val equate : Expression.expression list -> Expression.expression list -> predicate
  
  val min_equate : Expression.expression list -> Expression.expression list -> predicate
    (** renames all chlvals to the original -- useful for "cleaning" interpolants *)
  val unprime : predicate -> predicate 

    (** Lifts Expression.address_normalize to predicates, by transforming each atom *)
  val address_normalize : predicate -> predicate
    
 (** substitute *)


  val substitute: (Expression.expression * Expression.expression) list -> predicate -> predicate

  val canonicize : predicate -> predicate
  
  val eq_canonicize : predicate -> predicate 

  val constantfold : predicate -> (Expression.lval, Expression.expression) Hashtbl.t -> predicate

  val to_mccarthy : (Expression.lval -> Expression.lval list) -> predicate -> predicate
  
  val normalize_interpreted : predicate -> predicate

  val strip_quantifier_prefix : predicate -> ((bool * Symbol.symbol) list * predicate)  
    
  val stick_quantifier_prefix : predicate -> (bool * Symbol.symbol) list -> predicate

end

(**
   Module that implements the 0,1, ..., k, omega counter abstraction.
   Maxcount is stored internally as an integer, and omega is represented simply
   by Maxcount + 1. However the representation is opaque to the callers.
   *)
module Counter : 
  sig
    type counter 
    val set_max : int -> unit
    val set_counter : int -> unit
    val omega : unit -> counter
    val is_omega : counter -> bool
    val is_zero : counter -> bool
    val make_counter : int -> counter
    val to_int : counter -> int
    val to_string : counter -> string
    val plus : counter -> counter -> counter
    val minus : counter -> counter -> counter
    val leq : counter -> counter -> bool
    val geq : counter -> counter -> bool
    val eq : counter -> counter -> bool
    val ctrmax : counter -> counter -> counter
    val ctrmin : counter -> counter -> counter
    val array_plus  : counter array -> counter array -> counter array
    val array_minus : counter array -> counter array -> counter array
    val array_max   : counter array -> counter array -> counter array
    val array_min   : counter array -> counter array -> counter array
    val toString : counter -> string	
    val print : Format.formatter -> counter -> unit
  end

