(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

(**
   Depends on:
     Ast

**)

open Jni

open Ast

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate

module Command = BlastSystemDescr.Command
  
(* module to convert types between java and ocaml *)
module SootToAstGlue =
struct

  let rec get_type_or_void t (* : soot.Type *)  : JavaType.type_or_void  =
    if (is_instance_of t (find_class "soot/VoidType")) then
      JavaType.Void
    else
      JavaType.Type (get_type t)
  and
  get_type t (* : soot.Type *)= 
      begin
	if (is_instance_of t (find_class "soot/PrimType")) then
	  (* primitive types *)
	  begin
	    JavaType.Primitive (
	      if (is_instance_of t (find_class "soot/BooleanType")) then
		JavaType.BooleanType
	      else
		(* TAC: Added this case *)
		if (is_instance_of t (find_class "soot/ByteType")) then
		  JavaType.ByteType
		else
		  if (is_instance_of t (find_class "soot/CharType")) then
		    JavaType.CharType
		  else
		    if (is_instance_of t (find_class "soot/DoubleType")) then
		      JavaType.DoubleType
		    else
		      if (is_instance_of t (find_class "soot/FloatType")) then
			JavaType.FloatType
		      else
			if (is_instance_of t (find_class "soot/IntType")) then
			  JavaType.IntType
			else
			  if (is_instance_of t (find_class "soot/LongType")) then
			    JavaType.LongType
			  else
			    if (is_instance_of t (find_class "soot/ShortType")) then
			      JavaType.ShortType
			    else
			      failwith "get_jabs_type : Unknown primitive type"
	    ) (* Primitive *)
	  end
	else
	  begin
	    JavaType.Reference (
	    let jcls_soot_ArrayType = (find_class "soot/ArrayType") in
	    if (is_instance_of t jcls_soot_ArrayType) then
	      begin
		let basetype = get_object_field t (get_fieldID jcls_soot_ArrayType "baseType" "Lsoot/Type;") in
		let num_of_dim = get_int_field t (get_fieldID jcls_soot_ArrayType "numDimensions" "I") in
		let tp = get_type basetype in
		JavaType.ArrayType (tp, num_of_dim)
	      end
	    else
	      if (is_instance_of t (find_class "soot/RefType")) then
		let classname = call_object_method t (get_methodID 
                  (find_class "soot/RefType") "getClassName" "()Ljava/lang/String;") [||]
		in 
		JavaType.ClassOrInterfaceType (string_from_java  classname) 
	      else
		if (is_instance_of t (find_class "soot/NullType")) then
		  JavaType.NullType
		else
		  failwith "get_jabs_type: Unknown reference type!"
	    )
	  end
      end


end (* module SootToAstGlue *)

    
let com = call_object_method
let gmi = get_methodID
let cbm = call_boolean_method
let cvm = call_void_method
let csom = call_static_object_method
let csbm = call_static_boolean_method
let gsmi = get_static_methodID
let cim = call_camlint_method
	     
module Scene =
struct
  (**
    The main interesting variables are scene (that stores a SOOT Scene), ...
  *)
  let sclass = find_class "soot/Scene"
		 
  let sobj = (* : soot.Scene *) 
    let m_v = get_static_methodID sclass "v" "()Lsoot/Scene;" in
      call_static_object_method sclass m_v [| |] (* Scene.v() *)
	
  (* now defining all the methods for the class Scene in ocaml *)
  let loadClassAndSupport java_s =
    com sobj (gmi sclass "loadClassAndSupport" "(Ljava/lang/String;)Lsoot/SootClass;") [|Obj(java_s)|]
  let getOrMakeFastHierarchy _ = 
    com sobj (gmi sclass "getOrMakeFastHierarchy" "()Lsoot/FastHierarchy;") [| |] 
  let hasCallGraph _ =
    cbm sobj (gmi sclass "hasCallGraph" "()Z") [| |]
  let getClasses _ =
    com sobj (gmi sclass "getClasses" "()Lsoot/util/Chain;") [| |]
  let getSootClass name =
    com sobj (gmi sclass "getSootClass" "(Ljava/lang/String;)Lsoot/SootClass;") [|Obj(name)|]
end (* module Scene *)


module SootClass =
struct
  let sclass = find_class "soot/SootClass" 

  let getMethods obj =
    com obj (gmi sclass "getMethods" "()Ljava/util/List;") [| |]
  let setApplicationClass obj =
    cvm obj (gmi sclass "setApplicationClass" "()V") [| |]
  let getName obj =
    com obj (gmi sclass "getName" "()Ljava/lang/String;") [| |]
  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/RefType;") [||]
  let getFields obj =
    com obj (gmi sclass "getFields" "()Lsoot/util/Chain;") [||]
  let getModifiers obj =
    cim obj (gmi sclass "getModifiers" "()I") [||]
  let declaresMethodBySubsignature obj subsig =
    cbm obj (gmi sclass "declaresMethod" "(Ljava/lang/String;)Z") [|Obj subsig|]
end (* module SootClass *)


module Modifier =
struct
  let sclass = find_class "soot/Modifier"

  let isAbstract i =
    csbm sclass (gsmi sclass "isAbstract" "(I)Z") [|Camlint i|]
  let isFinal i =
    csbm sclass (gsmi sclass "isFinal" "(I)Z") [|Camlint i|]
  let isInterface i =
    csbm sclass (gsmi sclass "isInterface" "(I)Z") [|Camlint i|]
  let isNative i =
    csbm sclass (gsmi sclass "isNative" "(I)Z") [|Camlint i|]
  let isPrivate i =
    csbm sclass (gsmi sclass "isPrivate" "(I)Z") [|Camlint i|]
  let isProtected i =
    csbm sclass (gsmi sclass "isProtected" "(I)Z") [|Camlint i|]
  let isPublic i =
    csbm sclass (gsmi sclass "isPublic" "(I)Z") [|Camlint i|]
  let isStatic i =
    csbm sclass (gsmi sclass "isStatic" "(I)Z") [|Camlint i|]
  let isSynchronized i =
    csbm sclass (gsmi sclass "isSynchronized" "(I)Z") [|Camlint i|]
  let isTransient i =
    csbm sclass (gsmi sclass "isTransient" "(I)Z") [|Camlint i|]
  let isVolatile i =
    csbm sclass (gsmi sclass "isVolatile" "(I)Z") [|Camlint i|]
  let toString i =
    csom sclass (gsmi sclass "toString" "(I)Ljava/lang/Strig;") [|Camlint i|]
end (* module Modifier *)
  
module SootField =
struct
  let sclass = find_class "soot/SootField"

  let getName obj =
    com obj (gmi sclass "getName" "()Ljava/lang/String;") [||]
  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
  let getModifiers obj =
    cim obj (gmi sclass "getModifiers" "()I") [||]
  let getDeclaringClass obj =
    com obj (gmi sclass "getDeclaringClass" "()Lsoot/SootClass;") [||]
  let isStatic obj =
    cbm obj (gmi sclass "isStatic" "()Z") [||]
end (* module SootField *)

  
module Body =
struct
  let sclass = find_class "soot/Body"

  let getLocals obj =
    com obj (gmi sclass "getLocals" "()Lsoot/util/Chain;") [||]
  let getUnits obj =
    com obj (gmi sclass "getUnits" "()Lsoot/PatchingChain;") [||]
  let getParameterLocal obj index =
    com obj (gmi sclass "getParameterLocal" "(I)Lsoot/Local;") [|Camlint index|]
end

module SootMethod =
struct
  let sclass = find_class "soot/SootMethod"

  let getName obj = 
    com obj (gmi sclass "getName" "()Ljava/lang/String;") [| |]
  let isConcrete obj =
    cbm obj (gmi sclass "isConcrete" "()Z") [||]
  let getParameterCount obj =
    cim obj (gmi sclass "getParameterCount" "()I") [||]
  let retrieveActiveBody obj =
    com obj (gmi sclass "retrieveActiveBody" "()Lsoot/Body;") [||]
  let getArgs obj =
    let count = ref 0
    in let total = getParameterCount obj
    in let abody = retrieveActiveBody obj
    in let arglist = ref []
    in begin
	while (!count < total)
	do
	  let arg = Body.getParameterLocal abody !count
	  in
	    arglist := !arglist @ [arg];
	    count := !count + 1
	done;
	!arglist
      end
  let getDeclaringClass obj =
    com obj (gmi sclass "getDeclaringClass" "()Lsoot/SootClass;") [||]
  let getDeclaringClassName obj =
    SootClass.getName (getDeclaringClass obj)
  let getSignature obj =
    com obj (gmi sclass "getSignature" "()Ljava/lang/String;") [||]
  let getSubSignature obj =
    com obj (gmi sclass "getSubSignature" "()Ljava/lang/String;") [||]
end (* module SootMethod *)

module CompleteBlockGraph =
struct
  let sclass = find_class "soot/toolkits/graph/CompleteBlockGraph"

  let getSuccsOf obj b =
    com obj (gmi sclass "getSuccsOf" "(Ljava/lang/Object;)Ljava/util/List;") [| Obj b |]
  let getHeads obj =
    com obj (gmi sclass "getHeads" "()Ljava/util/List;") [||]
  let getBlocks obj =
    com obj (gmi sclass "getBlocks" "()Ljava/util/List;") [||]
end

module InvokeStmt =
struct
  let sclass = find_class "soot/jimple/InvokeStmt"

  let getInvokeExpr obj =
    com obj (gmi sclass "getInvokeExpr" "()Lsoot/jimple/InvokeExpr;") [||]
end

module ParameterRef =
struct
  let sclass = find_class "soot/jimple/ParameterRef"

  let getIndex obj = 
    cim obj (gmi sclass "getIndex" "()I") [||]
end	 
  
module CaughtExceptionRef =
struct
  let sclass = find_class "soot/jimple/CaughtExceptionRef"

  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
end

module BinopExpr =
struct
  let sclass = find_class "soot/jimple/BinopExpr"

  let getOp1 obj =
    com obj (gmi sclass "getOp1" "()Lsoot/Value;") [||]
  let getOp2 obj =
    com obj (gmi sclass "getOp2" "()Lsoot/Value;") [||]
end
  
module NewExpr =
struct
  let sclass = find_class "soot/jimple/NewExpr"

  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
end
		 
  
module InstanceInvokeExpr =
struct
  let sclass = find_class "soot/jimple/InstanceInvokeExpr"

  let getBase obj =
    com obj (gmi sclass "getBase" "()Lsoot/Value;") [||]
end

module StaticInvokeExpr =
struct
  let sclass = find_class "soot/jimple/StaticInvokeExpr"

  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
end
  
module InvokeExpr =
struct
  let sclass = find_class "soot/jimple/InvokeExpr"

  let getMethod obj =
    com obj (gmi sclass "getMethod" "()Lsoot/SootMethod;") [||]
  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
  let getArgs obj =
    com obj (gmi sclass "getArgs" "()Ljava/util/List;") [||]
  let getArg obj index =
    com obj (gmi sclass "getArg" "()Lsoot/Value;") [|Camlint index|]
end

module RefType =
struct
  let sclass = find_class "soot/RefType"

  let getClassName obj = 
    com obj (gmi sclass "getClassName" "()Ljava/lang/String;") [||]
end
  
module Local =
struct
  let sclass = find_class "soot/Local"

  let getName obj =
    com obj (gmi sclass "getName" "()Ljava/lang/String;") [||]
  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
  let setName obj name =
    cvm obj (gmi sclass "setName" "(Ljava/lang/String;)V") [|Obj name|]
end

module Unit =
struct
  let sclass = find_class "soot/Unit"

  let getUseAndDefBoxes obj =
    com obj (gmi sclass "getUseAndDefBoxes" "()Ljava/util/List;") [||]
end

module ValueBox =
struct
  let sclass = find_class "soot/ValueBox"

  let getValue obj =
    com obj (gmi sclass "getValue" "()Lsoot/Value;") [||]
end
  
module Value =
struct
  let sclass = find_class "soot/Value"

  let getType obj =
    com obj (gmi sclass "getType" "()Lsoot/Type;") [||]
  let getUseBoxes obj =
    com obj (gmi sclass "getUseBoxes" "()Ljava/util/List;") [||]
  
  let rec convertToBlastExp v =
    if is_instance_of v BinopExpr.sclass then
      convertBinopExpr v 
    else
      let jcls_CastExpr = find_class "soot/jimple/CastExpr" in
	if is_instance_of v jcls_CastExpr then
	  begin
	    let op = call_object_method v (get_methodID jcls_CastExpr "getOp" "()Lsoot/Value;") [||] in
	    let t = call_object_method v (get_methodID jcls_CastExpr "getType" "()Lsoot/Type;") [||] in
	      Expression.Cast (SootToAstGlue.get_type t, convertToBlastExp op)
	  end
	else
	  let jcls_InstanceOfExpr = find_class "soot/jimple/InstanceOfExpr" in
	    if is_instance_of v jcls_InstanceOfExpr then
	      begin
		let op = call_object_method v (get_methodID jcls_InstanceOfExpr "getOp" "()Lsoot/Value;") [||] in
		let t = call_object_method v (get_methodID jcls_InstanceOfExpr "getType" "()Lsoot/Type;") [||] in
		  Expression.InstanceOf (convertToBlastExp op, SootToAstGlue.get_type t)
	      end
	    else
	      let jcls_Constant = find_class "soot/jimple/Constant" in (* Constants *)
		if is_instance_of v jcls_Constant then
		  begin
		    Expression.Constant (convertToBlastConstant v)
		  end
		else
		  let jcls_Local = find_class "soot/Local" in (* Local variables *)
		    if is_instance_of v jcls_Local then
		      begin
			let l = call_object_method v (get_methodID jcls_Local "getName" "()Ljava/lang/String;") [||] in
			  Expression.Lval (Expression.Symbol (string_from_java l))
		      end
		    else
		      let jcls_ThisRef = find_class "soot/jimple/ThisRef" in
			if is_instance_of v jcls_ThisRef then
			  begin
			    Expression.Lval (Expression.This)
			  end
			else
			  let jcls_InstanceFieldRef = find_class "soot/jimple/InstanceFieldRef" in
			    if is_instance_of v jcls_InstanceFieldRef then
			      begin
				let base = call_object_method v (get_methodID jcls_InstanceFieldRef "getBase" "()Lsoot/Value;") [||] in
				let fld = call_object_method v (get_methodID jcls_InstanceFieldRef "getField" "()Lsoot/SootField;") [||] in
				let fldname = string_from_java (call_object_method fld (get_methodID (find_class "soot/SootField") "getName" "()Ljava/lang/String;") [||]) in
				  Expression.Lval (Expression.Access (Expression.Dot, convertToBlastExp base, fldname) )
			      end
			    else
			      let jcls_StaticFieldRef = find_class "soot/jimple/StaticFieldRef" in
				if is_instance_of v jcls_StaticFieldRef then
				  begin
				    (* what TO DO? - i know :D*)
				    let fld = (call_object_method v (get_methodID jcls_StaticFieldRef "getField" "()Lsoot/SootField;") [||]) in
				    let s = string_from_java (call_object_method fld (get_methodID (find_class "soot/SootField") "getName" "()Ljava/lang/String;") [||]) in
				    let staticClass = call_object_method fld (get_methodID (find_class "soot/SootField") "getDeclaringClass" "()Lsoot/SootClass;") [||] in
				    let staticClassName = string_from_java (call_object_method staticClass (get_methodID (find_class "soot/SootClass") "getName" "()Ljava/lang/String;") [||]) in
				      Expression.Lval (Expression.Access (Expression.Dot, Expression.Lval (Expression.Symbol staticClassName),  s))
				  end
				else
				  if (is_instance_of v ParameterRef.sclass) then
				    begin
				      Expression.Lval (Expression.Symbol
							 ("parameter"^ (string_of_int (ParameterRef.getIndex v))))
				    end
				  else
				    let jcls_ArrayRef = find_class "soot/jimple/ArrayRef" in
				      if is_instance_of v jcls_ArrayRef then
					begin
					  failwith "Array ref"
					end
				      else
					if (is_instance_of v NewExpr.sclass) then
					  begin
					    let tp = NewExpr.getType v in 
					    let t = SootToAstGlue.get_type tp in	
					      Expression.Constructor ("new", t)
					  end
					else
					  if (is_instance_of v CaughtExceptionRef.sclass) then
					    begin
					      let exc_type = getType v
					      in
						print_string "got type!";
						Expression.InstanceOf (Expression.Lval (Expression.Symbol "Exception"),
									SootToAstGlue.get_type exc_type)
					    end
					  else
					    if (is_instance_of v InstanceInvokeExpr.sclass) then
					      begin
						convertInvokeExpr v
					      end
					    else
					      if (is_instance_of v StaticInvokeExpr.sclass) then
						begin
						  convertStaticInvokeExpr v
						end
					      else
						begin
						  JniUtils.print_java_class v ; failwith "not handled"
						end
  and convertStaticInvokeExpr v =
    let ftype = StaticInvokeExpr.getType v
    in let ctype = SootToAstGlue.get_type ftype
    in let meth = InvokeExpr.getMethod v
    in let subsig = string_from_java (SootMethod.getSubSignature meth)
    in let fname = string_from_java (SootMethod.getName meth)
    in let defclassname = string_from_java (SootMethod.getDeclaringClassName meth)
    in let arglist = InvokeExpr.getArgs v
    in let exprarglist = List.map (fun x -> (convertToBlastExp x)) (JniUtils.javaList_to_ocaml_list arglist)
    in Expression.MethodCall (Expression.Lval (Expression.Symbol fname), Expression.Lval (Expression.Symbol "STATIC"),
			      Expression.Lval (Expression.Symbol defclassname), Expression.Lval (Expression.Symbol subsig)
				, exprarglist);
      
      
  and convertInvokeExpr v =
    let base = InstanceInvokeExpr.getBase v
    in let callclassname = string_from_java ( RefType.getClassName (Local.getType base))
    in let basename = string_from_java (Local.getName base)
    in let meth = InvokeExpr.getMethod v
    in let subsig = string_from_java (SootMethod.getSubSignature meth)
    in let fname = string_from_java (SootMethod.getName meth)
    in let defclassname = string_from_java (SootMethod.getDeclaringClassName meth)
    in let classt = Local.getType base
    in let arglist = InvokeExpr.getArgs v
    in let exprarglist = List.map (fun x -> (convertToBlastExp x)) (JniUtils.javaList_to_ocaml_list arglist)
    in
      Expression.MethodCall (Expression.Lval (Expression.Symbol fname),
			     Expression.InstanceOf (Expression.Lval (Expression.Symbol basename),
						    SootToAstGlue.get_type classt),
			     Expression.Lval (Expression.Symbol defclassname), Expression.Lval (Expression.Symbol subsig)
			       , exprarglist);
			 
  and convertBinopExpr v = 
    let u1 = convertToBlastExp (call_object_method v (get_methodID (find_class "soot/jimple/BinopExpr") "getOp1" "()Lsoot/Value;") [||]) 
    and u2 = convertToBlastExp (call_object_method v (get_methodID (find_class "soot/jimple/BinopExpr") "getOp2" "()Lsoot/Value;") [||]) 
    in
    let binop =
      let jcls_AddExpr = find_class "soot/jimple/AddExpr" in
	if is_instance_of v jcls_AddExpr then Expression.Plus
	else
	  let jcls_SubExpr = find_class "soot/jimple/SubExpr" in
	    if is_instance_of v jcls_SubExpr then Expression.Minus
	    else
	      let jcls_MulExpr = find_class "soot/jimple/MulExpr" in
		if is_instance_of v jcls_MulExpr then Expression.Mul
		else
		  let jcls_DivExpr = find_class "soot/jimple/DivExpr" in
		    if is_instance_of v jcls_DivExpr then Expression.Div
		    else
		      let jcls_RemExpr = find_class "soot/jimple/RemExpr" in
			if is_instance_of v jcls_RemExpr then Expression.Rem
			else
			  let jcls_ShlExpr = find_class "soot/jimple/ShlExpr" in
			    if is_instance_of v jcls_ShlExpr then Expression.LShift
			    else
			      let jcls_ShrExpr = find_class "soot/jimple/ShrExpr" in
				if is_instance_of v jcls_ShrExpr then Expression.RShift
				else
				  let jcls_XorExpr = find_class "soot/jimple/XorExpr" in
				    if is_instance_of v jcls_XorExpr then Expression.Xor
				    else
				      let jcls_AndExpr = find_class "soot/jimple/AndExpr" in
					if is_instance_of v jcls_AndExpr then Expression.And
					else
					  let jcls_OrExpr = find_class "soot/jimple/OrExpr" in
					    if is_instance_of v jcls_AddExpr then Expression.Or
					    else
					      let jcls_EqExpr = find_class "soot/jimple/EqExpr" in
						if is_instance_of v jcls_EqExpr then Expression.Eq
						else
						  let jcls_NeExpr = find_class "soot/jimple/NeExpr" in
						    if is_instance_of v jcls_NeExpr then Expression.Ne
						    else
						      let jcls_GtExpr = find_class "soot/jimple/GtExpr" in
							if is_instance_of v jcls_GtExpr then Expression.Gt
							else
							  let jcls_GeExpr = find_class "soot/jimple/GeExpr" in
							    if is_instance_of v jcls_GeExpr then Expression.Ge
							    else
							      let jcls_LtExpr = find_class "soot/jimple/LtExpr" in
								if is_instance_of v jcls_LtExpr then Expression.Lt
								else
								  let jcls_LeExpr = find_class "soot/jimple/LeExpr" in
								    if is_instance_of v jcls_LeExpr then Expression.Le
								    else
								      begin
									JniUtils.print_java_class v ;
									failwith "eh?"
								      end
    in
      Expression.Binary (binop, u1, u2)
  and convertToBlastConstant c =
    let jcls_NullConstant = find_class "soot/jimple/NullConstant" in
      if is_instance_of c jcls_NullConstant then
	Constant.Null
      else
	let jcls_NumericConstant = find_class "soot/jimple/NumericConstant" in
	  if is_instance_of c jcls_NumericConstant then
	    begin
	      let jcls_ArithmeticConstant = find_class "soot/jimple/ArithmeticConstant" in
		if is_instance_of c jcls_ArithmeticConstant then
		  begin
		    let jcls_IntConstant = find_class "soot/jimple/IntConstant" in
		      if is_instance_of c jcls_IntConstant then
			Constant.Int (get_camlint_field c (get_fieldID jcls_IntConstant "value" "I"))
		      else
			let jcls_LongConstant = find_class "soot/jimple/LongConstant" in
			  if is_instance_of c jcls_LongConstant then (* CHECK TYPE LONG *)
			    Constant.Long (get_long_field c (get_fieldID jcls_IntConstant "value" "J"))
			  else
			    begin
			      JniUtils.print_java_class c ;
			      failwith "Unexpected subclass of ArithmeticConstant"
			    end
		  end
	  else
	    let jcls_RealConstant = find_class "soot/jimple/RealConstant" in
	      if is_instance_of c jcls_RealConstant then
		begin
		  let jcls_FloatConstant = find_class "soot/jimple/FloatConstant" in
		    if is_instance_of c jcls_FloatConstant then
		      Constant.Float (get_float_field c (get_fieldID jcls_FloatConstant "value" "F"))
		    else
		      let jcls_DoubleConstant = find_class "soot/jimple/DoubleConstant" in
			if is_instance_of c jcls_DoubleConstant then
			  Constant.Float (get_double_field c (get_fieldID jcls_FloatConstant "value" "D"))
			else
			  begin
			    JniUtils.print_java_class c ;
			    failwith "Unexpected subclass of RealConstant"
			  end
		end
	      else
		begin
		  JniUtils.print_java_class c ;
		  failwith "Unexpected subclass of NumericConstant"
		end
		  
	    end
	  else
	    let jcls_StringConstant = find_class "soot/jimple/StringConstant" in
	      if is_instance_of c jcls_StringConstant then
		begin
		  let s = get_object_field c (get_fieldID jcls_StringConstant "value" "Ljava/lang/String;") in
		    Constant.String (string_from_java s)
		end
	      else
		begin
		  JniUtils.print_java_class c ;
		  failwith "Unexpected type in convertToBlastConstant"
		end
		  
  let convertToBlastLval ex = 
    let l = convertToBlastExp ex in
      match l with Expression.Lval lv -> lv | _ -> failwith ("convertToBlastLval: Not an lval "^(Expression.toString l)) 

end (* module Value *)
  
  
module Hierarchy =
struct
  let sclass = find_class "soot/FastHierarchy"

  let getSubclassesOf obj j_class=
    com obj (gmi sclass "getSubclassesOf" "(Lsoot/SootClass;)Ljava/util/Collection;") [|Obj j_class|]
      
  let rec print_hierarchy o_hierarchy ob jcls_soot_SootClass =
    let this_class_name = 
      call_object_method ob (get_methodID jcls_soot_SootClass "getName" "()Ljava/lang/String;") [| |]
    in if (call_boolean_method ob (get_methodID jcls_soot_SootClass "hasSuperclass" "()Z") [| |]) then
	begin
	  let parent = call_object_method ob (get_methodID jcls_soot_SootClass "getSuperclass" "()Lsoot/SootClass;") [| |]
	  in assert (call_boolean_method o_hierarchy 
		       (get_methodID (find_class "soot/FastHierarchy") "isSubclass" "(Lsoot/SootClass;Lsoot/SootClass;)Z") 
		       [| Obj ob; Obj parent; |] ) ;
	    print_hierarchy o_hierarchy parent jcls_soot_SootClass
	end
      else
	()
end (* Hierarchy *)
  
  
module IdentityStmt =
struct
  let sclass = find_class "soot/jimple/IdentityStmt"

  let getLeftOp obj = 
    com obj (gmi sclass "getLeftOp" "()Lsoot/Value;") [||]
  let getRightOp obj = 
    com obj (gmi sclass "getRightOp" "()Lsoot/Value;") [||]
end

module AssignStmt =
struct
  let sclass = find_class "soot/jimple/AssignStmt"

  let getLeftOp obj = 
    com obj (gmi sclass "getLeftOp" "()Lsoot/Value;") [||]
  let getRightOp obj = 
    com obj (gmi sclass "getRightOp" "()Lsoot/Value;") [||]
end

module ReturnVoidStmt=
struct
  let sclass = find_class "soot/jimple/ReturnVoidStmt"
end

module UnopExpr =
struct
  let sclass = find_class "soot/jimple/UnopExpr"

  let getOp obj =
    com obj (gmi sclass "getOp" "()Lsoot/Value;") [||]
end

  
module ReturnStmt =
struct
  let sclass = find_class "soot/jimple/ReturnStmt"

  let getOp obj = 
    com obj (gmi sclass "getOp" "()Lsoot/Value;") [||] 
end

module ThrowStmt =
struct
  let sclass = find_class "soot/jimple/ThrowStmt"

  let getOp obj = 
    com obj (gmi sclass "getOp" "()Lsoot/Value;") [||] 
end
  
module IfStmt =
struct
  let sclass = find_class "soot/jimple/IfStmt"
		 
  let getCondition obj = 
    com obj (gmi sclass "getCondition" "()Lsoot/Value;") [||]
  let getTarget obj =
    com obj (gmi sclass "getTarget" "()Lsoot/jimple/Stmt;") [||]
  let getTargetBox obj =
    com obj (gmi sclass "getTargetBox" "()Lsoot/UnitBox;") [||]
end

module UnitBox =
struct
  let sclass = find_class "soot/UnitBox"

  let getUnit obj =
    com obj (gmi sclass "getUnit" "()Lsoot/Unit;") [||]
end
  
module TrapManager =
struct
  let sclass = find_class "soot/TrapManager"
		 
  let getTrapsAt u activeBody =
    csom sclass (gsmi sclass "getTrapsAt" "(Lsoot/Unit;Lsoot/Body;)Ljava/util/List;") [| Obj u ; Obj activeBody|]
end
  
module Trap =
struct
  let sclass = find_class "soot/Trap"

  let getException obj =
    com obj (gmi sclass "getException" "()Lsoot/SootClass;") [||]
  let getHandlerUnit obj =
    com obj (gmi sclass "getHandlerUnit" "()Lsoot/Unit;") [||]
end
  

module LookupSwitchStmt =
struct
  let sclass = find_class "soot/jimple/LookupSwitchStmt"

  let getKey obj = 
    com obj (gmi sclass "getKey" "()Lsoot/Value;") [||]
  let getTargetCount obj = 
    cim obj (gmi sclass "getTargetCount" "()I") [||]
  let getLookupValue obj i =
    cim obj (gmi sclass "getLookupValue" "(I)I") [|Camlint i|]
  let getTarget obj i =
    com obj (gmi sclass "getTarget" "(I)Lsoot/Unit;") [| Camlint i |]
  let getDefaultTarget obj = 
    com obj (gmi sclass "getDefaultTarget" "()Lsoot/Unit;") [||]
end

module TableSwitchStmt =
struct
  let sclass = find_class "soot/jimple/TableSwitchStmt"

  let getLowIndex obj =
    cim obj (gmi sclass "getLowIndex" "()I") [||]
  let getHighIndex obj =
    cim obj (gmi sclass "getHighIndex" "()I") [||]
  let getKey obj =
    com obj (gmi sclass "getKey" "()Lsoot/Value;") [||]
  let getTarget obj i =
    com obj (gmi sclass "getTarget" "()Lsoot/Unit;") [| Camlint i|]
  let getDefaultTarget obj = 
    com obj (gmi sclass "getDefaultTarget" "()Lsoot/Unit;") [||]
end

module GotoStmt =
struct
  let sclass = find_class "soot/jimple/GotoStmt"

  let getTarget obj =
    com obj (gmi sclass "getTarget" "()Lsoot/Unit;") [||]
  let toString obj =
    com obj (gmi sclass "toString" "()Ljava/lang/String;") [||]
end

module ModifyGraph =
struct
  let sclass = find_class "ModifyGraph"

  let modifyGraph obj graph=
    cvm obj (gmi sclass "modifyGraph" "(Lsoot/toolkits/graph/BriefBlockGraph;)V") [|Obj graph|]
end

module EnterMonitorStmt =
struct
  let sclass = find_class "soot/jimple/EnterMonitorStmt"
end

module ExitMonitorStmt =
struct
  let sclass = find_class "soot/jimple/ExitMonitorStmt"
end

module BriefBlockGraph =
struct
  let sclass = find_class "soot/toolkits/graph/BriefBlockGraph"

  let getBlocks obj =
    com obj (gmi sclass "getBlocks" "()Ljava/util/List;") [||]
  let getPredsOf obj node =
    com obj (gmi sclass "getPredsOf" "(Ljava/lang/Object;)Ljava/util/List;") [|Obj node|]
  let mrm_getHeads obj =
    let allblocks = getBlocks obj
    in let heads = alloc_object JniUtils.JVector.sclass
    in
      JniUtils.JVector.init heads;
      let c_allblocks = JniUtils.javaList_to_ocaml_list allblocks
      in
	List.iter (fun x ->
		     let preds = getPredsOf obj x
		     in let length = JniUtils.JList.size preds (*(getPredsOf obj x)*)
		     in
		       begin
			 let tmp = if (length = 0) then JniUtils.JVector.add heads x else false
			 in ()
		       end
		  ) c_allblocks;
	heads

end
  
module Block =
struct
  let sclass = find_class "soot/toolkits/graph/Block"

  let toShortString obj = 
    com obj (gmi sclass "toShortString" "()Ljava/lang/String;") [||]
  let iterator obj =
    com obj (gmi sclass "iterator" "()Ljava/util/Iterator;") [||] 
  let getHead obj =
    com obj (gmi sclass "getHead" "()Lsoot/Unit;") [||]
  let getTail obj =
    com obj (gmi sclass "getTail" "()Lsoot/Unit;") [||]
end		 

module Iterator =
struct
  let sclass = find_class "java/util/Iterator"

  let hasNext obj =
    cbm obj (gmi sclass "hasNext" "()Z") [||]
  let next obj =
    com obj (gmi sclass "next" "()Ljava/lang/Object;") [||]
end

module PatchingChain =
struct
  let sclass = find_class "soot/PatchingChain"

  let getFirst obj =
    com obj (gmi sclass "getFirst" "()Ljava/lang/Object;") [||]
  let getSuccOf obj point =
    com obj (gmi sclass "getSuccOf" "(Ljava/lang/Object;)Ljava/lang/Object;") [| Obj point |]
  let getPredOf obj point =
    com obj (gmi sclass "getPredOf" "(Ljava/lang/Object;)Ljava/lang/Object;") [| Obj point |]
  let insertAfter obj o_list point =
    cvm obj (gmi sclass "insertAfter" "(Ljava/util/list;Ljava/lang/Object;)V") [| Obj o_list ; Obj point |]
  let insertBefore obj o_list point =
    cvm obj (gmi sclass "insertBefore" "(Ljava/util/list;Ljava/lang/Object;)V") [| Obj o_list ; Obj point |]
  let getFirst obj =
    com obj (gmi sclass "getFirst" "()Ljava/lang/Object;") [||]
  let iterator obj =
    com obj (gmi sclass "iterator" "()Ljava/util/Iterator;") [||]
  let size obj =
    cim obj (gmi sclass "size" "()I") [||]
  let contains obj point =
    cbm obj (gmi sclass "contains" "(Ljava/lang/Object;)Z") [|Obj point|]
end
  
  
	    
let rec unitToBlast_aux u activeBody =
  begin
    print_string "in process unit\n";
    let (thisCommand, new_u) = 
      if is_instance_of u IdentityStmt.sclass then
	begin
	  print_string "IdentityStmt!\n";
	  let leftop  =  IdentityStmt.getLeftOp u
	  in let rightop = IdentityStmt.getRightOp u
	  in 
	    JniUtils.print_java_class leftop ;
	    JniUtils.print_java_class rightop ;
	    let lop = Value.convertToBlastLval leftop
	    in let rop = (
		if (is_instance_of rightop CaughtExceptionRef.sclass)
		then Expression.InstanceOf (Expression.Lval (Expression.Symbol "Exception"),
					    SootToAstGlue.get_type (Value.getType leftop))
		else Value.convertToBlastExp rightop
	      )
	    in
	      print_string (Expression.lvalToString lop ) ;
	      print_string " = " ;
	      print_string (Expression.toString rop ) ;
	      print_newline () ;
	      (Some (Command.Block [(Command.Identity (lop, rop) )]), u)
	end
      else
	if is_instance_of u AssignStmt.sclass then
	  begin
	    print_string "AssignStmt!\n";
	    let leftop  = AssignStmt.getLeftOp u
	    in let rightop = AssignStmt.getRightOp u
	    in 
	      JniUtils.print_java_class leftop ;
	      JniUtils.print_java_class rightop ;
	      let lop = Value.convertToBlastLval leftop
	      in let rop = Value.convertToBlastExp rightop
	      in
		print_string (Expression.lvalToString lop ) ;
		print_string " = " ;
		print_string (Expression.toString rop ) ;
		print_newline () ;
		if (is_instance_of rightop InvokeExpr.sclass)
		then
		  (Some (Command.MethodCall (Expression.Assignment
				(Expression.Assign, Expression.Symbol (string_from_java (Local.getName leftop)), rop))),u)
		else
		  (Some (Command.Block [Command.Assignment (lop, rop) ]), u)
	  end
	else
	  if (is_instance_of u ReturnVoidStmt.sclass) then
	    begin
	      print_string "return void\n";
	      (Some (Command.Block [Command.ReturnVoid]), u)
	    end
	  else
	    if (is_instance_of u ReturnStmt.sclass) then
	      begin
		print_string "return statement\n";
		let op = ReturnStmt.getOp u
		in let retval = Value.convertToBlastExp op 
		in JniUtils.print_java_class op ;
		  (Some (Command.Block [Command.Return retval]), u)
		    
	      end
	    else
	      if (is_instance_of u InvokeStmt.sclass) then
		begin
		  print_string "invoke statement\n";
		  let invokeexpr = InvokeStmt.getInvokeExpr u
		  in let base = InstanceInvokeExpr.getBase invokeexpr
		  in let callclassname = string_from_java (RefType.getClassName (Local.getType base))
		  in let basename = string_from_java (Local.getName base)
		  in let classt = Local.getType base
		  in let invmethod = InvokeExpr.getMethod invokeexpr
		  in let subsig = string_from_java (SootMethod.getSubSignature invmethod)
		  in let arglist = JniUtils.javaList_to_ocaml_list (InvokeExpr.getArgs invokeexpr)
		  in let funname = (string_from_java (SootMethod.getName invmethod))
		  in let symbol_arglist = List.map (fun x -> Value.convertToBlastExp x) arglist
		  in let defclassname = string_from_java (SootMethod.getDeclaringClassName invmethod)
		  in let funcall = Expression.MethodCall
						   (Expression.Lval (Expression.Symbol funname),
						    Expression.InstanceOf (Expression.Lval (Expression.Symbol basename),
									   SootToAstGlue.get_type classt),
						    Expression.Lval (Expression.Symbol defclassname), 
						    Expression.Lval (Expression.Symbol subsig), symbol_arglist)
		  in
		    print_string ("Invoked:"^(Expression.toString funcall)^":\n");
		    (Some (Command.MethodCall (funcall)), u)
		end
	      else
		if is_instance_of u (find_class "soot/jimple/NopStmt") then
		  begin
		    print_string "noop statement\n";
		    (Some (Command.Skip), u)
		  end
		else
		  if (is_instance_of u EnterMonitorStmt.sclass) then
		    begin
		      print_string "enter monitor\n" ;
		      (Some Command.MonitorEnter, u)
		    end
		  else
		    if (is_instance_of u ExitMonitorStmt.sclass) then
		      begin
			print_string "exit monitor\n" ;
			(Some Command.MonitorExit, u)
		      end
		    else
		      if (is_instance_of u ThrowStmt.sclass) then
			begin
			  print_string "throw\n";
			  let op = ThrowStmt.getOp u
			  in let b_op = Value.convertToBlastExp op
			  in
			    JniUtils.print_java_class op ;
			    print_string (Expression.toString b_op) ;
			    print_newline () ;
			    (Some (Command.Throw b_op), u)
			end
		      else
			if (is_instance_of u IfStmt.sclass) then
			  begin
			      print_string "if\n";
			    let cond = IfStmt.getCondition u
			    in let p = Predicate.Atom (Value.convertToBlastExp cond)
			    in
			      print_string "Constructed if predicate ";
			      print_string (Predicate.toString p) ;
			      print_newline ();
			      (None, u)
			  end
			else
			  if (is_instance_of u LookupSwitchStmt.sclass) then
			    begin
			      print_string "if\n";
			      (None, u)
			    end
			  else
			    if (is_instance_of u TableSwitchStmt.sclass) then
			      begin
				print_string "if\n";
				(None, u)
			      end
			    else
			      if (is_instance_of u GotoStmt.sclass) then
				begin
				  print_string "goto is it?\n";
				  let target  = GotoStmt.getTarget u
				  in let strgoto = string_from_java (GotoStmt.toString u)
				  in print_string ("Goto::"^strgoto^":\n");
				    (None, u) (*unitToBlast_aux target activeBody*)
				end
			      else
				begin
				  print_string "\nControl flow statement.\n" ; 
				  JniUtils.print_java_class u ;
				  print_newline () ;
				  (None, u) 
				end
    in (thisCommand,u)
  end
let unitToBlast u activeBody =
  begin
    let (thisCommand, new_u) = unitToBlast_aux u activeBody
    in let trapsHere = JniUtils.javaList_to_ocaml_list (TrapManager .getTrapsAt new_u activeBody) 
    in
      (thisCommand, trapsHere)  
  end


