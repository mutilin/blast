(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

type ('a, 'b, 'c) automaton = {
  (* automaton identifier *)
  aut_id : int ;
  (* the start location *)
  mutable start_location : (('a, 'b, 'c) location) option ;
  (* the end location *)
  mutable end_location   : (('a, 'b, 'c) location) option ;
  (* the extra attributes of this automaton *)
  mutable aut_attributes : 'a ;
}
and ('a, 'b, 'c) location = {
  (* location identifier: two distinct locations within a single automaton
     have distinct ids *)
  loc_id : int ;
  (* successor locations *)
  mutable succs : ('c * ('a, 'b, 'c) location) list ;
  (* predecessor locations *)
  mutable preds : ('c * ('a, 'b, 'c) location) list ;
  (* the automaton which this location belongs to *)
  automaton : ('a, 'b, 'c) automaton ;
  (* the extra attributes of this location *)
  mutable loc_attributes : 'b ;
}

let next_aut_id = ref 0
let next_loc_ids = ref (Array.create 32 (-1))
let global_location_function_table = Hashtbl.create 1009


let expand_next_loc_ids_array_as_needed new_index =
  let len = Array.length !next_loc_ids
  in
    if (new_index >= len)
    then
      next_loc_ids := Array.append !next_loc_ids
        (Array.create (len / 2) (-1))

let location_function_name loc = 
  Hashtbl.find global_location_function_table (loc.automaton.aut_id, loc.loc_id)



let create_location (fname : string)  a attrs =
  let new_id = Array.get !next_loc_ids a.aut_id in
  let _ = Hashtbl.add global_location_function_table (a.aut_id,new_id) fname in 
  let _ = Array.set !next_loc_ids a.aut_id (new_id + 1) in
    { loc_id         = new_id ;
      succs          = [] ;
      preds          = [] ;
      automaton      = a ;
      loc_attributes = attrs ; }



let create_automaton attrs =
  let new_id = !next_aut_id
  in
    next_aut_id := new_id + 1 ;
    expand_next_loc_ids_array_as_needed new_id ;
    Array.set !next_loc_ids new_id 0 ;
    { aut_id         = new_id ;
      start_location = None ;
      end_location   = None ;
      aut_attributes = attrs ; }

let get_start_location a =
  match a.start_location with
      Some loc -> loc
  | _ -> failwith "BlastControlFlowAutomaton.get_start_location"

let get_end_location a =
  match a.end_location with
      Some loc -> loc
  | _ -> failwith "BlastControlFlowAutomaton.get_end_location"

let get_automaton_attributes a = a.aut_attributes

let set_start_location a l = a.start_location <- Some l
let set_end_location a l   = a.end_location   <- Some l

let set_automaton_attributes a attrs = a.aut_attributes <- attrs

let get_succs l = l.succs
let get_preds l = l.preds

let get_location_attributes l = l.loc_attributes

let set_succs l sl = l.succs <- sl
let set_preds l pl = l.preds <- pl

(* Warning: this does not check wether the edge is already there *)
(* Warning: use add_edge instead to ensure consistency between predecessors
   and successors *)
let add_succ l s = l.succs <- s::l.succs

(* Warning: this does not check wether the edge is already there *)
(* Warning: use add_edge instead to ensure consistency between predecessors
   and successors *)
let add_pred l p = l.preds <- p::l.preds

(* Warning: this does not check wether the edge is already there *)
let add_edge src target label =
  add_succ src (label, target) ;
  add_pred target (label, src)

(* Warning: this does not check wether the edge is already there *)
let remove_edge src target label =
  set_succs src (List.filter (fun (l, t) ->
                                not ((compare t target) = 0 && (compare l label) = 0 ))
                             (get_succs src)) ;
  set_preds target (List.filter (fun (l, s) ->
                                   not ((compare s src = 0) && (compare l label = 0)))
                                (get_preds target))

let set_location_attributes l attrs = l.loc_attributes <- attrs
