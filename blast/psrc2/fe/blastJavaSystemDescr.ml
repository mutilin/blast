(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

(**
   Depends on:
     Ast

**)

open Jni

open Ast

module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate

module SootToAstGlue = CamljavaUtils.SootToAstGlue
module SootClass = CamljavaUtils.SootClass
module Scene = CamljavaUtils.Scene
module SootMethod = CamljavaUtils.SootMethod
module Hierarchy = CamljavaUtils.Hierarchy
module Value = CamljavaUtils.Value
module Local = CamljavaUtils.Local
module Unit  = CamljavaUtils.Unit
module ValueBox = CamljavaUtils.ValueBox
module AssignStmt = CamljavaUtils.AssignStmt
module IdentityStmt = CamljavaUtils.IdentityStmt


module Java_System_Descr =
struct

  module Command = BlastSystemDescr.Command
  
  (* stuff for the Control Flow Automaton *)
  module CFA = BlastControlFlowAutomaton
    
  type source_position = {
    file_name : string ;
    startline : int ;
    endline   : int ;
    startpos  : int ;
    endpos    : int ;
  }
      
  let print_source_position fmt sp =
    Format.fprintf fmt "@[src=\"%s\";@ line=%d-%d@]" sp.file_name sp.startline sp.endline
      
  let location_tag_to_source_position thisClassName loctag =
    let jcls_SourceLnPosTag = find_class "soot/tagkit/SourceLnPos" in
      if (is_instance_of loctag jcls_SourceLnPosTag) then
	let startline = call_camlint_method loctag (get_methodID jcls_SourceLnPosTag "startLn" "()I") [||] in
	let endline = call_camlint_method loctag (get_methodID jcls_SourceLnPosTag "endLn" "()I") [||] in
	let startpos = call_camlint_method loctag (get_methodID jcls_SourceLnPosTag "startPos" "()I") [||] in
	let endpos = call_camlint_method loctag (get_methodID jcls_SourceLnPosTag "endPos" "()I") [||] in
	  { startline      = startline ;
	    endline        = endline ;
	    startpos       = startpos ;
	    endpos       = endpos ;
	    file_name = thisClassName ;
	  }
      else 
	begin
	  Message.msg_string Message.Error "Warning: Strange tag with unit!" ;
	  {startline = -1 ; endline = -1 ; startpos = -1 ; endpos = -1 ; file_name = thisClassName ; }
	end
	  
  let source_position_to_string = Misc.to_string_from_printer print_source_position

  (** with each location, we keep track of the source position,
    as well as the trap list, that is, where execution should
    proceed if an execution of a certain type has been raised. 
  *)
  type source_tag = 
    | Original of source_position
    | Artificial
  and trap_table = (JavaType.java_type, 
		    (automaton_attributes, location_attributes, Command.t) CFA.location) Hashtbl.t     
  and location_attributes =
      { 
	source_position : source_tag ;
	mutable traps   : trap_table ;
      }	 
  and automaton_attributes = {
    name : string ;
  }
      
  type location = (automaton_attributes,
                   location_attributes,
                   Command.t) CFA.location
      
  let print_location_attributes fmt st =
    begin
      match st.source_position with
	| Original (sp) ->
            Format.fprintf fmt "@[Original(" ;
            print_source_position fmt sp ;
            Format.fprintf fmt ")@]"
	| Artificial ->
            Format.fprintf fmt "@[Artificial@]" 
    end ;
    Hashtbl.iter (fun k l -> Format.fprintf fmt "@[" ; JavaType.print fmt k ; 
                    Format.fprintf fmt "(%d, %d)" l.CFA.automaton.CFA.aut_id l.CFA.loc_id ;
		    Format.fprintf fmt "@]" ) st.traps 
      
      
      
  type method_definition = {
    function_name    : string ;
    formals          : string list ;
    locals           : string list ;
    cfa              : (automaton_attributes,
                        location_attributes,
                        Command.t) CFA.automaton ;
  }
      
  (*********************end of the automata part**)

  (*****variable management part*******)


  (* Hashtable for all the lvals in the program. *)
  let all_lvals_hashtbl = Hashtbl.create 101

  let all_classes_list = ref []
	
  (*TAC: Changed this table *)
  (* Hashtable for class fields (maps each class name to a hashtable
     that maps field names to SootFields) *)
  let class_field_table = Hashtbl.create 50
			    
  (* Create a hashtable for each class that stores that class's
     fields. Gets the classes from all_classes_list *)
  let initialize_class_field_table () =
    List.iter (fun c ->
		 let new_field_table = Hashtbl.create 19
		 in Hashtbl.add class_field_table c new_field_table
	      ) !all_classes_list
      
  let remove_lval (lv:Expression.lval) = Hashtbl.remove all_lvals_hashtbl lv
					   
  let exists_lval (lv:Expression.lval) = Hashtbl.mem all_lvals_hashtbl lv 
					   
  (* Add the given lval to the all_lvals_hashtbl *)
  let add_lval (lv:Expression.lval) (java_type:JavaType.type_or_void) =
    Hashtbl.add all_lvals_hashtbl lv java_type	

  (* Convert the given soot value to an lval and add it to the
     all_lvals_hashtbl *)
  let convert_and_add_lval value =
    let lv = Value.convertToBlastLval value
    in let vtype = Value.getType value
    in let jtype = SootToAstGlue.get_type_or_void vtype
    in if not (exists_lval lv) then
	add_lval lv jtype
      else
	()

  (* Do a given function to all lvals in all_lvals_hashtbl *)
  let iterate_all_lvals (a:Expression.lval -> unit) =
    Hashtbl.iter (fun lv _ -> a lv) all_lvals_hashtbl

      
  (* TAC: Changed the name and functionality of this method *)
  let add_class_field classname fieldname field =
    let this_class_field_table =
      try Hashtbl.find class_field_table classname
      with Not_found ->
	failwith ("Missed class " ^ classname ^ " in initialization!") 
    in
      if (Hashtbl.mem this_class_field_table classname) then 
	(* this has already been seen *)
	failwith "Same variable declared twice!" 
      else
	begin
	  Hashtbl.add this_class_field_table fieldname field;
	end

     
  (* For testing *)
  let print_lval_info lv =
    Message.msg_string Message.Normal ("Lval: " ^ Expression.lvalToString lv);
    let type_string =
      try
	let java_type = Hashtbl.find all_lvals_hashtbl lv
	in match java_type with
	    JavaType.Void -> "Void type"
	  | JavaType.Type(jt) -> JavaType.toString jt
		
      with _ -> "Unavailable"
    in
      Message.msg_string Message.Normal ("Type: " ^ type_string)
  
  let cur_scope = ref ""
		    
  let set_scope scope =
    cur_scope := scope
    
  (* Rename the local values in the given unit using the current scope *)
  let process_unit_symbols u =
    
    (* Rename the given value by appending its scope to its name if
       it's a local *)
    let rename_value v =
      if is_instance_of v Local.sclass then
	let name = (string_from_java (Local.getName v))
	in
	  (*TAC: Hack to keep the scope from being appended multiple times *)
	  if String.contains name '@' then () else
	    (* TAC: End Hack *) 
	    Local.setName v (string_to_java (name ^ "@" ^ !cur_scope))
      else ()
    in 
      
    (* Rename each value used and defined by this unit *)
    let valueboxes = Unit.getUseAndDefBoxes u
    in let vb_iterator = JniUtils.JList.iterator valueboxes
    in let _ = 
	while JniUtils.JIterator.hasNext vb_iterator
	do
	  let vb = JniUtils.JIterator.next vb_iterator
	  in let value = ValueBox.getValue vb
	  in rename_value value
	done 
    in 

      (* Add the unit's lvals to the all_lvals_hashtbl *)
      
      if (is_instance_of u AssignStmt.sclass) then
	begin
	  let leftop = (AssignStmt.getLeftOp u)
	  in convert_and_add_lval leftop
	end
      else if (is_instance_of u IdentityStmt.sclass) then
	begin
	  let leftop = (IdentityStmt.getLeftOp u)
	  in convert_and_add_lval leftop
	end
      else ()

	
  let function_cfa_table = Hashtbl.create 50
			     
  let add_to_function_cfa_table key cfa =
    if (Hashtbl.mem function_cfa_table key) then (*this has already been seen *)
      ()
    else
      Hashtbl.add function_cfa_table key cfa

  let get_from_function_cfa_table key =
    try
      Hashtbl.find function_cfa_table key
    with
	Not_found -> failwith "Function not found in function_cfa_table!"
	
  (*****/variables*****)
      
  exception DuplicateFunctionName of string

  let fake_location : location = (* generic location that matches all locations *)
    { CFA.loc_id = -1;
      CFA.succs = [];
      CFA.preds = [];
      CFA.automaton = { CFA.aut_id = -1;
                        CFA.start_location = None;
			CFA.start_locations = [None];
                        CFA.end_location = None;
                        CFA.aut_attributes = { name = "fake" }; } ;
      CFA.loc_attributes = { source_position = Artificial ; traps = Hashtbl.create 1 ; }
    } 
      
  let get_source_position loc (*:location automaton*) =
    match ((CFA.get_location_attributes loc).source_position) with
      | Original sp ->  Some (sp.file_name, sp.startline, sp.endline)
      | Artificial -> None

  let print_location fmt (loc : location)  =
    Format.fprintf fmt "@[Location(@[id=%d#%d;@ attrs="
      loc.CFA.automaton.CFA.aut_id
      loc.CFA.loc_id ;
    print_location_attributes fmt (CFA.get_location_attributes loc) ;
    Format.fprintf fmt "@])@]"
      
  type edge = location * Command.t * location

  let fake_edge : edge =
    (fake_location, { Command.code = Command.Skip ; Command.read = [] ; Command.written = [] } , fake_location)

  let print_edge fmt (s, c, t) =
    Format.fprintf fmt "@[" ;
    print_location fmt s ;
    Format.fprintf fmt "---" ;
    Command.print fmt c ;
    Format.fprintf fmt "--->" ;
    print_location fmt t ;
    Format.fprintf fmt "@]"

  let print_code fmt c = Command.print fmt c

  let location_to_string = Misc.to_string_from_printer print_location

  let location_coords loc = (loc.CFA.automaton.CFA.aut_id , loc.CFA.loc_id)

  let edge_to_string = Misc.to_string_from_printer print_edge

  let get_source (s, c, t) = s
  let get_target (s, c, t) = t
  let get_command (s, c, t) = c

  let edge_to_command = get_command

  let make_edge (src : location) (target : location) newcode =
    ((src, newcode, target) : edge ) 

  let get_outgoing_edges loc =
    let succs = CFA.get_succs loc in
      List.map (fun (cmd, succ) -> (loc, cmd, succ)) succs

  let get_ingoing_edges loc = 
    let preds = CFA.get_preds loc in
      List.map (fun (cmd, pred) -> (pred, cmd, loc)) preds

  let reads_and_writes_at_edge e =
    let cmd = get_command e in
    cmd.Command.read, cmd.Command.written

  let get_edge_name edge =
    let (start_fn, start_node) = location_coords (get_source edge)
    and (targ_fn, targ_node) = location_coords (get_target edge)
    in
      (string_of_int start_fn) ^ "," ^ (string_of_int start_node) ^ "->" ^
      (string_of_int targ_fn) ^ "," ^ (string_of_int targ_node)

  let global_loc_table: (string * int , location list)Hashtbl.t = Hashtbl.create 2000

  let get_position_sources pos =
    (try
       Hashtbl.find global_loc_table pos
     with Not_found -> [])

  let set_location_attributes (loc:location) loc_attr =
    CFA.set_location_attributes loc loc_attr;
    let attr = loc_attr.source_position
    in 
      (match attr with
	 | Artificial -> ()
	 | Original pos ->
	     let pos = (pos.file_name, pos.startline) in
	       Hashtbl.replace global_loc_table pos (loc :: get_position_sources pos))

  let skip = Command.skip

	
  (*********to be provided***********)

  let stats_nb_cached_exp_closure = ref 0
	
  let stats_nb_recomp_exp_closure = ref 0

  let add_field_to_unroll (a:bool) (b:string) = false (* TC *)

  let expression_closure (a:Expression.expression) = [a] (* TC *)

  let expression_closure_stamp (b:bool) (a:Expression.expression) = [a] (* TC *)

  let lvalue_closure (a:Expression.lval) = [a] (* TC *)

  let lvalue_closure_stamp (b:bool) (a:Expression.lval) = [a] (* TC *)

  let iter_global_variables (a:Expression.lval -> unit)  = () (* TC *)

  let iterate_all_scope_lvals (b:string) (a:Expression.lval -> unit) = () (* TC *)

  let get_type_of_lval (a:Expression.lval) = Cil.intType (* TC *)

  let get_type_of_exp (a:Expression.expression) = Cil.intType (* TC *)

  let is_array (a:Expression.lval) = false (* TC *)

  let is_ptr (a:Expression.lval) = false (* TC *)

  let is_alloc (a:string) = false (* TC *)

  let is_dealloc (a:string) = false (* TC *)

  let is_global (a:Expression.lval) = false (* TC *)

  let can_escape (a:Expression.lval) = false (* TC? *)

  let is_a_lock (a:Expression.lval) = false (* TC *)

  let is_in_scope (b:string) (a:Expression.lval) = false (* TC *)

  let scope_of_lval (a:Expression.lval) = ["taylor"] (* TC *)

  let tid = "taylor"
	      
  let notTid = "taylor"

  let self = "taylor"

  exception NoSuchFunctionException of string

  let is_defined (a:string) = false

  let always_reach (a:location) (b:location) = false

  let mods_on_path (a:location) (b:location) = [ (Expression.Symbol "junk") ] (* TC *)

  let may_be_modified (a:Expression.lval list) (b:location) (c:location) = false (* TC *)

  let get_fname (a:Expression.expression) = "junk"

  let local_mod_on_edge (a:edge) (b:Expression.lval) = false (* TC *)

  let global_mod_on_call (a:string) (b:Expression.lval) = false (* TC *)

  let global_lvals_modified (a:string) = [(Expression.Symbol "junk")] (* TC *)

  let formal_lvals_modified (a:string) = [(Expression.Symbol "junk")] (* TC *)

  let global_lvals_mod_by  (a:Expression.lval) = ["taylor"] (* TC *)

  let may_mod_by  (a:Expression.lval) = ["taylor"] (* TC *)

  let lookup_entry_location (a:string) = fake_location

  let get_location_fname (a:location) = "taylor"

  let get_fname_id (a:string) = 5

  let list_of_functions () = ["taylor"]

  let lookup_location_from_loc_coords (co: int*int) = fake_location

  let is_loopback (a:location) = false

  let isReturn (a:edge) = false

  let returnExp (a:edge) = (Expression.Sizeof 5)

  let isBlock (a:edge) = false

  let isPredicate (a:edge) = false

  let isFunCall (a:edge) = false

  let replace_code_in_cfa (a:edge) (b:Expression.t) = ()

  type formals_kind =
    | Fixed of string list
    | Variable of string list

  let lookup_locals (a:string) = ["taylor"]

  let lookup_local_lvals (a:string) = [ (Expression.Symbol "taylor") ] (* TC *)

  let lookup_formals (a:string) = Fixed ["taylor"]

  let globals () = ["taylor"] (* TC *)

  let is_atomic (a:location) = false

  let is_event (a:location) = false

  let is_task (a:location) = false

  let is_spec (a:location) = false

  let get_locations_at_label (a:string) = [ fake_location ]

  let map_edges_fname f (fname:string) = 
    [ f fake_edge ]

  let map_edges f =
    List.flatten (List.map (map_edges_fname f) (list_of_functions ()))

  let compute_callgraph () = ()

  type edge_id_t =  (int * int) * (int * int)

  let calls_made_by (a:string) = [(Expression.Sizeof 5, "taylor")]

  let calls_made_into (a:string) =
    [ ( ( ((1,2),(3,4)),Expression.Sizeof 5 ), "taylor" ) ]

  let output_callgraph (s:string) = ()

  let get_reachable_functions () = ["taylor"]

  let caller_closure (a:string list) = ["taylor"]

  let backwards_caller_closure (a:string list) = ["taylor"]

  let bounded_backwards_caller_closure (a:int) (b:string list) = ["taylor"]

  let shortest_path (a:string) (b:string) = [ "taylor" ]

  let output_call_paths () = ()

  let scc_caller_closure (a:string) = [ "taylor" ]

  let get_all_multiply_defined_lvals (a:location) = [ Expression.Symbol "taylor" ]

  let compute_reaching_definitions () = ()

				          

      
  (********/to be provided***********)

  open CamljavaUtils


  (* Hash table mapping location id's to locations and its operations*)
  let location_id_to_location_ht = Hashtbl.create 200
  let add_to_loc_ht (l: location) = Hashtbl.add location_id_to_location_ht l.CFA.loc_id l
  let get_from_loc_ht i =
    try
      Hashtbl.find location_id_to_location_ht i 
    with
	Not_found -> failwith "Location not found in location_id_to_location_ht"
 (**/**)

  let find_this_target target t_succs : obj = 
    try
      JniUtils.javaList_find (fun succ ->
				(is_same_object target (Block.getHead succ))
			     ) t_succs
    with Not_found -> failwith "Target not found in switch statement!"


  let mrm_output_cfa_dot ch cfas =
    let module OrderedInt = struct type t = int let compare a b = b - a end
    in let module IntSet = Set.Make(OrderedInt)
    in let known_locs  = ref IntSet.empty in
    let rec output_loc loc =
      Printf.fprintf ch "  %d;\n" loc.CFA.loc_id;
      known_locs := IntSet.add loc.CFA.loc_id !known_locs;
      List.iter
	(fun (cmd, l) ->
           Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"
           loc.CFA.loc_id l.CFA.loc_id
           (String.escaped (Command.to_string cmd));
           if not (IntSet.mem l.CFA.loc_id !known_locs) then output_loc l)
	loc.CFA.succs
    in
      Printf.fprintf ch "digraph %s {\n" "m";
      List.iter (fun cfa ->
		   match cfa with
		     | None ->()
		     | Some loc ->
			 output_loc loc;
			 ()
		) cfas.CFA.start_locations;
      Printf.fprintf ch "}\n\n";
      ()
    
  let output_cfa_dot ch cfa =
    let module OrderedInt = struct type t = int let compare a b = b - a end in
    let module IntSet = Set.Make(OrderedInt) in
    let known_locs = ref IntSet.empty in
    let rec output_loc loc =
      Printf.fprintf ch "  %d;\n" loc.CFA.loc_id;
      known_locs := IntSet.add loc.CFA.loc_id !known_locs;
      List.iter
	(fun (cmd, l) ->
           Printf.fprintf ch "  %d -> %d [label=\"%s\"]\n"
           loc.CFA.loc_id l.CFA.loc_id
           (String.escaped (Command.to_string cmd));
           if not (IntSet.mem l.CFA.loc_id !known_locs) then output_loc l)
	loc.CFA.succs;
      List.iter (fun (cmd, l) -> if not (IntSet.mem l.CFA.loc_id !known_locs) then output_loc l) loc.CFA.preds
    in
      match cfa.CFA.start_location with
	| None -> ()
	| Some loc ->
	    Printf.fprintf ch "digraph %s {\n" "m";
	    output_loc loc;
            Printf.fprintf ch "}\n\n";
            ()

				       
  let initialize_scene (classname : string) =
    begin
      all_classes_list := [ classname ] @ !all_classes_list;
      let jcls_soot_options_Options = (find_class "soot/options/Options")  (*Command line options parser*)
      in let options = call_static_object_method jcls_soot_options_Options
		         (get_static_methodID jcls_soot_options_Options "v" "()Lsoot/options/Options;") [||] 
	(*In above, v calls an options method of the class Singletons kept in the class G - returns an Object method*)
      in 
	call_void_method options (get_methodID jcls_soot_options_Options "set_keep_line_number" "(Z)V")
	  [| Boolean true |] ;
	call_void_method options (get_methodID jcls_soot_options_Options "set_whole_program" "(Z)V") [| Boolean true |];
	let java_s : Jni.obj = string_to_java classname
				 (*so this ocaml string has been converted to a java String object*)
	in let loaded_classname : Jni.obj  = Scene.loadClassAndSupport java_s
		(* set this class to be an application class -- so that it can be analysed and output *)
	in
	  SootClass.setApplicationClass loaded_classname ;
    end

  let initialize_fields (classname : string) =
    begin
      Message.msg_string Message.Normal ("Initialising fields of the class:"^classname);
      let j_classname = string_to_java classname
      in let loaded_classname = Scene.getSootClass j_classname
      in let fields = SootClass.getFields loaded_classname
      in JniUtils.chain_iter (
	  fun t ->
	    let fieldname = string_from_java (SootField.getName t)
	    in
	      begin
		add_class_field classname fieldname t;
	      end
	) fields;
    end

  let get_arg_types args =
    let camllist = List.map (fun x -> Value.getType x) args
    in
      JniUtils.ocaml_list_to_javaList camllist

  let get_pred_1 funit =
    match funit with
      | Command.MethodCall (Expression.MethodCall (_, Expression.InstanceOf (obj, typ), _, _, _)) ->
	  Command.Pred (Predicate.Atom (Expression.Binary (Expression.Eq, obj, Expression.Constructor("new",typ))))
      | Command.MethodCall (Expression.Assignment (_, _, Expression.MethodCall(_,Expression.InstanceOf(obj,typ),_,_,_)))->
	  Command.Pred (Predicate.Atom (Expression.Binary (Expression.Eq, obj, Expression.Constructor("new",typ))))
      | _ -> Command.Skip

  let get_pred_2 funit =
    match funit with
      | Command.MethodCall (Expression.MethodCall (_, Expression.InstanceOf (obj, typ),
						   Expression.Lval (Expression.Symbol classname), _, _)) ->
	  Command.Pred (Predicate.Atom (Expression.Binary (Expression.Eq, obj,
			Expression.Constructor ("new",JavaType.Reference (JavaType.ClassOrInterfaceType (classname))))))
      | Command.MethodCall (Expression.Assignment (_, _, Expression.MethodCall (_, Expression.InstanceOf (obj, typ),
						   Expression.Lval (Expression.Symbol classname), _, _))) ->
	  Command.Pred (Predicate.Atom (Expression.Binary (Expression.Eq, obj,
			Expression.Constructor ("new",JavaType.Reference (JavaType.ClassOrInterfaceType (classname))))))
      | _ -> Command.Skip
    
  (* method that returns a list of functions that may be dispatched *)
  let get_function_list funcall =
    match funcall with
      | Expression.MethodCall (Expression.Lval (Expression.Symbol fname),
			       Expression.InstanceOf (v, JavaType.Reference (JavaType.ClassOrInterfaceType classname)),
			       Expression.Lval (Expression.Symbol sclsname)
				, Expression.Lval (Expression.Symbol subsig), z) ->
	  begin
	    match (Symbol.toString fname) with
	      | "<init>" -> []
	      | _ ->
		  begin
		    let x = Expression.Lval (Expression.Symbol fname)
		    in let y = Expression.InstanceOf (v, JavaType.Reference (JavaType.ClassOrInterfaceType classname))
(*		    in let classname = Symbol.toString sclsname*)
		    in let loaded_classname = Scene.getSootClass (string_to_java classname)
		    in let hierarchy = Scene.getOrMakeFastHierarchy ()
		    in let collection = Hierarchy.getSubclassesOf hierarchy loaded_classname
		    in let iterator = JniUtils.Collection.iterator collection
		    in let retlist = ref []
		    in
		      while (JniUtils.JIterator.hasNext iterator)
		      do
			let subclass = JniUtils.JIterator.next iterator
			in let string_subsig = Symbol.toString subsig
			in
			  if (SootClass.declaresMethodBySubsignature subclass (string_to_java string_subsig))
			  then 
			    let name = string_from_java (SootClass.getName subclass)
			    in let newcmd = Command.MethodCall
					      (Expression.MethodCall(x, y, Expression.Lval (Expression.Symbol name),
								     Expression.Lval (Expression.Symbol subsig), z))
			    in
			      retlist := !retlist @ [newcmd]
		      done;
		      !retlist
		  end
	  end
	    (*
	      | Expression.Access (Expression.Dot, Expression.InstanceOf ((Expression.Lval (Expression.Symbol basename)), JavaType.Reference( JavaType.ClassOrInterfaceType sym_classname )) , fname) ->
	      let classname = Symbol.toString sym_classname
	      in let loaded_classname = Scene.getSootClass (string_to_java classname)
	      in let hierarchy = Scene.getOrMakeFastHierarchy ()
	      in let collection = Hierarchy.getSubclassesOf loaded_classname
	      in let iterator = JniUtils.Collection.iterator collection
	      in ()*)
      | Expression.Assignment(a,b, Expression.MethodCall
				(Expression.Lval (Expression.Symbol fname),
				 Expression.InstanceOf (v, JavaType.Reference (JavaType.ClassOrInterfaceType classname)),
				 Expression.Lval (Expression.Symbol sclsname)
				   , Expression.Lval (Expression.Symbol subsig), z)) ->
	  begin
	    match (Symbol.toString fname) with
	      | "<init>" -> []
	      | _ ->
		  begin
		    let x = Expression.Lval (Expression.Symbol fname)
		    in let y = Expression.InstanceOf (v, JavaType.Reference (JavaType.ClassOrInterfaceType classname))
(*		    in let classname = Symbol.toString sclsname*)
		    in let loaded_classname = Scene.getSootClass (string_to_java classname)
		    in let hierarchy = Scene.getOrMakeFastHierarchy ()
		    in let collection = Hierarchy.getSubclassesOf hierarchy loaded_classname
		    in let iterator = JniUtils.Collection.iterator collection
		    in let retlist = ref []
		    in
		      while (JniUtils.JIterator.hasNext iterator)
		      do
			let subclass = JniUtils.JIterator.next iterator
			in let string_subsig = Symbol.toString subsig
			in
			  if (SootClass.declaresMethodBySubsignature subclass (string_to_java string_subsig))
			  then 
			    let name = string_from_java (SootClass.getName subclass)
			    in let newcmd = Command.MethodCall (Expression.Assignment (a, b, 
					      (Expression.MethodCall(x, y, Expression.Lval (Expression.Symbol name),
								     Expression.Lval (Expression.Symbol subsig), z))))
			    in
			      retlist := !retlist @ [newcmd]
		      done;
		      !retlist
		  end
	  end
      | _ -> failwith "uh"
	  
	  
	  
  let modify_graph graph(*:BriefBlockGraph*) =
    let modifier = alloc_object ModifyGraph.sclass
    in ModifyGraph.modifyGraph modifier graph
    
    
	  
  let transform meth(*:sootMethod*) =
    begin
      let mname = string_from_java (SootMethod.getName meth )
      in
	Message.msg_string Message.Normal ("\nProcessing method:"^mname);
	if (SootMethod.isConcrete meth)
	then
          let activeBody (* :Body *) = SootMethod.retrieveActiveBody meth
	  in let briefBlockGraph (* :BriefBlockGraph *) = (find_class "soot/toolkits/graph/BriefBlockGraph") in
	  let cug = alloc_object briefBlockGraph
	  in
	    begin
              call_void_method cug (get_methodID briefBlockGraph "<init>" "(Lsoot/Body;)V") [| Obj activeBody |] ;
	      modify_graph cug;
 	      let generate_cfa cug (* :briefBlockGraph *) =
		let block = (find_class "soot/toolkits/graph/Block")
		in let automaton = CFA.create_automaton { name = mname ; }(* : automaton_attributes *)
		in let allblocks (* :List *) =  (call_object_method cug (get_methodID briefBlockGraph
									   "getBlocks" "()Ljava/util/List;") [||])
		in let visited = new JniUtils.hashtable
		in let jcls_Integer = find_class "java/lang/Integer"			  
		in
		  visited#init 100;
		  (* checks if we have already allocated a location for this block.
		     if so, return it. else make a new location and map it to the hash table.
		  *)
		  let get_or_make_automaton_location b (* : Block *) :location =
		    let jcls_Integer = find_class "java/lang/Integer"
		    in if (visited#containsKey b) then
			begin
			  let loc_id : int  = 
			    let intid (* :Integer *) = visited#get b
			    in if is_null intid then (*this is_null is a camljava construct*)
				failwith "A block is not found in the hashtbl!"
			      else
				call_camlint_method intid (get_methodID jcls_Integer "intValue" "()I") [||]
			  in
			    get_from_loc_ht loc_id
			end
		      else
			begin
			  let l : location = CFA.create_location mname automaton
					       { source_position = Artificial; traps = Hashtbl.create 1 ; }
			  in let loc_id = l.CFA.loc_id
			  in			  
			    let integer_of_loc_id = (alloc_object jcls_Integer) in 
			      call_void_method integer_of_loc_id (get_methodID jcls_Integer "<init>" "(I)V")
				[| Camlint loc_id |] ;
			      let o = visited#put b integer_of_loc_id
			      in
				add_to_loc_ht l ;
				l
			end
			  
		  in let find_handler_node u (* : Unit *) =
		      JniUtils.javaList_find (fun b ->
						is_same_object u (Block.getHead b) : Jni.obj -> bool
					     ) allblocks (* reminder: this is the list of all the blocks in the graph *)
		  in let connect_succs b blocation succs last =
		      begin 
			let make_predicate bkey v =
			  Predicate.Atom (Expression.Binary (Expression.Eq, bkey, Expression.Constant (Constant.Int v))) 
			in let length = JniUtils.javaList_length succs
			in if length = 0 then
               		    begin
			      (* Put the last unit if it is not an assignment or identity statement *)
			      match last with
				| Command.Block _ -> ()
				| Command.Skip -> ()
				| Command.Throw _ -> ()
				| _ -> 
				    begin
				      let l = CFA.create_location mname automaton { source_position = Artificial ;
										    traps = Hashtbl.create 1; }
				      in
					add_to_loc_ht l ;
					CFA.add_edge blocation l { Command.code = last  ;
								   Command.read = [];
								   Command.written = [] ; }
				    end
			    end
			  else
			    if length = 1 then
			    begin
			      let succ = get_or_make_automaton_location  (JniUtils.javaList_hd succs) in
				(* attach an edge marked Skip between blocation and succ *)
				print_string ("::Connected"^(string_from_java (Block.toShortString b))^"::to::"
				  ^(string_from_java (Block.toShortString (JniUtils.javaList_hd succs)))^"\n");
				begin
				match last with
				  | Command.MethodCall _ ->
				      begin
					let succ2 = CFA.create_location mname automaton (CFA.get_location_attributes succ)
					in
					  add_to_loc_ht succ2;
					  CFA.add_edge blocation succ2 {Command.code=Command.Skip;
									Command.read=[];
									Command.written=[]};
					  CFA.add_edge succ2 succ {Command.code=Command.Skip;
								   Command.read=[];
								   Command.written=[]}
				      end
				  | _ ->
				      CFA.add_edge blocation succ { Command.code = Command.Skip ;
								    Command.read = [] ;
								    Command.written = [] ; } ;
				end;
(*				let lastt = {Command.code = last; read = []; written = [];}
				in print_string ("THIS IS THE LAST:"^(Command.to_string lastt)^"\n");*)
				let preds = CFA.get_preds blocation
				in let length = List.length preds
				in if length = 1 then
				    begin
				      let (cmd,s) = List.hd preds
				      in match cmd.Command.code with
					| Command.MethodCall (x) ->
					    begin
					      let newpreds = CFA.get_preds s
					      in let length = List.length newpreds
					      in
						if length = 1 then
						  begin
						    let (oldcmd, p) = List.hd newpreds
						    in let generate_paths fcmd =  (*path from p to s' to blocation*)
							begin
							  let new_s = CFA.create_location mname automaton
								       (CFA.get_location_attributes s)
							  in let new_blocation = CFA.create_location mname automaton
										   (CFA.get_location_attributes blocation)
							  in let (tmpcmd,tmpsucc) = List.hd (CFA.get_succs blocation)
							  in
							    add_to_loc_ht new_s;
							    add_to_loc_ht new_blocation;
							    CFA.add_edge p new_s {Command.code = (get_pred_2 fcmd);
										  Command.read = [];
										  Command.written = [];};
							    CFA.add_edge new_s new_blocation {Command.code = fcmd;
											      Command.read = [];
											      Command.written = [];};
							    CFA.add_edge new_blocation tmpsucc {Command.code=Command.Skip;
											        Command.read = [];
												Command.written = [];};
							    ()
							end
						    in match oldcmd.Command.code with
						      | Command.Pred _ ->
							  let f_list = get_function_list x
							  in List.iter (fun x -> generate_paths x ) f_list
						      | _ -> ()
						  end;
					    end
					| _ -> ()
				    end;
				  ()
			    end
			    else
			      begin
				print_string "beginning here..\n";
				(* The last unit of the  block determines the successors. *)
				let lastunit = Block.getTail b
				(* the last unit can be a goto, an if, or a switch *)
				in if (is_instance_of lastunit IfStmt.sclass) then
				    begin
				      (* there are two successors. The fall through, and the if *)
				      let cond = IfStmt.getCondition lastunit
				      in let p = Predicate.Atom (Value.convertToBlastExp cond) in
					print_string "Constructed if predicate ";
					print_string (Predicate.toString p) ;
					print_newline ();
					let target = IfStmt.getTarget lastunit
					in print_string "found target!\n";
					  JniUtils.javaList_iter 
					     (fun succ ->
						if (is_same_object (Block.getHead succ) target) then 
						  begin
						    (* add an edge between this location and the successor block with p *)
						    print_string "Found true branch!" ;
						    let succloc = get_or_make_automaton_location succ in
						      print_string("::Connected"^(string_from_java(Block.toShortString b))
								   ^"::to::"^(string_from_java (Block.toShortString succ))
								   ^"\n");
						      CFA.add_edge blocation succloc { Command.code = Command.Pred p ;
										       Command.read = [];
										       Command.written = [] ; } ;
						    ()
						  end
						else
						  begin 
						  (* add an edge between this location and successor block with not p *)
						    print_string "Found false branch!" ;
						    let succloc = get_or_make_automaton_location succ in
						      print_string("::Connected"^(string_from_java(Block.toShortString b))
								   ^"::to::"^(string_from_java (Block.toShortString succ))
								   ^"\n");
						      CFA.add_edge blocation succloc
							{ Command.code = Command.Pred (Predicate.Not p) ;
							  Command.read = [];
							  Command.written = [] ; } ;
						    () 
						end
					     ) 
					     succs ;
					()
				    end
				  else
				    if (is_instance_of lastunit TableSwitchStmt.sclass) then
				      begin
					print_string "inside table switch\n";
					(* first attempt: print out values to get a feeling *)
					let low = TableSwitchStmt.getLowIndex lastunit
					in let hi = TableSwitchStmt.getHighIndex lastunit
					in let key =  TableSwitchStmt.getKey lastunit
					in let bkey = Value.convertToBlastExp key
					in let current_loc = ref blocation
					in
					  (* transform to if statements *)
					  for i = low to hi do 
					    let i_target = TableSwitchStmt.getTarget lastunit i
					    in let nextblock = find_this_target i_target succs 
					    in let trueloc = get_or_make_automaton_location nextblock 
					    in let falseloc =
						if i = hi then 
						  let defaultblock = find_this_target (TableSwitchStmt.getDefaultTarget
											 lastunit) succs 
						  in get_or_make_automaton_location defaultblock 
						else CFA.create_location mname automaton { source_position = Artificial ;
										       traps = Hashtbl.create 1; }
					    in
					      add_to_loc_ht falseloc ;
					      let p = make_predicate bkey i in
						CFA.add_edge !current_loc trueloc { Command.code = Command.Pred p ;
										    Command.read = [] ;
										    Command.written = [] ; } ; 
						CFA.add_edge !current_loc falseloc
						          { Command.code = Command.Pred (Predicate.Not p) ;
							    Command.read = [] ;
							    Command.written = [] ; } ; 
						current_loc := falseloc ;
						()
					  done ;
				      end
				    else
				      if (is_instance_of lastunit LookupSwitchStmt.sclass) then
					begin
					  print_string "inside lookup switch!\n";
					  (* Switch *)
					  let key = LookupSwitchStmt.getKey lastunit
					  in let bkey = Value.convertToBlastExp key
					  in let num = LookupSwitchStmt.getTargetCount lastunit
					  in let current_loc = ref blocation in
					    for i = 0 to num - 1 do
					      let v = LookupSwitchStmt.getLookupValue lastunit i
					      in let p = make_predicate bkey v 
							   (* add an edge between current node and target node labeled p,
							      generate a new node c',
							      add an edge between current and c' labeled not p,
							      set current to c' 
							   *)
					      in let i_target = LookupSwitchStmt.getTarget lastunit i
					      in let nextblock = find_this_target i_target succs 
					      in let trueloc = get_or_make_automaton_location nextblock 
					      in let falseloc =
						  if i = num - 1 then
						    let defaultblock = find_this_target
								 (LookupSwitchStmt.getDefaultTarget lastunit) succs
						    in get_or_make_automaton_location defaultblock
						  else
						    CFA.create_location mname automaton { source_position = Artificial ;
											  traps = Hashtbl.create 1; }
					      in
						add_to_loc_ht falseloc ;
						let p = make_predicate bkey v in
						  CFA.add_edge !current_loc trueloc { Command.code = Command.Pred p ;
										      Command.read = [] ;
										      Command.written = [] ; } ;
						  CFA.add_edge !current_loc falseloc
						    { Command.code = Command.Pred (Predicate.Not p) ;
						      Command.read = [] ;
						      Command.written = [] ; } ;
						  current_loc := falseloc ;
						  ()    
					      done ;
					    ()
					end
				      else
					begin
					  JniUtils.print_java_class lastunit ;
					  failwith "eh?"
					end
			      end
		      end
		  in let process_block b =
		      begin
			print_string "In process block\n";
			print_string ((string_from_java (Block.toShortString b))^"\n");
			let blocation = get_or_make_automaton_location b
					  (* process_units b's units *)
			in let units (* : Iterator *) =  Block.iterator b
			in let thisBlock = ref [] 
			in let lastunit = ref Command.Skip
			in let wasThrow = ref false
			in let wasMethod = ref false
			in let current_location = ref blocation 
			in while Iterator.hasNext units do
			    print_string "One more unit.\n" ;
			    let curr_unit = Iterator.next units
			    in let _ = process_unit_symbols curr_unit
			    in let (cmd, traps) = (unitToBlast curr_unit activeBody) 
			    in let _ = match cmd with
			      | Some (Command.Block [ s ]) -> (*assignment,identity,return,returnvoid*)
				  begin
				    thisBlock := !thisBlock @ [s] ;
				    lastunit := Command.Block [s] ;
				    ()
				  end
			      | Some Command.Skip -> 
				  begin
				    lastunit := Command.Skip ;
				    ()
				  end
			      | Some Command.Throw (x) ->
				  lastunit := Command.Throw (x);
				  wasThrow := true;
				  ()
			      | Some Command.MethodCall (x) ->
				  (*get_function_list funcall;*)
				  lastunit := Command.MethodCall (x);
				  wasMethod := true;
				  ()
			      |	Some other -> 
				  print_string "Something else in this unit: " ;	
				  print_string (Command.to_string { Command.code = other ; Command.read = [] ;
								    Command.written = [] ; } ^"\n");
				  assert (not (Iterator.hasNext units) ) ;
				  lastunit := other ;
				  () 
			      | None -> print_string "None raised\n" ;
				  assert (not (Iterator.hasNext units))
			    in
			      match traps with
				| [] -> ()
				| s  -> (* Do we need to break the basic block? 
					   At the moment, I do not break a basic block. I assume that
					   we shall change sp and wp functions to return Normal (pred) or Exception (type)
					*)
				    (* for each trap, add an entry to the traps table of the current block. *)
				    begin
				      List.iter
					(fun t -> 
					   let ex (* : SootClass *) = Trap.getException t
					   in let extype = SootToAstGlue.get_type (SootClass.getType ex) 
					   in let handler (* : Unit *) = Trap.getHandlerUnit t
					   in let b = find_handler_node handler 
					   in let l = get_or_make_automaton_location b 
					   in Hashtbl.add (CFA.get_location_attributes !current_location).traps extype l 
					) s
				    end
			  done;
			  print_string "Out of this basic block." ;
			  let blocation' = CFA.create_location 
					     mname automaton { source_position = Artificial ; traps = Hashtbl.create 1; }
			  in
			    add_to_loc_ht blocation' ;
			    (* make an edge from blocation to blocation' marked with Block !thisBlock *)
			    print_string "Adding edge for block\n" ;
			    if (!wasThrow) then
			      begin
				CFA.add_edge !current_location blocation' { Command.code = !lastunit;
									    Command.read= [];
									    Command.written=[];} ;
			      end
			    else if (!wasMethod) then
			      begin
				let fblocation = CFA.create_location
						   mname automaton {source_position=Artificial; traps = Hashtbl.create 1;}
				in
				  CFA.add_edge !current_location fblocation {Command.code = get_pred_1 (!lastunit);
									     Command.read = [];
									     Command.written = [];};
				  CFA.add_edge fblocation blocation' {Command.code = !lastunit;
								      Command.read = [];
								      Command.written = []};
			      end
			    else 
			      begin
				CFA.add_edge !current_location blocation' { Command.code = Command.Block !thisBlock;
									    Command.read= [];
									    Command.written=[];} ;
			      end;
			    (* add edges to successors, adding successor nodes if required *)
			    let succs (* :list of blocks *) = CompleteBlockGraph.getSuccsOf cug b
			    in
			      print_string "Number of successors of this node " ;
			      print_string (string_of_int (JniUtils.javaList_length succs)) ;
			      print_string "\n" ;
			      print_string "Connecting edges \n" ;
			      connect_succs b blocation' succs !lastunit;
			      print_string "done connecting edges \n";
			      ()
		      end
		  in
		    (* Process all the blocks in the current graph *)
		    JniUtils.javaList_iter process_block allblocks ; 
		    
		    let heads = (*Complete*)BriefBlockGraph.mrm_getHeads cug
		    in let hds = (JniUtils.javaList_to_ocaml_list heads)
				  (*JniUtils.javaList_hd allblocks*) (*a CRUEL HACK by MRM*)
		    in
		      print_string "Setting start location \n" ;
		      if((List.length (JniUtils.javaList_to_ocaml_list heads))>1) then print_string "More than 1 !!!!!\n";
		      let hdlist = List.map (fun hd -> get_or_make_automaton_location hd) hds
		      in
			List.iter (fun x -> CFA.set_start_locations automaton x) hdlist;
			  (*(get_or_make_automaton_location hd) ;*)
(*			Format.fprintf Format.std_formatter "Printing all locations" ;
			Hashtbl.iter (fun locid loc -> print_location Format.std_formatter loc ;
					if List.mem loc hdlist then Format.fprintf Format.std_formatter "head\n" ; 
	 				List.iter (fun e -> print_edge Format.std_formatter e) (get_outgoing_edges loc) ;
					Format.fprintf Format.std_formatter "That's it.\n" 
				     ) location_id_to_location_ht  ;*)
			automaton
	      in
	      let cfa = generate_cfa cug(*:BriefBlockGraph*) in
		cfa
	    end 
	else 
	  failwith "method is not concrete!"
    end

  let print_all_methods (classname:string) =
    let j_classname = string_to_java classname
    in let loaded_classname = Scene.getSootClass j_classname
    in let methods = SootClass.getMethods loaded_classname
    in let methodIterator = JniUtils.JList.iterator methods
    in
      while(JniUtils.JIterator.hasNext methodIterator)
      do
        let thisMethod = JniUtils.JIterator.next methodIterator
	in let activeBody (* :Body *) = SootMethod.retrieveActiveBody thisMethod
	in let mname = string_from_java (SootMethod.getName thisMethod)
	in let briefBlockGraph (* :BriefBlockGraph *) = (find_class "soot/toolkits/graph/BriefBlockGraph") 
	in let cug = alloc_object briefBlockGraph
	in
	  call_void_method cug (get_methodID briefBlockGraph "<init>" "(Lsoot/Body;)V") [| Obj activeBody |] ;
	  let allblocks (* :List *) =  (call_object_method cug (get_methodID briefBlockGraph
								  "getBlocks" "()Ljava/util/List;") [||])
	  in let heads = JniUtils.javaList_to_ocaml_list (CompleteBlockGraph.getHeads cug)
	  in let key = (Expression.Symbol classname, Expression.Symbol mname)
	  in let (cfa:('a,'b,'c)CFA.automaton) = get_from_function_cfa_table key
	  in let ch = open_out ("cfa/"^mname^"@"^classname^".cfa")
	  in
	    begin
	      mrm_output_cfa_dot ch cfa ;
	      close_out ch;
	    end
      done
	
  let print_system () =
    List.iter (fun classname -> print_all_methods classname) !all_classes_list;
    iterate_all_lvals print_lval_info


      
  let rec doesValueContainInvoke op =
    begin
      if (is_instance_of op InvokeExpr.sclass) then
	true 
      else if (is_instance_of op UnopExpr.sclass) then
	begin
	  let unop = UnopExpr.getOp op
	  in doesValueContainInvoke unop
	end
      else if (is_instance_of op BinopExpr.sclass) then
	begin
	  let op1 = BinopExpr.getOp1 op
	  in let op2 = BinopExpr.getOp2 op
	  in (doesValueContainInvoke op1 || doesValueContainInvoke op2)
	end
      else false 
    end
      
  let transform_methods (classname:string) =
    begin
      let j_classname = string_to_java classname
      in let loaded_classname = Scene.getSootClass j_classname
      in let o_hierarchy = Scene.getOrMakeFastHierarchy ()   (* Find the current class hierarchy *)
      in 
	Hierarchy.print_hierarchy o_hierarchy loaded_classname  SootClass.sclass ;
	let methods = SootClass.getMethods loaded_classname
	in let methodIterator = call_object_method methods 
				  (get_methodID (find_class "java/util/List") "iterator" "()Ljava/util/Iterator;") [| |]
	in
	  print_newline () ;
	  let jcls_iterator = find_class "java/util/Iterator"
	  in while (call_boolean_method methodIterator (get_methodID jcls_iterator "hasNext" "()Z") [| |])
	    do
	      let thisMethod = call_object_method methodIterator
				 (get_methodID jcls_iterator "next" "()Ljava/lang/Object;") [||]
	      in let mname = SootMethod.getName thisMethod
	      in 
		if (SootMethod.isConcrete thisMethod) 
		then 
		  begin
		    let activeBody = SootMethod.retrieveActiveBody thisMethod
		    in
		      if is_instance_of activeBody (find_class "soot/jimple/JimpleBody") then
			()
		      else failwith "Method does not have a Jimple Body" ;
		      set_scope ((string_from_java mname)^"@"^classname);
		      let cfa = transform thisMethod (*note that we tranform the method - not the active body*)
		      in let key = (Expression.Symbol classname, Expression.Symbol (string_from_java mname))
		      in add_to_function_cfa_table key cfa
		  end 
		else () ; 
	    done
    end 
      
      
  let initialize_blast (files: string list) =
    let _ = List.map initialize_scene files
    in
      initialize_class_field_table ();
      let _ = List.map initialize_fields files
      in
	Message.msg_string Message.Normal ("initialised scene!");
	let _ = List.map transform_methods files 
	in print_system ()
      
end
  
