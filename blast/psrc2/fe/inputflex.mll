(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

{
module E = VampyreErrormsg
open Inputparse

let lexerror msg lexbuf = 
  E.s (E.unimp "%s" msg)

}

let digit    = ['0'-'9']
let letdig   = ['0'-'9' 'a'-'z' 'A'-'Z' '_' '@' '#']
let othersyms =[ '-' '$' '#' '!' '+' '=' 
                '<' '>' ',' '?' '\'']
let alphlet  = ['A'-'Z' 'a'-'z' '_' '$' '@']
let capital  = ['A'-'Z']
let small    = ['a'-'z' '$' '_']
let ws       = [' ' '\009' '\012']
let pathname = ['a'-'z' 'A'-'Z' '0'-'9' '.' '/' '\\' '-']

rule token = parse
    '\r'                { token lexbuf}
  | '\n'		{ begin
			    E.startNewline (Lexing.lexeme_end lexbuf);
			    token lexbuf 
			  end }
  | "//"[^'!''\n']*'\n'
                        { begin
                            E.startNewline (Lexing.lexeme_end lexbuf);
			    token lexbuf
                          end }
  | "CONSTANT"            { CONSTANT }
  | "sizeof"            { SIZEOF }
  | "int"               { INT }
  | "char"              { CHAR }
  | "long"              { LONG }
  | "float"             { FLOAT }
  | "double"            { DOUBLE }
  | "void"              { VOID }
  | ws			{ token lexbuf }
  | '['                 { LINDEX }
  | ']'                 { RINDEX }
  | '('			{ LPAREN }
  | ')'			{ RPAREN }
  | "->"		{ ARROW }
  | "->*"		{ ARROWSTAR }
  | "++"                { ICR }
  | "--"                { DECR }
  | '.'			{ DOT }
  | ".*"		{ DOTSTAR }
  | '+'			{ PLUS }
  | '-'			{ MINUS }
  | '*'			{ MUL }
  | '?'                 { QUESTION }
  | '&'                 { BITAND }
  | "&&"                { AND }
  | '|'                 { BITOR  }
  | "||"                { OR  }
  | '^'                 { XOR  }
  | '/'                 { DIV  }
  | '!'                 { NOT  }
  | '~'                 { BITNOT  }
  | '%'                 { REM  }
  | ','                 { COMMA  }
  | ';'                 { SEMICOLON  }
  | ':'                 { COLON  }
  | "<<"		{ LSHIFT }
  | ">>"		{ RSHIFT }
  | "<"			{ LT }
  | ">"			{ GT }
  | "<="		{ LE }
  | ">="		{ GE }
  | "=="		{ EQ }
  | "!="		{ NE }
  | "="                 { ASSIGN }
  | "*="                { MULTassign }
  | "/="                { DIVassign }
  | "%="                { MODassign }
  | "+="                { PLUSassign }
  | "-="                { MINUSassign }
  | "<<="                { LSassign }
  | ">>="                { RSassign }
  | "&="                { ANDassign }
  | "^="                { XORassign }
  | "|="                { ORassign }
  | "[]"                { INDEX }
  | "&&"		{ AND }
  | "||"		{ OR }
  | (digit)+	{ let str = Lexing.lexeme lexbuf in
			  let len = String.length str in
			  let zero = Char.code '0' in
			  let rec accum a d =
			    let acc c = a + (d * ((Char.code c) - zero)) in
			    function
			      0 -> let c = str.[0] in
				   if c='~' then - a else (acc c)
			    | i -> accum (acc str.[i]) (d * 10) (i - 1)
			  in
			  Num (accum 0 1 (len-1)) }
  | (alphlet)letdig*	{ Id    (Lexing.lexeme lexbuf) }

  | eof			{ EOF }
  | _			{ 
                          begin
                            lexerror ("Illegal Character '" ^ 
                                      (Lexing.lexeme lexbuf)) lexbuf;
			    token lexbuf
			  end }

