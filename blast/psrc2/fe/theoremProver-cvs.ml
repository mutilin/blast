(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * Interface to Vampyre and ICS.  This module contains the interface to the theorem provers.
 *)


open Ast
module Stats = Bstats
module E = Expression
module P = Predicate
module M = Message
module O = Options

exception AtomInExpressionException

let tp = ref "" (* ref on which theorem prover to use *)
  (* valid theorem provers: "cvc", "simplify", "simplify-bits" *)

let rec intToHex n x =
  let rec repeatZero m =
    if m = 0 then ""
    else "0"^repeatZero (m-1) 
  in
    if (x = 0) then (repeatZero n) 
    else  (intToHex (n-1) (x / 16)) ^ string_of_int (x mod 16)
 
(***********************************************************)
(************ Hashing for string constants *****************)
(***********************************************************)

let string_const_ctr_ref = ref 0
let string_const_table = Hashtbl.create 101
let const_string_table = Hashtbl.create 101

let get_string_constant s =
  try Hashtbl.find string_const_table s 
  with Not_found ->
     string_const_ctr_ref := 1 + !string_const_ctr_ref;
     let c = string_of_int !string_const_ctr_ref in
     Hashtbl.replace string_const_table s c;
     Hashtbl.replace const_string_table c s;
     c
let get_const_string c = 
  try Hashtbl.find const_string_table c 
  with Not_found -> failwith "Unknown string constant c"

(* only use get_string_const, get_const_string *)
(******************************************************************)

(* reduce predicate from sets, arrays, maps to equality and arithmetic *)


(* This code maintains a mapping from field names of structs to unique indices.  This allows us
   to translate an expression of the form x.f into (select x i), where i is the unique index of f. 
   This is a hack (that works). A more proper translation should look at
   the types, etc.
*)

(* convert an E.expression to Vampyre Input *)
let listOfAddrVars = ref []    
(*  let prfnos = string_of_int (!prfno) in*) (* get rid of prfnos once reinit is fixed *)

let rec convertExp e =
  match e with
    E.Lval lv -> convertLval lv
  | E.Chlval (l, s) -> Input.Fun ("chlval", [ convertLval l; Input.Var ("_"^s) ])
  | E.Binary (op, e1, e2) -> 
      begin
	match op with 
	  E.Minus -> Input.Minus(convertExp e1, convertExp e2)
	|	E.Plus  -> Input.Plus(convertExp e1, convertExp e2)
	|	E.Mul   -> Input.Times(convertExp e1, convertExp e2)
        |       E.Offset 
        |	E.BitAnd 
	|	E.BitOr 
	|	E.Div 
	|       E.RShift 
	|	E.Rem 
	|	E.Xor 
	|	E.Comma ->
	    Input.Fun(E.binaryOpToVanillaString op, [convertExp e1; convertExp e2])
	| E.LShift ->
	    begin
	      match (e2) with
		E.Constant (Constant.Int k) -> (* multiply by 2^k *)
		  Input.Times (convertExp e1, Input.Int (Misc.power 2 k))
	      |	_ -> Input.Fun (E.binaryOpToVanillaString op, [convertExp e1; convertExp e2])
	    end
	| E.FieldOffset ->
	    begin
	      let ve1 = convertExp e1 and ve2 = convertExp e2 in
              Input.Fun ("foffset", [ve1; ve2])
	    end
	|	_ -> failwith ("convertExp (Vampyre): This operator should not be handled here "^(E.toString e))
      end
  | E.Constant (c) -> 
      begin
	match c with
	  Constant.Int i -> Input.Int i
	| Constant.String s -> 
            let c = get_string_constant s in Input.Var ("_STRINGCONSTANT"^c)
	| Constant.Float f -> 
	    if (f -. (ceil f) = 0.0) then Input.Int (int_of_float f)
	    else Input.Var "_FLOATINGPTCONSTANT"
	| _ -> failwith ("Cannot handle constant "^(Constant.toString c))
      end

  | E.Unary (uop, e) -> 
      begin
	match uop with 
	  E.UnaryMinus -> Input.Minus(Input.Int 0, convertExp e)
	| E.UnaryPlus -> convertExp e
	| E.Address -> 
	    let v = convertExp e in
	    if (List.mem v (!listOfAddrVars)) then () 
	    else
	      listOfAddrVars := v::(!listOfAddrVars);
	    Input.Fun("addrOf", [v])
	|	_ -> Input.Fun(E.unaryOpToVanillaString uop, [convertExp e])
      end
  | E.FunctionCall (E.Lval (E.Symbol f), args) -> 
      begin
	Input.Fun (f, List.map convertExp args)
      end
  | _ -> failwith ("convertExp: " ^ (E.toString e) ^ "not handled")
	
  and convertLval l =
    match l with
    | E.This -> failwith "This not handled in convertLval"
    | E.Symbol(str) -> Input.Var ("_"^str) (* prepend all symbols with "_" for later print out in LF *)
    | E.Access(E.Dot, e, s) -> (* NEEDS DEBUGGING *) 
	let v = convertExp e in
	Input.Fun("select", [v; Input.Var ("_"^(s))])
	  
    | E.Access(E.Arrow, e, s) ->
	failwith "Arrrow"
    | E.Dereference e -> Input.Fun("select", [convertExp e; Input.Int 0])
    | E.Indexed (e1, e2) -> Input.Fun("sel4", [convertExp e1; convertExp e2])


  and convertAtom e =
    match e with
      E.Binary (op, e1, e2) -> 
	begin
	  match op with 
	    E.Eq  -> Input.Eq(convertExp e1, convertExp e2)
	  |	E.Ne  -> Input.Neq(convertExp e1, convertExp e2)
	  |	E.Gt  -> Input.Gt(convertExp e1, convertExp e2)
	  |	E.Lt  -> Input.Lt(convertExp e1, convertExp e2)
	  |	E.Ge  -> Input.Geq(convertExp e1, convertExp e2)
	  |	E.Le  -> Input.Leq(convertExp e1, convertExp e2)
	  |	_   -> failwith ("convertAtom: Cannot handle this expression: "^(E.toString e))
	end
    | E.FunctionCall(E.Lval (E.Symbol f), args) ->
	begin (* a boolean function *)
	  Input.Eq(convertExp e, Input.Int 0)
	end
    | _ -> failwith ("convertAtom: "^(E.toString e)^ " Not a binary relation operator")
  
  and convertPred e =
    match e with
      P.True -> Input.True
    | P.False -> Input.False
    | P.And plist -> 
	List.fold_left (function a -> function b -> Input.And(convertPred b, a)) 
	  Input.True (plist)
    | P.Or plist -> 
	List.fold_left (function a -> function b -> Input.Or(convertPred b, a)) 
	  Input.False (plist)
    | P.Implies (e1, e2) -> Input.Imp(convertPred e1, convertPred e2)
    | P.Iff (e1,e2) -> 
        let e1' = convertPred e1 in
        let e2' = convertPred e2 in
        Input.And (Input.Imp(e1',e2'), Input.Imp(e2',e1'))
    | P.Not e1 -> Input.Not(convertPred e1)
    | P.Atom e ->  Input.Atom (convertAtom e)
    | P.All (s,p) -> Input.All (("_"^s), convertPred p)
    | _ -> failwith ("Cannot handle this predicate "^(P.toString e))
 
and convertToVampyreSyntax e =
(*  let prfnos = string_of_int (!prfno) in*) (* get rid of prfnos once reinit is fixed *)(*  prfno := !prfno + 1; *)
  listOfAddrVars := [];
  let v_exp = convertPred (P.constantfold e (Hashtbl.create 1))
  in
  let genAddrOfEq b =
    Input.Atom(Input.Eq(b, Input.Fun("sel4", [Input.Fun("addrOf", [b]); Input.Int 0])))
  in
  begin	
    (* For each address variable, assert that &b != 0, and sel(&b, 0) = b *)
    let vv = List.fold_left (function a -> function b -> Input.Imp (Input.Atom (Input.Neq(b, Input.Int 0)) , Input.Imp(genAddrOfEq b, a)))
	v_exp !listOfAddrVars
    in
    vv
  end
      
      
      
(* Call to prover *)
(* An Engine.Failure is assumed to indicate falsehood *)
      
(* Initialize Vampyre *)
module S = Satproc
module CC : S.SATPROC = Equality
module SX : S.SATPROC = Simplex
module SI : S.SATPROC = Sup_inf
module GT : S.SATPROC = Tactical
module TYP: S.SATPROC = Typing
module SP: S.SATPROC = Special

let (vampyreProver,vampyre_assume_list, vampyre_prove_context, vampyre_prove_context_proof) =
  let allSatProcs = 
    [CC.initialize "CC";
      SX.initialize "SX";
      GT.initialize "GT";
     (* SI.initialize "SI"; 
      TYP.initialize "TYP";
      SP.initialize "SP" *)
    ]
  in
  (* circular dependency hack *)
  ignore(Proof.simp_and_subtract_is_zero := Sup_inf.simp_and_subtract_is_zero;
	 Proof.simp_and_subtract_is_at_least_zero := Sup_inf.simp_and_subtract_is_at_least_zero);
  (* end circular dependency hack *)
  Engine.prove allSatProcs

(* Interface functions*)
let proof = ref None 

let queryExpUsingVampyre exp =
  try
    let vampyreInput = convertToVampyreSyntax exp
    in
    let vampyreInput_pushneg = (Input.pushNeg  vampyreInput) in
    try
(*
	let _ = VampyrePretty.printf "Vampyre Saw queryExp: %a \n" Input.dp vampyreInput_pushneg
    	in
*)
      let _ = proof := Some (vampyreProver "foo" (vampyreInput_pushneg)) in
      ignore !proof ; true
    with Engine.Failure -> (M.msg_string M.Debug "Proof failed.\n") ; false
  with
    VampyreErrormsg.Error -> (failwith ("Bug in Vampyre ..."))

(* for now only vampyre supports contexts -- assume etc *)
let assume assumptions = 
  let atomize_asm asm_pred = 
    match asm_pred with
      P.Atom e -> (convertAtom e)
    | P.Not P.Atom e -> Input.negate  (convertAtom e)
    | _ -> 
	M.msg_string M.Error "Bad assumption in atomize_asm";
	M.msg_printer M.Error P.print asm_pred;
	failwith ("bad asm !")
  in

  (* try *) 
    let vam_assumptions = List.map atomize_asm assumptions 
    in
    vampyre_assume_list vam_assumptions
  (* with
    _ -> failwith "Vampyre Bug due to Jhala !" *)

let printproof pred = 
  let _eToCheck = P.implies pred (P.False) in
  let vampyreInput = convertToVampyreSyntax _eToCheck in
  let vampyreInput_pushneg = (Input.pushNeg  vampyreInput) in
    try
      let proof = vampyreProver "foo" (vampyreInput_pushneg)
      in
        ignore (VampyrePretty.printf "Vampyre proof: %a \n" Proof.dp proof); ()
    with Engine.Failure -> (M.msg_string M.Debug "Proof failed.\n") ; ()
     
	
(* This converts a vampyre predicate to one that blast can absorb *)
let convertToPredicate w =
  let rec convertExp e =
    match e with
      Input.Var var -> 
	      (* drop initial "_" *)
	if (String.get var 0 = '_') then
	  E.Lval (E.Symbol (String.sub var 1 ((String.length var) - 1)))
	else (* Huh? ConvertExp did not prepend a "_"! *)
	  E.Lval (E.Symbol var)
	    
    | Input.Int i   -> E.Constant (Constant.Int i)
    | Input.Plus (e1, e2) -> E.Binary (E.Plus, convertExp e1, convertExp e2)
    | Input.Minus (e1, e2) -> E.Binary (E.Minus, convertExp e1, convertExp e2)
    | Input.Times (e1, e2) -> E.Binary (E.Mul, convertExp e1, convertExp e2)
    | Input.Div (e1, e2) -> E.Binary (E.Div, convertExp e2, E.Constant (Constant.Int e1))
    | Input.Mod (e1, e2) -> E.Binary (E.Rem, convertExp e2, E.Constant (Constant.Int e1))
    | Input.Fun (s, el) -> E.FunctionCall (E.Lval (E.Symbol s), List.map convertExp el)
    | _ -> failwith "getUsefulPredicates: Do not handle Floor and Ceiling"
  in
  let rec convertAtom a =
    match a with
      Input.Beq (a1, a2) -> E.Binary(E.Eq, convertAtom a1, convertAtom a2)
    | Input.Eq (e1, e2) -> E.Binary (E.Eq, convertExp e1, convertExp e2)
    | Input.Neq (e1, e2) -> E.Binary (E.Ne, convertExp e1, convertExp e2)
    | Input.Geq (e1, e2) -> E.Binary (E.Ge, convertExp e1, convertExp e2)
    | Input.Leq (e1, e2) -> E.Binary (E.Le, convertExp e1, convertExp e2)
    | Input.Gt (e1, e2) -> E.Binary (E.Gt, convertExp e1, convertExp e2)
    | Input.Lt (e1, e2) -> E.Binary (E.Lt, convertExp e1, convertExp e2)
    | _ -> failwith "getUsefulPredicates: BFun and NotBFun not handled."
	in
  let _w = convertAtom w in
  P.Atom _w

  

(* Interface functions*)


(******************************************************************)
(* Interface to CVC                                               *)
(******************************************************************)
	     
(* keep around info: do we have a cvc server (= other process) 
	     * running? *)

exception ChannelException

let filed = open_out "blast-trace.cvc" 

let _ = at_exit (fun () -> close_out filed)


let reset_alarm () =
  (* Sys.set_signal Sys.sigalrm (Sys.Signal_ignore) *) ()

let set_alarm () =
  ()
  (*
  ignore (Sys.signal
            Sys.sigalrm
            (Sys.Signal_handle
               (fun i ->
                 M.msg_string M.Error "Caught exception sigalrm in stream!" ;
		  reset_alarm ();
                  raise  ChannelException;
               )) ) ;
  ignore (Unix.alarm (5))
  *)

(*
let simplify_line_tap = ref [] (* string list *)
*)

let secure_output_string oc s = (* CONTRACT: may raise channel exception ... *)
  try
    set_alarm ();
(*
    simplify_line_tap := s::!simplify_line_tap;    
*)
    output_string oc s;
    flush oc;
    reset_alarm ()
  with ChannelException -> 
    
    reset_alarm ();
(*
    dump_line_tap ();
*)
    raise ChannelException

let secure_input_line ic = (* CONTRACT: may raise channel exception *)
  
  set_alarm ();
  try
(*
    simplify_line_tap := ""::!simplify_line_tap;
*)
    let rv = input_line ic in
      reset_alarm ();
      rv
  with ChannelException ->
    reset_alarm () ;
(*
    dump_line_tap ();
*)
    raise ChannelException


let cvcServerWithProof = ref None   
let cvcPushes = ref 0
let simp_ctr = ref 0
let clear_varCvcHashtbl_flag = ref true (* the table will get cleared only if this flag is true *)
		    (* moreover -- when simplify_assume is called the flag gets set to false *)
		    (* when pop is called, the flag is again set to true *)

let varCvcHashtbl = Hashtbl.create 53 
let next_cvc_var = ref 0 
let chlvalCvcHashtbl = Hashtbl.create 53 
let unknown_Cvc_var_counter = ref 0 

let getCvcServerWithProof () = 
  match !cvcServerWithProof with
    None -> begin
      M.msg_string M.Normal "Forking CVC process (with proofs)..." ;
      flush stdout ;
      let ic,oc = Unix.open_process "cvcl -sat simple -assump -tcc" (* +proof *)
      in
      cvcServerWithProof := Some(ic,oc) ;
      M.msg_string M.Normal "done!\n";
      flush stdout ;
      at_exit (fun () -> 
	ignore (Unix.close_process (ic,oc) ) ) ;
      (ic,oc)
	end
  | Some(a) -> a

(* RuGang's functions 03/13/2005:
   getCvcServer
   askCvcIfValid *)

let cvcServer = ref None   

let cvclAsserts = "addrOf:[[REAL, REAL] -> REAL];
 addrof:[REAL->REAL];
 foffset:[[REAL, REAL] -> REAL]; 
 select:[[REAL, REAL] -> REAL]; 
 RShift:[[REAL, REAL] -> REAL]; 
 LShift:[[REAL, REAL] -> REAL]; 
And: [[REAL, REAL] -> REAL]; 
Or:  [[REAL, REAL] -> REAL]; 
 ASSERT (FORALL (x, y: REAL): ((select (addrOf (x,y),0)) = x)); 
 ASSERT (FORALL (x, y: REAL): (addrOf (x,y) /= 0)); 
 ASSERT (FORALL (x, y, z: REAL): (foffset(x, z) = foffset(y,z)) => (x = y));"


let cvc_ground_term_list = ref [] 
    

let add_cvc_ground str = 
  if List.mem str (!cvc_ground_term_list) 
  then () 
  else 
    begin
      if (Misc.is_prefix "ASSERT" str) then
        cvc_ground_term_list := (!cvc_ground_term_list) @ [ str ]
      else 
        cvc_ground_term_list := str::(!cvc_ground_term_list)
    end 
      
let reset_cvc_ground () = 
  cvc_ground_term_list := 
   [
   ]


let getCvcServer () = 
  match !cvcServer with
    None -> 
      begin	
	M.msg_string M.Debug "Forking CVC process (no proofs)..." ;
	let ic,oc = Unix.open_process  
	  "cvcl +int  -tcc 2>&1" 
	in
	  cvcServer := Some(ic,oc) ;
	  secure_output_string oc cvclAsserts; 
	  flush stdout ;
	  at_exit (fun () ->
		     ignore (Unix.close_process (ic,oc) ) ) ;
	  (ic,oc)
      end
    | Some(a) -> a

    
(* usually the answer is at the end like
   CVC> CVC> - CVC> - CVC> - CVC> - CVC> - CVC> InValid.
   so using substring 
   other cases might break it? -RuGang, 03-13-05 *)
let rec askCvcIfValid ic =
  M.msg_string M.Debug ("asking cvcl ... \n");
  let line = secure_input_line ic in 
  M.msg_string M.Debug ("got cvcl ");
  let result = if Misc.is_substring line "InValid." then
    begin 
      M.msg_string M.Debug ("CVC: Invalid"^line);
      false
    end

  else if Misc.is_substring line "Valid." then 
    begin
      M.msg_string M.Debug ("CVC: Valid"^line);
      true
    end
  else if Misc.is_substring line "Error" then 
   (print_string ("Error: "^line^"\n");
   print_string ((input_line ic)^"\n");
   print_string ((input_line ic)^"\n");
   flush stdout;
   failwith "Error in CVC output!")
  else (print_string line; askCvcIfValid ic)
  in
  result

    




       
let convertToCvcSyntax exp =
  let get_next_var () = next_cvc_var := !next_cvc_var + 1; !next_cvc_var in
  let get_current_var () = !next_cvc_var in

  if (!clear_varCvcHashtbl_flag) then 
   begin
     Hashtbl.clear chlvalCvcHashtbl;
     next_cvc_var :=0
   end;
  (* I have no idea what the convert recursive crap means? *)
  (* let chlval_convert_recursive (lv,s) =  Printf.sprintf "(chlval %s %s)" (convertLval lv) ("FLD"^ s) in    *)
  let chlval_convert_table e = 
   try (Hashtbl.find chlvalCvcHashtbl e) 
      with 
	  Not_found ->
	    begin
	      let s_e = get_next_var () (* E.toString e*) in
              let new_id = "chlval"^(string_of_int s_e) in
              if (not (Hashtbl.mem varCvcHashtbl new_id)) then
                  begin
                     add_cvc_ground (new_id^": REAL; ");
                      Hashtbl.add varCvcHashtbl new_id s_e
                  end;
      	      Hashtbl.add chlvalCvcHashtbl e new_id;
              new_id
	    end 
  in
  let rec convertExp e =
    match e with
      | E.Lval lv -> convertLval lv
      | E.Chlval (lv,s)  -> 
	  if (O.getValueOfBool "clock") then  Printf.sprintf "(chlval %s %s)" (convertLval lv) ("FLD"^ s)  
                                          else chlval_convert_table e   
    | E.Binary (op, e1, e2) -> 
	  begin
	    match op with 
	      E.Minus -> 
		begin
		    match e2 with
	              E.Constant (Constant.Int 0) -> (convertExp e1)
	            | _ -> Printf.sprintf "(%s - %s)" (convertExp e1) (convertExp e2)
		end
	      |	E.Plus  -> 
		  begin
		    match e2 with
                      E.Constant (Constant.Int 0) -> (convertExp e1)
	            | _ -> Printf.sprintf "(%s + %s)" (convertExp e1) (convertExp e2)
		  end
	      |	E.Offset -> 
	          Printf.sprintf "(offset (%s,%s))" (convertExp e1) (convertExp e2)
	      |	E.Mul ->   
	          Printf.sprintf "(%s * %s)" (convertExp e1) (convertExp e2)
	      |	E.Div ->
	          Printf.sprintf "(%s / %s)" (convertExp e1) (convertExp e2) 
	      |	E.BitAnd ->
	          Printf.sprintf "And(%s,%s)" (convertExp e1) (convertExp e2)
	      |	E.BitOr -> 
	          Printf.sprintf "Or(%s,%s)" (convertExp e1) (convertExp e2)
	      |	E.Rem 
	      |	E.Xor 
	      |	E.Comma ->
	          Printf.sprintf "guf(%s,%s)" (convertExp e1) (convertExp e2)
	      |	E.LShift ->
		  begin
		    match (e2) with
		      E.Constant (Constant.Int k) -> (* multiply by 2^k *)
			Printf.sprintf "(%s * %d)" (convertExp e1) (Misc.power 2 k)
		    |	_ -> Printf.sprintf "(%s (%s,%s))" (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2)
		  end
	      |	E.RShift ->
		  begin
		    match (e2) with
		      E.Constant (Constant.Int k) -> (* divide by 2^k *)
			Printf.sprintf "(%s / %d)" (convertExp e1) (Misc.power 2 k) 
		    |	_ -> Printf.sprintf "(%s (%s,%s))" (E.binaryOpToVanillaString op) (convertExp e1)  (convertExp e2)
		  end	 
	      |	_ -> 
              if E.isRelOp op 
	      then 
		raise AtomInExpressionException
	      else 
		failwith ("convertExp: (CVC) This operator should not be handled here "^(E.toString e))
	  end

    | E.Constant (c) -> 
	begin
	  match c with
		Constant.Int i ->  
	          string_of_int i
	  | Constant.String s -> 
	      if (not (Hashtbl.mem varCvcHashtbl "STRING")) then 
		begin
		  add_cvc_ground ("STRING"^" : REAL ;");
		  Hashtbl.add varCvcHashtbl "STRING"  1
		end;
	      ("STRING") 
	  | _ -> failwith ("unknown constant type")
	end
    | E.Unary (uop, e) -> 
	begin
	  match uop with 
	      E.UnaryMinus -> Printf.sprintf ("( - %s)") (convertExp e)
	    | E.UnaryPlus -> Printf.sprintf ("( + %s)") (convertExp e)
	    | E.Address -> 
	      begin
	        let e_cvc = convertExp e in
	        let expr = Printf.sprintf "(addrof(%s))" e_cvc in
	          (* Also add an assertion relating deref and address for this expression. *)
                  add_cvc_ground (Printf.sprintf "ASSERT (select (%s, 0) = %s);" expr e_cvc) ;
	          expr
              end
	  |	_ -> failwith "unknown unop" 
	end
    | E.FunctionCall(E.Lval (E.Symbol f), args) ->
        begin
          Printf.sprintf "(%s ( %s ))" f (Misc.strList (List.map convertExp args))
        end
    | _ -> failwith ("convertExp (Cvc): " ^ (E.toString e) ^ "not handled")
  and convertLval l =
   match l with
   | E.This -> failwith "This not handled in convertLval"
   | E.Symbol(str) -> 
       begin
	 let cvc_name = (Str.global_replace (Str.regexp "@") "LOCAL" str) in
	   if (Hashtbl.mem varCvcHashtbl cvc_name) then cvc_name
	   else 
	     begin
	       Hashtbl.add varCvcHashtbl cvc_name  1; (* number 1 doesn't matter *)
	       add_cvc_ground (cvc_name ^ ": REAL ;") ;
	       cvc_name
	     end
       end
   | E.Dereference drfe -> "select( "^(convertExp drfe)^ ", 0)"
   | E.Access(E.Dot, e, s) -> 
       begin
	  if (not (Hashtbl.mem varCvcHashtbl ("FLD"^s))) then 
	    begin
	      add_cvc_ground ("FLD"^s^" : REAL;");
	      Hashtbl.add varCvcHashtbl ("FLD"^s)  1
	    end;
	 Printf.sprintf "(select(%s,%s))" (convertExp e) ("FLD"^s)
       end
    | E.Access(E.Arrow, e, s) ->
	begin
	  if (not (Hashtbl.mem varCvcHashtbl ("FLD"^s))) then 
	    begin
	      add_cvc_ground ("FLD"^s^" : REAL;");
	      Hashtbl.add varCvcHashtbl ("FLD"^s)  1
	    end;
	  (Printf.sprintf "(select(%s,%s))" (convertExp e) ("FLD"^s))
	end
    | _ -> failwith ("convertLval (Cvc): " ^ (E.lvalToString l) ^ "not handled")
	 
  and convertAtom e =
   flush stdout;
    match e with
      E.Binary (op, e1, e2) -> 
	begin
	  let opstring = 
	    match op with 
	      E.Eq -> "=" | E.Ne -> "/=" | E.Gt  -> ">" | E.Lt  -> "<" | E.Ge  -> ">="
	    | E.Le  -> "<=" | _   -> "" in
          if opstring <> "" 
	  then
              try Printf.sprintf "(%s %s %s)"  (convertExp e1) opstring (convertExp e2)
              with AtomInExpressionException ->
		  if (opstring = "=" or opstring = "/=") then
                      match e1 with
			E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
			    let s1 = convertExp e1 in
			    let s2 = convertAtom e2 in
                            if (opstring = "=") then Printf.sprintf "((%s /= 0) <=> (%s))" s1 s2
                            else Printf.sprintf "((%s = 0) <=> (%s))" s1 s2
                      | _ -> 
			    (match e2 with
			      E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
				  let s1 = convertExp e2 in
				  let s2 = convertAtom e1 in
				    if (opstring = "=") then Printf.sprintf "((%s /= 0) <=> (%s))" s1 s2
				    else Printf.sprintf "((%s = 0) <=> (%s))" s1 s2
			    | _ -> "")
		  else ""
	  else (M.msg_string M.Debug "failed opthing";M.msg_string M.Debug (E.toString e); 
               Printf.sprintf "")
	end
    | E.FunctionCall(E.Lval (E.Symbol f), args) ->
	begin
          Printf.sprintf "(%s ( %s ) = 0)" f (Misc.strList (List.map convertExp args))
        end
    | _ -> (M.msg_string M.Debug "failed opthing2 (cvc)";M.msg_string M.Debug (E.toString e);  Printf.sprintf "")
 	  
  and convertPred e =
   flush stdout;
    match e with
      P.True -> "TRUE"
    | P.False -> "FALSE"
    | P.And plist -> 
	List.fold_left 
	  (fun a -> fun b -> Printf.sprintf " (%s AND %s) " (convertPred b) a) 
	  "TRUE" (plist)
    | P.Or plist -> 
	List.fold_left 
	  (function a -> function b -> Printf.sprintf "(%s OR %s)" (convertPred b) a )
	    "FALSE" (plist)
    | P.Implies (e1, e2) -> Printf.sprintf "(%s => %s)" (convertPred e1) (convertPred e2)
	  
    | P.Not e1 -> Printf.sprintf "(NOT %s)" (convertPred e1)
    | P.Atom e ->  (convertAtom e)
    | P.All (s,e1) -> Printf.sprintf "(FORALL (%s : REAL): %s)" s (convertPred e1)
    | _ -> failwith ("CVC: Cannot handle this predicate "^(P.toString e))
	  

  in
  (convertPred (P.constantfold exp (Hashtbl.create 1)))
    
    
    
let cvcQueryNum = ref 1
let cvcQueryNumPf = ref 1
    
    
let rec cvc_output_list oc lst =
   match lst with 
   [] -> () 
   | a::rest -> 
   begin
   M.msg_string M.Debug ("cvc input: "^a^"\n");
   secure_output_string oc (Printf.sprintf "%s\n" a) ;
   output_string filed  (Printf.sprintf "%s\n" a) ;
   flush oc;
   flush filed;
   cvc_output_list oc rest 
   end 

let queryExpUsingCvc exp = 
  M.msg_string M.Debug ("cvcl: querying exp:");
  M.msg_printer M.Debug P.print exp ;
  let cvcInput = convertToCvcSyntax exp
  in 
  M.msg_string M.Debug ("cvcl: "^cvcInput);
  flush stdout;
  flush stderr;
   let result = 
    try
      let ic,oc = getCvcServer () in
      cvc_output_list  oc !cvc_ground_term_list; 
      secure_output_string oc ("PUSH; QUERY ("^cvcInput^"); POP;\n") ;
      output_string filed ("PUSH; QUERY ("^cvcInput^"); POP;\n") ;
      M.msg_string M.Debug ("cvc input: "^"QUERY ("^cvcInput^");\n");
      reset_cvc_ground ();
      flush oc ; 
      flush filed;
      askCvcIfValid ic
    with e ->
      failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp)^". Check that cvcl is in your path.")
  in
  M.msg_string M.Debug (if result then "true" else "false");
  result

    
let queryExpUsingCvcGreedy exp =
  let _ = M.msg_string M.Debug ("querying exp in queryExpUsingCvcGreedy :"^(P.toString exp)^"\n") in
   (* first we have to convert the formula and collect all the ground terms that appear in it *)

  let fdgreedy = open_out "blast-trace-greedy.cvc" in

  reset_cvc_ground ();
  let formulas = match exp with P.And l -> l | _ -> failwith ("bad call to queryExpUsingCvcGreedy") in
  let all_asserts = List.map (convertToCvcSyntax) formulas
  in
	     (* at this point all_asserts is a list of all assertions, and !cvc_ground_term_list is
	        the list of all type declarations.
	     *)
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
	begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest 
	end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest 
        end
  in
  let ic,oc = getCvcServer () in
  let rec getUsefulPredicates usefulPreds  =
    let rec _worker assertlist =
      match assertlist with 
	[] -> failwith "Exception : Formula is not valid!"
      | a :: rest ->
	  output_string oc (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\nQUERY FALSE;\n" a (string_of_int !cvcQueryNum)) ;

	  output_string fdgreedy (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\n\n" a (string_of_int !cvcQueryNum)) ;

          incr cvcQueryNum ;
          flush oc ;
          flush fdgreedy ;
	  let result = 
	     try
   	       askCvcIfValid ic
	     with e ->
	       failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          if result then a
          else
             _worker rest
    in
    output_string oc "PUSH 1;\n" ;
    output_string fdgreedy "PUSH 1;\n" ;
    output_list oc !cvc_ground_term_list ;
    output_asserts oc usefulPreds ;
    output_list fdgreedy !cvc_ground_term_list ;
    output_asserts fdgreedy usefulPreds ;
    output_string oc (Printf.sprintf "ECHO \"Query %s\" ; \n" (string_of_int !cvcQueryNum)) ;
    incr cvcQueryNum ;
    output_string oc "QUERY FALSE; \n" ;
    flush oc ;
    flush fdgreedy ;
    let result = 
      try
	askCvcIfValid ic
      with e ->
	failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
    in
    if result then usefulPreds 
    else 
      begin
        (* go on adding predicates from assertlist and check for validity.
	   When valid, add the last formula to usefulPreds and repeat
           *)
        let nextpred = _worker all_asserts in
        output_string oc "POPTO 1; \n" ;
        output_string fdgreedy "POPTO 1; \n" ;
        getUsefulPredicates (nextpred :: usefulPreds) 
      end
    in
  let listOfPreds = getUsefulPredicates [] 
  in
  output_string oc "POPTO 1;\n" ;
  output_string fdgreedy "POPTO 1;\n" ;
  flush oc ;
  output_string fdgreedy "ECHO\"Predicates are \" ;\n" ;
  output_list fdgreedy (List.map (fun s -> "ECHO \""^s^"\";\n") listOfPreds) ;
  flush fdgreedy ;
  close_out fdgreedy ;
   listOfPreds 

    

    
let queryExpUsingCvcProof exp =

   M.msg_string M.Debug ("CVC querying exp:"^(P.toString (P.convertDNF exp))^"\n");

  reset_cvc_ground ();
  let formulas = exp::[] in 
  let all_asserts = List.map convertToCvcSyntax formulas
  in
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest
        end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest
        end
   in
(*  let ic,oc = getCvcServerWithProof () in - RuGang*)
   (* what are dump assertions? *)
   M.msg_string M.Debug "Asking CVC (Proof)";
   let ic,oc = getCvcServer () in
   output_string oc (Printf.sprintf "ECHO \"Query %s\";\nPUSH 1;\n" (string_of_int !cvcQueryNumPf)) ;
  output_string filed (Printf.sprintf "ECHO \"Query %s\";\nPUSH 1;\n" (string_of_int !cvcQueryNumPf)) ;
  output_list oc !cvc_ground_term_list ;
  output_list filed !cvc_ground_term_list ;
   M.msg_string M.Debug "after cvc ground crap";
   flush oc;
  let rec assert_loop asrtlist =
    match asrtlist with
      [] ->
        begin
          output_string oc "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
          output_string filed "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
          incr cvcQueryNumPf ;
          flush oc ; 
          flush filed ;
          let result = 
	     try
	       askCvcIfValid ic
	     with e ->
	      failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          reset_cvc_ground () ;
          flush oc;
          result       
        end
    | a::rest ->
       begin 
         output_string oc (Printf.sprintf "ASSERT %s;\n QUERY FALSE;\n" a);
         output_string filed (Printf.sprintf "ASSERT %s;\n QUERY FALSE;\n" a);
         flush oc ;
         let result = 
	     try
	       askCvcIfValid ic
	     with e ->
	       failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
         in
         if result then
           begin
             output_string oc "\nDUMP_ASSUMPTIONS; \nPOPTO 1;\n" ;
             output_string filed "\nDUMP_ASSUMPTIONS; \nPOPTO 1;\n" ;
             incr cvcQueryNumPf ;
             flush oc ; 
             flush filed ;
             reset_cvc_ground () ;
             flush oc;
             result      
           end
         else  
           assert_loop rest
       end
  in
  ignore (assert_loop all_asserts);
  output_asserts oc all_asserts ;
  output_list filed !cvc_ground_term_list ;
  output_asserts filed all_asserts ;
  output_string oc "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
  output_string filed "\nDUMP_ASSUMPTIONS; QUERY FALSE;\nPOPTO 1;\n" ;
  incr cvcQueryNumPf ;
  flush oc ; 
  flush filed ;
  let result = 
    try
      askCvcIfValid ic
    with e ->
      failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
  in
  reset_cvc_ground () ;
  flush oc;
  result       



(* This implements the greedy algorithm (SLAM, Ruess, ...) to get back a list
   of useful predicates.
*)
exception CVCException 

let getProofsFromCvc exp =
  let _ = M.msg_string M.Debug ("querying exp in getProofsFromCvc :"^(P.toString exp)^"\n") in
   (* first we have to convert the formula and collect all the ground terms that appear in it *)
  try  
  reset_cvc_ground ();
  let formulas = 
    match exp with 
      P.And l -> l 
    | _ -> failwith ("bad call to getProofsFromCvc -- we only handle listd of conjunections") in
  let all_blast_asserts_array = Array.of_list formulas in
  let all_asserts = List.map (convertToCvcSyntax) formulas
  in
	     (* at this point all_asserts is a list of all assertions, and !cvc_ground_term_list is
	        the list of all type declarations.
	     *)
  let rec output_asserts oc predlist =
    match predlist with 
      [] -> () 
    | a::rest -> 
	begin
          output_string oc (Printf.sprintf "ASSERT %s;\n" a) ; 
          output_asserts oc rest 
	end
  in 
  let rec output_list oc lst =
    match lst with 
      [] -> () 
    | a::rest -> 
        begin
          output_string oc (Printf.sprintf "%s\n" a) ; 
          output_list oc rest 
        end
  in
  let ic,oc = getCvcServer () in
  let rec usefulPredicates usefulPreds  usefulBlastPreds =
    let rec _worker assertlist num =
      match assertlist with 
	[] -> 
	  begin
	    M.msg_string M.Error "Exception : Formula is not valid! Reverting to Vampyre" ;
	    raise CVCException 
	  end
      | a :: rest ->
	  output_string oc (Printf.sprintf "ASSERT %s;\nECHO \"Query %s\";\nQUERY FALSE;\n" a (string_of_int !cvcQueryNum)) ;
	  incr cvcQueryNum ;
          flush oc ;
	  let result = 
	     try
   	       askCvcIfValid ic
	     with e ->
	       failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
          if result then (a, num)
          else
             _worker rest (num+1)
    in
    output_string oc "PUSH 1;\n" ;
    output_list oc !cvc_ground_term_list ;
    output_asserts oc usefulPreds ;
    output_string oc (Printf.sprintf "ECHO \"Query %s\" ; \n" (string_of_int !cvcQueryNum)) ;
    incr cvcQueryNum ;
    output_string oc "QUERY FALSE; \n" ;
    flush oc ;
    let result = 
      try
	askCvcIfValid ic
      with e ->
	failwith ("Cvc raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
    in
    if result then usefulBlastPreds 
    else 
      begin
        (* go on adding predicates from assertlist and check for validity.
	   When valid, add the last formula to usefulPreds and repeat
           *)
        let (nextpred, i) = _worker all_asserts 0 in
        output_string oc "POPTO 1; \n" ;
        usefulPredicates (nextpred :: usefulPreds) ((all_blast_asserts_array.(i)) :: usefulBlastPreds)
      end
    in
  let listOfPreds = usefulPredicates [] []
  in
  output_string oc "POPTO 1;\n" ;
  flush oc ;
  List.map 
    (fun a -> match a with P.Not b -> b | _ -> a)
    listOfPreds
with _ ->
	    begin
	      let _eToCheck = P.implies exp (P.False) in
              let ans = queryExpUsingVampyre _eToCheck
              in
              let _ = M.msg_string M.Debug "Got UsefulPred" in
              if ans = false
              then
		begin
		  failwith "Vampyre could not prove formula valid!" ; []
		end
              else
                begin
		  let _lst = Engine.getUsefulPredicates () in
            (*
	      let inferred_atoms = 
	      match (!proof) with 
	      Some(prf) -> 
	      Engine.getInferredAtoms (prf)
	      | None -> []
	      in
	      *)
		  List.map convertToPredicate _lst
		end
	    end

(*************************************************************************************)
(* Interface to Simplify.                                                            *)
(* Simplify is run as a server and queries are piped to it.                          *)
(* Thanks to Wes Weimer for the code.                                                *)
(*************************************************************************************)

(* Return a "\n" terminated string corresponding to simplify input *)
let clear_varHashtbl_flag = ref true (* the table will get cleared only if this flag is true *)
		    (* moreover -- when simplify_assume is called the flag gets set to false *)
		    (* when pop is called, the flag is again set to true *)

let varHashtbl = Hashtbl.create 53 
let next_var = ref 0 
let chlvalHashtbl = Hashtbl.create 53 

let unknown_var_counter = ref 0

let get_fresh_unknown () = 
  let rv = !unknown_var_counter in
    unknown_var_counter := !unknown_var_counter + 1;
    ("UNK"^(string_of_int rv))

(* Original *)

let convertToSimplifySyntax () exp =
(*Simplify has issues with variable names starting with "_", so we map all vars to v<num> *)
  (* M.msg_string M.Debug ("convertToSimplifySyntax exp = ");
     M.msg_printer M.Debug P.print exp; *)  (*RuGang*)
  let get_next_var () = next_var := !next_var + 1; !next_var in
  let get_current_var () = !next_var in
  if (!clear_varHashtbl_flag) then 
      (Hashtbl.clear varHashtbl;
       next_var :=0; 
       Hashtbl.clear chlvalHashtbl);
  let fieldId str =
    try Hashtbl.find varHashtbl str 
    with Not_found -> 
	let new_id = "v"^(string_of_int (get_next_var ())) in
	(Hashtbl.add varHashtbl str  new_id; new_id) in
  let id_of_var lv =
    match lv with
      E.Symbol s -> 
	  try
	    let vid = Hashtbl.find varHashtbl s in
	    int_of_string (Misc.chop_after_prefix "v" vid)
	  with Not_found -> failwith ("id_of_var: (Simplify) variable not found "^(s))
    | _ -> failwith ("id_of_var: (Simplify) lvalue not a symbol. "^(E.lvalToString lv)) in
  let id_of_fld s =
    let vid = 
      try Hashtbl.find varHashtbl s with Not_found -> failwith ("id_of_fld: (Simplify) variable not found "^(s)) in
    int_of_string (Misc.chop_after_prefix "v" vid) in
  let rec convertExp e =
    let chlval_convert_recursive (lv,s) = Printf.sprintf "(chlval %s %s)" (convertLval lv) (fieldId s) in  
    let chlval_convert_table e = 
      try (Hashtbl.find chlvalHashtbl e) 
      with Not_found ->
	      let s_e = get_next_var () (* E.toString e*) in
              let new_id = "chlval"^(string_of_int s_e) in
		(Hashtbl.add chlvalHashtbl e new_id; new_id) in
    match e with
      | E.Lval lv -> convertLval lv
      | E.Chlval (lv,s) -> if (O.getValueOfBool "clock") then chlval_convert_recursive (lv,s) else chlval_convert_table e
      | E.Binary (op, e1, e2) -> 
	      (match op with 
		  E.Minus -> Printf.sprintf "(- %s %s)" (convertExp e1) (convertExp e2)
                | E.Plus  -> Printf.sprintf "(+ %s %s)" (convertExp e1) (convertExp e2)
             (* |  E.Ordset_Succ ->  Printf.sprintf "(SUCC %s)" (convertExp e1)*)
		| E.Mul   -> Printf.sprintf "(* %s %s)" (convertExp e1) (convertExp e2)
                | E.Offset 
                | E.BitAnd 
		| E.BitOr 
		| E.Div 
		| E.Rem 
		| E.Xor 
		| E.Comma -> Printf.sprintf "(%s %s %s)" (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2) 
                | E.RShift -> (* second term / shifted value must be a constant *)
		    (match (e2) with E.Constant (Constant.Int _) | _ ->  
                      Printf.sprintf "(%s %s %s)" (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2))
                | E.LShift ->
		    (match e2 with E.Constant (Constant.Int k) -> Printf.sprintf "(* %s %d)" (convertExp e1) (Misc.power 2 k)
		     | _ -> Printf.sprintf "(%s %s %s)"
                     (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2))
                | E.FieldOffset -> (* the second argument to field offset is a field id *)
                    let fld = match e2 with E.Lval (E.Symbol s) -> s
		              | _ -> failwith ("convertExp (simp): field offset 2nd op not field "^(E.toString e)) in
	            Printf.sprintf "(foffset %s %s)" (convertExp  e1) (fieldId fld)
                |	_ -> if E.isRelOp op then raise AtomInExpressionException 
                             else failwith ("convertExp: (Simplify) not here:"^(E.binaryOpToVanillaString op)^" :  "^(E.toString e)))
    | E.Constant (c) -> 
	begin
	  match c with
	    Constant.Int i ->  if (i < 0) then "(- 0 "^(string_of_int (-i))^")" else string_of_int i 
	  | Constant.String s -> 
              let c = get_string_constant s in
              ("(_STRINGCONSTANT "^c^")")
	  | Constant.Float f -> 
	      if (f -. (ceil f) = 0.0) then string_of_int (int_of_float f)
	      else ("FLOATINGPT"^(string_of_int (get_next_var ())))
	  | _ -> failwith ("Cannot handle constant "^(Constant.toString c))
	end

    | E.Unary (uop, e1) -> 
	begin
	  match uop with 
	    E.UnaryMinus -> "(- 0 "^(convertExp e1)^")"
	  | E.UnaryPlus -> convertExp e1
	  | E.Address -> 
	      begin
		match e1 with
		  E.Lval lv -> 
		    let l = convertLval lv
		    in
		    let id = (id_of_var lv) in
		    Printf.sprintf "(addrOf %s %d)" l id
		| E.Chlval (lv, f) -> 
		    (* let lstr = (convertLval lv) in
		    let fstr = (fieldId f) in
		    let id = (id_of_fld f) in
		       *)
		    let chlv = convertExp e1 in
		    let id = int_of_string (Misc.chop_after_prefix "chlval" chlv) in
		    Printf.sprintf "(addrOf %s %d)" chlv id
		      
		| _ -> failwith ("convertExp: (Simplify) Address: strange expression "^(E.toString e))
	      end
	  | E.BitNot -> Printf.sprintf "(BitNot %s)" (convertExp e1)
	  |	_ -> "("^E.unaryOpToVanillaString uop^" "^(convertExp e1)^")"
	end
    | E.Select (e1,e2) -> Printf.sprintf "(select %s %s)" (convertExp e1) (convertExp e2)
    | E.Store (e1,e2,e3) -> Printf.sprintf "(store %s %s %s)" (convertExp e1) (convertExp e2) (convertExp e3) 
    | E.FunctionCall ((E.Lval (E.Symbol f) as flv), args) ->
        Printf.sprintf "(%s %s)" f (Misc.string_list_cat " " (List.map convertExp args)) (* TBD:RP *)
    | _ -> failwith ("convertExp (Simplify): " ^ (E.toString e) ^ "not handled")
	 
  and convertLval l =
    match l with
    | E.This -> failwith "This not handled in convertLval"
    | E.Symbol(str) -> 
	(try Hashtbl.find varHashtbl str with Not_found ->
	    let new_id = "v"^(string_of_int (get_next_var ())) in
	    (Hashtbl.add varHashtbl str new_id;new_id))
    | E.Dereference drfe  -> "(select "^(convertExp drfe)^" 0)"
    | E.Access(E.Dot, e, s) -> (Printf.sprintf "(select %s %s)" (convertExp e) (fieldId s))
    | E.Access(E.Arrow, e, s) ->
	let fieldId_s =
	  try Hashtbl.find varHashtbl s with Not_found -> 
	      (let new_id = "v"^(string_of_int (get_next_var ())) in
	       Hashtbl.add varHashtbl s new_id; new_id) in
	(Printf.sprintf "(select %s %s)" (convertExp e) fieldId_s)
	  (* JHALA: MAJOR BUG!! this should not be (+ e1 e2) surely *)
    | E.Indexed (e1, e2) -> Printf.sprintf "(select %s %s)" (convertExp e1) (convertExp e2)

  and convertAtom e =   
    match e with
      E.Binary (op, e1, e2) -> 
	begin
	  let opstring = 
	    match op with 
		E.Eq  -> "EQ"
	      |	E.Ne  -> "NEQ"
	      |	E.Gt  -> ">"
	      |	E.Lt  -> "<"
	      |	E.Ge  -> ">="
	      |	E.Le  -> "<="
	      |	_   -> M.msg_string M.Error ("Strange binop: "^(E.binaryOpToVanillaString op)); "" 
              (**** see below ** failwith ("convertAtom: Cannot handle this expression: "^(E.toString e)) *)
	  in
          if opstring <> "" 
	  then
	    begin
              try Printf.sprintf "(%s %s %s)" opstring (convertExp e1) (convertExp e2)
              with AtomInExpressionException ->
		begin
		  if (opstring = "EQ" or opstring = "NEQ") then
		    begin
                      match e1 with
			E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
			  begin
			    let s1 = convertExp e1 in
			    let s2 = convertAtom e2 in
			    Printf.sprintf "(OR (AND %s  (%s %s  1)) (AND (NOT %s) (%s %s 0)))" s2 opstring s1 s2 opstring s1 
			  end
                      | _ -> 
			  begin
			    match e2 with
			      E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
				begin
				  let s1 = convertExp e2 in
				  let s2 = convertAtom e1 in
				  Printf.sprintf "(OR (AND %s  (%s %s  1)) (AND (NOT %s) (%s %s 0)))" s2 opstring s1 s2 opstring s1 
				end
			    | _ -> 
				Printf.sprintf "(EQ %s 0)" (get_fresh_unknown ()) 
			  end 
		    end
		  else
		    Printf.sprintf "(EQ %s 0)" (get_fresh_unknown () )
		end
            end
	  else (M.msg_string M.Debug ("failed opthing (simplify): "^(E.toString e));
                Printf.sprintf "(EQ %s 0)" (get_fresh_unknown ()) )
	end
    | E.FunctionCall (E.Lval (E.Symbol f), args) ->
        Printf.sprintf "(EQ (%s %s) 0)" f (Misc.string_list_cat " " (List.map convertExp args))
    | E.Select (e1,e2) -> Printf.sprintf "(EQ (select %s %s) 0)" (convertExp e1) (convertExp e2)
    | E.Store (e1,e2,e3) -> Printf.sprintf "(EQ (store %s %s %s) 0)" (convertExp e1) (convertExp e2) (convertExp e3) 
    | E.Lval (E.Symbol s) when Misc.is_prefix "b_p__BS" s -> s
    | _ -> (M.msg_string M.Debug "failed opthing2 (simplify) ";M.msg_string M.Debug (E.toString e); 
            (* convertExp e *) Printf.sprintf "(EQ %s 0)" (get_fresh_unknown () ) )
	(*** instead i shall simply call convert expression on it *** failwith ("convertAtom: "^(E.toString e)^ " Not a binary relation operator") *)
  
  and convertPred e =
    match e with
      P.True -> "TRUE"
    | P.False -> "FALSE"
    | P.And plist -> 
	List.fold_left (function a -> function b -> "(AND "^(convertPred b)^" "^a^")") 
	  "TRUE" (plist)
    | P.Or plist -> 
	List.fold_left (function a -> function b -> "(OR "^(convertPred b)^" "^a^")") 
	  "FALSE" (plist)
    | P.Implies (e1, e2) -> "(IMPLIES "^(convertPred e1)^" "^(convertPred e2)^")"
    | P.Iff (e1,e2) -> Printf.sprintf "(IFF %s %s)" (convertPred e1) (convertPred e2)
    | P.Not e1 -> "(NOT "^(convertPred e1)^")"
    | P.Atom e ->  (convertAtom e) 
    | P.All (s,e1) -> Printf.sprintf "(FORALL (%s) %s)" (convertLval (E.Symbol s)) (convertPred e1)
    | _ -> failwith ("Simplify: Cannot handle this predicate "^(P.toString e))
  in
  (convertPred (P.constantfold exp (Hashtbl.create 1)))^"\n"


let bits =  ref 4


let assertBitVector s =
  let rec assertBitV n s = 
    if (n = 0) then ""
    else 
      begin
	let f = Printf.sprintf "(OR (EQ %s_%d 0) (EQ %s_%d 1)) " s n s n in
	  (f ^ (assertBitV (n-1) s))
      end in
    begin
      ("(BG_PUSH (AND "^(assertBitV !bits s)^"))")
    end
      
let shift_R res a b_num =
  let fu = ref "" in
    for i = 0 to (b_num-1) do
      fu := !fu ^ (Printf.sprintf "(EQ %s_%d 0) " res (!bits - i))
    done;
    
    for i = b_num to (!bits-1) do
      fu := !fu ^ (Printf.sprintf "(EQ %s_%d %s_%d) " res (!bits - i) a (!bits - i + b_num ))
    done;
    !fu

let rShiftVar res a b = 
  let everything = ref "(AND " in
    for i = 0 to !bits do
      everything := !everything ^ (Printf.sprintf "(IFF (EQ %s %d) (AND %s)) " 
				     b i (shift_R res a i))
    done;
    !everything ^ (Printf.sprintf "(IFF (> %s %d) (AND %s)))" 
		     b !bits (shift_R res a !bits))
      
(* let () = print_string ("(BG_PUSH "^(rShiftVar "d" "a" "b")^")\n") *)

let rShiftCons res a b_cons =
  (Printf.sprintf "(AND %s) " (shift_R res a b_cons))

let condAnd res a b = 
  let foo = ref "(AND " in
    foo := !foo ^ (Printf.sprintf "(EQ %s 1) " res);
    foo := !foo ^ (Printf.sprintf "(IFF (EQ %s 0) (OR " res);
    foo := !foo ^ "(AND ";
    for i = 1 to !bits do
      foo := !foo ^ (Printf.sprintf "(EQ %s_%d 0) " a i)
    done;
    foo := !foo ^ ") ";
    foo := !foo ^ "(AND ";
    for i = 1 to !bits do
      foo := !foo ^ (Printf.sprintf "(EQ %s_%d 0) " b i)
    done;
    foo := !foo ^ ")))) ";
    !foo


let rec convToBin x =
  if (x = 0) then []
  else (x mod 2)::(convToBin (x/2))
    

(* let () = print_string ("(BG_PUSH "^(condAnd "d" "a" "b") ^ ")\n") *)

let rec bitEqBlist n a b  =
  match b with
      [] -> ""
    | numb::rest -> ((Printf.sprintf "(EQ %s_%d %d) " a n numb) ^ 
	(bitEqBlist (n-1) a rest))

let rec bitNEqBlist n a b  =
  match b with
      [] -> ""
    | numb::rest -> ((Printf.sprintf "(NEQ %s_%d %d) " a n numb) ^ 
	(bitNEqBlist (n-1) a rest))


let bitEqInt a b = 
  let blist = convToBin b in
  let foo = ref "(AND " in
  let i = List.length blist in
    for j = !bits downto (i+1) do
      foo := !foo ^ (Printf.sprintf "(EQ %s_%d 0) " a j)
    done;
    foo := !foo ^ (bitEqBlist i a (List.rev blist));
    !foo ^ ")"


let bitNEqInt a b = 
  let blist = convToBin b in
  let foo = ref "(OR " in
  let i = List.length blist in
    for j = !bits downto (i+1) do
      foo := !foo ^ (Printf.sprintf "(NEQ %s_%d 0) " a j)
    done;
    foo := !foo ^ (bitNEqBlist i a (List.rev blist));
    !foo ^ ")"

    (*let () = print_string ("(BG_PUSH "^(bitEqInt "a" 10) ^ ")\n") *)

let bitEqS a b =
  let foo = ref "(AND " in
  for i = 1 to !bits do
    foo := !foo ^ (Printf.sprintf "(EQ %s_%d %s_%d) " a i b i)
  done;
    !foo ^ ")"
      
let bitNEqS a b =
  let foo = ref "(OR " in
  for i = 1 to !bits do
    foo := !foo ^ (Printf.sprintf "(NEQ %s_%d %s_%d) " a i b i)
  done;
    !foo ^ ")"

let fixed_simplify_axioms = ref 
	    [
(*
             "(DEFPRED (BitAnd x y))\n" ;
	     "(DEFPRED (BitOr x y))\n" ;
	     "(DEFPRED (BitNot x))\n" ;
	     "(DEFPRED (Xor x y))\n" ;
	     "(DEFPRED (addrOf x y))\n" ;
	     (* "(DEFPRED (select y x))\n" ; *)
	     "(DEFPRED (foffset x f))\n" ;
	     "(DEFPRED (RShift x y))\n" ;
	     "(DEFPRED (LShift x y))\n" ;
	     "(DEFPRED (Div x y))\n" ;
	     "(DEFPRED (Rem x y))\n" ;
*)
  (*
  "(BG_PUSH (FORALL (x y z) (IMPLIES (AND (NEQ y 0) (EQ z (BitOr x y))) (NEQ (BitAnd z y) 0))))\n" ;
*)
  "(BG_PUSH (FORALL (x y) (EQ (select (addrOf x y) 0) x)))" ;
  "(BG_PUSH (FORALL (x y d1) (IMPLIES (EQ (foffset x d1) (foffset y d1)) (EQ  x y))))\n" ;
  "(BG_PUSH (FORALL (x y) (NEQ (addrOf x y) 0)))" ;
  "(BG_PUSH (FORALL (x y) (EQ (* (Div x y) y) x )) ) " ;
  "(BG_PUSH (FORALL (x) (NEQ (_STRINGCONSTANT x) 0 ))) " ;

  "(BG_PUSH (FORALL (s t x ) (IMPLIES (EQ s (add t x)) (EQ (in s x) 1))))";
  "(BG_PUSH (FORALL (s x) (IMPLIES (EQ s (emptyset )) (NEQ (in s x) 1))))";
(*
  "(BG_PUSH (FORALL (x y z) (IMPLIES (EQ z (addrOf x)) (EQ (select z 0) x))))\n" ;
  "(BG_PUSH (FORALL (x) (EQ (addrOf (select x 0)) x)))" ;
  "(BG_PUSH (FORALL (x y d1 d2) (IMPLIES (EQ (addrOf (select x d1)) (addrOf (select y d1))) (EQ  (addrOf x) (addrOf y)))))\n"
*)
	]



let simplify_stack_counter = ref 0
let current_simplify_stack = ref [] (* a string list *)
let simplifyServer = ref None
let simplifyServerCount = ref 0 
let simplifyCacheCount = ref 0 

let simplify_query_flag = ref false (* true *)

let set_simplify_query_flag () = 
  simplify_query_flag := false (* true *)


let rec addAxioms axiom_list =
  M.msg_string M.Minor "(Adding axioms)" ;
  let oc  = match (!simplifyServer) with
      None -> snd (getServer ()) 
    | Some(_,oc1,_) -> oc1
  in
    try
      List.iter 
	(function x -> (M.msg_string M.Debug (x^"\n"); 
			secure_output_string oc (x^"\n")))
	axiom_list ;
      if (!simplify_query_flag) then
	List.iter 
	  (function x -> ( 
	     Misc.append_to_file "simp_query" (x^"\n")))
	  axiom_list ;
      ()
    with ChannelException -> failwith "Channel Exception in addAxioms!"

  and  getServer () =
  bits := O.getValueOfInt "bits";
    match !simplifyServer with
       None -> begin
	  M.msg_string M.Normal "Forking Simplify process..." ;
	  let ic,oc,ec = Unix.open_process_full "Simplify -nosc" 
			   (Unix.environment ()) in
	    simplifyServer := Some(ic,oc,ec) ;
    (* add axioms to simplify  -- assume axioms are in $BLASTHOME/bin/axioms.in *)
	    addAxioms (!fixed_simplify_axioms);
	    M.msg_string M.Normal "done!\n";
	    flush stdout ;
	    at_exit (fun () -> 
		       ignore ((*kill_simplify_server () *)Unix.close_process_full (ic,oc,ec)  ) ;  ) ;
	    (ic,oc)
	end
      | Some(a,b,c) -> (a,b)
  and  kill_simplify_server () = 
  match !simplifyServer with
      None -> ()
    | Some (ic,oc,ec) -> 
	begin
	  try
	    M.msg_string M.Error "Killing simplify server...";
	    (* ignore (Unix.close_process_full (ic,oc,ec) ); garbage process ... how to collect *)
	    ignore(Sys.command "killall Simplify");
	    simplifyServer := None;
	    M.msg_string M.Error "Done killing ..." ;
	    M.msg_string M.Normal "Done killing ..."
	  with _ -> failwith " failed to kill simplify !"
	end

(* RJ: is this maintained ? *)
let convertToSimplifyBitSyntax () exp =
  (*Simplify has issues with variable names starting with "_", so we map all vars to v<num> *)
  
  (*M.msg_string M.Debug ("convertToSimplifySyntax exp = ");
    M.msg_printer M.Debug P.print exp;*)  (*RuGang*)
  
  let  isAtom e =   
    match e with
	E.Binary (op, e1, e2) -> 
	  begin
	    match op with 
		E.Eq  | E.Ne | E.Gt 
	      | E.Lt | E.Ge  | E.Le 
		  -> true
	      | _ -> false
	  end
      | _ -> false in

  let get_next_var () = next_var := !next_var + 1; !next_var in
  let get_current_var () = !next_var in
    if (!clear_varHashtbl_flag) then 
      begin
	Hashtbl.clear varHashtbl;next_var :=0; 
	Hashtbl.clear chlvalHashtbl
      end ;
    let fieldId str =
      if Hashtbl.mem varHashtbl str then
	Hashtbl.find varHashtbl str
      else
	begin
	  let new_id = "v"^(string_of_int (get_next_var ())) in
	    Hashtbl.add varHashtbl str  new_id;
	    new_id
	end
    in
    let id_of_var lv =
      match lv with
	  E.Symbol s -> 
	    begin
	      try
		let vid = Hashtbl.find varHashtbl s in
		  int_of_string (Misc.chop_after_prefix "v" vid)
	      with 
		  Not_found -> failwith ("id_of_var: (Simplify) variable not found "^(s))
	    end
	      
	| _ -> failwith ("id_of_var: (Simplify) lvalue not a symbol. "^(E.lvalToString lv))
    in
    let id_of_fld s =
      
      let vid = try Hashtbl.find varHashtbl s 
      with Not_found -> failwith ("id_of_fld: (Simplify) variable not found "^(s))	
      in
	int_of_string (Misc.chop_after_prefix "v" vid)
	  
    in
      
    let newVar str = 
      if Hashtbl.mem varHashtbl str then
	(Hashtbl.find varHashtbl str)
      else
	begin
	  let new_id = "v"^(string_of_int (get_next_var ())) in
	  let (_, oc) = getServer () in
	    Hashtbl.add varHashtbl str new_id;
	    (*M.msg_string M.Debug ("NOT in hashtbl:"^str^":"^new_id);*)
	    let s =  ((assertBitVector new_id)^"\n") in
	      M.msg_string M.Debug ("new bitvector: "^s);
	      let _ = current_simplify_stack := s :: !current_simplify_stack in
	      let _ = simplify_stack_counter := !simplify_stack_counter + 1 in
		output_string oc s;
		new_id
	end in

    let rec  convertExp e =
      let chlval_convert_recursive (lv,s) = Printf.sprintf "(chlval %s %s)" (convertLval lv) (fieldId s) in  
      let chlval_convert_table e = 
	try (Hashtbl.find chlvalHashtbl e) 
	with 
	    Not_found ->
	      begin
		let s_e = get_next_var () (* E.toString e*) in
		let new_id = "chlval"^(string_of_int s_e) in
		  Hashtbl.add chlvalHashtbl e new_id;
		  new_id
	      end
      in
	match e with
	  | E.Lval lv -> convertLval lv
	  | E.Chlval (lv,s)  -> 
	      chlval_convert_table e
		
	  | E.Constant (c) -> 
	      begin
		match c with
		    Constant.Int i ->  if (i < 0) then "(- 0 "^(string_of_int (-i))^")" else string_of_int i 
		  | _ -> failwith ("Cannot handle constant "^(Constant.toString c))
	      end
		
	  | E.Unary (uop, e1) -> 
	      begin
		match uop with 
		    E.UnaryMinus -> "(- 0 "^(convertExp e1)^")"
		  | E.UnaryPlus -> convertExp e1
		  | E.Address -> failwith ("unimplemented unary")
		  | E.BitNot -> failwith ("unimplemented unary")
		  |	_ -> failwith ("unimplemented unary")
	      end
          | E.Select (e1,e2) -> Printf.sprintf "(select %s %s)" (convertExp e1) (convertExp e2)
          | E.Store (e1,e2,e3) -> Printf.sprintf "(store %s %s %s)" (convertExp e1) (convertExp e2) (convertExp e3) 
          | E.FunctionCall ((E.Lval (E.Symbol f) as flv), args) ->
               Printf.sprintf "(%s %s)" f (Misc.string_list_cat " " (List.map convertExp args)) (* TBD:RP *)
	  | _ -> failwith ("convertExp (Simplify): " ^ (E.toString e) ^ "not handled")
	      
    and convertLval l =
      match l with
	| E.Symbol(str) -> 
	    newVar str
	| _ ->failwith ("unimplemented lval")  
	    
    and convertAtom e =   
    match e with
	E.Binary (E.Eq, e1, e2 ) -> 
	  begin
	    match e1 with
		E.Lval _ | E.Chlval _ ->
		  let s1 = convertExp e1 in
		    begin 
		      match e2 with
			  E.Binary _ ->
			    let newRes = newVar "result" in
			      Printf.sprintf "(AND %s %s)"  (bitEqS s1 newRes) (convertExpRes newRes e2)		  
			| E.Lval _
			| E.Chlval _ -> (bitEqS s1 (convertExp e2))
			| E.Constant (Constant.Int i) -> 
			    (bitEqInt s1 i)
			| _ -> failwith ("not implemented "^(E.toString e2))
		    end
	      | E.Constant (Constant.Int j) ->
		  begin 
		    match e2 with
			E.Binary _ ->
			  let newRes = newVar "result" in
			    Printf.sprintf "(AND %s %s)"  (bitEqInt newRes j) (convertExpRes newRes e2)		  
		      | E.Lval _
		      | E.Chlval _ -> Printf.sprintf "%s"  (bitEqInt (convertExp e2) j)
		      | E.Constant (Constant.Int i) -> 
			  if (i = j) then "TRUE" else "FALSE"
		      | _ -> failwith ("not implemented "^(E.toString e2))
		  end
	      | E.Binary _ -> 
		  begin
		    match e2 with
			E.Binary _ -> failwith ("not implemented "^(E.toString e2))
		      | _ -> convertAtom (E.Binary (E.Eq, e2, e1))
		  end
	      | _ ->  failwith ("not implemented")
	  end
      | E.Binary (E.Ne, e1,  e2) -> 
	  begin
	    match e1 with
		E.Lval _ | E.Chlval _ ->
		  let s1 = convertExp e1 in
		    begin 
		      match e2 with
			  E.Binary _ ->
			    let newRes = newVar "result" in
			      Printf.sprintf "(AND %s %s)"  
				(bitNEqS s1 newRes) (convertExpRes newRes e2) 
			| E.Lval _
			| E.Chlval _ -> (bitNEqS s1 (convertExp e2))
			| E.Constant (Constant.Int i) -> 
			    (bitNEqInt s1 i)
			| _ -> failwith ("not implemented "^(E.toString e2))
		    end
	      | E.Constant (Constant.Int j) ->
		    begin 
		      match e2 with
			  E.Binary _ ->
			    let newRes = newVar "result" in
			      Printf.sprintf "(AND %s %s)"  (bitNEqInt newRes j) (convertExpRes newRes e2)
			| E.Lval _
			| E.Chlval _ -> Printf.sprintf "%s"  (bitNEqInt (convertExp e2) j)
			| E.Constant (Constant.Int i) -> 
			    if (j = i) then "TRUE" else "FALSE" 
			| _ -> failwith ("not implemented "^(E.toString e2))
		    end
	      | E.Binary _ -> 
		  begin
		    match e2 with
			E.Binary _ -> failwith ("not implemented "^(E.toString e2))
		      | _ -> convertAtom (E.Binary (E.Ne, e2, e1))
		  end
	      | _ ->  failwith ("not implemented "^(E.toString e1))
	  end
      | _ -> failwith ("not implemented "^(E.toString e))
  
	  
    and convertExpRes res e = 
      match e with 
      | E.FunctionCall (E.Lval (E.Symbol f), args) ->
          Printf.sprintf "(EQ (%s %s) 0)" f (Misc.string_list_cat " " (List.map convertExp args))
      |	E.Binary (op, e1, e2) -> 
	    begin
	      match op with 
		| E.RShift -> 
		    begin
		      match e2 with
			E.Constant (Constant.Int i) ->
			  begin 
			    match e1 with
			      E.Binary _ ->
				let newRes = newVar "result" in
				let s = convertExpRes newRes e1 in
				(Printf.sprintf "(AND %s %s)" (rShiftCons res newRes i) s)
				| _ -> (* e1 is just a lval? *)
				    let s = convertExp e1 in
				    (Printf.sprintf "%s" (rShiftCons res s i))
			  end
		      | E.Lval _ | E.Chlval _  ->
			    let s2 = convertExp e2 in
			    begin 
			      match e1 with
				E.Binary _ ->
				  let newRes = newVar "result" in
				  let s = convertExpRes newRes e1 in
				  (Printf.sprintf "(AND %s %s)" (rShiftVar res newRes s2) s)
			      | _ -> (* e1 is just a lval? *)
				  let s = convertExp e1 in
				  (Printf.sprintf "%s" (rShiftVar res s s2))
			    end
			| _ -> failwith ("not implemented "^(E.toString e2))
		    end
		| E.Minus 
		| E.Plus   
		| E.Mul   
		| E.BitAnd 
		| E.BitOr 
		| E.Div 
		| E.Rem 
		| E.Xor 
		| E.FieldOffset 
		| E.Comma -> failwith ("not implemented")
		| _ -> 
		    if E.isRelOp op then raise AtomInExpressionException
		    else failwith ("convertExp: (Simplify) invalid op "^(E.binaryOpToVanillaString op)^" in "^(E.toString e))
	    end
     | _ -> failwith "match failure : convertExpRes"

    and convertPred e =
      match e with
	  P.True -> "TRUE"
	| P.False -> "FALSE"
	| P.And plist -> 
	    List.fold_left (function a -> function b -> "(AND "^(convertPred b)^" "^a^")") 
	      "TRUE" (plist)
	| P.Or plist -> 
	    List.fold_left (function a -> function b -> "(OR "^(convertPred b)^" "^a^")") 
	      "FALSE" (plist)
	| P.Implies (e1, e2) -> "(IMPLIES "^(convertPred e1)^" "^(convertPred e2)^")"
        | P.Iff (e1,e2) -> Printf.sprintf "(IFF %s %s)" (convertPred e1) (convertPred e2)
        | P.Not e1 -> "(NOT "^(convertPred e1)^")"
	| P.Atom e ->  (convertAtom e)
        | P.All (s,e1) -> "(FORALL s "^(convertPred e1)^")" 
	| _ -> failwith ("SimplifyBit: Cannot handle this predicate "^(P.toString e))
    in
      (convertPred (P.constantfold exp (Hashtbl.create 1)))^"\n"


(*
let simplify_line_tap = ref [] (* string list *)
*)

let dump_line_tap () = ()
(*
  let oc = open_out "line_tap" in
    Marshal.to_channel oc ( List.rev !simplify_line_tap) []; 
    close_out oc;
    M.msg_string M.Error "Done writing line_tap";
    failwith "simplify choked!"
 *) 






let restart_simplify () = 
  M.msg_string M.Error "restarting simplify ...";
  M.msg_string M.Normal "restarting simplify ...";
  kill_simplify_server ();
  let (ic,oc) = getServer () in
    try
      List.iter (secure_output_string oc) (List.rev !current_simplify_stack)
    with ChannelException -> failwith "Simplify fails on restart!"


let cvc_push () =
  cvcPushes:=!cvcPushes+1;
  let (_,oc) = getCvcServer () in
    output_string oc "PUSH;\n";
    output_string filed "PUSH;\n";
    flush oc;
    flush filed
      
      
let cvc_assume pred = 
  M.msg_string M.Debug ("cvc_assume "^(P.toString pred));
  clear_varCvcHashtbl_flag := false;
  let cvcInput = convertToCvcSyntax pred;
  in 
    M.msg_string M.Debug ("cvc Assuming: "^cvcInput);
    flush stdout;
    flush stderr;
    let s = ("PUSH; ASSERT ("^cvcInput^");\n") in (* add PUSH? *)
    cvcPushes:=!cvcPushes+1; 
      let (_,oc) = getCvcServer () in
	cvc_output_list  oc !cvc_ground_term_list; 
	M.msg_string M.Debug ("cvc input: "^s); 
	output_string oc s;
	output_string filed s;
	flush oc;
	flush filed;
	reset_cvc_ground ();
	M.msg_string M.Debug "Done Assume"; 
	flush stdout
   
let simplify_assume () pred = 
(* deadchange *)
  (* M.msg_string M.Debug ("simplify_assume");
     M.msg_string M.Debug ("sstk:"^(string_of_int
     !simplify_stack_counter));*)
  let simplifyInput = 
    if (!tp = "simplify-bits") then
      let simplifyBitInput = Bstats.time "convertToSimplifySyntax" (convertToSimplifyBitSyntax ()) pred in
      let simplifyOldInput = (convertToSimplifySyntax ()) pred in
	(Printf.sprintf "(AND %s %s)" simplifyBitInput simplifyOldInput) 
    else  Bstats.time "convertToSimplifySyntax" (convertToSimplifySyntax ()) pred
  in
    clear_varHashtbl_flag := false;
    (* M.msg_string M.Debug ("Simplify Assuming: "^simplifyInput);*)
    flush stdout;
    flush stderr;
    let s = ("(BG_PUSH "^simplifyInput^" )\n") in
    let _ = current_simplify_stack := s :: !current_simplify_stack in
    let _ = simplify_stack_counter := !simplify_stack_counter + 1 in
    let (_,oc) = getServer () in
    try
      secure_output_string oc s ;
      flush oc ;     
      (* M.msg_string M.Debug "Done Assume"; *)
      flush stdout;
      if (!simplify_query_flag) then Misc.append_to_file "simp_query" ("\n"^s^"\n")
    with  ChannelException ->
      begin
	M.msg_string M.Error "ChannelException: in simp_assume";
	restart_simplify ()
      end
	
let cvc_pop () = 
   M.msg_string M.Debug ("cvc pop");
   M.msg_string M.Debug ("sstk:"^(string_of_int !cvcPushes));
   let (_,oc) = getCvcServer () in
   if (!cvcPushes > 0) then 
   begin
   cvcPushes:=!cvcPushes-1;
   output_string oc "POP;\n";
   output_string filed "POP;\n";
   flush filed;
   flush oc
   end else 
   M.msg_string M.Debug ("ERROR: cvc pop trying to pop nothing!")
   

let simplify_pop () = 
(* M.msg_string M.Debug ("simplify_pop"); 
   M.msg_string M.Debug ("sstk:"^(string_of_int !simplify_stack_counter));*)
  let _ = 
    if (!simplify_stack_counter = 0) then failwith "bad simplify stack!"
    else
      simplify_stack_counter := !simplify_stack_counter - 1 
  in
  let top_of_stack = List.hd !current_simplify_stack in
  let _ = current_simplify_stack := List.tl !current_simplify_stack in
    (* M.msg_string M.Debug ("Simplify Popping"); *)
    let (_,oc) = getServer () in
    let s = "(BG_POP)\n" in
    clear_varHashtbl_flag := true;
    try
      secure_output_string oc s;
      (* M.msg_string M.Debug "Done pop";*)
      if (!simplify_query_flag) then Misc.append_to_file "simp_query" ("\n"^s^"\n");
      flush oc
    with ChannelException ->
      begin
	M.msg_string M.Error "ChannelException: in simp_pop";
	restart_simplify ()
      end

(* doesnt clear the varHashtbl *)
let simplify_pop_noflush () =  
   let _ = 
    if (!simplify_stack_counter = 0) then failwith "bad simplify stack_nf!"
    else
      simplify_stack_counter := !simplify_stack_counter - 1 
  in
  let (_,oc) = getServer () in
  let s = "(BG_POP)\n" in
  let _ = current_simplify_stack := List.tl !current_simplify_stack in
    try
      secure_output_string oc s;
      if (!simplify_query_flag) then Misc.append_to_file "simp_query" s;
 flush oc
 with ChannelException ->
   begin
     M.msg_string M.Error "ChannelException: in simp_pop_pnf";
     restart_simplify ()
   end
 


let rec isValid ic = 
(* deadchange *)
  let  _ =  M.msg_string M.Debug ("Simplify : in isValid") in
  let  _ = flush stdout in
  (* let (_,oc) = getServer () in
  let _ = flush oc in*)
  let line = secure_input_line ic in (* any failure here will be caught further up *)
  let _ = M.msg_string M.Debug ("Simplify says: "^line) in
  let _ = 
    if Misc.is_substring line "Bad input" then 
      begin
	M.msg_string M.Error "Simplify poisoned!";
	exit 1
      end
  in
  let result = if String.contains line 'V' then 
    true
  else if String.contains line 'I' then
    false
  else isValid ic
  in
    result
      
let ques_flag = ref false (* to see if simplify is failing forever *)

let rec queryExpUsingSimplify pred = 
  (* M.msg_string M.Debug ("querying exp:");
  M.msg_printer M.Debug P.print pred ;*)
  let simplifyInput =
    if (!tp = "simplify-bits") then
      let simplifyBitInput = Bstats.time "convertToSimplifySyntax" (convertToSimplifyBitSyntax ()) pred in
      let simplifyOldInput = (convertToSimplifySyntax ()) pred in
	(Printf.sprintf "(AND %s %s)" simplifyBitInput simplifyOldInput) 
    else  Bstats.time "convertToSimplifySyntax" (convertToSimplifySyntax ()) pred
  in 
    M.msg_string M.Debug ("Simplify: "^simplifyInput);
    flush stdout;
    flush stderr;
    let result = 
    try
      let ic,oc = getServer () in
	secure_output_string oc (simplifyInput) ;
	if (!simplify_query_flag) 
	then Misc.append_to_file "simp_query" ("\n"^simplifyInput^"\n");
        flush oc ; 
        isValid ic
   with ChannelException ->
     begin
       if (!ques_flag) then failwith "Simplify fails again!";
       ques_flag := true;
       restart_simplify ();
       queryExpUsingSimplify pred
     end
   | e ->
       failwith ("Simplify raised exception "^(Printexc.to_string e)^" for "^(P.toString pred)^". Check that Simplify is in your path.")
 in
  M.msg_string M.Debug (if result then "true" else "false");
  ques_flag := false;
  result



	       
let getProofsFromSimplify exp =
  let _ = M.msg_string M.Debug("querying exp in getProofsFromSimplify :"^(P.toString exp)^"\n") in
   (* first we have to convert the formula and collect all the ground terms that appear in it *)
  let formulas = 
    match exp with 
      P.And l -> l 
    | _ -> failwith ("bad call to getProofsFromSimplify -- we only handle listd of conjunections") in
  let ic,oc = getServer () in
  let counter = ref 0 in
  let output_asserts oc lst =
    List.iter (fun pred -> simplify_assume () pred; incr counter) lst
  in
  let rec usefulPredicates usefulPreds  =
    try
      let rec _worker assertlist =
	match assertlist with 
	  [] -> 
	    begin
	      M.msg_string M.Error "Exception : Formula is not valid! Simplify cannot prove validity." ;
	      M.msg_string M.Debug "Exception : Formula is not valid! Simplify cannot prove validity." ;
	      M.msg_string M.Debug "\nThe formula was :" ;
	      M.msg_string M.Debug (P.toString exp) ;
	      
	      raise Not_found
	    end
	| a :: rest ->
	    simplify_assume () a ;
            incr counter ;
	    secure_output_string oc "FALSE\n" ;
            (* flush oc ;*)
	    if (!simplify_query_flag) then Misc.append_to_file "simp_query" 
("\n FALSE\n");
	    let result = 
	      try
   		isValid ic
	      with e ->
		failwith ("Simplify raised exception "^(Printexc.to_string e)^" for "^(P.toString exp))
          in
            if result then a
            else
              _worker rest 
      in
      begin
        M.msg_string M.Debug "In useFulPredicates, List is " ;
	List.iter (fun p -> M.msg_string M.Debug (P.toString p)) usefulPreds ;
        M.msg_string M.Debug "end List. " ;
	
        output_asserts oc usefulPreds ;
        secure_output_string oc "FALSE\n" ;
        (* flush oc ;*) 
	if (!simplify_query_flag) then Misc.append_to_file "simp_query" "\n FALSE \n";
	
        M.msg_string M.Debug "Asserted useful predicates. " ;
	
        let result = try isValid ic with e -> failwith "Simplify raised exception -- in getUseful" in
        if result then 
	  begin
            while !counter > 0 do simplify_pop () ; decr counter done ;
            usefulPreds 
          end
        else 
          begin 
           (* go on adding predicates from assertlist and check for validity.
	      When valid, add the last formula to usefulPreds and repeat
              *)
            let nextpred = _worker formulas  in
            while !counter > 0 do
              simplify_pop () ; decr counter 
            done ;
            usefulPredicates (nextpred ::  usefulPreds) 
          end
      end
    with
      Not_found -> begin (* failwith "simplify" ;*) while !counter >0 do simplify_pop (); decr counter done; formulas end
  in
  let listOfPreds = usefulPredicates [] 
  in
  (* iterate simplify pop to get rid of assumptions *)
  List.map 
    (fun a -> match a with P.Not b -> b | _ -> a)
    listOfPreds

      
(* get the hash table of theorems. For those theorems that are true,
   get proofs from Vampyre and put in another hash table. Finally Marshal
   the hashtable and write out to disk.
*)  
let generateProofsSimple htbl =
  let pfHt = Hashtbl.create 511 in
  let genPf thm ans =
    if (ans = true) then
      let pf = 
	try
	  let vampyreInput = convertToVampyreSyntax thm
	  in
	  try
	    vampyreProver "foo" (Input.pushNeg vampyreInput)
	  with Engine.Failure ->((M.msg_string M.Error "Bug in Vampyre.\n") ; failwith "Vampyre bug in proofgen")
	with
	  VampyreErrormsg.Error -> (failwith ("Bug in Vampyre ...Proof gen"))
      in
      Hashtbl.add pfHt thm pf
  in
  Hashtbl.iter genPf htbl ;
  let pfString = Marshal.to_string pfHt [] in
  M.msg_string M.Error ("Proof size is "^(string_of_int (String.length pfString)))


(***********************************************************************************)
(* Jhala: Adding the proof functions ... *)
(***********************************************************************************)


 let queryExpUsingVampyre_proof exp =
   M.msg_string M.Debug "in queryExpUsingVampyre_proof";
   let correct_ans = queryExpUsingSimplify exp in
   let vampyreInput = convertToVampyreSyntax exp in
   let vampyreInput_pushneg = (Input.pushNeg  vampyreInput) in  
     try
       let proof = vampyreProver "foo" (vampyreInput_pushneg)
       in
	 if (not correct_ans) then 
	   begin
	     M.msg_string M.Debug ("PROOF ERROR: TP MISMATCH + !: "^(P.toString exp)); 
	     (*VampyrePretty.printf "Vampyre Saw: %a \n" Input.dp vampyreInput_pushneg;
	     VampyrePretty.printf "Vampyre Proof: %a \n" Proof.dp proof;*)
	     None
	end
      else Some(proof)
    with Engine.Failure -> 
      begin
	(M.msg_string M.Debug "Proof failed.\n") ; if (correct_ans) then 
	  begin
	    M.msg_string M.Debug ("PROOF ERROR: TP MISMATCH - !: "^(P.toString exp)); 
            (* VampyrePretty.printf "Vampyre Saw: %a \n" Input.dp vampyreInput_pushneg; *)
	    Some (Proof.Unimplemented)
	  end
	else None
      end
      | VampyreErrormsg.Error -> 
	  begin
	    M.msg_string M.Debug ("PROOF ERROR: BUG IN VAMPYRE !: "^(P.toString exp)); 
	    ignore(VampyrePretty.printf "Vampyre Saw: %a \n" Input.dp vampyreInput_pushneg);
	    let backup_ans = queryExpUsingSimplify exp in
	      if backup_ans then Some (Proof.Unimplemented) else None
	  end
 (* (failwith ("Bug in Vampyre ...")) *)

let queryExp_proof exp = 
  M.msg_string M.Debug ("Query _proof : "^(P.toString exp)); 
  queryExpUsingVampyre_proof exp
  
let queryExpContext_proof query = 
  let vam_query = Input.pushNeg (convertToVampyreSyntax query) in
    vampyre_prove_context_proof vam_query
 
(****************************************************************************)
(************************  interface to UCLID *******************************)
(****************************************************************************)
let uclid_ground_term_list = ref [] 

let add_uclid_ground str = 
  if List.mem str (!uclid_ground_term_list) 
  then () 
  else uclid_ground_term_list := str::(!uclid_ground_term_list)
    
let reset_uclid_ground () = 
  uclid_ground_term_list := 
  ["zero : TERM;";
   "select : FUNC[2];"; 
   "select_dot : FUNC[2];";
   "guf : FUNC[2];";
   "gur : PRED[2];";
   "guf_plus : FUNC[2];";
   "guf_minus : FUNC[2];";
  ]
    
let convertToUclidSyntax exp =
(*Simplify has issues with variable names starting with "_", so we map all vars to v<num> *)
  let get_next_var () = next_var := !next_var + 1; !next_var in
  let _ = if (false) then (Hashtbl.clear varHashtbl;next_var :=0) in
  let rec convertExp e =
    match e with
	E.Binary (op, e1, e2) -> 
	  begin
	    match op with 
		E.Minus -> 
		  begin
		    match e2 with
	                E.Constant (Constant.Int 0) -> (convertExp e1)
		      |	E.Constant (Constant.Int i) -> 
			  if (i > 0) then Printf.sprintf "(pred^%s(%s))" (string_of_int i) (convertExp e1)
			  else Printf.sprintf "(succ^%s(%s))" (string_of_int (-i)) (convertExp e1)
		      | _ -> Printf.sprintf "guf_minus(%s,%s)" (convertExp e1) (convertExp e2)
		  end
	      |	E.Plus  -> 
		  begin
		    match e2 with
                        E.Constant (Constant.Int 0) -> (convertExp e1)
		      | E.Constant (Constant.Int i) -> 
			  if (i > 0) then Printf.sprintf "(succ^%s(%s))" (string_of_int i) (convertExp e1)
			  else Printf.sprintf "(pred^%s(%s))" (string_of_int (-i)) (convertExp e1)
		      | _ -> Printf.sprintf "guf_plus(%s,%s)" (convertExp e1) (convertExp e2)
		  end
              | E.Offset 
	      |	E.Mul   
	      |	E.BitAnd 
	      |	E.BitOr 
	      |	E.LShift 
	      |	E.Div 
	      |	E.Rem 
	      |	E.RShift 
	      |	E.Xor 
	      |	E.Comma ->
	        Printf.sprintf "%s(%s,%s)" (E.binaryOpToVanillaString op) (convertExp e1) (convertExp e2)
	 
	      |	_ -> failwith ("convertExp: (Uclid) This operator should not be handled here "^(E.toString e))
	  end
      | E.Constant (c) -> 
	  begin
	    match c with
		Constant.Int i ->  
	          if (i = 0) then "zero" 
	          else
		    if (i < 0) then Printf.sprintf "(pred^%s(zero)" (string_of_int (-i)) 
		    else Printf.sprintf "succ^%s(zero)" (string_of_int i) 
	      | Constant.String s -> add_uclid_ground (s^" : TERM;");(s) 
	      | _ -> failwith ("unknown constant type")
	  end

      | E.Unary (uop, e) -> 
	  begin
	    match uop with 
		E.UnaryMinus -> failwith "cannot handle uminus"
	      | E.UnaryPlus -> failwith "cannot handle uplus"
	      | E.Address -> failwith "cannot handle address"
	      |	_ -> failwith "unknown unop" 
	  end
      | E.Lval l -> convertLval l
      | E.FunctionCall _ -> failwith "ConvertExp (Uclid): Does not handle function call" 
      | _ -> failwith ("convertExp: " ^ (E.toString e) ^ "not handled")

  and convertLval l =
    match l with
    | E.This -> failwith "This not handled in convertLval"
    | E.Symbol(str) -> 
	begin
	  (if Misc.is_prefix "b_p" str then add_uclid_ground (str^" : TRUTH;") else add_uclid_ground (str^" : TERM;"));
	  str 
	end
    | E.Access(E.Dot, e, s) -> 
	begin
	  add_uclid_ground (s ^" : TERM;");
	  (Printf.sprintf "(select_dot(%s,%s))" (convertExp e) s)
	end
    | E.Access(E.Arrow, e, s) ->
	begin
	  add_uclid_ground (s ^" : TERM;"); 
	  (Printf.sprintf "(select_dot(%s,%s))" (convertExp e) s)
	end
    | E.Dereference e -> 
	begin
	  match e with
	    E.Binary(E.Plus,e1,e2) 
          | E.Binary(E.Offset,e1,e2) -> Printf.sprintf "(select(%s,%s))" (convertExp e1) (convertExp e2)
          | _ -> Printf.sprintf "(select(%s,zero))" (convertExp e)
	end
    | E.Indexed (e1, e2) ->
	begin
	  Printf.sprintf "(select (%s, %s))" (convertExp e1) (convertExp e2)
	end
  and convertAtom e =
    match e with
      E.Binary (op, e1, e2) -> 
	begin
	  let opstring = 
	    match op with 
	      E.Eq  -> "="
	    | E.Ne  -> "!="
	    | E.Gt  -> ">"
	    | E.Lt  -> "<"
	    | E.Ge  -> ">="
	    | E.Le  -> "<="
	    | _   -> "" 
	  in
          if not (opstring = "") then
	    Printf.sprintf "(%s %s %s)" (convertExp e1) opstring (convertExp e2)
	  else (convertExp e)
	end
    | _ -> convertExp e 
	  
and convertPred e =
    match e with
	P.True -> "1"
      | P.False -> "0"
      | P.And plist -> 
	    List.fold_left 
	      (fun a -> fun b -> Printf.sprintf " (%s & %s) " (convertPred b) a) 
	      "1" (plist)
      | P.Or plist -> 
	  List.fold_left 
	    (function a -> function b -> Printf.sprintf "(%s | %s)" (convertPred b) a )
	    "0" (plist)
      | P.Implies (e1, e2) -> Printf.sprintf "(%s => %s)" (convertPred e1) (convertPred e2)
      | P.Not e1 -> Printf.sprintf "(~ %s)" (convertPred e1)
      | P.Atom e ->  (convertAtom e)
      
      | P.Iff (e1,e2) ->
          let p1 = P.Implies (e1,e2) in
          let p2 = P.Implies (e2,e1) in
          Printf.sprintf " (%s & %s) " (convertPred p1) (convertPred p2)
      | _ -> failwith ("UCLID: Cannot handle this predicate "^(P.toString e))
  
      in
	(convertPred exp)


          (* this function checks if the formula given below is satisfiable -- 
	     right now it just dumps a trace into a file called blast-trace.ucl *)

let uclidSat fmla = 
   (* first we have to convert the formula and collect all the ground terms that appear in it *)
   reset_uclid_ground ();
   
   let block_counter = ref 0 in     
   let block_temp_list = ref [] in
   let formula_const _fmla = 
       block_counter := !block_counter + 1;
       block_temp_list := ("blast_temp_"^(string_of_int !block_counter))::(!block_temp_list);
       Printf.sprintf "blast_temp_%s := %s; \n" (string_of_int !block_counter) (convertToUclidSyntax _fmla)
   in
   let formulas = match fmla with P.And l -> l | _ -> failwith ("bad call to uclidSat") in
   
   let def_string = String.concat "\n" (List.map formula_const formulas) in	     
   let root_string = " \n root := "^(String.concat " & " (!block_temp_list))^";\n" in
   let const_def_string = List.fold_left (function a -> function b -> Printf.sprintf "%s \n  %s" b a )
	  "" !uclid_ground_term_list
   in
   let uclid_string = def_string^root_string in
   let send_to_file = 
       Printf.sprintf "MODEL blasttrace \n CONTROL \n EXTVAR \n STOREVAR \n VAR \n CONST \n %s \n DEFINE %s \n EXEC \n decide(~root); \n" const_def_string uclid_string 
   in 
   Misc.write_to_file "blast-trace.ucl" send_to_file;
   let uclidret = Sys.command "runuclid blast-trace.ucl" in
     (*if uclidret = 127 then failwith "uclid is NOT in the path or something!"
     else*)
     begin
       M.msg_string M.Debug "running smvsat...";
       let smvsat_ret =  Sys.command "smvsat blastrace.cnf foobaz" in
	 if (smvsat_ret <> 1) then failwith "issues with smvsat" 
	 else
	   begin
	     M.msg_string M.Debug "...smvsat finished";
             let _ = Sys.command "grep \"b\_p\" blasttrace.map > tmp-blast-bpvars" in
             let _ = Sys.command "cut -d ' ' -f 1 ztrace.variables > tmp-blast-pfvars" in
	     let useful_vars = List.map (List.hd) (Misc.grep_file "tmp-blast-bpvars" "tmp-blast-pfvars") in
	       useful_vars
	   end 
       end

let uclid_output modelname  fmla comment_string = 
  (* first we have to convert the formula and collect all the ground terms that appear in it *)
   reset_uclid_ground ();
   let block_counter = ref 0 in     
   let block_temp_list = ref [] in
   let formula_const _fmla = 
       block_counter := !block_counter + 1;
       block_temp_list := ("blast_temp_"^(string_of_int !block_counter))::(!block_temp_list);
       Printf.sprintf "blast_temp_%s := %s; \n" (string_of_int !block_counter) (convertToUclidSyntax _fmla)
   in
   let formulas = [fmla] (* match fmla with P.And l -> l | _ -> failwith ("bad call to uclidSat:"^(P.toString fmla)) *) in
   
   let def_string = String.concat "\n" (List.map formula_const formulas) in	     
   let root_string = " \n root := "^(String.concat " & " (!block_temp_list))^";\n" in
   let const_def_string = List.fold_left (function a -> function b -> Printf.sprintf "%s \n  %s" b a )
	  "" !uclid_ground_term_list
   in
   let uclid_string = def_string^root_string in
   let send_to_file = 
       Printf.sprintf "MODEL %s \n CONTROL \n EXTVAR \n STOREVAR \n VAR \n CONST \n %s \n DEFINE %s \n EXEC \n decide(~root); \n (* %s *)" modelname const_def_string uclid_string comment_string 
   in 
   Misc.write_to_file (modelname^".ucl") send_to_file

(*****************************************************************)
(** Implement a simple "predicate flow analysis" on the useful   *)
(*  predicates. This does a congruence closure on the variables  *)
(*  in the set of useful predicates and returns extra predicates *)
(*  that may be useful.                                          *)
(*****************************************************************)

let scope_pred_table = Hashtbl.create 37

let predicate_closure predlist = 
  let cong_classes = ref [] in
  let new_predicates = ref [] in
  let set_of_elt e = (* puts e in a new set if its not anywhere and if it is then removes it*)
    let _temp_set = List.filter (List.mem e) (!cong_classes) in
    let _cong_classes = List.filter (fun x -> not (List.mem e x)) (!cong_classes) in
    cong_classes := _cong_classes;
    if _temp_set = [] then ([e]) else List.hd _temp_set
  in
  
  let process_pred pred = 
    match pred with
      P.Atom (E.Binary (E.Eq,e1,e2)) -> 
	let s1 = set_of_elt e1 in
	if (List.mem e2 s1) then cong_classes := s1:: !cong_classes
	else 
	  let u = Misc.union s1 (set_of_elt e2) in
	  cong_classes := u::(!cong_classes)
    | _ -> ()
  in
  let _ = List.iter process_pred predlist 
  in
    let scope_of_exp exp =
      match exp with
	E.Lval E.Symbol(s) -> 
	  begin
	    let names = Misc.chop s "@" in
	    if (List.length names = 1) then "__BLAST_GLOBAL" else (List.nth names 1)
	  end
      | _ -> "__BLAST_GLOBAL"
    in
    
    let process_cong_class cc = 
      let _ = print_string "cc: \n";List.iter (fun x -> print_string ((E.toString x)^"  \n")) cc in  
  (* this should return a list of predicates *)
      let _ = Hashtbl.clear scope_pred_table in
      let process_exp e = 
	let scp = scope_of_exp e in
	let _ =  M.msg_string M.Debug (Printf.sprintf "exp: %s   scope: %s" (E.toString e) scp) in
	let present_set = try Hashtbl.find scope_pred_table scp with Not_found -> [] in
	Hashtbl.replace scope_pred_table scp (e::present_set)
      in
      let proc_scope_terms globals scp lst =
	if (not (scp = "__BLAST_GLOBAL")) then
	  begin
	    let pairs = Misc.cross_product lst globals (* Misc.make_all_pairs (lst@globals)*) in
	    new_predicates := (List.map (fun (x,y) -> P.Atom (E.Binary(E.Eq,x,y))) pairs )@(!new_predicates)
	  end
	else 
	  ()
      in
      List.iter process_exp cc;
      let globals_cc = try Hashtbl.find scope_pred_table "__BLAST_GLOBAL" with Not_found -> [] in
      let _ = print_string "globals: \n";List.iter (fun x -> print_string ((E.toString x)^"  \n")) globals_cc    
      in      
      Hashtbl.iter (proc_scope_terms globals_cc) scope_pred_table 
    in  
    let _ = List.iter process_cong_class (!cong_classes) in
    !new_predicates
    (* very inefficient congruence closure algorithm *)
   
(**********************************************************)      
(* Get the list of useful predicates from the last proof. *)
(**********************************************************)
let getUsefulPredicates pred =
  let getPredsFromProof pred =
    if ("cvc" = O.getValueOfString "s") 
    then
      getProofsFromCvc pred    
    else 
    if ("simplify" = O.getValueOfString "s"
    || "simplify-bits" = O.getValueOfString "s") then
      getProofsFromSimplify pred 
    else
      begin
        let _eToCheck = P.implies pred (P.False) in
        let ans = queryExpUsingVampyre _eToCheck
        in
        let _ = M.msg_string M.Debug "Got UsefulPred" in
        if ans = false
        then
          []
        else
          begin
            let _lst = Engine.getUsefulPredicates () in
	    List.map convertToPredicate _lst
          end
      end 
  in
  let plist = getPredsFromProof pred in
	  M.msg_string M.Debug "Leaf predicates.\n";
          List.iter (fun p -> M.msg_string M.Debug (P.toString p)) (plist); 
      (* let inf_plist = List.map convertToPredicate inferred_atoms in *)
      if(O.getValueOfBool "scope") then
	begin
	  let pred_closure_list = predicate_closure plist in
      
	  M.msg_string M.Debug "Closure predicates.\n";
   	  List.iter (fun p -> M.msg_string M.Debug (P.toString p)) (pred_closure_list); 
	  plist @ pred_closure_list 
	end
      else
	plist

	       
(*************************************************************************************)
(*************************************************************************************)
(*                               Interface to FOCI                                   *)
(*************************************************************************************)
(*************************************************************************************)
let stats_nb_foci_queries = ref 0

exception FociSatException
exception FociSatExceptionAsgn of string list

(* FOCI VERSION DIFF -- BEGIN *)
(* FOR FOCI 1 
module FociPredicate = FociAst.Predicate
module FociExpression = FociAst.Expression
module FociConstant = FociAst.Constant

let foci_interpolate (a,b) = 
  stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  try Foci.interpolate (a,b) with Foci.SAT -> raise FociSatException 
let foci_interpolateN _ = failwith "foci_interpolateN not supported in foci 1 !"

let foci_interpolateN_symm _ = failwith "foci_sym_interpolateN not supported in foci 1/2"

let foci_interpolateN_dasdill _ _ = failwith "foci_interpolateN_dasdill not supported in foci1/2"
let foci_theory_clauses () = failwith "foci_theory_clauses not supported in foci 1/2"  


END FOCI 1 *)  

(* FOR FOCI 2 *) 
module FociPredicate = FociAst
module FociExpression = FociAst
module FociConstant = FociAst.Constant

let debug_flag () = 
  (M.get_verbosity () = M.Debugging)

let time_fi () (x,y) =
  let focimax = O.getValueOfInt "focimax"  in
  let df = debug_flag () in
  if (focimax = (-1)) then Foci.interpolate (x,y)  
  else List.hd (FociSplit3.interpolateN [x;y] focimax) 

let foci_interpolate (a,b) =  
  stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  let a' = FociDag.pred_dag_of_tree a in
  let b' = FociDag.pred_dag_of_tree b in
  let itp' = 
    try  Stats.time "foci interpolate" (time_fi ()) (a',b') with Foci.SAT _ -> raise FociSatException 
  in
  let itp = FociDag.pred_tree_of_dag itp' in
  itp

let get_pred_symbol fp = 
  match fp with
  FociPredicate.Atom s -> Some s
  | _ -> None

let itpN itp_fun plist = 
  stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  let p'_list = List.map FociDag.pred_dag_of_tree plist in
   let get_itp_list () = 
    try 
      M.msg_string M.Debug "calling into foci";
      let rv = Stats.time "foci itpn" itp_fun p'_list in
      M.msg_string M.Debug "back from foci";
      rv
    with Foci.SAT (plist',_) | FociSplit3.SAT (plist',_) ->
      (M.msg_string M.Debug "back from foci - exn SAT";
        if plist' = [] 
        then (M.msg_string M.Normal "Foci SAT: Empty"; 
                raise FociSatException)
          else 
            let plist'_s = Misc.strList (List.map FociDag.Predicate.toString plist') in 
            let _ = M.msg_string M.Normal ("Foci SAT: "^plist'_s) in
            let plist = List.map (FociDag.pred_tree_of_dag) plist' in
            let pslist = Misc.map_partial get_pred_symbol plist in
            raise (FociSatExceptionAsgn pslist))
  in
  let itp'_list  = Stats.time "compute itpN" get_itp_list () in
  let itp_list = List.map FociDag.pred_tree_of_dag itp'_list in
  itp_list

(* let foci_interpolateN = itpN Foci.interpolateN  
 * let foci_interpolateN_symm = itpN Foci.interpolateN_symm 
 * w/o FOCISPLIT *)

(* w/ FOCISPLIT  *)
let foci_interpolateN plist =
  let focimax = O.getValueOfInt "focimax"  in
  let df = debug_flag () in
  if (focimax = (-1)) then  itpN Foci.interpolateN plist 
  else (* if (O.getValueOfBool "fociminp") 
          then 
          (let _ = FociSplit3.init df in 
	  itpN (fun l -> FociSplit3.interpolateN_irp df l focimax) plist) 
          else *) itpN (fun l -> FociSplit3.interpolateN l focimax) plist 
      
let foci_interpolateN_symm plist = 
  let focimax = O.getValueOfInt "focimax" in
    if not (O.getValueOfBool "mccarthy") (* true (focimax = (-1)) *) then itpN Foci.interpolateN_symm plist
    else itpN (fun l -> FociSplit3.interpolateN_symm l focimax) plist 
      (* let _ = 
        try ignore (Stats.time "itp-persist" (itpN (fun l ->
          FociSplit3.interpolateN l focimax)) plist) with _ -> () in *) 
      (* debug *)

(* Uncomment for Foci 1/2 
let foci_interpolateN_symm _ = failwith "foci_sym_interpolateN not supported in foci 1/2"
let foci_interpolateN_dasdill _ _ = failwith "foci_interpolateN_dasdill not supported in foci1/2"
let foci_theory_clauses () = failwith "foci_theory_clauses not supported in foci 1/2"  
let foci_instance_ref = None 
let foci_inc_pop () = failwith "foci_inc_pop: not implemented"
let foci_inc_push p = failwith "foci_inc_push: not implemented"
let foci_inc_check_sat () = failwith "foci_inc_check_sat: not implemented"
let foci_inc_reset () = failwith "foci_inc_reset: not implemented"
END FOCI 2 *)

(* the next is there only in FOCI 3 *)

let foci_theory_clauses () = 
  let fp_list_list = Foci.theoryClauses () in  
  let tc_size = List.length (List.flatten fp_list_list) in
  M.msg_string M.Normal (Printf.sprintf "Foci says: %d theory clauses" tc_size);
  List.map (List.map (FociDag.pred_tree_of_dag)) fp_list_list

let foci_interpolateN_dasdill t_list c_list = 
   stats_nb_foci_queries := 1 + !stats_nb_foci_queries;
  let t'_list = List.map FociDag.pred_dag_of_tree t_list in
  let c'_list = List.map FociDag.pred_dag_of_tree c_list in
  let get_dd_itp_list () =
   try 
     M.msg_string M.Error "Calling dd_itpn";
     Stats.time "foci dd_itpn" (FociDasDill.refine t'_list) c'_list  
    with Foci.SAT (plist',_) ->
        if plist' = [] 
        then (M.msg_string M.Normal "Foci SAT: Empty"; 
                raise FociSatException)
          else 
            let plist'_s = Misc.strList (List.map FociDag.Predicate.toString plist') in 
            let _ = M.msg_string M.Normal ("Foci SAT: "^plist'_s) in
            let plist = List.map (FociDag.pred_tree_of_dag) plist' in
            let pslist = Misc.map_partial get_pred_symbol plist in
            raise (FociSatExceptionAsgn pslist)
  in
  let itp'_list  = Stats.time "compute itpN" get_dd_itp_list () in
  let _ = M.msg_string M.Error "done dd_itpn" in
  let itp_list = List.map FociDag.pred_tree_of_dag itp'_list in
  itp_list





  
(* END FOCI 3 *)  
(* FOCI VERSION DIFF -- END *)

let foci_expression_toString = FociAst.Expression.toString
let foci_predicate_toString = FociAst.Predicate.toString


let compile_sets (plist : FociPredicate.predicate list) =
  if not (O.getValueOfBool "sets") then plist else 
  let list_of_var = ref [] in
  let rec collect_all_vars (e: FociExpression.expression) =
    match e with
      FociExpression.Constant _ -> ()
    | FociExpression.Variable s -> if List.mem e !list_of_var then () else list_of_var := e :: !list_of_var
    | FociExpression.Application ("sel", [e1; e2]) ->
        collect_all_vars e1
    | FociExpression.Application (s, el) ->
        begin
          List.iter collect_all_vars el
        end
    | FociExpression.Coeff _ -> assert (false)
    | FociExpression.Ite (p, e1, e2) -> trawl_all p ; collect_all_vars e1; collect_all_vars e2
    | _ -> ()
  and
      trawl_all e =
    match e with
      FociPredicate.True | FociPredicate.False -> ()
    | FociPredicate.And plist | FociPredicate.Or plist -> List.iter trawl_all plist
    | FociPredicate.Not  p -> trawl_all p
    | FociPredicate.Implies(p,p') -> trawl_all p; trawl_all p'
    | FociPredicate.Equality (e, e') ->
        begin
          match e' with
          | FociExpression.Application("emptyset",[]) -> ()
          | FociExpression.Application("add", [e1; e2]) ->
              collect_all_vars e2
          | FociExpression.Application("sel", [e1; e2]) ->
              collect_all_vars e; collect_all_vars e1
          | _ -> collect_all_vars e; collect_all_vars e'
        end;
    | FociPredicate.Leq (e, e') -> ()
    | FociPredicate.Atom s -> ()
    | FociPredicate.Forall (slist, p) -> assert (false)
  in
  let emptysets = ref [] in let in_is_called = ref "in" in
  let unions = ref [] in 
  (* trawl over the predicate, populating the above lists *)
  let rec    trawl e =
    match e with
      FociPredicate.True | FociPredicate.False -> ()
    | FociPredicate.And plist | FociPredicate.Or plist -> List.iter trawl plist 
    | FociPredicate.Not  p -> trawl p
    | FociPredicate.Implies(p,p') -> trawl p; trawl p'
    | FociPredicate.Equality (e, e') ->
	begin
	  match e' with
	  | FociExpression.Application(fnname,e1) ->
              if Misc.is_prefix "emptyset" fnname then
	        emptysets := e :: !emptysets ;
	      if Misc.is_prefix "add" fnname then begin
                match e1 with [x; t] -> unions := (e, x, t) :: !unions | _ -> assert false 
                end ;
              if Misc.is_prefix "in" fnname then in_is_called := fnname 
	  | _ -> () 
	end;
    | FociPredicate.Leq (e, e') -> ()
    | FociPredicate.Atom s -> ()
    | FociPredicate.Forall (slist, p) -> assert (false)
  in
  ignore(List.map trawl_all plist) ;
  List.iter (fun x -> match x with FociExpression.Variable s -> M.msg_string M.Normal (" " ^ s) | _ -> failwith "match failure: compile_sets") !list_of_var ;
  let translate_empty e =
    FociPredicate.And (List.map (fun x -> 
      FociExpression.Equality (FociExpression.Application (!in_is_called, [e ; x]), FociExpression.Constant (FociConstant.Int 0))) !list_of_var)
  in
  let translate_union (e, e1, t) =
    FociPredicate.And (List.map (fun x ->
      let t1 = FociExpression.Equality (FociExpression.Application (!in_is_called, [e; x]), FociExpression.Constant (FociConstant.Int 0)) in
      let t2 = FociExpression.Equality (FociExpression.Application (!in_is_called, [e1; x]), FociExpression.Constant (FociConstant.Int 0)) in
      let t3 = FociExpression.Equality (t, x) in
      let imp1 = FociPredicate.Implies (t1, FociPredicate.Or [ t2; t3]) in
      let imp2 = FociPredicate.Implies (FociPredicate.Or [ t2; t3], t1) in
      FociPredicate.And [ imp1; imp2]) !list_of_var)
  in
  let translate p =
    unions := [] ; emptysets := [] ;
    trawl p;
    let psets = FociPredicate.And ((List.map translate_empty !emptysets) @ (List.map translate_union !unions))
    in
    unions := [] ; emptysets := [];
    M.msg_string M.Debug "compiled sets..." ;
    M.msg_string M.Debug (foci_predicate_toString psets);
    FociPredicate.And [p; psets]
  in
  List.map translate plist 


type foci_return = Sat | Unsat of (P.predicate (* * P.predicate *))

let fctr = ref 0 

(* For the benefit of foci_symm -- we need to rename the UFs used with this suffix so that they are disjoint *)


let foci_make_disjoint_flag = ref false
let set_disjoint_flag () = 
  M.msg_string M.Debug "Set Disjoint flag";
  foci_make_disjoint_flag := true

let unset_disjoint_flag () = 
  M.msg_string M.Debug "Unset Disjoint flag";
  foci_make_disjoint_flag := false

let foci_UF_idx = ref (0)

let foci_UF_idx_tick () = foci_UF_idx := !foci_UF_idx + 1

let foci_UF_idx_reset () =  foci_UF_idx := 0


let foci_UF_string s =
  let rv = 
    if !foci_make_disjoint_flag && not (E.is_interpreted s) 
    then s^"_"^(string_of_int !foci_UF_idx) 
    else s 
  in
  (* M.msg_string M.Debug (Printf.sprintf "foci_UF_string: in = %s, out = %s" s rv); *)
  rv

let clean_foci_UF_string s = 
  if not (!foci_make_disjoint_flag) then s
  else
    let pieces = Misc.chop s "_" in
    if (List.length pieces > 1) then String.concat "_" (Misc.chop_last pieces) 
    else s
 
let new_foci_var () = 
  fctr := !fctr + 1;
  "foci"^(string_of_int !fctr)

let listOfAddrVarsFoci = ref []
let chlval_table = Hashtbl.create 109
let chlval_map_table  = Hashtbl.create 109
let int_table_to = Hashtbl.create 23
let int_table_from = Hashtbl.create 23
let int_table_seen = Hashtbl.create 23

let max_int = 999999 (* some RANDOM number I made up at 1am -- RJ *)
let num_offset = ref 100000
let make_new_num i =
  let s_i = string_of_int i in
  let s_i_c = Misc.k_suffix 2 s_i in
  let rv = int_of_string s_i_c in
    rv
  (*num_offset := !num_offset + 1; 
  !num_offset*)

let sign i = if i > 0 then 1 else if i = 0 then 0 else -1 
 
let int_map_to i =
  (* M.msg_string M.Debug ("int_map_to"^(string_of_int i));*)
  (* add i to the list of constants we've passed to foci *)
  let _ = Hashtbl.replace int_table_seen i true in 
  let abs_i = abs i in
  let ret_val = 
   (sign i) * 
   (if abs_i < max_int then abs_i
    else
      begin
	if (Hashtbl.mem int_table_to abs_i) 
	then 
	  (Hashtbl.find int_table_to abs_i)
	else
	  let key = make_new_num abs_i in
	  let _ = (Hashtbl.add int_table_to abs_i key) in
	  let _ = (Hashtbl.add int_table_from key abs_i) in
	    key
      end)
 in
   (*M.msg_string M.Debug ("returns: "^(string_of_int ret_val));*)
   
   let _ = if (ret_val <> i) then M.msg_string M.Normal "WARNING :int_maps_to overflow!"
   in
     ret_val

let int_map_from i = 
  let abs_i = abs i in
  let _ =
    let seen = 0::(Misc.hashtbl_keys int_table_seen) in
    let focimax = O.getValueOfInt "focimax"  in
    if false && (focimax <> -1) && 
        (not (Hashtbl.mem int_table_seen i)) && 
        (abs_i > focimax) && 
        List.for_all (fun k -> abs ((abs k) - abs_i) > focimax)  seen (* TBD:SPEED *)
    then 
      (M.msg_string M.Error ("Invalid Interpolant -- large constant:"^(string_of_int i));
      raise FociSatException)
    (* this will trickle up all the way and be caught by whoever *)
  in
  let ret_val =
    (try
      Hashtbl.find int_table_from abs_i
    with Not_found -> abs_i) * (sign i)
  in
    ret_val
 
(* in theory one could even use simplify or something else to "simplify" *)
let isConstant e = 
  match e with
      E.Constant (Constant.Int i) -> Some i
    | E.Unary (E.UnaryMinus,E.Constant (Constant.Int i)) -> 
	Some (-i)
    | E.Unary (E.UnaryPlus,E.Constant (Constant.Int i)) -> 
	Some (i)
    | _ -> None

let rec convertExpFoci e =
  let convert_times e1 e2 = 
    match (isConstant e1, isConstant e2) with
	(Some i1,Some i2) -> FociExpression.Constant (FociConstant.Int (i1 * i2))
      | (Some i1, None) -> FociExpression.Coeff (FociConstant.Int i1, (convertExpFoci e2))
      | (None, Some i2) -> FociExpression.Coeff (FociConstant.Int i2, (convertExpFoci e1))
      | (None, None) -> 
	  begin
	    M.msg_string M.Debug (Printf.sprintf "WARNING! convertExpFoci: times of %s * %s  " (E.toString e1) (E.toString e2));
	    let ufs = foci_UF_string "times" in
            FociExpression.Application(ufs,List.map convertExpFoci [e1;e2])
	  end
  in
  let foci_minus e' =
    match isConstant e' with
	Some i -> FociExpression.Constant (FociConstant.Int (int_map_to (-i)))
      | None -> FociExpression.Coeff (FociConstant.Int (-1),(convertExpFoci e'))
  in
  let chlval_convert_table e = 
    try FociExpression.Variable (Hashtbl.find chlval_map_table e) 
    with 
	Not_found ->
	  let s_e = new_foci_var () (* E.toString e*) in
	    Hashtbl.add chlval_table s_e e;
	    Hashtbl.add chlval_map_table e s_e;
	    FociExpression.Variable s_e
  in
  let chlval_convert_recursive (l,s) = 
    let ufs = foci_UF_string "chlval" in
    FociExpression.Application (ufs, [ convertLvalFoci l; FociExpression.Variable ("_"^s) ])
  in
    match e with
	E.Lval lv -> convertLvalFoci lv
      | E.Chlval (l, s) -> 
	  if (O.getValueOfBool "fociUF") then chlval_convert_recursive (l,s)
	  else chlval_convert_table e
	  
      (* So the chlval thing didnt work out so well. look at test/foci/ex3.foci 
	 so now we just convert it to string and for reconversion, stuff the result in a hashtbl!
	FociExpression.Application ("chlval", [ convertLvalFoci l; FociExpression.Variable ("_"^s) ])
      *)
  | E.Binary (op, e1, e2) -> 
      begin
	match op with 
	    E.Minus -> 
	      if (O.getValueOfBool "uif") then
                let ufs = foci_UF_string "UIminus" in
		FociExpression.Application (ufs, [convertExpFoci e1; convertExpFoci e2])
	      else
		FociExpression.Sum [convertExpFoci e1; foci_minus e2]
	  | E.Plus  -> 
	      if (O.getValueOfBool "uif") then
                let ufs = foci_UF_string "UIplus" in
		FociExpression.Application (ufs, [convertExpFoci e1; convertExpFoci e2])
	      else
		FociExpression.Sum[convertExpFoci e1; convertExpFoci e2]
	  | E.Mul   -> convert_times e1 e2
          | E.Offset
          | E.BitAnd 
	  | E.BitOr 
	  | E.Div 
	  | E.Rem 
	  | E.RShift 
	  | E.Xor 
	  | E.Comma ->
	        let ufs = foci_UF_string (E.binaryOpToVanillaString op ) in
                    FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])
          | E.LShift ->
	      begin
		match e2 with
		  E.Constant (Constant.Int k) -> convert_times e1 (E.Constant (Constant.Int (Misc.power 2 k)))
		| _ -> 	  
                    let ufs = foci_UF_string (E.binaryOpToVanillaString op ) in
                    FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])
	      end
	  | E.FieldOffset ->
	     let v = convertExpFoci e1 in
	      (* the second argument to field offset is a field id *)
	     let s =
	       match e2 with
	         E.Lval (E.Symbol s) -> s
	       | _ -> failwith ("convertExp : (FOCI) Second operator of field offset not a field id "^
				 (E.toString e))
	     in
	     let ufs_fof = foci_UF_string "foffset" in
             let ufs_fld = foci_UF_string ("@field_"^s) in
             FociExpression.Application(ufs_fof, [v; FociExpression.Variable ufs_fld])
	     
	  |	_ -> 
	      if E.isRelOp op then raise AtomInExpressionException
	      else failwith ("convertExp (Foci): This operator should not be handled here "^(E.toString e))
      end
  | E.Constant (c) -> 
      begin
	match c with
	    Constant.Int i -> FociExpression.Constant (FociConstant.Int (int_map_to i))
	  | Constant.String s -> 
	      begin
		M.msg_string M.Debug ("convertExpFoci: String Constant! in"^(E.toString e));
                let c = get_string_constant s in
		FociExpression.Variable ("_STRINGCONSTANT"^c)
	      end
	  | Constant.Float f ->
	     	    if (f -. (ceil f) = 0.0) then FociExpression.Constant (FociConstant.Int (int_of_float f))
	    else FociExpression.Variable "_FLOATINGPTCONSTANT"
	  | _ -> failwith ("Cannot handle constant "^(Constant.toString c))
      end
  | E.Unary (uop, e) -> 
      begin
	match uop with 
	    E.UnaryMinus -> foci_minus e
	  | E.UnaryPlus -> convertExpFoci e
	  | E.Address -> 
	    let v = convertExpFoci e in
            let ufs = foci_UF_string "addrOf" in
	    if (List.mem v (!listOfAddrVarsFoci)) then () 
	    else
	      listOfAddrVarsFoci := v::(!listOfAddrVarsFoci);
	    FociExpression.Application(ufs, [v])
	  |	_ -> 
              let ufs = foci_UF_string (E.unaryOpToVanillaString uop)
              in
              FociExpression.Application(ufs, [convertExpFoci e])
      end
  | E.FunctionCall(E.Lval (E.Symbol f), args) ->
      FociExpression.Application(foci_UF_string f, List.map convertExpFoci args)
  | E.Select (e1,e2) -> FociExpression.Application("select",List.map convertExpFoci [e1;e2])
  | E.Store  (e1,e2,e3) -> FociExpression.Application("store",List.map convertExpFoci [e1;e2;e3])
  | _ -> failwith ("convertExpFoci: " ^ (E.toString e) ^ "not handled")
	
  and convertLvalFoci l =
    match l with
    | E.This -> failwith "This not handled in convertLval"
    | E.Symbol(str) -> FociExpression.Variable (str) 
    | E.Access(E.Dot, e, s) -> (* NEEDS DEBUGGING *) 
	let v = convertExpFoci e in (* hack to get around foci bug! *)
        let ufs = foci_UF_string ("sel@field_"^s) in
	FociExpression.Application( ufs, [v(*; FociExpression.Variable ("@field_"^s)*)])
    | E.Access(E.Arrow, e, s) ->
	failwith "foci doesnt like Arrow"
    | E.Dereference e ->
        let ufs = foci_UF_string "sel" in
	FociExpression.Application(ufs, [convertExpFoci e; FociAst.zero])
    | E.Indexed (e1, e2) -> 
        let ufs = foci_UF_string "sel" in
	FociExpression.Application(ufs, List.map convertExpFoci [e1;e2])

  and 
    convertAtomFoci e = (* this returns a focipredicate *)
  let make_eq e1' e2' = 
    try 
      FociPredicate.Equality (convertExpFoci e1', convertExpFoci e2') 
    with AtomInExpressionException ->
      begin
        match e1' with
	  E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
	    begin
	      let s1 = convertExpFoci e1' in
	      let s2 = convertAtomFoci e2' in
	      FociPredicate.Or [ (FociPredicate.And [s2 ; FociPredicate.Equality (s1, FociExpression.Constant (FociConstant.Int 1)) ] ) ; 
				 (FociPredicate.And [FociPredicate.Not s2 ; FociPredicate.Equality (s1, FociAst.zero) ] ) ]
	    end
        | _ -> 
	    begin
	      match e2' with
		E.Lval _ | E.Chlval _ | E.Constant (Constant.Int _) ->
		  begin
		    let s1 = convertExpFoci e2' in
		    let s2 = convertAtomFoci e1' in
		    FociPredicate.Or [ (FociPredicate.And [s2 ; FociPredicate.Equality (s1, FociExpression.Constant (FociConstant.Int 1)) ] ) ; 
				       (FociPredicate.And [FociPredicate.Not s2 ; FociPredicate.Equality (s1, FociAst.zero) ] ) ]
		  end
		    
	      | _ -> failwith ("convertAtomFoci: Cannot handle this expression: "^(E.toString e))
		    
	    end 
      end
  in
  let make_leq e1' e2' = 
    FociPredicate.Leq (convertExpFoci e1', convertExpFoci e2') 
  in
  match e with
    E.Binary (op, e1, e2) -> 
      begin
	match op with 
	  E.Eq  -> make_eq e1 e2 
	|	E.Ne  -> FociPredicate.Not (make_eq e1 e2)
	|	E.Le  -> make_leq e1 e2	      
	|	E.Lt  -> FociPredicate.Not (make_leq e2 e1)
	|	E.Gt  -> FociPredicate.Not (make_leq e1 e2)
	|	E.Ge  -> make_leq e2 e1 
	|	_   -> failwith ("convertAtomFoci: Cannot handle this expression: "^(E.toString e))
      end	
  | E.FunctionCall(E.Lval (E.Symbol f), args) ->
      make_eq e (E.Constant (Constant.Int 0))
  | E.Lval (E.Symbol s) -> FociPredicate.Atom s
  | _ -> failwith ("convertAtomFoci: "^(E.toString e)^ " Not a binary relation operator")

and convertPredFoci e =
  match e with
    P.True -> FociPredicate.True
  | P.False -> FociPredicate.False
  | P.And plist -> 
      FociPredicate.And (List.map convertPredFoci plist)
  | P.Or plist -> 
      FociPredicate.Or (List.map convertPredFoci plist)
  | P.Implies (e1, e2) -> FociPredicate.Or [FociPredicate.Not (convertPredFoci e1); (convertPredFoci e2)]
  | P.Not e1 -> FociPredicate.Not (convertPredFoci e1)
  
  | P.Atom e ->  (convertAtomFoci e)
  | P.Iff (e1,e2) ->  convertPredFoci 
        (P.Or [(P.And [e1;e2]);(P.And [P.Not e1 ; P.Not e2])])
  | P.All (s,e1) -> FociPredicate.Forall ([s],convertPredFoci e1)  
  | _ -> failwith ("Foci Cannot handle this predicate "^(P.toString e))
	
and convertToFociSyntax (e: P.predicate) =
  listOfAddrVarsFoci := [];
  let v_exp = convertPredFoci (P.constantfold e (Hashtbl.create 1))
  in
  let ufs_sel = foci_UF_string "sel" in
  let ufs_aof = foci_UF_string "addrOf" in
  let genAddrOfEq b =
        FociPredicate.Equality(b, FociExpression.Application(ufs_sel, 
							 [FociExpression.Application(ufs_aof, [b]); FociAst.zero]))
  in
  let genAddrPositive b = FociPredicate.Not  (FociPredicate.Leq 
				( FociExpression.Application(ufs_aof, [b]), (FociAst.zero))) in
    begin
    let vv = FociPredicate.And ((v_exp 
                      ::(List.map (fun b -> FociPredicate.Not (FociPredicate.Equality ( 
                                                                       (FociExpression.Application(ufs_aof, [b])) ,
                                                                       FociAst.zero)))
                                  !listOfAddrVarsFoci))
                       @ (List.map genAddrOfEq !listOfAddrVarsFoci)
                       @ (List.map genAddrPositive !listOfAddrVarsFoci)
                       ) in
      vv
  end

(* oh no! now we have to write the convert from foci to BLAST. ugh. *)
let rec convertFociLval l = 
  (* this is weird ... since the arg: "l" is supposed to be a foci expression that is really an lval ! *)
  match convertFociExp l with
      E.Lval l' -> l'
    | _ -> failwith ("convertFociLval failed on argument"^(foci_expression_toString l))
    
and make_bin op e1 e2 = 
  E.Binary(op,convertFociExp e1, convertFociExp e2)
and make_eq e1 e2 = make_bin (E.Eq) e1 e2
and make_leq e1 e2 = make_bin (E.Le) e1 e2

and gatherFociSum el = 
  match el with
      [] -> E.Constant (Constant.Int 0)
    | h::tl -> 
		  List.fold_left 
	(fun a b -> E.Binary (E.Plus,a,convertFociExp b)) 
	(convertFociExp h) tl
	
and convertFociExp e = 
  match e with
	FociExpression.Constant (FociConstant.Int i) -> E.Constant (Constant.Int (int_map_from i))
    | FociExpression.Variable v -> 
	begin
	  match (Hashtbl.mem chlval_table v) with
	      true -> Hashtbl.find chlval_table v 
	    | false -> 
                if Misc.is_prefix "_STRINGCONSTANT" v then
                  let c = Misc.chop_after_prefix "_STRINGCONSTANT" v in 
                  let s = get_const_string c in
                  E.Constant (Constant.String s)
                else 
                    E.Lval (E.Symbol v)
	end
    | FociExpression.Application (f',el) ->
	begin
          let f = clean_foci_UF_string f' in
          match f with
            "select" ->
                (try
                  assert (List.length el = 2);
                  let (e1,e2) = (List.nth el 0, List.nth el 1) in
                  E.Select(convertFociExp e1, convertFociExp e2)
                  (* (match e1 with 
                    FociExpression.Variable _ -> E.Lval (E.push_deref (E.Dereference (convertFociExp e2)))
                   | _ -> E.Select(convertFociExp e1, convertFociExp e2))*)
                 with _ -> failwith "exn in convertFociExp @ select")
          | "store" ->
              (try
                assert (List.length el = 3);
                let (e1',e2',e3') = let l' = (List.map convertFociExp el) in (List.nth l' 0, List.nth l' 1, List.nth l' 2) in
                E.Store(e1',e2',e3')
               with _ -> failwith "exn in convertFociExp @ store") 
          | "chlval" -> 
		begin
		    match el with
                      [l;(FociExpression.Variable s)] -> E.Chlval (convertFociLval l, Misc.chop_after_prefix "_" s)
		    | _ -> failwith ("unknown chlval args in convertFociExp! "^(foci_expression_toString e))
		end
          | "*" -> 
		  begin
		    try
		      make_bin E.Mul (List.hd el) (List.nth el 2)
		    with _ ->
		      failwith ("unknown * args in convertFociExp! "^(foci_expression_toString e))
		  end
		  (* Hopefully we wont have to deal with wacko spl cases here! *)
	  
          | "addrOf" ->
		  begin
		    try 
		      let [e1] = el in
			E.Unary(E.Address, convertFociExp e1) 
		    with _ ->
		      failwith ("unknown & args in convertFociExp! "^(foci_expression_toString e))
		  end
	  
          | "sel" ->
		  begin
		    try 
		      let [e1;e2] = el in
			begin
			  match e2 with
			      FociExpression.Variable s ->
				begin
				  if (Misc.is_prefix "@field_" s)
				  then
				    E.Lval (E.Access(E.Dot, convertFociExp e1, (Misc.chop_after_prefix "@field_" s)))
				  else
				    E.Lval (E.Indexed(convertFociExp e1,convertFociExp e2))
				end

			    | _ -> (* JHALA: flaky! TBD:ARRAYCANON? *(x+i), x[i] *)
				if (e2 = FociAst.zero) then
				  E.Lval (
				  E.Dereference(convertFociExp e1))
				else 
				  E.Lval (
				    E.Indexed(convertFociExp e1,convertFociExp e2))
			end
		    with _ -> 
		      failwith ("unknown sel args in convertFociExp! "^(foci_expression_toString e))
				  
		  end
	  
          | "+" ->
		  begin
		    M.msg_string M.Normal "foci uses + in app";
		    gatherFociSum el
		  end
	      
          | "offset" -> 
              let [e1;e2] = List.map convertFociExp el in
              E.Binary(E.Offset,e1,e2)
          | "UIplus" ->
		  begin
		    M.msg_string M.Normal "foci uses UIplus in app";
		    gatherFociSum el
		  end
	  | "UIminus" ->
		  if (List.length el <> 2) then failwith "Bad use of UIminus";
		  let [e1;e2] = List.map convertFociExp el in
		    E.Binary(E.Minus,e1,e2)
          | "foffset" ->
                  begin
                    match el with
                      [e1;(FociExpression.Variable s)] -> 
                        let fld = if Misc.is_prefix "@field_" s then Misc.chop_after_prefix "@field_" s else failwith "bad foffset!" in
                        E.Binary (E.FieldOffset, (convertFociExp e1), (E.Lval (E.Symbol fld)))
                    | _ -> failwith "bad number of args to foffset!"
                  end
	      | _ -> 
		  begin
		    if (Misc.is_prefix "sel@field_" f )
		    then
		      (* a field "access" thing *)
		      let field_name = Misc.chop_after_prefix "sel@field_" f in
			try
			  let [e] = el in
			    E.Lval (E.Access(E.Dot, convertFociExp e, field_name))
			with _ -> failwith "multiple args to sel@field_ ! ?"
                    else
                      try 
                        let op = E.binaryOpFromVanillaString f in
                        let [e1;e2] = el in
                        E.Binary (op, convertFociExp e1,convertFociExp e2)
                      with _ ->
			E.FunctionCall(E.Lval (E.Symbol f), List.map convertFociExp el)
		  end
	end
      | FociExpression.Sum el -> gatherFociSum el
      | FociExpression.Coeff ((FociConstant.Int i),e') ->
	  if (i < 0) then 
	    if (i = (-1)) then
	      E.Unary (E.UnaryMinus, (convertFociExp e'))
	    else 
	      E.Binary(E.Mul, E.Constant (Constant.Int (int_map_from i)),
						   convertFociExp e')
	  else
	    if (i = 1) then convertFociExp e'
	    else
	      (E.Binary(E.Mul, E.Constant (Constant.Int (int_map_from i)), convertFociExp e'))
	      

and convertFociPred p =
  match p with
      FociPredicate.True -> P.True
    | FociPredicate.False -> P.False
    | FociPredicate.And plist -> P.And (List.map convertFociPred plist)
    | FociPredicate.Or plist -> P.Or (List.map convertFociPred plist)
    | FociPredicate.Not p' -> P.Not (convertFociPred p')
    | FociPredicate.Implies (p1,p2) -> P.Implies (convertFociPred p1,convertFociPred p2)
    | FociPredicate.Equality (e1,e2) -> P.Atom (make_eq e1 e2)
    | FociPredicate.Leq (e1,e2) -> P.Atom (make_leq e1 e2)
    | FociPredicate.Atom s -> P.Atom (E.Lval (E.Symbol s))

(* may raise "FociSatException" *)
let foci_plist_toString pl = String.concat "\n ; \n" (List.map foci_predicate_toString pl) 

let big_fq_count_ref = ref 0

let fq_dump num_cons cons_l_string =
  let foci_file_string = Printf.sprintf "\n \n \n %s \n \n \n" (cons_l_string) in
  let _file = 
    if num_cons > 2 then 
      (big_fq_count_ref:= !big_fq_count_ref + 1;
      (string_of_int !big_fq_count_ref)^".fq")
    else "fociqs" in
  let file = ((O.getValueOfString "mainsourcename")^"."^_file) in
  let _ = M.msg_string M.Normal ("writing to: "^file) in
  Misc.write_to_file file foci_file_string

(* now the somewhat non-trivial bit *)
(* cons_pre and cons_post are the two sets of constraints. This function returns the predicates
appearing in the fwd interpolant  *)


let check_interpolant 
  (itp : P.predicate) 
  (phi_1: P.predicate) (phi_2 : P.predicate) =  
  (* for now, only check that every symbol occurring in itp is in phi_1, and in phi_2 *)
  let syms_phi_1 = P.allVarExps phi_1 in
  let syms_phi_2 = P.allVarExps phi_2 in
  let syms_itp = P.allVarExps itp in
  let rv = 
    List.filter (fun x -> not ((List.mem x syms_phi_1) && (List.mem x syms_phi_2))) syms_itp
  in
    if rv <> [] then
      begin
	let s = Printf.sprintf "Interpolant issue: %s \n phi1: %s \n phi2: %s \n" 
		  (P.toString itp) (P.toString phi_1) (P.toString phi_1)
	in
	  M.msg_string M.Normal s;
	  M.msg_string M.Error s;
	  List.iter (fun e -> M.msg_string M.Normal (E.toString e)) rv
      end
    
      
(* itp_process_function: P.predicate -> 'a *)

let extract_and_process_interpolant itp_process_function (cons_pre,cons_post) = 
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let _ = M.msg_string M.Debug "In extract_and_process_interpolant"
  in
  let cons_l = List.map (fun x -> P.And x) [cons_pre;cons_post] in
  
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) cons_l 
  in
  let [foci_c_pre'; foci_c_post'] = List.map convertToFociSyntax cons_l in
  let [foci_c_pre; foci_c_post] = compile_sets [foci_c_pre'; foci_c_post'] in
  let cons_l_string = foci_plist_toString [foci_c_pre;foci_c_post] in
  try
      let _ = 
	M.msg_string M.Debug ">>>>>>>>>>>> calling foci";
	M.msg_string M.Debug (foci_predicate_toString foci_c_pre);
	M.msg_string M.Debug ";";
	M.msg_string M.Debug (foci_predicate_toString foci_c_post);
	M.msg_string M.Debug ">>>>>>>>>>>> foci done" 
      in
      let _ = if (O.getValueOfBool "fqdump") then fq_dump 2 cons_l_string in
      let foci_file_string = Printf.sprintf "\n \n \n %s \n ; \n  %s \n \n \n" (foci_predicate_toString foci_c_pre) (foci_predicate_toString foci_c_post)
          in
	if (false) then
          Misc.append_to_file "fociqs" foci_file_string ;
 let fwd_ip = 
      try 
        foci_interpolate (foci_c_pre,foci_c_post) 
      with e -> 
	(M.msg_string M.Error ("Foci FAILS!(eap_itp) raises exception!!"^ (Printexc.to_string e));
	 M.msg_string M.Normal ("Foci FAILS!(eap_itp) raises exception!!"^ (Printexc.to_string e));
	 Misc.append_to_file "foci_failed_q" foci_file_string;
	FociPredicate.True (* FociPredicate.And [foci_c_pre;foci_c_post ] *))
    in
      let _ = M.msg_string M.Debug ("Foci yields Interpolant: "^(foci_predicate_toString fwd_ip))
      in
      let fwd_ip_b = convertFociPred fwd_ip in
      let _ = M.msg_string M.Debug ("Foci yields b_Interpolant: "^(P.toString fwd_ip_b))
      in
	(* check if the interpolant is clean *)
      let _ = 
	match cons_l with
	    [c1;c2] -> check_interpolant fwd_ip_b c1 c2 
	  | _ -> failwith "this cannot happen"
      in
	itp_process_function fwd_ip_b
    with
	FociSatException -> (* should be that the thing was SAT *)
	  failwith "Foci raised SAT!"
      | e -> raise e(*failwith "Foci raises some other exception..."*)



let extract_foci_preds = extract_and_process_interpolant P.getAtoms

let extract_foci_preds_parity = 
  extract_and_process_interpolant (P.getAtoms_parity true)

let extract_foci_interpolant = extract_and_process_interpolant (fun x -> x)



(* cut and paste from the above function -- but here the argumentes are not lists!*)
let interpolant_sat (cons_pre,cons_post) =
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let cons_l = [cons_pre;cons_post] in
  let _ = M.msg_string M.Debug "In interpolant_sat:" in
  let _ = List.iter (fun x -> M.msg_string M.Debug (P.toString x)) cons_l 
  in
  let [foci_c_pre; foci_c_post] = List.map convertToFociSyntax cons_l in
  let foci_file_string = Printf.sprintf "\n \n \n %s \n ; \n  %s \n \n \n" (foci_predicate_toString foci_c_pre) (foci_predicate_toString foci_c_post)
  in
    try  
      let fwd_ip = foci_interpolate (foci_c_pre,foci_c_post) in
      let _ = M.msg_string M.Debug ("Foci yields Interpolant: "^(foci_predicate_toString fwd_ip))
      in
      let fwd_ip_b = convertFociPred fwd_ip in
      let _ = M.msg_string M.Debug ("Foci yields b_Interpolant: "^(P.toString fwd_ip_b))
      in
	Unsat(fwd_ip_b)
    with 
	FociSatException -> (M.msg_string M.Debug "Foci says SAT!"; Sat)
      | FociSatExceptionAsgn _ -> (M.msg_string M.Debug "Foci says SATa!"; Sat)
      | e ->
	  begin
	    M.msg_string M.Error ("Foci FAILS!(isat) raises exception!!"^ (Printexc.to_string e));
	    M.msg_string M.Error ("Foci FAILS!(isat) raises exception!!"^ (Printexc.to_string e));
	    Misc.append_to_file "foci_failed_q" foci_file_string;
	  raise e
 end
	      	  
let interpolant_implies a b = 
  match interpolant_sat (a, (P.negate b)) with
      Sat -> false
    | Unsat _ -> true
  
let interpolant_unsat a = interpolant_implies a P.False

let extract_and_process_interpolantN itp_fun foci_syn_conv itp_process_function cons_l : P.predicate list = 
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let _ = M.msg_string M.Debug "In extract_and_process_interpolantN:" in
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) cons_l 
  in
  let n_cons = List.length cons_l in
  let foci_cons_l' = List.map foci_syn_conv cons_l in
  let foci_cons_l = compile_sets foci_cons_l' in
  let cons_l_string = foci_plist_toString foci_cons_l in
  let _ =
    M.msg_string M.Debug ">>>>>>>>>>>> calling foci";
    M.msg_string M.Debug cons_l_string ;
    M.msg_string M.Debug ">>>>>>>>>>>> foci done" 
  in
  let _ = if (O.getValueOfBool "fqdump") then fq_dump n_cons cons_l_string in
  let itp_l = 
    try 
      let rv = (* foci_interpolateN *) itp_fun foci_cons_l in
      M.msg_string M.Debug "FOCI returns";
      rv
    with
      FociSatException -> (M.msg_string M.Normal "Foci says SAT!"; raise FociSatException)             
    | FociSatExceptionAsgn a -> (M.msg_string M.Normal "Foci says SATa!"; raise (FociSatExceptionAsgn a))
    | e -> 
	(M.msg_string M.Normal ("Foci FAILS!(eap_itpN) raises exception!!"^ (Printexc.to_string e));
	 M.msg_string M.Error ("Foci FAILS!(eap_itpN) raises exception!!"^ (Printexc.to_string e));
	 let foci_file_string = Printf.sprintf "\n \n \n %s \n \n \n" (cons_l_string) in
         Misc.append_to_file "foci_failed_q" foci_file_string;
         raise FociSatException)
    in
    let itp_l_string = foci_plist_toString itp_l in 
    let _ = M.msg_string M.Debug ("Foci yields Interpolants: \n"^itp_l_string) in
    let itp_b_l = List.map convertFociPred itp_l in
    let itp_b_l_string = String.concat "\n ; \n" (List.map P.toString itp_b_l) in
    let _ = M.msg_string M.Debug ("Foci yields b_Interpolant: "^itp_b_l_string) in
	List.map itp_process_function itp_b_l

let conv_fun p = 
  if (O.getValueOfBool "fmc") then 
    (foci_UF_idx_tick (); convertToFociSyntax p) 
  else (convertToFociSyntax p)
  
let extract_foci_interpolantN = 
  extract_and_process_interpolantN foci_interpolateN conv_fun  (fun x -> x)

let extract_foci_interpolantN_noconv =
  extract_and_process_interpolantN foci_interpolateN convertToFociSyntax (fun x -> x)
  
let extract_foci_interpolantN_symm arg =
 set_disjoint_flag ();
 try 
   let rv =  extract_and_process_interpolantN foci_interpolateN_symm conv_fun (fun x -> x) arg in
   unset_disjoint_flag ();
   (rv,[])
  with e -> unset_disjoint_flag (); raise e
  
let extract_minp_interpolantN cs =
  M.msg_string M.Debug "extract_minp_itpN";
  let _ = ignore(extract_foci_interpolantN cs) in
  let n = List.length cs in
  let ca = Array.of_list cs in
  let f i = 
    M.msg_string M.Debug "extract_minp_itpN:tick";
    try ignore(extract_foci_interpolantN (Misc.array_to_list_n ca (i+1))); true
    with FociSatException | FociSatExceptionAsgn _ -> false in
  let k = Misc.binary_search f (0,n-1) in
  M.msg_string M.Debug (Printf.sprintf "extract_minp_itpN: k/n = %d/%d" k n);
  let cs' = (Misc.array_to_list_n ca (k+1)) @ (Misc.clone P.True (n-k-1)) in
  extract_foci_interpolantN cs'


let extract_foci_interpolantN_dasdill trel_cons_l cube_cons_l =
  assert (List.length trel_cons_l + 1 = List.length cube_cons_l);
  let _ = Hashtbl.clear chlval_table in (* reset the string -> chlval map *)
  let _ = Hashtbl.clear chlval_map_table in
  let _ = M.msg_string M.Debug "In extract_and_process_interpolantN_dasdill:" in
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) trel_cons_l in
  let _ = List.iter (fun x -> M.msg_printer M.Debug P.print x) cube_cons_l in
  let n_trel_cons = List.length trel_cons_l in
  let foci_trel_cons_l = List.map conv_fun trel_cons_l in
  let foci_cube_cons_l = List.map conv_fun cube_cons_l in
  let trel_cons_l_string = foci_plist_toString foci_trel_cons_l in
  let cube_cons_l_string = foci_plist_toString foci_cube_cons_l in
  let _ =
    M.msg_string M.Debug ">>>>>>>>>>>> calling foci:trel";
    M.msg_string M.Debug trel_cons_l_string ;
    M.msg_string M.Debug ">>>>>>>>>>>>cube>";
    M.msg_string M.Debug cube_cons_l_string ;
    M.msg_string M.Debug ">>>>>>>>>>>> done"
  in
  let itp_l = 
    try 
      let rv = foci_interpolateN_dasdill foci_trel_cons_l foci_cube_cons_l in
      M.msg_string M.Debug "FOCI DasDill returns";
      rv
    with
    FociSatException -> (M.msg_string M.Normal "Foci says SAT!"; raise FociSatException)             
  | FociSatExceptionAsgn a -> (M.msg_string M.Normal "Foci says SATa!"; raise (FociSatExceptionAsgn a))
  | e -> 
	(M.msg_string M.Normal ("Foci FAILS!(eap_itpN_dd) raises exception!!"^ (Printexc.to_string e));
	 M.msg_string M.Error ("Foci FAILS!(eap_itpN_dd) raises exception!!"^ (Printexc.to_string e));
         raise FociSatException)
    in
    let itp_l_string = foci_plist_toString itp_l in 
    let _ = M.msg_string M.Debug ("FociDD yields Interpolants: (Negate if not TRUE) \n"^itp_l_string) in
    let itp_b_l = List.map convertFociPred itp_l in
    let itp_b_l_string = String.concat "\n ; \n" (List.map P.toString itp_b_l) in
    let _ = M.msg_string M.Debug ("Foci yields b_Interpolant: "^itp_b_l_string) in
       itp_b_l


(* incremental foci interface -- using foci just as a DP 
0. reset: unit -> unit
1. push: pred-> unit 
2. pop: unit -> unit
3. check_sat: unit -> bool
*)


(* foci 3 stuff *)
let foci_instance_ref = 
  let inst = Foci2.new_instance 1 false true in
  ref (inst,0)
      

let foci_inc_pop () = 
  let (inst,ht) = !foci_instance_ref in 
  if ht > 0 then 
    begin
      M.msg_string M.Debug "fociinc:pop";
    Foci2.pop inst;
    foci_instance_ref := (inst, ht-1)
    end
  
let foci_inc_push p =
  let (foci_inst,ht) = !foci_instance_ref in
  let p' = FociDag.pred_dag_of_tree (convertToFociSyntax p) in
  Foci2.push foci_inst;
  Foci2.add foci_inst 0 p';
  M.msg_string M.Debug "fociinc:push";
  foci_instance_ref := (foci_inst,ht+1)

let foci_inc_check_sat () =
  let (inst,ht) = !foci_instance_ref in 
  try ignore(Foci2.solve inst); 
  M.msg_string M.Debug ("fociinc: ht="^(string_of_int ht)^".");
  false 
  with Foci2.SAT (_,_) -> 
    true

let foci_inc_check_unsat p = 
  let _ = foci_inc_push p in
  let rv = foci_inc_check_sat () in
  let _ = foci_inc_pop () in
  not rv

let foci_inc_is_contra () = not (foci_inc_check_sat ())
  
let foci_inc_reset () =
  let inst = Foci2.new_instance 1 false true in
  foci_instance_ref := (inst,0)
(*  let (_,ht) = !foci_instance_ref in
  Misc.repeat_fn foci_inc_pop ht *)  


 
(******************************  End Interface to FOCI  ******************************)

(* Incremental DP wrappers for REFINEMENT (not cartesian post, see postBdd) *)

	
let queryExpContext pred query =
  if (!tp = "") then tp := "simplify";
  if (!tp = "simplify" ||  !tp = "simplify-bits") then
    (Bstats.time "simplify querytime" (queryExpUsingSimplify) query) 
  else if (!tp = "cvc") then queryExpUsingCvc query
  else if (!tp = "foci") then interpolant_implies pred P.False
  else 
    begin 
      let vam_query = Input.pushNeg (convertToVampyreSyntax query) in
	vampyre_prove_context vam_query
    end

let rec queryExp exp =
  M.msg_string M.Debug ("Query : ");
  M.msg_printer M.Debug P.print exp; 
  if (!tp = "vampyre") then
    queryExpUsingVampyre exp
  else if (!tp="cvc") then
    queryExpUsingCvc exp
  else if (!tp = "simplify" || !tp = "simplify-bits" ) then
      (Bstats.time "simplify querytime" (queryExpUsingSimplify) exp) 
  else if (!tp = "foci") then interpolant_implies (P.Not exp) P.False
  else
      begin
	tp :=  O.getValueOfString "s";
	if (!tp = "ics" || !tp = "vampyre" || !tp = "simplify" || !tp = "cvc" || !tp = "simplify-bits") then
	  queryExp exp
	else
	  failwith ("TheoremProver: queryExp: Unknown theorem prover "^(!tp)^". Options are foci, simplify, cvc and vampyre.")
      end




(************************************************************************************)

(*************************************************************************************)


let block_assert p = 
  M.msg_string M.Debug ("bs_assert: ");
  M.msg_printer M.Debug P.print p;
  let tp = O.getValueOfString "incdp" in
  simp_ctr := !simp_ctr + 1;
   if (tp = "simplify" || tp = "simplify-bits") then simplify_assume () p
   else if (tp = "cvc") then cvc_assume p
   else if (tp = "foci") then foci_inc_push p
   else failwith ("block_assert: unknown incdp: "^tp)

let block_pop () =
  M.msg_string M.Debug  "bs_pop"; 
  assert (!simp_ctr > 0);
  simp_ctr := !simp_ctr - 1;
  let tp = O.getValueOfString "incdp" in
  if (tp = "simplify" || tp = "simplify-bits") then simplify_pop_noflush ()
  else if (tp = "cvc") then cvc_pop ()
  else if (tp = "foci") then foci_inc_pop ()
  else failwith ("block_pop: unknown incdp "^tp)

    
let block_reset () = 
  M.msg_string M.Debug "bs_reset";
  let tp = O.getValueOfString "incdp" in
  if (tp = "simplify" || tp = "simplify-bits") then
    begin
      Misc.repeat_fn (simplify_pop) (!simp_ctr);
      simp_ctr := 0
    end
  else if  (tp = "cvc") then 
    begin
      Hashtbl.clear varCvcHashtbl;
      Hashtbl.clear chlvalCvcHashtbl;
      clear_varCvcHashtbl_flag := true;
      simp_ctr :=0;
      cvcPushes:=0;
      reset_cvc_ground ();
      cvcServer := None (* kill cvc; let garbage collect do it *)
    end
  else if tp = "foci" then foci_inc_reset () 
  else failwith ("block_reset: Strange theorem prover! "^tp)

let rec _is_contra () = 
  let tp = O.getValueOfString "incdp" in
  if (tp = "simplify" || tp = "simplify-bits") then 
    begin
      M.msg_string M.Debug "simplify check contra";
      flush stdout;
      try
	let rv = queryExpUsingSimplify (P.False) in
	  M.msg_string M.Debug ("Contra ? "^(string_of_bool rv));
	  rv
      with e -> 
	failwith ("Simplify raised exception "^(Printexc.to_string e)^" in simplify_is_contra")
    end
  else if (tp = "cvc") then
    begin 
      M.msg_string M.Debug "cvc check contra";
      let rv = queryExpUsingCvc (P.False) in
	M.msg_string M.Debug ("Contra ? "^(string_of_bool rv));
	rv
    end
  else if (tp = "foci") then foci_inc_is_contra () 
  else failwith ("is_contra: unknown incdp "^tp)

let check_unsat p =
    let _ = block_assert p in
    let rv = _is_contra () in
    let _ = block_pop () in
      rv

let rec _assume pred =
  let tp = O.getValueOfString "incdp" in 
  if (tp = "simplify" || tp = "simplify-bits") then simplify_assume () pred
   else if (tp = "cvc") then cvc_assume pred
   else if (tp = "foci") then foci_inc_push pred 
   else failwith ("_assume: unknown dp "^tp)

let _pop () = 
  let tp = O.getValueOfString "incdp" in 
  if (tp = "simplify" || tp = "simplify-bits") then simplify_pop ()
  else if (tp = "cvc") then cvc_pop ()
  else if (tp = "foci") then foci_inc_pop ()
  else failwith ("_pop: unknown dp "^tp) 

(*******************************************************************************)

let generateTest p =
	failwith "Test generation not supported"
(*
  (* Data structures used *)
(* let ac_generateTest p = *)
  let id_to_sym_ht = Hashtbl.create 31 in
  let sym_to_id_ht = Hashtbl.create 31 in
  let number_of_vars = ref 0 in
  let get_next_id () = incr number_of_vars ; !number_of_vars
  in 
  (* Helper functions *)
  let find_variables ap =
    match ap with 
      P.Atom aex | P.Not (P.Atom aex) ->
	let rec _trawl_exp aex =
	  match aex with
	    E.Lval _ (* add to hash table *)
	  | E.Chlval _ -> (* add to hash table *)
	      if Hashtbl.mem sym_to_id_ht aex then
		()
	      else 
		begin
		  let n = get_next_id () in
		  Hashtbl.add sym_to_id_ht aex n ;
		  Hashtbl.add id_to_sym_ht n aex ;
		  ()
		end
	  | E.Binary (E.FieldOffset, e1, e2) -> failwith "Do not handle field offset"
	  | E.Binary (_, e1, e2) ->
	      _trawl_exp e1 ; _trawl_exp e2
	  | E.Constant _ -> ()
	  | E.Unary (_, e) -> _trawl_exp e
	in
	_trawl_exp aex
    | _ -> (* cannot happen *) failwith "In find_variables: atomic predicate expected"
  in
  let get_lp_constraints aex =
    match aex with
      E.Binary (relop, e1, e2) ->
	begin
	  let (coeffs : int array) = Array.make (!number_of_vars + 1) 0 in (* coefficients for variables *)
	  let const = ref 0 in
	  let rec get_coeffs e pos_or_neg =
	    match e with 
	      E.Constant c ->
		begin
		  match c with Constant.Int i -> const := !const + (if pos_or_neg then i else -i) 
		  | _ -> failwith "add_lp_constraints: Only handle integers"
		end
	    | E.Lval _
	    | E.Chlval _ -> 
		let id = Hashtbl.find sym_to_id_ht e in
		coeffs.(id) <- coeffs.(id) + (if pos_or_neg then 1 else -1)
	    | E.Binary (E.Plus, e1, e2) ->
		get_coeffs e1 pos_or_neg ; get_coeffs e2 pos_or_neg
	    | E.Binary (E.Minus, e1, e2) ->
		get_coeffs e1 pos_or_neg ; get_coeffs e2 (not pos_or_neg)
	    | E.Binary (E.Minus, e1, e2) ->
		get_coeffs e1 pos_or_neg ; get_coeffs e2 (not pos_or_neg)
	    | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Lval l)
	    | E.Binary (E.Mul, E.Lval l, E.Constant (Constant.Int i)) ->
		let id = Hashtbl.find sym_to_id_ht (E.Lval l) in
		coeffs.(id) <- coeffs.(id) + (if pos_or_neg then i else -i)
	    | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Chlval (l,s))
	    | E.Binary (E.Mul, E.Chlval(l,s), E.Constant (Constant.Int i)) ->
		let id = Hashtbl.find sym_to_id_ht (E.Chlval (l,s)) in
		coeffs.(id) <- coeffs.(id) + (if pos_or_neg then i else -i)
	    | E.Binary (E.Mul, E.Constant (Constant.Int i), E.Constant (Constant.Int j)) ->
		let ij = i * j in const := !const + (if pos_or_neg then ij else -ij) 
	    | E.Unary (E.UnaryPlus, e1) -> get_coeffs e1 pos_or_neg
	    | E.Unary (E.UnaryMinus, e1) -> get_coeffs e1 pos_or_neg
	    | _ -> failwith ("get_coeffs: failing on "^(E.toString e))
	  in
	  get_coeffs e1 true ;
	  get_coeffs e2 false ;
	  (coeffs, !const, relop) 
	end
    | _ -> failwith ("get_lp_constraints: Strange expression "^(E.toString aex))
  in

  (* main function *)
  match p with
    P.And l ->
      begin
	if (List.exists 
	     (fun ap -> match ap with 
		P.Atom _ | P.Not (P.Atom _) -> false | _ -> true) l) then
	  failwith "generateTest: Assumes conjunction of atomic predicates"
	else 
	  begin (* Now start the work *)
	    (* first do a trawl and find out the number of variables. In the process, also
	       populate the two hash tables name -> id, and id -> name
	       *)
	    List.iter find_variables l ;
	    (* then for each atomic formula, generate the corresponding LP constraint *)
            let get_aexp ap =
              match ap with P.Atom aex -> aex 
              | P.Not (P.Atom aex) -> E.negateRel aex
              | _ -> failwith "Non atomic!"
            in
	    let constraints = List.map (fun ap -> get_lp_constraints (get_aexp ap)) l in 
	    (* first cut : assume no Neq *)
	    let linprog = Lp.lp_make_lp 0 !number_of_vars in
	    let add_constraints (coeffs, const, relop) =
	      match relop with
	      |	E.Le -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const)) 
	      |	E.Ge -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const)) 
	      |	E.Eq -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 1 (float_of_int (-const)) 
	      |	E.Gt -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const+ 1)) 
	      |	E.Lt -> Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const- 1)) 
	      |	E.Ne -> if (Random.float 1.0< 0.5) then Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 2 (float_of_int (-const+ 1)) 
				else Lp.lp_add_constraint linprog (Array.map float_of_int coeffs) 0 (float_of_int (-const- 1)) 
	      |	_ -> failwith "add_con: not handled"
	    in
	    List.iter (fun c -> ignore (add_constraints c)) constraints ;
	    (* for each variable, set lower bound to -infinity *)
	    Lp.lp_set_obj_fn linprog (Array.make (!number_of_vars + 1) 0.0) ;
	    (* if (O.getValueOfBool "quiet") then () else Lp.lp_print_lp linprog ; *)
            let stillnotdone = ref true in

            let set_minus_inf = ref false in
            while !stillnotdone do
	    begin 
              stillnotdone := false ;
              M.msg_string M.Normal "Next round of solving Linprog" ;
	      (* now solve linprog *)
	      let retcode = (Lp.lp_solve linprog) in
	      if retcode = 2 then (* infeasible! *) begin
                (if !set_minus_inf then failwith "LP is infeasible!"
                else set_minus_inf := true );
	        let (minus_infinity : float) =  -. (Lp.lp_get_infinite (linprog) ) in
	        for i = 1 to !number_of_vars do ignore (Lp.lp_set_lowbo linprog i minus_infinity) done ; 
	        stillnotdone := true
                end
              else 
              begin (* check that the solution is integral *) 
(*
	      if (O.getValueOfBool "quiet") then () else Lp.lp_print_lp linprog ;
	      if (O.getValueOfBool "quiet") then () else Lp.lp_print_solution linprog ;
*)
              let sol_array = Lp.lp_get_solution linprog in
              for i = 1 to !number_of_vars do
                if (floor sol_array.(i)  <> sol_array.(i) ) then
                begin
	          M.msg_string M.Normal "Solution not integral!";
                  stillnotdone := true;
                  ignore (Lp.lp_set_int linprog i 1 ) ; 
                end ; 
              done
              end
            end done ;
	    (* ok now we have the map from lvalues to values *)
	    let sol = Lp.lp_get_solution linprog in
	    let make_list ht sol =
              let l = ref [] in
              for i = 1 to !number_of_vars do
                l := (Hashtbl.find ht i, sol.(i)) :: !l ;
              done ;
              !l
            in
	    make_list id_to_sym_ht sol
	  end
      end
  | _ -> failwith "generateTest: Assumes conjunction of atomic predicates (not a conjunction)"



*)
