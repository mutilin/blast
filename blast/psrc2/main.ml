(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module O = Options
module M = Message
module P = Ast.Predicate
module E = Ast.Expression
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = BlastCSystemDescr.C_Command
module S = Bstats
module AA = AliasAnalyzer
module A = Abstraction.C_Abstraction
module TP = TheoremProver
module CFBMC = CfbLazyModelChecker.Make_Lazy_Model_Checker(A)
module FMC = FociModelChecker.Make_Foci_Model_Checker(A)

let final_string_ref = ref ""
let preds_ref = ref P.True

let read_preds fn =
  try
    if fn = "" then [] else 
      let _ = M.msg_string M.Normal "Reading in seed predicates..." in
      let ic = open_in fn in
      let rv = 
        let _ = VampyreErrormsg.startFile fn in
        let lexbuf = Lexing.from_channel ic in
        let _ = VampyreErrormsg.theLexbuf := lexbuf in
        Inputparse.main Inputflex.token lexbuf in
      let _ = close_in ic in 
      let _ = M.msg_string M.Normal "Seed predicates read." in rv	
  with Sys_error _ | VampyreErrormsg.Error -> 
    (M.msg_string M.Error ("Error raised reading preds in: "^fn); [])

let runmodelcheck () =
  M.msg_string M.Normal "****** Now running foci model checker ******";flush stdout;
  let _ = A.initialize_abstraction () in
  let r0 = A.create_region false !preds_ref P.True in 
  let err = A.create_region true P.True P.True in
  let mc = if O.getValueOfBool "fmc" then FMC.model_check else CFBMC.model_check in
  try S.time "Model Checking" mc r0 err
  with e -> (A.dump_abs (); A.print_stats (); raise e)

let print_header () = 
  M.msg_string M.Normal "BLAST 2.4, Copyright (c) 2002-2007, The BLAST Team.";
  M.msg_string M.Normal "This program contains Foci, Copyright (c) 2003, Cadence Berkeley Laboratories," ;
  M.msg_string M.Normal "Cadence Design Systems. All rights reserved.";
  M.msg_string M.Debug "Blast was run with the following command line:";
  Array.iter (fun s -> M.msg_string M.Debug (s^" ")) Sys.argv

let read_options () = 
  Ast.Counter.set_max (O.getValueOfInt "maxcounter"); 
  (if O.getValueOfBool "msvc" then Cil.msvcMode := true);
  (if (O.getValueOfBool "simpquery") then TP.set_simplify_query_flag ());
  preds_ref := P.conjoinL (read_preds (O.getValueOfString "pred"))

let parse_files () = 
  let srcs = O.getSourceFiles () in
  (if srcs = [] then (Misc.error "no input sources")); 
  M.msg_string M.Normal ("Begin Parsing: " ^ (Misc.strList srcs));
  C_SD.initialize_blast (C_SD.initialize_cil srcs);
  M.msg_string M.Normal ("done Parsing");
  M.msg_string M.Debug "Printing System Description:"; 
  M.msg_printer M.Debug C_SD.print_system ()

let alias_analysis () = 
  S.time "Alias DB" AA.constructAliasDatabase (); 
  M.msg_string M.Normal ("done aliasDB");
  AA.constructMustAliasDatabase (); 
  M.msg_string M.Normal ("done mustAliasDB");
  S.time "Mods DB" (C_SD.post_alias_analysis AA.get_lval_aliases_scope) ();
  M.msg_string M.Normal ("done modsDB");
  AA.add_alias_override (P.conjoinL (read_preds (O.getValueOfString "aliasfile"))); 
  M.msg_string M.Normal "done overRideDB"

let call_graph () = 
  C_SD.compute_callgraph ();
  let fn = O.getValueOfString "callgraph" in
  (if fn <> "" then C_SD.output_callgraph fn);
  C_SD.output_call_paths ()

let finish () = 
  let memstats = Gc.stat () in
  M.msg_string M.Normal ("Minor Words : "^(string_of_float memstats.Gc.minor_words)) ;
  M.msg_string M.Normal ("Major Words : "^(string_of_float memstats.Gc.major_words)) ;
  M.msg_string M.Normal ("Total size of heap in words : "^(string_of_int memstats.Gc.heap_words)) ;
  (if not (O.getValueOfBool "quiet") then S.print stderr "Timings:\n");
  M.msg_string M.Normal !final_string_ref

let _ =
try 
  M.set_verbosity M.Default;  O.buildOptionsDatabase ();
  S.reset ();print_header (); 
  Cil.initCIL (); parse_files (); 
  alias_analysis (); call_graph ();
  (if O.getValueOfString "query" <> "" then failwith "TBD:runquery see qmain.ml" 
   else runmodelcheck ());
  finish ()
with e -> (M.msg_string M.Error ("Ack! The gremlins again!: "^(Printexc.to_string e)); raise e)
