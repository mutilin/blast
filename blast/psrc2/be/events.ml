(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

open BlastArch

module E = Ast.Expression
module P = Ast.Predicate
module M = Message
module C_SD = BlastCSystemDescr.C_System_Descr
module PT = PredTable
module Command = C_SD.Command 

module Events = 
struct
  type eventHandler = string * int list
  
  let eventHandler_to_string (s,is) = "Event: "^s^" : "^(Misc.string_of_int_list is)
  
  let int_to_expr i = Ast.Expression.Constant (Ast.Constant.Int i)  
    
  let expr_to_int e = 
    match e with 
      Ast.Expression.Constant (Ast.Constant.Int i) -> i
    | _ -> (assert false; failwith "bad call to expr_to_int")

  let eventHandler_to_command (s,l) = 
    {Command.code = Command.FunctionCall (E.FunctionCall(E.Lval(E.Symbol(s)),List.map int_to_expr l));
     Command.read = [];
     Command.written = [];} 

  let _plug_args op args =
    let cmd' = 
      match (C_SD.get_command op).Command.code with
        Command.FunctionCall (E.FunctionCall(E.Lval(E.Symbol s) ,_)) 
      | Command.FunctionCall (E.Assignment (_,_,E.FunctionCall (E.Lval (E.Symbol s), _))) ->
          {Command.code = Command.FunctionCall (E.FunctionCall(E.Lval(E.Symbol s), args));
           Command.read = [];
           Command.written = [];}
      | _ -> (assert false;failwith "error in Events._plug_args") in
    C_SD.make_edge (C_SD.get_source op) (C_SD.get_target op) cmd' 

  let lit_to_int (i,b) = 2*i + (if b then 1 else 0)
  
  let int_to_lit n = 
    let rv = (n / 2, n mod 2 <> 0) in
    assert (n = (lit_to_int rv));
    rv

  let get_lit_bdd (i,b) = 
    let bddi = PT.getPredIndexBdd i in
    if b then bddi else CaddieBdd.bddNot bddi

  let deconstructAsyncCall op =
    M.msg_string M.Debug ("in deconstructAsyncCall:"^(C_SD.edge_to_string op));
    match (C_SD.get_command op).Command.code with
      Command.FunctionCall (E.FunctionCall (E.Lval (E.Symbol s), args)) 
    | Command.FunctionCall (E.Assignment (_,_,E.FunctionCall (E.Lval (E.Symbol s), args))) ->
        let is = List.map expr_to_int args in
        let ls = List.map int_to_lit is in
        let bdd = List.fold_left (fun b l -> CaddieBdd.bddAnd b (get_lit_bdd l)) PT.bddOne ls in
          ((s,is),bdd)
    | _ -> (assert false; failwith "error in Events.deconstructAsyncCall")

      
  let constructAsyncCall (op,bdd) =
    M.msg_string M.Debug (Printf.sprintf "in constructAsyncCall: %s : %s" 
    (C_SD.edge_to_string op) (P.toString (PT.convertBddToPred bdd)));
    let lits = PT.lits_of_cube (PT.bdd_cube_cover bdd) in
    let lits' = List.filter (fun (i,_) -> not (PT.is_pred_global i)) lits in
    let is = List.map lit_to_int lits' in
    let rv = _plug_args op (List.map int_to_expr is) in
    let _ = deconstructAsyncCall rv in
    rv

end


