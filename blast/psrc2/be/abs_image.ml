(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


(* This file has code for image computation -- wp, post, summary_post *)
     
module Op = Abs_op.Operation
module R = Abs_region.Region
module O = Options
module PT = PredTable
module TP = TheoremProver
module IM = Misc.IntMap
module IS = Misc.IntSet
module CB = CaddieBdd
module DL = Lattice.DataLattice
module E = Ast.Expression
module P = Ast.Predicate
module M = Message
module AA = AliasAnalyzer
module U = Abs_util
module C_SD = BlastCSystemDescr.C_System_Descr
open Abs_op.Defs

let statTotalNumberOfSummaryPosts = ref 0
let maxStackLen = ref 0 

(********************************************************************************)
(*************************  weakest preconditions *******************************)
(********************************************************************************)

let make_sub mcflag lv (e,v) =
  if not mcflag then (e,v) else 
    match U.mc_var lv with 
      None -> (e,(U.to_mccarthy v)) 
    | Some m -> (m,E.Store(m,U.to_mccarthy (E.mc_addressOf (E.Lval lv)),
    (U.to_mccarthy v)))
  
let wp_one pred t v mustAliases aliasSet =
  let mks = make_sub (O.getValueOfBool "mccarthy") t in
  let aliasExps = 
    List.map 
      (function (b,e) -> let tAddr = E.addressOf (E.Lval t) and eAddr = E.addressOf e in 
       let (a,a') = U.simplify_address_equality (tAddr,eAddr) in
       if b then P.Atom (E.Binary(E.Eq,a ,a')) else P.Atom (E.Binary(E.Ne,a ,a'))) 
    aliasSet in
  let ac_aliases = (true, E.Lval t) ::(List.filter fst aliasSet) in
  let boolexp = match v with E.Binary (op, _, _) when E.isRelOp op -> true | _ -> false in
  let vsub = if  boolexp then U.getNewTmpVar t else v in
  let p1 =
    let subs = mustAliases @ (List.map snd ac_aliases) in
    let substSet = List.map (fun e -> mks (e,vsub)) subs in 
    P.substitute substSet pred in
  let p2 = 
    if boolexp then 
      let [v0;v1] = List.map (fun n -> E.Constant (Ast.Constant.Int n)) [0;1] in 
      P.disjoinL [P.conjoinL [P.Atom (U.to_mccarthy v); (P.expr_equate vsub v1)];
                  P.conjoinL [P.Atom (E.negateRel (U.to_mccarthy v)); (P.expr_equate vsub v0)]]
    else P.True in
  (P.conjoinL (p1::p2:: aliasExps)) 
  
 let wp qp asgns =
   List.fold_left 
     (fun qp (lv,e) -> 
       let (qs, p) = P.strip_quantifier_prefix qp in
       let aliasSets = 
         if O.getValueOfBool "mccarthy" then [[]] else
           let possibleAliases = (U.getPossibleAliases lv) p in
           Misc.powerSet (List.filter ((<>) (E.Lval lv)) possibleAliases) in
       let musts = Stats.time "getMustAliases" (U.getMustAliases lv) p in
       let ap = (P.disjoinL (List.map (wp_one p lv e musts) aliasSets)) in
       P.stick_quantifier_prefix ap qs) 
   (U.pred_to_mccarthy qp) (List.rev asgns)

(********************************************************************************)

(* Self-recursion. See test/cftest/rec4.c for an example.
   * If there is a call to f() inside f... then we rename:
     * 1. the "Formals" of f, 
     * 2. the symlvals of f,
     * 3. the predicates inside f
     So that they don't clash with the caller region.
     Note that the funcall assignments are: fml = actual and sym_fml = fml.
     For the former, we rename only the fml, in the latter we rename both
     sym_fml and fml as they both correspond to the callee region.
   *)
  

let (self_rename_p, self_rename_lv, self_rename_e) = 
  let f lv =
    match lv with E.Symbol s ->
      if (String.contains s '@') then (E.Symbol (s^"BLAST")) else lv
    | _ -> lv in
  (P.alpha_convert f,E.alpha_convert_lval f,E.alpha_convert f)
  
let self_rename_ga (p,asgns) =
  let f (lv,e) = 
    (self_rename_lv lv, if E.is_symbolic lv then self_rename_e e else e) in
  (self_rename_p p, List.map f asgns)
  
let is_self_recursion op =
  match Op.get_info op with
    Op.Call (_,_,f,_) when f = C_SD.get_location_fname (C_SD.get_source op) -> true 
  | _ -> false 

let is_recursion s = 
  if O.getValueOfBool "cf" then false else 
    (match s with [] -> false | (h::t) -> Misc.list_count h t > 2)

let cube_sweep unmod cube = 
  Array.fold_left 
    (fun (i,p,pm') vi ->
      if vi <> 0 && vi <> 1 then (i+1,p,pm') else
        let b = (vi = 1) in
        let pi = PT.getPred i in
        (i+1,(if b then pi else (P.negate pi))::p, 
         if IS.mem i unmod then IM.add i b pm' else pm'))
    (0,[],IM.empty) cube 

let getBdd i b = 
  let bdd = PT.getPredIndexBdd i in
  if b then bdd else (CB.bddNot bdd)

let cube_query (asm,pre) post_m pts = 
try
  let asm' = asm::pre in
  TP.block_reset (); TP.block_assert (P.And asm'); 
  let ps' = 
    Misc.map_partial
    (fun (i,wpp,wpn) -> 
       try Some (i,IM.find i post_m) with Not_found ->
         let a1 = U.askTheoremProverContext (P.And (wpn::asm')) wpp in
         let a2 = U.askTheoremProverContext (P.And (wpp::asm')) wpn in
         if a1 && a2 then raise EmptyPost 
         else if not (a1 || a2) then None else Some (i,a1))
    pts in
  List.fold_left (fun bdd (i,b) -> CB.bddAnd bdd (getBdd i b)) PT.bddOne ps'
with EmptyPost -> 
  if O.getValueOfBool "scheck" && asm = P.True 
  then failwith "scheck fails" else PT.bddZero

let postBdd (asm,_) bdd pts =
  if CB.bddEqual bdd PT.bddZero then PT.bddZero else
    let unmod = 
      List.fold_left 
        (fun m (i,wp,_) -> if PT.getPred i = wp then IS.add i m else m)
        IS.empty pts in
    let sumBdd = ref PT.bddZero in
    PT.blast_bddForeachCube bdd
      (fun cube ->
         let (_,pre,post_m) = cube_sweep unmod cube in
         sumBdd :=  CB.bddOr !sumBdd (cube_query (asm,pre) post_m pts));
    TP.block_reset ();
    !sumBdd
      
let postBdd_bdd b e_id = 
  M.msg_string M.Normal "postBdd_bdd:begin"; 
  let rv = PT.bdd_postimage b (PT.get_transrel_bdd e_id) in
  M.msg_string M.Normal "postBdd_bdd:end";
  rv

let query_fn bdd p =
  let trueval = not (U.isSatisfiable (bdd,[P.Not p])) in 
  let falseval = not (U.isSatisfiable (bdd,[p])) in
  if (trueval = falseval) then Ast.Dontknow
  else if trueval then  Ast.True else Ast.False

let post_preds d l' =
  let ps = PT.lookup_location_predicates (C_SD.location_coords l') in
  if ps <> [] then ps else 
    if not (O.getValueOfBool "cf") then d.R.preds else
      List.filter (PT.pred_in_scope (C_SD.get_location_fname l')) d.R.preds
 
let make_pred_triple =
  let wp_table = Hashtbl.create 1009 in
  (fun (_,asgns) rf e_id a ->
    try Hashtbl.find wp_table (a,e_id) with Not_found ->
      let rv = 
        let p = PT.getPred a in
        let p = if rf then self_rename_p p else p in
        let wpp = wp p asgns in
        let wpn = P.negate wpp in
        (if wpp <> p then PT.update_pred_affected e_id a 
         else if O.getValueOfBool "bddpost" 
         then PT.update_eq_transrel_bdd e_id [a]);
        (a,wpp,wpn) in
      Hashtbl.replace wp_table (a,e_id) rv; rv)

let post_data_bdd rcr_flag gc e_id bdd ps =
  let pts = List.map (make_pred_triple gc rcr_flag e_id) ps in
  if not (O.getValueOfBool "bddpost") then postBdd gc bdd pts else 
    let _ =  List.map (make_pred_triple gc rcr_flag e_id) (PT.bdd_support bdd) in
    let b1 = postBdd_bdd bdd e_id in     
    let b2 = if (O.getValueOfBool "hybrid") then postBdd gc (PT.bdd_cube_cover bdd) pts else PT.bddOne in 
    (CB.bddAnd b1 b2) 

  let make_async_op op adr = failwith "TBD: make_async_op"
 (* {{{
    assert (C_SD.isAsyncCall op);
    let cmd = C_SD.get_command op in
    let cmds = Cmd.to_string cmd in 
    M.msg_string M.Debug ("in make_async_op: "^cmds);
    match cmd.Cmd.code with Cmd.FunctionCall fcallExp ->
      let (fname,_subs,_) = C_SD.deconstructFunCall fcallExp in
      let tloc = (C_SD.location_coords (C_SD.lookup_entry_location fname)) in
      let ps = post_preds adr tloc in
      M.msg_string M.Normal (Printf.sprintf "make_async_op preds: %s : %s" cmds (Misc.string_of_int_list ps));
      let asgns = 
          {cmd with Cmd.code = 
            Cmd.Block (List.map (fun (x,e) -> Cmd.Expr(E.Assignment (E.Assign,E.Symbol(x),e))) _subs)} in
      let bdd' = _H_cmd_gs op tloc ps (is_self_recursion op) (Hashtbl.create 2) asgns adr.Region.bdd ps in
      let rv = Events.constructAsyncCall (op,bdd') in
      M.msg_string M.Normal (Printf.sprintf "make_async_op returns: %s" (Cmd.to_string (C_SD.get_command rv)));
      rv
    | _ -> (assert false; failwith "err: make_async_op")
  }}} *)

  let adp_async op adr = failwith "TBD: adp_async"
  (* {{{
    M.msg_string M.Debug ("in adp_async:"^(C_SD.edge_to_string op));
    assert (C_SD.isAsyncCall op || C_SD.isAsyncExec op);
    let f _ b' _ _ = b' in 
    let cmd = C_SD.get_command op in
    match cmd.Cmd.code with Cmd.FunctionCall fcallExp ->
      if C_SD.isAsyncCall op then
        let _ = M.msg_string M.Debug "is async call" in 
        let op' = make_async_op op adr in
          adp_nextreg adr op' adr.Region.preds adr.Region.bdd f 
      else 
       (assert (C_SD.isAsyncExec op);
        let _ = M.msg_string M.Debug "is async exec" in 
        let (_,bdd') = Events.deconstructAsyncCall op in
        let (fname,_,_) = C_SD.deconstructFunCall fcallExp in
        let ps = post_preds adr (C_SD.location_coords (C_SD.lookup_entry_location fname)) in
        adp_nextreg adr op ps (CB.bddAnd bdd' adr.Region.bdd) f)
    | _ -> (assert false; failwith "non-funcall to adp_async")
   }}} *)       

let post_ctrl r op = 
  let s = R.list_of_stack r.R.stack in
  let l = C_SD.get_source op in
  match (Op.get_info op,s) with
    (Op.Call (_,_,f,_),_) when C_SD.enter_call op && not (is_recursion (l::s)) ->
      assert (C_SD.is_defined f);
      let _ = if List.length s = !maxStackLen then Misc.inc maxStackLen in
        (C_SD.lookup_entry_location f, R.CallStack (l::s))
  | (Op.Call _, _) when is_recursion (l::s) && not (O.getValueOfBool "enablecursion") ->
        raise RecursionException
  | (Op.Ret e, h::t) -> (C_SD.get_next_location h, R.CallStack t) 
  | (_,_) -> (C_SD.get_target op,r.R.stack)
    
let post_data d op l' =
  let rcr = is_self_recursion op in
  let ren gc = if rcr then self_rename_ga gc else gc in
  let ps =  post_preds d l' in
  let gc = ren (U.make_guarded_asgn op l') in
  let e_id = (C_SD.location_coords (C_SD.get_source op), C_SD.location_coords l') in 
  let b' = post_data_bdd rcr gc e_id d.R.bdd ps in
  let (ps',le') = DL.post d.R.lelement op (query_fn d.R.bdd) in 
  let ps' = List.filter ((<>) P.True) ps' in
  let b'' = if ps' = [] then b' else post_data_bdd rcr (ren (P.And ps',[])) e_id b' ps in
  {R.lelement = le'; R.preds = ps; R.bdd = b'';} 

(* API *)
let post r op =
  U.statNewVisitedFun (C_SD.get_location_fname (C_SD.get_target op));
  assert (R.is_loc_eq r.R.location (C_SD.get_source op));
  let (l',s') = post_ctrl r op in
  let d' = post_data r.R.data op l' in
  {R.location = l'; R.stack = s'; R.data = d'}


(*******************************************************************************)
(****************************  Summary Post ************************************)
(*******************************************************************************)

let (closure_fun,closure_fun_lv) = 
  if (O.getValueOfBool "fldunroll") 
  then (C_SD.expression_closure_stamp false, C_SD.lvalue_closure_stamp false)
  else (C_SD.expression_closure,C_SD.lvalue_closure) 
   
let get_sym_preds normal f subs = 
  let symvar_fun = 
    if normal then E.make_symvar f else
      (fun lv -> match E.make_symvar f lv with 
         E.Symbol s -> E.Symbol (s^"BLAST")) in
  let make_eq replace_fn lv =
    let lv_clos = closure_fun_lv lv in 
    P.equate
      (List.map (fun lv -> E.Lval (symvar_fun lv)) lv_clos)
      (List.map (fun lv -> replace_fn (E.Lval lv)) lv_clos) in
  let pg =
    P.conjoinL (List.map (make_eq (fun x -> x)) (PT.relevant_globals_mod f)) in
  let pl = 
    P.conjoinL 
    (List.map (fun (x,e) -> make_eq (E.substituteExpression [(E.Lval (E.Symbol x), e)]) (E.Symbol x)) subs) in
  (pg,pl)


(* API *)  
let summary_post cr er cop rop =
  Misc.inc statTotalNumberOfSummaryPosts;
  let l' = C_SD.get_next_location cr.R.location in
  let e_id = (C_SD.location_coords (C_SD.get_source cop), C_SD.location_coords l') in 
  let ps = post_preds cr.R.data l' in
  let (f,subs,t) = match C_SD.call_on_edge cop with Some(_,e) -> C_SD.deconstructFunCall e in
  let re = U.getRetexp rop in
  if (CB.bddEqual cr.R.data.R.bdd PT.bddOne && CB.bddEqual er.R.data.R.bdd PT.bddOne && 
     (not (PT.is_relevant_lval t) || ps = [] || t = C_SD.junkRet || E.is_lval re))
  then {{cr with R.location = l'} with R.data = er.R.data}
  else
    let rcr = is_self_recursion cop in
    let (re,erp) = 
      if not rcr then (re, R.to_predicate er) else 
        ((self_rename_e re),(self_rename_p (R.to_predicate er))) in
    let gc = 
      let erp' = wp erp (U.get_asgn_list (f,subs,t,re)) in
      let (gsp,lsp) =  get_sym_preds (not rcr) f subs in
      (P.conjoinL [erp';gsp;lsp], []) in
    let b' = post_data_bdd rcr gc e_id cr.R.data.R.bdd ps in
    let lat' = DL.summary_post cr.R.data.R.lelement er.R.data.R.lelement cop rop in
    {R.location = l';
     R.stack = cr.R.stack;
     R.data = {R.lelement = lat'; R.preds = ps; R.bdd = b'}}

