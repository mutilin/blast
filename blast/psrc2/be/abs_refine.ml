(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module M = Message
module O = Options
module S = Stats
module E = Ast.Expression
module P = Ast.Predicate
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module Op = Abs_op.Operation
module R = Abs_region.Region
module AA = AliasAnalyzer
module U = Abs_util
module PT = PredTable
module TP = TheoremProver
open Abs_op.Defs

(*****************************************************************************)
(************** Interpolation based predicate discovery **********************)
(************** 1. [API] block_wp      ***************************************)
(************** 2.       get_useful_blocks  **********************************)
(************** 3.	 (cf_)interpolant_refine  ****************************)
(************** 4.	 build_block_at_constraint_arrays ********************)
(************** 5. [API] block_analyze_trace *********************************)
(*****************************************************************************)

(*****************************************************************************)
(******************************* 1. block_wp *********************************)
(*****************************************************************************)

let wp_closure =
  if (O.getValueOfBool "fldunroll") 
  then C_SD.expression_closure_stamp true 
  else C_SD.expression_closure

let asgn_cons lvm (t,v) lv_triples =
  let clos_vs =
    List.map lvm#map_expr 
      (List.map 
        (fun x -> 
          let vx = (E.substituteExpression [(E.Lval t, v)] x) in 
          try E.Lval (E.push_deref (E.lv_of_expr vx)) with _ -> vx)
        (wp_closure (E.Lval t))) in
  let t_addr = E.addressOf (lvm#map_expr (E.Lval t)) in
  List.map 
  (fun (lv,clos_lvs,newvs) ->
    if lv = t then Misc.do_catch "eq1" (P.min_equate newvs) clos_vs else
      let oldvs = List.map lvm#map_expr clos_lvs in
      let eq_new = Misc.do_catch "eq2" (P.min_equate newvs) clos_vs  in 
      let eq_old = Misc.do_catch "eq3" (P.min_equate newvs) oldvs in
      let lv_addr = E.addressOf (lvm#map_expr (E.Lval lv)) in
      let (a,a') = U.simplify_address_equality (t_addr,lv_addr) in
      P.ite (P.Atom (E.Binary(E.Eq,a,a'))) eq_new eq_old) 
  lv_triples

let aliasesOf f (t,_) = 
  let cs = List.map E.push_deref (AA.get_lval_aliases t) in
  if not (O.getValueOfBool "cf") then cs 
  else Misc.cons_if t (U.aliases_in_scope f cs)

let must_close (t,e) =
  List.map (fun lv -> (lv,e)) (t::(AA.get_all_must_aliases t))

let doAssign_list_normal lvm lvm_oo fname tvs = 
  U.clock_tick (); 
  let tvs = 
    List.map (fun (t,v) -> (E.push_deref t, v)) (Misc.flap must_close tvs) in  
  let lv_tripless = 
   List.map 
    (List.map (fun lv -> let es = wp_closure (E.Lval lv) in (lv,es,List.map lvm#map_expr es)))
    (List.map (aliasesOf fname) tvs) in
  let relvs = Misc.flap (Misc.flap Misc.snd3) lv_tripless in
  (List.iter (fun x -> lvm#update (E.lv_of_expr x)) relvs);
  (match lvm_oo with None -> () | Some lvm_o -> lvm_o lvm);   
  List.flatten (List.map2 (asgn_cons lvm) tvs lv_tripless)

let doAssign_list_mccarthy lvm lvm_oo fname tvs = 
  (U.clock_tick ());  
  let t_t's = 
    List.map (fun (lv,_) ->
      match U.mc_var lv with None -> (E.Lval lv, lvm#map_expr (E.Lval lv))
      | Some m -> (m, lvm#map_expr m)) tvs in
  (List.iter (fun (e,_) -> lvm#update (E.lv_of_expr e)) t_t's); 
  (match lvm_oo with None -> () | Some lvm_o -> lvm_o lvm);    
  List.map2 
  (fun (t,t') (lv,e) -> 
    let y = 
      match U.mc_var lv with None -> U.to_mccarthy e | Some _ -> 
        E.Store(t, U.to_mccarthy(E.mc_addressOf (E.Lval lv)), (U.to_mccarthy e)) in 
    P.Atom (E.Binary(E.Eq,t',lvm#map_expr y))) t_t's tvs 

let doAssigns lvm lvm_oo fname p tvs =
  let da = 
    if (O.getValueOfBool "mccarthy") 
    then doAssign_list_mccarthy 
    else doAssign_list_normal in
  P.conjoinL ((da lvm lvm_oo fname tvs)@[p])


let block_wp lvm op l' =
  let fname = C_SD.get_location_fname (C_SD.get_source op) in
  let op = if O.getValueOfBool "events" && C_SD.isAsyncCall op then C_SD.acAsyncCall op else op in
  match (C_SD.get_command op).Cmd.code with
    Cmd.Skip -> P.True
  | Cmd.Pred e -> lvm#map_pred (U.pred_to_mccarthy (U.instantiate_quantifiers e))
  | Cmd.Block [Cmd.Return e] -> 
      let (f,subs,t) = C_SD.deconstructFunCall (U.get_ingoing_call l') in
      doAssigns lvm (Some (fun x -> x#push ())) f P.True (U.asgn_of_return (f,subs,t,e))
  | Cmd.Block es -> 
      let tvs = List.map (fun x -> [x]) (Misc.map_partial (U.asgns_of_stmt l') es) in
      List.fold_left (doAssigns lvm None fname) P.True (List.rev tvs)  
  | Cmd.GuardedFunctionCall(_,fce)  (* TBD: LOOK AT PREDICATE HERE!! *) 
  | Cmd.FunctionCall fce ->
      let (f,_,_) = C_SD.deconstructFunCall fce in
      let tvs = U.asgn_of_funcall (false,false) (Some (lvm#mem)) fce in
      let (f',lvm_oo) = 
        if (C_SD.isAsyncCall op) then (f,None) else 
          if (C_SD.is_defined f) then (f,Some(fun lvm -> lvm#pop())) else 
            (fname,None) in
      doAssigns lvm lvm_oo f' P.True tvs 
  | Cmd.SymbolicHook fname ->
      let gbls = PT.relevant_globals_mod fname in
      let fmls =
        match C_SD.lookup_formals fname with C_SD.Fixed x | C_SD.Variable x -> 
        List.map (fun s -> E.Symbol s) x in
      let asgns = List.map (fun lv -> (lv,E.Lval lv)) (gbls @ fmls) in
      doAssigns lvm None fname P.True asgns
  | _ -> failwith "block_wp" 
  
(* Notes about block_wp:
 * implicit INV:possibleAliases have the same closure set as the target 
 * FALSE!!! The aliases may have a "smaller" closure, due to heuristics in
 * closure that prevent unrolling a field if it already is in the prefix!!
 * now updating the map so that all the things that might possibly have 
 * changed have new values for _BEFORE_ the operation
 *
 * push indicates that this assignment is saving a return value.
 * We'll save the renaming AFTER changing names for aliases of the target but
 * BEFORE renaming local variables of the function we return from, to make
 * sure that these local variable renamings go out of scope outside this
 * function call. TBD:SOUNDNESS 
 * copy_back_asgns is [] if not "cf"  
 * TBD:ARRAYS
 * actually, you have to make a pass over the code and see what cells may have been modified.
 * if this includes things like arrays, this DOES NOT WORK...
 * unless, you repace the arrays wholesale... but this is not sound, as you may create
 * sharing where there is none... sigh. Ideally, you have to know which lvals have been "hit"
 * inside the function call, and copyback all those lvals at the return site. 
 * For now, we assume we only have to copy back the lvals passed in as args and their 
 * transitive derefs. One has to use trace_lv_table -- to copy back all the fellows 
 * that may be looked at in the future. One has to think about this...*)
(* TBD:SOUNDNESS -- in cf mode, only use lvals IN SCOPE! not all lvals *)

  (* Note on SymbolicHook case:
   * it suffices to just plug these in for
   * the lvals that are "relevant" i.e. appear in preds,
   * not for every bloody lvalue mod. 
   * Reason: if its mod in the body of this trace, then no need.
   * if its not mod in the body of this trace, then WAIT
   * first iter -- pred gets added -- lv becomes relevant.
   * second iter -- after mod-smashing in summary_post gives a
   * ctrex, you will add this constraint and thus the symvar gets
   * added... *)


(*****************************************************************************)
(**************************** 2. get_useful_blocks ***************************)
(*****************************************************************************)

class blockcons opa ba = 
  object (self)
    val mask = Array.make (Array.length opa) false 
    method get i = if mask.(i) then P.True else ba.(i)
    method drop i = if C_SD.isPredicate (opa.(i)) then Array.set mask i true 
    method restore () = Array.iteri (fun i a -> Array.set mask i false) mask
  end

(* predH 7 *)
let dom_rel l1 l2 = 
  if l2 = [] then true else
    let (l1',l2') = (List.sort compare l1,List.sort compare l2) in
    let (min,max) = (List.hd l2', Misc.list_last l2')  in
    let rec dom_check l =
      match l with
	  h1::h2::tl -> if (h1 < min && max < h2) then true else dom_check (h2::tl)
	| _ -> false in
      dom_check l1

let print_contra_block opa idx is =
  List.iter 
  (fun i -> 
    M.msg_string M.Normal 
    (Printf.sprintf "[INF%d] %s \n" idx (Op.to_string opa.(i)))) is; 
  M.msg_string M.Normal (Printf.sprintf "[INF%d] \n\n\n" idx)

let check_unsat_foci cba bc is =
  let q = P.And ((cba.(Misc.min is)) :: (List.map bc#get is)) in
  if TP.interpolant_unsat q then is 
  else (M.msg_string M.Normal "WARNING: foci says blockset SAT!";[])

let rec gub_sweep_inc cba bc n must = 
  let muste = (must = []) in
  let i0 = if muste then n else Misc.list_last must in
  TP.block_assert (cba.(i0)); 
  List.iter (fun i -> TP.block_assert (bc#get i)) must;
  if TP._is_contra () then (TP.block_reset (); check_unsat_foci cba bc must) else 
    let (contra,pos) = 
      let rec f pos =
        if TP._is_contra () then (TP.block_reset (); (true,pos)) else 
        if pos < 0 then (TP.block_reset (); (false,pos)) else 
          ((if muste then TP.block_pop ());
           TP.block_assert (bc#get pos);
           (if muste then TP.block_assert cba.(pos)); 
           f (pos - 1)) in f n in
    if contra then gub_sweep_inc cba bc n ((pos + 1)::must) else []
 
let gub_sweep_foci n_ops bc =
  let cs = List.map bc#get (Misc.range (0,n_ops,1)) in 
  try 
    let itps = List.tl (TP.extract_foci_interpolantN cs) in
    let (bs,_,_) = 
      List.fold_left 
      (fun (cur,h,i) h' -> 
        let cur' = if h <> h' || O.getValueOfBool "mccarthy" then i::cur else cur in
        (cur',h',i+1))
      ([],P.True,0) (itps@[P.False]) in
    let bs = List.rev bs in
    assert (bs <> []); bs
  with TP.FociSatException | TP.FociSatExceptionAsgn _ -> []

let get_useful_blocks opa ba cba =
  let n_ops = Array.length opa in
  let _ = M.msg_string M.Normal ("get_useful_blocks: len ="^(string_of_int n_ops)) in
  let bc = new blockcons opa ba in
  let gub_sweep () = 
    if O.getValueOfString "ubdp" = "foci" then gub_sweep_foci n_ops bc
    else gub_sweep_inc cba bc (n_ops - 1) [] in
  if n_ops <= 0 then (-1,[],bc#get) else
    let (pivot, bss) = 
      let rec gather j bss = 
        if List.length bss > O.getValueOfInt "bset" then bss else
          let bs = gub_sweep () in
          if bs = [] then bss else
            let _ = print_contra_block opa j bs in
            if O.getValueOfInt "predH" < 6 then [bs] else 
              (List.iter bc#drop bs; gather (j+1) (bs::bss)) in
      let bss = gather 1 [] in
      let bss = if (O.getValueOfInt "predH" = 7) then Misc.maximal dom_rel bss else bss in
      (Misc.min (-1::(List.flatten bss)), bss) in
    let _ = bc#restore () in
    let _ = M.msg_string M.Normal "get_useful_blocks done." in
    (pivot,bss, bc#get)

(*****************************************************************************)
(*********************** cf_interpolant_refine *******************************)
(*****************************************************************************)

let rec jump_paren (left,right) i =
  let (li,ri) = (left i, right i) in
  if i <> ri then (li,ri) else 
    if i+1 = left (i+1) then jump_paren (left,right) (right (i+1)) else 
      (left (i+1),right (i+1))

let cf_cut (left,right) bs b0 i = 
  let l_i = left i in
  let r_i = right i in
  let (l_i,r_i) = 
    if (r_i = i) then Misc.do_catch "paren" (jump_paren (left,right)) i 
    else (l_i,r_i) in
  let (ctx,hole) = 
    if (l_i = -1) then ([],bs) else
      List.partition (fun x -> (x <= l_i || (x >= r_i && r_i <> -1))) bs in
  let (pre_ctx,_) = List.partition ((>=) l_i) ctx in
  let (pre_hole,post_hole) = List.partition ((>=) i) hole in
  let b = (l_i < b0) && (b0 <= i) in 
  if r_i <> -1 then (i,pre_ctx,pre_hole,ctx@post_hole,b) 
  else (i,pre_ctx,ctx@pre_hole,post_hole,true) 

let process_cf_cut rega getb memo (b0,cb0) (i,call_past,past,future,b) =
  (* {{{ let _ = 
    M.msg_string M.Debug ("In cf process cut at:"^(string_of_int i)) in
    M.msg_string M.Debug ("b : "^(string_of_bool b));
    M.msg_string M.Debug ("start_cons : "^(P.toString cb0))
    M.msg_string M.Debug ("Past Constraints:");
    List.iter (fun i -> M.msg_string M.Debug (P.toString (bc#get i))) past
    M.msg_string M.Debug ("Future Constraints:");
    List.iter (fun i -> M.msg_string M.Debug (P.toString (bc#get i))) future in
}}} *)
  let (cs,cs') = 
    if b then (cb0::(List.map getb past),List.map getb future)
    else (List.map getb past, cb0 :: (List.map getb future)) in
  let loc = (rega.(i+1)).R.location in 
  let callee = C_SD.get_location_fname loc in
  let is =
    Misc.do_memo memo 
    (fun () -> 
      if TP.block_check_unsat (P.And cs) then [] else 
        if TP.block_check_unsat (P.And cs') then [] else 
          let pre_cs = P.And (List.map getb call_past) in
          U.extract_foci_preds_cf (callee,pre_cs) (cs,cs'))
    () past in
  PT.add_local_preds (C_SD.location_coords loc) is


let cf_interpolant_refine (left,right) getb cba rega bs = 
  M.msg_string M.Debug ("Inside cf interpolant refine:"^(string_of_int (List.length bs)));
  let n_ops = Array.length cba in
  let bs = List.sort compare bs in
  let (cb0,b0) = 
    let b0 = List.hd bs in
    if TP.block_check_unsat (P.And (List.map getb bs)) 
    then (P.True,-1) else (cba.(b0),b0) in
  let cuts = Misc.list_tabulate (cf_cut (left,right) bs b0) 0 n_ops in
  List.iter (process_cf_cut rega getb (Hashtbl.create 31) (b0,cb0)) cuts

(**************************************************************************)
(************************* interpolant_refine *****************************)
(**************************************************************************)

let gen_itps_from_cuts getb cb0 bs = 
  let rec _ac (pss,itps) (past,future) =  
    let cs = cb0 ::(List.map getb past) in 
    let cs' = List.map getb future in
    let ps =
      if TP.block_check_unsat (P.And cs) then [] else 
        if TP.block_check_unsat (P.And cs') then [] else
          U.extract_foci_preds (cs,cs') in 
    let (pss',itps') = (ps::pss,P.True::itps) in
    match future with [] -> (List.rev pss',itps')
    | h::t -> _ac (pss',itps') (h::past,t) in
  _ac ([],[]) ([],bs)
  
let proc_itps_cuts b0 rega itp_o x bs =  
  let rec _ac (pss,itps) (bs,bs') = 
    assert (pss <> [] && itps <> []);
    let (b,b') =
      match (bs,bs') with ([],_) -> (b0,b0)
      | (h::_,[]) -> (h + 1, Array.length rega - 2)
      | (h::_,h'::_) -> (h + 1,h') in
    let (ps::pss',itp::itps') = (pss,itps) in
    for i = b to b' do
      M.msg_string M.Debug ("Accessing i= "^(string_of_int i));
      (match itp_o with Some a -> Array.set a i (P.And [itp; a.(i)]) | _ -> ());
      PT.add_local_preds (C_SD.location_coords (rega.(i).R.location)) ps 
    done;
    if bs' <> [] then _ac (pss',itps') ((List.hd bs')::bs, List.tl bs') in 
  _ac x ([],bs) 

let interpolant_refine itp_o getb cba rega bs = 
  let bs = List.sort compare bs in
  let cs = List.map getb bs in
  let b0 = List.hd bs in
  let cb0 = if TP.block_check_unsat (P.And cs) then P.True else cba.(b0) in	
  let (pss,itps) = 
    if not (O.getValueOfBool "itpn") then gen_itps_from_cuts getb cb0 bs
    else
      let itps = TP.extract_foci_interpolantN (cb0::cs) in
      (List.map (fun p -> U.process_raw_foci_preds (P.getAtoms p)) itps, itps) in
  proc_itps_cuts b0 rega itp_o (pss,itps) bs

(**************************************************************************)
(********************** build_block_at_constraint_arrays ******************)
(**************************************************************************)

(* {{{ TBD: ASYNC
exception IncreaseEventCounter

let async_matcher ops regs = 
  assert (List.length ops = List.length regs - 1);
  let regs' = Misc.chop_last regs in
  let ops = 
    List.map2 (fun op r -> if C_SD.isAsyncCall op then make_async_op op r else op) ops regs' in
  let op_to_k op = Events.eventHandler_to_string (fst (Events.deconstructAsyncCall op)) in
  let fp op = 
    if C_SD.isAsyncCall op then ("(",op_to_k op) 
    else if C_SD.isAsyncExec op then (")",op_to_k op) 
    else ("","") in
  let f = 
    match Misc.gen_paren_match fp (Array.of_list ops) with 
    Some (_,r) -> r  | None -> raise IncreaseEventCounter in
  (fun i -> max i (f i))

let make_formal_pred lvm cname exec_i = 
  let ps =
    let eloc = C_SD.location_coords (C_SD.lookup_entry_location cname) in
    let allps = (PT.lookup_location_predicates eloc) in
    snd (List.partition PT.is_pred_global allps) in
  P.And 
  (List.map 
  (fun idx ->
    let si = Printf.sprintf "%s%d_%d" PT.bp_prefix idx exec_i in
    let pidx = lvm#map_pred (PT.getPred idx) in
    P.Iff (P.Atom (E.Lval (E.Symbol si)), pidx)) ps)

let async_block_wp lvm fname op callo i = 
  assert (C_SD.isAsyncCall op || C_SD.isAsyncExec op);
  match C_SD.call_on_edge op with None -> failwith "async_bcdp: no fname!" 
  | Some(cname,_) ->
      let formalp = make_formal_pred lvm cname i in
      if (C_SD.isAsyncCall op) then
        let actualp = block_wp lvm fname op callo in 
        P.And [actualp;formalp]
      else (lvm#pop (); formalp)

}}} *)

let block_at_wp lvm op l =
  if (O.getValueOfBool "events" && (C_SD.isAsyncCall op || C_SD.isAsyncExec op)) 
  then failwith "TBD: ASYNC" (* async_block_wp lvm fname op callo i *) 
  else block_wp lvm op l 

let add_unsat_check_op lvm conjf singlef opa ra ra' (ba,cba) = fun i ->
  let b = block_at_wp lvm opa.(i) ra'.(i).R.location in 
  let cb = if conjf then lvm#map_pred (R.to_predicate ra.(i)) else P.True in 
  Array.set ba i b; Array.set cba i cb;
  if not singlef then false else
    (TP.block_assert b;TP.block_assert cb;
    let rv = TP._is_contra () in TP.block_pop (); rv)

let build_trace_constraints lvm ops rs rs' =
  M.msg_string M.Debug "In build_trace_constraints";
  let n_ops = List.length ops in
  let (opa,ra,ra') = (Array.of_list ops, Array.of_list rs, Array.of_list rs') in
  let conjf = O.getValueOfString "ubdp" <> "foci" && not (O.getValueOfBool "fmc") in
  let singlef = (O.getValueOfInt "predH" < 6) in
  (* TBD: ASYNC let fexec = if (O.getValueOfBool "events") then async_matcher ops rs else fun i -> i in *)
  let ba = Array.make n_ops P.True in
  let cba = Array.make n_ops P.True in
  let process = add_unsat_check_op lvm conjf singlef opa ra ra' (ba,cba) in 
  U.load_lv_table ops; 
  TP.block_reset ();  
  let rec _do i =
    let unsat = process i in 
    if i > 0 && not unsat then _do (i-1) in 
  _do (n_ops - 1);
  TP.block_reset ();
  M.msg_string M.Debug "BLOCK CONSTRAINTS :\n"; 
  Array.iter (M.msg_printer M.Debug P.print) ba;
  (ba,cba)
       
(*************************************************************************)
(*********************** block_analyze_trace *****************************)
(*************************************************************************)


let bat_project rs ops =
  let (rs',rs'') = (Misc.chop_last rs, List.tl rs) in
  let n_ops = List.length ops in
  if (O.getValueOfBool "tproj") then failwith "TBD: TPROJ" 
  (* TBD: TPROJ 
    let baz = (Array.make n_ops P.True, Array.make n_ops P.True) in
    let (ra,ra') = (Array.of_list rs' , Array.of_list rs'') in
    let f = add_unsat_check_op (Lvmap.lvmap ()) false (O.getValueOfInt "predH" < 6) ra ra' baz in
    S.time "Trace Project" (Abs_tproj.trace_project f ops rs) ra *) 
  else (rs',rs'',ops,Misc.id,Misc.id)

let bat_unproject n_ops (proj,proj') bss' b0' cba' getb' =
  if not (O.getValueOfBool "tproj") then (bss',b0',cba',getb') else
    let a = Array.make n_ops P.True in
    (List.map (List.map proj') bss', 
     (proj' b0'),
     (Array.iteri (fun i v -> Array.set a (proj' i) v) cba' ; a),
     (fun i -> let i' = proj i in if i' = -1 then P.True else (getb' i')))

let bat_refine ra opa bss b0 cba getb =
  let n_ops = Array.length opa in
  let itp_o = 
    if O.getValueOfBool "mccarthy" 
    then Some (Array.make (n_ops + 1) P.True) else None in
  let refine =
    if (O.getValueOfBool "cf") 
    then cf_interpolant_refine (Misc.paren_match Op.paren_fun opa) 
    else interpolant_refine itp_o in
  let _ = Misc.ignore_list_iter (refine getb cba ra) bss; TP.block_reset () in
  let tps_o = 
    match itp_o with None -> None | Some a -> 
      Some(Array.init (Array.length a) 
          (fun i -> U.process_raw_foci_preds (P.getAtoms a.(i)))) in
  (itp_o,tps_o)

let bat_finish =
  let cfb_local_refine_table : (string,bool) Hashtbl.t = Hashtbl.create 31 in
  fun ra b0 x -> 
    let rv = (b0, x) in
    if (b0 = -1 || !PT.refined_flag || O.getValueOfBool "lazysymm") 
    then (Hashtbl.clear cfb_local_refine_table; rv) 
    else if (O.getValueOfInt "craig" >= 2) then
      if (O.getValueOfBool "localrestart") 
      then (U.dump_abs ();raise NoNewPredicatesException) 
      else (O.setValueOfBool "localrestart" true; U.dump_abs ();rv)
    else if (O.getValueOfBool "cfb") then 
      let fn = R.get_function_name ra.(b0) in
      if Hashtbl.mem cfb_local_refine_table fn 
      then raise NoNewPredicatesException
      else (Hashtbl.replace cfb_local_refine_table fn true;
            raise (RefineFunctionException fn))
    else (O.setValueOfBool "localrestart" false;U.dump_abs (); rv)

let block_analyze_trace rs ops =
  let n_ops = List.length ops in
  M.msg_string M.Normal ("In block_a_t:"^(string_of_int n_ops));
  assert (List.length ops > 0 && n_ops + 1 = List.length rs);
  let ra = Array.of_list rs in
  let opa = Array.of_list ops in 
  let _ = PT.block_a_t_reset () in
  try
    let (bss,b0,cba,getb) = 
      let (rs',rs'',ops',proj',proj) = bat_project rs ops in
      let (ba',cba') =  build_trace_constraints (new Lvmap.lvmap) ops' rs' rs'' in
      let (b0', bss',getb') = get_useful_blocks (Array.of_list ops') ba' cba' in 
      let pss = List.map (fun r -> String.concat "," (fst (R.to_strings r))) rs'' in
      let _ = Abs_html.htmlize_trace ops pss bss' "" in  
      bat_unproject n_ops (proj,proj') bss' b0' cba' getb' in
    let x = bat_refine ra opa bss b0 cba getb in
    bat_finish ra b0 x 
  with UnskipException b0 ->
    (O.setValueOfBool "localrestart" false; 
     TP.block_reset ();U.dump_abs ();(b0,(None,None)))
 | e -> (TP.block_reset (); M.msg_string M.Error ("bat:"^(Printexc.to_string e)); raise e)

