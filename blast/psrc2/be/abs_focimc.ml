(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


(*****************************************************************************) 
(*********** Transition Relation Refinement [CAV 05] for -focisymm ***********) 
(*****************************************************************************)
module O = Options
module M = Message
module S = Bstats
module E = Ast.Expression
module P = Ast.Predicate
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module PT = PredTable 
module TP = TheoremProver
module R = Abs_region.Region
module Op = Abs_op.Operation
module U = Abs_util
module Ar = Abs_refine
open Abs_op.Defs

(* TBD : options *)
let oBool = O.getValueOfBool

(* global *)
let lvm = new Lvmap.lvmap 

(*************************************************************)
(************** Printing Abstract Counterexamples ************)
(*************************************************************)

let print_upd_tr (x,y) (x',y') p =
  let s = P.toString p in
  M.msg_string M.Debug (Printf.sprintf "upd_trel: (%d,%d)--->(%d,%d): %s" x y x' y' s)

let printop i op = 
  M.msg_string M.Debug (Printf.sprintf "%d : %s" i (Op.to_string op))

let print_counterexample opa reva ps =
  M.msg_string M.Debug ("SAT preds: "^(Misc.strList ps));
  let pt = Hashtbl.create 101 in
  List.iter 
  (fun s -> 
    try
    if (Misc.is_prefix PT.bp_prefix s) then 
      let (pi,t) = Misc.map_pair int_of_string (Misc.last_two (Misc.chop s "_")) in
      Misc.hashtbl_update pt reva.(t) pi;
    with _ -> ()) ps;
  M.msg_string M.Normal "*********Abstract Counterexample**********";
  let (_,cubes) = 
    Array.fold_left
    (fun (i,cs) opi -> 
      let pis = try Hashtbl.find pt i with Not_found -> [] in
      let ps = List.map PT.getPred pis in
      let c = Misc.strList (List.map P.toString ps) in
      M.msg_string M.Normal (Printf.sprintf "Time: %d : %s \n %s" i c (Op.to_string opi));
      (i+1,c::cs)) (0,[]) opa in
  let rv = List.rev ("True"::cubes) in
  Abs_html.htmlize_trace (Array.to_list opa) (List.tl rv) [] "Abstract Counterexample";
  rv

(*********************************************************************************)
(********************************** helpers **************************************)
(*********************************************************************************)

let make_op_id op l' = 
  (fst (C_SD.get_edge_coords op),C_SD.location_coords l')

let make_boolean_state_var i = 
  let v = PT.bp_prefix^(string_of_int i) in
  let v' = PT.bp_next_prefix^(string_of_int i) in
  (P.Atom (E.Lval (E.Symbol v)),P.Atom (E.Lval (E.Symbol v')))

let copy_update_constraints is =
  P.conjoinL (List.map (fun i -> let (x,y) = make_boolean_state_var i in P.Iff (x,y)) is)
 
let is_pure_skip op e_id (is,is') =
  if is != is' then false else
  match (C_SD.get_command op).Cmd.code with Cmd.Pred e -> 
    let lvs = P.lvals_of_predicate (P.And [e; (U.pred_to_mccarthy e)]) in 
    not (List.exists PT.is_relevant_lval lvs)
  | _ -> not (List.exists (PT.is_pred_affected e_id) is)

let process_inconsistent_ops =
  let tick = ref 0 in
  fun opa maska opids ->
    M.msg_string M.Normal (Printf.sprintf "[INF%d] \n\n\n" !tick);
    tick := !tick + 1;
    List.fold_left 
    (fun () i ->
      let opi = opa.(i) in
      M.msg_string M.Normal (Printf.sprintf "[INF%d] %s \n\n" !tick (Op.to_string opi));
      if C_SD.isPredicate opi then (Array.set maska i true)) () opids

(********************************************************************************)
(************** block constraints (with "mccarthy" / splitprover) ***************)
(********************************************************************************)
 
let block_cons_op =
  fun op l' op_id ->  
    let _ = lvm#clear () in
    let rv1 = Ar.block_wp lvm op l' in
    let rv2 = Oo.copy lvm in
    let _ = lvm#clear () in 
    (rv1,rv2)

let block_cons_in lvm op_id i =
  let (v,_) = make_boolean_state_var i in
  P.iff v (lvm#map_pred (U.pred_to_mccarthy (PT.getPred i)))

let block_cons_out asgns op_id i = 
  let p = U.pred_to_mccarthy (PT.getPred i) in
  let _ = if p <> Abs_image.wp p asgns then PT.update_pred_affected op_id i in
  let p' = lvm#map_pred p in
  P.iff (lvm#map_pred p) (snd (make_boolean_state_var i))

let block_p_update_cons op l' (is,is') =
  let op_id = make_op_id op l' in
  let (c,lvm') = block_cons_op op l' op_id in
  let chain = List.map (block_cons_in lvm' op_id) is in
  let (_,asgns) = U.make_guarded_asgn op l' in
  let wpc = List.map (block_cons_out asgns op_id) is' in
  (chain,wpc,[c])

(**********************************************************************************)
(****************************** regular constraints *******************************)
(**********************************************************************************)
 
let cons_in =
  let memo = Hashtbl.create 1009 in
  fun i ->
    Misc.do_memo memo 
    (fun () -> P.iff (fst (make_boolean_state_var i)) (U.pred_to_mccarthy (PT.getPred i)))
    () i

let cons_out = 
  let memo = Hashtbl.create 1009 in
  fun asm asgns op_id i -> 
    Misc.do_memo memo
    (fun () ->
      let (v,v') = make_boolean_state_var i in
      let p = U.pred_to_mccarthy (PT.getPred i) in
      let wpa = Abs_image.wp p asgns in 
      let _ = if p <> wpa then PT.update_pred_affected op_id i in
      P.iff (P.Implies (U.pred_to_mccarthy asm , wpa)) v')
    () (op_id,i)

let p_update_cons op l' (is,is') =  
  let op_id = make_op_id op l' in
  let chain = List.map cons_in is in
  let (asm,asgns) = U.make_guarded_asgn op l' in
  let wpc = List.map (cons_out asm asgns op_id) is' in
  (chain,wpc,[U.pred_to_mccarthy asm])

(*************************************************************)

let predicate_update_constraints op l' isp =
  if is_pure_skip op (make_op_id op l') isp then P.True else   
  let (a,b,c) = 
    if O.getValueOfInt "focimax" = -1 
    then p_update_cons op l' isp 
    else block_p_update_cons op l' isp in
  P.conjoinL (a@b@c) 

(*************************************************************)
(***************** Conversion Functions **********************)
(*************************************************************)

let interpreted_rename_suffix s e = 
  match e with 
    E.FunctionCall (E.Lval (E.Symbol f),es) 
    when E.is_interpreted f && E.is_interpreted_renamable f ->
      (match Misc.flap E.lvals_of_expression es with _::_ -> (e,false) | _ -> 
        (E.FunctionCall (E.Lval(E.Symbol(f^s)),es),true))
  | E.Lval (E.Symbol x) when E.is_interpreted x && E.is_interpreted_renamable x -> 
        (E.Lval (E.Symbol (x^s)),true) 
  | _ -> (e,true)

let retime_pred i p =
  let new_name su su' l = 
    match l with 
      E.Symbol s when E.is_interpreted s -> E.Symbol s
    | E.Symbol s when Misc.is_prefix PT.bp_next_prefix s -> 
        E.Symbol (PT.bp_prefix^(Misc.chop_after_prefix PT.bp_next_prefix s)^su')
    | E.Symbol s -> E.Symbol (s^su) in 
  let i_s = "_"^(string_of_int i) in
  let p' = P.symbol_alpha_convert (new_name i_s ("_"^(string_of_int (i+1)))) p in
  P.atom_transform (E.deep_transform_top_cond (interpreted_rename_suffix i_s)) p'
  
let pred_to_bpred p =
  P.deep_transform 
  (fun p -> 
    match p with P.Atom e -> 
      let (i,b) = PT.getAtomIndex p in
      let p' = P.Atom (E.Lval (E.Symbol (PT.bp_prefix^(string_of_int i)))) in
      if b then p' else (P.negate p')
    | _ -> p) p

let bpred_to_pred j bp =
  P.deep_transform
  (fun p -> 
    match p with P.Atom (E.Lval (E.Symbol s))->
        assert (Misc.is_prefix PT.bp_prefix s);
        let (n::i::_) = List.map int_of_string (List.rev (Misc.chop s "_")) in
        assert (n = j || n = j+1);
        if (n = j) then PT.getPred i else (P.Next (PT.getPred i))
    | P.Atom _ -> failwith "error:bpred_to_pred" (* QUARC *)
    | _ -> p) bp 

let bpred_to_bdd j p = 
  Misc.do_catch ("bpred_to_bdd:"^(P.toString p))
  (PT.convertPredToBdd_map 
  (fun p ->
    match p with P.Atom (E.Lval (E.Symbol s)) -> 
      assert (Misc.is_prefix PT.bp_prefix s);
      let (n::i::_) = List.map int_of_string (List.rev (Misc.chop s "_")) in
      assert (n= j || n= j+1);
      if n = j then PT.getPredIndexBdd i else PT.getPredIndexNextBdd i)) p

      
(* WARNING: DNF *)  
let bdd_to_bpred b =
  PT.ac_convertBddToPred 
  (fun i -> 
    let s = if PT.isCurrentIndex i then PT.bp_prefix else PT.bp_next_prefix in
    P.Atom (E.Lval (E.Symbol (s^(string_of_int i))))) b 

(*************************************************************************)
(*****************  Building Constraints from Traces *********************)
(*************************************************************************)
  
let build_cons aps cpc rega cba ccba reva tps_o (i,j) opi = 
  let l = rega.(i).R.location in
  let l' = rega.(i+1).R.location in
  let (ps,ps') = 
    match tps_o with 
     Some a -> (Misc.sort_and_compact a.(i), Misc.sort_and_compact a.(i+1)) 
   | None when O.getValueOfInt "craig" = 2 -> 
       (PT.lookup_location_predicates (C_SD.location_coords l),
        PT.lookup_location_predicates (C_SD.location_coords l'))
   | _ -> (aps,aps) in
  let wpc = predicate_update_constraints opi l' (ps,ps') in
  if wpc = P.True then (i+1,j) else 
    let consi = retime_pred j wpc in
    let copyi = retime_pred j cpc in
    let _ = Array.set cba j consi in
    let _ = Array.set ccba j copyi in
    let _ = Array.set reva j i in
    (i+1,j+1)

let build_fmc_constraint_arrays rega opa tps_o =
  let n_ops = Array.length opa in 
  M.msg_string M.Debug ("build_fmc_cons: n_ops= "^(string_of_int n_ops));
  assert (Array.length rega = n_ops + 1);
  let cba = Array.create (n_ops+1) P.True in
  let ccba = Array.create (n_ops+1) P.True in
  let aps = PT.get_all_pred_indexes () in
  let cpc = copy_update_constraints aps in
  let reva = Array.create (n_ops+1) (-1) in  
  let (_,n) = Array.fold_left (build_cons aps cpc rega cba ccba reva tps_o) (0,0) opa in
  Array.set cba n 
  (retime_pred n (pred_to_bpred(R.to_predicate(rega.(n_ops))))); 
  M.msg_string M.Debug "Non-skip constraints";
  for i = 0 to n - 1 do M.msg_string M.Normal 
  (Printf.sprintf "%d: :%s" reva.(i) (Op.to_string opa.(reva.(i))))
  done;
  (cba,ccba,reva,n)

(*************************************************************************)
(*****************  Strengthen TR using interpolants *********************)
(*************************************************************************)

let get_cons maska ccba cba n_ops =
  let rec g i l =
    if i > n_ops then (List.rev l) else 
      let h = if maska.(i) then ccba.(i) else cba.(i) in
      g (i+1) (h::l) in
  g 0 []

let check_symm_itp c itp =               
  if oBool "checktree" then
    if TP.interpolant_sat (c, P.Not itp) = TP.Sat then 
      failwith (Printf.sprintf "Invalid itp: %s :: %s \n" 
      (P.toString itp) (P.toString c))

let update_skip_ops rega start stop pis =
  let b = PT.make_eq_bdd pis in
  let p = P.And (List.map (fun i -> let p = PT.getPred i in P.Iff (p,P.Next p)) pis) in
  for i = start to stop do
    let loc = C_SD.location_coords rega.(i).R.location in
    let loc' = C_SD.location_coords rega.(i+1).R.location in
    print_upd_tr loc loc' p;              
    PT.update_transrel_bdd (loc,loc') b 
  done

let strengthen_trel_symm rega reva cs itps =
  assert (List.length cs = List.length itps); 
  let (min_i,_,inc_ops) = 
    List.fold_left2 
    (fun (min_i,clock,inc_ops) c itp -> 
      if itp = P.True then (min_i, clock + 1, inc_ops) else
        let _ = check_symm_itp c itp in
        let i = reva.(clock) in
        let [l;l'] = List.map (fun j -> C_SD.location_coords rega.(j).R.location) [i;i+1] in
        let bi = bpred_to_bdd clock itp in
        let (ps,ps') = 
          let aps = PT.bdd_support bi in
          (List.filter PT.isCurrentIndex aps, 
           Misc.map_partial PT.getCIndexFromNIndex aps) in
        let _ = print_upd_tr l l' (bpred_to_pred clock itp) in             
        let _ = PT.update_transrel_bdd (l,l') bi in
        let _ = PT.add_local_preds l' ps' in
        let _ = if clock > 0 then update_skip_ops rega (reva.(clock-1)+1) (i-1) ps in
        ((if 0 <= min_i then min_i else i), clock+1, i::inc_ops)) (-1,0,[]) cs itps in
  ((if 0 <= min_i then min_i + 1 else (-1)), inc_ops)

let get_lazysymm_itps cs reva itpa = 
  M.msg_string M.Normal "in get_lazysymm_itps";
  assert (List.length cs > 0);
  let ren (i,p) = retime_pred i (pred_to_bpred (P.eq_canonicize (P.unprime p))) in
  snd(Misc.do_catch "get_lazysymm_itps" 
     (List.fold_left
       (fun (clock,rs) c ->
          let i = reva.(clock) in 
          let [ri;ri'] = List.map ren [(clock,itpa.(i));(clock+1,itpa.(i+1))] in
          let (rv::_,_) = TP.extract_foci_interpolantN_symm ([c;P.And [ri;P.Not ri']]) in
          (clock + 1,rv::rs)) (0,[])) cs)

(* may throw SAT exn *)
let refine_trel_symm rega cba ccba maska reva n acc_itp_o =
  let cs = get_cons maska ccba cba n in
  let itps = 
    match (oBool "slicesymm", acc_itp_o)  with 
      (true, Some a) -> Stats.time "get lazysymm itp" (get_lazysymm_itps cs reva) a 
    | _ -> fst (Stats.time "get symm itp" TP.extract_foci_interpolantN_symm cs) in 
  Stats.time "strengthen_trel_symm" (strengthen_trel_symm rega reva cs) itps 

let absref rega opa = 
  assert (oBool "focisymm");
  let n_rega = Array.length rega in 
  let maska = Array.create n_rega false in 
  let (itp_o,tps_o) =
    if not (oBool "lazysymm") then (None,None) else
      let (rsp,x) = Ar.block_analyze_trace (Array.to_list rega) (Array.to_list opa) in
      if rsp < 0 then raise TP.FociSatException else x in
  let (cba,ccba,reva,n) = build_fmc_constraint_arrays rega opa tps_o in
  let rec _do ((rsp,ibs,regss) as args) =
    if ((oBool "lazysymm") || not (oBool "multistr")) && rsp <= n_rega then args else
      try 
        let (rsp',ops') = refine_trel_symm rega cba ccba maska reva n itp_o in
        process_inconsistent_ops opa maska (List.rev ops');
        _do (min rsp rsp', ops'::ibs, regss)
      with TP.FociSatExceptionAsgn ps -> 
        (rsp,ibs,if rsp <= n_rega then regss else print_counterexample opa reva ps) in
  _do (n_rega + 1, [], Misc.clone "" n_rega)

(*********************************************************************)
(******************************* API *********************************)
(*********************************************************************)

(* API *)
let fociModelChecker_refine_trace_abstract rega opa =
   let regs = Array.to_list rega in
   let ops = Array.to_list opa in
   try
     if (oBool "fmcpa") then (fst (Ar.block_analyze_trace regs ops),regs) else
       let (rsp,ibs,regss) = absref rega opa in
       match () with
         _ when rsp <> -1 && !PT.refined_flag ->
           let _ = Abs_html.htmlize_trace ops (List.tl regss) ibs "Weak Transition Relation" in
           (rsp,regs)
       | _ when rsp <> -1 && oBool "localrestart" ->
           (U.dump_abs (); raise NoNewPredicatesException)
       | _ when rsp <> -1 && oBool "lazysymm" ->
           let _ = O.setValueOfBool "localrestart" true; U.dump_abs () in
           (rsp,regs)
       | _ when rsp = -1 && oBool "lazysymm" -> (rsp, regs)
       | _ -> 
           let (rsp,_) = Ar.block_analyze_trace regs ops in
           (if rsp <> -1 && not !PT.refined_flag then raise NoNewPredicatesException);
           if (rsp <> -1 && not (oBool "hybrid")) then 
             let (rsp,_,_) = absref rega opa in (rsp,regs)
           else (rsp,regs)  
    with TP.FociSatException -> (-1,[])

(* API *)
let check_abstract_transition reg op reg' = 
  M.msg_string M.Error "WARNING: in check_abstract_transition";
  let p = retime_pred 0 (bdd_to_bpred reg.R.data.R.bdd) in
  let p' = retime_pred 1 (bdd_to_bpred reg'.R.data.R.bdd) in
  let l' = reg'.R.location in
  let is = PT.get_all_pred_indexes () in
  let c = retime_pred 0 (predicate_update_constraints op l' (is,is)) in
  try TP.extract_foci_interpolantN [P.And [p;c];P.Not p']; true with e -> 
    let [s;s'] = List.map P.toString [p;p'] in
    let _ = M.msg_string M.Normal ("[FAIL] Bad transition! [IN] "^s^" [OUT] "^s') in 
    false
