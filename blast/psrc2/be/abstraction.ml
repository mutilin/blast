(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
  * This module implements the abstraction interface connecting the model
  * checker to the concrete transition system.  It defines: 
  * Operation module
  * Region module / including Coverers
  * Refinement functions : block_analyze_trace, block_analyze_trace_abstract
  * (Post-)Image functions : post, summary_post
  *) 

open BlastArch

module M = Message
module O = Options
module E = Ast.Expression
module P = Ast.Predicate
module AA = AliasAnalyzer
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module PT = PredTable
module TP = TheoremProver
module U = Abs_util
module DL = Lattice.DataLattice  
  
module C_Abstraction =
struct
  module Region = Abs_region.Region
  module Operation = Abs_op.Operation 
  module R = Region
  include Abs_op.Defs

  (* API *)
  let dump_abs =
    U.dump_abs

  (* API *)
  let is_fun_refined = 
    PT.is_fun_refined

  (* API *)
  let post = 
    Abs_image.post
  
  (* API *)
  let summary_post = 
    Abs_image.summary_post
    
  (* API *)
  let block_analyze_trace rs ops = 
    fst (Abs_refine.block_analyze_trace rs ops)
  
  (* API *)
  let fociModelChecker_refine_trace_abstract =
    Abs_focimc.fociModelChecker_refine_trace_abstract

  (* API *)
  let check_abstract_transition = 
    Abs_focimc.check_abstract_transition

  
  (***************************************************************************)
  (********************** enabled operations *********************************)
  (***************************************************************************)
  
  let skip_fun_process op =
    let subst fce f =
      match fce with
	  E.FunctionCall(E.Lval (E.Symbol _), es) -> 
            E.FunctionCall(E.Lval (E.Symbol f), es)
	| E.Assignment (E.Assign,t, E.FunctionCall(E.Lval (E.Symbol _), e)) ->
	    E.Assignment (E.Assign,t, E.FunctionCall(E.Lval (E.Symbol f), e)) in
    assert (O.getValueOfBool "skipfun");
    let cmd = C_SD.get_command op in
    match cmd.Cmd.code with Cmd.FunctionCall fce ->
      let (f,_,_) = C_SD.deconstructFunCall fce in
      if (PT.always_take f) || not (PT.always_skip f) then op else
        C_SD.make_edge (C_SD.get_source op) (C_SD.get_target op) 
        {cmd with Cmd.code = Cmd.FunctionCall (subst fce (C_SD.__SKIPFunctionName^f))} 
    | _ -> op 

  let is_fnptr_call e = match e with (E.Lval (E.Symbol _)) -> false | _ -> true
  
  let make_fnptr_calls l l' fce es ao =
    let gen_eq f e = P.Atom (E.Binary (E.Eq, E.Lval f, e)) in
    List.map 
    (fun fname -> 
      let e = E.FunctionCall(E.Lval fname, es) in
      let e = match ao with None -> e | Some (a,t) -> E.Assignment(a,t,e) in
      C_SD.make_edge l l' (Cmd.gather_accesses (Cmd.GuardedFunctionCall (gen_eq fname fce,e))))
    (AA.getFunctionsPointedTo fce)

  let disambiguate_fp op =
    let (l, cmd, l') = (C_SD.get_source op, C_SD.get_command op, C_SD.get_target op) in 
    match cmd.Cmd.code with
      Cmd.FunctionCall (E.FunctionCall(fce, args)) when is_fnptr_call fce ->
        make_fnptr_calls l l' fce args None
    | Cmd.FunctionCall (E.Assignment(aop,t,E.FunctionCall(fce, args))) when is_fnptr_call fce ->
        make_fnptr_calls l l' fce args (Some (aop,t))
    | _ -> [op]

  let lattice_enabled_ops r =
    if O.getValueOfBool "events" && C_SD.is_dispatch_loc r.R.location then 
      List.map (C_SD.make_edge r.R.location r.R.location) 
      (DL.enabled_ops r.R.data.R.lelement) 
    else []

  let async_proc reg op = 
    if O.getValueOfBool "events" && C_SD.isAsyncCall op then Abs_image.make_async_op op reg else op

  (* API *)
  let enabled_ops reg =
    let ops = C_SD.get_outgoing_edges reg.R.location @ (lattice_enabled_ops reg) in 
    let ops = if (O.getValueOfBool "nofp") then ops else Misc.flap disambiguate_fp ops in
    let ops = if (O.getValueOfBool "skipfun") then List.map skip_fun_process ops else ops in
    let ops = if (O.getValueOfBool "events") then List.map (async_proc reg) ops else ops in
    ops
 
  (* API *)
  let initialize_abstraction () =
    U.load_abs ();
    DL.initialize ();
    M.msg_string M.Normal "Initialized Abstraction"

  (* API *)
  let reset_decision_procedures = TP.reset 
 
  (* API *)
  let reset () = 
    Hashtbl.clear U.theoremProverCache;
    failwith "Abstraction.reset : not fully implemented"


  let init_location () = 
    C_SD.lookup_entry_location (List.hd (O.getValueOfStringList "main")) 

  let error_location () = 
    let elabel = O.getValueOfString "L" in 
    match C_SD.get_locations_at_label elabel with [l] -> l 
    | _  -> Misc.error ("Error: bad error label "^elabel)

  (* API *)
  let create_region errf seedp initp =
    let is = List.map PT.addPred (Misc.flap P.getAtoms [seedp; initp]) in 
    let is = List.filter ((<) (-1)) is in
    let gis = 
      if (O.getValueOfBool "scope" || O.getValueOfBool "cf") 
      then Misc.map_partial (PT.add_pred_to_scope) is else is in
    let (loc,stk) = 
      if errf then (error_location (), R.EveryStack) else 
        (init_location(),R.CallStack []) in 
    PT.add_global_preds gis;
    {R.location = loc; R.stack = stk ; 
     R.data = {R.lelement = DL.top; R.bdd = PT.convertPredToBdd initp; R.preds= is;}}

  (* API *)
  let print_stats () =
    let soi = string_of_int in
    List.iter (M.msg_string M.Normal) 
    [("Nb of summary-posts = "^(soi (!Abs_image.statTotalNumberOfSummaryPosts)));
     ("Nb of queries = "^(soi (!U.statTotalNumberOfQueries))^"\n");
     ("Nb posts = "^(soi (!U.statNumPost))^" ");
     (* ("Nb assume posts = "^(soi (!U.statNumAssumePost))^" ");*)
     ("Nb foci queries = "^(soi (!TP.stats_nb_foci_queries))^" ");
     ("Nb of alias queries = "^(soi !AA.stats_nb_alias_query));
     ("Nb of cached exp_closures ="^(soi !C_SD.stats_nb_cached_exp_closure));
     (*("Nb of queryAlias_cache calls = "^(soi !U.stats_nb_queryAlias_cache_called ));
     ("Nb of queryAlias_cache hits = "^(soi !U.stats_nb_queryAlias_cache_hits ));
     ("Nb of tproj alias queries = "^(soi !stats_nb_tproj_alias_queries));
     ("Nb of lv_rd alias queries (computed) = "^(soi !stats_nb_get_lvrd_aliases_compute));
     ("Nb of lv_rd alias queries (cached) = "^(soi !stats_nb_get_lvrd_aliases_cached));
     ("Max Alias Set size = "^(soi !stats_max_alias_set_size));
     ("Nb of recomp exp_closures ="^(soi !C_SD.stats_nb_recomp_exp_closure));
     ("Nb TP Queries:" ^ (soi !statActualQueriesForPost));
     ("Nb Reached Cubes:" ^ (soi !statReachedCubes));*)
     ("Funs visited:"^(String.concat "," (U.statAllVisitedFuns())))]; 
     PT.print_lp_table ();
     dump_abs ()

  (* API *)
  let notify_result = Abs_html.htmlize_trace_final 
  
end

