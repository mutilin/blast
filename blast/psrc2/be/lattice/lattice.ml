(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

(** Module for lattice implementations *)

module Stats = VampyreStats
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module Op = Abs_op.Operation

module TrivialLattice =
  struct
    type lattice = Bottom | Top
    type edge = C_SD.edge
    
    let bottom = Bottom
    let top = Top
    let init = top

    type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal
    let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) =
      Ast.Dontknow

    let join l1 l2 =
      match (l1, l2) with
        (Top, _) | (_, Top) -> Top
      | _ -> Bottom

    let meet l1 l2 =
      match (l1, l2) with
        (Bottom, _) | (_, Bottom) -> Bottom
      | _ -> Top

    let leq a b =
      match (a,b) with
       (Top, Bottom) -> false
      | _ -> true
	  
    let eq a b =
      a = b

    let focus () = ()

    let is_consistent a b = match a with Bottom -> false | Top -> true

    let post (a: lattice) (e: edge) (fn:_query_fn):(Ast.Predicate.predicate list*lattice)  = ([],a)

    let summary_post l _ _ _ = l

    let pre a b = a
    
    let toString _ = failwith "TBD: lattice toString"
    
    let print fmt l =
      match l with
	  Bottom -> Format.fprintf fmt "Bottom"
	| Top -> Format.fprintf fmt "Top"

    let initialize () = ()

    let enabled_ops _ = []

  end

(* JF: We did not make this an implementation of LATTICE for now, because
 * Make_ConstantLattice needs to get to the constructors within this module.
 *)
(** Lattice element for constant propagation. *)
module ConstantLattice =
  struct 
    type lattice = Bottom        (** Empty set - the lattice is not consistent with
				     predicates. Thus, this state should not be reachable *)
		   | Constant of Ast.Expression.expression (** We store only values of
							       Ast.Expression.expression.Constant
							       here.*)
		   | Top         (** Nothing is known about the value of this lattice
				     element (either because the variable is unitialized
                                     or because we are not tracking it. *)
    type edge = int
    let top = Top
    let bottom = Bottom
    let init = top

    type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal
    let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) =
      Ast.Dontknow

		   
    (** Take the union of the two elements. *)
    let join l1 l2 =
      match (l1, l2) with
      | (Top, _) | (_, Top) -> Top
      | (Constant i, Constant j) -> if i=j then Constant i else Top
      | (Bottom, Bottom) -> Bottom
      | (Bottom, l) | (l, Bottom) -> l

    (** Take the intersection of the two elements *)
    let meet l1 l2 =
      match (l1, l2) with
      | (Bottom, _) | (_, Bottom) -> Bottom
      | (Constant(i), Constant(j)) -> if i=j then Constant i else Bottom
      | (Top, Top) -> Top
      | (Top, l) | (l, Top) -> l

    (** Check for containment of a in b *)
    let leq a b =
      match (a,b) with
        (Bottom, _) -> true
      | (Constant i, Constant j) -> i=j
      | (Constant _, Top) -> true
      | (Top, Top) -> true 
      | _ -> false

    (** Check for equality of a and b *)
    let eq a b =
      match (a,b) with
        (Bottom, Bottom) -> true
      | (Constant i, Constant j) -> i=j
      | (Top, Top) -> true 
      | _ -> false

    let focus () = ()

    let is_consistent a b = match a with Bottom -> false | _ -> true
    
    let post a e fn:(Ast.Predicate.predicate list*lattice)  = ([],a)
    
    let summary_post l _ _ _ = l
    
    let pre a b = a

    let toString _ = failwith "TBD: lattice to string 2"

    let print fmt l =
      match l with
	  Bottom -> Format.fprintf fmt "Bottom"
	| Top -> Format.fprintf fmt "Top"
	| Constant c -> Ast.Expression.print fmt c
end


(** Module for representing a call stack. Used for Constant and Symbolic Store lattices *)
module CallStack =
struct
  (** A call stack frame consists of the name of the function called, a list of argument expressions
      passed to the function, and an lvalue that will receive the result of the called function. *)
  type frame = Ast.Symbol.symbol * ((string * Ast.Expression.expression) list) * Ast.Expression.lval
      (** A call stack is simply a list of frames *)
  and t = frame list

  let empty_stack = []

  exception Stack_empty

  (** Check two call stacks for equality. This comparison ignores the parameters passed into each function
    and only looks at the function names and return targets. *)
  let rec are_equal (a : t) (b : t) =
    match (a, b) with
	([], []) -> true
      | ([], first :: rest) | (first :: rest, []) -> false
      | ((n1, arglist1, retlv1) :: rest1, (n2, arglist2, retlv2) :: rest2) ->
	  if (n1 = n2) && (retlv1 = retlv2) then are_equal rest1 rest2 else false

  let push (stack : t) (f : frame) =
    f :: stack
      
  let pop (stack : t) =
    match stack with
	[] -> raise Stack_empty
      | head :: rest -> (head, rest)

  let toString _ = failwith "lattice tostring 4"

  let print fmt (stack : t) =
    let rec print_frame fmt stack' =
      match stack' with
	  (fname, formals, ret_target) :: rest ->
	    Format.fprintf fmt "@[[%s = %s(" (Ast.Expression.lvalToString ret_target) fname;
	    List.iter (fun formal -> let (name, value) = formal in
			 Format.fprintf fmt "%s=%s@ " name (Ast.Expression.toString value)) formals;
	    Format.fprintf fmt ")] @ ";
	    print_frame fmt rest
	| _ -> ()
    in
      match stack with
	  [] -> Format.fprintf fmt "@[Callstack: []@]"
	| _  ->
	    Format.fprintf fmt "@[Callstack: ";
	    print_frame fmt stack;
	    Format.fprintf fmt "]@]"

end



(** Lattice for tracking the heap *)
module SymbolicStoreLattice =
struct
  module Command = C_SD.Command

  type state = { store : SymbolicStore.t; mutable stack : CallStack.t }
  type lattice = Top | Store of state | Bottom
		    
  type edge = C_SD.edge

  let top = Top 
  let bottom = Bottom
  let init = top

  type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal
  let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) =
    Ast.Dontknow

  (** Function which determines whether the specified lvalue was declared as an array. We have to
      define it here because it is referenced by symstore_options below. *)
  let expr_is_array (expr:Ast.Expression.expression) :bool =
    match expr with
	Ast.Expression.Lval lv ->
	  (match C_SD.get_type_of_lval lv with
	       Cil.TArray _ -> true
	     | _ -> false)
      | _ -> false

  (** Submodule for managing the various options and heuritics for the symbolic lattice *)
  module SymbolicLatticeOptions =
    struct
      (** Select a heuristic for dropping constants, etc when a loopback node is encountered.
          Possible values are:
          - keep_all - don't drop anything
          - drop_constants - drop all constants
          - drop_multiply_defined_lvals - drop lvals which are changed on multiple incoming paths
          - use_dataflow_lattice - use the lattice resulting from a full dataflow analysis
      *)
      let loopback_drop_heuristic = "keep_all"

      (** If attempting to deference an uninitialized pointer, print a warning rather than throw
	an Invalid_access exception. TODO: need to have a special case for argv. *)
      let warn_on_uninitialized_ptr_deref = true

      (** Set this to Message.Debug to only get messages when the -debug option is set. Set this to
          to Message.Normal to always get messages *)
      let debug_level = Message.Debug

      let symstore_options = { SymbolicStore.AlgorithmOptions.expr_is_array = expr_is_array;
			       SymbolicStore.AlgorithmOptions.merge_string_constants = true }
    end

  open SymbolicLatticeOptions

  (** Helper function that returns true if the debug option was specified *)
  let is_debug_on () = (Message.get_verbosity ())= Message.Debugging

  (** Data flow analysis based on a lattice. Right now this is specific to the symbolic store lattice.
      However, it should be possible to make it independent of the specific lattice type with a little
      more work. *)
  module DataflowAnalysis =
  struct
    (** Set this to Message.Debug to only get messages when the -debug option is set. Set this to
      to Message.Normal to always get messages *)
    let dflow_dbglvl = Message.Debug

    (** When set to true, print the dataflow results even when the other debugging is not printed *)
    let print_results = false

    (** This boolean is set in run_data_flow based on the values of debug_level and the -debug option.
        It can be used to skip print calls altogether when debug printing is not enabled for
        the dataflow module *)
    let debug_on = ref false

    (** When set to true, we keep a summary lattice for each function call point, and use it to 
        avoid calls when covered. We false, we only use summary lattices for recursive functions. *)
    let use_function_summaries = true
				   
    (** If true, then group function summaries by the calling location rather than by function name *)
    let summarize_by_caller = true

    (** If true, only check coverage on loopback nodes *)
    let check_cvg_only_on_lpbk = true

    (** Statistic for tracking the number of dataflow iterations *)
    let dataflow_iter_counter = ref 0

    (** Statistic tracking number of function summaries used *)
    let function_summary_counter = ref 0

    (** To abstract the lattice, we have the caller pass in a set of functions for manipulating the
        lattice *)
    type lattice_functions =
	{ post_cmd : lattice -> C_SD.Command.t -> string -> lattice;
	  post : lattice -> edge -> lattice;
	  print : Format.formatter -> lattice -> unit;
	  leq : lattice -> lattice -> bool;
	  union : lattice -> lattice -> lattice;
	  drop_callstack : lattice -> lattice;
	  replace_callstack : lattice -> CallStack.t -> lattice;
	  remove_tos : lattice -> lattice;
	  get_callstack : lattice -> CallStack.t }

    type edge_id = (int*int)*(int*int)
    type location_stack = (C_SD.location*string*edge_id*lattice) list
    type pc = int * int (** program counter *)
    type region = pc * ((int*int) list) * SymbolicStore.t
    type region_set =
	(pc * ((int*int) list), SymbolicStore.t) Hashtbl.t (** map between locations and lattices *)

    let (dataflow_result_stores:(pc, SymbolicStore.t) Hashtbl.t) = Hashtbl.create 63

    let coordsToString (fn, node) = (string_of_int fn) ^ "#" ^ (string_of_int node)

    let coordStkToString (s:(int*int)list) =
      "[" ^
      (List.fold_left (fun str coord ->
			 if str="" then coordsToString coord else str ^ ", " ^ (coordsToString coord))
	 "" s)
	^ "]"

    let edgeIdToString ((src,dst):edge_id) :string =
      (coordsToString src) ^ "->" ^ (coordsToString dst)

    let locStackToCoordStack (s:location_stack) :(int*int) list =
      List.map (fun (loc,_,_,_) -> C_SD.location_coords loc) s

    let is_region_covered loc stack store region_set (** return true if region is covered by set *) =
      try
	let previous_store = Hashtbl.find region_set (loc, stack)
	in
	  SymbolicStore.leq store previous_store
      with
	  Not_found -> false

    let join_regions loc coordStack store (region_set:region_set) =
      (** join the specified region into the region set and return the joined store for the current
        location. *)
      try
	let previous_store = Hashtbl.find region_set (loc, coordStack)
	in let joined_store = SymbolicStore.union store previous_store
	in
	  Hashtbl.replace region_set (loc, coordStack) joined_store;
	  if !debug_on then
	    begin
	      Message.msg_string dflow_dbglvl
		("<DFLOW> Location " ^ (coordsToString loc) ^ ", Stack: " ^ (coordStkToString coordStack) ^
		   " already seen. Old store was:");
	      Message.msg_printer dflow_dbglvl SymbolicStore.print previous_store;
	      Message.msg_string dflow_dbglvl "Joined store:";
	      Message.msg_printer dflow_dbglvl SymbolicStore.print joined_store
	    end;
	  joined_store
      with
	    Not_found ->
	      Hashtbl.replace region_set (loc, coordStack) store;
	      if !debug_on then
		begin
		  Message.msg_string dflow_dbglvl
		    ("<DFLOW> New location " ^ (coordsToString loc) ^ ", Stack: " ^
		     (coordStkToString coordStack) ^ "  Store:");
		  Message.msg_printer dflow_dbglvl SymbolicStore.print store;
		end;
	      store

    let is_lattice_covered (l:lattice) (coords:int*int) stack (region_set:region_set) :bool =
      match l with
	  Top -> failwith "Post should never return Top!"
	| Bottom -> failwith "is_lattice_covered should not be called with Bottom!"
	| Store s -> is_region_covered coords stack s.store region_set

    let get_joined_lattice (l:lattice) (coords:int*int) stack (region_set:region_set) :lattice =
      match l with
	  Top -> failwith "Post should never return Top!"
	| Bottom -> failwith "get_joined_lattice should not be called with Bottom!"
	| Store s -> Store {store = join_regions coords stack s.store region_set; stack=s.stack}

    let merge_stores_for_each_location region_set =
      Hashtbl.iter
	(fun (loc, stack) store ->
	   try
	     let summary_store = Hashtbl.find dataflow_result_stores loc
	     in
	       if (SymbolicStore.leq store summary_store) = false
	       then Hashtbl.replace dataflow_result_stores loc (SymbolicStore.union summary_store store)
	   with Not_found -> Hashtbl.replace dataflow_result_stores loc store)
	region_set

    (**** Code for function summaries ****)
    let (function_summaries_by_caller:(edge_id, lattice*lattice)Hashtbl.t) =
      Hashtbl.create (if summarize_by_caller then 63 else 0)
    let (function_summaries_by_name:(string, lattice*lattice)Hashtbl.t) =
      Hashtbl.create (if summarize_by_caller then 0 else 127)
    (** Get a store function summary using either function_summaries_by_caller or
	function_summaries_by_name. Throws Not_found if no summary exists *)  
    let find_summary  =
      if summarize_by_caller
      then (fun fname edge_id -> Hashtbl.find function_summaries_by_caller edge_id)
      else (fun fname edge_id -> Hashtbl.find function_summaries_by_name fname)

    let replace_summary =
      if summarize_by_caller
      then (fun fname edge_id entry -> Hashtbl.replace function_summaries_by_caller edge_id entry)
      else (fun fname edge_id entry -> Hashtbl.replace function_summaries_by_name fname entry)

    (** We completed a function call - add in summary information for the specified edge *)
    let add_function_summary (fname:string) (entry_edge_id:edge_id) (entry_lattice:lattice)
                             (exit_lattice:lattice) (fns:lattice_functions) :unit =
      try
	(*Message.msg_string Message.Normal ("Adding function summary for " ^
					   (edgeIdToString entry_edge_id)); (*XXX*)
	  Message.msg_printer Message.Normal fns.print entry_lattice; (* XXX *)
	  Message.msg_printer Message.Normal fns.print exit_lattice; (* XXX *)*)
	let (existing_entry_lattice, existing_exit_lattice) = find_summary fname entry_edge_id
	in
	  replace_summary fname entry_edge_id
	    (fns.union existing_entry_lattice entry_lattice,
	     fns.union existing_exit_lattice exit_lattice)
      with Not_found ->
	replace_summary fname entry_edge_id (entry_lattice, exit_lattice)

    (** Is this function call covered by the summary that we already have for this call
      point? It returns a (bool, lattice) tuple. The bool indicates whether the call is covered.
      If it is, the lattice is the summary lattice for the result of the call. If the call was not
      covered, the lattice is the unioned lattice to be used for the call. *)
    let fcall_covered (fname:string) (edge_id:edge_id) (entry_lattice:lattice) (stack:location_stack)
                      (fns:lattice_functions)
                      :bool*lattice =
      let (covered, summary_lattice) =
	(try
	   let (summary_entry_lattice, summary_exit_lattice) = find_summary fname edge_id in
	     if fns.leq entry_lattice summary_entry_lattice then (true, summary_exit_lattice)
	     else (false, summary_entry_lattice)
	 with Not_found -> (*Message.msg_string Message.Normal "new entry..." (* XXX *);*) (false, Top))
      in
	if covered then
	   (true, summary_lattice) (* in this case, the summary lattice is the exit. *)
	else
	  begin (* check for recursion *)
	    try
	      let (_, _, _, recursive_entry_lattice) =
		List.find
		  (fun (loc,fname,entry_edge_id,l) -> if entry_edge_id = edge_id then true else false) stack
	      in
		(* we found a recursive call *)
		if fns.leq entry_lattice recursive_entry_lattice then
		  begin
		    add_function_summary fname edge_id  recursive_entry_lattice entry_lattice fns;
		    Message.msg_string dflow_dbglvl
		      ("<DFLOW> Encountered recursive function at edge " ^ (edgeIdToString edge_id) ^
		       " - treating as covered.");
		    (true, entry_lattice)
		  end
		else
		  begin
		    Message.msg_string dflow_dbglvl
		      ("<DFLOW> Encountered recursive function at edge " ^ (edgeIdToString edge_id) ^
		       " - not covered, merging with caller lattice.");
		    (false, fns.union entry_lattice recursive_entry_lattice)
		  end
	    with Not_found ->
	      (* We didn't find a recursive call. We return the entry lattice if no prior entry
		 exits. We return the union of the entry and summary lattices otherwise. *)
	      (*(Message.msg_string Message.Normal "result = false");*)
	      let result_lattice =
		if summary_lattice=Top then entry_lattice else fns.union entry_lattice summary_lattice
	      in
		(false, result_lattice)
	  end

    (**** Debug print functions ****)
    (** If we get an exception during the dataflow analysis, we dump this information *)
    let dump_visited_regions visited_region_set =
      Message.msg_string Message.Debug "<DFLOW> Visited Regions in Dataflow Analysis:";
      Hashtbl.iter
	(fun (pc,stack) store ->
	   Message.msg_string Message.Debug ("Location: " ^ (coordsToString pc) ^ "  Stack: " ^
					      (coordStkToString stack));
	   Message.msg_printer Message.Debug SymbolicStore.print store;
	   Message.msg_string Message.Debug "")
	visited_region_set

    (** Dump the results of the dataflow analysis. We keep the message level at Normal -
      this function isn't normally called, but we want to easily add it based on
      print_results *)
    let dump_merged_stores (fns:lattice_functions) =
      Message.msg_string Message.Debug "Result stores of dataflow analysis:";
      Hashtbl.iter
	(fun pc store ->
	   Message.msg_string Message.Debug ("<DFLOW> Location " ^ (coordsToString pc) ^ ":");
	   Message.msg_printer Message.Debug SymbolicStore.print store;
	   Message.msg_string Message.Debug "")
	dataflow_result_stores

    (** This is the main function for the data flow analysis. It walks the CFA from the designated
      root function and tracks the lattice at each location.
      
      If a node's lattice is Bottom, the node is not reachable. The edges out of this
      node are not traversed.
      
      If a node is traversed a second time, the new lattice is compared against the
      lattice from the first iteration. If the new lattice is <= the old lattice,
      it is considered "covered" and not re-transversed. If the new lattice is not
      covered, it is joined with the old lattice and the result saved for future
      coverage checks. The node is then re-traversed with this new lattice.
      
      An error region (program location, lattice) may be passed in. It is compared
      against each region found during the search. If there is a non-empty intersection
      between a traversed region and the error region, a message is printed with this
      intersection. *)
    let walk_cfa (root_fn_name:string) (fns:lattice_functions) :unit =
      let rec entry_location = C_SD.lookup_entry_location root_fn_name
      and add_to_worklist start_lattice edge_list stack worklist =
	(* helper fn to add a set of edges to worklist for dfs traversal *)
	begin
	  let rec add edge_list worklist =
	    match edge_list with
		[] -> worklist
	      | head :: tail -> add tail ((start_lattice, head, stack) :: worklist)
	  in add (List.rev edge_list) worklist
	end
      and print_worklist lst =
	begin
	  Message.msg_string Message.Debug "****************";
	  Message.msg_string Message.Debug "Current worklist:";
	  List.iter
	    (fun (l, edge, stack) ->
	       Message.msg_string Message.Debug "Edge:";
	       Message.msg_printer Message.Debug C_SD.print_location (C_SD.get_source edge);
	       Message.msg_printer Message.Debug C_SD.Command.print (C_SD.get_command edge);
	       Message.msg_printer Message.Debug C_SD.print_location (C_SD.get_target edge);
	       Message.msg_string Message.Debug ("Call Stack: " ^
						  (coordStkToString (locStackToCoordStack stack))))
	    lst;
	  Message.msg_string Message.Debug "****************"
	end
      and print_edge start_l edge end_l curr_stack =
	(* print the edge and the lattices *)
	begin
 	  let (start_fn, start_node) = C_SD.location_coords (C_SD.get_source edge)
	  and (targ_fn, targ_node) = C_SD.location_coords (C_SD.get_target edge)
	  in
	    Message.msg_string dflow_dbglvl "<DFLOW> Edge:";
	    Message.msg_printer dflow_dbglvl C_SD.print_location (C_SD.get_source edge);
	    Message.msg_string dflow_dbglvl "     --> ";
	    Message.msg_printer dflow_dbglvl C_SD.Command.print (C_SD.get_command edge);
	    Message.msg_string dflow_dbglvl "     --> ";
	    Message.msg_printer dflow_dbglvl C_SD.print_location (C_SD.get_target edge);
	    Message.msg_string dflow_dbglvl "<DFLOW> Pre lattice: ";
	    Message.msg_printer dflow_dbglvl fns.print start_l;
	    Message.msg_string dflow_dbglvl ("<DFLOW> Call Stack: " ^
					    (coordStkToString (locStackToCoordStack curr_stack)));
	    Message.msg_string dflow_dbglvl "<DFLOW> Post lattice: ";
	    Message.msg_printer dflow_dbglvl fns.print end_l;
	    Message.msg_string dflow_dbglvl ""
	end
      and compute_next_nodes curr_worklist curr_edge (call_stack:location_stack) post_lattice =
	(* Compute the next nodes to look at and return a new worklist *)
	begin
	  let cmd = C_SD.edge_to_command curr_edge
	  and target = (C_SD.get_target curr_edge)
	  and source = (C_SD.get_source curr_edge)
	  and post_lattice' = fns.drop_callstack post_lattice
	  and is_recursive_call target call_stack =
	    List.exists
	      (fun (target', _,  _, _) -> if target' == target then true else false) call_stack
	  in let edge_id = (C_SD.location_coords source,  C_SD.location_coords target)
	  in
	    match cmd.C_SD.Command.code with
		C_SD.Command.FunctionCall(expr) ->
		  begin
		    let (fname, _, _) = C_SD.deconstructFunCall expr
		    in 
		      try
			let fn_node = C_SD.lookup_entry_location fname
			in
			  if use_function_summaries || (is_recursive_call target call_stack) then
			    let (covered, summary_lattice') =
			      fcall_covered fname edge_id post_lattice' call_stack fns
			    in let summary_lattice =
				fns.replace_callstack summary_lattice' (fns.get_callstack post_lattice)
			    in
			      if covered then
				begin
				  Message.msg_string Message.Debug (* XXX *)
				    ("<DFLOW> Using function summary for call to " ^ fname);
				  function_summary_counter := !function_summary_counter + 1;
				  add_to_worklist (fns.remove_tos summary_lattice)
				    (C_SD.get_outgoing_edges target)
				    call_stack curr_worklist
				end
			      else
				begin
				  add_to_worklist summary_lattice (C_SD.get_outgoing_edges fn_node)
				    ((target, fname, edge_id, summary_lattice') :: call_stack) curr_worklist
				end
			  else (* function summaries not enabled and not recursive *)
			    begin
			      add_to_worklist post_lattice (C_SD.get_outgoing_edges fn_node)
				((target, fname, edge_id, post_lattice') :: call_stack) curr_worklist
			    end
		    with C_SD.NoSuchFunctionException(fname) ->
		      Message.msg_string dflow_dbglvl ("<DFLOW> Treating " ^ fname ^ " as external function");
		      add_to_worklist post_lattice (C_SD.get_outgoing_edges target) call_stack curr_worklist
		  end
	      | C_SD.Command.Block(statements) ->
		  begin
		    let has_return = ref false in
		    List.iter
		      (fun statement ->
			 match statement with C_SD.Command.Return(e) ->
			   has_return := true | C_SD.Command.Expr(e) -> ())
		      statements;
		    if !has_return
		    then
		      begin
			match call_stack with
			    (entry_node, fname, ((-1,-1),(-1,-1)), Top) :: [] ->
			      if not (entry_node == entry_location) then
				failwith "Dataflow expecting entry location on top of stack!";
			      Message.msg_string dflow_dbglvl "Popped main function from call stack.";
			      curr_worklist
			  | (rtn_node, fname, entry_edge_id, entry_summary_lattice) :: new_stack ->
			      if use_function_summaries then
				add_function_summary fname entry_edge_id entry_summary_lattice
				  post_lattice' fns;
			      add_to_worklist post_lattice (C_SD.get_outgoing_edges rtn_node) new_stack
				curr_worklist
			  | _ -> failwith "Dataflow analysis popping empty stack"
		      end
		    else add_to_worklist post_lattice (C_SD.get_outgoing_edges target) call_stack curr_worklist
		  end
	      | _ ->
		  add_to_worklist post_lattice (C_SD.get_outgoing_edges target) call_stack curr_worklist
      end
      and make_function_call_cmd fn_name arg_list =
	{ C_SD.Command.code =
	    C_SD.Command.FunctionCall(Ast.Expression.FunctionCall(
					Ast.Expression.Lval(Ast.Expression.Symbol fn_name),
					arg_list));
	  C_SD.Command.read = []; C_SD.Command.written = [] }
      and push_fcall_onto_lattice pre_lattice fn_name =
	let post_lattice =
	  let args = let formals = C_SD.lookup_formals fn_name in
          match formals with C_SD.Fixed l | C_SD.Variable l -> 
            List.map (fun s -> Ast.Expression.Lval (Ast.Expression.Symbol s)) l in 
	    fns.post_cmd pre_lattice (make_function_call_cmd fn_name args) ("global fn:" ^ fn_name)
	in Message.msg_string dflow_dbglvl ("Call to toplevel function " ^ fn_name);
	  Message.msg_printer dflow_dbglvl fns.print post_lattice;
	  post_lattice
      and check_coverage_on_edge edge =
	(* return true if we should do a coverage check after executing this edge (only used if
	   check_cvg_only_on_lpbk = true) *)
	let target = C_SD.get_target edge in
	  (C_SD.is_loopback target) ||
	  (match (C_SD.edge_to_command edge).C_SD.Command.code with
	       Command.Pred _ -> true | _ -> false) ||
	  (compare target (C_SD.get_source edge))=0
      and compute_globals start_lattice edge_list =
	match edge_list with
	    [] -> start_lattice
	  | edge :: [] ->
	      let new_lattice = fns.post start_lattice edge
	      in
		if !debug_on then print_edge start_lattice edge new_lattice [];
		compute_globals new_lattice (C_SD.get_outgoing_edges (C_SD.get_target edge))
	  | edge :: more_edges -> failwith "compute_globals should only see one outgoing edge per node"
      and ini_lattice = Top
      and ini_stack = [(entry_location, root_fn_name, ((-1,-1),(-1,-1)), Top)]
      in
	(* the worklist is a list of (start_lattice, edge, stack) tuples *)
      let worklist =
	ref (add_to_worklist (push_fcall_onto_lattice ini_lattice root_fn_name)
	       (C_SD.get_outgoing_edges entry_location) ini_stack [])
      and (visited_region_set:region_set) = Hashtbl.create 50
      in
	(* the main loop *)
	try
	  while !worklist != [] do
	    (*print_worklist !worklist;*)
	    match (!worklist) with
		(start_lattice, curr_edge, curr_stack) :: rest ->
		  begin
		    dataflow_iter_counter := !dataflow_iter_counter + 1;
		    let post_lattice = fns.post start_lattice curr_edge
		    and target_loc = C_SD.get_target curr_edge
		    in let curr_pc = C_SD.location_coords target_loc
		    in
		      if !debug_on then print_edge start_lattice curr_edge post_lattice curr_stack;
		      if post_lattice = bottom then
			begin                                              
			  Message.msg_string dflow_dbglvl "<DFLOW> Unreachable node";
			  worklist := rest
			end
		      else
			begin
			  if check_cvg_only_on_lpbk && (check_coverage_on_edge curr_edge) = false
			  then
			    begin
			      worklist := compute_next_nodes rest curr_edge curr_stack post_lattice
			    end
			  else
			    begin
			      let coordStack = locStackToCoordStack curr_stack in
				if (is_lattice_covered post_lattice curr_pc coordStack visited_region_set)
				then
				  begin
				    Message.msg_string dflow_dbglvl "<DFLOW> Region is covered.";
				    worklist := rest
				  end
				else
				  begin
				    let joined_lattice =
				      get_joined_lattice post_lattice curr_pc coordStack visited_region_set
				    in worklist := compute_next_nodes rest curr_edge curr_stack joined_lattice
				  end (* region not covered *)
			    end (* do coverage check *)
			end (* region reachable *)
		  end (* non-empty worklist *)
	      | [] -> ()
	  done;
	  merge_stores_for_each_location visited_region_set
	with SymbolicStore.Value.Invalid_access msg ->
	  Message.msg_string Message.Error "ERROR: Dataflow analysis failed due to invalid access exception!";
	  dump_visited_regions visited_region_set;
	  raise (SymbolicStore.Value.Invalid_access msg)

    let run_data_flow (fns:lattice_functions) :unit =
      Message.msg_string Message.Debug "<DFLOW> Starting dataflow analysis...";
      if dflow_dbglvl=Message.Debug ||
	(dflow_dbglvl=Message.Debug && (is_debug_on ()))
      then debug_on := true;
      List.iter
	(fun main_fn ->
	   Message.msg_string dflow_dbglvl ("<DFLOW>: Calling walk_cfa for function " ^ main_fn);
	   walk_cfa main_fn fns)
	(Options.getValueOfStringList "main");
      if !debug_on || print_results then dump_merged_stores fns;
      Message.msg_string Message.Debug "<DFLOW> Completed dataflow analysis.";
      Message.msg_string Message.Debug ("<DFLOW> Dataflow iterations: " ^
					 (string_of_int !dataflow_iter_counter));
      Message.msg_string Message.Debug ("<DFLOW> Number of function summaries used:" ^
					 (string_of_int !function_summary_counter))


    (** Get the store which was calculated for the specified location. Throws Not_found. *)
    let get_store_for_location (loc:C_SD.location) :SymbolicStore.t =
      let coords = C_SD.location_coords loc
      in
	Hashtbl.find dataflow_result_stores coords
  end

  (**** Helper functions ****)
  (** Wrapper over SymbolicStore.assign which allocates a new region if the target is an undefined pointer
      and the option warn_on_unitialized_ptr_deref is true *)
  let rec assign_real (s:state) (lv:Ast.Expression.lval) (e:Ast.Expression.expression) (prog_loc:string) :unit =
    let v = eval s e
    in
      assign_value_real s lv v prog_loc

  (** Wrapper over SymbolicStore.assign which allocates a new region if the target is an undefined pointer
    and the option warn_on_unitialized_ptr_deref is true *)
  and assign_value_real (s:state) (lv:Ast.Expression.lval) (v:SymbolicStore.Value.value) (prog_loc:string)
    :unit =
    try
      SymbolicStore.assign_value s.store (SymbolicStore.canonicize_lval s.store lv) lv v
    with SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg) ->
      if warn_on_uninitialized_ptr_deref then
	begin
	  Message.msg_string debug_level ("<SSL> WARNING: " ^ msg);
	  SymbolicStore.allocate_for_pointer s.store bad_ptr prog_loc;
	  assign_value_real s lv v prog_loc (* Try again with the allocated pointer *)
	end
      else raise (SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg))

  (** Wrapper of SymbolicStore.eval which returns Top if an access is made through an unitialized pointer *)
  and eval (s:state) (expr:Ast.Expression.expression) :SymbolicStore.Value.value =
    try
      SymbolicStore.eval s.store expr
    with SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg) ->
      if warn_on_uninitialized_ptr_deref then
	begin
	  Message.msg_string debug_level ("<SSL> WARNING: " ^ msg);
	  SymbolicStore.Value.Top
	end
      else raise (SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg))
	
  let assign_value = SymbolicStore.Value.dbg_wrap4 assign_value_real
		       (fun s lv v pl -> ["Lattice.assign_value"; (Ast.Expression.lvalToString lv);
					  (SymbolicStore.Value.valueToString v); pl])
		       (fun res -> "unit")

  let assign = SymbolicStore.Value.dbg_wrap4 assign_real
		 (fun s lv e pl -> ["Lattice.assign"; (Ast.Expression.lvalToString lv);
				    (Ast.Expression.toString e); pl])
		 (fun res -> "unit")

  let rec allocate_heap_region_real (s:state) (lv:Ast.Expression.lval) (program_location:string) :unit =
    try
      SymbolicStore.allocate_heap_region s.store lv program_location
    with SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg) ->
      if warn_on_uninitialized_ptr_deref then
	begin
	  Message.msg_string debug_level ("<SSL> WARNING: " ^ msg);
	  SymbolicStore.allocate_for_pointer s.store bad_ptr
	    (program_location ^ (SymbolicStore.Value.pointerToString bad_ptr));
	  allocate_heap_region_real s lv program_location (* Try again with the allocated pointer *)
	end
      else raise (SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg))

  let allocate_heap_region = SymbolicStore.Value.dbg_wrap3 allocate_heap_region_real
		 (fun s lv pl -> ["Lattice.allocate_heap_region"; (Ast.Expression.lvalToString lv); pl])
		 (fun res -> "unit")
      

  let rec havoc (s:state) target prog_loc =
    try
      SymbolicStore.havoc s.store target
    with SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg) ->
      if warn_on_uninitialized_ptr_deref then
	begin
	  Message.msg_string debug_level ("<SSL> WARNING: " ^ msg);
	  SymbolicStore.allocate_for_pointer s.store bad_ptr
	    (prog_loc ^ (SymbolicStore.Value.pointerToString bad_ptr));
	  havoc s target prog_loc (* Try again with the allocated pointer *)
	end
      else raise (SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg))

  (** Push the function call onto the stack and update the store with the new variables *)
  let push_fn_call (s : state) (fn_expr : Ast.Expression.expression) (program_location : string) =
    let (fname, formals, target) = C_SD.deconstructFunCall fn_expr
    in
      match (C_SD.is_defined fname, C_SD.is_alloc fname) with
	  (true, _) -> 
	    begin (* we only push if this is a real function that we will traverse *)
	      (* Add the formals from the funcall*)
	      List.iter
		(fun f -> let (name, value) = f in
		   assign s (Ast.Expression.Symbol name) value program_location)
		formals;
	      (* new stack *)
	      s.stack <- CallStack.push s.stack (fname, formals, target)
	    end
	| (false, true) ->
	    (* Allocate a new region and assign to target *)
	    allocate_heap_region s target program_location
	| _ ->
	    (* if the function is not defined, havoc the target *)
	    if target<>C_SD.junkRet then havoc s target program_location

  (** Pop a function call, removing local variables and assigning the return value target
      to rtn_val_expr. Constants are folded from this expression before assignment.
      This function returns the new symbol table.

      The function is_local_to_fn is used to determine whether variables are local to the stack
      frame being popped.
   *)
  let pop_fn_call (s : state) (rtn_val_expr : Ast.Expression.expression) (program_location:string) =
    let ((fname, formals, ret_target), rest_of_stack) = CallStack.pop s.stack
        (* We have to fold the new return value before we remove the existing local
           mappings, or else we may miss out on some constant folding opportunities.
	   However, we add the mapping later, after we've popped off this stack frame. *)
       and rtn_value= eval s rtn_val_expr
    in
      s.stack <- rest_of_stack; (* pop the stack *)
      (* Remove the formals and locals from the map *)
      SymbolicStore.remove_local_vars s.store
	(fun varname ->
	   let lv = Ast.Expression.Symbol varname
	   in (C_SD.is_in_scope fname lv)=true && (C_SD.is_global lv)=false);
      (* Add mapping for result, if it is trackable and not junkRet.  In either case, we
	 return the new table *)
      if (ret_target<>C_SD.junkRet) then
	assign_value s ret_target rtn_value program_location

  (** Return a lattice with the same store but an empty callstack *)
  let drop_callstack (l:lattice) :lattice =
    match l with
	Store s -> Store {store=s.store; stack=[]}
      | _ -> l

  let replace_callstack (l:lattice) (stack:CallStack.t) :lattice =
    match l with
	Store s -> Store {store=s.store; stack=stack}
      | _ -> l

  (** Return a lattice with the same store and callstack, but with the top entry in the stack removed *)
  let remove_tos (l:lattice) :lattice =
    match l with
	Store s ->
	  let (_, new_stack) = CallStack.pop s.stack in
	    Store {store=s.store; stack=new_stack}
      | _ -> failwith "remove_tos not expecting Top or Bottom"

  let get_callstack (l:lattice) :CallStack.t =
    match l with
	Store s -> s.stack
      | _ -> []

  let toString _ = failwith "lattice tostring 6"
  (**** Main lattice functions ****)
  let print fmt l =
    match l with
	Bottom -> Format.fprintf fmt "@[Bottom@]"
      | Top -> Format.fprintf fmt "@[Top@]"
      | Store(s) -> SymbolicStore.print fmt s.store

  let dbg_print fmt l =
    Format.fprintf fmt "<SSL> ";
    print fmt l

  let empty_store = SymbolicStore.create_new_store symstore_options

  (** Take the union of two regions. *)
  let join l1 l2 =
    Message.log_string_dbg Message.SymLat "Taking join of:";
    Message.log_print_dbg Message.SymLat print l1;
    Message.log_print_dbg Message.SymLat print l2;
    match (l1, l2) with
	(Top, Store s) | (Store s, Top) ->
	  Store ({store = SymbolicStore.union s.store empty_store;
		  stack = s.stack})
      | (Top, _) | (_, Top) -> Top
      | (Store s1, Store s2) ->
	  (if (not (Options.getValueOfBool "cf")) && (CallStack.are_equal s1.stack s2.stack)=false
	   then failwith "Attempt to take the join of two stores with different stacks");
	  let result = {store = SymbolicStore.union s1.store s2.store;
			       stack = s1.stack}
	  in Message.log_string_dbg Message.SymLat "Result:";
	    Message.log_print_dbg Message.SymLat print (Store result);
	    Store(result)
      | (Bottom, Bottom) -> Bottom
      | (Bottom, l) | (l, Bottom) -> l

    (** Take the intersection of two regions. For now, we return either Top or Bottom and
        ignore the stacks. *)
    let meet l1 l2 =
    Message.log_string_dbg Message.SymLat "Taking meet of:";
    Message.log_print_dbg Message.SymLat print l1;
    Message.log_print_dbg Message.SymLat print l2;
      match (l1, l2) with
      | (Bottom, _) | (_, Bottom) -> Bottom
      | (Store s1, Store s2) ->
	  begin
	    try
	      SymbolicStore.intersection s1.store s2.store; Top
	    with SymbolicStore.Value.Bottom -> Bottom
	  end
      | (Store s, Top) | (Top, Store s) -> Store s
      | _ -> Top

    (** Check for containment of a in b *)
    let leq a b =
      match (a,b) with
        (Bottom, _) -> true
      | (Store s1, Store s2) ->
	  ((Options.getValueOfBool "cf") || (CallStack.are_equal s1.stack s2.stack)) &&
	  (SymbolicStore.leq s1.store s2.store)
      | (Store _, Top) -> true
      | (Top, Top) -> true 
      | _ -> false

    (** Check for equality of a and b *)
    let eq a b =
      match (a,b) with
        (Bottom, Bottom) -> true
      | (Store s1, Store s2) -> (CallStack.are_equal s1.stack s2.stack) && ((compare s1.store s2.store) = 0)
      | (Top, Top) -> true 
      | _ -> false

    let focus () = ()

    let is_consistent a b = match a with Bottom -> false | _ -> true

    (** Variable to indicate whether we should print ruled out predicates. We disable printing
        during the dataflow analysis, unless -debug was specified *)
    let print_ruled_out_predicates = ref true

    (** post_cmd is called internally by post. It is also used by the unit tests for situations
        where we don't have an edge. We may eventually need to expose it on all the lattices
        so that we can run one command at a time for alias analysis. *)
    let post_cmd (l:lattice) (c:C_SD.Command.t) (program_location:string) :lattice =
      let s = match l with
	  Top -> {store = SymbolicStore.create_new_store symstore_options; stack = CallStack.empty_stack}
	| Store(s') -> {store = SymbolicStore.deep_copy s'.store; stack = s'.stack}
	| Bottom -> failwith "post: should not take post of Bottom"
      and print_result_and_return result =
	Message.log_string_dbg Message.SymLat "SymbolicStoreLattice.post result:";
	Message.log_print_dbg Message.SymLat print result;
	Message.log_string_dbg Message.SymLat "";
	result
	(** Helper function to add mappings to the store based on the predicate being true.
            Raises SymbolicStore.Not_satisfiable if a contradiction is found. *)
      and add_mappings_from_predicate (s : state) predicate =
	begin
	  try
	    match (predicate) with
		Ast.Predicate.False -> 
		  (* false is never satisfiable! *)
		  raise SymbolicStore.Not_satisfiable
	      | Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq,
							   Ast.Expression.Lval lv1,
							   Ast.Expression.Lval lv2))
	      | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Ne,
									      Ast.Expression.Lval lv1,
									      Ast.Expression.Lval lv2))) ->
		  (* both sides of predicate are lvals *)
		  SymbolicStore.add_predicate s.store lv1 (Ast.Expression.Lval lv2);
		  SymbolicStore.add_predicate s.store lv2 (Ast.Expression.Lval lv1)
	      | Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq, Ast.Expression.Lval lv, e))
	      | Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq, e,
							   Ast.Expression.Lval lv))
	      | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary
							 (Ast.Expression.Ne, e, Ast.Expression.Lval lv))) 
	      | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary
							 (Ast.Expression.Ne,
							  Ast.Expression.Lval lv, e))) ->
		  (* One side of predicate is an lval *)
		  SymbolicStore.add_predicate s.store lv e
	      | Ast.Predicate.Atom e ->
		  (* For an atomic expression, we evaluate it to see if it is false *)
		  if (eval s e) = (SymbolicStore.Value.Constant (Ast.Constant.Int 0)) then
		    raise SymbolicStore.Not_satisfiable
	      | Ast.Predicate.Not (Ast.Predicate.Atom e) ->
		  let v = eval s e in
		    (match v with
			 SymbolicStore.Value.Constant (Ast.Constant.Int i) ->
			   if i <> 0 then raise SymbolicStore.Not_satisfiable
		       | _ -> ())
	      | _ -> ()
	  with
	      SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg) ->
		if warn_on_uninitialized_ptr_deref
		then Message.msg_string debug_level ("<SSL> WARNING: " ^ msg)
		else raise (SymbolicStore.Value.Uninit_ptr_access (bad_ptr, msg))
	end
    in
      Message.log_string_dbg Message.SymLat "SymbolicStoreLattice.post input:";
      Message.log_string_dbg Message.SymLat "Lattice:";
      Message.log_print_dbg Message.SymLat print l;
      Message.log_print_dbg Message.SymLat CallStack.print s.stack;
      Message.log_string_dbg Message.SymLat "Command:";
      Message.log_print_dbg Message.SymLat C_SD.Command.print c;
      Message.log_string_dbg Message.SymLat "";
      (* the main body of this function *)
      match c.Command.code with
	  Command.Skip -> l (* lattice unchanged *)
	| Command.Block(statements) ->
	    List.iter
	      (function
		   Command.Expr (Ast.Expression.Assignment(op, lval, src_expr)) ->
		     assign s lval src_expr program_location
		 | Command.Expr(expr) -> () (* can ignore other expressions *)
		 | Command.Return(expr) ->
		     pop_fn_call s expr program_location)
	      statements;
	    print_result_and_return (Store s)
	| Command.Pred(predicate) ->
	    begin
	      try
		add_mappings_from_predicate s predicate;
		print_result_and_return (Store s)
	      with
		  SymbolicStore.Not_satisfiable ->
		    if !print_ruled_out_predicates then
                      Message.log_string_norm Message.SymLat
			("One more predicate ruled out by symbolic lattice: " ^
			 (Ast.Predicate.toString predicate));
		     print_result_and_return Bottom
            end
	| Command.FunctionCall(expr) ->
	    push_fn_call s expr program_location;
	    print_result_and_return (Store s)
        | Command.GuardedFunctionCall(predicate, expr) ->
	    begin
	      try
		add_mappings_from_predicate s predicate;
	        push_fn_call s expr program_location;
	        print_result_and_return (Store s)
	      with
		  SymbolicStore.Not_satisfiable ->
		    if !print_ruled_out_predicates then
                      Message.log_string_norm Message.SymLat
			("One more predicate ruled out by symbolic lattice: " ^
			 (Ast.Predicate.toString predicate));
		     print_result_and_return Bottom
            end

	| Command.Havoc(lval) ->
	    SymbolicStore.havoc s.store lval;
	    print_result_and_return (Store s)
	| Command.HavocAll -> print_result_and_return Top
	| Command.Phi(lval, sign) -> failwith "ConstantLattice post: encountered phi command"
	| Command.SymbolicHook _ -> print_result_and_return (Store s) (* ??? Is this right? *)

    (** Reference to the function which implements the loopback_drop_heuristic. The default case
        throws an exception indicating that the lattice wasn't initialized. *)
    let heuristic_drop : (lattice -> edge -> lattice) ref =
      ref (fun l e -> failwith "SymbolicLattice not initialized!")

    (** Drop heuristic that keeps the entire lattice *)
    let keep_all l e = l

    (** Drop heuristic which drops all constants and symbolic constants *)
    let drop_constants l e =
      match l with
	  Store s -> SymbolicStore.invalidate_consts s.store; Store s
	| _ -> l

    (** Drop heuristic *)
    let drop_multiply_defined_lvals l e =
      match l with
	  Store s ->
	    begin
	      let target_loc = C_SD.get_target e in
	      let lv_list = C_SD.get_all_multiply_defined_lvals target_loc
	      in
		List.iter (fun lv -> SymbolicStore.invalidate_lval s.store lv) lv_list;
		Store s
	    end
	| _ -> l

    (** Heuristic to use the dataflow lattice whenever we encounter a loopback node *)
    let use_dataflow_lattice l e =
      match l with
	  Store s ->
	    begin
	      let target_loc = C_SD.get_target e in
		try
		  Store {store=DataflowAnalysis.get_store_for_location target_loc; stack=s.stack}
		with Not_found ->
		  Message.msg_string debug_level
		    "<SSL> No store found for location, using lattice returned by post";
		  l
	    end
	| _ -> l

    (** Function used only when checking the soundness of loopback drop heuristics. It is a variant of
        leq which doesn't return false for the cases:
        * array leq non-array
        * heap_reagion with Top value leq Top
    *)
    let loopback_drop_soundness_check l1 l2 =
      let rec leq_values v1 v2 =
	match (v1, v2) with
	    (SymbolicStore.Value.Top, SymbolicStore.Value.Top)
	  | (SymbolicStore.Value.Constant _, SymbolicStore.Value.Top) -> ()
	  | (SymbolicStore.Value.Top, SymbolicStore.Value.Structure s) ->
	      (* When comparing a hash table to top, compare to all members *)
	      Hashtbl.iter (fun m entry -> leq_values SymbolicStore.Value.Top entry.SymbolicStore.Value.v) s
	  | (SymbolicStore.Value.Top, _) -> raise Not_found
	  | (SymbolicStore.Value.Pointer p, SymbolicStore.Value.Top) -> raise Not_found
	  | (SymbolicStore.Value.Constant c1, SymbolicStore.Value.Constant c2) ->
	      if c1 <> c2 then raise Not_found
	  | (SymbolicStore.Value.Constant c, SymbolicStore.Value.Pointer p) ->
	      if (v1 <> SymbolicStore.Value.zero_pointer_value)
		|| ((SymbolicStore.Value.pointer_contains_null p) = false)
	      then raise Not_found
	  | (SymbolicStore.Value.Pointer p, SymbolicStore.Value.Constant c) ->
	      if ((SymbolicStore.Value.Constant c) <> SymbolicStore.Value.zero_pointer_value) ||
		(p <> (SymbolicStore.Value.MustPointer SymbolicStore.Value.Null))
	      then raise Not_found
	  | (SymbolicStore.Value.Pointer p1, SymbolicStore.Value.Pointer p2) ->
	      SymbolicStore.Value.pointer_leq p1 p2
	  | (SymbolicStore.Value.Structure s1, SymbolicStore.Value.Structure s2) ->
	      Hashtbl.iter
		(fun m s2_entry ->
		   if Hashtbl.mem s1 m then
		     leq_values (Hashtbl.find s1 m).SymbolicStore.Value.v s2_entry.SymbolicStore.Value.v
		   else leq_values SymbolicStore.Value.Top s2_entry.SymbolicStore.Value.v)
		s2;
	      Hashtbl.iter
		(fun m s1_entry ->
		   if (Hashtbl.mem s2 m) = false then
		     leq_values s1_entry.SymbolicStore.Value.v SymbolicStore.Value.Top)
		s1;
	  | (SymbolicStore.Value.Structure s, SymbolicStore.Value.Top) ->
	      (* When comparing a hash table to top, compare to all members *)
	      Hashtbl.iter (fun m entry -> leq_values entry.SymbolicStore.Value.v SymbolicStore.Value.Top) s
	  | (SymbolicStore.Value.Structure _, _) | (_, SymbolicStore.Value.Structure _) -> raise Not_found
      and store_leq s1 s2 =
	try
	  SymbolicStore.iter_basic
	    (fun k v_s2 ->
	       if not (SymbolicStore.mem s1 k) then
		 begin
		   match k with
		       (* we allow the case where we have a heap region in the right lattice. *)
		       SymbolicStore.Value.BaseRegion (SymbolicStore.Value.Heap s) ->
			 leq_values SymbolicStore.Value.Top v_s2
		     | _ -> raise Not_found
		 end
	       else let v_s1 = SymbolicStore.find s1 k in
		 leq_values v_s1 v_s2)
	    s2;
	  SymbolicStore.iter_basic
	    (fun k v_s1 ->
	       if (SymbolicStore.mem s2 k) = false then
		 leq_values v_s1 SymbolicStore.Value.Top)
	    s1;
	  true
	with
	    Not_found -> false
      in
	match (l1, l2) with
	    (Store s, Store s') -> store_leq s.store s'.store
	  | (Store s, Top) -> store_leq s.store empty_store
	  | _ -> leq l1 l2
		    

    (** Association list of loopback drop heuristics. *)
    let loopback_drop_hr_assoc = [("keep_all", keep_all); ("drop_constants", drop_constants);
				  ("drop_multiply_defined_lvals", drop_multiply_defined_lvals);
				  ("use_dataflow_lattice", use_dataflow_lattice)]

    let post (l:lattice) (e:edge) (fn:_query_fn) :(Ast.Predicate.predicate list * lattice) =
      (*Message.msg_string debug_level ("<SSL> Post for edge " ^ (C_SD.get_edge_name e));*)
      let l' = post_cmd l (C_SD.get_command e) (C_SD.get_edge_name e)
      in (* If we have a store and this is a loopback node, run heuristic_drop *)
      let tmpval = 
	if C_SD.is_loopback (C_SD.get_target e)
	then
	  begin
	    let l'' = !heuristic_drop l' e in
	      Message.msg_printer debug_level C_SD.print_location (C_SD.get_target e);
	      Message.msg_string debug_level
		("<SSL> Applied heuristic " ^ loopback_drop_heuristic ^ " to post lattice:");
	      Message.msg_printer debug_level dbg_print l'';
	      (* This next check is expensive - only use when testing *)
	      (*if (loopback_drop_soundness_check l' l'') = false then
		begin
		  Message.msg_string Message.Error
		    ("<SSL> Loopback Heuristic not sound at edge " ^ (C_SD.get_edge_name e));
		  Message.msg_string Message.Error "Post lattice:";
		  Message.msg_printer Message.Error dbg_print l';
		  Message.msg_string Message.Error "<SSL> Result from loopback drop:";
		  Message.msg_printer Message.Error dbg_print l'';
		  failwith "Lattice Loopback Drop Heuristic not sound!"
		end;*)
	      Message.msg_string debug_level ""; (* skip line *)
	      l''
	  end
	else l'
      in ([], tmpval)

    let summary_post l _ _ _ = l 

    let pre a b = top 

    (** Initialization function for this lattice. *)
    let initialize () =
      (* setup the heuristic_drop function *)
      (try
	 heuristic_drop := List.assoc loopback_drop_heuristic loopback_drop_hr_assoc
       with Not_found ->
	 failwith ("bad value for loopback_drop_heuristic: " ^ loopback_drop_heuristic));
      if loopback_drop_heuristic = "drop_multiply_defined_lvals" then
	C_SD.compute_reaching_definitions ();
      if loopback_drop_heuristic = "use_dataflow_lattice" then
	begin
	  if not (is_debug_on ()) then print_ruled_out_predicates := false;
	  Stats.time "Lattice dataflow analysis"
	    DataflowAnalysis.run_data_flow
	    { DataflowAnalysis.post_cmd = post_cmd;
	      DataflowAnalysis.post = (fun l e -> post_cmd l (C_SD.get_command e) (C_SD.get_edge_name e));
	      DataflowAnalysis.print = print;
	      DataflowAnalysis.leq = leq;
	      DataflowAnalysis.union = join;
	      DataflowAnalysis.drop_callstack = drop_callstack;
	      DataflowAnalysis.replace_callstack = replace_callstack;
	      DataflowAnalysis.remove_tos = remove_tos;
	      DataflowAnalysis.get_callstack = get_callstack };
	  print_ruled_out_predicates := true
	end

   let enabled_ops _ = []

end

(* Make a product lattice. Assume that L1.edge = L2.edge 
 * How do I convince Ocaml about this *)
module ProductLattice = 
  functor (L1 : BlastArch.LATTICE) -> functor (L2 : BlastArch.LATTICE) ->
struct
  type lattice = L1.lattice * L2.lattice
  type edge = L1.edge * L2.edge
    
  let top = (L1.top, L2.top)
  let init = top

  type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal
  let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) =
    Ast.Dontknow

	      
  let bottom = (L1.bottom, L2.bottom) 
  let join (l1,l2) (l1',l2') = ((L1.join l1 l1'), (L2.join l2 l2'))
  let meet (l1,l2) (l1',l2') = ((L1.meet l1 l1'), (L2.meet l2 l2'))
  let leq (l1,l2) (l1',l2') = (L1.leq l1 l1') && (L2.leq l2 l2')
  let eq (l1,l2) (l1',l2') = (L1.eq l1 l1') && (L2.eq l2 l2')
  let focus () = (L1.focus (), L2.focus ())

  let is_consistent (l1,l2) b = (L1.is_consistent l1 b) && (L2.is_consistent l2 b)

  let pre (l1,l2) (c1,c2) = (L1.pre l1 c1, L2.pre l2 c2)
  
  let post (l1,l2) (e1,e2) fn  = (L1.post l1 e1 fn, L2.post l2 e2 fn)

  let summary_post l _ _ _ = l
 
  let toString _ = failwith "TBD lattice tostring 3"

  let print fmt (l1,l2) =
    Format.printf fmt l1;
    Format.printf fmt l2

  let initialize () = ()
  
  let enabled_ops _ = []
  
end

module DataLattice = 
  TrivialLattice 
  (* ((Unionlattice.Make_UnionLattice (C_SD)) (Operation)) (Events) *)
  (*((EventCounterLattice.Make_EventCounterLattice (C_SD)) (Op)) (Events.Events) *)

