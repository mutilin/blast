(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open Ast

exception StrangeError of string

type pred_type = Expression.expression
type name_type = Expression.expression
    
type list_pred_pair = {name: name_type ; pred: pred_type }
and value = Epsilon | Zero | Omega | Forall | Top | Bottom
and list_pred_vals = (list_pred_pair, value) Hashtbl.t
and info = {preds:pred_type list ; list_length:int} 
type t = {list_elems:list_pred_vals; pred_elems:(name_type, info) Hashtbl.t; id:int}

let idctr = ref 0
(** list lattice creation methods **)
    
let create_new_lattice _ =
  let _list_elems:list_pred_vals = Hashtbl.create 31
  in let _pred_elems = Hashtbl.create 31
  in { list_elems = _list_elems ; pred_elems = _pred_elems ; id = 0}

let get_dummy_info _ =
  { preds = [];
    list_length = 0
  }

let make_list_pred_pair listname listprop = 
  { name = listname ;
    pred = listprop
  }


    
(**** / list lattice creation methods ****)
    
(** utility functions **)

(* method to make sure that the result of appending two lists does not contain any duplicates *)
let rec unique_list_append list1 list2  =
  match list2 with
    | [] -> list1
    | head::tail ->
	if (List.mem head list1) = false
	then unique_list_append (List.append list1 [head]) tail
	else unique_list_append list1 tail

(* method to make a single string out of a list of strings *)
let rec aux_string_list_to_string strlist strres =
  match strlist with
    | [] -> strres
    | head::tail -> aux_string_list_to_string tail (strres^" ;; "^head)
	  
let string_list_to_string strlist =
  aux_string_list_to_string strlist ""

(**** / utility functions ****)

(** access functions **)

let get_list_elems arg =
  arg.list_elems

let get_pred_elems arg =
  arg.pred_elems
	  
let get_list_name key =
  key.name

let get_list_property key =
  key.pred

let get_info_list_preds arg =
  arg.preds

let get_info_list_length arg =
  arg.list_length

let get_all_lists (latlist:t) =
  let lists_list = ref []
  in
    begin
      Hashtbl.iter
	(fun listname _ ->
	   lists_list := !lists_list @ [listname]
	)
	(get_pred_elems latlist)
      ;
      !lists_list
    end
(**** / access functions ****)
	  
(** find/get methods **)
let get_id latlist =
  latlist.id
       
let find_info (latlist:t) key =
  Hashtbl.find (get_pred_elems latlist) key
       
let find_value (latlist:t) (key:list_pred_pair) =
  Hashtbl.find (get_list_elems latlist) key

let get_list_info (latlist:t) key =
  try
    find_info latlist key
  with Not_found -> get_dummy_info ()
    
let get_list_length (latlist:t) key =
  try
    let pred_elem = find_info latlist key
    in get_info_list_length pred_elem
  with Not_found -> 0

let get_list_preds (latlist:t) key =
   try
    let pred_elem = find_info latlist key
    in get_info_list_preds pred_elem
  with Not_found -> []

(* if you don't know the value for a particular predicate, then just return TOP *)
let get_value (latlist:t) (key:list_pred_pair) =
  try
    find_value latlist key
  with Not_found -> Top

let rec aux_get_list_preds_vals latlist listname list_preds res_list =
  match list_preds with
    | [] -> res_list
    | head::tail ->
	let _pair = make_list_pred_pair listname head
	in let _value = get_value latlist _pair
	in aux_get_list_preds_vals latlist listname tail (res_list @ [_value])

(* this function returns the values corresponding to all the predicates for a list in the lattice *)
let get_list_preds_vals latlist listname  =
  let list_preds = get_list_preds latlist listname
  in aux_get_list_preds_vals latlist listname list_preds []

(* this is the second version of the above version in which we want the values of certain known predicates *)
let get_list_preds_vals2 latlist listname list_preds  =
  aux_get_list_preds_vals latlist listname list_preds []

       
(**** /** find/get methods ****)

(** set/replace methods **)

let set_id latlist _id = 
  {list_elems = latlist.list_elems; pred_elems = latlist.pred_elems; id = _id}
  
let set_list_info_length info length =
  { preds = info.preds;
    list_length = length  (** changed! **)
  }

let set_list_info_list_preds info newlist =
  { preds = newlist;      (** changed! **)
    list_length = (get_info_list_length info)
  }
    
let info_replace latlist key info =
  Hashtbl.replace (get_pred_elems latlist) key info
  
let list_preds_replace latlist key newlist =
  let old_info = get_list_info latlist key
  in let new_info = set_list_info_list_preds old_info newlist
  in info_replace latlist key new_info

let value_replace latlist key value =
  Hashtbl.replace (get_list_elems latlist) key value

let length_replace latlist key len =
  let old_info = get_list_info latlist key
  in let new_info = set_list_info_length old_info len
  in let _ =
      if (len <= 0)
      then
      	  let _list_preds = get_list_preds latlist key
	  in List.iter
	       ( fun predicate ->
		   let hash_key = make_list_pred_pair key predicate
		   in value_replace latlist hash_key Epsilon
	       )
	       _list_preds
  in info_replace latlist key new_info

let add_pred_val latlist listname listpred predval =
  let oldpredlist = get_list_preds latlist listname
  in let newpredlist = unique_list_append oldpredlist [listpred]
  in let _ = list_preds_replace latlist listname newpredlist
  in let hashkey = make_list_pred_pair listname listpred
  in value_replace latlist hashkey predval

         
(* function to make a new lattice from known lattice with different values for a particular list *)
let make_new_list latlist listname preds predvals listlength =
  let _ = length_replace latlist listname listlength 
  in let _ = list_preds_replace latlist listname preds
  in let ctr = ref 0
  in let _ = List.iter
	       (fun _pred ->
		  begin
		    print_string "here1!!!";
		    value_replace latlist (make_list_pred_pair listname _pred) (List.nth predvals !ctr);
		    print_string "here2!!!";
		    ctr := !ctr + 1
		  end
	       )
	       preds
  in latlist

(* function to make a new lattice from a know lattice *)
let make_new_lat latlist =
  let _new_lat = create_new_lattice ()
  in let new_lat = set_id _new_lat ((get_id latlist)+10)
  in let all_lists = get_all_lists latlist
  in let _ = List.iter
	       (fun listname ->
		  let list_preds = get_list_preds latlist listname
		  in let _ = list_preds_replace new_lat listname list_preds
		  in let _ = length_replace new_lat listname (get_list_length latlist listname)
		  in List.iter
		       (fun predicate ->
			  let pair = make_list_pred_pair listname predicate
			  in let predval = find_value latlist pair
			  in value_replace new_lat pair predval
		       )
		       list_preds
	       )
	       all_lists
  in new_lat
  
(**** / set/replace methods ****)
       
(** change info/value fields **)

(* list length is always maintained >= 0*)
let inc_list_length (latlist:t) key =
  let currlen = get_list_length latlist key
  in let newlen = (if(currlen>=0) then (currlen + 1) else 1)
  in length_replace latlist key newlen

(* list length is always maintained >= 0*)
let dec_list_length (latlist:t) key =
  let currlen = get_list_length latlist key
  in let newlen = ( if(currlen>0) then (currlen - 1) else 0 )
  in length_replace latlist key newlen

let trunc_list_length latlist key =
  length_replace latlist key 0

(* this method takes care that we don't get any duplication *)
let add_preds_to_list_preds (latlist:t) key preds_to_add =
  let curr_preds = get_list_preds latlist key
  in let new_preds = unique_list_append curr_preds preds_to_add
  in list_preds_replace latlist key new_preds
       
let add_value latlist key propval  =
  let listname = get_list_name key
  in let listprop = get_list_property key
  in let _ = add_preds_to_list_preds latlist listname [listprop]
  in value_replace latlist key propval

(**** / change info/value fields ****)

(** misc interface functions **)
       
let add_value_to_lattice l listname listprop propval =
  let _ = value_replace l (make_list_pred_pair listname listprop) propval
  in l

let list_length_to_string len =
  string_of_int len
       
let list_name_to_string (lname:name_type) =
  Expression.toString lname

let pred_name_to_string (predname:pred_type) =
  Expression.toString predname
    
let value_to_string (x:value) =
  match x with
    | Epsilon -> "[Epsilon]"
    | Zero    -> "[Zero]"
    | Omega   -> "[Omega]"
    | Forall  -> "[Forall]"
    | Top     -> "[Top]"
    | Bottom  -> "[Bottom]"

let rec aux_list_preds_to_string arg str =
  match arg with
    | [] -> (str^" ]]")
    | head::tail ->
	let head_str = pred_name_to_string head
	in aux_list_preds_to_string tail (str^" ;; "^head_str)
	  
let list_preds_to_string (arg:pred_type list) =
  aux_list_preds_to_string arg "[[ "

let list_info_to_string (arg:info) = 
  let str1 = list_preds_to_string (get_info_list_preds arg)
  in let str2 = list_length_to_string (get_info_list_length arg)
  in "{("^str1^"),("^str2^")}"	  

let rec aux_list_preds_to_string_list arg1 arg2 strlist =
  match (arg1, arg2) with
    | ([], []) -> strlist
    | (head1::tail1 , head2::tail2) ->
	  let head1_str = pred_name_to_string head1
	  in let head2_str = value_to_string head2
	  in let pair_str = ("["^head1_str^":="^head2_str^"]")
	  in aux_list_preds_to_string_list tail1 tail2 (strlist @ [pair_str])
    | _ -> failwith "really bad mismatch in aux_list_preds_to_string_list @ listLatticeUtil.ml"
    
let list_preds_vals_to_string_list (arg1:pred_type list) (arg2:value list) =
  aux_list_preds_to_string_list arg1 arg2 []

let list_pred_pair_to_string (arg:list_pred_pair) =
  let s1 = list_name_to_string (get_list_name arg)
  in let s2 = pred_name_to_string (get_list_property arg)
  in s1^"::"^s2

let list_to_string (arg1:t) (arg2:name_type) =
  let length = get_list_length arg1 arg2
  in let list_preds = get_list_preds arg1 arg2
  in let list_vals = get_list_preds_vals arg1 arg2
  in let str1 = list_length_to_string length
  in let str_list = list_preds_vals_to_string_list list_preds list_vals
  in let str2 = string_list_to_string str_list
  in let str3 = list_name_to_string arg2
  in ("LIST(("^str3^"):["^str1^"]{"^str2^"})")
       
(**** / misc interface functions ****)

(** printing functions **)
(*** these functions are really superflous since we already have the comprehensive to_string functions
     but since they are often used, it is worth it having them seperately ***)

let printval fmt (pred_val:value) =
  Format.fprintf fmt "%s" (value_to_string pred_val)

let rec printpreds fmt (list_preds:pred_type list) =
  Format.fprintf fmt " %s " (list_preds_to_string list_preds)
    

let print fmt (latlist:t) =
  let all_lists = get_all_lists latlist
  in
    begin 
      Format.fprintf fmt "@[*LATTICE*[#";
      Format.fprintf fmt "%s" (string_of_int (get_id latlist));
      Format.fprintf fmt "]:[ ";
      List.iter
	( fun listname ->
	    let list_str = list_to_string latlist listname
	    in Format.fprintf fmt "[%s]" list_str
	)
	all_lists;
      Format.fprintf fmt "]@]"
    end
    
(*** / printing functions  ***)    
    

    
(**

Lattice theory:

               Top
               /|\
              / | \
             /  |  \
            /   |   omega
           /    |    \
          0   epsilon \
          \     |     /
           \    |    /
            \   |  forall
             \  |  /
              \ | /
               \|/
              Bottom
**)

(** the main algebraic functions **)
      
let leq_values val1 val2 =
  match val1 with
    | Top -> true
    | Bottom ->
	begin 
	  match val2 with
	    | Bottom -> true
	    | _ -> false
	end
    | Zero ->
	begin
	  match val2 with
	    | Bottom | Zero -> true
	    | _ -> false
	end
    | Epsilon ->
	begin
	  match val2 with
	    | Bottom | Epsilon -> true
	    | _ -> false
	end
    | Omega ->
	begin
	  match val2 with
	    | Bottom | Forall | Omega -> true
	    | _ -> false
	end
    | Forall ->
	begin
	  match val2 with
	    | Bottom | Forall -> true
	    | _ -> false
	end

let rec aux_leq_value_lists list1 list2 reslist =
  match (list1, list2) with
    | ([], []) -> reslist
    | (head1::tail1 , head2::tail2) ->
	let leq_result = leq_values head1 head2
	in aux_leq_value_lists tail1 tail2 (reslist @ [leq_result])
    | _ -> failwith "fatal mismatch in aux_leq_value_lists @ listLatticeUtil.ml"
	  
let leq_value_lists val_list1 val_list2 =
  aux_leq_value_lists val_list1 val_list2 []
	  
let join_values val1 val2 =
  match val2 with
    | Top    -> Top
    | Bottom -> val1
    | _ ->
	match val1 with
	  | Bottom -> val2
	  | Top    -> Top
	  | Epsilon ->
	      begin
		match val2 with
		| Epsilon -> Epsilon
		| _ -> Top
	      end
	  | Zero ->
	      begin
		match val2 with
		  | Zero -> Zero
		  | _ -> Top
	      end
	  | Omega ->
	      begin
		match val2 with
		  | Omega | Forall -> Omega
		  | _ -> Top
	      end
	  | Forall ->
	      begin
		match val2 with
		  | Omega -> Omega
		  | Forall -> Forall
		  | _ -> Top
	      end

let rec aux_join_value_lists val1_list val2_list res_list =
  match (val1_list, val2_list) with
    | ([],[]) -> res_list
    | (head1::tail1 , head2::tail2) ->
	let join_val = join_values head1 head2
	in aux_join_value_lists tail1 tail2 (res_list @ [join_val])
    | _ -> failwith "fatal mismatch error in aux_join_value_lists @ listLatticeUtil.ml"
		
let join_value_lists val1_list val2_list  =
  aux_join_value_lists val1_list val2_list [] 


let meet_values val1 val2 =
  match val2 with
    | Bottom -> Bottom
    | Top    -> val1
    | _ ->
	match val1 with
	  | Bottom -> Bottom
	  | Top    -> val2
	  | Epsilon ->
	      begin
		match val2 with
		  | Epsilon -> Epsilon
		  | _ -> Bottom
	      end
	  | Zero ->
	      begin
		match val2 with
		  | Zero -> Zero
		  | _ -> Bottom
	      end
	  | Omega ->
	      begin
		match val2 with
		  | Omega -> Omega
		  | Forall -> Forall
		  | _ -> Bottom
	      end
	  | Forall ->
	      begin
		match val2 with
		  | Omega | Forall -> Forall
		  | _ -> Bottom
	      end

let rec aux_meet_value_lists val1_list val2_list res_list =
  match (val1_list, val2_list) with
    | ([],[]) -> res_list
    | (head1::tail1 , head2::tail2) ->
	let meet_val = meet_values head1 head2
	in aux_meet_value_lists tail1 tail2 (res_list @ [meet_val])
    | _ -> failwith "fatal mismatch error in aux_meet_value_lists @ listLatticeUtil.ml"
		
let meet_value_lists val1_list val2_list  =
  aux_meet_value_lists val1_list val2_list [] 


let join le1 le2 =
  let _result_le = create_new_lattice ()
  in let result_le = set_id _result_le ((get_id le1) + (get_id le2))
  in let le1_all_lists = get_all_lists le1
  in let le2_all_lists = get_all_lists le2
  in let all_lists = unique_list_append le1_all_lists le2_all_lists
  in let _ = List.iter
	       (fun listname ->
		  let le1_list_length = get_list_length le1 listname
		  in let le2_list_length = get_list_length le2 listname
		  in let result_le_list_length = (if (le1_list_length < le2_list_length)
					      then le1_list_length else le2_list_length)
		  in let le1_preds = get_list_preds le1 listname
		  in let le2_preds = get_list_preds le2 listname
		  in let result_le_preds = unique_list_append le1_preds le2_preds
		  in let le1_vals = get_list_preds_vals2 le1 listname le1_preds
		  in let le2_vals = get_list_preds_vals2 le2 listname le2_preds
		  in let result_le_vals = join_value_lists le1_vals le2_vals
		  in let _ = make_new_list result_le listname result_le_preds result_le_vals result_le_list_length
		  in ()		    
	       )
	       all_lists
  in result_le
  
let meet le1 le2 =
  let _result_le = create_new_lattice ()
  in let result_le = set_id _result_le ((get_id le1) + (get_id le2))
  in let le1_all_lists = get_all_lists le1
  in let le2_all_lists = get_all_lists le2
  in let all_lists = unique_list_append le1_all_lists le2_all_lists
  in let _ = List.iter
	       (fun listname ->
		  let le1_list_length = get_list_length le1 listname
		  in let le2_list_length = get_list_length le2 listname
		  in let result_le_list_length = (if (le1_list_length < le2_list_length)  (*DOUBT --ask Rupak if correct*)
					      then le1_list_length else le2_list_length)
		  in let le1_preds = get_list_preds le1 listname
		  in let le2_preds = get_list_preds le2 listname
		  in let result_le_preds = unique_list_append le1_preds le2_preds
		  in let le1_vals = get_list_preds_vals2 le1 listname le1_preds
		  in let le2_vals = get_list_preds_vals2 le2 listname le2_preds
		  in let result_le_vals = meet_value_lists le1_vals le2_vals
		  in let _ = make_new_list result_le listname result_le_preds result_le_vals result_le_list_length
		  in ()
	       )
	       all_lists
  in result_le
  
let leq le1 le2 =
  let status = ref true
  in let le1_all_lists = get_all_lists le1
  in let _ = List.iter
	       (fun listname ->
		  let le1_list_preds = get_list_preds le1 listname
		  in let le1_vals = get_list_preds_vals le1 listname
		  in let le2_vals = get_list_preds_vals2 le2 listname le1_list_preds
		  in let leq_result = leq_value_lists le1_vals le2_vals
		  in if (List.mem false leq_result) then status := false
	       )
	       le1_all_lists
  in !status

