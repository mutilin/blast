(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open BlastArch
open Ast
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module Op = Abs_op.Operation

module ListLattice =
  struct
    module Command = C_SD.Command

    (* the list primitive functions that we reason with *)
    let list_init = "blast_list_initialize"
    let list_remove = "blast_list_remove"
    let list_add = "blast_list_add"
    let list_length = "blast_list_length"
			(* end of function list *)
			
    type llist = { lat_list:ListLatticeUtil.t;  }

    (* the different kinds of lattices that can exist at a given point *)
    type lattice = Bottom | State of llist | Top

    (* NOT IMPLEMENTED *)
    let match_exp_with_latlist latlist exp =
      Ast.Dontknow
      
    let query_fn lelmnt (pred:Ast.Predicate.predicate) =
      match lelmnt with
	| Bottom -> failwith "Ouchhh! watch your step... ListLattice.query_fn@lattice.ml"
	| Top -> Ast.Dontknow
	| State s ->
	    match pred with
	      | Predicate.True -> Ast.True
	      | Predicate.False -> Ast.False
	      | Predicate.Atom exp -> match_exp_with_latlist s.lat_list exp
	      | _ -> failwith "match failure: ListLattice.query_fn@lattice.ml"
	

    type edge = C_SD.edge

    let bottom = Bottom
    let top = Top
    let init = top 

    let toString _  = failwith "TBD: toString listLattice"

    (**** Main lattice functions ****)
    let print fmt l =
      match l with
	| Bottom -> Format.fprintf fmt "Bottom"
	| Top -> Format.fprintf fmt "Top"
	| State s -> ListLatticeUtil.print fmt s.lat_list (* CallStack.print fmt s.stack*)

    let empty_latticelist = ListLatticeUtil.create_new_lattice ()

		
    (* join is union *)
    let join l1 l2 =
      begin
	Message.log_string_dbg Message.ListLat "Taking join of:";
	Message.log_print_dbg Message.ListLat print l1;
	Message.log_print_dbg Message.ListLat print l2;
	match (l1, l2) with
	  | (Top, _) | (_, Top) -> Top
	  | (State s1, State  s2) ->
	    (*  (if (not (Options.getValueOfBool "cf")) && (CallStack.are_equal s1.stack s2.stack)=false
	     then failwith "Attempt to take the join of two stores with different stacks");*)
	      let result = {lat_list = ListLatticeUtil.join s1.lat_list s2.lat_list}
			     (*stack = s1.stack}*)
	      in begin
		  Message.log_string_dbg Message.ListLat "Result:";
		  Message.log_print_dbg Message.ListLat print (State result);
		  State(result)
		end
	  | (Bottom, Bottom) -> Bottom
	  | (Bottom, State s) | (State s, Bottom) ->
	      let result = {lat_list = ListLatticeUtil.make_new_lat s.lat_list}
	      in State (result)
      end
	    
    (* meet is intersection *)
    let meet l1 l2 =
      Message.log_string_dbg Message.ListLat "Taking meet of:";
      Message.log_print_dbg Message.ListLat print l1;
      Message.log_print_dbg Message.ListLat print l2;
      match (l1, l2) with
	| (Bottom, _) | (_, Bottom) -> Bottom
	| (State s1, State s2) ->
	   (* (if (not (Options.getValueOfBool "cf")) && (CallStack.are_equal s1.stack s2.stack)=false
	     then failwith "Attempt to take the join of two stores with different stacks");*)
	    let result = {lat_list = ListLatticeUtil.meet s1.lat_list s2.lat_list}
			   (*stack = s1.stack}*)
	    in begin
		Message.log_string_dbg Message.ListLat "Result:";
		Message.log_print_dbg Message.ListLat print (State result);
		State(result)
	      end
	| (State s, Top) | (Top, State s) ->
	    let result = {lat_list = ListLatticeUtil.make_new_lat s.lat_list}
	    in State (result)
	| _ -> Top

    let leq a b =
      Message.log_string_dbg Message.ListLat "Taking leq of:";
      Message.log_print_dbg Message.ListLat print a;
      Message.log_print_dbg Message.ListLat print b;
      match (a,b) with
	| (Top, Bottom) -> false
	| (State s1, State s2) ->
	    (*((Options.getValueOfBool "cf") || (CallStack.are_equal s1.stack s2.stack)) &&*)
	    let result = (ListLatticeUtil.leq s1.lat_list s2.lat_list) in
	      if result = false then begin
		Message.log_string_norm Message.ListLat "leq (cover) test failed:";
		Message.log_print_norm Message.ListLat print a;
		Message.log_print_norm Message.ListLat print b
	      end;
	      result
	| _ -> true
	  
    let eq a b =
      match (a,b) with
        | (Bottom, Bottom) -> true
	| (State s1, State s2) -> (*(CallStack.are_equal s1.stack s2.stack) &&*) ((compare s1.lat_list s2.lat_list) = 0)
	| (Top, Top) -> true 
	| _ -> false
	    
    let focus () = ()

    let is_consistent a b = match a with Bottom -> false | _ -> true
      

    (* the heart of this lattice -- the POST function *)

    let get_access_op lval =
      match lval with
	| Expression.Lval (_ ) -> Expression.Lval (Expression.Dereference lval)
	| Expression.Unary (Expression.Address, l) ->
	    l
	| Expression.Binary (Expression.Plus, e1, e2) -> Expression.Lval (Expression.Dereference lval)
	| _ -> let str = Expression.toString lval in failwith ("Please nOOOO!!!"^str^"!!!")
      
    let rec attach_outer_most exp lval =
      match exp with
	| Expression.Lval (Expression.Access (op, exp, sym)) ->
	    Expression.Lval (Expression.Access (op, (attach_outer_most exp lval), sym))
	| Expression.Lval (Expression.Symbol sym) ->
	    Expression.Lval (Expression.Access (Expression.Dot, (get_access_op lval), sym))
	| _ -> failwith "wrong kind of argument : attach_outer_most@lattice.ml"
   
      
    let make_predicate lval pred =
      match pred with
	| Expression.Binary (op, e1, e2) ->
	    let _e1 = attach_outer_most e1 lval
	    in let predexp = Expression.Binary (op, _e1, e2)
	    in Predicate.Atom predexp
	| _ -> failwith "sheeshsseeeh!!!"

    let make_not_predicate lval pred =
      Predicate.Not (make_predicate lval pred)

    let is_outermost exp listname =
      match exp with
	| Expression.Lval (Expression.Dereference (Expression.Lval (Expression.Symbol listname))) ->
	    true
	| _ -> false
	
    let rec remove_outer_access exp listname =
      match exp with
	| Expression.Lval (Expression.Access(op, e1, e2)) ->
	    if (is_outermost e1 listname) then Expression.Lval (Expression.Symbol e2)
	    else
	      Expression.Lval (Expression.Access(op, remove_outer_access e1 listname, e2))
	| _ -> failwith "something seriously wrong here : remove_outer_access@lattice.ml"
	
    let get_predicate_from_formal l exp listname =
      let _ = print_string "getting predicates for the lattice:"
      in let _ = Message.log_print_norm Message.ListLat print l
      in 
      begin
	match exp with
	  | Expression.Binary (op, e1, e2) ->
	      let e = Expression.Binary (op, (remove_outer_access e1 (Expression.toString listname)), e2)
			(* Expression.Binary (op, e1, e2)*)
	      in [e] 
	  | Expression.Constant (Constant.Int constpred) ->
	      if (constpred = 1) then
		match l with
		  | State (s) ->
		      ListLatticeUtil.get_list_preds s.lat_list listname
		  | _ ->  [] (*failwith "Uh oh - trying to set predicates for a list which has not been initialized!!"*)
	      else failwith "don't know what to do (yet) getPredicateFromFormal@lattice.ml"
	  | _ -> failwith "something went wrong in gePredicateFromFormal@lattice.ml"
      end

    let get_init_pred_val exp  =
      match exp with
	| Expression.Constant (Constant.Int intval) ->
	    if (intval = 0) then ListLatticeUtil.Epsilon
	    else if (intval = 1) then ListLatticeUtil.Forall
	    else failwith "unnegotiated value : get_init_pred_val@lattice.ml"
	| _ -> failwith "oops you did it again : get_init_pred_val@lattice.ml"

    let rec change_prop l listname arr_listprop initval =
      match arr_listprop with
	| [] -> l
	| listprop::tail ->
	    match l with
	      | Bottom -> failwith "Called post on a bottom list - illegal!!"
	      | Top ->
		  let l2 = ListLatticeUtil.create_new_lattice ()
		  in let _ = ListLatticeUtil.add_pred_val l2 listname listprop initval
		  in let newlat =  State({lat_list = l2})
		  in change_prop newlat listname tail initval
	      | State (s) ->
		  let l2 = ListLatticeUtil.make_new_lat s.lat_list
		  in let _ = ListLatticeUtil.add_pred_val l2 listname listprop initval
		  in let newlat = State({lat_list = l2})
		  in change_prop newlat listname tail initval

    let aux_trunc_list_length l name=
      match l with
	| State(s) -> ListLatticeUtil.trunc_list_length s.lat_list name
	| _ -> () (*do nothing!*)
		       
    let post_list_init (l:lattice) formals =
      let _ = print_string "post list init with:"
      in let _ = Message.log_print_norm Message.ListLat print l
      in 
      let listname = List.nth formals 0
      in let arr_listprop = get_predicate_from_formal l (List.nth formals 1) listname
      in let _ = aux_trunc_list_length l listname
      in let initval = get_init_pred_val (List.nth formals 2)
      in ([], change_prop l listname arr_listprop initval)
	  
    let rec r_aux_make_predicates fret preds predvals outlist =
      if (List.length preds) = 0 then outlist
      else
	let hpred = List.hd preds
	in let hpredval = List.hd predvals
	in match hpredval with
	  | ListLatticeUtil.Bottom ->
	      failwith "something seriously wrong in here: r_aux_make_predicates@lattice.ml"
	  | ListLatticeUtil.Epsilon ->
	      let _ = Message.log_string_norm Message.ListLat "WARNING: bast_list_remove called on empty list"
	      in 
		r_aux_make_predicates fret (List.tl preds) (List.tl predvals) outlist
	  | ListLatticeUtil.Omega | ListLatticeUtil.Top ->
		r_aux_make_predicates fret (List.tl preds) (List.tl predvals) outlist
	  | ListLatticeUtil.Zero ->
	      let fpred = make_not_predicate fret hpred
	      in Message.log_string_norm Message.ListLat ("\nPredicate formed::"^(Predicate.toString fpred)^"::\n");
		r_aux_make_predicates fret (List.tl preds) (List.tl predvals) (outlist @ [fpred])
	  | ListLatticeUtil.Forall ->
	      let fpred = make_predicate fret hpred
	      in Message.log_string_norm Message.ListLat ("\nPredicate formed::"^(Predicate.toString fpred)^"::\n");
		r_aux_make_predicates fret (List.tl preds) (List.tl predvals) (outlist @ [fpred])
	  
    let r_make_predicates fret preds predvals =
      r_aux_make_predicates fret preds predvals []
	  
    let post_list_remove (l:lattice) (formals,fret2) =
      let list = List.nth formals 0
      in let fret = List.nth formals 1
      in let _ = Message.log_string_dbg Message.ListLat
		   ("Called post list remove with :"^(Expression.toString list)^":"^(Expression.toString fret))
      in let _ = Message.log_print_dbg Message.ListLat print l
      in
	begin
	  match l with
	    | Bottom -> failwith "Called post_remove on a bottom list - illegal!!"
	    | Top -> (*Not do anything in this case!*)
		([], l)
	    | State (s) ->
		let preds  = ListLatticeUtil.get_list_preds s.lat_list list
		in let predvals = ListLatticeUtil.get_list_preds_vals s.lat_list list 
		in let fret_preds = r_make_predicates fret preds predvals
		in let newlatlist = ListLatticeUtil.make_new_lat s.lat_list
		in let _ = ListLatticeUtil.dec_list_length newlatlist list
		in let finallattice = State ({lat_list = newlatlist})
		in let _ = Message.log_string_dbg Message.ListLat "Final lattice:"
		in let _ = Message.log_print_dbg Message.ListLat print finallattice
		in (fret_preds, finallattice)
	end

    let predvaltostring pval =
      match pval with
	| Ast.True -> "True"
	| Ast.False -> "False"
	| Ast.Dontknow -> "Dunno"
      
    let rec a_aux_make_predicates fret preds predvals outlist m_query_fn =
      if (List.length preds) = 0 then outlist
      else
	let hpred = List.hd preds
	in let hpredval = List.hd predvals
	in let fpred = make_predicate fret hpred
	in let fval = m_query_fn fpred
	in
	  match hpredval with
	  | ListLatticeUtil.Bottom ->
	      failwith "something seriously wrong in here: aux_make_predicates@lattice.ml"
	  | ListLatticeUtil.Epsilon ->
	      if (fval=Ast.Dontknow)
	      then a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                (outlist @[ListLatticeUtil.Top]) m_query_fn
	      else
		if (fval = Ast.True)
		then a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                  (outlist@[ListLatticeUtil.Forall]) m_query_fn
		else a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                  (outlist @ [ListLatticeUtil.Zero]) m_query_fn
	  | ListLatticeUtil.Top ->
	      if (fval = Ast.Dontknow)
	      then a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                (outlist@[ListLatticeUtil.Top]) m_query_fn
	      else a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                (outlist@[ListLatticeUtil.Omega]) m_query_fn
	  | ListLatticeUtil.Omega  ->
	      a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                           (outlist @ [ListLatticeUtil.Omega]) m_query_fn
	  | ListLatticeUtil.Zero ->
	      if (fval = Ast.False)
	      then  a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                 (outlist@[ListLatticeUtil.Zero]) m_query_fn
	      else  a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                 (outlist@[ListLatticeUtil.Omega]) m_query_fn
	  | ListLatticeUtil.Forall ->
	      if (fval = Ast.True)
	      then a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                (outlist@[ListLatticeUtil.Forall]) m_query_fn
	      else a_aux_make_predicates fret (List.tl preds) (List.tl predvals)
		                                (outlist@[ListLatticeUtil.Omega]) m_query_fn
	  
    let a_make_predicates fret preds predvals m_query_fn =
      a_aux_make_predicates fret preds predvals [] m_query_fn

    let post_list_add (l:lattice) formals m_query_fn =
      let _ = Message.log_string_dbg Message.ListLat "adding to lattice::"
      in let _ =  Message.log_print_dbg Message.ListLat print l
      in let list = List.nth formals 0
      in let elmnt = List.nth formals 1
      in begin
	  match l with
	    | Bottom -> failwith "Called post_add on a bottom list - illegal!!"
	    | Top -> (* DOUBT:: what to do in this case? -- doing nothing for the moment *)
		([], l)
	    | State (s) ->
		let preds = ListLatticeUtil.get_list_preds s.lat_list list
		in let predvals = ListLatticeUtil.get_list_preds_vals s.lat_list list
		in let newpredvals = a_make_predicates elmnt preds predvals m_query_fn
		in let newlistlength = ListLatticeUtil.get_list_length s.lat_list list
		in let _newlatlist = ListLatticeUtil.make_new_lat s.lat_list
		in let newlatlist =
		    { lat_list = ListLatticeUtil.make_new_list _newlatlist list preds newpredvals (newlistlength+1) }
		in let _ = Message.log_string_dbg Message.ListLat "New lattice formed:::"
		in let _ = Message.log_print_dbg Message.ListLat print (State(newlatlist))
		in ([], State(newlatlist))
	end

    let make_length_pred lenvar len =
      let lenexpr = Expression.Constant (Constant.Int 0)
      in let predexp =
	  if (len > 0)
	  then Predicate.Not (Predicate.Atom (Expression.Binary (Expression.Eq, lenvar, lenexpr)))
	  else Predicate.Atom (Expression.Binary (Expression.Eq, lenvar, lenexpr))
      in predexp
	   
    let post_list_length l formals =
      let _ = Message.log_string_dbg Message.ListLat "getting length from lattice::"
      in let _ =  Message.log_print_dbg Message.ListLat print l
      in let list = List.nth formals 0
      in let lenvar = List.nth formals 1
      in 
	  match l with
	    | Bottom -> failwith "Called post_length on a bottom list - illegal!!"
	    | Top ->
		([], l)
	    | State (s) ->
		let listlen = ListLatticeUtil.get_list_length s.lat_list list
		in let _ = Message.log_string_dbg Message.ListLat ("List length:"^(string_of_int listlen)^"\n")
		in let lenpred = make_length_pred lenvar listlen
		in let _ = Message.log_string_dbg Message.ListLat
			     ("returning length predicate:"^(Predicate.toString lenpred)^"\n")
		in ([lenpred], State (s))

    (* my own deconstruct function call - coz i needed customized output *)
    let deconstructFunCall fn_expr =
      match fn_expr with
	| Expression.FunctionCall(Expression.Lval (Expression.Symbol(fname)), elist) ->
	    let junkret:Expression.lval =  Expression.Symbol ("mrm_junkreturn")
	    in (fname, elist, junkret)
	| Expression.Assignment (Expression.Assign,target, 
			   Expression.FunctionCall(Expression.Lval (Expression.Symbol(fname)), elist)) ->
	    (fname, elist, target)
	| _ -> Message.log_string_norm Message.ListLat
	    ("deconstructFunCall ignoring strange function expression (probably a fn pointer): " ^
	     (Expression.toString fn_expr));
	    ("mrm_junkreturn", [], Expression.Symbol ("mrm_junkreturn"))

	
    let post_cmd (l:lattice) (c:C_SD.Command.t) (program_location:string) m_query_fn  =
      match c.Command.code with
	| Command.FunctionCall (fn_expr) ->
	    let (fname, formals, fret) = deconstructFunCall fn_expr
	    in begin
		let _ = Message.log_string_norm Message.ListLat ("calling list function:"^fname^": for:")
		in let _ = Message.log_print_norm Message.ListLat print l
		in 
		if (fname = list_init) then post_list_init l formals 
		else if (fname = list_remove) then post_list_remove l (formals, Expression.Lval fret) 
		else if (fname = list_add) then post_list_add l formals m_query_fn
		else if (fname = list_length) then post_list_length l formals 
		else ([],l)
	      end
	| _ -> ([],l)  (* you basically return the same lattice otherwise *)

    (* passively passes on the burden to the post_cmd function *)
    let post (l:lattice) (e:edge)  fn  =
      post_cmd l (C_SD.get_command e) (C_SD.get_edge_name e) fn 

    let summary_post l _ _ _ = l 

    let pre a b =  top

    let initialize _ =  ()

    let enabled_ops _ = []

  end
