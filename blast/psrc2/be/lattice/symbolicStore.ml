(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
(** Module implementing low level operations needed to build a lattice for tracking the heap *)

(** 
{1 Overview}
This module attempts to model the C program store (heap and variables) for use in a lattice. It provides
the ability to rule out infeasible branches and to provide the aliasing information at a given program
point.

{2 The Memory Model}
The store is made up of uniquely identifiable, contiguous areas called {i regions}. There are two
top-level namespaces for regions: {i Variable}, for program variables, and {i Heap} for the heap.
Structures are modeled as subregions within a parent region. Each region and subregion has an
associated property indicating whether it is an array. If a region is an array, we do not track
the values of individual array elements -- we combine all values assigned to the region. 

A location in memory is identified by an {i address}. There are three types of addresses:
 - {i Region} - identifies the first memory location of a region
 - {i Offset} - identifies a memory location in a region, not necessarily the first.
Array accesses and pointer arithmetic are modeled as offset addresses.
 - {i Null} - The Null address

A {i pointer} is a program value containing an address. In a store that has not undergone the union operation
a pointer simply contains a single address (this is called a {i MustPointer}). However, when two regions with
different values are unioned, we no longer know for certain whether the given pointer refers to the specifc
address. A {i MayPointer} is used to represent this situation -- it is simply a set of addresses.
There are situations where we know the value of a pointer's target, but the pointer has more than one possible
address. We represent this using a {i PseudoPointer}, which contains a target value and a set of addresses.
Finally, a {i PoisonPointer} refers to a region that has been dellocated.

{2 Evaluation}
To be written...

{2 Organization}
There are two submodules in this module:
 - {b AlgorithmOptions} contains variables enabling/disabling various heuristics
 - {b Value} provides the data types and functions for region values.

Following these modules are the type declarations for the store, code for managing backpointers,
and the main entry points for the store (eval, cannonicize_lval, union, leq, etc). The long-term
goal is to provide enough abstraction in the Value submodule so that the code in the main module
does not need to do any matching on value subtypes.

{2 TODO}
 - heap merging
 - Null casting and array initialization.
 - Better predicate handling - pointers and NULL casting.
 - Mode for dropping changed constants
 - Do we need a different kind of pointer set for arrays? E.g. for bad derefs and merging poison pointers.
 - Can we use weak pointers to reduce maintenance of the backpointer table?

{1 Design Notes}
Here are some notes about some of the trickier implementation details.

{2 Pseudo Pointers}
There are two main situations where we create PseudoPointers:
 - The union of two unequal pointers, where the target value is known and is the same.
 - Assignment to an lvalue of the form *x, where x is a variable which evaluates to a MayPointer.

If we assign to a PseudoPointer, we must invalidate the underlying pointer targets as well as any other
PseudoPointers which have intersecting targets. If we assign to one of the targets, we must invalidate
all PseudoPointers including that target.

{v  Problem case 1: v}
{v  \[a = 1; b = 2; x = \{a,b\}\] v}
{v  *x = 5 v}
{v  \[a = T; b = T; x = \{a,b\}=>5\] v}
{v  a = 3 v}
{v  \[a = 3; b = T; x = \{a,b\}\] Need to invalidate x! v}

{v Problem case 2: v}
{v  \[a = 1; b = 2; c = 3; x = \{a, b\}; y = \{a, c\}\] v}
{v  *x = 5 v}
{v  \[a = T; b = T; c = 3; x = \{a, b\}=>5; y = \{a, c\}\] v}
{v  *y = 4 v}
{v  \[a = T; b = T; c = T; x = \{a, b\}; y = \{a, c\}=>4\] Need to invalidate all pseudo ptrs with overlapping sets v}

Data Structures:
 - pseudo_pointer : Contains id, (mutable) target value, and may set.
 - PseudoPointer : Pointer subtype with reference to a pseudo pointer
 - referencedBy : (region to RegionSet) map  => Set of regions which reference the region in key (may and must ptrs)

The pseudo pointer id is used to identify equivalent pseudo pointers to the one being assigned.
These are not invalidated. 

Assignment of v to region r:
{[  replace store r v ]}
{[  foreach region r' in referencedBy\[r\]: ]}
{[    if values\[r'\] is a PseudoPointer, replace the PseudoPointer with a MayPointer ]}

Assignment of v to *r where r = maypointer(s):
{[  For each r' in s: ]}
{[    Invalidate r' in store ]}
{[    For each r'' in referencedBy\[r'\]: ]}
{[      if values\[r''\] is a PseudoPointer, replace the PseudoPointer with a MayPointer ]}
{[    Create new pseudo pointer with target_val = v and set r to this pointer ]}

Assignment of v to *r where r = PseudoPointer(id, s)
{[   Same as assignment to MayPointer, but keep existing pseudo-pointer ad don't invalidate any ]}
{[     pseudo-pointers with same id. ]}


{2 Structures and Member Access}
Structures are represented as a nested hash table within their parent region. Members
are identified through the StructMem region. A StructMem does not have its own entry
in the values table -- is a subregion of a base region.
When accessing a StructMem, the lookup, find, and replace functions must traverse from
the base region entry to find the appropriate member.
However, the backpointers table contains entries for individual members, since there can
be pointers to these members.

The following subsections describe the key operations on structures.

{3 Canonicize}
If op is ".", call canonicize_expr on expr. If op is ->, call eval on expr.
Below are the cases for canonicize of (op, expr, m). The function ptr_to_mem takes a pointer
and a member name and converts each address in the pointer to a member access of the orginal
address.

{v Operation | eval/canonicize           | Result v}
{v ----------|---------------------------|-------------------------- v}
{v     .     | cz => RegionLval(r)       | RegionLval(r, m) v}
{v     .     | cz => Dereference(ptr)    | Dereference (pointer_for_member(ptr, m)) v}
{v     ->    | eval => Pointer(ptr)      | Dereference(pointer_for_member(ptr, m)) v}

{3 Eval}
If operation is ".", call canonicize_expr on expr. If result is a region,
return contents of region. If result is a Dereference, return the appropriate
member of each address in pointer.

If operation is "->" return the appropriate member of each address in pointer.

{3 Assign}
When assigning through a structure member, may need to create entries for intermediate
levels. If the base region is an array, we treat all members transitively as arrays.

Assignments of structures cause a deep copy to occur.

{3 Union}
When taking the union of two structures, recursively take the union of each member.
The union of a structure and Top is a structure build by taking the union of each
member with Top. The union of a structure value and any other type is an error.

{3 Removal of local variables}
Since the members are underneith the base region, removing the region removes all
members. When checking whether a pointer to a structure member is a poison pointer,
just look at the base region.

{3 Invalidation of structure values}
Recursively, look at each structure member. If a non-pointer, set to Top. If a pointer,
make it a may pointer.

{2 Merging of Heap Locations}
If we create a new heap location whenever a malloc is encountered, and do not merge any locations,
we will go into an infinite loop whenever a malloc occurs within a loop. The current solution to
this is the following heuristic:
 - We have a single heap location for each program point where a malloc occurs.
 - On the first malloc for this location, we simply create a new region.
 - If the region already exists, don't create a new one -- we make the existing one an array.

This will have the effect of merging all locations after the first malloc for a given location.
Another option would be to create new locations with each malloc and then merge them when we do
a union. This will make the union operation more expsensive but may allow further optimizations
such as separating regions that have a different value for a specified predicate.
*)


(** {1 AlgorithmOptions sub-module} *)

(** This module contains parameters which enable/disable various heuristics used by the symbolic
    store. In general, there is a trade-off between precision and performance. *)
module AlgorithmOptions =
struct
  (** If true, do details function call tracing *)
  let trace_function_calls = false

  (** If true print a warning on null dereferences rather than throwing an exception *)
  let warn_on_null_deref = true

  (** We place options that may be changed dynamically in a record. An instance of this record
      is passed in whenever we create a fresh store. *)
  type options =
      { expr_is_array : Ast.Expression.expression -> bool; (** fn to determine whether expr is an array *)
	merge_string_constants : bool (** If true, merge all string constants to one location *)
      }
end

(** {1 Value sub-module} *)

(** Module for representing symbolic values and their operations. The datatypes for regions, addresses, pointers,
    structures, and values are defined here as well as the functions for operating on them. *)
module Value =
struct

  open AlgorithmOptions

  (** A base region identifies an entry in the symbolic store. *)
  type base_region = Variable of Ast.Symbol.symbol
		     | Heap of string

  (** A region identify a particular value in the symbolic store. It may be either a base region
    or a subregion (StructMem) *)
  type region_name = BaseRegion of base_region
                     | VarStructMem of string list (* base region followed by members *)
		     | HeapStructMem of string list (* base region followed by members *)

  type address = Region of region_name | Null | Offset of region_name
    
  module AddressSet = Set.Make (struct type t = address let compare a b = Pervasives.compare a b end )

  module RegionSet = Set.Make (struct type t = region_name let compare a b = Pervasives.compare a b end )

  type pseudo_pointer = { id : int; mutable target_val : value; mutable may_set : AddressSet.t }

  and pointer = MustPointer of address (** Pointer to a specific address *)
		| PseudoPointer of pseudo_pointer (** Pointer to a pseudo address and a may set *)
		| MayPointer of AddressSet.t  (** Pointer to a set of possible addresss *)
		| PoisonPointer (** Pointer to a deallocated address *)
  and entry = { v : value; is_array : bool; }
  and region = (string, entry) Hashtbl.t
  and value =   Top
	      | Constant of Ast.Constant.constant
	      | Pointer of pointer
              | Structure of region

  exception Bottom
  exception Invalid_access of string (** access of a pointer with deallocated or non-pointer value *)
  exception Uninit_ptr_access of pointer * string (** access or assignment to uninitialized pointer *)
  exception Null_ptr_deref of string (** dereference through a null pointer *)
	  

  (***********************************************************************)
  (** {2 Printing-related functions} *)

  (** Helper function to add members to a base region name *)
  let memRegionToString base_region_str mem_list =
    List.fold_left (fun str m -> str ^ "." ^ m) base_region_str mem_list
      
  let regionToString (r:region_name) =
    match r with
	BaseRegion (Variable s) -> s
      | BaseRegion (Heap s) -> "*Heap<" ^ s ^ ">"
      | VarStructMem mem_list -> memRegionToString (List.hd mem_list) (List.tl mem_list)
      | HeapStructMem mem_list -> memRegionToString ("*Heap<" ^(List.hd mem_list) ^ ">") (List.tl mem_list)

  (** A debug printer for regions - more verbose *)
  let printdbg_region fmt (r:region_name) =
    match r with
	BaseRegion (Variable s) -> Format.fprintf fmt "BaseRegion(%s)" s
      | BaseRegion (Heap s) -> Format.fprintf fmt "BaseRegion(*Heap<%s>)" s
      | VarStructMem mem_list ->
	  Format.fprintf fmt "VarStructMem(%s)"
	    (memRegionToString (List.hd mem_list) (List.tl mem_list))
      | HeapStructMem mem_list ->
	  Format.fprintf fmt "HeapStructMem(%s)"
	    (memRegionToString ("*Heap<" ^(List.hd mem_list) ^ ">") (List.tl mem_list))

  let rec addressToString a =
    match a with
	Region(BaseRegion (Variable s)) -> "&" ^ s
      | Region(BaseRegion (Heap s)) -> "Heap<" ^ s ^ ">"
      | Region(VarStructMem mem_list) -> "&" ^  (memRegionToString (List.hd mem_list) (List.tl mem_list))
      | Region(HeapStructMem mem_list) ->
	  memRegionToString ("*Heap<" ^(List.hd mem_list) ^ ">") (List.tl mem_list)
      | Null -> "Null"
      | Offset(r) -> "(" ^ (addressToString (Region r)) ^ "+?)"	  
	  
  and  maySetToString set =
    "{" ^
    (AddressSet.fold
       (fun a str ->
	  if str = "" then addressToString a
	  else str ^ "," ^ (addressToString a))
       set "")
    ^ "}"
	
  and pseudoPointerToString p_ptr =
    (maySetToString p_ptr.may_set) ^ "=>" ^ valueToString p_ptr.target_val

  and pointerToString ptr =
    match ptr with
	MustPointer(a) -> addressToString a
      | PseudoPointer(p) -> pseudoPointerToString p
      | MayPointer(set) -> maySetToString set
      | PoisonPointer -> "PoisonPointer"
	 
  (** A debug printer for addresses (more verbose) *)
  and printdbg_address fmt a =
    match a with
	Region(BaseRegion (Variable s)) ->
	  Format.fprintf fmt "&(%s)" s
      | Region(BaseRegion (Heap s)) -> Format.fprintf fmt "Heap<%s>" s
      | Region(VarStructMem mem_list) ->
	  Format.fprintf fmt "&(%s)" (memRegionToString (List.hd mem_list) (List.tl mem_list))
      | Region(HeapStructMem mem_list) ->
	  Format.fprintf fmt "%s"
	    (memRegionToString ("*Heap<" ^(List.hd mem_list) ^ ">") (List.tl mem_list))
      | Null -> Format.fprintf fmt "Null"
      | Offset(r) ->
	  Format.fprintf fmt "Offset(";
	  printdbg_address fmt (Region r);
	  Format.fprintf fmt ")"

  (** A debug printer for may sets *)
  and  printdbg_mayset fmt set =
    Format.fprintf fmt "@[{";
    ignore (AddressSet.fold
	      (fun a is_fst ->
		 if is_fst then printdbg_address fmt a
		 else (Format.fprintf fmt ",@ "; printdbg_address fmt a); false)
	      set true);
    Format.fprintf fmt "}@]"

  (** A debug printer for pointers *)
  and printdbg_pointer fmt ptr =
    match ptr with
	MustPointer(a) ->
	  Format.fprintf fmt "@[MustPointer(";
	  printdbg_address fmt a;
	  Format.fprintf fmt ")@]"
      | PseudoPointer(p) ->
	  Format.fprintf fmt "@[PseudoPointer(%s)@]"(pseudoPointerToString p)
      | MayPointer(set) ->
	  Format.fprintf fmt "@[MayPointer(";
	  printdbg_mayset fmt set;
	  Format.fprintf fmt ")@]"
      | PoisonPointer -> Format.pp_print_string fmt "PoisonPointer"
    
  and valueToString v =
    match v with
      | Top -> "Top"
      | Constant(c) -> Ast.Constant.toString c
      | Pointer(ptr) -> pointerToString ptr
      | Structure(s) ->
	  "struct {" ^
	  (Hashtbl.fold
	     (fun k entry str ->
		let entry_str = k ^ (if entry.is_array then "[*]=" else "=") ^ (valueToString entry.v) in
		  if str = "" then entry_str
		  else str ^ "; " ^ entry_str)
	     s "")
	  ^ "}"

  let rec regionSetToString set =
    "{" ^
    (RegionSet.fold
       (fun r str ->
	  if str = "" then regionToString r
	  else str ^ "," ^ (regionToString r))
       set "")
    ^ "}"

  (***********************************************************************)
  (** {2 Debug/tracing helper functions} *)

  let dbg_buffer_size = 30
  let dbg_recent_msgs = Array.make dbg_buffer_size ""
  let dbg_index = ref 0
  let dbg_num_msgs = ref 0

  let add_dbg_msg msg =
    Array.set dbg_recent_msgs !dbg_index msg;
    dbg_index := (!dbg_index + 1) mod dbg_buffer_size;
    if !dbg_num_msgs < dbg_buffer_size then dbg_num_msgs := !dbg_num_msgs + 1

  let print_dbg_msgs () =
    print_string "******************\n";
    print_string "Most recent trace messages:\n";
    let idx = ref 0 in
      while !idx < !dbg_num_msgs do
	print_string (Array.get dbg_recent_msgs ((!idx + !dbg_index) mod dbg_buffer_size));
	idx := !idx + 1
      done;
      print_string "******************\n";
      flush stdout;
      dbg_index := 0;
      dbg_num_msgs := 0

  (* Functions for debug tracing *)
  let dbgPrintCall (call_list:string list) =
    add_dbg_msg
      ("<DBG>  " ^ (List.hd call_list) ^
       (List.fold_left (fun str arg -> str ^ "  [" ^ arg ^ "]") "" (List.tl call_list)) ^
       "\n")
    (*flush stdout*)

  let dbgPrintResult (call_args:string list) (result:'a) (resToStringFn:'a->string) =
    add_dbg_msg ("<DBG>  Returning from " ^ (List.hd call_args) ^ " => " ^ (resToStringFn result) ^ "\n")
    (* flush stdout*)

  let dbg_wrap1 fn argToStringFn resToStringFn =
    if trace_function_calls then
      (fun arg1 ->
	 let call_args = argToStringFn arg1
	 in
	   dbgPrintCall call_args;
	   let result = fn arg1 in
	     dbgPrintResult call_args result resToStringFn; result)
    else fn
      
  let dbg_wrap2 fn argToStringFn resToStringFn =
    if trace_function_calls then
      (fun arg1 arg2 ->
	 let call_args = argToStringFn arg1 arg2
	 in
	   dbgPrintCall call_args;
	   let result = fn arg1 arg2 in
	     dbgPrintResult call_args result resToStringFn; result)
    else fn

  let dbg_wrap3 fn argToStringFn resToStringFn =
    if trace_function_calls then
      (fun arg1 arg2 arg3 ->
	 let call_args = argToStringFn arg1 arg2 arg3
	 in
	   dbgPrintCall call_args;
	   let result = fn arg1 arg2 arg3 in
	     dbgPrintResult call_args result resToStringFn; result)
    else fn

  let dbg_wrap4 fn argToStringFn resToStringFn =
    if trace_function_calls then
      (fun arg1 arg2 arg3 arg4 ->
	 let call_args = argToStringFn arg1 arg2 arg3 arg4
	 in
	   dbgPrintCall call_args;
	   let result = fn arg1 arg2 arg3 arg4 in
	     dbgPrintResult call_args result resToStringFn; result)
    else fn	  


  (***********************************************************************)
  (** {2 Functions related to combine_values} *)

  (** Convert a pointer to the set of addresses it points to *)
  let pointer_to_address_set ptr =
    match ptr with
	MustPointer a -> AddressSet.singleton a
      | PseudoPointer p_ptr -> p_ptr.may_set
      | MayPointer set -> set
      | PoisonPointer -> AddressSet.empty

  (** Convert a value to a set of addresses. Used for array assignments. *)
  let value_to_address_set v =
    match v with
	Pointer ptr -> pointer_to_address_set ptr
      | _ -> AddressSet.empty

  (** Combine two values. Used for array assignments *)
  let rec combine_values (v1:value) (v2:value) =
    match (v1, v2) with
	(Structure s1, Structure s2) ->
	  begin
	    let combined_struct = Hashtbl.create 5
	    in Hashtbl.iter
		 (fun mem s1_entry ->
		    try
		      let v' = (Hashtbl.find s2 mem).v
		      in Hashtbl.replace combined_struct mem {v = combine_values s1_entry.v v'; is_array = true}
		    with Not_found ->
		      Hashtbl.replace combined_struct mem {v = combine_values s1_entry.v Top; is_array = true})
		 s1;
	      Hashtbl.iter
		(fun mem s2_entry ->
		   if (Hashtbl.mem s1 mem) = false
		   then Hashtbl.replace combined_struct mem {v = combine_values s2_entry.v Top; is_array = true})
		s2;
	      Structure combined_struct
	  end
      | (Structure s, Top) | (Top, Structure s) ->
	  begin
	    let new_struct = Hashtbl.create 5
	    in
	      Hashtbl.iter
		(fun mem entry -> Hashtbl.replace new_struct mem {v = combine_values entry.v Top; is_array = true})
		s;
	      Structure new_struct
	  end
      | (Structure s, _) | (_, Structure s) ->
	  failwith ("combining structure and non-structure values not supported: " ^ (valueToString v1)
		      ^ ", " ^ (valueToString v2))
      | (Pointer p1, Pointer p2) ->
	  Pointer (MayPointer (AddressSet.union (pointer_to_address_set p1) (pointer_to_address_set p2)))
      | (Pointer p, _) | (_, Pointer p) -> Pointer (MayPointer (pointer_to_address_set p))
      | _ -> Top

  (***********************************************************************)
  (** {2 Functions related to structures} *)

  (** return the base region associated with this region. *)
  let get_base_region (r:region_name) =
    match r with
	BaseRegion b -> b
      | VarStructMem mem_list -> Variable (List.hd mem_list)
      | HeapStructMem mem_list -> Heap (List.hd mem_list)

  (** Return the region describing the specified member of the parent region *)
  let region_for_member (parent_region:region_name) (member_name:string) :region_name =
    match parent_region with
	BaseRegion (Variable n) -> VarStructMem [n; member_name]
      | BaseRegion (Heap n) -> HeapStructMem [n; member_name]
      | VarStructMem mem_list -> VarStructMem (mem_list @ [member_name])
      | HeapStructMem mem_list -> HeapStructMem (mem_list @ [member_name])

  (** Deep copy a structure *)
  let rec copy_structure s =
    let rtn_structure = Hashtbl.create 5 in
      Hashtbl.iter
	 (fun m entry ->
	   let v' =
	     (match entry.v with
		  Structure s' -> Structure (copy_structure s')
		| _ -> entry.v)
	   in Hashtbl.replace rtn_structure m {v = v'; is_array = entry.is_array})
	s;
      rtn_structure

  (** Cast a value to a structure. Raises an exception if the value isn't a structure.
      The caller parameter is a string used to identify the function in the exception. *)
  let cast_val_to_struct (v:value) (caller:string) :region =
    match v with
	Structure s -> s
      | _ -> failwith (caller ^ ": cannot cast value " ^ (valueToString v) ^ " to a structure")

  (** Does the structure have the specified member? *)
  let rec has_structure_mem (s:region) (mem_list:string list) =
    match mem_list with
	mem :: [] ->
	  if Hashtbl.mem s mem then true
	  else false
      | mem :: rest ->
	  if Hashtbl.mem s mem then
	    begin
	      match Hashtbl.find s mem with
		  {v = Structure s'} -> has_structure_mem s' rest
		| _ -> false
	    end
	  else false
      | _ -> failwith "has_structure_mem called with empty member list!"

  let raise_structure_error mem_list v =
    raise (Invalid_access
	     ("Attempt to access a non structure value as a structure: " ^
	      (List.fold_left (fun str mem -> if str = "" then mem else str ^ "." ^ mem)
		 "" mem_list) ^
	      "  value: " ^ (valueToString v)))
    
  (** Return the value of the specified structure member. If the member does not exist, throws Not_found. *)
  let rec find_structure_mem (s:region) (mem_list:string list) =
    match mem_list with
	mem :: [] -> (Hashtbl.find s mem).v
      | mem :: rest ->
	  begin
	    match Hashtbl.find s mem with
		{v = Structure s'} -> find_structure_mem s' rest
	      | {v = Top} -> raise Not_found
	      | entry -> raise_structure_error mem_list entry.v
	  end
      | _ -> failwith "find_structure_mem called with empty member list!"

  (** Return the value of the specified structure member. If the member does not exist, return Top *)
  let lookup_structure_mem (s:region) (mem_list:string list) =
    try
      find_structure_mem s mem_list
    with Not_found -> Top

  let struct_to_assoc_list (s:region) :((string*entry) list) =
    Hashtbl.fold
      (fun m entry lst -> (m, entry) :: lst)
      s []

  let rec is_structure_mem_an_array (s:region) (mem_list:string list) =
    try
      match mem_list with
	  mem :: [] -> (Hashtbl.find s mem).is_array
	| mem :: rest ->
	    begin
	      match Hashtbl.find s mem with
		  {v = Structure s'; is_array = false} -> is_structure_mem_an_array s' rest
		| {v = Structure s'; is_array = true} -> true
		| {v = Top; is_array = is_array} -> is_array
		| entry -> raise_structure_error mem_list entry.v
	    end
	| _ -> failwith "find_structure_mem called with empty member list!"
    with Not_found -> false

  let rec make_structure_mem_an_array (s:region) (mem_list:string list) (parent_is_array:bool) =
    match mem_list with
	mem :: [] ->
	  if Hashtbl.mem s mem then
	    begin
	      let entry = Hashtbl.find s mem in
		if entry.is_array = false then
		  begin
		    (* combine values will also set the array bit in any sub structures *)
		    Hashtbl.replace s mem {v = combine_values entry.v Top; is_array = true}
		  end
		else () (* nothing to do if already an array *)
	    end
	  else Hashtbl.add s mem {v = Top; is_array = true}
      | mem :: rest ->
	  if Hashtbl.mem s mem then
	    begin
	      match Hashtbl.find s mem with
		  {v = Structure s'; is_array = child_is_array} ->
		    make_structure_mem_an_array s' rest (parent_is_array || child_is_array)
		| {v = Top; is_array = is_array} ->
		    let (s':region) = Hashtbl.create 5 in
		      Hashtbl.replace s mem {v = Structure s'; is_array = is_array};
		      make_structure_mem_an_array s' rest is_array
		| entry -> raise_structure_error mem_list entry.v
	    end
	  else 
	    begin
	      let (s':region) = Hashtbl.create 5 in
		Hashtbl.add s mem {v = Structure s'; is_array = parent_is_array};
		make_structure_mem_an_array s' rest parent_is_array
	    end
      | _ -> failwith "make_structure_mem_an_array called with empty member list!"

  (** Set the structure member to the specified value. If the member is an array, then combine
      the old and new values. If the parent is an array, then all members are arrays as well.
      We return the old and new values, which may be needed in backpointer maintenance. *)
  let rec set_structure_value (s:region) (mem_list:string list) (v:value) (parent_is_array:bool) =
    match mem_list with
	mem :: [] ->
	  if Hashtbl.mem s mem then
	    begin
	      let entry = Hashtbl.find s mem
	      in let oldv = entry.v
		 and is_array = entry.is_array || parent_is_array
	      in let nv = if is_array then combine_values oldv v else v
	      in
		Hashtbl.replace s mem {v = nv; is_array = is_array};
		(oldv, nv)
	    end
	  else
	    begin
		  (* The entry does not exist - create a new entry *)
	      let nv = if parent_is_array then combine_values v Top else v
	      in Hashtbl.add s mem {v = nv; is_array = parent_is_array};
		(Top, nv)
	    end
      | mem :: rest ->
	  if Hashtbl.mem s mem then
	    begin
	      match Hashtbl.find s mem with
		  {v = Structure s'; is_array = struct_is_array} ->
		    set_structure_value s' rest v (parent_is_array || struct_is_array)
		| {v = Top; is_array = is_array} ->
		    let (s':region) = Hashtbl.create 5 in
		      Hashtbl.replace s mem {v = Structure s'; is_array = is_array};
		      set_structure_value s' rest v parent_is_array
		| entry -> raise_structure_error mem_list entry.v
	    end
	  else 
	    begin
	      let (s':region) = Hashtbl.create 5 in
		Hashtbl.add s mem {v = Structure s'; is_array = parent_is_array};
		set_structure_value s' rest v parent_is_array
	    end
      | _ -> failwith "set_structure_value called with empty member list!"

  (***********************************************************************)
  (** {2 Helper functions for pointers} *)

  (** Equivalent constant value for a null pointer *)
  let zero_pointer_value = (Constant (Ast.Constant.Int 0))

  let is_pointer (v:value) :bool = match v with Pointer _ -> true | _ -> false

  (** Return the heap region associated with the specified program location *)
  let get_heap_region program_location =
    (BaseRegion (Heap program_location))

  let get_summary_heap_region program_location =
    (BaseRegion (Heap (program_location ^ "_sum")))

  (** Return true if the region is on the heap *)
  let is_heap_region (r:region_name) =
    match r with
	BaseRegion (Heap _) | HeapStructMem _ -> true
      | _ -> false

  let _next_pseudo_ptr_id = ref 0

  (** Reset the pseudo pointer counter for unit tests *)
  let _reset_pseudo_ptr_id () = _next_pseudo_ptr_id := 0

  (** Create a new pseudo pointer *)
  let make_new_pseudo_pointer (v:value) (set:AddressSet.t) =
    let new_id = !_next_pseudo_ptr_id
    in
      _next_pseudo_ptr_id := !_next_pseudo_ptr_id + 1;
      PseudoPointer {id = new_id; target_val = v; may_set = set}

  (** Return true if the pointer references the specified region, either directory of as an offset *)
  let points_to (ptr:pointer) (r :region_name) =
    match ptr with
	MustPointer a ->
	  if (a = (Region r)) || (a = (Offset r)) then true else false
      | MayPointer set ->
	  if (AddressSet.mem (Region r) set) || (AddressSet.mem (Offset r) set)
	  then true
	  else false
      | PseudoPointer p_ptr ->
	  if (AddressSet.mem (Region r) p_ptr.may_set) || (AddressSet.mem (Offset r) p_ptr.may_set)
	  then true
	  else false
      | _ -> false

  (** Use a pointer as the base for an array access (e.g. ptr[i] or (ptr + i).
    Convert each target to an an offset address. *)
  let use_pointer_as_offset ptr =
    let rec addr_to_offset addr =
      match addr with
	  Region(r) | Offset(r) -> Offset(r)
	| Null -> raise (Invalid_access "Attempt to use Null pointer as a base of offset address")
    and addr_set_to_offset (set:AddressSet.t) =
      AddressSet.fold
	(fun a set -> AddressSet.add (addr_to_offset a) set)
	set
	AddressSet.empty
    in
      match ptr with
	  MustPointer(a) -> MustPointer(addr_to_offset a)
	| PseudoPointer(ps_ptr) -> MayPointer (addr_set_to_offset ps_ptr.may_set)
	| MayPointer(may_set) -> MayPointer(addr_set_to_offset may_set)
	| PoisonPointer -> raise (Invalid_access "attempt to use poison pointer as base of offset address")

  (** Return true if the pointer contains a null pointer *)
  let pointer_contains_null ptr =
    match ptr with
	MustPointer(Null) -> true
      | MustPointer(_) -> false
      | MayPointer(s) -> AddressSet.mem Null s
      | PseudoPointer(p_ptr) -> AddressSet.mem Null p_ptr.may_set
      | PoisonPointer -> false

  (** Convert a pointer to another pointer that accesses the specified member of the pointer *)
  let pointer_for_member ptr mem_name =
    let rec  address_for_member a =
      match a with
	  Region r -> Region (region_for_member r mem_name)
	| Offset r -> Offset (region_for_member r mem_name)
	| Null -> Null
    and addrset_for_member s =
      AddressSet.fold
	(fun a set ->
	   AddressSet.add (address_for_member a) set)
	s AddressSet.empty;
    in
      match ptr with
	  MustPointer a -> MustPointer (address_for_member a)
	    (* ??? For now, we treat a pseudo pointer as a may pointer. Eventually, we
	       may want to create a new pseudo pointer which points to a member in the
	       value of the parent pointer *)
	| PseudoPointer p_ptr -> MayPointer (addrset_for_member p_ptr.may_set)
	| MayPointer set -> MayPointer (addrset_for_member set)
	| PoisonPointer ->
	    raise (Invalid_access "Attempt to perform member access through PoisonPointer")

  (** Take the union of two pointers. To support creating a pseudo pointer when two values are the
      same, two lookup functions are passed in. lookup_fn1 looks up a value in the store associated
      with pointer p1 and lookup_fn2 looks up a value in the store associated with pointer p2. These
      functions should return Top if the region does not exist in the store. *)
  let pointer_union (lookup_fn1:region_name -> value) (p1:pointer)
                    (lookup_fn2:region_name -> value) (p2:pointer) =
  if p1 = p2 then p1
  else
    begin
      match (p1, p2) with
	  (* The union of a poison pointer with any other pointer is still a poison pointer.
	     This is because any attempt to dereference this pointer will cause an error. *)
	  (* ???  We should treat array values differently, but still need to add support for them. *)
	  (PoisonPointer, _) | (_, PoisonPointer) -> PoisonPointer
	  (* everything else gets merged to a may pointer *)
	| (MayPointer s1, MayPointer s2) -> MayPointer (AddressSet.union s1 s2)
	| (MayPointer s, MustPointer a) | (MustPointer a, MayPointer s) ->
	    MayPointer (AddressSet.add a s)
	| (MustPointer (Region r1), MustPointer (Region r2)) ->
	    (* If the values of the two regions are the same, we can create a pseudo pointer with the value *)
	    let v_r1 = lookup_fn1 r1
	    and v_r2 = lookup_fn2 r2
	    and set = (AddressSet.add (Region r2) (AddressSet.singleton (Region r1)))
	    in if (v_r1 = v_r2) && (v_r1 <> Top) then make_new_pseudo_pointer v_r1 set
	      else MayPointer set
	| (MustPointer a1, MustPointer a2) ->
	    MayPointer(AddressSet.add a1 (AddressSet.singleton a2))
	| (PseudoPointer p_ptr, MayPointer s) | (MayPointer s, PseudoPointer p_ptr) ->
	    MayPointer(AddressSet.union s p_ptr.may_set)
	| (PseudoPointer p_ptr, MustPointer (Region r)) ->
	    let v_r = lookup_fn2 r
	    and set = AddressSet.add (Region r) p_ptr.may_set
	    in
	      if (v_r = p_ptr.target_val) && (v_r <> Top) then make_new_pseudo_pointer v_r set
	      else MayPointer set
	| (MustPointer (Region r), PseudoPointer p_ptr) ->
	    let v_r = lookup_fn1 r
	    and set = AddressSet.add (Region r) p_ptr.may_set
	    in
	      if (v_r = p_ptr.target_val) && (v_r <> Top) then make_new_pseudo_pointer v_r set
	      else MayPointer set
	| (PseudoPointer p_ptr, MustPointer a) | (MustPointer a, PseudoPointer p_ptr) ->
	    MayPointer (AddressSet.add a p_ptr.may_set)
	| (PseudoPointer p_ptr1, PseudoPointer p_ptr2) ->
	    let set = AddressSet.union p_ptr1.may_set p_ptr2.may_set
	    and v1 = p_ptr1.target_val
	    and v2 = p_ptr2.target_val
	    in
	      if (v1 = v2) && (v1 <> Top)
	      then make_new_pseudo_pointer v1 set
	      else MayPointer set
    end

  (** Take the union of two values *)
  let rec union_values (lookup_fn1:region_name -> value) (v1:value)
                       (lookup_fn2:region_name -> value) (v2:value) =
    match (v1, v2) with
	(Pointer p1, Pointer p2) when p1 = p2 -> v1
      | (Pointer p1, Pointer p2) -> Pointer (pointer_union lookup_fn1 p1 lookup_fn2 p2)
      | (Structure s1, Structure s2) ->
	  begin
	    let new_struct = Hashtbl.create 5 in
	      Hashtbl.iter
		(fun m s1_entry ->
		   try
		     let s2_entry = Hashtbl.find s2 m in
		       Hashtbl.replace new_struct m
			 {v = (union_values lookup_fn1 s1_entry.v lookup_fn2 s2_entry.v);
			  is_array = s1_entry.is_array || s2_entry.is_array}
		   with Not_found ->
		     Hashtbl.replace new_struct m
		        {v = (union_values lookup_fn1 s1_entry.v lookup_fn2 Top); is_array = s1_entry.is_array})
		s1;
	      Hashtbl.iter
		(fun m s2_entry ->
		   if (Hashtbl.mem s1 m) = false
		   then Hashtbl.replace new_struct m
		          {v = (union_values lookup_fn1 Top lookup_fn2 s2_entry.v); is_array = s2_entry.is_array})
		s2;
	      Structure new_struct
	  end
      | (Structure s1, Top) ->
	  let new_struct = Hashtbl.create 5 in
	    Hashtbl.iter
	      (fun m s1_entry ->
		   Hashtbl.replace new_struct m
                     {v = (union_values lookup_fn1 s1_entry.v lookup_fn2 Top); is_array = s1_entry.is_array})
	      s1;
	    Structure new_struct
      | (Top, Structure s2) ->
	  let new_struct = Hashtbl.create 5 in
	    Hashtbl.iter
	      (fun m s2_entry ->
		   Hashtbl.replace new_struct m
		     {v = (union_values lookup_fn2 s2_entry.v lookup_fn1 Top); is_array = s2_entry.is_array})
	      s2;
	    Structure new_struct
      | (Structure s, _) | (_, Structure s) ->
	  failwith "Attempt to take the union of a structure value and a non-structure value"
      | _ -> if v1 = v2 then v1 else Top

  let rec intersect_values v v' =
    match (v, v') with
	(Pointer PoisonPointer, Pointer PoisonPointer) -> ()
      | (Pointer (PoisonPointer), _) | (_, Pointer (PoisonPointer)) -> raise Bottom
      | (Pointer (MustPointer a1), Pointer (MustPointer a2)) when a1 <> a2 -> raise Bottom
      | (Pointer p, Constant c) | (Constant c, Pointer p) ->
	  if (v' <> zero_pointer_value) && ((pointer_contains_null p) = false)
	  then raise Bottom (* allow for null pointer = 0 *)
      | (Constant c1, Constant c2) when c1 <> c2 -> raise Bottom
      | (Structure s, Structure s') ->
	  begin
	    Hashtbl.iter
	      (fun m s_entry ->
		 if Hashtbl.mem s' m then intersect_values s_entry.v (Hashtbl.find s' m).v)
	      s;
	  end
      | _ -> ()

  (** Raise Not_found if p1 is not contained within p2. To check containment we first look for poison pointers.
     If neither is a poison pointer, we convert each pointer to an address set. We then check whether all
     addresses in the first pointer's set are contained in the second pointers set. If not, we raise an exception.

     We treat Offset pointers as more general than Region pointers. If p1 is a region pointer to r and p2 is
     an off set pointer to r, we consider p1 leq p2. However, the reverse is not true.
  *)
  let pointer_leq (p1:pointer) (p2:pointer) =
    if p1 <> p2 then
      begin
	(match (p1, p2) with
	     (PoisonPointer, _) | (_, PoisonPointer) -> raise Not_found
	   | _ -> ());
	let set1 = value_to_address_set (Pointer p1)
	and set2 = value_to_address_set (Pointer p2)
	in
	  AddressSet.iter
	    (fun a ->
	       match a with
		   Region r ->
		     if not (AddressSet.mem (Region r) set2) &&
		       not (AddressSet.mem (Offset r) set2)
		     then raise Not_found
		 | Offset r ->
		     if not (AddressSet.mem (Offset r) set2)
		     then raise Not_found
		 | Null -> if not (AddressSet.mem Null set2) then raise Not_found)
	    set1
      end

  (** Check two pointers for equality and inequality. Returns (true, false) if the pointers are definitely
      equal, (false, true) if they are definately not equal, and (true, true) if nothing can be concluded. *)
    let compare_pointers (p1:pointer) (p2:pointer) :(bool*bool) =
      match (p1, p2) with
	  (MustPointer (Region r1), MustPointer (Region r2)) ->
	    if r1 = r2 then (true, false)
	    else (false, true)
	| (MustPointer (Region r), MustPointer (Offset o))
	| (MustPointer (Offset o), MustPointer (Region r)) ->
	    if r = o then (true, true)
	    else (false, true)
	| _ -> (* XXX Do we need to somehow convert the offset addresses to regions for comparison purposes? *)
	    if AddressSet.is_empty (AddressSet.inter (pointer_to_address_set p1) (pointer_to_address_set p2))
	    then (false, true)
	    else (true, true)

  (** Raise Not_found if v1 is not contained within v2. Note that leq Pointer Top = false because
      we cannot lose any pointer information. *)
  let rec leq_values v1 v2 =
    match (v1, v2) with
	(Top, Top) | (Constant _, Top) -> ()
      | (Top, Structure s) ->
	  (* When comparing a hash table to top, compare to all members *)
	  Hashtbl.iter (fun m entry -> leq_values Top entry.v) s
      | (Top, _) -> raise Not_found
      | (Pointer p, Top) -> raise Not_found
      | (Constant c1, Constant c2) -> if c1 <> c2 then raise Not_found
      | (Constant c, Pointer p) ->
	  if (v1 <> zero_pointer_value) || ((pointer_contains_null p) = false)
	  then raise Not_found
      | (Pointer p, Constant c) ->
	  if ((Constant c) <> zero_pointer_value) || (p <> (MustPointer Null))
	  then raise Not_found
      | (Pointer p1, Pointer p2) -> pointer_leq p1 p2
      | (Structure s1, Structure s2) ->
	  Hashtbl.iter
	    (fun m s2_entry ->
	       if Hashtbl.mem s1 m then leq_values (Hashtbl.find s1 m).v s2_entry.v
	       else leq_values Top s2_entry.v)
	    s2;
	  Hashtbl.iter
	    (fun m s1_entry ->
	       if (Hashtbl.mem s2 m) = false then leq_values s1_entry.v Top)
	    s1;
      | (Structure s, Top) ->
	  (* When comparing a hash table to top, compare to all members *)
	  Hashtbl.iter (fun m entry -> leq_values entry.v Top) s
      | (Structure _, _) | (_, Structure _) -> raise Not_found

  (** Return a deep copy of the specified value. If the value is a pseudo pointer, we look it up in the
      table. If the pointer is in the table, we return that one. Otherwise, we create a new copy. *)
  let rec deep_copy_value (v:value) (pseudo_pointers:(int,pseudo_pointer)Hashtbl.t) :value =
    match v with
      | Pointer (PseudoPointer p_ptr) ->
	  begin
	    try
	      Pointer (PseudoPointer (Hashtbl.find pseudo_pointers p_ptr.id))
	    with Not_found ->
	      let new_p_ptr = {id = p_ptr.id; target_val = Top; may_set = p_ptr.may_set}
	      in
		Hashtbl.replace pseudo_pointers p_ptr.id new_p_ptr;
		new_p_ptr.target_val <- deep_copy_value p_ptr.target_val pseudo_pointers;
		Pointer (PseudoPointer new_p_ptr)
	  end
      | Top | Constant _ | Pointer _ -> v (* non-mutable values *)
      | Structure s -> Structure (deep_copy_structure s pseudo_pointers 5)

  and deep_copy_structure (s:region) (pseudo_pointers:(int,pseudo_pointer)Hashtbl.t)
                          (start_size:int) :region =
    let s' = Hashtbl.create start_size in
      Hashtbl.iter
	(fun m entry ->
	   Hashtbl.replace s' m {v = deep_copy_value entry.v pseudo_pointers;
				 is_array = entry.is_array})
	s;
      s'

    

end

(***********************************************************************)
(** {1 The main body of SymbolicStore} *)

open AlgorithmOptions
open Value

type lvalue = RegionLval of region_name | Dereference of pointer | PseudoPtrMem of pseudo_pointer * (string list)

type refd_by_tbl = (Value.region_name, Value.RegionSet.t) Hashtbl.t
  
type t = { var_values:region; heap_values:region; referencedBy:refd_by_tbl; options:options }
    
exception Poison_access (** Used internally to signal that a deallocated value was referenced *)
exception Not_satisfiable (** Thrown when a predicate is found to be not satisfiable *)

(** {2 Low-level functions to manipulate store} *)

(** create an empty store *)
let create_new_store (options:options) :t =
  let (var_vals:region) = (Hashtbl.create 31)
  and (heap_vals:region) = (Hashtbl.create 31)
  and (backpointers:refd_by_tbl) = (Hashtbl.create 15)
  in { var_values = var_vals; heap_values = heap_vals; referencedBy = backpointers; options = options }

let mem (store:t) (key:region_name) =
  match key with
      BaseRegion (Variable n) -> Hashtbl.mem store.var_values n
    | BaseRegion (Heap n) -> Hashtbl.mem store.heap_values n
    | VarStructMem mem_list -> has_structure_mem store.var_values mem_list
    | HeapStructMem mem_list -> has_structure_mem store.heap_values mem_list
	  
(** Find the value associated with the specified region. Throws Not_found *)    
let find (store:t) (key:region_name) =
  match key with
      BaseRegion (Variable n) -> (Hashtbl.find store.var_values n).v
    | BaseRegion (Heap n) -> (Hashtbl.find store.heap_values n).v
    | VarStructMem mem_list -> find_structure_mem store.var_values mem_list
    | HeapStructMem mem_list -> find_structure_mem store.heap_values mem_list
       
(** version of find which returns Top if value not found in store *)
let lookup (store:t) (key:region_name) =
  try
    find store key
  with Not_found -> Top

let store_to_assoc_list (store:t) =
  let fold_tbl tbl lst = Hashtbl.fold (fun n entry list -> (n, entry) :: list) tbl lst
  in
    Hashtbl.fold
      (fun n entry list -> (BaseRegion (Variable n), entry) :: list)
      store.var_values
      (Hashtbl.fold
	 (fun n entry list -> (BaseRegion (Heap n), entry) :: list)
	 store.heap_values [])

let is_array_region (store:t) (key:region_name) =
  try
    match key with
	BaseRegion (Variable n) -> (Hashtbl.find store.var_values n).is_array
      | BaseRegion (Heap n) -> (Hashtbl.find store.heap_values n).is_array
      | VarStructMem mem_list -> is_structure_mem_an_array store.var_values mem_list
      | HeapStructMem mem_list -> is_structure_mem_an_array store.heap_values mem_list
  with
      Not_found -> (* Message.msg_string Message.Normal "<SSL>is_array_region raised Not_found" ; *) false


(** A region is being used as an array. Adjust the flag in the store and make any pointer a may pointer *)
let make_region_an_array (store:t) (key:region_name) =
  let make_base_rgn par_region n =
    try
      let old_entry = Hashtbl.find par_region n
      in
	if not old_entry.is_array
	then Hashtbl.replace par_region n
	  {v = combine_values old_entry.v Top; is_array = true}
    with Not_found -> Hashtbl.replace par_region n {v = Top; is_array = true}
  in
    match key with
	BaseRegion (Variable n) -> make_base_rgn store.var_values n
      | BaseRegion (Heap n) -> make_base_rgn store.heap_values n
      | VarStructMem mem_list -> make_structure_mem_an_array store.var_values mem_list false
      | HeapStructMem mem_list -> make_structure_mem_an_array store.heap_values mem_list false



(** Remove region r from the RegionSets in backpointers table. This is done by extracting the
    points-to set S from v and then removing r from the RegionSets associated with each region in S. *)
let rec remove_backpointers (store: t) (r:region_name) (v:value) =
  AddressSet.iter
    (fun a ->
       match a with
	   (Region r') | (Offset r') ->
	     begin
	       try
		 let new_set = RegionSet.remove r (Hashtbl.find store.referencedBy r')
		 in
		   if new_set <> RegionSet.empty
		   then Hashtbl.replace store.referencedBy r' new_set
		   else Hashtbl.remove store.referencedBy r'
	       with Not_found ->
		 (* r has a pointer to r', but r' has no referencedBy set. This is OK if both are
		    from the same function, and we are removing them in a call to remove_locals *)
		 ()
	     end
	 | _ -> ())
    (value_to_address_set v);
  match v with
      Structure s ->
	Hashtbl.iter (fun m entry -> remove_backpointers store (region_for_member r m) entry.v) s
    | _ -> ()

(** Add region r to RegionSets in the backpointers table. This is done by extracting the
    points-to set S from v and then adding r to each of the RegionSets associated with each region in S. *)
let rec add_backpointers store r v =
  AddressSet.iter
    (fun a ->
       match a with
	   (Region r') | (Offset r') ->
	     begin
	       if Hashtbl.mem store.referencedBy r' then
		 Hashtbl.replace store.referencedBy r'
		   (RegionSet.add r (Hashtbl.find store.referencedBy r'))
	       else Hashtbl.replace store.referencedBy r' (RegionSet.singleton r)
	     end
	 | _ -> ())
    (value_to_address_set v);
  match v with
      Structure s ->
	Hashtbl.iter (fun m entry -> add_backpointers store (region_for_member r m) entry.v) s
    | _ -> ()

(** Helper function for replace that removes non-array entries with Top values from the store *)
let prune_tops_from_store (store: t) (r:region_name) =
  let rec prune_struct (s:region) (parent_is_array:bool) =
    begin
      let empty = ref true in
	Hashtbl.iter
	  (fun m entry ->
	     match entry.v with
		 Top ->
		   if parent_is_array || (entry.is_array=false) then Hashtbl.remove s m
		   else empty := false
	       | Structure s' ->
		   let empty_child = prune_struct s' (parent_is_array || entry.is_array) in
		     if empty_child && (parent_is_array || entry.is_array=false)
		     then Hashtbl.remove s m
		     else empty := false
	       | _ -> empty := false)
	  s;
	!empty
    end
  in
  let (table, n) =
    match r with
	BaseRegion (Heap n) -> (store.heap_values, n)
      | BaseRegion (Variable n) -> (store.var_values, n)
      | VarStructMem mem_list -> (store.var_values, List.hd mem_list)
      | HeapStructMem mem_list -> (store.heap_values, List.hd mem_list)
  in let entry = Hashtbl.find table n
  in
    match entry.v with
	Top ->
	  if entry.is_array=false then Hashtbl.remove table n
      | Structure s ->
	  let struct_empty = prune_struct s entry.is_array
	  in if struct_empty then
	      begin
		if entry.is_array=false then Hashtbl.remove table n
	      end
      | _ -> ()
	

(** Helper function for strings - returns true if the first string starts with the second string *)
let string_starts_with s t =
  let s_len = String.length s
  and t_len = String.length t
  in if (s_len>=t_len) && ((String.sub s 0 t_len)=t) then true
    else false

(** We represent string constants as a single integer whose value corresponds to the first character
    in the string. This gives us the right answer for *c where c is initialized by a string constant.
    If we access as an array, we end up with Top *)
let get_val_for_string_const (str:string) :value =
  if String.length str = 0 then Constant (Ast.Constant.Int 0)
  else Constant (Ast.Constant.Int (int_of_char str.[0]))

(** Helper for replace and replace_value_only. For string constants, the only values we accept are the
    first character of the constant and Top. Returns the new value *)
let handle_string_const_real (store:t) (heap_name:string) (entry:entry) (new_value:value) :value =
  let prefix_len = String.length "Const_String:"
  and total_len = String.length heap_name
  in let char_val = get_val_for_string_const (String.sub heap_name prefix_len (total_len - prefix_len))
  in
    match (new_value, entry.is_array) with
      | (Top, is_array) ->
	  Hashtbl.replace store.heap_values heap_name {v=Top; is_array=is_array}; Top
      | (c, false) when c = char_val ->
	  Hashtbl.replace store.heap_values heap_name {v=c; is_array=false}; c
      | (c, true) when c = char_val ->
	  Hashtbl.replace store.heap_values heap_name {v=Top; is_array=true}; Top
      | _ ->
	  raise (Invalid_access ("Attempt to change value of string constant " ^ heap_name ^
				 " to " ^ (valueToString new_value)))

let handle_string_const = dbg_wrap4 handle_string_const_real
			    (fun s n e v -> ["handle_string_const"; n; (valueToString v)])
			    (fun res -> (valueToString res))

(** Helper for replace functions - returns (old_value, new_value) *)
let replace_helper store key v =
  let replace_base tbl n v =
    let old_entry =
      if Hashtbl.mem tbl n then Hashtbl.find tbl n else {v=Top; is_array=false}
    in let nv = if old_entry.is_array then combine_values old_entry.v v else v
    in
      Hashtbl.replace tbl n {v = nv; is_array = old_entry.is_array};
      (old_entry.v, nv)
  in
    match key with
	BaseRegion (Heap n) when (string_starts_with n "Const_String:") ->
	  let old_entry =
	    if Hashtbl.mem store.heap_values n
	    then Hashtbl.find store.heap_values n else {v = Top; is_array = false}
	  in
	    (old_entry.v, handle_string_const store n old_entry v)
      | BaseRegion (Heap n) -> replace_base store.heap_values n v
      | BaseRegion (Variable n) -> replace_base store.var_values n v
      | VarStructMem mem_list -> set_structure_value store.var_values mem_list v false
      | HeapStructMem mem_list -> set_structure_value store.heap_values mem_list v false

(** Replace or add the the specified mapping. If the address is an array, then we always merge in
  the old and new values. If the new value is Top, and the region is not an array, we remove
  the region from the store. *)
let replace_real (store:t) (key:region_name) (v:value) =
  let (old_value, new_value) = replace_helper store key v
  in
    if is_pointer old_value then remove_backpointers store key old_value else ();
    add_backpointers store key new_value;
    prune_tops_from_store store key

let replace = dbg_wrap3 replace_real
		(fun s k v -> ["replace"; (regionToString k); (valueToString v)])
		(fun res -> "unit")

let replace_value_only_real store key v =
  let (old_value, new_value) = replace_helper store key v
  in
    prune_tops_from_store store key

let replace_value_only = dbg_wrap3 replace_value_only_real
			   (fun s k v -> ["replace_value_only"; (regionToString k); (valueToString v)])
			   (fun res -> "unit")


let remove (store:t) (key:region_name) =
  let rec remove_heap_structure_mems_from_backpointers par_mem_list (s:region) =
    Hashtbl.iter
      (fun m entry ->
	 let mem_list = par_mem_list @ [m] in
	   Hashtbl.remove store.referencedBy (HeapStructMem mem_list);
	   match entry.v with
	       Structure s' -> remove_heap_structure_mems_from_backpointers mem_list s'
	     | _ -> ())
      s
  and remove_var_structure_mems_from_backpointers par_mem_list (s:region) =
    Hashtbl.iter
      (fun m entry ->
	 let mem_list = par_mem_list @ [m] in
	   Hashtbl.remove store.referencedBy (VarStructMem mem_list);
	   match entry.v with
	       Structure s' -> remove_var_structure_mems_from_backpointers mem_list s'
	     | _ -> ())
      s
  and do_remove tbl n rmv_fn =
    let old_e = (try Hashtbl.find tbl n with Not_found -> {v=Top;is_array=false})
    in
      Hashtbl.remove tbl n;
      (match old_e.v with Structure s -> rmv_fn [n] s | _ -> ());
      Hashtbl.remove store.referencedBy key;
      remove_backpointers store key old_e.v
  in
    match key with
	BaseRegion (Variable n) ->
	  do_remove store.var_values n remove_var_structure_mems_from_backpointers
      | BaseRegion (Heap n) ->
	  do_remove store.heap_values n remove_heap_structure_mems_from_backpointers
      | _ -> failwith "Should not attempt a remove of a structure member!"

let clear (store:t) =
  Hashtbl.clear store.var_values; Hashtbl.clear store.heap_values; Hashtbl.clear store.referencedBy

let deep_copy (store:t) =
  let pseudo_ptrs = Hashtbl.create 31
  in let var_values = deep_copy_structure store.var_values pseudo_ptrs 31
     and heap_values = deep_copy_structure store.heap_values pseudo_ptrs 31
  in
    { var_values = var_values; heap_values = heap_values;
      referencedBy = Hashtbl.copy store.referencedBy; options = store.options }

(** perform a fold over all basic regions *)
let fold_basic (fn:region_name -> value -> 'a -> 'a) (store:t) (initial_val:'a) =
  let initial_val' =
    Hashtbl.fold (fun n entry -> fn (BaseRegion (Variable n)) entry.v) store.var_values initial_val
  in
    Hashtbl.fold (fun n entry -> fn (BaseRegion (Heap n)) entry.v) store.heap_values initial_val'

(** iterate over all basic regions *)    
let iter_basic (fn:region_name -> value -> unit) (store:t) =
  Hashtbl.iter (fun n entry -> fn (BaseRegion (Variable n)) entry.v) store.var_values;
  Hashtbl.iter (fun n entry -> fn (BaseRegion (Heap n)) entry.v) store.heap_values

(** iterate over all regions, including those in structures. However, we do not include the structures
    themselves. *)    
let iter_all (fn:region_name -> value -> unit) (store:t) =
  let rec iter_struct r s fn =
    Hashtbl.iter
      (fun m s_entry ->
	 match s_entry.v with
	     Structure s' -> iter_struct (region_for_member r m) s' fn
	   | v -> fn (region_for_member r m) v)
      s
  in
    Hashtbl.iter
      (fun n entry ->
	 match entry.v with
	     Structure s -> iter_struct (BaseRegion (Variable n)) s fn
	   | _ -> fn (BaseRegion (Variable n)) entry.v)
      store.var_values;
    Hashtbl.iter
      (fun n entry ->
	 match entry.v with
	     Structure s -> iter_struct (BaseRegion (Heap n)) s fn
	   | _ -> fn (BaseRegion (Heap n)) entry.v)
      store.heap_values

let get_pointers_to (store:t) (r:region_name) =
  if Hashtbl.mem store.referencedBy r
  then Hashtbl.find store.referencedBy r
  else RegionSet.empty

      
(** {2 Printing-related functions} *)

(** Return a string representation of the store *)	
let storeToString store =
  let make_entry r v =
    (regionToString r) ^ (if is_array_region store r then "[*] = " else " = ") ^ (valueToString v)
  in
    "[" ^
    (fold_basic (fun r v str ->
	     if str="" then (make_entry r v)
	     else str ^ "; " ^ (make_entry r v))
       store "") ^
    "]"

let lvalueToString lv =
  match lv with
      RegionLval r -> "Lvalue (" ^ (regionToString r) ^ ")"
    | Dereference p -> "Lvalue (" ^ (pointerToString p) ^ ")"
    | PseudoPtrMem (p, mem_list) ->
	" (" ^ (pseudoPointerToString p) ^ ", " ^
	(List.fold_left (fun str m -> if str = "" then m else "." ^ m) "" mem_list) ^ ")"

let printdbg_lvalue fmt lv =
  match lv with
      RegionLval r ->
	Format.fprintf fmt "RegionLvalue(@ ";
	printdbg_region fmt r;
	Format.fprintf fmt "@ )"
    | Dereference p ->
	Format.fprintf fmt "DerefLvalue(@ ";
	printdbg_pointer fmt p;
	Format.fprintf fmt "@ )"
    | PseudoPtrMem (p, mem_list) ->
	Format.fprintf fmt "PseudoPtrMemLvalue(@%s,@ %s@ )" (pseudoPointerToString p)
	  (List.fold_left (fun str m -> if str = "" then m else "." ^ m) "" mem_list)

let print_backpointers fmt (store:t) =
  Format.fprintf fmt "@[[ ";
  Hashtbl.iter
    (fun r set ->
       Format.fprintf fmt "%s=>%s@  " (regionSetToString set) (regionToString r))
    store.referencedBy;
  Format.fprintf fmt "]@]"
      
let print fmt (store:t) =
  Format.fprintf fmt "@[[ ";
  Hashtbl.iter
    (fun n entry ->
	 Format.fprintf fmt "%s" (regionToString (BaseRegion (Variable n)));
	 if entry.is_array then Format.fprintf fmt "[*] = " else Format.fprintf fmt " = ";
	 Format.fprintf fmt "%s@  " (valueToString entry.v))
    store.var_values;
  Hashtbl.iter
    (fun n entry ->
	 Format.fprintf fmt "%s" (regionToString (BaseRegion (Heap n)));
	 if entry.is_array then Format.fprintf fmt "[*] = " else Format.fprintf fmt " = ";
	 Format.fprintf fmt "%s@  " (valueToString entry.v))
    store.heap_values;
  Format.fprintf fmt "]@]"

let backpointersToString (store:t) =
  "[" ^
  (Hashtbl.fold
     (fun r set str ->
	   if str="" then (regionSetToString set) ^ "=>" ^ (regionToString r)
	   else str ^ "; " ^ (regionSetToString set) ^ "=>" ^ (regionToString r))
     store.referencedBy "") ^
  "]"


(** {2 Helper functions for pointers} *)	    

(** Helper function to raise Invalid_access when attempting to dereference through
    a non-pointer *)
let raise_bad_deref expr v =
  raise (Invalid_access
	   ("Attempt to dereference through expression " ^ (Ast.Expression.toString expr)
	    ^ " which has value " ^ (valueToString v)))

(** Get the value at the specified address *)
let get_value_at_addr (store:t) (addr:address) :value =
  match addr with
      (* We use lookup instead of find - if a value isn't in the store, we return Top.
	 This prevents us from finding some accesses to unitialized variables, but it
	 lets us remove Top values from the store *)
      Region(r) ->
	lookup store r
    | Offset(r) ->
	begin
	  let v = lookup store r
	  in
	    if is_array_region store r then v
	    else
	      begin (* if we are accessing a non-array region through an offset, we convert to a may
		       pointer. *)
		let set = value_to_address_set v in
		  if set = AddressSet.empty then Top else Pointer (MayPointer set)
	      end
	    end
    | Null ->
	if warn_on_null_deref then
	  begin
	    Message.msg_string Message.Error "<SSL> WARNING: Possible dereference through null pointer!";
	    Top
	  end
	else raise (Null_ptr_deref "Null pointer dereference")

(** Convert an lvalue to a pointer *)
let lvalue_to_pointer (lval:lvalue) :pointer =
  match lval with
      RegionLval rname -> MustPointer(Region rname)
    | Dereference ptr -> ptr
    | PseudoPtrMem (p_ptr, mem_list) ->
	List.fold_left (fun ptr mem -> pointer_for_member ptr mem) (PseudoPointer p_ptr) mem_list

(** Dereference a pointer, returning a value. Here are the cases:
  + A MustPointer is a simple lookup.
  + May pointers  involve derefencing each pointer in the set. We drop
  non-pointer values and build a set of the dereferenced values that are pointers. If the set is
  empty, we return Top. Otherwise we return a MayPointer.
  + For pseudo pointers, we known the true value of the pointer, just not were it is stored. Thus,
  we can simply dereference the pseudo_address.
  + It is an error to dereference a null address or a PoisonPointer *)
let dereference_pointer (store:t) (ptr:pointer)  =
  begin
    let rec deref_set adr_set =
      begin
	let v_union = ref Top
	and value_initialized = ref false
	in
	  AddressSet.iter
	    (fun addr ->
	       let v = get_value_at_addr store addr in
		 (if !value_initialized
		  then v_union := union_values (fun r -> lookup store r) !v_union (fun r -> lookup store r) v
		  else v_union := v);
		 value_initialized := true)
	    adr_set;
	  !v_union
      end
    in
      match ptr with
	  MustPointer(a) -> get_value_at_addr store a
	| PseudoPointer(p_ptr) -> p_ptr.target_val
	| MayPointer(may_set) -> deref_set may_set
	| PoisonPointer -> raise (Invalid_access "Attempt to dereference a deallocated value")
  end


(** Invalidate any pseudo pointers which point to the region r *)
let invalidate_pseudo_pointers (store:t) (r:region_name) (exclude_pseudo_ptr_id:int) =
  RegionSet.iter
    (fun r' ->
       match find store r' with
	   Pointer (PseudoPointer p_ptr) ->
	     if p_ptr.id <> exclude_pseudo_ptr_id then
	       begin
		 replace_value_only store r' (Pointer (MayPointer p_ptr.may_set))
	       end
	 | _ -> ())
    (get_pointers_to store r)

(** Invalidate the values reference by a set of addresss, because they were used as an lval in an
    assignment, and we don't know exactly which address was assigned. If the new value is a pointer,
    then we need to convert each value to a May  pointer and add in the new pointer. If the new value
    isn't a pointer, we still need to make all pointers may pointers since we don't know if we are
    really still pointing to it.

    For each target, we also need to invalidate any pseudo-pointers which reference that target,
    unless the pointer is for the same location that is being assigned. The exclude_pseudo_ptr_id parameter
    should be the id of the assigned pseudo pointer or -1 if there isn't one. *)
let invalidate_address_set (store:t) (addr_set:AddressSet.t)
                           (new_value:value) (exclude_pseudo_ptr_id:int) =
  let new_set = value_to_address_set new_value
  in
    begin
      AddressSet.iter
	(fun addr ->
	   (* If we are invalidating through an offset address, we might be using
	      the region as an array. *)
	   (match addr with Offset(r) -> make_region_an_array store r | _ -> ());
	   match addr with
	       Region(r) ->
		 replace store r (combine_values (lookup store r) new_value);
		 invalidate_pseudo_pointers store r exclude_pseudo_ptr_id
	     | Offset(r) ->
		 replace store r new_value; (* replace handles arrays by combining the old and new values *)
		 invalidate_pseudo_pointers store r exclude_pseudo_ptr_id
	     | Null ->
		 if warn_on_null_deref then
		   Message.msg_string Message.Error "<SSL> WARNING: Possible assignment through null pointer"
		 else raise (Null_ptr_deref "Possible assignment through null pointer"))
	addr_set
    end

(** {2 The main entry points for symbolic store} *)

let _empty_hash_table = Hashtbl.create 0 (* used for Ast.constantfold calls *)

(** Evaluate the specified expression in the context of the current store. Returns a value *)
let rec eval_real (store: t) (expr: Ast.Expression.expression) =
  let rec raise_uninit_deref (e:Ast.Expression.expression) :value =
    begin (* Get the underlying pointer that this expression references and throw an exception *)
      let bad_ptr =
	(match canonicize_expr store e with
	     RegionLval r ->  MustPointer (Region r)
	   | Dereference ptr -> ptr
	   | PseudoPtrMem (p_ptr, mem_list) -> PseudoPointer p_ptr)
      in
	raise (Uninit_ptr_access
		 (bad_ptr, ("Access through uninitialized pointer " ^ (pointerToString bad_ptr) ^
			    " in expression " ^ (Ast.Expression.toString expr))))
    end
  and handle_null_deref expr =
    if warn_on_null_deref then
      begin
	Message.msg_string Message.Error
	  ("<SSL> WARNING: null pointer dereference when evaluating expression " ^
	   (Ast.Expression.toString expr));
	Top
      end
    else
      raise (Null_ptr_deref ("null pointer dereference when evaluating expression " ^
			     Ast.Expression.toString expr))
  in
    match expr with
	Ast.Expression.Lval(Ast.Expression.Symbol s) ->
	  lookup store (BaseRegion (Variable s))
      | Ast.Expression.Lval(Ast.Expression.Access(op, e, s)) ->
	  if op = Ast.Expression.Dot then
	    begin
	      match canonicize_expr store e with
		  RegionLval r -> lookup store (region_for_member r s)
		| Dereference ptr ->
		    dereference_pointer store (pointer_for_member ptr s)
		| PseudoPtrMem (p_ptr, mem_list) ->
		    lookup_structure_mem (cast_val_to_struct p_ptr.target_val "eval") [s]
	    end
	  else (* op = Arrow *)
	    begin
	      let v = eval_real store e in
		match v with
		  | Pointer (PseudoPointer p_ptr) ->
		      lookup_structure_mem (cast_val_to_struct p_ptr.target_val "eval2") [s]
		  | Pointer ptr ->
		      dereference_pointer store (pointer_for_member ptr s)
		  | Top -> raise_uninit_deref e
		  | Constant (Ast.Constant.Int 0) -> handle_null_deref expr
		  | _ ->
		      raise (Invalid_access
			       ("Attempt to use pointer member access operator -> on a non-pointer value: "
				^ (valueToString v)))
	    end
    | Ast.Expression.Lval(Ast.Expression.Dereference e) ->
	let v = eval_real store e in
	  begin
	    match v with
	      | Pointer(p) -> dereference_pointer store p
	      | Top -> raise_uninit_deref e
	      | Constant (Ast.Constant.Int 0) -> handle_null_deref expr
	      | Structure s ->
		  (* Temporary Hack!!!! XXX *)
		  Message.log_string_err Message.SymLat
		    "UNSOUND: Attempting to dereference a structure as a pointer!";
		  Message.log_string_err Message.SymLat (valueToString (Structure s));
		  Structure s
	      | Constant (Ast.Constant.Int i) ->
		  (* Temporary Hack!!!! XXX
		     This is for wierdness in ntdrivers/floppy.i like:
                     if(((KUSER_SHARED_DATA * const)0xffdf0000)->AlternativeArchitecture
		              == NEC98x86) *)
		  Message.log_string_err Message.SymLat
		    ("UNSOUND: Attempting to dereference an integer as a pointer: expr " ^
		     (Ast.Expression.toString e) ^ " has value " ^ (string_of_int i));
		  Top
	      | _ -> raise_bad_deref e v
	  end
    | Ast.Expression.Lval(Ast.Expression.Indexed(e, i)) ->
	(* In c, an array access e[i] can be viewed as *(e + i). Since we don't track individual indexes,
	   we convert e to a may pointer and then dereference it. We need a special case for where e is
	   a stack variable v instead of a pointer, since there will be no entry for v in the store. *)
	let v = eval_real store e in
	  begin
	    match v with
		Top ->
		  (match e with
		       (* Special case for array variables *)
		       Ast.Expression.Lval (Ast.Expression.Symbol s) ->
			 lookup store (BaseRegion (Variable s))
		     | _ -> raise_uninit_deref e)
	      | Pointer(p) -> dereference_pointer store (use_pointer_as_offset p)
	      | Structure s ->
		  (* Temporary Hack!!!! XXX *)
		  Message.log_string_err Message.SymLat
		    "UNSOUND: Attempting to dereference a structure as a pointer!";
		  Message.log_string_err Message.SymLat (valueToString (Structure s));
		  Structure s
	      | Constant (Ast.Constant.Int 0) -> handle_null_deref expr
	      | Constant (Ast.Constant.Int i) ->
		  (* Temporary Hack!!!! XXX *)
		  Message.log_string_err Message.SymLat
		    ("UNSOUND: Attempting to dereference an integer as a pointer: expr " ^
		     (Ast.Expression.toString expr) ^ " has value " ^ (string_of_int i));
		  Top
	      | _ -> raise_bad_deref expr v
	  end
    | Ast.Expression.Binary(op, e1, e2) ->
	let v1 = eval_real store e1 and v2 = eval_real store e2 in
	  begin
	    match (op, v1, v2) with
		(_, Constant c1, Constant c2) ->
		  let e' =
		    Ast.Expression.constantfold
		      (Ast.Expression.Binary (op, (Ast.Expression.Constant c1),(Ast.Expression.Constant c2)))
		      _empty_hash_table
		  in (match e' with Ast.Expression.Constant c -> Constant(c) | _ -> Top)
	      | (Ast.Expression.Eq, Pointer(p1), Pointer(p2)) ->
		  begin
		    match compare_pointers p1 p2 with
			(true, false) -> Constant (Ast.Constant.Int 1)
		      | (false, true) -> Constant (Ast.Constant.Int 0)
		      | _ -> Top
		  end
	      | (Ast.Expression.Ne, Pointer(p1), Pointer(p2)) ->
		  begin
		    match compare_pointers p1 p2 with
			(true, false) -> Constant (Ast.Constant.Int 0)
		      | (false, true) -> Constant (Ast.Constant.Int 1)
		      | _ -> Top
		  end
	      | (Ast.Expression.FieldOffset, _, _) ->
		  begin
		    match (v1, e2) with
			(Pointer (MustPointer (Region r)), 
			 Ast.Expression.Lval (Ast.Expression.Symbol member)) ->
			  Pointer (MustPointer (Region (region_for_member r member)))
		      | (Top, Ast.Expression.Lval (Ast.Expression.Symbol member)) ->
			  (* XXX We are hitting this probably because we aren't creating
			     dummy locations for uninitialized pointers *)
			  Message.log_string_err Message.SymLat
			    ("eval: attempting to take field offset " ^ member ^ " of expression " ^
			     (Ast.Expression.toString e1) ^ " whose value is Top");
			  Top
		      | _ ->
			  (* XXX If we get a strange expression, just warn. Have see stuff in linux code
			     where they have expressions like:
			       (size_t) &((typeof(*scmd) *)0)->list
			  *)
			  Message.log_string_err Message.SymLat
			    ("eval: don't know how to take field offset of " ^
				       (valueToString v1) ^ " (expr=" ^ (Ast.Expression.toString e1)^
				       "), " ^ (Ast.Expression.toString e2));
			  Top
		  end
	      | (Ast.Expression.Minus, Pointer(p1), Pointer(p2)) ->
		  Top (* The difference of two pointers is an integer *)
	      | (_, Pointer(p1), Pointer(p2)) ->
		  Message.log_string_err Message.SymLat
		    ("eval: Unable to evaluate expression: " ^ (Ast.Expression.toString expr));
		  Top
	      | (_, Pointer(p), _) | (_, _, Pointer(p)) -> Pointer(use_pointer_as_offset p)
	      | _ -> Top
	  end
    | Ast.Expression.Cast(ast_type, e) -> eval_real store e
    | Ast.Expression.Constant(Ast.Constant.String s) ->
	if store.options.merge_string_constants then
	  begin (* merge all string constants to a single heap location *)
	    let heap_region = get_heap_region ("Const_String:merged") in
	      if (mem store heap_region) = false then make_region_an_array store heap_region;
	      Pointer (MustPointer (Region heap_region))
	  end
	else
	  begin (* special case for constant strings - we create a special heap location for
		   the first character, which is treated as an int. We return a pointer to
		   this heap location. *)
	    let heap_region = get_heap_region ("Const_String:" ^ s) in
	      replace_value_only store heap_region (get_val_for_string_const s);
	      Pointer (MustPointer (Region heap_region))
	  end
    | Ast.Expression.Constant(c) -> Constant(c)
    | Ast.Expression.Sizeof(i) -> Constant(Ast.Constant.Int i)
    | Ast.Expression.Unary(Ast.Expression.Address, e) ->
	begin
	  match e with
	      Ast.Expression.Lval(lv) ->
		begin
		  let lval = canonicize_lval_real store lv in
		    match lval with
			RegionLval(r) -> (Pointer (MustPointer (Region r)))
		      | Dereference(ptr) -> (Pointer ptr)
		      | PseudoPtrMem(p_ptr, mem_list) ->
			  (* ??? For now, we lose the fact that we had a pseudo pointer. Eventually, we
			     may want to preserve that. In that case, we would need a new pseudo pointer
			     which refers into part of the value of another pseudo pointer. *)
			  Pointer
			    (List.fold_left (fun p m -> pointer_for_member p m)
			       (PseudoPointer p_ptr) mem_list)
		end
	    | _ -> failwith ("Unable to take address of expression " ^ (Ast.Expression.toString e))
	end
    | Ast.Expression.Unary(op, e) ->
	let v = eval_real store e in
	  begin
	    match v with
		Constant(c) ->
		  eval_real store
		  (Ast.Expression.constantfold
		     (Ast.Expression.Unary (op, (Ast.Expression.Constant c)))
		     _empty_hash_table)
	      | Pointer(p) ->
		  failwith ("Unary operation on pointer not supported: " ^ (Ast.Expression.toString expr))
	      | _ -> Top
	  end
    | _ -> failwith ("Unable to parse expression " ^ (Ast.Expression.toString expr))
	
(** convert an lval to a canonical form to be used in an assigment - returns an lvalue *)
and canonicize_lval_real (store:t) (lv:Ast.Expression.lval) :lvalue =
  let rec raise_uninit_deref (e:Ast.Expression.expression) :lvalue =
    begin (* Get the underlying pointer that this expression references and throw an exception *)
      let bad_ptr =
	(match canonicize_expr store e with
	     RegionLval r ->  MustPointer (Region r)
	   | Dereference ptr -> ptr
	   | PseudoPtrMem (p_ptr, mem_list) -> PseudoPointer p_ptr)
      in
	raise (Uninit_ptr_access
		 (bad_ptr, ("Access through uninitialized pointer " ^ (pointerToString bad_ptr) ^
			    " in lval " ^ (Ast.Expression.lvalToString lv))))
    end
  and handle_null_deref expr =
    if warn_on_null_deref then
      begin
	Message.msg_string Message.Error
	  ("<SSL> WARNING: null pointer dereference when evaluating expression " ^
	   (Ast.Expression.toString expr));
	(* We get a value for this pointer by raising an unitialized derefence for the
	   lvalue containing the null pointer. Hacky but should work??? *)
	raise_uninit_deref expr
      end
    else
      raise (Null_ptr_deref ("null pointer dereference when evaluating expression " ^
			     Ast.Expression.toString expr))
  in
    match lv with
	Ast.Expression.Symbol(s) -> (RegionLval (BaseRegion (Variable s)))
      | Ast.Expression.Dereference(e) ->
	  begin
	    let v = eval_real store e in
	      match v with
		  Top ->
		    raise_uninit_deref e
		| Constant (Ast.Constant.Int 0) ->
		    handle_null_deref e
	      | Constant (Ast.Constant.Int i) ->
		  (* Temporary Hack!!!! XXX *)
		  Message.log_string_err Message.SymLat
		    ("UNSOUND: Canonicize attempting to dereference an integer as a pointer expr " ^
		     (Ast.Expression.toString e) ^ " has value " ^ (string_of_int i));
		  (Dereference (MustPointer (Null)))
		| Constant(_) ->
		    raise_bad_deref e v; 
		| Structure s ->
		    (* Temporary Hack!!!! XXX *)
		    Message.log_string_err Message.SymLat
		      "UNSOUND: Attempting to dereference a structure as a pointer!";
		    Message.log_string_err Message.SymLat (valueToString (Structure s));
		    (Dereference (MustPointer (Null))) (* Extra big hack! *)
		| Pointer(ptr) -> (Dereference ptr)
	  end
      | Ast.Expression.Indexed(expr, idx_expr) ->
	  if store.options.expr_is_array expr then
	    begin
	      match canonicize_expr store expr with
		  RegionLval r -> Dereference (MustPointer (Offset r))
		| Dereference ptr -> (Dereference (use_pointer_as_offset ptr))
		| PseudoPtrMem (p_ptr, mem_list) -> failwith "XXX neeed to implement"
	    end
	  else
	    begin
	      let v = eval_real store expr in
		match v with
		    Top -> raise_uninit_deref expr
		  | Constant (Ast.Constant.Int 0) ->
		      handle_null_deref expr
		  | Structure s ->
		      (* Temporary Hack!!!! XXX *)
		      Message.log_string_err Message.SymLat
			"UNSOUND: Attempting to dereference a structure as a pointer!";
		      Message.log_string_err Message.SymLat (valueToString (Structure s));
		      (Dereference (MustPointer Null)) (* Extra big hack *)
		  | Constant (Ast.Constant.Int i) ->
		      (* Temporary Hack!!!! XXX *)
		      Message.log_string_err Message.SymLat
			("UNSOUND: Canonicize ttempting to dereference an integer as a pointer: expr " ^
			 (Ast.Expression.toString expr) ^ " has value " ^ (string_of_int i));
		      (Dereference (MustPointer (Null)))
		  | Constant(_) -> raise_bad_deref expr v
		  | Pointer(ptr) -> (Dereference (use_pointer_as_offset ptr))
	    end
      | Ast.Expression.Access(op, expr, member) ->
	  if op = Ast.Expression.Dot then
	    begin
	      match (canonicize_expr store expr) with
		  RegionLval r -> RegionLval (region_for_member r member)
		| Dereference (PseudoPointer ptr) ->
		    PseudoPtrMem (ptr, [member])
		| Dereference ptr -> Dereference (pointer_for_member ptr member)
		| PseudoPtrMem (p_ptr, mem_list) -> PseudoPtrMem(p_ptr, mem_list @ [member])
	    end
	  else (* op = Arrow *)
	    begin
	      match (eval_real store expr) with
		| Pointer (PseudoPointer p_ptr) -> PseudoPtrMem(p_ptr, [member])
		| Pointer ptr -> Dereference (pointer_for_member ptr member)
		| Top ->
		    raise_uninit_deref expr
		| Constant (Ast.Constant.Int 0) ->
		    handle_null_deref expr
		| v ->
		    raise (Invalid_access
			     ("attempt to perform pointer member access with non-pointer value: " ^
			      (valueToString v)))
	    end
      | Ast.Expression.This -> failwith "canonicize of this not implemented"

(** Canonicize the specified expression. This is called when evaluating or canonicizing the Dot operator. *)
and canonicize_expr store expr =
  match expr with
      Ast.Expression.Lval lv -> canonicize_lval_real store lv
    | Ast.Expression.FunctionCall _ ->
	(* Cil should remove nested function calls *)
	failwith "Not expecting a function call in a structure dot expression!"
    | _ -> raise (Invalid_access ("Unable to use expression " ^ (Ast.Expression.toString expr) ^
				  " with Dot operator"))

let eval = dbg_wrap2 eval_real (fun s e -> ["eval"; (Ast.Expression.toString e)])
	     (fun res -> (valueToString res))

let canonicize_lval =
  dbg_wrap2 canonicize_lval_real (fun store lv -> ["canonicize_lval"; (Ast.Expression.lvalToString lv)])
    (fun res -> (lvalueToString res))

(** Caller got an invalid access error when derefencing through this pointer. Allocate a new heap
    region for each target. *)
let allocate_for_pointer (store:t) (p:pointer) (prog_loc:string) :unit =
  let rec create_struct_region (base:region_name) (mem_list:string list) (prog_loc:string) :pointer =
    Message.log_string_dbg Message.SymLat ("create_struct_region " ^
					   (List.fold_left (fun str mem -> str ^ "." ^ mem)
					      (regionToString base) mem_list) ^ " " ^ prog_loc);
    if (mem store base) = false then allocate_for_region base prog_loc;
    let target_base_region = get_heap_region prog_loc in
    let (base_struct:region) =
      (match lookup store target_base_region with
	   Structure s -> s
	 | Top -> 
	     let s = Hashtbl.create 5 in replace_value_only store target_base_region (Structure s); s
	 | _ ->
	     failwith
	       ("SYMLAT: allocate_for_pointer: unexpected value for structure target at region " ^
		(regionToString target_base_region)))
    in
      ignore (set_structure_value base_struct mem_list Top false);
      let ptr =
	List.fold_left (fun ptr m -> pointer_for_member ptr m)
	  (MustPointer (Region target_base_region)) mem_list
      in
	Message.log_string_dbg Message.SymLat ("create_struct_region returning:");
	Message.log_print_dbg Message.SymLat printdbg_pointer ptr;
	ptr
  and allocate_for_region (r:region_name) (prog_loc:string) :unit =
    match r with
	VarStructMem (base::members) ->
	  (* For structures, we create an on-demand region for the base region, if it does not
	     exist. *)
	  let ptr = create_struct_region (BaseRegion (Variable base)) members prog_loc in
	    replace store r (Pointer ptr)
      | HeapStructMem (base::members) ->
	  (* For structures, we create an on-demand region for the base region, if it does not
	     exist. *)
	  let ptr = create_struct_region (BaseRegion (Heap base)) members prog_loc in
	    replace store r (Pointer ptr)
      | _ ->
	  let heap_region = get_heap_region prog_loc
	  in
	    if mem store heap_region then make_region_an_array store heap_region
	    else replace_value_only store heap_region Top;
	    replace store r (Pointer (MustPointer (Region heap_region)));
	    Message.log_string_dbg Message.SymLat
	      ("allocate_for_region: allocating region " ^ (regionToString r) ^ " => " ^ prog_loc)
  and allocate_for_address (a:address) :unit =
    match a with
	Region r | Offset r -> allocate_for_region r (prog_loc ^ "_" ^ (addressToString a))
      | Null -> raise (Null_ptr_deref ("Attempt to assign through null pointer at " ^ prog_loc))
  in
    match p with
	MustPointer a -> allocate_for_address a
      | MayPointer set ->
	  AddressSet.iter
	    (fun a ->
	       match get_value_at_addr store a with
		   Top -> allocate_for_address a
		 | _ -> ())
	    set
      | PseudoPointer _ ->
	  failwith "should not have gotten deref error for pseudo ptr"
      | PoisonPointer ->
	  raise (Invalid_access "Deference of a poison pointer")

(** We know the target value of a may pointer either through an assignment or through a predicate. If
    the original lvalue was *V (where V is any variable), and the region associated with V is not
    an array, we can convert the value of V's region from a MayPointer to a PseudoPointer. *)
let convert_may_to_pseudo store original_lv may_set v =
  begin
    match original_lv with
	Ast.Expression.Dereference (Ast.Expression.Lval (Ast.Expression.Symbol s)) ->
	  let r = BaseRegion (Variable s) in
	    if not (is_array_region store r) then
	    replace_value_only store r (Pointer (make_new_pseudo_pointer v may_set))
      | _ -> ()
  end
       
(** Assign a value to the specified lvalue *)
let assign_value_real (store: t) (target:lvalue) (original_lv:Ast.Expression.lval) (v:value) =
    match target with
	RegionLval r ->
	  invalidate_pseudo_pointers store r (-1);
	  replace store r v
	| Dereference (MustPointer a) ->
	    begin
	      match a with
		  Region r ->
		    invalidate_pseudo_pointers store r (-1);
		    replace store r v
		| Offset r ->
		    invalidate_pseudo_pointers store r (-1);
		    make_region_an_array store r;
		    replace store r v
		| Null ->
		    if warn_on_null_deref then
		      Message.msg_string Message.Error
			("<SSL> WARNING: Attempt to assign through a null pointer" ^
			 (Ast.Expression.lvalToString original_lv))
		    else
		      raise (Null_ptr_deref ("Attempt to assign through a null pointer: " ^
					     (Ast.Expression.lvalToString original_lv)))
	    end
	| Dereference (PseudoPointer p_ptr) ->
	    invalidate_address_set store p_ptr.may_set v p_ptr.id;
	    p_ptr.target_val <- v
	| Dereference (MayPointer set) ->
	    invalidate_address_set store set v (-1);
	    if v <> Top then convert_may_to_pseudo store original_lv set v
	| Dereference (PoisonPointer) ->
	    raise (Invalid_access "Attempt to assign through a deallocated pointer")
	| PseudoPtrMem (p_ptr, mem_list) ->
	    (* ??? Do we really know parent isn't array? Is this the right way to do this? *)
	    let _ = set_structure_value (cast_val_to_struct p_ptr.target_val "assign_value")  mem_list v false
	    in ()

let assign_value =
  dbg_wrap4 assign_value_real
    (fun store target original_lv v ->
       ["assign_value"; (lvalueToString target); (Ast.Expression.lvalToString original_lv); (valueToString v)])
    (fun res -> "unit")
    

(** modify the store based on the assignment of expr to lv *)
let assign_real (store: t) (lv:Ast.Expression.lval) (expr:Ast.Expression.expression) =
  let target = canonicize_lval store lv
  and v = (* compute the value we will actuall assign *)
    (match eval store expr with
       | Structure s -> Structure (copy_structure s) (* We deep copy structures upon assignment *)
       | v -> v)
  in
    assign_value store target lv v

let assign =
  dbg_wrap3 assign_real
    (fun store lv expr ->
       ["assign"; (Ast.Expression.lvalToString lv); (Ast.Expression.toString expr)])
    (fun res -> "unit")
	
(** Havoc the associated lvalue. For now, that means setting it to Top. *)
let havoc (store:t) (lv:Ast.Expression.lval) =
  let target = canonicize_lval store lv
  in let old_val = eval store (Ast.Expression.Lval lv) in
    (match old_val with
	 Pointer p ->
	   Message.msg_string Message.Error
	   ("<SSL> Havoc of lval " ^ (Ast.Expression.lvalToString lv) ^ " is losing a pointer. Not sound!")
       | _ -> ());
    assign_value store target lv Top

(** Remove the local variables for a specific function, eg. because they are going out of scope.
    Any pointers containing references to these removed variables become PoisonPointers. *)
let remove_local_vars (store:t) (is_local_to_fn: string -> bool) =
  let rec is_region_local (r:region_name) =
    match r with
	BaseRegion (Variable vname) -> is_local_to_fn vname
      | VarStructMem mem_list -> is_local_to_fn (List.hd mem_list)
      | _ -> false
  and process_may_set set =
    begin (* helper function to check a set of addresses for references to dellocated variables. If a
	     reference is found, a Poison_access exception is thrown. *)
      AddressSet.iter
	(fun a ->
	   match a with
	       Region r | Offset r -> 
		 if is_region_local r then raise Poison_access
	     | _ -> ())
	set
    end
  and set_poison_pointers r v = 
    match v with
	Pointer (MustPointer (Region r)) ->
	  if is_region_local r then replace store r (Pointer PoisonPointer)
      | Pointer (MayPointer set) ->
	  (try
	     process_may_set set
	   with Poison_access -> replace store r (Pointer PoisonPointer))
      | Pointer (PseudoPointer p_ptr) ->
	  (try
	     process_may_set p_ptr.may_set
	   with Poison_access -> replace store r (Pointer PoisonPointer))
      | Structure s ->
	  Hashtbl.iter
	    (fun m entry -> set_poison_pointers (region_for_member r m) entry.v)
	    s
      | _ -> ()
  and check_region (r:region_name) =
    begin (* Helper function to check a region. It first sees whether the region is a local variable.
	     If so, the region is removed from the store. If the region wasn't removed from the store,
	     we look at its value. If the value is a pointer containing a reference to a local variable,
	     we set the value to PoisonPointer. *)
      let is_local =
	(match r with
	     BaseRegion (Variable(vname)) ->
	       if is_local_to_fn vname then (remove store r; true) else false
	   | _ -> false)
      in
	if is_local=false then set_poison_pointers r (find store r)
    end
  in
  let regions = fold_basic (fun r v lst -> r :: lst) store [] in
    List.iter check_region regions

(** Move all references from the region named r to r' *)
let rec move_region_pointers (store:t) (r:region_name) (r':region_name) :unit =
  let rec replace_in_addr_set set r r' :AddressSet.t =
    let rgn_addr = Region r
    and offset_addr = Offset r
    in let set' =
      if AddressSet.mem rgn_addr set then AddressSet.add (Region r') (AddressSet.remove rgn_addr set) else set
    in
      if AddressSet.mem offset_addr set'
      then AddressSet.add (Offset r') (AddressSet.remove offset_addr set') else set'
  and replace_in_address a r r' :address =
    match a with
	Region r -> Region r'
      | Offset r -> Offset r'
      | addr -> addr
  and replace_in_pointer ptr r r' :pointer =
    match ptr with
	MustPointer a -> MustPointer (replace_in_address a r r')
      | PseudoPointer p_ptr ->
	  p_ptr.may_set <- replace_in_addr_set p_ptr.may_set r r';
	  PseudoPointer p_ptr
      | MayPointer set -> MayPointer (replace_in_addr_set set r r')
      | p -> p
  and ref_set = get_pointers_to store r
  and ref_set_r' = get_pointers_to store r'
  and r_value = lookup store r
  in
    RegionSet.iter
      (fun ptr_rgn ->
	 match lookup store ptr_rgn with
	     Pointer p -> replace_value_only store ptr_rgn (Pointer (replace_in_pointer p r r'))
	   | _ -> failwith "inconsistent referencedBy table!")
      ref_set;
    Hashtbl.replace store.referencedBy r' (RegionSet.union ref_set_r' ref_set);
    Hashtbl.remove store.referencedBy r; (* Nothing points to r anymore *)
    match r_value with
	Structure s ->
	  Hashtbl.iter
	  (fun n entry -> move_region_pointers store (region_for_member r n) (region_for_member r' n))
	  s;
      | _ -> ()
	       
    
(** Allocate a new heap region and assign to lval a pointer to this region. 
    If the region already exists, we make it an array. Otherwise, we create a
    new heap region. *)
let allocate_heap_region_real (store:t) (lv:Ast.Expression.lval) (program_location:string) =
  let heap_region = get_heap_region program_location
  and target = canonicize_lval store lv
  in
    if (mem store heap_region) then
      begin
	let summary_region = get_summary_heap_region program_location
	and old_value = lookup store heap_region
	in
	  make_region_an_array store summary_region;
	  move_region_pointers store heap_region summary_region;
	  if old_value<>Top
	  then (replace store heap_region Top; replace store summary_region old_value)
      end;
    (* XXX 
    if mem store heap_region then make_region_an_array store heap_region
    else replace_value_only store heap_region Top;*)
    assign_value store target lv (Pointer (MustPointer (Region heap_region)))

let allocate_heap_region = dbg_wrap3 allocate_heap_region_real
			     (fun s lv p -> ["allocate_heap_region"; (Ast.Expression.lvalToString lv); p])
			     (fun res -> "unit")

(** Remove the specified region from the store and make any references to this region
    Poison pointers *)
let deallocate_region (store:t) (r:region_name) =
  remove store r;
  iter_basic
    (fun r v ->
       match v with
	   Pointer p -> if points_to p r then replace store r (Pointer PoisonPointer)
	 | _ -> ())
    store
	     
(** Take the intersection of two stores. For now, we either return nothing (unit) or raise an error *)
let intersection (s1:t) (s2:t) =
    iter_basic
      (fun k v ->
	 let v' = lookup s2 k
	 and is_array = is_array_region s1 k
	 and is_array' = is_array_region s2 k
	 in
	   if (is_array = false) && (is_array' = false)
	   then intersect_values v v')
      s1
	  
let union (s1:t) (s2:t) =
  let result_store = create_new_store s1.options
  in
    iter_basic
      (fun k v ->
	 let v' = lookup s2 k in
	 let u_val = union_values (fun r -> lookup s1 r) v (fun r -> lookup s2 r) v'
	 in
	   replace result_store k u_val;
           if (is_array_region s1 k) || (is_array_region s2 k) then make_region_an_array result_store k)
      s1;
    iter_basic
      (fun k v ->
	 if not (mem s1 k) then
	     let u_val = union_values (fun r -> lookup s2 r) v (fun r -> lookup s1 r) Top
	     in
	       replace result_store k u_val;
	       if is_array_region s2 k then make_region_an_array result_store k)
      s2;
    result_store


(** Check if the region represented by store s1 is contained within the region represented by store s2.
  When comparing two maps, if an element is in s1, but not s2, then it is OK, since
  Constant <= Top. However, if an element is in s2, but not s1, then s1 is not contained
  within s2 since Top > Constant. Pointers are more complex -- see the comments for pointer_leq. *)
let leq (s1:t) (s2:t) =
  let handle_mismatch r v1 v2 =
    if Message.is_dbg_logging_enabled Message.SymLat then
      begin
	let is_s1_array = if is_array_region s1 r then " [array]" else ""
	and is_s2_array = if is_array_region s2 r then " [array]" else ""
	in
	  Message.log_string_dbg Message.SymLat
	    ("Leq returning false: region " ^ (regionToString r) ^ " has " ^
	     (valueToString v1) ^ is_s1_array ^ " > " ^ (valueToString v2) ^ is_s2_array)
      end;
    raise Not_found (* abort the test *)
  in
    try
      iter_basic
	(fun k v_s2 ->
	   let v_s1 = lookup s1 k in
	     try
	       if (is_array_region s1 k) && (not (is_array_region s2 k))
	       then (Message.log_string_norm Message.SymLat ("leq failed due to array check: " ^
					(regionToString k)); raise Not_found);
	       leq_values v_s1 v_s2
	     with Not_found -> handle_mismatch k v_s1 v_s2)
	s2;
      iter_basic
	(fun k v_s1 ->
	   if (mem s2 k) = false then
	     try
	       (* if s1 is an array and there is no entry for s2, then not leq *)
	       if is_array_region s1 k then
		 (Message.log_string_norm Message.SymLat
		   ("leq failed due to array check: region " ^ (regionToString k) ^
		    " (not present in right store)"); raise Not_found);
	       (* if s1 is a heap region, and there is no entry for s2, then not leq *)
	       leq_values v_s1 Top
	     with Not_found -> () (* handle_mismatch k v_s1 Top *) )
	s1;
      true
    with
	Not_found -> false

(** We know from a predicate that the lval lv is equivalent to the expression e. Use this information
    to improve our knowledge of the store's state. If we find a contradiction, throw Not_satisfiable *)
let add_predicate (store:t) (lv:Ast.Expression.lval) (e:Ast.Expression.expression) =
  let add_predicate_to_region store r e =
    begin
      let old_val = lookup store r
      and equiv_val = eval store e
      in
	match (old_val, equiv_val) with
	    (Constant c1, Constant c2) ->
	      if c1 <> c2 then raise Not_satisfiable
	  | (Top, Constant c) -> replace store r (Constant c)
	  | (Pointer (MayPointer s), Pointer (MustPointer a)) ->
	      if not (AddressSet.mem a s) then raise Not_satisfiable
	      else replace store r equiv_val
	  | (Pointer (PseudoPointer p_ptr), Pointer (MustPointer a)) ->
	      if not (AddressSet.mem a p_ptr.may_set) then raise Not_satisfiable
	      (* XXX can eventually tighten the pseudo pointer by looking up the target value and
	         comparing to target of this address. If the address points to a region, we can
	         convert to a must pointer and set the value of this region to the target value,
	         if the target value is better. *)
	  | _ -> ()
    end
  in
  let lval = canonicize_lval store lv 
  in
    match lval with
	RegionLval r -> add_predicate_to_region store r e
      | Dereference (MustPointer (Region r)) -> add_predicate_to_region store r e
      | Dereference (PseudoPointer p_ptr) ->
	  begin
	    let equiv_val = eval store e
	    in
	      match (p_ptr.target_val, equiv_val) with
		  (Constant c1, Constant c2) ->
		    if c1 <> c2 then raise Not_satisfiable
		| _ -> ()
	  end
      | Dereference (MayPointer set) ->
	  (* If we are comparing to a may pointer, see if we can convert it to a pseudo pointer *)
	  begin
	    let equiv_val = eval store e
	    and old_val = eval store (Ast.Expression.Lval lv)
	    in
	      match (old_val, equiv_val) with
		  (Constant c1, Constant c2) ->
		    (* Special case for pseudo values *)
		    if c1 <> c2 then raise Not_satisfiable
		| (_, Constant _) ->
		    convert_may_to_pseudo store lv set equiv_val
		| _ -> ()
	  end
      | PseudoPtrMem (p_ptr, mem_list) ->
	  begin
	    let equiv_val = eval store e
	    and lv_struct = cast_val_to_struct p_ptr.target_val "add_predicate"
	    in let member_val = lookup_structure_mem lv_struct mem_list
	    in
	      match (member_val, equiv_val) with
		  (Constant c1, Constant c2) ->
		    if c1 <> c2 then raise Not_satisfiable
		| (Top, Constant c) ->
		    (* ??? Do we really know parent isn't array? Is this the right way to do this? *)
		    let _ = set_structure_value lv_struct mem_list equiv_val false in ()
		| _ -> ()
	  end
      | _ -> ()

(*let add_predicate =
  dbg_wrap3 add_predicate_real
    (fun store lv e ->
       "add_predicate " ^ (Ast.Expression.lvalToString lv) ^ " " ^ (Ast.Expression.toString e))*)

(** Iterate through all non-structure regions in store. It is safe to delete the current region
    in the course of the iteration. *)       
let iterate_nonstruct_regions (store:t) (f:region_name->value->bool->unit) :unit =
  let rec iter_struct (s:region) (f:region_name->value->bool->unit) (parent_region:region_name) =
    begin
      let assoc_list = struct_to_assoc_list s in
	List.iter
	  (fun i ->
	     let (m, entry) = i in
	       match entry.v with
		   Structure s' -> iter_struct s' f (region_for_member parent_region m)
		 | v -> f (region_for_member parent_region m) v entry.is_array)
	  assoc_list
    end
  in
  let store_assoc_list = store_to_assoc_list store
  in
    List.iter
      (fun i ->
	 let (r, entry) = i in
	   match entry.v with
	       Structure s -> iter_struct s f r
	     | v -> f r v entry.is_array)
      store_assoc_list

(** Invalidate all constants. This can be used as a loopback drop heuristic in the lattice *)
let invalidate_consts (store:t) =
  iterate_nonstruct_regions store
    (fun r v is_array ->
       match (v, is_array) with
	   (Constant _, false) ->
	     replace store r Top
	 | _ -> ())

(** A heuristic tells us we shouldn't track this lvalue anymore. For constants, we can just
    set to top. For pointers, we convert to MayPointers. If the canonicized lvalue is
    too complex (e.g. a may pointer), we just ignore this since it is a heuristic anyway. *)
let invalidate_lval (store:t) (lv:Ast.Expression.lval) =
  let target = canonicize_lval store lv in
    match target with
	RegionLval r | Dereference (MustPointer (Region r)) ->
	  replace store r (combine_values Top (lookup store r));
	  Message.msg_string Message.Normal
	    ("<SSL> invalidating lvalue " ^ (Ast.Expression.lvalToString lv)  ^
	     " which maps to region " ^ (regionToString r))
      | _ ->
	  Message.msg_string Message.Normal
	    ("<SSL> skipping invalidation of lvalue " ^ (Ast.Expression.lvalToString lv) ^
	     " - this maps to " ^ (lvalueToString target))
    
(** Used by unit tests to get reproducable names. Should not be used anywhere else! *)
let _reset_counters () =
  _reset_pseudo_ptr_id ()
