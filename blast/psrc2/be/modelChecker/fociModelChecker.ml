(* Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(**
 * This module implements the foci model checking algorithm.
 *)

open Ast
open BlastArch

module Stats = Bstats
module M = Message    
module O = Options
module T = BiDirectionalLabeledTree

module Make_Foci_Model_Checker : BlastArch.MAKE_LMC =
  functor(A : ABSTRACTION) ->
struct
  module R = A.Region
  module Op = A.Operation

  type error_path = (R.t * (Op.t list) * R.t)

type marking_data = {
    mutable time_stamp : int ;  
    mutable region     : R.t ;
}
      
let print_marking_data fmt md =
  Format.fprintf fmt "@[time_stamp=%d;@ region=" md.time_stamp ;
  R.print fmt md.region ;
  Format.fprintf fmt "@]"
	  
let print_marking fmt = function
  md ->
      Format.fprintf fmt "@[(" ;
        print_marking_data fmt md ;
      Format.fprintf fmt ")@]" 

type node_data = 
  { id           : int ;
    mutable mark : marking_data ; }

let print_node_data fmt nd =
  Format.fprintf fmt "@[Data(@[id=%d;@ mark=" nd.id ;
  print_marking fmt nd.mark ;
  Format.fprintf fmt "@])@]"

type tree_node = (node_data, Op.t) T.node 
    
(* helper functions to access a tree node's data *)
let get_node_data (n : tree_node) = T.get_node_label n
    
let get_op n =
  match (T.get_parent_edge n) with
    Some(e) ->
      (T.get_edge_label e)
  | None ->
          invalid_arg "get_op: input node has no parent edge"
	
let get_parent n =
  match (T.get_parent_edge n) with
    Some(e) ->
      (T.get_source e)
  | None ->
      invalid_arg "get_parent: input node has no parent edge"
	
let get_children n = T.get_children n
    
let get_id n = (get_node_data n).id
    
let get_marking n = (get_node_data n).mark
    
let get_marking_data n = get_marking n
	  
let get_time_stamp n =
  try (get_marking_data n).time_stamp
  with Invalid_argument "get_marking: input node is unprocessed" ->
    invalid_arg "get_time_stamp: input node is unprocessed"
      
let print_tree_node fmt n =
  Format.fprintf fmt "@[TNode(@[label=" ;
  print_node_data fmt (get_node_data n) ;
  begin
    match (T.get_parent_edge n) with
      None -> Format.fprintf fmt ";@ no parent"
    | Some(e) ->
        Format.fprintf fmt ";@ incoming_op=" ;
        Op.print fmt ((T.get_edge_label e)) ;
        Format.fprintf fmt ";@ parent_id=%d"
          (get_id (T.get_source e))
  end ;
  let children_ids = List.map get_id (T.get_children n) in
  Format.fprintf fmt ";@ children_ids=" ;
  Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
  Format.fprintf fmt "@])@]"
    
    
let get_region n =
  try (get_marking_data n).region
  with Invalid_argument "get_marking: input node is unprocessed" ->
    invalid_arg "get_region: input node is unprocessed"
      

  (* helper class to create nodes with appropriate ids *)
class tree_node_creator =
  object
  val mutable next_id = 0
			  
  val tree_node_table = Hashtbl.create 1009
			  (* hash int -> tree node for quick access to a tree node *)
			  (* q: why not an array instead ? -- as nodes get deleted ... This can be changed later *)
			  
  method create_root m =
    let this_id = next_id in
    next_id <- next_id + 1 ;
    let rv = ((T.create_root { id  = this_id ;
				  mark = m ;
				}) : tree_node) 
    in
    Hashtbl.add tree_node_table this_id (rv);
    rv
      (* FOCI *)
  method create_child m op par =
    let this_id = next_id in
    next_id <- next_id + 1 ;
    let rv = 
      ((T.create_child { id  = this_id ;mark = m ;} op par) : tree_node)
    in
      Hashtbl.add tree_node_table this_id (rv); 
      rv
	
  method delete_children par = 
    let children = T.get_children (par : tree_node) in
    let child_ids = List.map get_id children in
    let _ = List.iter (Hashtbl.remove tree_node_table) child_ids in
      T.delete_children (par : tree_node)

  method node_from_id id = 
    try
      (Hashtbl.find tree_node_table id)
    with Not_found -> failwith "get_node_from_id: unknown id!"
     
  method delete_node preproc n =
    let id = get_id n in
    let pp : unit = preproc n in
    M.msg_string M.Debug ("Deleting node: "^(string_of_int id));
    Hashtbl.remove tree_node_table id

  method node_exists n = 
    let i = get_id n in
    Hashtbl.mem tree_node_table i
end

exception RestartException
    
  (* statistic variables *)
let stats_nb_iterations = ref 0
let stats_nb_iterations_of_outer_loop = ref 0 (* added for races *)
let stats_nb_created_nodes = ref 0
let stats_nb_refinment_processes = ref 0
let stats_nb_refined_nodes = ref 0
let stats_nb_proof_tree_nodes = ref 0
let stats_nb_proof_tree_covered_nodes = ref 0
let stats_nb_deleted_nodes = ref 0
let local_restart_counter = ref 1

let reset_stats =
  stats_nb_iterations := 0 ;
  stats_nb_refinment_processes := 0 ;
  stats_nb_created_nodes := 0 ;
  stats_nb_refined_nodes := 0 ;
  stats_nb_proof_tree_nodes := 0;
  stats_nb_proof_tree_covered_nodes := 0
       
let print_stats () =
  M.msg_string M.Normal (Printf.sprintf "Nb iterations of outer while: %d \n"
  !stats_nb_iterations_of_outer_loop);
  M.msg_string M.Normal (Printf.sprintf "Nb iterations of reachability: %d \n"
  !stats_nb_iterations);
  M.msg_string M.Normal (Printf.sprintf "Nb created nodes: %d \n"
  !stats_nb_created_nodes);
  M.msg_string M.Normal (Printf.sprintf "Nb refinement processes: %d \n" !stats_nb_refinment_processes);
  M.msg_string M.Normal (Printf.sprintf "Nb refined nodes: %d \n"
  !stats_nb_refined_nodes);
  M.msg_string M.Normal (Printf.sprintf "Nb proof tree nodes: %d \n"
  !stats_nb_proof_tree_nodes);
  M.msg_string M.Normal (Printf.sprintf "Nb proof tree covered nodes: %d \n"
  !stats_nb_proof_tree_covered_nodes);
  M.msg_string M.Normal (Printf.sprintf "Nb deleted nodes: %d \n"
  !stats_nb_deleted_nodes);
  ()

(************************************************************************************************)
(** Model checker data types **)
type model_check_outcome =
  | No_error of tree_node
  | Error_reached of (error_path * tree_node)


(* the set of pending nodes to be processed *)
let unprocessed_nodes =  
  if (O.getValueOfBool "bfs") then new Search.bfsiterator
  else new Search.dfsiterator
  
let cutoff_nodes = new Search.dfsiterator (* aka a stack *)

(* helper object to create nodes *)
let tnc = new tree_node_creator

(* the global time used for time stamps *)
let time = ref 0 

let sig_caught_bit = ref false     

(**************************************************************************************************)

type error_check =
    EmptyErrorAtNode of R.t * tree_node * (tree_node list)
  | NonEmptyErrorAtRoot of R.t * tree_node * (tree_node list)

(*****************************************************************************************************************)
(************************************  FOCI MODEL CHECKER loop ***************************************************) 
(*****************************************************************************************************************)

let rec reclaim n = 
  if (T.has_child n || (not (T.has_parent n))) then ()
  else 
      (let par = get_parent n in
      T.delete_child n; 
      reclaim par)

(* covering data structures : aka the "reach set" *)
let cov = R.emptyCoverers ()


(*  define maps to track who is covered by whom -- 
    am postponing actually using these data structures *)

(* int -> int list map -- list of nodes that i covers *)
let covers_map = Hashtbl.create 1009

(* int -> int map -- the node that covers i *)
let coveredby_map = Hashtbl.create 1009

let cover_maps_add coverer coveree = 
  let [coverer_id;coveree_id] = List.map get_id [coverer;coveree] in
    Hashtbl.replace coveredby_map coveree_id coverer_id;
    Misc.hashtbl_check_update covers_map coverer_id coveree_id
         
let cover_maps_delete coverer coveree = 
  let [coverer_id;coveree_id] = List.map get_id [coverer;coveree] in
    Hashtbl.remove coveredby_map coveree_id;
    Misc.hashtbl_delete covers_map coverer_id coveree_id
     
(* This adds the successors of a node to the tree *)
let fmc_post reg op = 
   A.post reg op 

let add_children n =
  let n_region = get_region n in
  let add_child op = 
    let new_region = fmc_post n_region op in 
    let n_op_md = 
      {time_stamp = (time := !time + 1 ; !time) ;
       region = new_region; } in
    (tnc#create_child (n_op_md) op n) in
  List.rev (List.map add_child (A.enabled_ops n_region))
    
let delete_subtree n =
  let n_desc = T.descendants n in
  let n_cds = (get_parent n)::n_desc in
  let ris = List.map (fun n' -> (get_region n', get_id n')) n_cds in
  R.deleteCoverers cov ris;
  List.iter (tnc#delete_node (fun _ -> ())) n_desc;
  T.delete_child n

(* refinement functions *) 
 
let update_node node r =
  let r' = R.cap (get_region node) r in
  (get_node_data node).mark <- {time_stamp = 0; region = r'}

(* this is the core refine function -- used both when:
   1. A counterexample is found,
   2. For covered checking. *)
(* error_flag = true --> use full trace based update, if required (upon hitting error)
   error_flag = false --> use trel based update only, fail if it fails (for cover checking) *)
     
exception FeasiblePath 

let update_path_nodes error_flag n final_reg = 
  let _ = M.msg_string M.Debug (Printf.sprintf "In update_path_nodes: %b" error_flag) in
  let path = T.path_to_root n in
  let plen = List.length path in
  let _ = if error_flag then M.msg_string M.Normal ("trace depth: "^(string_of_int plen)) in
  let reg_l = List.map get_region path in
  let op_l  = List.map (get_op) (List.tl path) in
  let (ref_start_pos, new_reg_l) = 
    let reg_array = Array.of_list reg_l in
    let _ = Array.set reg_array (plen - 1) final_reg in
    A.fociModelChecker_refine_trace_abstract reg_array (Array.of_list op_l) in
  M.msg_string M.Debug (Printf.sprintf "Back from fmc_refine_trace: "^(string_of_int ref_start_pos));
  if (ref_start_pos = -1) then raise FeasiblePath 
  else
    (try
      (let n_pivot_o =  
        if ((O.getValueOfBool "bddpost" || O.getValueOfBool "fmcpa") 
            && error_flag && O.getValueOfString "comp" = "cut") 
        then
          let rsp = if (O.getValueOfBool "restart" || O.getValueOfBool "localrestart") 
                    then 1 else ref_start_pos in 
          Some (List.nth path rsp)
        else 
          (List.iter2 update_node (List.tl path) (List.tl new_reg_l);
           let i = Misc.findi R.is_empty new_reg_l in     
           if i < plen - 1 then Some (List.nth path (i+1)) else None) in 
      let _ = M.msg_string M.Debug (Printf.sprintf "found n_pivot_o") in
       match n_pivot_o with None -> () 
       | Some (n_pivot) ->
         let n_piv_par = get_parent n_pivot in 
         (Stats.time "delete subtree" delete_subtree n_pivot;
          unprocessed_nodes#add_element n_piv_par;
          ()))
     with e -> (M.msg_string M.Error ("Failure in update_node !"^(Printexc.to_string e)); raise e))
 
let ac_is_covered cov cover_check_fun n = 
  try 
    let r = get_region n in
    if R.is_empty r then true 
    else if (O.getValueOfBool "cov") && not (R.is_coverable r) then false 
    else cover_check_fun n (R.findCoverUnion cov r) 
  with ex -> (M.msg_string M.Error ("ERROR in ac_is_covered:"^(Printexc.to_string ex));raise ex)
    
let is_covered cov n =        
  let cover_check_and_update node cand_reg = 
    if (R.leq (get_region node) cand_reg) then true
    else if (R.is_empty cand_reg) then false
    else 
      try
        let t_reg = R.negate cand_reg in
        update_path_nodes false node t_reg;
        (get_node_data n).mark  <- {(get_node_data n).mark with region = cand_reg};
        true 
      with FeasiblePath -> false in
  ac_is_covered cov cover_check_and_update n

let is_covered_noupdate cov n = 
  ac_is_covered cov (fun n cr -> R.leq (get_region n) cr) n

let output_path n = 
  let path = T.path_to_root n in
  let op_l = (List.map (get_op) ( List.tl path)) in
  List.iter (fun op -> M.msg_string M.Normal (Op.to_string op)) op_l
  
let print_tree_from_root root = 
  let nl_to_string nl = 
    let (regs_l,loc_s) = R.to_strings (nl.mark.region) in
    let id = nl.id in
    Printf.sprintf "%d $$ (%s,%s)" id (loc_s) (Misc.strList regs_l) in
  M.msg_string M.Normal "**************************************************";
  M.msg_string M.Normal "**** Printing the Reachability Tree **************";
  M.msg_string M.Normal "**************************************************";
  let tree_size_ref = ref 0 in
  let node_iter n = 
    tree_size_ref := !tree_size_ref + 1;
    M.msg_printer M.Normal print_tree_node n in
  T.iter_descendants_nodes node_iter root;
  let name = "reachtree" in
  T.output_tree_dot nl_to_string Op.to_string (name^".dot") [root];
  Sys.command (Printf.sprintf "dot -Tps -o %s.ps %s.dot" name name);
  M.msg_string M.Normal ("Tree size: "^(string_of_int !tree_size_ref));
  Message.msg_string Message.Debug "Print Cover R";
  R.printCoverer cov
  
exception PostCheckException of (tree_node * tree_node)

exception CoveredCheckException of tree_node

(* WARNING:DNF *)
let check_tree root = 
  let _ = M.msg_string M.Normal "Checking Tree Validity" in
  let ctcov = R.emptyCoverers () in
  let check_post (n1, op , n2) = 
    let _ = M.msg_string M.Debug ("Checking Node: "^(string_of_int (get_id n2))) in
    let reg = get_region n1 in
    let reg' = get_region n2 in
    let res = 
      try not (A.check_abstract_transition reg op reg') 
      with e -> (M.msg_string M.Normal ("Error in check_abstract_transition"^(Printexc.to_string e)); true) in 
    if res then raise (PostCheckException (n1,n2))
      M.msg_string M.Debug ("[OK] Checking Node: "^(string_of_int (get_id n2)));
  in
  let gather_reach n = 
    if T.has_child n then R.addCoverer ctcov (get_region n) (get_id n) in
  let check_cov n = 
    if not (T.has_child n) && not (is_covered_noupdate ctcov n) then
    raise (CoveredCheckException n) in
  try
    T.iter_descendants_edges check_post root;
    T.iter_descendants_nodes gather_reach root;
    T.iter_descendants_nodes check_cov root; ()  
  with CoveredCheckException n -> 
	    (M.msg_string M.Error "Covered Check Fails!!";
	     M.msg_printer M.Error print_tree_node n)
  | PostCheckException (n1,n2) ->
	    (M.msg_string M.Error "Post Check Fails!!";
	    M.msg_string M.Error "Source Node:";
	    M.msg_printer M.Error print_tree_node n1;
	    M.msg_string M.Error "Target Node:";
	    M.msg_printer M.Error print_tree_node n2)


let post_process tree_root = 
  if (O.getValueOfBool "showtree") then print_tree_from_root tree_root;
  if (O.getValueOfBool "checktree") then Stats.time "check tree" check_tree tree_root;
  let size = T.subtree_size tree_root in
  let depth = T.subtree_depth tree_root in
  let reach_cubes = 
    T.fold_left_descendants 
    (fun c n -> c + R.cubes n.mark.region) 0 tree_root in
  M.msg_string M.Normal 
    (Printf.sprintf "Tree size: %d \n Tree depth: %d Tree cubes: %d \n" size depth reach_cubes );
    let s = "Program safe. Error not reachable! :-)" in    
    M.msg_string M.Error s;
  print_stats (); A.notify_result s

(* Now check if the cutoff nodes are really cut off -- if not, add them to the set of 
 * unprocessed nodes. This check should be the side-effect free -- i.e. is there 
 * someone in the tree that covers them -- without inducing further refines along the way *)

        (* if bddpost is false, then unproc_nodes = 0 at this point, hence just do
        the old thing -- i.e. make a pass using is_covered_noupdate ..
        if bddpost is true, then first make a pass using covered_update. 
                Now, if the unproc is not empty, then repeat mc ..
                     if it is empty, then, make a pass using noupddate, to be
                     double sure... 
        why this will terminate:
          claim: once you have computed the reachset in the tree, the only way a
          node gets added is if i fails the first check. this cannot happen once
          the trel is precise enough. So, rtp, eventually the trel is
          precise enough... suppose you hit error only finitely often. then you
          are done as you will find a fixpoint. how to hit error infinitely
          often...yak yak. TBProved properly. *)
let cutoff_check () =
  let filter_fun cov_check n = 
    if cov_check n then true else (unprocessed_nodes#add_element n;false) in
  let _ = cutoff_nodes#filter (fun n -> tnc#node_exists n) in 
  let _ = if (O.getValueOfBool "bddpost" || O.getValueOfBool "fmcpa") 
          then cutoff_nodes#filter (filter_fun (is_covered cov)) in
  let _ = if not (unprocessed_nodes#has_next ()) 
          then cutoff_nodes#filter (filter_fun (is_covered_noupdate cov)) in ()
         
exception Error of tree_node 

let mc_loop err = 
  while (unprocessed_nodes#has_next ()) do
    Misc.check_time_out ();
    (* {{{ begin debug info *)
      stats_nb_iterations := !stats_nb_iterations + 1;
      if (!stats_nb_iterations mod 100 == 0) then
          (M.msg_string M.Normal ("While loop tick:"^(string_of_int !stats_nb_iterations));
          flush stdout; flush stderr;);
      M.msg_string M.Debug "\n\n******************************************************\n" ;
      M.msg_string M.Debug "Next iteration of foci-model-check's big while-loop" ;
      M.msg_printer M.Debug Format.pp_print_int (!stats_nb_iterations);
      (* end debug info }}} *)
    let (n : tree_node) = unprocessed_nodes#next () in
    let r = get_region n in
    if tnc#node_exists n then
      if not (R.is_empty (R.cap r err)) then raise (Error n) else  
        if is_covered_noupdate cov n then cutoff_nodes#add_element n else 
          (R.addCoverer cov r (get_id n);
           unprocessed_nodes#add_list (add_children n))  
  done;
  cutoff_check ()

let check_refine n = 
  try update_path_nodes true n (R.erase_data (get_region n))
  with FeasiblePath -> (output_path n; raise FeasiblePath)

let model_check init err = 
  let rec mc_iter () = 
    try 
      M.msg_string M.Normal "[FMC] Continue mc_loop";
      mc_loop err; 
      (if unprocessed_nodes#has_next () then mc_iter ()); 
      M.msg_string M.Normal "Program Safe. Error not reachable! :-)"
    with (Error n) -> (M.msg_string M.Normal "Hit error: update"; check_refine n; mc_iter ()) in
  try
    M.msg_string M.Normal "Starting FMC model check";
    Misc.set_time_out_signal (O.getValueOfInt "timeout");
    let tree_root = tnc#create_root {time_stamp = !time; region = init} in
    unprocessed_nodes#add_element tree_root ;
    mc_iter ();
    post_process tree_root; 1 
  with FeasiblePath -> 
    let s = "Real Error Found! :-(" in
    (M.msg_string M.Error s; A.notify_result s;0)

end
