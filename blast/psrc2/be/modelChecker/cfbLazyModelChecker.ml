(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)



(* CF Reach with arbitrary search order.

Data Structures:
-- BiDirLabGraph.ml
-- Labels on node:
	{id : int;
	 region: R.t;
	 fname : string; 
	 entries : int set;
	 exits : int set; // only needed for entries
	 callsites : int set; // only needed for entries
         status : Unprocessed aka to be processed | Processed
	}
	
-- parents,children from the BDLG data structure
-- use the tnc object from focimodelchecker so that we have a clean API
   to find a node given id/delete node etc.
   
-- Assorted predicates:
	- is_exit n : if it has one out-edge marked with a return op
	- is_entry n : if it has no parents
	

-- Global "Worklist" Object: stores just integers
	- add node(id) to worklist
	- is_empty
	- pick (and remove) from worklist
	
   INVARIANTS:
   1. For all n in WKL:
	a. n's children WILL be added
   	b. n.region in the REACH set
	c. n is NOT covered by anyone else
   
   2. For all n: n.entries contains all the entries of n.fname
      that have a path to n in the graph of n.fname

   3. For all n', for all n \in n'.entries:
        is_exit n' ==> n' \in n.exits 
 
   4. Exit(n): n has an outgoing "Return" op.
      For all n. Exit(n) ==> (n \in wkl \/ n.kind = Exit _)

   Connectivity Invariants: (Predecessors always exist Graph)
   5. Forall n. 
      a. n.entries <> []
      b. Forall n' \in n.entries: exists(n')
   
   6. Forall n.
      is_entry n ==> (is_root n || n.callsites <> [])
*)

(* open Ast  
open BlastArch *)
module Stats = Bstats
module C_System_Descr = BlastCSystemDescr.C_System_Descr
module C_Command = BlastCSystemDescr.C_Command
module M = Message   
module O = Options

module Graph = BiDirectionalLabeledGraph

module Make_Lazy_Model_Checker : BlastArch.MAKE_LMC  =
  functor (A: BlastArch.ABSTRACTION) ->
  struct
    module R = A.Region
    module Op = A.Operation

 
(***********************************************************)
(********************* Data Types **************************)
(***********************************************************)

module IntSet = Set.Make(struct
  type t = int
  let compare = compare
end)

type node_kind = Entry | Internal | Exit of (Op.t)

type node_label =
  {
    id : int;
    region : R.t;
    mutable kind : node_kind;
    mutable entries : IntSet.t;
    mutable callsites : (Op.t * int) list ;
    mutable exits : int list;
    mutable callees : int list; (* only needed for refinement *)
    mutable returns : int list; (* only needed for refinement *)
  }
	
type edge_label = Intra of Op.t | Summary of (Op.t * int * int * Op.t)


(* bool : true iff edge usable for counterexample. false for leq-coverers *)
type graph_node = (node_label,(edge_label * bool)) Graph.node

type error_path = (R.t * Op.t list * R.t)

let get_node_id n = (Graph.get_node_label n).id

let get_node_region n = (Graph.get_node_label n).region

let get_node_fname n = 
  let reg = get_node_region n in
  R.get_function_name reg

let get_node_callsites n = (Graph.get_node_label n).callsites

let get_node_entries n = (Graph.get_node_label n).entries

let get_node_exits n = (Graph.get_node_label n).exits

let get_node_children n = Graph.get_children n

let get_node_calldepth n = 
  try R.get_call_depth (get_node_region n) with e ->
    (let es = Printexc.to_string e in
    M.msg_string M.Error es; -1) 

let get_node_callees n = (Graph.get_node_label n).callees

let get_node_returns n = (Graph.get_node_label n).returns

let get_node_kind n = (Graph.get_node_label n).kind

let add_node_callsite n c = 
  let cs = (Graph.get_node_label n).callsites in
  (Graph.get_node_label n).callsites <- (c::cs)
  
let add_node_callee n c = 
  let cs = (Graph.get_node_label n).callees in
  (Graph.get_node_label n).callees <- (c::cs)

let add_node_returns n r = 
  let rs = (Graph.get_node_label n).returns in 
  (Graph.get_node_label n).returns <- (r::rs)

let add_node_exits n e = 
  let es = (Graph.get_node_label n).exits in
  if not (List.mem e es) then ((Graph.get_node_label n).exits <- (e::es))

let del_node_callsite c_id n = 
  let cs = (Graph.get_node_label n).callsites in
  (Graph.get_node_label n).callsites <- (List.filter (fun (_,id) -> id <> c_id) cs)
  
let set_node_entries n es = (Graph.get_node_label n).entries <- es

let set_node_kind n k = (Graph.get_node_label n).kind <- k

let is_exit n = 
  match (Graph.get_node_label n).kind with
    Exit _ -> true
  | _ -> false

let is_entry n = 
  match (Graph.get_node_label n).kind with
   Entry -> true
  | _ -> false
  
let node_label_to_string nl =
  let (regs_l,loc_s) = R.to_strings (nl.region) in
  let id = nl.id in
  Printf.sprintf "%d $$ (%s,%s)" id loc_s (Misc.strList regs_l)

let edge_label_to_string el =
  match el with
    Intra(op),_ -> Op.to_string op
  | Summary(call_op,in_id,out_id,ret_op),_ -> 
      let op_s = Op.to_string call_op in
      Printf.sprintf "Summary(%s,%d,%d)" op_s in_id out_id

      
let print_graph_edge fmt e = 
  Format.fprintf fmt ";@ in_op=" ;
  Format.fprintf fmt ";@ %s" (edge_label_to_string (Graph.get_edge_label e)) ;
  Format.fprintf fmt ";@ parent_id=%d" (get_node_id (Graph.get_source e))

  
let print_graph_node fmt n =
  Format.fprintf fmt "@[GNode(@[label=" ;
  Format.fprintf fmt ";@ %s" (node_label_to_string (Graph.get_node_label n)) ;
  List.iter (print_graph_edge fmt) (Graph.get_in_edges n);
  let children_ids = List.map get_node_id (Graph.get_children n) in
  Format.fprintf fmt ";@ children_ids=" ;
      Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
  Format.fprintf fmt "@])@]"
    

(************************************************************)
(********************* Statistics Variables *****************)
(************************************************************)
let inc r = r := !r + 1
let stats_nb_iterations = ref 0
let stats_nb_created_nodes = ref 0
let stats_nb_refinment_processes = ref 0
let stats_nb_refined_nodes = ref 0
let stats_nb_proof_tree_nodes = ref 0
let stats_nb_proof_tree_covered_nodes = ref 0
let stats_nb_fun_proof_tree_nodes = ref 0
let stats_nb_deleted_nodes = ref 0
let local_restart_counter = ref 1

let reset_stats =
  stats_nb_iterations := 0 ;
  stats_nb_refinment_processes := 0 ;
  stats_nb_created_nodes := 0 ;
  stats_nb_refined_nodes := 0 ;
  stats_nb_proof_tree_nodes := 0;
  stats_nb_proof_tree_covered_nodes := 0
       
let print_stats fmt () =
  Format.fprintf fmt "@[<v>@[Nb iterations of reachability:@ %d@]" !stats_nb_iterations ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb created nodes:@ %d@]" !stats_nb_created_nodes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb refinment processes:@ %d@]" !stats_nb_refinment_processes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb refined nodes:@ %d@]" !stats_nb_refined_nodes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb proof tree nodes:@ %d@]" !stats_nb_proof_tree_nodes;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb fun tree nodes:@ %d@]" !stats_nb_fun_proof_tree_nodes;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb proof tree covered nodes:@ %d@]@]" !stats_nb_proof_tree_covered_nodes;
  Format.fprintf fmt "@[Nb deleted nodes:@ %d@]@]" !stats_nb_deleted_nodes;
  ()

(************************************************************)
(************************ Global Variables ******************)
(************************************************************)

(* reach set *)
let cov = R.emptyCoverers ()

(* worklist *)
let wkl =  
  if (O.getValueOfBool "bfs") then new Search.bfsiterator
  else new Search.dfsiterator

(* root node *)
let root_id_ref = ref (-1)

(* to prevent recomputation of summary-post *)
let sumpost_table = Hashtbl.create 1001

let is_sumpost_computed (caller_id,exit_id) = 
  let rv =  Hashtbl.mem sumpost_table (caller_id,exit_id) in
  M.msg_string M.Debug 
  (Printf.sprintf "Check sumpost_comp: (%d,%d) %b" caller_id exit_id rv);
  rv
  
let add_sumpost_computed (caller_id,exit_id) =
   M.msg_string M.Debug 
   (Printf.sprintf "Add sumpost_comp: (%d,%d)" caller_id exit_id);
   Hashtbl.replace sumpost_table (caller_id,exit_id) true 
 
let del_sumpost_computed (caller_id,exit_id) = 
  Hashtbl.remove sumpost_table (caller_id,exit_id)

class graph_node_creator = 
  object (self)
    val mutable next_id = 0
    
    val graph_node_table = Hashtbl.create 1009 
    (* hash int -> node ONLY persistent pointer to a node, single point of
     * deletion *)
    
    val entry_node_table : (string,int list) Hashtbl.t = Hashtbl.create 101
    val visited_fun_table : (string, bool) Hashtbl.t = Hashtbl.create 101
    val visited_fun_counter = ref 0

    
    method create_node is_entry reg = 
      let this_id = next_id in
      next_id <- next_id + 1;
      let es = 
        if is_entry then
          begin
          let fname = R.get_function_name reg in
          ignore(Misc.hashtbl_check_update entry_node_table fname this_id);
          if not (Hashtbl.mem visited_fun_table fname) then
            (Hashtbl.replace visited_fun_table fname true;
            visited_fun_counter := !visited_fun_counter + 1;
            M.msg_string M.Normal (Printf.sprintf "[CFB] Visited Fun %d : %s" !visited_fun_counter fname));
          IntSet.singleton this_id
          end
      else IntSet.empty
      in
      let label = 
        {id = this_id;region = reg;
        kind = if is_entry then Entry else Internal;
        entries = es;callsites = [];callees=[];exits = [];returns = []}
      in
      let rv = ((Graph.create_root label) : graph_node) in
      Hashtbl.add graph_node_table this_id rv;
      rv
    
    method node_from_id id = 
      try Some(Hashtbl.find graph_node_table id) with Not_found -> None
      
    method delete_node (id : int) = 
      let proc_in_e e = 
        match Graph.get_edge_label e with Intra _ , _ -> () 
        | Summary(_,_,ex_id,_), _ -> 
          let src_id = get_node_id (Graph.get_source e) in
          del_sumpost_computed (src_id,ex_id)
      in
      if Hashtbl.mem graph_node_table id then
        begin
        let n = Hashtbl.find graph_node_table id in
        let callees = Misc.map_partial (self#node_from_id) (get_node_callees n) in
        let reg = get_node_region n in
        let in_edges = Graph.get_in_edges n in
        List.iter proc_in_e in_edges;
        List.iter (del_node_callsite id) callees;        
        R.deleteCoverer cov reg id;
        Graph.delete_node n;
        Hashtbl.remove graph_node_table id
        end;

    
    method node_exists id = 
      try ignore(Hashtbl.find graph_node_table id);true with Not_found -> false
     

    method get_all_entries () = 
      let elist = Misc.hashtbl_to_list entry_node_table in
      let proc (fname,n_id_l) = 
        (Misc.map_partial (self#node_from_id) n_id_l, fname) in 
      List.map proc elist
     
    method get_entries fn = 
      let e_id_l = try Hashtbl.find entry_node_table fn with Not_found -> [] in
      Misc.map_partial (self#node_from_id) e_id_l
      
  end

let gnc = new graph_node_creator


(************************************************************)
(******************* Printing Functions *********************)
(************************************************************)

let get_proof_tree_size () = 
  let tups = gnc#get_all_entries () in
  let _add c (l,_)  = c + (List.length (Graph.descendants l)) in
  let fn = O.getValueOfString "getfunsize" in
  stats_nb_proof_tree_nodes := (List.fold_left _add 0 tups);
  stats_nb_fun_proof_tree_nodes := (try _add 0 (List.find (fun (_,f) -> f = fn) tups) with _ -> 0);
  ()

let print_reach_graph () =  
  M.msg_string M.Normal "Printing Reach Graph";
  let file = "reachgraph" in
  let tups = gnc#get_all_entries () in
  Graph.output_multi_graph_dot 
        node_label_to_string edge_label_to_string (file^".dot") tups;
  Sys.command (Printf.sprintf "dot -Tps -o %s.ps %s.dot" file file);
  ()
        

(************************************************************)
(****************** Propagation Functions *******************)
(************************************************************)

(* mods: WKL *) 
let rec push_entries ens n =
  let n_en = get_node_entries n in
  let n_en' = IntSet.union n_en ens in
  if (IntSet.cardinal n_en') > (IntSet.cardinal n_en) then
    (set_node_entries n n_en';
    if (is_exit n) then wkl#add_element (get_node_id n)
    else List.iter (push_entries ens) (get_node_children n))
    
(* mods: cov, wkl this is the ONLY place that:
    a. cov is added to/read, 
    b. new nodes are created using GNC *)
    (* TBD: HERE *)
let find_node is_entry reg =
  let (n_opt,b) = 
    let (i_opt,b) = 
      if R.is_noreturn reg
      then (R.findCoverer cov reg, false) 
      else (R.findExactCoverer cov reg, true) in
    match i_opt with
      Some id -> (gnc#node_from_id id,b) 
    | None -> (None,true) in
  let (n,is_new) = 
    match n_opt with
      Some n -> (n,false)
    | None -> 
        let n = gnc#create_node is_entry reg in 
        R.addCoverer cov reg (get_node_id n); 
        (n, true) in
  let wkl_add = 
    if is_new then [(get_node_id n)] else 
    if is_entry then (get_node_exits n) else [] in
  List.iter wkl#add_element wkl_add;
  (n,b)


let post_intra n e_label = 
  (* assert (e_label <> Intra(call/ret edge));*)
  let reg = get_node_region n in
  let (reg'_opt,n_ex_opt) = 
    match e_label with
      Summary (callexp,_,id_ex,ret_op) ->
        let id = get_node_id n in
        if (O.getValueOfBool "sumcache" && is_sumpost_computed (id,id_ex)) then (None,None)
        else
            (add_sumpost_computed (id,id_ex);
            match (gnc#node_from_id id_ex) with 
              None -> (assert false; failwith "post_intra: error!")
            | Some (n_ex) ->
               let n_ex_reg = get_node_region n_ex in
               (Some(A.summary_post reg n_ex_reg callexp ret_op), Some n_ex))
    | Intra (op) -> (Some(A.post reg op),None) in
  let n'b_opt = match reg'_opt with None -> None | Some(reg') -> Some(find_node false reg') in
  match n'b_opt with None -> () 
  | Some (n',b) ->
      (let _ = 
         match n_ex_opt with None -> () 
         | Some n_ex -> add_node_returns n_ex (get_node_id n') in
       push_entries (get_node_entries n) n';
       Graph.hookup_parent_child n n' (e_label,b)) 
    
let post_exit n ret_op =
  M.msg_string M.Debug "post_exit";
  set_node_kind n (Exit ret_op);
  let n_id = get_node_id n in
  let proc_callsite n_en_id (call_exp,call_id) = 
     match gnc#node_from_id call_id with
       None -> ()
     | Some n_call -> post_intra n_call (Summary (call_exp,n_en_id,n_id,ret_op))
  in
  let proc_entry n_en_id =
    match gnc#node_from_id n_en_id with
      None -> ()
    | Some n_en ->
        begin
          add_node_exits n_en n_id;
          List.iter (proc_callsite n_en_id) (get_node_callsites n_en)
        end
  in
  IntSet.iter proc_entry (get_node_entries n)
  
let post_call n op = 
  let reg = get_node_region n in
  let reg_en = A.post reg op in
  let n_en,_ = find_node true reg_en in 
  let cs = get_node_callsites n_en in
  add_node_callsite n_en (op, get_node_id n);
  add_node_callee n (get_node_id n_en)
    
let process_op n op = 
    match Op.get_info op with
      Op.Normal -> post_intra n (Intra op)
    | Op.Call _ -> post_call n op
    | Op.Ret _  -> post_exit n op

(***************************************************************)
(********************* Refinement Functions ********************)  
(***************************************************************)

(* A. find_path : node ->  (node * op) list *)


let efilter stk n e_label = 
  match e_label with
    Intra _,b -> b
  | Summary(_,_,ex_id,_),b -> b && (not (List.mem (get_node_id n,ex_id) stk))
  
let rec expand_se stk (n,e_label) = 
  match e_label with
      Intra(op) -> [(n,e_label)]
    | Summary(call_op,en_id,ex_id,ret_op) ->
        begin
          match Misc.map_partial gnc#node_from_id [en_id;ex_id] with
          [n_en;n_ex] -> 
            let stk' = (get_node_id n,ex_id)::stk in
              let p = sp_intra stk' n_en n_ex in 
                [n,Intra(call_op)] @ p @ [n_ex,Intra(ret_op)]
          | _ -> failwith "expand_se: bad number of nodes!"
        end
and sp_intra stk n_en n_ex =
  let ip = Graph.shortest_path (efilter stk) n_en n_ex in
  let ip = List.map (fun (n,(e,_)) -> (n,e)) ip in
  List.flatten (List.map (expand_se stk) ip)

  
let get_best_entry n =
  let proc en =
    let en_id = get_node_id en in
    if (en_id = !root_id_ref) then (en,-2,None) else
    match get_node_callsites en with
      [] -> failwith "get_best_entry: non-root with no callsites!"
    | cs -> 
        let ((cexp,id),d) = Misc.list_argmin snd cs in
    (en,d,Some(cexp,id))
  in
  match Misc.map_partial gnc#node_from_id (IntSet.elements (get_node_entries n)) with
   [] -> failwith "get_best_entry: no entries!"
  | entries -> 
      (let ((en,_,call_opt),_) = (Misc.list_argmin Misc.snd3 (List.map proc entries)) in
      assert (call_opt <> None || (get_node_id en = !root_id_ref));
      M.msg_string M.Normal ("[GBE] Out Node Depth:"^(string_of_int (get_node_calldepth en)));
      (en,call_opt))

let rec find_path n =
  let (n_en,caller_opt) = get_best_entry n in
  let ip = sp_intra [] n_en n in
  let prefix = 
    match caller_opt with 
      None -> []
    | Some (cexp,id) -> 
        begin
          match gnc#node_from_id id with None -> failwith "caller doesnt exist!"
          | Some (n_call) -> 
              let calledge = (id,get_node_id n_en) in
              (find_path n_call) @ [n_call,Intra(cexp)]
        end
  in
  prefix @ ip
    
(* B: delete nodes that were refined *)

let adjacent n =
  let intra_pred = Graph.get_parents n in
  let intra_succ = Graph.get_children n in
  let inter_pred = 
    let cs = get_node_callsites n in
    Misc.map_partial (fun (_,id) -> gnc#node_from_id id) cs in
  let call_succ = 
    let cs = get_node_callees n in
    let proc id = 
      let n_opt = gnc#node_from_id id in
      let (c1,c2) = 
        match n_opt with None -> (true,true)
          | Some n -> ((List.length (get_node_callsites n) = 1),
                        A.is_fun_refined (get_node_fname n)) in
      let take = 
        match O.getValueOfString "comp" with 
        "lazy" -> c1 | "demand" -> c1 || c2 | _ -> true in 
      if take then n_opt else None in 
    Misc.map_partial proc cs in 
  let ret_succ = Misc.map_partial (gnc#node_from_id) (get_node_returns n) in
  (intra_pred@inter_pred, intra_succ@call_succ@ret_succ)
  
let rec remove_node n =
  let n_id = get_node_id n in
  if (n_id = !root_id_ref) then 
    let s = "Warning: cannot remove root node!" in
    (M.msg_string M.Normal s;M.msg_string M.Error s)
  else
    begin
      let (preds,succs) = adjacent n in
      gnc#delete_node (get_node_id n);
      List.iter (fun n -> wkl#add_element (get_node_id n)) preds;
      List.iter remove_node succs
    end

let rec refine_path n_id_l = 
  match Misc.get_first_cont_suffix (gnc#node_exists) n_id_l with [] -> ()
  | h::t ->
    (match gnc#node_from_id h with None -> failwith "assert_failure!"
     | Some(n) -> (remove_node n;refine_path t))
 
let refine_function fn =
  let entries = gnc#get_entries fn in
  let del_roots = 
    if (List.mem fn (O.getValueOfStringList "main")) then
    List.flatten (List.map Graph.get_children entries)
    else entries in  
  List.iter remove_node del_roots
      
exception FeasiblePath



let feasible_path op_l = 
  M.msg_string M.Error "Real Error Found!  :-(";
  List.iter (fun op -> M.msg_string M.Normal (Op.to_string op)) op_l
  


let del_print_debug id = 
  match gnc#node_from_id id with 
    None -> failwith "deleting dead node!" 
  | Some (n) -> M.msg_printer M.Debug print_graph_node n 


  
let check_refine n = 
  M.msg_string M.Debug "Check Refine:";
  let (n_l',e_l) = Misc.unzip (find_path n) in
  if (O.getValueOfBool "norefine") then 
    (let s = "No Refine: ctrx size"^(string_of_int (List.length n_l')) in
    M.msg_string M.Error s;
    raise FeasiblePath);
  let n_l = n_l'@[n] in
  let op_l = List.map (fun e -> match e with Intra o -> o | _ -> failwith "non flat path!") e_l in
  let _ = 
    try
      let rsp = A.block_analyze_trace (List.map get_node_region n_l) op_l  in
      M.msg_string M.Normal ("[CFB] Refining from: "^(string_of_int rsp));
      if (rsp <= 0) then (feasible_path op_l;raise FeasiblePath)    
      else 
        let n_id_l = List.map get_node_id (snd (Misc.cut_list_at_k n_l (rsp - 1))) in
        List.iter del_print_debug n_id_l; (* DEBUG *)
      refine_path n_id_l 
    with A.RefineFunctionException fn -> refine_function fn in
  ()


(***************************************************************)
(******************** Top-level MC routine *********************)
(***************************************************************)

let reachloc_table = Hashtbl.create 1001

let add_reachloc reg = 
  let loc_id = R.get_location_id reg in
  Hashtbl.replace reachloc_table loc_id true

let is_reachable loc_id = Hashtbl.mem reachloc_table loc_id

exception Error of graph_node
(* returns () --> safe | raises Err(n) *)
let mc_loop err =
   while (wkl#has_next ()) do
     Misc.check_time_out ();
     inc stats_nb_iterations;
     if (!stats_nb_iterations mod 100 = 0) then
       M.msg_string M.Normal ("While loop tick:"^(string_of_int !stats_nb_iterations));
     let dp_reset = O.getValueOfInt "simpres" in
     let _ = 
       if dp_reset = 0 then () 
       else if (!stats_nb_iterations mod dp_reset = 0) then 
               A.reset_decision_procedures () in
     let n_id = wkl#next () in
     match gnc#node_from_id n_id with None -> () | Some (n) ->
       let n_region = get_node_region n in
       if not (R.is_empty n_region) then
         if not (R.is_empty (R.cap n_region err)) then raise (Error n)
         else (add_reachloc n_region; List.iter (process_op n) (A.enabled_ops n_region)) 
   done

(* returns () --> safe, or raises FeasiblePath *)
let model_check init err = 
  let rec mc_iter () = 
     try
       M.msg_string M.Normal "[CFB] Continue mc_loop"; 
       mc_loop err; 
       M.msg_string M.Normal "Program Safe. Error not reachable! :-)";
     with Error(n) -> (check_refine n;mc_iter ()) in
  try
    M.msg_string M.Normal "Starting CFB model check";
    Misc.set_time_out_signal (O.getValueOfInt "timeout");
    root_id_ref := get_node_id (fst (find_node true init));
    mc_iter ();
    get_proof_tree_size ();
    if (O.getValueOfBool "showtree") then print_reach_graph ();1
  with FeasiblePath -> (M.msg_string M.Error "Real Error Found! :-(";0)

end
