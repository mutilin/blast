(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
(**
 * This module defines a symbolic representation for C programs, equipped
 * with boolean operations.
 *)

module S = Bstats
module M = Message
module O = Options

module E = Ast.Expression
module P = Ast.Predicate
module AA = AliasAnalyzer
module C = CaddieBdd
module PT = PredTable
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module DL = Lattice.DataLattice

module Region = struct
  type stack_contents = CallStack of C_SD.location list | EveryStack
     
  type data_region = {
    lelement : DL.lattice ;
    bdd   : C.bdd ;
    preds : int list;
  }
     
  type t = {
    location : C_SD.location ;
    stack    : stack_contents;
    data     : data_region ;
  }
  
  let dr_top = { lelement = DL.top; bdd = PT.bddOne; preds = [] }
  
  let dr_bot = { lelement = DL.bottom; bdd = PT.bddZero; preds = [] }

  (***************************************************************************)
  (*************************** Printing etc. *********************************)
  (***************************************************************************)
  
  let list_of_stack stack = 
    match stack with EveryStack -> [] | CallStack s -> s
  
  let stack_to_string s =
    let f x = (C_SD.get_location_fname x)^":"^(C_SD.location_to_string x) in
    match s with EveryStack -> "EveryStack"
    | CallStack(s') -> "["^(String.concat ";" (List.map f s'))^"]"

  let loc_to_string_id loc = 
    let (a,b) = C_SD.location_coords loc in 
    Printf.sprintf "(%d,%d)" a b

  let print_data_region fmt abst_data_reg =
    Format.fprintf fmt "@[DataRgn:@\n";
    if (O.getValueOfBool "bddprint") then 
       (Format.fprintf fmt "Bdd:@\n";
        P.print fmt (PT.convertBddToPred abst_data_reg.bdd);
	Format.pp_force_newline fmt ())
    else (Format.fprintf fmt "Predicates: [...]@\n");
    Format.fprintf fmt "Lattice:@\n";
    DL.print fmt abst_data_reg.lelement ;
    Format.pp_force_newline fmt ();
    Format.pp_close_box fmt ()

  let print fmt r =
    Format.fprintf fmt "@[[Region:@ " ;
    C_SD.print_location fmt r.location ;
    Format.pp_force_newline fmt ();
    print_data_region fmt r.data;
    (match r.stack with EveryStack -> Format.fprintf fmt "Stack: Everystack@\n" | _ -> 
      (Format.fprintf fmt "Stack:@\n";Misc.list_printer_from_printer
       C_SD.print_location fmt (list_of_stack r.stack)));
    Format.fprintf fmt "]@]@\n"
 
  let log_region loglevel logtype reg  =
    M.log_print loglevel logtype print reg

  let to_string = Misc.to_string_from_printer print

  let to_strings r = 
    ([PT.bdd_to_string_pretty r.data.bdd; DL.toString r.data.lelement],
     loc_to_string_id r.location)

  (***************************************************************************)
  (*************************** Get/Extract   *********************************)
  (***************************************************************************)

  let to_predicate r = PT.convertBddToPred r.data.bdd

  let get_pcStack r =
    (r.location, if O.getValueOfBool "cf" then EveryStack else r.stack)

  let pcStacks (l,s) = 
    (loc_to_string_id l) ^":"^ (stack_to_string s)

  let get_call_depth r =  C_SD.call_depth r.location

  let get_function_name r = C_SD.get_location_fname r.location

  let get_location_id r = C_SD.location_coords  r.location

  (***************************************************************************)
  (********************************** Tests **********************************)
  (***************************************************************************)

  let is_loc_eq l1 l2 =
    C_SD.location_coords l1 = C_SD.location_coords l2

  let is_stack_eq stk1 stk2 =
    if (O.getValueOfBool "cf") then true else
    match (stk1, stk2) with 
       (EveryStack, EveryStack) -> true
     | (EveryStack,_) | (_,EveryStack) -> false
     | (CallStack _s1, CallStack _s2) -> 
         (try List.for_all2 (fun l1 l2 -> is_loc_eq l1 l2) _s1 _s2 with _ -> false)

  let is_control_eq r1 r2 = 
    is_loc_eq r1.location r2.location && is_stack_eq r1.stack r2.stack
   
  let is_coverable r =
     match O.getValueOfString "cover" with
       "join" -> C_SD.is_join r.location
     | "loop" -> C_SD.is_loopback r.location
     | _ -> true

  let is_noreturn r = 
    C_SD.is_noreturn (get_function_name r)

  let is_empty r =
    let b = r.data.bdd in
    (not (DL.is_consistent r.data.lelement b)) || 
    (C.bddEqual b PT.bddZero)
  
  let leq r1 r2 =
    is_loc_eq r1.location r2.location &&
    is_stack_eq r1.stack r2.stack &&
    C.bddEqual r2.data.bdd (C.bddOr r1.data.bdd r2.data.bdd) && 
    DL.leq r1.data.lelement r2.data.lelement

  let eq reg1 reg2 = 
    (leq reg1 reg2) && (leq reg2 reg1)

  (***************************************************************************)
  (*************************** Manipulation  *********************************)
  (***************************************************************************)


  let cubes r = PT.bdd_cubes r.data.bdd 

  let negate r = 
    {r with data = {r.data with bdd = (C.bddNot r.data.bdd)}}

  let erase_data r =
    {r with data = dr_top}
    
  let cap r1 r2 =
    match (is_loc_eq r1.location r2.location, r1.stack, r2.stack) with 
      (true, EveryStack, s) | (true, s, EveryStack) | (true, s,_) when s = r2.stack -> 
        {{r1 with stack = s} with data =  
            {lelement = DL.meet r1.data.lelement r2.data.lelement; 
             bdd = C.bddAnd r1.data.bdd r2.data.bdd ; 
             preds = Misc.union r1.data.preds r2.data.preds;}}
    | _ -> {r1 with data = dr_bot}

  let cup r1 r2 = 
    assert (is_loc_eq r1.location r2.location);
    assert (is_stack_eq r1.stack r2.stack);
    {r1 with data = 
      {lelement = DL.join r1.data.lelement r2.data.lelement;
       bdd = C.bddOr r1.data.bdd r2.data.bdd; 
       preds = Misc.union r1.data.preds r2.data.preds;}}

(***************************************************************************)
(********************** coverers data structure ****************************)
(***************************************************************************)
      
  module ATR =
    Hashtbl.Make (
      struct
        type t = C_SD.location * stack_contents 
        
        let hash (l,s) = 
          let c = C_SD.location_coords l in
          if O.getValueOfBool "cf" then Hashtbl.hash c  
          else Hashtbl.hash (c,stack_to_string s)

        let equal (l1,s1) (l2,s2) = 
          (is_loc_eq l1 l2) && (is_stack_eq s1 s2)
      end)
    
    
  type 'a coverers = (t * (t * 'a) list) ATR.t
       
  let emptyCoverers () = ATR.create 101 
            
  let printCoverer cov =
    let print_entry (l,s) (r,_) =
      M.msg_string M.Debug ("pc,stack: "^(pcStacks (l,s)));
      M.msg_string M.Debug (PT.bdd_to_string_pretty r.data.bdd) in
      ATR.iter print_entry cov

  let addCoverer cov r d =
    let pc = get_pcStack r in
    let (r',cs) = try ATR.find cov pc with Not_found -> (r,[]) in
    let r'' = 
      {r' with data = 
        {lelement = DL.join r.data.lelement r'.data.lelement;
         bdd = C.bddOr r.data.bdd r'.data.bdd;
         preds = Misc.union r.data.preds r'.data.preds;}} in
    ATR.replace cov pc (r'',(r, d)::cs)

  let deleteCoverer cov r d = 
    let pc = get_pcStack r in
    if ATR.mem cov pc then 
      let (r,cs) = ATR.find cov pc in
      let cs' = List.filter (fun (_,d') -> d <> d') cs in
      ATR.replace cov pc (r,cs')

  (* deletes and rebuilds *)
  let deleteCoverers cov rds = 
    let pct = Hashtbl.create 251 in
    let pcst = Hashtbl.create 251 in
    List.iter 
      (fun (r,d) -> 
         let pc = get_pcStack r in
         Hashtbl.add pcst (pcStacks pc) pc;
         Hashtbl.add pct (pcStacks pc) d) rds;
    Hashtbl.iter 
      (fun pcs pc ->
         if ATR.mem cov pc then
           let cur = snd (ATR.find cov pc)  in
           let dels = Hashtbl.find_all pct pcs in
           let cur' = Misc.delete_assoc_list cur dels in
           match List.map fst cur' with [] -> ATR.remove cov pc 
           | r::rs -> ATR.replace cov pc (List.fold_left cup r rs, cur')) pcst
    
  let findCoverUnion cov r =
    try fst (ATR.find cov (get_pcStack r)) 
    with Not_found -> {r with data = dr_bot} 

  let findCoverer cov r =
    try
      let (_,cs) = S.time "findCov:lookup" ATR.find cov (get_pcStack r) in
      let (_,d) = S.time "findCov:find" (List.find (fun (r', _) -> leq r r')) cs in
      Some d
    with Not_found -> None

  let findExactCoverer cov r = 
    try
      let (_,cs) = ATR.find cov (get_pcStack r) in
      let (_, d) = List.find (fun (r', _) -> eq r r') cs in
      Some d
    with Not_found -> None

end







