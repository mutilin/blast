
(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


open BlastArch
module Constant = Ast.Constant
module E = Ast.Expression
module P = Ast.Predicate
module S = Bstats
module M = Message
module O = Options
module C_SD = BlastCSystemDescr.C_System_Descr
module CB = CaddieBdd


(******************************************************************)
(******************** Part I : BDD Management *********************)
(******************************************************************)

(******************************************************************)
(************* Misc BDD manipulation functions ********************)
(******************************************************************)
    
  let bddManager = CB.init 0 32 256 512 

  (* API *)
  let bddZero = CB.bddFalse bddManager 
    
  (* API *)
  let bddOne = CB.bddTrue bddManager
  
  let bdd_support b = 
    let rv = ref [] in
    CB.bddForeachCube (CB.bddSupport b)
    (fun a -> rv := (Misc.array_filter ((<>) 2)  a) @ !rv); 
    Misc.sort_and_compact !rv 
  
  let blast_bddForeachCube bdd f = 
    let cc = ref 0 in
    let f' a = (cc := !cc + 1; f a) in
    CB.bddForeachCube bdd f';
    (if !cc > 150 then M.msg_string M.Error ("Warning: foreachCube #cubes="^(string_of_int !cc)))

  let bdd_cube_cover bdd = 
    let check_upd b b' = 
      if CB.bddEqual (CB.bddImp bdd b') bddOne 
      then CB.bddAnd b b' else b in  
    List.fold_left 
    (fun cube i -> 
      let bi  = CB.bddIthVar bddManager i in
      let bi' = CB.bddNot bi in
      check_upd (check_upd cube bi) bi')
    bddOne (bdd_support bdd) 
      
  let bdd_cubes bdd = 
    let ctr = ref 0 in
    if CB.bddEqual bdd bddZero || CB.bddEqual bdd bddOne then 1
    else (CB.bddForeachCube bdd (fun _ -> Misc.inc ctr); !ctr)

  let lits_of_cube bdd =
    assert (bdd_cubes bdd = 1);
    let abl = ref [] in
    blast_bddForeachCube bdd 
    (fun cube -> 
      for i = 0 to (Array.length cube -1) do
        match cube.(i) with
          0 ->  abl:= (i,false)::!abl
        | 1 ->  abl:= (i,true)::!abl
        | v ->  assert (v=2)
      done;); !abl
       
  (******************************************************************)
  (************* Managing the Set of Predicates *********************)
  (******************************************************************)

  let refined_flag = ref false  

  (* general caddie variables *)
  let maxPredIndex = 1300 
  let predIndex = ref (-1)
  let allPreds = ref ([] : (int * P.predicate * CB.bdd) list)
  let allPredt = Hashtbl.create 109
  let allPreda = Array.create maxPredIndex (P.True,bddOne) 
  let cPreda = Array.create maxPredIndex (-1)
  let nPreda = Array.create maxPredIndex (-1)
  
  (* bdd-image computation variables *)
  let transrelt : ((int*int) * (int*int),CB.bdd) Hashtbl.t = Hashtbl.create 109
  let eqtransrelt : ((int*int) * (int*int),CB.bdd) Hashtbl.t = Hashtbl.create 109
  let global_inv_bdd = ref bddOne
  let bddVars = ref [] 
  let curr_var_map = ref (CB.toMap bddManager [])
  let curr_var_cube = ref bddOne
  let next_var_map = ref (CB.toMap bddManager [])
  let next_var_cube = ref bddOne 

(**********************************************************************)
(***************************** Get ************************************)
(**********************************************************************)
 
   
  let validIndex i = 0 <= i && i <= !predIndex
    
  let getNextIndex () = 
    Misc.inc predIndex; 
    if !predIndex >= maxPredIndex then
      failwith "Bound on the maximum number of predicates exceeded!"
    else !predIndex

  let getCurrentIndex () = !predIndex

  let getPred i =
    assert (validIndex i);
    fst (allPreda.(i))
 
  let getPredIndexBdd i =
    assert (validIndex i);
    snd (allPreda.(i))

  let getPredIndex p = 
    Hashtbl.find allPredt p
  
  let getAtomIndex p = 
    try (getPredIndex p,true) with Not_found -> 
      try (getPredIndex (P.negate p),false) with Not_found -> 
        Misc.failokwith ("getAtomIndex "^(P.toString p))

  let getPredBdd p =
    try getPredIndexBdd (getPredIndex p) with Not_found -> 
      CB.bddNot (getPredIndexBdd (getPredIndex (P.negateAtom p)))

  let getNIndexFromCIndex i = 
    let i' = cPreda.(i) in
    if i' = -1 then None else Some i'

  let getCIndexFromNIndex i' = 
    let i = nPreda.(i') in
    if i = -1 then None else Some i

  let isCurrentIndex i = 
    (not (O.getValueOfBool "bddpost")) || getNIndexFromCIndex i <> None

  let isNextIndex i' = 
    getCIndexFromNIndex i' <> None
  
  let getPredIndexNextBdd i =
     assert (isCurrentIndex i);
     let i' = cPreda.(i) in
     snd (allPreda.(i'))

  let get_transrel_bdd key = 
    try Hashtbl.find transrelt key with Not_found -> bddOne

  let get_eq_transrel key = 
    try Hashtbl.find eqtransrelt key with Not_found -> bddOne

  let get_all_pred_indexes () = 
    Misc.map_partial (fun (i,_,_) -> if isCurrentIndex i then Some i else None) !allPreds

(******************************************************************)
(**************** BDD-based Image Computation *********************)
(******************************************************************)
 
  let bdd_inv b = 
    if (O.getValueOfBool "invbdd") then CB.bddAnd b !global_inv_bdd else b 
  
  let bdd_postimage b t = 
    let b' = CB.bddAndAbstract b t (!curr_var_cube) in
    bdd_inv (CB.replace b' (!curr_var_map)) 

  let bdd_preimage postb trel_bdd = 
    let postb' = CB.replace postb (!next_var_map) in
    bdd_inv (CB.bddAndAbstract trel_bdd postb' (!next_var_cube)) 
  
  let make_eq_bdd is = 
    List.fold_left
    (fun b i -> CB.bddAnd (CB.bddBiimp (getPredIndexBdd i) (getPredIndexNextBdd i)) b)
    bddOne is

(******************************************************************)
(************************* Set ************************************)
(******************************************************************)

 let update_bdd_info (i,i') =
    bddVars := (i',i)::!bddVars;
    curr_var_map := CB.toMap bddManager (!bddVars);
    next_var_map := CB.toMap bddManager (List.map (fun (x,y) -> (y,x)) !bddVars);
    curr_var_cube := CB.bddAnd !curr_var_cube (getPredIndexBdd i);
    next_var_cube := CB.bddAnd !next_var_cube (getPredIndexNextBdd i)

 let add_new_predicate p = 
   let _add () = 
     let i = getNextIndex () in
     let b = CB.bddNewVar bddManager in   
     allPreds := (i, p, b) :: (!allPreds); 
     Array.set allPreda i (p,b); i in
   let i = _add () in
   Hashtbl.add allPredt p i;
   let _ = 
     if (O.getValueOfBool "bddpost") then 
       let i' = _add () in 
       (Array.set cPreda i i'; Array.set nPreda i' i; update_bdd_info (i,i')) in
   i

 let update_transrel_bdd (((l1,l2),(l1',l2')) as key) b = 
   let tr = get_transrel_bdd key in
   let tr' = CB.bddAnd tr b in
   Hashtbl.replace transrelt key tr';
   if not (CB.bddEqual tr' tr) then refined_flag := true

 let update_eq_transrel_bdd k is =
   let tr' = CB.bddAnd (get_eq_transrel k) (make_eq_bdd is) in
   Hashtbl.replace eqtransrelt k tr'
 
 let update_global_inv_bdd bdd = 
   global_inv_bdd := CB.bddAnd bdd (!global_inv_bdd)


(******************************************************************)
(****************** Convert Predicates to BDDs ********************)
(******************************************************************)
  
  let rec convertPredToBdd_map f p =
    let g = convertPredToBdd_map f in
    match p with
      P.Atom a -> Misc.do_catch ("convertPredToBdd_map:"^(P.toString p)) f p 
    | P.And ps -> List.fold_left (fun b p -> CB.bddAnd b (g p)) bddOne ps 
    | P.Or ps ->  List.fold_left (fun b p -> CB.bddOr b (g p)) bddZero ps 
    | P.Not p -> CB.bddNot (g p)
    | P.Implies (p1, p2) -> CB.bddImp (g p1) (g p2)
    | P.True -> bddOne
    | P.False -> bddZero
    | _ -> failwith "convertPredToBdd_map : missing case"
  
  let rec convertPredToBdd p =
    convertPredToBdd_map 
    (fun ((P.Atom a) as p)-> 
      Misc.do_catch ("convertPredToBdd: "^(E.toString a)) getPredBdd p)
    p 
  
  let ac_convertCubeToPred f cube =
    let ps = ref [P.True] in
    Array.iteri 
    (fun i v ->
      let p = match v with 0 -> P.negate (f i) | 1 -> f i | _ -> P.True in
      ps := p :: !ps) 
    cube; 
    if (Array.length cube = 1) then (List.hd !ps) else (P.conjoinL !ps)
  
  let convertCubeToPred = ac_convertCubeToPred getPred

  let ac_convertBddToPred f b =
    if CB.bddEqual bddOne b then P.True else 
      if CB.bddEqual bddZero b then P.False else
        let ms = ref [] in
        let _ = blast_bddForeachCube b 
        (fun c -> ms := (ac_convertCubeToPred f c)::!ms) in
        if List.length !ms = 1 then List.hd !ms else P.disjoinL !ms 
              
   let convertBddToPred = ac_convertBddToPred getPred

   let bdd_to_string_pretty b =
     if not (O.getValueOfBool "bddprint") then "XXX" else 
       let p = ac_convertBddToPred 
       (fun i -> if isNextIndex i then P.Next (getPred i) else getPred i) b in
       P.toString p

   let bdd_to_string b = "XXX"

(***********************************************************************)
(****************** Part II : Predicate Management *********************)
(***********************************************************************)

(* constants *)
let bp_prefix = "b_p__BS__" 
let bp_next_prefix = bp_prefix^"_n"  

let locPredt : ((int * int) , int list) Hashtbl.t  = Hashtbl.create 1009
let funPredt : (int, int list) Hashtbl.t = Hashtbl.create 1009
let glbPreds = ref ([] : int list)

let print_lp_table () = 
  Hashtbl.iter 
  (fun (l1,l2) ps ->  
    M.msg_string M.Debug (Printf.sprintf "Location (%d,%d)" l1 l2);
    List.iter (fun p -> M.msg_string M.Debug (string_of_int p)) ps)
  locPredt;
  Hashtbl.iter
  (fun f ps ->  
    M.msg_string M.Debug (Printf.sprintf "Function (%d)" f);
    List.iter (fun p -> M.msg_string M.Debug (string_of_int p)) ps)
  funPredt

let lookup_location_predicates loc = 
  !glbPreds @
  try 
    if O.getValueOfInt "craig" >= 2 then Hashtbl.find locPredt loc 
    else Hashtbl.find funPredt (fst loc) 
  with Not_found -> [] 

let is_pred_global = 
  let pred_globalt = Hashtbl.create 101 in
  (fun i -> 
    Misc.do_memo pred_globalt 
    (fun i -> List.exists C_SD.is_global (P.allVarExps_deep (getPred i))) i i)

let pred_in_scope =
  let t = Hashtbl.create 37 in
  fun fname i ->
    Misc.do_memo t 
    (fun () -> 
      let lvs = P.lvals_of_predicate (getPred i) in
      List.for_all (C_SD.is_in_scope fname) lvs)
    () i

let get_all_loc_pred_tuples () = 
  Misc.hashtbl_to_list locPredt

(***************************************************************************)
(*********************** Skip Fun ******************************************)
(***************************************************************************)

(* not in abs_util.ml for dependency reasons *)

(* Theorem: Sound to skip a call "lv = f(es)" if SP.\varphi.mods = \varphi.*)

  let stats_nb_take_funs = ref 0

  let (add_skip_fun, delete_skip_fun, add_take_fun, always_take, always_skip) =   
    let taket = Hashtbl.create 101 in
    let skipt = Hashtbl.create 101 in
    ((fun f ->
      M.msg_string M.Debug ("Adding skip_fun: "^f);
      Hashtbl.replace skipt f true),
    (fun f ->
      try 
        M.msg_string M.Debug ("Removing skip_fun: "^f);
        Hashtbl.remove skipt f
      with _ -> ()),
    (fun f ->
      if not (Hashtbl.mem taket f) then
        (stats_nb_take_funs := 1 + !stats_nb_take_funs;
         M.msg_string M.Debug ("Adding take_fun: "^f);
         Hashtbl.replace taket f true;
         Hashtbl.remove skipt f)),
    (fun f -> Hashtbl.mem taket f),
    (fun f -> Hashtbl.mem skipt f))


(***************************************************************************)
(************************** Lvals/Preds Mod  *******************************)
(***************************************************************************)

  
  let (update_imp_lv_table, is_relevant_lval, relevant_globals_mod) =
    let impLvalst = Hashtbl.create 109 in
    let glbsModt = Hashtbl.create 101 in
    ((fun lv ->
      Misc.do_memo impLvalst 
      (fun () -> 
         M.msg_string M.Debug ("Adding imp_lval:"^(E.lvalToString lv));
         ignore(List.map (C_SD.add_field_to_unroll false) (E.fields_of_lval lv));
         if C_SD.is_global lv then
           let fs = C_SD.global_lvals_mod_by lv in 
           List.iter 
           (fun f ->
              ignore (Misc.hashtbl_check_update glbsModt f lv);
              add_take_fun f) fs
         else List.iter delete_skip_fun (C_SD.may_mod_by lv))
       () (E.lvalToString lv)),
     (fun lv ->
        List.exists 
          (Hashtbl.mem impLvalst) 
          (List.map E.lvalToString (lv::(E.lvals_of_expression (E.Lval lv))))),
     (fun f -> try Hashtbl.find glbsModt f with Not_found ->[]))
    
  let (update_pred_affected, is_pred_affected) = 
    let t :((C_SD.edge_id_t * int),bool) Hashtbl.t = Hashtbl.create 101 in
    ((fun e p -> Hashtbl.replace t (e,p) true),
     (fun e p -> Hashtbl.mem t (e,p)))
    
(****************************************************************************)
(******************** Add Predicates / Refine *******************************)
(****************************************************************************)

  let (block_a_t_reset, is_fun_refined, set_fun_refined) = 
    let t = Hashtbl.create 31 in
    ((fun () -> refined_flag := false;Hashtbl.clear t),
     (Hashtbl.mem t),(fun f -> Hashtbl.replace t f ()))
  
  let addPred p =
    let p = P.canonicize p in
    match p with
      P.Atom (E.Lval (E.Symbol s)) when Misc.is_prefix bp_prefix s -> -1 | _ -> 
        try fst (getAtomIndex p) with _ ->
          let rv = add_new_predicate p in  
          let _ = List.iter update_imp_lv_table (P.allVarExps_deep p);
          M.msg_string M.Normal (Printf.sprintf "addP: %d: %s" rv (P.toString p)) in
          rv

  let add_pred_to_scope i =
    let lvs = P.lvals_of_predicate (getPred i) in
    let fs = Misc.sort_and_compact (Misc.flap C_SD.scope_of_lval lvs) in
    let fis = Misc.map_partial (fun f -> try Some (C_SD.get_fname_id f) with _ -> None) fs in
    ignore (List.map (fun fi -> Misc.hashtbl_check_update funPredt fi i) fis);
    if List.mem "" fs then Some i else None

  let add_local_preds (x,y) ps = 
    M.msg_string M.Debug (Printf.sprintf "Add local pred: loc_id (%d, %d): %s" x y 
    (Misc.string_of_int_list ps)); 
    let loc_ps = try Hashtbl.find locPredt (x,y) with Not_found -> [] in
    let fun_ps = try Hashtbl.find funPredt x with Not_found -> [] in
    let loc_ps' = Misc.sort_and_compact (loc_ps @ ps) in
    let fun_ps' = Misc.sort_and_compact (fun_ps @ ps) in
    let _ = Hashtbl.replace locPredt (x,y) loc_ps' in
    let _ = Hashtbl.replace funPredt x fun_ps' in
    (if (List.length loc_ps < List.length loc_ps') then refined_flag := true);
    (if (List.length fun_ps < List.length fun_ps') then
      let l = C_SD.lookup_location_from_loc_coords (x,y) in
      set_fun_refined (C_SD.get_location_fname l))

  let add_global_preds ps = 
    glbPreds := Misc.sort_and_compact (ps @ !glbPreds)

