(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
(**
 * This module defines a symbolic representation for C programs, equipped
 * with boolean operations.
 *)

module M = Message
module O = Options
module E = Ast.Expression
module P = Ast.Predicate
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command

module Defs = 
  struct
    exception EmptyPost
    exception RecursionException
    exception NoNewPredicatesException
    exception RefineFunctionException of string
    exception UnskipException of int 
  end

(* move to C_SD *)
module Operation =
struct
  type t = C_SD.edge
  
  let print = C_SD.print_edge

  type info =
    Normal
  | Call of P.predicate * E.lval option * string * E.expression list
  | Ret of E.expression 

  let get_info edge =
    let _get_info_fce (p,fce) =
      if not (C_SD.enter_call edge) then Normal else
        match fce with
	  E.FunctionCall (E.Lval (E.Symbol n), es) -> Call (p, None, n, es) 
        | E.Assignment (E.Assign, t, E.FunctionCall (E.Lval (E.Symbol n), es)) ->
            Call (P.True, Some t, n, es) 
        | _ -> assert false in 
      match (C_SD.edge_to_command edge).Cmd.code with
          Cmd.Block [Cmd.Return e] -> Ret e
        | Cmd.FunctionCall e -> _get_info_fce (P.True,e) 
        | Cmd.GuardedFunctionCall (p,e) -> _get_info_fce (p,e) 
        | _ -> Normal
	  
   let paren_fun op = 
      match get_info op with Call _ -> "(" | Ret _  -> ")" | _ -> ""

   let loc_to_int loc =
     match C_SD.get_source_position loc with None -> -1 | Some(_,ln,_) -> ln 

   let fcexp_to_string fce = 
     let fcs (f,xs) = 
       let s = 
         try match f with E.Lval (E.Symbol s) -> 
           (match C_SD.lookup_formals s with C_SD.Fixed l -> String.concat "," 
              (List.map2 (fun f a -> f^" = "^ (E.toString a)) l xs))
         with _ -> String.concat "," (List.map E.toString xs) in
       (E.toString f)^"("^s^ ")" in   
     match fce with
      E.FunctionCall (f,xs) -> fcs (f,xs) 
    | E.Assignment (_, t, E.FunctionCall (f,xs)) -> (E.lvalToString t)^" = "^(fcs (f,xs))
    | _ -> failwith "error: fcexp_to_string"

   let to_string op =
     let l  = loc_to_int (C_SD.get_source op) in
     let l' = loc_to_int (C_SD.get_target op) in 
     let cmd_s =
       match (C_SD.get_command op).Cmd.code with 
         Cmd.FunctionCall f -> "FunctionCall("^ (fcexp_to_string f) ^ ")"
       | _ -> Cmd.to_string (C_SD.get_command op) in
     Printf.sprintf "%d :: %s :: %d" l cmd_s l' 
	   
  let opFunName op = 
    match (C_SD.get_command op).Cmd.code with Cmd.FunctionCall fce 
        -> Misc.fst3 (C_SD.deconstructFunCall fce)
    | _ ->  failwith "error: opFunName"

  end
