(*  
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module O = Options
module M = Message
module E = Ast.Expression
module P = Ast.Predicate
module AA = AliasAnalyzer
module Op = Abs_op.Operation
module U = Abs_util
module C_SD = BlastCSystemDescr.C_System_Descr

module LvalMap = Map.Make(struct
  type t = E.lval
  let compare lv1 lv2 = compare (E.lvalToString lv1) (E.lvalToString lv2)
end)

let lvm_cf_flag () = 
  O.getValueOfBool "cf" || O.getValueOfBool "enablerecursion" 

let get_new_frame =
  let frame = ref 0 in
  (fun () -> (Misc.inc frame; !frame))

let filter_copy f src dst =
  LvalMap.fold (fun lv d m -> if f lv then LvalMap.add lv d m else m) src dst

class lvmap = 
  object (self)
    val mutable cur = LvalMap.empty
    val mutable hist = []
    val mutable frames = [0]
 
    method print_frames () =
      M.msg_string M.Debug ("Frames: "^(Misc.strList (List.map (string_of_int) frames)))

    method print () =
      LvalMap.fold 
      (fun lv e () ->
        M.msg_string M.Debug 
        (Printf.sprintf "%s ---> %s" (E.lvalToString lv) (E.toString e))) 
      cur ()
   
    method clear () = 
      cur <- LvalMap.empty;
      hist <- [];
      frames <- [0]

    method update lv = 
      let lv' = 
        if O.getValueOfBool "clock" then U.getNewTmpVar_clock lv 
        else U.getNewTmpVar lv in
      cur <- LvalMap.add lv lv' cur

    method mem lv = LvalMap.mem lv cur

    (* TBD:WHY? we need this to be deterministic ? is it to do with "clock" ? *)
    method private map_lv lv =
      if E.is_interpreted_lval lv then E.Lval lv else 
        try LvalMap.find lv cur with Not_found -> 
          let id =
            try (string_of_int (List.hd frames)) with _ -> failwith "BAD FRAMES!" in
          let (id,cand) = 
            if (O.getValueOfBool "timezero") then (id,[lv])
            else ((E.lvalToString lv)^id,lv::(AA.ac_get_lval_aliases true lv)) in
          let lvs = List.filter (fun lv' -> not (self#mem lv')) cand in
          let vals = List.map (fun lv' -> E.Chlval (lv',id)) lvs in
          let _ = cur <- List.fold_right2 LvalMap.add lvs vals cur in
          E.Chlval (lv,id) 

    method map_expr e = 
      E.address_normalize (E.deep_alpha_convert (self#map_lv) e)

    method map_pred p = 
      P.address_normalize (P.deep_alpha_convert (self#map_lv) p) 

    method push () =
      if lvm_cf_flag () then
        (hist <- cur::hist;
         frames <- (get_new_frame ())::frames;
         cur <- filter_copy C_SD.is_global cur LvalMap.empty)
   
    method pop () = 
      if lvm_cf_flag () then
        match (hist,frames) with (h::hist', _::frames') -> 
          (cur <- filter_copy C_SD.is_global cur h;
           hist <- hist';
           frames <- frames')
        | _ -> failwith "error: lvmap.pop"

    method init start ops =
      self#clear ();
      if (lvm_cf_flag ()) then
        let stack = 
          List.fold_left 
          (fun stk op -> 
            match Op.get_info op with
              Op.Ret _ -> (try List.tl stk with _ -> failwith "lvmap.init: mismatched call/ret")
            | Op.Normal -> stk 
            | Op.Call (_, _, f,_) when (C_SD.isAsyncCall op) -> stk
            | Op.Call (_, _, f,_) -> f :: stk) [start] ops in
        List.iter (fun _ -> self#push ()) stack 
end
