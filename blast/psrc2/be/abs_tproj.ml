(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module C = C_SD.Command
module R = Region.Region
module AA = AliasAnalyzer
module U = Abs_util
module E = Ast.Expression
module P = Ast.Predicate

(**************************************************************************)
(****************** Trace Projection [Path Slicing PLDI 05] ***************)
(**************************************************************************)

(* We attempt to find a subset of the operations of the trace that actually matter 
 * in some way. Clearly, if the resulting TF clauses are UNSAT, then the whole TF 
 * is unsat. Dually, we require that if the TF clauses are SAT, then, modulo 
 * termination assumptions, the whole thing is SAT. [Path Slicing : PLDI 05]
 *)

(* TBD : exception UnskipException of int *)
exception RefineFunctionException of string

(* The relevant set of LVALS *)
(* this is used as a global and is an implicit parameter to many of the functions below *)


let error_variable = E.Symbol (O.getValueOfString "errorvar")
let exit_variable = E.Symbol (O.getValueOfString "exitvar")

let rel_field_table : (string,bool) Hashtbl.t = Hashtbl.create 37
let rel_variable_table : (E.lval, bool) Hashtbl.t = Hashtbl.create 37
let rel_variable_size = ref 0


let add_rel_fields l = 
  (* INV: see invariant for flush_rel_fields *)
  let l_fields = (E.fields_of_lval l) in
    List.iter (fun fld -> 
		 let added = C_SD.add_field_to_unroll true fld in
                 if added then Hashtbl.replace rel_field_table fld true) l_fields
      (* TBD:FIELDS -- mixing tproj/image relevant fields *) 
 

let add_rel_variable l =
  if (Hashtbl.mem rel_variable_table l) then ()
  else
    begin
      M.msg_string M.Debug
	(Printf.sprintf "Adding relevant lval %s" (E.lvalToString l));
      Hashtbl.replace rel_variable_table l true;
      rel_variable_size := 1 + !rel_variable_size;
      add_rel_fields l
    end
  
let del_rel_variable l =
  if (Hashtbl.mem rel_variable_table l) then 
    begin
      M.msg_string M.Debug
	(Printf.sprintf "Removing relevant lval %s" (E.lvalToString l));
      Hashtbl.remove rel_variable_table l;
      rel_variable_size := !rel_variable_size - 1 
    end

let flush_relevant_locals fname = 
  let del lv _ = 
        let sc = C_SD.scope_of_lval lv in
        if List.mem fname sc then (del_rel_variable lv)
  in
  Hashtbl.iter del rel_variable_table

let rel_variable_reset () =
  Hashtbl.clear rel_variable_table;
  Hashtbl.clear rel_field_table;
  rel_variable_size := 0;
  add_rel_variable error_variable;
  if (O.getValueOfBool "exit") then add_rel_variable exit_variable;
  ()


let hit_check_cache = Hashtbl.create 1001
let sub_lval_cache = Hashtbl.create 1001

(* alas, we have to take closure here. 
   take the closure of lv_wr ... for each item, 
   check if it is aliased to lv_rd -- 
   such a fellow is lv_rd' == lv_wr^i s.t. lv_rd' aliases lv_rd *)

(* CHECK: hmm is caching ok in the presence of changing closures ? 
   I believe so because if you cached with lv_rd, 
   then the result computed at that point contains all relevant fields of lv_rd.
   I can construct pathological situations where the aliasing happens via 
   some field NOT in lv_rd in which case you get the wrong
   answer -- but that can happen without caching as well :) *)
let stats_nb_tproj_alias_queries = ref 0
        
let hit_check aliasfn lv_wr lv_rd =
  M.msg_string M.Debug 
        (Printf.sprintf "In Hit check (lv_wr = %s ,lv_rd = %s)"        
        (E.lvalToString lv_wr) (E.lvalToString lv_rd));
  let in_cache = Hashtbl.mem hit_check_cache (lv_wr,lv_rd) in
  if (in_cache) then Hashtbl.find hit_check_cache (lv_wr,lv_rd)
  else
    begin
      let clos = C_SD.lvalue_closure_stamp true lv_wr in (* caching done inside C_SD *)
      M.msg_string M.Debug (Printf.sprintf "Closure lv_wr = %s"
      (Misc.strList (List.map E.lvalToString clos)));
      let get_res () = 
	try
	  Some
	    ((List.find 
		(fun lvwri -> 
		   let (e1,e2) = ((E.Lval lvwri),(E.Lval lv_rd)) in
		     if e1 = e2 then true else
		       begin 
                               stats_nb_tproj_alias_queries := 1 +
                               !stats_nb_tproj_alias_queries;
                               U.queryAlias_cache e1 e2
                       end      
                   ))
	       clos)
	with Not_found ->
          if E.occurs_check (E.Lval lv_wr) (E.Lval lv_rd) 
          then Some lv_rd else None
      in
      let get_res_aliasfn () = (* throwing this back in for time comparisons *)
        let get_aliases () = aliasfn lv_rd in
        let rd_aliases = Stats.time "get aliases aliasfn" get_aliases () in
        (* RJ: this is cached inside aliasfn -- so we don't cache again *)
        let a_table = Misc.hashtbl_of_list  7 rd_aliases in
        try 
           Some (List.find (Hashtbl.mem a_table) clos)
        with Not_found -> None
      in                
      let res = 
              if (O.getValueOfBool "aiter") then 
                      Stats.time "get res aliasfn" get_res_aliasfn () 
              else    
                      Stats.time "get res" get_res () 
      in
	Hashtbl.replace hit_check_cache (lv_wr,lv_rd) res;
        let lvos lvo = match lvo with None -> "None" | Some lv ->
          E.lvalToString lv in
         M.msg_string M.Debug 
        (Printf.sprintf "Hit check (lv_wr = %s ,lv_rd = %s,res = %s)"        
        (E.lvalToString lv_wr) (E.lvalToString lv_rd) (lvos
        res));
       res
    end

let gen_is_relevant aliasfn l = 
  let _mapper lv_read _ = 
    match hit_check aliasfn l lv_read with
	None -> None
      | Some lv_read' -> Some (lv_read,lv_read')
  in
    Misc.hashtbl_map_partial _mapper rel_variable_table

let max_relevant_size = ref 0

let dump_relevant () =
  let lvs = (Misc.hashtbl_keys rel_variable_table) in
  let slvs = List.map (fun lv -> E.lvals_of_expression (E.Lval
  lv)) lvs
  in 
  let rv = List.flatten slvs in
  if (List.length rv > !max_relevant_size) then
    begin
      max_relevant_size := List.length rv;
      M.msg_string M.Normal ("Max rel-cone size"^(string_of_int
      !max_relevant_size))
    end; 
  rv
  
let mods_on_path_table = Hashtbl.create 101
let mods_on_path_reset () = Hashtbl.clear mods_on_path_table

(* when you add a new "relevant" lv means you are taking the op, 
 * which means you are resetting the mods_on_path table. 
 * Hence, you ONLY do the add_and_hit_check when you are 
 * adding a mod_on_path *)

let add_and_hit_check aliasfn mod_lv =
  if Hashtbl.mem mods_on_path_table mod_lv then false
  else
    begin
      Hashtbl.replace mods_on_path_table mod_lv true;
      let find_rel = Stats.time "gen is relevant" (gen_is_relevant aliasfn) mod_lv in 
      (find_rel <> [])
    end	


(* NOTES on proc_write:
   when  you do an assign lv = e,
   then we cal gen_is_relevant to get a list:
   [ ... (lv_rd,lv_rd')...] where lv_rd is "read" in the future and thus  
   is relevant now, and lv_rd' == lv^i (in the closure of lv) such that lv_rd' aliases lv_rd.
   then, corresponding to this "hit", the new "read" value (add) is e^i,
   which is lv^i[e/lv] which is exactly e^i ... which is the new "value" of lv_rd,
   and "lv_rd" is itself irrelevant now (killed) and hence put in del.
   if lv_rd' == lv_rd then we can definitely delete lv_rd from the relevant set, 
   and replace with e^i otherwise not. We must do the above for all lv' that are 
   must aliases of lv (including lv)
*)
		       
let rec process_asgn_block aliasfn fname a_block add_c del_c take =
  (* first, lets put add_c/del_c in the table etc -- so we dont have to fuss with them when checking relevant etc. *)
  let aliasfn_f = aliasfn fname in    
  let _ = 
  List.iter del_rel_variable del_c; 
    List.iter add_rel_variable add_c 
in      
  let proc_write (lv,e) =
    (* returns a 4-tuple. (must_add, must_del, may_add,may_del) *)
    let (must_add,must_del,may_add,may_del) = 
      (ref [],ref [],ref [],ref []) 
    in
    let must_aliases_lv = lv::(AA.get_all_must_aliases lv) in    
    let get_hit_list () = 
      Misc.map_partial 
	(fun lv' -> 
	   let lv'_hits = gen_is_relevant aliasfn_f lv' in
	     if lv'_hits = [] then None else Some (lv',lv'_hits))
	must_aliases_lv
    in
    let hit_list = Stats.time "get hit list" get_hit_list () in
    let proc_hit lv' (lv_rd,lv_rd') = 
      let spairs = [((E.Lval lv'),e)] in
      let update al_ref dl_ref =
	let subs_lvals = 
	  E.lvals_of_expression 
	    (E.substituteExpression spairs (E.Lval lv_rd')) 
	    (* we compute e^i by subst. lv_rd' == lv' ^i using [e/lv'] *) 
	in
        M.msg_string M.Debug (Printf.sprintf "Subs (lv' , e) in lv_rd' : %s , %s, %s " 
        (E.lvalToString lv') (E.toString e)
        (E.lvalToString lv_rd'));
        M.msg_string M.Debug (Printf.sprintf "subs_lvals: %s"
        (Misc.strList (List.map E.lvalToString subs_lvals)));
	  al_ref :=  subs_lvals @ !al_ref;
	  dl_ref := lv_rd :: !dl_ref
      in
	(* INV: its a MUST hit if lv_rd == lv_rd', o.w. its a may hit *)
	if (lv_rd' = lv_rd) then
	  update must_add must_del
	else update may_add may_del
    in
    let process_all_hits () = 
      List.iter (fun (lv',hits) -> List.iter (proc_hit lv') hits) hit_list
    in
    let _ = Stats.time "process all hits" process_all_hits () in
      (!must_add,!must_del,!may_add,!may_del)
  in
    match a_block with
	(target,value)::tl ->
	  begin
	    let _ = M.msg_string M.Debug
		      (Printf.sprintf "Hit assign %s := %s"
			 (E.lvalToString target)
			 (E.toString value))
	    in
	    let (tadd,tdel,alias_add,alias_del) = 
	      Stats.time "proc write targ" proc_write (target,value) 
	    in
	      (* (alias_del) = [] empty in the worst case, use it only to see if this op was relevant,ie to compute "take" *)
	    let read = Misc.sort_and_compact (tadd @ alias_add) in
	    let written = (target::tdel) in
	    let take' = ((tdel@alias_del) <> []) in 
	      (* take' == true IFF some update made was relevant. if some update was relevant,
		then tdel@alias_del <> [], as every relevant update sends something into the del bin *)
	      (* OLD CODE: let written = [target] (* TBD:SOUNDNESS *) in
	            let read = E.allVarExps_deep value in
	         TBD:TPROJ -- do we really need allVarExps_deep ? *)
	    let (add_c',del_c') =
	      if take' then
		(Misc.union (Misc.difference add_c  written)  read,
		 Misc.difference (Misc.union del_c  written)  read)
	      else
		(add_c,del_c)
	    in
	      process_asgn_block aliasfn fname tl add_c' del_c' (take || take')
	  end
      | [] -> ((add_c,del_c),take)
	        
(* unsat_flag == true means we just want to gather the "residual" funcalls *)	  
let process_trace_op unsat_flag aliasfn op_array left last (i : int) =
  assert (0 <= i && i < Array.length op_array);
  (* INV: the two locations MUST be in the same CFA *)
  let level = M.Debug in
  let _ = M.msg_string M.Normal ("TPROJ: at op "^(string_of_int i)) in
  let op = try op_array.(i) with _ -> failwith "HERE1" in
  let call_i = left i (* may be -1: if the current loc i is in main *) in
  let op_fname = C_SD.get_location_fname (C_SD.get_source op) in
  let last_op = op_array.(last) in
  M.msg_string M.Debug ("proc_trace_op:"^(C_SD.Command.to_string (C_SD.get_command op))); 
  let process_return callsite_op call_triple caller_name retexp = 
    let (targ_option,fname,args) = call_triple in
    let cb_asgns =
      let fmls =
	match C_SD.lookup_formals fname with
	    C_SD.Variable l -> l
	  | C_SD.Fixed l -> l in
      copy_back_asgns (O.getValueOfBool "incref") (Misc.partial_combine fmls args) fname in
    let all_asgns = 
      match targ_option with
	  None -> cb_asgns
	| Some(targ) -> (targ,retexp)::cb_asgns in
    let ((add,del),ret_take) = process_asgn_block aliasfn caller_name all_asgns [] [] false in
    let _ = M.msg_string M.Debug "Now check glob_take" in
    let get_glob_take () = 
      if (O.getValueOfBool "dmod") 
      then 
        let (glob_lvals,ng_lvals) = Misc.filter_cut C_SD.is_global (dump_relevant ()) in
        let _ = M.msg_string M.Normal "check mod_on_edge" in
        (List.exists (C_SD.local_mod_on_edge callsite_op) ng_lvals
        || List.exists (C_SD.global_mod_on_call fname) glob_lvals)
      else
          (let globals_mod = (C_SD.global_lvals_modified fname) in (* closed under alias *) 
           List.exists (fun lv -> (gen_is_relevant (aliasfn "") lv) <> []) globals_mod) in
    let _ = M.msg_string M.Debug "done checking glob_take" in
    let glob_take = Stats.time "get glob take" get_glob_take () in
    let take = glob_take || ret_take in
    let i' = if take then i-1 else (call_i - 1) in	      
    ((add,del),take,i') in
    
  (* if fname is a skipped function, then returns the original function skipped, otherwise None *)    
  let unskip fname =
    try
      Some(Misc.chop_after_prefix __SKIPFunctionName fname) 
    with _ -> None
  
  let skipcheck (targ_option,fname,args) =
    M.msg_string M.Debug "in skipcheck" ;
    if (not (O.getValueOfBool "skipfun") || (O.getValueOfBool "skipcheck" = false)) then ()
    else
	match unskip fname with None -> ()
        | Some(orig_fname) ->
            if (not (C_SD.is_defined orig_fname)) then ()
            else
	      let dummyretexp = U.get_new_unknown orig_fname in
	      let (_,take,_) = process_return op  (targ_option,orig_fname,args) op_fname dummyretexp in
	      if take then 
                 (M.msg_string M.Normal ("Whoops: skipped --need to take: "^orig_fname);
                  if (Misc.is_substring orig_fname "__initialize__" && not (O.getValueOfBool "initialize")) 
                  then raise (UnskipException (-1));
                  U.add_take_fun orig_fname; 
                  raise (UnskipException (i-1)))
              else (M.msg_string M.Debug ("Can still skip"^orig_fname)) in
   match Operation.get_info op with
     Operation.Ret retexp ->
	  begin
	    if (unsat_flag) then (([],[]),false,call_i - 1)
	    else
	      let call_op = op_array.(call_i) in
  	      match Operation.get_info call_op with
		  Operation.Call (_, targ_option,fname,args) -> 
		    process_return call_op (targ_option,fname,args) (C_SD.get_location_fname (C_SD.get_source call_op))
		    retexp
		| _ -> failwith "bad call-op in process_trace_op"
	  end
      | Operation.Call (_, targ,fname,args) ->
	  (* note: as we are actually processing the call, we have to take it *)
	  begin
            if (unsat_flag) then (([],[]),true,i-1)
	    else
	    let ac_formals =
	      List.map (fun s -> (E.Symbol s))
		(match C_SD.lookup_formals fname with
		     C_SD.Variable fml -> fml
		   | C_SD.Fixed fml -> fml)
	    in
	    let asgn_list = Misc.partial_combine ac_formals args in
	      (* INV: List.length.formals = List.length args *)
	    let ((add,del),_) = process_asgn_block aliasfn fname asgn_list [] [] false in
	      (* NOTE: that we have used fname -- the callee_name NOT the op_fname here *)
            let _ = flush_relevant_locals fname in
            if (add = [] && !rel_variable_size = 0) then
	      begin
		M.msg_string M.Error ("CONE EMPTY! -- probably an ERROR:"^fname);
		M.msg_string M.Normal ("CONE EMPTY! -- probably an ERROR:"^fname);
		if (O.getValueOfBool "tpbug") then (failwith "CONE EMPTY UNSAFE ? :-(")
	      end;
            let ((add,del), take, next_i) =
              let ac_call_i = left (i-1) in  
              let current_loc = C_SD.get_source op in
              let current_fname = C_SD.get_location_fname current_loc in
              if (O.getValueOfBool "projfun" && ac_call_i <> -1  && not (O.is_errorseed current_fname))
              then
              begin
                let _ = 
                  List.iter (del_rel_variable) del;
                  List.iter (add_rel_variable) add;
                in      
                let last_loc = current_loc in 
                let this_loc = C_SD.lookup_entry_location current_fname in  
                let rel_lvals = dump_relevant () in
                let _ = M.msg_string M.Normal 
                "dmod: projf: check may_be_modif" in
                let cant_jump = Stats.time "projf : may be modified" (C_SD.may_be_modified rel_lvals this_loc) last_loc  in
                if (not cant_jump) then (M.msg_string M.Normal
                ("TP: Jumping!:"^(string_of_int i)));
                (([],[]),true,if cant_jump then (i-1) else ac_call_i)
              end
            else ((add,del), true, i-1) in
            ((add,del),take,next_i)
	  end
      | Operation.Normal -> (* split cases on whether its an assign or an assume *)
	  begin
	    if (unsat_flag) then (([],[]),false,i - 1)
	    else
	      match (C_SD.get_command op).C_SD.Command.code with
		C_SD.Command.Block l ->
		  let peel_stm stm =
		    match stm with
			C_SD.Command.Expr (E.Assignment
			  (E.Assign, targ,valu)) -> (targ,valu)
		      | _ -> failwith "Bad stm in peel_stm!"
		  in
		  let ((add,del),take) =
		    process_asgn_block aliasfn op_fname
		      (List.rev (List.map peel_stm l)) [] [] false
		  in
		    ((add,del),take,i-1)
	      | C_SD.Command.Pred p ->
		  begin
		    let this_loc = C_SD.get_source op in
		    let last_loc = C_SD.get_source last_op in
                    let hit_checker = (add_and_hit_check (aliasfn op_fname)) in
                    let this_pred_lvals = P.lvals_of_predicate p in
                    let this_pred_relevant = List.exists hit_checker this_pred_lvals in
                    let path_mods_relevant = 
                      if (O.getValueOfBool "dmod") 
                      then
                         begin
                          let rel_lvals = dump_relevant () in
                          let _ = M.msg_string M.Normal 
                          "dmod: check may_be_modif" in
                          Stats.time "may be modified" (C_SD.may_be_modified rel_lvals this_loc) last_loc                         
                        end
                      else
                        let mods_between =  Stats.time "mods_on_path" (C_SD.mods_on_path this_loc) last_loc in
                        Stats.time "add and check hits" (List.exists hit_checker) mods_between in 
                    (* note: ok to STOP at first "hit", ie use List.exists, because the set will be reset 
                     * if the exists is true *)
                    let not_always_reach = (not (Stats.time "Always Reach" (C_SD.always_reach this_loc) last_loc)) in 
                    let take = (i+1 = Array.length op_array) 
                               || not_always_reach || this_pred_relevant || path_mods_relevant in
		    let (add,del) = ((if take then this_pred_lvals else []), []) in
		      ((add,del),take,i-1)
		  end
	      | C_SD.Command.Skip -> (([],[]),false,i-1)
              | C_SD.Command.SymbolicHook _ -> (([],[]),(not unsat_flag),i-1)
              | C_SD.Command.FunctionCall e -> 
                  begin
		    match e with
			E.FunctionCall (fn, el) ->
			  let fname = C_SD.get_fname fn in
                          (skipcheck  (None,fname,el)); 
			    (* skipcheck will throw an exception to be caught in block_analyze_trace if reqd.,
			       AFTER changing the various tables etc.*)
			  M.msg_string M.Normal ("Unknown fun "^fname^" in trace_project : optimistic");
			    (([],[]),false,i-1)
		      | E.Assignment
			  (E.Assign, targ, E.FunctionCall(fn, el)) ->
			  let fname = C_SD.get_fname fn in
			  let _ = skipcheck  (Some(targ),fname,el) in
                          let _ = M.msg_string M.Debug "after skch2" in
                          M.msg_string M.Normal ("Unknown fun "^fname^" in trace_project : optimistic");
			  let asgn_block = [(targ,U.get_new_unknown fname)] in
			  let ((_,del),take) = process_asgn_block aliasfn op_fname asgn_block [] [] false in
			    (([],del),take,i-1)
		  end
	      | _ -> failwith ("unexpected value in process_trace_op")
	  end

let trace_project auco op_l reg_l =
  let aliasfn =
    if (O.getValueOfBool "cf") then AA.get_lval_aliases_scope
    else if (O.getValueOfBool "talias") then  (fun _ -> trace_lv_table_get_lval_aliases)
    else (fun _ -> get_lval_aliases) in
  let trace_size = List.length op_l in
  M.msg_string M.Major ("Trace Project: IN size "^(string_of_int trace_size));
  M.msg_string M.Error ("Trace Project: IN size "^(string_of_int trace_size));
  U.print_trace_reg M.Normal op_l (List.tl reg_l) ;
  let op_array = Array.of_list op_l in
  let reg_array = Array.of_list reg_l in
  let _ = M.msg_string M.Debug "Building tp_array" in
  let _ = block_reset () in
  let _ = Stats.time "make_lv_stack [tproj]" (make_lv_stack (R.get_function_name (List.hd reg_l))) op_l in 
    let (left,_) = Misc.paren_match Operation.paren_fun op_array in
  let tp_array = Array.make trace_size false in
  let _ = rel_variable_reset () in
  let last = ref (trace_size - 1) in	      
  let unsat_flag = ref false in
  (* now a strange way to do a decreasing for-loop in OCAML *)
  let i = ref (trace_size - 1) in
  let _ =
    while (!i >= 0) do
      Ast.check_time_out () ;
      let ((add,del),take,i') =
        Stats.time "process_trace_op" (process_trace_op !unsat_flag aliasfn op_array left (!last)) (!i) in
      Array.set tp_array !i take;
      if (take) then (last := !i; mods_on_path_reset ());
      i := i'; 
      List.iter del_rel_variable del;
      List.iter add_rel_variable add; 
      if take && (not !unsat_flag) && not (O.getValueOfInt "predH" >= 6) 
      then unsat_flag := auco !last
    done in
  let _ = TP.block_reset () in (* dont forget to reset the thmprover *)
  let _ = M.msg_string M.Debug "tp_array built" in
  let proj_size = Misc.array_counti (fun i a -> a) tp_array in
  let _ = M.msg_string M.Error ("Trace Project: OUT size "^(string_of_int (proj_size))) in
  let _ = M.msg_string M.Major ("Trace Project: OUT size "^(string_of_int (proj_size))) in
  let fwd_map_array = Array.create trace_size (-1) in
  let rev_map_array = Array.create proj_size (-1) in
  let idx = ref 0 in
  for i = 0 to trace_size - 1 do
    if tp_array.(i) then
      (Array.set fwd_map_array i (!idx);
       Array.set rev_map_array (!idx) i;
       idx := !idx + 1);
  done;
  assert (!idx = proj_size);
  let revmap = (fun i -> if (0 <= i) && (i < proj_size) then rev_map_array.(i) else -1) in
  let fwdmap = (fun i -> if (0 <= i) && (i < trace_size) then fwd_map_array.(i) else -1) in
  let mask_fn = fun i -> if 0 <= i && i < trace_size then tp_array.(i) else false in
  let op_l' = Misc.list_mask mask_fn op_l in
  let pre_reg_l = Misc.list_mask mask_fn reg_l in
  let post_reg_l = Misc.list_mask (fun i -> mask_fn (i-1)) reg_l in
  M.msg_string M.Debug "Out Trace";
  U.print_trace op_l';
  (pre_reg_l,post_reg_l,op_l',revmap,fwdmap)



