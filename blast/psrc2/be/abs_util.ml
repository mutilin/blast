
(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)


module O = Options
module M = Message
module S = Bstats
module Constant = Ast.Constant
module E = Ast.Expression
module P = Ast.Predicate
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = C_SD.Command
module AA = AliasAnalyzer
module PT = PredTable
module TP = TheoremProver
module Op = Abs_op.Operation
module R = Abs_region.Region
module LvSet = Set.Make (struct type t = E.lval let compare x y = compare x y end) 
	
(************************************************************************)
(********************** theorem prover cache ****************************)
(************************************************************************)
(* move to theoremProver.ml *)
    
  let theoremProverCache : (string , bool) Hashtbl.t = Hashtbl.create 49033
  let statTotalNumberOfQueries = ref 1
  let statPostBddCalls = ref 0
  let statNumPost = ref 0
  let statReachedCubes = ref 0
  let stats_nb_non_trivial_functions = ref 0

  let (statNewVisitedFun,statAllVisitedFuns) = 
    let t : (string, unit) Hashtbl.t  = Hashtbl.create 109 in
    ((fun fn -> Hashtbl.replace t fn ()),(fun () -> Misc.hashtbl_keys t))

  (* API *)
  let reset_abs_stats () = 
    statTotalNumberOfQueries := 1;
    statReachedCubes := 0;
    statNumPost := 0;
    ()

  (* API *)
  let askTheoremProver p =
    statTotalNumberOfQueries := !statTotalNumberOfQueries + 1;
    Misc.do_memo theoremProverCache TP.queryExp p (P.toString p)
 
  (* API *)
  let askTheoremProverContext p q = 
    statTotalNumberOfQueries := !statTotalNumberOfQueries + 1;
    Misc.do_memo theoremProverCache (TP.queryExpContext p) q (P.toString p) 
  
  (* API *)
  let isSatisfiable (b,ps) =
    Misc.do_catch "isSatisfiable" TP.sat (P.And ((PT.convertBddToPred b)::ps))

(**************************************************************)
(**************** new temporary variable names ****************)
(**************************************************************)
 
  (* API *)    
  let getNewTmpVar = 
    let i = ref (-1) in
    fun lv -> E.Chlval (lv, string_of_int (Misc.inc i; !i))
 
  (* API *)
  let (clock_tick, getNewTmpVar_clock) =  
    let clock = ref 0 in
    ((fun () -> Misc.inc clock; !clock),
     (fun lv -> E.Chlval (lv, string_of_int !clock)))
 
  (* API *)
  let get_new_unknown fname = 
    getNewTmpVar (E.Symbol ("BLAST_UNKNOWN@"^fname))

(******************************************************************)
(*************************** aliasing *****************************)
(******************************************************************)
    
  let get_scoped_aliases lv =
    let aliasfn = 
      if O.getValueOfBool "cf" then AA.get_lval_aliases_scope
      else (fun fn -> AA.get_lval_aliases) in
    let scopes = Misc.sort_and_compact (C_SD.scope_of_lval lv) in
    Misc.sort_and_compact (Misc.flap (fun fn -> aliasfn fn lv) scopes)

  let getVarsAndDerefs =
    let memo = Hashtbl.create 11 in
    fun p -> 
      Misc.do_memo memo 
      (fun () -> Misc.map_partial 
                 (fun e -> match e with E.Lval _ | E.Chlval _ -> Some e 
                  | E.Unary(E.Address, _) -> None
                  | _ -> Misc.error "getVarsAndDerefs")
                 (P.allVarExps p))
      () (P.toString p)

  let queryAlias_iter_cache = 
    let memo = Hashtbl.create 1001 in  
    fun ew er ->
      match (ew,er) with
      (E.Lval lvw,E.Lval lvr) ->
        LvSet.mem lvw
        (Misc.do_memo memo
          (fun () -> 
            let lvrs = get_scoped_aliases lvr in
            List.fold_left (fun s e -> LvSet.add e s) LvSet.empty lvrs) 
          () (E.lvalToString lvr))
      | _ -> false

  let _predAliases aa lv p =
    if (O.getValueOfString "alias" = "") then [] else 
      let xs = getVarsAndDerefs p in
      let xs' = if E.isDeref (E.Lval lv) then xs else List.filter E.isDeref xs in
      S.time "filter alias candidates" (List.filter (aa (E.Lval lv))) xs' 

  (* API *)  
  let queryAlias_cache =
    let memo = Hashtbl.create 1009 in
    fun x y -> 
      Misc.do_memo memo (fun () -> AA.queryAlias x y) 
      () (E.toString x, E.toString y)

  (* API *)    
  let getPossibleAliases =
    _predAliases 
    (if (O.getValueOfBool "aiter") then queryAlias_iter_cache else queryAlias_cache)
 
  (* API *)
  let getMustAliases = _predAliases (AA.is_must_alias) 
      

(***************************************************************************)
(*************** function calls, parameters, renaming **********************)
(***************************************************************************)

  let (add_relevant_symvar, get_relevant_symvar, all_symvars) = 
    let t = Hashtbl.create 101 in
    ((fun f symv ->
       let lv = E.peel_symbolic symv in
       if not (C_SD.is_struct lv) then ignore(Misc.hashtbl_check_update t f symv)),
     (fun f -> try Hashtbl.find t f with Not_found -> []),
     (fun () -> Misc.hashtbl_to_list t))

  let sym_hooks_funcall fn targ_sym_flag glvm_f_o = 
    let mk_sym flag symv = if flag then symv else (E.peel_symbolic symv) in
    if not (O.getValueOfBool "cf" && O.getValueOfBool "incmod") then [] else
      let syvs = 
        if targ_sym_flag then get_relevant_symvar fn else
          let glvs = C_SD.global_lvals_modified fn in
          let glvs' = match glvm_f_o with Some f -> List.filter f glvs | None -> glvs in 
          List.map (E.make_symvar fn) glvs' in
      List.map (fun x -> (mk_sym true x, E.Lval (mk_sym false x))) syvs 

  (* API *)
  let asgn_of_funcall (targ_sym_flag,_) glvm_f_o fce = 
    let (fn,subs,t) = C_SD.deconstructFunCall fce in
    let asgns = 
      if (C_SD.is_defined fn || t = C_SD.junkRet) then
	(List.map (fun (x,e) -> (E.Symbol x,e)) subs) @
        (if C_SD.is_noexpand fn then [(t,fce)] else [])
      else [(t,(get_new_unknown fn))] in
    asgns @ (sym_hooks_funcall fn targ_sym_flag glvm_f_o)
  
(* should only be called with lv which can possible be modified 
 * by the call, i.e. lv is either a global or a formal lvalue 
 * says if a DEREF of lv is modified -- not if lv itself is!
 * useful only if a local parameter has been passed to it *)
	
  let is_smashed_fname fn =
    let mods = (C_SD.formal_lvals_modified fn) @ (C_SD.global_lvals_modified fn) in
    fun lvs -> Misc.nonempty_intersect compare lvs mods 
            
  let return_tweak callee is_smashed sym_flag lv =
    if (not (O.getValueOfBool "modopt")) || (is_smashed [lv] && sym_flag) then
      E.make_symvar callee lv
    else lv

  (* lv is unique to each callee_name, so we can hash on it 
   * 1. do we need to copy back.
   * 2. if so, is lv itself modified ? *)

  let mk_deref x = E.Lval (E.Dereference (E.Lval x))
  
  let modified_by_call =
    let memo = Hashtbl.create 101 in
    fun callee sym_flag x -> 
      Misc.do_memo memo 
      (fun () -> 
        let mods = C_SD.formal_lvals_modified callee in
        let x' = mk_deref x in
        (List.exists (fun lv -> E.occurs_check x' (E.Lval lv)) mods,
         if (List.mem x mods) && sym_flag then E.make_symvar callee x else x))
      () (sym_flag,E.lvalToString x)

  (* API *)
  let copy_back_asgns sym_flag param_subs callee  =
    if not (O.getValueOfBool "cf") then [] else 
      Misc.map_partial 
        (fun (x,e) ->
          let (b,x') = modified_by_call callee sym_flag (E.Symbol x) in
          if b then Some (E.push_deref (E.Dereference e), mk_deref x') else None) 
        param_subs

  (* API *)
  let asgn_of_return (f,subs,t,e) =
    let cba = copy_back_asgns true subs f in
    if t = C_SD.junkRet then cba else ((t,e)::cba) 

  (* API *)
  let getRetexp op =
    match (C_SD.get_command op).Cmd.code with 
    Cmd.Block [(Cmd.Return e)] -> e

  (* API *)  
  let get_ingoing_call loc = 
    match C_SD.get_ingoing_edges loc with [op] -> 
      match C_SD.call_on_edge op with Some (_,e) -> e

  (* API *)
  let asgns_of_stmt l' s = 
    try 
      (match s with 
        Cmd.Expr (E.Assignment(E.Assign,x,e)) -> Some (x,e) 
      | Cmd.Return e -> 
          let (_,_,x) = C_SD.deconstructFunCall (get_ingoing_call l')  in 
          if x = C_SD.junkRet then None else (Some (x,e)))
    with _ -> failwith "error: asgns_of_stmt"
    
    
(*************************************************************************)
(*************** interpolants/predicate discovery ************************)
(*************************************************************************)

  let isTemporary s =
    Misc.is_digit (String.get s 0) || 
    String.contains s '%' 
  
  (* get back original varname from temporary name *)
   let _massage s = 
     let s' =
       if not (Misc.is_digit (String.get s 0)) then s
       else Str.string_after s (String.index s '_') in
     if not (String.contains s' '%') then s'
     else Str.string_before s' (String.index s '%')


(* TBD: REFACTOR -- can't this be done with E.deep_transform ? *)   
let rec massage_lval fs l =
  let me = massage fs in
  match l with
      | E.Symbol s -> E.Symbol (fs s)
      | E.This -> l
      | E.Access(op, e1, s) -> E.Access(op, me e1, s)
      |	E.Dereference e -> E.push_deref (E.Dereference (me e))
      |	E.Indexed (e1, e2) -> E.Indexed (me e1, me e2)

and massage fs e =
    let me = massage fs in
    match e with
	E.Lval l -> E.Lval (massage_lval fs l)
      |	E.Chlval (l,_) -> E.Lval (massage_lval fs l)
      |	E.Binary(op, e1, e2) -> E.Binary(op, me e1, me e2)
      | E.Unary(E.Address, e) -> E.addressOf (me e)
      | E.Unary(op, e) -> E.Unary(op, me e)
      | E.Constant _   -> e
      | E.Select(e1,e2) -> E.Select(me e1,me e2)
      | E.Store(e1,e2,e3) -> E.Store(me e1,me e2, me e3)
      | E.FunctionCall (E.Lval (E.Symbol _fnNm), args) -> 
          begin
            match _fnNm with
              "BitAnd" -> E.Binary(E.BitAnd, me (List.hd args), me (List.nth args 1))
            | "BitOr" -> E.Binary(E.BitOr, me (List.hd args),me (List.nth args 1))
            | "Xor" -> E.Binary(E.Xor, me (List.hd args), me (List.nth args 1))
            | "Div" -> E.Binary(E.Div, me (List.hd args), me (List.nth args 1))
            | "LShift" -> E.Binary(E.LShift, me (List.hd args), me (List.nth args 1))
            | "RShift" -> E.Binary(E.RShift, me (List.hd args), me (List.nth args 1))
            | "Rem" -> E.Binary(E.Rem, me (List.hd args), me (List.nth args 1))
            | "BitNot" -> E.Unary(E.BitNot, me (List.hd args))
            | "chlval" -> me (List.hd args)
            | "foffset" -> E.Binary(E.FieldOffset, me (List.hd args),me (List.nth args 1))
            | "sel4" ->
                 (* Rename sel4 to select, no? sel4 is a remnant of Necula's sel4 -- 
                  * but there we were keeping track of bytes for memory safety *)
                begin
                  if (List.length args <> 2) then 
                    failwith ("focus: sel4 with more than two args."^(E.toString e))
                  else
                    let _var = me (List.hd args) and _deref = List.nth args 1 in
                      match _deref with
                            E.Constant i -> E.Lval (E.Dereference _var)
                          | E.Lval (E.Symbol s) -> E.Lval (E.Access (E.Arrow, _var, _massage s))
                          | _ -> failwith "focus: unexpected use of sel4! Strange arguments."
                end
            
            | "select" -> (* case Dot *)
                begin
                  if (List.length args <> 2) then 
                    failwith ("focus: select with more than two args. "^(E.toString e))
                  else
                    let _var = (me (List.hd args)) and _deref = List.nth args 1 in
                          match _deref with
                            | E.Lval (E.Symbol s) -> 
				E.Lval (E.Access (E.Dot, _var, _massage s))
			    | E.Constant (Constant.Int _) ->
				E.Lval (E.Dereference _var)
                            | _ -> failwith ("focus: select used with strange args. (deref)"^(E.toString e))
                end
            
            | "addrOf" ->
                  if (List.length args <> 1) then
                    failwith ("focus: addrOf with more than one argument."^(E.toString e))
                  else
                    let _var = me (List.hd args) in 
                    E.Unary(E.Address, _var)
            | _ -> 
                if E.is_interpreted _fnNm then 
                  E.FunctionCall (E.Lval (E.Symbol _fnNm), List.map me args)
                else failwith ("focus: unexpected function name "^_fnNm)
          end
      | _ -> failwith "focus: Expression type not handled"
      
      
  let subOrig p =
      match p with
        P.Atom (E.Binary(E.Eq, E.Lval (E.Symbol s), e)) ->
           let (s,m) = ((if isTemporary s then _massage s else s), massage _massage) in
           P.Atom (E.Binary(E.Eq, E.Lval (E.Symbol s), m e))
      | P.Atom e -> P.Atom (massage _massage e)
      | _  -> Misc.error "do_focus: non-atomic predicate!"

  let expRels p = 
    match p with P.Atom (E.Binary(E.Eq,lhs,(E.Binary(op,_,_) as rhs))) when E.isRelOp op ->
      [P.Atom (E.Binary(E.Eq,lhs,E.Constant (Constant.Int 0)));
       P.Atom (E.Binary(E.Eq,lhs,E.Constant (Constant.Int 1)));
       P.Atom rhs]
      | _ -> [p] 

  let expSyms p =
    p::(Misc.flap
          (fun lv -> 
             let lv' = E.peel_symbolic lv in
             if lv' = lv then [] else (* lv is a symbolic constant *)
	     let eqp = P.expr_equate (E.Lval lv) (E.Lval lv') in
	     let subp = P.substitute [(E.Lval lv),(E.Lval lv')] p in
             [eqp;subp])   
          (P.allVarExps_deep p))
  
  (* API *) 
  let process_raw_foci_preds ps = 
    let ps = List.map P.normalize_interpreted ps in
    let ps = Misc.do_catch_ret "subOrig" (List.map subOrig) ps [] in
    let ps = Misc.flap expRels ps in  
    let ps = if (O.getValueOfBool "cf") then Misc.flap expSyms ps else ps in
    List.filter ((<>) (-1)) (List.map PT.addPred ps)

  (* API *)
  let extract_foci_preds arg = 
    process_raw_foci_preds (TP.extract_foci_preds arg)

  (* API *)
  let extract_foci_preds_cf (fn, pre_p) arg =
    let pre_ps = P.getAtoms pre_p in
    let is_symv e =
      (E.is_chlval e) &&
      List.exists (fun (P.Atom e') -> E.occurs_check e e') pre_ps in
    let tf e =
      if not (is_symv e) then e else
	let lv = E.lv_of_expr (E.replaceChlvals e) in
	let symlv = E.make_symvar fn lv in
	  (add_relevant_symvar fn symlv; E.Lval symlv) in
    let ps = Misc.flap P.getAtoms (TP.extract_foci_preds arg) in
    let ps = List.map (fun (P.Atom e) -> P.Atom (E.deep_transform_top tf e)) ps in
    process_raw_foci_preds ps

(***************************************************************************)
(******************* Miscellaneous Helper Routines *************************)
(***************************************************************************)
  let rec simplify_address_equality (a,a') =
    match (a,a') with
      (E.Binary(E.Plus, e1,e2),E.Binary(E.Plus, e1',e2')) -> 
        if e1 = e1' then (e2,e2') else (a,a')
    | (E.Binary (E.FieldOffset,e1,f1), E.Binary (E.FieldOffset,e2,f2))->
        if f1 = f2 then simplify_address_equality (e1,e2) else (a,a') 
    | _ -> (a,a')

  let rec instantiate_quantifiers p = 
    match p with P.All (s,p') ->
      let lvs = C_SD.globals_of_type (C_SD.get_type_of_lval (E.Symbol s)) in
      let lvs = List.filter C_SD.is_skolem lvs in 
      let ps = List.map (fun lv -> P.substitute [(E.Lval (E.Symbol s)),(E.Lval lv)] p) lvs in
      P.And (List.map instantiate_quantifiers ps) 
    | _ -> p
 
let aliases_in_scope f xs =
  let fml_t = Hashtbl.create 17 in
  let _ = match C_SD.lookup_formals f with
            C_SD.Fixed ys | C_SD.Variable ys -> 
              List.iter (fun y -> Hashtbl.replace fml_t y true) ys in
  let f_conv lv =
    match lv with
      E.Symbol x -> 
        E.Lval(
	  if (Hashtbl.mem fml_t x) && not (O.getValueOfBool "incref")
	  then (E.make_symvar f lv) else lv) 
    | _ -> E.Lval lv in
  Misc.flap 
  (fun lv -> 
    if not (C_SD.is_in_scope f lv) then [] else
      let lv' = E.lv_of_expr (E.deep_alpha_convert f_conv (E.Lval lv)) in
      if lv' = lv then [lv] else [lv;lv']) xs 

(*****************************************************************************)
(************************ McCarthy Code **************************************)
(*****************************************************************************)

let mc_var = E.mc_var AA.get_lval_aliases

let to_mccarthy = E.to_mccarthy AA.get_lval_aliases

let pred_to_mccarthy p = 
  if O.getValueOfBool "mccarthy" then P.to_mccarthy AA.get_lval_aliases p else p

(**************************************************************************)
(********** used by tproj, refinement (move to abs_op.ml? *****************)
(**************************************************************************)

let get_callEdge_o op_array = 
  let (left,_) = Misc.paren_match Op.paren_fun op_array in
  fun i l -> 
    if O.getValueOfBool "events" then 
      let ci = left i in
      (assert (0 <= ci && ci < Array.length op_array);
      Some (op_array.(ci))) 
    else try Some (List.find C_SD.isFunCall (C_SD.get_ingoing_edges l)) with _ -> None 

let add_to_lv_table es =
  let fields = Misc.flap E.fields_of_expr es in
  ignore(List.map (C_SD.add_field_to_unroll true) fields)

let load_lv_table ops =
  let exprs_p p = List.map (fun (P.Atom e) -> e) (P.getAtoms p) in
  let exprs_stm s = 
    match s with Cmd.Expr (E.Assignment(E.Assign,_, e)) | Cmd.Return e -> e in
  let exprs_op op = 
    match (C_SD.get_command op).Cmd.code with
      Cmd.Pred p -> exprs_p p
    | Cmd.Block es -> 
        List.map exprs_stm es
    | Cmd.FunctionCall e  ->
        List.map snd (asgn_of_funcall (true,false) None e)
    | Cmd.GuardedFunctionCall (p,e) ->
        (exprs_p p) @ (List.map snd (asgn_of_funcall (true,false) None e))
    | _ -> [] in
  List.iter 
    (fun e -> add_to_lv_table (getVarsAndDerefs (P.Atom e))) 
    (Misc.flap exprs_op ops)
    
let print_trace ops =
  List.iter (fun op -> M.msg_string M.Normal (Op.to_string op)) ops 

let print_trace_reg level ops regs = 
  List.fold_left 
  (fun i (op,reg) ->
    let d = if O.getValueOfBool "bddprint" then P.toString (R.to_predicate reg) else "" in
    let s = if C_SD.isAsyncCall op then ":ACall" else if C_SD.isAsyncExec op then ":AExec" else "" in
    M.msg_string level (Printf.sprintf "Op: %d %s :%s" i s (Op.to_string op));
    M.msg_string level ("post-reg: "^d);
    i+1) 
  0 (Misc.do_catch "print_trace_reg" (List.combine ops) regs)

(********************************************************************************)
(*************************** guarded assignments *********************************)
(********************************************************************************)
let get_asgn_list (f,subs,t,re) =
  let params =
    List.filter (fun (l,_) -> PT.is_relevant_lval l) 
    (asgn_of_return (f,subs,t,re)) in
  let globals =
    List.map 
      (fun glv -> (glv,get_new_unknown f))
      (PT.relevant_globals_mod f) in
  globals@params
  
let bk_asgn (Cmd.Expr (E.Assignment(E.Assign,lv,e))) = (lv,e)

let make_guarded_asgn op l' = 
  match (C_SD.get_command op).Cmd.code with 
    Cmd.Skip | Cmd.SymbolicHook _ | Cmd.Block [] | Cmd.Pred P.True -> 
      (P.True,[]) 
  | Cmd.FunctionCall fce -> 
      (P.True,asgn_of_funcall (true,false) None fce)
  | Cmd.Block b -> 
      (P.True, Misc.map_partial (asgns_of_stmt l') b)
  | Cmd.Pred p -> 
      (pred_to_mccarthy p,[]) 
  | Cmd.Havoc x -> 
      let x' = match getNewTmpVar (E.Symbol "__foo") with 
      E.Chlval (E.Symbol x, s) -> E.Lval (E.Symbol (s^x)) in
      (P.True, [(x,x')])
  | _ -> failwith "make_guarded_asgn: TBD" 

(*********************************************************************)
(********************** Loading/saving abstractions ******************)
(*********************************************************************)

  type abstraction_info = 
    AbsPred of P.predicate
  | LocPredTuple of ((int*int) * int list)
  | SymLvalTuple of (string * (E.lval * E.lval) list) (* (fn,[...(lv,symlv)...] *)

  let get_AbsPred ai = 
    match ai with AbsPred p -> Some p | _ -> None
        
  let get_LocPredTuple ai = 
    match ai with LocPredTuple t -> Some t | _ -> None
  
  let get_SymLvalTuple ai =
    match ai with SymLvalTuple t -> Some t | _ -> None

  let bccs () = 
    let seed =
      if (O.getValueOfString "funlabel" <> "") then O.get_errorseed () else 
        let elocs = C_SD.get_locations_at_label ( O.getValueOfString "L") in
        Misc.sort_and_compact (List.map C_SD.get_location_fname elocs) in 
    if (O.getValueOfBool "bmc") then
      let main = List.hd (O.getValueOfStringList "main") in   
      let ds = (List.map (fun f' -> List.length (C_SD.shortest_path main f')) seed) in
      C_SD.bounded_backwards_caller_closure 
        ((Misc.list_max ds) + (O.getValueOfInt "depthoffset")) 
        seed 
    else if (O.getValueOfBool "bccscc") then C_SD.scc_caller_closure (List.hd seed) 
    else C_SD.backwards_caller_closure seed 

  let exits () = 
    (C_SD.bounded_backwards_caller_closure 
      (O.getValueOfInt "exitDepth")
      ["exit";"abort";"__error__"]) @
    (if not (O.getValueOfBool "errlv") then [] else 
      Misc.flap C_SD.may_mod_by (C_SD.get_error_lvals ()))

  let initialize_skipfun () =
  if (O.getValueOfBool "skipfun") then
    (List.iter PT.add_skip_fun (C_SD.get_reachable_functions ());
     (if O.getValueOfBool "events" then PT.add_take_fun C_SD.__BLAST_DispatchFunctionName);
     List.iter PT.add_take_fun ((bccs ()) @ (exits ()));
     M.msg_string M.Normal ("Start take_fun size:"^(string_of_int !PT.stats_nb_take_funs)))
 
  let dump_predlocs aps = 
    let ps = Misc.sort_and_compact (List.map (fun (AbsPred p) -> p) aps) in
    let trans =
      let t = Hashtbl.create 101 in
      let _ = List.fold_left (fun i p -> Hashtbl.replace t p i; i+1) 0 ps in
      fun i -> Misc.do_catch "dump_predlocs" (Hashtbl.find t) (PT.getPred i) in 
    let maxr = ref 0 in let sumr = ref 0 in
    let rv = 
      List.map
      (fun (l,is) ->
         let rv = List.map trans is in 
         let size = List.length rv in
         let _ = sumr := !sumr + size; if (size > !maxr) then maxr := size in
         LocPredTuple (l,rv))
      (PT.get_all_loc_pred_tuples ()) in
    (rv,!maxr,(float_of_int !sumr)/.(float_of_int (1 + List.length rv)))

  let dump_symvars =
    let f lv = (E.peel_symbolic lv,lv) in
    fun () -> (List.map (fun (fn,lvs) -> SymLvalTuple (fn, (List.map f lvs))) (all_symvars ()))

  (* API *)
  let dump_abs () =
    M.msg_string M.Debug "Dumping Abstraction"; PT.print_lp_table ();
    let aps = List.map (fun i -> AbsPred (PT.getPred i)) (PT.get_all_pred_indexes ()) in
    let (plocs,pmax,pavg) = dump_predlocs aps in
    let symvars = dump_symvars () in 
    let f = (O.getValueOfString "mainsourcename")^".abs" in
    (let oc = open_out f in Marshal.to_channel oc (aps@plocs@symvars) [];close_out oc);     
    M.msg_string M.Normal (Printf.sprintf "#preds/loc: max = %d, avg = %f  " pmax pavg)
        
  let load_async_preds () = 
    M.msg_string M.Normal "Reading in async predicates...";
    let filename = (O.getValueOfString "mainsourcename")^".apred" in
    try
      let aps = 
        let _ = VampyreErrormsg.startFile filename in
        let inchan = open_in filename in
        let lexbuf = Lexing.from_channel inchan in
        let _ = VampyreErrormsg.theLexbuf := lexbuf in
        Inputparse.fun_main Inputflex.token lexbuf in
      M.msg_string M.Normal "Async predicates read." ; 
      List.iter 
        (fun (fn,ps) -> 
          let l = C_SD.location_coords (C_SD.lookup_entry_location fn) in
          PT.add_local_preds l (List.map PT.addPred ps))  
        aps
    with _ -> (M.msg_string M.Error ("No async preds in "^filename))

  let load_symvars = 
    Misc.flap 
      (fun (fn,lvs) ->
        List.map 
          (fun (lv,slv) -> 
             let slv' = E.make_symvar fn lv in 
             add_relevant_symvar fn slv'; (E.Lval slv,E. Lval slv')) lvs)

  let load_predlocs ps plocs = 
    let ns : int list = List.map (fun (_,ps) -> List.length ps) plocs in
    let pavg =  
      let l = List.length plocs in 
      if l = 0 then 0.0 else (float_of_int (Misc.list_add ns)) /. (float_of_int l) in
    let _ = M.msg_string M.Normal 
    (Printf.sprintf "#preds/loc: max = %d, avg = %f" (Misc.list_max (0::ns)) pavg) in
    let trans =
      let t = Hashtbl.create 101 in
      let _ = List.fold_left (fun i p -> Hashtbl.replace t i (PT.addPred p); i+1) 0 ps in
      fun i -> Misc.do_catch "load_predlocs" (Hashtbl.find t) i in 
    List.iter (fun (l,lps) -> PT.add_local_preds l (List.map trans lps)) plocs

  (* API *)
  let load_abs () = 
    M.msg_string M.Normal "start initialize abstraction.";
    let _= if (O.getValueOfBool "events") then load_async_preds () in
    let _ = initialize_skipfun () in 
    let fname = 
      let raw = O.getValueOfString "loadabs" in
      if raw = "self" then ((O.getValueOfString "mainsourcename")^".abs") else raw in
    if fname = "" then () else 
      let ais = 
        let ic = open_in fname in
        let rv = (Marshal.from_channel ic : abstraction_info list) in
        close_in ic; rv in
      let symsubs = load_symvars (Misc.map_partial get_SymLvalTuple ais) in
      let ps = List.map (P.substitute symsubs) (Misc.map_partial get_AbsPred ais) in
      load_predlocs ps (Misc.map_partial get_LocPredTuple ais); 
    M.msg_string M.Normal "done initialize abstraction." 

