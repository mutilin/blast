(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

module O = Options
module M = Message
module P = Ast.Predicate
module E = Ast.Expression
module C_SD = BlastCSystemDescr.C_System_Descr
module Cmd = BlastCSystemDescr.C_Command
module S = Bstats
module AA = AliasAnalyzer
module A = Abstraction.Make_C_Abstraction(C_SD) 

let all_edge_id_ref = ref []
let final_string_ref = ref ""
let pred_ref = ref P.True
let eloc_ref = ref None

(********************************************************************)
(**************************** Query Caching *************************)
(********************************************************************)

(* API *)
let query_id_to_string qid = 
  String.concat ";" (List.map (fun (x,y) -> Printf.sprintf " %d %d " x y) qid)

let qcfile () = 
  let fn = O.getValueOfString "qcfile" in
  let fn = if fn = "" then O.getValueOfString "mainsourcename" else fn in
  fn^".qc"

let queries fn = 
  Misc.map_partial
   (fun raw -> 
     match Misc.chop raw "," with [s1;s2;s3] ->
       Some (List.map 
               (fun s -> match Misc.words_of_string s with [w1;w2] -> 
                 (int_of_string w1,int_of_string w2))
               (Misc.chop s1 ";"),  
             (int_of_string (List.hd (Misc.words_of_string s2)),
              int_of_string (List.hd (Misc.words_of_string s3)))) in
     with e -> (M.msg_string M.Error ("qc line: "^raw^" raises:"^(Printexc.to_string e)); None))
   (Misc.do_catch_ret Misc.string_list_of_file fn [])

(* API *)
let lookup_user_queries () = 
  List.map fst (queries (O.getValueOfString "qf"))

(* API *)
let (load_query_cache, update_query_cache, lookup_query_cache) = 
  let qt = Hashtbl.create 101 in
  fun () -> List.iter (fun (k,d) -> Hashtbl.add qt k d) (queries (qcfile ())),
  fun q (r,t) -> 
    let _ = Hashtbl.add qt q (r,t) in
    Misc.append_to_file (qcfile ()) 
    (Printf.sprintf "%s , %d , %d \n\n" (query_id_to_string q) r t),
  fun q -> try Some (Hashtbl.find qt q) with Not_found -> None

(*API*)
let query_id es = 
  List.sort compare 
    (List.map (fun e -> C_SD.location_coords (C_SD.get_source e)) es)



let read_preds fn =
  try
    if fn = "" then [] else 
      let _ = M.msg_string M.Normal "Reading in seed predicates..." in
      let ic = open_in filename in
      let rv = 
        let _ = VampyreErrormsg.startFile filename in
        let lexbuf = Lexing.from_channel ic in
        let _ = VampyreErrormsg.theLexbuf := lexbuf in
        Inputparse.main Inputflex.token lexbuf in
      let _ = close_in ic in 
      let _ = M.msg_string M.Normal "Seed predicates read." in rv	
  with Sys_error _ | VampyreErrormsg.Error -> 
    (M.msg_string M.Error ("Error raised reading preds in: "^fn); [])

(* TBD: fix after refactor/recompile {{{
 
let model_check o_err_locs entries seedPreds =
  M.msg_string M.Normal "****** Now running cfb model checker ******";flush stdout;
  let _ = A.initialize_abstraction () in
  module This_LMC_CFB = CfbLazyModelChecker.Make_Lazy_Model_Checker(A) 
  let initStateFile = O.getValueOfString "init" in
  let initialState = P.conjoinL (S.time "read initial state" read_preds initStateFile) in
  let mainFunctions = O.getValueOfStringList "main" in
    begin
      let indent_loc = "                                                " in
      let print_op indent_op op =
        let target_loc = C_SD.get_target op
        and c_command = C_SD.get_command op
        in
        M.msg_string M.Normal
          (indent_op ^ (Cmd.to_string c_command)) ;
        M.msg_string M.Normal
          (indent_loc ^ (C_SD.location_to_string target_loc)) ;
        match c_command.Cmd.code with
          Cmd.FunctionCall _ ->
            indent_op ^ "   "
        | Cmd.Block l ->
            begin
              match (List.hd (List.rev l)) with
              | Cmd.Return _ ->
                  String.sub indent_op 0 ((String.length indent_op) - 3)
              | _ -> indent_op
            end
        | _ -> indent_op
      in
      let mc_return_val = 
	try
	  if (O.getValueOfBool "cf")
	  then
	    begin
	      let init_region =
		let rec buildloclist lst loclist =
		  match lst with [] -> loclist
		  | a ::rest -> 
		      try
			buildloclist rest ((C_SD.lookup_entry_location a) :: loclist) 
		      with 
			Not_found -> 
			  (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
			   failwith ("No function "^(a)^" defined"))
		in
		let loclist = match entries with
		  None -> buildloclist mainFunctions []
		| _ -> failwith "Unimplemented CF multi-entry" in
		This_Cf_C_Abstraction.create_region loclist false seedPreds initialState
		  
	      and error_region =
		    This_Cf_C_Abstraction.create_region !eloc_ref true P.True P.True
	      in 
              let cfbmc i (Some e) = 
                let start = Unix.times () in
                let rv = This_LMC_CFB.model_check i e in
                  let _ = 
                    if (O.getValueOfBool "reachloc") then
                    let finish = Unix.times () in
                    let time = int_of_float (finish.Unix.tms_utime -. start.Unix.tms_utime) in
                    let qid_all = !all_edge_id_ref in
                    let qid_safe = List.filter (fun id -> not (This_LMC_CFB.is_reachable id)) qid_all in
                    List.iter (fun id -> Q.update_query_cache [id] (1,time)) qid_safe
                  in
                rv
              in
              let mc_return_val = S.time "Actual model check[cf]" (cfbmc init_region) error_region in
              let _ = final_string := "" in
              if mc_return_val = 1 then
                (final_string := "\n\nNo error found.  The system is safe :-)\n";
                M.msg_string M.Major !final_string)
              else 
                (final_string := "\n\nError found! The system is unsafe :-(\n";
        	 M.msg_string M.Major !final_string)
	    end
	  else
	    begin 
	      (* vanilla tree unroll starts here *)
	      let init_region =
		let rec buildloclist lst loclist =
		  match lst with [] -> loclist
		  | a ::rest -> 
		      try
			buildloclist rest ((C_SD.lookup_entry_location a) :: loclist) 
		      with 
			Not_found -> 
			  (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
			   failwith ("No function "^(a)^" defined"))
		in
		let loclist = match entries with
		  None -> buildloclist mainFunctions []
		| Some entries -> entries in
		A.create_region 
		  loclist false seedPreds initialState
		  
	      and error_region = (A.create_region !eloc_ref true P.True P.True) in
	      let mc_return_val = This_LMC.model_check init_region error_region in
	      match (mc_return_val) with
		No_error ->
		  if not (O.getValueOfBool "interactive") then
		    final_string := "\n\nNo error found.  The system is safe :-)\n"
			  
	      | Error_reached (reg_i, op_list, reg_e) ->
		  if O.getValueOfBool "interactive" then begin
		    let coords = A.Region.get_location_id reg_e in
		    let loc = C_SD.lookup_location_from_loc_coords coords in
		    reachedLoc := Some loc
		  end else begin
		    final_string := "\n\nError found! The system is unsafe :-(\n\n Error trace:\n";	
                    M.msg_string M.Major !final_string;
		    M.msg_string M.Normal
		      (indent_loc ^ (C_SD.location_to_string
				       (C_SD.get_source (List.hd op_list)))) ;
		    let _ = List.fold_left print_op "" op_list in
		    M.msg_string M.Normal "\n\n" ;
		    M.msg_printer M.Normal A.Region.print reg_e;
		    (try 
		      M.msg_string M.Normal "Test case:" ;
                      let rec _prtest l = match l with [] -> () 
                      | (sym, v) :: t -> 
		  	M.msg_string M.Normal (E.toString sym);
		  	M.msg_string M.Normal (string_of_float v) ;
		  	_prtest t
                      in
                      _prtest (A.genTests reg_i) ;
                    with _ -> (M.msg_string M.Error "Test generation failed." ; ) ; (* end try *) );
		    (*if (O.getValueOfBool "xml") then Absutil.dumpXml
                    * op_list *)
		  end
                  end
	with e -> 
	  begin 
            final_string := ("Exception raised :("^(Printexc.to_string e));
            if (O.getValueOfBool "cf" || O.getValueOfBool "cfb") 
            then (This_Cf_C_Abstraction.notify_result !final_string;
                  This_Cf_C_Abstraction.dump_abs ())
	    else (A.notify_result !final_string;
                  A.dump_abs ()); 
            M.msg_string M.Error !final_string;
	    raise e
	  end
      in 
      ()
    end ;
    if (O.getValueOfBool "cf") 
    then
      begin
        let print_stats = if O.getValueOfBool "cfb" then
          This_LMC_CFB.print_stats else This_LMC_CF.print_stats
        in
	M.msg_string M.Normal "\n\nModel checker stats:\n" ;
	M.msg_printer M.Normal print_stats () ;
	M.msg_string M.Normal "\n\nAbstractor stats:\n" ;
	M.msg_printer M.Normal This_Cf_C_Abstraction.print_stats ();
        This_Cf_C_Abstraction.notify_result !final_string;
        This_Cf_C_Abstraction.exit_abstraction ()
    end
    else
      begin
	M.msg_string M.Normal "\n\nPhi Regions:\n" ;
	M.msg_string M.Normal (This_LMC.print_phis false) ;
	M.msg_string M.Normal "\n\nModel checker stats:\n" ;
	M.msg_printer M.Normal This_LMC.print_stats () ;
	M.msg_string M.Normal "\n\nAbstractor stats:\n" ;
	M.msg_printer M.Normal A.print_stats () ; 
        A.notify_result !final_string;
        A.exit_abstraction ()
      end

        
let get_edges funlabel f = 
  (* given a function f, find out all edges in its CFA where it	  calls the function funlabel *)
   let edgelist = ref [] in
	let _worker e =
	  match (C_SD.get_command e).Cmd.code with
	    Cmd.FunctionCall ex ->
	      begin
		match ex with
		  E.Assignment (_, _, E.FunctionCall (E.Lval (E.Symbol fl), _)) 
		| E.FunctionCall (E.Lval (E.Symbol fl), _) ->
		    if fl = funlabel then edgelist := e :: !edgelist 
		| _ -> ()
	      end
	  | _ -> ()
	in
	C_SD.map_edges_fname _worker f ;
	!edgelist

 

let get_lvals_on_guards elist =
  List.fold_left (fun allofthem e ->
    let lvs_of_e = 
      let src = C_SD.get_source e in
      let incoming = C_SD.get_ingoing_edges src in
      List.fold_left (fun sofar inedge ->
        match (C_SD.get_command inedge).Cmd.code with
        Cmd.Pred p ->
          begin
            let lv_list = Ast.P.lvals_of_predicate p in
            M.msg_string M.Normal 
            ("The lvalues in this edge are: "^(Misc.strList (List.map E.lvalToString lv_list)));
            lv_list @ sofar
          end
        | _ -> sofar ) [] incoming 
    in
    lvs_of_e @ allofthem) [] elist


 let local_synt_partition edges = 
   let fname_edge_table = Hashtbl.create 11 in
   List.iter 
   (fun e -> let src = C_SD.get_source e in
   let incoming = C_SD.get_ingoing_edges src in 
   List.iter 
   (fun inedge ->
     match (C_SD.get_command inedge).Cmd.code with
     Cmd.Pred p ->
       begin
         let lv = Ast.P.varsOfPred p in
         M.msg_string M.Normal "The lvalues in this edge are: ";
         List.iter (M.msg_string M.Normal ) lv ;
         let thisfp = List.hd lv in
         try let old = Hashtbl.find fname_edge_table thisfp in
             Hashtbl.replace fname_edge_table thisfp (e :: old)
         with Not_found -> Hashtbl.add fname_edge_table thisfp ([ e ])
       end
     | _ -> failwith "Strange incoming edge!") 
   incoming) 
   edges ;
   let l = ref [] in
   Hashtbl.iter (fun key data -> l := data::!l) fname_edge_table ;
   !l

(* TBD:SPEED quadratic in elist *)
let aliased_lvalue_cluster elist =
  let cl_tbl = Hashtbl.create 101 in
  let cllv_tbl = Hashtbl.create 101 in
  let keys = ref [] in
  let _proc e = 
    let lv_l = get_lvals_on_guards [e] in
    let check idx = 
      let lvi_l = Hashtbl.find cllv_tbl idx in
      let hit = 
        Misc.list_exists_cross2 (fun l l' -> l=l' ||
        (*AA.askAliasDatabase l l'*) AA.queryAlias
        (E.Lval l) (E.Lval l'))
        lvi_l lv_l
      in
      let _ = 
        if hit then 
        let _ = Misc.hashtbl_check_update cl_tbl idx e in 
        Hashtbl.replace cllv_tbl idx (Misc.sort_and_compact (lv_l@lvi_l)) 
      in
      hit
    in
    try ignore(List.find check !keys) with Not_found ->
      let newkey = List.length !keys + 1 in
      Hashtbl.replace cllv_tbl newkey lv_l;
      Hashtbl.replace cl_tbl newkey [e];
      keys := newkey ::!keys;
  in 
  let output_cluster k = 
            let qids = 
              let qid = Q.query_id (Hashtbl.find cl_tbl k) in
              (Q.query_id_to_string qid) 
            in
            let lvs = 
              let ll = List.map E.lvalToString (Hashtbl.find cllv_tbl k) in
              String.concat "," ll
            in
            M.msg_string M.Normal 
            (Printf.sprintf "[QUERY] id: %s lvals: %s" qids lvs)
  in
    List.iter _proc elist;
    List.iter output_cluster !keys;
    List.map snd (Misc.hashtbl_to_list cl_tbl)

let get_user_query uqid_list all_call_edges = 
  let elist = List.flatten all_call_edges in
  let get_query_from_qid uqid = 
    let tbl = Hashtbl.create 31 in
    let rec proc uql = 
      match uql with [] -> ()
      | (x,y)::t -> Hashtbl.replace tbl (x,y) true;proc t 
    in 
    let ff e = let [id] = Q.query_id [e] in Hashtbl.mem tbl id in
    proc uqid;
    List.filter ff elist
  in
  List.map get_query_from_qid uqid_list
    
let cluster_edges es = 
  let eps =
    if (O.getValueOfString "funlabel") = "" then [List.flatten es] else 
      match O.getValueOfInt "funmode" with  
        1 -> List.map (fun e -> [e]) (List.flatten es)
      | 2 -> Misc.flap local_synt_partition es
      | 3 -> S.time "alias cluster" aliased_lvalue_cluster (List.flatten es)
      | 4 -> get_user_query [Misc.group_pairwise (O.getValueOfIntList "qid")] es 
      | 5 -> get_user_query (Q.lookup_user_queries ()) es 
      | _ -> es in
  M.msg_string M.Error ("#Queries : "^(string_of_int (List.length eps)));
  List.map (List.sort compare) eps

let filter_preds p es =
  let lvt = Hashtbl.create 101 in
  let ps = P.getAtoms p in
  List.iter (fun lv -> Hashtbl.replace lvt lv true) (get_lvals_on_guards es); 
  P.conjoinL (List.filter (fun p -> List.exists (Hashtbl.mem lvt) (P.lvals_of_predicate p)) ps) 
  
let modelcheck o_err_locs =
  let seedPreds = !preds_ref in
  let _ = if O.getValueOfBool "qc" then Q.load_query_cache () in
  let funlabel = O.getValueOfString "funlabel" in
  let replacefunlabel = "__BLAST_"^funlabel in
  let callers = Misc.sort_and_compact (List.map snd (C_SD.calls_made_into funlabel)) in    
  let d = ref 0 in let cumulative = ref 0 in let successes = ref 0 in
  let failures = ref 0 in let no_new = ref [] in let timeout = ref [] in let bugs = ref [] in
  let replace_edges label elist =
    M.msg_string M.Error ("Replace Edges with :"^label);
    let rep e =
        match (C_SD.get_command e).Cmd.code with
       Cmd.FunctionCall (E.FunctionCall (_,args)) ->
         C_SD.replace_code_in_cfa e 
         (E.FunctionCall (E.Lval (E.Symbol label),args))
        | _ -> M.msg_string M.Error ("Bad edge in replace_edges!")
    in
       List.iter rep elist                      
   in
   let run_one_check elist =
     if elist = [] then () else 
       begin
         let error_lvs = get_lvals_on_guards elist in
         let _ = C_SD.set_error_lvals error_lvs in
         let edge_fname e =
           C_SD.get_location_fname (C_SD.get_source e) 
         in
         let edge_fnames = List.map edge_fname elist in
         let efn = Misc.strList edge_fnames in
         O.set_errorseed  edge_fnames ;
         M.msg_string M.Major (Printf.sprintf "Call %d (cumulative %d) in fun:%s" !d !cumulative efn ) ;
         incr d ;
         cumulative := !cumulative + (List.length elist) ; 
         if (!d <= O.getValueOfInt "skiptest" || 
             (O.getValueOfBool "selecttest") && not (List.mem !d (O.getValueOfIntList "checktest"))) then
	    M.msg_string M.Normal ("Skipping query # "^(string_of_int !d)) 
         else
	  begin
          let qid = Q.query_id elist in
          let qids = Q.query_id_to_string qid in
          let ms1 = (Printf.sprintf "Trying error call # %d (cumulative %d) edges: %d, in functions %s, qid: %s" 
            !d !cumulative (List.length elist) efn qids) 
          in
            M.msg_string M.Error ms1;
	  M.msg_string M.Major ms1 ;
          M.msg_string M.Major ("Lvalues on edges in this call: ");
          M.msg_string M.Error ("Lvalues on edges in this call: ");
          List.iter (fun lv -> let ls = E.lvalToString lv in 
            M.msg_string M.Major ls ; M.msg_string M.Error ls ) 
            (get_lvals_on_guards elist) ;
          let start = Unix.times () in
          let (outcome,time) =
            match Q.lookup_query_cache qid with
            Some(o,t) -> 
                (M.msg_string M.Error (Printf.sprintf "Query Cache: %d, time = %d" o t); (o,t))
            | None -> 
                  begin
                    let oref = ref 0 in
                    replace_edges replacefunlabel elist;
                    begin
                    try 
                      let seedPreds = filter_preds seedPreds elist in
                      model_check o_err_locs None seedPreds  ;
                      M.msg_string M.Error !final_string;
                      if (Misc.is_substring !final_string ":-(" ) then 
                        if (O.getValueOfBool "norefine") then 
                           (no_new := !d :: !no_new)
                        else
                        (bugs := !d :: !bugs;  oref := -1) 
                      else ( oref := 1) 
                    with 
                    Ast.TimeOutException ->
                      (M.msg_string M.Normal "This check times out!"; 
                      timeout:= !d :: !timeout)
                    | e -> 
                      (M.msg_string M.Error (Printexc.to_string e) ;
                      M.msg_string M.Error "Exception raised !" ; 
                      no_new := !d :: !no_new)
                    end;
                    replace_edges funlabel elist;
                    let time = 
                      let finish = Unix.times () in
                      int_of_float (finish.Unix.tms_utime -. start.Unix.tms_utime) 
                      in
                    Q.update_query_cache qid (!oref,time);
                    (!oref,time)
                  end
          in
          let os = 
            if outcome = 1 then (incr successes; "SAFE") 
            else if outcome = -1 then (incr successes; "UNSAFE")
            else (no_new := !d::!no_new; "FAIL")
          in
          M.msg_string M.Major (Printf.sprintf "Query %d: %d s, %s" !d time os) ;
          M.msg_string M.Error (Printf.sprintf "Query %d: %d s, %s" !d time os);
         end 
    end
  in 
    
  if funlabel = "" then model_check o_err_locs None seedPreds else
    begin
      M.msg_string M.Error 
       (Printf.sprintf "Number of modeled functions: %d" (List.length (C_SD.list_of_functions ())));
       let all_call_edges = List.map (get_edges funlabel) callers in
       let numcalls = List.length all_call_edges in
       let numinstr = List.fold_left (fun sofar thisone -> sofar + (List.length thisone)) 0 all_call_edges in
       M.msg_string M.Error (Printf.sprintf "#Funs/#Sites calling error: %d / %d "numcalls numinstr) ;
       let qids = Q.query_id_to_string (Q.query_id (List.flatten all_call_edges)) in 
       M.msg_string M.Error ("All query edges: "^qids);
       let edge_partition_l = cluster_edges all_call_edges in
       (List.iter run_one_check edge_partition_l);
             (* print stats *)
      M.msg_string M.Normal ("Number of safe uses is "^(string_of_int (!successes))) ;
      M.msg_string M.Error ("Number of safe uses is "^(string_of_int (!successes))) ;
      M.msg_string M.Normal ("Number of bugs is "^(string_of_int (!failures))) ;
      M.msg_string M.Error ("Number of bugs is "^(string_of_int (!failures))) ;
	List.iter (fun d -> Printf.fprintf Pervasives.stdout "Bug (%d) " d ;
		Printf.fprintf Pervasives.stderr "Bug (%d) " d ;
           ) !bugs ;
      Printf.fprintf Pervasives.stdout "\n" ;
      Printf.fprintf Pervasives.stderr "\n" ;
      M.msg_string M.Normal ("Failed : (Blast exception, including no new predicates )") ;
      M.msg_string M.Error ("Failed : (Blast exception, including no new predicates)") ;
	List.iter (fun d -> Printf.fprintf Pervasives.stdout "%d " d ;
		Printf.fprintf Pervasives.stderr "%d " d ;
           ) !no_new ;
      M.msg_string M.Normal ("Timed out : ") ;
      M.msg_string M.Error ("Timed out : ") ;
	List.iter (fun d -> Printf.fprintf Pervasives.stdout "%d " d ;
		Printf.fprintf Pervasives.stderr "%d " d ;
           ) !timeout ;
	Printf.fprintf Pervasives.stdout "\n\n-selecttest " ;
	List.iter (fun d -> Printf.fprintf Pervasives.stdout "-checktest %d " d ;
           ) !timeout ;
	Printf.fprintf Pervasives.stdout "\n\n" ;
	Printf.fprintf Pervasives.stderr "\n\n" ;
    end

let runquery () = 
  (* let queryfile = 0.getValueOfString "query" in 
  let inchan = open_in queryfile in
  let lexbuf = Qlflex.init inchan in
  M.msg_string M.Normal "In runquery -- About to parse" ;
  let stmts = Qlparse.main Qlflex.token lexbuf in
  M.msg_string M.Normal "In runquery -- Parsed input, now running statements" ;
  Ql.runStmts stmts*)
  failwith "TBD: runquery"
}}} *)

let runmodelcheck () =
  M.msg_string M.Normal "****** Now running foci model checker ******";flush stdout;
  let _ = A.initialize_abstraction () in
  let locs = List.map C_SD.lookup_entry_location (O.getValueOfStringList "main") in
  let r0 = A.ac_create_region true locs false !preds_ref P.True in 
  let err = A.create_region !eloc_ref true P.True P.True in
  let mc = 
    if O.getValueOfBool "fmc" then FociModelChecker.Make_Foci_Model_Checker(A).model_check
    else CfbLazyModelChecker.Make_Lazy_Model_Checker(A).model_check in
  try S.time "Model Checking" mc r0 err
  with e -> (A.dump_abs (); M.msg_printer M.Normal A.print_stats (); raise e)

let print_header () = 
  M.msg_string M.Normal "Blast 2.0. Copyright  2006  Regents of the University of California.";
  M.msg_string M.Normal "This program contains Foci, Copyright  2003 Cadence Berkeley Laboratories," ;
  M.msg_string M.Normal "Cadence Design Systems. All rights reserved."
  M.msg_string M.Debug "Blast was run with the following command line:";
  M.msg_apply M.Debug (fun () -> Array.iter (fun s -> print_string (s^" ") Sys.argv)) () 

let read_options () = 
  M.set_verbosity M.Default;  O.buildOptionsDatabase ();
  Ast.Counter.set_max (O.getValueOfInt "maxcounter"); 
  (if O.getValueOfBool "msvc" then Cil.msvcMode := true);
  preds_ref := P.conjoinL (read_preds (O.getValueOfString "pred"));
  (let elabel = O.getValueOfString "L" in 
   match C_SD.get_locations_at_label elabel with [l] -> eloc_ref:= Some l 
   | _  -> (M.msg_string M.Error ("Error: bad error label "^elabel); exit 1));

let parse_files () = 
  M.msg_string M.Normal ("Begin Parsing: " ^ (Misc.strList srcs));
  let srcs = O.getSourceFiles () in
  (if srcs = [] then (M.msg_string M.Error ("Error: no input sources"); exit 1)); 
  C_SD.initialize_blast (C_SD.initialize_cil srcs);
  M.msg_string M.Normal ("done Parsing");
  M.msg_string M.Debug "Printing System Description:"; 
  M.msg_printer M.Debug C_SD.print_system ()

let alias_analysis () = 
  S.time "Alias DB" AA.constructAliasDatabase (); 
  M.msg_string M.Normal ("done aliasDB");
  AA.constructMustAliasDatabase (); 
  M.msg_string M.Normal ("done mustAliasDB");
  S.time "Mods DB" (C_SD.post_alias_analysis AA.get_lval_aliases_scope) ();
  M.msg_string M.Normal ("done modsDB");
  AA.add_alias_override (P.conjoinL (read_preds (O.getValueOfString "aliasfile"))); 
  M.msg_string M.Normal "done overRideDB"

let call_graph () = 
  C_SD.compute_callgraph ();
  let fn = O.getValueOfString "callgraph" in
  (if fn <> "" then C_SD.output_callgraph fn);
  C_SD.output_call_paths ()

let finish () = 
  let memstats = Gc.stat () in
  M.msg_string M.Normal ("Minor Words : "^(string_of_float memstats.Gc.minor_words)) ;
  M.msg_string M.Normal ("Major Words : "^(string_of_float memstats.Gc.major_words)) ;
  M.msg_string M.Normal ("Total size of heap in words : "^(string_of_int memstats.Gc.heap_words)) ;
  (if not (O.getValueOfBool "quiet") then S.print stderr "Timings:\n");
  M.msg_string M.Normal !final_string_ref

let () =
  try 
    S.reset ();print_header (); Cil.initCIL (); parse_files (); alias_analysis (); call_graph ();
    (if O.getValueOfString "query" <> "" then runquery () else runmodelcheck ());

    finish ()
  with e -> (M.msg_string M.Error ("Ack! The gremlins again!: "^(Printexc.to_string e)); raise e)
