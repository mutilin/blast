(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)

open Ast

let constructInitialPredicates predFile =
  let readAndAddPredicates filename =
    let doParse inchan =
      let lexbuf = Lexing.from_channel inchan in
      Inputparse.main Inputflex.token lexbuf 
    in
    ignore (Message.msg_string Message.Normal "Reading in seed predicates...");
    try
      let inchan = open_in filename
      in
      let a = doParse inchan
      in
	ignore (Message.msg_string Message.Normal "Seed predicates read.\n") ;	
	List.iter (fun a -> Message.msg_string Message.Normal (Predicate.toString a)) a ;
	Message.msg_string Message.Normal ("Read "^(string_of_int (List.length a))^" predicates");
	a
    with
      Sys_error _ -> (Message.msg_string Message.Error ("Cannot find predicate information in "^filename^".\n"); [])
    | e -> (Message.msg_string Message.Error (Printexc.to_string e); [])
  in
  if (predFile = "") then
    (* Nothing to do *)
    []
  else
    readAndAddPredicates predFile

