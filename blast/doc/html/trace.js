var defaultBackground= '#ffffff';//'#eeeeee';
var hilitBackground='#aaf';
var defaultColor='#222';

function findPosY(obj)
{
 var curtop = 0;
 if (document.getElementById || document.all)
 {
  while (obj.offsetParent)
  {
   curtop += obj.offsetTop
   obj = obj.offsetParent;
  }
 }
 else if (document.layers)
  curtop += obj.y;
 return curtop;
}
// Browser Window Size and Position
// copyright Stephen Chapman, 3rd Jan 2005
// you may copy these functions but please keep the copyright notice as well
function pageHeight() {return window.innerHeight != null? window.innerHeight: document.body != null? document.body.clientHeight:null;}
function posTop() {return typeof window.pageYOffset != 'undefined' ? window.pageYOffset:document.documentElement.scrollTop? document.documentElement.scrollTop: document.body.scrollTop?document.body.scrollTop:0;}

function colorCode(tagName,pstart,pstop){
    var lines=document.getElementsByTagName(tagName);
    //scroll code
    var start=pstart-1;
    var stop=pstop-1;
    if (start >= 0){
       var startLine=lines[start];
       var lineY=findPosY(startLine);
       var ph = pageHeight();
       var pt = posTop();
       if ( pt > lineY || lineY > pt + ph) {
          window.scrollTo(0,lineY);
       }
    } //end scroll code  
    for (var i=0;i<lines.length;i++){
        var line=lines[i];
	if (start <= i && i < stop){
	  line.style.background = hilitBackground;
	} else {
	  line.style.background = defaultBackground;
	}
    }
    //redraw();
    return null;
}
  

  
function showfootnote() {
    var alldivs=document.getElementsByTagName("div");
    for (var i=0;i<alldivs.length;i++){
	var span=alldivs[i];
	if (span.className=="Desc") {
	    if (span.style.display=="none") {
		span.style.display = "block";
	    }
	    else {
		span.style.display = "none";
	    }
	}
    }
    //redraw();
    return null;
}


function colorRed(){
   var allitems=document.getElementsByTagName("li");
   //allitems[0].style.background='#000';
   for (var i=0;i<allitems.length;i++){
     allitems[i].style.color=defaultColor;
   }
   var items = colorRed.arguments.length;
   for (var j=0;j < items;j++){
     var idx = colorRed.arguments[j];
     if (0<=idx && idx<allitems.length){
        allitems[idx].style.color='red';
     }
   }
} 

var lastClickedIdx;
function colorme(elIdx){
   var items=document.getElementsByTagName("li");
   for (var i=0;i<items.length;i++){
      if (i==elIdx){
        items[i].style.background=hilitBackground;
      } else {
        items[i].style.background=defaultBackground;
      }
   }
   //redraw();
   return null;
}



// after this is rubbish
function getScrollPosition(){
if (navigator.appName == "Microsoft Internet Explorer"){var position = document.body.scrollTop;}
else {var position = window.pageYOffset;} 
return position;
}

function getWindowHeight(){
if (parseInt(navigator.appVersion)>3) {
 if (navigator.appName=="Netscape") {
  winW = window.innerWidth;
  winH = window.innerHeight;
 }
 if (navigator.appName.indexOf("Microsoft")!=-1) {
  winW = document.body.offsetWidth;
  winH = document.body.offsetHeight;
 }
}
return winH;
}

function findPosX(obj)
{
 var curleft = 0;
 if (document.getElementById || document.all)
 {
  while (obj.offsetParent)
  {
   curleft += obj.offsetLeft
   obj = obj.offsetParent;
  }
 }
 else if (document.layers)
  curleft += obj.x;
 return curleft;
}
function pageWidth() {return window.innerWidth != null? window.innerWidth: document.body != null? document.body.clientWidth:null;}
function posLeft() {return typeof window.pageXOffset != 'undefined' ? window.pageXOffset:document.documentElement.scrollLeft? document.documentElement.scrollLeft:document.body.scrollLeft? document.body.scrollLeft:0;}
function posRight() {return posLeft()+pageWidth();}
function posBottom() {return posTop()+pageHeight();}


