
\section{A Tutorial Introduction to \blast}\label{tutorial}

In this section we show how \blast\ can be used to check safety properties of
C code.
We shall discuss how to specify safety properties in the \blast\ specification
language, and the various options that \blast\ takes to check C code against
the specification.
\blast\ can be run both from the command line and from a GUI.
In this section we describe how to run \blast\ from the command line.

\subsection{Assertion Checking}

The simplest form of safety properties that programmers use in code is
{\em assertions}.
Asserts specify invariants of the program.
The programmer writes \begin{verbatim}assert (e)\end{verbatim} in the code,
where {\tt e} is a C expression.
If the expression {\tt e} evaluates to $0$ at run time, the program aborts.
The intent is that the programmer has reasoned that {\tt e} is an invariant
of the program at the place the assert has been introduced.
Consider for example the following piece of code.

\begin{verbatim}
#include <assert.h>

int foo(int x, int y) {
	if (x > y) {
	  x = x - y;
	  L: assert(x > 0);
        }
}
\end{verbatim}

By the constraints introduced by the checks, the programmer knows that at the label
{\tt L}, the value of {\tt x} is greater than $0$.
Assertions are typically checked at run time during the testing phase of a program.
However, using \blast, one can check assertions statically, during compile time.
Moreover, there is no explicit test case to be written.

To run the above example through \blast, you must do the following.
First, instead of the system header file {\tt assert.h}, you must use the {\tt assert.h}
file provided with \blast\ (in the directory {\tt test/headers} of the distribution.
Then, you must produce a preprocessed file containing the source code.
For example, suppose the above code is in a file called {\tt foo.c}.
You can create a preprocessed file by using the command

\begin{verbatim}
gcc -E -I \$BLASTHOME/test/headers/ foo.c > foo.i
\end{verbatim}

This creates a preprocessed file called {\tt foo.i}. 
Now we can run \blast\ on this file.

\begin{verbatim}
pblast.opt -main foo foo.i
\end{verbatim}

The option \c{-main foo} tells \blast\ to start the analysis from the function called
{\tt foo}. 
\blast\ comes back and says
\begin{verbatim}
No error found. The system is safe :-)
\end{verbatim}

While running, \blast\ prints out the predicates that it adds on the error output.
When the model checking is finished,
it also prints out statistics related to the model checking, including the set of
predicates used, the number of iterations, and the number of refinements required
to prove the property.

Now consider an erroneous version of {\tt foo} where the programmer swaps the variables
{\tt x} and {\tt y} in the subtraction.

\begin{verbatim}
#include <assert.h>

int foo(int x, int y) {
        if (x > y) {
          x = y - x;
          assert(x > 0);
        }
}
\end{verbatim}

We similarly create the file {\tt foo.i}, and run \blast\ with the same options.
This time, \blast\ comes back and says 
\begin{verbatim}
Error found. The system is unsafe :-(
\end{verbatim}

It additionally prints out the error trace.

\begin{verbatim}
Size:5

4 :: 4:          Pred(x@foo>y@foo) :: 5
5 :: 5:          Block(x@foo = y@foo  -  x@foo;) :: 6
6 :: 6:          Pred(Not (x@foo>0)) :: 6
6 :: 6:          FunctionCall(__assert("foo.c", 6, "x > 0")) :: -1
\end{verbatim}

The size parameter gives the length of the error trace.
The numbers on the left give the line numbers in the source code for the corresponding
program actions.
Notice that the statement {\tt assert(x>0)} of the source code is expanded
to the two actions

\begin{verbatim}
6 :: 6:          Pred(Not (x@foo>0)) :: 6
6 :: 6:          FunctionCall(__assert("foo.c", 6, "x > 0")) :: -1
\end{verbatim}

that checks the asserted invariant, and calls {\tt \_\_assert} if the
invariant fails.

\begin{exercise}{}
Assertions are also used to specify unreachable code, for example,
at the end of an infinite server loop.
Consider the simple example.
\begin{verbatim}
main () {
  int x,y, temp_x, temp_y;
  x = 0;
  y = 0;
  while (x==y) {
    x++;
    y++;
  }
  assert(0);
}
\end{verbatim}
Check that \blast\ finds that the assert is indeed unreachable (note that
\t{assert(0)} always fails).
\end{exercise}

\subsection{Temporal Safety Specifications}

Assertions are a particularly simple and local way to specify program correctness.
More generally, we are interested in temporal safety properties, where we wish
to check that our program satisfies some finite state property.
For example, we might wish to check that a program manipulating locks
acquires and releases locks in strict alternation 
(that is, two calls to lock without a call to unlock in the middle is an error, 
and vice versa).
More generally, we wish to check safety properties concerning the
proper sequencing of program ``events''.
Let us be concrete.
Consider the following program that manipulates locks (in the file driver.c).

\begin{verbatim}

int STATUS_SUCCESS = 0;
int STATUS_UNSUCCESSFUL = -1;
struct Irp {
  int Status;
  int Information;
};

struct Requests {
  int Status;
  struct Irp *irp;
  struct Requests *Next;	
};

struct Device {
  struct Requests *WriteListHeadVa; 
  int writeListLock;
};

void FSMInit() {
// code to initialize the global lock to unlocked state
}

void FSMLock() {
// code for acquiring the lock

}
void FSMUnLock() {
// code for releasing the lock
}

void SmartDevFreeBlock(struct Requests *r) {
// code omitted for simplicity
}

void IoCompleteRequest(struct Irp *irp, int status) {
// code omitted for simplicity
}

struct Device devE;

void main () {
 int IO_NO_INCREMENT = 3;
 int nPacketsOld, nPackets;
 struct Requests *request;
 struct Irp *irp;
 struct Device *devExt;

 FSMInit();
 devExt = &devE;	


 /* driver code */
 do {
   FSMLock();
   nPacketsOld = nPackets; 
	    
   request = devExt->WriteListHeadVa;
	    
   if(request!=0 && request->Status!=0){
     devExt->WriteListHeadVa = request->Next;	
		
     FSMUnLock();
     irp = request->irp;
		
     if((*request).Status >0) {
       (*irp).Status = STATUS_SUCCESS;
       (*irp).Information = (*request).Status;
     } else {
       (*irp).Status = STATUS_UNSUCCESSFUL;
       (*irp).Information = (*request).Status;
     }	
     SmartDevFreeBlock(request);
     IO_NO_INCREMENT = 3;
     IoCompleteRequest(irp, IO_NO_INCREMENT);
     nPackets = nPackets + 1;
   }
 } while (nPackets != nPacketsOld);
 FSMUnLock();
}
\end{verbatim}

For simplicity, we assume that there is just one global lock
that is acquired by a call to \t{FSMLock} and released by
a call to \t{FSMUnlock}.
We wish to check that the function \t{main} calls \t{FSMLock}
and \t{FSMUnlock} in alternation: if there are two successive calls
to \t{FSMLock} without a call to \t{FSMUnlock} in the middle, it
is an error; 
similarly, if there are two successive calls 
to \t{FSMUnlock} without a call to \t{FSMLock} in the middle, it
is an error. 

One way to check this property is to instrument the program manually by adding some
observer variables, and instrumenting the program to update
the observer variables at each interesting program
event.
Then the check for the safety property can be reduced to checking an
assertion on the observer variables.
For example, in the above code, we can add the observer variable
\t{int lockStatus} that tracks the current state of the global lock
(locked or unlocked), and update \t{lockStatus} to $1$ (for locked) 
whenever \t{FSMLock} is called, and update \t{lockStatus} to $0$ (for unlocked)
whenever \t{FSMUnlock} is called.
We also initialize \t{lockStatus} to $0$ in the call to \t{FSMInit}.
Finally, we add the assertion \t{assert(lockStatus == 0);} in the beginning
of \t{FSMLock} (to say that the lock is not held), and the assertion
\t{assert(lockStatus == 1);} in the beginning of \t{FSMUnlock} (to say that the
lock is held).
Then the instrumented program satisfies our property iff the assertions are never
violated.

The problem with this instrumentation is that it is specific for this code, and
tedious to do manually.
Therefore \blast\ comes with a specification language where safety properties
can be specified at a higher level.
The specification is automatically compiled into an instrumented program with
assertions as above.

For our safe locking property, we write the property as follows (in a file called
\t{lock.spc}).
\begin{verbatim}
global int lockStatus = 0;

event {
  pattern { FSMInit(); }
  action { lockStatus = 0; }
}

event {
  pattern { FSMLock(); }
  guard { lockStatus == 0 }
  action { lockStatus = 1; }
}

event {
  pattern { FSMUnlock(); }
  guard { lockStatus == 1 }
  action { lockStatus = 0; }
}
\end{verbatim}
We give an intuitive description of the specification.
The next section gives a detailed account of the specification language.
The declaration \t{global int lockStatus} defines an observer variable
\t{lockStatus} and initializes it to $0$.
The specification is given in terms of program events.
There are three interesting events in this specification:
calls to the functions \t{FSMInit}, \t{FSMLock}, and \t{FSMUnlock}.
Each event is associated with a syntactic pattern in the code.
For example, the pattern \t{FSMLock();} is matched every time there
is a call to \t{FSMLock} in the code.
Each pattern has a guard that must be met when that pattern matches in the
code: for example, when the pattern \t{FSMLock} is matched, the variable
\t{lockStatus} must be $0$.
If not, there is a violation of the property.
If the guard is true, it can be omitted.
Moreover, there is an action associated with the event that specifies how
the observer variables must be updated.

\blast\ comes with an instrumentation tool that takes a specification and a program
and builds an instrumented program from them.
To use this, we say
\begin{verbatim}
spec.opt lock.spc driver.c
\end{verbatim}
This builds an instrumented program called \t{instrumented.c} as well as a
predicate file \t{instrumented.pred}.
We can now run \blast\ on this file:
\begin{verbatim}
pblast.opt instrumented.c -pred instrumented.pred 
\end{verbatim}
The (optional) \t{-pred} option tells \blast\ to use the predicates in the
file \t{instrumented.pred} also produced by \t{spec.opt}.
\blast\ runs and reports that the program is safe, that is, the specification
is indeed met by this program.


\begin{exercise}
\begin{enumerate}
\item What predicates are used by \blast\ to prove this property?
\item
Now change the condition in the \t{do} $\ldots$ \t{while} loop of the program
to be \t{nPackets == nPacketsOld}.
Build the instrumented program for the specification \t{lock.spc} and run \blast.
\blast\ should now provide an error trace.
\end{enumerate}
\end{exercise}

\subsection{Reachability}

\blast\ implements an engine that checks whether a certain line of the C source
code is reachable.
Internally, the specifications are used to instrument the code such that
failure of the specification amounts to reaching a particular error label.
Such a transformation is possible for any safety property.
Thus, the basic command for running \blast\ is "\t{blast.opt prog.c -main start -L errlabel}". 
This runs the model checker on the program \t{prog.c} and checks for
reachability of the error label \t{errlabel} from the function \t{start}.
The run ends with either an error trace or with a message that the error
was unreachable (or, since the reachability problem is undecidable, the program may not terminate).
The defaults for \t{start} and \t{errlabel} are \t{main} and \t{ERROR} respectively.
Therefore, invoking \blast\ with \t{pblast.opt prog.c} checks whether the 
program label \t{ERROR} is reachable, when execution begins in the function \t{main}.

This also means that you can bypass the specification language and add
your own annotations directly to the code, and have a special error label
to signify violation of the property you are checking.
However we recommend the use of the specification language since we have found it
useful to have specifications separate from the implementation.
For example, the same specification can be checked for many programs.


\subsection{Additional Predicates}

Now consider the following example.

\begin{verbatim}
void main () {
  int x, y;
  int __BLAST_NONDET;
  x = 0;
  y = 0;
  while (__BLAST_NONDET) {
    x ++ ;
    y ++ ;
  }
  while (x > 0) {
    x -- ;
    y -- ;
  }
  assert (y == 0);
}
\end{verbatim}

The variable \t{\_\_BLAST\_NONDET} has special internal meaning:
it is treated as a nondeterministic choice. This is often useful
to model nondeterministic behavior for stub functions.
If you run \blast\ on this example, it fails with the error message

\begin{verbatim}
No new predicates found. Aborting.
Exception raised :(Failure("No new preds found !-- and not running allPreds ...")
\end{verbatim}

This is because the predicate discovery engine of \blast\ is not powerful to
infer the suitable predicate \t{x==y} from this example.
In such a case, the user can give additional useful predicates to \blast.
Additional predicates can be given using a {\em predicate file}.
A predicate file contains a list of useful predicates separated by semicolons
that \blast\ reads in at
the start of the model checking.
Each predicate is an atomic predicate written in C syntax, but where the local variable
\t{x} in function \t{f} is written as \t{x@f} (global variables \t{y} are still
written \t{y}). 
In this example, we can create a file \t{foo.pred} with the single entry
\t{x@foo == y@foo;} (the semicolon at the end is necessary).
We then run \blast\ with 
\begin{verbatim}
pblast.opt -pred foo.pred foo.i
\end{verbatim}
where \t{foo.i} is the preprocessed file produced as in previous examples.
The \t{-pred foo.pred} option tells \blast\ to read in the predicates from
the file \t{foo.pred}.
Now \blast\ runs and reports that the program is safe.
The choice and implementation of suitable widening operations 
to automatically infer suitable predicates 
in \blast\ is still an open
area of research.

Of course, additional predicates can be given for any program, not necessarily
those on which \blast\ fails. Our experience
is that it is often useful to start with the predicates generated from
the guards of the specification.
Therefore, the instrumentation of the specification also generates a predicate
file called \t{instrumented.pred}.


\subsection{Smarter Predicate Discovery}

The default predicate discovery engine in \blast\ may not always succeed in finding
``good'' predicates.
Therefore, \blast\ implements some additional analysis for finding suitable predicates.
These can be accessed with the command line options \c{-craig 1}, \c{-craig 1 -scope},
and \c{-craig 2}.
These implement predicate discovery based on interpolants.
The option \c{-craig 1 -scope} is an efficiency heuristic on top of \c{-craig 1},
it removes predicates not in scope.
The option \c{-craig 2} does a precise analysis.
On the first run, \c{-craig 2} often takes more time than \c{-craig 1}, but it
produces a much finer abstraction.
Even when the default predicate discovery succeeds, the \c{-craig} options often
succeed with fewer predicates.
Additional heuristics are implemented, and can be accessed with the \c{-predH} flag.
We recommend running \blast\ with \c{-predH 7} (level $7$ is the highest level).

Consider for example the program \t{foo.c}:
\begin{verbatim}
#include <assert.h>

int main() {
  int i, x, y, ctr;

  x = ctr;
  y = x;
  y = x + 1;
  if (ctr == i) {
    assert (y == i + 1); 
  }
}
\end{verbatim}
Let \t{foo.i} be the preprocessed program as above.
Running \t{pblast.opt foo.i} does not succeed, and says ``No new predicates found.''
However, running
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7
\end{verbatim}
verifies that the program is safe.

\subsection{Saved Abstractions}

When \blast\ finishes running on the program filename.c, it creates a file called filename.abs
containing the abstraction that was used.
This abstraction can be used in subsequent runs to save time in predicate discovery.
Subsequent runs may be required in a regression testing scenario where some parts
of the program has changed, and \blast\ is run to verify the property on this new program.
The old abstraction file can be read in with the \c{-loadabs} flag.
For example, after we have run 
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7
\end{verbatim}
a file called \t{foo.abs} is created.
Subsequently, running
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7 -loadabs
\end{verbatim}
will reuse the previous abstraction and do the verification much faster.

