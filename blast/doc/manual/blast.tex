\documentclass{article}

\usepackage{hyperref}
\usepackage{hevea}
\usepackage{color}

\def\secref#1{Section~\ref{sec-#1}}
\def\chref#1{Chapter~\ref{ch-#1}}

\def\apiref#1#2#3{\ahref{api/#1.html\##2#3}{#1.#3}}
\def\moduleref#1{\ahref{api/#1.html}{#1}}

% Use this to refer to a Cil type/val
\def\ciltyperef#1{\apiref{Cil}{TYPE}{#1}}
\def\cilvalref#1{\apiref{Cil}{VAL}{#1}}
\def\formatcilvalref#1{\apiref{Formatcil}{VAL}{#1}}
\def\cilvisit#1{\apiref{Cil.cilVisitor}{#1}}
\def\cilprinter#1{\apiref{Cil.cilPrinter}{#1}}

% Use this to refer to a type/val in the Pretty module
\def\ptyperef#1{\apiref{Pretty}{TYPE}{#1}}
\def\pvalref#1{\apiref{Pretty}{VAL}{#1}}

% Use this to refer to a type/val in the Errormsg module
\def\etyperef#1{\apiref{Errormsg}{TYPE}{#1}}
\def\evalref#1{\apiref{Errormsg}{VAL}{#1}}

%----------------------------------------------------------------------
% MACROS

\def\@envspa{{\hspace{0.3em}}}
\newtheorem{@proexe}{Exercise}
\newenvironment{exercise}{\begin{@proexe}\@envspa\rm}{\end{@proexe}}

\def\blast{{\sc Blast}}

\newcommand{\hsp}{\hspace{0.5in}}
\def\t#1{{\tt #1}}
\newcommand\codecolor{\ifhevea\blue\else\fi}
\renewcommand\c[1]{{\codecolor #1}} % Use for code fragments

%----------------------------------------------------------------------
\title{BLAST}

% Make sure that all documents show up in the main frame
%HEVEA \AtBeginDocument{\@print{<base target="main">}}

\begin{document}
\maketitle

\section{Introduction}

\blast\ ({\bf B}erkeley {\bf L}azy {\bf A}bstraction {\bf S}oftware
Verification {\bf T}ool) is a verification system for checking safety properties
of C programs.
\blast\ implements an abstract--model check--refine loop to check for reachability
of a specified label in the program. The abstract model is built on the fly using
predicate abstraction. This is model checked. If there is no path to the specified
error label, \blast\ reports that the system is safe. Otherwise, it checks if the
path is feasible using symbolic execution of the program. If the path is feasible,
\blast\ outputs the path as an error trace, otherwise, it uses the infeasibility
of the path to refine the abstract model. The algorithm of \blast\ is described in
the paper ``Lazy Abstraction'' (by Thomas A. Henzinger, Ranjit Jhala, Rupak Majumdar,
and Gregoire Sutre, in Proceedings of the ACM SIGPLAN-SIGACT 
Conference on Principles of Programming Languages, pages 58-70, 2002).

\blast\ is relatively independent on the underlying machine and compiler. 
However, \blast\ has only been tested on Intel x86 using the Ocaml (Version 3.04) 
compiler on Linux and Microsoft Windows under cygwin.
A Postscript version of this document is also available \href{blast.ps}{here}.

\section{Download and Installation}

\blast\ can be downloaded from \blast{}'s \ahref{../feedback.shtml}{download web page}. 

\subsection{Downloading Binaries}

You can download executable versions of \blast\ for Linux and
Windows under Cygwin.
You should independently download and install the 
\href{http://research.compaq.com/SRC/esc/Simplify.html}{Simplify Theorem Prover} (see item 5
below). \blast\ requires that the Simplify theorem prover is in the path.

\subsection{Downloading the Source Distribution}

You will need \href{http://pauillac.inria.fr/ocaml}{OCaml} release 3.02 or higher to build \blast. 
\blast\ has been tested on Linux and on Windows with cygwin.
If you want to use \blast\ on Windows then you must get a complete installation
of {\tt cygwin} and the source-code OCaml distribution and compile it yourself
using the cygwin tools (as opposed to getting the Win32 native-code version of
OCaml). If you have not done this before then take a look
\href{setup.html}{here}. 
%(Don't need to worry about {\tt cvs} and
%{\tt ssh} unless you will need to use the master CVS repository for \blast.)

\begin{enumerate}
\item Download the \blast\ source distribution.
\item Unzip and untar the source distribution. This will create a directory
      called \t{blast-1.0} whose structure is explained below. \\
      \hsp\verb!tar xvfz blast-1.0.tar.gz!
\item Enter the \t{blast-1.0} directory and run 
      GNU make to build the distribution. \\
      \hsp\verb!cd blast-1.0!\\
      \hsp\verb!make distclean!\\
      \hsp\verb!make!\\

\item You should now find the executables \t{pblast.opt} and \t{spec.opt} in the directory \t{bin}.
These are symbolic links to files of the same name in the directory \t{psrc} and \t{spec} respectively.
The executable \t{pblast.opt} is the \blast\ executable, the executable \t{spec.opt} is 
the specification instrumenter.

\item You should also download and install the
\href{http://research.compaq.com/SRC/esc/Simplify.html}{Simplify Theorem Prover}.
This involves putting the  
executables \t{Simplify} (Linux)
and \t{Simplify.exe} (Windows) in the  \t{bin} directory.
Additionally, \blast\ has interfaces to the \href{http://verify.stanford.edu/CVC/}{Cvc Theorem Prover}, 
should you
wish to install and use it as the back end.
Again, this involves putting the executable for Cvc in the \t{bin} directory. 
Note that in order for \blast\ to use Simplify or Cvc, the executable for Simplify
and Cvc
must be in your current path.
It is a good idea to add the \blast\ \t{bin} directory to your path.

\item \blast\ also comes with an independent GUI. In order to install the GUI, you must download and
install the \href{http://wwwfun.kurims.kyoto-u.ac.jp/soft/olabl/lablgtk.html}{LablGTK} package
in addition to Ocaml. After you have installed LablGTK, you can build the GUI by going to the
\t{blast-1.0} directory and typing:\\
      \hsp\verb!make gui!\\
This will create the GUI executable \t{blastgui.opt} in the directory \t{bin}.

\item \blast\ (actually the GUI) requires the use of the environment variable \t{BLASTHOME}.
Therefore you should set the environment variable \t{BLASTHOME} to point to the directory
\t{blast-1.0} where you have downloaded \blast.

\item Congratulations! You can now start using \blast.
\end{enumerate}


\subsubsection{Using the Foci Interpolating Theorem Prover}

Owing to copyright issues, we cannot release the source of the interpolating theorem
prover that \blast\ uses for smart predicate discovery.
This means that the option \c{-craig} will be unavailable in the version that can be built
from the source. 
%Therefore we are providing the \blast\ executables separately to allow users to use the
%\c{-craig} option.
%You can download executable versions of \blast\ for \href{linux_exe.tgz}{Linux} and
%\href{cygwin_exe.tgz}{Windows under Cygwin}.
Use the executables provided above if you want to use \c{-craig}.
We are working towards resolving the copyright issue to allow the use of the Foci Interpolating
Theorem Prover sources.


\input{gui}
%input{shell}

\section{The Specification Language}\label{speclang}

\input{specmanual}

\section{Using \blast: User Options}\label{sec-using-blast}


 The following command line options are useful for running \blast\
(see \t{pblast.opt -help} for a complete list).

   
\paragraph{Model Checking Options.}
The following options are available to customize the model checking run.
\begin{itemize}
\item \c{-main xxx}. Specify the entry point of the program. The default
is \t{main}.

\item \c{-L xxx}. Specify an error label. The default is \t{ERROR}.
Note that if there are several labels in the program with the same name,
the effect of \blast\ is nondeterministic.

\item \c{-msvc}. Parse file in MSVC mode. This is required by the CIL front end.
The default mode is GNU CC. Use this to read and analyze programs that use Microsoft
Visual C features.

\item
\c{-bfs} and \c{-dfs}. Specify the search strategy, breadth first or depth first.
The default is \c{-bfs}.

\item  \c{-pred xxx}. Specify a set of seed predicates as the initial abstraction.
When this is not specified, \blast\ starts running with the most abstract program:
where all the data has been abstracted.
The seed predicate file \t{xxx} contains a list of seed predicates, each predicate
is an atomic predicate using the C expression syntax.
See \secref{seed-preds} for the syntax of predicates.

\item \c{-cf}. Use context free reachability. This feature has not been 
tested for this release.

\item 
\c{-init xxx}. Specify the initial state in the file \c{xxx}.
The initial state is a (side effect free) C boolean expression in the 
program variables in scope in the \c{start} function.

\item 
\c{-inv xxx}. Specify additional invariants in a file. These are conjoined with
the set of reachable states.
The invariant file contains a C boolean expression.

\item  \c{-s xxx}. Specify the satisfiability solver to be used by the decision procedures
in \blast. The current options are Vampyre, Simplify, and Cvc, the default being Simplify.
Note however that the option \c{-craig} uses Vampyre internally.
See the programmer's documentation to see how to add your own decision procedure.
We recommend the use of Simplify as default as it is considerably faster than Vampyre.

\item
\c{-nocache}. Do not cache theorem prover calls. This makes the run require less
memory.

\item
\c{-cov}. With the option \c{-cov}, check for coverability is done only for
control flow nodes that have back pointers.

\item
\c{-reclaim}. A space saving heuristic: does not keep whole tree around during
the model checking, reclaims tree nodes on backtrack.

\item  \c{-ax xxx}. Specify a set of additional axioms for Simplify.
The axioms file is passed to Simplify. Simplify requires that the file
ends in ``.ax''. So, to pass the axiom file \c{file.ax}, say \c{-ax file}.
It is assumed that the axiom file is in the same directory from which
\blast\ is invoked.

\item
\c{-alias xxx}. Alias analysis options.
	If the option \c{xxx} is \c{bdd}, invokes a bdd based alias analysis,
	otherwise reads the alias relationships from the file \c{xxx}. 
Additionally, the option
  \c{-pta [flow|noflow]} specifies a mode for points to analysis: 
\c{noflow} runs a flow insensitive version, \c{flow} a flow sensitive version.
	This version of \blast\ only supports \c{noflow}.
	If this option is omitted, then no alias analysis is performed,
and \blast\ makes the assumption that lvalues are not aliased.
The option
  \c{-scheck} then performs some checks to ensure that this assumption is
met. However, it can happen that the analysis is unsound, even though \c{-scheck}
does not fail.

\item
\c{-incref}. \blast\ with full alias analysis is expensive, especially when the
alias analysis is imprecise.
Often aliasing is not important in This option gets a middle ground: the counterexample
analysis is done without the aliasing information, but the model checking uses
the alias information.
The analysis is sound, but \blast\ can fail if the analysis requires aliasing relationships
to be tracked.

\item \c{-nofp}. Ignore function pointer calls. This is put in as a convenience:
the programmer can ignore function pointer calls in the analysis. Use with caution:
the results of \blast\ are meaningful only under the assumption that the function
pointer calls did not change the predicate state of the program.




\item
\c{-bddcov} and \c{-nobddcov}. The option \c{-bddcov} uses only bdds in cover check,
\c{-nobddcov} does a full cover check. The default is \c{-bddcov}. It is unlikely you
will need to change the default.


\item  
\c{-wpsat}.  Keep only satisfiable disjuncts in the weakest precondition. 

\item
\c{-restart}. Restart model checking after every counterexample analysis (a.k.a. SLAM mode).


\item
  \c{-block}. Analyze trace as blocks. This is the default, overriding the earlier counterexample
analysis.
You can still use the old counterexample analysis by running 
\c{-noblock}.
With the old analysis, you can also specify the direction of the analysis
with the flag \c{-refine [fwd|bwd]} that runs the analysis forwards or backwards,
according to the option provided.

\item
\c{-predH k}. Perform predicate discovery heuristics.
There are several levels, we recommend using level $k=7$.
The heuristics include checking all causes of unsatisfiability, and adding
additional predicates based on the syntax.

\item
\c{-tsd [zig]}. Set trace search direction. [zig] goes zigzagging [] restarts from the end.

\item
\c{-craig [1|2]}. Use craig interpolants (FOCI) to get predicates 
([1] use with scope, [2] maintains local tables of predicates). 
See the paper ``Abstractions from Proofs'' (by T.A. Henzinger, R. Jhala, R. Majumdar, and
K.L. McMillan).
\c{-craig} internally uses the block based counterexample analysis (i.e.,
automatically sets \c{-block}).
See also \c{-scope}.

\item
\c{-scope}. Remove predicates not in scope. This should be run with
\c{-craig 1}, otherwise this fails.
\end{itemize} 
  
   

\paragraph{Program Optimization Options.} \blast\ implements a set
of program analysis routines that can make the analysis run significantly faster.
These can be turned on or off with the following options.

\begin{itemize}
%\item  \c{-subgoal xxx}. Specify a subgoal file. This is used to break
%a proof into smaller parts. This is still experimental, and not included
%in the current release.

\item \c{-pe}. Implements an aggressive interprocedural constant propagation
algorithm.

\item  \c{-O x}. Turn program optimizations on or off. The levels are $0$-$1$.
The default is $0$ (off). In level~$1$, a cone of influence optimization
is implemented. It assumes all variables occurring in conditionals are important,
and propagates this to find all useful assignments.
Further, the constant propagation algorithm is implemented (see also \c{-pe}).
%In level~$2$, we get rid of useless conditionals in addition to the analyses
%of level~$1$. This is done as follows: the variables appearing in the seed
%predicates file are deemed important. The conditional in an \t{if} statement
%is dropped (i.e., changed to nondeterministic choice) if neither the then branch
%nor the else branch (transitively) affects any important variable. Note that
%this assumes all specification variables are named in the seed predicates file.
%Note also that this optimization introduces infeasible paths in the program, and
%can lead to reporting error traces that are in fact not possible. 
%(The option \c{-O 2} is buggy. Do not use.)

\item  \c{-depth k}. Unroll CFA to depth $k$. This is experimental and not included
in the release.
\end{itemize}
   
\paragraph{Parallel Model Checking and Races.}
\blast\ implements a {\em Thread modular} algorithm for
checking races in multithreaded C programs.
These options relate to the algorithm for checking races.

\begin{itemize}
\item
\c{-checkRace}. Invoke the TAR algorithm to check for races on shared variable accesses.

\item
\c{-par}. Use data structures for parallel \blast. Not supported.
\end{itemize}
   
\paragraph{Saved Abstractions and Summarization.}
These options are used to save and load abstractions from a \blast\ run.
\begin{itemize}
\item
\c{-loadabs}.  When \blast\ is run with the file \t{fname.c}, 
it continuously outputs the abstraction used in the model checking
into the file \t{fname.abs}.
The \c{-loadabs} option can be used to read back the abstraction file
created in a previous run. This makes subsequent runs faster and is useful
for regression testing.
This option also allows us to run \blast\ with the abstraction generated
from an interrupted run.


\item
\c{-interface f}. Specify name of a function to take as describing a component interface.
Not supported in this release.

\item
\c{-component f}. Specify name of a function to check for satisfying the interface.
Not supported in this release.

\end{itemize}

\paragraph{Proof generation options.} \blast\ implements a set of options to generate
PCC style proofs. The proofs are output in textual form in LF syntax. These can be
read and encoded by a standard PCC proof encoder.
\begin{itemize}
\item \c{-pf}. Generate proof tree in the file \t{foo.tree}.

\item \c{-pfgen}. Spit out vampyre proofs. Always use this: the other option is buggy!

\item \c{-pffile xxx}. File to write vampyre proofs. If no file is specified,
proofs are written to \t{/tmp/lf.pfs}.
\end{itemize}

\paragraph{Old Heuristics that are no longer used/supported.}
You can omit reading about the options in this section.
These pertain to several heuristics in the older version.
The default is set to the heuristic that we found to work best.
Many of the following heuristics are no longer supported.

\begin{itemize}

\item \c{-comp xxx}. This implements the \t{keep\_subtree} heuristic from the
Lazy Abstraction paper. The options are \c{cut} to remove the subtree, and \c{path}
to keep the subtree. We found that \c{path} was nominally faster, but went into
loops very often. So the default is set to \c{cut}.

\item \c{-post xxx}. The algorithms to compute post. The options are \c{slam}
for an approximate {\it Cartesian post} as implemented in SLAM. 
and \c{H} for the most precise predicate abstraction.
We saw that \c{slam} post is vastly more efficient, yet is precise enough
to prove properties of interest. The default is \c{slam}.

\item \c{-forget}. This option naively forgets predicates found when it
backtracks out of a subtree. This repeats work: the next time the same
part of the program is visited, the same predicates are found again. The
default is not to forget.

\item  \c{-dc}.  Some don't care heuristic for predicates not in scope.
Deprecated. Do not use, see \c{-craig} instead.


\end{itemize}

\paragraph{General Options.}
The following options let the user select different
configurations, mostly for debugging.

\begin{itemize}
\item \c{-debug}. Prints out copious debugging information.

\item
\c{-cfa-dot xxx}. Output the CFA of every function in ATT dot format
in the file \c{xxx}.


\item
\c{-stop}. Stop when the model checker hits the first (possibly invalid) counterexample.
Useful for debugging.

\item \c{-traces}. Every time a false counterexample is encountered,
the trace itself is dumped. Used for diagnostic purposes.
With the option \c{-tracefile xxx}, you can additionally specify the name
of the file containing trace information. This is used by the trace viewer.
%  -file  file containing trace information -- for trace viewer

\item  
\c{-demo}. Run in demo mode for the GUI.

\item \c{-xml}. Generate error traces as a bunch of xml files that can be read in and
displayed by SLAM's GUI.

\item
  \c{-help} or 
  \c{--help}. Display the list of options.
\end{itemize}


%a section devoted to GUI details.
\section{Graphical User Interface}
\blast comes with a rudimentary whose chief purpose is to make it easier to view
counterexample traces. In this section we discuss the GUI.

The GUI is started by the command {\tt blastgui.opt}.

Source and predicate files are loaded in using  {\tt File} in the main toolbar,
or by entering the filenames in the appropriate text boxes and clicking the load button.
There are four sub-panes showing respectively a
 log of events, the source file, the predicate file
and counterexample traces.

To run \blast, the user must first select the source file and then optionally
a predicate file and then type in the options in the text pane labelled options, 
and click the {\tt Run} button. If the system is free of errors, \blast will (hopefully)
pop up a window saying so, if not, it will (hopefully) switch to the counterexample trace 
pane showing a counterexample that violates the specification. We say hopefully as it is 
possible as we saw before that \blast will be be stuck at some point unable to find the right
predicates to continue. In this case also, the GUI moves to the counterexample trace pane
which now shows a trace on which \blast is stuck -- the user can then stare at the trace and
guess some predicates which can then be fed to \blast.

\subsection*{The Counterexample Trace Pane}
The counterexample trace pane is broken into 3 subpanes -- the leftmost is the program source,
the middle pane is the sequence of operations that is the counterexample and the rightmost pane
contains the state of the system given by values for the various predicates in the top half
and the function call stack in the lower pane
at the corresponding points in the counterexample. One can see the state of the system at
different points of the trace by clicking on the corresponding operation in the middle pane.
When one chooses an operation in the middle pane, the corresponding program text is highlighted
in the left pane and the predicate values and control stack are shown in the right pane.
Alternatively, one can go back and forth along the trace using the arrows along the bottom.
\begin{itemize}
\item[FW/BK]
The first pair move to the next and previous operation,
\item[Undo/Redo] The second pair are like 
web-browser buttons in that they move to the previous or next operation among the various
operations that have been hitherto selected, 
\item[FF/REW] The third pair move to the next or previous point in the trace where the values
of the predicates are different from the present values -- thus they skip over irrelevant 
operations and proceed directly to the nearest operation that changes the values of the 
predicates.
\item[Pred Change] The fourth pair are used after first selecting some predicate 
in the rightmost pane -- after selecting the predicate if one clicks the right (left) button
one is taken to the first operation in the future (past) where the value of that predicate is 
about to (has just) change(d). This pair is used to see which operations cause a predicate to 
get its value at some point in the trace.
\end{itemize}

\section{Modeling Heuristics}

\subsection{Nondeterministic Choice}\label{sec-nondet}

\blast\ uses the special variable \t{\_\_BLAST\_NONDET} to implement
nondeterministic choice. Thus, 
\begin{verbatim}
if (__BLAST_NONDET) {
	// then branch
} else {
	// else branch
}
\end{verbatim}
is treated as a nondeterministic \t{if} statement whose either branch
may be taken. This is sometimes useful in modeling nondeterministic
choice in specification functions or in models of library functions.

\subsection{Stubs and Drivers}

\blast\ is essentially a whole program analysis.
If there are calls in your code to library functions, it expects to 
see the body of the function.
If the body of a function is not present, \blast\ optimistically assumes
that the function has no effect on the variables of the program other
than the one in which the return value is copied.

Sometimes we are interested in the effect of library functions, but not in
their detailed implementation.
For example, we may be interested in knowing that \t{malloc} returns
either a null pointer or a non-null pointer, without knowing exactly
how memory allocation works.
This is useful for scalability: we are abstracting unnecessary details
of the library.
Sometimes this is necessary as well: certain system services are written
in assembly and not amenable to our analysis.

\blast\ expects in these cases that the user provides stubs for useful
library functions.
Each stub function is basically a piece of C code, possibly with the use
of \t{\_\_BLAST\_NONDET} to allow nondeterministic choice.



\subsection{Syntax of Seed Predicates}\label{sec-seed-preds}

You can input initial predicates on the command line using the
option \c{-pred}. This section gives the syntax for input predicates.
The format of the predicate file is a list of predicates, separated
by semicolons.
Each predicate is a valid boolean expression in C syntax.
However, we change variable names to also reflect the scope of the variable.
So the variable \t{x} in function \t{foo} is written \t{x@foo}.
The detailed syntax can be seen in the file \t{inputparse.mly} in the directory
\t{psrc}.

Notice that if the same syntactic name is used for multiple variables in different scopes 
then Cil renames the local variables.
In this case, one has to look at the names produced by Cil 
to use the appropriate variable in the predicates.

\section{Aliasing}

Pointer aliasing is a major source of complexity in the implementation
of \blast.
\blast\ comes with a flow insensitive and field insensitive
Andersen's analysis for answering pointer
aliasing questions internally.  
The implementation of the pointer analysis uses BDDs.
Additionally, \blast\ allows the user to input alias information (generated
from some other alias analysis) from a file.
The syntax of an alias file is a list of C equalities between C memory expressions
(variables, dereferences, field accesses) separated by commas.

Considering possibly aliased lvalues is essential for soundness of the analysis.
Consider for example the following code:
\begin{verbatim}
int main() {
  int *a, *b;
  int i;
  i = 0;
  a = &i;
  b = &i;
  *b = 1 ;
  assert(*a == 1);
} 
\end{verbatim}
If the analysis proceeds without considering the alias relationship 
between \t{*a} and \t{*b}, the assertion passes.
However, updating \t{*b} also updates \t{*a}.
The analysis is expensive if the alias information is not precise,
since all (exponentially many) alias scenarios between the variables
must be considered.
In order to improve precision, 
\blast\ makes the (possibly unsound) assumption that the program is 
type-safe, so that only variables of the same type may be aliased.
Moreover, the current implementation does not handle function pointers.
Code with function pointer calls therefore cause \blast\ to fail with
an exception, unless the flag \c{-nofp} is used, in which case 
all function pointer calls are ignored.
 
The option \c{-alias} is used to provide aliasing information to \blast.
The option takes a string argument.
If the argument is \c{bdd} then the BDD based Andersen's analysis is run.
If it is some other string, then \blast\ assumes that the string indicates
a filename where aliasing relationships are given.
If the alias option is omitted, \blast\ makes the unsound assumption that
there are no aliases in the program. 

\begin{exercise}{}
Consider the program
\begin{verbatim}
#include <assert.h>

int __BLAST_NONDET;


void swap1(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

void* malloc(int k);

void main () {
	
  int *i, *j;

  int v1, v2;

  i = malloc(4);
  j = malloc(4);

  *i = v1;
  *j = v2;

  swap1 (i, j);
  swap1 (i, j);
  assert (  *i == v1 &&   *j == v2 ); 
}
\end{verbatim}
\begin{enumerate}
\item
Run \blast\ with 
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7
\end{verbatim}
There is an error trace because \blast\ does not consider the aliasing
among the variables.
Now run \blast with 
\begin{verbatim}
pblast.opt -alias bdd foo.i -craig 1 -predH 7
\end{verbatim}
\blast\ says that the system is safe.
\item
Now comment out the second call to \t{swap1} in \t{main}.
Check that \blast\ produces an error trace.
\item 
Now add a second swap routine
\begin{verbatim}
void swap2(int *a, int *b) {
  *a = *a + *b;
  *b = *a - *b;
  *a = *a - *b;
} 
\end{verbatim}
Replace one of the calls to \t{swap1} with \t{swap2}.
Verify that \blast\ still proves the program correct.
\item
Consider the following variant of \t{main}.
\begin{verbatim}
void main () {
	
  int *i, *j;

  int v1, v2;

  i = malloc(4);
  j = malloc(4);

  *i = v1;

  swap1 (i, i);
  assert (  *i == v1 ); 
}
\end{verbatim}
Does the assertion hold? What happens if you replace \t{swap1} with \t{swap2}?
Run \blast\ and verify in each case.
\end{enumerate}
\end{exercise}

\section{Programmer's Manual}\label{sec-prgrammers-manual} 

\subsection{Architecture of \blast}\label{sec-blastArch}

\blast\ uses the \href{http://manju.cs.berkeley.edu/cil}{CIL infrastructure}
as the front end to read in C programs. The programs are internally represented
as {\it control-flow automata} (implemented in module \c{CFA}). 
Sets of states are represented by the \c{Region} data structure. The \c{Region}
module represents sets of states as boolean formulas over a set of base predicates
and allows boolean operations on regions, and checks for emptiness and inclusion.
The \c{Abstraction} functor takes the \c{Region} module and the \c{CFA} module, 
providing in addition (concrete and abstract) {\it pre} and {\it post} operations,
and methods to analyze counterexamples.
Using the \c{Abstraction} module, the \c{LazyAbstraction} functor implements the model
checking algorithm at a high level of abstraction.

\blast\ uses the \href{http://research.compaq.com/SRC/esc/Simplify.html}{Simplify Theorem Prover} 
and the \href{http://www.eecs.berkeley.edu/~rupak/Vampyre}{Vampyre Proof-Generating Theorem
Prover} as underlying decision procedures.
Boolean formula manipulations are done using the 
\href{http://vlsi.colorado.edu/~fabio/cudd}{Colorado University Decision Diagram} package.

\subsection{API Documentation}\label{sec-api}

The architecture of \blast\ is described in the file \t{src/blastArch.ml}.
We also have an \href{api/index.html}{online documentation} extracted from
the code. We
index below the main types that are used to represent C programs in CIL: 

\begin{itemize}
\item \href{api/index\_types.html}{An index of all types}
\item \href{api/index\_values.html}{An index of all values}
\end{itemize}


 
\section{Known Limitations}

\begin{enumerate}
\item 
The current release does not support function pointers.
With the flag \c{-nofp} set, you can disregard all function
pointer calls. The correctness of the analysis is then modulo
the assumption that function pointer calls are irrelevant to
the property being checked.

\item {\bf Recursive functions.} Currently \blast\ inlines function calls.
This means that it loops on recursive calls. This is a big limitation on 
the files that can be analyzed. 
The option \c{-cf} implements context-free reachability. However it has
not been tested.

\item There are several bugs in the options \c{-craig [1|2]}.
If \c{-craig} should fail, we suggest you run \blast\ without this
option and check.

\end{enumerate}



\section{Authors}

\blast\ was developed by 
\href{mailto:beyer@eecs.berkeley.edu}{Dirk Beyer},
\href{mailto:adamc@eecs.berkeley.edu}{Adam Chlipala},
\href{mailto:tah@eecs.berkeley.edu}{Tom Henzinger},
\href{mailto:jhala@eecs.berkeley.edu}{Ranjit Jhala},
\href{mailto:rupak@eecs.berkeley.edu}{Rupak Majumdar},
and \href{mailto:sutre@labri.fr}{Gregoire Sutre},
with contributions from (among others) 
Yinghua Li, Ken McMillan, Shaz Qadeer, and Westley Weimer.

\section{Troubleshooting}

\begin{enumerate}
\item \blast\ fails with
\begin{verbatim}
Failure(``Simplify raised exception End_of_file'')
\end{verbatim}
Is Simplify in your path?

\item
\blast\ fails with
\begin{verbatim}
Failure(``convertExp: This expression Eq should not be handled here'') 
\end{verbatim}
\blast\ does not like expressions like 
\begin{verbatim}
return (x==y);
\end{verbatim}
Change this to
\begin{verbatim}
if (x==y) return 1; else return 0;
\end{verbatim} 
Similarly, change 
\begin{verbatim}
a = (x==y) ;
\end{verbatim}
to
\begin{verbatim}
if (x==y) a = 1; else a = 0;
\end{verbatim} 
and similarly for the other relational operators $<=$, $>=$, $>$, $<$, $!=$.
\end{enumerate}

Don't see your problem? Send mail to \href{mailto:blast@ic.eecs.berkeley.edu}{blast@ic.eecs.berkeley.edu}.

\section{Bug reports}
 
We are certain that there are still bugs in \blast. If you find
one please send email to \blast\ at 
\href{mailto:blast@ic.eecs.berkeley.edu}{blast@ic.eecs.berkeley.edu}
or to \href{mailto:rupak@eecs.berkeley.edu}{Rupak Majumdar}
or \href{mailto:jhala@eecs.berkeley.edu}{Ranjit Jhala}.  

\section{Changes}\label{changelog}

%
%\begin{thebibliography}{HJMS02}
%
%\bibitem{BMMR01}
%T.~Ball, R.~Majumdar, T.~Millstein, and S.K. Rajamani.
%\newblock Automatic predicate abstraction of {C} programs.
%%\newblock {\em Conf. Programming Language Design and Implementation},
%  pp. 203--213. ACM, 2001.
%
%%\bibitem{SLAMPOPL02}
%%T.~Ball and S.K. Rajamani.
%%\newblock The {\sc Slam} project: debugging system software via static 
%%  analysis.
%%\newblock {\em Symp. Principles of Programming Languages}, pp. 1--3.
%%  ACM, 2002.
%
%\bibitem{DDP99}
%S.~Das, D.~L. Dill, and S.~Park.
%\newblock Experience with predicate abstraction.
%\newblock {\em Computer-Aided Verification}, LNCS 1633, pp.
%  160--171. Springer-Verlag, 1999.
%
%\bibitem{HJMS02} 
%T.A. Henzinger, R.~Jhala, R.~Majumdar, and G.~Sutre.
%\newblock Lazy abstraction.
%\newblock {\em Symp. Principles of Programming Languages}, pp. 58--70.
%  ACM, 2002.
%
%\end{thebibliography}
\end{document}



