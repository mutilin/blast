
\section{A Tutorial Introduction to \blast}\label{tutorial}

In this section we show how \blast\ can be used to check safety properties of
C code.
We shall discuss how to specify safety properties in the \blast\ specification
language, and the various options that \blast\ takes to check C code against
the specification.
\blast\ can be run both from the command line and from a GUI.

\subsection{Assertion Checking}

The simplest form of safety properties are {\em assertions}
that specify invariants of the program.
The programmer writes \begin{verbatim}assert (e)\end{verbatim} in the code,
where {\tt e} is a C expression.
If the expression {\tt e} evaluates to $0$ at run time, the program aborts.
The intent is that the programmer has reasoned that {\tt e} is an invariant
of the program at the place the assert has been introduced.
Consider for example the following piece of code.


\begin{verbatim}
#include <assert.h>

int foo(int x, int y) {
	if (x > y) {
	  x = x - y;
	  L: assert(x > 0);
        }
}
\end{verbatim}

By the constraints introduced by the checks, the programmer knows that at the label
{\tt L}, the value of {\tt x} is greater than $0$.
Assertions are typically checked at run time during the testing phase of a program.
However, using \blast, one can check assertions statically, during compile time.
Moreover, there is no explicit test case to be written.

To run the above example through \blast, you must do the following.
First, instead of the system header file {\tt assert.h}, you must use the {\tt assert.h}
file provided with \blast\ (in the directory {\tt test/headers} of the distribution.
Then, you must produce a preprocessed file containing the source code.
The above file is in {\tt test/tutorial/tut1.c}.
Create a preprocessed file by using the commands:

\begin{verbatim}
gcc -E -I $BLASTHOME/test/headers/ tut1.c > tmp
mfilter tmp > tut1.i
\end{verbatim}

This creates a preprocessed file called {\tt tut1.i}. 
Now we can run \blast\ on this file. 
Note that the program \t{mfilter} simply removes line number information from
the preprocessed file. This helps the GUI, since the GUI presently does not support
loading multiple source files. 
If you are running \blast\ from the command line, you can ignore the mfilter-step.
The program \t{mfilter} is built from the file \t{mfilter.ml} in the \t{blastgui} directory,
and should be present in the \t{bin} directory if you have compiled the GUI. 


\paragraph{Running \blast\ from the Command Line.}
The basic command to run \blast\ is
\verb+pblast.opt filename -main mainfunction -L ErrorLabel+
where \t{filename} is the file you are checking, \t{mainfunction}
is the name of the starting function, and \t{ErrorLabel}
is an error label.
The default for mainfuntion is \t{main}, and ErrorLabel is \t{ERROR},
so e.g., if you are checking a program starting from \t{main}, and
checking reachability of the label \t{ERROR}, you can just write
\verb+pblast.opt filename+.
In our case, the filename is \t{tut1.i}, and the start function is
\t{foo}; the assertion check automatically introduces an error label
\t{ERROR}.
So we invoke \blast\ with
\begin{verbatim}
pblast.opt tut1.i -main foo
\end{verbatim} 
(make sure Simplify is in your path). \blast\ comes back and reports
that the system is safe, i.e., the assertion is not violated.


\paragraph{Running \blast\ from the GUI.}

First, at the command prompt, type
\verb+ blastgui.opt + after making sure that that file is in your path. 
First we must load the file {\tt foo.i}: this is done by clicking on {\tt File}
and then selecting {\tt Load Source} and then selecting the file {\tt tut1.i}.
Next, to run \blast\ on this file, in the text pane labelled ``Command'' 
type \verb+ -main foo+, and then click the button labelled {\tt Run}.

The text written into the command pane is the options that \blast\ is run with
{\tt -main foo} tells \blast\ to start the analysis from the function called
{\tt foo}. Quickly, the tool pops up a window that states that the system is safe,
meaning the asserts never fail. 
In the pane labelled {\tt Log}, 
\blast\ prints out the predicates that it learns and uses to prove the property.
When the model checking is finished, it comes with other statistics 
related to the analysis, the number of iterations, and the number of refinements required
to prove the property.

\paragraph{Error Traces.}

Now consider an erroneous version of the same program --
 where the programmer swaps the variables
{\tt x} and {\tt y} in the subtraction.

\begin{verbatim}
#include <assert.h>

int foo(int x, int y) {
        if (x > y) {
          x = y - x;
          assert(x > 0);
        }
}
\end{verbatim}
Make the above change in the file \verb+tut.c+, and then 
do \\
\verb+ gcc -E -I $BLASTHOME/test/headers/ tut1.c > tut1.i $+.

This time, \blast\ comes back and says that an error is found, and moves
to a pane where the error trace is shown in the middle.
\begin{verbatim}
Size:5

4 :: 4:          Pred(x@foo>y@foo) :: 5
5 :: 5:          Block(x@foo = y@foo  -  x@foo;) :: 6
6 :: 6:          Pred(Not (x@foo>0)) :: 6
6 :: 6:          FunctionCall(__assert("foo.c", 6, "x > 0")) :: -1
\end{verbatim}

The size parameter gives the length of the error trace.
The numbers on the left give the line numbers in the source code for the corresponding
program actions.
Notice that the statement {\tt assert(x>0)} of the source code is expanded
to the two actions

\begin{verbatim}
6 :: 6:          Pred(Not (x@foo>0)) :: 6
6 :: 6:          FunctionCall(__assert("foo.c", 6, "x > 0")) :: -1
\end{verbatim}

that checks the asserted invariant, and calls {\tt \_\_assert} if the
invariant fails.

\begin{exercise}{}
Assertions are also used to specify unreachable code, for example,
at the end of an infinite server loop.
Consider the simple example.
\begin{verbatim}
main () {
  int x,y;
  x = 0;
  y = 0;
  while (x==y) {
    x++;
    y++;
  }
  assert(0);
}
\end{verbatim}
Check that \blast\ finds that the assert is indeed unreachable (note that
\t{assert(0)} always fails).
\end{exercise}

\subsection{Temporal Safety Specifications}

Assertions are a particularly simple and local way to specify program correctness.
More generally, we are interested in temporal safety properties, where we wish
to check that our program satisfies some finite state property.
For example, we might wish to check that a program manipulating locks
acquires and releases locks in strict alternation 
(that is, two calls to lock without a call to unlock in the middle is an error, 
and vice versa).
More generally, we wish to check safety properties concerning the
proper sequencing of program ``events''.
Let us be concrete.
Consider the following program that manipulates locks (in the file {\tt tut2.c}).

\begin{verbatim}

int STATUS_SUCCESS = 0;
int STATUS_UNSUCCESSFUL = -1;
struct Irp {
  int Status;
  int Information;
};

struct Requests {
  int Status;
  struct Irp *irp;
  struct Requests *Next;	
};

struct Device {
  struct Requests *WriteListHeadVa; 
  int writeListLock;
};

void FSMInit() {
// code to initialize the global lock to unlocked state
}

void FSMLock() {
// code for acquiring the lock

}
void FSMUnLock() {
// code for releasing the lock
}

void SmartDevFreeBlock(struct Requests *r) {
// code omitted for simplicity
}

void IoCompleteRequest(struct Irp *irp, int status) {
// code omitted for simplicity
}

struct Device devE;

void main () {
 int IO_NO_INCREMENT = 3;
 int nPacketsOld, nPackets;
 struct Requests *request;
 struct Irp *irp;
 struct Device *devExt;

 FSMInit();
 devExt = &devE;	


 /* driver code */
 do {
   FSMLock();
   nPacketsOld = nPackets; 
	    
   request = devExt->WriteListHeadVa;
	    
   if(request!=0 && request->Status!=0){
     devExt->WriteListHeadVa = request->Next;	
		
     FSMUnLock();
     irp = request->irp;
		
     if((*request).Status >0) {
       (*irp).Status = STATUS_SUCCESS;
       (*irp).Information = (*request).Status;
     } else {
       (*irp).Status = STATUS_UNSUCCESSFUL;
       (*irp).Information = (*request).Status;
     }	
     SmartDevFreeBlock(request);
     IO_NO_INCREMENT = 3;
     IoCompleteRequest(irp, IO_NO_INCREMENT);
     nPackets = nPackets + 1;
   }
 } while (nPackets != nPacketsOld);
 FSMUnLock();
}
\end{verbatim}

For simplicity, we assume that there is just one global lock
that is acquired by a call to \t{FSMLock} and released by
a call to \t{FSMUnLock}.
We wish to check that the function \t{main} calls \t{FSMLock}
and \t{FSMUnLock} in alternation: if there are two successive calls
to \t{FSMLock} without a call to \t{FSMUnLock} in the middle, it
is an error; 
similarly, if there are two successive calls 
to \t{FSMUnLock} without a call to \t{FSMLock} in the middle, it
is an error. 

One way to check this property is to instrument the program manually by adding some
observer variables, and instrumenting the program to update
the observer variables at each interesting program
event.
Then the check for the safety property can be reduced to checking an
assertion on the observer variables.
For example, in the above code, we can add the observer variable
\t{int lockStatus} that tracks the current state of the global lock
(locked or unlocked), and update \t{lockStatus} to $1$ (for locked) 
whenever \t{FSMLock} is called, and update \t{lockStatus} to $0$ (for unlocked)
whenever \t{FSMUnLock} is called.
We also initialize \t{lockStatus} to $0$ in the call to \t{FSMInit}.
Finally, we add the assertion \t{assert(lockStatus == 0);} in the beginning
of \t{FSMLock} (to say that the lock is not held), and the assertion
\t{assert(lockStatus == 1);} in the beginning of \t{FSMUnLock} (to say that the
lock is held).
Then the instrumented program satisfies our property iff the assertions are never
violated.

The problem with this instrumentation is that it is specific for this code, and
tedious to do manually.
Therefore \blast\ comes with a specification language where safety properties
can be specified at a higher level.
The specification is automatically compiled into an instrumented program with
assertions as above.

For our safe locking property, we write the property as follows (in a file called
\t{lock.spc}).
\begin{verbatim}
global int lockStatus = 0;

event {
  pattern { FSMInit(); }
  action { lockStatus = 0; }
}

event {
  pattern { FSMLock(); }
  guard { lockStatus == 0 }
  action { lockStatus = 1; }
}

event {
  pattern { FSMUnLock(); }
  guard { lockStatus == 1 }
  action { lockStatus = 0; }
}
\end{verbatim}
We give an intuitive description of the specification.
The next section gives a detailed account of the specification language.
The declaration \t{global int lockStatus} defines an observer variable
\t{lockStatus} and initializes it to $0$.
The specification is given in terms of program events.
There are three interesting events in this specification:
calls to the functions \t{FSMInit}, \t{FSMLock}, and \t{FSMUnLock}.
Each event is associated with a syntactic pattern in the code.
For example, the pattern \t{FSMLock();} is matched every time there
is a call to \t{FSMLock} in the code.
Each pattern has a guard that must be met when that pattern matches in the
code: for example, when the pattern \t{FSMLock} is matched, the variable
\t{lockStatus} must be $0$.
If not, there is a violation of the property.
If the guard is true, it can be omitted.
Moreover, there is an action associated with the event that specifies how
the observer variables must be updated.

\blast\ comes with an instrumentation tool that takes a specification and a program
and builds an instrumented program from them.
To use this, we say
\begin{verbatim}
spec.opt lock.spc tut2.c
\end{verbatim}
This builds an instrumented program called \t{instrumented.c} as well as a
predicate file \t{instrumented.pred}.


We can now run \blast\ on this file: first, clear the contents of the ``Command'' pane, 
then in the {\tt File} menu load {\tt instrumented.c} as the source file and 
{\tt instrumented.pred}  as the predicate file. Notice that in the Source pane, the source
of this program is given and in the {\tt Predicates} pane are listed the predicates created
from the specification, namely a predicate tracking the values of {\tt lockstatus},
 \verb+ lockstatus == 0, lockstatus == 1 +. 
Now press the {\tt Run} button to run \blast, which reports that the specification
is indeed met by the program.

\begin{exercise}
\begin{enumerate}
\item What predicates are used by \blast\ to prove this property?
%\item
%Now change the condition in the \t{do} $\ldots$ \t{while} loop of the program
%to be \t{nPackets == nPacketsOld}.
%Build the instrumented program for the specification \t{lock.spc} and run \blast.
%\blast\ should now provide an error trace.
\end{enumerate}
\end{exercise}

Let us now repeat the above with a buggy version of the {\tt tut2.c}. Comment out the line
\verb+ nPackets = nPackets + 1; + and repeat the above -- {\it i.e. }, run {\tt spec.opt} 
and then run \blast\ on  {\tt instrumented.c}. This time the tool reports an error trace in the
counterexample trace pane.

\subsubsection{The Counterexample Trace Pane}
The counterexample trace pane is broken into 3 subpanes -- the leftmost is the program source,
the middle pane is the sequence of operations that is the counterexample and the rightmost pane
contains the state of the system given by values for the various predicates in the top half
and the function call stack in the lower pane
at the corresponding points in the counterexample. One can see the state of the system at
different points of the trace by clicking on the corresponding operation in the middle pane.
When one chooses an operation in the middle pane, the corresponding program text is highlighted
in the left pane and the predicate values and control stack are shown in the right pane.
Alternatively, one can go back and forth along the trace using the arrows along the bottom.
\begin{itemize}
\item[Adjacent]
The first pair move to the appropriate adjacent operation, 
\item[Undo/Redo] The second pair are like 
web-browser buttons in that they move to the previous or next operation among the various
operations that have been hitherto selected, 
\item[FF/REW] The third pair move to the next or previous point in the trace where the values
of the predicates are different from the present values -- thus they skip over irrelevant 
operations and proceed directly to the nearest operation that changes the values of the 
predicates.
\item[Pred Change] The fourth pair are used after first selecting some predicate 
in the rightmost pane -- after selecting the predicate if one clicks the right (left) button
one is taken to the first operation in the future (past) where the value of that predicate is 
about to (has just) change(d). This pair is used to see which operations cause a predicate to 
get its value at some point in the trace.
\end{itemize}

In the given counterexample, we see that the first operation is a call to \verb+ __initialize__+
which is a spec function that sets the initial value of the spec variable {\tt lockStatus}.
The very next statement sets {\tt lockStatus} to {\tt 0}, and on selecting that operation, 
notice that in the rightmost pane, the state is such that \verb+lockStatus == 0+ and 
\verb+lockStatus == 1+ is false ({\it i.e. } its negation is true). In this pane, select
the predicate \verb+lockStatus == 0+ and then click the Right Pred. 
Change button (the right arrow with the orange ``sun'' behind it). The gui leaps forward to 
the operation where \verb+lockStatus+ is going to be set to {\tt 1}. 
On pressing the Undo button, the gui jumps back to the previous operation that was being viewed.
You can play around with the trace and view the various operations and convince yourself that 
this is indeed a violation of the specification.

\subsection{Reachability}

\blast\ implements an engine that checks whether a certain line of the C source
code is reachable.
Internally, the specifications are used to instrument the code such that
failure of the specification amounts to reaching a particular error label.
Such a transformation is possible for any safety property.
Thus, the basic command for running \blast\ is "\t{blast.opt prog.c -main start -L errlabel}". 
This runs the model checker on the program \t{prog.c} and checks for
reachability of the error label \t{errlabel} from the function \t{start}.
The run ends with either an error trace or with a message that the error
was unreachable (or, since the reachability problem is undecidable, the program may not terminate).
The defaults for \t{start} and \t{errlabel} are \t{main} and \t{ERROR} respectively.
Therefore, invoking \blast\ with \t{pblast.opt prog.c} checks whether the 
program label \t{ERROR} is reachable, when execution begins in the function \t{main}.

This also means that you can bypass the specification language and add
your own annotations directly to the code, and have a special error label
to signify violation of the property you are checking.
However we recommend the use of the specification language since we have found it
useful to have specifications separate from the implementation.
For example, the same specification can be checked for many programs.


\subsection{Additional Predicates}

Now consider the following example.

\begin{verbatim}
void main () {
  int x, y;
  int __BLAST_NONDET;
  x = 0;
  y = 0;
  while (__BLAST_NONDET) {
    x ++ ;
    y ++ ;
  }
  while (x > 0) {
    x -- ;
    y -- ;
  }
  assert (y == 0);
}
\end{verbatim}

The variable \t{\_\_BLAST\_NONDET} has special internal meaning:
it is treated as a nondeterministic choice. This is often useful
to model nondeterministic behavior for stub functions.
If you run \blast\ on this example (after processing with {\tt gcc} and {\tt mfilter})
, it fails with the error message: \verb+ No new predicates found! +

This is because the predicate discovery engine of \blast\ is not powerful to
infer the predicates \t{x==y, x >= 0} from this example.
In such a case, the user can give additional useful predicates to \blast.
Additional predicates can be given using a {\em predicate file}.
A predicate file contains a list of useful predicates separated by semicolons
that \blast\ reads in at
the start of the model checking.
Each predicate is an atomic predicate written in C syntax, but where the local variable
\t{x} in function \t{f} is written as \t{x@f} (global variables \t{y} are still
written \t{y}). 
In this example, we can create a file \t{tut3.pred} with the single entry
\t{x@main == y@main; x@main >= 0;} (the semicolons are necessary).
We then run \blast\ after selecting the file \t{tut3.pred} as a predicate file.
and this time it reports that the program is safe.
Methods to automatically infer suitable predicates is still an open area of research --
the problem is of course undecidable in general.

Of course, additional predicates can be given for any program, not necessarily
those on which \blast\ fails. Our experience
is that it is often useful to start with the predicates generated from
the guards of the specification.
Therefore, the instrumentation of the specification also generates a predicate
file called \t{instrumented.pred}, which we used earlier for the previous example.


\subsection{Smarter Predicate Discovery}

The default predicate discovery engine in \blast\ may not always succeed in finding
``good'' predicates.
Therefore, \blast\ implements some additional analysis for finding suitable predicates.
These can be accessed with the command line options \c{-craig 1}, \c{-craig 1 -scope},
and \c{-craig 2}.
These implement predicate discovery based on interpolants.
The option \c{-craig 1 -scope} is an efficiency heuristic on top of \c{-craig 1},
it removes predicates not in scope.
The option \c{-craig 2} does a precise analysis.
On the first run, \c{-craig 2} often takes more time than \c{-craig 1}, but it
produces a much finer abstraction.
Even when the default predicate discovery succeeds, the \c{-craig} options often
succeed with fewer predicates.
Additional heuristics are implemented, and can be accessed with the \c{-predH} flag.
We recommend running \blast\ with \c{-predH 7} (level $7$ is the highest level).

Consider for example the program \t{foo.c}:
\begin{verbatim}
#include <assert.h>

int main() {
  int i, x, y, ctr;

  x = ctr;
  y = x;
  y = x + 1;
  if (ctr == i) {
    assert (y == i + 1); 
  }
}
\end{verbatim}
Let \t{foo.i} be the preprocessed program as above.
Running \t{pblast.opt foo.i} does not succeed, and says ``No new predicates found.''
However, running
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7
\end{verbatim}
verifies that the program is safe.

\subsection{Saved Abstractions}

When \blast\ finishes running on the program filename.c, it creates a file called filename.abs
containing the abstraction that was used.
This abstraction can be used in subsequent runs to save time in predicate discovery.
Subsequent runs may be required in a regression testing scenario where some parts
of the program has changed, and \blast\ is run to verify the property on this new program.
The old abstraction file can be read in with the \c{-loadabs} flag.
For example, after we have run 
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7
\end{verbatim}
a file called \t{foo.abs} is created.
Subsequently, running
\begin{verbatim}
pblast.opt foo.i -craig 1 -predH 7 -loadabs
\end{verbatim}
will reuse the previous abstraction and do the verification much faster.

