export HEVEADIR=/users/rupak/work/hevea-1.06/

mkdir -p html
cd html && rm -rf *

${HEVEADIR}/hevea.opt -I /users/rupak/work/hevea-1.06 -I /users/rupak/work/hevea-1.06/html ../setup.tex
${HEVEADIR}/hevea.opt -I /users/rupak/work/hevea-1.06 -I /users/rupak/work/hevea-1.06/html ../setup.tex

cp ../specmanual.tex .
cp ../gui.tex .
${HEVEADIR}/hevea.opt -I /users/rupak/work/hevea-1.06 -I /users/rupak/work/hevea-1.06/html ../blast.tex
${HEVEADIR}/hevea.opt -I /users/rupak/work/hevea-1.06 -I /users/rupak/work/hevea-1.06/html ../blast.tex

rm -f specmanual.tex 
rm -f gui.tex 

${HEVEADIR}/hacha.opt -o blasttoc.html blast.html
echo "<base target=\"main\">" > tmptoc
cat blasttoc.html >> tmptoc
mv tmptoc blasttoc.html

cp ../blastheader.html .
cp ../blastindex.html .
ln -sf blastindex.html index.shtml 
rm -f *.log *.aux *.haux

cd ..

pdflatex blast.tex
pdflatex blast.tex
#latex blast.tex
#latex blast.tex
#dvips -o blast.ps blast.dvi
rm -f *.log *.aux *.haux *.dvi
mv blast.pdf html

pdflatex blast.tex
pdflatex blast.tex

