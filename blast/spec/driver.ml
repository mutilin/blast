(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
if Array.length Sys.argv < 2 then
  raise (Failure ("Missing parameters. Call with: " ^ Sys.argv.(0) ^ " <spec file> [<C files>]\nIf no C file is supplied, all .c files in the cwd will be checked."));

let specFile = Sys.argv.(1) in
print_string "Parsing sources....\n";
flush stdout;
let fnames =
  if Array.length Sys.argv >= 3 then
    Array.to_list (Array.sub Sys.argv 2 (Array.length Sys.argv - 2))
  else
    (let dir = Unix.opendir "." in
    let rec getFilenames () =
      try
	let fname = Unix.readdir dir in
	if (String.length fname >= 2 && compare ".c" (String.sub fname (String.length fname - 2) 2) == 0)
	    && compare "merged.c" fname != 0
	    && compare "instrumented.c" fname != 0 then
	  fname :: getFilenames ()
	else
	  getFilenames ()
      with End_of_file -> [] in
    let fnames = getFilenames () in
    Unix.closedir dir;
    fnames) in
let files = List.map Common.parseFile fnames in
let file = Mergecil.merge files "merged.c" in

let merged = open_out "merged.c" in
Common.outputFile merged file;
close_out merged;

(*print_string "Initial code:\n";
Common.printFile file;*)

Rmtmps.removeUnusedTemps file;

let spec = Spec.parseSpec specFile in

print_string "Spec read\n";
flush stdout;

(*print_string "\nSpec:\n";
Spec.printSpec spec;*)

let file = Spec.instrument spec file in

(*print_string "\Before rmtmps:\n";
Common.printFile file;

Rmtmps.removeUnusedTemps file;*)

(*print_string "\nInstrumented code:\n";
Common.printFile file;*)

let outf = open_out "instrumented.c" in
Common.outputFile outf file;
close_out outf;

print_string "Done\n"
