#include "lock_var.h"

shadow lock_t {
	int locked = 0;
}

event {
	pattern { init($1); }
	action { $1->locked = 0; }
}

event {
	pattern { lock($1); }
	guard { $1->locked == 0 }
	action { $1->locked = 1; }
}

event {
	pattern { unlock($1); }
	guard { $1->locked == 1 }
	action { $1->locked = 0; }
}
