typedef struct context *Context;
typedef struct image *Image;
typedef struct display *Display;

Display newDisplay(void);
Context genContext(Display);
Image genImage(Display, int);
void putText(Display, Context, Image);
