#include "x11.h"

shadow Image {
	Display display = 0;
}

shadow Context {
	Display display = 0;
}

event {
	after
	pattern { $1 = genContext($2); }
	action { $1->display = $2; }
}

event {
	after
	pattern { $1 = genImage($2, $3); }
	action { $1->display = $2; }
}

event {
	pattern { $? = putText($1, $2, $3); }
	guard { $2->display == $3->display && $2->display == $1 }
}
