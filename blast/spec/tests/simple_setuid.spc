#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>

global int __uid__ = 0;

event {
	pattern { $? = system($?); }
	guard { __uid__ != 0 }
}

event {
	pattern { $? = setuid($1); }
	action { __uid__ = $1; }
}

event {
	pattern { $1 = getuid(); }
	action { $1 = __uid__; }
	after
}
