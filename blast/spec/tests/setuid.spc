#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>

global int __R__ = 0;
global int __E__ = 0;
global int __S__ = 0;
global int __saved_euid__ = 0;
global int __original_real_uid__ = 1;

event {
	pattern { $? = popen($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = system($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execl($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execlp($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execle($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execv($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execvp($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = execve($?); }
	guard { __E__ != 0 }
}

event {
	pattern { $? = setuid(saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setuid(original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setuid(getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setuid(geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setuid(0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setuid(1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(-1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = seteuid(1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(saved_euid, 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(original_real_uid, 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(getuid(), 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(geteuid(), 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(-1, 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(0, 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 0;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, saved_euid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, original_real_uid); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, getuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, geteuid()); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, -1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, 0); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 0;
		__S__ = 0;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
event {
	pattern { $? = setreuid(1, 1); }

	guard { __R__ == 0 && __E__ == 0 && __S__ == 0 && __saved_euid__ == 0 && __original_real_uid__ == 1 }

	action {
		__R__ = 1;
		__E__ = 1;
		__S__ = 1;
		__saved_euid__ = 0;
		__original_real_uid__ = 1;
	}

	repair { }
}
