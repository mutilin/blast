typedef struct lock_s *lock_t;

void init(lock_t);
void lock(lock_t);
void unlock(lock_t);
