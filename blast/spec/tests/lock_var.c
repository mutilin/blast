#include "lock_var.h"

int f(int x)
{
  if (x == 14)
    return 1;
  else
    return 0;
}

void locker(lock_t l2)
{
  lock(l2);
}

main()
{
  lock_t l;
  init(l);
  lock(l);
  if (f(14))
    locker(l);
  unlock(l);
}
