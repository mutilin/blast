#include "x11.h"

main()
{
  int nondet;
  Display d = newDisplay();
  Context c = genContext(d);
  Image i;

  if (nondet)
    i = genImage(d, 0);
  else
    i = genImage(newDisplay(), 1);

  putText(d, c, i);
}
