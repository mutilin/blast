#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>

int original_real_uid;
int saved_euid;

void drop_priv()
{
  struct passwd *passwd;

  if ((passwd = getpwuid(getuid())) == NULL)
  {
    printf("getpwuid() failed");
    //seteuid(1);
    return;
  }
  printf("Drop user %s's privilege\n", passwd->pw_name);
  seteuid(1);
}

int main(int argc, char *argv[])
{
  drop_priv();
  printf("About to exec\n");
  execv(argv[1], argv + 1);
}
