(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open Cil

(* Convert a string to a list of characters *)
let rec explode = function
    "" -> []
  | s  -> (String.get s 0) ::
          explode (String.sub s 1 ((String.length s) - 1));;

(* Convert a list of characters to a string *)  
let rec implode = function
    []       -> ""
  | h::t     -> String.make 1 h ^ implode t;;

let isSpace ch = ch == ' ' || ch == '\n' || ch == '\t' || ch == '\r'

let outputFile outf file =
  List.iter (fun x -> Pretty.fprint outf 80 (Cil.printGlobal Cil.defaultCilPrinter () x);
    output_char outf '\n')
    file.globals

let printFile = outputFile stdout

let printIns ins =
  Pretty.fprint stdout 80 (Cil.printInstr Cil.defaultCilPrinter () ins)

let outputExp outf ins =
  Pretty.fprint outf 80 (Cil.printExp Cil.defaultCilPrinter () ins)

let printExp = outputExp stdout

let outputStmt outf s =
  Pretty.fprint outf 80 (Cil.printStmt Cil.defaultCilPrinter () s)

let printStmt = outputStmt stdout

let outputTyp outf typ =
  Pretty.fprint outf 80 (Cil.printType Cil.defaultCilPrinter () typ)

let printTyp = outputTyp stdout

let outputTypsig outf typsig =
  Pretty.fprint outf 80 (Cil.d_typsig () typsig)

let outputGlobal outf gl =
  Pretty.fprint outf 80 (Cil.printGlobal Cil.defaultCilPrinter () gl)

let printGlobal = outputGlobal stdout

module IntOrdered =
struct
  type t = int
  let compare = compare
end

module IntMap = Map.Make(IntOrdered)
module IntSet = Set.Make(IntOrdered)

module StringOrdered =
struct
  type t = string
  let compare = compare
end

module StringMap = Map.Make(StringOrdered)

let for_all2 f l1 l2 =
  let rec check l1 l2 =
    match (l1, l2) with
	([], []) -> true
      |	(h1::t1, h2::t2) -> f h1 h2 && check t1 t2
      |	_ -> false
  in check l1 l2
 
let get_fundec file name =
  let rec search = function
      [] -> raise (Failure ("Function declaration not found: "^name))
    | (GFun (fundec, _))::rest ->
	if compare (fundec.svar.vname) name == 0 then
	  fundec
	else
	  search rest
    | _::rest -> search rest
  in search file.globals

let rename_locals f =
  Cil.iterGlobals f (fun g -> match g with
    Cil.GFun(fd,_) ->
  let fnName = fd.Cil.svar.Cil.vname in
  let locals = List.map (fun var -> (var.Cil.vname <- (var.Cil.vname^"@"^fnName));var) fd.Cil.slocals
  and formals = List.map (fun var -> (var.Cil.vname <- (var.Cil.vname^"@"^fnName));var) fd.Cil.sformals
  in
  fd.Cil.slocals <- locals ;
  fd.Cil.sformals <- formals
  | _ -> ())

let unrename_locals f =
  Cil.iterGlobals f (fun g -> match g with
    Cil.GFun(fd,_) ->
  let fnName = fd.Cil.svar.Cil.vname in
  let chop s =
   let at_index = String.index s '@' in
   String.sub s 0 at_index
  in
  let locals = List.map (fun var -> (var.Cil.vname <- (chop var.Cil.vname));var) fd.Cil.slocals
  and formals = List.map (fun var -> (var.Cil.vname <- (chop var.Cil.vname));var) fd.Cil.sformals
  in
  fd.Cil.slocals <- locals ;
  fd.Cil.sformals <- formals
  | _ -> ())

let parseFile fname =
    let _ = Unix.system ("cpp -I. " ^ fname ^ " >test.i") in
    let file = Frontc.parse "test.i" () in
    Sys.remove "test.i";
    file

let list_sub l start len =
  let rec take = function
      (0, _) -> []
    | (n, h::t) -> h::(take (n-1, t))
    | _ -> raise Not_found
  in let rec drop = function
      (0, l) -> l
    | (n, _::t) -> drop (n-1, t)
    | _ -> raise Not_found
  in take (len, drop (start, l))
