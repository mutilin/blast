(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
class eventInliner events predfile =
  let
      fname,fund = (ref ""), (ref None) 
  in 
object
  inherit nopCilVisitor
    
  method vfunc fundec =
    fname := fundec.svar.vname;
    fund := Some fundec;
    DoChildren

  method vstmt stm =
    match (!fund) with 
      | None -> assert false
      | Some fd -> begin (*--- Event inliner code ---*) 
	  match stm with
	      {skind = Instr insts} ->
		(let rec replace = function
		     ([], acc) -> [mkStmt (Instr (List.rev acc))]
		   | (((Call (None, func, params, loc)) as inst) :: rest, acc) ->
		       (match func with
			    Lval (Var vinfo, NoOffset) ->
			      if String.length vinfo.vname >= 9
				&& compare "__event" (String.sub vinfo.vname 0 7) == 0
				&& compare "__" (String.sub vinfo.vname (String.length vinfo.vname - 2) 2) == 0 
			      then ((
				(let n = int_of_string (String.sub vinfo.vname 7 (String.length vinfo.vname - 9)) in
				 let ev = IntMap.find n events in
				   (*print_string "Matched an event call!\n";*)
				 let sub = IntMap.map (List.nth params) ev.eParams in 
				   let body' =
				     (match ev.eBody with
					  None -> raise (Failure "No definition for event function")
					| Some body -> body) in
				   let body = Cil.copyFunction body' "__tmp__" in
				   (*- create an association list -*)
				   let fTrList = List.map2 (fun vi vj -> (vi.vid,vj.vid)) 
				     body'.sformals body.sformals in 
				   let translateFormal vid  = try 
				       List.assoc vid fTrList 
				   with Not_found -> assert false 
				   in 
				       (*- translate the entries in sub -*)
				   let sub' =  
				     IntMap.fold 
				       ( fun v exp s ->
					     IntMap.add (translateFormal v) exp s 
				       ) 
				       sub 
				       IntMap.empty 
				     in 
				     (*- fix the situation with locals -*) 
				   let var_count = ref (List.length fd.slocals) in 
				     List.iter 
				       (fun vi ->  ( 
					  vi.vname <-  Printf.sprintf "%s_%d@%s" vi.vname !var_count fd.svar.vname; 
					  incr var_count )
				       )
				       body.slocals ; 
				     fd.slocals <- fd.slocals@body.slocals ;
				     Common.printStmt (mkStmt (Block {bstmts = body.sbody.bstmts; battrs = []})) ;
				     let replaced = Cil.visitCilStmt (new varSubst sub')
				       (mkStmt (Block {bstmts = body.sbody.bstmts; battrs = []})) in
				     let _ = Cil.visitCilStmt (new makePreds predfile (!fname)) replaced in
				       
				       mkStmt (Instr (List.rev acc))
				       :: replaced
				       :: replace (rest, [])) ;
			      
			      
				    )) else
				replace (rest, inst::acc)
		   | _ -> replace (rest, inst::acc))
		   | (inst::rest, acc) -> replace (rest, inst::acc)
		 in ChangeTo {stm with skind = Block {bstmts = replace (insts, []); battrs = []}})
	    |	_ -> DoChildren
	end
end
