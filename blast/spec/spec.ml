(*
BLAST is a tool for software model checking.
This file is part of BLAST.

Copyright (c) 2002-2008, The BLAST Team.
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


The BLAST Team consists of
        Dirk Beyer (SFU), Thomas A. Henzinger (EPFL),
        Ranjit Jhala (UCSD), and Rupak Majumdar (UCLA).

BLAST web page:
        http://mtc.epfl.ch/blast/

Bug reports:
        Dirk Beyer:      firstname.lastname@sfu.ca or
        Rupak Majumdar:  firstname@cs.ucla.edu or
        Ranjit Jhala:    lastname@cs.ucla.edu
 *)
open Cil

module IntMap = Common.IntMap
module IntSet = Common.IntSet

module StringMap = Common.StringMap

type temporal =
  | BEFORE
  | AFTER

type pat = Cil.instr list

type event =
    {eId: int;
     eVarName: string;
     eVarExp: Cil.exp;
     mutable eVarTypes: Cil.typ IntMap.t;
     ePatternText: string;
     mutable ePattern: pat;
     eWhen: temporal;
     eGuard: string option;
     eAction: string option;
     eRepair: string option;
     mutable eBody: Cil.fundec option;
     mutable eParams: int IntMap.t;
     mutable eRemaining: pat;
     mutable eMatched: Cil.instr list;
     mutable eVars: Cil.exp IntMap.t option}

let nullLocation = {line = 0;
		    file = "spec"; byte = 0 ; }

let nullFunc =
  let outf = open_out "spec.work" in
  output_string outf "main(){}";
  close_out outf;
  let fundec = Common.get_fundec (Common.parseFile "spec.work") "main" in
  Sys.remove "spec.work";
  fundec

let nullExp = Const (CInt64 (Int64.zero, IInt, None))
let nullStmt = Cil.mkStmt (Cil.Instr [])
let nullInstr = Cil.Asm ([], [], [], [], [], nullLocation)

let defaultEvent =
  {eId = 0;
   eVarName = "__default__";
   eVarExp = nullExp;
   eVarTypes = IntMap.empty;
   ePatternText = "";
   ePattern = [];
   eWhen = BEFORE;
   eGuard = None;
   eAction = None;
   eRepair = None;
   eBody = None;
   eParams = IntMap.empty;
   eRemaining = [];
   eMatched = [];
   eVars = None}

let idMaker () =
  let counter = ref 0 in
  fun () ->
    let ret = !counter in
    counter := ret + 1;
    ret

let newEventId = idMaker ()
let newShadowId = idMaker ()

type member =
    {mDec: string;
     mInit: string}

type shadow =
    {shName: string;
     shTyp: string;
     shMembers: member list}

type spec =
    {sIncludes: string list;
     sGlobals: string list;
     sShadows: shadow list;
     sEvents: event IntMap.t}

let isWhitespace =
  function
    ' ' -> true
  | '\t' -> true
  | '\n' -> true
  | '\r' -> true
  | _ -> false

let getSpecVar s =
  if String.length s >= 8 && compare (String.sub s 0 6) "__spec" == 0
      && compare (String.sub s (String.length s - 2) 2) "__" == 0 then
    Some (int_of_string (String.sub s 6 (String.length s - 8)))
  else
    None

let rmWork () = () (*Sys.remove "spec.work"*)

let varRegexp = Str.regexp "\\$\\([0-9]+\\)"
let replaceVars' s =
  let s = Common.explode s in
  let rec traverse = function
      (visited, [], set) -> (List.fold_right (^) (List.rev visited) "", set)
    | (visited, h::t, set) ->
	match h with
	  '$' ->
	    let rec readInt = function
		([], acc) -> (acc, [])
	      |	((h::t) as l, acc) ->
		  if h >= '0' && h <= '9' then
		    readInt (t, acc*10 + Char.code h - Char.code '0')
		  else
		    (acc, l)
	    in (match t with
	      '?'::t -> traverse("__any__"::visited, t, set)
	    | _ ->
		let (n, t) = readInt (t, 0) in
		traverse (("__spec" ^ string_of_int n ^ "__")::visited, t, IntSet.add n set))
	| _ -> traverse ((String.make 1 h) :: visited, t, set)
  in traverse ([], s, IntSet.empty)
	      
let replaceVars s =
  let (s, _) = replaceVars' s in s

let maxVar = 10

let defineVars outf =
  let rec loop n =
    if n > maxVar then
      ()
    else
      (output_string outf ("int __spec" ^ string_of_int n ^ "__;\n");
       loop (n+1))
  in loop 1;
  output_string outf "int __any__;\n"
let compileStmt stmt =
  (*let _ = print_string "compileStmt\n" in
  let _ = flush stdout in*)
  let (stmt, vars) = replaceVars' stmt in
  let outf = open_out "spec.work" in
  let _ = defineVars outf in
  let _ = output_string outf "void main(void)\n{{\n" in
  let _ = output_string outf stmt in
  let _ = output_string outf "\n}}\n" in
  let _ = close_out outf in
  let file = Common.parseFile "spec.work" in
  rmWork ();
  (*Common.printFile file;*)
  try
    (match List.find (function GFun(fundec, _) -> compare fundec.svar.vname "main" == 0 | _ -> false)
	file.globals with
      GFun(fundec, _) -> (fundec.sbody.bstmts, vars, stmt)
    | _ -> raise (Failure "Impossible"))
  with Not_found -> raise (Failure "No main? Uh oh")

let extractInstrs stmts =
  match stmts with
    {skind = Instr instrs}::_ -> instrs
  | _ -> raise (Failure "Unexpected function body")

let compileStruct name members =
  (*let _ = print_string "compileStruct\n" in
  let _ = flush stdout in*)
  let outf = open_out "spec.work" in
  let _ = output_string outf "struct " in
  let _ = output_string outf name in
  let _ = output_string outf " {\n" in
  let _ = List.iter (fun s -> output_string outf s; output_string outf ";\n") members in
  let _ = output_string outf "};\n" in
  let _ = close_out outf in
  let file = Common.parseFile "spec.work" in
  rmWork ();
  try
    (match List.find (function GCompTag(info, _) -> compare info.cname name == 0 | _ -> false)
	file.globals with
      GCompTag(info, _) -> info
    | _ -> raise (Failure "Impossible"))
  with Not_found -> raise (Failure "No struct? Uh oh")

exception Duplicate

let printVars' f m =
  match m with
    None -> print_string "ERROR\n"
  | Some m ->
      print_char '{';
      IntMap.iter (fun v e ->
	print_int v;
	print_string " = ";
	f e;
	print_string "; ") m;
      print_string "}\n"

let printVars = printVars' Common.printExp
let printVarTypes = printVars' Common.printTyp

let rec joinWith func m1 m2 =
  (*print_string "Join\n";
  printVars m1;
  printVars m2;*)
  let result = match (m1, m2) with
    (Some m1, Some m2) ->
      (try
	Some (IntMap.fold (fun v e m' ->
	  try
	    let e' = IntMap.find v m2 in
	    match func e' e with
	      None -> raise Duplicate
	    | Some e'' -> IntMap.add v e'' m'
	  with
	    Not_found -> IntMap.add v e m') m1 m2)
      with
	Duplicate -> None)
  | _ -> None
  in
  (* printVars result; *)
  result

let joinMatch = joinWith (fun e e' ->
  match (e, e') with
    (Lval (Var vinfo, NoOffset), Lval (Var vinfo', NoOffset)) ->
      if compare vinfo.vname vinfo'.vname == 0 then
	Some e
      else
	None
  | _ -> None)
let joinUnify = joinWith (fun t t' ->
  if compare (Cil.typeSig t) (Cil.typeSig t') == 0 then
    Some t
  else
    None)

let emptyMatch = Some IntMap.empty

let rec stripCasts exp =
  match exp with
    CastE (_, exp) -> stripCasts exp
  | _ -> exp

let rec matchIns pat ins =
  (*print_string "Compare ";
  Common.printIns pat;
  print_string " vs. ";
  Common.printIns ins;
  print_char '\n';*)
  match pat with
    Set (lval, exp, _) ->
      (match ins with
	Set (lval', exp', _) ->
	  joinMatch (matchLval lval lval') (matchExp exp exp')
      |	_ -> None)
  | Call (lvalOpt, func, args, _) ->
      (*print_string "Call compare:\n";
      Common.printIns pat;
      print_string "\nvs\n";
      Common.printIns ins;
      print_char '\n';*)
      (match ins with
	Call (lvalOpt', func', args', _) ->
	  let dest = 
	    (match lvalOpt with
	      Some (Var {vname = "__any__"}, NoOffset) -> emptyMatch
	    | _ -> matchOpt matchLval lvalOpt lvalOpt')
	  in (*print_string "Checking....\n";*)
	  let rec checkArgs pat act =
	    (*print_string "One arg....\n";*)
	    (match (pat, act) with
	      ([], []) -> emptyMatch
	    | (p::pr, a::ar) ->
		(match stripCasts p with
		  Lval (Var {vname = "__any__"}, NoOffset) ->
		    (*print_string "VARARG!\n";*)
		    emptyMatch
		| _ ->
		    (*print_string "Normal ";
		    Common.printExp p;
		    print_string " vs. ";
		    Common.printExp a;
		    print_char '\n';*)
		    joinMatch (matchExp p a) (checkArgs pr ar))
	    | _ -> (*print_string "NO MATCH\n";*) None)
	  in joinMatch (checkArgs args args') (joinMatch dest (matchExp func func'))
      |	_ -> None)
  | _ -> None
and matchInsPrefix pat act =
  match (pat, act) with
      (_, []) -> emptyMatch
    | (p::pt, a::at) ->
	joinMatch (matchIns p a) (matchInsPrefix pt at)
    | _ -> None
and matchExp pat act =
  (*print_string "Compare exp ";
  Common.printExp pat;
  print_string " vs. ";
  Common.printExp act;
  print_char '\n';*)
  match act with
    CastE (_, act') -> matchExp pat act'
  | _ ->
      match pat with
	Const const ->
	  (match act with
	    Const const' -> matchConstant const const'
	  | _ -> None)
      | Lval ((Var vinfo, NoOffset) as lval) ->
	  if compare "__any__" vinfo.vname == 0 then
	    emptyMatch
	  else
	    (match getSpecVar vinfo.vname with
	      Some n ->
		Some (IntMap.add n act IntMap.empty)
	    |	None ->
		(match act with
		  Lval lval' -> matchLval lval lval'
		| _ -> None))
      | Lval lval ->
	  (match act with
	    Lval lval' -> matchLval lval lval'
	  |	_ -> None)
      | UnOp (unop, exp, _) ->
	  (match act with
	    UnOp (unop', exp', _) -> if unop == unop' then matchExp exp exp' else None
	  |	_ -> None)
      | BinOp (binop, exp1, exp2, _) ->
	  (match act with
	    BinOp (binop', exp1', exp2', _) ->
	      if binop == binop' then
		joinMatch (matchExp exp1 exp1') (matchExp exp2 exp2')
	      else
		None
	  |	_ -> None)
      | CastE (_, exp) -> matchExp exp act
      | AddrOf lval ->
	  (match act with
	    AddrOf lval' -> matchLval lval lval'
	  |	_ -> None)
      | StartOf lval ->
	  (match act with
	    StartOf lval' -> matchLval lval lval'
	  |	_ -> None)
      | _ -> None
and matchConstant pat act =
  match pat with
    CInt64 (v, k, _) ->
      (match act with
	CInt64 (v', k', _) ->
	  if compare v v' == 0 then
	    emptyMatch
	  else
	    None
      |	_ -> None)
  | CStr s ->
      (match act with
	CStr s' ->
	  if compare s s' == 0 then
	    emptyMatch
	  else
	    None
      |	_ -> None)
  | CChr c ->
      (match act with
	CChr c' ->
	  if c == c' then
	    emptyMatch
	  else
	    None
      |	_ -> None)
  | CReal (v, k, _) ->
      (match act with
	CReal (v', k', _) ->
	  if compare v v' == 0 then
	    emptyMatch
	  else
	    None
      |	_ -> None)
  | CWStr int64list -> failwith "Do not handle wide strings!"
and matchLval ((pat, patOff) as lv1) ((act, actOff) as lv2) =
(*
  print_string "Compare lval ";
  Common.printExp (Lval lv1);
  print_string " vs. ";
  Common.printExp (Lval lv2);
  print_char '\n';
*)
  
  let m = 
	match pat with 
	Var vinfo -> 
		begin
		match getSpecVar vinfo.vname with
		  Some n -> joinMatch (Some (IntMap.add n (Lval lv2) IntMap.empty)) (emptyMatch)
		| None -> joinMatch (matchLhost pat act) (matchOffset patOff actOff) 
		end
        | _ -> joinMatch (matchLhost pat act) (matchOffset patOff actOff) 
  in
  m
and matchLhost pat act =
  match pat with
    Var vinfo ->
      if compare "__any__" vinfo.vname == 0 then
	emptyMatch
      else
	(match getSpecVar vinfo.vname with
	  Some n ->
	    Some (IntMap.add n (Lval (act, NoOffset)) IntMap.empty)
	| None ->
	    (match act with
	      Var vinfo' ->
		if compare vinfo.vname vinfo'.vname == 0 then
		  emptyMatch
		else
		  None
	    | _ -> None))
  | _ -> None
and matchOffset pat act =
  match pat with
    NoOffset ->
      (match act with
	NoOffset -> emptyMatch
      |	_ -> None)
  | _ -> None
and matchOpt func pat act =
  match (pat, act) with
    (None, None) -> emptyMatch
  | (Some pat, Some act) -> func pat act
  | _ -> None

let rec unifyExp (ty : Cil.typ) exp =
  (*print_string "Unify exp ";
  Common.printExp exp;
  print_string " : ";
  Common.printTyp ty;
  print_char '\n';*)
  match exp with
    Lval lval -> unifyLval ty lval
  | SizeOfE exp -> unifyExp ty exp
  | AlignOfE exp -> unifyExp ty exp
  | CastE (ty, exp) -> unifyExp ty exp
  | _ -> Some IntMap.empty
and unifyLval ty (host, offset) : Cil.typ IntMap.t option =
  (*print_string "Unify lval ";
  Common.printExp (Lval (host, offset));
  print_string " : ";
  Common.printTyp ty;
  print_char '\n';*)
  match offset with
    NoOffset ->
      (match host with
	Var vinfo ->
	  (*print_string "Unify ";
	  print_string vinfo.vname;
	  print_string " : ";
	  Common.printTyp ty;
	  print_char '\n';*)
	  (match getSpecVar vinfo.vname with
	    None -> Some IntMap.empty
	  | Some n -> Some (IntMap.add n ty IntMap.empty))
      |	_ -> Some IntMap.empty)
  | _ -> Some IntMap.empty
and unifyInst inst =
  match inst with
    Set (lval, exp, _) -> joinUnify
	(unifyLval (Cil.typeOf exp) lval)
	(unifyExp (Cil.typeOf (Lval lval)) exp)
  | Call (dest, func, args, _) ->
      (*print_string "Unify a call\n";*)
      let (ret, fargs, vararg) =
	match Cil.typeOf func with
	  TFun (ret, fargs, vararg, _) -> (ret, fargs, vararg)
	| _ -> raise (Failure "Non-function called") in
      (match fargs with
	None ->
	  print_string "Function ";
	  Common.printExp func;
	  print_string " must have a prototype.\n";
	  None
      |	Some fargs ->
	  let destMatched =
	    match dest with
	      None -> emptyMatch
	    | Some dest -> unifyLval ret dest in
	  let fargs_len = List.length fargs in
	  let args_len = List.length args in
	  if args_len < fargs_len || (args_len > fargs_len && not vararg) then
	    (print_string "Argument count mismatch for function ";
	     Common.printExp func;
	     print_char '\n';
	     None)
	  else
	    ((*print_string "Unifying parameters to ";
	     Common.printExp func;
	     print_char '\n';*)
	     List.fold_left2 (fun m a (s,t,attr) -> joinUnify m (unifyExp t a))
	       destMatched
	       (Common.list_sub args 0 fargs_len) 
		fargs))
      | _ -> Some IntMap.empty
and unifyInsts insts = List.fold_left (fun m inst -> joinUnify m (unifyInst inst))
    (Some IntMap.empty) insts

class tmpReplacer =
  object
    inherit nopCilVisitor
	
    val count = ref 1000
    val subst = ref IntMap.empty

    method vvrbl vinfo =
      (match getSpecVar vinfo.vname with
	None ->
	  if vinfo.vglob then
	    SkipChildren
	  else
	    (try
	      let new_var = IntMap.find vinfo.vid (!subst) in
	      ChangeTo new_var
	    with Not_found ->
	      let new_name = "__spec" ^ string_of_int (!count) ^ "__" in
	      let new_var = Cil.makeTempVar nullFunc ~name:new_name vinfo.vtype in
	      subst := IntMap.add vinfo.vid new_var (!subst);
	      (*print_string ("Would change " ^ vinfo.vname ^ " to " ^ new_name ^ "\n");*)
	      count := !count + 1;
	      ChangeTo new_var)
      |	_ -> SkipChildren)
  end

let parseSpec fname =
  let inf = open_in fname in
  
  let readCommand () =
    let rec loop chars =
      try
	let ch = input_char inf in
	if isWhitespace ch then
	  (match chars with
	    [] -> loop chars
	  | _ -> Common.implode (List.rev chars))
	else
	  loop (ch::chars)
      with End_of_file ->
	match chars with
	  [] -> raise End_of_file
	| _ -> Common.implode (List.rev chars)
    in loop [] in
  
  let readUntil stop =
    let rec loop chars =
      let ch = input_char inf in
      if ch == stop then
	Common.implode (List.rev chars)
      else
	loop (ch::chars)
    in loop [] in
  
  let readBlock' depth =
    let rec loop depth chars =
      match input_char inf with
	'{' ->
	  if depth == 0 then
	    loop 1 chars
	  else
	    loop (depth+1) ('{'::chars)
      |	'}' ->
	  if depth <= 1 then
	    Common.implode (List.rev chars)
	  else
	    loop (depth-1) ('}'::chars)
      |	';' ->
	  if depth < 1 then
	    Common.implode (List.rev (';'::chars))
	  else
	    loop depth (';'::chars)
      |	ch -> loop depth (ch::chars)
    in loop depth [] in

  let readBlock () = readBlock' 0 in



  let rec loop spec =
    try
      match readCommand () with
	"#include" -> loop {spec with sIncludes = readCommand () :: spec.sIncludes}
      |	"global" ->
	  let decl = readBlock () in
	  loop {spec with sGlobals = decl::spec.sGlobals}
      |	"shadow" ->
	  let tyS = readUntil '{' in
	  let members = readBlock' 1 in
	  let len = String.length members in
	  let rec findMembers (i, mems) =
	    try
	      if i >= len then
		mems
	      else
		let eq = String.index_from members i '=' in
		let semi = String.index_from members (eq+1) ';' in
		findMembers (semi+1, {mDec = String.sub members i (eq-i);
				      mInit = String.sub members (eq+1) (semi-eq-1)} :: mems)
	    with Not_found ->
	      if List.for_all Common.isSpace (Common.explode
						(String.sub members i
						   (String.length members - i))) then
		()
	      else
		print_string "Error parsing shadow declaration\n";
	      mems in
	  let members = findMembers (0, []) in
	  let shadow = {shName = "__shadow" ^ string_of_int (newShadowId()) ^ "__";
			shTyp = tyS; shMembers = members} in
	  loop {spec with sShadows = shadow :: spec.sShadows}
      |	"event" ->
	  let _ = readUntil '{' in
	  let rec loop' event =
	    match readCommand () with
	      "}" -> event
	    | "pattern" ->
		let pat = readBlock () in
		let stm = replaceVars pat in
		loop' {event with ePatternText = stm}
	    | "guard" -> loop' {event with eGuard = Some (replaceVars (readBlock ()))}
	    | "action" -> loop' {event with eAction = Some (replaceVars (readBlock ()))}
	    | "repair" -> loop' {event with eRepair = Some (replaceVars (readBlock ()))}
	    | "before" -> loop' {event with eWhen = BEFORE}
	    | "after" -> loop' {event with eWhen = AFTER}
	    | cmd ->
		print_string ("Unknown event property " ^ cmd ^ "\n");
		loop' event in
	  let id = newEventId() in
	  let var = "__event" ^	string_of_int id ^ "__" in
	  let event = loop' {defaultEvent with
			     eId = id;
			     eVarName = var;
			     eVarExp = Lval (Var (Cil.makeGlobalVar var (TVoid [])), NoOffset)} in
	  loop {spec with sEvents = IntMap.add id event spec.sEvents}
      |	cmd ->
	  print_string ("Unknown command " ^ cmd ^ "\n");
	  loop spec
    with End_of_file -> {spec with sGlobals = List.rev spec.sGlobals}
  in loop {sIncludes = []; sGlobals = []; sShadows = [];
	   sEvents = IntMap.empty}

class eventFinder events =
  object
    inherit nopCilVisitor
	
    method vstmt stmt =
      (*print_string "I visit a statement.\n";
      Common.printStmt stmt;
      flush stdout;*)
      match stmt with
	{skind = Instr instrs} ->
	  IntMap.iter (fun _ event ->
	    event.eVars <- Some IntMap.empty;
	    event.eRemaining <- event.ePattern;
	    event.eMatched <- []) events;
	  let rec traverse = function
	      (visited, []) -> ChangeTo {stmt with skind = Instr (List.rev visited)}
	    | (visited, ins::rest) ->
		let visited = ins::visited in
		let visited = IntMap.fold (fun _ event visited ->
		  (match event.eRemaining with
		    [] -> visited
		  | patIns::rest ->
		      (match matchIns patIns ins with
			None ->
			  let rec backtrack (matched, remaining, acc) =
			    match (matched, remaining) with
			      (_::mt, p::pt) ->
				(match matchInsPrefix event.ePattern matched with
				  None -> backtrack (mt, pt, p::acc)
				| m ->
				    event.eVars <- joinMatch m event.eVars;
				    event.eRemaining <- acc;
				    event.eMatched <- List.rev matched;
				    visited)
			    | _ ->
				event.eRemaining <- event.ePattern;
				event.eMatched <- [];
				visited
			  in
			  let patRev = List.rev event.ePattern
			  in backtrack (List.tl (List.rev (ins::event.eMatched)),
					List.tl patRev, [List.hd patRev])
		      | m ->
			  (match rest with
			    [] ->
			      (*let _ = print_string "I matched a pattern!\n" in
			      let _ = flush stdout in*)
			      let _ = event.eMatched <- ins::event.eMatched in
			      let _ = event.eVars <- joinMatch m event.eVars in
			      let len = List.length event.ePattern in
			      (*let _ = print_string "Vars: " in
			      let _ = flush stdout in
			      List.iter Common.printIns (List.rev event.eMatched);
			      printVars event.eVars;*)
			      let vars =
				match event.eVars with
				  Some vars -> vars
				| None -> IntMap.empty in
			      let params = List.rev (IntMap.fold (fun n e l -> e::l)
						       vars []) in
			      let rec twiddleParams = function
				  ([], _) -> []
				| (e::rest, n) ->
				    try
				      (match Cil.unrollType (IntMap.find n event.eVarTypes) with
					TComp _ ->
					  (match e with
					    Lval lv -> AddrOf lv
					  | _ -> raise (Failure "Lval expected"))
				      | _ -> e) :: twiddleParams (rest, n-1)
				    with
				      Not_found -> twiddleParams (rest, n-1)
				      
			      in let params = twiddleParams (params, List.length params) in
			      event.eVars <- Some IntMap.empty;
			      event.eRemaining <- event.ePattern;
			      event.eMatched <- [];
			      let call = Call (None, event.eVarExp, params, nullLocation) in
			      (match event.eWhen with
				BEFORE ->
				  let rec add = (function
				      (visited, [], acc) -> List.rev_append acc (call::visited)
				    | (v::vr, _::pr, acc) -> add (vr, pr, v::acc)
				    | _ ->
					raise (Failure "Pattern was shorter than it should be"))
				  in add (visited, event.ePattern, [])
			      |	AFTER -> call :: visited)
			  | _ ->
			      event.eVars <- joinMatch m event.eVars;
			      event.eRemaining <- rest;
			      event.eMatched <- ins::event.eMatched;
			      visited))
		  )) events visited in
		traverse (visited, rest)
	  in traverse ([], instrs)
      |	_ -> DoChildren
  end

class shadowReplacer shadowTable =
  object
    inherit nopCilVisitor
	
    method vtype typ =
      let typ = Cil.unrollType typ in
      try
	let shadow = Hashtbl.find shadowTable (Cil.typeSig typ) in
	(*print_string "Replace ";
	Common.printTyp typ;
	print_char '\n';*)
	ChangeTo shadow
      with
	Not_found -> DoChildren
  end

class varSubst sub =
  object
    inherit nopCilVisitor

    method vexpr exp =
      match exp with
	Lval (Var vinfo, NoOffset) ->
	  (try
	    ChangeTo (IntMap.find vinfo.vid sub)
	  with Not_found -> DoChildren)
      |	_ -> DoChildren

    method vlval lval =
      match lval with
	(Var vinfo, NoOffset) ->
	  (try
	    match IntMap.find vinfo.vid sub with
	      Lval lv -> ChangeTo lv
	    | _ -> DoChildren
	  with Not_found -> DoChildren)
      |	_ -> DoChildren
  end

class makePreds preds fname =
  let rec printExp exp =
    (match exp with
      Lval lval -> pLval lval
    | SizeOfE exp ->
	output_string preds "sizeof(";
	printExp exp;
	output_string preds ")"
    | AlignOfE exp ->
	output_string preds "alignof(";
	printExp exp;
	output_string preds ")"
    | UnOp (op, exp, _) ->
	printUnop op;
	output_string preds "(";
	printExp exp;
	output_string preds ")"
    | BinOp (op, exp1, exp2, _) ->
	output_string preds "(";
	printExp exp1;
	output_string preds ") ";
	printBinop op;
	output_string preds " (";
	printExp exp2;
	output_string preds ")";
    | CastE (_, exp) -> printExp exp
    | AddrOf lval ->
	output_string preds "&(";
	pLval lval;
	output_string preds ")"
    | StartOf lval -> pLval lval
    |	_ -> Common.outputExp preds exp)
      
  and pLval ((lhost, offset) as lval) =
    match offset with
      NoOffset ->
	(match lhost with
	  Var vinfo ->
	    if vinfo.vglob then
	      output_string preds vinfo.vname
	    else
	      (output_string preds vinfo.vname;
	       output_char preds '@';
	       output_string preds fname)
	| _ -> Common.outputExp preds (Lval lval))
    | _ -> Common.outputExp preds (Lval lval)

  and printUnop op =
    let ch = match op with
      Neg -> '-'
    | BNot -> '~'
    | LNot -> '!'
    in output_char preds ch

  and printBinop op =
    let s = match op with
      PlusA -> "+"
    | PlusPI -> "+"
    | IndexPI -> "+"
    | MinusA -> "-"
    | MinusPI -> "-"
    | MinusPP -> "-"
    | Mult -> "*"
    | Div -> "/"
    | Mod -> "%"
    | Shiftlt -> "<<"
    | Shiftrt -> ">>"
    | Lt -> "<"
    | Gt -> ">"
    | Le -> "<="
    | Ge -> ">="
    | Eq -> "=="
    | Ne -> "!="
(*
    | LtP -> "<"
    | GtP -> ">"
    | LeP -> "<="
    | GeP -> ">="
    | EqP -> "=="
    | NeP -> "!="
*)
    | BAnd -> "&"
    | BXor -> "^"
    | BOr -> "|"
    | LAnd -> "&&"
    | LOr -> "||"
    in output_string preds s
  in object
    inherit nopCilVisitor

    method vexpr exp =
      (match exp with
	BinOp ((Lt|Gt|Le|Ge|Eq|Ne), e1, e2, _) ->
	  printExp exp;
	  output_string preds ";\n"
      |	_ -> ());
      DoChildren
  end

class eventInliner events predfile =
  let
      fname = ref ""
  in object
    inherit nopCilVisitor
	
    method vfunc fundec =
      fname := fundec.svar.vname;
      DoChildren

    method vstmt stm =
      match stm with
	{skind = Instr insts} ->
	  (let rec replace = function
	      ([], acc) -> [mkStmt (Instr (List.rev acc))]
	    | (((Call (None, func, params, loc)) as inst) :: rest, acc) ->
		(match func with
		  Lval (Var vinfo, NoOffset) ->
		    if String.length vinfo.vname >= 9
			&& compare "__event" (String.sub vinfo.vname 0 7) == 0
			&& compare "__" (String.sub vinfo.vname (String.length vinfo.vname - 2) 2) == 0 then
		      (let n = int_of_string (String.sub vinfo.vname 7 (String.length vinfo.vname - 9)) in
		      let ev = IntMap.find n events in
		      (*print_string "Matched an event call!\n";*)
		      let sub = IntMap.map (List.nth params) ev.eParams in
		      (*print_string "I would apply this substitution:\n";
		      IntMap.iter (fun k v ->
			print_string (string_of_int k);
			print_string " = ";
			Common.printExp v;
			print_char '\n') sub;*)

		      let body =
			(match ev.eBody with
			  None -> raise (Failure "No definition for event function")
			| Some body -> body) in
		      let body = Cil.copyFunction body "__tmp__" in
		      let replaced = Cil.visitCilStmt (new varSubst sub)
			  (mkStmt (Block {bstmts = body.sbody.bstmts; battrs = []})) in
		      let _ = Cil.visitCilStmt (new makePreds predfile (!fname)) replaced in

		      (*print_string "Replaced with:\n";
		      Common.printStmt replaced;*)

		      mkStmt (Instr (List.rev acc))
		      :: replaced
		      :: replace (rest, []))
		    else
		      replace (rest, inst::acc)
		| _ -> replace (rest, inst::acc))
	    | (inst::rest, acc) -> replace (rest, inst::acc)
	  in ChangeTo {stm with skind = Block {bstmts = replace (insts, []); battrs = []}})
      |	_ -> DoChildren
  end

class byReference =
  object
    inherit nopCilVisitor
	
    method vfunc fundec =
      List.iter (fun vinfo ->
	match Cil.unrollType vinfo.vtype with
	  TComp _ -> vinfo.vtype <- TPtr (vinfo.vtype, [])
	| _ -> ()) fundec.sformals;
      DoChildren

    method vinst inst =
      match inst with
	Call (_, Lval (Var vinfo, NoOffset), _, _) ->
	  if String.length vinfo.vname >= 9
	      && compare "__guard" (String.sub vinfo.vname 0 7) == 0
	      && compare "__" (String.sub vinfo.vname (String.length vinfo.vname - 2) 2) == 0
	      then
	    SkipChildren
	  else
	    DoChildren
      |	_ -> DoChildren

    method vlval (host, offset) =
      match host with
	Var vinfo ->
	  (match getSpecVar vinfo.vname with
	    None -> DoChildren
	  | _ ->
	      	(match Cil.unrollType vinfo.vtype with
		  TComp _ -> ChangeTo (Mem (Lval (host, NoOffset)), offset)
		| _ -> DoChildren))
      | _ -> DoChildren
  end

let inits = ref []

class initGlobals =
  object
    inherit nopCilVisitor
	
    method vglob global =
      (match global with
	GVar (vinfo, init, location) ->
	  (match init.init with
	    Some (SingleInit e) -> inits := Set ((Var vinfo, NoOffset), e, location) :: !inits
	  | _ -> print_string ("Warning: a compound global initializer was found, but not propagated\n"))
      |	_ -> ());
      SkipChildren
  end

class removeReturn =
  object
    inherit nopCilVisitor

    method vstmt stm =
      match stm with
	{skind = Return _} -> ChangeTo {stm with skind = Block {bstmts = []; battrs = []}}
      |	_ -> DoChildren
  end

let printSpec spec =
  print_string "Globals:\n";
  List.iter (fun s -> print_string s; print_char '\n') spec.sGlobals;
  print_string "\nEvents:\n"(*;
  List.iter (fun ev ->
    print_string "\nPattern: ";
    List.iter (fun s ->
      print_string "Stmt: ";
      Pretty.fprint stdout 80 (Cil.printInstr Cil.defaultCilPrinter () s);
      print_char '\n') ev.ePattern;
    print_string "\nWhen: ";
    (match ev.eWhen with
      BEFORE -> print_string "Before"
    | AFTER -> print_string "After");
    print_string ("\nGuard: " ^ ev.eGuard);
    print_string ("\nAction: " ^ ev.eAction);
    print_string ("\nRepair: " ^ ev.eRepair);
    print_char '\n') spec.sEvents*)

let instrument spec file =
  let shadowTable = Hashtbl.create 100 in

  let output_includes outf =
    List.iter (fun s ->
      output_string outf "#include ";
      output_string outf s;
      output_char outf '\n') spec.sIncludes in

  (*print_string "INCLUDES:\n";
  output_includes stdout;
  print_string "--DONE--\n";*)

  (* Separate the type declarations out of the source file *)
  let isTypeDecl = function
      GType _ -> true
    | GCompTag _ -> true
    | GCompTagDecl _ -> true
    | GEnumTag _ -> true
    | GEnumTagDecl _ -> true
    | _ -> false in
  let (typeDecls, otherDecls) = List.partition isTypeDecl file.globals in

  (* Process parts of spec that depend on bindings in source file *)
  print_string "Process patterns\n";
  flush stdout;
  let work = "spec.work" in
  let outf = open_out work in
  Common.outputFile outf file;
  output_includes outf;
  defineVars outf;
  let _ = List.iter (fun sh ->
    let fields = (sh.shTyp ^ " __value__") :: (List.map (fun m -> m.mDec) sh.shMembers) in
    output_string outf "struct ";
    output_string outf sh.shName;
    output_string outf " {\n";
    List.iter (fun s -> output_string outf s; output_string outf ";\n") fields;
    output_string outf "};\n") spec.sShadows in
  let _ = IntMap.iter (fun _ ev ->
    (*print_string "Output event...\n";*)
    output_string outf "\nvoid __pattern";
    output_string outf (string_of_int ev.eId);
    output_string outf "__(void)\n{{\n";
    output_string outf ev.ePatternText;
    output_string outf "\n}}\n") spec.sEvents in
  let _ = close_out outf in

  let file' = Common.parseFile work in
  (*Common.printFile file;*)
  flush stdout;
  Sys.remove work;
  (*print_string "Parsed\n";
  flush stdout;*)
  List.iter (function
      GCompTag (compinfo, _) ->
	let name = compinfo.cname in
	if String.length name > 10
	    && "__shadow" = String.sub name 0 8
	    && "__" = String.sub name (String.length name - 2) 2 then
	  ((*Printf.printf "Here's shadow %s.\n" name;*)
	   let __value__ = List.hd compinfo.cfields in
	   let ty = __value__.ftype in
	   Hashtbl.add shadowTable (Cil.typeSig (Cil.unrollType ty))
	     (Cil.TPtr (Cil.TComp (compinfo, []), [])))
    | GFun(fundec, _) ->
	let name = fundec.svar.vname in
	if String.length name >= 11 && compare "__pattern" (String.sub name 0 9) == 0
	    && compare "__" (String.sub name (String.length name - 2) 2) == 0 then
	  try
	    let n = int_of_string (String.sub name 9 (String.length name - 11)) in
	    let ev = IntMap.find n spec.sEvents in
	    let instrs = extractInstrs fundec.sbody.bstmts in
	    let varTypes = unifyInsts instrs in

	    (*print_string "For ";
	    print_string name;
	    print_string ":\n";
	    List.iter Common.printIns instrs;
	    print_char '\n';*)

	    (*print_string ("Unified " ^ name ^ ": ");
	    printVarTypes varTypes;*)

	    (match varTypes with
	      None -> ()
	    | Some v -> ev.eVarTypes <- IntMap.map (fun t ->
		try
		  Hashtbl.find shadowTable (Cil.typeSig t)
		with Not_found -> t) v);
		
	    (*print_string ("Pattern text: " ^ ev.ePatternText ^ "\n");*)
	    let insts = extractInstrs fundec.sbody.bstmts in
	    let insts =
	      (match Cil.visitCilStmt (new tmpReplacer) (mkStmt (Instr insts)) with
		({skind = Instr insts} as stmt) ->
		      (*print_string "\nPattern:\n";
			 Common.printStmt stmt;*)
		  insts
	      | _ -> raise (Failure "Didn't get Instr back from visitCilStmt")) in
	    ev.ePattern <- insts;
	    (*List.iter Common.printIns ev.ePattern*)
	  with Not_found -> raise (Failure "Unexpected pattern function!")
	else
	  ()
    | _ -> ()) file'.globals;

  (* Generate new definitions to add to source file *)
  print_string "Add definitions\n";
  flush stdout;
  let outf = open_out work in
  (*Common.outputFile outf file;*)
  output_includes outf;
  output_string outf "int __BLAST_error ; /* global variable tweaked in error functions */\n" ;
  output_string outf "void __error__()\n{\n__BLAST_error = 0;\tERROR: goto ERROR;\n}\n\n";
  output_string outf "void __BLAST___error__()\n{\n__BLAST_error = 0;\tBERROR: goto BERROR;\n}\n\n";
  List.iter (fun sh ->
    output_string outf "\nstruct ";
    output_string outf sh.shName;
    output_string outf " {\n\t";
    (*output_string outf sh.shTyp;
    output_string outf " __value__;\n";*)
    List.iter (fun mem -> output_string outf mem.mDec; output_string outf ";\n") sh.shMembers;
    output_string outf "};\n") spec.sShadows;
  List.iter (fun s ->
    output_string outf s;
    output_string outf ";\n") spec.sGlobals;
  IntMap.iter (fun _ ev ->
    let params () =
      let _ = IntMap.fold (fun n ty s ->
	if s then
	  output_string outf ", ";
	Common.outputTyp outf (Cil.unrollType ty);
	output_string outf " __spec";
	output_string outf (string_of_int n);
	output_string outf "__";
	true) ev.eVarTypes false
      in () in
    let aparams () =
      let _ = IntMap.fold (fun n _ s ->
	if s then
	  output_string outf ", ";
	output_string outf "__spec";
	output_string outf (string_of_int n);
	output_string outf "__";
	true) ev.eVarTypes false
      in () in
    (*output_string outf "\nint __guard";
    output_string outf (string_of_int ev.eId);
    output_string outf "__(";
    params ();
    output_string outf ")\n{\n\tif (";
    let exp =
      match ev.eGuard with
	None -> "1"
      |	Some exp -> exp in
    output_string outf exp;
    output_string outf ") return 1; else return 0;\n}\n";*)
    let exp =
      match ev.eGuard with
	None -> "1"
      |	Some exp -> exp in
    output_string outf "\nvoid ";
    output_string outf ev.eVarName;
    output_string outf "(";
    params ();
    output_string outf ")\n{\n";
    output_string outf "\tif (";
    (*output_string outf (string_of_int ev.eId);
    output_string outf "__(";
    aparams ();
    output_string outf ")) {\n";*)
    output_string outf exp;
    output_string outf ") {\n";
    (match ev.eAction with
      None -> ()
    | Some action -> output_string outf action);
    output_string outf "\t} else {\n";
    (match ev.eRepair with
      None -> output_string outf "\t__error__();\n"
    | Some repair -> output_string outf repair);
    output_string outf "\t}\n}\n") spec.sEvents;
  close_out outf;
  print_string "Parsing\n";
  flush stdout;
  let file' = Common.parseFile work in
  Cil.visitCilFile (new shadowReplacer shadowTable) file' ;

  Cil.iterGlobals file' (function
      GFun(fundec, _) ->
	let name = fundec.svar.vname in
	if String.length name >= 9 && compare "__event" (String.sub name 0 7) == 0
	    && compare "__" (String.sub name (String.length name - 2) 2) == 0 then
	  (let n = int_of_string (String.sub name 7 (String.length name - 9)) in
	  let ev = IntMap.find n spec.sEvents in
	  let remover = new removeReturn in
	  fundec.sbody.bstmts <- List.map (Cil.visitCilStmt remover) fundec.sbody.bstmts;
	  ev.eBody <- Some fundec;
(*
	  (match Cil.unrollType (fundec.svar.vtype) with
	    TFun (_, Some params, _, _) ->
	      let rec build = function
		  (_, []) -> IntMap.empty
		| (n, h::t) -> IntMap.add h.vid n (build (n+1, t))
	      in ev.eParams <- build (0, params)
	  | _ -> raise (Failure "Unexpected non-function type"))
*)
	      let rec build = function
		  (_, []) -> IntMap.empty
		| (n, h::t) -> IntMap.add h.vid n (build (n+1, t))
	      in ev.eParams <- build (0, fundec.sformals) ;
		()
          
          )
    | _ -> ()) 
    ;

  (*print_string "Parsed\n";
  flush stdout;*)
  rmWork ();
  (*let file = Cil.visitCilFile (new byReference) file' in*)
  Cil.visitCilFile (new eventFinder spec.sEvents) file ;

  Common.rename_locals file ;
  let preds = open_out "instrumented.pred" in
  Cil.visitCilFile (new eventInliner spec.sEvents preds) file ;
  close_out preds;
  Common.unrename_locals file ;

  Cil.visitCilFile (new shadowReplacer shadowTable) file ;
  let initer = Cil.emptyFunction "__initialize__" in
  let keepGlobals = List.filter (function
      GFun (fundec, _) ->
	String.length fundec.svar.vname < 9
      || compare "__event" (String.sub fundec.svar.vname 0 7) != 0
      || compare "__" (String.sub fundec.svar.vname (String.length fundec.svar.vname - 2) 2) != 0
      |	_ -> true) file'.globals in
  (*Format.printf "keepGlobals@.";
  List.iter (fun g -> (Common.printGlobal g; Format.printf "@.")) keepGlobals;*)
  file.globals <- keepGlobals @ GVarDecl (initer.svar, nullLocation)
				:: file.globals @ [GFun (initer, nullLocation)];
(*
  let ig = new initGlobals in
  inits := [];
  let file = Cil.visitCilFile ig file in
  initer.sbody <- {battrs = []; bstmts = [mkStmt (Instr (List.rev (!inits)))]};
  let main = Common.get_fundec file "main" in
  main.sbody <- {main.sbody with
   bstmts = mkStmt (Instr [Call (None, Lval (Var initer.svar, NoOffset), [], nullLocation)]) :: main.sbody.bstmts};
*)
  file


