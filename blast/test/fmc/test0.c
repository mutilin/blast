/* Shows the use of __BLAST_NONDET.
   The while loop is traversed an arbitrary
   number of times.

	blast.opt test1.c
*/
int err(){
 ERROR: goto ERROR;
}

main () {
	int __BLAST_NONDET;
	int x,y, temp_x, temp_y;
	int *p;


	x = 0;
	while (__BLAST_NONDET) {
		y++;
	} 
	if (x!=0) err();



}
