// This gives a bogus counterexample as we don't encode:
// i=j -> a[i] = a[j] 
// need to generalize the boolean closure operation of the POPL04 paper

int skip;
int x[10], y[10];
int i,j,n;

main(){
    for(i = 0; i < n; i++){
	x[i] = i;
    }

    if(j >= 0 && j < n - 1){
	if (x[j] > x[j+1]) 
    		ERROR: goto ERROR;
    }
  
END : goto END;

}
