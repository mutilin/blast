/* SHows the use of __BLAST_NONDET.
   The while loop is traversed an arbitrary
   number of times.

	blast.opt test1.c
*/


int x,y;

main () {
	int __BLAST_NONDET;
	x = 0;
	y = 0;
	while (__BLAST_NONDET) {
		x++;
		y++;
	} 
	if (x!=y) 
ERROR: goto ERROR;
}
