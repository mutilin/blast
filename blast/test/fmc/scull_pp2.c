# 1 "scull.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "scull.c"
# 25 "scull.c"
void *malloc(int size);

void memset(void *ptr, int val, int length);


int sel(int mem, int p);
int ptr(int addr, int field);
int upd(int mem, int addr, int val);
int updflatrange(int mem, int lo, int hi, int v);
int updbothranges(int mem, int loAddr, int hiAddr, int loIndex, int hiIndex, int v);
int canReach(int mem, int field, int src, int dest);
int canReachWO(int mem, int field, int src, int dest, int avoid);
int noCycles(int mem, int field, int ptr);
int disjoint(int mem, int field, int p, int q);
int ptr_field(int ptr);
int ptr_addr(int ptr);
int metaVar(int whatever);



int const dead;
int const live;



int mem;
int meta;
int *tag;
int objsize;
int objct;






void kfree(void *freedPtr){
  if(tag[(int)freedPtr] != 0)
    tag[(int)freedPtr] = 0;
  else ;
}





typedef int loff_t;
typedef int ssize_t;
typedef int size_t;
typedef int filldir_t;
typedef int kdev_t;
typedef int pid_t;




void printf(char const *fmt, ...);
# 98 "scull.c"
struct file;
struct poll_table_struct;
struct inode;
struct vm_area_struct;

typedef struct file_operations {

        loff_t (*llseek) (struct file *, loff_t, int);
        ssize_t (*read) (struct file *, char *, size_t, loff_t *);
        ssize_t (*write) (struct file *, const char *, size_t, loff_t *);
        int (*readdir) (struct file *, void *, filldir_t);
        unsigned int (*poll) (struct file *, struct poll_table_struct *);
        int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned long);
        int (*mmap) (struct file *, struct vm_area_struct *);
        int (*open) (struct inode *, struct file *);
        int (*flush) (struct file *);
        int (*release) (struct inode *, struct file *);







} file_operations;


typedef int ModuleId;
typedef enum FileStateEnum { FS_CLOSED, FS_OPEN } FileState;



typedef struct file {



        struct file_operations *f_op;

        unsigned int f_flags;

        loff_t f_pos;
# 147 "scull.c"
        void *private_data;






        ModuleId id;
        FileState state;
} file;



typedef struct inode {
# 175 "scull.c"
        kdev_t i_rdev;
# 242 "scull.c"
} inode;



int register_chrdev(unsigned int, const char *, struct file_operations *);
int unregister_chrdev(unsigned int, const char *);



typedef struct task_struct {
# 306 "scull.c"
        pid_t pid;
# 383 "scull.c"
} task_struct;
struct task_struct *current;
# 408 "scull.c"
unsigned long copy_to_user(void *to, const void *from, unsigned long n){
}

unsigned long copy_from_user(void *to, const void *from, unsigned long n){
}



int access_ok(int type, void *addr, int size);
# 511 "scull.c"
typedef int Err;


typedef int bool;
# 533 "scull.c"
enum {

  DF_LEAKS = 'L',
  DF_OPEN_RELEASE = 'O',
  DF_SYSCALL = 'Y',
  DF_SIGNAL = 'G',
  DF_SLEEP = 'S',
  DF_BADIOCTL = 'B',

};
# 556 "scull.c"
struct semaphore {};
# 648 "scull.c"
  typedef void * devfs_handle_t;
# 659 "scull.c"
extern inline void devfs_unregister(devfs_handle_t de) {}


extern devfs_handle_t scull_devfs_dir;


typedef struct Scull_Dev {
   void **data;
   struct Scull_Dev *next;
   int quantum;
   int qset;
   unsigned long size;
   devfs_handle_t handle;
   unsigned int access_key;
   struct semaphore sem;


  struct Scull_Dev *prev;

} Scull_Dev;
# 690 "scull.c"
extern struct file_operations scull_fops;
extern struct file_operations scull_priv_fops;
extern struct file_operations scull_pipe_fops;
extern struct file_operations scull_sngl_fops;
extern struct file_operations scull_user_fops;
extern struct file_operations scull_wusr_fops;




extern int scull_major;
extern int scull_nr_devs;
extern int scull_quantum;
extern int scull_qset;

extern int scull_p_nr_devs;
extern int scull_p_buffer;






int scull_p_init(void);
void scull_p_cleanup(void);
int scull_access_init(void);
void scull_access_cleanup(void);

int scull_trim(Scull_Dev *dev);

ssize_t scull_read (struct file *filp, char *buf, size_t count,
                    loff_t *f_pos);
ssize_t scull_write (struct file *filp, const char *buf, size_t count,
                     loff_t *f_pos);
loff_t scull_llseek (struct file *filp, loff_t off, int whence);
int scull_ioctl (struct inode *inode, struct file *filp,
                     unsigned int cmd, unsigned long arg);
# 777 "scull.c"
enum ModuleState { MS_UNLOADED, MS_LOADED } module_state = MS_UNLOADED;






int scull_major = 0;

int scull_quantum = 4000;
int scull_qset = 1000;







Scull_Dev *scull_devices;






struct file_operations *scull_fop_array[]={
    &scull_fops,
    &scull_priv_fops,
    &scull_pipe_fops,
    &scull_sngl_fops,
    &scull_user_fops,
    &scull_wusr_fops
};



int scull_trim(Scull_Dev *dev)
{
    Scull_Dev *next, *dptr;
    int qset = dev->qset;
    int i;

    for (dptr = dev; dptr; dptr = next) {
        if (dptr->data) {
            for (i = 0; i < qset; i++)
                if (dptr->data[i])
                    kfree(dptr->data[i]);
            kfree(dptr->data);
            dptr->data=0;
        }
        next=dptr->next;
        if (dptr != dev) kfree(dptr);
    }
    dev->size = 0;
    dev->quantum = scull_quantum;
    dev->qset = scull_qset;
    dev->next = 0;
    return 0;
}
# 928 "scull.c"
int scull_open(struct inode *inode, struct file *filp)
{
    Scull_Dev *dev;
    int num = ((inode->i_rdev & 0xFF) & 0xf);
    int type = ((inode->i_rdev & 0xFF) >> 4);
# 945 "scull.c"
    if (!filp->private_data && type) {
        if (type > 5) return -19;
        filp->f_op = scull_fop_array[type];
        return filp->f_op->open(inode, filp);
    }


    dev = (Scull_Dev *)filp->private_data;
    if (!dev) {
        if (num >= scull_nr_devs) return -19;
        dev = &scull_devices[num];
        filp->private_data = dev;
    }

    ;

    if ( (filp->f_flags & 0003) == 01) {
        if (down_interruptible(&dev->sem)) {
            ;
            return -513;
        }
        scull_trim(dev);
        up(&dev->sem);
    }

    return 0;
}

int scull_release(struct inode *inode, struct file *filp)
{
    ;
    return 0;
}






Scull_Dev *scull_follow(Scull_Dev *dev, int n)
{
    while (n--) {
        if (!dev->next) {
            dev->next = malloc(1);
            memset(dev->next, 0, 1);

            dev->next->prev = dev;

        }
        dev = dev->next;
        continue;
    }
    return dev;
}





ssize_t scull_read(struct file *filp, char *buf, size_t count,
                loff_t *f_pos)
{
    Scull_Dev *dev = filp->private_data;
    Scull_Dev *dptr;
    int quantum = dev->quantum;
    int qset = dev->qset;
    int itemsize = quantum * qset;
    int item, s_pos, q_pos, rest;
    ssize_t ret = 0;

    if (down_interruptible(&dev->sem))
            return -513;
    if (*f_pos >= dev->size)
        goto out;
    if (*f_pos + count > dev->size)
        count = dev->size - *f_pos;

    item = (long)*f_pos / itemsize;
    rest = (long)*f_pos % itemsize;
    s_pos = rest / quantum; q_pos = rest % quantum;


    dptr = scull_follow(dev, item);

    if (!dptr->data)
        goto out;
    if (!dptr->data[s_pos])
        goto out;

    if (count > quantum - q_pos)
        count = quantum - q_pos;

    if (copy_to_user(buf, dptr->data[s_pos]+q_pos, count)) {
        ret = -14;
        goto out;
    }
    *f_pos += count;
    ret = count;

 out:
    up(&dev->sem);
    return ret;
}

ssize_t scull_write(struct file *filp, const char *buf, size_t count,
                loff_t *f_pos)
{
    Scull_Dev *dev = filp->private_data;
    Scull_Dev *dptr;
    int quantum = dev->quantum;
    int qset = dev->qset;
    int itemsize = quantum * qset;
    int item, s_pos, q_pos, rest;
    ssize_t ret = -12;

    if (down_interruptible(&dev->sem))
            return -513;


    item = (long)*f_pos / itemsize;
    rest = (long)*f_pos % itemsize;
    s_pos = rest / quantum; q_pos = rest % quantum;


    dptr = scull_follow(dev, item);
    if (!dptr->data) {
        dptr->data = malloc(qset * 1);
        if (!dptr->data)
            goto out;
        memset(dptr->data, 0, qset * 1);
    }
    if (!dptr->data[s_pos]) {
        dptr->data[s_pos] = malloc(quantum);
        if (!dptr->data[s_pos])
            goto out;
    }

    if (count > quantum - q_pos)
        count = quantum - q_pos;

    if (copy_from_user(dptr->data[s_pos]+q_pos, buf, count)) {
        ret = -14;
        goto out;
    }
    *f_pos += count;
    ret = count;


    if (dev->size < *f_pos)
        dev-> size = *f_pos;

  out:
    up(&dev->sem);
    return ret;
}
# 1111 "scull.c"
int scull_ioctl(struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{

    int err = 0, tmp;
    int ret = 0;





    if ((((cmd) >> (0 +8)) & ((1 << 8)-1)) != 'k') return -514;
    if ((((cmd) >> 0) & ((1 << 8)-1)) > 15) return -514;







    if ((((cmd) >> (((0 +8)+8)+14)) & ((1 << 2)-1)) & 2U)
        err = !access_ok(1, (void *)arg, (((cmd) >> ((0 +8)+8)) & ((1 << 14)-1)));
    else if ((((cmd) >> (((0 +8)+8)+14)) & ((1 << 2)-1)) & 1U)
        err = !access_ok(0, (void *)arg, (((cmd) >> ((0 +8)+8)) & ((1 << 14)-1)));
    if (err) return -14;

    switch(cmd) {
# 1152 "scull.c"
      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((0)) << 0) | ((0) << ((0 +8)+8))):
        scull_quantum = 4000;
        scull_qset = 1000;
        break;

      case (((1U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((1)) << 0) | ((1) << ((0 +8)+8))):
        if (! 0)
            return -515;
        ret = (scull_quantum = *((int *)arg), 0);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((3)) << 0) | ((0) << ((0 +8)+8))):
        if (! 0)
            return -515;
        scull_quantum = arg;
        break;

      case (((2U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((5)) << 0) | ((1) << ((0 +8)+8))):
        ret = __put_user(scull_quantum, (int *)arg);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((7)) << 0) | ((0) << ((0 +8)+8))):
        return scull_quantum;

      case (((2U|1U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((9)) << 0) | ((1) << ((0 +8)+8))):
        if (! 0)
            return -515;
        tmp = scull_quantum;
        ret = (scull_quantum = *((int *)arg), 0);
        if (ret == 0)
            ret = __put_user(tmp, (int *)arg);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((11)) << 0) | ((0) << ((0 +8)+8))):
        if (! 0)
            return -515;
        tmp = scull_quantum;
        scull_quantum = arg;
        return tmp;

      case (((1U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((2)) << 0) | ((1) << ((0 +8)+8))):
        if (! 0)
            return -515;
        ret = (scull_qset = *((int *)arg), 0);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((4)) << 0) | ((0) << ((0 +8)+8))):
        if (! 0)
            return -515;
        scull_qset = arg;
        break;

      case (((2U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((6)) << 0) | ((1) << ((0 +8)+8))):
        ret = __put_user(scull_qset, (int *)arg);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((8)) << 0) | ((0) << ((0 +8)+8))):
        return scull_qset;

      case (((2U|1U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((10)) << 0) | ((1) << ((0 +8)+8))):
        if (! 0)
            return -515;
        tmp = scull_qset;
        ret = (scull_qset = *((int *)arg), 0);
        if (ret == 0)
            ret = (*((int *)arg) = tmp, 0);
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((12)) << 0) | ((0) << ((0 +8)+8))):
        if (! 0)
            return -515;
        tmp = scull_qset;
        scull_qset = arg;
        return tmp;







      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((13)) << 0) | ((0) << ((0 +8)+8))):
        scull_p_buffer = arg;
        break;

      case (((0U) << (((0 +8)+8)+14)) | ((('k')) << (0 +8)) | (((14)) << 0) | ((0) << ((0 +8)+8))):
        return scull_p_buffer;


      default:
        return -514;
    }
    return ret;

}
# 1363 "scull.c"
loff_t scull_llseek(struct file *filp, loff_t off, int whence)
{
    Scull_Dev *dev = filp->private_data;
    loff_t newpos;

    switch(whence) {
      case 0:
        newpos = off;
        break;

      case 1:
        newpos = filp->f_pos + off;
        break;

      case 2:
        newpos = dev->size + off;
        break;

      default:
        return -22;
    }
    if (newpos<0) return -22;
    filp->f_pos = newpos;
    return newpos;
}
# 1423 "scull.c"
struct file_operations scull_fops = {
    llseek: scull_llseek,
    read: scull_read,
    write: scull_write,
    ioctl: scull_ioctl,
    open: scull_open,
    release: scull_release,
};
# 1446 "scull.c"
void scull_cleanup_module(void)
{
    int i;



    unregister_chrdev(scull_major, "scull");





    if (scull_devices) {
        for (i=0; i<scull_nr_devs; i++) {
            scull_trim(scull_devices+i);

            devfs_unregister(scull_devices[i].handle);
        }
        kfree(scull_devices);
    }


    scull_p_cleanup();
    scull_access_cleanup();


    devfs_unregister(scull_devfs_dir);

}


int scull_init_module(void)
{
    int result, i;

    SET_MODULE_OWNER(&scull_fops);
# 1494 "scull.c"
    result = register_chrdev(scull_major, "scull", &scull_fops);
    if (result < 0) {
        printf("<4>" "scull: can't get major %d\n",scull_major);
        return result;
    }
    if (scull_major == 0) scull_major = result;






    scull_devices = malloc(scull_nr_devs * 1);
    if (!scull_devices) {
        result = -12;
        goto fail;
    }
    memset(scull_devices, 0, scull_nr_devs * 1);
    for (i=0; i < scull_nr_devs; i++) {
        scull_devices[i].quantum = scull_quantum;
        scull_devices[i].qset = scull_qset;
        sema_init(&scull_devices[i].sem, 1);

        scull_devices[i].next = 0;
# 1527 "scull.c"
    }
# 1539 "scull.c"
    ;






    module_state = MS_LOADED;
    return 0;

  fail:
    scull_cleanup_module();
    return result;
}







error(){
 ERROR: goto ERROR;
}

main(){
# 1588 "scull.c"
  int j;

  if(module_state == MS_UNLOADED)
    if(scull_init_module() >= 0){
      if(0 <= j && j < scull_nr_devs){
        scull_follow(&scull_devices[j],1);
        if(scull_devices[j].next->prev != &scull_devices[j])
          error();
      }
    }




}
