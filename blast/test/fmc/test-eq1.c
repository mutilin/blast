/* Shows the use of __BLAST_NONDET.
   The while loop is traversed an arbitrary
   number of times.

	blast.opt test1.c
*/
main () {
	int __BLAST_NONDET;
	int x,y, temp_x, temp_y;
	int *p;
	int c;

	if (c > 0) {

	x = c;
	y = c;
	while (__BLAST_NONDET) {
		x++;
		y++;
	} 

	while (x > 0){
		x--;
		y--;
	}
	if (y != 0) 
		ERROR: goto ERROR;
}
END: goto END;

}
