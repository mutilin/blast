/* Shows the use of __BLAST_NONDET.
   The while loop is traversed an arbitrary
   number of times.

	blast.opt test1.c
*/
	int __BLAST_NONDET;
	int x,y, temp_x, temp_y;
	int c,k;


main () {

	{__blockattribute__((assume(c > 0))) }
	
	x = c;
	y = c+k;
	while (__BLAST_NONDET) {
		x++;
		y++;
	} 

	while (x > 0){
		x--;
		y--;
	}
	if (y != k) 
		ERROR: goto ERROR;

END: goto END;

}
