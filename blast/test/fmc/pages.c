int pages[4];
int Npages;

void error() {
ERROR: ;
}

void map (int i) {
	if (pages[i] != -1) {
		error();
	}
        pages[i] = 0; // map page
}

void die () {
LOOP: goto LOOP;
}

int main() {
	int i;
	if (Npages <= 0) {
		die();
	}

	// initialize page table
	for (i= 0; i < Npages ; i++) {
		pages[i] = -1;
	}

	while(1){
	// allocate page table
	for (i = 0 ; i < Npages; i++) {
		if (pages[i] == -1) 
			break;
	}
	if (i == Npages) return;
	// check that returned page is unmapped	
	map(i);
	}
}

