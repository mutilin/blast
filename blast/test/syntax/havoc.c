int x __attribute__((__BLAST_skolem));

void main(){

  x = 0;

 {__blockattribute__((havoc(x)))};

 {__blockattribute__((assume(x==0)))};

  if (x != 0) { ERROR: goto ERROR;};
}
