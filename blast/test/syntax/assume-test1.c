//a test for the "assume" functionality.

typedef int idx __attribute__((foo));

 idx k __attribute__((skolem)); 
//UNSAFE idx k; 
idx __BLAST_q_i;

int x,y,z;
int a[1000];

char *s;

main () {


	{__blockattribute__((assume(a(__BLAST_q_i)==0)))}
//	{__blockattribute__((assume(a[__BLAST_q_i]==0)))}

//	{__blockattribute__((assume(x==y && y == z)))}
  
	if (a[k]!=0) 
	    ERROR: goto ERROR;
}
