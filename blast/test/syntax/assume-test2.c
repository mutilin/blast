//a test for the "assume" functionality.

typedef int idx __attribute__((id1));
typedef int idy __attribute__((id2));

idx p, __BLAST_q_i;
idy q, __BLAST_q_j;

int x,y,z;
int a[1000][1000];

char *s;

main () {


	{__blockattribute__((assume(a(__BLAST_q_i,__BLAST_q_j)==0)))}

  
	if (a[p][q] !=0) 
	    ERROR: goto ERROR;
}
