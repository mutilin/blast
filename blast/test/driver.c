int STATUS_SUCCESS = 0;
int STATUS_UNSUCCESSFUL = -1;
struct Irp {
	int Status;
	int Information;
};

struct Requests {
	int Status;
	struct Irp *irp;
	struct Requests *Next;	
};

struct Device {
	struct Requests *WriteListHeadVa; 
	int writeListLock;
};

/* FSM Specification for the locking discipline */
int lockStatus;
int a;

void errorFn(){
 ERROR: goto ERROR;
}

void FSMLock() {
                if(lockStatus==0) {
                        lockStatus = 1;
                } else {
                        errorFn();
                }
	return;

}
void FSMUnLock() {
                if(lockStatus==1) {
                        lockStatus = 0;
                } else {
                        errorFn();
                }
}

// stubs
void SmartDevFreeBlock(struct Requests *r) {
}

void IoCompleteRequest(struct Irp *irp, int status) {
}


struct Device devE;

void main () {
 int IO_NO_INCREMENT = 3;
 int nPacketsOld, nPackets;
 struct Requests *request;
 struct Irp *irp;
 struct Device *devExt;

 devExt = &devE;	

 lockStatus = 0; /* Initialize FSM */

 /* driver code */
	// instrumentation

	// end instrumentation


 LOOP: {
	    FSMLock();
	    nPacketsOld = nPackets; 
	    
	    request = (*devExt).WriteListHeadVa;
	    
	    if(request!=0 && (*request).Status!=0){
		(*devExt).WriteListHeadVa = (*request).Next;	
		
		FSMUnLock();
		irp = (*request).irp;
		
		if((*request).Status >0) {
		    (*irp).Status = STATUS_SUCCESS;
		    (*irp).Information = (*request).Status;
		} else {
		    (*irp).Status = STATUS_UNSUCCESSFUL;
		    (*irp).Information = (*request).Status;
		}	
		SmartDevFreeBlock(request);
		IO_NO_INCREMENT = 3;
		/* IoCompleteRequest(irp, IO_NO_INCREMENT);*/
		nPackets = nPackets + 1;
	    }
	} 
	if (nPackets != nPacketsOld) goto LOOP;
	
	FSMUnLock();
}
