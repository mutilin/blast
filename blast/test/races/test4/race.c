int m_inode __attribute__((lock));
int m_busy __attribute__((lock));
int block, busy, inode;
int tid __attribute__((lock));

void acquire_1() 
__attribute__((atomic))
{
    {__blockattribute__(assume(m_inode == 0))}

    m_inode = tid;
}

void acquire_2() 
__attribute__((atomic))
{
    {__blockattribute__(assume(m_busy == 0))}

    m_busy = tid;
}

void release_1() 
__attribute__((atomic))
{
    m_inode = 0;
}

void release_2() 
__attribute__((atomic))
{
    m_busy = 0;
}

void main() {
if(tid == 1) {
  acquire_1();
  if(inode == 0) {
	acquire_2();
	busy = 1;
	release_2();
	inode = 1;
  }
  block = 1;

  release_1(); 
} 
if(tid == 2) {

  acquire_2();
  if(busy == 0) {
	block = 0;
  }
  release_2(); 
}

}


