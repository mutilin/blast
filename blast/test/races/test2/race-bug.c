int flag __attribute__((lock)) ;
int data;

int tid __attribute__((lock)) ;
int notTid __attribute__((lock)) ;

int main () {

	if (tid == 1) {
		data = 40;
		flag = 1;
	} else {
		if (tid == 2) {
			while (flag == 1) ;
			data = 50;
		}
	}
}
