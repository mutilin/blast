int MAX_THREADS __attribute__((lock));
int writer __attribute__((lock));
int *readers __attribute__((lock));
int tid __attribute__((lock));
int x;

void acq_rlock() 
__attribute__((atomic))
{
    {__blockattribute__(assume(writer == 0))}

    readers[tid] = 1;
}

void acq_wlock() 
__attribute__((atomic))
{
    {__blockattribute__(assume(writer == 0))}
    for (int i = 1; i < MAX_THREADS; i++) {
        {__blockattribute__(assume(readers[i] == 0))}
    }
    writer = tid;
}

void rel_rlock()
__attribute__((atomic))
{
    readers[tid] = 0;
}

void rel_wlock()
__attribute__((atomic))
{
    writer = 0;
}


void main() {
    int t;

    acq_wlock();
    x++;
    rel_wlock();

    acq_rlock();
    t = x;
    rel_rlock();
}
