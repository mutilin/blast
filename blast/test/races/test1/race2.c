struct internal {
	int io __attribute__((lock));
	char * cmd;
};
struct driver {
	int data;
	int lock1 __attribute__((lock));
	struct internal *Io;
};

//int lock2 __attribute__((lock));
struct driver dev;

int tid __attribute__((lock));

void acquire(int *l) 
__attribute__((atomic))
{
    {__blockattribute__(assume(sel (l) == 0))}

    *l = tid;
}

void release(int *l) 
__attribute__((atomic))
{
    *l = 0;
}


int main() {
  int __BLAST_NONDET;
  int *local;
  int *t = &dev.lock1;

  acquire(t);
  local = &dev.data;
  release(t);
  *local = 0;
/*
  t = &lock2;
  acquire(t);
  dev.data++;
  release(t);
*/
}


