int m __attribute__((lock));
int n __attribute__((lock));
int x;
int tid __attribute__((lock));

void acquire(int *l) 
__attribute__((atomic))
{
    {__blockattribute__(assume(sel (at(l, acquire)) == 0))}

    *l = tid;
}

void release(int *l) 
__attribute__((atomic))
{
    *l = 0;
}


int main() {
  int __BLAST_NONDET;
  int *t;
  if(__BLAST_NONDET)
	t = &m;
  else
	t = &n;
  acquire(t);
  x++;
  release(t);
}


