int m __attribute__((lock));
int x;
int tid __attribute__((lock));

void acquire() 
__attribute__((atomic))
{
    {__blockattribute__(assume(m == 0))}

    m = tid;
}

void release() 
__attribute__((atomic))
{
    m = 0;
}


int main() {
  int t;
  acquire();
  x = 0;
  if (x!=0) {
ERROR: ;
  }
  release();
}


