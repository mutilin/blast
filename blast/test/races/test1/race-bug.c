int m __attribute__((lock));
int n __attribute__((lock));
int x;
int tid __attribute__((lock));

void acquire_1() 
__attribute__((atomic))
{
    {__blockattribute__(assume(m == 0))}

    m = tid;
}

void acquire_2() 
__attribute__((atomic))
{
    {__blockattribute__(assume(n == 0))}

    n = tid;
}

void release_1() 
__attribute__((atomic))
{
    m = 0;
}

void release_2() 
__attribute__((atomic))
{
    n = 0;
}

void main() {
	int t;
if(tid == 1) {
  //acquire_1();
//  x++;
  t=x;
  m=0;
} else {
if(tid == 2) {

  acquire_2();
  x = 0;
  n=0;
}
}

}


