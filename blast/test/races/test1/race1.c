int m __attribute__((lock));
int x;
int tid __attribute__((lock));

void acquire(int *l) 
__attribute__((atomic))
{
//    {__blockattribute__(assume(* at(l,acquire) == 0))}
    {__blockattribute__(assume(sel (at (l, acquire)) == 0))}

    *l = tid;
}

void release(int *l) 
__attribute__((atomic))
{
    *l = 0;
}


int main() {
  int __BLAST_NONDET;
  int *t;
  t = &m;
  acquire(t);
  x++;
  release(t);
}


