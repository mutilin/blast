
struct lock {
	int mutex __attribute__((lock)); 
	int exclusive, shared;

	int data;
};

struct lock lockarray[10] ;

int tid __attribute__((lock));

void errorFn() {
ERROR: goto ERROR;
}

void acquire(int *lock) 
__attribute__((atomic))
{
    while (*lock != 0) ;
    *lock = tid;


}
void release(int *lck) 
__attribute__((atomic))
{
    if (*lck != tid) {
	errorFn();
    }
    * lck = 0;

}

void LWacquire(int i, int mode) {
    int mustwait;

    struct lock *lock = lockarray + i;
    if (lock->shared >= 0) {
	for(;;) {
   		acquire( &(lock->mutex) );

   /* If I can get the lock, do so quickly. */
		if (mode == 0/* exclusive */)
		{
			if (lock->exclusive == 0 && lock->shared == 0)
			{
				lock->exclusive = tid;
				mustwait = 0;
			}
			else
				mustwait = 1;
		}
		else
		{
			if (lock->exclusive == 0)
			{
				lock->shared++;
				mustwait = 0;
			}
			else
				mustwait = 1;
		}
                if (!mustwait) 
			break;
   		release( & (lock->mutex) );
		/* and go to sleep */
	}
	release( & (lock->mutex) );

	if (lock->exclusive == 0) {
		if (lock->shared ==0) 
			errorFn();
	}
	if (lock->exclusive != 0) {
		if (lock->shared != 0) {
			errorFn();
		}
	}	
        } else {
	STUCK: goto STUCK;
	}
}

void LWrelease (int i, int mode) {
	struct lock *m = lockarray + i;
	acquire ( & (m-> mutex) );
	if (mode == 0) {
		m->exclusive = 0 ;
	} else {
		m->shared -- ;
	}	
	release ( & (m->mutex) );
}


int main() {
  int i;
  int __BLAST_NONDET;
  int mode; int cpy;
      if (tid == 1) {
	LWacquire(i, 1);
	lockarray[i].data = 0;
	LWrelease(i,1);
      } else {
	 if (tid == 2) {
	//LWacquire(i, 1);
        cpy = 	lockarray[i].data;
	//LWrelease(i,1);
	 }
      }
      /*
      LWacquire(i, mode);
      if (mode == 0) {
	lockarray[i].data = 0 ;
      } else {
	cpy = lockarray[i].data;
      }
      LWrelease(i, mode);
      */
}


