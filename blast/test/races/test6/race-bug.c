int m __attribute__((lock));

struct foo {
	int a;
	int b;
};

int x;
int tid __attribute__((lock));
int notTid __attribute__((lock));

int acquire() 
__attribute__((atomic))
{
int __BLAST_NONDET;
{__blockattribute__(assume(m==0))}

if (__BLAST_NONDET){
        m = tid; return 1;
} else {
        return 0;
}
}

void release() 
__attribute__((atomic))
{
    m = 0;
}

void foo () {
	int loc;
	struct foo foo_struct;

	loc = 0;
	//foo_struct.a = 1;
}

main(){
int t;
foo();
t = acquire();
if (t == 0) {
        x++;
	release();
}
}



