int tid __attribute__((lock));

typedef 
struct _irp { int data; int flag __attribute__((lock)); } IRP;


IRP irp;
IRP *device = &irp;

void put(IRP *p, int data) 
__attribute__((atomic))
{
    {__blockattribute__(assume(dot(sel(at(p,put)),flag) == 0))}
	p->data = data;
	p->flag = 1;
}

int get(IRP *p) 
__attribute__((atomic))
{
  IRP *t;
  {__blockattribute__(assume(dot(sel(at(p,get)),flag) == 0))}
  t = p->data;
  p->flag = 0;
  return t;
}
  
int m __attribute__((lock));
void acquire() {
	{__blockattribute__(assume(m==0))}
	m = tid;
}

void release () {
	m = 0;
}

void main() {
  int __BLAST_NONDET;
  IRP *pc, *pp;
  int foo;
  int t;

/*
acquire();
device = & irp;
release();
*/

  if (tid == 1) {
    acquire();
    //pc = device;
    pc = &irp;
    release();

    while (__BLAST_NONDET) {
      t = get(pc);
    }
  } else {
	if (tid == 2) {
		acquire();
		// pp = device;
		pp = & irp;
		release();
			
		while(__BLAST_NONDET) {
			foo++;
    			put(pp, foo);
		}
	}
  }
}

