int m __attribute__((lock));
int n __attribute__((lock));
int x;
int tid __attribute__((lock));
int notTid __attribute__((lock));

void acquire_1() 
__attribute__((atomic))
{
    {__blockattribute__(assume(m == 0))}

    m = tid;
}

void acquire_2() 
__attribute__((atomic))
{
    {__blockattribute__(assume(n == 0))}

    n = tid;
}

void release_1() 
__attribute__((atomic))
{
    m = 0;
}

void release_2() 
__attribute__((atomic))
{
    n = 0;
}

void main() {
  {__blockattribute__(assume(tid!=0))}

  acquire_1();
  x++;
  release_1(); 

  acquire_2();
  x++;
  release_2(); 

}


