int tid __attribute__((lock));
int notTid __attribute__((lock));

int data;
int lck __attribute__((lock));


void acquire(int *l) __attribute__((atomic)) {
	{__blockattribute__((assume(sel(at(l,acquire))==0)))}
	*l = tid;
}

void release (int *l) __attribute__((atomic)) {
	*l = 0;
}	

int main () {

	acquire(&lck);


	data ++ ;

	release(&lck);
}
