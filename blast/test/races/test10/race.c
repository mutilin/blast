int tid __attribute__((lock));

struct net_device {
	int data;
	int lock __attribute__((lock));
};

struct net_device netdev;

void acquire(int *l) __attribute__((atomic)) {
	{__blockattribute__((assume(sel(at(l,acquire))==0)))}
	*l = tid;
}

void release (int *l) __attribute__((atomic)) {
	*l = 0;
}	

int main () {
	int *l;
	l = & netdev.lock;
	acquire(l);
	netdev.data ++;
	release(l);
}
