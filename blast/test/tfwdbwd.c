struct irp {
	int status;
};

int s;


void errorFn() {
	ERROR: goto ERROR;
}


void foo (struct irp *Irp) {
	Irp->status = 17;
	if (s==0)
		s = 1;
	else 
		errorFn();
}

struct irp *pirp;
struct irp *p_other;
struct irp r;
int main () {
	//pirp = (struct irp *)malloc(sizeof(struct irp));
	r.status = 0;
	pirp = &r;
	s = 0;
	pirp->status = 0;
	p_other = pirp;
	p_other->status = 1;
	foo (pirp);
	if (p_other->status==17)
		errorFn();

}
