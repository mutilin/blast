/* Two test cases for field offsets.
 * Both should pass. They do under non-cf. Under -cf, the second fails. It
 * looks like the heap state is being dropped in the second call to test().
 */

struct device {
  int devno;
  void *container;
};
struct scsi_device {
  int host_lock;
  struct device gendev;
};

void __ERROR__() {
ERROR: goto ERROR;
}

void test(struct device *devp)
{
    struct scsi_device *sdev = (struct scsi_device *)devp->container;
    if (sdev->host_lock!=1) {
	__ERROR__();
    }
}  

int main(int argc, char* argv[]) {
  struct scsi_device sdev;
  struct scsi_device *sdevp;

  sdev.host_lock = 1;
  sdev.gendev.container = &sdev;
  test(&(sdev.gendev));

  sdevp = (struct scsi_device*)malloc(sizeof(struct scsi_device));
  sdevp->host_lock = 1;
  sdevp->gendev.container = sdevp;
  test(&(sdevp->gendev));

  return 0;
}

