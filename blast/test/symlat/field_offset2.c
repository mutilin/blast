/* Testcase for passing field offsets between functions.
 * Created 3/28/05
 * Derived from linux driver scsi/scsi_sysfs.c.
 * The checks should pass. Unfortunately, they currently do not. This
 * is due to limitations in the handling of unitialized structure pointers.
 * When an uninitialized pointer is referenced in eval(), we should create
 * a new pointer on the heap. Otherwise, something like the following fails:
 *   sdev->host = host;
 *   host->host_lock->lock = 1;
 *   host->host_lock->magic = 0x1D244B3C;
 * In the current implementation, separate structures are created for
 *  host->host_lock->lock and host->host_lock->magic.
 */
struct device {
  void *container;
};
typedef struct {
  int lock;
  int magic;
} spinlock_t;
struct Scsi_Host {
  spinlock_t *host_lock;
};
struct scsi_device {
  struct Scsi_Host *host;
  struct device     sdev_gendev;
};

void __ERROR__() {
 ERROR: goto ERROR;
}

void get_lock(struct device *dev)
{
  struct scsi_device *sdev;
  sdev = ((struct scsi_device*)(dev->container));

  if ((sdev->host->host_lock)->magic != 0x1D244B3C) {
    __ERROR__();
  }
  if (!((sdev->host->host_lock)->lock==0)) {
    __ERROR__();
  }
  sdev->host->host_lock->lock = 1;
}

extern struct scsi_device* blast_nondet_sdev(void);
extern struct Scsi_Host* blast_nondet_shost(void);

int main(int argc, char* argv[]) {
  struct scsi_device  *sdev;
  struct Scsi_Host    *host;

  sdev = blast_nondet_sdev();
  host = blast_nondet_shost();
  
  sdev->host = host;
  /* setup the parent pointer for the generic device */
  sdev->sdev_gendev.container = (void*)sdev;
  host->host_lock->lock = 0;
  host->host_lock->magic = 0x1D244B3C;

  get_lock(&(sdev->sdev_gendev));

  if (host->host_lock->lock!=1) __ERROR__();
}
