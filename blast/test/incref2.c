//alias analysis fooled by lack of malloc. bogus test (?)
typedef struct __FILE { int BLAST_FLAG; } FILE ;
void *malloc(int t);
void errorFn() { ERROR: goto ERROR; }

FILE *get_stderr() {
/*
    FILE *f = (FILE *)malloc(sizeof(FILE));
/*
    f->BLAST_FLAG = 1; return f;
*
    if (f->BLAST_FLAG==1) {
    	return f;
    } else {
STUCK: goto STUCK;
    }
*/
}

FILE *fopen (char *fname) {
/*
    FILE *f = (FILE *)malloc(sizeof(FILE));
    f->BLAST_FLAG = 1; return f; 
*/
}

int fclose (FILE *f) {
/*
    if (f->BLAST_FLAG==0) {
	errorFn(); 
    }
    f->BLAST_FLAG = 0;
*/
}

void fprintf(FILE *f) {
/*
    if (f->BLAST_FLAG==0) {
	errorFn(); 
    } 
*/
}

int main() {
    FILE *f, *out;
    char *outfile;
    while (1) {
    if (outfile!=0) {
	f = fopen(outfile);
	f->BLAST_FLAG = 1; // instrumentation
        out = f; 
    } else {
    	f = get_stderr();
	f->BLAST_FLAG = 1; // instrumentation
        out = f; 
    }
    if (out->BLAST_FLAG==0) errorFn();
    fprintf(out);
    if (f->BLAST_FLAG==0) errorFn();
    fprintf(f);
    if (f->BLAST_FLAG==0) errorFn();
    f->BLAST_FLAG = 0;
    fclose(f);

	/* Sound */
    if (f->BLAST_FLAG!=0) errorFn();
    fprintf(f);

	/* Unsound */
	/* in the absence of alias info, out->BLAST_FLAG is still 1!! */
	if (out->BLAST_FLAG!=0) errorFn();
	fprintf(out);
    }
}
