struct _IO_marker;
# 3 "file_openclose.h"
typedef long __off_t;
# 4
typedef long long __off64_t;
# 6
typedef void _IO_lock_t;
# 9
struct _IO_FILE {
   int __BLAST_FLAG ;
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   int _mode ;
   char _unused2[(int )(15U * sizeof(int ) - 2U * sizeof(void *))] ;
};
# 39
typedef struct _IO_FILE FILE;
# 2
struct _IO_marker;
# 2 "spec.work"
void __error__(void) 
{ 

  {
  ERROR: 
  goto ERROR;
}
}
void __initialize__(void) ;
# 201 "/usr/lib/gcc-lib/i386-redhat-linux/3.2/include/stddef.h"
typedef unsigned int size_t;
# 35 "/usr/include/bits/types.h"
typedef unsigned short __u_short;
# 131
typedef unsigned long long __dev_t;
# 132
typedef unsigned int __uid_t;
# 133
typedef unsigned int __gid_t;
# 134
typedef unsigned long __ino_t;
# 136
typedef unsigned int __mode_t;
# 137
typedef unsigned int __nlink_t;
# 138
typedef long __off_t;
# 139
typedef long long __off64_t;
# 140
typedef int __pid_t;
# 146
typedef long __time_t;
# 148
typedef long __suseconds_t;
# 161
typedef long __blksize_t;
# 166
typedef long __blkcnt_t;
# 180
typedef int __ssize_t;
# 189
typedef unsigned int __socklen_t;
# 36 "/usr/include/sys/types.h"
typedef __u_short u_short;
# 67
typedef __gid_t gid_t;
# 110
typedef __ssize_t ssize_t;
# 76 "/users/rupak/ccured/include/gcc_3.2/time.h"
typedef __time_t time_t;
# 28 "/usr/include/bits/sigset.h"
struct __anonstruct___sigset_t_2 {
   unsigned long __val[(int )(1024U / (8U * sizeof(unsigned long )))] ;
};
# 28
typedef struct __anonstruct___sigset_t_2 __sigset_t;
# 38 "/usr/include/sys/select.h"
typedef __sigset_t sigset_t;
# 118 "/users/rupak/ccured/include/gcc_3.2/time.h"
struct timespec {
   __time_t tv_sec ;
   long tv_nsec ;
};
# 69 "/usr/include/bits/time.h"
struct timeval {
   __time_t tv_sec ;
   __suseconds_t tv_usec ;
};
# 55 "/usr/include/sys/select.h"
typedef long __fd_mask;
# 67
struct __anonstruct_fd_set_3 {
   __fd_mask __fds_bits[(int )(1024U / (8U * sizeof(__fd_mask )))] ;
};
# 67
typedef struct __anonstruct_fd_set_3 fd_set;
# 83 "/usr/include/bits/sched.h"
struct __sched_param {
   int __sched_priority ;
};
# 26 "/usr/include/bits/pthreadtypes.h"
struct _pthread_fastlock {
   long __status ;
   int __spinlock ;
};
# 35
typedef struct _pthread_descr_struct *_pthread_descr;
# 41
struct __pthread_attr_s {
   int __detachstate ;
   int __schedpolicy ;
   struct __sched_param __schedparam ;
   int __inheritsched ;
   int __scope ;
   size_t __guardsize ;
   int __stackaddr_set ;
   void *__stackaddr ;
   size_t __stacksize ;
};
# 41
typedef struct __pthread_attr_s pthread_attr_t;
# 86
struct __anonstruct_pthread_mutex_t_6 {
   int __m_reserved ;
   int __m_count ;
   _pthread_descr __m_owner ;
   int __m_kind ;
   struct _pthread_fastlock __m_lock ;
};
# 86
typedef struct __anonstruct_pthread_mutex_t_6 pthread_mutex_t;
# 97
struct __anonstruct_pthread_mutexattr_t_7 {
   int __mutexkind ;
};
# 97
typedef struct __anonstruct_pthread_mutexattr_t_7 pthread_mutexattr_t;
# 150
typedef unsigned long pthread_t;
# 46 "/users/rupak/ccured/include/gcc_3.2/stdio.h"
typedef struct _IO_FILE FILE;
# 243 "/users/rupak/ccured/include/ccured_GNUCC.patch"
typedef struct __ccured_va_list *__gnuc_va_list;
# 172 "/usr/include/libio.h"
typedef void _IO_lock_t;
# 178
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
# 263
struct _IO_FILE {
   int __BLAST_FLAG ;
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   int _mode ;
   char _unused2[(int )(15U * sizeof(int ) - 2U * sizeof(void *))] ;
};
# 263 "/users/rupak/ccured/include/ccured_GNUCC.patch"
struct __ccured_va_list {
   int next ;
};
# 263
typedef struct __ccured_va_list *__ccured_va_list;
# 131 "/users/rupak/ccured/include/gcc_3.2/time.h"
struct tm {
   int tm_sec ;
   int tm_min ;
   int tm_hour ;
   int tm_mday ;
   int tm_mon ;
   int tm_year ;
   int tm_wday ;
   int tm_yday ;
   int tm_isdst ;
   long tm_gmtoff ;
   char const   *tm_zone ;
};
# 36 "/usr/include/bits/stat.h"
struct stat {
   __dev_t st_dev ;
   unsigned short __pad1 ;
   __ino_t st_ino ;
   __mode_t st_mode ;
   __nlink_t st_nlink ;
   __uid_t st_uid ;
   __gid_t st_gid ;
   __dev_t st_rdev ;
   unsigned short __pad2 ;
   __off_t st_size ;
   __blksize_t st_blksize ;
   __blkcnt_t st_blocks ;
   struct timespec st_atim ;
   struct timespec st_mtim ;
   struct timespec st_ctim ;
   unsigned long __unused4 ;
   unsigned long __unused5 ;
};
# 130 "common.h"
typedef unsigned char u8;
# 134
typedef unsigned short u16;
# 140
typedef unsigned int u32;
# 73 "/users/rupak/ccured/include/gcc_3.2/signal.h"
typedef void (*__sighandler_t)(int  );
# 25 "/users/rupak/ccured/include/gcc_3.2/bits/sigaction.h"
struct sigaction {
   void (*sa_handler)(int  ) ;
   __sigset_t sa_mask ;
   int sa_flags ;
   void (*sa_restorer)(void) ;
};
# 247 "/users/rupak/ccured/include/gcc_3.2/unistd.h"
typedef __socklen_t socklen_t;
# 50 "/users/rupak/ccured/include/gcc_3.2/pwd.h"
struct passwd {
   char *pw_name ;
   char *pw_passwd ;
   __uid_t pw_uid ;
   __gid_t pw_gid ;
   char *pw_gecos ;
   char *pw_dir ;
   char *pw_shell ;
};
# 43 "/users/rupak/ccured/include/gcc_3.2/grp.h"
struct group {
   char *gr_name ;
   char *gr_passwd ;
   __gid_t gr_gid ;
   char **gr_mem ;
};
# 50 "/usr/include/stdint.h"
typedef unsigned short uint16_t;
# 52
typedef unsigned int uint32_t;
# 89 "/usr/include/netinet/in.h"
typedef uint16_t in_port_t;
# 133
typedef uint32_t in_addr_t;
# 134
struct in_addr {
   in_addr_t s_addr ;
};
# 29 "/usr/include/bits/sockaddr.h"
typedef unsigned short sa_family_t;
# 145 "/usr/include/bits/socket.h"
struct sockaddr {
   sa_family_t sa_family ;
   char sa_data[14] ;
};
# 309
struct linger {
   int l_onoff ;
   int l_linger ;
};
# 216 "/usr/include/netinet/in.h"
struct sockaddr_in {
   sa_family_t sin_family ;
   in_port_t sin_port ;
   struct in_addr sin_addr ;
   unsigned char sin_zero[(int )(((sizeof(struct sockaddr ) -
                                   sizeof(unsigned short )) - sizeof(in_port_t )) -
                                 sizeof(struct in_addr ))] ;
};
# 28 "/usr/include/bits/ioctl-types.h"
struct winsize {
   unsigned short ws_row ;
   unsigned short ws_col ;
   unsigned short ws_xpixel ;
   unsigned short ws_ypixel ;
};
# 96 "/users/rupak/ccured/include/gcc_3.2/netdb.h"
struct hostent {
   char *h_name ;
   char **h_aliases ;
   int h_addrtype ;
   int h_length ;
   char **h_addr_list ;
};
# 207
struct servent {
   char *s_name ;
   char **s_aliases ;
   int s_port ;
   char *s_proto ;
};
# 393
struct addrinfo {
   int ai_flags ;
   int ai_family ;
   int ai_socktype ;
   int ai_protocol ;
   socklen_t ai_addrlen ;
   struct sockaddr *ai_addr ;
   char *ai_canonname ;
   struct addrinfo *ai_next ;
};
# 37 "/users/rupak/ccured/include/functions/getaddrinfo.h"
struct libc_addrinfo {
   int ai_flags ;
   int ai_family ;
   int ai_socktype ;
   int ai_protocol ;
   socklen_t ai_addrlen ;
   struct sockaddr *ai_addr ;
   char *ai_canonname ;
   struct libc_addrinfo *ai_next ;
};
# 66 "/usr/local/ssl/include/openssl/stack.h"
struct stack_st {
   int num ;
   char **data ;
   int sorted ;
   int num_alloc ;
   int (*comp)(char const   * const  * , char const   * const  * ) ;
};
# 66
typedef struct stack_st STACK;
# 192 "/usr/local/ssl/include/openssl/crypto.h"
struct crypto_ex_data_st {
   STACK *sk ;
   int dummy ;
};
# 192
typedef struct crypto_ex_data_st CRYPTO_EX_DATA;
# 217 "/usr/local/ssl/include/openssl/bio.h"
typedef struct bio_st BIO;
# 219
typedef void bio_info_cb(struct bio_st * , int  , char const   * , int  ,
                         long  , long  );
# 222
struct bio_method_st {
   int type ;
   char const   *name ;
   int (*bwrite)(BIO * , char const   * , int  ) ;
   int (*bread)(BIO * , char * , int  ) ;
   int (*bputs)(BIO * , char const   * ) ;
   int (*bgets)(BIO * , char * , int  ) ;
   long (*ctrl)(BIO * , int  , long  , void * ) ;
   int (*create)(BIO * ) ;
   int (*destroy)(BIO * ) ;
   long (*callback_ctrl)(BIO * , int  , bio_info_cb * ) ;
};
# 222
typedef struct bio_method_st BIO_METHOD;
# 251
struct bio_st {
   BIO_METHOD *method ;
   long (*callback)(struct bio_st * , int  , char const   * , int  , long  ,
                    long  ) ;
   char *cb_arg ;
   int init ;
   int shutdown ;
   int flags ;
   int retry_reason ;
   int num ;
   void *ptr ;
   struct bio_st *next_bio ;
   struct bio_st *prev_bio ;
   int references ;
   unsigned long num_read ;
   unsigned long num_write ;
   CRYPTO_EX_DATA ex_data ;
};
# 78 "/usr/local/ssl/include/openssl/lhash.h"
struct lhash_node_st {
   void const   *data ;
   struct lhash_node_st *next ;
   unsigned long hash ;
};
# 78
typedef struct lhash_node_st LHASH_NODE;
# 137
struct lhash_st {
   LHASH_NODE **b ;
   int (*comp)(void const   * , void const   * ) ;
   unsigned long (*hash)(void const   * ) ;
   unsigned int num_nodes ;
   unsigned int num_alloc_nodes ;
   unsigned int p ;
   unsigned int pmax ;
   unsigned long up_load ;
   unsigned long down_load ;
   unsigned long num_items ;
   unsigned long num_expands ;
   unsigned long num_expand_reallocs ;
   unsigned long num_contracts ;
   unsigned long num_contract_reallocs ;
   unsigned long num_hash_calls ;
   unsigned long num_comp_calls ;
   unsigned long num_insert ;
   unsigned long num_replace ;
   unsigned long num_delete ;
   unsigned long num_no_delete ;
   unsigned long num_retrieve ;
   unsigned long num_retrieve_miss ;
   unsigned long num_hash_comps ;
   int error ;
};
# 11 "/usr/local/ssl/include/openssl/comp.h"
struct comp_method_st {
   int type ;
   char const   *name ;
   int (*init)() ;
   void (*finish)() ;
   int (*compress)() ;
   int (*expand)() ;
   long (*ctrl)() ;
   long (*callback_ctrl)() ;
};
# 11
typedef struct comp_method_st COMP_METHOD;
# 23
struct comp_ctx_st {
   COMP_METHOD *meth ;
   unsigned long compress_in ;
   unsigned long compress_out ;
   unsigned long expand_in ;
   unsigned long expand_out ;
   CRYPTO_EX_DATA ex_data ;
};
# 23
typedef struct comp_ctx_st COMP_CTX;
# 69 "/usr/local/ssl/include/openssl/buffer.h"
struct buf_mem_st {
   int length ;
   char *data ;
   int max ;
};
# 69
typedef struct buf_mem_st BUF_MEM;
# 79 "/usr/local/ssl/include/openssl/ossl_typ.h"
typedef struct asn1_string_st ASN1_INTEGER;
# 80
typedef struct asn1_string_st ASN1_ENUMERATED;
# 81
typedef struct asn1_string_st ASN1_BIT_STRING;
# 82
typedef struct asn1_string_st ASN1_OCTET_STRING;
# 83
typedef struct asn1_string_st ASN1_PRINTABLESTRING;
# 84
typedef struct asn1_string_st ASN1_T61STRING;
# 85
typedef struct asn1_string_st ASN1_IA5STRING;
# 86
typedef struct asn1_string_st ASN1_GENERALSTRING;
# 87
typedef struct asn1_string_st ASN1_UNIVERSALSTRING;
# 88
typedef struct asn1_string_st ASN1_BMPSTRING;
# 89
typedef struct asn1_string_st ASN1_UTCTIME;
# 90
typedef struct asn1_string_st ASN1_TIME;
# 91
typedef struct asn1_string_st ASN1_GENERALIZEDTIME;
# 92
typedef struct asn1_string_st ASN1_VISIBLESTRING;
# 93
typedef struct asn1_string_st ASN1_UTF8STRING;
# 94
typedef int ASN1_BOOLEAN;
# 103
typedef struct evp_cipher_st EVP_CIPHER;
# 104
typedef struct evp_cipher_ctx_st EVP_CIPHER_CTX;
# 105
typedef struct env_md_st EVP_MD;
# 106
typedef struct env_md_ctx_st EVP_MD_CTX;
# 107
typedef struct evp_pkey_st EVP_PKEY;
# 109
typedef struct x509_st X509;
# 110
typedef struct X509_algor_st X509_ALGOR;
# 111
typedef struct X509_crl_st X509_CRL;
# 112
typedef struct X509_name_st X509_NAME;
# 113
typedef struct x509_store_st X509_STORE;
# 114
typedef struct x509_store_ctx_st X509_STORE_CTX;
# 116
typedef struct engine_st ENGINE;
# 232 "/usr/local/ssl/include/openssl/bn.h"
struct bignum_st {
   unsigned long *d ;
   int top ;
   int dmax ;
   int neg ;
   int flags ;
};
# 232
typedef struct bignum_st BIGNUM;
# 243
typedef struct bignum_ctx BN_CTX;
# 245
struct bn_blinding_st {
   int init ;
   BIGNUM *A ;
   BIGNUM *Ai ;
   BIGNUM *mod ;
   unsigned long thread_id ;
};
# 245
typedef struct bn_blinding_st BN_BLINDING;
# 256
struct bn_mont_ctx_st {
   int ri ;
   BIGNUM RR ;
   BIGNUM N ;
   BIGNUM Ni ;
   unsigned long n0 ;
   int flags ;
};
# 256
typedef struct bn_mont_ctx_st BN_MONT_CTX;
# 184 "/usr/local/ssl/include/openssl/asn1.h"
struct asn1_object_st {
   char const   *sn ;
   char const   *ln ;
   int nid ;
   int length ;
   unsigned char *data ;
   int flags ;
};
# 184
typedef struct asn1_object_st ASN1_OBJECT;
# 195
struct asn1_string_st {
   int length ;
   int type ;
   unsigned char *data ;
   long flags ;
};
# 195
typedef struct asn1_string_st ASN1_STRING;
# 444
union __anonunion_value_55 {
   char *ptr ;
   ASN1_BOOLEAN boolean ;
   ASN1_STRING *asn1_string ;
   ASN1_OBJECT *object ;
   ASN1_INTEGER *integer ;
   ASN1_ENUMERATED *enumerated ;
   ASN1_BIT_STRING *bit_string ;
   ASN1_OCTET_STRING *octet_string ;
   ASN1_PRINTABLESTRING *printablestring ;
   ASN1_T61STRING *t61string ;
   ASN1_IA5STRING *ia5string ;
   ASN1_GENERALSTRING *generalstring ;
   ASN1_BMPSTRING *bmpstring ;
   ASN1_UNIVERSALSTRING *universalstring ;
   ASN1_UTCTIME *utctime ;
   ASN1_GENERALIZEDTIME *generalizedtime ;
   ASN1_VISIBLESTRING *visiblestring ;
   ASN1_UTF8STRING *utf8string ;
   ASN1_STRING *set ;
   ASN1_STRING *sequence ;
};
# 444
struct asn1_type_st {
   int type ;
   union __anonunion_value_55 value ;
};
# 444
typedef struct asn1_type_st ASN1_TYPE;
# 79 "/usr/local/ssl/include/openssl/rsa.h"
typedef struct rsa_st RSA;
# 81
struct rsa_meth_st {
   char const   *name ;
   int (*rsa_pub_enc)(int flen , unsigned char const   *from ,
                      unsigned char *to , RSA *rsa , int padding ) ;
   int (*rsa_pub_dec)(int flen , unsigned char const   *from ,
                      unsigned char *to , RSA *rsa , int padding ) ;
   int (*rsa_priv_enc)(int flen , unsigned char const   *from ,
                       unsigned char *to , RSA *rsa , int padding ) ;
   int (*rsa_priv_dec)(int flen , unsigned char const   *from ,
                       unsigned char *to , RSA *rsa , int padding ) ;
   int (*rsa_mod_exp)(BIGNUM *r0 , BIGNUM const   *I , RSA *rsa ) ;
   int (*bn_mod_exp)(BIGNUM *r , BIGNUM const   *a , BIGNUM const   *p ,
                     BIGNUM const   *m , BN_CTX *ctx , BN_MONT_CTX *m_ctx ) ;
   int (*init)(RSA *rsa ) ;
   int (*finish)(RSA *rsa ) ;
   int flags ;
   char *app_data ;
   int (*rsa_sign)(int type , unsigned char const   *m , unsigned int m_length ,
                   unsigned char *sigret , unsigned int *siglen ,
                   RSA const   *rsa ) ;
   int (*rsa_verify)(int dtype , unsigned char const   *m ,
                     unsigned int m_length , unsigned char *sigbuf ,
                     unsigned int siglen , RSA const   *rsa ) ;
};
# 81
typedef struct rsa_meth_st RSA_METHOD;
# 120
struct rsa_st {
   int pad ;
   long version ;
   RSA_METHOD const   *meth ;
   ENGINE *engine ;
   BIGNUM *n ;
   BIGNUM *e ;
   BIGNUM *d ;
   BIGNUM *p ;
   BIGNUM *q ;
   BIGNUM *dmp1 ;
   BIGNUM *dmq1 ;
   BIGNUM *iqmp ;
   CRYPTO_EX_DATA ex_data ;
   int references ;
   int flags ;
   BN_MONT_CTX *_method_mod_n ;
   BN_MONT_CTX *_method_mod_p ;
   BN_MONT_CTX *_method_mod_q ;
   char *bignum_data ;
   BN_BLINDING *blinding ;
};
# 79 "/usr/local/ssl/include/openssl/dh.h"
typedef struct dh_st DH;
# 81
struct dh_method {
   char const   *name ;
   int (*generate_key)(DH *dh ) ;
   int (*compute_key)(unsigned char *key , BIGNUM const   *pub_key , DH *dh ) ;
   int (*bn_mod_exp)(DH const   *dh , BIGNUM *r , BIGNUM const   *a ,
                     BIGNUM const   *p , BIGNUM const   *m , BN_CTX *ctx ,
                     BN_MONT_CTX *m_ctx ) ;
   int (*init)(DH *dh ) ;
   int (*finish)(DH *dh ) ;
   int flags ;
   char *app_data ;
};
# 81
typedef struct dh_method DH_METHOD;
# 96
struct dh_st {
   int pad ;
   int version ;
   BIGNUM *p ;
   BIGNUM *g ;
   long length ;
   BIGNUM *pub_key ;
   BIGNUM *priv_key ;
   int flags ;
   char *method_mont_p ;
   BIGNUM *q ;
   BIGNUM *j ;
   unsigned char *seed ;
   int seedlen ;
   BIGNUM *counter ;
   int references ;
   CRYPTO_EX_DATA ex_data ;
   DH_METHOD const   *meth ;
   ENGINE *engine ;
};
# 88 "/usr/local/ssl/include/openssl/dsa.h"
typedef struct dsa_st DSA;
# 90
struct DSA_SIG_st {
   BIGNUM *r ;
   BIGNUM *s ;
};
# 90
typedef struct DSA_SIG_st DSA_SIG;
# 96
struct dsa_method {
   char const   *name ;
   DSA_SIG *(*dsa_do_sign)(unsigned char const   *dgst , int dlen , DSA *dsa ) ;
   int (*dsa_sign_setup)(DSA *dsa , BN_CTX *ctx_in , BIGNUM **kinvp ,
                         BIGNUM **rp ) ;
   int (*dsa_do_verify)(unsigned char const   *dgst , int dgst_len ,
                        DSA_SIG *sig , DSA *dsa ) ;
   int (*dsa_mod_exp)(DSA *dsa , BIGNUM *rr , BIGNUM *a1 , BIGNUM *p1 ,
                      BIGNUM *a2 , BIGNUM *p2 , BIGNUM *m , BN_CTX *ctx ,
                      BN_MONT_CTX *in_mont ) ;
   int (*bn_mod_exp)(DSA *dsa , BIGNUM *r , BIGNUM *a , BIGNUM const   *p ,
                     BIGNUM const   *m , BN_CTX *ctx , BN_MONT_CTX *m_ctx ) ;
   int (*init)(DSA *dsa ) ;
   int (*finish)(DSA *dsa ) ;
   int flags ;
   char *app_data ;
};
# 96
typedef struct dsa_method DSA_METHOD;
# 115
struct dsa_st {
   int pad ;
   long version ;
   int write_params ;
   BIGNUM *p ;
   BIGNUM *q ;
   BIGNUM *g ;
   BIGNUM *pub_key ;
   BIGNUM *priv_key ;
   BIGNUM *kinv ;
   BIGNUM *r ;
   int flags ;
   char *method_mont_p ;
   int references ;
   CRYPTO_EX_DATA ex_data ;
   DSA_METHOD const   *meth ;
   ENGINE *engine ;
};
# 177 "/usr/local/ssl/include/openssl/evp.h"
union __anonunion_pkey_56 {
   char *ptr ;
   struct rsa_st *rsa ;
   struct dsa_st *dsa ;
   struct dh_st *dh ;
};
# 177
struct evp_pkey_st {
   int type ;
   int save_type ;
   int references ;
   union __anonunion_pkey_56 pkey ;
   int save_parameters ;
   STACK *attributes ;
};
# 271
struct env_md_st {
   int type ;
   int pkey_type ;
   int md_size ;
   unsigned long flags ;
   int (*init)(EVP_MD_CTX *ctx ) ;
   int (*update)(EVP_MD_CTX *ctx , void const   *data , unsigned long count ) ;
   int (*final)(EVP_MD_CTX *ctx , unsigned char *md ) ;
   int (*copy)(EVP_MD_CTX *to , EVP_MD_CTX const   *from ) ;
   int (*cleanup)(EVP_MD_CTX *ctx ) ;
   int (*sign)() ;
   int (*verify)() ;
   int required_pkey_type[5] ;
   int block_size ;
   int ctx_size ;
};
# 318
struct env_md_ctx_st {
   EVP_MD const   *digest ;
   ENGINE *engine ;
   unsigned long flags ;
   void *md_data ;
};
# 335
struct evp_cipher_st {
   int nid ;
   int block_size ;
   int key_len ;
   int iv_len ;
   unsigned long flags ;
   int (*init)(EVP_CIPHER_CTX *ctx , unsigned char const   *key ,
               unsigned char const   *iv , int enc ) ;
   int (*do_cipher)(EVP_CIPHER_CTX *ctx , unsigned char *out ,
                    unsigned char const   *in , unsigned int inl ) ;
   int (*cleanup)(EVP_CIPHER_CTX * ) ;
   int ctx_size ;
   int (*set_asn1_parameters)(EVP_CIPHER_CTX * , ASN1_TYPE * ) ;
   int (*get_asn1_parameters)(EVP_CIPHER_CTX * , ASN1_TYPE * ) ;
   int (*ctrl)(EVP_CIPHER_CTX * , int type , int arg , void *ptr ) ;
   void *app_data ;
};
# 392
struct evp_cipher_ctx_st {
   EVP_CIPHER const   *cipher ;
   ENGINE *engine ;
   int encrypt ;
   int buf_len ;
   unsigned char oiv[16] ;
   unsigned char iv[16] ;
   unsigned char buf[32] ;
   int num ;
   void *app_data ;
   int key_len ;
   unsigned long flags ;
   void *cipher_data ;
   int final_used ;
   int block_mask ;
   unsigned char final[32] ;
};
# 124 "/usr/local/ssl/include/openssl/x509.h"
struct X509_algor_st {
   ASN1_OBJECT *algorithm ;
   ASN1_TYPE *parameter ;
};
# 133
struct X509_val_st {
   ASN1_TIME *notBefore ;
   ASN1_TIME *notAfter ;
};
# 133
typedef struct X509_val_st X509_VAL;
# 139
struct X509_pubkey_st {
   X509_ALGOR *algor ;
   ASN1_BIT_STRING *public_key ;
   EVP_PKEY *pkey ;
};
# 139
typedef struct X509_pubkey_st X509_PUBKEY;
# 164
struct X509_name_st {
   STACK *entries ;
   int modified ;
   BUF_MEM *bytes ;
   unsigned long hash ;
};
# 224
struct x509_cinf_st {
   ASN1_INTEGER *version ;
   ASN1_INTEGER *serialNumber ;
   X509_ALGOR *signature ;
   X509_NAME *issuer ;
   X509_VAL *validity ;
   X509_NAME *subject ;
   X509_PUBKEY *key ;
   ASN1_BIT_STRING *issuerUID ;
   ASN1_BIT_STRING *subjectUID ;
   STACK *extensions ;
};
# 224
typedef struct x509_cinf_st X509_CINF;
# 244
struct x509_cert_aux_st {
   STACK *trust ;
   STACK *reject ;
   ASN1_UTF8STRING *alias ;
   ASN1_OCTET_STRING *keyid ;
   STACK *other ;
};
# 244
typedef struct x509_cert_aux_st X509_CERT_AUX;
# 253
struct x509_st {
   X509_CINF *cert_info ;
   X509_ALGOR *sig_alg ;
   ASN1_BIT_STRING *signature ;
   int valid ;
   int references ;
   char *name ;
   CRYPTO_EX_DATA ex_data ;
   long ex_pathlen ;
   unsigned long ex_flags ;
   unsigned long ex_kusage ;
   unsigned long ex_xkusage ;
   unsigned long ex_nscert ;
   ASN1_OCTET_STRING *skid ;
   struct AUTHORITY_KEYID_st *akid ;
   unsigned char sha1_hash[20] ;
   X509_CERT_AUX *aux ;
};
# 393
struct X509_revoked_st {
   ASN1_INTEGER *serialNumber ;
   ASN1_TIME *revocationDate ;
   STACK *extensions ;
   int sequence ;
};
# 393
typedef struct X509_revoked_st X509_REVOKED;
# 404
struct X509_crl_info_st {
   ASN1_INTEGER *version ;
   X509_ALGOR *sig_alg ;
   X509_NAME *issuer ;
   ASN1_TIME *lastUpdate ;
   ASN1_TIME *nextUpdate ;
   STACK *revoked ;
   STACK *extensions ;
};
# 404
typedef struct X509_crl_info_st X509_CRL_INFO;
# 415
struct X509_crl_st {
   X509_CRL_INFO *crl ;
   X509_ALGOR *sig_alg ;
   ASN1_BIT_STRING *signature ;
   int references ;
};
# 120 "/usr/local/ssl/include/openssl/x509_vfy.h"
union __anonunion_data_58 {
   char *ptr ;
   X509 *x509 ;
   X509_CRL *crl ;
   EVP_PKEY *pkey ;
};
# 120
struct x509_object_st {
   int type ;
   union __anonunion_data_58 data ;
};
# 120
typedef struct x509_object_st X509_OBJECT;
# 132
typedef struct x509_lookup_st X509_LOOKUP;
# 138
struct x509_lookup_method_st {
   char const   *name ;
   int (*new_item)(X509_LOOKUP *ctx ) ;
   void (*free)(X509_LOOKUP *ctx ) ;
   int (*init)(X509_LOOKUP *ctx ) ;
   int (*shutdown)(X509_LOOKUP *ctx ) ;
   int (*ctrl)(X509_LOOKUP *ctx , int cmd , char const   *argc , long argl ,
               char **ret ) ;
   int (*get_by_subject)(X509_LOOKUP *ctx , int type , X509_NAME *name ,
                         X509_OBJECT *ret ) ;
   int (*get_by_issuer_serial)(X509_LOOKUP *ctx , int type , X509_NAME *name ,
                               ASN1_INTEGER *serial , X509_OBJECT *ret ) ;
   int (*get_by_fingerprint)(X509_LOOKUP *ctx , int type ,
                             unsigned char *bytes , int len , X509_OBJECT *ret ) ;
   int (*get_by_alias)(X509_LOOKUP *ctx , int type , char *str , int len ,
                       X509_OBJECT *ret ) ;
};
# 138
typedef struct x509_lookup_method_st X509_LOOKUP_METHOD;
# 161
struct x509_store_st {
   int cache ;
   STACK *objs ;
   STACK *get_cert_methods ;
   unsigned long flags ;
   int purpose ;
   int trust ;
   int (*verify)(X509_STORE_CTX *ctx ) ;
   int (*verify_cb)(int ok , X509_STORE_CTX *ctx ) ;
   int (*get_issuer)(X509 **issuer , X509_STORE_CTX *ctx , X509 *x ) ;
   int (*check_issued)(X509_STORE_CTX *ctx , X509 *x , X509 *issuer ) ;
   int (*check_revocation)(X509_STORE_CTX *ctx ) ;
   int (*get_crl)(X509_STORE_CTX *ctx , X509_CRL **crl , X509 *x ) ;
   int (*check_crl)(X509_STORE_CTX *ctx , X509_CRL *crl ) ;
   int (*cert_crl)(X509_STORE_CTX *ctx , X509_CRL *crl , X509 *x ) ;
   int (*cleanup)(X509_STORE_CTX *ctx ) ;
   CRYPTO_EX_DATA ex_data ;
   int references ;
   int depth ;
};
# 199
struct x509_lookup_st {
   int init ;
   int skip ;
   X509_LOOKUP_METHOD *method ;
   char *method_data ;
   X509_STORE *store_ctx ;
};
# 212
struct x509_store_ctx_st {
   X509_STORE *ctx ;
   int current_method ;
   X509 *cert ;
   STACK *untrusted ;
   int purpose ;
   int trust ;
   time_t check_time ;
   unsigned long flags ;
   void *other_ctx ;
   int (*verify)(X509_STORE_CTX *ctx ) ;
   int (*verify_cb)(int ok , X509_STORE_CTX *ctx ) ;
   int (*get_issuer)(X509 **issuer , X509_STORE_CTX *ctx , X509 *x ) ;
   int (*check_issued)(X509_STORE_CTX *ctx , X509 *x , X509 *issuer ) ;
   int (*check_revocation)(X509_STORE_CTX *ctx ) ;
   int (*get_crl)(X509_STORE_CTX *ctx , X509_CRL **crl , X509 *x ) ;
   int (*check_crl)(X509_STORE_CTX *ctx , X509_CRL *crl ) ;
   int (*cert_crl)(X509_STORE_CTX *ctx , X509_CRL *crl , X509 *x ) ;
   int (*cleanup)(X509_STORE_CTX *ctx ) ;
   int depth ;
   int valid ;
   int last_untrusted ;
   STACK *chain ;
   int error_depth ;
   int error ;
   X509 *current_cert ;
   X509 *current_issuer ;
   X509_CRL *current_crl ;
   CRYPTO_EX_DATA ex_data ;
};
# 480 "/usr/local/ssl/include/openssl/pem.h"
typedef int pem_password_cb(char *buf , int size , int rwflag , void *userdata );
# 337 "/usr/local/ssl/include/openssl/ssl.h"
struct ssl_cipher_st {
   int valid ;
   char const   *name ;
   unsigned long id ;
   unsigned long algorithms ;
   unsigned long algo_strength ;
   unsigned long algorithm2 ;
   int strength_bits ;
   int alg_bits ;
   unsigned long mask ;
   unsigned long mask_strength ;
};
# 337
typedef struct ssl_cipher_st SSL_CIPHER;
# 353
typedef struct ssl_st SSL;
# 354
typedef struct ssl_ctx_st SSL_CTX;
# 357
struct ssl_method_st {
   int version ;
   int (*ssl_new)(SSL *s ) ;
   void (*ssl_clear)(SSL *s ) ;
   void (*ssl_free)(SSL *s ) ;
   int (*ssl_accept)(SSL *s ) ;
   int (*ssl_connect)(SSL *s ) ;
   int (*ssl_read)(SSL *s , void *buf , int len ) ;
   int (*ssl_peek)(SSL *s , void *buf , int len ) ;
   int (*ssl_write)(SSL *s , void const   *buf , int len ) ;
   int (*ssl_shutdown)(SSL *s ) ;
   int (*ssl_renegotiate)(SSL *s ) ;
   int (*ssl_renegotiate_check)(SSL *s ) ;
   long (*ssl_ctrl)(SSL *s , int cmd , long larg , void *parg ) ;
   long (*ssl_ctx_ctrl)(SSL_CTX *ctx , int cmd , long larg , void *parg ) ;
   SSL_CIPHER *(*get_cipher_by_char)(unsigned char const   *ptr ) ;
   int (*put_cipher_by_char)(SSL_CIPHER const   *cipher , unsigned char *ptr ) ;
   int (*ssl_pending)(SSL *s ) ;
   int (*num_ciphers)(void) ;
   SSL_CIPHER *(*get_cipher)(unsigned int ncipher ) ;
   struct ssl_method_st *(*get_ssl_method)(int version ) ;
   long (*get_timeout)(void) ;
   struct ssl3_enc_method *ssl3_enc ;
   int (*ssl_version)() ;
   long (*ssl_callback_ctrl)(SSL *s , int cb_id , void (*fp)() ) ;
   long (*ssl_ctx_callback_ctrl)(SSL_CTX *s , int cb_id , void (*fp)() ) ;
};
# 357
typedef struct ssl_method_st SSL_METHOD;
# 405
struct ssl_session_st {
   int ssl_version ;
   unsigned int key_arg_length ;
   unsigned char key_arg[8] ;
   int master_key_length ;
   unsigned char master_key[48] ;
   unsigned int session_id_length ;
   unsigned char session_id[32] ;
   unsigned int sid_ctx_length ;
   unsigned char sid_ctx[32] ;
   int not_resumable ;
   struct sess_cert_st *sess_cert ;
   X509 *peer ;
   long verify_result ;
   int references ;
   long timeout ;
   long time ;
   int compress_meth ;
   SSL_CIPHER *cipher ;
   unsigned long cipher_id ;
   STACK *ciphers ;
   CRYPTO_EX_DATA ex_data ;
   struct ssl_session_st *prev ;
   struct ssl_session_st *next ;
};
# 405
typedef struct ssl_session_st SSL_SESSION;
# 581
struct ssl_comp_st {
   int id ;
   char *name ;
   COMP_METHOD *method ;
};
# 581
typedef struct ssl_comp_st SSL_COMP;
# 594
struct __anonstruct_stats_62 {
   int sess_connect ;
   int sess_connect_renegotiate ;
   int sess_connect_good ;
   int sess_accept ;
   int sess_accept_renegotiate ;
   int sess_accept_good ;
   int sess_miss ;
   int sess_timeout ;
   int sess_cache_full ;
   int sess_hit ;
   int sess_cb_hit ;
};
# 594
struct ssl_ctx_st {
   SSL_METHOD *method ;
   STACK *cipher_list ;
   STACK *cipher_list_by_id ;
   struct x509_store_st *cert_store ;
   struct lhash_st *sessions ;
   unsigned long session_cache_size ;
   struct ssl_session_st *session_cache_head ;
   struct ssl_session_st *session_cache_tail ;
   int session_cache_mode ;
   long session_timeout ;
   int (*new_session_cb)(struct ssl_st *ssl , SSL_SESSION *sess ) ;
   void (*remove_session_cb)(struct ssl_ctx_st *ctx , SSL_SESSION *sess ) ;
   SSL_SESSION *(*get_session_cb)(struct ssl_st *ssl , unsigned char *data ,
                                  int len , int *copy ) ;
   struct __anonstruct_stats_62 stats ;
   int references ;
   int (*app_verify_callback)(X509_STORE_CTX * , void * ) ;
   void *app_verify_arg ;
   pem_password_cb *default_passwd_callback ;
   void *default_passwd_callback_userdata ;
   int (*client_cert_cb)(SSL *ssl , X509 **x509 , EVP_PKEY **pkey ) ;
   CRYPTO_EX_DATA ex_data ;
   EVP_MD const   *rsa_md5 ;
   EVP_MD const   *md5 ;
   EVP_MD const   *sha1 ;
   STACK *extra_certs ;
   STACK *comp_methods ;
   void (*info_callback)(SSL const   *ssl , int type , int val ) ;
   STACK *client_CA ;
   unsigned long options ;
   unsigned long mode ;
   long max_cert_list ;
   struct cert_st *cert ;
   int read_ahead ;
   void (*msg_callback)(int write_p , int version , int content_type ,
                        void const   *buf , size_t len , SSL *ssl , void *arg ) ;
   void *msg_callback_arg ;
   int verify_mode ;
   int verify_depth ;
   unsigned int sid_ctx_length ;
   unsigned char sid_ctx[32] ;
   int (*default_verify_callback)(int ok , X509_STORE_CTX *ctx ) ;
   int (*generate_session_id)(SSL const   *ssl , unsigned char *id ,
                              unsigned int *id_len ) ;
   int purpose ;
   int trust ;
   int quiet_shutdown ;
};
# 777
struct ssl_st {
   int version ;
   int type ;
   SSL_METHOD *method ;
   BIO *rbio ;
   BIO *wbio ;
   BIO *bbio ;
   int rwstate ;
   int in_handshake ;
   int (*handshake_func)() ;
   int server ;
   int new_session ;
   int quiet_shutdown ;
   int shutdown ;
   int state ;
   int rstate ;
   BUF_MEM *init_buf ;
   void *init_msg ;
   int init_num ;
   int init_off ;
   unsigned char *packet ;
   unsigned int packet_length ;
   struct ssl2_state_st *s2 ;
   struct ssl3_state_st *s3 ;
   int read_ahead ;
   void (*msg_callback)(int write_p , int version , int content_type ,
                        void const   *buf , size_t len , SSL *ssl , void *arg ) ;
   void *msg_callback_arg ;
   int hit ;
   int purpose ;
   int trust ;
   STACK *cipher_list ;
   STACK *cipher_list_by_id ;
   EVP_CIPHER_CTX *enc_read_ctx ;
   EVP_MD const   *read_hash ;
   COMP_CTX *expand ;
   EVP_CIPHER_CTX *enc_write_ctx ;
   EVP_MD const   *write_hash ;
   COMP_CTX *compress ;
   struct cert_st *cert ;
   unsigned int sid_ctx_length ;
   unsigned char sid_ctx[32] ;
   SSL_SESSION *session ;
   int (*generate_session_id)(SSL const   *ssl , unsigned char *id ,
                              unsigned int *id_len ) ;
   int verify_mode ;
   int verify_depth ;
   int (*verify_callback)(int ok , X509_STORE_CTX *ctx ) ;
   void (*info_callback)(SSL const   *ssl , int type , int val ) ;
   int error ;
   int error_code ;
   SSL_CTX *ctx ;
   int debug ;
   long verify_result ;
   CRYPTO_EX_DATA ex_data ;
   STACK *client_CA ;
   int references ;
   unsigned long options ;
   unsigned long mode ;
   long max_cert_list ;
   int first_packet ;
   int client_version ;
};
# 158 "/usr/local/ssl/include/openssl/ssl2.h"
struct __anonstruct_tmp_63 {
   unsigned int conn_id_length ;
   unsigned int cert_type ;
   unsigned int cert_length ;
   unsigned int csl ;
   unsigned int clear ;
   unsigned int enc ;
   unsigned char ccl[32] ;
   unsigned int cipher_spec_length ;
   unsigned int session_id_length ;
   unsigned int clen ;
   unsigned int rlen ;
};
# 158
struct ssl2_state_st {
   int three_byte_header ;
   int clear_text ;
   int escape ;
   int ssl2_rollback ;
   unsigned int wnum ;
   int wpend_tot ;
   unsigned char const   *wpend_buf ;
   int wpend_off ;
   int wpend_len ;
   int wpend_ret ;
   int rbuf_left ;
   int rbuf_offs ;
   unsigned char *rbuf ;
   unsigned char *wbuf ;
   unsigned char *write_ptr ;
   unsigned int padding ;
   unsigned int rlength ;
   int ract_data_length ;
   unsigned int wlength ;
   int wact_data_length ;
   unsigned char *ract_data ;
   unsigned char *wact_data ;
   unsigned char *mac_data ;
   unsigned char *read_key ;
   unsigned char *write_key ;
   unsigned int challenge_length ;
   unsigned char challenge[32] ;
   unsigned int conn_id_length ;
   unsigned char conn_id[16] ;
   unsigned int key_material_length ;
   unsigned char key_material[48] ;
   unsigned long read_sequence ;
   unsigned long write_sequence ;
   struct __anonstruct_tmp_63 tmp ;
};
# 284 "/usr/local/ssl/include/openssl/ssl3.h"
struct ssl3_record_st {
   int type ;
   unsigned int length ;
   unsigned int off ;
   unsigned char *data ;
   unsigned char *input ;
   unsigned char *comp ;
};
# 284
typedef struct ssl3_record_st SSL3_RECORD;
# 294
struct ssl3_buffer_st {
   unsigned char *buf ;
   size_t len ;
   int offset ;
   int left ;
};
# 294
typedef struct ssl3_buffer_st SSL3_BUFFER;
# 317
struct __anonstruct_tmp_64 {
   unsigned char cert_verify_md[72] ;
   unsigned char finish_md[72] ;
   int finish_md_len ;
   unsigned char peer_finish_md[72] ;
   int peer_finish_md_len ;
   unsigned long message_size ;
   int message_type ;
   SSL_CIPHER *new_cipher ;
   DH *dh ;
   int next_state ;
   int reuse_message ;
   int cert_req ;
   int ctype_num ;
   char ctype[7] ;
   STACK *ca_names ;
   int use_rsa_tmp ;
   int key_block_length ;
   unsigned char *key_block ;
   EVP_CIPHER const   *new_sym_enc ;
   EVP_MD const   *new_hash ;
   SSL_COMP const   *new_compression ;
   int cert_request ;
};
# 317
struct ssl3_state_st {
   long flags ;
   int delay_buf_pop_ret ;
   unsigned char read_sequence[8] ;
   unsigned char read_mac_secret[36] ;
   unsigned char write_sequence[8] ;
   unsigned char write_mac_secret[36] ;
   unsigned char server_random[32] ;
   unsigned char client_random[32] ;
   int need_empty_fragments ;
   int empty_fragment_done ;
   SSL3_BUFFER rbuf ;
   SSL3_BUFFER wbuf ;
   SSL3_RECORD rrec ;
   SSL3_RECORD wrec ;
   unsigned char alert_fragment[2] ;
   unsigned int alert_fragment_len ;
   unsigned char handshake_fragment[4] ;
   unsigned int handshake_fragment_len ;
   unsigned int wnum ;
   int wpend_tot ;
   int wpend_type ;
   int wpend_ret ;
   unsigned char const   *wpend_buf ;
   EVP_MD_CTX finish_dgst1 ;
   EVP_MD_CTX finish_dgst2 ;
   int change_cipher_spec ;
   int warn_alert ;
   int fatal_alert ;
   int alert_dispatch ;
   unsigned char send_alert[2] ;
   int renegotiate ;
   int total_renegotiations ;
   int num_renegotiations ;
   int in_read_app_data ;
   struct __anonstruct_tmp_64 tmp ;
};
# 74 "prototypes.h"
enum __anonenum_section_code_65 {
    CRIT_KEYGEN = 0, 
    CRIT_NTOA = 1, 
    CRIT_CLIENTS = 2, 
    CRIT_WIN_LOG = 3, 
    CRIT_SECTIONS = 4,  };
# 74
typedef enum __anonenum_section_code_65 section_code;
# 99
struct __anonstruct_option_67 {
   unsigned int cert : 1 ;
   unsigned int client : 1 ;
   unsigned int foreground : 1 ;
   unsigned int syslog : 1 ;
   unsigned int rand_write : 1 ;
};
# 99
struct __anonstruct_GLOBAL_OPTIONS_66 {
   char *ca_dir ;
   char *ca_file ;
   char *crl_dir ;
   char *crl_file ;
   char *cipher_list ;
   char *cert ;
   char *egd_sock ;
   char *key ;
   char *rand_file ;
   int random_bytes ;
   long session_timeout ;
   int verify_level ;
   int verify_use_only_my ;
   long ssl_options ;
   char *chroot_dir ;
   unsigned long dpid ;
   char *pidfile ;
   char *setuid_user ;
   char *setgid_group ;
   int debug_level ;
   int facility ;
   char *output_file ;
   struct __anonstruct_option_67 option ;
};
# 99
typedef struct __anonstruct_GLOBAL_OPTIONS_66 GLOBAL_OPTIONS;
# 155
struct __anonstruct_option_68 {
   unsigned int delayed_lookup : 1 ;
   unsigned int accept : 1 ;
   unsigned int remote : 1 ;
   unsigned int program : 1 ;
   unsigned int pty : 1 ;
   unsigned int transparent : 1 ;
};
# 155
struct local_options {
   struct local_options *next ;
   char local_address[16] ;
   char *servname ;
   int fd ;
   unsigned short localport ;
   unsigned short remoteport ;
   char *execname ;
   char **execargs ;
   u32 *localnames ;
   u32 *remotenames ;
   u32 *local_ip ;
   char *username ;
   char *remote_address ;
   int timeout_busy ;
   int timeout_idle ;
   int timeout_close ;
   char *protocol ;
   struct __anonstruct_option_68 option ;
};
# 155
typedef struct local_options LOCAL_OPTIONS;
# 193
enum __anonenum_VAL_TYPE_69 {
    TYPE_NONE = 0, 
    TYPE_FLAG = 1, 
    TYPE_INT = 2, 
    TYPE_LINGER = 3, 
    TYPE_TIMEVAL = 4, 
    TYPE_STRING = 5,  };
# 193
typedef enum __anonenum_VAL_TYPE_69 VAL_TYPE;
# 197
union __anonunion_OPT_UNION_70 {
   int i_val ;
   long l_val ;
   char c_val[16] ;
   struct linger linger_val ;
   struct timeval timeval_val ;
};
# 197
typedef union __anonunion_OPT_UNION_70 OPT_UNION;
# 205
struct __anonstruct_SOCK_OPT_71 {
   char *opt_str ;
   int opt_level ;
   int opt_name ;
   VAL_TYPE opt_type ;
   OPT_UNION *opt_val[3] ;
};
# 205
typedef struct __anonstruct_SOCK_OPT_71 SOCK_OPT;
# 218
struct __anonstruct_FD_72 {
   int fd ;
   int rd ;
   int wr ;
   int is_socket ;
};
# 218
typedef struct __anonstruct_FD_72 FD;
# 225
struct __anonstruct_CLI_73 {
   LOCAL_OPTIONS *opt ;
   char accepting_address[16] ;
   char connecting_address[16] ;
   struct sockaddr_in addr ;
   FD local_rfd ;
   FD local_wfd ;
   FD remote_fd ;
   SSL *ssl ;
   int bind_ip ;
   unsigned long pid ;
   u32 *resolved_addresses ;
   char sock_buff[16384] ;
   char ssl_buff[16384] ;
   int sock_ptr ;
   int ssl_ptr ;
   FD *sock_rfd ;
   FD *sock_wfd ;
   FD *ssl_rfd ;
   FD *ssl_wfd ;
   int sock_bytes ;
   int ssl_bytes ;
};
# 225
typedef struct __anonstruct_CLI_73 CLI;
# 11 "/usr/include/tcpd.h"
struct host_info {
   char name[128] ;
   char addr[128] ;
   struct sockaddr_in *sin ;
   struct t_unitdata *unit ;
   struct request_info *request ;
};
# 25
struct request_info {
   int fd ;
   char user[128] ;
   char daemon[128] ;
   char pid[10] ;
   struct host_info client[1] ;
   struct host_info server[1] ;
   void (*sink)() ;
   void (*hostname)() ;
   void (*hostaddr)() ;
   void (*cleanup)() ;
   struct netconfig *config ;
};
# 49 "options.c"
enum __anonenum_CMD_74 {
    CMD_INIT = 0, 
    CMD_EXEC = 1, 
    CMD_DEFAULT = 2, 
    CMD_HELP = 3,  };
# 49
typedef enum __anonenum_CMD_74 CMD;
# 1199
struct __anonstruct_facilitylevel_75 {
   char *name ;
   int value ;
};
# 1199
typedef struct __anonstruct_facilitylevel_75 facilitylevel;
# 1282
struct __anonstruct_ssl_opts_76 {
   char *name ;
   long value ;
};
# 171 "sselect.c"
union __anonunion___u_74 {
   int __in ;
   int __i ;
};
# 174
union __anonunion___u_75 {
   int __in ;
   int __i ;
};
# 169
union __anonunion___u_76 {
   int __in ;
   int __i ;
};
# 324 "ssl.c"
struct keytabstruct {
   RSA *key ;
   time_t timeout ;
};
# 24 "/usr/include/bits/termios.h"
typedef unsigned char cc_t;
# 25
typedef unsigned int speed_t;
# 26
typedef unsigned int tcflag_t;
# 29
struct termios {
   tcflag_t c_iflag ;
   tcflag_t c_oflag ;
   tcflag_t c_cflag ;
   tcflag_t c_lflag ;
   cc_t c_line ;
   cc_t c_cc[32] ;
   speed_t c_ispeed ;
   speed_t c_ospeed ;
};
struct ssl3_enc_method;
struct cert_st;
struct _pthread_descr_struct;
struct sess_cert_st;
struct bignum_ctx;
struct netconfig;
struct t_unitdata;
struct engine_st;
struct AUTHORITY_KEYID_st;
# 4 "stunnel_comb.c"
# 19 "/users/rupak/ccured/include/ccured.h"
extern void *wrapperAlloc(unsigned int  ) ;
# 52
extern void * __attribute__((__safe__)) __ptrof_nocheck(void *ptr ) ;
# 64
extern void * __attribute__((__safe__)) __endof(void *ptr ) ;
# 79
extern void * __attribute__((__safe__)) __ptrof(void *ptr ) ;
# 93
extern void __verify_nul(char const   *ptr ) ;
# 126
extern char *__stringof(char const   *ptr ) ;
# 182
extern void *__mkptr(void * __attribute__((__safe__)) p , void *phome ) ;
# 198
extern void *__mkptr_size(void * __attribute__((__safe__)) p , unsigned int len ) ;
# 208
extern char *__mkptr_string(char * __attribute__((__safe__)) p ) ;
# 229
extern void *__trusted_cast(void *p ) ;
# 239
extern void * __attribute__((__safe__)) __trusted_deepcast(void * __attribute__((__safe__)) p ) ;
# 311
extern int __ccured_has_empty_mangling(unsigned int  ) ;
# 48 "/users/rupak/ccured/include/time_wrappers.h"
extern time_t time(time_t *__timer ) ;
# 106 "/usr/include/sys/select.h"
extern int select(int __nfds , fd_set * __restrict  __readfds ,
                  fd_set * __restrict  __writefds ,
                  fd_set * __restrict  __exceptfds ,
                  struct timeval * __restrict  __timeout ) ;
# 171 "/users/rupak/ccured/include/ccured_GNUCC.patch"
FILE *get_stderr(void) ;
# 174
extern void *malloc(size_t __size )  __attribute__((__malloc__)) ;
# 172
FILE *get_stderr(void) 
{ FILE *f ;

  {
  {
# 174
  f = (FILE *)malloc(sizeof(FILE ));
# 175
  f->__BLAST_FLAG = 1;
  }
# 176
  return (f);
}
}
# 202 "/usr/include/stdio.h"
int fclose(FILE *__stream ) ;
# 203
int fclose(FILE *__stream ) 
{ 

  {
# 204
  }
# 204
  return (0);
}

# 204
extern int fflush(FILE *__stream ) ;
# 221
FILE *fopen(char const   * __restrict  __filename ,
            char const   * __restrict  __modes ) ;
# 222
FILE *fopen(char const   * __restrict  __filename ,
            char const   * __restrict  __modes ) 
{ FILE *f ;
  FILE *tmp ;

  {
  {
# 223
  tmp = (FILE *)malloc(sizeof(FILE ));
# 223
  f = tmp;
# 224
  f->__BLAST_FLAG = 1;
  }
# 225
  return (f);
}
}
# 252
FILE *fdopen(int __fd , char const   *__modes ) ;
# 253
FILE *fdopen(int __fd , char const   *__modes ) 
{ FILE *f ;
  FILE *tmp ;

  {
  {
# 254
  tmp = (FILE *)malloc(sizeof(FILE ));
# 254
  f = tmp;
# 255
  f->__BLAST_FLAG = 1;
  }
# 256
  return (f);
}
}
# 297
int fprintf(FILE * __restrict  __stream , char const   * __restrict  __format 
            , ...) ;
# 299
int fprintf(FILE * __restrict  __stream , char const   * __restrict  __format 
            , ...) 
{ 

  {
# 301
}
}
# 302
extern int sprintf(char * __restrict  __s , char const   * __restrict  __format 
                   , ...) ;
# 319
extern int snprintf(char * __restrict  __s , size_t __maxlen ,
                    char const   * __restrict  __format  , ...) ;
# 323
extern int vsnprintf(char * __restrict  __s , size_t __maxlen ,
                     char const   * __restrict  __format , __gnuc_va_list __arg ) ;
# 358
extern int sscanf(char const   * __restrict  __s ,
                  char const   * __restrict  __format  , ...) ;
# 442
extern char *fgets(char * __restrict  __s , int __n ,
                   FILE * __restrict  __stream ) ;
# 594
extern void perror(char const   *__s ) ;
# 264 "/users/rupak/ccured/include/ccured_GNUCC.patch"
extern void __ccured_va_start(__ccured_va_list  , unsigned long  ) ;
# 267
extern void __ccured_va_end(__ccured_va_list  ) ;
# 337 "/users/rupak/ccured/include/stdio_wrappers.h"
extern int ( /* missing proto */  GCC_STDARG_START)() ;
# 468
void perror_wrapper(char const   *s ) 
{ char const   *tmp ;

  {
  {
# 469
  tmp = (char const   *)__stringof(s);
# 469
  perror(tmp);
  }
# 470
  return;
}
}
# 38 "/usr/include/bits/errno.h"
extern int *__errno_location(void)  __attribute__((__const__)) ;
# 144 "/users/rupak/ccured/include/gcc_3.2/stdlib.h"
extern int atoi(char const   *__nptr )  __attribute__((__pure__)) ;
# 558 "/usr/include/stdlib.h"
extern void *calloc(size_t __nmemb , size_t __size )  __attribute__((__malloc__)) ;
# 539 "/users/rupak/ccured/include/ccured_GNUCC.patch"
extern void *realloc(void *__ptr , size_t __size ) ;
# 569 "/usr/include/stdlib.h"
extern void free(void *__ptr ) ;
# 598
extern int atexit(void (*__func)(void) ) ;
# 612
void exit(int __status )  __attribute__((__noreturn__)) ;
# 613
void exit(int __status )  __attribute__((__noreturn__)) ;
# 613
void exit(int __status ) 
{ 

  {
  EXIT: 
  goto EXIT;
}
}
# 637
extern int putenv(char *__string ) ;
# 10 "/users/rupak/ccured/include/malloc_wrappers.h"
void *realloc_wrapper(void *b , int sz ) 
{ void *res ;
  void *tmp ;
  void *tmp___0 ;
  void *result ;

  {
  {
# 12
  tmp = (void *)__ptrof(b);
# 12
  tmp___0 = realloc(tmp, (unsigned int )sz);
# 12
  res = tmp___0;
# 15
  result = b;
# 17
  result = __mkptr_size((void */* __attribute__((__safe__)) */)res,
                        (unsigned int )sz);
  }
# 18
  return (result);
}
}
# 23
extern void free_wrapper(void *x ) 
{ void *tmp ;

  {
  {
# 24
  tmp = (void *)__ptrof(x);
# 24
  free(tmp);
  }
# 25
  return;
}
}
# 152 "/users/rupak/ccured/include/stdlib_wrappers.h"
static void *__qsort_base  ;
# 156
static int (*__qsort_compare)(void * , void * )  ;
# 195
static void *__bsearch_base  ;
# 196
static void *__bsearch_key  ;
# 200
static int (*__bsearch_compare)(void * , void * )  ;
# 38 "/users/rupak/ccured/include/gcc_3.2/string.h"
extern void *memcpy(void * __restrict  __dest ,
                    void const   * __restrict  __src , size_t __n ) ;
# 42
extern void *memmove(void *__dest , void const   *__src , size_t __n ) ;
# 58
extern void *memset(void *__s , int __c , size_t __n ) ;
# 82
extern char *strcpy(char * __restrict  __dest ,
                    char const   * __restrict  __src ) ;
# 85
extern char *strncpy(char * __restrict  __dest ,
                     char const   * __restrict  __src , size_t __n ) ;
# 92
extern char *strncat(char * __restrict  __dest ,
                     char const   * __restrict  __src , size_t __n ) ;
# 96
extern int strcmp(char const   *__s1 , char const   *__s2 )  __attribute__((__pure__)) ;
# 162
extern char *strchr(char const   *__s , int __c )  __attribute__((__pure__)) ;
# 164
extern char *strrchr(char const   *__s , int __c )  __attribute__((__pure__)) ;
# 191 "/usr/include/string.h"
extern char *strtok(char * __restrict  __s , char const   * __restrict  __delim ) ;
# 230
extern size_t strlen(char const   *__s )  __attribute__((__pure__)) ;
# 243
extern char *strerror(int __errnum ) ;
# 287
extern int strcasecmp(char const   *__s1 , char const   *__s2 )  __attribute__((__pure__)) ;
# 291
extern int strncasecmp(char const   *__s1 , char const   *__s2 , size_t __n )  __attribute__((__pure__)) ;
# 138 "/users/rupak/ccured/include/string_wrappers.h"
char *strrchr_wrapper(char const   *s , int chr ) 
{ char *result ;
  char *tmp ;
  char *tmp___0 ;

  {
  {
# 141
  tmp = __stringof(s);
# 141
  result = strrchr((char const   *)tmp, chr);
# 142
  tmp___0 = (char *)__mkptr((void */* __attribute__((__safe__)) */)((void *)result),
                            (void *)s);
  }
# 142
  return (tmp___0);
}
}
# 168
int strcasecmp_wrapper(char const   *s1 , char const   *s2 ) 
{ char *tmp ;
  char *tmp___0 ;
  int tmp___1 ;

  {
  {
# 170
  tmp = __stringof(s2);
# 170
  tmp___0 = __stringof(s1);
# 170
  tmp___1 = strcasecmp((char const   *)tmp___0, (char const   *)tmp);
  }
# 170
  return (tmp___1);
}
}
# 181
int strncasecmp_wrapper(char const   *s1 , char const   *s2 , unsigned int n ) 
{ void * __attribute__((__safe__)) tmp ;
  void * __attribute__((__safe__)) tmp___0 ;
  void * __attribute__((__safe__)) tmp___1 ;
  void * __attribute__((__safe__)) tmp___2 ;
  char *tmp___3 ;
  char *tmp___4 ;
  int tmp___5 ;

  {
  {
# 183
  tmp = __endof((void *)s1);
# 183
  tmp___0 = __ptrof_nocheck((void *)s1);
  }
# 183
  if ((unsigned int )tmp - (unsigned int )tmp___0 < n) {
    {
# 185
    __verify_nul(s1);
    }
  }
  {
# 187
  tmp___1 = __endof((void *)s2);
# 187
  tmp___2 = __ptrof_nocheck((void *)s2);
  }
# 187
  if ((unsigned int )tmp___1 - (unsigned int )tmp___2 < n) {
    {
# 188
    __verify_nul(s2);
    }
  }
  {
# 190
  tmp___3 = (char *)__ptrof((void *)s2);
# 190
  tmp___4 = (char *)__ptrof((void *)s1);
# 190
  tmp___5 = strncasecmp((char const   *)tmp___4, (char const   *)tmp___3, n);
  }
# 190
  return (tmp___5);
}
}
# 254
static char const   *saved_str   = (char const   *)((void *)0);
# 72 "/users/rupak/ccured/include/ccured_GNUCC.patch"
extern int _get__ctype_b(int  ) ;
# 116 "/usr/include/ctype.h"
extern int tolower(int __c ) ;
# 244 "/users/rupak/ccured/include/gcc_3.2/time.h"
extern struct tm *localtime_r(time_t const   * __restrict  __timer ,
                              struct tm * __restrict  __tp ) ;
# 320 "/users/rupak/ccured/include/gcc_3.2/sys/stat.h"
extern int __xstat(int __ver , char const   *__filename ,
                   struct stat *__stat_buf ) ;
# 356
__inline static int stat__extinline(char const   *__path ,
                                    struct stat *__statbuf ) 
{ int tmp ;

  {
  {
# 359
  tmp = __xstat(3, __path, __statbuf);
  }
# 359
  return (tmp);
}
}
# 172 "/usr/include/sys/syslog.h"
extern void closelog(void) ;
# 175
extern void openlog(char const   *__ident , int __option , int __facility ) ;
# 182
extern void syslog(int __pri , char const   *__fmt  , ...) ;
# 90 "/users/rupak/ccured/include/gcc_3.2/signal.h"
extern __sighandler_t signal(int __sig , void (*__handler)(int  ) ) ;
# 209
extern int sigemptyset(sigset_t *__set ) ;
# 215
extern int sigaddset(sigset_t *__set , int __signo ) ;
# 241
extern int sigprocmask(int __how , sigset_t const   * __restrict  __set ,
                       sigset_t * __restrict  __oset ) ;
# 31 "/usr/include/bits/sigthread.h"
extern int pthread_sigmask(int __how ,
                           __sigset_t const   * __restrict  __newmask ,
                           __sigset_t * __restrict  __oldmask ) ;
# 57 "/users/rupak/ccured/include/signal_wrappers.h"
__inline static void *__mkfat_sighandler(void *in ) 
{ void *tmp ;
  void *tmp___0 ;

  {
# 58
  if ((int )in == 1) {
    {
# 59
    tmp = __mkptr((void */* __attribute__((__safe__)) */)in, (void *)0);
    }
# 59
    return (tmp);
  } else {
    {
# 61
    tmp___0 = __mkptr_size((void */* __attribute__((__safe__)) */)in, 1U);
    }
# 61
    return (tmp___0);
  }
}
}
# 80
extern void __deepcopy_sigaction_from_compat(struct sigaction *fat ,
                                             struct sigaction  __attribute__((__compat__)) *compat ) 
{ 

  {
  {
# 81
  fat->sa_handler = (void (*)(int  ))__mkfat_sighandler((void *)compat->sa_handler);
# 82
  fat->sa_restorer = (void (*)(void))__mkfat_sighandler((void *)compat->sa_restorer);
  }
# 84
  return;
}
}
# 129 "/usr/include/sys/wait.h"
extern __pid_t waitpid(__pid_t __pid , int *__stat_loc , int __options ) ;
# 306 "/users/rupak/ccured/include/gcc_3.2/unistd.h"
extern int close(int __fd ) ;
# 310
extern ssize_t read(int __fd , void *__buf , size_t __nbytes ) ;
# 313
extern ssize_t write(int __fd , void const   *__buf , size_t __n ) ;
# 353
extern int pipe(int *__pipedes ) ;
# 371
extern unsigned int sleep(unsigned int __seconds ) ;
# 409
extern int chdir(char const   *__path ) ;
# 444
extern int dup2(int __fd , int __fd2 ) ;
# 479
extern int execvp(char const   *__file , char * const  *__argv ) ;
# 494
extern void _exit(int __status )  __attribute__((__noreturn__)) ;
# 509
extern long sysconf(int __name )  __attribute__((__const__)) ;
# 518
extern __pid_t getpid(void) ;
# 580
extern __pid_t setsid(void) ;
# 613
extern int setuid(__uid_t __uid ) ;
# 630
extern int setgid(__gid_t __gid ) ;
# 665
extern __pid_t fork(void) ;
# 711
extern int unlink(char const   *__name ) ;
# 806
extern int daemon(int __nochdir , int __noclose ) ;
# 813
extern int chroot(char const   *__path ) ;
# 15 "/users/rupak/ccured/include/functions/deepcopy_stringarray.h"
__inline static char **__deepcopy_stringarray_from_compat(char * __attribute__((__safe__)) * __attribute__((__safe__)) array_in ) 
{ int num_strings ;
  int i ;
  char **new_array ;
  char * __attribute__((__safe__)) * __attribute__((__safe__)) p ;
  void * __attribute__((__safe__)) tmp ;
  char **tmp___0 ;
  int tmp___2 ;
  void *tmp___3 ;
  void *tmp___5 ;

  {
  {
# 17
  num_strings = 0;
  }
# 24
  if ((unsigned int )array_in ==
      (unsigned int )((char * __attribute__((__safe__)) *)0)) {
# 25
    return ((char **)0);
  }
  {
# 28
  p = array_in;
  }
# 29
  while ((unsigned int )(*p) != (unsigned int )((char *)0)) {
    {
# 31
    tmp___3 = __trusted_cast((void *)((unsigned long )p +
                                      (unsigned long )(1U * sizeof((*p)))));
# 31
    p = (char * __attribute__((__safe__)) */* __attribute__((__safe__)) */)((char * __attribute__((__safe__)) *)tmp___3);
# 32
    num_strings ++;
    }
  }
  {
# 34
  num_strings ++;
# 36
  tmp___2 = __ccured_has_empty_mangling(sizeof((*new_array)));
  }
# 36
  if (tmp___2) {
    {
# 38
    tmp = __trusted_deepcast((void */* __attribute__((__safe__)) */)((void *)array_in));
# 38
    tmp___0 = (char **)__mkptr_size(tmp, (unsigned int )num_strings *
                                         sizeof((*(new_array + 0))));
    }
# 38
    return (tmp___0);
  } else {
    {
# 42
    new_array = (char **)wrapperAlloc((unsigned int )num_strings *
                                      sizeof((*(new_array + 0))));
# 43
    i = 0;
    }
# 43
    while (i < num_strings) {
      {
# 46
      tmp___5 = __trusted_cast((void *)((unsigned long )array_in +
                                        (unsigned long )((unsigned int )i *
                                                         sizeof((*array_in)))));
# 46
      p = (char * __attribute__((__safe__)) */* __attribute__((__safe__)) */)((char * __attribute__((__safe__)) *)tmp___5);
# 47
      (*(new_array + i)) = __mkptr_string((*p));
# 43
      i ++;
      }
    }
  }
# 50
  return (new_array);
}
}
# 92 "/users/rupak/ccured/include/gcc_3.2/pwd.h"
extern struct passwd *getpwnam(char const   *__name ) ;
# 38 "/users/rupak/ccured/include/pwd_wrappers.h"
extern void __deepcopy_passwd_from_compat(struct passwd *fat ,
                                          struct passwd  __attribute__((__compat__)) *compat ) 
{ 

  {
  {
# 40
  fat->pw_name = __mkptr_string((char */* __attribute__((__safe__)) */)compat->pw_name);
# 41
  fat->pw_passwd = __mkptr_string((char */* __attribute__((__safe__)) */)compat->pw_passwd);
# 42
  fat->pw_gecos = __mkptr_string((char */* __attribute__((__safe__)) */)compat->pw_gecos);
# 43
  fat->pw_dir = __mkptr_string((char */* __attribute__((__safe__)) */)compat->pw_dir);
# 44
  fat->pw_shell = __mkptr_string((char */* __attribute__((__safe__)) */)compat->pw_shell);
  }
# 45
  return;
}
}
# 48
static struct passwd my_passwd_result  ;
# 84 "/users/rupak/ccured/include/gcc_3.2/grp.h"
extern struct group *getgrnam(char const   *__name ) ;
# 137
extern int setgroups(size_t __n , __gid_t const   *__groups ) ;
# 13 "/users/rupak/ccured/include/grp_wrappers.h"
extern void __deepcopy_group_from_compat(struct group *fat ,
                                         struct group  __attribute__((__compat__)) *compat ) 
{ 

  {
  {
# 15
  fat->gr_name = __mkptr_string((char */* __attribute__((__safe__)) */)compat->gr_name);
# 16
  fat->gr_passwd = __mkptr_string((char */* __attribute__((__safe__)) */)compat->gr_passwd);
# 18
  fat->gr_mem = __deepcopy_stringarray_from_compat((char * __attribute__((__safe__)) */* __attribute__((__safe__)) */)((char * __attribute__((__safe__)) *)compat->gr_mem));
  }
# 19
  return;
}
}
# 21
static struct group my_group_result  ;
# 60 "/users/rupak/ccured/include/gcc_3.2/fcntl.h"
extern int fcntl(int __fd , int __cmd  , ...) ;
# 66
extern int open(char const   *file , int flag  , ...) ;
# 100 "/usr/include/sys/socket.h"
extern int socket(int __domain , int __type , int __protocol ) ;
# 110
extern int bind(int __fd , void const   *__addr , socklen_t __len ) ;
# 114
extern int getsockname(int __fd , void * __restrict  __addr ,
                       socklen_t * __restrict  __len ) ;
# 121
extern int connect(int __fd , void const   *__addr , socklen_t __len ) ;
# 126
extern int getpeername(int __fd , void * __restrict  __addr ,
                       socklen_t * __restrict  __len ) ;
# 168
extern int getsockopt(int __fd , int __level , int __optname ,
                      void * __restrict  __optval ,
                      socklen_t * __restrict  __optlen ) ;
# 175
extern int setsockopt(int __fd , int __level , int __optname ,
                      void const   *__optval , socklen_t __optlen ) ;
# 182
extern int listen(int __fd , int __n ) ;
# 189
extern int accept(int __fd , void * __restrict  __addr ,
                  socklen_t * __restrict  __addr_len ) ;
# 199
extern int shutdown(int __fd , int __how ) ;
# 35 "/users/rupak/ccured/include/gcc_3.2/arpa/inet.h"
extern in_addr_t inet_addr(char const   *__cp ) ;
# 54
extern char *inet_ntoa(struct in_addr __in ) ;
# 522 "/users/rupak/ccured/include/ccured_GNUCC.patch"
char *inet_ntoa_wrapper(struct in_addr __in ) 
{ char * __attribute__((__safe__)) tmp ;
  char *tmp___0 ;

  {
  {
# 524
  tmp = (char */* __attribute__((__safe__)) */)inet_ntoa(__in);
# 524
  tmp___0 = __mkptr_string(tmp);
  }
# 524
  return (tmp___0);
}
}
# 111 "/users/rupak/ccured/include/gcc_3.2/netdb.h"
extern void endhostent(void) ;
# 123
extern struct hostent *gethostbyname(char const   *__name ) ;
# 228
extern struct servent *getservbyname(char const   *__name ,
                                     char const   *__proto ) ;
# 468
extern char const   *gai_strerror(int __ecode ) ;
# 48 "/users/rupak/ccured/include/functions/getaddrinfo.h"
extern int true_getaddrinfo(char const   *node , char const   *service ,
                            struct libc_addrinfo  const  *hints ,
                            struct libc_addrinfo **res ) ;
# 52
extern void true_freeaddrinfo(struct libc_addrinfo *res ) ;
# 58
int getaddrinfo_wrapper(char const   *node , char const   *service ,
                        struct addrinfo  const  *hints , struct addrinfo **res ) 
{ struct libc_addrinfo libc_hints ;
  struct libc_addrinfo *libc_res ;
  struct libc_addrinfo *source ;
  struct addrinfo *dest ;
  int len ;
  int ret ;
  char const   *tmp ;
  char const   *tmp___0 ;
  struct addrinfo *newNode ;
  struct addrinfo *tmp___1 ;
  void * __restrict  tmp___3 ;
  size_t tmp___4 ;
  void * __restrict  tmp___6 ;

  {
  {
# 67
  dest = (struct addrinfo *)((void *)0);
# 75
  libc_hints.ai_flags = hints->ai_flags;
# 76
  libc_hints.ai_family = hints->ai_family;
# 77
  libc_hints.ai_socktype = hints->ai_socktype;
# 78
  libc_hints.ai_protocol = hints->ai_protocol;
# 79
  libc_hints.ai_addrlen = hints->ai_addrlen;
# 80
  libc_hints.ai_addr = (struct sockaddr *)__ptrof((void *)hints->ai_addr);
# 81
  libc_hints.ai_canonname = (char *)__ptrof((void *)hints->ai_canonname);
# 82
  libc_hints.ai_next = (struct libc_addrinfo *)((void *)0);
# 88
  tmp = (char const   *)__ptrof((void *)service);
# 88
  tmp___0 = (char const   *)__ptrof((void *)node);
# 88
  ret = true_getaddrinfo(tmp___0, tmp,
                         (struct libc_addrinfo  const  *)(& libc_hints),
                         & libc_res);
  }
# 90
  if (ret != 0) {
# 91
    return (ret);
  }
  {
# 97
  (*res) = (struct addrinfo *)((void *)0);
# 98
  source = libc_res;
  }
# 98
  while ((unsigned int )source != (unsigned int )((void *)0)) {
    {
# 100
    tmp___1 = (struct addrinfo *)wrapperAlloc(sizeof((*dest)));
# 100
    newNode = tmp___1;
    }
# 102
    if (dest) {
      {
# 108
      dest->ai_next = newNode;
      }
    } else {
      {
# 104
      (*res) = newNode;
      }
    }
    {
# 110
    dest = newNode;
# 113
    dest->ai_flags = source->ai_flags;
# 114
    dest->ai_family = source->ai_family;
# 115
    dest->ai_socktype = source->ai_socktype;
# 116
    dest->ai_protocol = source->ai_protocol;
# 117
    dest->ai_addrlen = source->ai_addrlen;
# 119
    len = (int )source->ai_addrlen;
# 120
    dest->ai_addr = (struct sockaddr *)wrapperAlloc((unsigned int )len);
# 122
    tmp___3 = (void */* __restrict  */)__ptrof((void *)dest->ai_addr);
# 122
    memcpy(tmp___3,
           (void const   */* __restrict  */)((void const   *)source->ai_addr),
           (unsigned int )len);
    }
# 124
    if (source->ai_canonname) {
      {
# 125
      tmp___4 = strlen((char const   *)source->ai_canonname);
# 125
      len = (int )(tmp___4 + 1U);
# 126
      dest->ai_canonname = (char *)wrapperAlloc((unsigned int )len);
# 127
      tmp___6 = (void */* __restrict  */)__ptrof((void *)dest->ai_canonname);
# 127
      memcpy(tmp___6,
             (void const   */* __restrict  */)((void const   *)source->ai_canonname),
             (unsigned int )len);
      }
    } else {
      {
# 130
      dest->ai_canonname = (char *)((void *)0);
      }
    }
    {
# 134
    dest->ai_next = (struct addrinfo *)((void *)0);
# 98
    source = source->ai_next;
    }
  }
  {
# 139
  true_freeaddrinfo(libc_res);
  }
# 141
  return (ret);
}
}
# 146
void freeaddrinfo_wrapper(struct addrinfo *res ) 
{ struct addrinfo *next ;

  {
# 150
  while (res) {
    {
# 151
    next = res->ai_next;
# 153
    free_wrapper((void *)res->ai_addr);
    }
# 154
    if (res->ai_canonname) {
      {
# 155
      free_wrapper((void *)res->ai_canonname);
      }
    }
    {
# 157
    free_wrapper((void *)res);
# 159
    res = next;
    }
  }
# 161
  return;
}
}
# 164
char const   *gai_strerror_wrapper(int code ) 
{ char * __attribute__((__safe__)) tmp ;
  char const   *tmp___0 ;

  {
  {
# 165
  tmp = (char */* __attribute__((__safe__)) */)gai_strerror(code);
# 165
  tmp___0 = (char const   *)__mkptr_string(tmp);
  }
# 165
  return (tmp___0);
}
}
# 79 "/usr/local/ssl/include/openssl/stack.h"
extern int sk_num(STACK const   * ) ;
# 80
extern char *sk_value(STACK const   * , int  ) ;
# 295 "/usr/local/ssl/include/openssl/crypto.h"
extern char const   *SSLeay_version(int type ) ;
# 330
extern void CRYPTO_set_locking_callback(void (*func)(int mode , int type ,
                                                     char const   *file ,
                                                     int line ) ) ;
# 338
extern void CRYPTO_set_id_callback(unsigned long (*func)(void) ) ;
# 385
extern void CRYPTO_free(void * ) ;
# 517 "/usr/local/ssl/include/openssl/bio.h"
extern BIO *BIO_new(BIO_METHOD *type ) ;
# 519
extern int BIO_free(BIO *a ) ;
# 521
extern int BIO_read(BIO *b , void *data , int len ) ;
# 526
extern long BIO_ctrl(BIO *bp , int cmd , long larg , void *parg ) ;
# 552
extern BIO_METHOD *BIO_s_mem(void) ;
# 614
extern int BIO_printf(BIO *bio , char const   *format  , ...) ;
# 753 "/usr/local/ssl/include/openssl/asn1.h"
extern int ASN1_INTEGER_cmp(ASN1_INTEGER *x , ASN1_INTEGER *y ) ;
# 821
extern long ASN1_INTEGER_get(ASN1_INTEGER *a ) ;
# 873
extern int ASN1_UTCTIME_print(BIO *fp , ASN1_UTCTIME *a ) ;
# 192 "/usr/local/ssl/include/openssl/rsa.h"
extern RSA *RSA_generate_key(int bits , unsigned long e ,
                             void (*callback)(int  , int  , void * ) ,
                             void *cb_arg ) ;
# 204
extern void RSA_free(RSA *r ) ;
# 774 "/usr/local/ssl/include/openssl/evp.h"
extern void EVP_PKEY_free(EVP_PKEY *pkey ) ;
# 333 "/usr/local/ssl/include/openssl/x509_vfy.h"
extern void X509_OBJECT_free_contents(X509_OBJECT *a ) ;
# 334
extern X509_STORE *X509_STORE_new(void) ;
# 346
extern int X509_STORE_CTX_init(X509_STORE_CTX *ctx , X509_STORE *store ,
                               X509 *x509 , STACK *chain ) ;
# 349
extern void X509_STORE_CTX_cleanup(X509_STORE_CTX *ctx ) ;
# 351
extern X509_LOOKUP *X509_STORE_add_lookup(X509_STORE *v , X509_LOOKUP_METHOD *m ) ;
# 353
extern X509_LOOKUP_METHOD *X509_LOOKUP_hash_dir(void) ;
# 354
extern X509_LOOKUP_METHOD *X509_LOOKUP_file(void) ;
# 359
extern int X509_STORE_get_by_subject(X509_STORE_CTX *vs , int type ,
                                     X509_NAME *name , X509_OBJECT *ret ) ;
# 362
extern int X509_LOOKUP_ctrl(X509_LOOKUP *ctx , int cmd , char const   *argc ,
                            long argl , char **ret ) ;
# 396
extern void X509_STORE_CTX_set_error(X509_STORE_CTX *ctx , int s ) ;
# 398
extern X509 *X509_STORE_CTX_get_current_cert(X509_STORE_CTX *ctx ) ;
# 703 "/usr/local/ssl/include/openssl/x509.h"
extern char const   *X509_verify_cert_error_string(long n ) ;
# 710
extern int X509_CRL_verify(X509_CRL *a , EVP_PKEY *r ) ;
# 817
extern int X509_cmp_current_time(ASN1_TIME *s ) ;
# 871
extern void X509_free(X509 *a ) ;
# 909
extern char *X509_NAME_oneline(X509_NAME *a , char *buf , int size ) ;
# 934
extern ASN1_INTEGER *X509_get_serialNumber(X509 *x ) ;
# 936
extern X509_NAME *X509_get_issuer_name(X509 *a ) ;
# 938
extern X509_NAME *X509_get_subject_name(X509 *a ) ;
# 942
extern EVP_PKEY *X509_get_pubkey(X509 *x ) ;
# 1170 "/usr/local/ssl/include/openssl/ssl.h"
extern int SSL_CTX_set_cipher_list(SSL_CTX * , char const   *str ) ;
# 1171
extern SSL_CTX *SSL_CTX_new(SSL_METHOD *meth ) ;
# 1172
extern void SSL_CTX_free(SSL_CTX * ) ;
# 1173
extern long SSL_CTX_set_timeout(SSL_CTX *ctx , long t ) ;
# 1177
extern int SSL_want(SSL *s ) ;
# 1182
extern SSL_CIPHER *SSL_get_current_cipher(SSL *s ) ;
# 1193
extern int SSL_pending(SSL *s ) ;
# 1195
extern int SSL_set_fd(SSL *s , int fd ) ;
# 1196
extern int SSL_set_rfd(SSL *s , int fd ) ;
# 1197
extern int SSL_set_wfd(SSL *s , int fd ) ;
# 1225
extern int SSL_CTX_use_RSAPrivateKey_file(SSL_CTX *ctx , char const   *file ,
                                          int type ) ;
# 1228
extern int SSL_CTX_use_certificate_chain_file(SSL_CTX *ctx , char const   *file ) ;
# 1241
extern void SSL_load_error_strings(void) ;
# 1244
extern char const   *SSL_state_string_long(SSL const   *s ) ;
# 1263
extern int SSL_set_session(SSL *to , SSL_SESSION *session ) ;
# 1273
extern X509 *SSL_get_peer_certificate(SSL *s ) ;
# 1281
extern void SSL_CTX_set_verify(SSL_CTX *ctx , int mode ,
                               int (*callback)(int  , X509_STORE_CTX * ) ) ;
# 1298
extern int SSL_CTX_check_private_key(SSL_CTX *ctx ) ;
# 1304
extern SSL *SSL_new(SSL_CTX *ctx ) ;
# 1305
extern int SSL_set_session_id_context(SSL *ssl ,
                                      unsigned char const   *sid_ctx ,
                                      unsigned int sid_ctx_len ) ;
# 1313
extern void SSL_free(SSL *ssl ) ;
# 1314
extern int SSL_accept(SSL *ssl ) ;
# 1315
extern int SSL_connect(SSL *ssl ) ;
# 1316
extern int SSL_read(SSL *ssl , void *buf , int num ) ;
# 1318
extern int SSL_write(SSL *ssl , void const   *buf , int num ) ;
# 1321
extern long SSL_CTX_ctrl(SSL_CTX *ctx , int cmd , long larg , void *parg ) ;
# 1324
extern int SSL_get_error(SSL *s , int ret_code ) ;
# 1336
extern SSL_METHOD *SSLv3_client_method(void) ;
# 1339
extern SSL_METHOD *SSLv23_server_method(void) ;
# 1351
extern int SSL_shutdown(SSL *s ) ;
# 1355
extern char const   *SSL_alert_type_string_long(int value ) ;
# 1357
extern char const   *SSL_alert_desc_string_long(int value ) ;
# 1367
extern void SSL_set_connect_state(SSL *s ) ;
# 1368
extern void SSL_set_accept_state(SSL *s ) ;
# 1372
extern int SSL_library_init(void) ;
# 1374
extern char *SSL_CIPHER_description(SSL_CIPHER * , char *buf , int size ) ;
# 1386
extern void SSL_set_shutdown(SSL *ssl , int mode ) ;
# 1390
extern int SSL_CTX_load_verify_locations(SSL_CTX *ctx , char const   *CAfile ,
                                         char const   *CApath ) ;
# 1447
extern void SSL_CTX_set_tmp_rsa_callback(SSL_CTX *ctx ,
                                         RSA *(*cb)(SSL *ssl , int is_export ,
                                                    int keylength ) ) ;
# 242 "/usr/local/ssl/include/openssl/err.h"
extern unsigned long ERR_get_error(void) ;
# 246
extern unsigned long ERR_peek_error(void) ;
# 255
extern char *ERR_error_string(unsigned long e , char *buf ) ;
# 275
extern void ERR_remove_state(unsigned long pid ) ;
# 99 "/usr/local/ssl/include/openssl/rand.h"
extern int RAND_load_file(char const   *file , long max_bytes ) ;
# 100
extern int RAND_write_file(char const   *file ) ;
# 101
extern char const   *RAND_file_name(char *file , size_t num ) ;
# 102
extern int RAND_status(void) ;
# 104
extern int RAND_egd(char const   *path ) ;
# 317 "/usr/local/ssl/include/openssl/engine.h"
extern void ENGINE_load_builtin_engines(void) ;
# 361
extern int ENGINE_register_all_complete(void) ;
# 28 "prototypes.h"
int num_clients ;
# 30
void main_initialize(char *arg1 , char *arg2 ) ;
# 31
void main_execute(void) ;
# 32
void ioerror(char *txt ) ;
# 33
void sockerror(char *txt ) ;
# 34
void log_error(int level , int error , char *txt ) ;
# 35
char *my_strerror(int errnum ) ;
# 36
int set_socket_options(int s , int type ) ;
# 37
char *stunnel_info(void) ;
# 38
int alloc_fd(int sock ) ;
# 39
char *safe_ntoa(char *text , struct in_addr in ) ;
# 43
void context_init(void) ;
# 44
void context_free(void) ;
# 45
void sslerror(char *txt ) ;
# 49
void log_open(void) ;
# 50
void log_close(void) ;
# 55
void log(int level , char const   *format  , ...) ;
# 63
void log_raw(char const   *format  , ...) ;
# 78
void enter_critical_section(section_code i ) ;
# 79
void leave_critical_section(section_code i ) ;
# 80
void sthreads_init(void) ;
# 81
unsigned long stunnel_process_id(void) ;
# 82
unsigned long stunnel_thread_id(void) ;
# 83
int create_client(int ls , int s , void *arg , void *(*cli)(void * ) ) ;
# 91
int pty_allocate(int *ptyfd , int *ttyfd , char *namebuf , int namebuflen ) ;
# 153
GLOBAL_OPTIONS options  ;
# 191
LOCAL_OPTIONS local_options  ;
# 213
void parse_config(char *name , char *parameter ) ;
# 214
int name2nums(char *name , char *default_host , u32 **names , u_short *port ) ;
# 244
int max_clients  ;
# 246
int max_fds  ;
# 254
void *alloc_client_session(LOCAL_OPTIONS *opt , int rfd , int wfd ) ;
# 255
void *client(void *arg ) ;
# 259
int negotiate(CLI *c ) ;
# 263
int sselect(int n , fd_set *readfds , fd_set *writefds , fd_set *exceptfds ,
            struct timeval *timeout ) ;
# 264
int waitforsocket(int fd , int dir , int timeout ) ;
# 266
void sselect_init(fd_set *set , int *n ) ;
# 267
void exec_status(void) ;
# 269
int write_blocking(CLI *c , int fd , u8 *ptr , int len ) ;
# 270
int read_blocking(CLI *c , int fd , u8 *ptr , int len ) ;
# 272
int fdprintf(CLI *c , int fd , char const   *format  , ...) ;
# 280
int fdscanf(CLI *c , int fd , char const   *format , char *buffer ) ;
# 73 "/usr/include/tcpd.h"
extern int hosts_access() ;
# 99
extern struct request_info *request_init(struct request_info *  , ...) ;
# 135
extern void sock_host() ;
# 50 "client.c"
int allow_severity   = 5;
# 51
int deny_severity   = 4;
# 55
static unsigned char *sid_ctx   = (unsigned char *)"stunnel SID";
# 59
SSL_CTX *ctx  ;
# 61
static int do_client(CLI *c ) ;
# 62
static int init_local(CLI *c ) ;
# 63
static int init_remote(CLI *c ) ;
# 64
static int init_ssl(CLI *c ) ;
# 65
static int transfer(CLI *c ) ;
# 66
static void cleanup(CLI *c , int error ) ;
# 68
static void print_cipher(CLI *c ) ;
# 69
static int auth_libwrap(CLI *c ) ;
# 70
static int auth_user(CLI *c ) ;
# 71
static int connect_local(CLI *c ) ;
# 73
static int make_sockets(int *fd ) ;
# 75
static int connect_remote(CLI *c ) ;
# 76
static void reset(int fd , char *txt ) ;
# 84
void *alloc_client_session(LOCAL_OPTIONS *opt , int rfd , int wfd ) 
{ CLI *c ;

  {
  {
# 87
  c = (CLI *)calloc(1U, sizeof(CLI ));
  }
# 88
  if (! c) {
    {
# 89
    log(3, (char const   *)"Memory allocation failed");
    }
# 90
    return ((void *)0);
  }
  {
# 92
  c->opt = opt;
# 93
  c->local_rfd.fd = rfd;
# 94
  c->local_wfd.fd = wfd;
  }
# 95
  return ((void *)c);
}
}
# 98
void *client(void *arg ) 
{ CLI *c ;
  int tmp ;

  {
  {
# 99
  c = (CLI *)arg;
# 104
  log(7, (char const   *)"%s started", (c->opt)->servname);
  }
# 106
  if ((c->opt)->option.remote) {
# 106
    if ((c->opt)->option.program) {
      {
# 107
      c->local_wfd.fd = connect_local(c);
# 107
      c->local_rfd.fd = c->local_wfd.fd;
      }
    }
  }
  {
# 111
  c->remote_fd.fd = -1;
# 112
  c->ssl = (SSL *)((void *)0);
# 113
  tmp = do_client(c);
# 113
  cleanup(c, tmp);
# 118
  enter_critical_section(2);
# 119
  num_clients --;
# 119
  log(7, (char const   *)"%s finished (%d left)", (c->opt)->servname,
      num_clients);
# 121
  leave_critical_section(2);
# 123
  free((void *)c);
  }
# 127
  return ((void *)0);
}
}
# 130
static int do_client(CLI *c ) 
{ int result ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  char const   *tmp___5 ;

  {
  {
# 133
  tmp = init_local(c);
  }
# 133
  if (tmp) {
# 134
    return (-1);
  }
# 135
  if (options.option.client) {
    _L: 
    {
# 142
    tmp___2 = init_remote(c);
    }
# 142
    if (tmp___2) {
# 143
      return (-1);
    }
    {
# 144
    tmp___3 = negotiate(c);
    }
# 144
    if (tmp___3 < 0) {
      {
# 145
      log(3, (char const   *)"Protocol negotiations failed");
      }
# 146
      return (-1);
    }
    {
# 148
    tmp___4 = init_ssl(c);
    }
# 148
    if (tmp___4) {
# 149
      return (-1);
    }
  } else {
# 135
    if ((c->opt)->protocol) {
      goto _L;
    } else {
      {
# 137
      tmp___0 = init_ssl(c);
      }
# 137
      if (tmp___0) {
# 138
        return (-1);
      }
      {
# 139
      tmp___1 = init_remote(c);
      }
# 139
      if (tmp___1) {
# 140
        return (-1);
      }
    }
  }
  {
# 151
  result = transfer(c);
  }
# 152
  if (result) {
    {
# 152
    tmp___5 = (char const   *)"reset";
    }
  } else {
    {
# 152
    tmp___5 = (char const   *)"closed";
    }
  }
  {
# 152
  log(5,
      (char const   *)"Connection %s: %d bytes sent to SSL, %d bytes sent to socket",
      tmp___5, c->ssl_bytes, c->sock_bytes);
  }
# 155
  return (result);
}
}
# 158
static int init_local(CLI *c ) 
{ int addrlen ;
  int *tmp ;
  int tmp___0 ;
  int tmp___1 ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp___2 ;
  int tmp___3 ;
  register unsigned short __v___0 ;
  register unsigned short __x___0 ;
  int tmp___4 ;
  int tmp___5 ;

  {
  {
# 161
  addrlen = (int )sizeof(c->addr);
# 163
  tmp___5 = getpeername(c->local_rfd.fd,
                        (void */* __restrict  */)((void *)((struct sockaddr *)(& c->addr))),
                        (socklen_t */* __restrict  */)((socklen_t *)(& addrlen)));
  }
# 163
  if (tmp___5 < 0) {
    {
# 164
    strcpy((char */* __restrict  */)(c->accepting_address),
           (char const   */* __restrict  */)((char const   *)"NOT A SOCKET"));
# 165
    c->local_rfd.is_socket = 0;
# 166
    c->local_wfd.is_socket = 0;
    }
# 170
    if ((c->opt)->option.transparent) {
      {
# 172
      sockerror("getpeerbyname");
      }
# 173
      return (-1);
    } else {
      {
# 170
      tmp = __errno_location();
      }
# 170
      if ((*tmp) != 88) {
        {
# 172
        sockerror("getpeerbyname");
        }
# 173
        return (-1);
      }
    }
  } else {
    {
# 177
    safe_ntoa(c->accepting_address, c->addr.sin_addr);
# 178
    c->local_rfd.is_socket = 1;
# 179
    c->local_wfd.is_socket = 1;
# 181
    tmp___0 = set_socket_options(c->local_rfd.fd, 1);
    }
# 181
    if (tmp___0 < 0) {
# 182
      return (-1);
    }
    {
# 183
    tmp___1 = auth_libwrap(c);
    }
# 183
    if (tmp___1 < 0) {
# 184
      return (-1);
    }
    {
# 185
    tmp___3 = auth_user(c);
    }
# 185
    if (tmp___3 < 0) {
      {
# 187
      __x = c->addr.sin_port;
# 187
      tmp___2 = ((int )__x);
      }
# 187
      if (tmp___2) {
        {
# 187
        __v = (unsigned short )((((int )__x >> 8) & 255) |
                                (((int )__x & 255) << 8));
        }
      } else {
        {
# 187
        __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
        }
      }
      {
# 187
      log(4, (char const   *)"Connection from %s:%d REFUSED by IDENT",
          c->accepting_address, __v);
      }
# 188
      return (-1);
    }
    {
# 191
    __x___0 = c->addr.sin_port;
# 191
    tmp___4 = ((int )__x___0);
    }
# 191
    if (tmp___4) {
      {
# 191
      __v___0 = (unsigned short )((((int )__x___0 >> 8) & 255) |
                                  (((int )__x___0 & 255) << 8));
      }
    } else {
      {
# 191
      __asm__  ("rorw $8, %w0": "=r" (__v___0): "0" (__x___0): "cc");
      }
    }
    {
# 191
    log(5, (char const   *)"%s connected from %s:%d", (c->opt)->servname,
        c->accepting_address, __v___0);
    }
  }
# 193
  return (0);
}
}
# 196
static int init_remote(CLI *c ) 
{ int fd ;
  int tmp ;

  {
# 200
  if ((c->opt)->local_ip) {
    {
# 201
    c->bind_ip = (int )(*((c->opt)->local_ip));
    }
  } else {
# 203
    if ((c->opt)->option.transparent) {
      {
# 204
      c->bind_ip = (int )c->addr.sin_addr.s_addr;
      }
    } else {
      {
# 207
      c->bind_ip = 0;
      }
    }
  }
# 209
  if ((c->opt)->option.remote) {
    {
# 210
    c->resolved_addresses = (u32 *)((void *)0);
# 211
    fd = connect_remote(c);
    }
# 212
    if (c->resolved_addresses) {
      {
# 213
      free((void *)c->resolved_addresses);
      }
    }
  } else {
    {
# 215
    fd = connect_local(c);
    }
  }
# 216
  if (fd < 0) {
    {
# 217
    log(3, (char const   *)"Failed to initialize remote connection");
    }
# 218
    return (-1);
  }
# 221
  if (fd >= max_fds) {
    {
# 222
    log(3, (char const   *)"Remote file descriptor out of range (%d>=%d)", fd,
        max_fds);
# 224
    close(fd);
    }
# 225
    return (-1);
  }
  {
# 228
  log(7, (char const   *)"Remote FD=%d initialized", fd);
# 229
  c->remote_fd.fd = fd;
# 230
  c->remote_fd.is_socket = 1;
# 231
  tmp = set_socket_options(fd, 2);
  }
# 231
  if (tmp < 0) {
# 232
    return (-1);
  }
# 233
  return (0);
}
}
# 236
static int init_ssl(CLI *c ) 
{ int i ;
  int err ;
  unsigned int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int *tmp___3 ;

  {
  {
# 239
  c->ssl = SSL_new(ctx);
  }
# 239
  if (! c->ssl) {
    {
# 240
    sslerror("SSL_new");
    }
# 241
    return (-1);
  }
  {
# 244
  tmp = strlen((char const   *)sid_ctx);
# 244
  SSL_set_session_id_context(c->ssl, (unsigned char const   *)sid_ctx, tmp);
  }
# 246
  if (options.option.client) {
# 248
    if (ctx->session_cache_head) {
      {
# 249
      tmp___0 = SSL_set_session(c->ssl, ctx->session_cache_head);
      }
# 249
      if (! tmp___0) {
        {
# 250
        log(4, (char const   *)"Cannot set SSL session id to most recent used");
        }
      }
    }
    {
# 251
    SSL_set_fd(c->ssl, c->remote_fd.fd);
# 252
    SSL_set_connect_state(c->ssl);
    }
  } else {
# 254
    if (c->local_rfd.fd == c->local_wfd.fd) {
      {
# 255
      SSL_set_fd(c->ssl, c->local_rfd.fd);
      }
    } else {
      {
# 258
      SSL_set_rfd(c->ssl, c->local_rfd.fd);
# 259
      SSL_set_wfd(c->ssl, c->local_wfd.fd);
      }
    }
    {
# 261
    SSL_set_accept_state(c->ssl);
    }
  }
# 265
  if (options.option.client) {
    {
# 266
    c->sock_rfd = & c->local_rfd;
# 267
    c->sock_wfd = & c->local_wfd;
# 268
    c->ssl_wfd = & c->remote_fd;
# 268
    c->ssl_rfd = c->ssl_wfd;
    }
  } else {
    {
# 270
    c->sock_wfd = & c->remote_fd;
# 270
    c->sock_rfd = c->sock_wfd;
# 271
    c->ssl_rfd = & c->local_rfd;
# 272
    c->ssl_wfd = & c->local_wfd;
    }
  }
# 275
  while (1) {
# 276
    if (options.option.client) {
      {
# 277
      i = SSL_connect(c->ssl);
      }
    } else {
      {
# 279
      i = SSL_accept(c->ssl);
      }
    }
    {
# 280
    err = SSL_get_error(c->ssl, i);
    }
# 281
    if (err == 0) {
# 282
      break;
    }
# 283
    if (err == 2) {
      {
# 284
      tmp___1 = waitforsocket((c->ssl_rfd)->fd, 0, (c->opt)->timeout_busy);
      }
# 284
      if (tmp___1 == 1) {
# 285
        continue;
      }
# 286
      return (-1);
    }
# 288
    if (err == 3) {
      {
# 289
      tmp___2 = waitforsocket((c->ssl_wfd)->fd, 1, (c->opt)->timeout_busy);
      }
# 289
      if (tmp___2 == 1) {
# 290
        continue;
      }
# 291
      return (-1);
    }
# 293
    if (err == 5) {
      {
# 294
      tmp___3 = __errno_location();
      }
# 294
      switch ((*tmp___3)) {
      case 4: 
      {

      }
      case 11: 
      {

      }
# 297
      continue;
      }
    }
# 300
    if (options.option.client) {
      {
# 301
      sslerror("SSL_connect");
      }
    } else {
      {
# 303
      sslerror("SSL_accept");
      }
    }
# 304
    return (-1);
  }
  {
# 306
  print_cipher(c);
  }
# 307
  return (0);
}
}
# 310
static int transfer(CLI *c ) 
{ fd_set rd_set ;
  fd_set wr_set ;
  int num ;
  int err ;
  int fdno ;
  int check_SSL_pending ;
  int ssl_closing ;
  int ready ;
  struct timeval tv ;
  unsigned int __i ;
  fd_set *__arr ;
  int tmp ;
  unsigned int __i___0 ;
  fd_set *__arr___0 ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int *tmp___4 ;
  int *tmp___5 ;
  int tmp___6 ;
  int *tmp___7 ;
  int *tmp___8 ;
  int tmp___9 ;
  int tmp___10 ;

  {
  {
# 322
  fdno = (c->sock_rfd)->fd;
  }
# 323
  if ((c->sock_wfd)->fd > fdno) {
    {
# 323
    fdno = (c->sock_wfd)->fd;
    }
  }
# 324
  if ((c->ssl_rfd)->fd > fdno) {
    {
# 324
    fdno = (c->ssl_rfd)->fd;
    }
  }
# 325
  if ((c->ssl_wfd)->fd > fdno) {
    {
# 325
    fdno = (c->ssl_wfd)->fd;
    }
  }
  {
# 326
  fdno ++;
# 328
  c->ssl_ptr = 0;
# 328
  c->sock_ptr = c->ssl_ptr;
# 329
  (c->ssl_wfd)->wr = 1;
# 329
  (c->ssl_rfd)->rd = (c->ssl_wfd)->wr;
# 329
  (c->sock_wfd)->wr = (c->ssl_rfd)->rd;
# 329
  (c->sock_rfd)->rd = (c->sock_wfd)->wr;
# 330
  c->ssl_bytes = 0;
# 330
  c->sock_bytes = c->ssl_bytes;
# 331
  ssl_closing = 0;
  }
# 333
  while (1) {
# 333
    if ((c->sock_rfd)->rd) {
      goto _L___12;
    } else {
# 333
      if (c->sock_ptr) {
        _L___12: 
# 333
        if (! (c->ssl_wfd)->wr) {
          goto _L___11;
        }
      } else {
        _L___11: 
# 333
        if ((c->ssl_rfd)->rd) {
          goto _L___10;
        } else {
# 333
          if (c->ssl_ptr) {
            _L___10: 
# 333
            if (! (c->sock_wfd)->wr) {
# 333
              break;
            }
          } else {
# 333
            break;
          }
        }
      }
    }
# 335
    while (1) {
      {
# 335
      __arr = & rd_set;
# 335
      __i = 0U;
      }
# 335
      while (__i < sizeof(fd_set ) / sizeof(__fd_mask )) {
        {
# 335
        __arr->__fds_bits[__i] = 0L;
# 335
        __i ++;
        }
      }
# 335
      break;
    }
# 336
    if ((c->sock_rfd)->rd) {
# 336
      if (c->sock_ptr < 16384) {
        {
# 337
        rd_set.__fds_bits[(unsigned int )(c->sock_rfd)->fd /
                          (8U * sizeof(__fd_mask ))] |= 1L <<
                                                        (unsigned int )(c->sock_rfd)->fd %
                                                        (8U * sizeof(__fd_mask ));
        }
      }
    }
# 338
    if ((c->ssl_rfd)->rd) {
# 338
      if (c->ssl_ptr < 16384) {
        {
# 344
        rd_set.__fds_bits[(unsigned int )(c->ssl_rfd)->fd /
                          (8U * sizeof(__fd_mask ))] |= 1L <<
                                                        (unsigned int )(c->ssl_rfd)->fd %
                                                        (8U * sizeof(__fd_mask ));
        }
      } else {
# 338
        if (c->sock_ptr) {
          goto _L;
        } else {
# 338
          if (ssl_closing) {
            _L: 
            {
# 338
            tmp = SSL_want(c->ssl);
            }
# 338
            if (tmp == 3) {
              {
# 344
              rd_set.__fds_bits[(unsigned int )(c->ssl_rfd)->fd /
                                (8U * sizeof(__fd_mask ))] |= 1L <<
                                                              (unsigned int )(c->ssl_rfd)->fd %
                                                              (8U *
                                                               sizeof(__fd_mask ));
              }
            }
          }
        }
      }
    }
# 347
    while (1) {
      {
# 347
      __arr___0 = & wr_set;
# 347
      __i___0 = 0U;
      }
# 347
      while (__i___0 < sizeof(fd_set ) / sizeof(__fd_mask )) {
        {
# 347
        __arr___0->__fds_bits[__i___0] = 0L;
# 347
        __i___0 ++;
        }
      }
# 347
      break;
    }
# 348
    if ((c->sock_wfd)->wr) {
# 348
      if (c->ssl_ptr) {
        {
# 349
        wr_set.__fds_bits[(unsigned int )(c->sock_wfd)->fd /
                          (8U * sizeof(__fd_mask ))] |= 1L <<
                                                        (unsigned int )(c->sock_wfd)->fd %
                                                        (8U * sizeof(__fd_mask ));
        }
      }
    }
# 350
    if ((c->ssl_wfd)->wr) {
# 350
      if (c->sock_ptr) {
        {
# 358
        wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                          (8U * sizeof(__fd_mask ))] |= 1L <<
                                                        (unsigned int )(c->ssl_wfd)->fd %
                                                        (8U * sizeof(__fd_mask ));
        }
      } else {
# 350
        if (ssl_closing == 1) {
          {
# 358
          wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                            (8U * sizeof(__fd_mask ))] |= 1L <<
                                                          (unsigned int )(c->ssl_wfd)->fd %
                                                          (8U *
                                                           sizeof(__fd_mask ));
          }
        } else {
# 350
          if (c->ssl_ptr < 16384) {
            goto _L___0;
          } else {
# 350
            if (ssl_closing == 2) {
              _L___0: 
              {
# 350
              tmp___0 = SSL_want(c->ssl);
              }
# 350
              if (tmp___0 == 2) {
                {
# 358
                wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                                  (8U * sizeof(__fd_mask ))] |= 1L <<
                                                                (unsigned int )(c->ssl_wfd)->fd %
                                                                (8U *
                                                                 sizeof(__fd_mask ));
                }
              }
            }
          }
        }
      }
    }
# 361
    if ((c->sock_rfd)->rd) {
      {
# 361
      tv.tv_sec = (long )(c->opt)->timeout_idle;
      }
    } else {
# 361
      if ((c->ssl_wfd)->wr) {
# 361
        if (c->sock_ptr) {
          {
# 361
          tv.tv_sec = (long )(c->opt)->timeout_idle;
          }
        } else {
          goto _L___1;
        }
      } else {
        _L___1: 
# 361
        if ((c->sock_wfd)->wr) {
# 361
          if (c->ssl_ptr) {
            {
# 361
            tv.tv_sec = (long )(c->opt)->timeout_idle;
            }
          } else {
            {
# 361
            tv.tv_sec = (long )(c->opt)->timeout_close;
            }
          }
        } else {
          {
# 361
          tv.tv_sec = (long )(c->opt)->timeout_close;
          }
        }
      }
    }
    {
# 364
    tv.tv_usec = 0L;
# 365
    ready = sselect(fdno, & rd_set, & wr_set, (fd_set *)((void *)0), & tv);
    }
# 366
    if (ready < 0) {
      {
# 367
      sockerror("select");
      }
# 368
      return (-1);
    }
# 370
    if (! ready) {
# 371
      if ((c->sock_rfd)->rd) {
        {
# 372
        log(7, (char const   *)"select timeout: connection reset");
        }
# 373
        return (-1);
      } else {
        {
# 375
        log(7, (char const   *)"select timeout waiting for SSL close_notify");
        }
# 376
        break;
      }
    }
# 380
    if (ssl_closing == 1) {
      goto _L___3;
    } else {
# 380
      if (ssl_closing == 2) {
        {
# 380
        tmp___2 = SSL_want(c->ssl);
        }
# 380
        if (tmp___2 == 3) {
# 380
          if (rd_set.__fds_bits[(unsigned int )(c->ssl_rfd)->fd /
                                (8U * sizeof(__fd_mask ))] &
              (1L << (unsigned int )(c->ssl_rfd)->fd % (8U * sizeof(__fd_mask )))) {
            goto _L___3;
          } else {
            goto _L___4;
          }
        } else {
          _L___4: 
          {
# 380
          tmp___3 = SSL_want(c->ssl);
          }
# 380
          if (tmp___3 == 2) {
# 380
            if (wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                                  (8U * sizeof(__fd_mask ))] &
                (1L << (unsigned int )(c->ssl_wfd)->fd %
                       (8U * sizeof(__fd_mask )))) {
              _L___3: 
              {
# 384
              tmp___1 = SSL_shutdown(c->ssl);
              }
# 384
              switch (tmp___1) {
              case 1: 
              {
# 386
              log(6,
                  (char const   *)"SSL_shutdown successfully sent close_notify");
# 387
              (c->ssl_wfd)->wr = 0;
# 390
              ssl_closing = 3;
              }
# 391
              break;
              case 0: 
              {
# 393
              log(7, (char const   *)"SSL_shutdown retrying");
# 394
              ssl_closing = 2;
              }
# 395
              break;
              case -1: 
              {
# 397
              sslerror("SSL_shutdown");
              }
# 398
              return (-1);
              }
            }
          }
        }
      }
    }
    {
# 404
    check_SSL_pending = 0;
    }
# 406
    if ((c->sock_wfd)->wr) {
# 406
      if (wr_set.__fds_bits[(unsigned int )(c->sock_wfd)->fd /
                            (8U * sizeof(__fd_mask ))] &
          (1L << (unsigned int )(c->sock_wfd)->fd % (8U * sizeof(__fd_mask )))) {
        {
# 407
        num = write((c->sock_wfd)->fd, (void const   *)(c->ssl_buff),
                    (unsigned int )c->ssl_ptr);
        }
# 407
        switch (num) {
        case -1: 
        {
# 409
        tmp___4 = __errno_location();
        }
# 409
        switch ((*tmp___4)) {
        case 4: 
        {
# 411
        log(7, (char const   *)"writesocket interrupted by a signal: retrying");
        }
# 413
        break;
        case 11: 
        {
# 415
        log(5, (char const   *)"writesocket would block: retrying");
        }
# 416
        break;
        default: 
        {
# 418
        sockerror("writesocket");
        }
# 419
        return (-1);
        }
# 421
        break;
        case 0: 
        {
# 423
        log(7, (char const   *)"No data written to the socket: retrying");
        }
# 424
        break;
        default: 
        {
# 426
        memmove((void *)(c->ssl_buff), (void const   *)(c->ssl_buff + num),
                (unsigned int )(c->ssl_ptr - num));
        }
# 427
        if (c->ssl_ptr == 16384) {
          {
# 428
          check_SSL_pending = 1;
          }
        }
        {
# 429
        c->ssl_ptr -= num;
# 430
        c->sock_bytes += num;
        }
# 431
        if (! (c->ssl_rfd)->rd) {
# 431
          if (! c->ssl_ptr) {
            {
# 432
            shutdown((c->sock_wfd)->fd, 1);
# 433
            log(7,
                (char const   *)"Socket write shutdown (no more data to send)");
# 435
            (c->sock_wfd)->wr = 0;
            }
          }
        }
        }
      }
    }
# 440
    if ((c->ssl_wfd)->wr) {
# 440
      if (c->sock_ptr) {
# 440
        if (wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                              (8U * sizeof(__fd_mask ))] &
            (1L << (unsigned int )(c->ssl_wfd)->fd % (8U * sizeof(__fd_mask )))) {
          goto _L___5;
        } else {
          goto _L___6;
        }
      } else {
        _L___6: 
        {
# 440
        tmp___6 = SSL_want(c->ssl);
        }
# 440
        if (tmp___6 == 3) {
# 440
          if (rd_set.__fds_bits[(unsigned int )(c->ssl_rfd)->fd /
                                (8U * sizeof(__fd_mask ))] &
              (1L << (unsigned int )(c->ssl_rfd)->fd % (8U * sizeof(__fd_mask )))) {
            _L___5: 
            {
# 447
            num = SSL_write(c->ssl, (void const   *)(c->sock_buff), c->sock_ptr);
# 449
            err = SSL_get_error(c->ssl, num);
            }
# 450
            switch (err) {
            case 0: 
            {
# 452
            memmove((void *)(c->sock_buff),
                    (void const   *)(c->sock_buff + num),
                    (unsigned int )(c->sock_ptr - num));
# 453
            c->sock_ptr -= num;
# 454
            c->ssl_bytes += num;
            }
# 455
            if (! ssl_closing) {
# 455
              if (! (c->sock_rfd)->rd) {
# 455
                if (! c->sock_ptr) {
# 455
                  if ((c->ssl_wfd)->wr) {
                    {
# 456
                    log(7,
                        (char const   *)"SSL write shutdown (no more data to send)");
# 458
                    ssl_closing = 1;
                    }
                  }
                }
              }
            }
# 460
            break;
            case 3: 
            {

            }
            case 2: 
            {

            }
            case 4: 
            {
# 464
            log(7, (char const   *)"SSL_write returned WANT_: retrying");
            }
# 465
            break;
            case 5: 
            {

            }
# 467
            if (num < 0) {
              {
# 468
              tmp___5 = __errno_location();
              }
# 468
              switch ((*tmp___5)) {
              case 4: 
              {
# 470
              log(7,
                  (char const   *)"SSL_write interrupted by a signal: retrying");
              }
# 472
              break;
              case 11: 
              {
# 474
              log(7, (char const   *)"SSL_write returned EAGAIN: retrying");
              }
# 476
              break;
              default: 
              {
# 478
              sockerror("SSL_write (ERROR_SYSCALL)");
              }
# 479
              return (-1);
              }
            }
# 482
            break;
            case 6: 
            {
# 484
            log(7, (char const   *)"SSL closed on SSL_write");
# 485
            (c->ssl_wfd)->wr = 0;
# 485
            (c->ssl_rfd)->rd = (c->ssl_wfd)->wr;
            }
# 486
            break;
            case 1: 
            {
# 488
            sslerror("SSL_write");
            }
# 489
            return (-1);
            default: 
            {
# 491
            log(3, (char const   *)"SSL_write/SSL_get_error returned %d", err);
            }
# 492
            return (-1);
            }
          }
        }
      }
    }
# 496
    if ((c->sock_rfd)->rd) {
# 496
      if (rd_set.__fds_bits[(unsigned int )(c->sock_rfd)->fd /
                            (8U * sizeof(__fd_mask ))] &
          (1L << (unsigned int )(c->sock_rfd)->fd % (8U * sizeof(__fd_mask )))) {
        {
# 497
        num = read((c->sock_rfd)->fd, (void *)(c->sock_buff + c->sock_ptr),
                   (unsigned int )(16384 - c->sock_ptr));
        }
# 497
        switch (num) {
        case -1: 
        {
# 500
        tmp___7 = __errno_location();
        }
# 500
        switch ((*tmp___7)) {
        case 4: 
        {
# 502
        log(7, (char const   *)"readsocket interrupted by a signal: retrying");
        }
# 504
        break;
        case 11: 
        {
# 506
        log(5, (char const   *)"readsocket would block: retrying");
        }
# 507
        break;
        default: 
        {
# 509
        sockerror("readsocket");
        }
# 510
        return (-1);
        }
# 512
        break;
        case 0: 
        {
# 514
        log(7, (char const   *)"Socket closed on read");
# 515
        (c->sock_rfd)->rd = 0;
        }
# 516
        if (! ssl_closing) {
# 516
          if (! c->sock_ptr) {
# 516
            if ((c->ssl_wfd)->wr) {
              {
# 517
              log(7, (char const   *)"SSL write shutdown (output buffer empty)");
# 519
              ssl_closing = 1;
              }
            }
          }
        }
# 521
        break;
        default: 
        {
# 523
        c->sock_ptr += num;
        }
        }
      }
    }
# 527
    if ((c->ssl_rfd)->rd) {
# 527
      if (c->ssl_ptr < 16384) {
# 527
        if (rd_set.__fds_bits[(unsigned int )(c->ssl_rfd)->fd /
                              (8U * sizeof(__fd_mask ))] &
            (1L << (unsigned int )(c->ssl_rfd)->fd % (8U * sizeof(__fd_mask )))) {
          goto _L___7;
        } else {
          goto _L___9;
        }
      } else {
        _L___9: 
        {
# 527
        tmp___9 = SSL_want(c->ssl);
        }
# 527
        if (tmp___9 == 2) {
# 527
          if (wr_set.__fds_bits[(unsigned int )(c->ssl_wfd)->fd /
                                (8U * sizeof(__fd_mask ))] &
              (1L << (unsigned int )(c->ssl_wfd)->fd % (8U * sizeof(__fd_mask )))) {
            goto _L___7;
          } else {
            goto _L___8;
          }
        } else {
          _L___8: 
# 527
          if (check_SSL_pending) {
            {
# 527
            tmp___10 = SSL_pending(c->ssl);
            }
# 527
            if (tmp___10) {
              _L___7: 
              {
# 536
              num = SSL_read(c->ssl, (void *)(c->ssl_buff + c->ssl_ptr),
                             16384 - c->ssl_ptr);
# 538
              err = SSL_get_error(c->ssl, num);
              }
# 539
              switch (err) {
              case 0: 
              {
# 541
              c->ssl_ptr += num;
              }
# 542
              break;
              case 3: 
              {

              }
              case 2: 
              {

              }
              case 4: 
              {
# 546
              log(7, (char const   *)"SSL_read returned WANT_: retrying");
              }
# 547
              break;
              case 5: 
              {

              }
# 549
              if (num < 0) {
                {
# 550
                tmp___8 = __errno_location();
                }
# 550
                switch ((*tmp___8)) {
                case 4: 
                {
# 552
                log(7,
                    (char const   *)"SSL_read interrupted by a signal: retrying");
                }
# 554
                break;
                case 11: 
                {
# 556
                log(7, (char const   *)"SSL_read returned EAGAIN: retrying");
                }
# 558
                break;
                default: 
                {
# 560
                sockerror("SSL_read (ERROR_SYSCALL)");
                }
# 561
                return (-1);
                }
              } else {
                {
# 564
                log(7, (char const   *)"SSL socket closed on SSL_read");
# 565
                (c->ssl_wfd)->wr = 0;
# 565
                (c->ssl_rfd)->rd = (c->ssl_wfd)->wr;
                }
              }
# 567
              break;
              case 6: 
              {
# 569
              log(7, (char const   *)"SSL closed on SSL_read");
# 570
              (c->ssl_rfd)->rd = 0;
              }
# 571
              if (! ssl_closing) {
# 571
                if (! c->sock_ptr) {
# 571
                  if ((c->ssl_wfd)->wr) {
                    {
# 572
                    log(7,
                        (char const   *)"SSL write shutdown (output buffer empty)");
# 574
                    ssl_closing = 1;
                    }
                  }
                }
              }
# 576
              if (! c->ssl_ptr) {
# 576
                if ((c->sock_wfd)->wr) {
                  {
# 577
                  shutdown((c->sock_wfd)->fd, 1);
# 578
                  log(7,
                      (char const   *)"Socket write shutdown (output buffer empty)");
# 580
                  (c->sock_wfd)->wr = 0;
                  }
                }
              }
# 582
              break;
              case 1: 
              {
# 584
              sslerror("SSL_read");
              }
# 585
              return (-1);
              default: 
              {
# 587
              log(3, (char const   *)"SSL_read/SSL_get_error returned %d", err);
              }
# 588
              return (-1);
              }
            }
          }
        }
      }
    }
  }
# 592
  return (0);
}
}
# 595
static void cleanup(CLI *c , int error ) 
{ 

  {
# 597
  if (c->ssl) {
    {
# 598
    SSL_set_shutdown(c->ssl, 3);
# 599
    SSL_free(c->ssl);
# 600
    ERR_remove_state(0UL);
    }
  }
# 603
  if (c->remote_fd.fd >= 0) {
# 604
    if (error) {
# 604
      if (c->remote_fd.is_socket) {
        {
# 605
        reset(c->remote_fd.fd, "linger (remote)");
        }
      }
    }
    {
# 606
    close(c->remote_fd.fd);
    }
  }
# 609
  if (c->local_rfd.fd >= 0) {
# 610
    if (c->local_rfd.fd == c->local_wfd.fd) {
# 611
      if (error) {
# 611
        if (c->local_rfd.is_socket) {
          {
# 612
          reset(c->local_rfd.fd, "linger (local)");
          }
        }
      }
      {
# 613
      close(c->local_rfd.fd);
      }
    } else {
# 615
      if (error) {
# 615
        if (c->local_rfd.is_socket) {
          {
# 616
          reset(c->local_rfd.fd, "linger (local_rfd)");
          }
        }
      }
# 617
      if (error) {
# 617
        if (c->local_wfd.is_socket) {
          {
# 618
          reset(c->local_wfd.fd, "linger (local_wfd)");
          }
        }
      }
    }
  }
# 621
  return;
}
}
# 623
static void print_cipher(CLI *c ) 
{ SSL_CIPHER *cipher ;
  char buf[1024] ;
  int len ;

  {
  {
# 632
  cipher = SSL_get_current_cipher(c->ssl);
# 633
  SSL_CIPHER_description(cipher, buf, 1024);
# 634
  len = (int )strlen((char const   *)(buf));
  }
# 635
  if (len > 0) {
    {
# 636
    buf[len - 1] = '\0';
    }
  }
  {
# 637
  log(6, (char const   *)"Negotiated ciphers: %s", buf);
  }
# 639
  return;
}
}
# 641
static int auth_libwrap(CLI *c ) 
{ struct request_info request ;
  int result ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp ;

  {
  {
# 646
  enter_critical_section(1);
# 647
  request_init(& request, 2, (c->opt)->servname, 1, c->local_rfd.fd, 0);
# 649
  sock_host(& request);
# 650
  result = hosts_access(& request);
# 651
  leave_critical_section(1);
  }
# 653
  if (! result) {
    {
# 655
    __x = c->addr.sin_port;
# 655
    tmp = ((int )__x);
    }
# 655
    if (tmp) {
      {
# 655
      __v = (unsigned short )((((int )__x >> 8) & 255) |
                              (((int )__x & 255) << 8));
      }
    } else {
      {
# 655
      __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
      }
    }
    {
# 655
    log(4, (char const   *)"Connection from %s:%d REFUSED by libwrap",
        c->accepting_address, __v);
# 656
    log(7, (char const   *)"See hosts_access(5) for details");
    }
# 657
    return (-1);
  }
# 660
  return (0);
}
}
# 663
static int auth_user(CLI *c ) 
{ struct servent *s_ent ;
  struct sockaddr_in ident ;
  int fd ;
  char name[1024] ;
  int retval___0 ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp ;
  int *tmp___0 ;
  int tmp___1 ;
  int *tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  register unsigned short __v___0 ;
  register unsigned short __x___0 ;
  int tmp___5 ;
  register unsigned short __v___1 ;
  register unsigned short __x___1 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___10 ;
  char *p ;
  int tmp___11 ;

  {
# 670
  if (! (c->opt)->username) {
# 671
    return (0);
  }
  {
# 672
  fd = socket(2, 1, 0);
  }
# 672
  if (fd < 0) {
    {
# 673
    sockerror("socket (auth_user)");
    }
# 674
    return (-1);
  }
  {
# 676
  alloc_fd(fd);
# 677
  memcpy((void */* __restrict  */)((void *)(& ident)),
         (void const   */* __restrict  */)((void const   *)(& c->addr)),
         sizeof(ident));
# 678
  s_ent = getservbyname((char const   *)"auth", (char const   *)"tcp");
  }
# 679
  if (s_ent) {
    {
# 683
    ident.sin_port = (unsigned short )s_ent->s_port;
    }
  } else {
    {
# 680
    log(4, (char const   *)"Unknown service \'auth\': using default 113");
# 681
    __x = (unsigned short )113;
# 681
    tmp = ((int )__x);
    }
# 681
    if (tmp) {
      {
# 681
      __v = (unsigned short )((((int )__x >> 8) & 255) |
                              (((int )__x & 255) << 8));
      }
    } else {
      {
# 681
      __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
      }
    }
    {
# 681
    ident.sin_port = __v;
    }
  }
  {
# 685
  tmp___4 = connect(fd, (void const   *)((struct sockaddr *)(& ident)),
                    sizeof(ident));
  }
# 685
  if (tmp___4 < 0) {
    {
# 686
    tmp___0 = __errno_location();
    }
# 686
    switch ((*tmp___0)) {
    case 115: 
    {
# 688
    log(7, (char const   *)"connect #1 (auth_user): EINPROGRESS: retrying");
    }
# 689
    break;
    case 11: 
    {
# 691
    log(7, (char const   *)"connect #1 (auth_user): EWOULDBLOCK: retrying");
    }
# 692
    break;
    default: 
    {
# 694
    sockerror("connect #1 (auth_user)");
# 695
    close(fd);
    }
# 696
    return (-1);
    }
    {
# 698
    tmp___1 = waitforsocket(fd, 1, (c->opt)->timeout_busy);
    }
# 698
    if (tmp___1 < 1) {
      {
# 699
      close(fd);
      }
# 700
      return (-1);
    }
    {
# 702
    tmp___3 = connect(fd, (void const   *)((struct sockaddr *)(& ident)),
                      sizeof(ident));
    }
# 702
    if (tmp___3 < 0) {
      {
# 703
      tmp___2 = __errno_location();
      }
# 703
      switch ((*tmp___2)) {
      case 22: 
      {
# 705
      log(7, (char const   *)"connect #2 (auth_user): EINVAL: ok");
      }
      case 106: 
      {

      }
# 707
      break;
      default: 
      {
# 709
      sockerror("connect #2 (auth_user))");
# 710
      close(fd);
      }
# 711
      return (-1);
      }
    }
  }
  {
# 715
  log(7, (char const   *)"IDENT server connected");
# 717
  __x___0 = (c->opt)->localport;
# 717
  tmp___5 = ((int )__x___0);
  }
# 717
  if (tmp___5) {
    {
# 717
    __v___0 = (unsigned short )((((int )__x___0 >> 8) & 255) |
                                (((int )__x___0 & 255) << 8));
    }
  } else {
    {
# 717
    __asm__  ("rorw $8, %w0": "=r" (__v___0): "0" (__x___0): "cc");
    }
  }
  {
# 717
  __x___1 = c->addr.sin_port;
# 717
  tmp___6 = ((int )__x___1);
  }
# 717
  if (tmp___6) {
    {
# 717
    __v___1 = (unsigned short )((((int )__x___1 >> 8) & 255) |
                                (((int )__x___1 & 255) << 8));
    }
  } else {
    {
# 717
    __asm__  ("rorw $8, %w0": "=r" (__v___1): "0" (__x___1): "cc");
    }
  }
  {
# 717
  tmp___7 = fdprintf(c, fd, (char const   *)"%u , %u", __v___1, __v___0);
  }
# 717
  if (tmp___7 < 0) {
    {
# 718
    sockerror("fdprintf (auth_user)");
# 719
    close(fd);
    }
# 720
    return (-1);
  }
  {
# 722
  tmp___8 = fdscanf(c, fd, (char const   *)"%*[^:]: USERID :%*[^:]:%s", name);
  }
# 722
  if (tmp___8 != 1) {
    {
# 723
    log(3, (char const   *)"Incorrect data from IDENT server");
# 724
    close(fd);
    }
# 725
    return (-1);
  }
  {
# 727
  close(fd);
# 728
  tmp___10 = strcmp((char const   *)(name), (char const   *)(c->opt)->username);
  }
# 728
  if (tmp___10) {
    {
# 728
    retval___0 = -1;
    }
  } else {
    {
# 728
    retval___0 = 0;
    }
  }
# 729
  while (1) {
    {
# 729
    p = name;
    }
# 729
    while ((*p)) {
      {
# 729
      tmp___11 = _get__ctype_b((int )(*p));
      }
# 729
      if (! (tmp___11 & 16384)) {
        {
# 729
        (*p) = '.';
        }
      }
      {
# 729
      p ++;
      }
    }
# 729
    break;
  }
  {
# 730
  log(6, (char const   *)"IDENT resolved remote user to %s", name);
  }
# 731
  return (retval___0);
}
}
# 734
static int connect_local(CLI *c ) 
{ char env[3][1024] ;
  char name[1024] ;
  int fd[2] ;
  int pid ;
  X509 *peer ;
  sigset_t newmask ;
  char tty[1024] ;
  int tmp ;
  int tmp___0 ;
  size_t tmp___1 ;
  X509_NAME *tmp___2 ;
  char *p ;
  int tmp___3 ;
  size_t tmp___4 ;
  X509_NAME *tmp___5 ;
  char *p___0 ;
  int tmp___6 ;
  size_t tmp___7 ;

  {
# 745
  if ((c->opt)->option.pty) {
    {
# 748
    tmp = pty_allocate(fd, fd + 1, tty, 1024);
    }
# 748
    if (tmp) {
# 749
      return (-1);
    }
    {
# 751
    log(7, (char const   *)"%s allocated", tty);
    }
  } else {
    {
# 753
    tmp___0 = make_sockets(fd);
    }
# 753
    if (tmp___0) {
# 754
      return (-1);
    }
  }
  {
# 756
  pid = fork();
# 757
  c->pid = (unsigned long )pid;
  }
# 758
  switch (pid) {
  case -1: 
  {
# 760
  close(fd[0]);
# 761
  close(fd[1]);
# 762
  ioerror("fork");
  }
# 763
  return (-1);
  case 0: 
  {
# 765
  close(fd[0]);
# 766
  dup2(fd[1], 0);
# 767
  dup2(fd[1], 1);
  }
# 768
  if (! options.option.foreground) {
    {
# 769
    dup2(fd[1], 2);
    }
  }
  {
# 770
  close(fd[1]);
# 771
  env[0][1023] = '\0';
# 771
  strncpy((char */* __restrict  */)(env[0]),
          (char const   */* __restrict  */)((char const   *)"REMOTE_HOST="),
          1023U);
# 772
  env[0][1023] = '\0';
# 772
  tmp___1 = strlen((char const   *)(env[0]));
# 772
  strncat((char */* __restrict  */)(env[0]),
          (char const   */* __restrict  */)((char const   *)(c->accepting_address)),
          (1024U - tmp___1) - 1U);
# 773
  putenv(env[0]);
  }
# 774
  if ((c->opt)->option.transparent) {
    {
# 775
    putenv("LD_PRELOAD=/usr/local/lib/libstunnel.so");
# 777
    putenv("_RLD_LIST=/usr/local/lib/libstunnel.so:DEFAULT");
    }
  }
# 779
  if (c->ssl) {
    {
# 780
    peer = SSL_get_peer_certificate(c->ssl);
    }
# 781
    if (peer) {
      {
# 782
      env[1][1023] = '\0';
# 782
      strncpy((char */* __restrict  */)(env[1]),
              (char const   */* __restrict  */)((char const   *)"SSL_CLIENT_DN="),
              1023U);
# 783
      tmp___2 = X509_get_subject_name(peer);
# 783
      X509_NAME_oneline(tmp___2, name, 1024);
      }
# 784
      while (1) {
        {
# 784
        p = name;
        }
# 784
        while ((*p)) {
          {
# 784
          tmp___3 = _get__ctype_b((int )(*p));
          }
# 784
          if (! (tmp___3 & 16384)) {
            {
# 784
            (*p) = '.';
            }
          }
          {
# 784
          p ++;
          }
        }
# 784
        break;
      }
      {
# 785
      env[1][1023] = '\0';
# 785
      tmp___4 = strlen((char const   *)(env[1]));
# 785
      strncat((char */* __restrict  */)(env[1]),
              (char const   */* __restrict  */)((char const   *)(name)),
              (1024U - tmp___4) - 1U);
# 786
      putenv(env[1]);
# 787
      env[2][1023] = '\0';
# 787
      strncpy((char */* __restrict  */)(env[2]),
              (char const   */* __restrict  */)((char const   *)"SSL_CLIENT_I_DN="),
              1023U);
# 788
      tmp___5 = X509_get_issuer_name(peer);
# 788
      X509_NAME_oneline(tmp___5, name, 1024);
      }
# 789
      while (1) {
        {
# 789
        p___0 = name;
        }
# 789
        while ((*p___0)) {
          {
# 789
          tmp___6 = _get__ctype_b((int )(*p___0));
          }
# 789
          if (! (tmp___6 & 16384)) {
            {
# 789
            (*p___0) = '.';
            }
          }
          {
# 789
          p___0 ++;
          }
        }
# 789
        break;
      }
      {
# 790
      env[2][1023] = '\0';
# 790
      tmp___7 = strlen((char const   *)(env[2]));
# 790
      strncat((char */* __restrict  */)(env[2]),
              (char const   */* __restrict  */)((char const   *)(name)),
              (1024U - tmp___7) - 1U);
# 791
      putenv(env[2]);
# 792
      X509_free(peer);
      }
    }
  }
  {
# 796
  sigemptyset(& newmask);
# 797
  sigprocmask(2,
              (sigset_t const   */* __restrict  */)((sigset_t const   *)(& newmask)),
              (sigset_t */* __restrict  */)((sigset_t *)((void *)0)));
# 799
  execvp((char const   *)(c->opt)->execname, (char * const  *)(c->opt)->execargs);
# 800
  ioerror((c->opt)->execname);
# 801
  _exit(1);
  }
  default: 
  {

  }
# 803
  break;
  }
  {
# 806
  log(6, (char const   *)"Local mode child started (PID=%lu)", c->pid);
# 807
  close(fd[1]);
# 809
  fcntl(fd[0], 2, 1);
  }
# 811
  return (fd[0]);
}
}
# 817
static int make_sockets(int *fd ) 
{ struct sockaddr_in addr ;
  int addrlen ;
  int s ;
  register unsigned int __v ;
  register unsigned int __x ;
  int tmp ;
  int *tmp___0 ;
  int tmp___1 ;
  int *tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;

  {
  {
# 823
  s = socket(2, 1, 0);
  }
# 823
  if (s < 0) {
    {
# 824
    sockerror("socket#1");
    }
# 825
    return (-1);
  }
  {
# 827
  (*(fd + 1)) = socket(2, 1, 0);
  }
# 827
  if ((*(fd + 1)) < 0) {
    {
# 828
    sockerror("socket#2");
    }
# 829
    return (-1);
  }
  {
# 831
  addrlen = (int )sizeof(addr);
# 832
  memset((void *)(& addr), 0, (unsigned int )addrlen);
# 833
  addr.sin_family = (unsigned short )2;
# 834
  __x = 2130706433U;
# 834
  tmp = ((int )__x);
  }
# 834
  if (tmp) {
    {
# 834
    __v = ((((__x & 4278190080U) >> 24) | ((__x & 16711680U) >> 8)) |
           ((__x & 65280U) << 8)) | ((__x & 255U) << 24);
    }
  } else {
    {
# 834
    __asm__  ("rorw $8, %w0;"
              "rorl $16, %0;"
              "rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
    }
  }
  {
# 834
  addr.sin_addr.s_addr = __v;
# 835
  addr.sin_port = (unsigned short )0;
# 836
  tmp___1 = bind(s, (void const   *)((struct sockaddr *)(& addr)),
                 (unsigned int )addrlen);
  }
# 836
  if (tmp___1) {
    {
# 837
    tmp___0 = __errno_location();
# 837
    log_error(7, (*tmp___0), "bind#1");
    }
  }
  {
# 838
  tmp___3 = bind((*(fd + 1)), (void const   *)((struct sockaddr *)(& addr)),
                 (unsigned int )addrlen);
  }
# 838
  if (tmp___3) {
    {
# 839
    tmp___2 = __errno_location();
# 839
    log_error(7, (*tmp___2), "bind#2");
    }
  }
  {
# 840
  tmp___4 = listen(s, 5);
  }
# 840
  if (tmp___4) {
    {
# 841
    sockerror("listen");
    }
# 842
    return (-1);
  }
  {
# 844
  tmp___5 = getsockname(s,
                        (void */* __restrict  */)((void *)((struct sockaddr *)(& addr))),
                        (socklen_t */* __restrict  */)((socklen_t *)(& addrlen)));
  }
# 844
  if (tmp___5) {
    {
# 845
    sockerror("getsockname");
    }
# 846
    return (-1);
  }
  {
# 848
  tmp___6 = connect((*(fd + 1)), (void const   *)((struct sockaddr *)(& addr)),
                    (unsigned int )addrlen);
  }
# 848
  if (tmp___6) {
    {
# 849
    sockerror("connect");
    }
# 850
    return (-1);
  }
  {
# 852
  (*(fd + 0)) = accept(s,
                       (void */* __restrict  */)((void *)((struct sockaddr *)(& addr))),
                       (socklen_t */* __restrict  */)((socklen_t *)(& addrlen)));
  }
# 852
  if ((*(fd + 0)) < 0) {
    {
# 853
    sockerror("accept");
    }
# 854
    return (-1);
  }
  {
# 856
  close(s);
  }
# 863
  return (0);
}
}
# 867
static int connect_remote(CLI *c ) 
{ struct sockaddr_in addr ;
  u32 *list ;
  int error ;
  int s ;
  u16 dport ;
  int tmp ;
  int tmp___0 ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp___1 ;
  int tmp___2 ;
  register unsigned short __v___0 ;
  register unsigned short __x___0 ;
  int tmp___3 ;
  int tmp___4 ;
  int *tmp___5 ;
  char *tmp___6 ;
  register unsigned short __v___1 ;
  register unsigned short __x___1 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___9 ;
  int *tmp___10 ;
  char *tmp___11 ;
  register unsigned short __v___2 ;
  register unsigned short __x___2 ;
  int tmp___12 ;

  {
  {
# 874
  memset((void *)(& addr), 0, sizeof(addr));
# 875
  addr.sin_family = (unsigned short )2;
  }
# 877
  if ((c->opt)->option.delayed_lookup) {
    {
# 878
    tmp = name2nums((c->opt)->remote_address, "127.0.0.1",
                    & c->resolved_addresses, & dport);
    }
# 878
    if (tmp == 0) {
# 881
      return (-1);
    }
    {
# 883
    list = c->resolved_addresses;
    }
  } else {
    {
# 885
    list = (c->opt)->remotenames;
# 886
    dport = (c->opt)->remoteport;
    }
  }
# 890
  while ((*list) + 1U) {
    {
# 891
    s = socket(2, 1, 0);
    }
# 891
    if (s < 0) {
      {
# 892
      sockerror("remote socket");
      }
# 893
      return (-1);
    }
    {
# 895
    tmp___0 = alloc_fd(s);
    }
# 895
    if (tmp___0) {
# 896
      return (-1);
    }
# 898
    if (c->bind_ip) {
      {
# 899
      addr.sin_addr.s_addr = (unsigned int )c->bind_ip;
# 900
      __x = (unsigned short )0;
# 900
      tmp___1 = ((int )__x);
      }
# 900
      if (tmp___1) {
        {
# 900
        __v = (unsigned short )((((int )__x >> 8) & 255) |
                                (((int )__x & 255) << 8));
        }
      } else {
        {
# 900
        __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
        }
      }
      {
# 900
      addr.sin_port = __v;
# 901
      tmp___2 = bind(s, (void const   *)((struct sockaddr *)(& addr)),
                     sizeof(addr));
      }
# 901
      if (tmp___2 < 0) {
        {
# 902
        sockerror("bind transparent");
# 903
        close(s);
        }
# 904
        return (-1);
      }
    }
    {
# 909
    addr.sin_port = dport;
# 910
    addr.sin_addr.s_addr = (*list);
# 911
    safe_ntoa(c->connecting_address, addr.sin_addr);
# 913
    __x___0 = addr.sin_port;
# 913
    tmp___3 = ((int )__x___0);
    }
# 913
    if (tmp___3) {
      {
# 913
      __v___0 = (unsigned short )((((int )__x___0 >> 8) & 255) |
                                  (((int )__x___0 & 255) << 8));
      }
    } else {
      {
# 913
      __asm__  ("rorw $8, %w0": "=r" (__v___0): "0" (__x___0): "cc");
      }
    }
    {
# 913
    log(7, (char const   *)"%s connecting %s:%d", (c->opt)->servname,
        c->connecting_address, __v___0);
# 914
    tmp___4 = connect(s, (void const   *)((struct sockaddr *)(& addr)),
                      sizeof(addr));
    }
# 914
    if (! tmp___4) {
# 915
      return (s);
    }
    {
# 916
    tmp___5 = __errno_location();
# 916
    error = (*tmp___5);
    }
# 917
    switch (error) {
    case 115: 
    {
# 919
    log(7, (char const   *)"remote connect #1: EINPROGRESS: retrying");
    }
# 920
    break;
    case 11: 
    {
# 922
    log(7, (char const   *)"remote connect #1: EWOULDBLOCK: retrying");
    }
# 923
    break;
    default: 
    {
# 925
    tmp___6 = my_strerror(error);
# 926
    __x___1 = addr.sin_port;
# 926
    tmp___7 = ((int )__x___1);
    }
# 926
    if (tmp___7) {
      {
# 926
      __v___1 = (unsigned short )((((int )__x___1 >> 8) & 255) |
                                  (((int )__x___1 & 255) << 8));
      }
    } else {
      {
# 926
      __asm__  ("rorw $8, %w0": "=r" (__v___1): "0" (__x___1): "cc");
      }
    }
    {
# 926
    log(3, (char const   *)"remote connect #1 (%s:%d): %s (%d)",
        c->connecting_address, __v___1, tmp___6, error);
# 928
    close(s);
    }
    goto __Cont;
    }
    {
# 933
    tmp___8 = waitforsocket(s, 1, (c->opt)->timeout_busy);
    }
# 933
    if (tmp___8 < 1) {
      {
# 934
      close(s);
      }
      goto __Cont;
    }
    {
# 939
    tmp___9 = connect(s, (void const   *)((struct sockaddr *)(& addr)),
                      sizeof(addr));
    }
# 939
    if (! tmp___9) {
# 940
      return (s);
    }
    {
# 941
    tmp___10 = __errno_location();
# 941
    error = (*tmp___10);
    }
# 942
    switch (error) {
    case 22: 
    {
# 944
    log(7, (char const   *)"remote connect #2: EINVAL: ok");
    }
    case 106: 
    {

    }
# 946
    return (s);
    default: 
    {
# 948
    tmp___11 = my_strerror(error);
# 949
    __x___2 = addr.sin_port;
# 949
    tmp___12 = ((int )__x___2);
    }
# 949
    if (tmp___12) {
      {
# 949
      __v___2 = (unsigned short )((((int )__x___2 >> 8) & 255) |
                                  (((int )__x___2 & 255) << 8));
      }
    } else {
      {
# 949
      __asm__  ("rorw $8, %w0": "=r" (__v___2): "0" (__x___2): "cc");
      }
    }
    {
# 949
    log(3, (char const   *)"remote connect #2 (%s:%d): %s (%d)",
        c->connecting_address, __v___2, tmp___11, error);
# 951
    close(s);
    }
    goto __Cont;
    }
    __Cont: 
    {
# 890
    list ++;
    }
  }
# 955
  return (-1);
}
}
# 958
static void reset(int fd , char *txt ) 
{ struct linger l ;
  int *tmp ;
  int tmp___0 ;

  {
  {
# 962
  l.l_onoff = 1;
# 963
  l.l_linger = 0;
# 964
  tmp___0 = setsockopt(fd, 1, 13, (void const   *)((void *)(& l)), sizeof(l));
  }
# 964
  if (tmp___0) {
    {
# 965
    tmp = __errno_location();
# 965
    log_error(7, (*tmp), txt);
    }
  }
# 966
  return;
}
}
# 34 "log.c"
static FILE *outfile   = (FILE *)((void *)0);
# 71
void log_open(void) 
{ int fd ;

  {
# 74
  if (options.output_file) {
    {
# 75
    fd = open((char const   *)options.output_file, 1089, 416);
    }
# 76
    if (fd >= 0) {
      {
# 77
      fcntl(fd, 2, 1);
      {
# 19 "spec.work"
      outfile->__BLAST_FLAG = 1;
      {

      }
      }
# 78 "log.c"
      outfile = fdopen(fd, (char const   *)"a");
      }
# 79
      if (outfile) {
# 80
        return;
      }
    }
  }
# 83
  if (options.option.syslog) {
    {
# 87
    openlog((char const   *)"stunnel", 11, options.facility);
    }
  }
# 90
  if (options.output_file) {
    {
# 91
    log(3, (char const   *)"Unable to open output file: %s", options.output_file);
    }
  }
# 92
  return;
}
}
# 94
void log_close(void) 
{ 

  {
# 95
  if (outfile) {
    {

    {
# 50 "spec.work"
    if (outfile->__BLAST_FLAG == 1) {
# 51
      outfile->__BLAST_FLAG = 0;
    } else {
# 52
      __error__();
    }
    {

    }
    }
# 96 "log.c"
    fclose(outfile);
    }
# 97
    return;
  }
# 99
  if (options.option.syslog) {
    {
# 100
    closelog();
    }
  }
# 101
  return;
}
}
# 105
void log(int level , char const   *format  , ...) 
{ __ccured_va_list arglist ;
  char text[1024] ;
  char timestamped[1024] ;
  FILE *out ;
  time_t gmt ;
  struct tm *timeptr ;
  struct tm timestruct ;
  unsigned long tmp ;
  unsigned long tmp___1 ;
  unsigned long tmp___2 ;

  {
# 115
  if (level > options.debug_level) {
# 116
    return;
  }
  {
# 117
  tmp = (unsigned long )GCC_STDARG_START();
# 117
  __ccured_va_start(arglist, tmp);
# 119
  vsnprintf((char */* __restrict  */)(text), 1024U,
            (char const   */* __restrict  */)format, arglist);
# 123
  __ccured_va_end(arglist);
  }
# 125
  if (! outfile) {
# 125
    if (options.option.syslog) {
      {
# 126
      syslog(level, (char const   *)"%s", text);
      }
# 127
      return;
    }
  }
# 130
  if (outfile) {
    {
# 130
    out = outfile;
    }
  } else {
    {
# 130
    out = get_stderr();
    {
# 59 "spec.work"
    out->__BLAST_FLAG = 1;
    {

    }
    }

    }
  }
  {
# 131 "log.c"
  time(& gmt);
# 133
  timeptr = localtime_r((time_t const   */* __restrict  */)((time_t const   *)(& gmt)),
                        (struct tm */* __restrict  */)(& timestruct));
# 138
  tmp___1 = stunnel_thread_id();
# 138
  tmp___2 = stunnel_process_id();
# 138
  snprintf((char */* __restrict  */)(timestamped), 1024U,
           (char const   */* __restrict  */)((char const   *)"%04d.%02d.%02d %02d:%02d:%02d LOG%d[%lu:%lu]: %s"),
           timeptr->tm_year + 1900, timeptr->tm_mon + 1, timeptr->tm_mday,
           timeptr->tm_hour, timeptr->tm_min, timeptr->tm_sec, level, tmp___2,
           tmp___1, text);
  {
# 42 "spec.work"
  if (! (out->__BLAST_FLAG == 1)) {
# 44
    __error__();
  }
  {

  }
  }
# 151 "log.c"
  fprintf((FILE */* __restrict  */)out,
          (char const   */* __restrict  */)((char const   *)"%s\n"), timestamped);
# 152
  fflush(out);
  }
# 154
  return;
}
}
# 156
void log_raw(char const   *format  , ...) 
{ __ccured_va_list arglist ;
  char text[1024] ;
  FILE *out ;
  unsigned long tmp ;

  {
  {
# 161
  tmp = (unsigned long )GCC_STDARG_START();
# 161
  __ccured_va_start(arglist, tmp);
# 163
  vsnprintf((char */* __restrict  */)(text), 1024U,
            (char const   */* __restrict  */)format, arglist);
# 167
  __ccured_va_end(arglist);
  }
# 168
  if (outfile) {
    {
# 168
    out = outfile;
    }
  } else {
    {
# 168
    out = get_stderr();
    {
# 59 "spec.work"
    out->__BLAST_FLAG = 1;
    {

    }
    }

    }
  }
  {

  {
# 42
  if (! (out->__BLAST_FLAG == 1)) {
# 44
    __error__();
  }
  {

  }
  }
# 172 "log.c"
  fprintf((FILE */* __restrict  */)out,
          (char const   */* __restrict  */)((char const   *)"%s\n"), text);
# 173
  fflush(out);
  }
# 175
  return;
}
}
# 34 "options.c"
static int host2nums(char *hostname , u32 **hostlist ) ;
# 35
static int parse_debug_level(char *arg ) ;
# 36
static int parse_ssl_option(char *arg ) ;
# 37
static int print_socket_options(void) ;
# 38
static void print_option(char *line , int type , OPT_UNION *val ) ;
# 39
static int parse_socket_option(char *arg ) ;
# 40
static char *section_validate(LOCAL_OPTIONS *section ) ;
# 41
static char *stralloc(char *str ) ;
# 43
static char **argalloc(char *str ) ;
# 56
static char *option_not_found   = "Specified option name is not valid here";
# 58
static char *global_options(CMD cmd , char *opt , char *arg ) 
{ int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___9 ;
  int tmp___10 ;
  int tmp___11 ;
  int tmp___12 ;
  int tmp___13 ;
  int tmp___14 ;
  int tmp___15 ;
  int tmp___16 ;
  int tmp___17 ;
  int tmp___18 ;
  int tmp___19 ;
  int tmp___20 ;
  int tmp___21 ;
  int tmp___22 ;
  int tmp___23 ;
  int tmp___24 ;
  int tmp___25 ;
  int tmp___26 ;
  int tmp___27 ;
  int tmp___28 ;
  int tmp___29 ;
  int tmp___30 ;
  int tmp___31 ;
  int tmp___32 ;
  int tmp___33 ;

  {
# 60
  if (cmd == 2) {
    {
# 61
    log_raw((char const   *)"Global options");
    }
  } else {
# 60
    if (cmd == 3) {
      {
# 61
      log_raw((char const   *)"Global options");
      }
    }
  }
# 65
  switch (cmd) {
  case 0: 
  {
# 70
  options.ca_dir = (char *)((void *)0);
  }
# 71
  break;
  case 1: 
  {
# 73
  tmp = strcasecmp((char const   *)opt, (char const   *)"CApath");
  }
# 73
  if (tmp) {
# 74
    break;
  }
# 75
  if ((*(arg + 0))) {
    {
# 76
    options.ca_dir = stralloc(arg);
    }
  } else {
    {
# 78
    options.ca_dir = (char *)((void *)0);
    }
  }
# 79
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 85
  break;
  case 3: 
  {
# 87
  log_raw((char const   *)"%-15s = CA certificate directory for \'verify\' option",
          "CApath");
  }
# 89
  break;
  }
# 93
  switch (cmd) {
  case 0: 
  {
# 98
  options.ca_file = (char *)((void *)0);
  }
# 99
  break;
  case 1: 
  {
# 101
  tmp___0 = strcasecmp((char const   *)opt, (char const   *)"CAfile");
  }
# 101
  if (tmp___0) {
# 102
    break;
  }
# 103
  if ((*(arg + 0))) {
    {
# 104
    options.ca_file = stralloc(arg);
    }
  } else {
    {
# 106
    options.ca_file = (char *)((void *)0);
    }
  }
# 107
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 113
  break;
  case 3: 
  {
# 115
  log_raw((char const   *)"%-15s = CA certificate file for \'verify\' option",
          "CAfile");
  }
# 117
  break;
  }
# 121
  switch (cmd) {
  case 0: 
  {
# 124
  options.cert = "/usr/local/etc/stunnel/stunnel.pem";
  }
# 128
  break;
  case 1: 
  {
# 130
  tmp___1 = strcasecmp((char const   *)opt, (char const   *)"cert");
  }
# 130
  if (tmp___1) {
# 131
    break;
  }
  {
# 132
  options.cert = stralloc(arg);
# 133
  options.option.cert = 1U;
  }
# 134
  return ((char *)((void *)0));
  case 2: 
  {
# 136
  log_raw((char const   *)"%-15s = %s", "cert", options.cert);
  }
# 137
  break;
  case 3: 
  {
# 139
  log_raw((char const   *)"%-15s = certificate chain", "cert");
  }
# 140
  break;
  }
# 145
  switch (cmd) {
  case 0: 
  {
# 147
  options.chroot_dir = (char *)((void *)0);
  }
# 148
  break;
  case 1: 
  {
# 150
  tmp___2 = strcasecmp((char const   *)opt, (char const   *)"chroot");
  }
# 150
  if (tmp___2) {
# 151
    break;
  }
  {
# 152
  options.chroot_dir = stralloc(arg);
  }
# 153
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 155
  break;
  case 3: 
  {
# 157
  log_raw((char const   *)"%-15s = directory to chroot stunnel process",
          "chroot");
  }
# 158
  break;
  }
# 163
  switch (cmd) {
  case 0: 
  {
# 165
  options.cipher_list = "ALL:!ADH:+RC4:@STRENGTH";
  }
# 166
  break;
  case 1: 
  {
# 168
  tmp___3 = strcasecmp((char const   *)opt, (char const   *)"ciphers");
  }
# 168
  if (tmp___3) {
# 169
    break;
  }
  {
# 170
  options.cipher_list = stralloc(arg);
  }
# 171
  return ((char *)((void *)0));
  case 2: 
  {
# 173
  log_raw((char const   *)"%-15s = %s", "ciphers", "ALL:!ADH:+RC4:@STRENGTH");
  }
# 174
  break;
  case 3: 
  {
# 176
  log_raw((char const   *)"%-15s = list of permitted SSL ciphers", "ciphers");
  }
# 177
  break;
  }
# 181
  switch (cmd) {
  case 0: 
  {
# 183
  options.option.client = 0U;
  }
# 184
  break;
  case 1: 
  {
# 186
  tmp___4 = strcasecmp((char const   *)opt, (char const   *)"client");
  }
# 186
  if (tmp___4) {
# 187
    break;
  }
  {
# 188
  tmp___6 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 188
  if (tmp___6) {
    {
# 190
    tmp___5 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 190
    if (tmp___5) {
# 193
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 191
      options.option.client = 0U;
      }
    }
  } else {
    {
# 189
    options.option.client = 1U;
    }
  }
# 194
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 196
  break;
  case 3: 
  {
# 198
  log_raw((char const   *)"%-15s = yes|no client mode (remote service uses SSL)",
          "client");
  }
# 200
  break;
  }
# 204
  switch (cmd) {
  case 0: 
  {
# 206
  options.crl_dir = (char *)((void *)0);
  }
# 207
  break;
  case 1: 
  {
# 209
  tmp___7 = strcasecmp((char const   *)opt, (char const   *)"CRLpath");
  }
# 209
  if (tmp___7) {
# 210
    break;
  }
# 211
  if ((*(arg + 0))) {
    {
# 212
    options.crl_dir = stralloc(arg);
    }
  } else {
    {
# 214
    options.crl_dir = (char *)((void *)0);
    }
  }
# 215
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 217
  break;
  case 3: 
  {
# 219
  log_raw((char const   *)"%-15s = CRL directory", "CRLpath");
  }
# 220
  break;
  }
# 224
  switch (cmd) {
  case 0: 
  {
# 226
  options.crl_file = (char *)((void *)0);
  }
# 227
  break;
  case 1: 
  {
# 229
  tmp___8 = strcasecmp((char const   *)opt, (char const   *)"CRLfile");
  }
# 229
  if (tmp___8) {
# 230
    break;
  }
# 231
  if ((*(arg + 0))) {
    {
# 232
    options.crl_file = stralloc(arg);
    }
  } else {
    {
# 234
    options.crl_file = (char *)((void *)0);
    }
  }
# 235
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 237
  break;
  case 3: 
  {
# 239
  log_raw((char const   *)"%-15s = CRL file", "CRLfile");
  }
# 240
  break;
  }
# 244
  switch (cmd) {
  case 0: 
  {
# 246
  options.debug_level = 5;
# 248
  options.facility = 24;
  }
# 250
  break;
  case 1: 
  {
# 252
  tmp___9 = strcasecmp((char const   *)opt, (char const   *)"debug");
  }
# 252
  if (tmp___9) {
# 253
    break;
  }
  {
# 254
  tmp___10 = parse_debug_level(arg);
  }
# 254
  if (! tmp___10) {
# 255
    return ("Illegal debug argument");
  }
# 256
  return ((char *)((void *)0));
  case 2: 
  {
# 258
  log_raw((char const   *)"%-15s = %d", "debug", options.debug_level);
  }
# 259
  break;
  case 3: 
  {
# 261
  log_raw((char const   *)"%-15s = [facility].level (e.g. daemon.info)", "debug");
  }
# 262
  break;
  }
# 267
  switch (cmd) {
  case 0: 
  {
# 269
  options.egd_sock = (char *)((void *)0);
  }
# 270
  break;
  case 1: 
  {
# 272
  tmp___11 = strcasecmp((char const   *)opt, (char const   *)"EGD");
  }
# 272
  if (tmp___11) {
# 273
    break;
  }
  {
# 274
  options.egd_sock = stralloc(arg);
  }
# 275
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 280
  break;
  case 3: 
  {
# 282
  log_raw((char const   *)"%-15s = path to Entropy Gathering Daemon socket",
          "EGD");
  }
# 283
  break;
  }
# 289
  switch (cmd) {
  case 0: 
  {
# 291
  options.option.syslog = 0U;
# 292
  options.option.foreground = 0U;
  }
# 293
  break;
  case 1: 
  {
# 295
  tmp___12 = strcasecmp((char const   *)opt, (char const   *)"foreground");
  }
# 295
  if (tmp___12) {
# 296
    break;
  }
  {
# 297
  tmp___14 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 297
  if (tmp___14) {
    {
# 299
    tmp___13 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 299
    if (tmp___13) {
# 302
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 300
      options.option.foreground = 0U;
      }
    }
  } else {
    {
# 298
    options.option.foreground = 1U;
    }
  }
# 303
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 305
  break;
  case 3: 
  {
# 307
  log_raw((char const   *)"%-15s = yes|no foreground mode (don\'t fork, log to stderr)",
          "foreground");
  }
# 309
  break;
  }
# 314
  switch (cmd) {
  case 0: 
  {
# 316
  options.key = (char *)((void *)0);
  }
# 317
  break;
  case 1: 
  {
# 319
  tmp___15 = strcasecmp((char const   *)opt, (char const   *)"key");
  }
# 319
  if (tmp___15) {
# 320
    break;
  }
  {
# 321
  options.key = stralloc(arg);
  }
# 322
  return ((char *)((void *)0));
  case 2: 
  {
# 324
  log_raw((char const   *)"%-15s = %s", "key", options.cert);
  }
# 325
  break;
  case 3: 
  {
# 327
  log_raw((char const   *)"%-15s = certificate private key", "key");
  }
# 328
  break;
  }
# 332
  switch (cmd) {
  case 0: 
  {
# 334
  options.ssl_options = 0L;
  }
# 335
  break;
  case 1: 
  {
# 337
  tmp___16 = strcasecmp((char const   *)opt, (char const   *)"options");
  }
# 337
  if (tmp___16) {
# 338
    break;
  }
  {
# 339
  tmp___17 = parse_ssl_option(arg);
  }
# 339
  if (! tmp___17) {
# 340
    return ("Illegal SSL option");
  }
# 341
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 343
  break;
  case 3: 
  {
# 345
  log_raw((char const   *)"%-15s = SSL option", "options");
# 346
  log_raw((char const   *)"%18sset an SSL option", "");
  }
# 347
  break;
  }
# 351
  switch (cmd) {
  case 0: 
  {
# 353
  options.output_file = (char *)((void *)0);
  }
# 354
  break;
  case 1: 
  {
# 356
  tmp___18 = strcasecmp((char const   *)opt, (char const   *)"output");
  }
# 356
  if (tmp___18) {
# 357
    break;
  }
  {
# 358
  options.output_file = stralloc(arg);
  }
# 359
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 361
  break;
  case 3: 
  {
# 363
  log_raw((char const   *)"%-15s = file to append log messages", "output");
  }
# 364
  break;
  }
# 369
  switch (cmd) {
  case 0: 
  {
# 371
  options.pidfile = "/usr/local/var/run/stunnel.pid";
  }
# 372
  break;
  case 1: 
  {
# 374
  tmp___19 = strcasecmp((char const   *)opt, (char const   *)"pid");
  }
# 374
  if (tmp___19) {
# 375
    break;
  }
# 376
  if ((*(arg + 0))) {
    {
# 377
    options.pidfile = stralloc(arg);
    }
  } else {
    {
# 379
    options.pidfile = (char *)((void *)0);
    }
  }
# 380
  return ((char *)((void *)0));
  case 2: 
  {
# 382
  log_raw((char const   *)"%-15s = %s", "pid", "/usr/local/var/run/stunnel.pid");
  }
# 383
  break;
  case 3: 
  {
# 385
  log_raw((char const   *)"%-15s = pid file (empty to disable creating)", "pid");
  }
# 386
  break;
  }
# 391
  switch (cmd) {
  case 0: 
  {
# 393
  options.random_bytes = 64;
  }
# 394
  break;
  case 1: 
  {
# 396
  tmp___20 = strcasecmp((char const   *)opt, (char const   *)"RNDbytes");
  }
# 396
  if (tmp___20) {
# 397
    break;
  }
  {
# 398
  options.random_bytes = atoi((char const   *)arg);
  }
# 399
  return ((char *)((void *)0));
  case 2: 
  {
# 401
  log_raw((char const   *)"%-15s = %d", "RNDbytes", 64);
  }
# 402
  break;
  case 3: 
  {
# 404
  log_raw((char const   *)"%-15s = bytes to read from random seed files",
          "RNDbytes");
  }
# 405
  break;
  }
# 409
  switch (cmd) {
  case 0: 
  {
# 411
  options.rand_file = (char *)((void *)0);
  }
# 412
  break;
  case 1: 
  {
# 414
  tmp___21 = strcasecmp((char const   *)opt, (char const   *)"RNDfile");
  }
# 414
  if (tmp___21) {
# 415
    break;
  }
  {
# 416
  options.rand_file = stralloc(arg);
  }
# 417
  return ((char *)((void *)0));
  case 2: 
  {
# 420
  log_raw((char const   *)"%-15s = %s", "RNDfile", "/dev/urandom");
  }
# 422
  break;
  case 3: 
  {
# 424
  log_raw((char const   *)"%-15s = path to file with random seed data",
          "RNDfile");
  }
# 425
  break;
  }
# 429
  switch (cmd) {
  case 0: 
  {
# 431
  options.option.rand_write = 1U;
  }
# 432
  break;
  case 1: 
  {
# 434
  tmp___22 = strcasecmp((char const   *)opt, (char const   *)"RNDoverwrite");
  }
# 434
  if (tmp___22) {
# 435
    break;
  }
  {
# 436
  tmp___24 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 436
  if (tmp___24) {
    {
# 438
    tmp___23 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 438
    if (tmp___23) {
# 441
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 439
      options.option.rand_write = 0U;
      }
    }
  } else {
    {
# 437
    options.option.rand_write = 1U;
    }
  }
# 442
  return ((char *)((void *)0));
  case 2: 
  {
# 444
  log_raw((char const   *)"%-15s = yes", "RNDoverwrite");
  }
# 445
  break;
  case 3: 
  {
# 447
  log_raw((char const   *)"%-15s = yes|no overwrite seed datafiles with new random data",
          "RNDoverwrite");
  }
# 449
  break;
  }
# 453
  switch (cmd) {
  case 0: 
  {
# 455
  local_options.servname = stralloc("stunnel");
  }
# 460
  break;
  case 1: 
  {
# 462
  tmp___25 = strcasecmp((char const   *)opt, (char const   *)"service");
  }
# 462
  if (tmp___25) {
# 463
    break;
  }
  {
# 464
  local_options.servname = stralloc(arg);
  }
# 476
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 481
  break;
  case 3: 
  {
# 483
  log_raw((char const   *)"%-15s = service name", "service");
  }
# 484
  break;
  }
# 488
  switch (cmd) {
  case 0: 
  {
# 490
  options.session_timeout = 300L;
  }
# 491
  break;
  case 1: 
  {
# 493
  tmp___26 = strcasecmp((char const   *)opt, (char const   *)"session");
  }
# 493
  if (tmp___26) {
# 494
    break;
  }
  {
# 495
  tmp___27 = atoi((char const   *)arg);
  }
# 495
  if (tmp___27 > 0) {
    {
# 496
    options.session_timeout = (long )atoi((char const   *)arg);
    }
  } else {
# 498
    return ("Illegal session timeout");
  }
# 499
  return ((char *)((void *)0));
  case 2: 
  {
# 501
  log_raw((char const   *)"%-15s = %ld seconds", "session",
          options.session_timeout);
  }
# 502
  break;
  case 3: 
  {
# 504
  log_raw((char const   *)"%-15s = session cache timeout (in seconds)",
          "session");
  }
# 505
  break;
  }
# 510
  switch (cmd) {
  case 0: 
  {
# 512
  options.setgid_group = (char *)((void *)0);
  }
# 513
  break;
  case 1: 
  {
# 515
  tmp___28 = strcasecmp((char const   *)opt, (char const   *)"setgid");
  }
# 515
  if (tmp___28) {
# 516
    break;
  }
  {
# 517
  options.setgid_group = stralloc(arg);
  }
# 518
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 520
  break;
  case 3: 
  {
# 522
  log_raw((char const   *)"%-15s = groupname for setgid()", "setgid");
  }
# 523
  break;
  }
# 529
  switch (cmd) {
  case 0: 
  {
# 531
  options.setuid_user = (char *)((void *)0);
  }
# 532
  break;
  case 1: 
  {
# 534
  tmp___29 = strcasecmp((char const   *)opt, (char const   *)"setuid");
  }
# 534
  if (tmp___29) {
# 535
    break;
  }
  {
# 536
  options.setuid_user = stralloc(arg);
  }
# 537
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 539
  break;
  case 3: 
  {
# 541
  log_raw((char const   *)"%-15s = username for setuid()", "setuid");
  }
# 542
  break;
  }
# 547
  switch (cmd) {
  case 0: 
  {

  }
# 549
  break;
  case 1: 
  {
# 551
  tmp___30 = strcasecmp((char const   *)opt, (char const   *)"socket");
  }
# 551
  if (tmp___30) {
# 552
    break;
  }
  {
# 553
  tmp___31 = parse_socket_option(arg);
  }
# 553
  if (! tmp___31) {
# 554
    return ("Illegal socket option");
  }
# 555
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 557
  break;
  case 3: 
  {
# 559
  log_raw((char const   *)"%-15s = a|l|r:option=value[:value]", "socket");
# 560
  log_raw((char const   *)"%18sset an option on accept/local/remote socket", "");
  }
# 561
  break;
  }
# 590
  switch (cmd) {
  case 0: 
  {
# 592
  options.verify_level = -1;
# 593
  options.verify_use_only_my = 0;
  }
# 594
  break;
  case 1: 
  {
# 596
  tmp___32 = strcasecmp((char const   *)opt, (char const   *)"verify");
  }
# 596
  if (tmp___32) {
# 597
    break;
  }
  {
# 598
  options.verify_level = 0;
# 599
  tmp___33 = atoi((char const   *)arg);
  }
# 599
  switch (tmp___33) {
  case 3: 
  {
# 601
  options.verify_use_only_my = 1;
  }
  case 2: 
  {
# 603
  options.verify_level = options.verify_level | 2;
  }
  case 1: 
  {
# 605
  options.verify_level = options.verify_level | 1;
  }
  case 0: 
  {

  }
# 607
  return ((char *)((void *)0));
  default: 
  {

  }
# 609
  return ("Bad verify level");
  }
  case 2: 
  {
# 612
  log_raw((char const   *)"%-15s = none", "verify");
  }
# 613
  break;
  case 3: 
  {
# 615
  log_raw((char const   *)"%-15s = level of peer certificate verification",
          "verify");
# 616
  log_raw((char const   *)"%18slevel 1 - verify peer certificate if present", "");
# 617
  log_raw((char const   *)"%18slevel 2 - require valid peer certificate always",
          "");
# 618
  log_raw((char const   *)"%18slevel 3 - verify peer with locally installed certificate",
          "");
  }
# 620
  break;
  }
# 623
  if (cmd == 1) {
# 624
    return (option_not_found);
  }
# 625
  return ((char *)((void *)0));
}
}
# 628
static char *service_options(CMD cmd , LOCAL_OPTIONS *section , char *opt ,
                             char *arg ) 
{ int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___9 ;
  int tmp___10 ;
  int tmp___11 ;
  int tmp___12 ;
  int tmp___13 ;
  int tmp___14 ;
  int tmp___15 ;
  int tmp___16 ;
  int tmp___17 ;
  int tmp___18 ;
  int tmp___19 ;
  int tmp___20 ;
  int tmp___21 ;
  int tmp___22 ;
  int tmp___23 ;
  int tmp___24 ;

  {
# 631
  if (cmd == 2) {
    {
# 632
    log_raw((char const   *)" ");
# 633
    log_raw((char const   *)"Service-level options");
    }
  } else {
# 631
    if (cmd == 3) {
      {
# 632
      log_raw((char const   *)" ");
# 633
      log_raw((char const   *)"Service-level options");
      }
    }
  }
# 637
  switch (cmd) {
  case 0: 
  {
# 639
  section->option.accept = 0U;
  }
# 640
  break;
  case 1: 
  {
# 642
  tmp = strcasecmp((char const   *)opt, (char const   *)"accept");
  }
# 642
  if (tmp) {
# 643
    break;
  }
  {
# 644
  section->option.accept = 1U;
# 645
  tmp___0 = name2nums(arg, "0.0.0.0", & section->localnames,
                      & section->localport);
  }
# 645
  if (! tmp___0) {
    {
# 647
    exit(2);
    }
  }
# 648
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 650
  break;
  case 3: 
  {
# 652
  log_raw((char const   *)"%-15s = [host:]port accept connections on specified host:port",
          "accept");
  }
# 654
  break;
  }
# 658
  switch (cmd) {
  case 0: 
  {
# 660
  section->option.remote = 0U;
# 661
  section->remote_address = (char *)((void *)0);
# 662
  section->remotenames = (u32 *)((void *)0);
# 663
  section->remoteport = (unsigned short )0;
  }
# 664
  break;
  case 1: 
  {
# 666
  tmp___1 = strcasecmp((char const   *)opt, (char const   *)"connect");
  }
# 666
  if (tmp___1) {
# 667
    break;
  }
  {
# 668
  section->option.remote = 1U;
# 669
  section->remote_address = stralloc(arg);
  }
# 670
  if (! section->option.delayed_lookup) {
    {
# 670
    tmp___2 = name2nums(arg, "127.0.0.1", & section->remotenames,
                        & section->remoteport);
    }
# 670
    if (! tmp___2) {
      {
# 672
      log_raw((char const   *)"Cannot resolve \'%s\' - delaying DNS lookup", arg);
# 673
      section->option.delayed_lookup = 1U;
      }
    }
  }
# 675
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 677
  break;
  case 3: 
  {
# 679
  log_raw((char const   *)"%-15s = [host:]port connect remote host:port",
          "connect");
  }
# 681
  break;
  }
# 685
  switch (cmd) {
  case 0: 
  {
# 687
  section->option.delayed_lookup = 0U;
  }
# 688
  break;
  case 1: 
  {
# 690
  tmp___3 = strcasecmp((char const   *)opt, (char const   *)"delay");
  }
# 690
  if (tmp___3) {
# 691
    break;
  }
  {
# 692
  tmp___5 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 692
  if (tmp___5) {
    {
# 694
    tmp___4 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 694
    if (tmp___4) {
# 697
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 695
      section->option.delayed_lookup = 0U;
      }
    }
  } else {
    {
# 693
    section->option.delayed_lookup = 1U;
    }
  }
# 698
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 700
  break;
  case 3: 
  {
# 702
  log_raw((char const   *)"%-15s = yes|no delay DNS lookup for \'connect\' option",
          "delay");
  }
# 704
  break;
  }
# 709
  switch (cmd) {
  case 0: 
  {
# 711
  section->option.program = 0U;
# 712
  section->execname = (char *)((void *)0);
  }
# 713
  break;
  case 1: 
  {
# 715
  tmp___6 = strcasecmp((char const   *)opt, (char const   *)"exec");
  }
# 715
  if (tmp___6) {
# 716
    break;
  }
  {
# 717
  section->option.program = 1U;
# 718
  section->execname = stralloc(arg);
  }
# 719
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 721
  break;
  case 3: 
  {
# 723
  log_raw((char const   *)"%-15s = file execute local inetd-type program",
          "exec");
  }
# 725
  break;
  }
# 731
  switch (cmd) {
  case 0: 
  {
# 733
  section->execargs = (char **)((void *)0);
  }
# 734
  break;
  case 1: 
  {
# 736
  tmp___7 = strcasecmp((char const   *)opt, (char const   *)"execargs");
  }
# 736
  if (tmp___7) {
# 737
    break;
  }
  {
# 738
  section->execargs = argalloc(arg);
  }
# 739
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 741
  break;
  case 3: 
  {
# 743
  log_raw((char const   *)"%-15s = arguments for \'exec\' (including $0)",
          "execargs");
  }
# 745
  break;
  }
# 750
  switch (cmd) {
  case 0: 
  {
# 752
  section->username = (char *)((void *)0);
  }
# 753
  break;
  case 1: 
  {
# 755
  tmp___8 = strcasecmp((char const   *)opt, (char const   *)"ident");
  }
# 755
  if (tmp___8) {
# 756
    break;
  }
  {
# 757
  section->username = stralloc(arg);
  }
# 758
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 760
  break;
  case 3: 
  {
# 762
  log_raw((char const   *)"%-15s = username for IDENT (RFC 1413) checking",
          "ident");
  }
# 763
  break;
  }
# 767
  switch (cmd) {
  case 0: 
  {
# 769
  section->local_ip = (u32 *)((void *)0);
  }
# 770
  break;
  case 1: 
  {
# 772
  tmp___9 = strcasecmp((char const   *)opt, (char const   *)"local");
  }
# 772
  if (tmp___9) {
# 773
    break;
  }
  {
# 774
  tmp___10 = host2nums(arg, & section->local_ip);
  }
# 774
  if (! tmp___10) {
    {
# 775
    exit(2);
    }
  }
# 776
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 778
  break;
  case 3: 
  {
# 780
  log_raw((char const   *)"%-15s = IP address to be used as source for remote connections",
          "local");
  }
# 782
  break;
  }
# 786
  switch (cmd) {
  case 0: 
  {
# 788
  section->protocol = (char *)((void *)0);
  }
# 789
  break;
  case 1: 
  {
# 791
  tmp___11 = strcasecmp((char const   *)opt, (char const   *)"protocol");
  }
# 791
  if (tmp___11) {
# 792
    break;
  }
  {
# 793
  section->protocol = stralloc(arg);
  }
# 794
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 796
  break;
  case 3: 
  {
# 798
  log_raw((char const   *)"%-15s = protocol to negotiate before SSL initialization",
          "protocol");
# 800
  log_raw((char const   *)"%18scurrently supported: cifs, nntp, pop3, smtp", "");
  }
# 801
  break;
  }
# 806
  switch (cmd) {
  case 0: 
  {
# 808
  section->option.pty = 0U;
  }
# 809
  break;
  case 1: 
  {
# 811
  tmp___12 = strcasecmp((char const   *)opt, (char const   *)"pty");
  }
# 811
  if (tmp___12) {
# 812
    break;
  }
  {
# 813
  tmp___14 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 813
  if (tmp___14) {
    {
# 815
    tmp___13 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 815
    if (tmp___13) {
# 818
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 816
      section->option.pty = 0U;
      }
    }
  } else {
    {
# 814
    section->option.pty = 1U;
    }
  }
# 819
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 821
  break;
  case 3: 
  {
# 823
  log_raw((char const   *)"%-15s = yes|no allocate pseudo terminal for \'exec\' option",
          "pty");
  }
# 825
  break;
  }
# 830
  switch (cmd) {
  case 0: 
  {
# 832
  section->timeout_busy = 300;
  }
# 833
  break;
  case 1: 
  {
# 835
  tmp___15 = strcasecmp((char const   *)opt, (char const   *)"TIMEOUTbusy");
  }
# 835
  if (tmp___15) {
# 836
    break;
  }
  {
# 837
  tmp___16 = atoi((char const   *)arg);
  }
# 837
  if (tmp___16 > 0) {
    {
# 838
    section->timeout_busy = atoi((char const   *)arg);
    }
  } else {
# 840
    return ("Illegal busy timeout");
  }
# 841
  return ((char *)((void *)0));
  case 2: 
  {
# 843
  log_raw((char const   *)"%-15s = %d seconds", "TIMEOUTbusy",
          section->timeout_busy);
  }
# 844
  break;
  case 3: 
  {
# 846
  log_raw((char const   *)"%-15s = seconds to wait for expected data",
          "TIMEOUTbusy");
  }
# 847
  break;
  }
# 851
  switch (cmd) {
  case 0: 
  {
# 853
  section->timeout_close = 60;
  }
# 854
  break;
  case 1: 
  {
# 856
  tmp___17 = strcasecmp((char const   *)opt, (char const   *)"TIMEOUTclose");
  }
# 856
  if (tmp___17) {
# 857
    break;
  }
  {
# 858
  tmp___18 = atoi((char const   *)arg);
  }
# 858
  if (tmp___18 > 0) {
    {
# 859
    section->timeout_close = atoi((char const   *)arg);
    }
  } else {
    {
# 858
    tmp___19 = strcmp((char const   *)arg, (char const   *)"0");
    }
# 858
    if (tmp___19) {
# 861
      return ("Illegal close timeout");
    } else {
      {
# 859
      section->timeout_close = atoi((char const   *)arg);
      }
    }
  }
# 862
  return ((char *)((void *)0));
  case 2: 
  {
# 864
  log_raw((char const   *)"%-15s = %d seconds", "TIMEOUTclose",
          section->timeout_close);
  }
# 865
  break;
  case 3: 
  {
# 867
  log_raw((char const   *)"%-15s = seconds to wait for close_notify (set to 0 for buggy MSIE)",
          "TIMEOUTclose");
  }
# 869
  break;
  }
# 873
  switch (cmd) {
  case 0: 
  {
# 875
  section->timeout_idle = 43200;
  }
# 876
  break;
  case 1: 
  {
# 878
  tmp___20 = strcasecmp((char const   *)opt, (char const   *)"TIMEOUTidle");
  }
# 878
  if (tmp___20) {
# 879
    break;
  }
  {
# 880
  tmp___21 = atoi((char const   *)arg);
  }
# 880
  if (tmp___21 > 0) {
    {
# 881
    section->timeout_idle = atoi((char const   *)arg);
    }
  } else {
# 883
    return ("Illegal idle timeout");
  }
# 884
  return ((char *)((void *)0));
  case 2: 
  {
# 886
  log_raw((char const   *)"%-15s = %d seconds", "TIMEOUTidle",
          section->timeout_idle);
  }
# 887
  break;
  case 3: 
  {
# 889
  log_raw((char const   *)"%-15s = seconds to keep idle connection",
          "TIMEOUTidle");
  }
# 890
  break;
  }
# 895
  switch (cmd) {
  case 0: 
  {
# 897
  section->option.transparent = 0U;
  }
# 898
  break;
  case 1: 
  {
# 900
  tmp___22 = strcasecmp((char const   *)opt, (char const   *)"transparent");
  }
# 900
  if (tmp___22) {
# 901
    break;
  }
  {
# 902
  tmp___24 = strcasecmp((char const   *)arg, (char const   *)"yes");
  }
# 902
  if (tmp___24) {
    {
# 904
    tmp___23 = strcasecmp((char const   *)arg, (char const   *)"no");
    }
# 904
    if (tmp___23) {
# 907
      return ("argument should be either \'yes\' or \'no\'");
    } else {
      {
# 905
      section->option.transparent = 0U;
      }
    }
  } else {
    {
# 903
    section->option.transparent = 1U;
    }
  }
# 908
  return ((char *)((void *)0));
  case 2: 
  {

  }
# 910
  break;
  case 3: 
  {
# 912
  log_raw((char const   *)"%-15s = yes|no transparent proxy mode", "transparent");
  }
# 914
  break;
  }
# 918
  if (cmd == 1) {
# 919
    return (option_not_found);
  }
# 920
  return ((char *)((void *)0));
}
}
# 923
static void syntax(char *confname ) 
{ 

  {
  {
# 924
  log_raw((char const   *)" ");
# 925
  log_raw((char const   *)"Syntax:");
# 930
  log_raw((char const   *)"stunnel [filename] | -fd [n] | -help | -version | -sockets");
# 932
  log_raw((char const   *)"    filename    - use specified config file instead of %s",
          confname);
# 934
  log_raw((char const   *)"    -fd n       - read the config file from specified file descriptor");
# 935
  log_raw((char const   *)"    -help       - get config file help");
# 936
  log_raw((char const   *)"    -version    - display version and defaults");
# 937
  log_raw((char const   *)"    -sockets    - display default socket options");
# 942
  exit(1);
  }
# 942
  return;
}
}
# 945
void parse_config(char *name , char *parameter ) 
{ char *default_config_file ;
  FILE *fp ;
  char line[1024] ;
  char *arg ;
  char *opt ;
  char *errstr ;
  char *filename ;
  int line_number ;
  int i ;
  LOCAL_OPTIONS *section ;
  LOCAL_OPTIONS *new_section ;
  int tmp ;
  char *tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  size_t tmp___5 ;
  int tmp___6 ;
  size_t tmp___7 ;
  size_t tmp___8 ;
  char *tmp___9 ;
  size_t tmp___10 ;
  int tmp___11 ;
  int tmp___12 ;
  char *tmp___13 ;

  {
  {
# 947
  default_config_file = "/usr/local/etc/stunnel/stunnel.conf";
# 956
  memset((void *)(& options), 0, sizeof(GLOBAL_OPTIONS ));
# 958
  memset((void *)(& local_options), 0, sizeof(LOCAL_OPTIONS ));
# 959
  local_options.next = (struct local_options *)((void *)0);
# 960
  section = & local_options;
# 962
  global_options(0, (char *)((void *)0), (char *)((void *)0));
# 963
  service_options(0, section, (char *)((void *)0), (char *)((void *)0));
  }
# 964
  if (! name) {
    {
# 965
    name = default_config_file;
    }
  }
  {
# 966
  tmp = strcasecmp((char const   *)name, (char const   *)"-help");
  }
# 966
  if (! tmp) {
    {
# 967
    global_options(3, (char *)((void *)0), (char *)((void *)0));
# 968
    service_options(3, section, (char *)((void *)0), (char *)((void *)0));
# 969
    exit(1);
    }
  }
  {
# 971
  tmp___1 = strcasecmp((char const   *)name, (char const   *)"-version");
  }
# 971
  if (! tmp___1) {
    {
# 972
    tmp___0 = stunnel_info();
# 972
    log_raw((char const   *)"%s", tmp___0);
# 973
    log_raw((char const   *)" ");
# 974
    global_options(2, (char *)((void *)0), (char *)((void *)0));
# 975
    service_options(2, section, (char *)((void *)0), (char *)((void *)0));
# 976
    exit(1);
    }
  }
  {
# 978
  tmp___2 = strcasecmp((char const   *)name, (char const   *)"-sockets");
  }
# 978
  if (! tmp___2) {
    {
# 979
    print_socket_options();
# 980
    exit(1);
    }
  }
  {
# 983
  tmp___3 = strcasecmp((char const   *)name, (char const   *)"-fd");
  }
# 983
  if (tmp___3) {
    {

    {
# 11 "spec.work"
    fp->__BLAST_FLAG = 1;
    {

    }
    }
# 1004 "options.c"
    fp = fopen((char const   */* __restrict  */)((char const   *)name),
               (char const   */* __restrict  */)((char const   *)"r"));
    }
# 1005
    if (! fp) {
      {
# 1010
      ioerror(name);
# 1012
      syntax(default_config_file);
      }
    }
    {
# 1014
    filename = name;
    }
  } else {
# 984
    if (! parameter) {
      {
# 985
      log_raw((char const   *)"No file descriptor specified");
# 986
      syntax(default_config_file);
      }
    }
    {
# 988
    arg = parameter;
# 988
    i = 0;
    }
# 988
    while ((*arg)) {
# 989
      if ((int )(*arg) < 48) {
        {
# 990
        log_raw((char const   *)"Invalid file descriptor %s", parameter);
# 991
        syntax(default_config_file);
        }
      } else {
# 989
        if ((int )(*arg) > 57) {
          {
# 990
          log_raw((char const   *)"Invalid file descriptor %s", parameter);
# 991
          syntax(default_config_file);
          }
        }
      }
      {
# 993
      i = (10 * i + (int )(*arg)) - 48;
# 988
      arg ++;
      }
    }
    {

    {
# 19 "spec.work"
    fp->__BLAST_FLAG = 1;
    {

    }
    }
# 995 "options.c"
    fp = fdopen(i, (char const   *)"r");
    }
# 996
    if (! fp) {
      {
# 997
      log_raw((char const   *)"Invalid file descriptor %s", parameter);
# 998
      syntax(default_config_file);
      }
    }
    {
# 1000
    filename = "descriptor";
    }
  }
  {
# 1016
  line_number = 0;
  }
# 1017
  while (1) {
    {

    {
# 26 "spec.work"
    if (! (fp->__BLAST_FLAG == 1)) {
# 28
      __error__();
    }
    {

    }
    }
# 1017 "options.c"
    tmp___13 = fgets((char */* __restrict  */)(line), 1024,
                     (FILE */* __restrict  */)fp);
    }
# 1017
    if (! tmp___13) {
# 1017
      break;
    }
    {
# 1018
    line_number ++;
# 1019
    opt = line;
    }
# 1020
    while (1) {
      {
# 1020
      tmp___4 = _get__ctype_b((int )(*opt));
      }
# 1020
      if (! (tmp___4 & 8192)) {
# 1020
        break;
      }
      {
# 1021
      opt ++;
      }
    }
    {
# 1022
    tmp___5 = strlen((char const   *)opt);
# 1022
    i = (int )(tmp___5 - 1U);
    }
# 1022
    while (1) {
# 1022
      if (i >= 0) {
        {
# 1022
        tmp___6 = _get__ctype_b((int )(*(opt + i)));
        }
# 1022
        if (! (tmp___6 & 8192)) {
# 1022
          break;
        }
      } else {
# 1022
        break;
      }
      {
# 1023
      (*(opt + i)) = '\0';
# 1022
      i --;
      }
    }
# 1024
    if ((int )(*(opt + 0)) == 0) {
# 1025
      continue;
    } else {
# 1024
      if ((int )(*(opt + 0)) == 35) {
# 1025
        continue;
      }
    }
# 1026
    if ((int )(*(opt + 0)) == 91) {
      {
# 1026
      tmp___8 = strlen((char const   *)opt);
      }
# 1026
      if ((int )(*(opt + (tmp___8 - 1U))) == 93) {
        {
# 1027
        errstr = section_validate(section);
        }
# 1028
        if (errstr) {
          {
# 1029
          log_raw((char const   *)"file %s line %d: %s", filename, line_number,
                  errstr);
# 1030
          exit(1);
          }
        }
        {
# 1032
        opt ++;
# 1033
        tmp___7 = strlen((char const   *)opt);
# 1033
        (*(opt + (tmp___7 - 1U))) = '\0';
# 1034
        new_section = (LOCAL_OPTIONS *)calloc(1U, sizeof(LOCAL_OPTIONS ));
        }
# 1035
        if (! new_section) {
          {
# 1036
          log_raw((char const   *)"Fatal memory allocation error");
# 1037
          exit(2);
          }
        }
        {
# 1039
        memcpy((void */* __restrict  */)((void *)new_section),
               (void const   */* __restrict  */)((void const   *)(& local_options)),
               sizeof(LOCAL_OPTIONS ));
# 1040
        new_section->servname = stralloc(opt);
# 1041
        new_section->next = (struct local_options *)((void *)0);
# 1042
        section->next = new_section;
# 1043
        section = new_section;
        }
# 1044
        continue;
      }
    }
    {
# 1046
    arg = strchr((char const   *)(line), (int )'=');
    }
# 1047
    if (! arg) {
      {
# 1048
      log_raw((char const   *)"file %s line %d: No \'=\' found", filename,
              line_number);
# 1049
      exit(1);
      }
    }
    {
# 1051
    tmp___9 = arg;
# 1051
    arg ++;
# 1051
    (*tmp___9) = '\0';
# 1052
    tmp___10 = strlen((char const   *)opt);
# 1052
    i = (int )(tmp___10 - 1U);
    }
# 1052
    while (1) {
# 1052
      if (i >= 0) {
        {
# 1052
        tmp___11 = _get__ctype_b((int )(*(opt + i)));
        }
# 1052
        if (! (tmp___11 & 8192)) {
# 1052
          break;
        }
      } else {
# 1052
        break;
      }
      {
# 1053
      (*(opt + i)) = '\0';
# 1052
      i --;
      }
    }
# 1054
    while (1) {
      {
# 1054
      tmp___12 = _get__ctype_b((int )(*arg));
      }
# 1054
      if (! (tmp___12 & 8192)) {
# 1054
        break;
      }
      {
# 1055
      arg ++;
      }
    }
    {
# 1056
    errstr = service_options(1, section, opt, arg);
    }
# 1057
    if ((unsigned int )section == (unsigned int )(& local_options)) {
# 1057
      if ((unsigned int )errstr == (unsigned int )option_not_found) {
        {
# 1058
        errstr = global_options(1, opt, arg);
        }
      }
    }
# 1059
    if (errstr) {
      {
# 1060
      log_raw((char const   *)"file %s line %d: %s", filename, line_number,
              errstr);
# 1061
      exit(1);
      }
    }
  }
  {
# 1064
  errstr = section_validate(section);
  }
# 1065
  if (errstr) {
    {
# 1066
    log_raw((char const   *)"file %s line %d: %s", filename, line_number, errstr);
# 1067
    exit(1);
    }
  }
  {

  {
# 50 "spec.work"
  if (fp->__BLAST_FLAG == 1) {
# 51
    fp->__BLAST_FLAG = 0;
  } else {
# 52
    __error__();
  }
  {

  }
  }
# 1069 "options.c"
  fclose(fp);
  }
# 1070
  if (! local_options.next) {
# 1070
    if (section->option.accept) {
      {
# 1071
      log_raw((char const   *)"accept option is not allowed in inetd mode");
# 1072
      log_raw((char const   *)"remove accept option or define a [section]");
# 1073
      exit(1);
      }
    }
  }
# 1075
  if (! options.option.client) {
    {
# 1076
    options.option.cert = 1U;
    }
  }
# 1077
  if (! options.option.foreground) {
    {
# 1078
    options.option.syslog = 1U;
    }
  }
# 1079
  return;
}
}
# 1081
static char *section_validate(LOCAL_OPTIONS *section ) 
{ 

  {
# 1082
  if ((unsigned int )section == (unsigned int )(& local_options)) {
# 1083
    return ((char *)((void *)0));
  }
# 1087
  if ((section->option.accept + section->option.program) +
      section->option.remote != 2U) {
# 1091
    return ("Each service section must define exactly two endpoints");
  }
# 1092
  return ((char *)((void *)0));
}
}
# 1095
static char *stralloc(char *str ) 
{ char *retval___0 ;
  size_t tmp ;

  {
  {
# 1098
  tmp = strlen((char const   *)str);
# 1098
  retval___0 = (char *)calloc(tmp + 1U, 1U);
  }
# 1099
  if (! retval___0) {
    {
# 1100
    log_raw((char const   *)"Fatal memory allocation error");
# 1101
    exit(2);
    }
  }
  {
# 1103
  strcpy((char */* __restrict  */)retval___0,
         (char const   */* __restrict  */)((char const   *)str));
  }
# 1104
  return (retval___0);
}
}
# 1108
static char **argalloc(char *str ) 
{ int max_arg ;
  int i ;
  char *ptr ;
  char **retval___0 ;
  size_t tmp ;
  int tmp___0 ;
  int tmp___1 ;
  char *tmp___2 ;
  int tmp___3 ;

  {
  {
# 1112
  tmp = strlen((char const   *)str);
# 1112
  max_arg = (int )(tmp / 2U + 1U);
# 1113
  ptr = stralloc(str);
# 1114
  retval___0 = (char **)calloc((unsigned int )(max_arg + 1), sizeof(char *));
  }
# 1115
  if (! retval___0) {
    {
# 1116
    log_raw((char const   *)"Fatal memory allocation error");
# 1117
    exit(2);
    }
  }
  {
# 1119
  i = 0;
  }
# 1120
  while (1) {
# 1120
    if ((*ptr)) {
# 1120
      if (! (i < max_arg)) {
# 1120
        break;
      }
    } else {
# 1120
      break;
    }
    {
# 1121
    tmp___0 = i;
# 1121
    i ++;
# 1121
    (*(retval___0 + tmp___0)) = ptr;
    }
# 1122
    while (1) {
# 1122
      if ((*ptr)) {
        {
# 1122
        tmp___1 = _get__ctype_b((int )(*ptr));
        }
# 1122
        if (tmp___1 & 8192) {
# 1122
          break;
        }
      } else {
# 1122
        break;
      }
      {
# 1123
      ptr ++;
      }
    }
# 1124
    while (1) {
# 1124
      if ((*ptr)) {
        {
# 1124
        tmp___3 = _get__ctype_b((int )(*ptr));
        }
# 1124
        if (! (tmp___3 & 8192)) {
# 1124
          break;
        }
      } else {
# 1124
        break;
      }
      {
# 1125
      tmp___2 = ptr;
# 1125
      ptr ++;
# 1125
      (*tmp___2) = '\0';
      }
    }
  }
  {
# 1127
  (*(retval___0 + i)) = (char *)((void *)0);
  }
# 1128
  return (retval___0);
}
}
# 1132
int name2nums(char *name , char *default_host , u32 **names , u_short *port ) 
{ char tmp[1024] ;
  char *host_str ;
  char *port_str ;
  struct servent *p ;
  char *tmp___0 ;
  register unsigned short __v ;
  register unsigned short __x ;
  u_short tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;

  {
  {
# 1136
  tmp[1023] = '\0';
# 1136
  strncpy((char */* __restrict  */)(tmp),
          (char const   */* __restrict  */)((char const   *)name), 1023U);
# 1137
  port_str = strrchr((char const   *)(tmp), (int )':');
  }
# 1138
  if (port_str) {
    {
# 1139
    host_str = tmp;
# 1140
    tmp___0 = port_str;
# 1140
    port_str ++;
# 1140
    (*tmp___0) = '\0';
    }
  } else {
    {
# 1142
    host_str = default_host;
# 1143
    port_str = tmp;
    }
  }
  {
# 1145
  tmp___1 = (u_short )atoi((char const   *)port_str);
# 1145
  __x = tmp___1;
# 1145
  tmp___2 = ((int )__x);
  }
# 1145
  if (tmp___2) {
    {
# 1145
    __v = (unsigned short )((((int )__x >> 8) & 255) | (((int )__x & 255) << 8));
    }
  } else {
    {
# 1145
    __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
    }
  }
  {
# 1145
  (*port) = __v;
  }
# 1146
  if (! (*port)) {
    {
# 1147
    p = getservbyname((char const   *)port_str, (char const   *)"tcp");
    }
# 1148
    if (! p) {
      {
# 1149
      log(3, (char const   *)"Invalid port: %s", port_str);
      }
# 1150
      return (0);
    }
    {
# 1152
    (*port) = (unsigned short )p->s_port;
    }
  }
  {
# 1154
  tmp___3 = host2nums(host_str, names);
  }
# 1154
  return (tmp___3);
}
}
# 1157
static int host2nums(char *hostname , u32 **hostlist ) 
{ struct hostent *h ;
  u32 ip ;
  int results ;
  int i ;
  char **tab ;

  {
  {
# 1164
  ip = inet_addr((char const   *)hostname);
  }
# 1165
  if (ip + 1U) {
    {
# 1166
    (*hostlist) = (u32 *)calloc(2U, sizeof(u32 ));
    }
# 1167
    if (! (*hostlist)) {
      {
# 1168
      log(3, (char const   *)"Memory allocation error");
      }
# 1169
      return (0);
    }
    {
# 1171
    (*((*hostlist) + 0)) = ip;
# 1172
    (*((*hostlist) + 1)) = 4294967295U;
    }
# 1173
    return (1);
  }
  {
# 1177
  h = gethostbyname((char const   *)hostname);
  }
# 1177
  if (! h) {
    {
# 1178
    log(3, (char const   *)"Failed to resolve hostname \'%s\'", hostname);
    }
# 1179
    return (0);
  }
  {
# 1181
  results = 0;
# 1181
  tab = h->h_addr_list;
  }
# 1181
  while ((*tab)) {
    {
# 1182
    results ++;
# 1181
    tab ++;
    }
  }
  {
# 1183
  (*hostlist) = (u32 *)calloc((unsigned int )(results + 1), sizeof(u32 ));
  }
# 1184
  if (! (*hostlist)) {
    {
# 1185
    log(3, (char const   *)"Memory allocation error");
    }
# 1186
    return (0);
  }
  {
# 1188
  i = 0;
  }
# 1188
  while (i < results) {
    {
# 1189
    (*((*hostlist) + i)) = (*((u32 *)(*(h->h_addr_list + i))));
# 1188
    i ++;
    }
  }
  {
# 1190
  (*((*hostlist) + results)) = 4294967295U;
# 1192
  endhostent();
  }
# 1194
  return (results);
}
}
# 1204
static int parse_debug_level(char *arg ) 
{ char arg_copy[1024] ;
  char *string ;
  facilitylevel *fl ;
  facilitylevel facilities[21] ;
  facilitylevel levels[9] ;
  int tmp ;
  char *tmp___0 ;
  size_t tmp___1 ;
  int tmp___2 ;

  {
  {
# 1211
  facilities[0].name = "auth";
# 1211
  facilities[0].value = 32;
# 1211
  facilities[1].name = "cron";
# 1211
  facilities[1].value = 72;
# 1211
  facilities[2].name = "daemon";
# 1211
  facilities[2].value = 24;
# 1211
  facilities[3].name = "kern";
# 1211
  facilities[3].value = 0;
# 1211
  facilities[4].name = "lpr";
# 1211
  facilities[4].value = 48;
# 1211
  facilities[5].name = "mail";
# 1211
  facilities[5].value = 16;
# 1211
  facilities[6].name = "news";
# 1211
  facilities[6].value = 56;
# 1211
  facilities[7].name = "syslog";
# 1211
  facilities[7].value = 40;
# 1211
  facilities[8].name = "user";
# 1211
  facilities[8].value = 8;
# 1211
  facilities[9].name = "uucp";
# 1211
  facilities[9].value = 64;
# 1211
  facilities[10].name = "local0";
# 1211
  facilities[10].value = 128;
# 1211
  facilities[11].name = "local1";
# 1211
  facilities[11].value = 136;
# 1211
  facilities[12].name = "local2";
# 1211
  facilities[12].value = 144;
# 1211
  facilities[13].name = "local3";
# 1211
  facilities[13].value = 152;
# 1211
  facilities[14].name = "local4";
# 1211
  facilities[14].value = 160;
# 1211
  facilities[15].name = "local5";
# 1211
  facilities[15].value = 168;
# 1211
  facilities[16].name = "local6";
# 1211
  facilities[16].value = 176;
# 1211
  facilities[17].name = "local7";
# 1211
  facilities[17].value = 184;
# 1211
  facilities[18].name = "authpriv";
# 1211
  facilities[18].value = 80;
# 1211
  facilities[19].name = "ftp";
# 1211
  facilities[19].value = 88;
# 1211
  facilities[20].name = (char *)((void *)0);
# 1211
  facilities[20].value = 0;
# 1233
  levels[0].name = "emerg";
# 1233
  levels[0].value = 0;
# 1233
  levels[1].name = "alert";
# 1233
  levels[1].value = 1;
# 1233
  levels[2].name = "crit";
# 1233
  levels[2].value = 2;
# 1233
  levels[3].name = "err";
# 1233
  levels[3].value = 3;
# 1233
  levels[4].name = "warning";
# 1233
  levels[4].value = 4;
# 1233
  levels[5].name = "notice";
# 1233
  levels[5].value = 5;
# 1233
  levels[6].name = "info";
# 1233
  levels[6].value = 6;
# 1233
  levels[7].name = "debug";
# 1233
  levels[7].value = 7;
# 1233
  levels[8].name = (char *)((void *)0);
# 1233
  levels[8].value = -1;
# 1241
  arg_copy[1023] = '\0';
# 1241
  strncpy((char */* __restrict  */)(arg_copy),
          (char const   */* __restrict  */)((char const   *)arg), 1023U);
# 1242
  string = arg_copy;
# 1246
  tmp___0 = strchr((char const   *)string, (int )'.');
  }
# 1246
  if (tmp___0) {
    {
# 1247
    options.facility = -1;
# 1248
    string = strtok((char */* __restrict  */)(arg_copy),
                    (char const   */* __restrict  */)((char const   *)"."));
# 1250
    fl = facilities;
    }
# 1250
    while (fl->name) {
      {
# 1251
      tmp = strcasecmp((char const   *)fl->name, (char const   *)string);
      }
# 1251
      if (! tmp) {
        {
# 1252
        options.facility = fl->value;
        }
# 1253
        break;
      }
      {
# 1250
      fl ++;
      }
    }
# 1256
    if (options.facility == -1) {
# 1257
      return (0);
    }
    {
# 1258
    string = strtok((char */* __restrict  */)((char *)((void *)0)),
                    (char const   */* __restrict  */)((char const   *)"."));
    }
  }
# 1263
  if (string) {
    {
# 1263
    tmp___1 = strlen((char const   *)string);
    }
# 1263
    if (tmp___1 == 1U) {
# 1263
      if ((int )(*string) >= 48) {
# 1263
        if ((int )(*string) <= 55) {
          {
# 1264
          options.debug_level = (int )(*string) - 48;
          }
# 1265
          return (1);
        }
      }
    }
  }
  {
# 1267
  options.debug_level = 8;
# 1268
  fl = levels;
  }
# 1268
  while (fl->name) {
    {
# 1269
    tmp___2 = strcasecmp((char const   *)fl->name, (char const   *)string);
    }
# 1269
    if (! tmp___2) {
      {
# 1270
      options.debug_level = fl->value;
      }
# 1271
      break;
    }
    {
# 1268
    fl ++;
    }
  }
# 1274
  if (options.debug_level == 8) {
# 1275
    return (0);
  }
# 1276
  return (1);
}
}
# 1281
static int parse_ssl_option(char *arg ) 
{ struct __anonstruct_ssl_opts_76 ssl_opts[22] ;
  struct __anonstruct_ssl_opts_76 *option ;
  int tmp ;

  {
  {
# 1282
  ssl_opts[0].name = "MICROSOFT_SESS_ID_BUG";
# 1282
  ssl_opts[0].value = 1L;
# 1282
  ssl_opts[1].name = "NETSCAPE_CHALLENGE_BUG";
# 1282
  ssl_opts[1].value = 2L;
# 1282
  ssl_opts[2].name = "NETSCAPE_REUSE_CIPHER_CHANGE_BUG";
# 1282
  ssl_opts[2].value = 8L;
# 1282
  ssl_opts[3].name = "SSLREF2_REUSE_CERT_TYPE_BUG";
# 1282
  ssl_opts[3].value = 16L;
# 1282
  ssl_opts[4].name = "MICROSOFT_BIG_SSLV3_BUFFER";
# 1282
  ssl_opts[4].value = 32L;
# 1282
  ssl_opts[5].name = "MSIE_SSLV2_RSA_PADDING";
# 1282
  ssl_opts[5].value = 64L;
# 1282
  ssl_opts[6].name = "SSLEAY_080_CLIENT_DH_BUG";
# 1282
  ssl_opts[6].value = 128L;
# 1282
  ssl_opts[7].name = "TLS_D5_BUG";
# 1282
  ssl_opts[7].value = 256L;
# 1282
  ssl_opts[8].name = "TLS_BLOCK_PADDING_BUG";
# 1282
  ssl_opts[8].value = 512L;
# 1282
  ssl_opts[9].name = "TLS_ROLLBACK_BUG";
# 1282
  ssl_opts[9].value = 8388608L;
# 1282
  ssl_opts[10].name = "DONT_INSERT_EMPTY_FRAGMENTS";
# 1282
  ssl_opts[10].value = 2048L;
# 1282
  ssl_opts[11].name = "ALL";
# 1282
  ssl_opts[11].value = 4095L;
# 1282
  ssl_opts[12].name = "SINGLE_DH_USE";
# 1282
  ssl_opts[12].value = 1048576L;
# 1282
  ssl_opts[13].name = "EPHEMERAL_RSA";
# 1282
  ssl_opts[13].value = 2097152L;
# 1282
  ssl_opts[14].name = "NO_SSLv2";
# 1282
  ssl_opts[14].value = 16777216L;
# 1282
  ssl_opts[15].name = "NO_SSLv3";
# 1282
  ssl_opts[15].value = 33554432L;
# 1282
  ssl_opts[16].name = "NO_TLSv1";
# 1282
  ssl_opts[16].value = 67108864L;
# 1282
  ssl_opts[17].name = "PKCS1_CHECK_1";
# 1282
  ssl_opts[17].value = 134217728L;
# 1282
  ssl_opts[18].name = "PKCS1_CHECK_2";
# 1282
  ssl_opts[18].value = 268435456L;
# 1282
  ssl_opts[19].name = "NETSCAPE_CA_DN_BUG";
# 1282
  ssl_opts[19].value = 536870912L;
# 1282
  ssl_opts[20].name = "NETSCAPE_DEMO_CIPHER_CHANGE_BUG";
# 1282
  ssl_opts[20].value = 1073741824L;
# 1282
  ssl_opts[21].name = (char *)((void *)0);
# 1282
  ssl_opts[21].value = 0L;
# 1317
  option = ssl_opts;
  }
# 1317
  while (option->name) {
    {
# 1318
    tmp = strcasecmp((char const   *)option->name, (char const   *)arg);
    }
# 1318
    if (! tmp) {
      {
# 1319
      options.ssl_options = options.ssl_options | option->value;
      }
# 1320
      return (1);
    }
    {
# 1317
    option ++;
    }
  }
# 1322
  return (0);
}
}
# 1327
static int on   = 1;
# 1332
SOCK_OPT sock_opts[17]   = 
# 1332
  {{"SO_DEBUG", 1, 1, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_DONTROUTE", 1, 5, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_KEEPALIVE", 1, 9, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_LINGER", 1, 13, (VAL_TYPE )3,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_OOBINLINE", 1, 10, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_RCVBUF", 1, 8, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_SNDBUF", 1, 7, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_RCVLOWAT", 1, 18, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_SNDLOWAT", 1, 19, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_RCVTIMEO", 1, 20, (VAL_TYPE )4,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_SNDTIMEO", 1, 21, (VAL_TYPE )4,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"SO_REUSEADDR", 1, 2, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)(& on)),
     (OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0)}},
   {"SO_BINDTODEVICE", 1, 25, (VAL_TYPE )5,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"IP_TOS", 0, 1, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"IP_TTL", 0, 2, (VAL_TYPE )2,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {"TCP_NODELAY", 6, 1, (VAL_TYPE )1,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}},
   {(char *)((void *)0), 0, 0, (VAL_TYPE )0,
    {(OPT_UNION *)((void *)0), (OPT_UNION *)((void *)0),
     (OPT_UNION *)((void *)0)}}};
# 1369
static int print_socket_options(void) 
{ int fd ;
  int len ;
  SOCK_OPT *ptr ;
  OPT_UNION val ;
  char line[1024] ;
  int *tmp ;
  size_t tmp___0 ;
  int tmp___1 ;

  {
  {
# 1375
  fd = socket(2, 1, 0);
# 1377
  log_raw((char const   *)"Socket option defaults:");
# 1378
  log_raw((char const   *)"    %-16s%-10s%-10s%-10s%-10s", "Option", "Accept",
          "Local", "Remote", "OS default");
# 1380
  ptr = sock_opts;
  }
# 1380
  while (ptr->opt_str) {
    {
# 1382
    sprintf((char */* __restrict  */)(line),
            (char const   */* __restrict  */)((char const   *)"    %-16s"),
            ptr->opt_str);
# 1384
    print_option(line, ptr->opt_type, ptr->opt_val[0]);
# 1385
    print_option(line, ptr->opt_type, ptr->opt_val[1]);
# 1386
    print_option(line, ptr->opt_type, ptr->opt_val[2]);
# 1388
    len = (int )sizeof(val);
# 1389
    tmp___1 = getsockopt(fd, ptr->opt_level, ptr->opt_name,
                         (void */* __restrict  */)((void *)(& val)),
                         (socklen_t */* __restrict  */)((socklen_t *)(& len)));
    }
# 1389
    if (tmp___1) {
      {
# 1390
      tmp = __errno_location();
      }
# 1390
      if ((*tmp) != 92) {
        {
# 1391
        log_raw((char const   *)"%s", line);
# 1392
        sockerror("getsockopt");
        }
# 1393
        return (0);
      }
      {
# 1395
      line[1023] = '\0';
# 1395
      tmp___0 = strlen((char const   *)(line));
# 1395
      strncat((char */* __restrict  */)(line),
              (char const   */* __restrict  */)((char const   *)"    --    "),
              (1024U - tmp___0) - 1U);
      }
    } else {
      {
# 1397
      print_option(line, ptr->opt_type, & val);
      }
    }
    {
# 1398
    log_raw((char const   *)"%s", line);
# 1380
    ptr ++;
    }
  }
# 1400
  return (1);
}
}
# 1403
static void print_option(char *line , int type , OPT_UNION *val ) 
{ char text[1024] ;
  size_t tmp ;

  {
# 1406
  if (val) {
# 1409
    switch (type) {
    case 1: 
    {

    }
    case 2: 
    {
# 1412
    sprintf((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"%10d"),
            val->i_val);
    }
# 1413
    break;
    case 3: 
    {
# 1415
    sprintf((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"%d:%-8d"),
            val->linger_val.l_onoff, val->linger_val.l_linger);
    }
# 1417
    break;
    case 4: 
    {
# 1419
    sprintf((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"%6d:%-3d"),
            (int )val->timeval_val.tv_sec, (int )val->timeval_val.tv_usec);
    }
# 1421
    break;
    case 5: 
    {
# 1423
    sprintf((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"%10s"),
            val->c_val);
    }
# 1424
    break;
    default: 
    {
# 1426
    text[1023] = '\0';
# 1426
    strncpy((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"  Ooops?  "),
            1023U);
    }
    }
  } else {
    {
# 1407
    text[1023] = '\0';
# 1407
    strncpy((char */* __restrict  */)(text),
            (char const   */* __restrict  */)((char const   *)"    --    "),
            1023U);
    }
  }
  {
# 1429
  (*(line + 1023)) = '\0';
# 1429
  tmp = strlen((char const   *)line);
# 1429
  strncat((char */* __restrict  */)line,
          (char const   */* __restrict  */)((char const   *)(text)),
          (1024U - tmp) - 1U);
  }
# 1430
  return;
}
}
# 1432
static int parse_socket_option(char *arg ) 
{ int socket_type ;
  char *opt_val_str ;
  char *opt_val2_str ;
  SOCK_OPT *ptr ;
  char *tmp ;
  int tmp___0 ;
  char *tmp___1 ;
  char *tmp___2 ;
  size_t tmp___3 ;

  {
# 1437
  if ((int )(*(arg + 1)) != 58) {
# 1438
    return (0);
  }
# 1439
  switch ((int )(*(arg + 0))) {
  case 97: 
  {
# 1441
  socket_type = 0;
  }
# 1441
  break;
  case 108: 
  {
# 1443
  socket_type = 1;
  }
# 1443
  break;
  case 114: 
  {
# 1445
  socket_type = 2;
  }
# 1445
  break;
  default: 
  {

  }
# 1447
  return (0);
  }
  {
# 1449
  arg += 2;
# 1450
  opt_val_str = strchr((char const   *)arg, (int )'=');
  }
# 1451
  if (! opt_val_str) {
# 1452
    return (0);
  }
  {
# 1453
  tmp = opt_val_str;
# 1453
  opt_val_str ++;
# 1453
  (*tmp) = '\0';
# 1454
  ptr = sock_opts;
  }
# 1455
  while (1) {
# 1456
    if (! ptr->opt_str) {
# 1457
      return (0);
    }
    {
# 1458
    tmp___0 = strcmp((char const   *)arg, (char const   *)ptr->opt_str);
    }
# 1458
    if (! tmp___0) {
# 1459
      break;
    }
    {
# 1460
    ptr ++;
    }
  }
  {
# 1462
  ptr->opt_val[socket_type] = (OPT_UNION *)calloc(1U, sizeof(OPT_UNION ));
  }
# 1463
  switch (ptr->opt_type) {
  case 1: 
  {

  }
  case 2: 
  {
# 1466
  (ptr->opt_val[socket_type])->i_val = atoi((char const   *)opt_val_str);
  }
# 1467
  return (1);
  case 3: 
  {
# 1469
  opt_val2_str = strchr((char const   *)opt_val_str, (int )':');
  }
# 1470
  if (opt_val2_str) {
    {
# 1471
    tmp___1 = opt_val2_str;
# 1471
    opt_val2_str ++;
# 1471
    (*tmp___1) = '\0';
# 1472
    (ptr->opt_val[socket_type])->linger_val.l_linger = atoi((char const   *)opt_val2_str);
    }
  } else {
    {
# 1474
    (ptr->opt_val[socket_type])->linger_val.l_linger = 0;
    }
  }
  {
# 1476
  (ptr->opt_val[socket_type])->linger_val.l_onoff = atoi((char const   *)opt_val_str);
  }
# 1477
  return (1);
  case 4: 
  {
# 1479
  opt_val2_str = strchr((char const   *)opt_val_str, (int )':');
  }
# 1480
  if (opt_val2_str) {
    {
# 1481
    tmp___2 = opt_val2_str;
# 1481
    opt_val2_str ++;
# 1481
    (*tmp___2) = '\0';
# 1482
    (ptr->opt_val[socket_type])->timeval_val.tv_usec = (__suseconds_t )atoi((char const   *)opt_val2_str);
    }
  } else {
    {
# 1484
    (ptr->opt_val[socket_type])->timeval_val.tv_usec = 0L;
    }
  }
  {
# 1486
  (ptr->opt_val[socket_type])->timeval_val.tv_sec = (__time_t )atoi((char const   *)opt_val_str);
  }
# 1487
  return (1);
  case 5: 
  {
# 1489
  tmp___3 = strlen((char const   *)opt_val_str);
  }
# 1489
  if (tmp___3 + 1U > sizeof(OPT_UNION )) {
# 1490
    return (0);
  }
  {
# 1491
  strcpy((char */* __restrict  */)((ptr->opt_val[socket_type])->c_val),
         (char const   */* __restrict  */)((char const   *)opt_val_str));
  }
# 1492
  return (1);
  default: 
  {

  }
  }
# 1496
  return (0);
}
}
# 35 "protocol.c"
static int cifs_client(CLI *c ) ;
# 36
static int cifs_server(CLI *c ) ;
# 37
static int smtp_client(CLI *c ) ;
# 38
static int smtp_server(CLI *c ) ;
# 39
static int pop3_client(CLI *c ) ;
# 40
static int pop3_server(CLI *c ) ;
# 41
static int nntp_client(CLI *c ) ;
# 42
static int nntp_server(CLI *c ) ;
# 43
static int RFC2487(int fd ) ;
# 45
int negotiate(CLI *c ) 
{ int retval___0 ;
  char const   *tmp ;
  char const   *tmp___8 ;
  int tmp___9 ;
  int tmp___10 ;
  int tmp___11 ;
  int tmp___12 ;

  {
  {
# 46
  retval___0 = -1;
  }
# 48
  if (! (c->opt)->protocol) {
# 49
    return (0);
  }
# 50
  if (options.option.client) {
    {
# 50
    tmp = (char const   *)"client";
    }
  } else {
    {
# 50
    tmp = (char const   *)"server";
    }
  }
  {
# 50
  log(5, (char const   *)"Negotiations for %s (%s side) started",
      (c->opt)->protocol, tmp);
# 53
  tmp___12 = strcmp((char const   *)(c->opt)->protocol, (char const   *)"cifs");
  }
# 53
  if (tmp___12) {
    {
# 55
    tmp___11 = strcmp((char const   *)(c->opt)->protocol, (char const   *)"smtp");
    }
# 55
    if (tmp___11) {
      {
# 57
      tmp___10 = strcmp((char const   *)(c->opt)->protocol,
                        (char const   *)"pop3");
      }
# 57
      if (tmp___10) {
        {
# 59
        tmp___9 = strcmp((char const   *)(c->opt)->protocol,
                         (char const   *)"nntp");
        }
# 59
        if (tmp___9) {
# 62
          if (options.option.client) {
            {
# 62
            tmp___8 = (char const   *)"client";
            }
          } else {
            {
# 62
            tmp___8 = (char const   *)"server";
            }
          }
          {
# 62
          log(3, (char const   *)"Protocol %s not supported in %s mode",
              (c->opt)->protocol, tmp___8);
          }
# 64
          return (-1);
        } else {
# 60
          if (options.option.client) {
            {
# 60
            retval___0 = nntp_client(c);
            }
          } else {
            {
# 60
            retval___0 = nntp_server(c);
            }
          }
        }
      } else {
# 58
        if (options.option.client) {
          {
# 58
          retval___0 = pop3_client(c);
          }
        } else {
          {
# 58
          retval___0 = pop3_server(c);
          }
        }
      }
    } else {
# 56
      if (options.option.client) {
        {
# 56
        retval___0 = smtp_client(c);
        }
      } else {
        {
# 56
        retval___0 = smtp_server(c);
        }
      }
    }
  } else {
# 54
    if (options.option.client) {
      {
# 54
      retval___0 = cifs_client(c);
      }
    } else {
      {
# 54
      retval___0 = cifs_server(c);
      }
    }
  }
# 67
  if (retval___0) {
    {
# 68
    log(5, (char const   *)"Protocol negotiation failed");
    }
  } else {
    {
# 70
    log(5, (char const   *)"Protocol negotiation succeded");
    }
  }
# 71
  return (retval___0);
}
}
# 74
static int cifs_client(CLI *c ) 
{ u8 buffer[5] ;
  u8 request_dummy[4] ;
  int tmp ;
  int tmp___0 ;

  {
  {
# 76
  request_dummy[0] = (unsigned char )129;
# 76
  request_dummy[1] = (unsigned char )0;
# 76
  request_dummy[2] = (unsigned char )0;
# 76
  request_dummy[3] = (unsigned char )0;
# 78
  tmp = write_blocking(c, c->remote_fd.fd, request_dummy, 4);
  }
# 78
  if (tmp < 0) {
# 79
    return (-1);
  }
  {
# 80
  tmp___0 = read_blocking(c, c->remote_fd.fd, buffer, 5);
  }
# 80
  if (tmp___0 < 0) {
    {
# 81
    log(3, (char const   *)"Failed to read NetBIOS response");
    }
# 82
    return (-1);
  }
# 84
  if ((int )buffer[0] != 131) {
    {
# 85
    log(3, (char const   *)"Negative response expected");
    }
# 86
    return (-1);
  }
# 88
  if ((int )buffer[2] != 0) {
    {
# 89
    log(3, (char const   *)"Unexpected NetBIOS response size");
    }
# 90
    return (-1);
  } else {
# 88
    if ((int )buffer[3] != 1) {
      {
# 89
      log(3, (char const   *)"Unexpected NetBIOS response size");
      }
# 90
      return (-1);
    }
  }
# 92
  if ((int )buffer[4] != 142) {
    {
# 93
    log(3, (char const   *)"Remote server does not require SSL");
    }
# 94
    return (-1);
  }
# 96
  return (0);
}
}
# 99
static int cifs_server(CLI *c ) 
{ u8 buffer[128] ;
  u8 response_access_denied[5] ;
  u8 response_use_ssl[5] ;
  u16 len ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;

  {
  {
# 101
  response_access_denied[0] = (unsigned char )131;
# 101
  response_access_denied[1] = (unsigned char )0;
# 101
  response_access_denied[2] = (unsigned char )0;
# 101
  response_access_denied[3] = (unsigned char )1;
# 101
  response_access_denied[4] = (unsigned char )129;
# 102
  response_use_ssl[0] = (unsigned char )131;
# 102
  response_use_ssl[1] = (unsigned char )0;
# 102
  response_use_ssl[2] = (unsigned char )0;
# 102
  response_use_ssl[3] = (unsigned char )1;
# 102
  response_use_ssl[4] = (unsigned char )142;
# 105
  tmp = read_blocking(c, c->local_rfd.fd, buffer, 4);
  }
# 105
  if (tmp < 0) {
# 106
    return (-1);
  }
  {
# 107
  len = (unsigned short )buffer[3];
# 108
  len = (unsigned short )((int )len | ((int )((unsigned short )buffer[2]) << 8));
  }
# 109
  if ((unsigned int )len > sizeof(buffer) - 4U) {
    {
# 110
    log(3, (char const   *)"Received block too long");
    }
# 111
    return (-1);
  }
  {
# 113
  tmp___0 = read_blocking(c, c->local_rfd.fd, buffer + 4, (int )len);
  }
# 113
  if (tmp___0 < 0) {
# 114
    return (-1);
  }
# 115
  if ((int )buffer[0] != 129) {
    {
# 116
    log(3, (char const   *)"Client did not send session setup");
# 117
    write_blocking(c, c->local_wfd.fd, response_access_denied, 5);
    }
# 118
    return (-1);
  }
  {
# 120
  tmp___1 = write_blocking(c, c->local_wfd.fd, response_use_ssl, 5);
  }
# 120
  if (tmp___1 < 0) {
# 121
    return (-1);
  }
# 122
  return (0);
}
}
# 125
static int smtp_client(CLI *c ) 
{ char line[1024] ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;
  int tmp___9 ;

  {
# 128
  while (1) {
    {
# 129
    tmp = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
    }
# 129
    if (tmp < 0) {
# 130
      return (-1);
    }
    {
# 131
    tmp___0 = fdprintf(c, c->local_wfd.fd, (char const   *)"%s", line);
    }
# 131
    if (tmp___0 < 0) {
# 132
      return (-1);
    }
    {
# 128
    tmp___1 = strncasecmp((char const   *)(line), (char const   *)"220-", 4U);
    }
# 128
    if (! (tmp___1 == 0)) {
# 128
      break;
    }
  }
  {
# 135
  tmp___2 = fdprintf(c, c->remote_fd.fd, (char const   *)"EHLO localhost");
  }
# 135
  if (tmp___2 < 0) {
# 136
    return (-1);
  }
# 137
  while (1) {
    {
# 138
    tmp___3 = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
    }
# 138
    if (tmp___3 < 0) {
# 139
      return (-1);
    }
    {
# 137
    tmp___4 = strncasecmp((char const   *)(line), (char const   *)"250-", 4U);
    }
# 137
    if (! (tmp___4 == 0)) {
# 137
      break;
    }
  }
  {
# 141
  tmp___5 = strncasecmp((char const   *)(line), (char const   *)"250 ", 4U);
  }
# 141
  if (tmp___5 != 0) {
    {
# 142
    log(3, (char const   *)"Remote server is not RFC 1425 compliant");
    }
# 143
    return (-1);
  }
  {
# 146
  tmp___6 = fdprintf(c, c->remote_fd.fd, (char const   *)"STARTTLS");
  }
# 146
  if (tmp___6 < 0) {
# 147
    return (-1);
  }
# 148
  while (1) {
    {
# 149
    tmp___7 = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
    }
# 149
    if (tmp___7 < 0) {
# 150
      return (-1);
    }
    {
# 148
    tmp___8 = strncasecmp((char const   *)(line), (char const   *)"220-", 4U);
    }
# 148
    if (! (tmp___8 == 0)) {
# 148
      break;
    }
  }
  {
# 152
  tmp___9 = strncasecmp((char const   *)(line), (char const   *)"220 ", 4U);
  }
# 152
  if (tmp___9 != 0) {
    {
# 153
    log(3, (char const   *)"Remote server is not RFC 2487 compliant");
    }
# 154
    return (-1);
  }
# 156
  return (0);
}
}
# 159
static int smtp_server(CLI *c ) 
{ char line[1024] ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;

  {
  {
# 162
  tmp = RFC2487(c->local_rfd.fd);
  }
# 162
  if (tmp == 0) {
# 163
    return (0);
  }
  {
# 165
  tmp___0 = fdscanf(c, c->remote_fd.fd, (char const   *)"220%[^\n]", line);
  }
# 165
  if (tmp___0 != 1) {
    {
# 166
    log(3, (char const   *)"Unknown server welcome");
    }
# 167
    return (-1);
  }
  {
# 169
  tmp___1 = fdprintf(c, c->local_wfd.fd, (char const   *)"220%s + stunnel", line);
  }
# 169
  if (tmp___1 < 0) {
# 170
    return (-1);
  }
  {
# 171
  tmp___2 = fdscanf(c, c->local_rfd.fd, (char const   *)"EHLO %[^\n]", line);
  }
# 171
  if (tmp___2 != 1) {
    {
# 172
    log(3, (char const   *)"Unknown client EHLO");
    }
# 173
    return (-1);
  }
  {
# 175
  tmp___3 = fdprintf(c, c->local_wfd.fd, (char const   *)"250-%s Welcome", line);
  }
# 175
  if (tmp___3 < 0) {
# 176
    return (-1);
  }
  {
# 177
  tmp___4 = fdprintf(c, c->local_wfd.fd, (char const   *)"250 STARTTLS");
  }
# 177
  if (tmp___4 < 0) {
# 178
    return (-1);
  }
  {
# 179
  tmp___5 = fdscanf(c, c->local_rfd.fd, (char const   *)"STARTTLS", line);
  }
# 179
  if (tmp___5 < 0) {
    {
# 180
    log(3, (char const   *)"STARTTLS expected");
    }
# 181
    return (-1);
  }
  {
# 183
  tmp___6 = fdprintf(c, c->local_wfd.fd, (char const   *)"220 Go ahead");
  }
# 183
  if (tmp___6 < 0) {
# 184
    return (-1);
  }
# 185
  return (0);
}
}
# 188
static int pop3_client(CLI *c ) 
{ char line[1024] ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;

  {
  {
# 191
  tmp = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
  }
# 191
  if (tmp < 0) {
# 192
    return (-1);
  }
  {
# 193
  tmp___0 = strncasecmp((char const   *)(line), (char const   *)"+OK ", 4U);
  }
# 193
  if (tmp___0) {
    {
# 194
    log(3, (char const   *)"Unknown server welcome");
    }
# 195
    return (-1);
  }
  {
# 197
  tmp___1 = fdprintf(c, c->local_wfd.fd, (char const   *)"%s", line);
  }
# 197
  if (tmp___1 < 0) {
# 198
    return (-1);
  }
  {
# 199
  tmp___2 = fdprintf(c, c->remote_fd.fd, (char const   *)"STLS");
  }
# 199
  if (tmp___2 < 0) {
# 200
    return (-1);
  }
  {
# 201
  tmp___3 = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
  }
# 201
  if (tmp___3 < 0) {
# 202
    return (-1);
  }
  {
# 203
  tmp___4 = strncasecmp((char const   *)(line), (char const   *)"+OK ", 4U);
  }
# 203
  if (tmp___4) {
    {
# 204
    log(3, (char const   *)"Server does not support TLS");
    }
# 205
    return (-1);
  }
# 207
  return (0);
}
}
# 210
static int pop3_server(CLI *c ) 
{ char line[1024] ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;

  {
  {
# 213
  tmp = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
  }
# 213
  if (tmp < 0) {
# 214
    return (-1);
  }
  {
# 215
  tmp___0 = fdprintf(c, c->local_wfd.fd, (char const   *)"%s + stunnel", line);
  }
# 215
  if (tmp___0 < 0) {
# 216
    return (-1);
  }
  {
# 217
  tmp___1 = fdscanf(c, c->local_rfd.fd, (char const   *)"%[^\n]", line);
  }
# 217
  if (tmp___1 < 0) {
# 218
    return (-1);
  }
  {
# 219
  tmp___4 = strncasecmp((char const   *)(line), (char const   *)"CAPA", 4U);
  }
# 219
  if (! tmp___4) {
    {
# 220
    tmp___2 = fdprintf(c, c->local_wfd.fd,
                       (char const   *)"-ERR Stunnel does not support capabilities");
    }
# 220
    if (tmp___2 < 0) {
# 221
      return (-1);
    }
    {
# 222
    tmp___3 = fdscanf(c, c->local_rfd.fd, (char const   *)"%[^\n]", line);
    }
# 222
    if (tmp___3 < 0) {
# 223
      return (-1);
    }
  }
  {
# 225
  tmp___5 = strncasecmp((char const   *)(line), (char const   *)"STLS", 4U);
  }
# 225
  if (tmp___5) {
    {
# 226
    log(3, (char const   *)"Client does not want TLS");
    }
# 227
    return (-1);
  }
  {
# 229
  tmp___6 = fdprintf(c, c->local_wfd.fd,
                     (char const   *)"+OK Stunnel starts TLS negotiation");
  }
# 229
  if (tmp___6 < 0) {
# 230
    return (-1);
  }
# 232
  return (0);
}
}
# 235
static int nntp_client(CLI *c ) 
{ char line[1024] ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;

  {
  {
# 238
  tmp = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
  }
# 238
  if (tmp < 0) {
# 239
    return (-1);
  }
  {
# 240
  tmp___0 = strncasecmp((char const   *)(line), (char const   *)"200 ", 4U);
  }
# 240
  if (tmp___0) {
    {
# 240
    tmp___1 = strncasecmp((char const   *)(line), (char const   *)"201 ", 4U);
    }
# 240
    if (tmp___1) {
      {
# 241
      log(3, (char const   *)"Unknown server welcome");
      }
# 242
      return (-1);
    }
  }
  {
# 244
  tmp___2 = fdprintf(c, c->local_wfd.fd, (char const   *)"%s", line);
  }
# 244
  if (tmp___2 < 0) {
# 245
    return (-1);
  }
  {
# 246
  tmp___3 = fdprintf(c, c->remote_fd.fd, (char const   *)"STARTTLS");
  }
# 246
  if (tmp___3 < 0) {
# 247
    return (-1);
  }
  {
# 248
  tmp___4 = fdscanf(c, c->remote_fd.fd, (char const   *)"%[^\n]", line);
  }
# 248
  if (tmp___4 < 0) {
# 249
    return (-1);
  }
  {
# 250
  tmp___5 = strncasecmp((char const   *)(line), (char const   *)"382 ", 4U);
  }
# 250
  if (tmp___5) {
    {
# 251
    log(3, (char const   *)"Server does not support TLS");
    }
# 252
    return (-1);
  }
# 254
  return (0);
}
}
# 257
static int nntp_server(CLI *c ) 
{ 

  {
  {
# 258
  log(3, (char const   *)"Protocol not supported in server mode");
  }
# 259
  return (-1);
}
}
# 262
static int RFC2487(int fd ) 
{ fd_set fdsRead ;
  struct timeval timeout ;
  unsigned int __i ;
  fd_set *__arr ;
  int tmp ;

  {
# 266
  while (1) {
    {
# 266
    __arr = & fdsRead;
# 266
    __i = 0U;
    }
# 266
    while (__i < sizeof(fd_set ) / sizeof(__fd_mask )) {
      {
# 266
      __arr->__fds_bits[__i] = 0L;
# 266
      __i ++;
      }
    }
# 266
    break;
  }
  {
# 267
  fdsRead.__fds_bits[(unsigned int )fd / (8U * sizeof(__fd_mask ))] = fdsRead.__fds_bits[(unsigned int )fd /
                                                                                         (8U *
                                                                                          sizeof(__fd_mask ))] |
                                                                      (1L <<
                                                                       (unsigned int )fd %
                                                                       (8U *
                                                                        sizeof(__fd_mask )));
# 268
  timeout.tv_usec = 0L;
# 268
  timeout.tv_sec = timeout.tv_usec;
# 270
  tmp = sselect(fd + 1, & fdsRead, (fd_set *)((void *)0), (fd_set *)((void *)0),
                & timeout);
  }
# 270
  switch (tmp) {
  case 0: 
  {
# 272
  log(7, (char const   *)"RFC 2487 detected");
  }
# 273
  return (1);
  case 1: 
  {
# 275
  log(7, (char const   *)"RFC 2487 not detected");
  }
# 276
  return (0);
  default: 
  {
# 278
  sockerror("RFC2487 (select)");
  }
# 279
  return (-1);
  }
# 279
  return (0);
}
}
# 40 "sselect.c"
static int signal_pipe[2]  ;
# 41
static char signal_buffer[16]  ;
# 45
static void sigchld_handler(int sig ) 
{ int save_errno ;
  int *tmp ;
  int *tmp___0 ;

  {
  {
# 46
  tmp = __errno_location();
# 46
  save_errno = (*tmp);
# 48
  write(signal_pipe[1], (void const   *)(signal_buffer), 1U);
# 49
  signal(17, & sigchld_handler);
# 50
  tmp___0 = __errno_location();
# 50
  (*tmp___0) = save_errno;
  }
# 51
  return;
}
}
# 55
void sselect_init(fd_set *set , int *n ) 
{ int tmp ;

  {
  {
# 56
  tmp = pipe(signal_pipe);
  }
# 56
  if (tmp) {
    {
# 57
    ioerror("pipe");
# 58
    exit(1);
    }
  }
  {
# 60
  alloc_fd(signal_pipe[0]);
# 61
  alloc_fd(signal_pipe[1]);
# 64
  fcntl(signal_pipe[0], 2, 1);
# 65
  fcntl(signal_pipe[1], 2, 1);
# 67
  set->__fds_bits[(unsigned int )signal_pipe[0] / (8U * sizeof(__fd_mask ))] = set->__fds_bits[(unsigned int )signal_pipe[0] /
                                                                                               (8U *
                                                                                                sizeof(__fd_mask ))] |
                                                                               (1L <<
                                                                                (unsigned int )signal_pipe[0] %
                                                                                (8U *
                                                                                 sizeof(__fd_mask )));
  }
# 68
  if (signal_pipe[0] > (*n)) {
    {
# 69
    (*n) = signal_pipe[0];
    }
  }
  {
# 70
  signal(17, & sigchld_handler);
  }
# 71
  return;
}
}
# 75
int sselect(int n , fd_set *readfds , fd_set *writefds , fd_set *exceptfds ,
            struct timeval *timeout ) 
{ int retval___0 ;
  struct timeval tv ;
  int *tmp ;

  {
# 80
  while (1) {
# 81
    if (timeout) {
      {
# 82
      memcpy((void */* __restrict  */)((void *)(& tv)),
             (void const   */* __restrict  */)((void const   *)timeout),
             sizeof(struct timeval ));
# 83
      retval___0 = select(n, (fd_set */* __restrict  */)readfds,
                          (fd_set */* __restrict  */)writefds,
                          (fd_set */* __restrict  */)exceptfds,
                          (struct timeval */* __restrict  */)(& tv));
      }
    } else {
      {
# 85
      retval___0 = select(n, (fd_set */* __restrict  */)readfds,
                          (fd_set */* __restrict  */)writefds,
                          (fd_set */* __restrict  */)exceptfds,
                          (struct timeval */* __restrict  */)((struct timeval *)((void *)0)));
      }
# 87
      if (retval___0 > 0) {
# 87
        if (readfds->__fds_bits[(unsigned int )signal_pipe[0] /
                                (8U * sizeof(__fd_mask ))] &
            (1L << (unsigned int )signal_pipe[0] % (8U * sizeof(__fd_mask )))) {
          {
# 89
          read(signal_pipe[0], (void *)(signal_buffer), sizeof(signal_buffer));
# 91
          exec_status();
          }
        }
      }
    }
# 80
    if (retval___0 < 0) {
      {
# 80
      tmp = __errno_location();
      }
# 80
      if (! ((*tmp) == 4)) {
# 80
        break;
      }
    } else {
# 80
      break;
    }
  }
# 100
  return (retval___0);
}
}
# 103
int waitforsocket(int fd , int dir , int timeout ) 
{ struct timeval tv ;
  fd_set set ;
  int ready ;
  char const   *tmp ;
  unsigned int __i ;
  fd_set *__arr ;
  fd_set *tmp___0 ;
  fd_set *tmp___1 ;

  {
# 109
  if (dir) {
    {
# 109
    tmp = (char const   *)"write";
    }
  } else {
    {
# 109
    tmp = (char const   *)"read";
    }
  }
  {
# 109
  log(7, (char const   *)"waitforsocket: FD=%d, DIR=%s", fd, tmp);
  }
# 110
  while (1) {
    {
# 110
    __arr = & set;
# 110
    __i = 0U;
    }
# 110
    while (__i < sizeof(fd_set ) / sizeof(__fd_mask )) {
      {
# 110
      __arr->__fds_bits[__i] = 0L;
# 110
      __i ++;
      }
    }
# 110
    break;
  }
  {
# 111
  set.__fds_bits[(unsigned int )fd / (8U * sizeof(__fd_mask ))] = set.__fds_bits[(unsigned int )fd /
                                                                                 (8U *
                                                                                  sizeof(__fd_mask ))] |
                                                                  (1L <<
                                                                   (unsigned int )fd %
                                                                   (8U *
                                                                    sizeof(__fd_mask )));
# 112
  tv.tv_sec = (long )timeout;
# 113
  tv.tv_usec = 0L;
  }
# 114
  if (dir) {
    {
# 114
    tmp___0 = & set;
    }
  } else {
    {
# 114
    tmp___0 = (fd_set *)((void *)0);
    }
  }
# 114
  if (dir) {
    {
# 114
    tmp___1 = (fd_set *)((void *)0);
    }
  } else {
    {
# 114
    tmp___1 = & set;
    }
  }
  {
# 114
  ready = sselect(fd + 1, tmp___1, tmp___0, (fd_set *)((void *)0), & tv);
  }
# 115
  switch (ready) {
  case -1: 
  {
# 117
  sockerror("waitforsocket");
  }
# 118
  break;
  case 0: 
  {
# 120
  log(7, (char const   *)"waitforsocket: timeout");
  }
# 121
  break;
  case 1: 
  {
# 123
  log(7, (char const   *)"waitforsocket: ok");
  }
# 124
  break;
  default: 
  {
# 126
  log(6, (char const   *)"waitforsocket: unexpected result");
  }
  }
# 128
  return (ready);
}
}
# 160
void exec_status(void) 
{ int pid ;
  int status ;
  union __anonunion___u_74 __u ;
  union __anonunion___u_75 __u___0 ;
  int __status ;
  union __anonunion___u_76 __u___1 ;
  int tmp ;

  {
# 164
  while (1) {
    {
# 164
    pid = waitpid(-1, & status, 1);
    }
# 164
    if (! (pid > 0)) {
# 164
      break;
    }
    {
# 169
    __u___1.__in = status;
# 169
    __status = __u___1.__i;
    }
# 169
    if ((__status & 255) == 127) {
      {
# 169
      tmp = 0;
      }
    } else {
# 169
      if ((__status & 127) == 0) {
        {
# 169
        tmp = 0;
        }
      } else {
        {
# 169
        tmp = 1;
        }
      }
    }
# 169
    if (tmp) {
      {
# 171
      __u.__in = status;
# 171
      log(6, (char const   *)"Local process %d terminated on signal %d", pid,
          __u.__i & 127);
      }
    } else {
      {
# 174
      __u___0.__in = status;
# 174
      log(6, (char const   *)"Local process %d finished with code %d", pid,
          (__u___0.__i & 65280) >> 8);
      }
    }
  }
# 181
  return;
}
}
# 184
int write_blocking(CLI *c , int fd , u8 *ptr , int len ) 
{ int num ;
  int tmp ;

  {
# 189
  while (len > 0) {
    {
# 190
    tmp = waitforsocket(fd, 1, (c->opt)->timeout_busy);
    }
# 190
    if (tmp < 1) {
# 191
      return (-1);
    }
    {
# 192
    num = write(fd, (void const   *)ptr, (unsigned int )len);
    }
# 193
    switch (num) {
    case -1: 
    {
# 195
    sockerror("writesocket (write_blocking)");
    }
# 196
    return (-1);
    }
    {
# 198
    ptr += num;
# 199
    len -= num;
    }
  }
# 201
  return (0);
}
}
# 204
int read_blocking(CLI *c , int fd , u8 *ptr , int len ) 
{ int num ;
  int tmp ;

  {
# 209
  while (len > 0) {
    {
# 210
    tmp = waitforsocket(fd, 0, (c->opt)->timeout_busy);
    }
# 210
    if (tmp < 1) {
# 211
      return (-1);
    }
    {
# 212
    num = read(fd, (void *)ptr, (unsigned int )len);
    }
# 213
    switch (num) {
    case -1: 
    {
# 215
    sockerror("readsocket (read_blocking)");
    }
# 216
    return (-1);
    case 0: 
    {
# 218
    log(3, (char const   *)"Unexpected socket close (read_blocking)");
    }
# 219
    return (-1);
    }
    {
# 221
    ptr += num;
# 222
    len -= num;
    }
  }
# 224
  return (0);
}
}
# 227
int fdprintf(CLI *c , int fd , char const   *format  , ...) 
{ __ccured_va_list arglist ;
  char line[1024] ;
  char logline[1024] ;
  char crlf[3] ;
  int len ;
  unsigned long tmp ;
  size_t tmp___0 ;
  int tmp___1 ;
  char *p ;
  int tmp___2 ;

  {
  {
# 230
  crlf[0] = '\r';
# 230
  crlf[1] = '\n';
# 230
  crlf[2] = '\0';
# 233
  tmp = (unsigned long )GCC_STDARG_START();
# 233
  __ccured_va_start(arglist, tmp);
# 235
  len = vsnprintf((char */* __restrict  */)(line), 1024U,
                  (char const   */* __restrict  */)format, arglist);
# 239
  __ccured_va_end(arglist);
# 240
  len += 2;
  }
# 241
  if (len >= 1024) {
    {
# 242
    log(3, (char const   *)"Line too long in fdprintf");
    }
# 243
    return (-1);
  }
  {
# 245
  logline[1023] = '\0';
# 245
  strncpy((char */* __restrict  */)(logline),
          (char const   */* __restrict  */)((char const   *)(line)), 1023U);
# 246
  line[1023] = '\0';
# 246
  tmp___0 = strlen((char const   *)(line));
# 246
  strncat((char */* __restrict  */)(line),
          (char const   */* __restrict  */)((char const   *)(crlf)),
          (1024U - tmp___0) - 1U);
# 247
  tmp___1 = write_blocking(c, fd, (u8 *)(line), len);
  }
# 247
  if (tmp___1 < 0) {
# 248
    return (-1);
  }
# 249
  while (1) {
    {
# 249
    p = logline;
    }
# 249
    while ((*p)) {
      {
# 249
      tmp___2 = _get__ctype_b((int )(*p));
      }
# 249
      if (! (tmp___2 & 16384)) {
        {
# 249
        (*p) = '.';
        }
      }
      {
# 249
      p ++;
      }
    }
# 249
    break;
  }
  {
# 250
  log(7, (char const   *)" -> %s", logline);
  }
# 251
  return (len);
}
}
# 254
int fdscanf(CLI *c , int fd , char const   *format , char *buffer ) 
{ char line[1024] ;
  char logline[1024] ;
  char lformat[1024] ;
  int ptr ;
  int retval___0 ;
  int tmp ;
  int tmp___0 ;
  char *p ;
  int tmp___1 ;
  int tmp___2 ;

  {
  {
# 258
  ptr = 0;
  }
# 258
  while (ptr < 1023) {
    {
# 259
    tmp = waitforsocket(fd, 0, (c->opt)->timeout_busy);
    }
# 259
    if (tmp < 1) {
# 260
      return (-1);
    }
    {
# 261
    tmp___0 = read(fd, (void *)(line + ptr), 1U);
    }
# 261
    switch (tmp___0) {
    case -1: 
    {
# 263
    sockerror("readsocket (fdscanf)");
    }
# 264
    return (-1);
    case 0: 
    {
# 266
    log(3, (char const   *)"Unexpected socket close (fdscanf)");
    }
# 267
    return (-1);
    }
# 269
    if ((int )line[ptr] == 13) {
      goto __Cont;
    }
# 271
    if ((int )line[ptr] == 10) {
# 272
      break;
    }
    __Cont: 
    {
# 258
    ptr ++;
    }
  }
  {
# 274
  line[ptr] = '\0';
# 275
  logline[1023] = '\0';
# 275
  strncpy((char */* __restrict  */)(logline),
          (char const   */* __restrict  */)((char const   *)(line)), 1023U);
  }
# 276
  while (1) {
    {
# 276
    p = logline;
    }
# 276
    while ((*p)) {
      {
# 276
      tmp___1 = _get__ctype_b((int )(*p));
      }
# 276
      if (! (tmp___1 & 16384)) {
        {
# 276
        (*p) = '.';
        }
      }
      {
# 276
      p ++;
      }
    }
# 276
    break;
  }
  {
# 277
  log(7, (char const   *)" <- %s", logline);
# 278
  retval___0 = sscanf((char const   */* __restrict  */)((char const   *)(line)),
                      (char const   */* __restrict  */)format, buffer);
  }
# 279
  if (retval___0 >= 0) {
# 280
    return (retval___0);
  }
  {
# 281
  log(7, (char const   *)"fdscanf falling back to lowercase");
# 282
  lformat[1023] = '\0';
# 282
  strncpy((char */* __restrict  */)(lformat),
          (char const   */* __restrict  */)format, 1023U);
# 283
  ptr = 0;
  }
# 283
  while (lformat[ptr]) {
    {
# 284
    lformat[ptr] = (char )tolower((int )lformat[ptr]);
# 283
    ptr ++;
    }
  }
  {
# 285
  ptr = 0;
  }
# 285
  while (line[ptr]) {
    {
# 286
    line[ptr] = (char )tolower((int )line[ptr]);
# 285
    ptr ++;
    }
  }
  {
# 287
  tmp___2 = sscanf((char const   */* __restrict  */)((char const   *)(line)),
                   (char const   */* __restrict  */)((char const   *)(lformat)),
                   buffer);
  }
# 287
  return (tmp___2);
}
}
# 48 "ssl.c"
static int init_dh(void) ;
# 49
static int init_prng(void) ;
# 50
static int prng_seeded(int bytes ) ;
# 51
static int add_rand_file(char *filename ) ;
# 53
static RSA *tmp_rsa_cb(SSL *s , int export , int keylen ) ;
# 54
static RSA *make_temp_key(int keylen ) ;
# 56
static void verify_init(void) ;
# 57
static int verify_callback(int preverify_ok , X509_STORE_CTX *callback_ctx ) ;
# 58
static int crl_callback(X509_STORE_CTX *callback_ctx ) ;
# 60
static void info_callback(SSL const   *s , int where , int ret ) ;
# 64
static void print_stats(void) ;
# 65
static void sslerror_stack(void) ;
# 68
static X509_STORE *revocation_store   = (X509_STORE *)((void *)0);
# 70
void context_init(void) 
{ int i ;
  int tmp ;
  SSL_METHOD *tmp___0 ;
  SSL_METHOD *tmp___1 ;
  int tmp___2 ;
  long tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  unsigned long tmp___6 ;
  int tmp___7 ;
  int tmp___8 ;

  {
  {
# 75
  ENGINE_load_builtin_engines();
# 77
  ENGINE_register_all_complete();
# 79
  tmp = init_prng();
  }
# 79
  if (! tmp) {
    {
# 80
    log(6, (char const   *)"PRNG seeded successfully");
    }
  }
  {
# 81
  SSL_library_init();
# 82
  SSL_load_error_strings();
  }
# 83
  if (options.option.client) {
    {
# 84
    tmp___0 = SSLv3_client_method();
# 84
    ctx = SSL_CTX_new(tmp___0);
    }
  } else {
    {
# 86
    tmp___1 = SSLv23_server_method();
# 86
    ctx = SSL_CTX_new(tmp___1);
# 88
    SSL_CTX_set_tmp_rsa_callback(ctx, & tmp_rsa_cb);
# 90
    tmp___2 = init_dh();
    }
# 90
    if (tmp___2) {
      {
# 91
      log(4, (char const   *)"Diffie-Hellman initialization failed");
      }
    }
  }
# 93
  if (options.ssl_options) {
    {
# 94
    log(7, (char const   *)"Configuration SSL options: 0x%08lX",
        options.ssl_options);
# 96
    tmp___3 = SSL_CTX_ctrl(ctx, 32, options.ssl_options, (void *)0);
# 96
    log(7, (char const   *)"SSL options set: 0x%08lX", tmp___3);
    }
  }
  {
# 100
  SSL_CTX_ctrl(ctx, 33, 3L, (void *)0);
# 104
  SSL_CTX_ctrl(ctx, 44, 3L, (void *)0);
# 105
  SSL_CTX_set_timeout(ctx, options.session_timeout);
  }
# 106
  if (options.option.cert) {
    {
# 107
    tmp___4 = SSL_CTX_use_certificate_chain_file(ctx,
                                                 (char const   *)options.cert);
    }
# 107
    if (! tmp___4) {
      {
# 108
      log(3, (char const   *)"Error reading certificate file: %s", options.cert);
# 109
      sslerror("SSL_CTX_use_certificate_chain_file");
# 110
      exit(1);
      }
    }
    {
# 112
    log(7, (char const   *)"Certificate: %s", options.cert);
# 113
    log(7, (char const   *)"Key file: %s", options.key);
# 117
    i = 0;
    }
# 117
    while (i < 3) {
      {
# 122
      tmp___5 = SSL_CTX_use_RSAPrivateKey_file(ctx, (char const   *)options.key,
                                               1);
      }
# 122
      if (tmp___5) {
# 125
        break;
      }
# 126
      if (i < 2) {
        {
# 126
        tmp___6 = ERR_peek_error();
        }
# 126
        if ((int )(tmp___6 & 4095UL) == 100) {
          {
# 127
          sslerror_stack();
# 128
          log(3, (char const   *)"Wrong pass phrase: retrying");
          }
          goto __Cont;
        }
      }
      {
# 134
      sslerror("SSL_CTX_use_RSAPrivateKey_file");
# 136
      exit(1);
      }
      __Cont: 
      {
# 117
      i ++;
      }
    }
    {
# 138
    tmp___7 = SSL_CTX_check_private_key(ctx);
    }
# 138
    if (! tmp___7) {
      {
# 139
      sslerror("Private key does not match the certificate");
# 140
      exit(1);
      }
    }
  }
  {
# 144
  verify_init();
# 146
  ctx->info_callback = & info_callback;
  }
# 148
  if (options.cipher_list) {
    {
# 149
    tmp___8 = SSL_CTX_set_cipher_list(ctx, (char const   *)options.cipher_list);
    }
# 149
    if (! tmp___8) {
      {
# 150
      sslerror("SSL_CTX_set_cipher_list");
# 151
      exit(1);
      }
    }
  }
# 154
  return;
}
}
# 156
void context_free(void) 
{ 

  {
  {
# 157
  SSL_CTX_free(ctx);
  }
# 158
  return;
}
}
# 160
static int init_prng(void) 
{ int totbytes ;
  char filename[1024] ;
  int bytes ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;
  int tmp___6 ;

  {
  {
# 161
  totbytes = 0;
# 165
  bytes = 0;
# 167
  filename[0] = '\0';
  }
# 171
  if (options.rand_file) {
    {
# 172
    tmp = add_rand_file(options.rand_file);
# 172
    totbytes += tmp;
# 173
    tmp___0 = prng_seeded(totbytes);
    }
# 173
    if (tmp___0) {
# 174
      return (0);
    }
  }
  {
# 178
  RAND_file_name(filename, 1024U);
  }
# 179
  if (filename[0]) {
    {
# 180
    filename[1023] = '\0';
# 181
    tmp___1 = add_rand_file(filename);
# 181
    totbytes += tmp___1;
# 182
    tmp___2 = prng_seeded(totbytes);
    }
# 182
    if (tmp___2) {
# 183
      return (0);
    }
  }
  {
# 187
  tmp___3 = add_rand_file("/dev/urandom");
# 187
  totbytes += tmp___3;
# 188
  tmp___4 = prng_seeded(totbytes);
  }
# 188
  if (tmp___4) {
# 189
    return (0);
  }
# 202
  if (options.egd_sock) {
    {
# 203
    bytes = RAND_egd((char const   *)options.egd_sock);
    }
# 203
    if (bytes == -1) {
      {
# 204
      log(4, (char const   *)"EGD Socket %s failed", options.egd_sock);
# 205
      bytes = 0;
      }
    } else {
      {
# 207
      totbytes += bytes;
# 208
      log(7, (char const   *)"Snagged %d random bytes from EGD Socket %s",
          bytes, options.egd_sock);
      }
# 210
      return (0);
    }
  }
  {
# 229
  tmp___5 = add_rand_file("/dev/urandom");
# 229
  totbytes += tmp___5;
# 230
  tmp___6 = prng_seeded(totbytes);
  }
# 230
  if (tmp___6) {
# 231
    return (0);
  }
  {
# 234
  log(6, (char const   *)"PRNG seeded with %d bytes total", totbytes);
# 235
  log(4,
      (char const   *)"PRNG may not have been seeded with enough random bytes");
  }
# 236
  return (-1);
}
}
# 239
static int init_dh(void) 
{ 

  {
# 278
  return (0);
}
}
# 282
static int prng_seeded(int bytes ) 
{ int tmp ;

  {
  {
# 284
  tmp = RAND_status();
  }
# 284
  if (tmp) {
    {
# 285
    log(7, (char const   *)"RAND_status claims sufficient entropy for the PRNG");
    }
# 286
    return (1);
  }
# 294
  return (0);
}
}
# 297
static int add_rand_file(char *filename ) 
{ int readbytes ;
  int writebytes ;
  struct stat sb ;
  int tmp ;

  {
  {
# 302
  tmp = stat__extinline((char const   *)filename, & sb);
  }
# 302
  if (tmp) {
# 303
    return (0);
  }
  {
# 304
  readbytes = RAND_load_file((char const   *)filename,
                             (long )options.random_bytes);
  }
# 304
  if (readbytes) {
    {
# 305
    log(7, (char const   *)"Snagged %d random bytes from %s", readbytes,
        filename);
    }
  } else {
    {
# 307
    log(6, (char const   *)"Unable to retrieve any random data from %s",
        filename);
    }
  }
# 309
  if (options.option.rand_write) {
# 309
    if (sb.st_mode & 32768U) {
      {
# 310
      writebytes = RAND_write_file((char const   *)filename);
      }
# 311
      if (writebytes == -1) {
        {
# 312
        log(4,
            (char const   *)"Failed to write strong random data to %s - may be a permissions or seeding problem",
            filename);
        }
      } else {
        {
# 315
        log(7, (char const   *)"Wrote %d new random bytes to %s", writebytes,
            filename);
        }
      }
    }
  }
# 317
  return (readbytes);
}
}
# 323
static int initialized   = 0;
# 324
static struct keytabstruct keytable[2049]  ;
# 328
static RSA *longkey   = (RSA *)((void *)0);
# 329
static int longlen   = 0;
# 330
static long longtime   = (long )0;
# 322
static RSA *tmp_rsa_cb(SSL *s , int export , int keylen ) 
{ RSA *oldkey ;
  RSA *retval___0 ;
  time_t now ;
  int i ;

  {
  {
# 335
  enter_critical_section(0);
  }
# 336
  if (! initialized) {
    {
# 337
    i = 0;
    }
# 337
    while (i < 2049) {
      {
# 338
      keytable[i].key = (RSA *)((void *)0);
# 339
      keytable[i].timeout = 0L;
# 337
      i ++;
      }
    }
    {
# 341
    initialized = 1;
    }
  }
  {
# 343
  time(& now);
  }
# 344
  if (keylen < 2049) {
# 345
    if (keytable[keylen].timeout < now) {
      {
# 346
      oldkey = keytable[keylen].key;
# 347
      keytable[keylen].key = make_temp_key(keylen);
# 348
      keytable[keylen].timeout = now + 3600L;
      }
# 349
      if (oldkey) {
        {
# 350
        RSA_free(oldkey);
        }
      }
    }
    {
# 352
    retval___0 = keytable[keylen].key;
    }
  } else {
# 354
    if (longtime < now) {
      goto _L;
    } else {
# 354
      if (longlen != keylen) {
        _L: 
        {
# 355
        oldkey = longkey;
# 356
        longkey = make_temp_key(keylen);
# 357
        longtime = now + 3600L;
# 358
        longlen = keylen;
        }
# 359
        if (oldkey) {
          {
# 360
          RSA_free(oldkey);
          }
        }
      }
    }
    {
# 362
    retval___0 = longkey;
    }
  }
  {
# 364
  leave_critical_section(0);
  }
# 365
  return (retval___0);
}
}
# 368
static RSA *make_temp_key(int keylen ) 
{ RSA *result ;

  {
  {
# 371
  log(7, (char const   *)"Generating %d bit temporary RSA key...", keylen);
# 373
  result = RSA_generate_key(keylen, 65537UL,
                            (void (*)(int  , int  , void * ))((void *)0),
                            (void *)0);
# 377
  log(7, (char const   *)"Temporary RSA key created");
  }
# 378
  return (result);
}
}
# 383
static void verify_init(void) 
{ X509_LOOKUP *lookup ;
  int tmp ;
  int tmp___0 ;
  X509_LOOKUP_METHOD *tmp___1 ;
  int tmp___2 ;
  X509_LOOKUP_METHOD *tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;

  {
# 386
  if (options.verify_level < 0) {
# 387
    return;
  }
# 389
  if (options.verify_level > 1) {
# 389
    if (! options.ca_file) {
# 389
      if (! options.ca_dir) {
        {
# 390
        log(3,
            (char const   *)"Either CApath or CAfile has to be used for authentication");
# 392
        exit(1);
        }
      }
    }
  }
# 395
  if (options.ca_file) {
    {
# 396
    tmp = SSL_CTX_load_verify_locations(ctx, (char const   *)options.ca_file,
                                        (char const   *)((void *)0));
    }
# 396
    if (! tmp) {
      {
# 397
      log(3, (char const   *)"Error loading verify certificates from %s",
          options.ca_file);
# 399
      sslerror("SSL_CTX_load_verify_locations");
# 400
      exit(1);
      }
    }
    {
# 406
    log(7, (char const   *)"Loaded verify certificates from %s", options.ca_file);
    }
  }
# 410
  if (options.ca_dir) {
    {
# 411
    tmp___0 = SSL_CTX_load_verify_locations(ctx, (char const   *)((void *)0),
                                            (char const   *)options.ca_dir);
    }
# 411
    if (! tmp___0) {
      {
# 412
      log(3, (char const   *)"Error setting verify directory to %s",
          options.ca_dir);
# 414
      sslerror("SSL_CTX_load_verify_locations");
# 415
      exit(1);
      }
    }
    {
# 417
    log(7, (char const   *)"Verify directory set to %s", options.ca_dir);
    }
  }
# 420
  if (options.crl_file) {
    goto _L;
  } else {
# 420
    if (options.crl_dir) {
      _L: 
      {
# 421
      revocation_store = X509_STORE_new();
      }
# 422
      if (! revocation_store) {
        {
# 423
        sslerror("X509_STORE_new");
# 424
        exit(1);
        }
      }
# 426
      if (options.crl_file) {
        {
# 427
        tmp___1 = X509_LOOKUP_file();
# 427
        lookup = X509_STORE_add_lookup(revocation_store, tmp___1);
        }
# 429
        if (! lookup) {
          {
# 430
          sslerror("X509_STORE_add_lookup");
# 431
          exit(1);
          }
        }
        {
# 433
        tmp___2 = X509_LOOKUP_ctrl(lookup, 1, (char const   *)options.crl_file,
                                   1L, (char **)((void *)0));
        }
# 433
        if (! tmp___2) {
          {
# 435
          log(3, (char const   *)"Error loading CRLs from %s", options.crl_file);
# 437
          sslerror("X509_LOOKUP_load_file");
# 438
          exit(1);
          }
        }
        {
# 440
        log(7, (char const   *)"Loaded CRLs from %s", options.crl_file);
        }
      }
# 442
      if (options.crl_dir) {
        {
# 443
        tmp___3 = X509_LOOKUP_hash_dir();
# 443
        lookup = X509_STORE_add_lookup(revocation_store, tmp___3);
        }
# 445
        if (! lookup) {
          {
# 446
          sslerror("X509_STORE_add_lookup");
# 447
          exit(1);
          }
        }
        {
# 449
        tmp___4 = X509_LOOKUP_ctrl(lookup, 2, (char const   *)options.crl_dir,
                                   1L, (char **)((void *)0));
        }
# 449
        if (! tmp___4) {
          {
# 451
          log(3, (char const   *)"Error setting CRL directory to %s",
              options.crl_dir);
# 453
          sslerror("X509_LOOKUP_add_dir");
# 454
          exit(1);
          }
        }
        {
# 456
        log(7, (char const   *)"CRL directory set to %s", options.crl_dir);
        }
      }
    }
  }
# 460
  if (options.verify_level == 0) {
    {
# 460
    tmp___5 = 1;
    }
  } else {
    {
# 460
    tmp___5 = options.verify_level;
    }
  }
  {
# 460
  SSL_CTX_set_verify(ctx, tmp___5, & verify_callback);
  }
# 463
  if (options.ca_dir) {
# 463
    if (options.verify_use_only_my) {
      {
# 464
      log(5, (char const   *)"Peer certificate location %s", options.ca_dir);
      }
    }
  }
# 465
  return;
}
}
# 467
static int verify_callback(int preverify_ok , X509_STORE_CTX *callback_ctx ) 
{ char txt[1024] ;
  X509_OBJECT ret ;
  X509_NAME *tmp ;
  char *p ;
  int tmp___0 ;
  char const   *tmp___1 ;
  X509_NAME *tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;

  {
  {
# 472
  tmp = X509_get_subject_name(callback_ctx->current_cert);
# 472
  X509_NAME_oneline(tmp, txt, 1024);
  }
# 474
  while (1) {
    {
# 474
    p = txt;
    }
# 474
    while ((*p)) {
      {
# 474
      tmp___0 = _get__ctype_b((int )(*p));
      }
# 474
      if (! (tmp___0 & 16384)) {
        {
# 474
        (*p) = '.';
        }
      }
      {
# 474
      p ++;
      }
    }
# 474
    break;
  }
# 475
  if (options.verify_level == 0) {
    {
# 476
    log(5, (char const   *)"VERIFY IGNORE: depth=%d, %s",
        callback_ctx->error_depth, txt);
    }
# 478
    return (1);
  }
# 480
  if (! preverify_ok) {
    {
# 482
    tmp___1 = X509_verify_cert_error_string((long )callback_ctx->error);
# 482
    log(4, (char const   *)"VERIFY ERROR: depth=%d, error=%s: %s",
        callback_ctx->error_depth, tmp___1, txt);
    }
# 485
    return (0);
  }
# 487
  if (options.verify_use_only_my) {
# 487
    if (callback_ctx->error_depth == 0) {
      {
# 487
      tmp___2 = X509_get_subject_name(callback_ctx->current_cert);
# 487
      tmp___3 = X509_STORE_get_by_subject(callback_ctx, 1, tmp___2, & ret);
      }
# 487
      if (tmp___3 != 1) {
        {
# 490
        log(4, (char const   *)"VERIFY ERROR ONLY MY: no cert for %s", txt);
        }
# 491
        return (0);
      }
    }
  }
# 493
  if (revocation_store) {
    {
# 493
    tmp___4 = crl_callback(callback_ctx);
    }
# 493
    if (! tmp___4) {
# 494
      return (0);
    }
  }
  {
# 497
  log(5, (char const   *)"VERIFY OK: depth=%d, %s", callback_ctx->error_depth,
      txt);
  }
# 498
  return (1);
}
}
# 502
static int crl_callback(X509_STORE_CTX *callback_ctx ) 
{ X509_STORE_CTX store_ctx ;
  X509_OBJECT obj ;
  X509_NAME *subject ;
  X509_NAME *issuer ;
  X509 *xs ;
  X509_CRL *crl ;
  X509_REVOKED *revoked ;
  EVP_PKEY *pubkey ;
  long serial ;
  BIO *bio ;
  int i ;
  int n ;
  int rc ;
  char *cp ;
  char *cp2 ;
  ASN1_TIME *t ;
  BIO_METHOD *tmp ;
  int tmp___1 ;
  int tmp___2 ;
  ASN1_INTEGER *tmp___4 ;
  int tmp___5 ;

  {
  {
# 519
  xs = X509_STORE_CTX_get_current_cert(callback_ctx);
# 520
  subject = X509_get_subject_name(xs);
# 521
  issuer = X509_get_issuer_name(xs);
# 525
  memset((void *)((char *)(& obj)), 0, sizeof(obj));
# 526
  X509_STORE_CTX_init(& store_ctx, revocation_store, (X509 *)((void *)0),
                      (STACK *)((void *)0));
# 527
  rc = X509_STORE_get_by_subject(& store_ctx, 2, subject, & obj);
# 528
  X509_STORE_CTX_cleanup(& store_ctx);
# 529
  crl = obj.data.crl;
  }
# 530
  if (rc > 0) {
# 530
    if (crl) {
      {
# 533
      tmp = BIO_s_mem();
# 533
      bio = BIO_new(tmp);
# 534
      BIO_printf(bio, (char const   *)"lastUpdate: ");
# 535
      ASN1_UTCTIME_print(bio, (crl->crl)->lastUpdate);
# 536
      BIO_printf(bio, (char const   *)", nextUpdate: ");
# 537
      ASN1_UTCTIME_print(bio, (crl->crl)->nextUpdate);
# 538
      n = (int )BIO_ctrl(bio, 10, 0L, (void *)0);
# 539
      cp = (char *)malloc((unsigned int )(n + 1));
# 540
      n = BIO_read(bio, (void *)cp, n);
# 541
      (*(cp + n)) = '\0';
# 542
      BIO_free(bio);
# 543
      cp2 = X509_NAME_oneline(subject, (char *)((void *)0), 0);
# 544
      log(5, (char const   *)"CA CRL: Issuer: %s, %s", cp2, cp);
# 545
      CRYPTO_free((void *)cp2);
# 546
      free((void *)cp);
# 549
      pubkey = X509_get_pubkey(xs);
# 550
      tmp___1 = X509_CRL_verify(crl, pubkey);
      }
# 550
      if (tmp___1 <= 0) {
        {
# 551
        log(4, (char const   *)"Invalid signature on CRL");
# 552
        X509_STORE_CTX_set_error(callback_ctx, 8);
# 554
        X509_OBJECT_free_contents(& obj);
        }
# 555
        if (pubkey) {
          {
# 556
          EVP_PKEY_free(pubkey);
          }
        }
# 557
        return (0);
      }
# 559
      if (pubkey) {
        {
# 560
        EVP_PKEY_free(pubkey);
        }
      }
      {
# 563
      t = (crl->crl)->nextUpdate;
      }
# 564
      if (! t) {
        {
# 565
        log(4, (char const   *)"Found CRL has invalid nextUpdate field");
# 566
        X509_STORE_CTX_set_error(callback_ctx, 16);
# 568
        X509_OBJECT_free_contents(& obj);
        }
# 569
        return (0);
      }
      {
# 571
      tmp___2 = X509_cmp_current_time(t);
      }
# 571
      if (tmp___2 < 0) {
        {
# 572
        log(4,
            (char const   *)"Found CRL is expired - revoking all certificates until you get updated CRL");
# 574
        X509_STORE_CTX_set_error(callback_ctx, 12);
# 575
        X509_OBJECT_free_contents(& obj);
        }
# 576
        return (0);
      }
      {
# 578
      X509_OBJECT_free_contents(& obj);
      }
    }
  }
  {
# 583
  memset((void *)((char *)(& obj)), 0, sizeof(obj));
# 584
  X509_STORE_CTX_init(& store_ctx, revocation_store, (X509 *)((void *)0),
                      (STACK *)((void *)0));
# 585
  rc = X509_STORE_get_by_subject(& store_ctx, 2, issuer, & obj);
# 586
  X509_STORE_CTX_cleanup(& store_ctx);
# 587
  crl = obj.data.crl;
  }
# 588
  if (rc > 0) {
# 588
    if (crl) {
      {
# 591
      n = sk_num((STACK const   *)(crl->crl)->revoked);
# 595
      i = 0;
      }
# 595
      while (i < n) {
        {
# 597
        revoked = (X509_REVOKED *)sk_value((STACK const   *)(crl->crl)->revoked,
                                           i);
# 601
        tmp___4 = X509_get_serialNumber(xs);
# 601
        tmp___5 = ASN1_INTEGER_cmp(revoked->serialNumber, tmp___4);
        }
# 601
        if (tmp___5 == 0) {
          {
# 603
          serial = ASN1_INTEGER_get(revoked->serialNumber);
# 604
          cp = X509_NAME_oneline(issuer, (char *)((void *)0), 0);
# 605
          log(5,
              (char const   *)"Certificate with serial %ld (0x%lX) revoked per CRL from issuer %s",
              serial, serial, cp);
# 607
          CRYPTO_free((void *)cp);
# 608
          X509_STORE_CTX_set_error(callback_ctx, 23);
# 609
          X509_OBJECT_free_contents(& obj);
          }
# 610
          return (0);
        }
        {
# 595
        i ++;
        }
      }
      {
# 613
      X509_OBJECT_free_contents(& obj);
      }
    }
  }
# 615
  return (1);
}
}
# 619
static void info_callback(SSL const   *s , int where , int ret ) 
{ char const   *tmp ;
  char const   *tmp___1 ;
  char const   *tmp___2 ;
  char const   *tmp___3 ;
  char const   *tmp___4 ;

  {
# 623
  if (where & 1) {
    {
# 624
    tmp = SSL_state_string_long(s);
    }
# 624
    if (where & 4096) {
      {
# 624
      tmp___1 = (char const   *)"connect";
      }
    } else {
# 624
      if (where & 8192) {
        {
# 624
        tmp___1 = (char const   *)"accept";
        }
      } else {
        {
# 624
        tmp___1 = (char const   *)"undefined";
        }
      }
    }
    {
# 624
    log(7, (char const   *)"SSL state (%s): %s", tmp___1, tmp);
    }
  } else {
# 628
    if (where & 16384) {
      {
# 629
      tmp___2 = SSL_alert_desc_string_long(ret);
# 629
      tmp___3 = SSL_alert_type_string_long(ret);
      }
# 629
      if (where & 4) {
        {
# 629
        tmp___4 = (char const   *)"read";
        }
      } else {
        {
# 629
        tmp___4 = (char const   *)"write";
        }
      }
      {
# 629
      log(7, (char const   *)"SSL alert (%s): %s: %s", tmp___4, tmp___3, tmp___2);
      }
    } else {
# 633
      if (where == 32) {
        {
# 634
        print_stats();
        }
      }
    }
  }
# 635
  return;
}
}
# 637
static void print_stats(void) 
{ long tmp ;
  long tmp___0 ;
  long tmp___1 ;
  long tmp___2 ;
  long tmp___3 ;
  long tmp___4 ;
  long tmp___5 ;
  long tmp___6 ;
  long tmp___7 ;
  long tmp___8 ;

  {
  {
# 638
  tmp = SSL_CTX_ctrl(ctx, 20, 0L, (void *)0);
# 638
  log(7, (char const   *)"%4ld items in the session cache", tmp);
# 640
  tmp___0 = SSL_CTX_ctrl(ctx, 21, 0L, (void *)0);
# 640
  log(7, (char const   *)"%4ld client connects (SSL_connect())", tmp___0);
# 642
  tmp___1 = SSL_CTX_ctrl(ctx, 22, 0L, (void *)0);
# 642
  log(7, (char const   *)"%4ld client connects that finished", tmp___1);
# 645
  tmp___2 = SSL_CTX_ctrl(ctx, 23, 0L, (void *)0);
# 645
  log(7, (char const   *)"%4ld client renegotiatations requested", tmp___2);
# 648
  tmp___3 = SSL_CTX_ctrl(ctx, 24, 0L, (void *)0);
# 648
  log(7, (char const   *)"%4ld server connects (SSL_accept())", tmp___3);
# 650
  tmp___4 = SSL_CTX_ctrl(ctx, 25, 0L, (void *)0);
# 650
  log(7, (char const   *)"%4ld server connects that finished", tmp___4);
# 653
  tmp___5 = SSL_CTX_ctrl(ctx, 26, 0L, (void *)0);
# 653
  log(7, (char const   *)"%4ld server renegotiatiations requested", tmp___5);
# 656
  tmp___6 = SSL_CTX_ctrl(ctx, 27, 0L, (void *)0);
# 656
  log(7, (char const   *)"%4ld session cache hits", tmp___6);
# 657
  tmp___7 = SSL_CTX_ctrl(ctx, 29, 0L, (void *)0);
# 657
  log(7, (char const   *)"%4ld session cache misses", tmp___7);
# 658
  tmp___8 = SSL_CTX_ctrl(ctx, 30, 0L, (void *)0);
# 658
  log(7, (char const   *)"%4ld session cache timeouts", tmp___8);
  }
# 659
  return;
}
}
# 661
void sslerror(char *txt ) 
{ unsigned long err ;
  char string[120] ;

  {
  {
# 665
  err = ERR_get_error();
  }
# 666
  if (! err) {
    {
# 667
    log(3, (char const   *)"%s: Peer suddenly disconnected", txt);
    }
# 668
    return;
  }
  {
# 670
  sslerror_stack();
# 671
  ERR_error_string(err, string);
# 672
  log(3, (char const   *)"%s: %lX: %s", txt, err, string);
  }
# 673
  return;
}
}
# 675
static void sslerror_stack(void) 
{ unsigned long err ;
  char string[120] ;

  {
  {
# 679
  err = ERR_get_error();
  }
# 680
  if (! err) {
# 681
    return;
  }
  {
# 682
  sslerror_stack();
# 683
  ERR_error_string(err, string);
# 684
  log(3, (char const   *)"error stack: %lX : %s", err, string);
  }
# 685
  return;
}
}
# 163 "/users/rupak/ccured/include/gcc_3.2/pthread.h"
extern int pthread_create(pthread_t * __restrict  __threadp ,
                          pthread_attr_t const   * __restrict  __attr ,
                          void *(*__start_routine)(void * ) ,
                          void * __restrict  __arg ) ;
# 169
extern pthread_t pthread_self(void) ;
# 195
extern int pthread_attr_init(pthread_attr_t *__attr ) ;
# 201
extern int pthread_attr_setdetachstate(pthread_attr_t *__attr ,
                                       int __detachstate ) ;
# 284
extern int pthread_attr_setstacksize(pthread_attr_t *__attr ,
                                     size_t __stacksize ) ;
# 331
extern int pthread_mutex_init(pthread_mutex_t * __restrict  __mutex ,
                              pthread_mutexattr_t const   * __restrict  __mutex_attr ) ;
# 342
extern int pthread_mutex_lock(pthread_mutex_t *__mutex ) ;
# 352
extern int pthread_mutex_unlock(pthread_mutex_t *__mutex ) ;
# 40 "sthreads.c"
static pthread_mutex_t stunnel_cs[4]  ;
# 42
static pthread_mutex_t lock_cs[33]  ;
# 43
static pthread_attr_t pth_attr  ;
# 45
void enter_critical_section(section_code i ) 
{ 

  {
  {
# 46
  pthread_mutex_lock(stunnel_cs + i);
  }
# 47
  return;
}
}
# 49
void leave_critical_section(section_code i ) 
{ 

  {
  {
# 50
  pthread_mutex_unlock(stunnel_cs + i);
  }
# 51
  return;
}
}
# 53
static void locking_callback(int mode , int type , char const   *file ,
                             int line ) 
{ 

  {
# 58
  if (mode & 1) {
    {
# 59
    pthread_mutex_lock(lock_cs + type);
    }
  } else {
    {
# 61
    pthread_mutex_unlock(lock_cs + type);
    }
  }
# 62
  return;
}
}
# 64
void sthreads_init(void) 
{ int i ;

  {
  {
# 68
  i = 0;
  }
# 68
  while (i < 4) {
    {
# 69
    pthread_mutex_init((pthread_mutex_t */* __restrict  */)(stunnel_cs + i),
                       (pthread_mutexattr_t const   */* __restrict  */)((pthread_mutexattr_t const   *)((void *)0)));
# 68
    i ++;
    }
  }
  {
# 72
  i = 0;
  }
# 72
  while (i < 33) {
    {
# 73
    pthread_mutex_init((pthread_mutex_t */* __restrict  */)(lock_cs + i),
                       (pthread_mutexattr_t const   */* __restrict  */)((pthread_mutexattr_t const   *)((void *)0)));
# 72
    i ++;
    }
  }
  {
# 74
  CRYPTO_set_id_callback(& stunnel_thread_id);
# 75
  CRYPTO_set_locking_callback(& locking_callback);
# 77
  pthread_attr_init(& pth_attr);
# 78
  pthread_attr_setdetachstate(& pth_attr, 1);
# 79
  pthread_attr_setstacksize(& pth_attr, 65536U);
  }
# 80
  return;
}
}
# 82
unsigned long stunnel_process_id(void) 
{ unsigned long tmp ;

  {
  {
# 83
  tmp = (unsigned long )getpid();
  }
# 83
  return (tmp);
}
}
# 86
unsigned long stunnel_thread_id(void) 
{ unsigned long tmp ;

  {
  {
# 87
  tmp = pthread_self();
  }
# 87
  return (tmp);
}
}
# 90
int create_client(int ls , int s , void *arg , void *(*cli)(void * ) ) 
{ pthread_t thread ;
  sigset_t newmask ;
  sigset_t oldmask ;
  int tmp ;

  {
  {
# 97
  sigemptyset(& newmask);
# 98
  sigaddset(& newmask, 17);
# 99
  sigaddset(& newmask, 15);
# 100
  sigaddset(& newmask, 3);
# 101
  sigaddset(& newmask, 2);
# 102
  sigaddset(& newmask, 1);
# 103
  pthread_sigmask(0,
                  (__sigset_t const   */* __restrict  */)((__sigset_t const   *)(& newmask)),
                  (__sigset_t */* __restrict  */)(& oldmask));
# 105
  tmp = pthread_create((pthread_t */* __restrict  */)(& thread),
                       (pthread_attr_t const   */* __restrict  */)((pthread_attr_t const   *)(& pth_attr)),
                       cli, (void */* __restrict  */)arg);
  }
# 105
  if (tmp) {
    {
# 107
    pthread_sigmask(2,
                    (__sigset_t const   */* __restrict  */)((__sigset_t const   *)(& oldmask)),
                    (__sigset_t */* __restrict  */)((__sigset_t *)((void *)0)));
    }
# 109
    if (s >= 0) {
      {
# 110
      close(s);
      }
    }
# 111
    return (-1);
  }
  {
# 114
  pthread_sigmask(2,
                  (__sigset_t const   */* __restrict  */)((__sigset_t const   *)(& oldmask)),
                  (__sigset_t */* __restrict  */)((__sigset_t *)((void *)0)));
  }
# 116
  return (0);
}
}
# 40 "stunnel.c"
static void daemon_loop(void) ;
# 41
static void accept_connection(LOCAL_OPTIONS *opt ) ;
# 42
static void get_limits(void) ;
# 44
static void drop_privileges(void) ;
# 45
static void daemonize(void) ;
# 46
static void create_pid(void) ;
# 47
static void delete_pid(void) ;
# 49
static void setnonblock(int sock , unsigned long l ) ;
# 53
static void signal_handler(int sig ) ;
# 56
int num_clients   = 0;
# 61
int main(int argc , char **argv ) 
{ char *tmp ;
  char *tmp___0 ;

  {
  __initialize__();
# 63
  if (argc > 2) {
    {
# 63
    tmp = (*(argv + 2));
    }
  } else {
    {
# 63
    tmp = (char *)((void *)0);
    }
  }
# 63
  if (argc > 1) {
    {
# 63
    tmp___0 = (*(argv + 1));
    }
  } else {
    {
# 63
    tmp___0 = (char *)((void *)0);
    }
  }
  {
# 63
  main_initialize(tmp___0, tmp);
# 65
  signal(13, (void (*)(int  ))1);
# 66
  signal(15, & signal_handler);
# 67
  signal(3, & signal_handler);
# 68
  signal(2, & signal_handler);
# 69
  signal(1, & signal_handler);
# 72
  main_execute();
  }
# 74
  return (0);
}
}
# 78
void main_initialize(char *arg1 , char *arg2 ) 
{ struct stat st ;
  char *tmp ;
  int tmp___0 ;

  {
  {
# 81
  sthreads_init();
# 82
  parse_config(arg1, arg2);
# 83
  log_open();
# 84
  tmp = stunnel_info();
# 84
  log(5, (char const   *)"%s", tmp);
  }
# 87
  if (! options.key) {
    {
# 88
    options.key = options.cert;
    }
  }
# 89
  if (options.option.cert) {
    {
# 90
    tmp___0 = stat__extinline((char const   *)options.key, & st);
    }
# 90
    if (tmp___0) {
      {
# 91
      ioerror(options.key);
# 92
      exit(1);
      }
    }
# 95
    if (st.st_mode & 7U) {
      {
# 96
      log(4, (char const   *)"Wrong permissions on %s", options.key);
      }
    }
  }
# 99
  return;
}
}
# 101
void main_execute(void) 
{ void *tmp ;

  {
  {
# 102
  context_init();
  }
# 104
  if (local_options.next) {
    {
# 105
    daemon_loop();
    }
  } else {
    {
# 108
    max_fds = 1024;
# 109
    drop_privileges();
# 111
    num_clients = 1;
# 112
    tmp = alloc_client_session(& local_options, 0, 1);
# 112
    client(tmp);
    }
  }
  {
# 115
  context_free();
# 116
  log_close();
  }
# 117
  return;
}
}
# 119
static void daemon_loop(void) 
{ struct sockaddr_in addr ;
  fd_set base_set ;
  fd_set current_set ;
  int n ;
  LOCAL_OPTIONS *opt ;
  unsigned int __i ;
  fd_set *__arr ;
  int tmp ;
  int tmp___0 ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp___1 ;
  int tmp___2 ;
  register unsigned short __v___0 ;
  register unsigned short __x___0 ;
  int tmp___3 ;
  int tmp___4 ;
  void *tmp___5 ;
  int *tmp___6 ;
  int tmp___7 ;

  {
  {
# 125
  get_limits();
  }
# 126
  while (1) {
    {
# 126
    __arr = & base_set;
# 126
    __i = 0U;
    }
# 126
    while (__i < sizeof(fd_set ) / sizeof(__fd_mask )) {
      {
# 126
      __arr->__fds_bits[__i] = 0L;
# 126
      __i ++;
      }
    }
# 126
    break;
  }
# 127
  if (! local_options.next) {
    {
# 128
    log(3, (char const   *)"No connections defined in config file");
# 129
    exit(1);
    }
  }
  {
# 132
  num_clients = 0;
# 135
  n = 0;
# 136
  opt = local_options.next;
  }
# 136
  while (opt) {
# 137
    if (! opt->option.accept) {
      goto __Cont;
    }
    {
# 139
    opt->fd = socket(2, 1, 0);
    }
# 139
    if (opt->fd < 0) {
      {
# 140
      sockerror("local socket");
# 141
      exit(1);
      }
    }
    {
# 143
    tmp = alloc_fd(opt->fd);
    }
# 143
    if (tmp) {
      {
# 144
      exit(1);
      }
    }
    {
# 145
    tmp___0 = set_socket_options(opt->fd, 0);
    }
# 145
    if (tmp___0 < 0) {
      {
# 146
      exit(1);
      }
    }
    {
# 147
    memset((void *)(& addr), 0, sizeof(addr));
# 148
    addr.sin_family = (unsigned short )2;
# 149
    addr.sin_addr.s_addr = (*(opt->localnames));
# 150
    addr.sin_port = opt->localport;
# 151
    safe_ntoa(opt->local_address, addr.sin_addr);
# 152
    tmp___2 = bind(opt->fd, (void const   *)((struct sockaddr *)(& addr)),
                   sizeof(addr));
    }
# 152
    if (tmp___2) {
      {
# 154
      __x = addr.sin_port;
# 154
      tmp___1 = ((int )__x);
      }
# 154
      if (tmp___1) {
        {
# 154
        __v = (unsigned short )((((int )__x >> 8) & 255) |
                                (((int )__x & 255) << 8));
        }
      } else {
        {
# 154
        __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
        }
      }
      {
# 154
      log(3, (char const   *)"Error binding %s to %s:%d", opt->servname,
          opt->local_address, __v);
# 155
      sockerror("bind");
# 156
      exit(1);
      }
    }
    {
# 159
    __x___0 = addr.sin_port;
# 159
    tmp___3 = ((int )__x___0);
    }
# 159
    if (tmp___3) {
      {
# 159
      __v___0 = (unsigned short )((((int )__x___0 >> 8) & 255) |
                                  (((int )__x___0 & 255) << 8));
      }
    } else {
      {
# 159
      __asm__  ("rorw $8, %w0": "=r" (__v___0): "0" (__x___0): "cc");
      }
    }
    {
# 159
    log(7, (char const   *)"%s bound to %s:%d", opt->servname,
        opt->local_address, __v___0);
# 160
    tmp___4 = listen(opt->fd, 5);
    }
# 160
    if (tmp___4) {
      {
# 161
      sockerror("listen");
# 162
      exit(1);
      }
    }
    {
# 165
    fcntl(opt->fd, 2, 1);
# 167
    base_set.__fds_bits[(unsigned int )opt->fd / (8U * sizeof(__fd_mask ))] = base_set.__fds_bits[(unsigned int )opt->fd /
                                                                                                  (8U *
                                                                                                   sizeof(__fd_mask ))] |
                                                                              (1L <<
                                                                               (unsigned int )opt->fd %
                                                                               (8U *
                                                                                sizeof(__fd_mask )));
    }
# 168
    if (opt->fd > n) {
      {
# 169
      n = opt->fd;
      }
    }
    __Cont: 
    {
# 136
    opt = opt->next;
    }
  }
  {
# 173
  sselect_init(& base_set, & n);
  }
# 177
  if (! options.option.foreground) {
    {
# 178
    daemonize();
    }
  }
  {
# 179
  drop_privileges();
# 180
  create_pid();
# 184
  opt = local_options.next;
  }
# 184
  while (opt) {
# 185
    if (opt->option.accept) {
      goto __Cont___0;
    }
    {
# 187
    enter_critical_section(2);
# 188
    num_clients = num_clients + 1;
# 189
    leave_critical_section(2);
# 190
    tmp___5 = alloc_client_session(opt, -1, -1);
# 190
    create_client(-1, -1, tmp___5, & client);
    }
    __Cont___0: 
    {
# 184
    opt = opt->next;
    }
  }
# 193
  while (1) {
    {
# 194
    memcpy((void */* __restrict  */)((void *)(& current_set)),
           (void const   */* __restrict  */)((void const   *)(& base_set)),
           sizeof(fd_set ));
# 195
    tmp___7 = sselect(n + 1, & current_set, (fd_set *)((void *)0),
                      (fd_set *)((void *)0), (struct timeval *)((void *)0));
    }
# 195
    if (tmp___7 < 0) {
      {
# 197
      tmp___6 = __errno_location();
# 197
      log_error(6, (*tmp___6), "main loop select");
      }
    } else {
      {
# 199
      opt = local_options.next;
      }
# 199
      while (opt) {
# 200
        if (current_set.__fds_bits[(unsigned int )opt->fd /
                                   (8U * sizeof(__fd_mask ))] &
            (1L << (unsigned int )opt->fd % (8U * sizeof(__fd_mask )))) {
          {
# 201
          accept_connection(opt);
          }
        }
        {
# 199
        opt = opt->next;
        }
      }
    }
  }
  {
# 203
  log(3, (char const   *)"INTERNAL ERROR: End of infinite loop 8-)");
  }
# 204
  return;
}
}
# 206
static void accept_connection(LOCAL_OPTIONS *opt ) 
{ struct sockaddr_in addr ;
  int err ;
  int s ;
  int addrlen ;
  int *tmp ;
  register unsigned short __v ;
  register unsigned short __x ;
  int tmp___0 ;
  char *tmp___1 ;
  int tmp___2 ;
  void *tmp___3 ;
  int tmp___4 ;

  {
  {
# 208
  addrlen = (int )sizeof(addr);
  }
# 210
  while (1) {
    {
# 211
    s = accept(opt->fd,
               (void */* __restrict  */)((void *)((struct sockaddr *)(& addr))),
               (socklen_t */* __restrict  */)((socklen_t *)(& addrlen)));
    }
# 212
    if (s < 0) {
      {
# 213
      tmp = __errno_location();
# 213
      err = (*tmp);
      }
    }
# 210
    if (s < 0) {
# 210
      if (! (err == 4)) {
# 210
        break;
      }
    } else {
# 210
      break;
    }
  }
# 215
  if (s < 0) {
    {
# 216
    sockerror("accept");
    }
# 217
    switch (err) {
    case 24: 
    {

    }
    case 23: 
    {

    }
    case 105: 
    {

    }
    case 12: 
    {
# 224
    sleep(1U);
    }
    default: 
    {

    }
    }
# 228
    return;
  }
  {
# 230
  enter_critical_section(1);
# 232
  __x = addr.sin_port;
# 232
  tmp___0 = ((int )__x);
  }
# 232
  if (tmp___0) {
    {
# 232
    __v = (unsigned short )((((int )__x >> 8) & 255) | (((int )__x & 255) << 8));
    }
  } else {
    {
# 232
    __asm__  ("rorw $8, %w0": "=r" (__v): "0" (__x): "cc");
    }
  }
  {
# 232
  tmp___1 = inet_ntoa(addr.sin_addr);
# 232
  log(7, (char const   *)"%s accepted FD=%d from %s:%d", opt->servname, s,
      tmp___1, __v);
# 233
  leave_critical_section(1);
  }
# 234
  if (num_clients >= max_clients) {
    {
# 235
    log(4, (char const   *)"Connection rejected: too many clients (>=%d)",
        max_clients);
# 237
    close(s);
    }
# 238
    return;
  }
  {
# 240
  tmp___2 = alloc_fd(s);
  }
# 240
  if (tmp___2) {
# 241
    return;
  }
  {
# 243
  fcntl(s, 2, 1);
# 245
  tmp___3 = alloc_client_session(opt, s, s);
# 245
  tmp___4 = create_client(opt->fd, s, tmp___3, & client);
  }
# 245
  if (tmp___4) {
    {
# 246
    log(3, (char const   *)"Connection rejected: create_client failed");
# 247
    close(s);
    }
# 248
    return;
  }
  {
# 250
  enter_critical_section(2);
# 251
  num_clients = num_clients + 1;
# 252
  leave_critical_section(2);
  }
# 253
  return;
}
}
# 255
static void get_limits(void) 
{ int fds_ulimit ;
  char const   *tmp ;

  {
  {
# 260
  fds_ulimit = -1;
# 263
  fds_ulimit = (int )sysconf(4);
  }
# 264
  if (fds_ulimit < 0) {
    {
# 265
    ioerror("sysconf");
    }
  }
# 275
  if (fds_ulimit < 1024) {
    {
# 275
    max_fds = fds_ulimit;
    }
  } else {
    {
# 275
    max_fds = 1024;
    }
  }
# 276
  if (max_fds < 16) {
    {
# 277
    max_fds = 16;
    }
  }
# 278
  if (max_fds >= 256) {
    {
# 278
    max_clients = (max_fds * 125) / 256;
    }
  } else {
    {
# 278
    max_clients = (max_fds - 6) / 2;
    }
  }
# 279
  if (fds_ulimit < 0) {
    {
# 279
    tmp = (char const   *)" (unlimited)";
    }
  } else {
    {
# 279
    tmp = (char const   *)"";
    }
  }
  {
# 279
  log(5,
      (char const   *)"FD_SETSIZE=%d, file ulimit=%d%s -> %d clients allowed",
      1024, fds_ulimit, tmp, max_clients);
  }
# 282
  return;
}
}
# 286
static void drop_privileges(void) 
{ int uid ;
  int gid ;
  struct group *gr ;
  gid_t gr_list[1] ;
  struct passwd *pw ;
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;
  int tmp___2 ;
  int tmp___3 ;
  int tmp___4 ;
  int tmp___5 ;

  {
  {
# 287
  uid = 0;
# 287
  gid = 0;
  }
# 295
  if (options.setgid_group) {
    {
# 296
    gr = getgrnam((char const   *)options.setgid_group);
    }
# 297
    if (gr) {
      {
# 298
      gid = (int )gr->gr_gid;
      }
    } else {
      {
# 299
      tmp = atoi((char const   *)options.setgid_group);
      }
# 299
      if (tmp) {
        {
# 300
        gid = atoi((char const   *)options.setgid_group);
        }
      } else {
        {
# 302
        log(3, (char const   *)"Failed to get GID for group %s",
            options.setgid_group);
# 304
        exit(1);
        }
      }
    }
  }
# 307
  if (options.setuid_user) {
    {
# 308
    pw = getpwnam((char const   *)options.setuid_user);
    }
# 309
    if (pw) {
      {
# 310
      uid = (int )pw->pw_uid;
      }
    } else {
      {
# 311
      tmp___0 = atoi((char const   *)options.setuid_user);
      }
# 311
      if (tmp___0) {
        {
# 312
        uid = atoi((char const   *)options.setuid_user);
        }
      } else {
        {
# 314
        log(3, (char const   *)"Failed to get UID for user %s",
            options.setuid_user);
# 316
        exit(1);
        }
      }
    }
  }
# 322
  if (options.chroot_dir) {
    {
# 323
    tmp___1 = chroot((char const   *)options.chroot_dir);
    }
# 323
    if (tmp___1) {
      {
# 324
      sockerror("chroot");
# 325
      exit(1);
      }
    }
    {
# 327
    tmp___2 = chdir((char const   *)"/");
    }
# 327
    if (tmp___2) {
      {
# 328
      sockerror("chdir");
# 329
      exit(1);
      }
    }
  }
# 335
  if (gid) {
    {
# 336
    tmp___3 = setgid((unsigned int )gid);
    }
# 336
    if (tmp___3) {
      {
# 337
      sockerror("setgid");
# 338
      exit(1);
      }
    }
    {
# 341
    gr_list[0] = (unsigned int )gid;
# 342
    tmp___4 = setgroups(1U, (__gid_t const   *)(gr_list));
    }
# 342
    if (tmp___4) {
      {
# 343
      sockerror("setgroups");
# 344
      exit(1);
      }
    }
  }
# 348
  if (uid) {
    {
# 349
    tmp___5 = setuid((unsigned int )uid);
    }
# 349
    if (tmp___5) {
      {
# 350
      sockerror("setuid");
# 351
      exit(1);
      }
    }
  }
# 354
  return;
}
}
# 356
static void daemonize(void) 
{ int tmp ;

  {
  {
# 358
  tmp = daemon(0, 0);
  }
# 358
  if (tmp == -1) {
    {
# 359
    ioerror("daemon");
# 360
    exit(1);
    }
  }
  {
# 378
  setsid();
  }
# 380
  return;
}
}
# 382
static void create_pid(void) 
{ int pf ;
  char pid[1024] ;
  size_t tmp___0 ;

  {
# 386
  if (! options.pidfile) {
    {
# 387
    log(7, (char const   *)"No pid file being created");
    }
# 388
    return;
  }
# 390
  if ((int )(*(options.pidfile + 0)) != 47) {
    {
# 391
    log(3, (char const   *)"Pid file (%s) must be full path name",
        options.pidfile);
# 395
    exit(1);
    }
  }
  {
# 397
  options.dpid = (unsigned long )getpid();
# 400
  unlink((char const   *)options.pidfile);
# 401
  pf = open((char const   *)options.pidfile, 705, 420);
  }
# 401
  if (pf == -1) {
    {
# 402
    log(3, (char const   *)"Cannot create pid file %s", options.pidfile);
# 403
    ioerror("create");
# 404
    exit(1);
    }
  }
  {
# 406
  sprintf((char */* __restrict  */)(pid),
          (char const   */* __restrict  */)((char const   *)"%lu\n"),
          options.dpid);
# 407
  tmp___0 = strlen((char const   *)(pid));
# 407
  write(pf, (void const   *)(pid), tmp___0);
# 408
  close(pf);
# 409
  log(7, (char const   *)"Created pid file %s", options.pidfile);
# 410
  atexit(& delete_pid);
  }
# 411
  return;
}
}
# 413
static void delete_pid(void) 
{ __pid_t tmp ;
  int tmp___0 ;

  {
  {
# 414
  log(7, (char const   *)"removing pid file %s", options.pidfile);
# 415
  tmp = getpid();
  }
# 415
  if ((unsigned long )tmp != options.dpid) {
# 416
    return;
  }
  {
# 417
  tmp___0 = unlink((char const   *)options.pidfile);
  }
# 417
  if (tmp___0 < 0) {
    {
# 418
    ioerror(options.pidfile);
    }
  }
# 419
  return;
}
}
# 425
static char *type_str[3]   = {"accept", "local", "remote"};
# 422
int set_socket_options(int s , int type ) 
{ SOCK_OPT *ptr ;
  int opt_size ;
  size_t tmp ;
  int tmp___0 ;

  {
  {
# 428
  ptr = sock_opts;
  }
# 428
  while (ptr->opt_str) {
# 429
    if (! ptr->opt_val[type]) {
      goto __Cont;
    }
# 431
    switch (ptr->opt_type) {
    case 3: 
    {
# 433
    opt_size = (int )sizeof(struct linger );
    }
# 433
    break;
    case 4: 
    {
# 435
    opt_size = (int )sizeof(struct timeval );
    }
# 435
    break;
    case 5: 
    {
# 437
    tmp = strlen((char const   *)((ptr->opt_val[type])->c_val));
# 437
    opt_size = (int )(tmp + 1U);
    }
# 437
    break;
    default: 
    {
# 439
    opt_size = (int )sizeof(int );
    }
# 439
    break;
    }
    {
# 441
    tmp___0 = setsockopt(s, ptr->opt_level, ptr->opt_name,
                         (void const   *)((void *)ptr->opt_val[type]),
                         (unsigned int )opt_size);
    }
# 441
    if (tmp___0) {
      {
# 443
      sockerror(ptr->opt_str);
      }
# 444
      return (-1);
    } else {
      {
# 446
      log(7, (char const   *)"%s option set on %s socket", ptr->opt_str,
          type_str[type]);
      }
    }
    __Cont: 
    {
# 428
    ptr ++;
    }
  }
# 450
  return (0);
}
}
# 453
void ioerror(char *txt ) 
{ int *tmp ;

  {
  {
# 454
  tmp = __errno_location();
# 454
  log_error(3, (*tmp), txt);
  }
# 455
  return;
}
}
# 457
void sockerror(char *txt ) 
{ int *tmp ;

  {
  {
# 458
  tmp = __errno_location();
# 458
  log_error(3, (*tmp), txt);
  }
# 459
  return;
}
}
# 461
void log_error(int level , int error , char *txt ) 
{ char *tmp ;

  {
  {
# 462
  tmp = my_strerror(error);
# 462
  log(level, (char const   *)"%s: %s (%d)", txt, tmp, error);
  }
# 463
  return;
}
}
# 465
char *my_strerror(int errnum ) 
{ char *tmp ;

  {
# 466
  switch (errnum) {
  default: 
  {
# 574
  tmp = strerror(errnum);
  }
# 574
  return (tmp);
  }
# 574
  return ((char *)0);
}
}
# 580
static void signal_handler(int sig ) 
{ int tmp ;

  {
# 581
  if (sig == 15) {
    {
# 581
    tmp = 5;
    }
  } else {
    {
# 581
    tmp = 3;
    }
  }
  {
# 581
  log(tmp, (char const   *)"Received signal %d; terminating", sig);
# 583
  exit(3);
  }
# 583
  return;
}
}
# 589
static char retval[1024]  ;
# 588
char *stunnel_info(void) 
{ size_t tmp ;
  size_t tmp___0 ;
  size_t tmp___1 ;
  size_t tmp___2 ;
  char const   * __restrict  tmp___3 ;

  {
  {
# 591
  retval[1023] = '\0';
# 591
  strncpy((char */* __restrict  */)(retval),
          (char const   */* __restrict  */)((char const   *)"stunnel 4.05 on i686-pc-linux-gnu"),
          1023U);
# 593
  retval[1023] = '\0';
# 593
  tmp = strlen((char const   *)(retval));
# 593
  strncat((char */* __restrict  */)(retval),
          (char const   */* __restrict  */)((char const   *)" PTHREAD"),
          (1024U - tmp) - 1U);
# 602
  retval[1023] = '\0';
# 602
  tmp___0 = strlen((char const   *)(retval));
# 602
  strncat((char */* __restrict  */)(retval),
          (char const   */* __restrict  */)((char const   *)"+LIBWRAP"),
          (1024U - tmp___0) - 1U);
# 604
  retval[1023] = '\0';
# 604
  tmp___1 = strlen((char const   *)(retval));
# 604
  strncat((char */* __restrict  */)(retval),
          (char const   */* __restrict  */)((char const   *)" with "),
          (1024U - tmp___1) - 1U);
# 605
  retval[1023] = '\0';
# 605
  tmp___2 = strlen((char const   *)(retval));
# 605
  tmp___3 = (char const   */* __restrict  */)SSLeay_version(0);
# 605
  strncat((char */* __restrict  */)(retval), tmp___3, (1024U - tmp___2) - 1U);
  }
# 606
  return (retval);
}
}
# 609
int alloc_fd(int sock ) 
{ 

  {
# 611
  if (sock >= max_fds) {
    {
# 612
    log(3, (char const   *)"File descriptor out of range (%d>=%d)", sock,
        max_fds);
# 614
    close(sock);
    }
# 615
    return (-1);
  }
  {
# 618
  setnonblock(sock, 1UL);
  }
# 619
  return (0);
}
}
# 627
static void setnonblock(int sock , unsigned long l ) 
{ int retval___0 ;
  int flags ;
  int *tmp ;
  int *tmp___0 ;
  char const   *tmp___1 ;

  {
# 630
  while (1) {
    {
# 631
    flags = fcntl(sock, 3, 0);
    }
# 630
    if (flags < 0) {
      {
# 630
      tmp = __errno_location();
      }
# 630
      if (! ((*tmp) == 4)) {
# 630
        break;
      }
    } else {
# 630
      break;
    }
  }
# 633
  if (l) {
    {
# 633
    flags = flags | 2048;
    }
  } else {
    {
# 633
    flags = flags & -2049;
    }
  }
# 634
  while (1) {
    {
# 635
    retval___0 = fcntl(sock, 4, flags);
    }
# 634
    if (retval___0 < 0) {
      {
# 634
      tmp___0 = __errno_location();
      }
# 634
      if (! ((*tmp___0) == 4)) {
# 634
        break;
      }
    } else {
# 634
      break;
    }
  }
# 637
  if (retval___0 < 0) {
    {
# 641
    sockerror("nonblocking");
    }
  } else {
# 643
    if (l) {
      {
# 643
      tmp___1 = (char const   *)"non-";
      }
    } else {
      {
# 643
      tmp___1 = (char const   *)"";
      }
    }
    {
# 643
    log(7, (char const   *)"FD %d in %sblocking mode", sock, tmp___1);
    }
  }
# 645
  return;
}
}
# 647
char *safe_ntoa(char *text , struct in_addr in ) 
{ char const   * __restrict  tmp ;

  {
  {
# 648
  enter_critical_section(1);
# 649
  tmp = (char const   */* __restrict  */)inet_ntoa(in);
# 649
  strncpy((char */* __restrict  */)text, tmp, 15U);
# 650
  leave_critical_section(1);
# 651
  (*(text + 15)) = '\0';
  }
# 652
  return (text);
}
}
# 34 "/usr/include/pty.h"
extern int openpty(int *__amaster , int *__aslave , char *__name ,
                   struct termios *__termp , struct winsize *__winp ) ;
# 64 "pty.c"
int pty_allocate(int *ptyfd , int *ttyfd , char *namebuf , int namebuflen ) 
{ char buf[64] ;
  int i ;

  {
  {
# 70
  i = openpty(ptyfd, ttyfd, buf, (struct termios *)((void *)0),
              (struct winsize *)((void *)0));
  }
# 71
  if (i < 0) {
    {
# 72
    ioerror("openpty");
    }
# 73
    return (-1);
  }
  {
# 75
  (*(namebuf + 1023)) = '\0';
# 75
  strncpy((char */* __restrict  */)namebuf,
          (char const   */* __restrict  */)((char const   *)(buf)), 1023U);
  }
# 76
  return (0);
}
}
void __initialize__(void) 
{ 

  {
# 254 "/users/rupak/ccured/include/string_wrappers.h"
  saved_str = (char const   *)((void *)0);
# 50 "client.c"
  allow_severity = 5;
# 51
  deny_severity = 4;
# 55
  sid_ctx = (unsigned char *)"stunnel SID";
# 34 "log.c"
  outfile = (FILE *)((void *)0);
# 56 "options.c"
  option_not_found = "Specified option name is not valid here";
# 1327
  on = 1;
# 68 "ssl.c"
  revocation_store = (X509_STORE *)((void *)0);
# 323
  initialized = 0;
# 328
  longkey = (RSA *)((void *)0);
# 329
  longlen = 0;
# 330
  longtime = (long )0;
# 56 "stunnel.c"
  num_clients = 0;
}
}
