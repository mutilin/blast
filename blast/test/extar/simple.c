int x;

int m __attribute__((lock));

void lock() { // __attribute__((atomic)) 
  while(m!=0);
  m = 1;
}

void unlock() {
  m = 0;
}

int main() // __attribute__((atomic)) 
{
	int __BLAST_NONDET;
        int local;

  while (1) {
  lock();
  x = 1;
  unlock();

  lock();
  local = x;
  unlock();

/*  if(local!=1) 
    ERROR: goto ERROR;
  lock();
  x = 1;
  unlock(); */
  }
}


