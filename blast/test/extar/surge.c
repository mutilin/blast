//#define dbg(mode, format, ...) ((void)0)
//#define dbg_clear(mode, format, ...) ((void)0)
//#define dbg_active(mode) 0
# 60 "/usr/local/avr/include/inttypes.h"
typedef signed char int8_t;




typedef unsigned char uint8_t;
# 83 "/usr/local/avr/include/inttypes.h" 3
typedef int int16_t;




typedef unsigned int uint16_t;


/**********************************/
int __BLAST_interrupt_enabled;
int __BLAST_task_enabled;
int __BLAST_packetReceived_enabled ;
int __BLAST_timer1_interrupt;
/**********************************/







typedef long int32_t;




typedef unsigned long uint32_t;
#line 117
typedef long long int64_t;




typedef unsigned long long uint64_t;
#line 134
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 213 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef unsigned int size_t;
#line 325
typedef int wchar_t;
# 60 "/usr/local/avr/include/stdlib.h"
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;


typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *, const void *);
# 172 "/usr/local/avr/include/stdlib.h" 3
extern void qsort(void *__base, size_t __nmemb, size_t __size, 
__compar_fn_t __compar);
# 151 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int ptrdiff_t;
# 85 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
typedef unsigned char bool;
enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};

uint16_t TOS_LOCAL_ADDRESS = 1;

enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};
static inline 

uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t  result_t;
static inline 






result_t rcombine(result_t r1, result_t r2);
static inline 
#line 121
result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4);





enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 81 "/usr/local/avr/include/avr/pgmspace.h"
typedef void __attribute((__progmem__)) prog_void;
typedef char __attribute((__progmem__)) prog_char;
typedef unsigned char __attribute((__progmem__)) prog_uchar;
typedef int __attribute((__progmem__)) prog_int;
typedef long __attribute((__progmem__)) prog_long;
typedef long long __attribute((__progmem__)) prog_long_long;
# 124 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};
static inline 
void TOSH_wait(void);
static inline 




void TOSH_sleep(void);









typedef uint8_t __nesc_atomic_t;

__inline __nesc_atomic_t  __nesc_atomic_start(void );






__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg);
static 



__inline void __nesc_enable_interrupt(void);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RED_LED_PIN(void);
#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void);
static __inline void TOSH_SET_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void);
static __inline void TOSH_SET_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void);

static __inline void TOSH_SET_UD_PIN(void);
#line 61
static __inline void TOSH_CLR_UD_PIN(void);
static __inline void TOSH_SET_INC_PIN(void);
#line 62
static __inline void TOSH_CLR_INC_PIN(void);
static __inline void TOSH_SET_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void);
static __inline void TOSH_SET_POT_POWER_PIN(void);
#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void);
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void);
#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void);

static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void);




static __inline void TOSH_SET_INT1_PIN(void);
#line 72
static __inline void TOSH_MAKE_INT1_OUTPUT(void);
static __inline void TOSH_CLR_INT2_PIN(void);
#line 73
static __inline void TOSH_MAKE_INT2_INPUT(void);


static __inline int TOSH_READ_RFM_RXD_PIN(void);
static __inline void TOSH_CLR_RFM_TXD_PIN(void);
#line 77
static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void);
#line 77
static __inline void TOSH_MAKE_RFM_TXD_INPUT(void);
static __inline void TOSH_SET_RFM_CTL0_PIN(void);
#line 78
static __inline void TOSH_CLR_RFM_CTL0_PIN(void);
#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void);
static __inline void TOSH_SET_RFM_CTL1_PIN(void);
#line 79
static __inline void TOSH_CLR_RFM_CTL1_PIN(void);
#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void);

static __inline void TOSH_MAKE_PW0_OUTPUT(void);
static __inline void TOSH_MAKE_PW1_OUTPUT(void);
static __inline void TOSH_SET_PW2_PIN(void);
#line 83
static __inline void TOSH_CLR_PW2_PIN(void);
#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void);
static __inline void TOSH_MAKE_PW3_OUTPUT(void);
static __inline void TOSH_MAKE_PW4_OUTPUT(void);
static __inline void TOSH_MAKE_PW5_OUTPUT(void);
static __inline void TOSH_MAKE_PW6_OUTPUT(void);
static __inline void TOSH_MAKE_PW7_OUTPUT(void);









static __inline void TOSH_SET_UART_RXD0_PIN(void);


static __inline void TOSH_SET_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_CLR_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_OUTPUT(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void);
static inline 
void TOSH_SET_PIN_DIRECTIONS(void );
#line 146
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};


enum __nesc_unnamed4249 {

  TOSH_ACTUAL_VOLTAGE_PORT = 7
};
enum __nesc_unnamed4250 {

  TOS_ADC_VOLTAGE_PORT = 7
};
# 54 "C:/cygwin/opt/tinyos-1.x/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 




  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
typedef struct __nesc_unnamed4252 {
  void (*tp)(void);
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {
  TOSH_MAX_TASKS = 8, 
  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
volatile uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;
static inline 

void TOSH_wait(void );
static inline void TOSH_sleep(void );
static inline 
void TOSH_sched_init(void );
#line 98
bool  TOS_post(void (*tp)(void));
static inline 
#line 139
bool TOSH_run_next_task(void);
static inline 
#line 162
void TOSH_run_task(void);
static inline 
# 137 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
void *nmemcpy(void *to, const void *from, size_t n);
static inline 








void *nmemset(void *to, int val, size_t n);
# 33 "Surge.h"
int INITIAL_TIMER_RATE = 2048;
int FOCUS_TIMER_RATE = 1000;
int FOCUS_NOTME_TIMER_RATE = 1000;




enum __nesc_unnamed4254 {
  SURGE_TYPE_SENSORREADING = 0, 
  SURGE_TYPE_ROOTBEACON = 1, 
  SURGE_TYPE_SETRATE = 2, 
  SURGE_TYPE_SLEEP = 3, 
  SURGE_TYPE_WAKEUP = 4, 
  SURGE_TYPE_FOCUS = 5, 
  SURGE_TYPE_UNFOCUS = 6
};


typedef struct SurgeMsg {
  uint8_t type;
  uint16_t reading;
  uint16_t parentaddr;
} __attribute((packed))  SurgeMsg;

enum __nesc_unnamed4255 {
  AM_SURGEMSG = 17
};
# 32 "SurgeCmd.h"
typedef struct SurgeCmdMsg {
  uint8_t type;
  union __nesc_unnamed4256 {

    uint32_t newrate;

    uint16_t focusaddr;
  } args;
} 
__attribute((packed))  SurgeCmdMsg;

enum __nesc_unnamed4257 {
  AM_SURGECMDMSG = 18
};
# 49 "C:/cygwin/opt/tinyos-1.x/tos/types/AM.h"
enum __nesc_unnamed4258 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};





enum __nesc_unnamed4259 {
  TOS_DEFAULT_AM_GROUP = 0x7d
};

uint8_t TOS_AM_GROUP = TOS_DEFAULT_AM_GROUP;
#line 84
typedef struct TOS_Msg {


  uint16_t addr;
  uint8_t type;
  uint8_t group;
  uint8_t length;
  int8_t data[29];
  uint16_t crc;







  uint16_t strength;
  uint8_t ack;
  uint16_t time;
  uint8_t sendSecurityMode;
  uint8_t receiveSecurityMode;
} TOS_Msg;

typedef struct TOS_Msg_TinySecCompat {


  uint16_t addr;
  uint8_t type;

  uint8_t length;
  uint8_t group;
  int8_t data[29];
  uint16_t crc;







  uint16_t strength;
  uint8_t ack;
  uint16_t time;
  uint8_t sendSecurityMode;
  uint8_t receiveSecurityMode;
} TOS_Msg_TinySecCompat;

typedef struct TinySec_Msg {

  uint16_t addr;
  uint8_t type;
  uint8_t length;

  uint8_t iv[4];

  uint8_t enc[29];

  uint8_t mac[4];


  uint8_t calc_mac[4];
  uint8_t ack_byte;
  bool cryptoDone;
  bool receiveDone;

  bool validMAC;
} __attribute((packed))  TinySec_Msg;



enum __nesc_unnamed4260 {
  MSG_DATA_SIZE = (size_t )& ((struct TOS_Msg *)0)->crc + sizeof(uint16_t ), 
  TINYSEC_MSG_DATA_SIZE = (size_t )& ((struct TinySec_Msg *)0)->mac + 4, 
  DATA_LENGTH = 29, 
  LENGTH_BYTE_NUMBER = (size_t )& ((struct TOS_Msg *)0)->length + 1, 
  TINYSEC_NODE_ID_SIZE = sizeof(uint16_t )
};

enum __nesc_unnamed4261 {
  TINYSEC_AUTH_ONLY = 1, 
  TINYSEC_ENCRYPT_AND_AUTH = 2, 
  TINYSEC_DISABLED = 3, 
  TINYSEC_ENABLED_BIT = 128, 
  TINYSEC_ENCRYPT_ENABLED_BIT = 64
} __attribute((packed)) ;


typedef TOS_Msg *TOS_MsgPtr;
# 48 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHop.h"
enum __nesc_unnamed4262 {
  AM_MULTIHOPMSG = 250
};


typedef struct TOS_MHopNeighbor {
  uint16_t addr;
  uint16_t recv_count;
  uint16_t fail_count;
  int16_t last_seqno;
  uint8_t goodness;
  uint8_t hopcount;
  uint8_t timeouts;
} TOS_MHopNeighbor;

typedef struct MultihopMsg {
  uint16_t sourceaddr;
  uint16_t originaddr;
  int16_t seqno;
  uint8_t hopcount;
  uint8_t data[29 - 7];
} __attribute((packed))  TOS_MHopMsg;
# 35 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.h"
enum __nesc_unnamed4263 {
  TOS_ADCSample3750ns = 0, 
  TOS_ADCSample7500ns = 1, 
  TOS_ADCSample15us = 2, 
  TOS_ADCSample30us = 3, 
  TOS_ADCSample60us = 4, 
  TOS_ADCSample120us = 5, 
  TOS_ADCSample240us = 6, 
  TOS_ADCSample480us = 7
};
# 45 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
enum __nesc_unnamed4264 {
  TOSH_ACTUAL_PHOTO_PORT = 1, 
  TOSH_ACTUAL_TEMP_PORT = 1, 
  TOSH_ACTUAL_MIC_PORT = 2, 
  TOSH_ACTUAL_ACCEL_X_PORT = 3, 
  TOSH_ACTUAL_ACCEL_Y_PORT = 4, 
  TOSH_ACTUAL_MAG_X_PORT = 6, 
  TOSH_ACTUAL_MAG_Y_PORT = 5
};

enum __nesc_unnamed4265 {
  TOS_ADC_PHOTO_PORT = 1, 
  TOS_ADC_TEMP_PORT = 2, 
  TOS_ADC_MIC_PORT = 3, 
  TOS_ADC_ACCEL_X_PORT = 4, 
  TOS_ADC_ACCEL_Y_PORT = 5, 
  TOS_ADC_MAG_X_PORT = 6, 

  TOS_ADC_MAG_Y_PORT = 8
};

enum __nesc_unnamed4266 {
  TOS_MAG_POT_ADDR = 0, 
  TOS_MIC_POT_ADDR = 1
};

static __inline void TOSH_SET_PHOTO_CTL_PIN(void);
#line 71
static __inline void TOSH_MAKE_PHOTO_CTL_OUTPUT(void);
static __inline void TOSH_CLR_TEMP_CTL_PIN(void);
#line 72
static __inline void TOSH_MAKE_TEMP_CTL_INPUT(void);


static __inline void TOSH_SET_SOUNDER_CTL_PIN(void);
#line 75
static __inline void TOSH_CLR_SOUNDER_CTL_PIN(void);
#line 75
static __inline void TOSH_MAKE_SOUNDER_CTL_OUTPUT(void);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.h"
enum __nesc_unnamed4267 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 3
};
# 32 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/Clock.h"
enum __nesc_unnamed4268 {
  TOS_I1000PS = 33, TOS_S1000PS = 1, 
  TOS_I100PS = 41, TOS_S100PS = 2, 
  TOS_I10PS = 102, TOS_S10PS = 3, 
  TOS_I4096PS = 1, TOS_S4096PS = 2, 
  TOS_I2048PS = 2, TOS_S2048PS = 2, 
  TOS_I1024PS = 1, TOS_S1024PS = 3, 
  TOS_I512PS = 2, TOS_S512PS = 3, 
  TOS_I256PS = 4, TOS_S256PS = 3, 
  TOS_I128PS = 8, TOS_S128PS = 3, 
  TOS_I64PS = 16, TOS_S64PS = 3, 
  TOS_I32PS = 32, TOS_S32PS = 3, 
  TOS_I16PS = 64, TOS_S16PS = 3, 
  TOS_I8PS = 128, TOS_S8PS = 3, 
  TOS_I4PS = 128, TOS_S4PS = 4, 
  TOS_I2PS = 128, TOS_S2PS = 5, 
  TOS_I1PS = 128, TOS_S1PS = 6, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4269 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 128
};
# 33 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TosTime.h"
typedef struct __nesc_unnamed4270 {
  uint32_t high32;
  uint32_t low32;
} tos_time_t;
# 31 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t __attribute((__progmem__)) crcTable[256] = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };
static inline 

uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
# 49 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/Bcast.h"
typedef struct _BcastMsg {
  int16_t seqno;
  uint8_t data[29 - 2];
} __attribute((packed))  TOS_BcastMsg;
static  result_t PotM___Pot___init(uint8_t arg_0xa2855c0);
static  result_t HPLPotC___Pot___finalise(void);
static  result_t HPLPotC___Pot___decrease(void);
static  result_t HPLPotC___Pot___increase(void);
static  result_t HPLInit___init(void);
static  TOS_MsgPtr SurgeM___Bcast___receive(TOS_MsgPtr arg_0xa309b60, void *arg_0xa309cb8, uint16_t arg_0xa309e10);
static  result_t SurgeM___Send___sendDone(TOS_MsgPtr arg_0xa308718, result_t arg_0xa308868);
static   result_t SurgeM___ADC___dataReady(uint16_t arg_0xa2cc418);
static  result_t SurgeM___StdControl___init(void);
static  result_t SurgeM___StdControl___start(void);
static  result_t SurgeM___Timer___fired(void);
static   result_t TimerM___Clock___fire(void);
static  result_t TimerM___StdControl___init(void);
static  result_t TimerM___StdControl___start(void);
static  result_t TimerM___Timer___default___fired(uint8_t arg_0xa332858);
static  result_t TimerM___Timer___start(uint8_t arg_0xa332858, char arg_0xa2feac8, uint32_t arg_0xa2fec20);
static  result_t TimerM___Timer___stop(uint8_t arg_0xa332858);
static   void HPLClock___Clock___setInterval(uint8_t arg_0xa352f30);
static   result_t HPLClock___Clock___setRate(char arg_0xa352430, char arg_0xa352570);
static   result_t NoLeds___Leds___greenToggle(void);
static   result_t NoLeds___Leds___redToggle(void);
static   uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void);
static   result_t LedsC___Leds___yellowOff(void);
static   result_t LedsC___Leds___greenOff(void);
static  result_t PhotoTempM___PhotoStdControl___init(void);
static  result_t PhotoTempM___PhotoStdControl___start(void);
static   result_t PhotoTempM___InternalTempADC___dataReady(uint16_t arg_0xa2cc418);
static   result_t PhotoTempM___ExternalTempADC___default___dataReady(uint16_t arg_0xa2cc418);
static   result_t PhotoTempM___ExternalPhotoADC___getData(void);
static   result_t PhotoTempM___InternalPhotoADC___dataReady(uint16_t arg_0xa2cc418);
static   result_t ADCM___HPLADC___dataReady(uint16_t arg_0xa40e9e8);
static  result_t ADCM___ADCControl___bindPort(uint8_t arg_0xa3defe0, uint8_t arg_0xa3df128);
static  result_t ADCM___ADCControl___init(void);
static   result_t ADCM___ADC___getData(uint8_t arg_0xa401b10);
static   result_t ADCM___ADC___default___dataReady(uint8_t arg_0xa401b10, uint16_t arg_0xa2cc418);
static   result_t HPLADCC___ADC___bindPort(uint8_t arg_0xa3fd858, uint8_t arg_0xa3fd9a0);
static   result_t HPLADCC___ADC___sampleStop(void);
static   result_t HPLADCC___ADC___init(void);
static   result_t HPLADCC___ADC___samplePort(uint8_t arg_0xa3fde88);
static   uint16_t RandomLFSR___Random___rand(void);
static   result_t RandomLFSR___Random___init(void);
static  TOS_MsgPtr AMPromiscuous___ReceiveMsg___default___receive(uint8_t arg_0xa456c38, TOS_MsgPtr arg_0xa443de8);
static  result_t AMPromiscuous___ActivityTimer___fired(void);
static  result_t AMPromiscuous___UARTSend___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18);
static  TOS_MsgPtr AMPromiscuous___RadioReceive___receive(TOS_MsgPtr arg_0xa443de8);
static  result_t AMPromiscuous___CommControl___setPromiscuous(bool arg_0xa446720);
static  result_t AMPromiscuous___Control___init(void);
static  result_t AMPromiscuous___Control___start(void);
static  result_t AMPromiscuous___default___sendDone(void);
static  result_t AMPromiscuous___RadioSend___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18);
static  result_t AMPromiscuous___SendMsg___send(uint8_t arg_0xa456680, uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
static  TOS_MsgPtr AMPromiscuous___UARTReceive___receive(TOS_MsgPtr arg_0xa443de8);
static   result_t SlavePinM___SlavePin___low(void);
static   result_t SlavePinM___SlavePin___high(bool arg_0xa441ac8);
static   result_t HPLSlavePinC___SlavePin___low(void);
static   result_t HPLSlavePinC___SlavePin___high(void);
static  result_t MicaHighSpeedRadioM___Send___send(TOS_MsgPtr arg_0xa44e6b0);
static   result_t MicaHighSpeedRadioM___Code___decodeDone(char arg_0xa4b4e60, char arg_0xa4b4fa0);
static   result_t MicaHighSpeedRadioM___Code___encodeDone(char arg_0xa4b53c0);
static   void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58);
static   void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___startSymbol(void);
static   result_t MicaHighSpeedRadioM___SpiByteFifo___dataReady(uint8_t arg_0xa4b7e60);
static  result_t MicaHighSpeedRadioM___Control___init(void);
static  result_t MicaHighSpeedRadioM___Control___start(void);
static   void MicaHighSpeedRadioM___RadioSendCoordinator___default___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58);
static   void MicaHighSpeedRadioM___RadioSendCoordinator___default___startSymbol(void);
static   result_t MicaHighSpeedRadioM___ChannelMon___idleDetect(void);
static   result_t MicaHighSpeedRadioM___ChannelMon___startSymDetect(void);
static   result_t SecDedEncoding___Code___encode_flush(void);
static   result_t SecDedEncoding___Code___decode(char arg_0xa4b4a48);
static   result_t SecDedEncoding___Code___encode(char arg_0xa4b4638);
static   result_t ChannelMonC___ChannelMon___macDelay(void);
static   result_t ChannelMonC___ChannelMon___startSymbolSearch(void);
static   result_t ChannelMonC___ChannelMon___init(void);
static   uint16_t RadioTimingC___RadioTiming___currentTime(void);
static   uint16_t RadioTimingC___RadioTiming___getTiming(void);
static  result_t SpiByteFifoC___SlavePin___notifyHigh(void);
static   result_t SpiByteFifoC___SpiByteFifo___send(uint8_t arg_0xa4b6af0);
static   result_t SpiByteFifoC___SpiByteFifo___phaseShift(void);
static   result_t SpiByteFifoC___SpiByteFifo___startReadBytes(uint16_t arg_0xa4b71e0);
static   result_t SpiByteFifoC___SpiByteFifo___idle(void);
static   result_t SpiByteFifoC___SpiByteFifo___txMode(void);
static   result_t SpiByteFifoC___SpiByteFifo___rxMode(void);
static   result_t FramerM___ByteComm___txDone(void);
static   result_t FramerM___ByteComm___txByteReady(bool arg_0xa5727e0);
static   result_t FramerM___ByteComm___rxByteReady(uint8_t arg_0xa572010, bool arg_0xa572158, uint16_t arg_0xa5722b0);
static  result_t FramerM___BareSendMsg___send(TOS_MsgPtr arg_0xa44e6b0);
static  result_t FramerM___StdControl___init(void);
static  result_t FramerM___StdControl___start(void);
static  result_t FramerM___TokenReceiveMsg___ReflectToken(uint8_t arg_0xa5580e8);
static  TOS_MsgPtr FramerAckM___ReceiveMsg___receive(TOS_MsgPtr arg_0xa443de8);
static  TOS_MsgPtr FramerAckM___TokenReceiveMsg___receive(TOS_MsgPtr arg_0xa545948, uint8_t arg_0xa545a90);
static   result_t UARTM___HPLUART___get(uint8_t arg_0xa5a3490);
static   result_t UARTM___HPLUART___putDone(void);
static   result_t UARTM___ByteComm___txByte(uint8_t arg_0xa559b58);
static  result_t UARTM___Control___init(void);
static  result_t UARTM___Control___start(void);
static   result_t HPLUARTM___UART___init(void);
static   result_t HPLUARTM___UART___put(uint8_t arg_0xa5a2f90);
static  TOS_MsgPtr BcastM___ReceiveMsg___receive(uint8_t arg_0xa5e52e0, TOS_MsgPtr arg_0xa443de8);
static  TOS_MsgPtr BcastM___Receive___default___receive(uint8_t arg_0xa5e4950, TOS_MsgPtr arg_0xa309b60, void *arg_0xa309cb8, uint16_t arg_0xa309e10);
static  result_t BcastM___SendMsg___sendDone(uint8_t arg_0xa5e57e0, TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
static  result_t BcastM___StdControl___init(void);
static  result_t BcastM___StdControl___start(void);
static  result_t QueuedSendM___QueueSendMsg___send(uint8_t arg_0xa613448, uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
static  result_t QueuedSendM___StdControl___init(void);
static  result_t QueuedSendM___StdControl___start(void);
static  result_t QueuedSendM___SerialSendMsg___sendDone(uint8_t arg_0xa613dd0, TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
static  TOS_MsgPtr MultiHopEngineM___ReceiveMsg___receive(uint8_t arg_0xa6189c8, TOS_MsgPtr arg_0xa443de8);
static  result_t MultiHopEngineM___Intercept___default___intercept(uint8_t arg_0xa61b800, TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578);
static  result_t MultiHopEngineM___Send___send(uint8_t arg_0xa61b198, TOS_MsgPtr arg_0xa2f9440, uint16_t arg_0xa2f9590);
static  void *MultiHopEngineM___Send___getBuffer(uint8_t arg_0xa61b198, TOS_MsgPtr arg_0xa2f9bc8, uint16_t *arg_0xa2f9d30);
static  result_t MultiHopEngineM___Send___default___sendDone(uint8_t arg_0xa61b198, TOS_MsgPtr arg_0xa308718, result_t arg_0xa308868);
static  result_t MultiHopEngineM___SendMsg___sendDone(uint8_t arg_0xa618ec8, TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
static  result_t MultiHopEngineM___StdControl___init(void);
static  result_t MultiHopEngineM___StdControl___start(void);
static  uint16_t MultiHopEngineM___RouteControl___getParent(void);
static  result_t MultiHopLEPSM___RouteSelect___selectRoute(TOS_MsgPtr arg_0xa634960, uint8_t arg_0xa634aa8);
static  result_t MultiHopLEPSM___RouteSelect___initializeFields(TOS_MsgPtr arg_0xa635020, uint8_t arg_0xa635168);
static  result_t MultiHopLEPSM___Timer___fired(void);
static  TOS_MsgPtr MultiHopLEPSM___ReceiveMsg___receive(TOS_MsgPtr arg_0xa443de8);
static  result_t MultiHopLEPSM___Snoop___intercept(uint8_t arg_0xa66b9a8, TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578);
static  result_t MultiHopLEPSM___SendMsg___sendDone(TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
static  result_t MultiHopLEPSM___StdControl___init(void);
static  result_t MultiHopLEPSM___StdControl___start(void);
static  uint16_t MultiHopLEPSM___RouteControl___getParent(void);
static  result_t SounderM___StdControl___init(void);
static  result_t SounderM___StdControl___start(void);
static  result_t SounderM___StdControl___stop(void);
static  
# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
result_t RealMain___hardwareInit(void);
static  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
result_t RealMain___Pot___init(uint8_t arg_0xa2855c0);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t RealMain___StdControl___init(void);
static  





result_t RealMain___StdControl___start(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void);
static  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
result_t PotM___HPLPot___finalise(void);
static  
#line 59
result_t PotM___HPLPot___decrease(void);
static  






result_t PotM___HPLPot___increase(void);
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
uint8_t PotM___potSetting;
static inline 
void PotM___setPot(uint8_t value);
static inline  
#line 106
result_t PotM___Pot___init(uint8_t initialSetting);
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___decrease(void);
static inline  







result_t HPLPotC___Pot___increase(void);
static inline  







result_t HPLPotC___Pot___finalise(void);
static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit___init(void);
static  
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteControl.nc"
uint16_t SurgeM___RouteControl___getParent(void);
static  
# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
result_t SurgeM___Send___send(TOS_MsgPtr arg_0xa2f9440, uint16_t arg_0xa2f9590);
static  
#line 106
void *SurgeM___Send___getBuffer(TOS_MsgPtr arg_0xa2f9bc8, uint16_t *arg_0xa2f9d30);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t SurgeM___Sounder___init(void);
static  





result_t SurgeM___Sounder___start(void);
static  






result_t SurgeM___Sounder___stop(void);
static   
# 122 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t SurgeM___Leds___yellowOff(void);
static   
#line 97
result_t SurgeM___Leds___greenOff(void);
static   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t SurgeM___ADC___getData(void);
static  
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t SurgeM___Timer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20);
static  







result_t SurgeM___Timer___stop(void);
# 56 "SurgeM.nc"
enum SurgeM_____nesc_unnamed4271 {
  SurgeM___TIMER_GETADC_COUNT = 1, 
  SurgeM___TIMER_CHIRP_COUNT = 10
};

bool SurgeM___sleeping;
bool SurgeM___focused;
bool SurgeM___rebroadcast_adc_packet;

TOS_Msg SurgeM___gMsgBuffer;
 uint16_t SurgeM___gSensorData;
bool SurgeM___gfSendBusy;


int SurgeM___timer_rate;
int SurgeM___timer_ticks;




static void SurgeM___initialize(void);
static  






void SurgeM___SendData(void);
static inline  
#line 100
result_t SurgeM___StdControl___init(void);
static inline  



result_t SurgeM___StdControl___start(void);
static inline  
#line 118
result_t SurgeM___Timer___fired(void);
static inline   
#line 135
result_t SurgeM___ADC___dataReady(uint16_t data);
static inline  
#line 149
result_t SurgeM___Send___sendDone(TOS_MsgPtr pMsg, result_t success);
static inline  
#line 161
TOS_MsgPtr SurgeM___Bcast___receive(TOS_MsgPtr pMsg, void *payload, uint16_t payloadLen);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t TimerM___PowerManagement___adjustPower(void);
static   
# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
void TimerM___Clock___setInterval(uint8_t arg_0xa352f30);
static   
#line 96
result_t TimerM___Clock___setRate(char arg_0xa352430, char arg_0xa352570);
static  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t TimerM___Timer___fired(
# 45 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t arg_0xa332858);









uint32_t TimerM___mState;
uint8_t TimerM___setIntervalFlag;
uint8_t TimerM___mScale;
#line 57
uint8_t TimerM___mInterval;
int8_t TimerM___queue_head;
int8_t TimerM___queue_tail;
uint8_t TimerM___queue_size;
uint8_t TimerM___queue[NUM_TIMERS];

struct TimerM___timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM___mTimerList[NUM_TIMERS];

enum TimerM_____nesc_unnamed4272 {
  TimerM___maxTimerInterval = 230
};
static  result_t TimerM___StdControl___init(void);
static inline  








result_t TimerM___StdControl___start(void);
static  









result_t TimerM___Timer___start(uint8_t id, char type, 
uint32_t interval);
#line 116
static void TimerM___adjustInterval(void);
static  
#line 140
result_t TimerM___Timer___stop(uint8_t id);
static inline   
#line 154
result_t TimerM___Timer___default___fired(uint8_t id);
static inline 


void TimerM___enqueue(uint8_t value);
static inline 






uint8_t TimerM___dequeue(void);
static inline  








void TimerM___signalOneTimer(void);
static inline  




void TimerM___HandleFire(void);
static inline   
#line 204
result_t TimerM___Clock___fire(void);
static   
# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLClock___Clock___fire(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
uint8_t HPLClock___set_flag;
uint8_t HPLClock___mscale;
#line 55
uint8_t HPLClock___nextScale;
#line 55
uint8_t HPLClock___minterval;
static inline   
#line 87
void HPLClock___Clock___setInterval(uint8_t value);
static inline   
#line 149
result_t HPLClock___Clock___setRate(char interval, char scale);
#line 167
void __attribute((interrupt))   __vector_15(void);
static inline   
# 63 "C:/cygwin/opt/tinyos-1.x/tos/system/NoLeds.nc"
result_t NoLeds___Leds___redToggle(void);
static inline   
#line 75
result_t NoLeds___Leds___greenToggle(void);
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
bool HPLPowerManagementM___disabled = TRUE;

enum HPLPowerManagementM_____nesc_unnamed4273 {
  HPLPowerManagementM___IDLE = 0, 
  HPLPowerManagementM___ADC_NR = 1 << 3, 
  HPLPowerManagementM___POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM___POWER_DOWN = 1 << 3
};
static inline 


uint8_t HPLPowerManagementM___getPowerLevel(void);
static inline  
#line 85
void HPLPowerManagementM___doAdjustment(void);
static   
#line 103
uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void);
# 50 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
uint8_t LedsC___ledsOn;

enum LedsC_____nesc_unnamed4274 {
  LedsC___RED_BIT = 1, 
  LedsC___GREEN_BIT = 2, 
  LedsC___YELLOW_BIT = 4
};
static inline   
#line 107
result_t LedsC___Leds___greenOff(void);
static inline   
#line 136
result_t LedsC___Leds___yellowOff(void);
static  
# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
result_t PhotoTempM___ADCControl___bindPort(uint8_t arg_0xa3defe0, uint8_t arg_0xa3df128);
static  
#line 50
result_t PhotoTempM___ADCControl___init(void);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t PhotoTempM___ExternalTempADC___dataReady(uint16_t arg_0xa2cc418);
static   
#line 70
result_t PhotoTempM___ExternalPhotoADC___dataReady(uint16_t arg_0xa2cc418);
static   
#line 52
result_t PhotoTempM___InternalPhotoADC___getData(void);
# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
enum PhotoTempM_____nesc_unnamed4275 {
  PhotoTempM___IDLE = 1, 
  PhotoTempM___BUSY = 2, 
  PhotoTempM___CONTINUOUS = 3
};
int PhotoTempM___state;
static inline  
result_t PhotoTempM___PhotoStdControl___init(void);
static inline  







result_t PhotoTempM___PhotoStdControl___start(void);
static inline   
#line 146
result_t PhotoTempM___ExternalPhotoADC___getData(void);
static inline   
#line 204
result_t PhotoTempM___InternalPhotoADC___dataReady(uint16_t data);
static inline    
#line 231
result_t PhotoTempM___ExternalTempADC___default___dataReady(uint16_t data);
static inline   


result_t PhotoTempM___InternalTempADC___dataReady(uint16_t data);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
result_t ADCM___HPLADC___bindPort(uint8_t arg_0xa3fd858, uint8_t arg_0xa3fd9a0);
static   
#line 91
result_t ADCM___HPLADC___sampleStop(void);
static   
#line 54
result_t ADCM___HPLADC___init(void);
static   
#line 77
result_t ADCM___HPLADC___samplePort(uint8_t arg_0xa3fde88);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t ADCM___ADC___dataReady(
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
uint8_t arg_0xa401b10, 
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
uint16_t arg_0xa2cc418);
# 63 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
enum ADCM_____nesc_unnamed4276 {
  ADCM___IDLE = 0, 
  ADCM___SINGLE_CONVERSION = 1, 
  ADCM___CONTINUOUS_CONVERSION = 2
};

uint16_t ADCM___ReqPort;
uint16_t ADCM___ReqVector;
uint16_t ADCM___ContReqMask;
static inline  
result_t ADCM___ADCControl___init(void);
static inline  
#line 87
result_t ADCM___ADCControl___bindPort(uint8_t port, uint8_t adcPort);
static inline    


result_t ADCM___ADC___default___dataReady(uint8_t port, uint16_t data);
static   


result_t ADCM___HPLADC___dataReady(uint16_t data);
static 
#line 132
__inline result_t ADCM___startGet(uint8_t newState, uint8_t port);
static inline   
#line 165
result_t ADCM___ADC___getData(uint8_t port);
static   
# 99 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
result_t HPLADCC___ADC___dataReady(uint16_t arg_0xa40e9e8);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
bool HPLADCC___init_portmap_done;
enum HPLADCC_____nesc_unnamed4277 {
  HPLADCC___TOSH_ADC_PORTMAPSIZE = 10
};

uint8_t HPLADCC___TOSH_adc_portmap[HPLADCC___TOSH_ADC_PORTMAPSIZE];
static 
void HPLADCC___init_portmap(void);
static inline   
#line 76
result_t HPLADCC___ADC___init(void);
static inline   
#line 96
result_t HPLADCC___ADC___bindPort(uint8_t port, uint8_t adcPort);
static   
#line 109
result_t HPLADCC___ADC___samplePort(uint8_t port);
static inline   
#line 125
result_t HPLADCC___ADC___sampleStop(void);





void __attribute((signal))   __vector_21(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR___shiftReg;
uint16_t RandomLFSR___initSeed;
uint16_t RandomLFSR___mask;
static inline   

result_t RandomLFSR___Random___init(void);
static   









uint16_t RandomLFSR___Random___rand(void);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr AMPromiscuous___ReceiveMsg___receive(
# 57 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
uint8_t arg_0xa456c38, 
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr arg_0xa443de8);
static  
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t AMPromiscuous___ActivityTimer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20);
static  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t AMPromiscuous___UARTSend___send(TOS_MsgPtr arg_0xa44e6b0);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t AMPromiscuous___PowerManagement___adjustPower(void);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t AMPromiscuous___RadioControl___init(void);
static  





result_t AMPromiscuous___RadioControl___start(void);
static  
#line 63
result_t AMPromiscuous___TimerControl___init(void);
static  





result_t AMPromiscuous___TimerControl___start(void);
static  
#line 63
result_t AMPromiscuous___UARTControl___init(void);
static  





result_t AMPromiscuous___UARTControl___start(void);
static   
# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t AMPromiscuous___Leds___greenToggle(void);
static  
# 66 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___sendDone(void);
static  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t AMPromiscuous___RadioSend___send(TOS_MsgPtr arg_0xa44e6b0);
static  
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t AMPromiscuous___SendMsg___sendDone(
# 56 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
uint8_t arg_0xa456680, 
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
# 83 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
bool AMPromiscuous___state;
TOS_MsgPtr AMPromiscuous___buffer;
uint16_t AMPromiscuous___lastCount;
uint16_t AMPromiscuous___counter;
bool AMPromiscuous___promiscuous_mode;
bool AMPromiscuous___crc_check;
static  

result_t AMPromiscuous___Control___init(void);
static  
#line 107
result_t AMPromiscuous___Control___start(void);
static inline  
#line 144
result_t AMPromiscuous___CommControl___setPromiscuous(bool value);
static inline 
#line 157
void AMPromiscuous___dbgPacket(TOS_MsgPtr data);
static 









result_t AMPromiscuous___reportSendDone(TOS_MsgPtr msg, result_t success);
static inline  







result_t AMPromiscuous___ActivityTimer___fired(void);
static inline   







result_t AMPromiscuous___default___sendDone(void);
static inline  



void AMPromiscuous___sendTask(void);
static inline  
#line 204
result_t AMPromiscuous___SendMsg___send(uint8_t id, uint16_t addr, uint8_t length, TOS_MsgPtr data);
static inline  
#line 234
result_t AMPromiscuous___UARTSend___sendDone(TOS_MsgPtr msg, result_t success);
static inline  

result_t AMPromiscuous___RadioSend___sendDone(TOS_MsgPtr msg, result_t success);




TOS_MsgPtr   prom_received(TOS_MsgPtr packet);
static inline   
#line 269
TOS_MsgPtr AMPromiscuous___ReceiveMsg___default___receive(uint8_t id, TOS_MsgPtr msg);
static inline  


TOS_MsgPtr AMPromiscuous___UARTReceive___receive(TOS_MsgPtr packet);
static inline  


TOS_MsgPtr AMPromiscuous___RadioReceive___receive(TOS_MsgPtr packet);
static  
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
result_t SlavePinM___SlavePin___notifyHigh(void);
static   
# 47 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
result_t SlavePinM___HPLSlavePin___low(void);
static   result_t SlavePinM___HPLSlavePin___high(void);
# 90 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
int8_t SlavePinM___n;
bool SlavePinM___signalHigh;
static 
#line 110
__inline   result_t SlavePinM___SlavePin___low(void);
static inline  







void SlavePinM___signalHighTask(void);
static 





__inline   result_t SlavePinM___SlavePin___high(bool needEvent);
static inline   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC___SlavePin___high(void);
static inline   




result_t HPLSlavePinC___SlavePin___low(void);
static   
# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
uint16_t MicaHighSpeedRadioM___RadioTiming___currentTime(void);
static   
#line 33
uint16_t MicaHighSpeedRadioM___RadioTiming___getTiming(void);
static  
# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t MicaHighSpeedRadioM___Send___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18);
static   
# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
result_t MicaHighSpeedRadioM___Random___init(void);
static   
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
result_t MicaHighSpeedRadioM___Code___encode_flush(void);
static   
result_t MicaHighSpeedRadioM___Code___decode(char arg_0xa4b4a48);
static   
#line 34
result_t MicaHighSpeedRadioM___Code___encode(char arg_0xa4b4638);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t MicaHighSpeedRadioM___PowerManagement___adjustPower(void);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr MicaHighSpeedRadioM___Receive___receive(TOS_MsgPtr arg_0xa443de8);
static   
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
void MicaHighSpeedRadioM___RadioReceiveCoordinator___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58);
static   
#line 45
void MicaHighSpeedRadioM___RadioReceiveCoordinator___startSymbol(void);
static   
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
result_t MicaHighSpeedRadioM___SpiByteFifo___send(uint8_t arg_0xa4b6af0);
static   



result_t MicaHighSpeedRadioM___SpiByteFifo___phaseShift(void);
static   
#line 35
result_t MicaHighSpeedRadioM___SpiByteFifo___startReadBytes(uint16_t arg_0xa4b71e0);
static   
#line 34
result_t MicaHighSpeedRadioM___SpiByteFifo___idle(void);
static   
result_t MicaHighSpeedRadioM___SpiByteFifo___txMode(void);
static   result_t MicaHighSpeedRadioM___SpiByteFifo___rxMode(void);
static   
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
void MicaHighSpeedRadioM___RadioSendCoordinator___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58);
static   
#line 45
void MicaHighSpeedRadioM___RadioSendCoordinator___startSymbol(void);
static   
# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
result_t MicaHighSpeedRadioM___ChannelMon___macDelay(void);
static   
#line 34
result_t MicaHighSpeedRadioM___ChannelMon___startSymbolSearch(void);
static   
#line 33
result_t MicaHighSpeedRadioM___ChannelMon___init(void);
# 55 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
enum MicaHighSpeedRadioM_____nesc_unnamed4278 {
  MicaHighSpeedRadioM___IDLE_STATE, 
  MicaHighSpeedRadioM___SEND_WAITING, 
  MicaHighSpeedRadioM___RX_STATE, 
  MicaHighSpeedRadioM___TRANSMITTING, 
  MicaHighSpeedRadioM___WAITING_FOR_ACK, 
  MicaHighSpeedRadioM___SENDING_STRENGTH_PULSE, 
  MicaHighSpeedRadioM___TRANSMITTING_START, 
  MicaHighSpeedRadioM___RX_DONE_STATE, 
  MicaHighSpeedRadioM___ACK_SEND_STATE, 
  MicaHighSpeedRadioM___STOPPED_STATE
};

enum MicaHighSpeedRadioM_____nesc_unnamed4279 {
  MicaHighSpeedRadioM___ACK_CNT = 4, 
  MicaHighSpeedRadioM___ENCODE_PACKET_LENGTH_DEFAULT = MSG_DATA_SIZE * 3
};






char  TOSH_MHSR_start[12] = 
{ 0xf0, 0xf0, 0xf0, 0xff, 0x00, 0xff, 0x0f, 0x00, 0xff, 0x0f, 0x0f, 0x0f };

char MicaHighSpeedRadioM___state;
char MicaHighSpeedRadioM___send_state;
char MicaHighSpeedRadioM___tx_count;
uint16_t MicaHighSpeedRadioM___calc_crc;
uint8_t MicaHighSpeedRadioM___ack_count;
char MicaHighSpeedRadioM___rec_count;
TOS_Msg MicaHighSpeedRadioM___buffer;
TOS_Msg *MicaHighSpeedRadioM___rec_ptr;
TOS_Msg *MicaHighSpeedRadioM___send_ptr;
unsigned char MicaHighSpeedRadioM___rx_count;
char MicaHighSpeedRadioM___msg_length;
char MicaHighSpeedRadioM___buf_head;
char MicaHighSpeedRadioM___buf_end;
char MicaHighSpeedRadioM___encoded_buffer[4];
char MicaHighSpeedRadioM___enc_count;
static inline  



void MicaHighSpeedRadioM___packetReceived(void);
static inline  
#line 113
void MicaHighSpeedRadioM___packetSent(void);
static inline  
#line 125
result_t MicaHighSpeedRadioM___Send___send(TOS_MsgPtr msg);
static inline  
#line 144
result_t MicaHighSpeedRadioM___Control___init(void);
static inline  









result_t MicaHighSpeedRadioM___Control___start(void);
static inline   
#line 197
result_t MicaHighSpeedRadioM___ChannelMon___startSymDetect(void);
static inline   
#line 218
result_t MicaHighSpeedRadioM___ChannelMon___idleDetect(void);
static inline   
#line 259
result_t MicaHighSpeedRadioM___Code___decodeDone(char data, char error);
static   
#line 299
result_t MicaHighSpeedRadioM___Code___encodeDone(char data1);
static   








result_t MicaHighSpeedRadioM___SpiByteFifo___dataReady(uint8_t data);
static inline    
#line 400
void MicaHighSpeedRadioM___RadioSendCoordinator___default___startSymbol(void);
static inline    void MicaHighSpeedRadioM___RadioSendCoordinator___default___byte(TOS_MsgPtr msg, uint8_t byteCount);
static inline    void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___startSymbol(void);
static inline    void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___byte(TOS_MsgPtr msg, uint8_t byteCount);
static   
# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
result_t SecDedEncoding___Code___decodeDone(char arg_0xa4b4e60, char arg_0xa4b4fa0);
static   result_t SecDedEncoding___Code___encodeDone(char arg_0xa4b53c0);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
enum SecDedEncoding_____nesc_unnamed4280 {
  SecDedEncoding___IDLE_STATE = 0, 
  SecDedEncoding___DECODING_BYTE_3 = 1, 
  SecDedEncoding___DECODING_BYTE_2 = 2, 
  SecDedEncoding___DECODING_BYTE_1 = 3, 
  SecDedEncoding___ENCODING_BYTE = 4
};

char SecDedEncoding___data1;
char SecDedEncoding___data2;
char SecDedEncoding___data3;
char SecDedEncoding___state;
static inline 
void SecDedEncoding___radio_decode_thread(void);
static inline void SecDedEncoding___radio_encode_thread(void);
static inline   
result_t SecDedEncoding___Code___decode(char d1);
static inline   
#line 76
result_t SecDedEncoding___Code___encode_flush(void);
static   


result_t SecDedEncoding___Code___encode(char d);
static inline 
#line 99
void SecDedEncoding___radio_encode_thread(void);
static inline 
#line 167
void SecDedEncoding___radio_decode_thread(void);
static   
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
uint16_t ChannelMonC___Random___rand(void);
static   
# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
result_t ChannelMonC___ChannelMon___idleDetect(void);
static   
#line 38
result_t ChannelMonC___ChannelMon___startSymDetect(void);
# 42 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
enum ChannelMonC_____nesc_unnamed4281 {
  ChannelMonC___IDLE_STATE, 
  ChannelMonC___START_SYMBOL_SEARCH, 
  ChannelMonC___PACKET_START, 
  ChannelMonC___DISABLED_STATE
};

enum ChannelMonC_____nesc_unnamed4282 {
  ChannelMonC___SAMPLE_RATE = 100 / 2 * 4
};

unsigned short ChannelMonC___CM_search[2];
char ChannelMonC___CM_state;
unsigned char ChannelMonC___CM_lastBit;
unsigned char ChannelMonC___CM_startSymBits;
short ChannelMonC___CM_waiting;
static inline   
result_t ChannelMonC___ChannelMon___init(void);
static   





result_t ChannelMonC___ChannelMon___startSymbolSearch(void);
#line 94
void __attribute((signal))   __vector_9(void);
static inline   
#line 151
result_t ChannelMonC___ChannelMon___macDelay(void);
static inline   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC___RadioTiming___getTiming(void);
static inline   
#line 55
uint16_t RadioTimingC___RadioTiming___currentTime(void);
static   
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
result_t SpiByteFifoC___SlavePin___low(void);
static   








result_t SpiByteFifoC___SlavePin___high(bool arg_0xa441ac8);
static   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
result_t SpiByteFifoC___SpiByteFifo___dataReady(uint8_t arg_0xa4b7e60);
# 41 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
uint8_t SpiByteFifoC___nextByte;
uint8_t SpiByteFifoC___state;

enum SpiByteFifoC_____nesc_unnamed4283 {
  SpiByteFifoC___IDLE, 
  SpiByteFifoC___FULL, 
  SpiByteFifoC___OPEN, 
  SpiByteFifoC___READING
};

enum SpiByteFifoC_____nesc_unnamed4284 {
  SpiByteFifoC___BIT_RATE = 20 * 4 / 2 * 5 / 4
};


void __attribute((signal))   __vector_17(void);
static   
#line 71
result_t SpiByteFifoC___SpiByteFifo___send(uint8_t data);
static   
#line 102
result_t SpiByteFifoC___SpiByteFifo___idle(void);
static inline   
#line 119
result_t SpiByteFifoC___SpiByteFifo___startReadBytes(uint16_t timing);
static inline   
#line 164
result_t SpiByteFifoC___SpiByteFifo___txMode(void);
static inline   






result_t SpiByteFifoC___SpiByteFifo___rxMode(void);
static inline   
#line 188
result_t SpiByteFifoC___SpiByteFifo___phaseShift(void);
static inline  








result_t SpiByteFifoC___SlavePin___notifyHigh(void);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr FramerM___ReceiveMsg___receive(TOS_MsgPtr arg_0xa443de8);
static   
# 55 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t FramerM___ByteComm___txByte(uint8_t arg_0xa559b58);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t FramerM___ByteControl___init(void);
static  





result_t FramerM___ByteControl___start(void);
static  
# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t FramerM___BareSendMsg___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
TOS_MsgPtr FramerM___TokenReceiveMsg___receive(TOS_MsgPtr arg_0xa545948, uint8_t arg_0xa545a90);
# 82 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
enum FramerM_____nesc_unnamed4285 {
  FramerM___HDLC_QUEUESIZE = 2, 
  FramerM___HDLC_MTU = sizeof(TOS_Msg ), 
  FramerM___HDLC_FLAG_BYTE = 0x7e, 
  FramerM___HDLC_CTLESC_BYTE = 0x7d, 
  FramerM___PROTO_ACK = 64, 
  FramerM___PROTO_PACKET_ACK = 65, 
  FramerM___PROTO_PACKET_NOACK = 66, 
  FramerM___PROTO_UNKNOWN = 255
};

enum FramerM_____nesc_unnamed4286 {
  FramerM___RXSTATE_NOSYNC, 
  FramerM___RXSTATE_PROTO, 
  FramerM___RXSTATE_TOKEN, 
  FramerM___RXSTATE_INFO, 
  FramerM___RXSTATE_ESC
};

enum FramerM_____nesc_unnamed4287 {
  FramerM___TXSTATE_IDLE, 
  FramerM___TXSTATE_PROTO, 
  FramerM___TXSTATE_INFO, 
  FramerM___TXSTATE_ESC, 
  FramerM___TXSTATE_FCS1, 
  FramerM___TXSTATE_FCS2, 
  FramerM___TXSTATE_ENDFLAG, 
  FramerM___TXSTATE_FINISH, 
  FramerM___TXSTATE_ERROR
};

enum FramerM_____nesc_unnamed4288 {
  FramerM___FLAGS_TOKENPEND = 0x2, 
  FramerM___FLAGS_DATAPEND = 0x4, 
  FramerM___FLAGS_UNKNOWN = 0x8
};

TOS_Msg FramerM___gMsgRcvBuf[FramerM___HDLC_QUEUESIZE];

typedef struct FramerM____MsgRcvEntry {
  uint8_t Proto;
  uint8_t Token;
  uint16_t Length;
  TOS_MsgPtr pMsg;
} FramerM___MsgRcvEntry_t;

FramerM___MsgRcvEntry_t FramerM___gMsgRcvTbl[FramerM___HDLC_QUEUESIZE];

uint8_t *FramerM___gpRxBuf;
uint8_t *FramerM___gpTxBuf;

uint8_t FramerM___gFlags;
 

uint8_t FramerM___gTxState;
 uint8_t FramerM___gPrevTxState;
 uint8_t FramerM___gTxProto;
 uint16_t FramerM___gTxByteCnt;
 uint16_t FramerM___gTxLength;
 uint16_t FramerM___gTxRunningCRC;


uint8_t FramerM___gRxState;
uint8_t FramerM___gRxHeadIndex;
uint8_t FramerM___gRxTailIndex;
uint16_t FramerM___gRxByteCnt;

uint16_t FramerM___gRxRunningCRC;

TOS_MsgPtr FramerM___gpTxMsg;
uint8_t FramerM___gTxTokenBuf;
uint8_t FramerM___gTxUnknownBuf;
 uint8_t FramerM___gTxEscByte;
static  
void FramerM___PacketSent(void);
static 
result_t FramerM___StartTx(void);
static inline  
#line 202
void FramerM___PacketUnknown(void);
static inline  






void FramerM___PacketRcvd(void);
static  
#line 246
void FramerM___PacketSent(void);
static 
#line 268
void FramerM___HDLCInitialize(void);
static inline  
#line 291
result_t FramerM___StdControl___init(void);
static inline  



result_t FramerM___StdControl___start(void);
static inline  








result_t FramerM___BareSendMsg___send(TOS_MsgPtr pMsg);
static inline  
#line 328
result_t FramerM___TokenReceiveMsg___ReflectToken(uint8_t Token);
static   
#line 348
result_t FramerM___ByteComm___rxByteReady(uint8_t data, bool error, uint16_t strength);
static 
#line 469
result_t FramerM___TxArbitraryByte(uint8_t Byte);
static inline   
#line 482
result_t FramerM___ByteComm___txByteReady(bool LastByteSuccess);
static inline   
#line 552
result_t FramerM___ByteComm___txDone(void);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr FramerAckM___ReceiveCombined___receive(TOS_MsgPtr arg_0xa443de8);
static  
# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
result_t FramerAckM___TokenReceiveMsg___ReflectToken(uint8_t arg_0xa5580e8);
# 72 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
uint8_t FramerAckM___gTokenBuf;
static inline  
void FramerAckM___SendAckTask(void);
static inline  



TOS_MsgPtr FramerAckM___TokenReceiveMsg___receive(TOS_MsgPtr Msg, uint8_t token);
static inline  
#line 91
TOS_MsgPtr FramerAckM___ReceiveMsg___receive(TOS_MsgPtr Msg);
static   
# 62 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t UARTM___HPLUART___init(void);
static   
#line 80
result_t UARTM___HPLUART___put(uint8_t arg_0xa5a2f90);
static   
# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t UARTM___ByteComm___txDone(void);
static   
#line 75
result_t UARTM___ByteComm___txByteReady(bool arg_0xa5727e0);
static   
#line 66
result_t UARTM___ByteComm___rxByteReady(uint8_t arg_0xa572010, bool arg_0xa572158, uint16_t arg_0xa5722b0);
# 58 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
bool UARTM___state;
static inline  
result_t UARTM___Control___init(void);
static inline  






result_t UARTM___Control___start(void);
static inline   







result_t UARTM___HPLUART___get(uint8_t data);
static   








result_t UARTM___HPLUART___putDone(void);
static   
#line 110
result_t UARTM___ByteComm___txByte(uint8_t data);
static   
# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t HPLUARTM___UART___get(uint8_t arg_0xa5a3490);
static   






result_t HPLUARTM___UART___putDone(void);
static inline   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM___UART___init(void);
#line 71
void __attribute((signal))   __vector_18(void);





void __attribute((interrupt))   __vector_20(void);
static inline   


result_t HPLUARTM___UART___put(uint8_t data);
# 74 "C:/cygwin/opt/tinyos-1.x/tos/system/NoCRCPacket.nc"
enum NoCRCPacket_____nesc_unnamed4289 {
  NoCRCPacket___IDLE, 
  NoCRCPacket___PACKET, 
  NoCRCPacket___BYTES
};
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t BcastM___SubControl___init(void);
static  





result_t BcastM___SubControl___start(void);
static  
# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Receive.nc"
TOS_MsgPtr BcastM___Receive___receive(
# 41 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
uint8_t arg_0xa5e4950, 
# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Receive.nc"
TOS_MsgPtr arg_0xa309b60, void *arg_0xa309cb8, uint16_t arg_0xa309e10);
static  
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t BcastM___SendMsg___send(
# 46 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
uint8_t arg_0xa5e57e0, 
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
# 52 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
enum BcastM_____nesc_unnamed4290 {
  BcastM___FWD_QUEUE_SIZE = 4
};

int16_t BcastM___BcastSeqno;
struct TOS_Msg BcastM___FwdBuffer[BcastM___FWD_QUEUE_SIZE];
uint8_t BcastM___iFwdBufHead;
#line 58
uint8_t BcastM___iFwdBufTail;





static void BcastM___initialize(void);








static bool BcastM___newBcast(int16_t proposed);
#line 93
static void BcastM___FwdBcast(TOS_BcastMsg *pRcvMsg, uint8_t Len, uint8_t id);
static inline  
#line 115
result_t BcastM___StdControl___init(void);
static inline  



result_t BcastM___StdControl___start(void);
static inline  






result_t BcastM___SendMsg___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success);
static inline  





TOS_MsgPtr BcastM___ReceiveMsg___receive(uint8_t id, TOS_MsgPtr pMsg);
static inline   
#line 148
TOS_MsgPtr BcastM___Receive___default___receive(uint8_t id, TOS_MsgPtr pMsg, void *payload, 
uint16_t payloadLen);
static  
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t QueuedSendM___QueueSendMsg___sendDone(
# 60 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
uint8_t arg_0xa613448, 
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998);
static   
# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t QueuedSendM___Leds___greenToggle(void);
static   
#line 81
result_t QueuedSendM___Leds___redToggle(void);
static  
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t QueuedSendM___SerialSendMsg___send(
# 65 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
uint8_t arg_0xa613dd0, 
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
# 74 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
enum QueuedSendM_____nesc_unnamed4291 {
  QueuedSendM___MESSAGE_QUEUE_SIZE = 32, 
  QueuedSendM___MAX_RETRANSMIT_COUNT = 5
};

struct QueuedSendM____msgq_entry {
  uint16_t address;
  uint8_t length;
  uint8_t id;
  uint8_t xmit_count;
  TOS_MsgPtr pMsg;
} QueuedSendM___msgqueue[QueuedSendM___MESSAGE_QUEUE_SIZE];

uint16_t QueuedSendM___enqueue_next;
#line 87
uint16_t QueuedSendM___dequeue_next;
bool QueuedSendM___retransmit;
bool QueuedSendM___fQueueIdle;
static  
result_t QueuedSendM___StdControl___init(void);
static inline  
#line 107
result_t QueuedSendM___StdControl___start(void);
static  
#line 122
void QueuedSendM___QueueServiceTask(void);
static  
#line 141
result_t QueuedSendM___QueueSendMsg___send(uint8_t id, uint16_t address, uint8_t length, TOS_MsgPtr msg);
static inline  
#line 174
result_t QueuedSendM___SerialSendMsg___sendDone(uint8_t id, TOS_MsgPtr msg, result_t success);
static  
# 71 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteSelect.nc"
result_t MultiHopEngineM___RouteSelect___selectRoute(TOS_MsgPtr arg_0xa634960, uint8_t arg_0xa634aa8);
static  
#line 86
result_t MultiHopEngineM___RouteSelect___initializeFields(TOS_MsgPtr arg_0xa635020, uint8_t arg_0xa635168);
static  
# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
result_t MultiHopEngineM___Intercept___intercept(
# 52 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
uint8_t arg_0xa61b800, 
# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578);
static  
# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteControl.nc"
uint16_t MultiHopEngineM___RouteSelectCntl___getParent(void);
static  
# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
result_t MultiHopEngineM___Snoop___intercept(
# 53 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
uint8_t arg_0xa61bd30, 
# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578);
static  
# 119 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
result_t MultiHopEngineM___Send___sendDone(
# 51 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
uint8_t arg_0xa61b198, 
# 119 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
TOS_MsgPtr arg_0xa308718, result_t arg_0xa308868);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t MultiHopEngineM___CommStdControl___init(void);
static  





result_t MultiHopEngineM___CommStdControl___start(void);
static  
#line 63
result_t MultiHopEngineM___SubControl___init(void);
static  





result_t MultiHopEngineM___SubControl___start(void);
static  
# 65 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/CommControl.nc"
result_t MultiHopEngineM___CommControl___setPromiscuous(bool arg_0xa446720);
static  
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t MultiHopEngineM___SendMsg___send(
# 58 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
uint8_t arg_0xa618ec8, 
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
# 69 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
enum MultiHopEngineM_____nesc_unnamed4292 {
  MultiHopEngineM___FWD_QUEUE_SIZE = 16, 
  MultiHopEngineM___EMPTY = 0xff
};






struct TOS_Msg MultiHopEngineM___FwdBuffers[MultiHopEngineM___FWD_QUEUE_SIZE];
struct TOS_Msg *MultiHopEngineM___FwdBufList[MultiHopEngineM___FWD_QUEUE_SIZE];

uint8_t MultiHopEngineM___iFwdBufHead;
#line 82
uint8_t MultiHopEngineM___iFwdBufTail;








static void MultiHopEngineM___initialize(void);
static inline  








result_t MultiHopEngineM___StdControl___init(void);
static inline  




result_t MultiHopEngineM___StdControl___start(void);
static inline  
#line 124
result_t MultiHopEngineM___Send___send(uint8_t id, TOS_MsgPtr pMsg, uint16_t PayloadLen);
static  
#line 150
void *MultiHopEngineM___Send___getBuffer(uint8_t id, TOS_MsgPtr pMsg, uint16_t *length);










static TOS_MsgPtr MultiHopEngineM___mForward(TOS_MsgPtr pMsg, uint8_t id);
static inline  
#line 182
TOS_MsgPtr MultiHopEngineM___ReceiveMsg___receive(uint8_t id, TOS_MsgPtr pMsg);
static  
#line 205
result_t MultiHopEngineM___SendMsg___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success);
static inline  








uint16_t MultiHopEngineM___RouteControl___getParent(void);
static inline   
#line 246
result_t MultiHopEngineM___Send___default___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success);
static inline   


result_t MultiHopEngineM___Intercept___default___intercept(uint8_t id, TOS_MsgPtr pMsg, void *payload, 
uint16_t payloadLen);
static  
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t MultiHopLEPSM___Timer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20);
static  
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t MultiHopLEPSM___SendMsg___send(uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
enum MultiHopLEPSM_____nesc_unnamed4293 {
  MultiHopLEPSM___NBRFLAG_VALID = 0x01, 
  MultiHopLEPSM___NBRFLAG_NEW = 0x02, 
  MultiHopLEPSM___NBRFLAG_EST_INIT = 0x04
};

enum MultiHopLEPSM_____nesc_unnamed4294 {
  MultiHopLEPSM___BASE_STATION_ADDRESS = 0, 
  MultiHopLEPSM___ROUTE_TABLE_SIZE = 16, 
  MultiHopLEPSM___ESTIMATE_TO_ROUTE_RATIO = 5, 
  MultiHopLEPSM___ACCEPTABLE_MISSED = -20, 
  MultiHopLEPSM___DATA_TO_ROUTE_RATIO = 2, 
  MultiHopLEPSM___DATA_FREQ = 10000, 
  MultiHopLEPSM___MAX_ALLOWABLE_LINK_COST = 1536, 
  MultiHopLEPSM___MIN_LIVELINESS = 2
};


enum MultiHopLEPSM_____nesc_unnamed4295 {
  MultiHopLEPSM___ROUTE_INVALID = 0xff
};

struct MultiHopLEPSM___SortEntry {
  uint16_t id;
  uint8_t receiveEst;
};

typedef struct MultiHopLEPSM___RPEstEntry {
  uint16_t id;
  uint8_t receiveEst;
} __attribute((packed))  MultiHopLEPSM___RPEstEntry;

typedef struct MultiHopLEPSM___RoutePacket {
  uint16_t parent;


  uint8_t estEntries;
  MultiHopLEPSM___RPEstEntry estList[1];
} __attribute((packed))  MultiHopLEPSM___RoutePacket;

typedef struct MultiHopLEPSM___TableEntry {
  uint16_t id;
  uint16_t parent;
  uint16_t missed;
  uint16_t received;
  int16_t lastSeqno;
  uint8_t flags;
  uint8_t liveliness;
  uint8_t hop;
  uint8_t receiveEst;
  uint8_t sendEst;
} MultiHopLEPSM___TableEntry;

TOS_Msg MultiHopLEPSM___routeMsg;
bool MultiHopLEPSM___gfSendRouteBusy;

MultiHopLEPSM___TableEntry MultiHopLEPSM___BaseStation;
MultiHopLEPSM___TableEntry MultiHopLEPSM___NeighborTbl[MultiHopLEPSM___ROUTE_TABLE_SIZE];
MultiHopLEPSM___TableEntry *MultiHopLEPSM___gpCurrentParent;
uint8_t MultiHopLEPSM___gbCurrentHopCount;
int16_t MultiHopLEPSM___gCurrentSeqNo;
uint16_t MultiHopLEPSM___gwEstTicks;
uint32_t MultiHopLEPSM___gUpdateInterval;
static inline 









uint8_t MultiHopLEPSM___findEntry(uint8_t id);
static inline 
#line 145
uint8_t MultiHopLEPSM___findEntryToBeReplaced(void);
static 
#line 169
void MultiHopLEPSM___newEntry(uint8_t indes, uint16_t id);
static inline 
#line 195
uint8_t MultiHopLEPSM___findPreparedIndex(uint16_t id);
static inline 








int MultiHopLEPSM___sortByReceiveEstFcn(const void *x, const void *y);
static inline 








uint32_t MultiHopLEPSM___evaluateCost(uint8_t sendEst, uint8_t receiveEst);
static inline 









void MultiHopLEPSM___updateEst(MultiHopLEPSM___TableEntry *Nbr);
static inline 
#line 272
void MultiHopLEPSM___updateTable(void);
static 
#line 288
bool MultiHopLEPSM___updateNbrCounters(uint16_t saddr, int16_t seqno, uint8_t *NbrIndex);
static inline 
#line 325
void MultiHopLEPSM___chooseParent(void);
static inline  
#line 370
void MultiHopLEPSM___SendRouteTask(void);
static  
#line 416
void MultiHopLEPSM___TimerTask(void);
static inline  
#line 450
result_t MultiHopLEPSM___StdControl___init(void);
static inline  
#line 474
result_t MultiHopLEPSM___StdControl___start(void);
static  
#line 499
result_t MultiHopLEPSM___RouteSelect___selectRoute(TOS_MsgPtr Msg, uint8_t id);
static inline  
#line 552
result_t MultiHopLEPSM___RouteSelect___initializeFields(TOS_MsgPtr Msg, uint8_t id);
static inline  
#line 565
uint16_t MultiHopLEPSM___RouteControl___getParent(void);
static inline  
#line 614
result_t MultiHopLEPSM___Timer___fired(void);
static inline  



TOS_MsgPtr MultiHopLEPSM___ReceiveMsg___receive(TOS_MsgPtr Msg);
static inline  
#line 645
result_t MultiHopLEPSM___Snoop___intercept(uint8_t id, TOS_MsgPtr Msg, void *Payload, uint16_t Len);
static inline  








result_t MultiHopLEPSM___SendMsg___sendDone(TOS_MsgPtr pMsg, result_t success);
static inline  
# 50 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/SounderM.nc"
result_t SounderM___StdControl___init(void);
static inline  





result_t SounderM___StdControl___start(void);
static inline  




result_t SounderM___StdControl___stop(void);
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 5;
}

#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) &= ~(1 << 5);
}

#line 65
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 4;
}

#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 4;
}

#line 59
static __inline void TOSH_SET_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_SET_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_SET_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}








static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void)
#line 67
{
#line 67
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 64
static __inline void TOSH_SET_POT_POWER_PIN(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 7;
}











static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 7;
}

static __inline void TOSH_MAKE_PW0_OUTPUT(void)
#line 81
{
#line 81
  ;
}

#line 82
static __inline void TOSH_MAKE_PW1_OUTPUT(void)
#line 82
{
#line 82
  ;
}

#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void)
#line 83
{
#line 83
  ;
}

#line 84
static __inline void TOSH_MAKE_PW3_OUTPUT(void)
#line 84
{
#line 84
  ;
}

#line 85
static __inline void TOSH_MAKE_PW4_OUTPUT(void)
#line 85
{
#line 85
  ;
}

#line 86
static __inline void TOSH_MAKE_PW5_OUTPUT(void)
#line 86
{
#line 86
  ;
}

#line 87
static __inline void TOSH_MAKE_PW6_OUTPUT(void)
#line 87
{
#line 87
  ;
}

#line 88
static __inline void TOSH_MAKE_PW7_OUTPUT(void)
#line 88
{
#line 88
  ;
}

#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 7;
}

#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 5;
}

#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 2;
}

static inline 
#line 103
void TOSH_SET_PIN_DIRECTIONS(void )
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) = 0x02;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) = 0x02;
  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();
  TOSH_MAKE_POT_SELECT_OUTPUT();
  TOSH_MAKE_POT_POWER_OUTPUT();

  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();

  TOSH_MAKE_RFM_CTL0_OUTPUT();
  TOSH_MAKE_RFM_CTL1_OUTPUT();
  TOSH_MAKE_RFM_TXD_OUTPUT();
  TOSH_SET_POT_POWER_PIN();



  TOSH_MAKE_FLASH_SELECT_INPUT();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();


  TOSH_MAKE_BOOST_ENABLE_OUTPUT();
  TOSH_SET_BOOST_ENABLE_PIN();

  TOSH_MAKE_ONE_WIRE_INPUT();
  TOSH_SET_ONE_WIRE_PIN();
}

static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit___init(void)
#line 57
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
inline static  result_t RealMain___hardwareInit(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLInit___init();
#line 47

#line 47
  return result;
#line 47
}
#line 47
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}

#line 61
static __inline void TOSH_SET_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

static inline  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___finalise(void)
#line 74
{
  TOSH_SET_UD_PIN();
  TOSH_SET_INC_PIN();
  return SUCCESS;
}

# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___finalise(void){
#line 74
  unsigned char result;
#line 74

#line 74
  result = HPLPotC___Pot___finalise();
#line 74

#line 74
  return result;
#line 74
}
#line 74
# 63 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 5;
}

#line 62
static __inline void TOSH_CLR_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 5);
}

#line 61
static __inline void TOSH_CLR_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline  
# 65 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___increase(void)
#line 65
{
  TOSH_CLR_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___increase(void){
#line 67
  unsigned char result;
#line 67

#line 67
  result = HPLPotC___Pot___increase();
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___decrease(void)
#line 56
{
  TOSH_SET_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___decrease(void){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLPotC___Pot___decrease();
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline 
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
void PotM___setPot(uint8_t value)
#line 93
{
  uint8_t i;

#line 95
  for (i = 0; i < 151; i++) 
    PotM___HPLPot___decrease();

  for (i = 0; i < value; i++) 
    PotM___HPLPot___increase();

  PotM___HPLPot___finalise();

  PotM___potSetting = value;
}

static inline  result_t PotM___Pot___init(uint8_t initialSetting)
#line 106
{
  PotM___setPot(initialSetting);
  return SUCCESS;
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
inline static  result_t RealMain___Pot___init(uint8_t arg_0xa2855c0){
#line 78
  unsigned char result;
#line 78

#line 78
  result = PotM___Pot___init(arg_0xa2855c0);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 76 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
void TOSH_sched_init(void )
{
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
}

static inline 
# 108 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

static inline 
#line 147
void *nmemset(void *to, int val, size_t n)
{
  char *cto = to;

  while (n--) * cto++ = val;

  return to;
}

static inline  
# 450 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___StdControl___init(void)
#line 450
{

  nmemset((void *)MultiHopLEPSM___NeighborTbl, 0, sizeof(MultiHopLEPSM___TableEntry ) * MultiHopLEPSM___ROUTE_TABLE_SIZE);
  MultiHopLEPSM___BaseStation.id = TOS_UART_ADDR;
  MultiHopLEPSM___BaseStation.parent = TOS_UART_ADDR;
  MultiHopLEPSM___BaseStation.flags = MultiHopLEPSM___NBRFLAG_VALID;
  MultiHopLEPSM___BaseStation.hop = 0;
  MultiHopLEPSM___gpCurrentParent = (void *)0;
  MultiHopLEPSM___gbCurrentHopCount = MultiHopLEPSM___ROUTE_INVALID;
  MultiHopLEPSM___gCurrentSeqNo = 0;
  MultiHopLEPSM___gwEstTicks = 0;
  MultiHopLEPSM___gUpdateInterval = MultiHopLEPSM___DATA_TO_ROUTE_RATIO * MultiHopLEPSM___DATA_FREQ;
  MultiHopLEPSM___gfSendRouteBusy = FALSE;

  if (TOS_LOCAL_ADDRESS == MultiHopLEPSM___BASE_STATION_ADDRESS) {

      MultiHopLEPSM___gpCurrentParent = &MultiHopLEPSM___BaseStation;
      MultiHopLEPSM___gbCurrentHopCount = 0;
    }


  return SUCCESS;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t MultiHopEngineM___SubControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = QueuedSendM___StdControl___init();
#line 63
  result = rcombine(result, MultiHopLEPSM___StdControl___init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
inline static  result_t MultiHopEngineM___CommStdControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = AMPromiscuous___Control___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
# 91 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
static void MultiHopEngineM___initialize(void)
#line 91
{
  int n;

  for (n = 0; n < MultiHopEngineM___FWD_QUEUE_SIZE; n++) {
      MultiHopEngineM___FwdBufList[n] = &MultiHopEngineM___FwdBuffers[n];
    }

  MultiHopEngineM___iFwdBufHead = MultiHopEngineM___iFwdBufTail = 0;
}

static inline  result_t MultiHopEngineM___StdControl___init(void)
#line 101
{
  MultiHopEngineM___initialize();
  MultiHopEngineM___CommStdControl___init();
  return MultiHopEngineM___SubControl___init();
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t BcastM___SubControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = QueuedSendM___StdControl___init();
#line 63
  result = rcombine(result, AMPromiscuous___Control___init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
# 64 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
static void BcastM___initialize(void)
#line 64
{
  BcastM___iFwdBufHead = BcastM___iFwdBufTail = 0;
  BcastM___BcastSeqno = 0;
}

static inline  
#line 115
result_t BcastM___StdControl___init(void)
#line 115
{
  BcastM___initialize();
  return BcastM___SubControl___init();
}

static inline   
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC___ADC___init(void)
#line 76
{
  HPLADCC___init_portmap();

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) = 0x04;

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 6);
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 4;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 3;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 7);

  return SUCCESS;
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM___HPLADC___init(void){
#line 54
  unsigned char result;
#line 54

#line 54
  result = HPLADCC___ADC___init();
#line 54

#line 54
  return result;
#line 54
}
#line 54
static inline  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM___ADCControl___init(void)
#line 73
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 74
    {
      ADCM___ReqPort = 0;
      ADCM___ReqVector = ADCM___ContReqMask = 0;
    }
#line 77
    __nesc_atomic_end(__nesc_atomic); }
  {
  }
#line 78
  ;

  return ADCM___HPLADC___init();
}

# 50 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
inline static  result_t PhotoTempM___ADCControl___init(void){
#line 50
  unsigned char result;
#line 50

#line 50
  result = ADCM___ADCControl___init();
#line 50

#line 50
  return result;
#line 50
}
#line 50
static inline   
# 96 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC___ADC___bindPort(uint8_t port, uint8_t adcPort)
#line 96
{
  if (port < HPLADCC___TOSH_ADC_PORTMAPSIZE) 
    {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 99
        {
          HPLADCC___init_portmap();
          HPLADCC___TOSH_adc_portmap[port] = adcPort;
        }
#line 102
        __nesc_atomic_end(__nesc_atomic); }
      return SUCCESS;
    }
  else {
    return FAIL;
    }
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM___HPLADC___bindPort(uint8_t arg_0xa3fd858, uint8_t arg_0xa3fd9a0){
#line 70
  unsigned char result;
#line 70

#line 70
  result = HPLADCC___ADC___bindPort(arg_0xa3fd858, arg_0xa3fd9a0);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 87 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM___ADCControl___bindPort(uint8_t port, uint8_t adcPort)
#line 87
{
  return ADCM___HPLADC___bindPort(port, adcPort);
}

# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
inline static  result_t PhotoTempM___ADCControl___bindPort(uint8_t arg_0xa3defe0, uint8_t arg_0xa3df128){
#line 89
  unsigned char result;
#line 89

#line 89
  result = ADCM___ADCControl___bindPort(arg_0xa3defe0, arg_0xa3df128);
#line 89

#line 89
  return result;
#line 89
}
#line 89
static inline  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___PhotoStdControl___init(void)
#line 78
{
  PhotoTempM___ADCControl___bindPort(TOS_ADC_PHOTO_PORT, TOSH_ACTUAL_PHOTO_PORT);
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
    {
      PhotoTempM___state = PhotoTempM___IDLE;
    }
#line 82
    __nesc_atomic_end(__nesc_atomic); }
  {
  }
#line 83
  ;
  return PhotoTempM___ADCControl___init();
}

# 76 "SurgeM.nc"
static void SurgeM___initialize(void)
#line 76
{
  SurgeM___timer_rate = INITIAL_TIMER_RATE;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 78
    SurgeM___gfSendBusy = FALSE;
#line 78
    __nesc_atomic_end(__nesc_atomic); }
  SurgeM___sleeping = FALSE;
  SurgeM___rebroadcast_adc_packet = FALSE;
  SurgeM___focused = FALSE;
}

static inline  
#line 100
result_t SurgeM___StdControl___init(void)
#line 100
{
  SurgeM___initialize();
  return SUCCESS;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain___StdControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = SurgeM___StdControl___init();
#line 63
  result = rcombine(result, PhotoTempM___PhotoStdControl___init());
#line 63
  result = rcombine(result, BcastM___StdControl___init());
#line 63
  result = rcombine(result, MultiHopEngineM___StdControl___init());
#line 63
  result = rcombine(result, QueuedSendM___StdControl___init());
#line 63
  result = rcombine(result, TimerM___StdControl___init());
#line 63
  result = rcombine(result, AMPromiscuous___Control___init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
inline static  result_t AMPromiscuous___TimerControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = TimerM___StdControl___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 149 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
result_t HPLClock___Clock___setRate(char interval, char scale)
#line 149
{
  scale &= 0x7;
  scale |= 0x8;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 0);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 1);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) |= 1 << 3;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = scale;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 1;
    }
#line 162
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t TimerM___Clock___setRate(char arg_0xa352430, char arg_0xa352570){
#line 96
  unsigned char result;
#line 96

#line 96
  result = HPLClock___Clock___setRate(arg_0xa352430, arg_0xa352570);
#line 96

#line 96
  return result;
#line 96
}
#line 96
static inline  
# 60 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM___Control___init(void)
#line 60
{
  {
  }
#line 61
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 62
    {
      UARTM___state = FALSE;
    }
#line 64
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM___ByteControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = UARTM___Control___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 291 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___StdControl___init(void)
#line 291
{
  FramerM___HDLCInitialize();
  return FramerM___ByteControl___init();
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMPromiscuous___UARTControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = FramerM___StdControl___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
result_t RandomLFSR___Random___init(void)
#line 59
{
  {
  }
#line 60
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 61
    {
      RandomLFSR___shiftReg = 119 * 119 * (TOS_LOCAL_ADDRESS + 1);
      RandomLFSR___initSeed = RandomLFSR___shiftReg;
      RandomLFSR___mask = 137 * 29 * (TOS_LOCAL_ADDRESS + 1);
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   result_t MicaHighSpeedRadioM___Random___init(void){
#line 57
  unsigned char result;
#line 57

#line 57
  result = RandomLFSR___Random___init();
#line 57

#line 57
  return result;
#line 57
}
#line 57
static inline   
# 59 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC___ChannelMon___init(void)
#line 59
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 60
    {
      ChannelMonC___CM_waiting = -1;
    }
#line 62
    __nesc_atomic_end(__nesc_atomic); }
  return ChannelMonC___ChannelMon___startSymbolSearch();
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioM___ChannelMon___init(void){
#line 33
  unsigned char result;
#line 33

#line 33
  result = ChannelMonC___ChannelMon___init();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline  
# 144 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___Control___init(void)
#line 144
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 145
    {
      MicaHighSpeedRadioM___rec_ptr = &MicaHighSpeedRadioM___buffer;
      MicaHighSpeedRadioM___send_state = MicaHighSpeedRadioM___IDLE_STATE;
      MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___IDLE_STATE;
    }
#line 149
    __nesc_atomic_end(__nesc_atomic); }
  return rcombine(MicaHighSpeedRadioM___ChannelMon___init(), MicaHighSpeedRadioM___Random___init());
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMPromiscuous___RadioControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = MicaHighSpeedRadioM___Control___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 82 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___StdControl___start(void)
#line 82
{
  return SUCCESS;
}

static inline  
# 107 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
result_t QueuedSendM___StdControl___start(void)
#line 107
{
  return SUCCESS;
}

static inline  
# 144 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___CommControl___setPromiscuous(bool value)
#line 144
{
  AMPromiscuous___promiscuous_mode = value;
  return SUCCESS;
}

# 65 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/CommControl.nc"
inline static  result_t MultiHopEngineM___CommControl___setPromiscuous(bool arg_0xa446720){
#line 65
  unsigned char result;
#line 65

#line 65
  result = AMPromiscuous___CommControl___setPromiscuous(arg_0xa446720);
#line 65

#line 65
  return result;
#line 65
}
#line 65
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t MultiHopLEPSM___Timer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM___Timer___start(2, arg_0xa2feac8, arg_0xa2fec20);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline  
# 474 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___StdControl___start(void)
#line 474
{
  TOS_post(MultiHopLEPSM___TimerTask);
  MultiHopLEPSM___Timer___start(TIMER_REPEAT, MultiHopLEPSM___gUpdateInterval);
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t MultiHopEngineM___SubControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = QueuedSendM___StdControl___start();
#line 70
  result = rcombine(result, MultiHopLEPSM___StdControl___start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
inline static  result_t MultiHopEngineM___CommStdControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = AMPromiscuous___Control___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 107 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
result_t MultiHopEngineM___StdControl___start(void)
#line 107
{
  MultiHopEngineM___CommStdControl___start();
  MultiHopEngineM___SubControl___start();
  return MultiHopEngineM___CommControl___setPromiscuous(TRUE);
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t BcastM___SubControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = QueuedSendM___StdControl___start();
#line 70
  result = rcombine(result, AMPromiscuous___Control___start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 120 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
result_t BcastM___StdControl___start(void)
#line 120
{
  return BcastM___SubControl___start();
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_INT1_OUTPUT(void)
#line 72
{
#line 72
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 1;
}

# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_MAKE_PHOTO_CTL_OUTPUT(void)
#line 71
{
#line 71
  TOSH_MAKE_INT1_OUTPUT();
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INT1_PIN(void)
#line 72
{
#line 72
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 1;
}

# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_SET_PHOTO_CTL_PIN(void)
#line 71
{
#line 71
  TOSH_SET_INT1_PIN();
}

static inline  
# 87 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___PhotoStdControl___start(void)
#line 87
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 88
    {
      TOSH_SET_PHOTO_CTL_PIN();
      TOSH_MAKE_PHOTO_CTL_OUTPUT();
    }
#line 91
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t SurgeM___Timer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM___Timer___start(0, arg_0xa2feac8, arg_0xa2fec20);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline  
# 105 "SurgeM.nc"
result_t SurgeM___StdControl___start(void)
#line 105
{
  return SurgeM___Timer___start(TIMER_REPEAT, SurgeM___timer_rate);
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain___StdControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SurgeM___StdControl___start();
#line 70
  result = rcombine(result, PhotoTempM___PhotoStdControl___start());
#line 70
  result = rcombine(result, BcastM___StdControl___start());
#line 70
  result = rcombine(result, MultiHopEngineM___StdControl___start());
#line 70
  result = rcombine(result, QueuedSendM___StdControl___start());
#line 70
  result = rcombine(result, TimerM___StdControl___start());
#line 70
  result = rcombine(result, AMPromiscuous___Control___start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline 
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM___getPowerLevel(void)
#line 62
{
  uint8_t diff;

#line 64
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {

      return HPLPowerManagementM___IDLE;
    }
  else {
#line 67
    if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM___IDLE;
      }
    else {
#line 69
      if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) & ((1 << 6) | (1 << 7))) {
          return HPLPowerManagementM___IDLE;
        }
      else {
        if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) & (1 << 7)) {
            return HPLPowerManagementM___ADC_NR;
          }
        else {
#line 75
          if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
              diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) - * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
              if (diff < 16) {
                return HPLPowerManagementM___IDLE;
                }
#line 79
              return HPLPowerManagementM___POWER_SAVE;
            }
          else 
#line 80
            {
              return HPLPowerManagementM___POWER_DOWN;
            }
          }
        }
      }
    }
}

static inline  
#line 85
void HPLPowerManagementM___doAdjustment(void)
#line 85
{
  uint8_t foo;
#line 86
  uint8_t mcu;

#line 87
  foo = HPLPowerManagementM___getPowerLevel();
  mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM___POWER_SAVE) {
      mcu |= HPLPowerManagementM___IDLE;
      while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMPromiscuous___TimerControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = TimerM___StdControl___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 98 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_UART_RXD0_PIN(void)
#line 98
{
#line 98
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 0;
}

static inline   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM___UART___init(void)
#line 56
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x09 + 0x20) = 12;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20);
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) = 0xd8;
  TOSH_SET_UART_RXD0_PIN();

  return SUCCESS;
}

# 62 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM___HPLUART___init(void){
#line 62
  unsigned char result;
#line 62

#line 62
  result = HPLUARTM___UART___init();
#line 62

#line 62
  return result;
#line 62
}
#line 62
static inline  
# 68 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM___Control___start(void)
#line 68
{
  return UARTM___HPLUART___init();
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM___ByteControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = UARTM___Control___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 296 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___StdControl___start(void)
#line 296
{
  FramerM___HDLCInitialize();
  return FramerM___ByteControl___start();
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMPromiscuous___UARTControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = FramerM___StdControl___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioM___ChannelMon___startSymbolSearch(void){
#line 34
  unsigned char result;
#line 34

#line 34
  result = ChannelMonC___ChannelMon___startSymbolSearch();
#line 34

#line 34
  return result;
#line 34
}
#line 34
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t MicaHighSpeedRadioM___PowerManagement___adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM___PowerManagement___adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline  
# 155 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___Control___start(void)
#line 155
{
  uint8_t oldState;
  result_t rval = SUCCESS;

#line 158
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 158
    {
      oldState = MicaHighSpeedRadioM___state;
      if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___STOPPED_STATE) {
          MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___IDLE_STATE;
          MicaHighSpeedRadioM___send_state = MicaHighSpeedRadioM___IDLE_STATE;
          MicaHighSpeedRadioM___PowerManagement___adjustPower();
          rval = MicaHighSpeedRadioM___ChannelMon___startSymbolSearch();
        }
    }
#line 166
    __nesc_atomic_end(__nesc_atomic); }

  return rval;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMPromiscuous___RadioControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = MicaHighSpeedRadioM___Control___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t AMPromiscuous___ActivityTimer___start(char arg_0xa2feac8, uint32_t arg_0xa2fec20){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM___Timer___start(1, arg_0xa2feac8, arg_0xa2fec20);
#line 59

#line 59
  return result;
#line 59
}
#line 59
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t AMPromiscuous___PowerManagement___adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM___PowerManagement___adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline 
# 121 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4)
{
  return rcombine(r1, rcombine(r2, rcombine(r3, r4)));
}

static inline 
# 226 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
void MultiHopLEPSM___updateEst(MultiHopLEPSM___TableEntry *Nbr)
#line 226
{
  uint16_t usExpTotal;
#line 227
  uint16_t usActTotal;
#line 227
  uint16_t newAve;

  if (Nbr->flags & MultiHopLEPSM___NBRFLAG_NEW) {
    return;
    }
  usExpTotal = MultiHopLEPSM___ESTIMATE_TO_ROUTE_RATIO;



  {
  }
#line 236
  ;


  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 239
    {
      usActTotal = Nbr->received + Nbr->missed;

      if (usActTotal < usExpTotal) {
          usActTotal = usExpTotal;
        }

      newAve = (uint16_t )255 * (uint16_t )Nbr->received / (uint16_t )usActTotal;
      Nbr->missed = 0;
      Nbr->received = 0;



      if (Nbr->liveliness < MultiHopLEPSM___MIN_LIVELINESS) {
          Nbr->sendEst <<= 1;
        }
      Nbr->liveliness = 0;
    }
#line 256
    __nesc_atomic_end(__nesc_atomic); }



  if (Nbr->flags & MultiHopLEPSM___NBRFLAG_EST_INIT) {
      uint16_t tmp;

#line 262
      tmp = (2 * (uint16_t )Nbr->receiveEst + (uint16_t )newAve * 6) / 8;
      Nbr->receiveEst = (uint8_t )tmp;
    }
  else {
      Nbr->receiveEst = (uint8_t )newAve;
      Nbr->flags ^= MultiHopLEPSM___NBRFLAG_EST_INIT;
    }
}

static inline 
void MultiHopLEPSM___updateTable(void)
#line 272
{
  MultiHopLEPSM___TableEntry *pNbr;
  uint8_t i = 0;

  MultiHopLEPSM___gwEstTicks++;
  MultiHopLEPSM___gwEstTicks %= MultiHopLEPSM___ESTIMATE_TO_ROUTE_RATIO;

  for (i = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
      pNbr = &MultiHopLEPSM___NeighborTbl[i];
      if (pNbr->flags & MultiHopLEPSM___NBRFLAG_VALID) {
          if (MultiHopLEPSM___gwEstTicks == 0) {
            MultiHopLEPSM___updateEst(pNbr);
            }
        }
    }
}

static inline 
#line 215
uint32_t MultiHopLEPSM___evaluateCost(uint8_t sendEst, uint8_t receiveEst)
#line 215
{
  uint32_t transEst = (uint32_t )sendEst * (uint32_t )receiveEst;
  uint32_t immed = (uint32_t )1 << 24;

  if (transEst == 0) {
#line 219
    return (uint32_t )1 << (uint32_t )16;
    }
  immed = immed / transEst;
  return immed;
}

static inline 
#line 325
void MultiHopLEPSM___chooseParent(void)
#line 325
{
  MultiHopLEPSM___TableEntry *pNbr;
  uint32_t ulNbrLinkCost = (uint32_t )-1;
  uint32_t ulMinLinkCost = (uint32_t )-1;
  MultiHopLEPSM___TableEntry *pNewParent = (void *)0;
  uint8_t bNewHopCount = MultiHopLEPSM___ROUTE_INVALID;
  uint8_t i;

  if (TOS_LOCAL_ADDRESS == MultiHopLEPSM___BASE_STATION_ADDRESS) {
#line 333
    return;
    }




  for (i = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
      pNbr = &MultiHopLEPSM___NeighborTbl[i];

      if (!(pNbr->flags & MultiHopLEPSM___NBRFLAG_VALID)) {
#line 342
        continue;
        }
#line 343
      if (pNbr->parent == TOS_LOCAL_ADDRESS) {
#line 343
        continue;
        }
#line 344
      if (pNbr->parent == MultiHopLEPSM___ROUTE_INVALID) {
#line 344
        continue;
        }
#line 345
      if (pNbr->hop == MultiHopLEPSM___ROUTE_INVALID) {
#line 345
        continue;
        }
#line 346
      if (pNbr->sendEst < 25) {
#line 346
        continue;
        }
#line 347
      if (pNbr->hop != 0 && pNbr->receiveEst < 25) {
#line 347
        continue;
        }
      ulNbrLinkCost = MultiHopLEPSM___evaluateCost(pNbr->sendEst, pNbr->receiveEst);

      if (pNbr->hop != 0 && ulNbrLinkCost > MultiHopLEPSM___MAX_ALLOWABLE_LINK_COST) {
#line 351
        continue;
        }
      if (pNbr->hop < bNewHopCount || (
      pNbr->hop == bNewHopCount && ulMinLinkCost > ulNbrLinkCost)) {
          ulMinLinkCost = ulNbrLinkCost;
          pNewParent = pNbr;
          bNewHopCount = pNbr->hop;
        }
    }


  if (pNewParent) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 363
        {
          MultiHopLEPSM___gpCurrentParent = pNewParent;
          MultiHopLEPSM___gbCurrentHopCount = bNewHopCount + 1;
        }
#line 366
        __nesc_atomic_end(__nesc_atomic); }
    }
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t MultiHopLEPSM___SendMsg___send(uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428){
#line 48
  unsigned char result;
#line 48

#line 48
  result = QueuedSendM___QueueSendMsg___send(AM_MULTIHOPMSG, arg_0xa442190, arg_0xa4422d8, arg_0xa442428);
#line 48

#line 48
  return result;
#line 48
}
#line 48
static inline 
# 205 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
int MultiHopLEPSM___sortByReceiveEstFcn(const void *x, const void *y)
#line 205
{
  struct MultiHopLEPSM___SortEntry *nx = (struct MultiHopLEPSM___SortEntry *)x;
  struct MultiHopLEPSM___SortEntry *ny = (struct MultiHopLEPSM___SortEntry *)y;
  uint8_t xReceiveEst = nx->receiveEst;
#line 208
  uint8_t yReceiveEst = ny->receiveEst;

#line 209
  if (xReceiveEst > yReceiveEst) {
#line 209
    return -1;
    }
#line 210
  if (xReceiveEst == yReceiveEst) {
#line 210
    return 0;
    }
#line 211
  if (xReceiveEst < yReceiveEst) {
#line 211
    return 1;
    }
#line 212
  return 0;
}

static inline  
#line 370
void MultiHopLEPSM___SendRouteTask(void)
#line 370
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)&MultiHopLEPSM___routeMsg.data[0];
  MultiHopLEPSM___RoutePacket *pRP = (MultiHopLEPSM___RoutePacket *)&pMHMsg->data[0];
  struct MultiHopLEPSM___SortEntry sortTbl[MultiHopLEPSM___ROUTE_TABLE_SIZE];
  uint8_t length = (size_t )& ((TOS_MHopMsg *)0)->data + (size_t )& ((MultiHopLEPSM___RoutePacket *)0)->estList;
  uint8_t maxEstEntries;
  uint8_t i;
#line 376
  uint8_t j;

  if (MultiHopLEPSM___gfSendRouteBusy) {
      return;
    }

  {
  }
#line 382
  ;

  maxEstEntries = 29 - length;
  maxEstEntries = maxEstEntries / sizeof(MultiHopLEPSM___RPEstEntry );

  for (i = 0, j = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
      if (MultiHopLEPSM___NeighborTbl[i].flags & MultiHopLEPSM___NBRFLAG_VALID) {
          sortTbl[j].id = MultiHopLEPSM___NeighborTbl[i].id;
          sortTbl[j].receiveEst = MultiHopLEPSM___NeighborTbl[i].receiveEst;
          j++;
        }
    }
  qsort(sortTbl, j, sizeof(struct MultiHopLEPSM___SortEntry ), MultiHopLEPSM___sortByReceiveEstFcn);

  pRP->parent = MultiHopLEPSM___gpCurrentParent ? MultiHopLEPSM___gpCurrentParent->id : MultiHopLEPSM___ROUTE_INVALID;


  pRP->estEntries = j > maxEstEntries ? maxEstEntries : j;
  for (i = 0; i < pRP->estEntries; i++) {
      pRP->estList[i].id = sortTbl[i].id;
      pRP->estList[i].receiveEst = sortTbl[i].receiveEst;
      length += sizeof(MultiHopLEPSM___RPEstEntry );
    }

  pMHMsg->sourceaddr = pMHMsg->originaddr = TOS_LOCAL_ADDRESS;
  pMHMsg->hopcount = MultiHopLEPSM___gbCurrentHopCount;
  pMHMsg->seqno = MultiHopLEPSM___gCurrentSeqNo++;

  if (MultiHopLEPSM___SendMsg___send(TOS_BCAST_ADDR, length, &MultiHopLEPSM___routeMsg) == SUCCESS) {
      MultiHopLEPSM___gfSendRouteBusy = TRUE;
    }
}

static inline   
# 75 "C:/cygwin/opt/tinyos-1.x/tos/system/NoLeds.nc"
result_t NoLeds___Leds___greenToggle(void)
#line 75
{
  return SUCCESS;
}

# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t QueuedSendM___Leds___greenToggle(void){
#line 106
  unsigned char result;
#line 106

#line 106
  result = NoLeds___Leds___greenToggle();
#line 106

#line 106
  return result;
#line 106
}
#line 106
static inline 
# 157 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
void AMPromiscuous___dbgPacket(TOS_MsgPtr data)
#line 157
{
  uint8_t i;

  for (i = 0; i < sizeof(TOS_Msg ); i++) 
    {
      {
      }
#line 162
      ;
    }
  {
  }
#line 164
  ;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   uint16_t ChannelMonC___Random___rand(void){
#line 63
  unsigned int result;
#line 63

#line 63
  result = RandomLFSR___Random___rand();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 151 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC___ChannelMon___macDelay(void)
#line 151
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      ChannelMonC___CM_search[0] = 0xff;
      if (ChannelMonC___CM_waiting == -1) {
          ChannelMonC___CM_waiting = (ChannelMonC___Random___rand() & 0x2f) + 80;
        }
    }
#line 157
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioM___ChannelMon___macDelay(void){
#line 36
  unsigned char result;
#line 36

#line 36
  result = ChannelMonC___ChannelMon___macDelay();
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline  
# 125 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___Send___send(TOS_MsgPtr msg)
#line 125
{
  uint8_t oldSendState;

#line 127
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 127
    {
      oldSendState = MicaHighSpeedRadioM___send_state;
      if (MicaHighSpeedRadioM___send_state == MicaHighSpeedRadioM___IDLE_STATE) {
          MicaHighSpeedRadioM___send_state = MicaHighSpeedRadioM___SEND_WAITING;
          MicaHighSpeedRadioM___send_ptr = msg;
          MicaHighSpeedRadioM___tx_count = 1;
        }
    }
#line 134
    __nesc_atomic_end(__nesc_atomic); }

  if (oldSendState == MicaHighSpeedRadioM___IDLE_STATE) {
      return MicaHighSpeedRadioM___ChannelMon___macDelay();
    }
  else 
#line 138
    {
      return FAIL;
    }
}

# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t AMPromiscuous___RadioSend___send(TOS_MsgPtr arg_0xa44e6b0){
#line 58
  unsigned char result;
#line 58

#line 58
  result = MicaHighSpeedRadioM___Send___send(arg_0xa44e6b0);
#line 58

#line 58
  return result;
#line 58
}
#line 58
static inline  
# 306 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___BareSendMsg___send(TOS_MsgPtr pMsg)
#line 306
{
  result_t Result = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 309
    {
      if (!(FramerM___gFlags & FramerM___FLAGS_DATAPEND)) {
          FramerM___gFlags |= FramerM___FLAGS_DATAPEND;
          FramerM___gpTxMsg = pMsg;
        }
      else 

        {
          Result = FAIL;
        }
    }
#line 319
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM___StartTx();
    }

  return Result;
}

# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t AMPromiscuous___UARTSend___send(TOS_MsgPtr arg_0xa44e6b0){
#line 58
  unsigned char result;
#line 58

#line 58
  result = FramerM___BareSendMsg___send(arg_0xa44e6b0);
#line 58

#line 58
  return result;
#line 58
}
#line 58
static inline  
# 191 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
void AMPromiscuous___sendTask(void)
#line 191
{
  result_t ok;

  if (AMPromiscuous___buffer->addr == TOS_UART_ADDR) {
    ok = AMPromiscuous___UARTSend___send(AMPromiscuous___buffer);
    }
  else {
#line 197
    ok = AMPromiscuous___RadioSend___send(AMPromiscuous___buffer);
    }
  if (ok == FAIL) {
    AMPromiscuous___reportSendDone(AMPromiscuous___buffer, FAIL);
    }
}

# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t AMPromiscuous___Leds___greenToggle(void){
#line 106
  unsigned char result;
#line 106

#line 106
  result = NoLeds___Leds___greenToggle();
#line 106

#line 106
  return result;
#line 106
}
#line 106
static inline  
# 204 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___SendMsg___send(uint8_t id, uint16_t addr, uint8_t length, TOS_MsgPtr data)
#line 204
{
  if (!AMPromiscuous___state) {
      AMPromiscuous___state = TRUE;
      AMPromiscuous___Leds___greenToggle();

      if (length > DATA_LENGTH) {
          {
          }
#line 210
          ;
          AMPromiscuous___state = FALSE;
          return FAIL;
        }
      if (!TOS_post(AMPromiscuous___sendTask)) {
          {
          }
#line 215
          ;
          AMPromiscuous___state = FALSE;
          return FAIL;
        }
      else {
          AMPromiscuous___buffer = data;
          data->length = length;
          data->addr = addr;
          data->type = id;
          AMPromiscuous___buffer->group = TOS_AM_GROUP;
          AMPromiscuous___dbgPacket(data);
          {
          }
#line 226
          ;
        }
      return SUCCESS;
    }

  return FAIL;
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t QueuedSendM___SerialSendMsg___send(uint8_t arg_0xa613dd0, uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428){
#line 48
  unsigned char result;
#line 48

#line 48
  result = AMPromiscuous___SendMsg___send(arg_0xa613dd0, arg_0xa442190, arg_0xa4422d8, arg_0xa442428);
#line 48

#line 48
  return result;
#line 48
}
#line 48
static inline   
# 81 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM___UART___put(uint8_t data)
#line 81
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) |= 1 << 6;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20) = data;
  return SUCCESS;
}

# 80 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM___HPLUART___put(uint8_t arg_0xa5a2f90){
#line 80
  unsigned char result;
#line 80

#line 80
  result = HPLUARTM___UART___put(arg_0xa5a2f90);
#line 80

#line 80
  return result;
#line 80
}
#line 80
static inline  
# 234 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___UARTSend___sendDone(TOS_MsgPtr msg, result_t success)
#line 234
{
  return AMPromiscuous___reportSendDone(msg, success);
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t FramerM___BareSendMsg___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18){
#line 67
  unsigned char result;
#line 67

#line 67
  result = AMPromiscuous___UARTSend___sendDone(arg_0xa44ebc8, arg_0xa44ed18);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 655 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___SendMsg___sendDone(TOS_MsgPtr pMsg, result_t success)
#line 655
{
  MultiHopLEPSM___gfSendRouteBusy = FALSE;

  return SUCCESS;
}

static inline  
# 128 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
result_t BcastM___SendMsg___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success)
#line 128
{
  if (pMsg == &BcastM___FwdBuffer[BcastM___iFwdBufTail]) {
      BcastM___iFwdBufTail++;
#line 130
      BcastM___iFwdBufTail %= BcastM___FWD_QUEUE_SIZE;
    }
  return SUCCESS;
}

# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t QueuedSendM___QueueSendMsg___sendDone(uint8_t arg_0xa613448, TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998){
#line 49
  unsigned char result;
#line 49

#line 49
  result = MultiHopEngineM___SendMsg___sendDone(arg_0xa613448, arg_0xa442848, arg_0xa442998);
#line 49
  result = rcombine(result, BcastM___SendMsg___sendDone(arg_0xa613448, arg_0xa442848, arg_0xa442998));
#line 49
  switch (arg_0xa613448) {
#line 49
    case AM_MULTIHOPMSG:
#line 49
      result = rcombine(result, MultiHopLEPSM___SendMsg___sendDone(arg_0xa442848, arg_0xa442998));
#line 49
      break;
#line 49
  }
#line 49

#line 49
  return result;
#line 49
}
#line 49
static inline   
# 63 "C:/cygwin/opt/tinyos-1.x/tos/system/NoLeds.nc"
result_t NoLeds___Leds___redToggle(void)
#line 63
{
  return SUCCESS;
}

# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t QueuedSendM___Leds___redToggle(void){
#line 81
  unsigned char result;
#line 81

#line 81
  result = NoLeds___Leds___redToggle();
#line 81

#line 81
  return result;
#line 81
}
#line 81
static inline  
# 174 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
result_t QueuedSendM___SerialSendMsg___sendDone(uint8_t id, TOS_MsgPtr msg, result_t success)
#line 174
{
  if (msg != QueuedSendM___msgqueue[QueuedSendM___dequeue_next].pMsg) {
      return FAIL;
    }


  if ((!QueuedSendM___retransmit || msg->ack != 0) || QueuedSendM___msgqueue[QueuedSendM___dequeue_next].address == TOS_UART_ADDR) {

      QueuedSendM___QueueSendMsg___sendDone(id, msg, success);
      QueuedSendM___msgqueue[QueuedSendM___dequeue_next].length = 0;
      {
      }
#line 184
      ;
      QueuedSendM___dequeue_next++;
#line 185
      QueuedSendM___dequeue_next %= QueuedSendM___MESSAGE_QUEUE_SIZE;
    }
  else 







    {
      QueuedSendM___Leds___redToggle();
      if (++ QueuedSendM___msgqueue[QueuedSendM___dequeue_next].xmit_count > QueuedSendM___MAX_RETRANSMIT_COUNT) {


          QueuedSendM___QueueSendMsg___sendDone(id, msg, FAIL);
          QueuedSendM___msgqueue[QueuedSendM___dequeue_next].length = 0;
          QueuedSendM___dequeue_next++;
#line 202
          QueuedSendM___dequeue_next %= QueuedSendM___MESSAGE_QUEUE_SIZE;
        }
    }


  TOS_post(QueuedSendM___QueueServiceTask);

  return SUCCESS;
}

# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t AMPromiscuous___SendMsg___sendDone(uint8_t arg_0xa456680, TOS_MsgPtr arg_0xa442848, result_t arg_0xa442998){
#line 49
  unsigned char result;
#line 49

#line 49
  result = QueuedSendM___SerialSendMsg___sendDone(arg_0xa456680, arg_0xa442848, arg_0xa442998);
#line 49

#line 49
  return result;
#line 49
}
#line 49
static inline  
# 149 "SurgeM.nc"
result_t SurgeM___Send___sendDone(TOS_MsgPtr pMsg, result_t success)
#line 149
{
  {
  }
#line 150
  ;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    SurgeM___gfSendBusy = FALSE;
#line 152
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   
# 246 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
result_t MultiHopEngineM___Send___default___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success)
#line 246
{
  return SUCCESS;
}

# 119 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
inline static  result_t MultiHopEngineM___Send___sendDone(uint8_t arg_0xa61b198, TOS_MsgPtr arg_0xa308718, result_t arg_0xa308868){
#line 119
  unsigned char result;
#line 119

#line 119
  switch (arg_0xa61b198) {
#line 119
    case AM_SURGEMSG:
#line 119
      result = SurgeM___Send___sendDone(arg_0xa308718, arg_0xa308868);
#line 119
      break;
#line 119
    default:
#line 119
      result = MultiHopEngineM___Send___default___sendDone(arg_0xa61b198, arg_0xa308718, arg_0xa308868);
#line 119
    }
#line 119

#line 119
  return result;
#line 119
}
#line 119
static inline   
# 186 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___default___sendDone(void)
#line 186
{
  return SUCCESS;
}

#line 66
inline static  result_t AMPromiscuous___sendDone(void){
#line 66
  unsigned char result;
#line 66

#line 66
  result = AMPromiscuous___default___sendDone();
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline 
# 135 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
void TOSH_wait(void)
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

static inline 
void TOSH_sleep(void)
{

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
   __asm volatile ("sleep");}

#line 160
__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) = oldSreg;
  //__BLAST_interrupt_enabled = 1;
}

#line 153
__inline __nesc_atomic_t  __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20);

#line 156
  //__BLAST_interrupt_enabled = 0;
   // __asm volatile ("cli");
  return result;
}

static inline 
# 139 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool TOSH_run_next_task(void)
#line 139
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  if (TOSH_sched_full == TOSH_sched_free) {

      return 0;
    }
  else {

      fInterruptFlags = __nesc_atomic_start();
      old_full = TOSH_sched_full;
      TOSH_sched_full++;
      TOSH_sched_full &= TOSH_TASK_BITMASK;
      func = TOSH_queue[(int )old_full].tp;
      TOSH_queue[(int )old_full].tp = 0;
      __nesc_atomic_end(fInterruptFlags);
      func();
      return 1;
    }
}

static inline void TOSH_run_task(void)
#line 162
{
  while (TOSH_run_next_task()) 
    ;
  TOSH_sleep();
  TOSH_wait();
}

# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t TimerM___PowerManagement___adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM___PowerManagement___adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void HPLClock___Clock___setInterval(uint8_t value)
#line 87
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = value;
}

# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerM___Clock___setInterval(uint8_t arg_0xa352f30){
#line 105
  HPLClock___Clock___setInterval(arg_0xa352f30);
#line 105
}
#line 105
# 116 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
static void TimerM___adjustInterval(void)
#line 116
{
  uint8_t i;
#line 117
  uint8_t val = TimerM___maxTimerInterval;

#line 118
  if (TimerM___mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM___mState & (0x1 << i) && TimerM___mTimerList[i].ticksLeft < val) {
              val = TimerM___mTimerList[i].ticksLeft;
            }
        }
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
        {
          TimerM___mInterval = val;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
        }
#line 128
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
        {
          TimerM___mInterval = TimerM___maxTimerInterval;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
        }
#line 135
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM___PowerManagement___adjustPower();
}

# 83 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_PW2_PIN(void)
#line 83
{
#line 83
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x15 + 0x20) &= ~(1 << 2);
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_CLR_SOUNDER_CTL_PIN(void)
#line 75
{
#line 75
  TOSH_CLR_PW2_PIN();
}

static inline  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/SounderM.nc"
result_t SounderM___StdControl___stop(void)
#line 63
{
  TOSH_CLR_SOUNDER_CTL_PIN();
  {
  }
#line 65
  ;
  return SUCCESS;
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SurgeM___Sounder___stop(void){
#line 78
  unsigned char result;
#line 78

#line 78
  result = SounderM___StdControl___stop();
#line 78

#line 78
  return result;
#line 78
}
#line 78
# 83 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_PW2_PIN(void)
#line 83
{
#line 83
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x15 + 0x20) |= 1 << 2;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_SET_SOUNDER_CTL_PIN(void)
#line 75
{
#line 75
  TOSH_SET_PW2_PIN();
}

static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/SounderM.nc"
result_t SounderM___StdControl___start(void)
#line 57
{
  TOSH_SET_SOUNDER_CTL_PIN();
  {
  }
#line 59
  ;
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SurgeM___Sounder___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SounderM___StdControl___start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 77 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM___HPLADC___samplePort(uint8_t arg_0xa3fde88){
#line 77
  unsigned char result;
#line 77

#line 77
  result = HPLADCC___ADC___samplePort(arg_0xa3fde88);
#line 77

#line 77
  return result;
#line 77
}
#line 77
static 
# 132 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
__inline result_t ADCM___startGet(uint8_t newState, uint8_t port)
#line 132
{
  uint16_t PortMask;
#line 133
  uint16_t oldReqVector;
  result_t Result = SUCCESS;

  if (port > TOSH_ADC_PORTMAPSIZE) {
      return FAIL;
    }

  PortMask = 1 << port;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 142
    {
      if ((PortMask & ADCM___ReqVector) != 0) {

          Result = FAIL;
        }
      else {
          oldReqVector = ADCM___ReqVector;
          ADCM___ReqVector |= PortMask;
          if (newState == ADCM___CONTINUOUS_CONVERSION) {
              ADCM___ContReqMask |= PortMask;
            }
          if (oldReqVector == 0) {

              ADCM___ReqPort = port;
              Result = ADCM___HPLADC___samplePort(port);
            }
        }
    }
#line 159
    __nesc_atomic_end(__nesc_atomic); }


  return Result;
}

static inline   result_t ADCM___ADC___getData(uint8_t port)
#line 165
{
  return ADCM___startGet(ADCM___SINGLE_CONVERSION, port);
}

# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM___InternalPhotoADC___getData(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = ADCM___ADC___getData(TOS_ADC_PHOTO_PORT);
#line 52

#line 52
  return result;
#line 52
}
#line 52
# 73 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_INT2_INPUT(void)
#line 73
{
#line 73
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) &= ~(1 << 2);
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_MAKE_TEMP_CTL_INPUT(void)
#line 72
{
#line 72
  TOSH_MAKE_INT2_INPUT();
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_INT2_PIN(void)
#line 73
{
#line 73
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 2);
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_CLR_TEMP_CTL_PIN(void)
#line 72
{
#line 72
  TOSH_CLR_INT2_PIN();
}

static inline   
# 146 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___ExternalPhotoADC___getData(void)
#line 146
{
  uint8_t oldState;

#line 148
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 148
    {
      oldState = PhotoTempM___state;
      if (PhotoTempM___state == PhotoTempM___IDLE) {
          PhotoTempM___state = PhotoTempM___BUSY;
        }
    }
#line 153
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == PhotoTempM___IDLE) {
      TOSH_CLR_TEMP_CTL_PIN();
      TOSH_MAKE_TEMP_CTL_INPUT();
      TOSH_SET_PHOTO_CTL_PIN();
      TOSH_MAKE_PHOTO_CTL_OUTPUT();
      return PhotoTempM___InternalPhotoADC___getData();
    }
  return FAIL;
}

# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t SurgeM___ADC___getData(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = PhotoTempM___ExternalPhotoADC___getData();
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline  
# 118 "SurgeM.nc"
result_t SurgeM___Timer___fired(void)
#line 118
{
  {
  }
#line 119
  ;
  SurgeM___timer_ticks++;
  if (SurgeM___timer_ticks % SurgeM___TIMER_GETADC_COUNT == 0) {
      SurgeM___ADC___getData();
    }

  if (SurgeM___focused && SurgeM___timer_ticks % SurgeM___TIMER_CHIRP_COUNT == 0) {
      SurgeM___Sounder___start();
    }

  if (SurgeM___focused && SurgeM___timer_ticks % SurgeM___TIMER_CHIRP_COUNT == 1) {
      SurgeM___Sounder___stop();
    }
  return SUCCESS;
}

static inline  
# 177 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___ActivityTimer___fired(void)
#line 177
{
  AMPromiscuous___lastCount = AMPromiscuous___counter;
  AMPromiscuous___counter = 0;
  return SUCCESS;
}

static inline  
# 614 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___Timer___fired(void)
#line 614
{
  TOS_post(MultiHopLEPSM___TimerTask);
  return SUCCESS;
}

static inline   
# 154 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___Timer___default___fired(uint8_t id)
#line 154
{
  return SUCCESS;
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t TimerM___Timer___fired(uint8_t arg_0xa332858){
#line 73
  unsigned char result;
#line 73

#line 73
  switch (arg_0xa332858) {
#line 73
    case 0:
#line 73
      result = SurgeM___Timer___fired();
#line 73
      break;
#line 73
    case 1:
#line 73
      result = AMPromiscuous___ActivityTimer___fired();
#line 73
      break;
#line 73
    case 2:
#line 73
      result = MultiHopLEPSM___Timer___fired();
#line 73
      break;
#line 73
    default:
#line 73
      result = TimerM___Timer___default___fired(arg_0xa332858);
#line 73
    }
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 166 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t TimerM___dequeue(void)
#line 166
{
  if (TimerM___queue_size == 0) {
    return NUM_TIMERS;
    }
#line 169
  if (TimerM___queue_head == NUM_TIMERS - 1) {
    TimerM___queue_head = -1;
    }
#line 171
  TimerM___queue_head++;
  TimerM___queue_size--;
  return TimerM___queue[(uint8_t )TimerM___queue_head];
}

static inline  void TimerM___signalOneTimer(void)
#line 176
{
  uint8_t itimer = TimerM___dequeue();

#line 178
  if (itimer < NUM_TIMERS) {
    TimerM___Timer___fired(itimer);
    }
}

static inline 
#line 158
void TimerM___enqueue(uint8_t value)
#line 158
{
  if (TimerM___queue_tail == NUM_TIMERS - 1) {
    TimerM___queue_tail = -1;
    }
#line 161
  TimerM___queue_tail++;
  TimerM___queue_size++;
  TimerM___queue[(uint8_t )TimerM___queue_tail] = value;
}

static inline  
#line 182
void TimerM___HandleFire(void)
#line 182
{
  uint8_t i;

#line 184
  TimerM___setIntervalFlag = 1;
  if (TimerM___mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM___mState & (0x1 << i)) {
              TimerM___mTimerList[i].ticksLeft -= TimerM___mInterval + 1;
              if (TimerM___mTimerList[i].ticksLeft <= 2) {
                  if (TimerM___mTimerList[i].type == TIMER_REPEAT) {
                      TimerM___mTimerList[i].ticksLeft += TimerM___mTimerList[i].ticks;
                    }
                  else 
#line 192
                    {
                      TimerM___mState &= ~(0x1 << i);
                    }
                  TimerM___enqueue(i);
                  TOS_post(TimerM___signalOneTimer);
                }
            }
        }
    }
  TimerM___adjustInterval();
}

static inline   result_t TimerM___Clock___fire(void)
#line 204
{
  TOS_post(TimerM___HandleFire);
  return SUCCESS;
}

# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLClock___Clock___fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerM___Clock___fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
# 99 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t HPLADCC___ADC___dataReady(uint16_t arg_0xa40e9e8){
#line 99
  unsigned char result;
#line 99

#line 99
  result = ADCM___HPLADC___dataReady(arg_0xa40e9e8);
#line 99

#line 99
  return result;
#line 99
}
#line 99
static 
# 165 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
__inline void __nesc_enable_interrupt(void)
#line 165
{
	__BLAST_interrupt_enabled = 1;
   __asm volatile ("sei");}

# 131 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
void /* __attribute((signal)) */   __vector_21(void)
#line 131
{
  uint16_t data = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x04 + 0x20);

  __nesc_enable_interrupt();
  HPLADCC___ADC___dataReady(data);
}

static inline   
#line 125
result_t HPLADCC___ADC___sampleStop(void)
#line 125
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 7);
  return SUCCESS;
}

# 91 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM___HPLADC___sampleStop(void){
#line 91
  unsigned char result;
#line 91

#line 91
  result = HPLADCC___ADC___sampleStop();
#line 91

#line 91
  return result;
#line 91
}
#line 91
static inline    
# 231 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___ExternalTempADC___default___dataReady(uint16_t data)
#line 231
{
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM___ExternalTempADC___dataReady(uint16_t arg_0xa2cc418){
#line 70
  unsigned char result;
#line 70

#line 70
  result = PhotoTempM___ExternalTempADC___default___dataReady(arg_0xa2cc418);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline   
# 235 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___InternalTempADC___dataReady(uint16_t data)
#line 235
{
  uint8_t oldState;

#line 237
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 237
    {
      oldState = PhotoTempM___state;
      if (PhotoTempM___state == PhotoTempM___BUSY) {
          PhotoTempM___state = PhotoTempM___IDLE;
        }
    }
#line 242
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == PhotoTempM___BUSY) {


      return PhotoTempM___ExternalTempADC___dataReady(data);
    }
  else {
#line 247
    if (oldState == PhotoTempM___CONTINUOUS) {
        int ret;

#line 249
        ret = PhotoTempM___ExternalTempADC___dataReady(data);
        if (ret == FAIL) {


            { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 253
              {
                PhotoTempM___state = PhotoTempM___IDLE;
              }
#line 255
              __nesc_atomic_end(__nesc_atomic); }
          }
        return ret;
      }
    }
#line 259
  return FAIL;
}

static inline   
# 135 "SurgeM.nc"
result_t SurgeM___ADC___dataReady(uint16_t data)
#line 135
{


  {
  }
#line 138
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 139
    {
      if (!SurgeM___gfSendBusy) {
          SurgeM___gfSendBusy = TRUE;
          SurgeM___gSensorData = data;
          TOS_post(SurgeM___SendData);
        }
    }
#line 145
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM___ExternalPhotoADC___dataReady(uint16_t arg_0xa2cc418){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SurgeM___ADC___dataReady(arg_0xa2cc418);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline   
# 204 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM___InternalPhotoADC___dataReady(uint16_t data)
#line 204
{
  uint8_t oldState;

#line 206
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 206
    {
      oldState = PhotoTempM___state;
      if (PhotoTempM___state == PhotoTempM___BUSY) {
          PhotoTempM___state = PhotoTempM___IDLE;
        }
    }
#line 211
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == PhotoTempM___BUSY) {


      return PhotoTempM___ExternalPhotoADC___dataReady(data);
    }
  else {
#line 216
    if (oldState == PhotoTempM___CONTINUOUS) {
        int ret;

#line 218
        ret = PhotoTempM___ExternalPhotoADC___dataReady(data);
        if (ret == FAIL) {


            { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 222
              {
                PhotoTempM___state = PhotoTempM___IDLE;
              }
#line 224
              __nesc_atomic_end(__nesc_atomic); }
          }
        return ret;
      }
    }
#line 228
  return FAIL;
}

static inline    
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM___ADC___default___dataReady(uint8_t port, uint16_t data)
#line 91
{
  return FAIL;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t ADCM___ADC___dataReady(uint8_t arg_0xa401b10, uint16_t arg_0xa2cc418){
#line 70
  unsigned char result;
#line 70

#line 70
  switch (arg_0xa401b10) {
#line 70
    case TOS_ADC_PHOTO_PORT:
#line 70
      result = PhotoTempM___InternalPhotoADC___dataReady(arg_0xa2cc418);
#line 70
      break;
#line 70
    case TOS_ADC_TEMP_PORT:
#line 70
      result = PhotoTempM___InternalTempADC___dataReady(arg_0xa2cc418);
#line 70
      break;
#line 70
    default:
#line 70
      result = ADCM___ADC___default___dataReady(arg_0xa401b10, arg_0xa2cc418);
#line 70
    }
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
inline static  void *SurgeM___Send___getBuffer(TOS_MsgPtr arg_0xa2f9bc8, uint16_t *arg_0xa2f9d30){
#line 106
  void *result;
#line 106

#line 106
  result = MultiHopEngineM___Send___getBuffer(AM_SURGEMSG, arg_0xa2f9bc8, arg_0xa2f9d30);
#line 106

#line 106
  return result;
#line 106
}
#line 106
static inline  
# 565 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
uint16_t MultiHopLEPSM___RouteControl___getParent(void)
#line 565
{

  uint16_t addr;

  addr = MultiHopLEPSM___gpCurrentParent != (void *)0 ? MultiHopLEPSM___gpCurrentParent->id : 0xffff;

  return addr;
}

# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteControl.nc"
inline static  uint16_t MultiHopEngineM___RouteSelectCntl___getParent(void){
#line 49
  unsigned int result;
#line 49

#line 49
  result = MultiHopLEPSM___RouteControl___getParent();
#line 49

#line 49
  return result;
#line 49
}
#line 49
static inline  
# 215 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
uint16_t MultiHopEngineM___RouteControl___getParent(void)
#line 215
{
  return MultiHopEngineM___RouteSelectCntl___getParent();
}

# 49 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteControl.nc"
inline static  uint16_t SurgeM___RouteControl___getParent(void){
#line 49
  unsigned int result;
#line 49

#line 49
  result = MultiHopEngineM___RouteControl___getParent();
#line 49

#line 49
  return result;
#line 49
}
#line 49
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t MultiHopEngineM___SendMsg___send(uint8_t arg_0xa618ec8, uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428){
#line 48
  unsigned char result;
#line 48

#line 48
  result = QueuedSendM___QueueSendMsg___send(arg_0xa618ec8, arg_0xa442190, arg_0xa4422d8, arg_0xa442428);
#line 48

#line 48
  return result;
#line 48
}
#line 48
# 71 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteSelect.nc"
inline static  result_t MultiHopEngineM___RouteSelect___selectRoute(TOS_MsgPtr arg_0xa634960, uint8_t arg_0xa634aa8){
#line 71
  unsigned char result;
#line 71

#line 71
  result = MultiHopLEPSM___RouteSelect___selectRoute(arg_0xa634960, arg_0xa634aa8);
#line 71

#line 71
  return result;
#line 71
}
#line 71
static inline  
# 552 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___RouteSelect___initializeFields(TOS_MsgPtr Msg, uint8_t id)
#line 552
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)&Msg->data[0];

  pMHMsg->sourceaddr = pMHMsg->originaddr = TOS_LOCAL_ADDRESS;
  pMHMsg->hopcount = MultiHopLEPSM___ROUTE_INVALID;

  return SUCCESS;
}

# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RouteSelect.nc"
inline static  result_t MultiHopEngineM___RouteSelect___initializeFields(TOS_MsgPtr arg_0xa635020, uint8_t arg_0xa635168){
#line 86
  unsigned char result;
#line 86

#line 86
  result = MultiHopLEPSM___RouteSelect___initializeFields(arg_0xa635020, arg_0xa635168);
#line 86

#line 86
  return result;
#line 86
}
#line 86
static inline  
# 124 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
result_t MultiHopEngineM___Send___send(uint8_t id, TOS_MsgPtr pMsg, uint16_t PayloadLen)
#line 124
{

  uint16_t usMHLength = (size_t )& ((TOS_MHopMsg *)0)->data + PayloadLen;

  if (usMHLength > 29) {
      return FAIL;
    }



  MultiHopEngineM___RouteSelect___initializeFields(pMsg, id);

  if (MultiHopEngineM___RouteSelect___selectRoute(pMsg, id) != SUCCESS) {
      return FAIL;
    }



  if (MultiHopEngineM___SendMsg___send(id, pMsg->addr, usMHLength, pMsg) != SUCCESS) {
      return FAIL;
    }

  return SUCCESS;
}

# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Send.nc"
inline static  result_t SurgeM___Send___send(TOS_MsgPtr arg_0xa2f9440, uint16_t arg_0xa2f9590){
#line 83
  unsigned char result;
#line 83

#line 83
  result = MultiHopEngineM___Send___send(AM_SURGEMSG, arg_0xa2f9440, arg_0xa2f9590);
#line 83

#line 83
  return result;
#line 83
}
#line 83
static inline 
# 145 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
uint8_t MultiHopLEPSM___findEntryToBeReplaced(void)
#line 145
{
  uint8_t i = 0;
  uint8_t minSendEst = -1;
  uint8_t minSendEstIndex = MultiHopLEPSM___ROUTE_INVALID;

#line 149
  for (i = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
      if ((MultiHopLEPSM___NeighborTbl[i].flags & MultiHopLEPSM___NBRFLAG_VALID) == 0) {
          return i;
        }
      if (minSendEst >= MultiHopLEPSM___NeighborTbl[i].sendEst) {
          minSendEst = MultiHopLEPSM___NeighborTbl[i].sendEst;
          minSendEstIndex = i;
        }
    }
  return minSendEstIndex;
}

static inline 
#line 127
uint8_t MultiHopLEPSM___findEntry(uint8_t id)
#line 127
{
  uint8_t i = 0;

#line 129
  for (i = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
      if (MultiHopLEPSM___NeighborTbl[i].flags & MultiHopLEPSM___NBRFLAG_VALID && MultiHopLEPSM___NeighborTbl[i].id == id) {
          return i;
        }
    }
  return MultiHopLEPSM___ROUTE_INVALID;
}

static inline 
#line 195
uint8_t MultiHopLEPSM___findPreparedIndex(uint16_t id)
#line 195
{
  uint8_t indes = MultiHopLEPSM___findEntry(id);

#line 197
  if (indes == (uint8_t )MultiHopLEPSM___ROUTE_INVALID) {
      indes = MultiHopLEPSM___findEntryToBeReplaced();
      MultiHopLEPSM___newEntry(indes, id);
    }
  return indes;
}

static inline  
#line 645
result_t MultiHopLEPSM___Snoop___intercept(uint8_t id, TOS_MsgPtr Msg, void *Payload, uint16_t Len)
#line 645
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)&Msg->data[0];
  uint8_t iNbr;

  MultiHopLEPSM___updateNbrCounters(pMHMsg->sourceaddr, pMHMsg->seqno, &iNbr);

  return SUCCESS;
}

# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
inline static  result_t MultiHopEngineM___Snoop___intercept(uint8_t arg_0xa61bd30, TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578){
#line 86
  unsigned char result;
#line 86

#line 86
  result = MultiHopLEPSM___Snoop___intercept(arg_0xa61bd30, arg_0xa61f2c8, arg_0xa61f420, arg_0xa61f578);
#line 86

#line 86
  return result;
#line 86
}
#line 86
# 161 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
static TOS_MsgPtr MultiHopEngineM___mForward(TOS_MsgPtr pMsg, uint8_t id)
#line 161
{
  TOS_MsgPtr pNewBuf = pMsg;

  if ((MultiHopEngineM___iFwdBufHead + 1) % MultiHopEngineM___FWD_QUEUE_SIZE == MultiHopEngineM___iFwdBufTail) {
    return pNewBuf;
    }
  if (MultiHopEngineM___RouteSelect___selectRoute(pMsg, id) != SUCCESS) {
    return pNewBuf;
    }


  if (MultiHopEngineM___SendMsg___send(id, pMsg->addr, pMsg->length, pMsg) == SUCCESS) {
      pNewBuf = MultiHopEngineM___FwdBufList[MultiHopEngineM___iFwdBufHead];
      MultiHopEngineM___FwdBufList[MultiHopEngineM___iFwdBufHead] = pMsg;
      MultiHopEngineM___iFwdBufHead++;
#line 175
      MultiHopEngineM___iFwdBufHead %= MultiHopEngineM___FWD_QUEUE_SIZE;
    }

  return pNewBuf;
}

static inline   
#line 250
result_t MultiHopEngineM___Intercept___default___intercept(uint8_t id, TOS_MsgPtr pMsg, void *payload, 
uint16_t payloadLen)
#line 251
{
  return SUCCESS;
}

# 86 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Intercept.nc"
inline static  result_t MultiHopEngineM___Intercept___intercept(uint8_t arg_0xa61b800, TOS_MsgPtr arg_0xa61f2c8, void *arg_0xa61f420, uint16_t arg_0xa61f578){
#line 86
  unsigned char result;
#line 86

#line 86
    result = MultiHopEngineM___Intercept___default___intercept(arg_0xa61b800, arg_0xa61f2c8, arg_0xa61f420, arg_0xa61f578);
#line 86

#line 86
  return result;
#line 86
}
#line 86
static inline  
# 182 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
TOS_MsgPtr MultiHopEngineM___ReceiveMsg___receive(uint8_t id, TOS_MsgPtr pMsg)
#line 182
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)pMsg->data;
  uint16_t PayloadLen = pMsg->length - (size_t )& ((TOS_MHopMsg *)0)->data;







  if (pMsg->addr == TOS_LOCAL_ADDRESS) {
      if (MultiHopEngineM___Intercept___intercept(id, pMsg, &pMHMsg->data[0], PayloadLen) == SUCCESS) {
          pMsg = MultiHopEngineM___mForward(pMsg, id);
        }
    }
  else {

      MultiHopEngineM___Snoop___intercept(id, pMsg, &pMHMsg->data[0], PayloadLen);
    }

  return pMsg;
}

# 68 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t SurgeM___Timer___stop(void){
#line 68
  unsigned char result;
#line 68

#line 68
  result = TimerM___Timer___stop(0);
#line 68

#line 68
  return result;
#line 68
}
#line 68
# 75 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_MAKE_SOUNDER_CTL_OUTPUT(void)
#line 75
{
}

static inline  
# 50 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/SounderM.nc"
result_t SounderM___StdControl___init(void)
#line 50
{
  TOSH_MAKE_SOUNDER_CTL_OUTPUT();
  TOSH_CLR_SOUNDER_CTL_PIN();
  {
  }
#line 53
  ;
  return SUCCESS;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SurgeM___Sounder___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = SounderM___StdControl___init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 136 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC___Leds___yellowOff(void)
#line 136
{
  {
  }
#line 137
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 138
    {
      TOSH_SET_YELLOW_LED_PIN();
      LedsC___ledsOn &= ~LedsC___YELLOW_BIT;
    }
#line 141
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 122 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SurgeM___Leds___yellowOff(void){
#line 122
  unsigned char result;
#line 122

#line 122
  result = LedsC___Leds___yellowOff();
#line 122

#line 122
  return result;
#line 122
}
#line 122
static inline   
# 107 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC___Leds___greenOff(void)
#line 107
{
  {
  }
#line 108
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 109
    {
      TOSH_SET_GREEN_LED_PIN();
      LedsC___ledsOn &= ~LedsC___GREEN_BIT;
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 97 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SurgeM___Leds___greenOff(void){
#line 97
  unsigned char result;
#line 97

#line 97
  result = LedsC___Leds___greenOff();
#line 97

#line 97
  return result;
#line 97
}
#line 97
static inline  
# 161 "SurgeM.nc"
TOS_MsgPtr SurgeM___Bcast___receive(TOS_MsgPtr pMsg, void *payload, uint16_t payloadLen)
#line 161
{
  SurgeCmdMsg *pCmdMsg = (SurgeCmdMsg *)payload;

  {
  }
#line 164
  ;

  if (pCmdMsg->type == SURGE_TYPE_SETRATE) {
      SurgeM___timer_rate = pCmdMsg->args.newrate;
      {
      }
#line 168
      ;
      SurgeM___Timer___stop();
      SurgeM___Timer___start(TIMER_REPEAT, SurgeM___timer_rate);
    }
  else {
#line 172
    if (pCmdMsg->type == SURGE_TYPE_SLEEP) {

        {
        }
#line 174
        ;
        SurgeM___sleeping = TRUE;
        SurgeM___Timer___stop();
        SurgeM___Leds___greenOff();
        SurgeM___Leds___yellowOff();
      }
    else {
#line 180
      if (pCmdMsg->type == SURGE_TYPE_WAKEUP) {
          {
          }
#line 181
          ;


          if (SurgeM___sleeping) {
              SurgeM___initialize();
              SurgeM___Timer___start(TIMER_REPEAT, SurgeM___timer_rate);
              SurgeM___sleeping = FALSE;
            }
        }
      else {
#line 190
        if (pCmdMsg->type == SURGE_TYPE_FOCUS) {
            {
            }
#line 191
            ;


            if (pCmdMsg->args.focusaddr == TOS_LOCAL_ADDRESS) {

                SurgeM___focused = TRUE;
                SurgeM___Sounder___init();
                SurgeM___Timer___stop();
                SurgeM___Timer___start(TIMER_REPEAT, FOCUS_TIMER_RATE);
              }
            else 
#line 200
              {

                SurgeM___Timer___stop();
                SurgeM___Timer___start(TIMER_REPEAT, FOCUS_NOTME_TIMER_RATE);
              }
          }
        else {
#line 206
          if (pCmdMsg->type == SURGE_TYPE_UNFOCUS) {

              {
              }
#line 208
              ;
              SurgeM___focused = FALSE;
              SurgeM___Sounder___stop();
              SurgeM___Timer___stop();
              SurgeM___Timer___start(TIMER_REPEAT, SurgeM___timer_rate);
            }
          }
        }
      }
    }
#line 214
  return pMsg;
}

static inline   
# 148 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
TOS_MsgPtr BcastM___Receive___default___receive(uint8_t id, TOS_MsgPtr pMsg, void *payload, 
uint16_t payloadLen)
#line 149
{
  return pMsg;
}

# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Receive.nc"
inline static  TOS_MsgPtr BcastM___Receive___receive(uint8_t arg_0xa5e4950, TOS_MsgPtr arg_0xa309b60, void *arg_0xa309cb8, uint16_t arg_0xa309e10){
#line 81
  struct TOS_Msg *result;
#line 81

#line 81
  switch (arg_0xa5e4950) {
#line 81
    case AM_SURGECMDMSG:
#line 81
      result = SurgeM___Bcast___receive(arg_0xa309b60, arg_0xa309cb8, arg_0xa309e10);
#line 81
      break;
#line 81
    default:
#line 81
      result = BcastM___Receive___default___receive(arg_0xa5e4950, arg_0xa309b60, arg_0xa309cb8, arg_0xa309e10);
#line 81
    }
#line 81

#line 81
  return result;
#line 81
}
#line 81
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t BcastM___SendMsg___send(uint8_t arg_0xa5e57e0, uint16_t arg_0xa442190, uint8_t arg_0xa4422d8, TOS_MsgPtr arg_0xa442428){
#line 48
  unsigned char result;
#line 48

#line 48
  result = QueuedSendM___QueueSendMsg___send(arg_0xa5e57e0, arg_0xa442190, arg_0xa4422d8, arg_0xa442428);
#line 48

#line 48
  return result;
#line 48
}
#line 48
static inline 
# 137 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
void *nmemcpy(void *to, const void *from, size_t n)
{
  char *cto = to;
  const char *cfrom = from;

  while (n--) * cto++ = * cfrom++;

  return to;
}

# 93 "C:/cygwin/opt/tinyos-1.x/tos/lib/Broadcast/BcastM.nc"
static void BcastM___FwdBcast(TOS_BcastMsg *pRcvMsg, uint8_t Len, uint8_t id)
#line 93
{
  TOS_BcastMsg *pFwdMsg;

  if ((BcastM___iFwdBufHead + 1) % BcastM___FWD_QUEUE_SIZE == BcastM___iFwdBufTail) {

      return;
    }

  pFwdMsg = (TOS_BcastMsg *)& BcastM___FwdBuffer[BcastM___iFwdBufHead].data;

  nmemcpy(pFwdMsg, pRcvMsg, sizeof(TOS_BcastMsg ));

  {
  }
#line 105
  ;
  if (BcastM___SendMsg___send(id, TOS_BCAST_ADDR, sizeof(TOS_BcastMsg ), &BcastM___FwdBuffer[BcastM___iFwdBufHead]) == SUCCESS) {
      BcastM___iFwdBufHead++;
#line 107
      BcastM___iFwdBufHead %= BcastM___FWD_QUEUE_SIZE;
    }
}

#line 73
static bool BcastM___newBcast(int16_t proposed)
#line 73
{







  if (proposed - BcastM___BcastSeqno > 0) {
      BcastM___BcastSeqno++;
      return TRUE;
    }
  else 
#line 84
    {
      return FALSE;
    }
}

static inline  
#line 135
TOS_MsgPtr BcastM___ReceiveMsg___receive(uint8_t id, TOS_MsgPtr pMsg)
#line 135
{
  TOS_BcastMsg *pBCMsg = (TOS_BcastMsg *)pMsg->data;
  uint16_t Len = pMsg->length - (size_t )& ((TOS_BcastMsg *)0)->data;

  {
  }
#line 139
  ;

  if (BcastM___newBcast(pBCMsg->seqno)) {
      BcastM___FwdBcast(pBCMsg, pMsg->length, id);
      BcastM___Receive___receive(id, pMsg, &pBCMsg->data[0], Len);
    }
  return pMsg;
}

static inline  
# 619 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
TOS_MsgPtr MultiHopLEPSM___ReceiveMsg___receive(TOS_MsgPtr Msg)
#line 619
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)&Msg->data[0];
  MultiHopLEPSM___RoutePacket *pRP = (MultiHopLEPSM___RoutePacket *)&pMHMsg->data[0];
  uint16_t saddr;
  uint8_t i;
#line 623
  uint8_t iNbr;

  saddr = pMHMsg->sourceaddr;

  MultiHopLEPSM___updateNbrCounters(saddr, pMHMsg->seqno, &iNbr);


  MultiHopLEPSM___NeighborTbl[iNbr].parent = pRP->parent;
  MultiHopLEPSM___NeighborTbl[iNbr].hop = pMHMsg->hopcount;



  for (i = 0; i < pRP->estEntries; i++) {
      if (pRP->estList[i].id == TOS_LOCAL_ADDRESS) {
          MultiHopLEPSM___NeighborTbl[iNbr].sendEst = pRP->estList[i].receiveEst;
          MultiHopLEPSM___NeighborTbl[iNbr].liveliness++;
        }
    }

  return Msg;
}

static inline   
# 269 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
TOS_MsgPtr AMPromiscuous___ReceiveMsg___default___receive(uint8_t id, TOS_MsgPtr msg)
#line 269
{
  return msg;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr AMPromiscuous___ReceiveMsg___receive(uint8_t arg_0xa456c38, TOS_MsgPtr arg_0xa443de8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  switch (arg_0xa456c38) {
#line 75
    case AM_SURGEMSG:
#line 75
      result = MultiHopEngineM___ReceiveMsg___receive(AM_SURGEMSG, arg_0xa443de8);
#line 75
      break;
#line 75
    case AM_SURGECMDMSG:
#line 75
      result = BcastM___ReceiveMsg___receive(AM_SURGECMDMSG, arg_0xa443de8);
#line 75
      break;
#line 75
    case AM_MULTIHOPMSG:
#line 75
      result = MultiHopLEPSM___ReceiveMsg___receive(arg_0xa443de8);
#line 75
      break;
#line 75
    default:
#line 75
      result = AMPromiscuous___ReceiveMsg___default___receive(arg_0xa456c38, arg_0xa443de8);
#line 75
    }
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline    
# 400 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
void MicaHighSpeedRadioM___RadioSendCoordinator___default___startSymbol(void)
#line 400
{
}

# 45 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioM___RadioSendCoordinator___startSymbol(void){
#line 45
  MicaHighSpeedRadioM___RadioSendCoordinator___default___startSymbol();
#line 45
}
#line 45
static inline 
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

static inline   
# 55 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC___RadioTiming___currentTime(void)
#line 55
{
  return ({
#line 56
    uint16_t __t;
#line 56
    bool bStatus;

#line 56
    bStatus = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) & (1 << 7);
#line 56
     __asm volatile ("cli");__t = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x2C + 0x20);
#line 56
    if (bStatus) {
#line 56
       __asm volatile ("sei");
      }
#line 56
    __t;
  }
  );
}

# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
inline static   uint16_t MicaHighSpeedRadioM___RadioTiming___currentTime(void){
#line 34
  unsigned int result;
#line 34

#line 34
  result = RadioTimingC___RadioTiming___currentTime();
#line 34

#line 34
  return result;
#line 34
}
#line 34
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___send(uint8_t arg_0xa4b6af0){
#line 33
  unsigned char result;
#line 33
#line 33
  result = SpiByteFifoC___SpiByteFifo___send(arg_0xa4b6af0);
#line 33
#line 33
  return result;
#line 33
}
#line 33
# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioM___Code___encode(char arg_0xa4b4638){
#line 34
  unsigned char result;
#line 34

#line 34
  result = SecDedEncoding___Code___encode(arg_0xa4b4638);
#line 34

#line 34
  return result;
#line 34
}
#line 34
static inline   
# 218 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___ChannelMon___idleDetect(void)
#line 218
{
  uint8_t firstSSByte;
  uint8_t firstMsgByte;
  uint16_t timeVal;
  uint16_t tmpCrc;
  uint8_t oldSendState;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 225
    {
      oldSendState = MicaHighSpeedRadioM___send_state;
      if (MicaHighSpeedRadioM___send_state == MicaHighSpeedRadioM___SEND_WAITING) {
          MicaHighSpeedRadioM___send_state = MicaHighSpeedRadioM___IDLE_STATE;
          MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___TRANSMITTING_START;
        }
    }
#line 231
    __nesc_atomic_end(__nesc_atomic); }

  if (oldSendState == MicaHighSpeedRadioM___SEND_WAITING) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 234
        {
          firstMsgByte = ((char *)MicaHighSpeedRadioM___send_ptr)[0];
          firstSSByte = TOSH_MHSR_start[0];

          MicaHighSpeedRadioM___buf_end = MicaHighSpeedRadioM___buf_head = 0;
          MicaHighSpeedRadioM___enc_count = 0;
          MicaHighSpeedRadioM___Code___encode(firstMsgByte);
          MicaHighSpeedRadioM___rx_count = 0;
          MicaHighSpeedRadioM___msg_length = (unsigned char )MicaHighSpeedRadioM___send_ptr->length + MSG_DATA_SIZE - DATA_LENGTH - 2;
        }
#line 243
        __nesc_atomic_end(__nesc_atomic); }



      MicaHighSpeedRadioM___SpiByteFifo___send(firstSSByte);
      timeVal = MicaHighSpeedRadioM___RadioTiming___currentTime();
      tmpCrc = crcByte(0x00, firstMsgByte);
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 250
        {
          MicaHighSpeedRadioM___send_ptr->time = timeVal;
          MicaHighSpeedRadioM___calc_crc = tmpCrc;
        }
#line 253
        __nesc_atomic_end(__nesc_atomic); }
    }
  MicaHighSpeedRadioM___RadioSendCoordinator___startSymbol();
  return 1;
}

# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t ChannelMonC___ChannelMon___idleDetect(void){
#line 39
  unsigned char result;
#line 39

#line 39
  result = MicaHighSpeedRadioM___ChannelMon___idleDetect();
#line 39

#line 39
  return result;
#line 39
}
#line 39
# 37 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t SecDedEncoding___Code___encodeDone(char arg_0xa4b53c0){
#line 37
  unsigned char result;
#line 37

#line 37
  result = MicaHighSpeedRadioM___Code___encodeDone(arg_0xa4b53c0);
#line 37

#line 37
  return result;
#line 37
}
#line 37
static inline 
# 99 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
void SecDedEncoding___radio_encode_thread(void)
#line 99
{
  char ret_high = 0;
  char ret_low = 0;
  char parity = 0;
  char val;

#line 104
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 104
    {
      val = SecDedEncoding___data1;
    }
#line 106
    __nesc_atomic_end(__nesc_atomic); }




  if ((val & 0x1) != 0) {
      parity ^= 0x5b;
      ret_low |= 0x1;
    }
  else 
#line 114
    {
#line 114
      ret_low |= 0x2;
    }
  if ((val & 0x2) != 0) {
      parity ^= 0x58;
      ret_low |= 0x4;
    }
  else 
#line 119
    {
#line 119
      ret_low |= 0x8;
    }
  if ((val & 0x4) != 0) {
      parity ^= 0x52;
      ret_low |= 0x10;
    }
  else 
#line 124
    {
#line 124
      ret_low |= 0x20;
    }
  if ((val & 0x8) != 0) {
      parity ^= 0x51;
      ret_low |= 0x40;
    }
  else 
#line 129
    {
#line 129
      ret_low |= 0x80;
    }
  if ((val & 0x10) != 0) {
      parity ^= 0x4a;
      ret_high |= 0x1;
    }
  else 
#line 134
    {
#line 134
      ret_high |= 0x2;
    }
  if ((val & 0x20) != 0) {
      parity ^= 0x49;
      ret_high |= 0x4;
    }
  else 
#line 139
    {
#line 139
      ret_high |= 0x8;
    }
  if ((val & 0x40) != 0) {
      parity ^= 0x13;
      ret_high |= 0x10;
    }
  else 
#line 144
    {
#line 144
      ret_high |= 0x20;
    }
  if ((val & 0x80) != 0) {
      parity ^= 0x0b;
      ret_high |= 0x40;
    }
  else 
#line 149
    {
#line 149
      ret_high |= 0x80;
    }


  if (!(parity & 0x40)) {
#line 153
    parity |= 0x80;
    }
#line 154
  if (!(parity & 0x50)) {
#line 154
    parity |= 0x20;
    }
#line 155
  if (!(parity & 0xa)) {
#line 155
    parity |= 0x4;
    }
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 157
    {
      SecDedEncoding___state = SecDedEncoding___IDLE_STATE;
    }
#line 159
    __nesc_atomic_end(__nesc_atomic); }
  SecDedEncoding___Code___encodeDone(parity);
  SecDedEncoding___Code___encodeDone(ret_high);
  SecDedEncoding___Code___encodeDone(ret_low);
}

static inline    
# 401 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
void MicaHighSpeedRadioM___RadioSendCoordinator___default___byte(TOS_MsgPtr msg, uint8_t byteCount)
#line 401
{
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioM___RadioSendCoordinator___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58){
#line 48
  MicaHighSpeedRadioM___RadioSendCoordinator___default___byte(arg_0xa4b8c08, arg_0xa4b8d58);
#line 48
}
#line 48
static inline   
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
result_t SecDedEncoding___Code___encode_flush(void)
#line 76
{
  return 1;
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioM___Code___encode_flush(void){
#line 33
  unsigned char result;
#line 33

#line 33
  result = SecDedEncoding___Code___encode_flush();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline   
# 188 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___phaseShift(void)
#line 188
{
  unsigned char f;

#line 190
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 190
    {
      f = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20);
      if (f > 20) {
#line 192
        f -= 20;
        }
#line 193
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = f;
    }
#line 194
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 38 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___phaseShift(void){
#line 38
  unsigned char result;
#line 38

#line 38
  result = SpiByteFifoC___SpiByteFifo___phaseShift();
#line 38

#line 38
  return result;
#line 38
}
#line 38
# 79 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RFM_CTL1_PIN(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_SET_RFM_CTL0_PIN(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 7;
}

#line 77
static __inline void TOSH_MAKE_RFM_TXD_INPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 3);
}

#line 77
static __inline void TOSH_CLR_RFM_TXD_PIN(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 3);
}

static inline   
# 172 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___rxMode(void)
#line 172
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 173
    {
      TOSH_CLR_RFM_TXD_PIN();
      TOSH_MAKE_RFM_TXD_INPUT();
      TOSH_SET_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
    }
#line 178
    __nesc_atomic_end(__nesc_atomic); }






  return SUCCESS;
}

# 37 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___rxMode(void){
#line 37
  unsigned char result;
#line 37

#line 37
  result = SpiByteFifoC___SpiByteFifo___rxMode();
#line 37

#line 37
  return result;
#line 37
}
#line 37
#line 34
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___idle(void){
#line 34
  unsigned char result;
#line 34

#line 34
  result = SpiByteFifoC___SpiByteFifo___idle();
#line 34

#line 34
  return result;
#line 34
}
#line 34
static inline  
# 198 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SlavePin___notifyHigh(void)
#line 198
{
  return SUCCESS;
}

# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static  result_t SlavePinM___SlavePin___notifyHigh(void){
#line 66
  unsigned char result;
#line 66

#line 66
  result = SpiByteFifoC___SlavePin___notifyHigh();
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline  
# 119 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
void SlavePinM___signalHighTask(void)
#line 119
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    SlavePinM___signalHigh = FALSE;
#line 121
    __nesc_atomic_end(__nesc_atomic); }

  SlavePinM___SlavePin___notifyHigh();
}

# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_ONE_WIRE_OUTPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 5;
}

static inline   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC___SlavePin___high(void)
#line 52
{
  TOSH_MAKE_ONE_WIRE_OUTPUT();
  TOSH_SET_ONE_WIRE_PIN();
  return SUCCESS;
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
inline static   result_t SlavePinM___HPLSlavePin___high(void){
#line 48
  unsigned char result;
#line 48

#line 48
  result = HPLSlavePinC___SlavePin___high();
#line 48

#line 48
  return result;
#line 48
}
#line 48
static 
# 126 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
__inline   result_t SlavePinM___SlavePin___high(bool needEvent)
#line 126
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      SlavePinM___n++;
      if (SlavePinM___n > 0) 
        {
          SlavePinM___HPLSlavePin___high();
          if (needEvent || SlavePinM___signalHigh) {
            TOS_post(SlavePinM___signalHighTask);
            }
        }
      else {
#line 137
        SlavePinM___signalHigh |= needEvent;
        }
    }
#line 139
    __nesc_atomic_end(__nesc_atomic); }
#line 139
  return SUCCESS;
}

# 61 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static   result_t SpiByteFifoC___SlavePin___high(bool arg_0xa441ac8){
#line 61
  unsigned char result;
#line 61

#line 61
  result = SlavePinM___SlavePin___high(arg_0xa441ac8);
#line 61

#line 61
  return result;
#line 61
}
#line 61
# 79 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RFM_CTL1_PIN(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 6);
}

static inline  
# 237 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___RadioSend___sendDone(TOS_MsgPtr msg, result_t success)
#line 237
{
  return AMPromiscuous___reportSendDone(msg, success);
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t MicaHighSpeedRadioM___Send___sendDone(TOS_MsgPtr arg_0xa44ebc8, result_t arg_0xa44ed18){
#line 67
  unsigned char result;
#line 67

#line 67
  result = AMPromiscuous___RadioSend___sendDone(arg_0xa44ebc8, arg_0xa44ed18);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 113 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
void MicaHighSpeedRadioM___packetSent(void)
#line 113
{
  TOS_MsgPtr ptr;

#line 115
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
    {
      MicaHighSpeedRadioM___send_state = MicaHighSpeedRadioM___IDLE_STATE;
      MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___IDLE_STATE;
      ptr = (TOS_MsgPtr )MicaHighSpeedRadioM___send_ptr;
    }
#line 119
    __nesc_atomic_end(__nesc_atomic); }
  MicaHighSpeedRadioM___ChannelMon___startSymbolSearch();
  MicaHighSpeedRadioM___Send___sendDone(ptr, SUCCESS);
}

static inline    
#line 403
void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___byte(TOS_MsgPtr msg, uint8_t byteCount)
#line 403
{
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioM___RadioReceiveCoordinator___byte(TOS_MsgPtr arg_0xa4b8c08, uint8_t arg_0xa4b8d58){
#line 48
  MicaHighSpeedRadioM___RadioReceiveCoordinator___default___byte(arg_0xa4b8c08, arg_0xa4b8d58);
#line 48
}
#line 48
static inline   
# 259 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___Code___decodeDone(char data, char error)
#line 259
{
  result_t rval = SUCCESS;

#line 261
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 261
    {
      if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___IDLE_STATE) {
          rval = FAIL;
        }
      else {
#line 265
        if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___RX_STATE) {
            ((char *)MicaHighSpeedRadioM___rec_ptr)[(int )MicaHighSpeedRadioM___rec_count] = data;
            MicaHighSpeedRadioM___rec_count++;
            if (MicaHighSpeedRadioM___rec_count >= MSG_DATA_SIZE) {

                if (MicaHighSpeedRadioM___calc_crc == MicaHighSpeedRadioM___rec_ptr->crc) {
                    MicaHighSpeedRadioM___rec_ptr->crc = 1;
                    if (MicaHighSpeedRadioM___rec_ptr->addr == TOS_LOCAL_ADDRESS || 
                    MicaHighSpeedRadioM___rec_ptr->addr == TOS_BCAST_ADDR) {
                        MicaHighSpeedRadioM___SpiByteFifo___send(0x55);
                      }
                  }
                else 
#line 276
                  {
                    MicaHighSpeedRadioM___rec_ptr->crc = 0;
                  }
                MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___ACK_SEND_STATE;
                rval = 0;
                MicaHighSpeedRadioM___RadioReceiveCoordinator___byte(MicaHighSpeedRadioM___rec_ptr, (uint8_t )MicaHighSpeedRadioM___rec_count);
              }
            else {
#line 283
              if (MicaHighSpeedRadioM___rec_count <= MSG_DATA_SIZE - 2) {
                  MicaHighSpeedRadioM___calc_crc = crcByte(MicaHighSpeedRadioM___calc_crc, data);
                  if (MicaHighSpeedRadioM___rec_count == LENGTH_BYTE_NUMBER) {
                      if ((unsigned char )data < DATA_LENGTH) {
                          MicaHighSpeedRadioM___msg_length = (unsigned char )data + MSG_DATA_SIZE - DATA_LENGTH - 2;
                        }
                    }
                  if (MicaHighSpeedRadioM___rec_count == MicaHighSpeedRadioM___msg_length) {
                      MicaHighSpeedRadioM___rec_count = MSG_DATA_SIZE - 2;
                    }
                }
              }
          }
        }
    }
#line 297
    __nesc_atomic_end(__nesc_atomic); }
#line 296
  return rval;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t SecDedEncoding___Code___decodeDone(char arg_0xa4b4e60, char arg_0xa4b4fa0){
#line 36
  unsigned char result;
#line 36

#line 36
  result = MicaHighSpeedRadioM___Code___decodeDone(arg_0xa4b4e60, arg_0xa4b4fa0);
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline 
# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
void SecDedEncoding___radio_decode_thread(void)
#line 167
{


  char ret_high = 0;
  char ret_low = 0;
  char parity;
  char error = 0;
  short encoded_value = 0;

#line 175
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 175
    {
      parity = SecDedEncoding___data1;
      ret_high = SecDedEncoding___data2;
      ret_low = SecDedEncoding___data3;
    }
#line 179
    __nesc_atomic_end(__nesc_atomic); }

  if ((ret_low & 0x1) != 0) {
#line 181
    encoded_value |= 0x1;
    }
#line 182
  if ((ret_low & 0x4) != 0) {
#line 182
    encoded_value |= 0x2;
    }
#line 183
  if ((ret_low & 0x10) != 0) {
#line 183
    encoded_value |= 0x4;
    }
#line 184
  if ((ret_low & 0x40) != 0) {
#line 184
    encoded_value |= 0x8;
    }
#line 185
  if ((ret_high & 0x01) != 0) {
#line 185
    encoded_value |= 0x10;
    }
#line 186
  if ((ret_high & 0x04) != 0) {
#line 186
    encoded_value |= 0x20;
    }
#line 187
  if ((ret_high & 0x10) != 0) {
#line 187
    encoded_value |= 0x40;
    }
#line 188
  if ((ret_high & 0x40) != 0) {
#line 188
    encoded_value |= 0x80;
    }
  parity = ((parity & 0x3) | ((parity & 0x18) >> 1)) | ((parity & 0x40) >> 2);
  encoded_value = (encoded_value << 5) | parity;


  parity = 0;
  if ((encoded_value & 0x1) != 0) {
#line 195
    parity ^= 0x1;
    }
#line 196
  if ((encoded_value & 0x2) != 0) {
#line 196
    parity ^= 0x2;
    }
#line 197
  if ((encoded_value & 0x4) != 0) {
#line 197
    parity ^= 0x4;
    }
#line 198
  if ((encoded_value & 0x8) != 0) {
#line 198
    parity ^= 0x8;
    }
#line 199
  if ((encoded_value & 0x10) != 0) {
#line 199
    parity ^= 0x10;
    }
#line 200
  if ((encoded_value & 0x20) != 0) {
#line 200
    parity ^= 0x1f;
    }
#line 201
  if ((encoded_value & 0x40) != 0) {
#line 201
    parity ^= 0x1c;
    }
#line 202
  if ((encoded_value & 0x80) != 0) {
#line 202
    parity ^= 0x1a;
    }
#line 203
  if ((encoded_value & 0x100) != 0) {
#line 203
    parity ^= 0x19;
    }
#line 204
  if ((encoded_value & 0x200) != 0) {
#line 204
    parity ^= 0x16;
    }
#line 205
  if ((encoded_value & 0x400) != 0) {
#line 205
    parity ^= 0x15;
    }
#line 206
  if ((encoded_value & 0x800) != 0) {
#line 206
    parity ^= 0xb;
    }
#line 207
  if ((encoded_value & 0x1000) != 0) {
#line 207
    parity ^= 0x7;
    }

  error = -1;
  if (parity == 0) {
    }
  else {
#line 212
    if (parity == 0x1) {
#line 212
        encoded_value ^= 0x1;
      }
    else {
#line 213
      if (parity == 0x2) {
#line 213
          encoded_value ^= 0x2;
        }
      else {
#line 214
        if (parity == 0x4) {
#line 214
            encoded_value ^= 0x4;
          }
        else {
#line 215
          if (parity == 0x8) {
#line 215
              encoded_value ^= 0x8;
            }
          else {
#line 216
            if (parity == 0x10) {
#line 216
                encoded_value ^= 0x10;
              }
            else 
#line 217
              {
                error = 0;
                if (parity == 0x1f) {
#line 219
                    encoded_value ^= 0x20;
                  }
                else {
#line 220
                  if (parity == 0x1c) {
#line 220
                      encoded_value ^= 0x40;
                    }
                  else {
#line 221
                    if (parity == 0x1a) {
#line 221
                        encoded_value ^= 0x80;
                      }
                    else {
#line 222
                      if (parity == 0x19) {
#line 222
                          encoded_value ^= 0x100;
                        }
                      else {
#line 223
                        if (parity == 0x16) {
#line 223
                            encoded_value ^= 0x200;
                          }
                        else {
#line 224
                          if (parity == 0x15) {
#line 224
                              encoded_value ^= 0x400;
                            }
                          else {
#line 225
                            if (parity == 0xb) {
#line 225
                                encoded_value ^= 0x800;
                              }
                            else {
#line 226
                              if (parity == 0x7) {
#line 226
                                  encoded_value ^= 0x1000;
                                }
                              else 
#line 227
                                {
                                  error = 1;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
              }
            }
          }
        }
      }
    }
#line 233
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 233
    {
      SecDedEncoding___state = SecDedEncoding___IDLE_STATE;
    }
#line 235
    __nesc_atomic_end(__nesc_atomic); }
  SecDedEncoding___Code___decodeDone((encoded_value >> 5) & 0xff, error);
}

static inline   
#line 55
result_t SecDedEncoding___Code___decode(char d1)
#line 55
{
  result_t rval = 1;

#line 57
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 57
    {
      if (SecDedEncoding___state == SecDedEncoding___IDLE_STATE) {
          SecDedEncoding___state = SecDedEncoding___DECODING_BYTE_1;
          SecDedEncoding___data1 = d1;
        }
      else {
#line 61
        if (SecDedEncoding___state == SecDedEncoding___DECODING_BYTE_1) {
            SecDedEncoding___state = SecDedEncoding___DECODING_BYTE_2;
            SecDedEncoding___data2 = d1;
          }
        else {
#line 64
          if (SecDedEncoding___state == SecDedEncoding___DECODING_BYTE_2) {
              SecDedEncoding___state = SecDedEncoding___DECODING_BYTE_3;
              SecDedEncoding___data3 = d1;
              SecDedEncoding___radio_decode_thread();
            }
          else 
#line 68
            {
              rval = 0;
            }
          }
        }
    }
#line 73
    __nesc_atomic_end(__nesc_atomic); }
#line 72
  return rval;
}

# 35 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioM___Code___decode(char arg_0xa4b4a48){
#line 35
  unsigned char result;
#line 35

#line 35
  result = SecDedEncoding___Code___decode(arg_0xa4b4a48);
#line 35

#line 35
  return result;
#line 35
}
#line 35
static inline  
# 277 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
TOS_MsgPtr AMPromiscuous___RadioReceive___receive(TOS_MsgPtr packet)
#line 277
{
  return prom_received(packet);
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr MicaHighSpeedRadioM___Receive___receive(TOS_MsgPtr arg_0xa443de8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = AMPromiscuous___RadioReceive___receive(arg_0xa443de8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 100 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
void MicaHighSpeedRadioM___packetReceived(void)
#line 100
{
  TOS_MsgPtr tmp = (TOS_MsgPtr )MicaHighSpeedRadioM___rec_ptr;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 103
    MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___IDLE_STATE;
#line 103
    __nesc_atomic_end(__nesc_atomic); }

  tmp = MicaHighSpeedRadioM___Receive___receive(tmp);
  if (tmp != 0) {
      {/* __blockattribute__((atomic)) */ 
__nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 106
        {
          MicaHighSpeedRadioM___rec_ptr = tmp;
        }
#line 108
        __nesc_atomic_end(__nesc_atomic); }
    }
  MicaHighSpeedRadioM___ChannelMon___startSymbolSearch();
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RFM_CTL0_PIN(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 7);
}

static inline   
# 164 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___txMode(void)
#line 164
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 165
    {
      TOSH_CLR_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
    }
#line 168
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___txMode(void){
#line 36
  unsigned char result;
#line 36

#line 36
  result = SpiByteFifoC___SpiByteFifo___txMode();
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline    
# 402 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
void MicaHighSpeedRadioM___RadioReceiveCoordinator___default___startSymbol(void)
#line 402
{
}

# 45 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioM___RadioReceiveCoordinator___startSymbol(void){
#line 45
  MicaHighSpeedRadioM___RadioReceiveCoordinator___default___startSymbol();
#line 45
}
#line 45
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) &= ~(1 << 5);
}

static inline   
# 58 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC___SlavePin___low(void)
#line 58
{
  TOSH_MAKE_ONE_WIRE_OUTPUT();
  TOSH_CLR_ONE_WIRE_PIN();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
inline static   result_t SlavePinM___HPLSlavePin___low(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLSlavePinC___SlavePin___low();
#line 47

#line 47
  return result;
#line 47
}
#line 47
static 
# 110 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
__inline   result_t SlavePinM___SlavePin___low(void)
#line 110
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      SlavePinM___n--;
      SlavePinM___HPLSlavePin___low();
    }
#line 115
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static   result_t SpiByteFifoC___SlavePin___low(void){
#line 51
  unsigned char result;
#line 51

#line 51
  result = SlavePinM___SlavePin___low();
#line 51

#line 51
  return result;
#line 51
}
#line 51
static inline   
# 119 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___startReadBytes(uint16_t timing)
#line 119
{
  uint8_t oldState;




  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 125
    {
      oldState = SpiByteFifoC___state;
      if (SpiByteFifoC___state == SpiByteFifoC___IDLE) {
          SpiByteFifoC___state = SpiByteFifoC___READING;
        }
    }
#line 130
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == SpiByteFifoC___IDLE) {


      SpiByteFifoC___SlavePin___low();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x1;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = SpiByteFifoC___BIT_RATE;

      timing += 400 - 19;
      if (timing > 0xfff0) {
#line 143
        timing = 0xfff0;
        }
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x19;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = SpiByteFifoC___BIT_RATE - 20;
      while (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x16 + 0x20) & 0x80) {
#line 147
          ;
        }
#line 148
      while (* (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x2C + 0x20) < timing) {
#line 148
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x0;
        }
#line 149
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0xc0;






      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) |= 1 << 6;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 6);
      return SUCCESS;
    }
  return FAIL;
}

# 35 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioM___SpiByteFifo___startReadBytes(uint16_t arg_0xa4b71e0){
#line 35
  unsigned char result;
#line 35

#line 35
  result = SpiByteFifoC___SpiByteFifo___startReadBytes(arg_0xa4b71e0);
#line 35

#line 35
  return result;
#line 35
}
#line 35
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline int TOSH_READ_RFM_RXD_PIN(void)
#line 76
{
#line 76
  if((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x16 + 0x20) & (1 << 2)) != 0) return 1;
  else return 0;
}

static inline   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC___RadioTiming___getTiming(void)
#line 40
{


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 4);
  while (TOSH_READ_RFM_RXD_PIN()) {
    }
#line 45
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x2E + 0x20) = 0x41;

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x36 + 0x20) = 0x1 << 5;

  while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x36 + 0x20) & (0x1 << 5)) == 0) {
    }
#line 50
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) |= 1 << 6;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 6);
  return ({
#line 52
    uint16_t __t;
#line 52
    bool bStatus;

#line 52
    bStatus = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) & (1 << 7);
#line 52
     __asm volatile ("cli");__t = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x26 + 0x20);
#line 52
    if (bStatus) {
#line 52
       __asm volatile ("sei");
      }
#line 52
    __t;
  }
  );
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
inline static   uint16_t MicaHighSpeedRadioM___RadioTiming___getTiming(void){
#line 33
  unsigned int result;
#line 33

#line 33
  result = RadioTimingC___RadioTiming___getTiming();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline   
# 197 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___ChannelMon___startSymDetect(void)
#line 197
{
  uint16_t tmp;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 200
    {
      MicaHighSpeedRadioM___ack_count = 0;
      MicaHighSpeedRadioM___rec_count = 0;
      MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___RX_STATE;
    }
#line 204
    __nesc_atomic_end(__nesc_atomic); }
  tmp = MicaHighSpeedRadioM___RadioTiming___getTiming();

  MicaHighSpeedRadioM___SpiByteFifo___startReadBytes(tmp);
  { //__blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 208
    {
      MicaHighSpeedRadioM___msg_length = MSG_DATA_SIZE - 2;
      MicaHighSpeedRadioM___calc_crc = 0;
      MicaHighSpeedRadioM___rec_ptr = 0; /* BLAST race check */
      MicaHighSpeedRadioM___rec_ptr->time = tmp;
      MicaHighSpeedRadioM___rec_ptr->strength = 0;
    }
#line 213
    /* __nesc_atomic_end(__nesc_atomic); */}
  MicaHighSpeedRadioM___RadioReceiveCoordinator___startSymbol();
  return SUCCESS;
}

# 38 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t ChannelMonC___ChannelMon___startSymDetect(void){
#line 38
  unsigned char result;
#line 38

#line 38
  result = MicaHighSpeedRadioM___ChannelMon___startSymDetect();
#line 38

#line 38
  return result;
#line 38
}
#line 38
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t SpiByteFifoC___SpiByteFifo___dataReady(uint8_t arg_0xa4b7e60){
#line 40
  unsigned char result;
#line 40

#line 40
  result = MicaHighSpeedRadioM___SpiByteFifo___dataReady(arg_0xa4b7e60);
#line 40

#line 40
  return result;
#line 40
}
#line 40
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
void __attribute((signal))   __vector_17(void)
#line 56
{
  uint8_t temp = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = SpiByteFifoC___nextByte;
  SpiByteFifoC___state = SpiByteFifoC___OPEN;






  SpiByteFifoC___SpiByteFifo___dataReady(temp);
}

# 66 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM___ByteComm___rxByteReady(uint8_t arg_0xa572010, bool arg_0xa572158, uint16_t arg_0xa5722b0){
#line 66
  unsigned char result;
#line 66

#line 66
  result = FramerM___ByteComm___rxByteReady(arg_0xa572010, arg_0xa572158, arg_0xa5722b0);
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline   
# 77 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM___HPLUART___get(uint8_t data)
#line 77
{




  UARTM___ByteComm___rxByteReady(data, FALSE, 0);
  {
  }
#line 83
  ;
  return SUCCESS;
}

# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUARTM___UART___get(uint8_t arg_0xa5a3490){
#line 88
  unsigned char result;
#line 88

#line 88
  result = UARTM___HPLUART___get(arg_0xa5a3490);
#line 88

#line 88
  return result;
#line 88
}
#line 88
# 71 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
void __attribute((signal))   __vector_18(void)
#line 71
{
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) & 0x80) {
    HPLUARTM___UART___get(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20));
    }
}

static inline  
# 202 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM___PacketUnknown(void)
#line 202
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 203
    {
      FramerM___gFlags |= FramerM___FLAGS_UNKNOWN;
    }
#line 205
    __nesc_atomic_end(__nesc_atomic); }

  FramerM___StartTx();
}

static inline  
# 273 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
TOS_MsgPtr AMPromiscuous___UARTReceive___receive(TOS_MsgPtr packet)
#line 273
{
  packet->group = TOS_AM_GROUP;
  return prom_received(packet);
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr FramerAckM___ReceiveCombined___receive(TOS_MsgPtr arg_0xa443de8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = AMPromiscuous___UARTReceive___receive(arg_0xa443de8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
TOS_MsgPtr FramerAckM___ReceiveMsg___receive(TOS_MsgPtr Msg)
#line 91
{
  TOS_MsgPtr pBuf;

  pBuf = FramerAckM___ReceiveCombined___receive(Msg);

  return pBuf;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM___ReceiveMsg___receive(TOS_MsgPtr arg_0xa443de8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = FramerAckM___ReceiveMsg___receive(arg_0xa443de8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 328 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___TokenReceiveMsg___ReflectToken(uint8_t Token)
#line 328
{
  result_t Result = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 331
    {
      if (!(FramerM___gFlags & FramerM___FLAGS_TOKENPEND)) {
          FramerM___gFlags |= FramerM___FLAGS_TOKENPEND;
          FramerM___gTxTokenBuf = Token;
        }
      else {
          Result = FAIL;
        }
    }
#line 339
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM___StartTx();
    }

  return Result;
}

# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  result_t FramerAckM___TokenReceiveMsg___ReflectToken(uint8_t arg_0xa5580e8){
#line 88
  unsigned char result;
#line 88

#line 88
  result = FramerM___TokenReceiveMsg___ReflectToken(arg_0xa5580e8);
#line 88

#line 88
  return result;
#line 88
}
#line 88
static inline  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
void FramerAckM___SendAckTask(void)
#line 74
{

  FramerAckM___TokenReceiveMsg___ReflectToken(FramerAckM___gTokenBuf);
}

static inline  TOS_MsgPtr FramerAckM___TokenReceiveMsg___receive(TOS_MsgPtr Msg, uint8_t token)
#line 79
{
  TOS_MsgPtr pBuf;

  FramerAckM___gTokenBuf = token;

  TOS_post(FramerAckM___SendAckTask);

  pBuf = FramerAckM___ReceiveCombined___receive(Msg);

  return pBuf;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM___TokenReceiveMsg___receive(TOS_MsgPtr arg_0xa545948, uint8_t arg_0xa545a90){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = FramerAckM___TokenReceiveMsg___receive(arg_0xa545948, arg_0xa545a90);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 210 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM___PacketRcvd(void)
#line 210
{
  FramerM___MsgRcvEntry_t *pRcv = &FramerM___gMsgRcvTbl[FramerM___gRxTailIndex];
  TOS_MsgPtr pBuf = pRcv->pMsg;


  if (pRcv->Length >= (size_t )& ((struct TOS_Msg *)0)->data) {

      switch (pRcv->Proto) {
          case FramerM___PROTO_ACK: 
            break;
          case FramerM___PROTO_PACKET_ACK: 
            pBuf->crc = 1;
          pBuf = FramerM___TokenReceiveMsg___receive(pBuf, pRcv->Token);
          break;
          case FramerM___PROTO_PACKET_NOACK: 
            pBuf->crc = 1;
          pBuf = FramerM___ReceiveMsg___receive(pBuf);
          break;
          default: 
            FramerM___gTxUnknownBuf = pRcv->Proto;
          TOS_post(FramerM___PacketUnknown);
          break;
        }
    }

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 235
    {
      if (pBuf) {
          pRcv->pMsg = pBuf;
        }
      pRcv->Length = 0;
      pRcv->Token = 0;
      FramerM___gRxTailIndex++;
      FramerM___gRxTailIndex %= FramerM___HDLC_QUEUESIZE;
    }
#line 243
    __nesc_atomic_end(__nesc_atomic); }
}

# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUARTM___UART___putDone(void){
#line 96
  unsigned char result;
#line 96

#line 96
  result = UARTM___HPLUART___putDone();
#line 96

#line 96
  return result;
#line 96
}
#line 96
# 77 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
void __attribute((interrupt))   __vector_20(void)
#line 77
{
  HPLUARTM___UART___putDone();
}

static inline   
# 552 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___ByteComm___txDone(void)
#line 552
{

  if (FramerM___gTxState == FramerM___TXSTATE_FINISH) {
      FramerM___gTxState = FramerM___TXSTATE_IDLE;
      TOS_post(FramerM___PacketSent);
    }

  return SUCCESS;
}

# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM___ByteComm___txDone(void){
#line 83
  unsigned char result;
#line 83

#line 83
  result = FramerM___ByteComm___txDone();
#line 83

#line 83
  return result;
#line 83
}
#line 83
#line 55
inline static   result_t FramerM___ByteComm___txByte(uint8_t arg_0xa559b58){
#line 55
  unsigned char result;
#line 55

#line 55
  result = UARTM___ByteComm___txByte(arg_0xa559b58);
#line 55

#line 55
  return result;
#line 55
}
#line 55
static inline   
# 482 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___ByteComm___txByteReady(bool LastByteSuccess)
#line 482
{
  result_t TxResult = SUCCESS;
  uint8_t nextByte;

  if (LastByteSuccess != TRUE) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 487
        FramerM___gTxState = FramerM___TXSTATE_ERROR;
#line 487
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(FramerM___PacketSent);
      return SUCCESS;
    }

  switch (FramerM___gTxState) {

      case FramerM___TXSTATE_PROTO: 
        FramerM___gTxState = FramerM___TXSTATE_INFO;
      FramerM___gTxRunningCRC = crcByte(FramerM___gTxRunningCRC, FramerM___gTxProto);
      TxResult = FramerM___ByteComm___txByte(FramerM___gTxProto);
      break;

      case FramerM___TXSTATE_INFO: 
        nextByte = FramerM___gpTxBuf[FramerM___gTxByteCnt];

      FramerM___gTxRunningCRC = crcByte(FramerM___gTxRunningCRC, nextByte);
      FramerM___gTxByteCnt++;
      if (FramerM___gTxByteCnt >= FramerM___gTxLength) {
          FramerM___gTxState = FramerM___TXSTATE_FCS1;
        }

      TxResult = FramerM___TxArbitraryByte(nextByte);
      break;

      case FramerM___TXSTATE_ESC: 

        TxResult = FramerM___ByteComm___txByte(FramerM___gTxEscByte ^ 0x20);
	//FramerM___gTxState = FramerM___gPrevTxState; // JHALA -- earlier error ?
      break;

      case FramerM___TXSTATE_FCS1: 
        nextByte = (uint8_t )(FramerM___gTxRunningCRC & 0xff);
      FramerM___gTxState = FramerM___TXSTATE_FCS2;
      TxResult = FramerM___TxArbitraryByte(nextByte);
      break;

      case FramerM___TXSTATE_FCS2: 
        nextByte = (uint8_t )((FramerM___gTxRunningCRC >> 8) & 0xff);
      FramerM___gTxState = FramerM___TXSTATE_ENDFLAG;
      TxResult = FramerM___TxArbitraryByte(nextByte);
      break;

      case FramerM___TXSTATE_ENDFLAG: 
        FramerM___gTxState = FramerM___TXSTATE_FINISH;
      TxResult = FramerM___ByteComm___txByte(FramerM___HDLC_FLAG_BYTE);

      break;

      case FramerM___TXSTATE_FINISH: 
        case FramerM___TXSTATE_ERROR: 

          default: 
            break;
    }


  if (TxResult != SUCCESS) {
      FramerM___gTxState = FramerM___TXSTATE_ERROR;
      TOS_post(FramerM___PacketSent);
    }

  return SUCCESS;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM___ByteComm___txByteReady(bool arg_0xa5727e0){
#line 75
  unsigned char result;
#line 75

#line 75
  result = FramerM___ByteComm___txByteReady(arg_0xa5727e0);
#line 75

#line 75
  return result;
#line 75
}
#line 75
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool  TOS_post(void (*tp)(void))
#line 98
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;
/*


  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;
  TOSH_sched_free++;
  TOSH_sched_free &= TOSH_TASK_BITMASK;

  if (TOSH_sched_free != TOSH_sched_full) {
      __nesc_atomic_end(fInterruptFlags);

      TOSH_queue[tmp].tp = tp;
      return TRUE;
    }
  else {
      TOSH_sched_free = tmp;
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
*/
}


void call_tasks ()  __attribute__((task)){
  int __BLAST_NONDET;
  int random;

 
    switch (__BLAST_NONDET) {
    case 1: 
      { __blockattribute__((atomic)) 
	{__blockattribute__((assume(__BLAST_packetReceived_enabled == 1)))} 

      __BLAST_task_enabled = 0;
      __BLAST_packetReceived_enabled = 0;
      }
      random++;
      MicaHighSpeedRadioM___packetReceived();
      __BLAST_task_enabled = 1;
      break;
    }
 
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void)
#line 54
{
  int __BLAST_NONDET;
/*
  RealMain___hardwareInit();
  RealMain___Pot___init(10);
  TOSH_sched_init();

  RealMain___StdControl___init();
  RealMain___StdControl___start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
*/
  {__blockattribute__((assume(__BLAST_packetReceived_enabled == 0)))};
  while(1) {
	  switch (__BLAST_NONDET) {
	  case 9:
	    {__blockattribute__((atomic)) 
	      {__blockattribute__((assume(__BLAST_interrupt_enabled == 1)))}
	      {__blockattribute__((assume(__BLAST_timer1_interrupt == 1)))}
	    __BLAST_interrupt_enabled = 0;

	    
	    __vector_9(); __BLAST_task_enabled = 1; __BLAST_interrupt_enabled = 1;}
	    break;
	    
	  case 15: if (__BLAST_interrupt_enabled) __vector_15(); break;
	      /* case 17: if (__BLAST_interrupt_enabled) __vector_17(); break; 
		 //Real error? this goes and posts packetRec task ...
		 */
	  case 18: if (__BLAST_interrupt_enabled) __vector_18(); break;
	  case 20: if (__BLAST_interrupt_enabled) __vector_20(); break;
	  case 21: if (__BLAST_interrupt_enabled) __vector_21(); break;
	    
	  default : call_tasks() ; break;
          }
  }

}

static 
# 64 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
void HPLADCC___init_portmap(void)
#line 64
{

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 66
    {
      if (HPLADCC___init_portmap_done == FALSE) {
          int i;

#line 69
          for (i = 0; i < HPLADCC___TOSH_ADC_PORTMAPSIZE; i++) 
            HPLADCC___TOSH_adc_portmap[i] = i;
          HPLADCC___init_portmap_done = TRUE;
        }
    }
#line 73
    __nesc_atomic_end(__nesc_atomic); }
}

static  
# 91 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
result_t QueuedSendM___StdControl___init(void)
#line 91
{
  int i;

#line 93
  for (i = 0; i < QueuedSendM___MESSAGE_QUEUE_SIZE; i++) {
      QueuedSendM___msgqueue[i].length = 0;
    }



  QueuedSendM___retransmit = TRUE;

  QueuedSendM___enqueue_next = 0;
  QueuedSendM___dequeue_next = 0;
  QueuedSendM___fQueueIdle = TRUE;
  return SUCCESS;
}

static  
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___Control___init(void)
#line 91
{
  result_t ok1;
#line 92
  result_t ok2;

#line 93
  AMPromiscuous___TimerControl___init();
  ok1 = AMPromiscuous___UARTControl___init();
  ok2 = AMPromiscuous___RadioControl___init();
  AMPromiscuous___state = FALSE;
  AMPromiscuous___lastCount = 0;
  AMPromiscuous___counter = 0;
  AMPromiscuous___promiscuous_mode = FALSE;
  AMPromiscuous___crc_check = TRUE;
  {
  }
#line 101
  ;

  return rcombine(ok1, ok2);
}

static  
# 72 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___StdControl___init(void)
#line 72
{
  TimerM___mState = 0;
  TimerM___setIntervalFlag = 0;
  TimerM___queue_head = TimerM___queue_tail = -1;
  TimerM___queue_size = 0;
  TimerM___mScale = 3;
  TimerM___mInterval = TimerM___maxTimerInterval;
  return TimerM___Clock___setRate(TimerM___mInterval, TimerM___mScale);
}

static 
# 268 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM___HDLCInitialize(void)
#line 268
{
  int i;

#line 270
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 270
    {
      for (i = 0; i < FramerM___HDLC_QUEUESIZE; i++) {
          FramerM___gMsgRcvTbl[i].pMsg = &FramerM___gMsgRcvBuf[i];
          FramerM___gMsgRcvTbl[i].Length = 0;
          FramerM___gMsgRcvTbl[i].Token = 0;
        }
      FramerM___gTxState = FramerM___TXSTATE_IDLE;
      FramerM___gTxByteCnt = 0;
      FramerM___gTxLength = 0;
      FramerM___gTxRunningCRC = 0;
      FramerM___gpTxMsg = (void *)0;

      FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
      FramerM___gRxHeadIndex = 0;
      FramerM___gRxTailIndex = 0;
      FramerM___gRxByteCnt = 0;
      FramerM___gRxRunningCRC = 0;
      FramerM___gpRxBuf = (uint8_t *)FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].pMsg;
    }
#line 288
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC___ChannelMon___startSymbolSearch(void)
#line 66
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 67
    {

      ChannelMonC___CM_state = ChannelMonC___IDLE_STATE;

      TOSH_SET_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
      TOSH_CLR_RFM_TXD_PIN();






      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
      __BLAST_timer1_interrupt = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 6);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x09;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = ChannelMonC___SAMPLE_RATE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 7; /* TIMSK, OCIE Timer 1 interrupt */
      __BLAST_timer1_interrupt = 1;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 6;
    }
#line 88
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static  
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___Timer___start(uint8_t id, char type, 
uint32_t interval)
#line 94
{
  uint8_t diff;

#line 96
  if (id >= NUM_TIMERS) {
#line 96
    return FAIL;
    }
#line 97
  if (type > 1) {
#line 97
    return FAIL;
    }
#line 98
  TimerM___mTimerList[id].ticks = interval;
  TimerM___mTimerList[id].type = type;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
      interval += diff;
      TimerM___mTimerList[id].ticksLeft = interval;
      TimerM___mState |= 0x1 << id;
      if (interval < TimerM___mInterval) {
          TimerM___mInterval = interval;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
          TimerM___PowerManagement___adjustPower();
        }
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 103 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void)
#line 103
{
  uint8_t mcu;

#line 105
  if (!HPLPowerManagementM___disabled) {
    TOS_post(HPLPowerManagementM___doAdjustment);
    }
  else 
#line 107
    {
      mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM___IDLE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

static  
# 107 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___Control___start(void)
#line 107
{
  result_t ok0;
#line 108
  result_t ok1;
#line 108
  result_t ok2;
#line 108
  result_t ok3;

  ok0 = AMPromiscuous___TimerControl___start();
  ok1 = AMPromiscuous___UARTControl___start();
  ok2 = AMPromiscuous___RadioControl___start();
  ok3 = AMPromiscuous___ActivityTimer___start(TIMER_REPEAT, 1000);
  AMPromiscuous___PowerManagement___adjustPower();



  AMPromiscuous___state = FALSE;

  return rcombine4(ok0, ok1, ok2, ok3);
}

static  
# 416 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
void MultiHopLEPSM___TimerTask(void)
#line 416
{

  {
  }
#line 418
  ;
  MultiHopLEPSM___updateTable();


  {
    int i;

#line 424
    {
    }
#line 424
    ;
    for (i = 0; i < MultiHopLEPSM___ROUTE_TABLE_SIZE; i++) {
        if (MultiHopLEPSM___NeighborTbl[i].flags) {
            {
            }
#line 427
            ;
          }
      }








    if (MultiHopLEPSM___gpCurrentParent) {
        {
        }
#line 439
        ;
      }
  }

  MultiHopLEPSM___chooseParent();

  TOS_post(MultiHopLEPSM___SendRouteTask);
}

static  
# 141 "C:/cygwin/opt/tinyos-1.x/tos/lib/Queue/QueuedSendM.nc"
result_t QueuedSendM___QueueSendMsg___send(uint8_t id, uint16_t address, uint8_t length, TOS_MsgPtr msg)
#line 141
{
  {
  }
#line 142
  ;

  if ((QueuedSendM___enqueue_next + 1) % QueuedSendM___MESSAGE_QUEUE_SIZE == QueuedSendM___dequeue_next) {

      return FAIL;
    }
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].address = address;
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].length = length;
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].id = id;
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].pMsg = msg;
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].xmit_count = 0;
  QueuedSendM___msgqueue[QueuedSendM___enqueue_next].pMsg->ack = 0;

  QueuedSendM___enqueue_next++;
#line 155
  QueuedSendM___enqueue_next %= QueuedSendM___MESSAGE_QUEUE_SIZE;

  {
  }
#line 157
  ;








  if (QueuedSendM___fQueueIdle) {
      QueuedSendM___fQueueIdle = FALSE;
      TOS_post(QueuedSendM___QueueServiceTask);
    }
  return SUCCESS;
}

static  
#line 122
void QueuedSendM___QueueServiceTask(void)
#line 122
{
  uint8_t id;

  if (QueuedSendM___msgqueue[QueuedSendM___dequeue_next].length != 0) {
      QueuedSendM___Leds___greenToggle();
      {
      }
#line 127
      ;
      id = QueuedSendM___msgqueue[QueuedSendM___dequeue_next].id;

      if (!QueuedSendM___SerialSendMsg___send(id, QueuedSendM___msgqueue[QueuedSendM___dequeue_next].address, 
      QueuedSendM___msgqueue[QueuedSendM___dequeue_next].length, 
      QueuedSendM___msgqueue[QueuedSendM___dequeue_next].pMsg)) {
          {
          }
#line 133
          ;
        }
    }
  else {
      QueuedSendM___fQueueIdle = TRUE;
    }
}

static 
# 158 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___StartTx(void)
#line 158
{
  result_t Result = SUCCESS;
  bool fInitiate = FALSE;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 162
    {
      if (FramerM___gTxState == FramerM___TXSTATE_IDLE) {
          if (FramerM___gFlags & FramerM___FLAGS_TOKENPEND) {
              FramerM___gpTxBuf = (uint8_t *)&FramerM___gTxTokenBuf;
              FramerM___gTxProto = FramerM___PROTO_ACK;
              FramerM___gTxLength = sizeof FramerM___gTxTokenBuf;
              fInitiate = TRUE;
              FramerM___gTxState = FramerM___TXSTATE_PROTO;
            }
          else {
#line 171
            if (FramerM___gFlags & FramerM___FLAGS_DATAPEND) {
                FramerM___gpTxBuf = (uint8_t *)FramerM___gpTxMsg;
                FramerM___gTxProto = FramerM___PROTO_PACKET_NOACK;
                FramerM___gTxLength = FramerM___gpTxMsg->length + (MSG_DATA_SIZE - DATA_LENGTH - 2);
                fInitiate = TRUE;
                FramerM___gTxState = FramerM___TXSTATE_PROTO;
              }
            else {
#line 178
              if (FramerM___gFlags & FramerM___FLAGS_UNKNOWN) {
                  FramerM___gpTxBuf = (uint8_t *)&FramerM___gTxUnknownBuf;
                  FramerM___gTxProto = FramerM___PROTO_UNKNOWN;
                  FramerM___gTxLength = sizeof FramerM___gTxUnknownBuf;
                  fInitiate = TRUE;
                  FramerM___gTxState = FramerM___TXSTATE_PROTO;
                }
              }
            }
        }
    }
#line 188
    __nesc_atomic_end(__nesc_atomic); }
#line 188
  if (fInitiate) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 189
        {
          FramerM___gTxRunningCRC = 0;
#line 190
          FramerM___gTxByteCnt = 0;
        }
#line 191
        __nesc_atomic_end(__nesc_atomic); }
      Result = FramerM___ByteComm___txByte(FramerM___HDLC_FLAG_BYTE);
      if (Result != SUCCESS) {
          { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 194
            FramerM___gTxState = FramerM___TXSTATE_ERROR;
#line 194
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(FramerM___PacketSent);
        }
    }

  return Result;
}

static   
# 110 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM___ByteComm___txByte(uint8_t data)
#line 110
{
  bool oldState;

  {
  }
#line 113
  ;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
    {
      oldState = UARTM___state;
      if (UARTM___state == FALSE) //JHALA
	  UARTM___state = TRUE;
    }
#line 118
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == TRUE) {
    return FAIL;
    }
  UARTM___HPLUART___put(data);

  return SUCCESS;
}

static  
# 246 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM___PacketSent(void)
#line 246
{
  result_t TxResult = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 249
    {
      if (FramerM___gTxState == FramerM___TXSTATE_ERROR) {
          TxResult = FAIL;
          FramerM___gTxState = FramerM___TXSTATE_IDLE;
        }
    }
#line 254
    __nesc_atomic_end(__nesc_atomic); }
  if (FramerM___gTxProto == FramerM___PROTO_ACK) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 256
        FramerM___gFlags ^= FramerM___FLAGS_TOKENPEND;
#line 256
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 259
        FramerM___gFlags ^= FramerM___FLAGS_DATAPEND;
#line 259
        __nesc_atomic_end(__nesc_atomic); }
      FramerM___BareSendMsg___sendDone((TOS_MsgPtr )FramerM___gpTxMsg, TxResult);
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 261
        FramerM___gpTxMsg = (void *)0;
#line 261
        __nesc_atomic_end(__nesc_atomic); }
    }


  FramerM___StartTx();
}

static 
# 168 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
result_t AMPromiscuous___reportSendDone(TOS_MsgPtr msg, result_t success)
#line 168
{
  {
  }
#line 169
  ;
  AMPromiscuous___state = FALSE;
  AMPromiscuous___SendMsg___sendDone(msg->type, msg, success);
  AMPromiscuous___sendDone();

  return SUCCESS;
}

static  
# 205 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
result_t MultiHopEngineM___SendMsg___sendDone(uint8_t id, TOS_MsgPtr pMsg, result_t success)
#line 205
{

  if (pMsg == MultiHopEngineM___FwdBufList[MultiHopEngineM___iFwdBufTail]) {
      MultiHopEngineM___iFwdBufTail++;
#line 208
      MultiHopEngineM___iFwdBufTail %= MultiHopEngineM___FWD_QUEUE_SIZE;
    }
  else 
#line 209
    {
      MultiHopEngineM___Send___sendDone(id, pMsg, success);
    }
  return SUCCESS;
}

static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR___Random___rand(void)
#line 70
{
  bool endbit;
  uint16_t tmpShiftReg;

#line 73
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
    {
      tmpShiftReg = RandomLFSR___shiftReg;
      endbit = (tmpShiftReg & 0x8000) != 0;
      tmpShiftReg <<= 1;
      if (endbit) {
        tmpShiftReg ^= 0x100b;
        }
#line 79
      tmpShiftReg++;
      RandomLFSR___shiftReg = tmpShiftReg;
      tmpShiftReg = tmpShiftReg ^ RandomLFSR___mask;
    }
#line 82
    __nesc_atomic_end(__nesc_atomic); }
  return tmpShiftReg;
}

# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void __attribute((interrupt))   __vector_15(void)
#line 167
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 168
    {
      if (HPLClock___set_flag) {
          HPLClock___mscale = HPLClock___nextScale;
          HPLClock___nextScale |= 0x8;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLClock___nextScale;

          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLClock___minterval;
          HPLClock___set_flag = 0;
        }
    }
#line 177
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock___Clock___fire();
}

static   
# 109 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC___ADC___samplePort(uint8_t port)
#line 109
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 110
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x07 + 0x20) = HPLADCC___TOSH_adc_portmap[port];
      if (HPLADCC___TOSH_adc_portmap[port] == 8 && port == 8) {
        TOSH_SET_PW2_PIN();
        }
#line 114
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 6;
    }
#line 116
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 95 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM___HPLADC___dataReady(uint16_t data)
#line 95
{
  uint16_t doneValue = data;
  uint8_t donePort;
  result_t Result;


  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      donePort = ADCM___ReqPort;

      if (((1 << donePort) & ADCM___ContReqMask) == 0) {
          ADCM___HPLADC___sampleStop();
          ADCM___ReqVector &= ~(1 << donePort);
        }

      if (ADCM___ReqVector) {
          do {
              ADCM___ReqPort++;
              ADCM___ReqPort = ADCM___ReqPort == TOSH_ADC_PORTMAPSIZE ? 0 : ADCM___ReqPort;
            }
          while (((
#line 113
          1 << ADCM___ReqPort) & ADCM___ReqVector) == 0);
          ADCM___HPLADC___samplePort(ADCM___ReqPort);
        }
    }
#line 116
    __nesc_atomic_end(__nesc_atomic); }


  {
  }
#line 119
  ;
  Result = ADCM___ADC___dataReady(donePort, doneValue);

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 122
    {
      if (Result == FAIL && ADCM___ContReqMask & (1 << donePort)) {
          ADCM___HPLADC___sampleStop();
          ADCM___ContReqMask &= ~(1 << donePort);
        }
    }
#line 127
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

static  
# 84 "SurgeM.nc"
void SurgeM___SendData(void)
#line 84
{
  SurgeMsg *pReading;
  uint16_t Len;

#line 87
  {
  }
#line 87
  ;

  if ((pReading = (SurgeMsg *)SurgeM___Send___getBuffer(&SurgeM___gMsgBuffer, &Len))) {
      pReading->type = SURGE_TYPE_SENSORREADING;
      pReading->parentaddr = SurgeM___RouteControl___getParent();
      pReading->reading = SurgeM___gSensorData;

      if (SurgeM___Send___send(&SurgeM___gMsgBuffer, sizeof(SurgeMsg )) != SUCCESS) {
        { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 95
          SurgeM___gfSendBusy = FALSE;
#line 95
          __nesc_atomic_end(__nesc_atomic); }
        }
    }
}

static  
# 150 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopEngineM.nc"
void *MultiHopEngineM___Send___getBuffer(uint8_t id, TOS_MsgPtr pMsg, uint16_t *length)
#line 150
{

  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)pMsg->data;

  *length = 29 - (size_t )& ((TOS_MHopMsg *)0)->data;

  return &pMHMsg->data[0];
}

static  
# 499 "C:/cygwin/opt/tinyos-1.x/tos/lib/Route/MultiHopLEPSM.nc"
result_t MultiHopLEPSM___RouteSelect___selectRoute(TOS_MsgPtr Msg, uint8_t id)
#line 499
{
  TOS_MHopMsg *pMHMsg = (TOS_MHopMsg *)&Msg->data[0];

  uint8_t iNbr;
  bool fIsDuplicate;
  result_t Result = SUCCESS;


  if (MultiHopLEPSM___gpCurrentParent == (void *)0) {


      if (pMHMsg->sourceaddr == TOS_LOCAL_ADDRESS && 
      pMHMsg->originaddr == TOS_LOCAL_ADDRESS) {
          pMHMsg->sourceaddr = TOS_LOCAL_ADDRESS;
          pMHMsg->hopcount = MultiHopLEPSM___gbCurrentHopCount;
          pMHMsg->seqno = MultiHopLEPSM___gCurrentSeqNo++;
          Msg->addr = TOS_BCAST_ADDR;
          return SUCCESS;
        }
      else {
          return FAIL;
        }
    }

  if (MultiHopLEPSM___gbCurrentHopCount >= pMHMsg->hopcount) {

      return FAIL;
    }

  if (pMHMsg->sourceaddr == TOS_LOCAL_ADDRESS && 
  pMHMsg->originaddr == TOS_LOCAL_ADDRESS) {
      fIsDuplicate = FALSE;
    }
  else {
      fIsDuplicate = MultiHopLEPSM___updateNbrCounters(pMHMsg->sourceaddr, pMHMsg->seqno, &iNbr);
    }

  if (!fIsDuplicate) {
      pMHMsg->sourceaddr = TOS_LOCAL_ADDRESS;
      pMHMsg->hopcount = MultiHopLEPSM___gbCurrentHopCount;
      if (MultiHopLEPSM___gpCurrentParent->id != TOS_UART_ADDR) {
          pMHMsg->seqno = MultiHopLEPSM___gCurrentSeqNo++;
        }
      Msg->addr = MultiHopLEPSM___gpCurrentParent->id;
    }
  else {
      Result = FAIL;
    }

  return Result;
}

static 
#line 288
bool MultiHopLEPSM___updateNbrCounters(uint16_t saddr, int16_t seqno, uint8_t *NbrIndex)
#line 288
{
  MultiHopLEPSM___TableEntry *pNbr;
  int16_t sDelta;
  uint8_t iNbr;
  bool Result = FALSE;

  iNbr = MultiHopLEPSM___findPreparedIndex(saddr);
  pNbr = &MultiHopLEPSM___NeighborTbl[iNbr];

  sDelta = seqno - MultiHopLEPSM___NeighborTbl[iNbr].lastSeqno - 1;

  if (pNbr->flags & MultiHopLEPSM___NBRFLAG_NEW) {
      pNbr->received++;
      pNbr->lastSeqno = seqno;
      pNbr->flags ^= MultiHopLEPSM___NBRFLAG_NEW;
    }
  else {
#line 304
    if (sDelta >= 0) {
        pNbr->missed += sDelta;
        pNbr->received++;
        pNbr->lastSeqno = seqno;
      }
    else {
#line 309
      if (sDelta < MultiHopLEPSM___ACCEPTABLE_MISSED) {

          MultiHopLEPSM___newEntry(iNbr, saddr);
          pNbr->received++;
          pNbr->lastSeqno = seqno;
          pNbr->flags ^= MultiHopLEPSM___NBRFLAG_NEW;
        }
      else {
          Result = TRUE;
        }
      }
    }
#line 320
  *NbrIndex = iNbr;
  return Result;
}

static 
#line 169
void MultiHopLEPSM___newEntry(uint8_t indes, uint16_t id)
#line 169
{
  MultiHopLEPSM___NeighborTbl[indes].id = id;
  MultiHopLEPSM___NeighborTbl[indes].flags = MultiHopLEPSM___NBRFLAG_VALID | MultiHopLEPSM___NBRFLAG_NEW;
  MultiHopLEPSM___NeighborTbl[indes].liveliness = 0;
  MultiHopLEPSM___NeighborTbl[indes].parent = MultiHopLEPSM___ROUTE_INVALID;
  MultiHopLEPSM___NeighborTbl[indes].hop = MultiHopLEPSM___ROUTE_INVALID;
  MultiHopLEPSM___NeighborTbl[indes].missed = 0;
  MultiHopLEPSM___NeighborTbl[indes].received = 0;
  MultiHopLEPSM___NeighborTbl[indes].receiveEst = 0;
  MultiHopLEPSM___NeighborTbl[indes].sendEst = 0;
}

# 242 "C:/cygwin/opt/tinyos-1.x/tos/system/AMPromiscuous.nc"
TOS_MsgPtr   prom_received(TOS_MsgPtr packet)
#line 242
{
  AMPromiscuous___counter++;
  {
  }
#line 244
  ;




  if (
#line 246
  packet->group == TOS_AM_GROUP && ((
  AMPromiscuous___promiscuous_mode == TRUE || 
  packet->addr == TOS_BCAST_ADDR) || 
  packet->addr == TOS_LOCAL_ADDRESS) && (
  AMPromiscuous___crc_check == FALSE || packet->crc == 1)) 
    {
      uint8_t type = packet->type;
      TOS_MsgPtr tmp;


      {
      }
#line 256
      ;
      AMPromiscuous___dbgPacket(packet);
      {
      }
#line 258
      ;


      tmp = AMPromiscuous___ReceiveMsg___receive(type, packet);
      if (tmp) {
        packet = tmp;
        }
    }
#line 265
  return packet;
}

static  
# 140 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___Timer___stop(uint8_t id)
#line 140
{

  if (id >= NUM_TIMERS) {
#line 142
    return FAIL;
    }
#line 143
  if (TimerM___mState & (0x1 << id)) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 144
        TimerM___mState &= ~(0x1 << id);
#line 144
        __nesc_atomic_end(__nesc_atomic); }
      if (!TimerM___mState) {
          TimerM___setIntervalFlag = 1;
        }
      return SUCCESS;
    }
  return FAIL;
}

# 94 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
void __attribute((signal)) __attribute((event))  __vector_9(void)
#line 94
{
  uint8_t bit = TOSH_READ_RFM_RXD_PIN();

#line 96
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {

      if (ChannelMonC___CM_state == ChannelMonC___IDLE_STATE) {
          ChannelMonC___CM_search[0] <<= 1;
          ChannelMonC___CM_search[0] = ChannelMonC___CM_search[0] | (bit & 0x1);
          if (ChannelMonC___CM_waiting != -1) {
              ChannelMonC___CM_waiting--;
              if (ChannelMonC___CM_waiting == 1) {
                  if ((ChannelMonC___CM_search[0] & 0xfff) == 0) {
                      ChannelMonC___CM_waiting = -1;
                      ChannelMonC___ChannelMon___idleDetect();
                    }
                  else 
#line 107
                    {
                      ChannelMonC___CM_waiting = (ChannelMonC___Random___rand() & 0x1f) + 30;
                    }
                }
            }
          if ((ChannelMonC___CM_search[0] & 0x777) == 0x707) {
              ChannelMonC___CM_state = ChannelMonC___START_SYMBOL_SEARCH;
              ChannelMonC___CM_search[0] = ChannelMonC___CM_search[1] = 0;
              ChannelMonC___CM_startSymBits = 30;
	  }
      }
      else {
#line 117
        if (ChannelMonC___CM_state == ChannelMonC___START_SYMBOL_SEARCH) {
            unsigned int current = ChannelMonC___CM_search[ChannelMonC___CM_lastBit];

#line 119
            ChannelMonC___CM_startSymBits--;
            if (ChannelMonC___CM_startSymBits == 0) {
                ChannelMonC___CM_state = ChannelMonC___IDLE_STATE;
              }
            if (ChannelMonC___CM_state != ChannelMonC___IDLE_STATE) {
                current <<= 1;
                current &= 0x1ff;
                if (bit) {
#line 126
                  current |= 0x1;
                  }
#line 127
                if (current == 0x135) {
                    * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
		    __BLAST_timer1_interrupt = 0;
                    ChannelMonC___CM_state = ChannelMonC___IDLE_STATE;
                    ChannelMonC___ChannelMon___startSymDetect();
                  }
                if (ChannelMonC___CM_state != ChannelMonC___IDLE_STATE) {
                    ChannelMonC___CM_search[ChannelMonC___CM_lastBit] = current;
                    ChannelMonC___CM_lastBit ^= 1;
                  }
              }
          }
        }
    }
#line 139
    __nesc_atomic_end(__nesc_atomic); }
#line 139
  return;
}

static   
# 80 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
result_t SecDedEncoding___Code___encode(char d)
#line 80
{
  uint8_t oldState;

#line 82
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 82
    {
      oldState = SecDedEncoding___state;
      if (SecDedEncoding___state == SecDedEncoding___IDLE_STATE) {
          SecDedEncoding___state = SecDedEncoding___ENCODING_BYTE;
          SecDedEncoding___data1 = d;
        }
    }
#line 88
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == SecDedEncoding___IDLE_STATE) {
      SecDedEncoding___radio_encode_thread();
      return 1;
    }
  else {
      return 0;
    }
}

static   
# 299 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___Code___encodeDone(char data1)
#line 299
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 300
    {
      MicaHighSpeedRadioM___encoded_buffer[(int )MicaHighSpeedRadioM___buf_end] = data1;
      MicaHighSpeedRadioM___buf_end++;
      MicaHighSpeedRadioM___buf_end &= 0x3;
      MicaHighSpeedRadioM___enc_count += 1;
    }
#line 305
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 71 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___send(uint8_t data)
#line 71
{
  result_t rval = FAIL;

#line 73
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
    {
      if (SpiByteFifoC___state == SpiByteFifoC___OPEN) {
          SpiByteFifoC___nextByte = data;
          SpiByteFifoC___state = SpiByteFifoC___FULL;
          rval = SUCCESS;
        }
      else {
#line 79
        if (SpiByteFifoC___state == SpiByteFifoC___IDLE) {
            SpiByteFifoC___state = SpiByteFifoC___OPEN;
            SpiByteFifoC___SpiByteFifo___dataReady(0);
            SpiByteFifoC___SlavePin___low();
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 7);
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 7;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0xc0;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = data;

            TOSH_CLR_RFM_CTL0_PIN();
            TOSH_SET_RFM_CTL1_PIN();

            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 6);
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
	    __BLAST_timer1_interrupt = 0;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = SpiByteFifoC___BIT_RATE;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x19;
            rval = SUCCESS;
          }
        }
    }
#line 99
    __nesc_atomic_end(__nesc_atomic); }
#line 99
  return rval;
}

static   
# 309 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioM.nc"
result_t MicaHighSpeedRadioM___SpiByteFifo___dataReady(uint8_t data)
#line 309
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 310
    {
      if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___TRANSMITTING_START) {
	/*BLAST recursion
          MicaHighSpeedRadioM___SpiByteFifo___send(TOSH_MHSR_start[(int )MicaHighSpeedRadioM___tx_count]);
	*/
          MicaHighSpeedRadioM___tx_count++;
          if (MicaHighSpeedRadioM___tx_count == sizeof TOSH_MHSR_start) {
              MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___TRANSMITTING;
              MicaHighSpeedRadioM___tx_count = 1;
            }
        }
      else {
#line 318
        if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___TRANSMITTING) {
	  /*BLAST recursion
            MicaHighSpeedRadioM___SpiByteFifo___send(MicaHighSpeedRadioM___encoded_buffer[(int )MicaHighSpeedRadioM___buf_head]);
	  */
            MicaHighSpeedRadioM___buf_head++;
            MicaHighSpeedRadioM___buf_head &= 0x3;
            MicaHighSpeedRadioM___enc_count--;


            if (MicaHighSpeedRadioM___enc_count >= 2) {
                ;
              }
            else {
#line 327
              if (MicaHighSpeedRadioM___tx_count < MSG_DATA_SIZE) {
                  char next_data = ((char *)MicaHighSpeedRadioM___send_ptr)[(int )MicaHighSpeedRadioM___tx_count];

#line 329
                  MicaHighSpeedRadioM___Code___encode(next_data);
                  MicaHighSpeedRadioM___tx_count++;
                  if (MicaHighSpeedRadioM___tx_count <= MicaHighSpeedRadioM___msg_length) {
                      MicaHighSpeedRadioM___calc_crc = crcByte(MicaHighSpeedRadioM___calc_crc, next_data);
                    }
                  if (MicaHighSpeedRadioM___tx_count == MicaHighSpeedRadioM___msg_length) {


                      MicaHighSpeedRadioM___tx_count = MSG_DATA_SIZE - 2;
                      MicaHighSpeedRadioM___send_ptr->crc = MicaHighSpeedRadioM___calc_crc;
                    }
                  MicaHighSpeedRadioM___RadioSendCoordinator___byte(MicaHighSpeedRadioM___send_ptr, (uint8_t )MicaHighSpeedRadioM___tx_count);
                }
              else {
#line 341
                if (MicaHighSpeedRadioM___buf_head != MicaHighSpeedRadioM___buf_end) {
                    MicaHighSpeedRadioM___Code___encode_flush();
                  }
                else 
#line 343
                  {
                    MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___SENDING_STRENGTH_PULSE;
                    MicaHighSpeedRadioM___tx_count = 0;
                  }
                }
              }
          }
        else {
#line 347
          if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___SENDING_STRENGTH_PULSE) {
              MicaHighSpeedRadioM___tx_count++;
              if (MicaHighSpeedRadioM___tx_count == 3) {
                  MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___WAITING_FOR_ACK;
                  MicaHighSpeedRadioM___SpiByteFifo___phaseShift();
                  MicaHighSpeedRadioM___tx_count = 1;
		  /* BLAST recursion
                  MicaHighSpeedRadioM___SpiByteFifo___send(0x00);
		  */
                }
              else {
		/*BLAST recursion
                  MicaHighSpeedRadioM___SpiByteFifo___send(0xff);
		*/
                }
            }
          else {
#line 359
            if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___WAITING_FOR_ACK) {
                data &= 0x7f;
		/*BLAST recusrion
                MicaHighSpeedRadioM___SpiByteFifo___send(0x00);
		*/
                if (MicaHighSpeedRadioM___tx_count == 1) {
                  MicaHighSpeedRadioM___SpiByteFifo___rxMode();
                  }
#line 364
                MicaHighSpeedRadioM___tx_count++;
                if (MicaHighSpeedRadioM___tx_count == MicaHighSpeedRadioM___ACK_CNT + 2) {
                    MicaHighSpeedRadioM___send_ptr->ack = data == 0x55;
                    MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___IDLE_STATE;

                    MicaHighSpeedRadioM___SpiByteFifo___idle();
                    TOS_post(MicaHighSpeedRadioM___packetSent);
                  }
              }
            else {
#line 372
              if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___RX_STATE) {
		/*BLAST recursion
                  MicaHighSpeedRadioM___Code___decode(data);
		*/
                }
              else {
#line 374
                if (MicaHighSpeedRadioM___state == MicaHighSpeedRadioM___ACK_SEND_STATE) {
                    MicaHighSpeedRadioM___ack_count++;
                    if (MicaHighSpeedRadioM___ack_count > MicaHighSpeedRadioM___ACK_CNT + 1) {
                        MicaHighSpeedRadioM___state = MicaHighSpeedRadioM___RX_DONE_STATE;
                        MicaHighSpeedRadioM___SpiByteFifo___idle();
		        __BLAST_packetReceived_enabled = 1;
                        TOS_post(MicaHighSpeedRadioM___packetReceived);
                      }
                    else 
#line 380
                      {
                        MicaHighSpeedRadioM___SpiByteFifo___txMode();
                      }
                  }
                }
              }
            }
          }
        }
    }
#line 389
    __nesc_atomic_end(__nesc_atomic); }
#line 385
  return 1;
}

static   
# 102 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC___SpiByteFifo___idle(void)
#line 102
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 103
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x00;
      SpiByteFifoC___nextByte = 0;
      SpiByteFifoC___SlavePin___high(FALSE);
      TOSH_MAKE_RFM_TXD_OUTPUT();
      TOSH_CLR_RFM_TXD_PIN();
      TOSH_CLR_RFM_CTL0_PIN();
      TOSH_CLR_RFM_CTL1_PIN();
      SpiByteFifoC___state = SpiByteFifoC___IDLE;
      SpiByteFifoC___nextByte = 0;
    }
#line 115
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 348 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___ByteComm___rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 348
{
    int random;

  switch (FramerM___gRxState) {

      case FramerM___RXSTATE_NOSYNC: 
	  random = FramerM___gRxHeadIndex;
        if (data == FramerM___HDLC_FLAG_BYTE && FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length == 0) {
            FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0;
            FramerM___gRxByteCnt = FramerM___gRxRunningCRC = 0;
            FramerM___gpRxBuf = (uint8_t *)FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].pMsg;
            FramerM___gRxState = FramerM___RXSTATE_PROTO;
          }
      break;

      case FramerM___RXSTATE_PROTO: 
        if (data == FramerM___HDLC_FLAG_BYTE) {
            break;
          }
      FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Proto = data;
      FramerM___gRxRunningCRC = crcByte(FramerM___gRxRunningCRC, data);
      switch (data) {
          case FramerM___PROTO_PACKET_ACK: 
            FramerM___gRxState = FramerM___RXSTATE_TOKEN;
          break;
          case FramerM___PROTO_PACKET_NOACK: 
            FramerM___gRxState = FramerM___RXSTATE_INFO;
          break;
          default: 
            FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
          break;
        }
      break;

      case FramerM___RXSTATE_TOKEN: 
        if (data == FramerM___HDLC_FLAG_BYTE) {
            FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
          }
        else {
#line 384
          if (data == FramerM___HDLC_CTLESC_BYTE) {
              FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0x20;
            }
          else {
              FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token ^= data;
              FramerM___gRxRunningCRC = crcByte(FramerM___gRxRunningCRC, FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token);
              FramerM___gRxState = FramerM___RXSTATE_INFO;
            }
          }
#line 392
      break;


      case FramerM___RXSTATE_INFO: 
        if (FramerM___gRxByteCnt > FramerM___HDLC_MTU) {
            FramerM___gRxByteCnt = FramerM___gRxRunningCRC = 0;
            FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length = 0;
            FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0;
            FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
          }
        else {
#line 402
          if (data == FramerM___HDLC_CTLESC_BYTE) {
              FramerM___gRxState = FramerM___RXSTATE_ESC;
            }
          else {
#line 405
            if (data == FramerM___HDLC_FLAG_BYTE) {
                if (FramerM___gRxByteCnt >= 2) {
                    uint16_t usRcvdCRC = FramerM___gpRxBuf[FramerM___gRxByteCnt - 1] & 0xff;

#line 408
                    usRcvdCRC = (usRcvdCRC << 8) | (FramerM___gpRxBuf[FramerM___gRxByteCnt - 2] & 0xff);
                    if (usRcvdCRC == FramerM___gRxRunningCRC) {
                        FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length = FramerM___gRxByteCnt - 2;
                        TOS_post(FramerM___PacketRcvd);
                        FramerM___gRxHeadIndex++;
#line 412
                        FramerM___gRxHeadIndex %= FramerM___HDLC_QUEUESIZE;
                      }
                    else {
                        FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length = 0;
                        FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0;
                      }
                    if (FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length == 0) {
                        FramerM___gpRxBuf = (uint8_t *)FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].pMsg;
                        FramerM___gRxState = FramerM___RXSTATE_PROTO;
                      }
                    else {
                        FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
                      }
                  }
                else {
                    FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length = 0;
                    FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0;
                    FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
                  }
                FramerM___gRxByteCnt = FramerM___gRxRunningCRC = 0;
              }
            else {
                FramerM___gpRxBuf[FramerM___gRxByteCnt] = data;
                if (FramerM___gRxByteCnt >= 2) {
                    FramerM___gRxRunningCRC = crcByte(FramerM___gRxRunningCRC, FramerM___gpRxBuf[FramerM___gRxByteCnt - 2]);
                  }
                FramerM___gRxByteCnt++;
              }
            }
          }
#line 440
      break;

      case FramerM___RXSTATE_ESC: 
        if (data == FramerM___HDLC_FLAG_BYTE) {

            FramerM___gRxByteCnt = FramerM___gRxRunningCRC = 0;
            FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Length = 0;
            FramerM___gMsgRcvTbl[FramerM___gRxHeadIndex].Token = 0;
            FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
          }
        else {
            data = data ^ 0x20;
            FramerM___gpRxBuf[FramerM___gRxByteCnt] = data;
            if (FramerM___gRxByteCnt >= 2) {
                FramerM___gRxRunningCRC = crcByte(FramerM___gRxRunningCRC, FramerM___gpRxBuf[FramerM___gRxByteCnt - 2]);
              }
            FramerM___gRxByteCnt++;
            FramerM___gRxState = FramerM___RXSTATE_INFO;
          }
      break;

      default: 
        FramerM___gRxState = FramerM___RXSTATE_NOSYNC;
      break;
    }

  return SUCCESS;
}

static   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM___HPLUART___putDone(void)
#line 87
{
  bool oldState;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 90
    {
      {
      }
#line 91
      ;
      oldState = UARTM___state;
      if (UARTM___state == TRUE){ //JHALA
	  UARTM___state = FALSE;
      }
    }
#line 94
    __nesc_atomic_end(__nesc_atomic); }








  if (oldState==TRUE) {//JHALA
      UARTM___ByteComm___txDone();
      UARTM___ByteComm___txByteReady(TRUE);
    }
  return SUCCESS;
}

static 
# 469 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM___TxArbitraryByte(uint8_t Byte)
#line 469
{
  if (Byte == FramerM___HDLC_FLAG_BYTE || Byte == FramerM___HDLC_CTLESC_BYTE) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 471
        {
          FramerM___gPrevTxState = FramerM___gTxState;
          FramerM___gTxState = FramerM___TXSTATE_ESC;
          FramerM___gTxEscByte = Byte;
        }
#line 475
        __nesc_atomic_end(__nesc_atomic); }
      Byte = FramerM___HDLC_CTLESC_BYTE;
    }

  return FramerM___ByteComm___txByte(Byte);
}

