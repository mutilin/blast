
int x;
int m __attribute__((lock)) ;
int state;

void lock() __attribute__((atomic)) {
int foo;
  {__blockattribute__(assume(m == 0))}
  m = 1;
}

void unlock(){
  m = 0;
}

int main() // __attribute__((atomic)) 
{

    int oldstate;
while (1){

    lock() ; //should have an "atomic" instead ...
    oldstate = state;
    if (state == 0){
	state = 1;
    }
    unlock ();
    
    if (oldstate == 0){
	x++;
	state = 0;
    }

}

}


