//#define dbg(mode, format, ...) ((void)0)
//#define dbg_clear(mode, format, ...) ((void)0)
//#define dbg_active(mode) 0
# 60 "/usr/local/avr/include/inttypes.h"
typedef signed char int8_t;




typedef unsigned char uint8_t;
# 83 "/usr/local/avr/include/inttypes.h" 3
typedef int int16_t;




typedef unsigned int uint16_t;


/**************************************/
/* BLAST */
int __BLAST_interrupt_enabled ;
int __BLAST_task_enabled;
int __BLAST_NONDET;
int __BLAST_vector_21_flag;

/**************************************/







typedef long int32_t;




typedef unsigned long uint32_t;
#line 117
typedef long long int64_t;




typedef unsigned long long uint64_t;
#line 134
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 213 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef unsigned int size_t;
#line 325
typedef int wchar_t;
# 60 "/usr/local/avr/include/stdlib.h"
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;


typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *, const void *);
# 151 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int ptrdiff_t;
# 85 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
typedef unsigned char bool;
enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};



enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};
static inline 

uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t  result_t;
static inline 






result_t rcombine(result_t r1, result_t r2);
#line 128
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 81 "/usr/local/avr/include/avr/pgmspace.h"
typedef void __attribute((__progmem__)) prog_void;
typedef char __attribute((__progmem__)) prog_char;
typedef unsigned char __attribute((__progmem__)) prog_uchar;
typedef int __attribute((__progmem__)) prog_int;
typedef long __attribute((__progmem__)) prog_long;
typedef long long __attribute((__progmem__)) prog_long_long;
# 124 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};
static inline 
void TOSH_wait(void);
static inline 




void TOSH_sleep(void);









typedef uint8_t __nesc_atomic_t;

__inline __nesc_atomic_t  __nesc_atomic_start(void );






__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg);
static 



__inline void __nesc_enable_interrupt(void);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RED_LED_PIN(void);
#line 57
static __inline void TOSH_CLR_RED_LED_PIN(void);
#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void);
static __inline void TOSH_SET_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_CLR_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void);
static __inline void TOSH_SET_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_CLR_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void);

static __inline void TOSH_SET_UD_PIN(void);
#line 61
static __inline void TOSH_CLR_UD_PIN(void);
static __inline void TOSH_SET_INC_PIN(void);
#line 62
static __inline void TOSH_CLR_INC_PIN(void);
static __inline void TOSH_SET_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void);
static __inline void TOSH_SET_POT_POWER_PIN(void);
#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void);
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void);
#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void);

static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void);




static __inline void TOSH_SET_INT1_PIN(void);
#line 72
static __inline void TOSH_MAKE_INT1_OUTPUT(void);
static __inline void TOSH_CLR_INT2_PIN(void);
#line 73
static __inline void TOSH_MAKE_INT2_INPUT(void);



static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void);
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void);
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void);

static __inline void TOSH_MAKE_PW0_OUTPUT(void);
static __inline void TOSH_MAKE_PW1_OUTPUT(void);
static __inline void TOSH_SET_PW2_PIN(void);
#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void);
static __inline void TOSH_MAKE_PW3_OUTPUT(void);
static __inline void TOSH_MAKE_PW4_OUTPUT(void);
static __inline void TOSH_MAKE_PW5_OUTPUT(void);
static __inline void TOSH_MAKE_PW6_OUTPUT(void);
static __inline void TOSH_MAKE_PW7_OUTPUT(void);
#line 101
static __inline void TOSH_SET_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void);
static inline 
void TOSH_SET_PIN_DIRECTIONS(void );
#line 146
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};


enum __nesc_unnamed4249 {

  TOSH_ACTUAL_VOLTAGE_PORT = 7
};
enum __nesc_unnamed4250 {

  TOS_ADC_VOLTAGE_PORT = 7
};
# 54 "C:/cygwin/opt/tinyos-1.x/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 




  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
typedef struct __nesc_unnamed4252 {
  void (*tp)(void);
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {
  TOSH_MAX_TASKS = 8, 
  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
volatile uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;
static inline 

void TOSH_wait(void );
static inline void TOSH_sleep(void );
static inline 
void TOSH_sched_init(void );
#line 98
bool  TOS_post(void (*tp)(void));
static inline 
#line 139
bool TOSH_run_next_task(void);
static inline 
#line 162
void TOSH_run_task(void);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.h"
enum __nesc_unnamed4254 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 1
};
# 35 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.h"
enum __nesc_unnamed4255 {
  TOS_ADCSample3750ns = 0, 
  TOS_ADCSample7500ns = 1, 
  TOS_ADCSample15us = 2, 
  TOS_ADCSample30us = 3, 
  TOS_ADCSample60us = 4, 
  TOS_ADCSample120us = 5, 
  TOS_ADCSample240us = 6, 
  TOS_ADCSample480us = 7
};
# 45 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
enum __nesc_unnamed4256 {
  TOSH_ACTUAL_PHOTO_PORT = 1, 
  TOSH_ACTUAL_TEMP_PORT = 1, 
  TOSH_ACTUAL_MIC_PORT = 2, 
  TOSH_ACTUAL_ACCEL_X_PORT = 3, 
  TOSH_ACTUAL_ACCEL_Y_PORT = 4, 
  TOSH_ACTUAL_MAG_X_PORT = 6, 
  TOSH_ACTUAL_MAG_Y_PORT = 5
};

enum __nesc_unnamed4257 {
  TOS_ADC_PHOTO_PORT = 1, 
  TOS_ADC_TEMP_PORT = 2, 
  TOS_ADC_MIC_PORT = 3, 
  TOS_ADC_ACCEL_X_PORT = 4, 
  TOS_ADC_ACCEL_Y_PORT = 5, 
  TOS_ADC_MAG_X_PORT = 6, 

  TOS_ADC_MAG_Y_PORT = 8
};

enum __nesc_unnamed4258 {
  TOS_MAG_POT_ADDR = 0, 
  TOS_MIC_POT_ADDR = 1
};

static __inline void TOSH_SET_PHOTO_CTL_PIN(void);
#line 71
static __inline void TOSH_MAKE_PHOTO_CTL_OUTPUT(void);
static __inline void TOSH_CLR_TEMP_CTL_PIN(void);
#line 72
static __inline void TOSH_MAKE_TEMP_CTL_INPUT(void);
# 32 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/Clock.h"
enum __nesc_unnamed4259 {
  TOS_I1000PS = 33, TOS_S1000PS = 1, 
  TOS_I100PS = 41, TOS_S100PS = 2, 
  TOS_I10PS = 102, TOS_S10PS = 3, 
  TOS_I4096PS = 1, TOS_S4096PS = 2, 
  TOS_I2048PS = 2, TOS_S2048PS = 2, 
  TOS_I1024PS = 1, TOS_S1024PS = 3, 
  TOS_I512PS = 2, TOS_S512PS = 3, 
  TOS_I256PS = 4, TOS_S256PS = 3, 
  TOS_I128PS = 8, TOS_S128PS = 3, 
  TOS_I64PS = 16, TOS_S64PS = 3, 
  TOS_I32PS = 32, TOS_S32PS = 3, 
  TOS_I16PS = 64, TOS_S16PS = 3, 
  TOS_I8PS = 128, TOS_S8PS = 3, 
  TOS_I4PS = 128, TOS_S4PS = 4, 
  TOS_I2PS = 128, TOS_S2PS = 5, 
  TOS_I1PS = 128, TOS_S1PS = 6, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4260 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 128
};
static  result_t PotM__Pot__init(uint8_t arg_0xa26e970);
static  result_t HPLPotC__Pot__finalise(void);
static  result_t HPLPotC__Pot__decrease(void);
static  result_t HPLPotC__Pot__increase(void);
static  result_t HPLInit__init(void);
static   result_t SenseM__ADC__dataReady(uint16_t arg_0xa2dd8c0);
static  result_t SenseM__StdControl__init(void);
static  result_t SenseM__StdControl__start(void);
static  result_t SenseM__Timer__fired(void);
static   result_t LedsC__Leds__yellowOff(void);
static   result_t LedsC__Leds__yellowOn(void);
static   result_t LedsC__Leds__init(void);
static   result_t LedsC__Leds__greenOff(void);
static   result_t LedsC__Leds__redOff(void);
static   result_t LedsC__Leds__redOn(void);
static   result_t LedsC__Leds__greenOn(void);
static   result_t TimerM__Clock__fire(void);
static  result_t TimerM__StdControl__init(void);
static  result_t TimerM__StdControl__start(void);
static  result_t TimerM__Timer__default__fired(uint8_t arg_0xa2ff3d0);
static  result_t TimerM__Timer__start(uint8_t arg_0xa2ff3d0, char arg_0xa299e10, uint32_t arg_0xa2ac010);
static   void HPLClock__Clock__setInterval(uint8_t arg_0xa31db60);
static   result_t HPLClock__Clock__setRate(char arg_0xa31d060, char arg_0xa31d1a0);
static   uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void);
static  result_t PhotoTempM__PhotoStdControl__init(void);
static   result_t PhotoTempM__InternalTempADC__dataReady(uint16_t arg_0xa2dd8c0);
static   result_t PhotoTempM__ExternalTempADC__default__dataReady(uint16_t arg_0xa2dd8c0);
static   result_t PhotoTempM__ExternalPhotoADC__getData(void);
static   result_t PhotoTempM__InternalPhotoADC__dataReady(uint16_t arg_0xa2dd8c0);
static   result_t ADCM__HPLADC__dataReady(uint16_t arg_0xa3d1868);
static  result_t ADCM__ADCControl__bindPort(uint8_t arg_0xa3a1ea8, uint8_t arg_0xa3ac010);
static  result_t ADCM__ADCControl__init(void);
static   result_t ADCM__ADC__getData(uint8_t arg_0xa3c29d8);
static   result_t ADCM__ADC__default__dataReady(uint8_t arg_0xa3c29d8, uint16_t arg_0xa2dd8c0);
static   result_t HPLADCC__ADC__bindPort(uint8_t arg_0xa3d0710, uint8_t arg_0xa3d0858);
static   result_t HPLADCC__ADC__sampleStop(void);
static   result_t HPLADCC__ADC__init(void);
static   result_t HPLADCC__ADC__samplePort(uint8_t arg_0xa3d0d40);
static  
# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
result_t RealMain__hardwareInit(void);
static  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
result_t RealMain__Pot__init(uint8_t arg_0xa26e970);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t RealMain__StdControl__init(void);
static  





result_t RealMain__StdControl__start(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void);
static  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
result_t PotM__HPLPot__finalise(void);
static  
#line 59
result_t PotM__HPLPot__decrease(void);
static  






result_t PotM__HPLPot__increase(void);
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
uint8_t PotM__potSetting;
static inline 
void PotM__setPot(uint8_t value);
static inline  
#line 106
result_t PotM__Pot__init(uint8_t initialSetting);
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__decrease(void);
static inline  







result_t HPLPotC__Pot__increase(void);
static inline  







result_t HPLPotC__Pot__finalise(void);
static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit__init(void);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t SenseM__ADCControl__init(void);
static   
# 122 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t SenseM__Leds__yellowOff(void);
static   
#line 114
result_t SenseM__Leds__yellowOn(void);
static   
#line 56
result_t SenseM__Leds__init(void);
static   
#line 97
result_t SenseM__Leds__greenOff(void);
static   
#line 72
result_t SenseM__Leds__redOff(void);
static   
#line 64
result_t SenseM__Leds__redOn(void);
static   
#line 89
result_t SenseM__Leds__greenOn(void);
static   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t SenseM__ADC__getData(void);
static  
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t SenseM__Timer__start(char arg_0xa299e10, uint32_t arg_0xa2ac010);
static 
# 70 "SenseM.nc"
result_t SenseM__display(uint16_t value);
static inline  
#line 86
result_t SenseM__StdControl__init(void);
static inline  






result_t SenseM__StdControl__start(void);
static inline  
#line 112
result_t SenseM__Timer__fired(void);
static inline   








result_t SenseM__ADC__dataReady(uint16_t data);
# 50 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
uint8_t LedsC__ledsOn;

enum LedsC____nesc_unnamed4261 {
  LedsC__RED_BIT = 1, 
  LedsC__GREEN_BIT = 2, 
  LedsC__YELLOW_BIT = 4
};
static inline   
result_t LedsC__Leds__init(void);
static inline   









result_t LedsC__Leds__redOn(void);
static inline   







result_t LedsC__Leds__redOff(void);
static inline   
#line 98
result_t LedsC__Leds__greenOn(void);
static inline   







result_t LedsC__Leds__greenOff(void);
static inline   
#line 127
result_t LedsC__Leds__yellowOn(void);
static inline   







result_t LedsC__Leds__yellowOff(void);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t TimerM__PowerManagement__adjustPower(void);
static   
# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
void TimerM__Clock__setInterval(uint8_t arg_0xa31db60);
static   
#line 96
result_t TimerM__Clock__setRate(char arg_0xa31d060, char arg_0xa31d1a0);
static  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t TimerM__Timer__fired(
# 45 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t arg_0xa2ff3d0);









uint32_t TimerM__mState;
uint8_t TimerM__setIntervalFlag;
uint8_t TimerM__mScale;
#line 57
uint8_t TimerM__mInterval;
int8_t TimerM__queue_head;
int8_t TimerM__queue_tail;
uint8_t TimerM__queue_size;
uint8_t TimerM__queue[NUM_TIMERS];

struct TimerM__timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM__mTimerList[NUM_TIMERS];

enum TimerM____nesc_unnamed4262 {
  TimerM__maxTimerInterval = 230
};
static inline  result_t TimerM__StdControl__init(void);
static inline  








result_t TimerM__StdControl__start(void);
static inline  









result_t TimerM__Timer__start(uint8_t id, char type, 
uint32_t interval);
#line 116
static void TimerM__adjustInterval(void);
static inline   
#line 154
result_t TimerM__Timer__default__fired(uint8_t id);
static inline 


void TimerM__enqueue(uint8_t value);
static inline 






uint8_t TimerM__dequeue(void);
static inline  








void TimerM__signalOneTimer(void);
static inline  




void TimerM__HandleFire(void);
static inline   
#line 204
result_t TimerM__Clock__fire(void);
static   
# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLClock__Clock__fire(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
uint8_t HPLClock__set_flag;
uint8_t HPLClock__mscale;
#line 55
uint8_t HPLClock__nextScale;
#line 55
uint8_t HPLClock__minterval;
static inline   
#line 87
void HPLClock__Clock__setInterval(uint8_t value);
static inline   
#line 149
result_t HPLClock__Clock__setRate(char interval, char scale);
#line 167
void __attribute((interrupt))   __vector_15(void);
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
bool HPLPowerManagementM__disabled = TRUE;

enum HPLPowerManagementM____nesc_unnamed4263 {
  HPLPowerManagementM__IDLE = 0, 
  HPLPowerManagementM__ADC_NR = 1 << 3, 
  HPLPowerManagementM__POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM__POWER_DOWN = 1 << 3
};
static inline 


uint8_t HPLPowerManagementM__getPowerLevel(void);
static inline  
#line 85
void HPLPowerManagementM__doAdjustment(void);
static   
#line 103
uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void);
static  
# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
result_t PhotoTempM__ADCControl__bindPort(uint8_t arg_0xa3a1ea8, uint8_t arg_0xa3ac010);
static  
#line 50
result_t PhotoTempM__ADCControl__init(void);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t PhotoTempM__ExternalTempADC__dataReady(uint16_t arg_0xa2dd8c0);
static   
#line 70
result_t PhotoTempM__ExternalPhotoADC__dataReady(uint16_t arg_0xa2dd8c0);
static   
#line 52
result_t PhotoTempM__InternalPhotoADC__getData(void);
# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
enum PhotoTempM____nesc_unnamed4264 {
  PhotoTempM__IDLE = 1, 
  PhotoTempM__BUSY = 2, 
  PhotoTempM__CONTINUOUS = 3
};
int PhotoTempM__state;
static inline  
result_t PhotoTempM__PhotoStdControl__init(void);
static inline   
#line 146
result_t PhotoTempM__ExternalPhotoADC__getData(void);
static inline   
#line 204
result_t PhotoTempM__InternalPhotoADC__dataReady(uint16_t data);
static inline    
#line 231
result_t PhotoTempM__ExternalTempADC__default__dataReady(uint16_t data);
static inline   


result_t PhotoTempM__InternalTempADC__dataReady(uint16_t data);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
result_t ADCM__HPLADC__bindPort(uint8_t arg_0xa3d0710, uint8_t arg_0xa3d0858);
static   
#line 91
result_t ADCM__HPLADC__sampleStop(void);
static   
#line 54
result_t ADCM__HPLADC__init(void);
static   
#line 77
result_t ADCM__HPLADC__samplePort(uint8_t arg_0xa3d0d40);
static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
result_t ADCM__ADC__dataReady(
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
uint8_t arg_0xa3c29d8, 
# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
uint16_t arg_0xa2dd8c0);
# 63 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
enum ADCM____nesc_unnamed4265 {
  ADCM__IDLE = 0, 
  ADCM__SINGLE_CONVERSION = 1, 
  ADCM__CONTINUOUS_CONVERSION = 2
};

uint16_t ADCM__ReqPort;
uint16_t ADCM__ReqVector;
uint16_t ADCM__ContReqMask;
static inline  
result_t ADCM__ADCControl__init(void);
static inline  
#line 87
result_t ADCM__ADCControl__bindPort(uint8_t port, uint8_t adcPort);
static inline    


result_t ADCM__ADC__default__dataReady(uint8_t port, uint16_t data);
static   


result_t ADCM__HPLADC__dataReady(uint16_t data);
static 
#line 132
__inline result_t ADCM__startGet(uint8_t newState, uint8_t port);
static inline   
#line 165
result_t ADCM__ADC__getData(uint8_t port);
static   
# 99 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
result_t HPLADCC__ADC__dataReady(uint16_t arg_0xa3d1868);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
bool HPLADCC__init_portmap_done;
enum HPLADCC____nesc_unnamed4266 {
  HPLADCC__TOSH_ADC_PORTMAPSIZE = 10
};

uint8_t HPLADCC__TOSH_adc_portmap[HPLADCC__TOSH_ADC_PORTMAPSIZE];
static 
void HPLADCC__init_portmap(void);
static inline   
#line 76
result_t HPLADCC__ADC__init(void);
static inline   
#line 96
result_t HPLADCC__ADC__bindPort(uint8_t port, uint8_t adcPort);
static   
#line 109
result_t HPLADCC__ADC__samplePort(uint8_t port);
static inline   
#line 125
result_t HPLADCC__ADC__sampleStop(void);





void __attribute((signal))   __vector_21(void);
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 5;
}

#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) &= ~(1 << 5);
}

#line 65
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 4;
}

#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 4;
}

#line 59
static __inline void TOSH_SET_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_SET_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_SET_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}








static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void)
#line 67
{
#line 67
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 64
static __inline void TOSH_SET_POT_POWER_PIN(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 7;
}











static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 7;
}

static __inline void TOSH_MAKE_PW0_OUTPUT(void)
#line 81
{
#line 81
  ;
}

#line 82
static __inline void TOSH_MAKE_PW1_OUTPUT(void)
#line 82
{
#line 82
  ;
}

#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void)
#line 83
{
#line 83
  ;
}

#line 84
static __inline void TOSH_MAKE_PW3_OUTPUT(void)
#line 84
{
#line 84
  ;
}

#line 85
static __inline void TOSH_MAKE_PW4_OUTPUT(void)
#line 85
{
#line 85
  ;
}

#line 86
static __inline void TOSH_MAKE_PW5_OUTPUT(void)
#line 86
{
#line 86
  ;
}

#line 87
static __inline void TOSH_MAKE_PW6_OUTPUT(void)
#line 87
{
#line 87
  ;
}

#line 88
static __inline void TOSH_MAKE_PW7_OUTPUT(void)
#line 88
{
#line 88
  ;
}

#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 7;
}

#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 5;
}

#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 2;
}

static inline 
#line 103
void TOSH_SET_PIN_DIRECTIONS(void )
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) = 0x02;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) = 0x02;
  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();
  TOSH_MAKE_POT_SELECT_OUTPUT();
  TOSH_MAKE_POT_POWER_OUTPUT();

  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();

  TOSH_MAKE_RFM_CTL0_OUTPUT();
  TOSH_MAKE_RFM_CTL1_OUTPUT();
  TOSH_MAKE_RFM_TXD_OUTPUT();
  TOSH_SET_POT_POWER_PIN();



  TOSH_MAKE_FLASH_SELECT_INPUT();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();


  TOSH_MAKE_BOOST_ENABLE_OUTPUT();
  TOSH_SET_BOOST_ENABLE_PIN();

  TOSH_MAKE_ONE_WIRE_INPUT();
  TOSH_SET_ONE_WIRE_PIN();
}

static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit__init(void)
#line 57
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
inline static  result_t RealMain__hardwareInit(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLInit__init();
#line 47

#line 47
  return result;
#line 47
}
#line 47
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}

#line 61
static __inline void TOSH_SET_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

static inline  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__finalise(void)
#line 74
{
  TOSH_SET_UD_PIN();
  TOSH_SET_INC_PIN();
  return SUCCESS;
}

# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__finalise(void){
#line 74
  unsigned char result;
#line 74

#line 74
  result = HPLPotC__Pot__finalise();
#line 74

#line 74
  return result;
#line 74
}
#line 74
# 63 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 5;
}

#line 62
static __inline void TOSH_CLR_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 5);
}

#line 61
static __inline void TOSH_CLR_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline  
# 65 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__increase(void)
#line 65
{
  TOSH_CLR_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__increase(void){
#line 67
  unsigned char result;
#line 67

#line 67
  result = HPLPotC__Pot__increase();
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__decrease(void)
#line 56
{
  TOSH_SET_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__decrease(void){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLPotC__Pot__decrease();
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline 
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
void PotM__setPot(uint8_t value)
#line 93
{
  uint8_t i;

#line 95
  for (i = 0; i < 151; i++) 
    PotM__HPLPot__decrease();

  for (i = 0; i < value; i++) 
    PotM__HPLPot__increase();

  PotM__HPLPot__finalise();

  PotM__potSetting = value;
}

static inline  result_t PotM__Pot__init(uint8_t initialSetting)
#line 106
{
  PotM__setPot(initialSetting);
  return SUCCESS;
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
inline static  result_t RealMain__Pot__init(uint8_t arg_0xa26e970){
#line 78
  unsigned char result;
#line 78

#line 78
  result = PotM__Pot__init(arg_0xa26e970);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 76 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
void TOSH_sched_init(void )
{
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
}

static inline 
# 108 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

static inline   
# 149 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
result_t HPLClock__Clock__setRate(char interval, char scale)
#line 149
{
  scale &= 0x7;
  scale |= 0x8;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 0);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 1);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) |= 1 << 3;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = scale;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 1;
    }
#line 162
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t TimerM__Clock__setRate(char arg_0xa31d060, char arg_0xa31d1a0){
#line 96
  unsigned char result;
#line 96

#line 96
  result = HPLClock__Clock__setRate(arg_0xa31d060, arg_0xa31d1a0);
#line 96

#line 96
  return result;
#line 96
}
#line 96
static inline  
# 72 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM__StdControl__init(void)
#line 72
{
  TimerM__mState = 0;
  TimerM__setIntervalFlag = 0;
  TimerM__queue_head = TimerM__queue_tail = -1;
  TimerM__queue_size = 0;
  TimerM__mScale = 3;
  TimerM__mInterval = TimerM__maxTimerInterval;
  return TimerM__Clock__setRate(TimerM__mInterval, TimerM__mScale);
}

static inline   
# 58 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__init(void)
#line 58
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 59
    {
      LedsC__ledsOn = 0;
      {
      }
#line 61
      ;
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__init(void){
#line 56
  unsigned char result;
#line 56

#line 56
  result = LedsC__Leds__init();
#line 56

#line 56
  return result;
#line 56
}
#line 56
static inline   
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC__ADC__init(void)
#line 76
{
  HPLADCC__init_portmap();

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) = 0x04;

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 6);
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 4;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 3;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 7);//this initial disabling is in main.

  return SUCCESS;
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM__HPLADC__init(void){
#line 54
  unsigned char result;
#line 54

#line 54
  result = HPLADCC__ADC__init();
#line 54

#line 54
  return result;
#line 54
}
#line 54
static inline  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM__ADCControl__init(void)
#line 73
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 74
    {
      ADCM__ReqPort = 0;
      ADCM__ReqVector = ADCM__ContReqMask = 0;
    }
#line 77
    __nesc_atomic_end(__nesc_atomic); }
  {
  }
#line 78
  ;

  return ADCM__HPLADC__init();
}

# 50 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
inline static  result_t PhotoTempM__ADCControl__init(void){
#line 50
  unsigned char result;
#line 50

#line 50
  result = ADCM__ADCControl__init();
#line 50

#line 50
  return result;
#line 50
}
#line 50
static inline   
# 96 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC__ADC__bindPort(uint8_t port, uint8_t adcPort)
#line 96
{
  if (port < HPLADCC__TOSH_ADC_PORTMAPSIZE) 
    {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 99
        {
          HPLADCC__init_portmap();
          HPLADCC__TOSH_adc_portmap[port] = adcPort;
        }
#line 102
        __nesc_atomic_end(__nesc_atomic); }
      return SUCCESS;
    }
  else {
    return FAIL;
    }
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM__HPLADC__bindPort(uint8_t arg_0xa3d0710, uint8_t arg_0xa3d0858){
#line 70
  unsigned char result;
#line 70

#line 70
  result = HPLADCC__ADC__bindPort(arg_0xa3d0710, arg_0xa3d0858);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 87 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM__ADCControl__bindPort(uint8_t port, uint8_t adcPort)
#line 87
{
  return ADCM__HPLADC__bindPort(port, adcPort);
}

# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADCControl.nc"
inline static  result_t PhotoTempM__ADCControl__bindPort(uint8_t arg_0xa3a1ea8, uint8_t arg_0xa3ac010){
#line 89
  unsigned char result;
#line 89

#line 89
  result = ADCM__ADCControl__bindPort(arg_0xa3a1ea8, arg_0xa3ac010);
#line 89

#line 89
  return result;
#line 89
}
#line 89
static inline  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM__PhotoStdControl__init(void)
#line 78
{
  PhotoTempM__ADCControl__bindPort(TOS_ADC_PHOTO_PORT, TOSH_ACTUAL_PHOTO_PORT);
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
    {
      PhotoTempM__state = PhotoTempM__IDLE;
    }
#line 82
    __nesc_atomic_end(__nesc_atomic); }
  {
  }
#line 83
  ;
  return PhotoTempM__ADCControl__init();
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SenseM__ADCControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = PhotoTempM__PhotoStdControl__init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 86 "SenseM.nc"
result_t SenseM__StdControl__init(void)
#line 86
{
  return rcombine(SenseM__ADCControl__init(), SenseM__Leds__init());
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain__StdControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = SenseM__StdControl__init();
#line 63
  result = rcombine(result, TimerM__StdControl__init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 82 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM__StdControl__start(void)
#line 82
{
  return SUCCESS;
}

# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t TimerM__PowerManagement__adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM__PowerManagement__adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void HPLClock__Clock__setInterval(uint8_t value)
#line 87
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = value;
}

# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerM__Clock__setInterval(uint8_t arg_0xa31db60){
#line 105
  HPLClock__Clock__setInterval(arg_0xa31db60);
#line 105
}
#line 105
static inline  
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM__Timer__start(uint8_t id, char type, 
uint32_t interval)
#line 94
{
  uint8_t diff;

#line 96
  if (id >= NUM_TIMERS) {
#line 96
    return FAIL;
    }
#line 97
  if (type > 1) {
#line 97
    return FAIL;
    }
#line 98
  TimerM__mTimerList[id].ticks = interval;
  TimerM__mTimerList[id].type = type;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
      interval += diff;
      TimerM__mTimerList[id].ticksLeft = interval;
      TimerM__mState |= 0x1 << id;
      if (interval < TimerM__mInterval) {
          TimerM__mInterval = interval;
          TimerM__Clock__setInterval(TimerM__mInterval);
          TimerM__setIntervalFlag = 0;
          TimerM__PowerManagement__adjustPower();
        }
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t SenseM__Timer__start(char arg_0xa299e10, uint32_t arg_0xa2ac010){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM__Timer__start(0, arg_0xa299e10, arg_0xa2ac010);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline  
# 94 "SenseM.nc"
result_t SenseM__StdControl__start(void)
#line 94
{
  return SenseM__Timer__start(TIMER_REPEAT, 500);
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain__StdControl__start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SenseM__StdControl__start();
#line 70
  result = rcombine(result, TimerM__StdControl__start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline 
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM__getPowerLevel(void)
#line 62
{
  uint8_t diff;

#line 64
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {

      return HPLPowerManagementM__IDLE;
    }
  else {
#line 67
    if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM__IDLE;
      }
    else {
#line 69
      if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) & ((1 << 6) | (1 << 7))) {
          return HPLPowerManagementM__IDLE;
        }
      else {
	  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) & (1 << 7)) {//JHALA this is a mere read
            return HPLPowerManagementM__ADC_NR;
          }
        else {
#line 75
          if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
              diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) - * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
              if (diff < 16) {
                return HPLPowerManagementM__IDLE;
                }
#line 79
              return HPLPowerManagementM__POWER_SAVE;
            }
          else 
#line 80
            {
              return HPLPowerManagementM__POWER_DOWN;
            }
          }
        }
      }
    }
}

static inline  
#line 85
void HPLPowerManagementM__doAdjustment(void)
#line 85
{
  uint8_t foo;
#line 86
  uint8_t mcu;

#line 87
  foo = HPLPowerManagementM__getPowerLevel();
  mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM__POWER_SAVE) {
      mcu |= HPLPowerManagementM__IDLE;
      while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
}

static inline 
# 135 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
void TOSH_wait(void)
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

static inline 
void TOSH_sleep(void)
{

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
   __asm volatile ("sleep");}

#line 160
__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) = oldSreg;
	__BLAST_interrupt_enabled = 1;
}

#line 153
__inline __nesc_atomic_t  __nesc_atomic_start(void ) __attribute__((atomic))
{
  __nesc_atomic_t result = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20);

	__BLAST_interrupt_enabled = 0;
#line 156
   __asm volatile ("cli");
  return result;
}

static inline 
# 139 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool TOSH_run_next_task(void)
#line 139
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  if (TOSH_sched_full == TOSH_sched_free) {

      return 0;
    }
  else {

      fInterruptFlags = __nesc_atomic_start();
      old_full = TOSH_sched_full;
      TOSH_sched_full++;
      TOSH_sched_full &= TOSH_TASK_BITMASK;
      func = TOSH_queue[(int )old_full].tp;
      TOSH_queue[(int )old_full].tp = 0;
      __nesc_atomic_end(fInterruptFlags);
      func();
      return 1;
    }
}

static inline void TOSH_run_task(void)
#line 162
{
  while (TOSH_run_next_task()) 
    ;
  TOSH_sleep();
  TOSH_wait();
}

# 116 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
static void TimerM__adjustInterval(void)
#line 116
{
  uint8_t i;
#line 117
  uint8_t val = TimerM__maxTimerInterval;

#line 118
  if (TimerM__mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM__mState & (0x1 << i) && TimerM__mTimerList[i].ticksLeft < val) {
              val = TimerM__mTimerList[i].ticksLeft;
            }
        }
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
        {
          TimerM__mInterval = val;
          TimerM__Clock__setInterval(TimerM__mInterval);
          TimerM__setIntervalFlag = 0;
        }
#line 128
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
        {
          TimerM__mInterval = TimerM__maxTimerInterval;
          TimerM__Clock__setInterval(TimerM__mInterval);
          TimerM__setIntervalFlag = 0;
        }
#line 135
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM__PowerManagement__adjustPower();
}

# 77 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM__HPLADC__samplePort(uint8_t arg_0xa3d0d40){
#line 77
  unsigned char result;
#line 77

#line 77
  result = HPLADCC__ADC__samplePort(arg_0xa3d0d40);
#line 77

#line 77
  return result;
#line 77
}
#line 77
static 
# 132 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
__inline result_t ADCM__startGet(uint8_t newState, uint8_t port)
#line 132
{
  uint16_t PortMask;
#line 133
  uint16_t oldReqVector;
  result_t Result = SUCCESS;

  if (port > TOSH_ADC_PORTMAPSIZE) {
      return FAIL;
    }

  PortMask = 1 << port;

  { // JHALA__blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 142
    {
      if ((PortMask & ADCM__ReqVector) != 0) {

          Result = FAIL;
        } 
      else {
          oldReqVector = ADCM__ReqVector;
          ADCM__ReqVector |= PortMask;
          if (__BLAST_NONDET /* newState == ADCM__CONTINUOUS_CONVERSION*) {
              ADCM__ContReqMask |= PortMask;
            }
          if (oldReqVector == 0) {

              ADCM__ReqPort = port;
              Result = ADCM__HPLADC__samplePort(port);
            }
        }
    }
#line 159
    /* __nesc_atomic_end(__nesc_atomic); */ }


  return Result;
}

static inline   result_t ADCM__ADC__getData(uint8_t port)
#line 165
{
  return ADCM__startGet(ADCM__SINGLE_CONVERSION, port);
}

# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM__InternalPhotoADC__getData(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = ADCM__ADC__getData(TOS_ADC_PHOTO_PORT);
#line 52

#line 52
  return result;
#line 52
}
#line 52
# 72 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_INT1_OUTPUT(void)
#line 72
{
#line 72
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 1;
}

# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_MAKE_PHOTO_CTL_OUTPUT(void)
#line 71
{
#line 71
  TOSH_MAKE_INT1_OUTPUT();
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INT1_PIN(void)
#line 72
{
#line 72
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 1;
}

# 71 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_SET_PHOTO_CTL_PIN(void)
#line 71
{
#line 71
  TOSH_SET_INT1_PIN();
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_INT2_INPUT(void)
#line 73
{
#line 73
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) &= ~(1 << 2);
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_MAKE_TEMP_CTL_INPUT(void)
#line 72
{
#line 72
  TOSH_MAKE_INT2_INPUT();
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_INT2_PIN(void)
#line 73
{
#line 73
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 2);
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/sensorboard.h"
static __inline void TOSH_CLR_TEMP_CTL_PIN(void)
#line 72
{
#line 72
  TOSH_CLR_INT2_PIN();
}

static inline   
# 146 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM__ExternalPhotoADC__getData(void)
#line 146
{
  uint8_t oldState;

#line 148
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 148
    {
      oldState = PhotoTempM__state;
      if (PhotoTempM__state == PhotoTempM__IDLE) {
          PhotoTempM__state = PhotoTempM__BUSY;
        }
    }
#line 153
    __nesc_atomic_end(__nesc_atomic); }

  if (oldState == PhotoTempM__IDLE) {
      TOSH_CLR_TEMP_CTL_PIN();
      TOSH_MAKE_TEMP_CTL_INPUT();
      TOSH_SET_PHOTO_CTL_PIN();
      TOSH_MAKE_PHOTO_CTL_OUTPUT();
      return PhotoTempM__InternalPhotoADC__getData();
    }
  return FAIL;
}

# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t SenseM__ADC__getData(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = PhotoTempM__ExternalPhotoADC__getData();
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline  
# 112 "SenseM.nc"
result_t SenseM__Timer__fired(void)
#line 112
{
  return SenseM__ADC__getData();
}

static inline   
# 154 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM__Timer__default__fired(uint8_t id)
#line 154
{
  return SUCCESS;
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t TimerM__Timer__fired(uint8_t arg_0xa2ff3d0){
#line 73
  unsigned char result;
#line 73

#line 73
  switch (arg_0xa2ff3d0) {
#line 73
    case 0:
#line 73
      result = SenseM__Timer__fired();
#line 73
      break;
#line 73
    default:
#line 73
      result = TimerM__Timer__default__fired(arg_0xa2ff3d0);
#line 73
    }
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 166 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t TimerM__dequeue(void)
#line 166
{
  if (TimerM__queue_size == 0) {
    return NUM_TIMERS;
    }
#line 169
  if (TimerM__queue_head == NUM_TIMERS - 1) {
    TimerM__queue_head = -1;
    }
#line 171
  TimerM__queue_head++;
  TimerM__queue_size--;
  return TimerM__queue[(uint8_t )TimerM__queue_head];
}

static inline  void TimerM__signalOneTimer(void)
#line 176
{
  uint8_t itimer = TimerM__dequeue();

#line 178
  if (itimer < NUM_TIMERS) {
    TimerM__Timer__fired(itimer);
    }
}

static inline 
#line 158
void TimerM__enqueue(uint8_t value)
#line 158
{
  if (TimerM__queue_tail == NUM_TIMERS - 1) {
    TimerM__queue_tail = -1;
    }
#line 161
  TimerM__queue_tail++;
  TimerM__queue_size++;
  TimerM__queue[(uint8_t )TimerM__queue_tail] = value;
}

static inline  
#line 182
void TimerM__HandleFire(void)
#line 182
{
  uint8_t i;

#line 184
  TimerM__setIntervalFlag = 1;
  if (TimerM__mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM__mState & (0x1 << i)) {
              TimerM__mTimerList[i].ticksLeft -= TimerM__mInterval + 1;
              if (TimerM__mTimerList[i].ticksLeft <= 2) {
                  if (TimerM__mTimerList[i].type == TIMER_REPEAT) {
                      TimerM__mTimerList[i].ticksLeft += TimerM__mTimerList[i].ticks;
                    }
                  else 
#line 192
                    {
                      TimerM__mState &= ~(0x1 << i);
                    }
                  TimerM__enqueue(i);
                  //JHALATOS_post(TimerM__signalOneTimer);
		  TimerM__signalOneTimer(); 

                }
            }
        }
    }
  TimerM__adjustInterval();
}

static inline   result_t TimerM__Clock__fire(void)
#line 204
{
    TimerM__HandleFire ();

    //JHALATOS_post(TimerM__HandleFire);
  
  return SUCCESS;
}

# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLClock__Clock__fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerM__Clock__fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
# 83 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_PW2_PIN(void)
#line 83
{
#line 83
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x15 + 0x20) |= 1 << 2;
}

# 99 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t HPLADCC__ADC__dataReady(uint16_t arg_0xa3d1868){
#line 99
  unsigned char result;
#line 99

#line 99
  result = ADCM__HPLADC__dataReady(arg_0xa3d1868);
#line 99

#line 99
  return result;
#line 99
}
#line 99
static 
# 165 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
__inline void __nesc_enable_interrupt(void)
#line 165
{
	__BLAST_interrupt_enabled = 1;
   __asm volatile ("sei");}

# 131 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
void __attribute((signal))   __vector_21(void)
#line 131
{
  uint16_t data = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x04 + 0x20);

  __nesc_enable_interrupt();
  HPLADCC__ADC__dataReady(data);
}

static inline   
#line 125
result_t HPLADCC__ADC__sampleStop(void)
#line 125
{
    {__blockattribute__((atomic))
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) &= ~(1 << 7);
    / *__BLAST_vector_21_flag = 1; */}//JHALA
  return SUCCESS;
}

# 91 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLADC.nc"
inline static   result_t ADCM__HPLADC__sampleStop(void){
#line 91
  unsigned char result;
#line 91

#line 91
  result = HPLADCC__ADC__sampleStop();
#line 91

#line 91
  return result;
#line 91
}
#line 91
static inline    
# 231 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM__ExternalTempADC__default__dataReady(uint16_t data)
#line 231
{
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM__ExternalTempADC__dataReady(uint16_t arg_0xa2dd8c0){
#line 70
  unsigned char result;
#line 70

#line 70
  result = PhotoTempM__ExternalTempADC__default__dataReady(arg_0xa2dd8c0);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline   
# 235 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM__InternalTempADC__dataReady(uint16_t data)
#line 235
{
  uint8_t oldState;

#line 237
  {__blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 237
    {
      oldState = PhotoTempM__state;
      if (PhotoTempM__state == PhotoTempM__BUSY) {
          PhotoTempM__state = PhotoTempM__IDLE;
        }
    }
#line 242
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == PhotoTempM__BUSY) {


      return PhotoTempM__ExternalTempADC__dataReady(data);
    }
  else {
#line 247
    if (oldState == PhotoTempM__CONTINUOUS) {
        int ret;

#line 249
        ret = PhotoTempM__ExternalTempADC__dataReady(data);
        if (ret == FAIL) {


            { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 253
              {
                PhotoTempM__state = PhotoTempM__IDLE;
              }
#line 255
              __nesc_atomic_end(__nesc_atomic); }
          }
        return ret;
      }
    }
#line 259
  return FAIL;
}

static inline   
# 122 "SenseM.nc"
result_t SenseM__ADC__dataReady(uint16_t data)
#line 122
{
  SenseM__display(7 - ((data >> 7) & 0x7));
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t PhotoTempM__ExternalPhotoADC__dataReady(uint16_t arg_0xa2dd8c0){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SenseM__ADC__dataReady(arg_0xa2dd8c0);
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline   
# 204 "C:/cygwin/opt/tinyos-1.x/tos/sensorboards/micasb/PhotoTempM.nc"
result_t PhotoTempM__InternalPhotoADC__dataReady(uint16_t data)
#line 204
{
  uint8_t oldState;

#line 206
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 206
    {
      oldState = PhotoTempM__state;
      if (PhotoTempM__state == PhotoTempM__BUSY) {
          PhotoTempM__state = PhotoTempM__IDLE;
        }
    }
#line 211
    __nesc_atomic_end(__nesc_atomic); }

  if (oldState == PhotoTempM__BUSY) {


      return PhotoTempM__ExternalPhotoADC__dataReady(data);
    }
  else {
#line 216
    if (oldState == PhotoTempM__CONTINUOUS) {
        int ret;

#line 218
        ret = PhotoTempM__ExternalPhotoADC__dataReady(data);
        if (ret == FAIL) {


            { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 222
              {
                PhotoTempM__state = PhotoTempM__IDLE;
              }
#line 224
              __nesc_atomic_end(__nesc_atomic); }
          }
        return ret;
      }
    }
#line 228
  return FAIL;
}

static inline    
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM__ADC__default__dataReady(uint8_t port, uint16_t data)
#line 91
{
  return FAIL;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ADC.nc"
inline static   result_t ADCM__ADC__dataReady(uint8_t arg_0xa3c29d8, uint16_t arg_0xa2dd8c0){
#line 70
  unsigned char result;
#line 70

#line 70
  switch (arg_0xa3c29d8) {
#line 70
    case TOS_ADC_PHOTO_PORT:
#line 70
      result = PhotoTempM__InternalPhotoADC__dataReady(arg_0xa2dd8c0);
#line 70
      break;
#line 70
    case TOS_ADC_TEMP_PORT:
#line 70
      result = PhotoTempM__InternalTempADC__dataReady(arg_0xa2dd8c0);
#line 70
      break;
#line 70
    default:
#line 70
      result = ADCM__ADC__default__dataReady(arg_0xa3c29d8, arg_0xa2dd8c0);
#line 70
    }
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 58 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 0);
}

static inline   
# 127 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__yellowOn(void)
#line 127
{
  {
  }
#line 128
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 129
    {
      TOSH_CLR_YELLOW_LED_PIN();
      LedsC__ledsOn |= LedsC__YELLOW_BIT;
    }
#line 132
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 114 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__yellowOn(void){
#line 114
  unsigned char result;
#line 114

#line 114
  result = LedsC__Leds__yellowOn();
#line 114

#line 114
  return result;
#line 114
}
#line 114
static inline   
# 136 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__yellowOff(void)
#line 136
{
  {
  }
#line 137
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 138
    {
      TOSH_SET_YELLOW_LED_PIN();
      LedsC__ledsOn &= ~LedsC__YELLOW_BIT;
    }
#line 141
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 122 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__yellowOff(void){
#line 122
  unsigned char result;
#line 122

#line 122
  result = LedsC__Leds__yellowOff();
#line 122

#line 122
  return result;
#line 122
}
#line 122
# 59 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline   
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__greenOn(void)
#line 98
{
  {
  }
#line 99
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 100
    {
      TOSH_CLR_GREEN_LED_PIN();
      LedsC__ledsOn |= LedsC__GREEN_BIT;
    }
#line 103
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__greenOn(void){
#line 89
  unsigned char result;
#line 89

#line 89
  result = LedsC__Leds__greenOn();
#line 89

#line 89
  return result;
#line 89
}
#line 89
static inline   
# 107 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__greenOff(void)
#line 107
{
  {
  }
#line 108
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 109
    {
      TOSH_SET_GREEN_LED_PIN();
      LedsC__ledsOn &= ~LedsC__GREEN_BIT;
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 97 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__greenOff(void){
#line 97
  unsigned char result;
#line 97

#line 97
  result = LedsC__Leds__greenOff();
#line 97

#line 97
  return result;
#line 97
}
#line 97
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

static inline   
# 69 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__redOn(void)
#line 69
{
  {
  }
#line 70
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 71
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC__ledsOn |= LedsC__RED_BIT;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 64 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__redOn(void){
#line 64
  unsigned char result;
#line 64

#line 64
  result = LedsC__Leds__redOn();
#line 64

#line 64
  return result;
#line 64
}
#line 64
static inline   
# 78 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__redOff(void)
#line 78
{
  {
  }
#line 79
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
    {
      TOSH_SET_RED_LED_PIN();
      LedsC__ledsOn &= ~LedsC__RED_BIT;
    }
#line 83
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 72 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SenseM__Leds__redOff(void){
#line 72
  unsigned char result;
#line 72

#line 72
  result = LedsC__Leds__redOff();
#line 72

#line 72
  return result;
#line 72
}
#line 72
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool  TOS_post(void (*tp)(void))
#line 98
{
/*BLAST stub 
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;
  TOSH_sched_free++;
  TOSH_sched_free &= TOSH_TASK_BITMASK;

  if (TOSH_sched_free != TOSH_sched_full) {
      __nesc_atomic_end(fInterruptFlags);

      TOSH_queue[tmp].tp = tp;
      return TRUE;
    }
  else {
      TOSH_sched_free = tmp;
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
*/
}


void blast_run_task(){

    {__blockattribute__((atomic))
	 if (__BLAST_task_enabled == 0) {
	     __BLAST_task_enabled = 1;
	 } 
	 else {
	 STUCK: goto STUCK; // BLAST-- a "deadlock" type thing ...
	 }
    }
    // can only get here if no other task is running ...
    switch(__BLAST_NONDET){
	//case 1: TimerM__signalOneTimer(); break;
	//case 2: TimerM__HandleFire(); break;
	//case 3: HPLPowerManagementM__doAdjustment();break;
    default: break;
    }
    __BLAST_task_enabled = 0; //let another task run if they like
    
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void)
#line 54
{

/*
  RealMain__hardwareInit();
  RealMain__Pot__init(10);
  TOSH_sched_init();

  RealMain__StdControl__init();
  RealMain__StdControl__start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
*/

    if (__BLAST_vector_21_flag == 1) { //init assumption JHALA
	while(1) {
	    switch(__BLAST_NONDET){
	    case 15: if(__BLAST_interrupt_enabled) { 
		__BLAST_task_enabled = 1;
		__vector_15(); 
		__BLAST_task_enabled = 0;
	    }
		break;
	    case 21: 
		if(__BLAST_interrupt_enabled) {
		    __BLAST_task_enabled = 1;
		    {__blockattribute__((atomic))
			 if (__BLAST_vector_21_flag == 0) 
			     __BLAST_vector_21_flag = 1; 
		    //dont let another occurence of this happen
			 else {
			     STUCKK : goto STUCKK;
			 }
		    }
		    __vector_21(); 
		    
		    __BLAST_task_enabled = 0;
		}
	    break; 
	    case 99: blast_run_task ();
	    default: break;
	    }
	}
    }
}

static 
# 64 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
void HPLADCC__init_portmap(void)
#line 64
{

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 66
    {
      if (HPLADCC__init_portmap_done == FALSE) {
          int i;

#line 69
          for (i = 0; i < HPLADCC__TOSH_ADC_PORTMAPSIZE; i++) 
            HPLADCC__TOSH_adc_portmap[i] = i;
          HPLADCC__init_portmap_done = TRUE;
        }
    }
#line 73
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 103 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void)
#line 103
{
  uint8_t mcu;

#line 105
  if (!HPLPowerManagementM__disabled) {
    TOS_post(HPLPowerManagementM__doAdjustment);
    }
  else 
#line 107
    {
      mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM__IDLE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void __attribute((interrupt))   __vector_15(void)
#line 167
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 168
    {
      if (HPLClock__set_flag) {
          HPLClock__mscale = HPLClock__nextScale;
          HPLClock__nextScale |= 0x8;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLClock__nextScale;

          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLClock__minterval;
          HPLClock__set_flag = 0;
        }
    }
#line 177
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock__Clock__fire();
}

static   
# 109 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLADCC.nc"
result_t HPLADCC__ADC__samplePort(uint8_t port)
#line 109
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 110
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x07 + 0x20) = HPLADCC__TOSH_adc_portmap[port];
      if (HPLADCC__TOSH_adc_portmap[port] == 8 && port == 8) {
        TOSH_SET_PW2_PIN();
        }
#line 114
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 7; 
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) |= 1 << 6;

      __BLAST_vector_21_flag = 0;// JHALA enable the interrupt
    }
#line 116
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 95 "C:/cygwin/opt/tinyos-1.x/tos/system/ADCM.nc"
result_t ADCM__HPLADC__dataReady(uint16_t data)
#line 95
{
  uint16_t doneValue = data;
  uint8_t donePort;
  result_t Result;


  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      donePort = ADCM__ReqPort;

      if (((1 << donePort) & ADCM__ContReqMask) == 0) {
          ADCM__HPLADC__sampleStop();
          ADCM__ReqVector &= ~(1 << donePort);
        }

      if (ADCM__ReqVector) {
          do {
              ADCM__ReqPort++;
              ADCM__ReqPort = ADCM__ReqPort == TOSH_ADC_PORTMAPSIZE ? 0 : ADCM__ReqPort;
            }
          while (((
#line 113
          1 << ADCM__ReqPort) & ADCM__ReqVector) == 0);
          ADCM__HPLADC__samplePort(ADCM__ReqPort);
        }
    }
#line 116
    __nesc_atomic_end(__nesc_atomic);  }


  {
  }
#line 119
  ;
  Result = ADCM__ADC__dataReady(donePort, doneValue);

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 122
    {
      if (Result == FAIL && ADCM__ContReqMask & (1 << donePort)) {
          ADCM__HPLADC__sampleStop();
          ADCM__ContReqMask &= ~(1 << donePort);
        }
    }
#line 127
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

static 
# 70 "SenseM.nc"
result_t SenseM__display(uint16_t value)
{
  if (value & 1) {
#line 72
    SenseM__Leds__yellowOn();
    }
  else {
#line 73
    SenseM__Leds__yellowOff();
    }
#line 74
  if (value & 2) {
#line 74
    SenseM__Leds__greenOn();
    }
  else {
#line 75
    SenseM__Leds__greenOff();
    }
#line 76
  if (value & 4) {
#line 76
    SenseM__Leds__redOn();
    }
  else {
#line 77
    SenseM__Leds__redOff();
    }
#line 78
  return SUCCESS;
}

