
int x;

int state;
int i_enabled,task_posted;
int __BLAST_NONDET;

int foo () {
    return 0;
}

void task() __attribute__((task)){
    {__blockattribute__((assume(task_posted == 1)))}    
    task_posted = 0;
    foo();
    x ++ ;
    foo ();
    i_enabled = 1;
    foo();
}

int start_sym_detect(){
    
    x++;
    foo();
    task_posted = 1;
}

int int_handler() __attribute__((signal)){
    {__blockattribute__((assume(i_enabled == 1)))}
    foo ();
    i_enabled = 0;
    foo();
    start_sym_detect();
}

int main() 
{
    {__blockattribute__((assume(task_posted == 0)))}
    while (1){
	if (__BLAST_NONDET){
	    int_handler();
	}
	else 
	    task();
    }
}


