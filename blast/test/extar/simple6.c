
int x;

int m __attribute__((lock));

int lock()  __attribute__((atomic)) {
  if (m!=0) return 0;
  m = 1;
  return 1;
}

void unlock() {
  m = 0;
}

int main() // __attribute__((atomic)) 
{
	int __BLAST_NONDET;
        int local;
  
  do { local = lock(); } while (local==0); 
  x = 0;
  unlock();
}


