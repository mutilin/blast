
int x;
int m __attribute__((lock)) ;

void lock() __attribute__((atomic)) {

  {__blockattribute__(assume(m == 0))}
  m = 1;
}

void unlock(){
  m = 0;
}

int main() // __attribute__((atomic)) 
{
while (1) {
  lock() ;
  x = 0;
  unlock ();
}
}


