
int x;
int m __attribute__((lock)) ;
int state;

void lock() __attribute__((atomic)) {
int foo;
  {__blockattribute__(assume(m == 0))}
  m = 1;
}

void unlock() __attribute__((atomic)) {

  m = 0;
}

int stuff() __attribute__((atomic)){
    int oldstate;

	oldstate = state ;
	if (state == 0){
		state = 1;
	}
	return oldstate;
}

int main() // __attribute__((atomic)) 
{

    int oldstate;

while (1){
    oldstate = stuff();
    
    if (oldstate == 0){
	x++;
	state = 0;
    }
}

}


