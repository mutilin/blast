//#define dbg(mode, format, ...) ((void)0)
//#define dbg_clear(mode, format, ...) ((void)0)
//#define dbg_active(mode) 0
# 60 "/usr/local/avr/include/inttypes.h"
typedef signed char int8_t;




typedef unsigned char uint8_t;
# 83 "/usr/local/avr/include/inttypes.h" 3
typedef int int16_t;




typedef unsigned int uint16_t;


/*****************************************/
int __BLAST_task_enabled;
int __BLAST_interrupt_enabled;
int __BLAST_NONDET ;

/*****************************************/







typedef long int32_t;




typedef unsigned long uint32_t;
#line 117
typedef long long int64_t;




typedef unsigned long long uint64_t;
#line 134
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 213 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef unsigned int size_t;
#line 325
typedef int wchar_t;
# 60 "/usr/local/avr/include/stdlib.h"
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;


typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *, const void *);
# 151 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int ptrdiff_t;
# 85 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
typedef unsigned char bool;
enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};

uint16_t TOS_LOCAL_ADDRESS = 1;

enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};
static inline 

uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t  result_t;
static inline 






result_t rcombine(result_t r1, result_t r2);
static inline 






result_t rcombine3(result_t r1, result_t r2, result_t r3);
#line 128
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 81 "/usr/local/avr/include/avr/pgmspace.h"
typedef void __attribute((__progmem__)) prog_void;
typedef char __attribute((__progmem__)) prog_char;
typedef unsigned char __attribute((__progmem__)) prog_uchar;
typedef int __attribute((__progmem__)) prog_int;
typedef long __attribute((__progmem__)) prog_long;
typedef long long __attribute((__progmem__)) prog_long_long;
# 124 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};
static inline 
void TOSH_wait(void);
static inline 




void TOSH_sleep(void);









typedef uint8_t __nesc_atomic_t;

__inline __nesc_atomic_t  __nesc_atomic_start(void );






__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg);
static 



__inline void __nesc_enable_interrupt(void);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RED_LED_PIN(void);
#line 57
static __inline void TOSH_CLR_RED_LED_PIN(void);
#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void);
static __inline void TOSH_SET_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_CLR_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void);
static __inline void TOSH_SET_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_CLR_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void);

static __inline void TOSH_SET_UD_PIN(void);
#line 61
static __inline void TOSH_CLR_UD_PIN(void);
static __inline void TOSH_SET_INC_PIN(void);
#line 62
static __inline void TOSH_CLR_INC_PIN(void);
static __inline void TOSH_SET_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void);
static __inline void TOSH_SET_POT_POWER_PIN(void);
#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void);
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void);
#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void);

static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void);








static __inline int TOSH_READ_RFM_RXD_PIN(void);
static __inline void TOSH_CLR_RFM_TXD_PIN(void);
#line 77
static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void);
#line 77
static __inline void TOSH_MAKE_RFM_TXD_INPUT(void);
static __inline void TOSH_SET_RFM_CTL0_PIN(void);
#line 78
static __inline void TOSH_CLR_RFM_CTL0_PIN(void);
#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void);
static __inline void TOSH_SET_RFM_CTL1_PIN(void);
#line 79
static __inline void TOSH_CLR_RFM_CTL1_PIN(void);
#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void);

static __inline void TOSH_MAKE_PW0_OUTPUT(void);
static __inline void TOSH_MAKE_PW1_OUTPUT(void);
static __inline void TOSH_MAKE_PW2_OUTPUT(void);
static __inline void TOSH_MAKE_PW3_OUTPUT(void);
static __inline void TOSH_MAKE_PW4_OUTPUT(void);
static __inline void TOSH_MAKE_PW5_OUTPUT(void);
static __inline void TOSH_MAKE_PW6_OUTPUT(void);
static __inline void TOSH_MAKE_PW7_OUTPUT(void);









static __inline void TOSH_SET_UART_RXD0_PIN(void);


static __inline void TOSH_SET_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_CLR_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_OUTPUT(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void);
static inline 
void TOSH_SET_PIN_DIRECTIONS(void );
#line 146
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};


enum __nesc_unnamed4249 {

  TOSH_ACTUAL_VOLTAGE_PORT = 7
};
enum __nesc_unnamed4250 {

  TOS_ADC_VOLTAGE_PORT = 7
};
# 54 "C:/cygwin/opt/tinyos-1.x/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 




  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
typedef struct __nesc_unnamed4252 {
  void (*tp)(void);
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {
  TOSH_MAX_TASKS = 8, 
  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
volatile uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;
static inline 

void TOSH_wait(void );
static inline void TOSH_sleep(void );
static inline 
void TOSH_sched_init(void );
#line 98
bool  TOS_post(void (*tp)(void));
static inline 
#line 139
bool TOSH_run_next_task(void);
static inline 
#line 162
void TOSH_run_task(void);
static 
# 137 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
void *nmemcpy(void *to, const void *from, size_t n);
static inline 








void *nmemset(void *to, int val, size_t n);
# 49 "C:/cygwin/opt/tinyos-1.x/tos/types/AM.h"
enum __nesc_unnamed4254 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};





enum __nesc_unnamed4255 {
  TOS_DEFAULT_AM_GROUP = 0x7d
};

uint8_t TOS_AM_GROUP = TOS_DEFAULT_AM_GROUP;
#line 84
typedef struct TOS_Msg {


  uint16_t addr;
  uint8_t type;
  uint8_t group;
  uint8_t length;
  int8_t data[29];
  uint16_t crc;







  uint16_t strength;
  uint8_t ack;
  uint16_t time;
  uint8_t sendSecurityMode;
  uint8_t receiveSecurityMode;
} TOS_Msg;

typedef struct TOS_Msg_TinySecCompat {


  uint16_t addr;
  uint8_t type;

  uint8_t length;
  uint8_t group;
  int8_t data[29];
  uint16_t crc;







  uint16_t strength;
  uint8_t ack;
  uint16_t time;
  uint8_t sendSecurityMode;
  uint8_t receiveSecurityMode;
} TOS_Msg_TinySecCompat;

typedef struct TinySec_Msg {

  uint16_t addr;
  uint8_t type;
  uint8_t length;

  uint8_t iv[4];

  uint8_t enc[29];

  uint8_t mac[4];


  uint8_t calc_mac[4];
  uint8_t ack_byte;
  bool cryptoDone;
  bool receiveDone;

  bool validMAC;
} __attribute((packed))  TinySec_Msg;



enum __nesc_unnamed4256 {
  MSG_DATA_SIZE = (size_t )& ((struct TOS_Msg *)0)->crc + sizeof(uint16_t ), 
  TINYSEC_MSG_DATA_SIZE = (size_t )& ((struct TinySec_Msg *)0)->mac + 4, 
  DATA_LENGTH = 29, 
  LENGTH_BYTE_NUMBER = (size_t )& ((struct TOS_Msg *)0)->length + 1, 
  TINYSEC_NODE_ID_SIZE = sizeof(uint16_t )
};

enum __nesc_unnamed4257 {
  TINYSEC_AUTH_ONLY = 1, 
  TINYSEC_ENCRYPT_AND_AUTH = 2, 
  TINYSEC_DISABLED = 3, 
  TINYSEC_ENABLED_BIT = 128, 
  TINYSEC_ENCRYPT_ENABLED_BIT = 64
} __attribute((packed)) ;


typedef TOS_Msg *TOS_MsgPtr;
# 48 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/crypto.h"
typedef struct CipherContext {


  uint8_t context[128];
} CipherContext;






typedef struct MACContext {
  CipherContext cc;
  uint8_t context[12];
} MACContext;




typedef struct CipherModeContext {
  CipherContext cc;
  uint8_t context[24];
} CipherModeContext;
# 32 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TimeSyncMsg.h"
enum __nesc_unnamed4258 {
#line 32
  AM_TIMESYNCMSG = 37
};

struct TimeSyncMsg {
  uint16_t source_addr;
  uint32_t timeH;
  uint32_t timeL;
  uint8_t level;
  uint8_t phase;
};




enum __nesc_unnamed4259 {
  MASTER = 1, 
  SLAVE_SYNCED = 2, 
  SLAVE_UNSYNCED = 0
};
# 33 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TosTime.h"
typedef struct __nesc_unnamed4260 {
  uint32_t high32;
  uint32_t low32;
} tos_time_t;
# 31 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t __attribute((__progmem__)) crcTable[256] = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };
static inline 

uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.h"
enum __nesc_unnamed4261 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 1
};
# 32 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/Clock.h"
enum __nesc_unnamed4262 {
  TOS_I1000PS = 33, TOS_S1000PS = 1, 
  TOS_I100PS = 41, TOS_S100PS = 2, 
  TOS_I10PS = 102, TOS_S10PS = 3, 
  TOS_I4096PS = 1, TOS_S4096PS = 2, 
  TOS_I2048PS = 2, TOS_S2048PS = 2, 
  TOS_I1024PS = 1, TOS_S1024PS = 3, 
  TOS_I512PS = 2, TOS_S512PS = 3, 
  TOS_I256PS = 4, TOS_S256PS = 3, 
  TOS_I128PS = 8, TOS_S128PS = 3, 
  TOS_I64PS = 16, TOS_S64PS = 3, 
  TOS_I32PS = 32, TOS_S32PS = 3, 
  TOS_I16PS = 64, TOS_S16PS = 3, 
  TOS_I8PS = 128, TOS_S8PS = 3, 
  TOS_I4PS = 128, TOS_S4PS = 4, 
  TOS_I2PS = 128, TOS_S2PS = 5, 
  TOS_I1PS = 128, TOS_S1PS = 6, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4263 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 128
};
# 32 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/AbsoluteTimer.h"
enum __nesc_unnamed4264 {

  MAX_NUM_TIMERS = 0
};
static  result_t PotM__Pot__init(uint8_t arg_0xa272f00);
static  result_t HPLPotC__Pot__finalise(void);
static  result_t HPLPotC__Pot__decrease(void);
static  result_t HPLPotC__Pot__increase(void);
static  result_t HPLInit__init(void);
static  result_t SecureTOSBaseM__UARTSend__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static  TOS_MsgPtr SecureTOSBaseM__UARTTokenReceive__receive(TOS_MsgPtr arg_0xa2c85d0, uint8_t arg_0xa2c8718);
static  TOS_MsgPtr SecureTOSBaseM__RadioReceive__receive(TOS_MsgPtr arg_0xa2a2f98);
static  result_t SecureTOSBaseM__RadioSend__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static  result_t SecureTOSBaseM__StdControl__init(void);
static  result_t SecureTOSBaseM__StdControl__start(void);
static  TOS_MsgPtr SecureTOSBaseM__UARTReceive__receive(TOS_MsgPtr arg_0xa2a2f98);
static   result_t SlavePinM__SlavePin__low(void);
static   result_t SlavePinM__SlavePin__high(bool arg_0xa3040a8);
static   result_t HPLSlavePinC__SlavePin__low(void);
static   result_t HPLSlavePinC__SlavePin__high(void);
static   result_t SecDedEncoding__Code__encode_flush(void);
static   result_t SecDedEncoding__Code__decode(char arg_0xa320ea8);
static   result_t SecDedEncoding__Code__encode(char arg_0xa320a98);
static   uint16_t RandomLFSR__Random__rand(void);
static   result_t RandomLFSR__Random__init(void);
static   result_t ChannelMonC__ChannelMon__macDelay(void);
static   result_t ChannelMonC__ChannelMon__startSymbolSearch(void);
static   result_t ChannelMonC__ChannelMon__init(void);
static   uint16_t RadioTimingC__RadioTiming__currentTime(void);
static   uint16_t RadioTimingC__RadioTiming__getTiming(void);
static  result_t SpiByteFifoC__SlavePin__notifyHigh(void);
static   result_t SpiByteFifoC__SpiByteFifo__send(uint8_t arg_0xa38eb28);
static   result_t SpiByteFifoC__SpiByteFifo__phaseShift(void);
static   result_t SpiByteFifoC__SpiByteFifo__startReadBytes(uint16_t arg_0xa38f218);
static   result_t SpiByteFifoC__SpiByteFifo__idle(void);
static   result_t SpiByteFifoC__SpiByteFifo__txMode(void);
static   result_t SpiByteFifoC__SpiByteFifo__rxMode(void);
static  result_t TinySecM__TinySecControl__init(uint8_t arg_0xa3c7c28, uint8_t *arg_0xa3c7d90, uint8_t *arg_0xa3c7ef0);
static   uint16_t TinySecM__TinySec__sendInit(TOS_Msg_TinySecCompat *arg_0xa3c9800);
static   result_t TinySecM__TinySec__send(void);
static   result_t TinySecM__TinySec__receiveInit(TOS_Msg_TinySecCompat *arg_0xa3c8188);
static   result_t TinySecM__TinySecRadio__byteReceived(uint8_t arg_0xa3d4178);
static   uint8_t TinySecM__TinySecRadio__getTransmitByte(void);
static   uint8_t SkipJackM__BlockCipherInfo__getPreferredBlockSize(void);
static   result_t SkipJackM__BlockCipher__decrypt(CipherContext *arg_0xa4570b8, uint8_t *arg_0xa457230, uint8_t *arg_0xa457398);
static  result_t SkipJackM__BlockCipher__init(CipherContext *arg_0xa3dbc88, uint8_t arg_0xa3dbde8, uint8_t arg_0xa3dbf30, uint8_t *arg_0xa4560b8);
static   result_t SkipJackM__BlockCipher__encrypt(CipherContext *arg_0xa4567a8, uint8_t *arg_0xa456920, uint8_t *arg_0xa456a88);
static   result_t CBCModeM__BlockCipherMode__incrementalDecrypt(CipherModeContext *arg_0xa3e38c8, uint8_t *arg_0xa3e3a40, uint8_t *arg_0xa3e3bb8, uint16_t arg_0xa3e3d18, uint16_t *arg_0xa3e3e90);
static  result_t CBCModeM__BlockCipherMode__init(CipherModeContext *arg_0xa3e4590, uint8_t arg_0xa3e46e8, uint8_t *arg_0xa3e4848);
static   result_t CBCModeM__BlockCipherMode__encrypt(CipherModeContext *arg_0xa3e4fe0, uint8_t *arg_0xa3e5158, uint8_t *arg_0xa3e52c0, uint16_t arg_0xa3e5428, uint8_t *arg_0xa3e5588);
static   result_t CBCModeM__BlockCipherMode__initIncrementalDecrypt(CipherModeContext *arg_0xa3e2d70, uint8_t *arg_0xa3e2ee0, uint16_t arg_0xa3e3040);
static   result_t CBCMAC__MAC__MAC(MACContext *arg_0xa3f4060, uint8_t *arg_0xa3f41c0, uint16_t arg_0xa3f4310, uint8_t *arg_0xa3f4480, uint8_t arg_0xa3f45c8);
static   result_t CBCMAC__MAC__incrementalMAC(MACContext *arg_0xa3f6b20, uint8_t *arg_0xa3f6c80, uint16_t arg_0xa3f6de0);
static  result_t CBCMAC__MAC__init(MACContext *arg_0xa3e18e0, uint8_t arg_0xa3e1a28, uint8_t *arg_0xa3e1b88);
static   result_t CBCMAC__MAC__initIncrementalMAC(MACContext *arg_0xa3f6330, uint16_t arg_0xa3f6480);
static   result_t CBCMAC__MAC__getIncrementalMAC(MACContext *arg_0xa3f7560, uint8_t *arg_0xa3f76c0, uint8_t arg_0xa3f7818);
static   result_t LedsC__Leds__yellowOff(void);
static   result_t LedsC__Leds__yellowOn(void);
static   result_t LedsC__Leds__init(void);
static   result_t LedsC__Leds__greenOff(void);
static   result_t LedsC__Leds__redOff(void);
static   result_t LedsC__Leds__greenToggle(void);
static   result_t LedsC__Leds__yellowToggle(void);
static   result_t LedsC__Leds__redToggle(void);
static   result_t LedsC__Leds__redOn(void);
static   result_t LedsC__Leds__greenOn(void);
static   result_t MicaHighSpeedRadioTinySecM__TinySec__receiveInitDone(result_t arg_0xa3c87f8, uint16_t arg_0xa3c8950, bool arg_0xa3c8aa0);
static   result_t MicaHighSpeedRadioTinySecM__TinySec__receiveDone(result_t arg_0xa3c9190);
static   result_t MicaHighSpeedRadioTinySecM__TinySec__sendDone(result_t arg_0xa3c6560);
static  result_t MicaHighSpeedRadioTinySecM__Send__send(TOS_MsgPtr arg_0xa2c3448);
static   result_t MicaHighSpeedRadioTinySecM__Code__decodeDone(char arg_0xa3212c0, char arg_0xa321400);
static   result_t MicaHighSpeedRadioTinySecM__Code__encodeDone(char arg_0xa321820);
static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__dataReady(uint8_t arg_0xa38fe98);
static   void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8);
static   void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__startSymbol(void);
static   result_t MicaHighSpeedRadioTinySecM__ChannelMon__idleDetect(void);
static   result_t MicaHighSpeedRadioTinySecM__ChannelMon__startSymDetect(void);
static  result_t MicaHighSpeedRadioTinySecM__Control__init(void);
static  result_t MicaHighSpeedRadioTinySecM__Control__start(void);
static   void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8);
static   void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__startSymbol(void);
static  result_t RadioPacketTinySecM__Send__send(TOS_MsgPtr arg_0xa2c3448);
static  TOS_MsgPtr RadioPacketTinySecM__RadioReceive__receive(TOS_MsgPtr arg_0xa2a2f98);
static  result_t RadioPacketTinySecM__RadioSend__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static   result_t TimerM__Clock__fire(void);
static  result_t TimerM__Timer__default__fired(uint8_t arg_0xa584490);
static   void HPLClock__Clock__setInterval(uint8_t arg_0xa57ebe8);
static   uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void);
static  result_t SimpleTimeM__AbsoluteTimer__default__fired(uint8_t arg_0xa5d5330);
static   uint16_t SimpleTimeM__Time__getUs(void);
static   tos_time_t SimpleTimeM__Time__get(void);
static  result_t SimpleTimeM__Timer__fired(void);
static   tos_time_t TimeUtilC__TimeUtil__addUint32(tos_time_t arg_0xa561520, uint32_t arg_0xa561670);
static   char TimeUtilC__TimeUtil__compare(tos_time_t arg_0xa55e2f8, tos_time_t arg_0xa55e448);
static   result_t FramerM__ByteComm__txDone(void);
static   result_t FramerM__ByteComm__txByteReady(bool arg_0xa615dd0);
static   result_t FramerM__ByteComm__rxByteReady(uint8_t arg_0xa615600, bool arg_0xa615748, uint16_t arg_0xa6158a0);
static  result_t FramerM__BareSendMsg__send(TOS_MsgPtr arg_0xa2c3448);
static  result_t FramerM__StdControl__init(void);
static  result_t FramerM__StdControl__start(void);
static  result_t FramerM__TokenReceiveMsg__ReflectToken(uint8_t arg_0xa2c8d30);
static   result_t UARTM__HPLUART__get(uint8_t arg_0xa6511b8);
static   result_t UARTM__HPLUART__putDone(void);
static   result_t UARTM__ByteComm__txByte(uint8_t arg_0xa615170);
static  result_t UARTM__Control__init(void);
static  result_t UARTM__Control__start(void);
static   result_t HPLUARTM__UART__init(void);
static   result_t HPLUARTM__UART__put(uint8_t arg_0xa650cb8);
static  result_t SecurityM__Control__init(void);
static  result_t SecurityM__Control__start(void);
static  
# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
result_t RealMain__hardwareInit(void);
static  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
result_t RealMain__Pot__init(uint8_t arg_0xa272f00);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t RealMain__StdControl__init(void);
static  





result_t RealMain__StdControl__start(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void);
static  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
result_t PotM__HPLPot__finalise(void);
static  
#line 59
result_t PotM__HPLPot__decrease(void);
static  






result_t PotM__HPLPot__increase(void);
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
uint8_t PotM__potSetting;
static inline 
void PotM__setPot(uint8_t value);
static inline  
#line 106
result_t PotM__Pot__init(uint8_t initialSetting);
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__decrease(void);
static inline  







result_t HPLPotC__Pot__increase(void);
static inline  







result_t HPLPotC__Pot__finalise(void);
static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit__init(void);
static  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t SecureTOSBaseM__UARTSend__send(TOS_MsgPtr arg_0xa2c3448);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t SecureTOSBaseM__RadioControl__init(void);
static  





result_t SecureTOSBaseM__RadioControl__start(void);
static  
# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
result_t SecureTOSBaseM__UARTTokenReceive__ReflectToken(uint8_t arg_0xa2c8d30);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t SecureTOSBaseM__UARTControl__init(void);
static  





result_t SecureTOSBaseM__UARTControl__start(void);
static   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t SecureTOSBaseM__Leds__init(void);
static   
#line 106
result_t SecureTOSBaseM__Leds__greenToggle(void);
static   
#line 131
result_t SecureTOSBaseM__Leds__yellowToggle(void);
static   
#line 81
result_t SecureTOSBaseM__Leds__redToggle(void);
static  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t SecureTOSBaseM__RadioSend__send(TOS_MsgPtr arg_0xa2c3448);
# 69 "SecureTOSBaseM.nc"
enum SecureTOSBaseM____nesc_unnamed4265 {
  SecureTOSBaseM__QUEUE_SIZE = 5
};

enum SecureTOSBaseM____nesc_unnamed4266 {
  SecureTOSBaseM__TXFLAG_BUSY = 0x1, 
  SecureTOSBaseM__TXFLAG_TOKEN = 0x2
};


TOS_Msg SecureTOSBaseM__gRxBufPool[SecureTOSBaseM__QUEUE_SIZE];
TOS_MsgPtr SecureTOSBaseM__gRxBufPoolTbl[SecureTOSBaseM__QUEUE_SIZE];
uint8_t SecureTOSBaseM__gRxHeadIndex;
#line 81
uint8_t SecureTOSBaseM__gRxTailIndex;

TOS_Msg SecureTOSBaseM__gTxBuf;
TOS_MsgPtr SecureTOSBaseM__gpTxMsg;
uint8_t SecureTOSBaseM__gTxPendingToken;
uint8_t SecureTOSBaseM__gfTxFlags;
static inline  
void SecureTOSBaseM__RadioRcvdTask(void);
static  
#line 106
void SecureTOSBaseM__UARTRcvdTask(void);
static inline  
#line 122
void SecureTOSBaseM__SendAckTask(void);
static inline  







result_t SecureTOSBaseM__StdControl__init(void);
static inline  
#line 155
result_t SecureTOSBaseM__StdControl__start(void);
static inline  
#line 173
TOS_MsgPtr SecureTOSBaseM__RadioReceive__receive(TOS_MsgPtr Msg);
static inline  
#line 211
TOS_MsgPtr SecureTOSBaseM__UARTReceive__receive(TOS_MsgPtr Msg);
static inline  
#line 238
TOS_MsgPtr SecureTOSBaseM__UARTTokenReceive__receive(TOS_MsgPtr Msg, uint8_t Token);
static inline  
#line 266
result_t SecureTOSBaseM__UARTSend__sendDone(TOS_MsgPtr Msg, result_t success);
static inline  



result_t SecureTOSBaseM__RadioSend__sendDone(TOS_MsgPtr Msg, result_t success);
static  
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
result_t SlavePinM__SlavePin__notifyHigh(void);
static   
# 47 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
result_t SlavePinM__HPLSlavePin__low(void);
static   result_t SlavePinM__HPLSlavePin__high(void);
# 90 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
int8_t SlavePinM__n;
bool SlavePinM__signalHigh;
static 
#line 110
__inline   result_t SlavePinM__SlavePin__low(void);
static inline  







void SlavePinM__signalHighTask(void);
static 





__inline   result_t SlavePinM__SlavePin__high(bool needEvent);
static inline   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC__SlavePin__high(void);
static inline   




result_t HPLSlavePinC__SlavePin__low(void);
static   
# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
result_t SecDedEncoding__Code__decodeDone(char arg_0xa3212c0, char arg_0xa321400);
static   result_t SecDedEncoding__Code__encodeDone(char arg_0xa321820);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
enum SecDedEncoding____nesc_unnamed4267 {
  SecDedEncoding__IDLE_STATE = 0, 
  SecDedEncoding__DECODING_BYTE_3 = 1, 
  SecDedEncoding__DECODING_BYTE_2 = 2, 
  SecDedEncoding__DECODING_BYTE_1 = 3, 
  SecDedEncoding__ENCODING_BYTE = 4
};

char SecDedEncoding__data1;
char SecDedEncoding__data2;
char SecDedEncoding__data3;
char SecDedEncoding__state;
static inline 
void SecDedEncoding__radio_decode_thread(void);
static inline void SecDedEncoding__radio_encode_thread(void);
static inline   
result_t SecDedEncoding__Code__decode(char d1);
static inline   
#line 76
result_t SecDedEncoding__Code__encode_flush(void);
static   


result_t SecDedEncoding__Code__encode(char d);
static inline 
#line 99
void SecDedEncoding__radio_encode_thread(void);
static inline 
#line 167
void SecDedEncoding__radio_decode_thread(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR__shiftReg;
uint16_t RandomLFSR__initSeed;
uint16_t RandomLFSR__mask;
static inline   

result_t RandomLFSR__Random__init(void);
static   









uint16_t RandomLFSR__Random__rand(void);
static   
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
uint16_t ChannelMonC__Random__rand(void);
static   
# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
result_t ChannelMonC__ChannelMon__idleDetect(void);
static   
#line 38
result_t ChannelMonC__ChannelMon__startSymDetect(void);
# 42 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
enum ChannelMonC____nesc_unnamed4268 {
  ChannelMonC__IDLE_STATE, 
  ChannelMonC__START_SYMBOL_SEARCH, 
  ChannelMonC__PACKET_START, 
  ChannelMonC__DISABLED_STATE
};

enum ChannelMonC____nesc_unnamed4269 {
  ChannelMonC__SAMPLE_RATE = 100 / 2 * 4
};

unsigned short ChannelMonC__CM_search[2];
char ChannelMonC__CM_state;
unsigned char ChannelMonC__CM_lastBit;
unsigned char ChannelMonC__CM_startSymBits;
short ChannelMonC__CM_waiting;
static inline   
result_t ChannelMonC__ChannelMon__init(void);
static   





result_t ChannelMonC__ChannelMon__startSymbolSearch(void);
#line 94
void __attribute((signal))   __vector_9(void);
static inline   
#line 151
result_t ChannelMonC__ChannelMon__macDelay(void);
static inline   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC__RadioTiming__getTiming(void);
static inline   
#line 55
uint16_t RadioTimingC__RadioTiming__currentTime(void);
static   
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
result_t SpiByteFifoC__SlavePin__low(void);
static   








result_t SpiByteFifoC__SlavePin__high(bool arg_0xa3040a8);
static   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
result_t SpiByteFifoC__SpiByteFifo__dataReady(uint8_t arg_0xa38fe98);
# 41 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
uint8_t SpiByteFifoC__nextByte;
uint8_t SpiByteFifoC__state;

enum SpiByteFifoC____nesc_unnamed4270 {
  SpiByteFifoC__IDLE, 
  SpiByteFifoC__FULL, 
  SpiByteFifoC__OPEN, 
  SpiByteFifoC__READING
};

enum SpiByteFifoC____nesc_unnamed4271 {
  SpiByteFifoC__BIT_RATE = 20 * 4 / 2 * 5 / 4
};


void __attribute((signal))   __vector_17(void);
static   
#line 71
result_t SpiByteFifoC__SpiByteFifo__send(uint8_t data);
static   
#line 102
result_t SpiByteFifoC__SpiByteFifo__idle(void);
static inline   
#line 119
result_t SpiByteFifoC__SpiByteFifo__startReadBytes(uint16_t timing);
static inline   
#line 164
result_t SpiByteFifoC__SpiByteFifo__txMode(void);
static inline   






result_t SpiByteFifoC__SpiByteFifo__rxMode(void);
static inline   
#line 188
result_t SpiByteFifoC__SpiByteFifo__phaseShift(void);
static inline  








result_t SpiByteFifoC__SlavePin__notifyHigh(void);
static   
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
result_t TinySecM__TinySec__receiveInitDone(result_t arg_0xa3c87f8, uint16_t arg_0xa3c8950, bool arg_0xa3c8aa0);
static   
#line 96
result_t TinySecM__TinySec__receiveDone(result_t arg_0xa3c9190);
static   
#line 150
result_t TinySecM__TinySec__sendDone(result_t arg_0xa3c6560);
static   
# 127 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
result_t TinySecM__MAC__MAC(MACContext *arg_0xa3f4060, uint8_t *arg_0xa3f41c0, uint16_t arg_0xa3f4310, 
uint8_t *arg_0xa3f4480, uint8_t arg_0xa3f45c8);
static   
#line 89
result_t TinySecM__MAC__incrementalMAC(MACContext *arg_0xa3f6b20, uint8_t *arg_0xa3f6c80, 
uint16_t arg_0xa3f6de0);
static  
#line 60
result_t TinySecM__MAC__init(MACContext *arg_0xa3e18e0, uint8_t arg_0xa3e1a28, uint8_t *arg_0xa3e1b88);
static   
#line 73
result_t TinySecM__MAC__initIncrementalMAC(MACContext *arg_0xa3f6330, uint16_t arg_0xa3f6480);
static   
#line 109
result_t TinySecM__MAC__getIncrementalMAC(MACContext *arg_0xa3f7560, uint8_t *arg_0xa3f76c0, 
uint8_t arg_0xa3f7818);
static   
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
uint16_t TinySecM__Random__rand(void);
static   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
uint8_t TinySecM__BlockCipherInfo__getPreferredBlockSize(void);
static   
# 165 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherMode.nc"
result_t TinySecM__BlockCipherMode__incrementalDecrypt(CipherModeContext *arg_0xa3e38c8, 
uint8_t *arg_0xa3e3a40, 
uint8_t *arg_0xa3e3bb8, 
uint16_t arg_0xa3e3d18, 
uint16_t *arg_0xa3e3e90);
static  
#line 65
result_t TinySecM__BlockCipherMode__init(CipherModeContext *arg_0xa3e4590, 
uint8_t arg_0xa3e46e8, uint8_t *arg_0xa3e4848);
static   
#line 88
result_t TinySecM__BlockCipherMode__encrypt(CipherModeContext *arg_0xa3e4fe0, 
uint8_t *arg_0xa3e5158, uint8_t *arg_0xa3e52c0, 
uint16_t arg_0xa3e5428, uint8_t *arg_0xa3e5588);
static   
#line 138
result_t TinySecM__BlockCipherMode__initIncrementalDecrypt(CipherModeContext *arg_0xa3e2d70, 
uint8_t *arg_0xa3e2ee0, 
uint16_t arg_0xa3e3040);
# 59 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
bool TinySecM__initialized;

enum TinySecM____nesc_unnamed4272 {


  TinySecM__TINYSECM_MAX_BLOCK_SIZE = 16, 
  TinySecM__COMPUTE_MAC_IDLE, 
  TinySecM__COMPUTE_MAC_INITIALIZED, 

  TinySecM__COMPUTE_MAC_BUSY, 
  TinySecM__DECRYPT_IDLE, 
  TinySecM__DECRYPT_INITIALIZED, 

  TinySecM__DECRYPT_BUSY
};

int16_t TinySecM__txlength = 0;
int16_t TinySecM__rxlength = 0;

bool TinySecM__txencrypt = TRUE;
bool TinySecM__rxdecrypt = TRUE;

int16_t TinySecM__TxByteCnt = 0;
int16_t TinySecM__RxByteCnt = 0;

int16_t TinySecM__recDataLength = 0;
int16_t TinySecM__sendDataLength = 0;

uint8_t TinySecM__compute_mac_state;
uint8_t TinySecM__decrypt_state;
uint8_t TinySecM__blockSize;


struct TinySecM__computeMACBuffer {
  bool computeMACWaiting;
  bool computeMACInitWaiting;
  uint8_t position;
  uint8_t amount;
  bool finishMACWaiting;
} TinySecM__computeMACBuffer;


struct TinySecM__decryptBuffer {
  bool decryptWaiting;
  bool decryptInitWaiting;
  uint8_t position;
  uint8_t amount;
} TinySecM__decryptBuffer;

CipherModeContext TinySecM__cipherModeContext;
MACContext TinySecM__macContext;

uint8_t TinySecM__iv[TinySecM__TINYSECM_MAX_BLOCK_SIZE];


TinySec_Msg TinySecM__tinysec_rec_buffer;
TinySec_Msg TinySecM__tinysec_send_buffer;

TinySec_Msg *TinySecM__ciphertext_send_ptr;
TinySec_Msg *TinySecM__ciphertext_rec_ptr;

TOS_Msg_TinySecCompat *TinySecM__cleartext_send_ptr;
TOS_Msg_TinySecCompat *TinySecM__cleartext_rec_ptr;

bool TinySecM__test_state = FALSE;
static inline  
#line 138
result_t TinySecM__TinySecControl__init(uint8_t keySize, 
uint8_t *encryptionKey, 
uint8_t *MACKey);
static 
#line 195
result_t TinySecM__decryptIncrementalInit(void);
static inline result_t TinySecM__decryptIncremental(uint8_t incr_decrypt_start, uint8_t amount);
static inline result_t TinySecM__MACincrementalInit(void);
static inline result_t TinySecM__computeMACIncremental(uint8_t incr_mac_start, uint8_t amount);
static inline result_t TinySecM__computeMACIncrementalFinish(void);
static result_t TinySecM__verifyMAC(void);
static inline result_t TinySecM__computeMAC(void);
static inline result_t TinySecM__encrypt(void);
static inline result_t TinySecM__noEncrypt(void);
static inline 


bool TinySecM__interruptDisable(void);
static inline 




result_t TinySecM__interruptEnable(void);
static inline 



result_t TinySecM__postIncrementalMACInit(void);
static 



result_t TinySecM__postIncrementalMAC(uint8_t incr_mac_start, uint8_t amount);
static inline 









result_t TinySecM__postIncrementalMACFinish(void);
static inline 



result_t TinySecM__postIncrementalDecryptInit(void);
static 



result_t TinySecM__postIncrementalDecrypt(uint8_t incr_decrypt_start, uint8_t amount);
static 









result_t TinySecM__checkQueuedCrypto(void);
static 
#line 297
result_t TinySecM__decryptIncrementalInit(void);
static inline 
#line 346
result_t TinySecM__decryptIncremental(uint8_t incr_decrypt_start, uint8_t amount);
static inline 
#line 384
result_t TinySecM__MACincrementalInit(void);
static inline 
#line 400
result_t TinySecM__computeMACIncremental(uint8_t incr_mac_start, uint8_t amount);
static inline 
#line 424
result_t TinySecM__computeMACIncrementalFinish(void);
static 
#line 452
result_t TinySecM__verifyMAC(void);
static inline   
#line 490
result_t TinySecM__TinySec__receiveInit(
TOS_Msg_TinySecCompat *cleartext_ptr);
static   
#line 516
result_t TinySecM__TinySecRadio__byteReceived(uint8_t byte);
static inline   
#line 642
uint16_t TinySecM__TinySec__sendInit(TOS_Msg_TinySecCompat *cleartext_ptr);
static inline 
#line 681
result_t TinySecM__computeMAC(void);
static inline 
#line 701
result_t TinySecM__addPadding(TOS_Msg_TinySecCompat *bufptr, uint16_t dataLength);
static inline 
#line 714
result_t TinySecM__encrypt(void);
static inline 
#line 764
result_t TinySecM__noEncrypt(void);
static inline   





result_t TinySecM__TinySec__send(void);
static   
#line 786
uint8_t TinySecM__TinySecRadio__getTransmitByte(void);
# 55 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
typedef struct SkipJackM__SJContext {
  uint8_t skey[32 * 4];
} SkipJackM__SJContext;


enum SkipJackM____nesc_unnamed4273 {
#line 60
  SkipJackM__BSIZE = 8
};

uint8_t  F[256] = 
{ 
0xA3, 0xD7, 0x09, 0x83, 0xF8, 0x48, 0xF6, 0xF4, 
0xB3, 0x21, 0x15, 0x78, 0x99, 0xB1, 0xAF, 0xF9, 
0xE7, 0x2D, 0x4D, 0x8A, 0xCE, 0x4C, 0xCA, 0x2E, 
0x52, 0x95, 0xD9, 0x1E, 0x4E, 0x38, 0x44, 0x28, 
0x0A, 0xDF, 0x02, 0xA0, 0x17, 0xF1, 0x60, 0x68, 
0x12, 0xB7, 0x7A, 0xC3, 0xE9, 0xFA, 0x3D, 0x53, 
0x96, 0x84, 0x6B, 0xBA, 0xF2, 0x63, 0x9A, 0x19, 
0x7C, 0xAE, 0xE5, 0xF5, 0xF7, 0x16, 0x6A, 0xA2, 
0x39, 0xB6, 0x7B, 0x0F, 0xC1, 0x93, 0x81, 0x1B, 
0xEE, 0xB4, 0x1A, 0xEA, 0xD0, 0x91, 0x2F, 0xB8, 
0x55, 0xB9, 0xDA, 0x85, 0x3F, 0x41, 0xBF, 0xE0, 
0x5A, 0x58, 0x80, 0x5F, 0x66, 0x0B, 0xD8, 0x90, 
0x35, 0xD5, 0xC0, 0xA7, 0x33, 0x06, 0x65, 0x69, 
0x45, 0x00, 0x94, 0x56, 0x6D, 0x98, 0x9B, 0x76, 
0x97, 0xFC, 0xB2, 0xC2, 0xB0, 0xFE, 0xDB, 0x20, 
0xE1, 0xEB, 0xD6, 0xE4, 0xDD, 0x47, 0x4A, 0x1D, 
0x42, 0xED, 0x9E, 0x6E, 0x49, 0x3C, 0xCD, 0x43, 
0x27, 0xD2, 0x07, 0xD4, 0xDE, 0xC7, 0x67, 0x18, 
0x89, 0xCB, 0x30, 0x1F, 0x8D, 0xC6, 0x8F, 0xAA, 
0xC8, 0x74, 0xDC, 0xC9, 0x5D, 0x5C, 0x31, 0xA4, 
0x70, 0x88, 0x61, 0x2C, 0x9F, 0x0D, 0x2B, 0x87, 
0x50, 0x82, 0x54, 0x64, 0x26, 0x7D, 0x03, 0x40, 
0x34, 0x4B, 0x1C, 0x73, 0xD1, 0xC4, 0xFD, 0x3B, 
0xCC, 0xFB, 0x7F, 0xAB, 0xE6, 0x3E, 0x5B, 0xA5, 
0xAD, 0x04, 0x23, 0x9C, 0x14, 0x51, 0x22, 0xF0, 
0x29, 0x79, 0x71, 0x7E, 0xFF, 0x8C, 0x0E, 0xE2, 
0x0C, 0xEF, 0xBC, 0x72, 0x75, 0x6F, 0x37, 0xA1, 
0xEC, 0xD3, 0x8E, 0x62, 0x8B, 0x86, 0x10, 0xE8, 
0x08, 0x77, 0x11, 0xBE, 0x92, 0x4F, 0x24, 0xC5, 
0x32, 0x36, 0x9D, 0xCF, 0xF3, 0xA6, 0xBB, 0xAC, 
0x5E, 0x6C, 0xA9, 0x13, 0x57, 0x25, 0xB5, 0xE3, 
0xBD, 0xA8, 0x3A, 0x01, 0x05, 0x59, 0x2A, 0x46 };
static 
#line 157
result_t SkipJackM__setupKey(CipherContext *context, uint8_t *key, uint8_t keysize);
static inline 



void SkipJackM__dumpBuffer(char *bufName, uint8_t *buf, uint8_t size);
static inline  
#line 191
result_t SkipJackM__BlockCipher__init(CipherContext *context, uint8_t blockSize, 
uint8_t keySize, uint8_t *key);
static   
#line 212
result_t SkipJackM__BlockCipher__encrypt(CipherContext *context, 
uint8_t *plainBlock, 
uint8_t *cipherBlock);
static   
#line 267
result_t SkipJackM__BlockCipher__decrypt(CipherContext *context, 
uint8_t *cipherBlock, 
uint8_t *plainBlock);
static 
#line 312
result_t SkipJackM__setupKey(CipherContext *context, uint8_t *key, uint8_t keysize);
static inline   
#line 338
uint8_t SkipJackM__BlockCipherInfo__getPreferredBlockSize(void);
static   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
uint8_t CBCModeM__BlockCipherInfo__getPreferredBlockSize(void);
static   
# 90 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
result_t CBCModeM__BlockCipher__decrypt(CipherContext *arg_0xa4570b8, 
uint8_t *arg_0xa457230, uint8_t *arg_0xa457398);
static  
#line 60
result_t CBCModeM__BlockCipher__init(CipherContext *arg_0xa3dbc88, 
uint8_t arg_0xa3dbde8, uint8_t arg_0xa3dbf30, uint8_t *arg_0xa4560b8);
static   
#line 74
result_t CBCModeM__BlockCipher__encrypt(CipherContext *arg_0xa4567a8, 
uint8_t *arg_0xa456920, uint8_t *arg_0xa456a88);
# 74 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCModeM.nc"
enum CBCModeM____nesc_unnamed4274 {


  CBCModeM__CBCMODE_MAX_BLOCK_SIZE = 8
};
#line 90
enum CBCModeM____nesc_unnamed4275 {
  CBCModeM__ONE_BLOCK, 
  CBCModeM__GENERAL, 
  CBCModeM__TWO_LEFT_A, 
  CBCModeM__TWO_LEFT_B
};

typedef struct CBCModeM__CBCModeContext {
  uint8_t spill1[CBCModeM__CBCMODE_MAX_BLOCK_SIZE];
  uint8_t spill2[CBCModeM__CBCMODE_MAX_BLOCK_SIZE];
  uint8_t bsize;
  uint16_t remaining;
  uint16_t completed;
  uint8_t accum;

  uint8_t offset;
  uint8_t state;
} __attribute((packed))  CBCModeM__CBCModeContext;
static inline  
#line 127
result_t CBCModeM__BlockCipherMode__init(CipherModeContext *context, 
uint8_t keySize, uint8_t *key);
static inline 









void CBCModeM__dumpBuffer(char *bufName, uint8_t *buf, uint8_t size);
static   
#line 172
result_t CBCModeM__BlockCipherMode__encrypt(CipherModeContext *context, 
uint8_t *plainBlocks, 
uint8_t *cipherBlocks, 
uint16_t numBytes, uint8_t *IV);
static   
#line 440
result_t CBCModeM__BlockCipherMode__initIncrementalDecrypt(
CipherModeContext *context, 
uint8_t *IV, 
uint16_t length);
static inline   
#line 494
result_t CBCModeM__BlockCipherMode__incrementalDecrypt(
CipherModeContext *context, 
uint8_t *cipher, 
uint8_t *plain, 
uint16_t length, 
uint16_t *done);
static   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
uint8_t CBCMAC__BlockCipherInfo__getPreferredBlockSize(void);
static  
# 60 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
result_t CBCMAC__BlockCipher__init(CipherContext *arg_0xa3dbc88, 
uint8_t arg_0xa3dbde8, uint8_t arg_0xa3dbf30, uint8_t *arg_0xa4560b8);
static   
#line 74
result_t CBCMAC__BlockCipher__encrypt(CipherContext *arg_0xa4567a8, 
uint8_t *arg_0xa456920, uint8_t *arg_0xa456a88);
# 50 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCMAC.nc"
enum CBCMAC____nesc_unnamed4276 {


  CBCMAC__CBCMAC_BLOCK_SIZE = 8
};
typedef struct CBCMAC__CBCMACContext {


  uint8_t partial[CBCMAC__CBCMAC_BLOCK_SIZE];

  uint16_t length;

  uint8_t blockPos;
} __attribute((packed))  CBCMAC__CBCMACContext;
static inline  
#line 79
result_t CBCMAC__MAC__init(MACContext *context, uint8_t keySize, 
uint8_t *key);
static   
#line 102
result_t CBCMAC__MAC__initIncrementalMAC(MACContext *context, 
uint16_t length);
static   
#line 143
result_t CBCMAC__MAC__incrementalMAC(MACContext *context, uint8_t *msg, 
uint16_t msgLen);
static   
#line 188
result_t CBCMAC__MAC__getIncrementalMAC(MACContext *context, uint8_t *res, 
uint8_t macSize);
static inline   
#line 230
result_t CBCMAC__MAC__MAC(MACContext *context, uint8_t *msg, 
uint16_t length, 
uint8_t *res, uint8_t macSize);
# 50 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
uint8_t LedsC__ledsOn;

enum LedsC____nesc_unnamed4277 {
  LedsC__RED_BIT = 1, 
  LedsC__GREEN_BIT = 2, 
  LedsC__YELLOW_BIT = 4
};
static inline   
result_t LedsC__Leds__init(void);
static inline   









result_t LedsC__Leds__redOn(void);
static inline   







result_t LedsC__Leds__redOff(void);
static inline   







result_t LedsC__Leds__redToggle(void);
static inline   









result_t LedsC__Leds__greenOn(void);
static inline   







result_t LedsC__Leds__greenOff(void);
static inline   







result_t LedsC__Leds__greenToggle(void);
static inline   









result_t LedsC__Leds__yellowOn(void);
static inline   







result_t LedsC__Leds__yellowOff(void);
static inline   







result_t LedsC__Leds__yellowToggle(void);
static   
# 117 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
uint16_t MicaHighSpeedRadioTinySecM__TinySec__sendInit(TOS_Msg_TinySecCompat *arg_0xa3c9800);
static   
#line 135
result_t MicaHighSpeedRadioTinySecM__TinySec__send(void);
static   
#line 59
result_t MicaHighSpeedRadioTinySecM__TinySec__receiveInit(TOS_Msg_TinySecCompat *arg_0xa3c8188);
static   
# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
uint16_t MicaHighSpeedRadioTinySecM__RadioTiming__currentTime(void);
static   
#line 33
uint16_t MicaHighSpeedRadioTinySecM__RadioTiming__getTiming(void);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t MicaHighSpeedRadioTinySecM__PowerManagement__adjustPower(void);
static  
# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t MicaHighSpeedRadioTinySecM__Send__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static   
# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
result_t MicaHighSpeedRadioTinySecM__Random__init(void);
static   
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
result_t MicaHighSpeedRadioTinySecM__Code__encode_flush(void);
static   
result_t MicaHighSpeedRadioTinySecM__Code__decode(char arg_0xa320ea8);
static   
#line 34
result_t MicaHighSpeedRadioTinySecM__Code__encode(char arg_0xa320a98);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr MicaHighSpeedRadioTinySecM__Receive__receive(TOS_MsgPtr arg_0xa2a2f98);
static   
# 61 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySecRadio.nc"
result_t MicaHighSpeedRadioTinySecM__TinySecRadio__byteReceived(uint8_t arg_0xa3d4178);
static   
#line 46
uint8_t MicaHighSpeedRadioTinySecM__TinySecRadio__getTransmitByte(void);
static   
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__send(uint8_t arg_0xa38eb28);
static   



result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__phaseShift(void);
static   
#line 35
result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__startReadBytes(uint16_t arg_0xa38f218);
static   
#line 34
result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__idle(void);
static   
result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__txMode(void);
static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__rxMode(void);
static   
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8);
static   
#line 45
void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__startSymbol(void);
static   
# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
result_t MicaHighSpeedRadioTinySecM__ChannelMon__macDelay(void);
static   
#line 34
result_t MicaHighSpeedRadioTinySecM__ChannelMon__startSymbolSearch(void);
static   
#line 33
result_t MicaHighSpeedRadioTinySecM__ChannelMon__init(void);
static   
# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Time.nc"
uint16_t MicaHighSpeedRadioTinySecM__Time__getUs(void);
static   
#line 46
tos_time_t MicaHighSpeedRadioTinySecM__Time__get(void);
static   
# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8);
static   
#line 45
void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__startSymbol(void);
# 61 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
enum MicaHighSpeedRadioTinySecM____nesc_unnamed4278 {
  MicaHighSpeedRadioTinySecM__IDLE_STATE, 
  MicaHighSpeedRadioTinySecM__SEND_WAITING, 
  MicaHighSpeedRadioTinySecM__HEADER_RX_STATE, 
  MicaHighSpeedRadioTinySecM__RX_STATE_TINYSEC, 
  MicaHighSpeedRadioTinySecM__RX_STATE, 
  MicaHighSpeedRadioTinySecM__TRANSMITTING, 
  MicaHighSpeedRadioTinySecM__TRANSMITTING_TINYSEC, 
  MicaHighSpeedRadioTinySecM__WAITING_FOR_ACK, 
  MicaHighSpeedRadioTinySecM__SENDING_STRENGTH_PULSE, 
  MicaHighSpeedRadioTinySecM__TRANSMITTING_START, 
  MicaHighSpeedRadioTinySecM__RX_DONE_STATE, 
  MicaHighSpeedRadioTinySecM__ACK_SEND_STATE, 
  MicaHighSpeedRadioTinySecM__STOPPED_STATE
};

enum MicaHighSpeedRadioTinySecM____nesc_unnamed4279 {
  MicaHighSpeedRadioTinySecM__ACK_CNT = 4, 
  MicaHighSpeedRadioTinySecM__ENCODE_PACKET_LENGTH_DEFAULT = MSG_DATA_SIZE * 3
};






char  TOSH_MHSR_start[12] = 
{ 0xf0, 0xf0, 0xf0, 0xff, 0x00, 0xff, 0x0f, 0x00, 0xff, 0x0f, 0x0f, 0x0f };

char MicaHighSpeedRadioTinySecM__state;
char MicaHighSpeedRadioTinySecM__send_state;
char MicaHighSpeedRadioTinySecM__tx_count;
uint16_t MicaHighSpeedRadioTinySecM__calc_crc;
uint8_t MicaHighSpeedRadioTinySecM__ack_count;
char MicaHighSpeedRadioTinySecM__rec_count;
TOS_Msg_TinySecCompat MicaHighSpeedRadioTinySecM__buffer;
TOS_Msg_TinySecCompat *MicaHighSpeedRadioTinySecM__rec_ptr;
TOS_Msg_TinySecCompat *MicaHighSpeedRadioTinySecM__send_ptr;
unsigned char MicaHighSpeedRadioTinySecM__rx_count;
char MicaHighSpeedRadioTinySecM__msg_length;
char MicaHighSpeedRadioTinySecM__buf_head;
char MicaHighSpeedRadioTinySecM__buf_end;
char MicaHighSpeedRadioTinySecM__encoded_buffer[4];
char MicaHighSpeedRadioTinySecM__enc_count;




bool MicaHighSpeedRadioTinySecM__tx_done;
bool MicaHighSpeedRadioTinySecM__tinysec_rx_done;
static 

void MicaHighSpeedRadioTinySecM__swapLengthAndGroup(TOS_Msg *buf);
static  







void MicaHighSpeedRadioTinySecM__packetReceived(void);
static inline  
#line 141
void MicaHighSpeedRadioTinySecM__packetSent(void);
static inline  
#line 156
result_t MicaHighSpeedRadioTinySecM__Send__send(TOS_MsgPtr msg);
static inline  
#line 179
result_t MicaHighSpeedRadioTinySecM__Control__init(void);
static inline  
#line 192
result_t MicaHighSpeedRadioTinySecM__Control__start(void);
static inline   
#line 233
result_t MicaHighSpeedRadioTinySecM__TinySec__sendDone(result_t result);
static   





result_t MicaHighSpeedRadioTinySecM__TinySec__receiveInitDone(result_t result, 
uint16_t length, 
bool ts_enabled);
static inline   
#line 263
result_t MicaHighSpeedRadioTinySecM__TinySec__receiveDone(result_t result);
static inline   
#line 276
result_t MicaHighSpeedRadioTinySecM__ChannelMon__startSymDetect(void);
static 
#line 302
void MicaHighSpeedRadioTinySecM__timeSyncFunctionHack(void);
static inline   
#line 314
result_t MicaHighSpeedRadioTinySecM__ChannelMon__idleDetect(void);
static inline   
#line 387
result_t MicaHighSpeedRadioTinySecM__Code__decodeDone(char data, char error);
static   
#line 444
result_t MicaHighSpeedRadioTinySecM__Code__encodeDone(char data1);
static   








result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__dataReady(uint8_t data);
static inline    
#line 590
void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__startSymbol(void);
static inline    void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__byte(TOS_MsgPtr msg, uint8_t byteCount);
static inline    void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__startSymbol(void);
static inline    void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__byte(TOS_MsgPtr msg, uint8_t byteCount);
static  
# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t RadioPacketTinySecM__Send__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr RadioPacketTinySecM__Receive__receive(TOS_MsgPtr arg_0xa2a2f98);
static  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t RadioPacketTinySecM__RadioSend__send(TOS_MsgPtr arg_0xa2c3448);
static inline  
# 46 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioPacketTinySecM.nc"
result_t RadioPacketTinySecM__Send__send(TOS_MsgPtr msg);
static inline  
#line 58
result_t RadioPacketTinySecM__RadioSend__sendDone(TOS_MsgPtr msg, result_t success);
static inline  




TOS_MsgPtr RadioPacketTinySecM__RadioReceive__receive(TOS_MsgPtr msg);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t TimerM__PowerManagement__adjustPower(void);
static   
# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
void TimerM__Clock__setInterval(uint8_t arg_0xa57ebe8);
static  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t TimerM__Timer__fired(
# 45 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t arg_0xa584490);









uint32_t TimerM__mState;
uint8_t TimerM__setIntervalFlag;
uint8_t TimerM__mInterval;
int8_t TimerM__queue_head;
int8_t TimerM__queue_tail;
uint8_t TimerM__queue_size;
uint8_t TimerM__queue[NUM_TIMERS];

struct TimerM__timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM__mTimerList[NUM_TIMERS];

enum TimerM____nesc_unnamed4280 {
  TimerM__maxTimerInterval = 230
};
#line 116
static void TimerM__adjustInterval(void);
static inline   
#line 154
result_t TimerM__Timer__default__fired(uint8_t id);
static inline 


void TimerM__enqueue(uint8_t value);
static inline 






uint8_t TimerM__dequeue(void);
static inline  








void TimerM__signalOneTimer(void);
static inline  




void TimerM__HandleFire(void);
static inline   
#line 204
result_t TimerM__Clock__fire(void);
static   
# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLClock__Clock__fire(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
uint8_t HPLClock__set_flag;
uint8_t HPLClock__mscale;
#line 55
uint8_t HPLClock__nextScale;
#line 55
uint8_t HPLClock__minterval;
static inline   
#line 87
void HPLClock__Clock__setInterval(uint8_t value);
#line 167
void __attribute((interrupt))   __vector_15(void);
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
bool HPLPowerManagementM__disabled = TRUE;

enum HPLPowerManagementM____nesc_unnamed4281 {
  HPLPowerManagementM__IDLE = 0, 
  HPLPowerManagementM__ADC_NR = 1 << 3, 
  HPLPowerManagementM__POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM__POWER_DOWN = 1 << 3
};
static inline 


uint8_t HPLPowerManagementM__getPowerLevel(void);
static inline  
#line 85
void HPLPowerManagementM__doAdjustment(void);
static   
#line 103
uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void);
static  
# 61 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/AbsoluteTimer.nc"
result_t SimpleTimeM__AbsoluteTimer__fired(
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
uint8_t arg_0xa5d5330);
static   
# 65 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TimeUtil.nc"
tos_time_t SimpleTimeM__TimeUtil__addUint32(tos_time_t arg_0xa561520, uint32_t arg_0xa561670);
static   
#line 82
char SimpleTimeM__TimeUtil__compare(tos_time_t arg_0xa55e2f8, tos_time_t arg_0xa55e448);
# 65 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
enum SimpleTimeM____nesc_unnamed4282 {
  SimpleTimeM__INTERVAL = 32
};
tos_time_t SimpleTimeM__time;
tos_time_t SimpleTimeM__aTimer[MAX_NUM_TIMERS];
static inline   
#line 94
uint16_t SimpleTimeM__Time__getUs(void);
static inline   


tos_time_t SimpleTimeM__Time__get(void);
static inline   
#line 147
result_t SimpleTimeM__AbsoluteTimer__default__fired(uint8_t id);
static inline  


result_t SimpleTimeM__Timer__fired(void);
static inline   
# 53 "C:/cygwin/opt/tinyos-1.x/tos/system/TimeUtilC.nc"
char TimeUtilC__TimeUtil__compare(tos_time_t a, tos_time_t b);
static inline   
#line 99
tos_time_t TimeUtilC__TimeUtil__addUint32(tos_time_t a, uint32_t ms);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr FramerM__ReceiveMsg__receive(TOS_MsgPtr arg_0xa2a2f98);
static   
# 55 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t FramerM__ByteComm__txByte(uint8_t arg_0xa615170);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t FramerM__ByteControl__init(void);
static  





result_t FramerM__ByteControl__start(void);
static  
# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t FramerM__BareSendMsg__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0);
static  
# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
TOS_MsgPtr FramerM__TokenReceiveMsg__receive(TOS_MsgPtr arg_0xa2c85d0, uint8_t arg_0xa2c8718);
# 82 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
enum FramerM____nesc_unnamed4283 {
  FramerM__HDLC_QUEUESIZE = 2, 
  FramerM__HDLC_MTU = sizeof(TOS_Msg ), 
  FramerM__HDLC_FLAG_BYTE = 0x7e, 
  FramerM__HDLC_CTLESC_BYTE = 0x7d, 
  FramerM__PROTO_ACK = 64, 
  FramerM__PROTO_PACKET_ACK = 65, 
  FramerM__PROTO_PACKET_NOACK = 66, 
  FramerM__PROTO_UNKNOWN = 255
};

enum FramerM____nesc_unnamed4284 {
  FramerM__RXSTATE_NOSYNC, 
  FramerM__RXSTATE_PROTO, 
  FramerM__RXSTATE_TOKEN, 
  FramerM__RXSTATE_INFO, 
  FramerM__RXSTATE_ESC
};

enum FramerM____nesc_unnamed4285 {
  FramerM__TXSTATE_IDLE, 
  FramerM__TXSTATE_PROTO, 
  FramerM__TXSTATE_INFO, 
  FramerM__TXSTATE_ESC, 
  FramerM__TXSTATE_FCS1, 
  FramerM__TXSTATE_FCS2, 
  FramerM__TXSTATE_ENDFLAG, 
  FramerM__TXSTATE_FINISH, 
  FramerM__TXSTATE_ERROR
};

enum FramerM____nesc_unnamed4286 {
  FramerM__FLAGS_TOKENPEND = 0x2, 
  FramerM__FLAGS_DATAPEND = 0x4, 
  FramerM__FLAGS_UNKNOWN = 0x8
};

TOS_Msg FramerM__gMsgRcvBuf[FramerM__HDLC_QUEUESIZE];

typedef struct FramerM___MsgRcvEntry {
  uint8_t Proto;
  uint8_t Token;
  uint16_t Length;
  TOS_MsgPtr pMsg;
} FramerM__MsgRcvEntry_t;

FramerM__MsgRcvEntry_t FramerM__gMsgRcvTbl[FramerM__HDLC_QUEUESIZE];

uint8_t *FramerM__gpRxBuf;
uint8_t *FramerM__gpTxBuf;

uint8_t FramerM__gFlags;
 

uint8_t FramerM__gTxState;
 uint8_t FramerM__gPrevTxState;
 uint8_t FramerM__gTxProto;
 uint16_t FramerM__gTxByteCnt;
 uint16_t FramerM__gTxLength;
 uint16_t FramerM__gTxRunningCRC;


uint8_t FramerM__gRxState;
uint8_t FramerM__gRxHeadIndex;
uint8_t FramerM__gRxTailIndex;
uint16_t FramerM__gRxByteCnt;

uint16_t FramerM__gRxRunningCRC;

TOS_MsgPtr FramerM__gpTxMsg;
uint8_t FramerM__gTxTokenBuf;
uint8_t FramerM__gTxUnknownBuf;
 uint8_t FramerM__gTxEscByte;
static  
void FramerM__PacketSent(void);
static 
result_t FramerM__StartTx(void);
static inline  
#line 202
void FramerM__PacketUnknown(void);
static inline  






void FramerM__PacketRcvd(void);
static  
#line 246
void FramerM__PacketSent(void);
static 
#line 268
void FramerM__HDLCInitialize(void);
static inline  
#line 291
result_t FramerM__StdControl__init(void);
static inline  



result_t FramerM__StdControl__start(void);
static inline  








result_t FramerM__BareSendMsg__send(TOS_MsgPtr pMsg);
static inline  
#line 328
result_t FramerM__TokenReceiveMsg__ReflectToken(uint8_t Token);
static   
#line 348
result_t FramerM__ByteComm__rxByteReady(uint8_t data, bool error, uint16_t strength);
static 
#line 469
result_t FramerM__TxArbitraryByte(uint8_t Byte);
static inline   
#line 482
result_t FramerM__ByteComm__txByteReady(bool LastByteSuccess);
static inline   
#line 552
result_t FramerM__ByteComm__txDone(void);
static   
# 62 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t UARTM__HPLUART__init(void);
static   
#line 80
result_t UARTM__HPLUART__put(uint8_t arg_0xa650cb8);
static   
# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t UARTM__ByteComm__txDone(void);
static   
#line 75
result_t UARTM__ByteComm__txByteReady(bool arg_0xa615dd0);
static   
#line 66
result_t UARTM__ByteComm__rxByteReady(uint8_t arg_0xa615600, bool arg_0xa615748, uint16_t arg_0xa6158a0);
# 58 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
bool UARTM__state;
static inline  
result_t UARTM__Control__init(void);
static inline  






result_t UARTM__Control__start(void);
static inline   







result_t UARTM__HPLUART__get(uint8_t data);
static   








result_t UARTM__HPLUART__putDone(void);
static   
#line 110
result_t UARTM__ByteComm__txByte(uint8_t data);
static   
# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t HPLUARTM__UART__get(uint8_t arg_0xa6511b8);
static   






result_t HPLUARTM__UART__putDone(void);
static inline   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM__UART__init(void);
#line 71
void __attribute((signal))   __vector_18(void);





void __attribute((interrupt))   __vector_20(void);
static inline   


result_t HPLUARTM__UART__put(uint8_t data);
static  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySecControl.nc"
result_t SecurityM__TinySecControl__init(uint8_t arg_0xa3c7c28, uint8_t *arg_0xa3c7d90, uint8_t *arg_0xa3c7ef0);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SecurityM.nc"
uint8_t SecurityM__enc_key[8];
uint8_t SecurityM__mac_key[8];
static inline  

result_t SecurityM__Control__init(void);
static inline  
#line 71
result_t SecurityM__Control__start(void);
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 5;
}

#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) &= ~(1 << 5);
}

#line 65
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 4;
}

#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 4;
}

#line 59
static __inline void TOSH_SET_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_SET_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_SET_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}








static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void)
#line 67
{
#line 67
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 64
static __inline void TOSH_SET_POT_POWER_PIN(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 7;
}











static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 7;
}

static __inline void TOSH_MAKE_PW0_OUTPUT(void)
#line 81
{
#line 81
  ;
}

#line 82
static __inline void TOSH_MAKE_PW1_OUTPUT(void)
#line 82
{
#line 82
  ;
}

#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void)
#line 83
{
#line 83
  ;
}

#line 84
static __inline void TOSH_MAKE_PW3_OUTPUT(void)
#line 84
{
#line 84
  ;
}

#line 85
static __inline void TOSH_MAKE_PW4_OUTPUT(void)
#line 85
{
#line 85
  ;
}

#line 86
static __inline void TOSH_MAKE_PW5_OUTPUT(void)
#line 86
{
#line 86
  ;
}

#line 87
static __inline void TOSH_MAKE_PW6_OUTPUT(void)
#line 87
{
#line 87
  ;
}

#line 88
static __inline void TOSH_MAKE_PW7_OUTPUT(void)
#line 88
{
#line 88
  ;
}

#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 7;
}

#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 5;
}

#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 2;
}

static inline 
#line 103
void TOSH_SET_PIN_DIRECTIONS(void )
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) = 0x02;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) = 0x02;
  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();
  TOSH_MAKE_POT_SELECT_OUTPUT();
  TOSH_MAKE_POT_POWER_OUTPUT();

  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();

  TOSH_MAKE_RFM_CTL0_OUTPUT();
  TOSH_MAKE_RFM_CTL1_OUTPUT();
  TOSH_MAKE_RFM_TXD_OUTPUT();
  TOSH_SET_POT_POWER_PIN();



  TOSH_MAKE_FLASH_SELECT_INPUT();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();


  TOSH_MAKE_BOOST_ENABLE_OUTPUT();
  TOSH_SET_BOOST_ENABLE_PIN();

  TOSH_MAKE_ONE_WIRE_INPUT();
  TOSH_SET_ONE_WIRE_PIN();
}

static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit__init(void)
#line 57
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
inline static  result_t RealMain__hardwareInit(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLInit__init();
#line 47

#line 47
  return result;
#line 47
}
#line 47
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}

#line 61
static __inline void TOSH_SET_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

static inline  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__finalise(void)
#line 74
{
  TOSH_SET_UD_PIN();
  TOSH_SET_INC_PIN();
  return SUCCESS;
}

# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__finalise(void){
#line 74
  unsigned char result;
#line 74

#line 74
  result = HPLPotC__Pot__finalise();
#line 74

#line 74
  return result;
#line 74
}
#line 74
# 63 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 5;
}

#line 62
static __inline void TOSH_CLR_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 5);
}

#line 61
static __inline void TOSH_CLR_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline  
# 65 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__increase(void)
#line 65
{
  TOSH_CLR_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__increase(void){
#line 67
  unsigned char result;
#line 67

#line 67
  result = HPLPotC__Pot__increase();
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC__Pot__decrease(void)
#line 56
{
  TOSH_SET_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM__HPLPot__decrease(void){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLPotC__Pot__decrease();
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline 
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
void PotM__setPot(uint8_t value)
#line 93
{
  uint8_t i;

#line 95
  for (i = 0; i < 151; i++) 
    PotM__HPLPot__decrease();

  for (i = 0; i < value; i++) 
    PotM__HPLPot__increase();

  PotM__HPLPot__finalise();

  PotM__potSetting = value;
}

static inline  result_t PotM__Pot__init(uint8_t initialSetting)
#line 106
{
  PotM__setPot(initialSetting);
  return SUCCESS;
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
inline static  result_t RealMain__Pot__init(uint8_t arg_0xa272f00){
#line 78
  unsigned char result;
#line 78

#line 78
  result = PotM__Pot__init(arg_0xa272f00);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 76 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
void TOSH_sched_init(void )
{
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
}

static inline 
# 108 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

static inline  
# 191 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
result_t SkipJackM__BlockCipher__init(CipherContext *context, uint8_t blockSize, 
uint8_t keySize, uint8_t *key)
{

  if (blockSize != SkipJackM__BSIZE) {
      return FAIL;
    }
  return SkipJackM__setupKey(context, key, keySize);
}

# 60 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
inline static  result_t CBCMAC__BlockCipher__init(CipherContext *arg_0xa3dbc88, uint8_t arg_0xa3dbde8, uint8_t arg_0xa3dbf30, uint8_t *arg_0xa4560b8){
#line 60
  unsigned char result;
#line 60

#line 60
  result = SkipJackM__BlockCipher__init(arg_0xa3dbc88, arg_0xa3dbde8, arg_0xa3dbf30, arg_0xa4560b8);
#line 60

#line 60
  return result;
#line 60
}
#line 60
static inline   
# 338 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
uint8_t SkipJackM__BlockCipherInfo__getPreferredBlockSize(void)
{
  return SkipJackM__BSIZE;
}

# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
inline static   uint8_t CBCMAC__BlockCipherInfo__getPreferredBlockSize(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = SkipJackM__BlockCipherInfo__getPreferredBlockSize();
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline  
# 79 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCMAC.nc"
result_t CBCMAC__MAC__init(MACContext *context, uint8_t keySize, 
uint8_t *key)
{
  if (CBCMAC__BlockCipherInfo__getPreferredBlockSize() != CBCMAC__CBCMAC_BLOCK_SIZE) {

      return FAIL;
    }

  return CBCMAC__BlockCipher__init(& context->cc, CBCMAC__CBCMAC_BLOCK_SIZE, 
  keySize, key);
}

# 60 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
inline static  result_t TinySecM__MAC__init(MACContext *arg_0xa3e18e0, uint8_t arg_0xa3e1a28, uint8_t *arg_0xa3e1b88){
#line 60
  unsigned char result;
#line 60

#line 60
  result = CBCMAC__MAC__init(arg_0xa3e18e0, arg_0xa3e1a28, arg_0xa3e1b88);
#line 60

#line 60
  return result;
#line 60
}
#line 60
# 60 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
inline static  result_t CBCModeM__BlockCipher__init(CipherContext *arg_0xa3dbc88, uint8_t arg_0xa3dbde8, uint8_t arg_0xa3dbf30, uint8_t *arg_0xa4560b8){
#line 60
  unsigned char result;
#line 60

#line 60
  result = SkipJackM__BlockCipher__init(arg_0xa3dbc88, arg_0xa3dbde8, arg_0xa3dbf30, arg_0xa4560b8);
#line 60

#line 60
  return result;
#line 60
}
#line 60
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
inline static   uint8_t CBCModeM__BlockCipherInfo__getPreferredBlockSize(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = SkipJackM__BlockCipherInfo__getPreferredBlockSize();
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline  
# 127 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCModeM.nc"
result_t CBCModeM__BlockCipherMode__init(CipherModeContext *context, 
uint8_t keySize, uint8_t *key)
{
  uint8_t blockSize = CBCModeM__BlockCipherInfo__getPreferredBlockSize();

#line 131
  if (blockSize > CBCModeM__CBCMODE_MAX_BLOCK_SIZE) {
      return FAIL;
    }
  (
  (CBCModeM__CBCModeContext *)context->context)->bsize = blockSize;
  return CBCModeM__BlockCipher__init(& context->cc, blockSize, keySize, key);
}

# 65 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherMode.nc"
inline static  result_t TinySecM__BlockCipherMode__init(CipherModeContext *arg_0xa3e4590, uint8_t arg_0xa3e46e8, uint8_t *arg_0xa3e4848){
#line 65
  unsigned char result;
#line 65

#line 65
  result = CBCModeM__BlockCipherMode__init(arg_0xa3e4590, arg_0xa3e46e8, arg_0xa3e4848);
#line 65

#line 65
  return result;
#line 65
}
#line 65
# 52 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherInfo.nc"
inline static   uint8_t TinySecM__BlockCipherInfo__getPreferredBlockSize(void){
#line 52
  unsigned char result;
#line 52

#line 52
  result = SkipJackM__BlockCipherInfo__getPreferredBlockSize();
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline  
# 138 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__TinySecControl__init(uint8_t keySize, 
uint8_t *encryptionKey, 
uint8_t *MACKey)
#line 140
{
  result_t r1;
#line 141
  result_t r2;
#line 141
  result_t r3;
#line 141
  result_t r4;
  int i;
#line 142
  int local_addr;
  uint8_t tmp = TinySecM__BlockCipherInfo__getPreferredBlockSize();

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 145
    {
      if (tmp > TinySecM__TINYSECM_MAX_BLOCK_SIZE) {
          TinySecM__blockSize = 0;
          r3 = FAIL;
        }
      else {
          TinySecM__blockSize = tmp;
          r3 = SUCCESS;
        }

      TinySecM__computeMACBuffer.computeMACWaiting = FALSE;
      TinySecM__computeMACBuffer.computeMACInitWaiting = FALSE;
      TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_IDLE;
      TinySecM__decrypt_state = TinySecM__DECRYPT_IDLE;
      TinySecM__decryptBuffer.decryptWaiting = FALSE;
      TinySecM__decryptBuffer.decryptInitWaiting = FALSE;

      TinySecM__computeMACBuffer.finishMACWaiting = FALSE;



      for (i = 0; i < TinySecM__blockSize; i++) {
          TinySecM__iv[i] = 0;
        }

      if (4 < TINYSEC_NODE_ID_SIZE) {
        r4 = FAIL;
        }
      else {
#line 173
        r4 = SUCCESS;
        }

      local_addr = TOS_LOCAL_ADDRESS;
      TinySecM__iv[4 - TINYSEC_NODE_ID_SIZE] = local_addr & 0xff;
      for (i = 1; i < TINYSEC_NODE_ID_SIZE; i++) {
          local_addr = local_addr >> 8;
          TinySecM__iv[4 - TINYSEC_NODE_ID_SIZE + i] = local_addr & 0xff;
        }
    }
#line 182
    __nesc_atomic_end(__nesc_atomic); }
  r1 = TinySecM__BlockCipherMode__init(&TinySecM__cipherModeContext, keySize, encryptionKey);
  r2 = TinySecM__MAC__init(&TinySecM__macContext, keySize, MACKey);
  if (rcombine(rcombine(r1, r2), rcombine(r3, r4)) == FAIL) {
    return FAIL;
    }
  else 
#line 187
    {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 188
        {
          TinySecM__initialized = TRUE;
        }
#line 190
        __nesc_atomic_end(__nesc_atomic); }
      return SUCCESS;
    }
}

# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySecControl.nc"
inline static  result_t SecurityM__TinySecControl__init(uint8_t arg_0xa3c7c28, uint8_t *arg_0xa3c7d90, uint8_t *arg_0xa3c7ef0){
#line 56
  unsigned char result;
#line 56

#line 56
  result = TinySecM__TinySecControl__init(arg_0xa3c7c28, arg_0xa3c7d90, arg_0xa3c7ef0);
#line 56

#line 56
  return result;
#line 56
}
#line 56
static inline  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SecurityM.nc"
result_t SecurityM__Control__init(void)
#line 58
{
  uint8_t key_tmp[2 * 8] = { 0x64, 0x82, 0xBE, 0x8B, 0x14, 0x67, 0x1B, 0x60, 0xBE, 0xCB, 0xD2, 0xFB, 0xC5, 0x36, 0x98, 0xD2 };

#line 60
  nmemcpy(SecurityM__enc_key, key_tmp, 8);
  nmemcpy(SecurityM__mac_key, key_tmp + 8, 8);

  if (!SecurityM__TinySecControl__init(8, SecurityM__enc_key, SecurityM__mac_key)) {
      {
      }
#line 64
      ;
      return FAIL;
    }

  return SUCCESS;
}

static inline 
# 116 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine3(result_t r1, result_t r2, result_t r3)
{
  return rcombine(r1, rcombine(r2, r3));
}

static inline   
# 58 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__init(void)
#line 58
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 59
    {
      LedsC__ledsOn = 0;
      {
      }
#line 61
      ;
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SecureTOSBaseM__Leds__init(void){
#line 56
  unsigned char result;
#line 56

#line 56
  result = LedsC__Leds__init();
#line 56

#line 56
  return result;
#line 56
}
#line 56
static inline   
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
result_t RandomLFSR__Random__init(void)
#line 59
{
  {
  }
#line 60
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 61
    {
      RandomLFSR__shiftReg = 119 * 119 * (TOS_LOCAL_ADDRESS + 1);
      RandomLFSR__initSeed = RandomLFSR__shiftReg;
      RandomLFSR__mask = 137 * 29 * (TOS_LOCAL_ADDRESS + 1);
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__Random__init(void){
#line 57
  unsigned char result;
#line 57

#line 57
  result = RandomLFSR__Random__init();
#line 57

#line 57
  return result;
#line 57
}
#line 57
static inline   
# 59 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC__ChannelMon__init(void)
#line 59
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 60
    {
      ChannelMonC__CM_waiting = -1;
    }
#line 62
    __nesc_atomic_end(__nesc_atomic); }
  return ChannelMonC__ChannelMon__startSymbolSearch();
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__ChannelMon__init(void){
#line 33
  unsigned char result;
#line 33

#line 33
  result = ChannelMonC__ChannelMon__init();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline  
# 179 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__Control__init(void)
#line 179
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 180
    {
      MicaHighSpeedRadioTinySecM__rec_ptr = &MicaHighSpeedRadioTinySecM__buffer;
      MicaHighSpeedRadioTinySecM__send_state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
      MicaHighSpeedRadioTinySecM__tx_done = FALSE;
      MicaHighSpeedRadioTinySecM__tinysec_rx_done = FALSE;
    }
#line 186
    __nesc_atomic_end(__nesc_atomic); }
  return rcombine(MicaHighSpeedRadioTinySecM__ChannelMon__init(), MicaHighSpeedRadioTinySecM__Random__init());
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SecureTOSBaseM__RadioControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = MicaHighSpeedRadioTinySecM__Control__init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 60 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM__Control__init(void)
#line 60
{
  {
  }
#line 61
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 62
    {
      UARTM__state = FALSE;
    }
#line 64
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM__ByteControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = UARTM__Control__init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 291 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__StdControl__init(void)
#line 291
{
  FramerM__HDLCInitialize();
  return FramerM__ByteControl__init();
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SecureTOSBaseM__UARTControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = FramerM__StdControl__init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 131 "SecureTOSBaseM.nc"
result_t SecureTOSBaseM__StdControl__init(void)
#line 131
{
  result_t ok1;
#line 132
  result_t ok2;
#line 132
  result_t ok3;
  uint8_t i;

  for (i = 0; i < SecureTOSBaseM__QUEUE_SIZE; i++) {
      SecureTOSBaseM__gRxBufPool[i].length = 0;
      SecureTOSBaseM__gRxBufPoolTbl[i] = &SecureTOSBaseM__gRxBufPool[i];
    }
  SecureTOSBaseM__gRxHeadIndex = 0;
  SecureTOSBaseM__gRxTailIndex = 0;

  SecureTOSBaseM__gTxBuf.length = 0;
  SecureTOSBaseM__gpTxMsg = &SecureTOSBaseM__gTxBuf;
  SecureTOSBaseM__gfTxFlags = 0;

  ok1 = SecureTOSBaseM__UARTControl__init();
  ok2 = SecureTOSBaseM__RadioControl__init();
  ok3 = SecureTOSBaseM__Leds__init();

  {
  }
#line 150
  ;

  return rcombine3(ok1, ok2, ok3);
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain__StdControl__init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = SecureTOSBaseM__StdControl__init();
#line 63
  result = rcombine(result, SecurityM__Control__init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 71 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SecurityM.nc"
result_t SecurityM__Control__start(void)
#line 71
{
  return SUCCESS;
}

# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__ChannelMon__startSymbolSearch(void){
#line 34
  unsigned char result;
#line 34

#line 34
  result = ChannelMonC__ChannelMon__startSymbolSearch();
#line 34

#line 34
  return result;
#line 34
}
#line 34
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t MicaHighSpeedRadioTinySecM__PowerManagement__adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM__PowerManagement__adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline  
# 192 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__Control__start(void)
#line 192
{
  uint8_t oldState;
  result_t rval = SUCCESS;

#line 195
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 195
    {
      oldState = MicaHighSpeedRadioTinySecM__state;
      if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__STOPPED_STATE) {
          MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
          MicaHighSpeedRadioTinySecM__send_state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
          MicaHighSpeedRadioTinySecM__PowerManagement__adjustPower();
          rval = MicaHighSpeedRadioTinySecM__ChannelMon__startSymbolSearch();
        }
    }
#line 203
    __nesc_atomic_end(__nesc_atomic); }

  return rval;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SecureTOSBaseM__RadioControl__start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = MicaHighSpeedRadioTinySecM__Control__start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 98 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_UART_RXD0_PIN(void)
#line 98
{
#line 98
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 0;
}

static inline   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM__UART__init(void)
#line 56
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x09 + 0x20) = 12;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20);
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) = 0xd8;
  TOSH_SET_UART_RXD0_PIN();

  return SUCCESS;
}

# 62 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM__HPLUART__init(void){
#line 62
  unsigned char result;
#line 62

#line 62
  result = HPLUARTM__UART__init();
#line 62

#line 62
  return result;
#line 62
}
#line 62
static inline  
# 68 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM__Control__start(void)
#line 68
{
  return UARTM__HPLUART__init();
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM__ByteControl__start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = UARTM__Control__start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 296 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__StdControl__start(void)
#line 296
{
  FramerM__HDLCInitialize();
  return FramerM__ByteControl__start();
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t SecureTOSBaseM__UARTControl__start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = FramerM__StdControl__start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 155 "SecureTOSBaseM.nc"
result_t SecureTOSBaseM__StdControl__start(void)
#line 155
{
  result_t ok1;
#line 156
  result_t ok2;

  ok1 = SecureTOSBaseM__UARTControl__start();
  ok2 = SecureTOSBaseM__RadioControl__start();

  return rcombine(ok1, ok2);
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain__StdControl__start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SecureTOSBaseM__StdControl__start();
#line 70
  result = rcombine(result, SecurityM__Control__start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline 
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM__getPowerLevel(void)
#line 62
{
  uint8_t diff;

#line 64
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {

      return HPLPowerManagementM__IDLE;
    }
  else {
#line 67
    if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM__IDLE;
      }
    else {
#line 69
      if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) & ((1 << 6) | (1 << 7))) {
          return HPLPowerManagementM__IDLE;
        }
      else {
        if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) & (1 << 7)) {
            return HPLPowerManagementM__ADC_NR;
          }
        else {
#line 75
          if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
              diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) - * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
              if (diff < 16) {
                return HPLPowerManagementM__IDLE;
                }
#line 79
              return HPLPowerManagementM__POWER_SAVE;
            }
          else 
#line 80
            {
              return HPLPowerManagementM__POWER_DOWN;
            }
          }
        }
      }
    }
}

static inline  
#line 85
void HPLPowerManagementM__doAdjustment(void)
#line 85
{
  uint8_t foo;
#line 86
  uint8_t mcu;

#line 87
  foo = HPLPowerManagementM__getPowerLevel();
  mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM__POWER_SAVE) {
      mcu |= HPLPowerManagementM__IDLE;
      while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
}

static 
# 165 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
__inline void __nesc_enable_interrupt(void)
#line 165
{
	__BLAST_interrupt_enabled = 1;
   __asm volatile ("sei");}


static inline 
#line 135
void TOSH_wait(void)
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

static inline 
void TOSH_sleep(void)
{

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
   __asm volatile ("sleep");}

#line 160
__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg) __attribute__((atomic))
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) = oldSreg;
   __BLAST_interrupt_enabled = 1;
}

#line 153
__inline __nesc_atomic_t  __nesc_atomic_start(void ) __attribute__((atomic))
{
  __nesc_atomic_t result = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20);

#line 156
   __BLAST_interrupt_enabled = 0;

   // __asm volatile ("cli");
  return result;
}

static inline 
# 139 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool TOSH_run_next_task(void)
#line 139
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );
/*
  if (TOSH_sched_full == TOSH_sched_free) {

      return 0;
    }
  else {

      fInterruptFlags = __nesc_atomic_start();
      old_full = TOSH_sched_full;
      TOSH_sched_full++;
      TOSH_sched_full &= TOSH_TASK_BITMASK;
      func = TOSH_queue[(int )old_full].tp;
      TOSH_queue[(int )old_full].tp = 0;
      __nesc_atomic_end(fInterruptFlags);
      func();
      return 1;
    }
*/
}

static inline void TOSH_run_task(void)
#line 162
{
  while (TOSH_run_next_task()) 
    ;
  TOSH_sleep();
  TOSH_wait();
}

static inline 
# 207 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
bool TinySecM__interruptDisable(void)
#line 207
{
  bool result = (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) & 0x80) != 0;

#line 209
   __asm volatile ("cli");
  return result;
}

static inline   
# 230 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCMAC.nc"
result_t CBCMAC__MAC__MAC(MACContext *context, uint8_t *msg, 
uint16_t length, 
uint8_t *res, uint8_t macSize)
{

  if (CBCMAC__MAC__initIncrementalMAC(context, length) != SUCCESS) {
    return FAIL;
    }
#line 237
  if (CBCMAC__MAC__incrementalMAC(context, msg, length) != SUCCESS) {
    return FAIL;
    }
#line 239
  return CBCMAC__MAC__getIncrementalMAC(context, res, macSize);
}

# 127 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
inline static   result_t TinySecM__MAC__MAC(MACContext *arg_0xa3f4060, uint8_t *arg_0xa3f41c0, uint16_t arg_0xa3f4310, uint8_t *arg_0xa3f4480, uint8_t arg_0xa3f45c8){
#line 127
  unsigned char result;
#line 127

#line 127
  result = CBCMAC__MAC__MAC(arg_0xa3f4060, arg_0xa3f41c0, arg_0xa3f4310, arg_0xa3f4480, arg_0xa3f45c8);
#line 127

#line 127
  return result;
#line 127
}
#line 127
static inline 
# 681 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__computeMAC(void)
#line 681
{
  result_t result = TinySecM__MAC__MAC(&TinySecM__macContext, 
  (uint8_t *)& TinySecM__ciphertext_send_ptr->addr, 
  TinySecM__txlength, 
  TinySecM__ciphertext_send_ptr->calc_mac, 
  4 + 1);



  nmemcpy(TinySecM__ciphertext_send_ptr->mac, TinySecM__ciphertext_send_ptr->calc_mac, 
  4);
  {
  }
#line 692
  ;




  return result;
}

static inline 
# 147 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
void *nmemset(void *to, int val, size_t n)
{
  char *cto = to;

  while (n--) * cto++ = val;

  return to;
}

static inline 
# 764 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__noEncrypt(void)
#line 764
{

  nmemset(TinySecM__ciphertext_send_ptr->iv, 0, 4);
  nmemcpy(TinySecM__ciphertext_send_ptr->enc, TinySecM__cleartext_send_ptr->data, TinySecM__sendDataLength);
  return SUCCESS;
}

# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherMode.nc"
inline static   result_t TinySecM__BlockCipherMode__encrypt(CipherModeContext *arg_0xa3e4fe0, uint8_t *arg_0xa3e5158, uint8_t *arg_0xa3e52c0, uint16_t arg_0xa3e5428, uint8_t *arg_0xa3e5588){
#line 88
  unsigned char result;
#line 88

#line 88
  result = CBCModeM__BlockCipherMode__encrypt(arg_0xa3e4fe0, arg_0xa3e5158, arg_0xa3e52c0, arg_0xa3e5428, arg_0xa3e5588);
#line 88

#line 88
  return result;
#line 88
}
#line 88
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   uint16_t TinySecM__Random__rand(void){
#line 63
  unsigned int result;
#line 63

#line 63
  result = RandomLFSR__Random__rand();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline 
# 701 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__addPadding(TOS_Msg_TinySecCompat *bufptr, uint16_t dataLength)
#line 701
{
  uint16_t r = TinySecM__Random__rand();
  uint8_t i = 0;

#line 704
  for (i = dataLength; i < TinySecM__blockSize - 1; i = i + 2) {
      nmemcpy(bufptr->data + i, &r, 2);
      r = TinySecM__Random__rand();
    }
  if (i == TinySecM__blockSize - 1) {
      nmemcpy(bufptr->data + i, &r, 1);
    }
  return SUCCESS;
}

static inline result_t TinySecM__encrypt(void)
#line 714
{

  uint16_t ivLengthRemaining = sizeof  TinySecM__ciphertext_send_ptr->addr + 
  sizeof  TinySecM__ciphertext_send_ptr->type + sizeof  TinySecM__ciphertext_send_ptr->length;
  result_t result;
  uint8_t i;

  if (ivLengthRemaining > TinySecM__blockSize - 4) {
    ivLengthRemaining = TinySecM__blockSize - 4;
    }

  nmemcpy(& TinySecM__ciphertext_send_ptr->iv, TinySecM__iv, 4);

  nmemcpy(TinySecM__iv + 4, & TinySecM__ciphertext_send_ptr->addr, ivLengthRemaining);


  for (i = ivLengthRemaining + 4; i < TinySecM__blockSize; i++) {
      TinySecM__iv[i] = 0;
    }

  if (TinySecM__sendDataLength < TinySecM__blockSize) {

      TinySecM__addPadding(TinySecM__cleartext_send_ptr, TinySecM__sendDataLength);
      result = TinySecM__BlockCipherMode__encrypt(&TinySecM__cipherModeContext, 
      TinySecM__cleartext_send_ptr->data, 
      TinySecM__ciphertext_send_ptr->enc, 
      TinySecM__blockSize, TinySecM__iv);
    }
  else 
#line 741
    {
      result = TinySecM__BlockCipherMode__encrypt(&TinySecM__cipherModeContext, 
      TinySecM__cleartext_send_ptr->data, 
      TinySecM__ciphertext_send_ptr->enc, 
      TinySecM__sendDataLength, TinySecM__iv);
    }

  i = 0;


  while (i < 4 - TINYSEC_NODE_ID_SIZE) {
      if (TinySecM__iv[i] == 0xff) {
          TinySecM__iv[i] = 0;
        }
      else 
#line 754
        {
          TinySecM__iv[i] = TinySecM__iv[i] + 1;
          break;
        }
      i++;
    }
  return result;
}

static inline 
#line 213
result_t TinySecM__interruptEnable(void)
#line 213
{
   __asm volatile ("sei");
  return SUCCESS;
}

static inline   
#line 771
result_t TinySecM__TinySec__send(void)
#line 771
{
  result_t r1;
#line 772
  result_t r2;

  TinySecM__interruptEnable();
  if (TinySecM__txencrypt) {
    r1 = TinySecM__encrypt();
    }
  else {
#line 778
    r1 = TinySecM__noEncrypt();
    }
#line 779
  r2 = TinySecM__computeMAC();
  TinySecM__interruptDisable();

  return rcombine(r1, r2);
}

# 135 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__TinySec__send(void){
#line 135
  unsigned char result;
#line 135

#line 135
  result = TinySecM__TinySec__send();
#line 135

#line 135
  return result;
#line 135
}
#line 135
static inline    
# 590 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__startSymbol(void)
#line 590
{
}

# 45 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__startSymbol(void){
#line 45
  MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__startSymbol();
#line 45
}
#line 45
static inline 
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

static inline   
# 55 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC__RadioTiming__currentTime(void)
#line 55
{
  return ({
#line 56
    uint16_t __t;
#line 56
    bool bStatus;

#line 56
    bStatus = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) & (1 << 7);
#line 56
     __asm volatile ("cli");__t = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x2C + 0x20);
#line 56
    if (bStatus) {
#line 56
       __asm volatile ("sei");
      }
#line 56
    __t;
  }
  );
}

# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
inline static   uint16_t MicaHighSpeedRadioTinySecM__RadioTiming__currentTime(void){
#line 34
  unsigned int result;
#line 34

#line 34
  result = RadioTimingC__RadioTiming__currentTime();
#line 34

#line 34
  return result;
#line 34
}
#line 34
# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__send(uint8_t arg_0xa38eb28){
#line 33
  unsigned char result;
#line 33
/*BLAST recursion
#line 33
  result = SpiByteFifoC__SpiByteFifo__send(arg_0xa38eb28);
#line 33
*/
#line 33
  return result;
#line 33
}
#line 33
static inline   
# 642 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
uint16_t TinySecM__TinySec__sendInit(TOS_Msg_TinySecCompat *cleartext_ptr)
#line 642
{
  TinySecM__cleartext_send_ptr = cleartext_ptr;
  TinySecM__ciphertext_send_ptr = &TinySecM__tinysec_send_buffer;

  TinySecM__ciphertext_send_ptr->addr = TinySecM__cleartext_send_ptr->addr;
  TinySecM__ciphertext_send_ptr->length = TinySecM__cleartext_send_ptr->length;
  TinySecM__ciphertext_send_ptr->type = TinySecM__cleartext_send_ptr->type;

  TinySecM__sendDataLength = TinySecM__cleartext_send_ptr->length & (
  TINYSEC_ENCRYPT_ENABLED_BIT - 1);
  {
  }
#line 652
  ;
  {
  }
#line 653
  ;



  if (TinySecM__sendDataLength > DATA_LENGTH) {
      TinySecM__sendDataLength = DATA_LENGTH;
      TinySecM__ciphertext_send_ptr->length = (DATA_LENGTH | TINYSEC_ENABLED_BIT) | (
      TinySecM__cleartext_send_ptr->length & TINYSEC_ENCRYPT_ENABLED_BIT);
    }

  if (TinySecM__cleartext_send_ptr->length & TINYSEC_ENCRYPT_ENABLED_BIT) {
    TinySecM__txencrypt = TRUE;
    }
  else {
#line 666
    TinySecM__txencrypt = FALSE;
    }
  if (TinySecM__sendDataLength < TinySecM__blockSize && TinySecM__txencrypt) {
      TinySecM__txlength = TinySecM__blockSize + TINYSEC_MSG_DATA_SIZE - DATA_LENGTH - 
      4;
    }
  else 
#line 671
    {
      TinySecM__txlength = TinySecM__sendDataLength + TINYSEC_MSG_DATA_SIZE - DATA_LENGTH - 
      4;
    }

  TinySecM__TxByteCnt = -1;

  return TinySecM__txlength;
}

# 117 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   uint16_t MicaHighSpeedRadioTinySecM__TinySec__sendInit(TOS_Msg_TinySecCompat *arg_0xa3c9800){
#line 117
  unsigned int result;
#line 117

#line 117
  result = TinySecM__TinySec__sendInit(arg_0xa3c9800);
#line 117

#line 117
  return result;
#line 117
}
#line 117
# 34 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__Code__encode(char arg_0xa320a98){
#line 34
  unsigned char result;
#line 34

#line 34
  result = SecDedEncoding__Code__encode(arg_0xa320a98);
#line 34

#line 34
  return result;
#line 34
}
#line 34
static inline   
# 314 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__ChannelMon__idleDetect(void)
#line 314
{
  uint8_t firstSSByte;
  uint8_t firstMsgByte;
  uint16_t timeVal;
  uint16_t tmpCrc;
  uint8_t oldSendState;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 321
    {
      oldSendState = MicaHighSpeedRadioTinySecM__send_state;
      if (MicaHighSpeedRadioTinySecM__send_state == MicaHighSpeedRadioTinySecM__SEND_WAITING) {
          MicaHighSpeedRadioTinySecM__send_state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
          MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__TRANSMITTING_START;
        }
    }
#line 327
    __nesc_atomic_end(__nesc_atomic); }

  if (oldSendState == MicaHighSpeedRadioTinySecM__SEND_WAITING) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 330
        {
          firstMsgByte = ((char *)MicaHighSpeedRadioTinySecM__send_ptr)[0];
          firstSSByte = TOSH_MHSR_start[0];

          MicaHighSpeedRadioTinySecM__buf_end = MicaHighSpeedRadioTinySecM__buf_head = 0;
          MicaHighSpeedRadioTinySecM__enc_count = 0;
          MicaHighSpeedRadioTinySecM__Code__encode(firstMsgByte);
          MicaHighSpeedRadioTinySecM__rx_count = 0;

          if (MicaHighSpeedRadioTinySecM__send_ptr->length & TINYSEC_ENABLED_BIT) {
              MicaHighSpeedRadioTinySecM__msg_length = MicaHighSpeedRadioTinySecM__TinySec__sendInit(MicaHighSpeedRadioTinySecM__send_ptr);
            }
          else 
#line 341
            {
              MicaHighSpeedRadioTinySecM__msg_length = (unsigned char )MicaHighSpeedRadioTinySecM__send_ptr->length + 
              MSG_DATA_SIZE - DATA_LENGTH - 2;
            }
#line 361
          if (MicaHighSpeedRadioTinySecM__send_ptr->type == AM_TIMESYNCMSG) {
              MicaHighSpeedRadioTinySecM__timeSyncFunctionHack();
            }
          else {
#line 364
            if (0) {
                MicaHighSpeedRadioTinySecM__timeSyncFunctionHack();
              }
            }
        }
#line 368
        __nesc_atomic_end(__nesc_atomic); }

      MicaHighSpeedRadioTinySecM__SpiByteFifo__send(firstSSByte);
      timeVal = MicaHighSpeedRadioTinySecM__RadioTiming__currentTime();
      tmpCrc = crcByte(0x00, firstMsgByte);
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 373
        {
          MicaHighSpeedRadioTinySecM__send_ptr->time = timeVal;
          MicaHighSpeedRadioTinySecM__calc_crc = tmpCrc;
        }
#line 376
        __nesc_atomic_end(__nesc_atomic); }
    }
  MicaHighSpeedRadioTinySecM__RadioSendCoordinator__startSymbol();

  if (MicaHighSpeedRadioTinySecM__send_ptr->length & TINYSEC_ENABLED_BIT) {
      MicaHighSpeedRadioTinySecM__TinySec__send();
    }

  return 1;
}

# 39 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t ChannelMonC__ChannelMon__idleDetect(void){
#line 39
  unsigned char result;
#line 39

#line 39
  result = MicaHighSpeedRadioTinySecM__ChannelMon__idleDetect();
#line 39

#line 39
  return result;
#line 39
}
#line 39
# 37 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t SecDedEncoding__Code__encodeDone(char arg_0xa321820){
#line 37
  unsigned char result;
#line 37

#line 37
  result = MicaHighSpeedRadioTinySecM__Code__encodeDone(arg_0xa321820);
#line 37

#line 37
  return result;
#line 37
}
#line 37
static inline 
# 99 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
void SecDedEncoding__radio_encode_thread(void)
#line 99
{
  char ret_high = 0;
  char ret_low = 0;
  char parity = 0;
  char val;

#line 104
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 104
    {
      val = SecDedEncoding__data1;
    }
#line 106
    __nesc_atomic_end(__nesc_atomic); }




  if ((val & 0x1) != 0) {
      parity ^= 0x5b;
      ret_low |= 0x1;
    }
  else 
#line 114
    {
#line 114
      ret_low |= 0x2;
    }
  if ((val & 0x2) != 0) {
      parity ^= 0x58;
      ret_low |= 0x4;
    }
  else 
#line 119
    {
#line 119
      ret_low |= 0x8;
    }
  if ((val & 0x4) != 0) {
      parity ^= 0x52;
      ret_low |= 0x10;
    }
  else 
#line 124
    {
#line 124
      ret_low |= 0x20;
    }
  if ((val & 0x8) != 0) {
      parity ^= 0x51;
      ret_low |= 0x40;
    }
  else 
#line 129
    {
#line 129
      ret_low |= 0x80;
    }
  if ((val & 0x10) != 0) {
      parity ^= 0x4a;
      ret_high |= 0x1;
    }
  else 
#line 134
    {
#line 134
      ret_high |= 0x2;
    }
  if ((val & 0x20) != 0) {
      parity ^= 0x49;
      ret_high |= 0x4;
    }
  else 
#line 139
    {
#line 139
      ret_high |= 0x8;
    }
  if ((val & 0x40) != 0) {
      parity ^= 0x13;
      ret_high |= 0x10;
    }
  else 
#line 144
    {
#line 144
      ret_high |= 0x20;
    }
  if ((val & 0x80) != 0) {
      parity ^= 0x0b;
      ret_high |= 0x40;
    }
  else 
#line 149
    {
#line 149
      ret_high |= 0x80;
    }


  if (!(parity & 0x40)) {
#line 153
    parity |= 0x80;
    }
#line 154
  if (!(parity & 0x50)) {
#line 154
    parity |= 0x20;
    }
#line 155
  if (!(parity & 0xa)) {
#line 155
    parity |= 0x4;
    }
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 157
    {
      SecDedEncoding__state = SecDedEncoding__IDLE_STATE;
    }
#line 159
    __nesc_atomic_end(__nesc_atomic); }
  SecDedEncoding__Code__encodeDone(parity);
  SecDedEncoding__Code__encodeDone(ret_high);
  SecDedEncoding__Code__encodeDone(ret_low);
}

static inline   
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
tos_time_t SimpleTimeM__Time__get(void)
#line 98
{
  tos_time_t t;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    t = SimpleTimeM__time;
#line 101
    __nesc_atomic_end(__nesc_atomic); }
  return t;
}

# 46 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Time.nc"
inline static   tos_time_t MicaHighSpeedRadioTinySecM__Time__get(void){
#line 46
  struct __nesc_unnamed4260 result;
#line 46

#line 46
  result = SimpleTimeM__Time__get();
#line 46

#line 46
  return result;
#line 46
}
#line 46
static inline   
# 94 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
uint16_t SimpleTimeM__Time__getUs(void)
#line 94
{
  return 0;
}

# 57 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Time.nc"
inline static   uint16_t MicaHighSpeedRadioTinySecM__Time__getUs(void){
#line 57
  unsigned int result;
#line 57

#line 57
  result = SimpleTimeM__Time__getUs();
#line 57

#line 57
  return result;
#line 57
}
#line 57
# 46 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySecRadio.nc"
inline static   uint8_t MicaHighSpeedRadioTinySecM__TinySecRadio__getTransmitByte(void){
#line 46
  unsigned char result;
#line 46

#line 46
  result = TinySecM__TinySecRadio__getTransmitByte();
#line 46

#line 46
  return result;
#line 46
}
#line 46
static inline   
# 233 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__TinySec__sendDone(result_t result)
#line 233
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 234
    {
      MicaHighSpeedRadioTinySecM__tx_done = TRUE;
    }
#line 236
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 150 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   result_t TinySecM__TinySec__sendDone(result_t arg_0xa3c6560){
#line 150
  unsigned char result;
#line 150

#line 150
  result = MicaHighSpeedRadioTinySecM__TinySec__sendDone(arg_0xa3c6560);
#line 150

#line 150
  return result;
#line 150
}
#line 150
static inline    
# 591 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__byte(TOS_MsgPtr msg, uint8_t byteCount)
#line 591
{
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioTinySecM__RadioSendCoordinator__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8){
#line 48
  MicaHighSpeedRadioTinySecM__RadioSendCoordinator__default__byte(arg_0xa504a88, arg_0xa504bd8);
#line 48
}
#line 48
static inline   
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
result_t SecDedEncoding__Code__encode_flush(void)
#line 76
{
  return 1;
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__Code__encode_flush(void){
#line 33
  unsigned char result;
#line 33

#line 33
  result = SecDedEncoding__Code__encode_flush();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline   
# 188 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__phaseShift(void)
#line 188
{
  unsigned char f;

#line 190
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 190
    {
      f = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20);
      if (f > 20) {
#line 192
        f -= 20;
        }
#line 193
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = f;
    }
#line 194
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 38 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__phaseShift(void){
#line 38
  unsigned char result;
#line 38

#line 38
  result = SpiByteFifoC__SpiByteFifo__phaseShift();
#line 38

#line 38
  return result;
#line 38
}
#line 38
# 79 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RFM_CTL1_PIN(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_SET_RFM_CTL0_PIN(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 7;
}

#line 77
static __inline void TOSH_MAKE_RFM_TXD_INPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 3);
}

#line 77
static __inline void TOSH_CLR_RFM_TXD_PIN(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 3);
}

static inline   
# 172 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__rxMode(void)
#line 172
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 173
    {
      TOSH_CLR_RFM_TXD_PIN();
      TOSH_MAKE_RFM_TXD_INPUT();
      TOSH_SET_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
    }
#line 178
    __nesc_atomic_end(__nesc_atomic); }






  return SUCCESS;
}

# 37 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__rxMode(void){
#line 37
  unsigned char result;
#line 37

#line 37
  result = SpiByteFifoC__SpiByteFifo__rxMode();
#line 37

#line 37
  return result;
#line 37
}
#line 37
#line 34
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__idle(void){
#line 34
  unsigned char result;
#line 34

#line 34
  result = SpiByteFifoC__SpiByteFifo__idle();
#line 34

#line 34
  return result;
#line 34
}
#line 34
static inline  
# 198 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SlavePin__notifyHigh(void)
#line 198
{
  return SUCCESS;
}

# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static  result_t SlavePinM__SlavePin__notifyHigh(void){
#line 66
  unsigned char result;
#line 66

#line 66
  result = SpiByteFifoC__SlavePin__notifyHigh();
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline  
# 119 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
void SlavePinM__signalHighTask(void)
#line 119
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    SlavePinM__signalHigh = FALSE;
#line 121
    __nesc_atomic_end(__nesc_atomic); }

  SlavePinM__SlavePin__notifyHigh();
}

# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_MAKE_ONE_WIRE_OUTPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 5;
}

static inline   
# 52 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC__SlavePin__high(void)
#line 52
{
  TOSH_MAKE_ONE_WIRE_OUTPUT();
  TOSH_SET_ONE_WIRE_PIN();
  return SUCCESS;
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
inline static   result_t SlavePinM__HPLSlavePin__high(void){
#line 48
  unsigned char result;
#line 48

#line 48
  result = HPLSlavePinC__SlavePin__high();
#line 48

#line 48
  return result;
#line 48
}
#line 48
static 
# 126 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
__inline   result_t SlavePinM__SlavePin__high(bool needEvent)
#line 126
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      SlavePinM__n++;
      if (SlavePinM__n > 0) 
        {
          SlavePinM__HPLSlavePin__high();
          if (needEvent || SlavePinM__signalHigh) {
            TOS_post(SlavePinM__signalHighTask);
            }
        }
      else {
#line 137
        SlavePinM__signalHigh |= needEvent;
        }
    }
#line 139
    __nesc_atomic_end(__nesc_atomic); }
#line 139
  return SUCCESS;
}

# 61 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static   result_t SpiByteFifoC__SlavePin__high(bool arg_0xa3040a8){
#line 61
  unsigned char result;
#line 61

#line 61
  result = SlavePinM__SlavePin__high(arg_0xa3040a8);
#line 61

#line 61
  return result;
#line 61
}
#line 61
# 79 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RFM_CTL1_PIN(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 6);
}

#line 58
static __inline void TOSH_CLR_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 0);
}

static inline   
# 127 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__yellowOn(void)
#line 127
{
  {
  }
#line 128
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 129
    {
      TOSH_CLR_YELLOW_LED_PIN();
      LedsC__ledsOn |= LedsC__YELLOW_BIT;
    }
#line 132
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__yellowOff(void)
#line 136
{
  {
  }
#line 137
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 138
    {
      TOSH_SET_YELLOW_LED_PIN();
      LedsC__ledsOn &= ~LedsC__YELLOW_BIT;
    }
#line 141
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__yellowToggle(void)
#line 145
{
  result_t rval;

#line 147
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 147
    {
      if (LedsC__ledsOn & LedsC__YELLOW_BIT) {
        rval = LedsC__Leds__yellowOff();
        }
      else {
#line 151
        rval = LedsC__Leds__yellowOn();
        }
    }
#line 153
    __nesc_atomic_end(__nesc_atomic); }
#line 153
  return rval;
}

# 131 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SecureTOSBaseM__Leds__yellowToggle(void){
#line 131
  unsigned char result;
#line 131

#line 131
  result = LedsC__Leds__yellowToggle();
#line 131

#line 131
  return result;
#line 131
}
#line 131
static inline  
# 328 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__TokenReceiveMsg__ReflectToken(uint8_t Token)
#line 328
{
  result_t Result = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 331
    {
      if (!(FramerM__gFlags & FramerM__FLAGS_TOKENPEND)) {
          FramerM__gFlags |= FramerM__FLAGS_TOKENPEND;
          FramerM__gTxTokenBuf = Token;
        }
      else {
          Result = FAIL;
        }
    }
#line 339
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM__StartTx();
    }

  return Result;
}

# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  result_t SecureTOSBaseM__UARTTokenReceive__ReflectToken(uint8_t arg_0xa2c8d30){
#line 88
  unsigned char result;
#line 88

#line 88
  result = FramerM__TokenReceiveMsg__ReflectToken(arg_0xa2c8d30);
#line 88

#line 88
  return result;
#line 88
}
#line 88
static inline  
# 122 "SecureTOSBaseM.nc"
void SecureTOSBaseM__SendAckTask(void)
#line 122
{
  SecureTOSBaseM__UARTTokenReceive__ReflectToken(SecureTOSBaseM__gTxPendingToken);
  SecureTOSBaseM__Leds__yellowToggle();
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 125
    {
      SecureTOSBaseM__gpTxMsg->length = 0;
      SecureTOSBaseM__gfTxFlags = 0;
    }
#line 128
    __nesc_atomic_end(__nesc_atomic); }
}

static inline  
#line 271
result_t SecureTOSBaseM__RadioSend__sendDone(TOS_MsgPtr Msg, result_t success)
#line 271
{


  if (SecureTOSBaseM__gfTxFlags & SecureTOSBaseM__TXFLAG_TOKEN) {
      if (success == SUCCESS) {

          TOS_post(SecureTOSBaseM__SendAckTask);
        }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 281
        {
          SecureTOSBaseM__gpTxMsg->length = 0;
          SecureTOSBaseM__gfTxFlags = 0;
        }
#line 284
        __nesc_atomic_end(__nesc_atomic); }
    }
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t RadioPacketTinySecM__Send__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0){
#line 67
  unsigned char result;
#line 67

#line 67
  result = SecureTOSBaseM__RadioSend__sendDone(arg_0xa2c3960, arg_0xa2c3ab0);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 58 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioPacketTinySecM.nc"
result_t RadioPacketTinySecM__RadioSend__sendDone(TOS_MsgPtr msg, result_t success)
#line 58
{
  msg->length = msg->length & ~(TINYSEC_ENABLED_BIT | 
  TINYSEC_ENCRYPT_ENABLED_BIT);
  return RadioPacketTinySecM__Send__sendDone(msg, success);
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t MicaHighSpeedRadioTinySecM__Send__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0){
#line 67
  unsigned char result;
#line 67

#line 67
  result = RadioPacketTinySecM__RadioSend__sendDone(arg_0xa2c3960, arg_0xa2c3ab0);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 141 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__packetSent(void)
#line 141
{
  TOS_MsgPtr ptr;

#line 143
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 143
    {
      MicaHighSpeedRadioTinySecM__send_state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;

      MicaHighSpeedRadioTinySecM__swapLengthAndGroup((TOS_Msg *)MicaHighSpeedRadioTinySecM__send_ptr);

      ptr = (TOS_MsgPtr )MicaHighSpeedRadioTinySecM__send_ptr;
    }
#line 150
    __nesc_atomic_end(__nesc_atomic); }
  MicaHighSpeedRadioTinySecM__ChannelMon__startSymbolSearch();
  MicaHighSpeedRadioTinySecM__Send__sendDone(ptr, SUCCESS);
}

static inline   
# 81 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
result_t HPLUARTM__UART__put(uint8_t data)
#line 81
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) |= 1 << 6;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20) = data;
  return SUCCESS;
}

# 80 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM__HPLUART__put(uint8_t arg_0xa650cb8){
#line 80
  unsigned char result;
#line 80

#line 80
  result = HPLUARTM__UART__put(arg_0xa650cb8);
#line 80

#line 80
  return result;
#line 80
}
#line 80
static inline  
# 266 "SecureTOSBaseM.nc"
result_t SecureTOSBaseM__UARTSend__sendDone(TOS_MsgPtr Msg, result_t success)
#line 266
{
  Msg->length = 0;
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t FramerM__BareSendMsg__sendDone(TOS_MsgPtr arg_0xa2c3960, result_t arg_0xa2c3ab0){
#line 67
  unsigned char result;
#line 67

#line 67
  result = SecureTOSBaseM__UARTSend__sendDone(arg_0xa2c3960, arg_0xa2c3ab0);
#line 67

#line 67
  return result;
#line 67
}
#line 67
# 61 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySecRadio.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__TinySecRadio__byteReceived(uint8_t arg_0xa3d4178){
#line 61
  unsigned char result;
#line 61

#line 61
  result = TinySecM__TinySecRadio__byteReceived(arg_0xa3d4178);
#line 61

#line 61
  return result;
#line 61
}
#line 61
static inline    
# 593 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__byte(TOS_MsgPtr msg, uint8_t byteCount)
#line 593
{
}

# 48 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__byte(TOS_MsgPtr arg_0xa504a88, uint8_t arg_0xa504bd8){
#line 48
  MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__byte(arg_0xa504a88, arg_0xa504bd8);
#line 48
}
#line 48
static inline   
# 387 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__Code__decodeDone(char data, char error)
#line 387
{
  result_t rval = SUCCESS;

#line 389
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 389
    {
      if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__IDLE_STATE) {
          rval = FAIL;
        }
      else {
#line 393
        if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__HEADER_RX_STATE) {
            ((char *)MicaHighSpeedRadioTinySecM__rec_ptr)[(int )MicaHighSpeedRadioTinySecM__rec_count] = data;
            MicaHighSpeedRadioTinySecM__rec_count++;
            MicaHighSpeedRadioTinySecM__calc_crc = crcByte(MicaHighSpeedRadioTinySecM__calc_crc, data);
            MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__byte((TOS_MsgPtr )MicaHighSpeedRadioTinySecM__rec_ptr, 
            (uint8_t )MicaHighSpeedRadioTinySecM__rec_count);
            MicaHighSpeedRadioTinySecM__TinySecRadio__byteReceived(data);
          }
        else {
#line 401
          if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__RX_STATE) {
              ((char *)MicaHighSpeedRadioTinySecM__rec_ptr)[(int )MicaHighSpeedRadioTinySecM__rec_count] = data;
              MicaHighSpeedRadioTinySecM__rec_count++;
              if (MicaHighSpeedRadioTinySecM__rec_count >= MSG_DATA_SIZE) {

                  if (MicaHighSpeedRadioTinySecM__calc_crc == MicaHighSpeedRadioTinySecM__rec_ptr->crc) {
                      MicaHighSpeedRadioTinySecM__rec_ptr->crc = 1;
                      if (MicaHighSpeedRadioTinySecM__rec_ptr->addr == TOS_LOCAL_ADDRESS || 
                      MicaHighSpeedRadioTinySecM__rec_ptr->addr == TOS_BCAST_ADDR) {
                          MicaHighSpeedRadioTinySecM__SpiByteFifo__send(0x55);
                        }
                    }
                  else 
#line 412
                    {
                      MicaHighSpeedRadioTinySecM__rec_ptr->crc = 0;
                    }
                  MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__ACK_SEND_STATE;
                  rval = 0;
                  MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__byte((TOS_MsgPtr )MicaHighSpeedRadioTinySecM__rec_ptr, 
                  (uint8_t )MicaHighSpeedRadioTinySecM__rec_count);
                }
              else {
#line 420
                if (MicaHighSpeedRadioTinySecM__rec_count <= MSG_DATA_SIZE - 2) {
                    MicaHighSpeedRadioTinySecM__calc_crc = crcByte(MicaHighSpeedRadioTinySecM__calc_crc, data);
                    if (MicaHighSpeedRadioTinySecM__rec_count == MicaHighSpeedRadioTinySecM__msg_length) {
                        MicaHighSpeedRadioTinySecM__rec_count = MSG_DATA_SIZE - 2;
                      }
                  }
                }
            }
          else {
#line 426
            if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__RX_STATE_TINYSEC) {
                uint8_t rec_count_save = ++MicaHighSpeedRadioTinySecM__rec_count;

#line 428
                MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__byte((TOS_MsgPtr )MicaHighSpeedRadioTinySecM__rec_ptr, 
                (uint8_t )MicaHighSpeedRadioTinySecM__rec_count);
                MicaHighSpeedRadioTinySecM__TinySecRadio__byteReceived(data);
                if (rec_count_save == MicaHighSpeedRadioTinySecM__msg_length + 4) {
                    if (MicaHighSpeedRadioTinySecM__rec_ptr->crc == 1 && (
                    MicaHighSpeedRadioTinySecM__rec_ptr->addr == TOS_LOCAL_ADDRESS || 
                    MicaHighSpeedRadioTinySecM__rec_ptr->addr == TOS_BCAST_ADDR)) {
                        MicaHighSpeedRadioTinySecM__SpiByteFifo__send(0x55);
                      }
                    MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__ACK_SEND_STATE;
                  }
              }
            }
          }
        }
    }
#line 443
    __nesc_atomic_end(__nesc_atomic); }
#line 441
  return rval;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t SecDedEncoding__Code__decodeDone(char arg_0xa3212c0, char arg_0xa321400){
#line 36
  unsigned char result;
#line 36

#line 36
  result = MicaHighSpeedRadioTinySecM__Code__decodeDone(arg_0xa3212c0, arg_0xa321400);
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline 
# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
void SecDedEncoding__radio_decode_thread(void)
#line 167
{


  char ret_high = 0;
  char ret_low = 0;
  char parity;
  char error = 0;
  short encoded_value = 0;

#line 175
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 175
    {
      parity = SecDedEncoding__data1;
      ret_high = SecDedEncoding__data2;
      ret_low = SecDedEncoding__data3;
    }
#line 179
    __nesc_atomic_end(__nesc_atomic); }

  if ((ret_low & 0x1) != 0) {
#line 181
    encoded_value |= 0x1;
    }
#line 182
  if ((ret_low & 0x4) != 0) {
#line 182
    encoded_value |= 0x2;
    }
#line 183
  if ((ret_low & 0x10) != 0) {
#line 183
    encoded_value |= 0x4;
    }
#line 184
  if ((ret_low & 0x40) != 0) {
#line 184
    encoded_value |= 0x8;
    }
#line 185
  if ((ret_high & 0x01) != 0) {
#line 185
    encoded_value |= 0x10;
    }
#line 186
  if ((ret_high & 0x04) != 0) {
#line 186
    encoded_value |= 0x20;
    }
#line 187
  if ((ret_high & 0x10) != 0) {
#line 187
    encoded_value |= 0x40;
    }
#line 188
  if ((ret_high & 0x40) != 0) {
#line 188
    encoded_value |= 0x80;
    }
  parity = ((parity & 0x3) | ((parity & 0x18) >> 1)) | ((parity & 0x40) >> 2);
  encoded_value = (encoded_value << 5) | parity;


  parity = 0;
  if ((encoded_value & 0x1) != 0) {
#line 195
    parity ^= 0x1;
    }
#line 196
  if ((encoded_value & 0x2) != 0) {
#line 196
    parity ^= 0x2;
    }
#line 197
  if ((encoded_value & 0x4) != 0) {
#line 197
    parity ^= 0x4;
    }
#line 198
  if ((encoded_value & 0x8) != 0) {
#line 198
    parity ^= 0x8;
    }
#line 199
  if ((encoded_value & 0x10) != 0) {
#line 199
    parity ^= 0x10;
    }
#line 200
  if ((encoded_value & 0x20) != 0) {
#line 200
    parity ^= 0x1f;
    }
#line 201
  if ((encoded_value & 0x40) != 0) {
#line 201
    parity ^= 0x1c;
    }
#line 202
  if ((encoded_value & 0x80) != 0) {
#line 202
    parity ^= 0x1a;
    }
#line 203
  if ((encoded_value & 0x100) != 0) {
#line 203
    parity ^= 0x19;
    }
#line 204
  if ((encoded_value & 0x200) != 0) {
#line 204
    parity ^= 0x16;
    }
#line 205
  if ((encoded_value & 0x400) != 0) {
#line 205
    parity ^= 0x15;
    }
#line 206
  if ((encoded_value & 0x800) != 0) {
#line 206
    parity ^= 0xb;
    }
#line 207
  if ((encoded_value & 0x1000) != 0) {
#line 207
    parity ^= 0x7;
    }

  error = -1;
  if (parity == 0) {
    }
  else {
#line 212
    if (parity == 0x1) {
#line 212
        encoded_value ^= 0x1;
      }
    else {
#line 213
      if (parity == 0x2) {
#line 213
          encoded_value ^= 0x2;
        }
      else {
#line 214
        if (parity == 0x4) {
#line 214
            encoded_value ^= 0x4;
          }
        else {
#line 215
          if (parity == 0x8) {
#line 215
              encoded_value ^= 0x8;
            }
          else {
#line 216
            if (parity == 0x10) {
#line 216
                encoded_value ^= 0x10;
              }
            else 
#line 217
              {
                error = 0;
                if (parity == 0x1f) {
#line 219
                    encoded_value ^= 0x20;
                  }
                else {
#line 220
                  if (parity == 0x1c) {
#line 220
                      encoded_value ^= 0x40;
                    }
                  else {
#line 221
                    if (parity == 0x1a) {
#line 221
                        encoded_value ^= 0x80;
                      }
                    else {
#line 222
                      if (parity == 0x19) {
#line 222
                          encoded_value ^= 0x100;
                        }
                      else {
#line 223
                        if (parity == 0x16) {
#line 223
                            encoded_value ^= 0x200;
                          }
                        else {
#line 224
                          if (parity == 0x15) {
#line 224
                              encoded_value ^= 0x400;
                            }
                          else {
#line 225
                            if (parity == 0xb) {
#line 225
                                encoded_value ^= 0x800;
                              }
                            else {
#line 226
                              if (parity == 0x7) {
#line 226
                                  encoded_value ^= 0x1000;
                                }
                              else 
#line 227
                                {
                                  error = 1;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
              }
            }
          }
        }
      }
    }
#line 233
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 233
    {
      SecDedEncoding__state = SecDedEncoding__IDLE_STATE;
    }
#line 235
    __nesc_atomic_end(__nesc_atomic); }
  SecDedEncoding__Code__decodeDone((encoded_value >> 5) & 0xff, error);
}

static inline   
#line 55
result_t SecDedEncoding__Code__decode(char d1)
#line 55
{
  result_t rval = 1;

#line 57
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 57
    {
      if (SecDedEncoding__state == SecDedEncoding__IDLE_STATE) {
          SecDedEncoding__state = SecDedEncoding__DECODING_BYTE_1;
          SecDedEncoding__data1 = d1;
        }
      else {
#line 61
        if (SecDedEncoding__state == SecDedEncoding__DECODING_BYTE_1) {
            SecDedEncoding__state = SecDedEncoding__DECODING_BYTE_2;
            SecDedEncoding__data2 = d1;
          }
        else {
#line 64
          if (SecDedEncoding__state == SecDedEncoding__DECODING_BYTE_2) {
              SecDedEncoding__state = SecDedEncoding__DECODING_BYTE_3;
              SecDedEncoding__data3 = d1;
              SecDedEncoding__radio_decode_thread();
            }
          else 
#line 68
            {
              rval = 0;
            }
          }
        }
    }
#line 73
    __nesc_atomic_end(__nesc_atomic); }
#line 72
  return rval;
}

# 35 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioEncoding.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__Code__decode(char arg_0xa320ea8){
#line 35
  unsigned char result;
#line 35

#line 35
  result = SecDedEncoding__Code__decode(arg_0xa320ea8);
#line 35

#line 35
  return result;
#line 35
}
#line 35
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   result_t TinySecM__TinySec__receiveInitDone(result_t arg_0xa3c87f8, uint16_t arg_0xa3c8950, bool arg_0xa3c8aa0){
#line 78
  unsigned char result;
#line 78

#line 78
  result = MicaHighSpeedRadioTinySecM__TinySec__receiveInitDone(arg_0xa3c87f8, arg_0xa3c8950, arg_0xa3c8aa0);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 218 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__postIncrementalMACInit(void)
#line 218
{
  TinySecM__computeMACBuffer.computeMACInitWaiting = TRUE;
  return SUCCESS;
}

static inline 
#line 239
result_t TinySecM__postIncrementalDecryptInit(void)
#line 239
{
  TinySecM__decryptBuffer.decryptInitWaiting = TRUE;
  return SUCCESS;
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
inline static   result_t TinySecM__MAC__initIncrementalMAC(MACContext *arg_0xa3f6330, uint16_t arg_0xa3f6480){
#line 73
  unsigned char result;
#line 73

#line 73
  result = CBCMAC__MAC__initIncrementalMAC(arg_0xa3f6330, arg_0xa3f6480);
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 384 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__MACincrementalInit(void)
#line 384
{
  result_t result;

#line 386
  if (TinySecM__compute_mac_state != TinySecM__COMPUTE_MAC_IDLE || !TinySecM__initialized) {
    return FAIL;
    }
  TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_BUSY;
  TinySecM__interruptEnable();
  result = TinySecM__MAC__initIncrementalMAC(&TinySecM__macContext, TinySecM__rxlength);
  {
  }
#line 392
  ;
  TinySecM__interruptDisable();
  TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_INITIALIZED;
  result = rcombine(result, TinySecM__checkQueuedCrypto());
  return result;
}

# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
inline static   result_t CBCMAC__BlockCipher__encrypt(CipherContext *arg_0xa4567a8, uint8_t *arg_0xa456920, uint8_t *arg_0xa456a88){
#line 74
  unsigned char result;
#line 74

#line 74
  result = SkipJackM__BlockCipher__encrypt(arg_0xa4567a8, arg_0xa456920, arg_0xa456a88);
#line 74

#line 74
  return result;
#line 74
}
#line 74
static inline 
# 162 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
void SkipJackM__dumpBuffer(char *bufName, uint8_t *buf, uint8_t size)
{
}

# 89 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
inline static   result_t TinySecM__MAC__incrementalMAC(MACContext *arg_0xa3f6b20, uint8_t *arg_0xa3f6c80, uint16_t arg_0xa3f6de0){
#line 89
  unsigned char result;
#line 89

#line 89
  result = CBCMAC__MAC__incrementalMAC(arg_0xa3f6b20, arg_0xa3f6c80, arg_0xa3f6de0);
#line 89

#line 89
  return result;
#line 89
}
#line 89
static inline 
# 400 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__computeMACIncremental(uint8_t incr_mac_start, uint8_t amount)
#line 400
{
  if (!TinySecM__initialized) {
    return FAIL;
    }
#line 403
  if (incr_mac_start >= TINYSEC_MSG_DATA_SIZE - 4) {
    return FAIL;
    }
  if (TinySecM__compute_mac_state == TinySecM__COMPUTE_MAC_IDLE) {
      return FAIL;
    }
  else {
#line 408
    if (TinySecM__compute_mac_state == TinySecM__COMPUTE_MAC_INITIALIZED) {
        result_t result;

#line 410
        TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_BUSY;
        TinySecM__interruptEnable();
        result = TinySecM__MAC__incrementalMAC(
        &TinySecM__macContext, 
        (uint8_t *)TinySecM__ciphertext_rec_ptr + incr_mac_start, amount);
        TinySecM__interruptDisable();
        TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_INITIALIZED;
        result = rcombine(result, TinySecM__checkQueuedCrypto());
        return result;
      }
    else 
#line 419
      {
        return FAIL;
      }
    }
}

# 109 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/MAC.nc"
inline static   result_t TinySecM__MAC__getIncrementalMAC(MACContext *arg_0xa3f7560, uint8_t *arg_0xa3f76c0, uint8_t arg_0xa3f7818){
#line 109
  unsigned char result;
#line 109

#line 109
  result = CBCMAC__MAC__getIncrementalMAC(arg_0xa3f7560, arg_0xa3f76c0, arg_0xa3f7818);
#line 109

#line 109
  return result;
#line 109
}
#line 109
static inline 
# 424 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__computeMACIncrementalFinish(void)
#line 424
{
  if (!TinySecM__initialized) {
    return FAIL;
    }
  if (TinySecM__compute_mac_state == TinySecM__COMPUTE_MAC_IDLE) {

      return FAIL;
    }
  else {
#line 431
    if (TinySecM__compute_mac_state == TinySecM__COMPUTE_MAC_INITIALIZED) {
        result_t result;

#line 433
        TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_BUSY;
        TinySecM__interruptEnable();
        result = TinySecM__MAC__getIncrementalMAC(
        &TinySecM__macContext, 
        TinySecM__ciphertext_rec_ptr->calc_mac, 
        4 + 1);
        TinySecM__interruptDisable();
        TinySecM__ciphertext_rec_ptr->validMAC = TRUE;
        if (TinySecM__ciphertext_rec_ptr->receiveDone) {
            TinySecM__verifyMAC();
          }
        TinySecM__compute_mac_state = TinySecM__COMPUTE_MAC_IDLE;
        result = rcombine(result, TinySecM__checkQueuedCrypto());
        return result;
      }
    else 
#line 447
      {
        return FAIL;
      }
    }
}

# 138 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherMode.nc"
inline static   result_t TinySecM__BlockCipherMode__initIncrementalDecrypt(CipherModeContext *arg_0xa3e2d70, uint8_t *arg_0xa3e2ee0, uint16_t arg_0xa3e3040){
#line 138
  unsigned char result;
#line 138

#line 138
  result = CBCModeM__BlockCipherMode__initIncrementalDecrypt(arg_0xa3e2d70, arg_0xa3e2ee0, arg_0xa3e3040);
#line 138

#line 138
  return result;
#line 138
}
#line 138
# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipher.nc"
inline static   result_t CBCModeM__BlockCipher__encrypt(CipherContext *arg_0xa4567a8, uint8_t *arg_0xa456920, uint8_t *arg_0xa456a88){
#line 74
  unsigned char result;
#line 74

#line 74
  result = SkipJackM__BlockCipher__encrypt(arg_0xa4567a8, arg_0xa456920, arg_0xa456a88);
#line 74

#line 74
  return result;
#line 74
}
#line 74
#line 90
inline static   result_t CBCModeM__BlockCipher__decrypt(CipherContext *arg_0xa4570b8, uint8_t *arg_0xa457230, uint8_t *arg_0xa457398){
#line 90
  unsigned char result;
#line 90

#line 90
  result = SkipJackM__BlockCipher__decrypt(arg_0xa4570b8, arg_0xa457230, arg_0xa457398);
#line 90

#line 90
  return result;
#line 90
}
#line 90
static inline 
# 139 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCModeM.nc"
void CBCModeM__dumpBuffer(char *bufName, uint8_t *buf, uint8_t size)
{
}

static inline   
#line 494
result_t CBCModeM__BlockCipherMode__incrementalDecrypt(
CipherModeContext *context, 
uint8_t *cipher, 
uint8_t *plain, 
uint16_t length, 
uint16_t *done)
{
  CBCModeM__CBCModeContext *mcontext = (CBCModeM__CBCModeContext *)context->context;
  int i;
#line 502
  int j;
  uint8_t *accum;
  uint8_t *lastCipher;
  uint8_t bsize = mcontext->bsize;
  uint16_t completed = mcontext->completed;






  {
  }
#line 513
  ;

  if (!length) {
#line 515
      *done = mcontext->completed;
#line 515
      return SUCCESS;
    }
#line 516
  if (length > mcontext->remaining) {
      {
      }
#line 517
      ;
#line 517
      return FAIL;
    }
  while (length) {



      if (mcontext->accum) {
          accum = mcontext->spill1;
          lastCipher = mcontext->spill2;
        }
      else 
#line 526
        {
          accum = mcontext->spill2;
          lastCipher = mcontext->spill1;
        }



      if (mcontext->state != CBCModeM__TWO_LEFT_B) {
          if (mcontext->offset + length < bsize) {


              {
              }
#line 537
              ;

              nmemcpy(accum + mcontext->offset, cipher, length);
              *done = mcontext->completed = completed;
              mcontext->offset += length;
              mcontext->remaining -= length;
              return SUCCESS;
            }


          j = bsize - mcontext->offset;
          nmemcpy(accum + mcontext->offset, cipher, j);

          {
          }
#line 550
          ;

          mcontext->remaining -= j;
          cipher += j;
          length -= j;
          mcontext->offset = 0;
        }



      if (mcontext->state == CBCModeM__ONE_BLOCK) {

          {
          }
#line 562
          ;

          if (
#line 563
          CBCModeM__BlockCipher__decrypt(& context->cc, accum, 
          plain) != SUCCESS) {
              return FAIL;
            }

          for (i = 0; i < bsize; i++) {
              plain[i] ^= lastCipher[i];
            }
          CBCModeM__dumpBuffer("plain", plain, 8);

          *done = mcontext->completed = bsize;
          return SUCCESS;
        }

      if (mcontext->state == CBCModeM__GENERAL) {

          {
          }
#line 579
          ;


          if (
#line 581
          CBCModeM__BlockCipher__decrypt(& context->cc, accum, 
          plain + completed) != SUCCESS) {
              {
              }
#line 583
              ;
              return FAIL;
            }

          for (i = 0; i < bsize; i++) {
              plain[i + completed] ^= lastCipher[i];
            }

          completed += bsize;
          mcontext->accum = ! mcontext->accum;

          if (mcontext->remaining <= bsize * 2) {
              mcontext->state = CBCModeM__TWO_LEFT_A;
              continue;
            }
        }

      if (mcontext->state == CBCModeM__TWO_LEFT_A) {


          {
          }
#line 603
          ;




          if (CBCModeM__BlockCipher__decrypt(& context->cc, accum, accum) != 
          SUCCESS) {
              {
              }
#line 610
              ;
              return FAIL;
            }

          mcontext->state = CBCModeM__TWO_LEFT_B;
          {
          }
#line 615
          ;

          continue;
        }

      if (mcontext->state == CBCModeM__TWO_LEFT_B) {





          {
          }
#line 626
          ;
          j = mcontext->offset + length;

          for (i = mcontext->offset; i < j; i++) {
              {
              }
#line 630
              ;

              plain[completed + bsize + i] = 
              accum[i] ^ cipher[i - mcontext->offset];

              accum[i] = cipher[i - mcontext->offset];
            }
          mcontext->remaining -= length;
          if (mcontext->remaining) {
              mcontext->offset += length;
              length = 0;
            }
          else 
#line 641
            {




              if (
#line 645
              CBCModeM__BlockCipher__decrypt(& context->cc, accum, 
              plain + completed) != SUCCESS) {
                  {
                  }
#line 647
                  ;
                  return FAIL;
                }
              for (i = 0; i < bsize; i++) {
                  plain[i + completed] ^= lastCipher[i];
                }

              mcontext->remaining = 0;
              mcontext->completed += 
              bsize + length + mcontext->offset;
              *done = mcontext->completed;
              return SUCCESS;
            }
        }
    }

  *done = mcontext->completed = completed;
  return SUCCESS;
}

# 165 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BlockCipherMode.nc"
inline static   result_t TinySecM__BlockCipherMode__incrementalDecrypt(CipherModeContext *arg_0xa3e38c8, uint8_t *arg_0xa3e3a40, uint8_t *arg_0xa3e3bb8, uint16_t arg_0xa3e3d18, uint16_t *arg_0xa3e3e90){
#line 165
  unsigned char result;
#line 165

#line 165
  result = CBCModeM__BlockCipherMode__incrementalDecrypt(arg_0xa3e38c8, arg_0xa3e3a40, arg_0xa3e3bb8, arg_0xa3e3d18, arg_0xa3e3e90);
#line 165

#line 165
  return result;
#line 165
}
#line 165
static inline 
# 346 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__decryptIncremental(uint8_t incr_decrypt_start, uint8_t amount)
#line 346
{
  result_t result;
  uint16_t done;

  if (!TinySecM__initialized) {
    return FAIL;
    }
  if (TinySecM__decrypt_state == TinySecM__DECRYPT_IDLE) {
      return FAIL;
    }
  else {
#line 355
    if (TinySecM__decrypt_state == TinySecM__DECRYPT_INITIALIZED) {
        TinySecM__decrypt_state = TinySecM__DECRYPT_BUSY;
        TinySecM__interruptEnable();
        result = TinySecM__BlockCipherMode__incrementalDecrypt(
        &TinySecM__cipherModeContext, 
        TinySecM__ciphertext_rec_ptr->enc + incr_decrypt_start, 
        TinySecM__cleartext_rec_ptr->data, amount, 
        &done);
        TinySecM__interruptDisable();
        TinySecM__decrypt_state = TinySecM__DECRYPT_INITIALIZED;
        if (TinySecM__recDataLength < TinySecM__blockSize) {
            if (done == TinySecM__blockSize) {
                TinySecM__decrypt_state = TinySecM__DECRYPT_IDLE;
              }
          }
        else 
#line 369
          {
            if (done == TinySecM__recDataLength) {
                TinySecM__decrypt_state = TinySecM__DECRYPT_IDLE;
              }
          }



        result = rcombine(result, TinySecM__checkQueuedCrypto());
        return result;
      }
    else 
#line 379
      {
        return FAIL;
      }
    }
}

static inline 
#line 234
result_t TinySecM__postIncrementalMACFinish(void)
#line 234
{
  TinySecM__computeMACBuffer.finishMACWaiting = TRUE;
  return SUCCESS;
}

static inline   
# 263 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__TinySec__receiveDone(result_t result)
#line 263
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 264
    {
      if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__RX_DONE_STATE) {
          MicaHighSpeedRadioTinySecM__tinysec_rx_done = TRUE;
          TOS_post(MicaHighSpeedRadioTinySecM__packetReceived);
        }
      else 
#line 268
        {
          MicaHighSpeedRadioTinySecM__tinysec_rx_done = TRUE;
        }
    }
#line 271
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   result_t TinySecM__TinySec__receiveDone(result_t arg_0xa3c9190){
#line 96
  unsigned char result;
#line 96

#line 96
  result = MicaHighSpeedRadioTinySecM__TinySec__receiveDone(arg_0xa3c9190);
#line 96

#line 96
  return result;
#line 96
}
#line 96
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

static inline   
# 69 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__redOn(void)
#line 69
{
  {
  }
#line 70
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 71
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC__ledsOn |= LedsC__RED_BIT;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__redOff(void)
#line 78
{
  {
  }
#line 79
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
    {
      TOSH_SET_RED_LED_PIN();
      LedsC__ledsOn &= ~LedsC__RED_BIT;
    }
#line 83
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__redToggle(void)
#line 87
{
  result_t rval;

#line 89
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 89
    {
      if (LedsC__ledsOn & LedsC__RED_BIT) {
        rval = LedsC__Leds__redOff();
        }
      else {
#line 93
        rval = LedsC__Leds__redOn();
        }
    }
#line 95
    __nesc_atomic_end(__nesc_atomic); }
#line 95
  return rval;
}

# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SecureTOSBaseM__Leds__redToggle(void){
#line 81
  unsigned char result;
#line 81

#line 81
  result = LedsC__Leds__redToggle();
#line 81

#line 81
  return result;
#line 81
}
#line 81
static inline  
# 306 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__BareSendMsg__send(TOS_MsgPtr pMsg)
#line 306
{
  result_t Result = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 309
    {
      if (!(FramerM__gFlags & FramerM__FLAGS_DATAPEND)) {
          FramerM__gFlags |= FramerM__FLAGS_DATAPEND;
          FramerM__gpTxMsg = pMsg;
        }
      else 

        {
          Result = FAIL;
        }
    }
#line 319
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM__StartTx();
    }

  return Result;
}

# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t SecureTOSBaseM__UARTSend__send(TOS_MsgPtr arg_0xa2c3448){
#line 58
  unsigned char result;
#line 58

#line 58
  result = FramerM__BareSendMsg__send(arg_0xa2c3448);
#line 58

#line 58
  return result;
#line 58
}
#line 58
static inline  
# 88 "SecureTOSBaseM.nc"
void SecureTOSBaseM__RadioRcvdTask(void)
#line 88
{
  TOS_MsgPtr pMsg;
  result_t Result;

  {
  }
#line 92
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 93
    {
      pMsg = SecureTOSBaseM__gRxBufPoolTbl[SecureTOSBaseM__gRxTailIndex];
      SecureTOSBaseM__gRxTailIndex++;
#line 95
      SecureTOSBaseM__gRxTailIndex %= SecureTOSBaseM__QUEUE_SIZE;
    }
#line 96
    __nesc_atomic_end(__nesc_atomic); }
  Result = SecureTOSBaseM__UARTSend__send(pMsg);
  if (Result != SUCCESS) {
      pMsg->length = 0;
    }
  else {
      SecureTOSBaseM__Leds__redToggle();
    }
}

static inline  
#line 173
TOS_MsgPtr SecureTOSBaseM__RadioReceive__receive(TOS_MsgPtr Msg)
#line 173
{
  TOS_MsgPtr pBuf;

  {
  }
#line 176
  ;

  if (Msg->crc && (
  Msg->receiveSecurityMode == TINYSEC_ENCRYPT_AND_AUTH || 
  Msg->receiveSecurityMode == TINYSEC_AUTH_ONLY)) {


      if (Msg->group != TOS_AM_GROUP) {
        return Msg;
        }
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 186
        {
          pBuf = SecureTOSBaseM__gRxBufPoolTbl[SecureTOSBaseM__gRxHeadIndex];
          if (pBuf->length == 0) {
              SecureTOSBaseM__gRxBufPoolTbl[SecureTOSBaseM__gRxHeadIndex] = Msg;
              SecureTOSBaseM__gRxHeadIndex++;
#line 190
              SecureTOSBaseM__gRxHeadIndex %= SecureTOSBaseM__QUEUE_SIZE;
            }
          else {
              pBuf = (void *)0;
            }
        }
#line 195
        __nesc_atomic_end(__nesc_atomic); }

      if (pBuf) {
          TOS_post(SecureTOSBaseM__RadioRcvdTask);
        }
      else {
          pBuf = Msg;
        }
    }
  else {
      pBuf = Msg;
    }

  return pBuf;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr RadioPacketTinySecM__Receive__receive(TOS_MsgPtr arg_0xa2a2f98){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = SecureTOSBaseM__RadioReceive__receive(arg_0xa2a2f98);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 64 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioPacketTinySecM.nc"
TOS_MsgPtr RadioPacketTinySecM__RadioReceive__receive(TOS_MsgPtr msg)
#line 64
{
  if (msg->length & TINYSEC_ENABLED_BIT) {
      if (msg->length & TINYSEC_ENCRYPT_ENABLED_BIT) {
          msg->receiveSecurityMode = (uint8_t )TINYSEC_ENCRYPT_AND_AUTH;
        }
      else 
#line 68
        {
          msg->receiveSecurityMode = (uint8_t )TINYSEC_AUTH_ONLY;
        }
    }
  else 
#line 71
    {
      msg->receiveSecurityMode = (uint8_t )TINYSEC_DISABLED;
    }
  msg->length = msg->length & ~(TINYSEC_ENABLED_BIT | TINYSEC_ENCRYPT_ENABLED_BIT);
  return RadioPacketTinySecM__Receive__receive(msg);
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr MicaHighSpeedRadioTinySecM__Receive__receive(TOS_MsgPtr arg_0xa2a2f98){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = RadioPacketTinySecM__RadioReceive__receive(arg_0xa2a2f98);
#line 75

#line 75
  return result;
#line 75
}
#line 75
# 78 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RFM_CTL0_PIN(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 7);
}

static inline   
# 164 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__txMode(void)
#line 164
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 165
    {
      TOSH_CLR_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
    }
#line 168
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__txMode(void){
#line 36
  unsigned char result;
#line 36

#line 36
  result = SpiByteFifoC__SpiByteFifo__txMode();
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline   
# 490 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__TinySec__receiveInit(
TOS_Msg_TinySecCompat *cleartext_ptr)
#line 491
{
  TinySecM__ciphertext_rec_ptr = &TinySecM__tinysec_rec_buffer;
  TinySecM__cleartext_rec_ptr = cleartext_ptr;
  TinySecM__RxByteCnt = 0;
  TinySecM__rxlength = TINYSEC_MSG_DATA_SIZE - 4;
  TinySecM__rxdecrypt = FALSE;


  TinySecM__ciphertext_rec_ptr->cryptoDone = FALSE;
  TinySecM__ciphertext_rec_ptr->receiveDone = FALSE;
  TinySecM__ciphertext_rec_ptr->validMAC = FALSE;


  TinySecM__computeMACBuffer.computeMACWaiting = FALSE;
  TinySecM__computeMACBuffer.computeMACInitWaiting = FALSE;
  TinySecM__decryptBuffer.decryptWaiting = FALSE;
  TinySecM__decryptBuffer.decryptInitWaiting = FALSE;
  TinySecM__computeMACBuffer.finishMACWaiting = FALSE;


  TinySecM__cleartext_rec_ptr->crc = 0;

  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TinySec.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__TinySec__receiveInit(TOS_Msg_TinySecCompat *arg_0xa3c8188){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TinySecM__TinySec__receiveInit(arg_0xa3c8188);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline    
# 592 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__startSymbol(void)
#line 592
{
}

# 45 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/RadioCoordinator.nc"
inline static   void MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__startSymbol(void){
#line 45
  MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__default__startSymbol();
#line 45
}
#line 45
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) &= ~(1 << 5);
}

static inline   
# 58 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePinC.nc"
result_t HPLSlavePinC__SlavePin__low(void)
#line 58
{
  TOSH_MAKE_ONE_WIRE_OUTPUT();
  TOSH_CLR_ONE_WIRE_PIN();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLSlavePin.nc"
inline static   result_t SlavePinM__HPLSlavePin__low(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLSlavePinC__SlavePin__low();
#line 47

#line 47
  return result;
#line 47
}
#line 47
static 
# 110 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePinM.nc"
__inline   result_t SlavePinM__SlavePin__low(void)
#line 110
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      SlavePinM__n--;
      SlavePinM__HPLSlavePin__low();
    }
#line 115
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SlavePin.nc"
inline static   result_t SpiByteFifoC__SlavePin__low(void){
#line 51
  unsigned char result;
#line 51

#line 51
  result = SlavePinM__SlavePin__low();
#line 51

#line 51
  return result;
#line 51
}
#line 51
static inline   
# 119 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__startReadBytes(uint16_t timing)
#line 119
{
  uint8_t oldState;




  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 125
    {
      oldState = SpiByteFifoC__state;
      if (SpiByteFifoC__state == SpiByteFifoC__IDLE) {
          SpiByteFifoC__state = SpiByteFifoC__READING;
        }
    }
#line 130
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == SpiByteFifoC__IDLE) {


      SpiByteFifoC__SlavePin__low();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x1;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = SpiByteFifoC__BIT_RATE;

      timing += 400 - 19;
      if (timing > 0xfff0) {
#line 143
        timing = 0xfff0;
        }
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x19;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = SpiByteFifoC__BIT_RATE - 20;
      while (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x16 + 0x20) & 0x80) {
#line 147
          ;
        }
#line 148
      while (* (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x2C + 0x20) < timing) {
#line 148
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x0;
        }
#line 149
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0xc0;






      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) |= 1 << 6;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 6);
      return SUCCESS;
    }
  return FAIL;
}

# 35 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__startReadBytes(uint16_t arg_0xa38f218){
#line 35
  unsigned char result;
#line 35

#line 35
  result = SpiByteFifoC__SpiByteFifo__startReadBytes(arg_0xa38f218);
#line 35

#line 35
  return result;
#line 35
}
#line 35
# 76 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline int TOSH_READ_RFM_RXD_PIN(void)
#line 76
{
#line 76
  return (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x16 + 0x20) & (1 << 2)) != 0;
}

static inline   
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTimingC.nc"
uint16_t RadioTimingC__RadioTiming__getTiming(void)
#line 40
{


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 4);
  while (TOSH_READ_RFM_RXD_PIN()) {
    }
#line 45
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x2E + 0x20) = 0x41;

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x36 + 0x20) = 0x1 << 5;

  while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x36 + 0x20) & (0x1 << 5)) == 0) {
    }
#line 50
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) |= 1 << 6;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 6);
  return ({
#line 52
    uint16_t __t;
#line 52
    bool bStatus;

#line 52
    bStatus = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) & (1 << 7);
#line 52
     __asm volatile ("cli");__t = * (volatile unsigned int *)(unsigned int )& * (volatile unsigned char *)(0x26 + 0x20);
#line 52
    if (bStatus) {
#line 52
       __asm volatile ("sei");
      }
#line 52
    __t;
  }
  );
}

# 33 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioTiming.nc"
inline static   uint16_t MicaHighSpeedRadioTinySecM__RadioTiming__getTiming(void){
#line 33
  unsigned int result;
#line 33

#line 33
  result = RadioTimingC__RadioTiming__getTiming();
#line 33

#line 33
  return result;
#line 33
}
#line 33
static inline   
# 276 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__ChannelMon__startSymDetect(void)
#line 276
{
  uint16_t tmp;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 279
    {
      MicaHighSpeedRadioTinySecM__ack_count = 0;
      MicaHighSpeedRadioTinySecM__rec_count = 0;
      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__HEADER_RX_STATE;
    }
#line 283
    __nesc_atomic_end(__nesc_atomic); }
  tmp = MicaHighSpeedRadioTinySecM__RadioTiming__getTiming();

  MicaHighSpeedRadioTinySecM__SpiByteFifo__startReadBytes(tmp);
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 287
    {
      MicaHighSpeedRadioTinySecM__msg_length = MSG_DATA_SIZE - 2;
      MicaHighSpeedRadioTinySecM__calc_crc = 0;
      MicaHighSpeedRadioTinySecM__rec_ptr->time = tmp;
      MicaHighSpeedRadioTinySecM__rec_ptr->strength = 0;
    }
#line 292
    __nesc_atomic_end(__nesc_atomic); }
  MicaHighSpeedRadioTinySecM__RadioReceiveCoordinator__startSymbol();

  MicaHighSpeedRadioTinySecM__TinySec__receiveInit(MicaHighSpeedRadioTinySecM__rec_ptr);

  return SUCCESS;
}

# 38 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t ChannelMonC__ChannelMon__startSymDetect(void){
#line 38
  unsigned char result;
#line 38

#line 38
  result = MicaHighSpeedRadioTinySecM__ChannelMon__startSymDetect();
#line 38

#line 38
  return result;
#line 38
}
#line 38
# 40 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifo.nc"
inline static   result_t SpiByteFifoC__SpiByteFifo__dataReady(uint8_t arg_0xa38fe98){
#line 40
  unsigned char result;
#line 40

#line 40
  result = MicaHighSpeedRadioTinySecM__SpiByteFifo__dataReady(arg_0xa38fe98);
#line 40

#line 40
  return result;
#line 40
}
#line 40
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
void __attribute((signal))   __vector_17(void)
#line 56
{
  uint8_t temp = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = SpiByteFifoC__nextByte;
  SpiByteFifoC__state = SpiByteFifoC__OPEN;






  SpiByteFifoC__SpiByteFifo__dataReady(temp);
}

# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t TimerM__PowerManagement__adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM__PowerManagement__adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void HPLClock__Clock__setInterval(uint8_t value)
#line 87
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = value;
}

# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerM__Clock__setInterval(uint8_t arg_0xa57ebe8){
#line 105
  HPLClock__Clock__setInterval(arg_0xa57ebe8);
#line 105
}
#line 105
# 116 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
static void TimerM__adjustInterval(void)
#line 116
{
  uint8_t i;
#line 117
  uint8_t val = TimerM__maxTimerInterval;

#line 118
  if (TimerM__mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM__mState & (0x1 << i) && TimerM__mTimerList[i].ticksLeft < val) {
              val = TimerM__mTimerList[i].ticksLeft;
            }
        }
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
        {
          TimerM__mInterval = val;
          TimerM__Clock__setInterval(TimerM__mInterval);
          TimerM__setIntervalFlag = 0;
        }
#line 128
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
        {
          TimerM__mInterval = TimerM__maxTimerInterval;
          TimerM__Clock__setInterval(TimerM__mInterval);
          TimerM__setIntervalFlag = 0;
        }
#line 135
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM__PowerManagement__adjustPower();
}

static inline   
# 147 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
result_t SimpleTimeM__AbsoluteTimer__default__fired(uint8_t id)
#line 147
{
  return SUCCESS;
}

# 61 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/AbsoluteTimer.nc"
inline static  result_t SimpleTimeM__AbsoluteTimer__fired(uint8_t arg_0xa5d5330){
#line 61
  unsigned char result;
#line 61

#line 61
    result = SimpleTimeM__AbsoluteTimer__default__fired(arg_0xa5d5330);
#line 61

#line 61
  return result;
#line 61
}
#line 61
static inline   
# 53 "C:/cygwin/opt/tinyos-1.x/tos/system/TimeUtilC.nc"
char TimeUtilC__TimeUtil__compare(tos_time_t a, tos_time_t b)
#line 53
{
  if (a.high32 > b.high32) {
#line 54
    return 1;
    }
#line 55
  if (a.high32 < b.high32) {
#line 55
    return -1;
    }
#line 56
  if (a.low32 > b.low32) {
#line 56
    return 1;
    }
#line 57
  if (a.low32 < b.low32) {
#line 57
    return -1;
    }
#line 58
  return 0;
}

# 82 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TimeUtil.nc"
inline static   char SimpleTimeM__TimeUtil__compare(tos_time_t arg_0xa55e2f8, tos_time_t arg_0xa55e448){
#line 82
  char result;
#line 82

#line 82
  result = TimeUtilC__TimeUtil__compare(arg_0xa55e2f8, arg_0xa55e448);
#line 82

#line 82
  return result;
#line 82
}
#line 82
static inline   
# 99 "C:/cygwin/opt/tinyos-1.x/tos/system/TimeUtilC.nc"
tos_time_t TimeUtilC__TimeUtil__addUint32(tos_time_t a, uint32_t ms)
#line 99
{
  tos_time_t result = a;

#line 101
  result.low32 += ms;
  if (result.low32 < a.low32) {
      result.high32++;
    }

  return result;
}

# 65 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TimeUtil.nc"
inline static   tos_time_t SimpleTimeM__TimeUtil__addUint32(tos_time_t arg_0xa561520, uint32_t arg_0xa561670){
#line 65
  struct __nesc_unnamed4260 result;
#line 65

#line 65
  result = TimeUtilC__TimeUtil__addUint32(arg_0xa561520, arg_0xa561670);
#line 65

#line 65
  return result;
#line 65
}
#line 65
static inline  
# 151 "C:/cygwin/opt/tinyos-1.x/tos/system/SimpleTimeM.nc"
result_t SimpleTimeM__Timer__fired(void)
#line 151
{
  uint8_t i;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 154
    SimpleTimeM__time = SimpleTimeM__TimeUtil__addUint32(SimpleTimeM__time, SimpleTimeM__INTERVAL);
#line 154
    __nesc_atomic_end(__nesc_atomic); }

  for (i = 1; i <= MAX_NUM_TIMERS; i++) 
    if ((SimpleTimeM__aTimer[i - 1].low32 || SimpleTimeM__aTimer[i - 1].high32) && 
    SimpleTimeM__TimeUtil__compare(SimpleTimeM__time, SimpleTimeM__aTimer[i - 1]) >= 0) 
      {
        SimpleTimeM__aTimer[i - 1].high32 = 0;
        SimpleTimeM__aTimer[i - 1].low32 = 0;
        SimpleTimeM__AbsoluteTimer__fired(i - 1);
      }
  return SUCCESS;
}

static inline   
# 154 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM__Timer__default__fired(uint8_t id)
#line 154
{
  return SUCCESS;
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t TimerM__Timer__fired(uint8_t arg_0xa584490){
#line 73
  unsigned char result;
#line 73

#line 73
  switch (arg_0xa584490) {
#line 73
    case 0:
#line 73
      result = SimpleTimeM__Timer__fired();
#line 73
      break;
#line 73
    default:
#line 73
      result = TimerM__Timer__default__fired(arg_0xa584490);
#line 73
    }
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 166 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t TimerM__dequeue(void)
#line 166
{
  if (TimerM__queue_size == 0) {
    return NUM_TIMERS;
    }
#line 169
  if (TimerM__queue_head == NUM_TIMERS - 1) {
    TimerM__queue_head = -1;
    }
#line 171
  TimerM__queue_head++;
  TimerM__queue_size--;
  return TimerM__queue[(uint8_t )TimerM__queue_head];
}

static inline  void TimerM__signalOneTimer(void)
#line 176
{
  uint8_t itimer = TimerM__dequeue();

#line 178
  if (itimer < NUM_TIMERS) {
    TimerM__Timer__fired(itimer);
    }
}

static inline 
#line 158
void TimerM__enqueue(uint8_t value)
#line 158
{
  if (TimerM__queue_tail == NUM_TIMERS - 1) {
    TimerM__queue_tail = -1;
    }
#line 161
  TimerM__queue_tail++;
  TimerM__queue_size++;
  TimerM__queue[(uint8_t )TimerM__queue_tail] = value;
}

static inline  
#line 182
void TimerM__HandleFire(void)
#line 182
{
  uint8_t i;

#line 184
  TimerM__setIntervalFlag = 1;
  if (TimerM__mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM__mState & (0x1 << i)) {
              TimerM__mTimerList[i].ticksLeft -= TimerM__mInterval + 1;
              if (TimerM__mTimerList[i].ticksLeft <= 2) {
                  if (TimerM__mTimerList[i].type == TIMER_REPEAT) {
                      TimerM__mTimerList[i].ticksLeft += TimerM__mTimerList[i].ticks;
                    }
                  else 
#line 192
                    {
                      TimerM__mState &= ~(0x1 << i);
                    }
                  TimerM__enqueue(i);
                  TOS_post(TimerM__signalOneTimer);
                }
            }
        }
    }
  TimerM__adjustInterval();
}

static inline   result_t TimerM__Clock__fire(void)
#line 204
{
  TOS_post(TimerM__HandleFire);
  return SUCCESS;
}

# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLClock__Clock__fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerM__Clock__fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
# 66 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM__ByteComm__rxByteReady(uint8_t arg_0xa615600, bool arg_0xa615748, uint16_t arg_0xa6158a0){
#line 66
  unsigned char result;
#line 66

#line 66
  result = FramerM__ByteComm__rxByteReady(arg_0xa615600, arg_0xa615748, arg_0xa6158a0);
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline   
# 77 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM__HPLUART__get(uint8_t data)
#line 77
{




  UARTM__ByteComm__rxByteReady(data, FALSE, 0);
  {
  }
#line 83
  ;
  return SUCCESS;
}

# 88 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUARTM__UART__get(uint8_t arg_0xa6511b8){
#line 88
  unsigned char result;
#line 88

#line 88
  result = UARTM__HPLUART__get(arg_0xa6511b8);
#line 88

#line 88
  return result;
#line 88
}
#line 88
# 71 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
void __attribute((signal))   __vector_18(void)
#line 71
{
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) & 0x80) {
    HPLUARTM__UART__get(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20));
    }
}

static inline  
# 202 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM__PacketUnknown(void)
#line 202
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 203
    {
      FramerM__gFlags |= FramerM__FLAGS_UNKNOWN;
    }
#line 205
    __nesc_atomic_end(__nesc_atomic); }

  FramerM__StartTx();
}

static inline  
# 211 "SecureTOSBaseM.nc"
TOS_MsgPtr SecureTOSBaseM__UARTReceive__receive(TOS_MsgPtr Msg)
#line 211
{
  TOS_MsgPtr pBuf;

  {
  }
#line 214
  ;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 216
    {
      if (SecureTOSBaseM__gfTxFlags & SecureTOSBaseM__TXFLAG_BUSY) {
          pBuf = (void *)0;
        }
      else {
          pBuf = SecureTOSBaseM__gpTxMsg;
          SecureTOSBaseM__gfTxFlags |= SecureTOSBaseM__TXFLAG_BUSY;
          SecureTOSBaseM__gpTxMsg = Msg;
        }
    }
#line 225
    __nesc_atomic_end(__nesc_atomic); }

  if (pBuf == (void *)0) {
      pBuf = Msg;
    }
  else {
      TOS_post(SecureTOSBaseM__UARTRcvdTask);
    }

  return pBuf;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM__ReceiveMsg__receive(TOS_MsgPtr arg_0xa2a2f98){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = SecureTOSBaseM__UARTReceive__receive(arg_0xa2a2f98);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 238 "SecureTOSBaseM.nc"
TOS_MsgPtr SecureTOSBaseM__UARTTokenReceive__receive(TOS_MsgPtr Msg, uint8_t Token)
#line 238
{
  TOS_MsgPtr pBuf;

  {
  }
#line 241
  ;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 243
    {
      if (SecureTOSBaseM__gfTxFlags & SecureTOSBaseM__TXFLAG_BUSY) {
          pBuf = (void *)0;
        }
      else {
          pBuf = SecureTOSBaseM__gpTxMsg;
          SecureTOSBaseM__gfTxFlags |= SecureTOSBaseM__TXFLAG_BUSY | SecureTOSBaseM__TXFLAG_TOKEN;
          SecureTOSBaseM__gpTxMsg = Msg;
          SecureTOSBaseM__gTxPendingToken = Token;
        }
    }
#line 253
    __nesc_atomic_end(__nesc_atomic); }

  if (pBuf == (void *)0) {
      pBuf = Msg;
    }
  else {

      TOS_post(SecureTOSBaseM__UARTRcvdTask);
    }

  return pBuf;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM__TokenReceiveMsg__receive(TOS_MsgPtr arg_0xa2c85d0, uint8_t arg_0xa2c8718){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = SecureTOSBaseM__UARTTokenReceive__receive(arg_0xa2c85d0, arg_0xa2c8718);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 210 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM__PacketRcvd(void)
#line 210
{
  FramerM__MsgRcvEntry_t *pRcv = &FramerM__gMsgRcvTbl[FramerM__gRxTailIndex];
  TOS_MsgPtr pBuf = pRcv->pMsg;


  if (pRcv->Length >= (size_t )& ((struct TOS_Msg *)0)->data) {

      switch (pRcv->Proto) {
          case FramerM__PROTO_ACK: 
            break;
          case FramerM__PROTO_PACKET_ACK: 
            pBuf->crc = 1;
          pBuf = FramerM__TokenReceiveMsg__receive(pBuf, pRcv->Token);
          break;
          case FramerM__PROTO_PACKET_NOACK: 
            pBuf->crc = 1;
          pBuf = FramerM__ReceiveMsg__receive(pBuf);
          break;
          default: 
            FramerM__gTxUnknownBuf = pRcv->Proto;
          TOS_post(FramerM__PacketUnknown);
          break;
        }
    }

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 235
    {
      if (pBuf) {
          pRcv->pMsg = pBuf;
        }
      pRcv->Length = 0;
      pRcv->Token = 0;
      FramerM__gRxTailIndex++;
      FramerM__gRxTailIndex %= FramerM__HDLC_QUEUESIZE;
    }
#line 243
    __nesc_atomic_end(__nesc_atomic); }
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   uint16_t ChannelMonC__Random__rand(void){
#line 63
  unsigned int result;
#line 63

#line 63
  result = RandomLFSR__Random__rand();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 151 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC__ChannelMon__macDelay(void)
#line 151
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      ChannelMonC__CM_search[0] = 0xff;
      if (ChannelMonC__CM_waiting == -1) {
          ChannelMonC__CM_waiting = (ChannelMonC__Random__rand() & 0x2f) + 80;
        }
    }
#line 157
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 36 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMon.nc"
inline static   result_t MicaHighSpeedRadioTinySecM__ChannelMon__macDelay(void){
#line 36
  unsigned char result;
#line 36

#line 36
  result = ChannelMonC__ChannelMon__macDelay();
#line 36

#line 36
  return result;
#line 36
}
#line 36
static inline  
# 156 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__Send__send(TOS_MsgPtr msg)
#line 156
{
  uint8_t oldSendState;

#line 158
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 158
    {
      oldSendState = MicaHighSpeedRadioTinySecM__send_state;
      if (MicaHighSpeedRadioTinySecM__send_state == MicaHighSpeedRadioTinySecM__IDLE_STATE) {
          MicaHighSpeedRadioTinySecM__send_state = MicaHighSpeedRadioTinySecM__SEND_WAITING;

          MicaHighSpeedRadioTinySecM__swapLengthAndGroup(msg);
          MicaHighSpeedRadioTinySecM__send_ptr = (TOS_Msg_TinySecCompat *)msg;
          MicaHighSpeedRadioTinySecM__tx_done = FALSE;

          MicaHighSpeedRadioTinySecM__tx_count = 1;
        }
    }
#line 169
    __nesc_atomic_end(__nesc_atomic); }

  if (oldSendState == MicaHighSpeedRadioTinySecM__IDLE_STATE) {
      return MicaHighSpeedRadioTinySecM__ChannelMon__macDelay();
    }
  else 
#line 173
    {
      return FAIL;
    }
}

# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t RadioPacketTinySecM__RadioSend__send(TOS_MsgPtr arg_0xa2c3448){
#line 58
  unsigned char result;
#line 58

#line 58
  result = MicaHighSpeedRadioTinySecM__Send__send(arg_0xa2c3448);
#line 58

#line 58
  return result;
#line 58
}
#line 58
static inline  
# 46 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/RadioPacketTinySecM.nc"
result_t RadioPacketTinySecM__Send__send(TOS_MsgPtr msg)
#line 46
{
  if (msg->sendSecurityMode == TINYSEC_AUTH_ONLY) {
    msg->length = msg->length | TINYSEC_ENABLED_BIT;
    }
  else {
#line 49
    if (msg->sendSecurityMode == TINYSEC_ENCRYPT_AND_AUTH) {
      msg->length = (msg->length | TINYSEC_ENABLED_BIT) | 
      TINYSEC_ENCRYPT_ENABLED_BIT;
      }
    else {
#line 52
      if (msg->sendSecurityMode != TINYSEC_DISABLED) {
        return FAIL;
        }
      }
    }
#line 55
  return RadioPacketTinySecM__RadioSend__send(msg);
}

# 58 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t SecureTOSBaseM__RadioSend__send(TOS_MsgPtr arg_0xa2c3448){
#line 58
  unsigned char result;
#line 58

#line 58
  result = RadioPacketTinySecM__Send__send(arg_0xa2c3448);
#line 58

#line 58
  return result;
#line 58
}
#line 58
# 59 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline   
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC__Leds__greenOn(void)
#line 98
{
  {
  }
#line 99
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 100
    {
      TOSH_CLR_GREEN_LED_PIN();
      LedsC__ledsOn |= LedsC__GREEN_BIT;
    }
#line 103
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__greenOff(void)
#line 107
{
  {
  }
#line 108
  ;
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 109
    {
      TOSH_SET_GREEN_LED_PIN();
      LedsC__ledsOn &= ~LedsC__GREEN_BIT;
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC__Leds__greenToggle(void)
#line 116
{
  result_t rval;

#line 118
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 118
    {
      if (LedsC__ledsOn & LedsC__GREEN_BIT) {
        rval = LedsC__Leds__greenOff();
        }
      else {
#line 122
        rval = LedsC__Leds__greenOn();
        }
    }
#line 124
    __nesc_atomic_end(__nesc_atomic); }
#line 124
  return rval;
}

# 106 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t SecureTOSBaseM__Leds__greenToggle(void){
#line 106
  unsigned char result;
#line 106

#line 106
  result = LedsC__Leds__greenToggle();
#line 106

#line 106
  return result;
#line 106
}
#line 106
# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUARTM__UART__putDone(void){
#line 96
  unsigned char result;
#line 96

#line 96
  result = UARTM__HPLUART__putDone();
#line 96

#line 96
  return result;
#line 96
}
#line 96
# 77 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLUARTM.nc"
void __attribute((interrupt))   __vector_20(void)
#line 77
{
  HPLUARTM__UART__putDone();
}

static inline   
# 552 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__ByteComm__txDone(void)
#line 552
{

    if (FramerM__gTxState == FramerM__TXSTATE_FINISH) {
      FramerM__gTxState = FramerM__TXSTATE_IDLE;
      TOS_post(FramerM__PacketSent);
    }

  return SUCCESS;
}

# 83 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM__ByteComm__txDone(void){
#line 83
  unsigned char result;
#line 83

#line 83
  result = FramerM__ByteComm__txDone();
#line 83

#line 83
  return result;
#line 83
}
#line 83
#line 55
inline static   result_t FramerM__ByteComm__txByte(uint8_t arg_0xa615170){
#line 55
  unsigned char result;
#line 55

#line 55
  result = UARTM__ByteComm__txByte(arg_0xa615170);
#line 55

#line 55
  return result;
#line 55
}
#line 55
static inline   
# 482 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__ByteComm__txByteReady(bool LastByteSuccess)
#line 482
{
  result_t TxResult = SUCCESS;
  uint8_t nextByte;

  if (LastByteSuccess != TRUE) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 487
        FramerM__gTxState = FramerM__TXSTATE_ERROR;
#line 487
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(FramerM__PacketSent);//JHALA
      //FramerM__PacketSent ();
      return SUCCESS;
    }
  
  switch (FramerM__gTxState ) {/* depends on state! */

      case FramerM__TXSTATE_PROTO: 
        FramerM__gTxState = FramerM__TXSTATE_INFO;
      FramerM__gTxRunningCRC = crcByte(FramerM__gTxRunningCRC, FramerM__gTxProto);
      TxResult = FramerM__ByteComm__txByte(FramerM__gTxProto);
      break;

      case FramerM__TXSTATE_INFO: 
        nextByte = FramerM__gpTxBuf[FramerM__gTxByteCnt];

      FramerM__gTxRunningCRC = crcByte(FramerM__gTxRunningCRC, nextByte);
      FramerM__gTxByteCnt++;
      if (FramerM__gTxByteCnt >= FramerM__gTxLength) {
          FramerM__gTxState = FramerM__TXSTATE_FCS1;
        }

      TxResult = FramerM__TxArbitraryByte(nextByte);
      break;

  case FramerM__TXSTATE_ESC: // JHALA error... first thread comes through here
      //second thread pre-empts follows same path comes through here..
      //swapping the next two may make things perfect... bug ?
        
      FramerM__gTxState = FramerM__gPrevTxState;
      TxResult = FramerM__ByteComm__txByte(FramerM__gTxEscByte ^ 0x20);
      break; //JHALA -- am swapping the above

  case FramerM__TXSTATE_FCS1: 
      nextByte = (uint8_t )(FramerM__gTxRunningCRC & 0xff);
      FramerM__gTxState = FramerM__TXSTATE_FCS2;
      TxResult = FramerM__TxArbitraryByte(nextByte);
      break;

  case FramerM__TXSTATE_FCS2: 
      nextByte = (uint8_t )((FramerM__gTxRunningCRC >> 8) & 0xff);
      FramerM__gTxState = FramerM__TXSTATE_ENDFLAG;
      TxResult = FramerM__TxArbitraryByte(nextByte);
      break;

  case FramerM__TXSTATE_ENDFLAG: 
      FramerM__gTxState = FramerM__TXSTATE_FINISH;
      TxResult = FramerM__ByteComm__txByte(FramerM__HDLC_FLAG_BYTE);

      break;

  case FramerM__TXSTATE_FINISH: 
  case FramerM__TXSTATE_ERROR: 
      
          default: 
            break;
    }


  if (TxResult != SUCCESS ) {
      FramerM__gTxState = FramerM__TXSTATE_ERROR; //JHALA
      TOS_post(FramerM__PacketSent);
    }

  return SUCCESS;
}

# 75 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM__ByteComm__txByteReady(bool arg_0xa615dd0){
#line 75
  unsigned char result;
#line 75

#line 75
  result = FramerM__ByteComm__txByteReady(arg_0xa615dd0);
#line 75

#line 75
  return result;
#line 75
}
#line 75
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool  TOS_post(void (*tp)(void))
#line 98
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;

/*

  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;
  TOSH_sched_free++;
  TOSH_sched_free &= TOSH_TASK_BITMASK;

  if (TOSH_sched_free != TOSH_sched_full) {
      __nesc_atomic_end(fInterruptFlags);

      TOSH_queue[tmp].tp = tp;
      return TRUE;
    }
  else {
      TOSH_sched_free = tmp;
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
*/
}

void blast_run_tasks(void) __attribute__((task)){
	int __BLAST_NONDET;

	//if (__BLAST_task_enabled==0) { 
	//	__BLAST_task_enabled = 1; 
		switch (__BLAST_NONDET) {
		case 0: SlavePinM__signalHighTask();
		case 1: SecureTOSBaseM__SendAckTask();
		case 2: SecureTOSBaseM__RadioRcvdTask();
		case 3: SecureTOSBaseM__UARTRcvdTask();
		case 4: FramerM__PacketUnknown();
		    //case 5: FramerM__PacketSent();
		case 6: FramerM__PacketRcvd();
		case 7: TimerM__signalOneTimer();
		case 8: TimerM__HandleFire();
		case 9: MicaHighSpeedRadioTinySecM__packetReceived();
		case 10: MicaHighSpeedRadioTinySecM__packetSent();
		case 11: HPLPowerManagementM__doAdjustment();
		}
		//__BLAST_task_enabled = 0;
		//}
}


/* BLAST */
void atomic_check_and_set() __attribute__((atomic)) {
  {__blockattribute__((assume(__BLAST_interrupt_enabled == 1)))}
  __BLAST_interrupt_enabled = 0;
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void)
#line 54
{
	int __BLAST_NONDET;
/*
  RealMain__hardwareInit();
  RealMain__Pot__init(10);
  TOSH_sched_init();

  RealMain__StdControl__init();
  RealMain__StdControl__start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
*/
  while(1) {
	  switch(__BLAST_NONDET) {
	  case 9: 
	    /*if(__BLAST_interrupt_enabled) 
	      { __BLAST_task_enabled = 1; __BLAST_interrupt_enabled = 0; __vector_9(); __BLAST_task_enabled = 0; }
	    */
	    //atomic_check_and_set(); 
	    {__blockattribute__((atomic)) {__blockattribute__((assume(__BLAST_interrupt_enabled == 1)))}
	    __BLAST_interrupt_enabled = 0;
	    }
	    /*__BLAST_task_enabled = 1;*/ __vector_9(); __BLAST_task_enabled = 0; __BLAST_interrupt_enabled = 1;
	    break;
	  case 15: if(__BLAST_interrupt_enabled) 
		{ __BLAST_task_enabled = 1; __vector_15(); __BLAST_task_enabled = 0; }
		break;
	  case 17: 
	    /*if(__BLAST_interrupt_enabled) 
	      { __BLAST_task_enabled = 1; __vector_17(); __BLAST_task_enabled = 0; } */
	    //atomic_check_and_set();
	    {__blockattribute__((atomic)) 
	      {__blockattribute__((assume(__BLAST_interrupt_enabled == 1)))}
	    __BLAST_interrupt_enabled = 0;
	    }
	    __BLAST_task_enabled = 1; __vector_17(); __BLAST_task_enabled = 0; __BLAST_interrupt_enabled = 1;
	    break;
	  case 18: 
	    /* if(__BLAST_interrupt_enabled) 
	       { __BLAST_task_enabled = 1; __vector_18(); __BLAST_task_enabled = 0; } */
	    //atomic_check_and_set();
	    {__blockattribute__((atomic)) 
	      {__blockattribute__((assume(__BLAST_interrupt_enabled == 1)))}
	    __BLAST_interrupt_enabled = 0;
	    }
	    __BLAST_task_enabled = 1; __vector_18(); __BLAST_task_enabled = 0; __BLAST_interrupt_enabled = 1;
		break;
	  case 20: if(__BLAST_interrupt_enabled) 
		{ __BLAST_task_enabled = 1; __vector_20(); __BLAST_task_enabled = 0; }
		break;
	   case 99: blast_run_tasks();
	  default : break;
	}
  }
}

static 
# 268 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM__HDLCInitialize(void)
#line 268
{
  int i;

#line 270
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 270
    {
      for (i = 0; i < FramerM__HDLC_QUEUESIZE; i++) {
          FramerM__gMsgRcvTbl[i].pMsg = &FramerM__gMsgRcvBuf[i];
          FramerM__gMsgRcvTbl[i].Length = 0;
          FramerM__gMsgRcvTbl[i].Token = 0;
        }
      FramerM__gTxState = FramerM__TXSTATE_IDLE;
      FramerM__gTxByteCnt = 0;
      FramerM__gTxLength = 0;
      FramerM__gTxRunningCRC = 0;
      FramerM__gpTxMsg = (void *)0;

      FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
      FramerM__gRxHeadIndex = 0;
      FramerM__gRxTailIndex = 0;
      FramerM__gRxByteCnt = 0;
      FramerM__gRxRunningCRC = 0;
      FramerM__gpRxBuf = (uint8_t *)FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].pMsg;
    }
#line 288
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 66 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
result_t ChannelMonC__ChannelMon__startSymbolSearch(void)
#line 66
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 67
    {

      ChannelMonC__CM_state = ChannelMonC__IDLE_STATE;

      TOSH_SET_RFM_CTL0_PIN();
      TOSH_SET_RFM_CTL1_PIN();
      TOSH_CLR_RFM_TXD_PIN();






      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 6);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x09;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = ChannelMonC__SAMPLE_RATE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 6;
    }
#line 88
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static 
# 137 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
void *nmemcpy(void *to, const void *from, size_t n)
{
  char *cto = to;
  const char *cfrom = from;

  while (n--) * cto++ = * cfrom++;

  return to;
}

static 
# 312 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
result_t SkipJackM__setupKey(CipherContext *context, uint8_t *key, uint8_t keysize)
{
  int i = 0;
#line 314
  int m;
  uint8_t *skey = ((SkipJackM__SJContext *)context->context)->skey;




  for (; i < 128; i++) {
      m = i % 10;
      if (m >= keysize) {
        skey[i] = 0;
        }
      else {
#line 325
        skey[i] = key[m];
        }
    }
#line 327
  return SUCCESS;
}

static   
# 103 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM__PowerManagement__adjustPower(void)
#line 103
{
  uint8_t mcu;

#line 105
  if (!HPLPowerManagementM__disabled) {
    TOS_post(HPLPowerManagementM__doAdjustment);
    }
  else 
#line 107
    {
      mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM__IDLE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

# 94 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/ChannelMonC.nc"
void __attribute((signal))   __vector_9(void)
#line 94
{
  uint8_t bit = TOSH_READ_RFM_RXD_PIN();

#line 96
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 96
    {

      if (ChannelMonC__CM_state == ChannelMonC__IDLE_STATE) {
          ChannelMonC__CM_search[0] <<= 1;
          ChannelMonC__CM_search[0] = ChannelMonC__CM_search[0] | (bit & 0x1);
          if (ChannelMonC__CM_waiting != -1) {
              ChannelMonC__CM_waiting--;
              if (ChannelMonC__CM_waiting == 1) {
                  if ((ChannelMonC__CM_search[0] & 0xfff) == 0) {
                      ChannelMonC__CM_waiting = -1;
                      ChannelMonC__ChannelMon__idleDetect();
                    }
                  else 
#line 107
                    {
                      ChannelMonC__CM_waiting = (ChannelMonC__Random__rand() & 0x1f) + 30;
                    }
                }
            }
          if ((ChannelMonC__CM_search[0] & 0x777) == 0x707) {
              ChannelMonC__CM_state = ChannelMonC__START_SYMBOL_SEARCH;
              ChannelMonC__CM_search[0] = ChannelMonC__CM_search[1] = 0;
              ChannelMonC__CM_startSymBits = 30;
            }
        }
      else {
#line 117
        if (ChannelMonC__CM_state == ChannelMonC__START_SYMBOL_SEARCH) {
            unsigned int current = ChannelMonC__CM_search[ChannelMonC__CM_lastBit];

#line 119
            ChannelMonC__CM_startSymBits--;
            if (ChannelMonC__CM_startSymBits == 0) {
                ChannelMonC__CM_state = ChannelMonC__IDLE_STATE;
              }
            if (ChannelMonC__CM_state != ChannelMonC__IDLE_STATE) {
                current <<= 1;
                current &= 0x1ff;
                if (bit) {
#line 126
                  current |= 0x1;
                  }
#line 127
                if (current == 0x135) {
                    * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
                    ChannelMonC__CM_state = ChannelMonC__IDLE_STATE;
                    ChannelMonC__ChannelMon__startSymDetect();
                  }
                if (ChannelMonC__CM_state != ChannelMonC__IDLE_STATE) {
                    ChannelMonC__CM_search[ChannelMonC__CM_lastBit] = current;
                    ChannelMonC__CM_lastBit ^= 1;
                  }
              }
          }
        }
    }
#line 139
    __nesc_atomic_end(__nesc_atomic); }
#line 139
  return;
}

static   
# 80 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SecDedEncoding.nc"
result_t SecDedEncoding__Code__encode(char d)
#line 80
{
  uint8_t oldState;

#line 82
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 82
    {
      oldState = SecDedEncoding__state;
      if (SecDedEncoding__state == SecDedEncoding__IDLE_STATE) {
          SecDedEncoding__state = SecDedEncoding__ENCODING_BYTE;
          SecDedEncoding__data1 = d;
        }
    }
#line 88
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == SecDedEncoding__IDLE_STATE) {
      SecDedEncoding__radio_encode_thread();
      return 1;
    }
  else {
      return 0;
    }
}

static   
# 444 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__Code__encodeDone(char data1)
#line 444
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 445
    {
      MicaHighSpeedRadioTinySecM__encoded_buffer[(int )MicaHighSpeedRadioTinySecM__buf_end] = data1;
      MicaHighSpeedRadioTinySecM__buf_end++;
      MicaHighSpeedRadioTinySecM__buf_end &= 0x3;
      MicaHighSpeedRadioTinySecM__enc_count += 1;
    }
#line 450
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static 
#line 302
void MicaHighSpeedRadioTinySecM__timeSyncFunctionHack(void)
#line 302
{
  struct TimeSyncMsg *ptr;
  tos_time_t tt;

  ptr = (struct TimeSyncMsg *)MicaHighSpeedRadioTinySecM__send_ptr->data;
  tt = MicaHighSpeedRadioTinySecM__Time__get();

  ptr->timeH = tt.high32;
  ptr->timeL = tt.low32;
  ptr->phase = MicaHighSpeedRadioTinySecM__Time__getUs();
}

static   
# 71 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__send(uint8_t data)
#line 71
{
  result_t rval = FAIL;

#line 73
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
    {
      if (SpiByteFifoC__state == SpiByteFifoC__OPEN) {
          SpiByteFifoC__nextByte = data;
          SpiByteFifoC__state = SpiByteFifoC__FULL;
          rval = SUCCESS;
        }
      else {
#line 79
        if (SpiByteFifoC__state == SpiByteFifoC__IDLE) {
            SpiByteFifoC__state = SpiByteFifoC__OPEN;
            SpiByteFifoC__SpiByteFifo__dataReady(0);
            SpiByteFifoC__SlavePin__low();
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 7);
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 7;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0xc0;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = data;

            TOSH_CLR_RFM_CTL0_PIN();
            TOSH_SET_RFM_CTL1_PIN();

            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 6);
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = SpiByteFifoC__BIT_RATE;
            * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x19;
            rval = SUCCESS;
          }
        }
    }
#line 99
    __nesc_atomic_end(__nesc_atomic); }
#line 99
  return rval;
}

static   
# 454 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__SpiByteFifo__dataReady(uint8_t data)
#line 454
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 455
    {
      if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__TRANSMITTING_START) {
          MicaHighSpeedRadioTinySecM__SpiByteFifo__send(TOSH_MHSR_start[(int )MicaHighSpeedRadioTinySecM__tx_count]);
          MicaHighSpeedRadioTinySecM__tx_count++;
          if (MicaHighSpeedRadioTinySecM__tx_count == sizeof TOSH_MHSR_start) {
              if (MicaHighSpeedRadioTinySecM__send_ptr->length & TINYSEC_ENABLED_BIT) {

                  MicaHighSpeedRadioTinySecM__TinySecRadio__getTransmitByte();
                  MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__TRANSMITTING_TINYSEC;
                }
              else {
                  MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__TRANSMITTING;
                }
              MicaHighSpeedRadioTinySecM__tx_count = 1;
            }
        }
      else {
#line 470
        if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__TRANSMITTING) {
            MicaHighSpeedRadioTinySecM__SpiByteFifo__send(MicaHighSpeedRadioTinySecM__encoded_buffer[(int )MicaHighSpeedRadioTinySecM__buf_head]);
            MicaHighSpeedRadioTinySecM__buf_head++;
            MicaHighSpeedRadioTinySecM__buf_head &= 0x3;
            MicaHighSpeedRadioTinySecM__enc_count--;


            if (MicaHighSpeedRadioTinySecM__enc_count >= 2) {
                ;
              }
            else {
#line 479
              if (MicaHighSpeedRadioTinySecM__tx_count < MSG_DATA_SIZE) {
                  char next_data = ((char *)MicaHighSpeedRadioTinySecM__send_ptr)[(int )MicaHighSpeedRadioTinySecM__tx_count];

#line 481
                  MicaHighSpeedRadioTinySecM__Code__encode(next_data);
                  MicaHighSpeedRadioTinySecM__tx_count++;
                  if (MicaHighSpeedRadioTinySecM__tx_count <= MicaHighSpeedRadioTinySecM__msg_length) {
                      MicaHighSpeedRadioTinySecM__calc_crc = crcByte(MicaHighSpeedRadioTinySecM__calc_crc, next_data);
                    }
                  if (MicaHighSpeedRadioTinySecM__tx_count == MicaHighSpeedRadioTinySecM__msg_length) {


                      MicaHighSpeedRadioTinySecM__tx_count = MSG_DATA_SIZE - 2;
                      MicaHighSpeedRadioTinySecM__send_ptr->crc = MicaHighSpeedRadioTinySecM__calc_crc;
                    }
                  MicaHighSpeedRadioTinySecM__RadioSendCoordinator__byte((TOS_MsgPtr )MicaHighSpeedRadioTinySecM__send_ptr, 
                  (uint8_t )MicaHighSpeedRadioTinySecM__tx_count);
                }
              else {
#line 494
                if (MicaHighSpeedRadioTinySecM__buf_head != MicaHighSpeedRadioTinySecM__buf_end) {
                    MicaHighSpeedRadioTinySecM__Code__encode_flush();
                  }
                else 
#line 496
                  {
                    MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__SENDING_STRENGTH_PULSE;
                    MicaHighSpeedRadioTinySecM__tx_count = 0;
                    MicaHighSpeedRadioTinySecM__tx_done = TRUE;
                  }
                }
              }
          }
        else {
#line 501
          if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__TRANSMITTING_TINYSEC) {
              MicaHighSpeedRadioTinySecM__SpiByteFifo__send(MicaHighSpeedRadioTinySecM__encoded_buffer[(int )MicaHighSpeedRadioTinySecM__buf_head]);
              MicaHighSpeedRadioTinySecM__buf_head++;
              MicaHighSpeedRadioTinySecM__buf_head &= 0x3;
              MicaHighSpeedRadioTinySecM__enc_count--;


              if (MicaHighSpeedRadioTinySecM__enc_count >= 2) {
                  ;
                }
              else {
#line 510
                if (MicaHighSpeedRadioTinySecM__tx_count < TINYSEC_MSG_DATA_SIZE) {
                    uint8_t next_data = MicaHighSpeedRadioTinySecM__TinySecRadio__getTransmitByte();

#line 512
                    MicaHighSpeedRadioTinySecM__Code__encode(next_data);
                    MicaHighSpeedRadioTinySecM__tx_count++;
                    if (MicaHighSpeedRadioTinySecM__tx_done) {
                        MicaHighSpeedRadioTinySecM__tx_count = TINYSEC_MSG_DATA_SIZE;
                      }
                    MicaHighSpeedRadioTinySecM__RadioSendCoordinator__byte((TOS_MsgPtr )MicaHighSpeedRadioTinySecM__send_ptr, 
                    (uint8_t )MicaHighSpeedRadioTinySecM__tx_count);
                  }
                else {
#line 519
                  if (MicaHighSpeedRadioTinySecM__buf_head != MicaHighSpeedRadioTinySecM__buf_end) {
                      MicaHighSpeedRadioTinySecM__Code__encode_flush();
                    }
                  else 
#line 521
                    {
                      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__SENDING_STRENGTH_PULSE;
                      MicaHighSpeedRadioTinySecM__tx_count = 0;
                    }
                  }
                }
            }
          else {
#line 525
            if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__SENDING_STRENGTH_PULSE) {
                MicaHighSpeedRadioTinySecM__tx_count++;
                if (MicaHighSpeedRadioTinySecM__tx_count == 3) {
                    MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__WAITING_FOR_ACK;
                    MicaHighSpeedRadioTinySecM__SpiByteFifo__phaseShift();
                    MicaHighSpeedRadioTinySecM__tx_count = 1;
                    MicaHighSpeedRadioTinySecM__SpiByteFifo__send(0x00);
                  }
                else {
                    MicaHighSpeedRadioTinySecM__SpiByteFifo__send(0xff);
                  }
              }
            else {
#line 537
              if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__WAITING_FOR_ACK) {
                  data &= 0x7f;
                  MicaHighSpeedRadioTinySecM__SpiByteFifo__send(0x00);
                  if (MicaHighSpeedRadioTinySecM__tx_count == 1) {
                    MicaHighSpeedRadioTinySecM__SpiByteFifo__rxMode();
                    }
#line 542
                  MicaHighSpeedRadioTinySecM__tx_count++;
                  if (MicaHighSpeedRadioTinySecM__tx_count == MicaHighSpeedRadioTinySecM__ACK_CNT + 2) {
                      MicaHighSpeedRadioTinySecM__send_ptr->ack = data == 0x55;
                      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;








                      MicaHighSpeedRadioTinySecM__SpiByteFifo__idle();
                      TOS_post(MicaHighSpeedRadioTinySecM__packetSent);
                    }
                }
              else {
#line 558
                if ((
#line 557
                MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__RX_STATE || 
                MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__RX_STATE_TINYSEC) || 
                MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__HEADER_RX_STATE) {
                    MicaHighSpeedRadioTinySecM__Code__decode(data);
                  }
                else {
#line 561
                  if (MicaHighSpeedRadioTinySecM__state == MicaHighSpeedRadioTinySecM__ACK_SEND_STATE) {
                      MicaHighSpeedRadioTinySecM__ack_count++;
                      if (MicaHighSpeedRadioTinySecM__ack_count > MicaHighSpeedRadioTinySecM__ACK_CNT + 1) {
                          MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__RX_DONE_STATE;
                          MicaHighSpeedRadioTinySecM__SpiByteFifo__idle();

                          if (MicaHighSpeedRadioTinySecM__tinysec_rx_done) {
                              TOS_post(MicaHighSpeedRadioTinySecM__packetReceived);
                            }
                        }
                      else 
#line 570
                        {
                          MicaHighSpeedRadioTinySecM__SpiByteFifo__txMode();
                        }
                    }
                  }
                }
              }
            }
          }
        }
    }
#line 580
    __nesc_atomic_end(__nesc_atomic); }
#line 575
  return 1;
}

static   
# 786 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
uint8_t TinySecM__TinySecRadio__getTransmitByte(void)
#line 786
{
  uint8_t NextTxByte = 0;

#line 788
  TinySecM__TxByteCnt++;
  if (TinySecM__TxByteCnt < TinySecM__txlength) {

      if (TinySecM__TxByteCnt == (size_t )& ((struct TinySec_Msg *)0)->iv && !TinySecM__txencrypt) {
        TinySecM__TxByteCnt += 4;
        }
#line 793
      NextTxByte = ((uint8_t *)&TinySecM__tinysec_send_buffer)[TinySecM__TxByteCnt];
      {
      }
#line 794
      ;
    }
  else {
#line 796
    if (TinySecM__TxByteCnt < TinySecM__txlength + 4) {
        NextTxByte = TinySecM__tinysec_send_buffer.mac[TinySecM__TxByteCnt - TinySecM__txlength];
        {
        }
#line 798
        ;

        if (TinySecM__TxByteCnt == TinySecM__txlength + 4 - 1) {
            TinySecM__TinySec__sendDone(SUCCESS);
            {
            }
#line 802
            ;
          }
      }
    else 
#line 804
      {
        {
        }
#line 805
        ;
        TinySecM__TinySec__sendDone(FAIL);
      }
    }
  return NextTxByte;
}

static   
# 102 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/SpiByteFifoC.nc"
result_t SpiByteFifoC__SpiByteFifo__idle(void)
#line 102
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 103
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x00;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0x00;
      SpiByteFifoC__nextByte = 0;
      SpiByteFifoC__SlavePin__high(FALSE);
      TOSH_MAKE_RFM_TXD_OUTPUT();
      TOSH_CLR_RFM_TXD_PIN();
      TOSH_CLR_RFM_CTL0_PIN();
      TOSH_CLR_RFM_CTL1_PIN();
      SpiByteFifoC__state = SpiByteFifoC__IDLE;
      SpiByteFifoC__nextByte = 0;
    }
#line 115
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static 
# 113 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__swapLengthAndGroup(TOS_Msg *buf)
#line 113
{
  uint8_t tmp = buf->group;

  ((TOS_Msg_TinySecCompat *)buf)->length = buf->length;
  ((TOS_Msg_TinySecCompat *)buf)->group = tmp;
}

static 
# 158 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__StartTx(void)
#line 158
{
  result_t Result = SUCCESS;
  bool fInitiate = FALSE;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 162
    {
      if (FramerM__gTxState == FramerM__TXSTATE_IDLE) {
          if (FramerM__gFlags & FramerM__FLAGS_TOKENPEND) {
              FramerM__gpTxBuf = (uint8_t *)&FramerM__gTxTokenBuf;
              FramerM__gTxProto = FramerM__PROTO_ACK;
              FramerM__gTxLength = sizeof FramerM__gTxTokenBuf;
              fInitiate = TRUE;
              FramerM__gTxState = FramerM__TXSTATE_PROTO;
            }
          else {
#line 171
            if (FramerM__gFlags & FramerM__FLAGS_DATAPEND) {
                FramerM__gpTxBuf = (uint8_t *)FramerM__gpTxMsg;
                FramerM__gTxProto = FramerM__PROTO_PACKET_NOACK;
                FramerM__gTxLength = FramerM__gpTxMsg->length + (MSG_DATA_SIZE - DATA_LENGTH - 2);
                fInitiate = TRUE;
                FramerM__gTxState = FramerM__TXSTATE_PROTO;
              }
            else {
#line 178
              if (FramerM__gFlags & FramerM__FLAGS_UNKNOWN) {
                  FramerM__gpTxBuf = (uint8_t *)&FramerM__gTxUnknownBuf;
                  FramerM__gTxProto = FramerM__PROTO_UNKNOWN;
                  FramerM__gTxLength = sizeof FramerM__gTxUnknownBuf;
                  fInitiate = TRUE;
                  FramerM__gTxState = FramerM__TXSTATE_PROTO;
                }
              }
            }
        }
    }
#line 188
    __nesc_atomic_end(__nesc_atomic); }
#line 188
  if (fInitiate) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 189
        {
          FramerM__gTxRunningCRC = 0;
#line 190
          FramerM__gTxByteCnt = 0;
        }
#line 191
        __nesc_atomic_end(__nesc_atomic); }
      Result = FramerM__ByteComm__txByte(FramerM__HDLC_FLAG_BYTE);
      if (Result != SUCCESS) {
          { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 194
            FramerM__gTxState = FramerM__TXSTATE_ERROR;
#line 194
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(FramerM__PacketSent);
        }
    }

  return Result;
}

static   
# 110 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM__ByteComm__txByte(uint8_t data)
#line 110
{
  bool oldState;

  {
  }
#line 113
  ;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
    {
      oldState = UARTM__state;
      if (UARTM__state == FALSE)
	  UARTM__state = TRUE;
    }
#line 118
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState == TRUE) {
    return FAIL;
    }
  UARTM__HPLUART__put(data);

  return SUCCESS;
}

static  
# 246 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
void FramerM__PacketSent(void) __attribute__((task))
#line 246
{
  result_t TxResult = SUCCESS;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 249
    {
      if (FramerM__gTxState == FramerM__TXSTATE_ERROR) {
          TxResult = FAIL;
          FramerM__gTxState = FramerM__TXSTATE_IDLE;
        }
    }
#line 254
    __nesc_atomic_end(__nesc_atomic); }
  if (FramerM__gTxProto == FramerM__PROTO_ACK) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 256
        FramerM__gFlags ^= FramerM__FLAGS_TOKENPEND;
#line 256
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 259
        FramerM__gFlags ^= FramerM__FLAGS_DATAPEND;
#line 259
        __nesc_atomic_end(__nesc_atomic); }
      FramerM__BareSendMsg__sendDone((TOS_MsgPtr )FramerM__gpTxMsg, TxResult);
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 261
        FramerM__gpTxMsg = (void *)0;
#line 261
        __nesc_atomic_end(__nesc_atomic); }
    }


  FramerM__StartTx();
}

static   
# 516 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__TinySecRadio__byteReceived(uint8_t byte)
#line 516
{
  int8_t macRecCount = -1;
#line 517
  int8_t decryptRecCount = -1;

#line 518
  if (TinySecM__RxByteCnt < TinySecM__rxlength) {


      if (!TinySecM__rxdecrypt && TinySecM__RxByteCnt == (size_t )& ((struct TinySec_Msg *)0)->iv) {
          TinySecM__RxByteCnt += 4;
          ((uint8_t *)TinySecM__ciphertext_rec_ptr)[(int )TinySecM__RxByteCnt] = byte;
          TinySecM__RxByteCnt++;
          macRecCount = ((TinySecM__RxByteCnt - 4) & (TinySecM__blockSize - 1)) + 
          4;
          decryptRecCount = TinySecM__RxByteCnt - (size_t )& ((struct TinySec_Msg *)0)->enc;
        }
      else 
#line 528
        {
          ((uint8_t *)TinySecM__ciphertext_rec_ptr)[(int )TinySecM__RxByteCnt] = byte;
          TinySecM__RxByteCnt++;
          macRecCount = TinySecM__RxByteCnt & (TinySecM__blockSize - 1);
          decryptRecCount = TinySecM__RxByteCnt - (size_t )& ((struct TinySec_Msg *)0)->enc;
        }
    }
  else {
#line 534
    if (TinySecM__RxByteCnt < TinySecM__rxlength + 4) {
        TinySecM__ciphertext_rec_ptr->mac[TinySecM__RxByteCnt - TinySecM__rxlength] = byte;
        TinySecM__RxByteCnt++;
      }
    }
  {
  }
#line 539
  ;



  if (TinySecM__RxByteCnt < TinySecM__rxlength) {
      if (TinySecM__RxByteCnt == (size_t )& ((struct TinySec_Msg *)0)->length + 
      sizeof  ((struct TinySec_Msg *)0)->length) {

          TinySecM__recDataLength = TinySecM__ciphertext_rec_ptr->length & (
          TINYSEC_ENCRYPT_ENABLED_BIT - 1);
          if (TinySecM__recDataLength > DATA_LENGTH) {
            TinySecM__TinySec__receiveInitDone(FAIL, 0, FALSE);
            }
          if (TinySecM__ciphertext_rec_ptr->length & TINYSEC_ENABLED_BIT) {
              {
              }
#line 553
              ;
              if (TinySecM__ciphertext_rec_ptr->length & TINYSEC_ENCRYPT_ENABLED_BIT) {
                  {
                  }
#line 555
                  ;
                  TinySecM__rxdecrypt = TRUE;
                }
              else 
#line 557
                {
                  {
                  }
#line 558
                  ;
                  TinySecM__rxdecrypt = FALSE;
                }
              TinySecM__rxlength = (size_t )& ((struct TinySec_Msg *)0)->enc;

              if (TinySecM__recDataLength < TinySecM__blockSize && TinySecM__rxdecrypt) {
                TinySecM__rxlength += TinySecM__blockSize;
                }
              else {
#line 566
                TinySecM__rxlength += TinySecM__recDataLength;
                }
              if (TinySecM__rxdecrypt) {
                  TinySecM__TinySec__receiveInitDone(SUCCESS, TinySecM__rxlength, TRUE);
                }
              else 
#line 570
                {

                  nmemset(TinySecM__ciphertext_rec_ptr->iv, 0, 4);
                  TinySecM__TinySec__receiveInitDone(SUCCESS, 
                  TinySecM__rxlength - 4, TRUE);
                }
              TinySecM__postIncrementalMACInit();
            }
          else 
#line 577
            {
              TinySecM__rxlength = TinySecM__recDataLength + (size_t )& ((struct TOS_Msg_TinySecCompat *)0)->data;

              TinySecM__TinySec__receiveInitDone(SUCCESS, TinySecM__rxlength, FALSE);
            }
        }
      else 
#line 582
        {



          if (macRecCount == 0) {
            macRecCount = TinySecM__blockSize;
            }
#line 588
          if (macRecCount >= TinySecM__blockSize) {

              TinySecM__postIncrementalMAC(TinySecM__RxByteCnt - macRecCount, TinySecM__blockSize);
            }

          if (TinySecM__rxdecrypt) {
              if (decryptRecCount == 0) {
                  TinySecM__postIncrementalDecryptInit();
                }
              else {
#line 596
                if ((decryptRecCount & (TinySecM__blockSize - 1)) == 0) {
                    TinySecM__postIncrementalDecrypt(decryptRecCount - TinySecM__blockSize, TinySecM__blockSize);
                  }
                }
            }
        }
#line 601
      TinySecM__checkQueuedCrypto();
    }
  else {
#line 602
    if (TinySecM__RxByteCnt == TinySecM__rxlength) {
        if (macRecCount == 0) {
          macRecCount = TinySecM__blockSize;
          }
#line 605
        TinySecM__postIncrementalMAC(TinySecM__RxByteCnt - macRecCount, macRecCount);
        TinySecM__postIncrementalMACFinish();
        if (TinySecM__rxdecrypt) {
            if ((decryptRecCount & (TinySecM__blockSize - 1)) == 0) {
                TinySecM__postIncrementalDecrypt(decryptRecCount - TinySecM__blockSize, TinySecM__blockSize);
              }
            else {
                TinySecM__postIncrementalDecrypt(
                decryptRecCount - (decryptRecCount & (TinySecM__blockSize - 1)), 
                decryptRecCount & (TinySecM__blockSize - 1));
              }
          }
        else 
#line 616
          {
            nmemcpy(TinySecM__cleartext_rec_ptr->data, TinySecM__ciphertext_rec_ptr->enc, TinySecM__recDataLength);
          }
        TinySecM__checkQueuedCrypto();
        TinySecM__ciphertext_rec_ptr->cryptoDone = TRUE;
        TinySecM__cleartext_rec_ptr->group = TOS_AM_GROUP;
        if (TinySecM__ciphertext_rec_ptr->receiveDone) {
            TinySecM__TinySec__receiveDone(SUCCESS);
          }
      }
    else {
#line 625
      if (TinySecM__RxByteCnt == TinySecM__rxlength + 4) {
          TinySecM__ciphertext_rec_ptr->receiveDone = TRUE;
          if (TinySecM__ciphertext_rec_ptr->validMAC) {
            TinySecM__verifyMAC();
            }
#line 629
          if (TinySecM__ciphertext_rec_ptr->cryptoDone) {
              TinySecM__TinySec__receiveDone(SUCCESS);
            }
        }
      else 
#line 632
        {
          return FAIL;
        }
      }
    }
#line 636
  return SUCCESS;
}

static   
# 240 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
result_t MicaHighSpeedRadioTinySecM__TinySec__receiveInitDone(result_t result, 
uint16_t length, 
bool ts_enabled)
#line 242
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 243
    {
      MicaHighSpeedRadioTinySecM__msg_length = length;
      MicaHighSpeedRadioTinySecM__tinysec_rx_done = FALSE;
      if (result == SUCCESS) {
          if (ts_enabled) {
              MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__RX_STATE_TINYSEC;
            }
          else 
#line 249
            {
              MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__RX_STATE;


              MicaHighSpeedRadioTinySecM__tinysec_rx_done = TRUE;
            }
        }
      else 
#line 255
        {
          MicaHighSpeedRadioTinySecM__rec_ptr->length = 0;
          MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
        }
    }
#line 259
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static 
# 223 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__postIncrementalMAC(uint8_t incr_mac_start, uint8_t amount)
#line 223
{
  if (TinySecM__computeMACBuffer.computeMACWaiting) {
      TinySecM__computeMACBuffer.position += amount;
    }
  else 
#line 226
    {
      TinySecM__computeMACBuffer.computeMACWaiting = TRUE;
      TinySecM__computeMACBuffer.position = incr_mac_start;
      TinySecM__computeMACBuffer.amount = amount;
    }
  return SUCCESS;
}

static 









result_t TinySecM__postIncrementalDecrypt(uint8_t incr_decrypt_start, uint8_t amount)
#line 244
{
  if (TinySecM__decryptBuffer.decryptWaiting) {
      TinySecM__decryptBuffer.amount += amount;
    }
  else 
#line 247
    {
      TinySecM__decryptBuffer.decryptWaiting = TRUE;
      TinySecM__decryptBuffer.position = incr_decrypt_start;
      TinySecM__decryptBuffer.amount = amount;
    }
  return SUCCESS;
}

static result_t TinySecM__checkQueuedCrypto(void)
#line 255
{
  result_t result = SUCCESS;

/*BLAST recursion
  if (TinySecM__compute_mac_state == TinySecM__COMPUTE_MAC_BUSY || TinySecM__decrypt_state == TinySecM__DECRYPT_BUSY) {
    return SUCCESS;
    }
  if (TinySecM__computeMACBuffer.computeMACInitWaiting) {
      TinySecM__computeMACBuffer.computeMACInitWaiting = FALSE;
      result = rcombine(result, TinySecM__MACincrementalInit());
    }

  if (TinySecM__computeMACBuffer.computeMACWaiting) {
      TinySecM__computeMACBuffer.computeMACWaiting = FALSE;
      result = rcombine(result, TinySecM__computeMACIncremental(TinySecM__computeMACBuffer.position, 
      TinySecM__computeMACBuffer.amount));
    }

  if (TinySecM__computeMACBuffer.finishMACWaiting) {
      TinySecM__computeMACBuffer.finishMACWaiting = FALSE;
      result = rcombine(result, TinySecM__computeMACIncrementalFinish());
    }
  if (TinySecM__decryptBuffer.decryptInitWaiting) {
      TinySecM__decryptBuffer.decryptInitWaiting = FALSE;
      result = rcombine(result, TinySecM__decryptIncrementalInit());
    }
  if (TinySecM__decryptBuffer.decryptWaiting) {
      TinySecM__decryptBuffer.decryptWaiting = FALSE;
      if (TinySecM__decrypt_state == TinySecM__DECRYPT_INITIALIZED) {
          result = rcombine(result, TinySecM__decryptIncremental(TinySecM__decryptBuffer.position, 
          TinySecM__decryptBuffer.amount));
        }
      else {
#line 286
        if (TinySecM__decrypt_state == TinySecM__DECRYPT_IDLE) {
            result = rcombine(result, TinySecM__decryptIncrementalInit());
          }
        else 
#line 288
          {
            return FAIL;
          }
        }
    }
*/
#line 292
  return result;
}

static   
# 102 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCMAC.nc"
result_t CBCMAC__MAC__initIncrementalMAC(MACContext *context, 
uint16_t length)
{
#line 116
  uint8_t partial[CBCMAC__CBCMAC_BLOCK_SIZE];

  uint8_t numBlocks = length >> 3;

#line 119
  nmemset(partial, 0, 6);
  partial[6] = (numBlocks >> 8) & 0xff;
  partial[7] = numBlocks & 0xff;
  (
  (CBCMAC__CBCMACContext *)context->context)->length = length;
  ((CBCMAC__CBCMACContext *)context->context)->blockPos = 0;
  return CBCMAC__BlockCipher__encrypt(& context->cc, partial, (
  (CBCMAC__CBCMACContext *)context->context)->partial);
}

static   
# 212 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
result_t SkipJackM__BlockCipher__encrypt(CipherContext *context, 
uint8_t *plainBlock, 
uint8_t *cipherBlock)
{

  register uint8_t counter = 1;
  register uint8_t *skey = ((SkipJackM__SJContext *)context->context)->skey;
  register uint16_t w1;
#line 219
  register uint16_t w2;
#line 219
  register uint16_t w3;
#line 219
  register uint16_t w4;
#line 219
  register uint16_t tmp;
  register uint8_t bLeft;
#line 220
  register uint8_t bRight;



  w1 = (unsigned short )*plainBlock << 8L, w1 |= (unsigned short )*(plainBlock + 1);
  plainBlock += 2;
  w2 = (unsigned short )*plainBlock << 8L, w2 |= (unsigned short )*(plainBlock + 1);
  plainBlock += 2;
  w3 = (unsigned short )*plainBlock << 8L, w3 |= (unsigned short )*(plainBlock + 1);
  plainBlock += 2;
  w4 = (unsigned short )*plainBlock << 8L, w4 |= (unsigned short )*(plainBlock + 1);
  plainBlock += 2;

  while (counter < 9) 
    {
#line 234
      tmp = w4;
#line 234
      w4 = w3;
#line 234
      w3 = w2;
#line 234
      w2 = (bLeft = w1 >> 8, bRight = w1, bLeft ^= F[bRight ^ skey[0]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[3]], (bLeft << 8) | bRight);
#line 234
      w1 = (tmp ^ w2) ^ counter;
#line 234
      counter++;
#line 234
      skey += 4;
    }
#line 234
  ;
  while (counter < 17) 
    {
#line 236
      tmp = w1;
#line 236
      w1 = w4;
#line 236
      w4 = w3;
#line 236
      w3 = (tmp ^ w2) ^ counter;
#line 236
      w2 = (bLeft = tmp >> 8, bRight = tmp, bLeft ^= F[bRight ^ skey[0]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[3]], (bLeft << 8) | bRight);
#line 236
      counter++;
#line 236
      skey += 4;
    }
#line 236
  ;
  while (counter < 25) 
    {
#line 238
      tmp = w4;
#line 238
      w4 = w3;
#line 238
      w3 = w2;
#line 238
      w2 = (bLeft = w1 >> 8, bRight = w1, bLeft ^= F[bRight ^ skey[0]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[3]], (bLeft << 8) | bRight);
#line 238
      w1 = (tmp ^ w2) ^ counter;
#line 238
      counter++;
#line 238
      skey += 4;
    }
#line 238
  ;
  while (counter < 33) 
    {
#line 240
      tmp = w1;
#line 240
      w1 = w4;
#line 240
      w4 = w3;
#line 240
      w3 = (tmp ^ w2) ^ counter;
#line 240
      w2 = (bLeft = tmp >> 8, bRight = tmp, bLeft ^= F[bRight ^ skey[0]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[3]], (bLeft << 8) | bRight);
#line 240
      counter++;
#line 240
      skey += 4;
    }
#line 240
  ;

  *cipherBlock = (unsigned short )((w1 >> 8L) & 0xff), *(cipherBlock + 1) = (unsigned short )(w1 & 0xff);
  cipherBlock += 2;
  *cipherBlock = (unsigned short )((w2 >> 8L) & 0xff), *(cipherBlock + 1) = (unsigned short )(w2 & 0xff);
  cipherBlock += 2;
  *cipherBlock = (unsigned short )((w3 >> 8L) & 0xff), *(cipherBlock + 1) = (unsigned short )(w3 & 0xff);
  cipherBlock += 2;
  *cipherBlock = (unsigned short )((w4 >> 8L) & 0xff), *(cipherBlock + 1) = (unsigned short )(w4 & 0xff);
  cipherBlock += 2;
  SkipJackM__dumpBuffer("SkipJack.encrypt: cipherBlock", cipherBlock - 8, 8);
  return SUCCESS;
}

static   
# 143 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCMAC.nc"
result_t CBCMAC__MAC__incrementalMAC(MACContext *context, uint8_t *msg, 
uint16_t msgLen)
{
  uint8_t i;
#line 146
  uint8_t pos = ((CBCMAC__CBCMACContext *)context->context)->blockPos;
  uint8_t *partial = ((CBCMAC__CBCMACContext *)context->context)->partial;


  if (((CBCMAC__CBCMACContext *)context->context)->length < msgLen) {
      return FAIL;
    }


  for (i = 0; i < msgLen; i++) {

      partial[pos++] ^= msg[i];
      if (pos == 7) {
          if (!CBCMAC__BlockCipher__encrypt(& context->cc, partial, partial)) {
              return FAIL;
            }
          pos = 0;
        }
    }
  (
  (CBCMAC__CBCMACContext *)context->context)->length -= msgLen;
  ((CBCMAC__CBCMACContext *)context->context)->blockPos = pos;
  return SUCCESS;
}

static   
#line 188
result_t CBCMAC__MAC__getIncrementalMAC(MACContext *context, uint8_t *res, 
uint8_t macSize)
{
  uint8_t blockPos = ((CBCMAC__CBCMACContext *)context->context)->blockPos;
  uint8_t *partial = ((CBCMAC__CBCMACContext *)context->context)->partial;


  if ((!macSize || macSize > 8) || (
  (CBCMAC__CBCMACContext *)context->context)->length) {
      {
      }
#line 197
      ;

      return FAIL;
    }


  if (blockPos) {

      partial[++blockPos] ^= 1;
      if (!CBCMAC__BlockCipher__encrypt(& context->cc, partial, partial)) {
          return FAIL;
        }
      ((CBCMAC__CBCMACContext *)context->context)->blockPos = 0;
    }
  nmemcpy(res, ((CBCMAC__CBCMACContext *)context->context)->partial, macSize);
  return SUCCESS;
}

static 
# 452 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/TinySecM.nc"
result_t TinySecM__verifyMAC(void)
#line 452
{
  int i;

  if (!TinySecM__initialized) {
    return FAIL;
    }

  if (! TinySecM__ciphertext_rec_ptr->validMAC) {
      return FAIL;
    }

  {
  }
#line 463
  ;










  for (i = 0; i < 4; i++) {
      if (TinySecM__ciphertext_rec_ptr->calc_mac[i] != TinySecM__ciphertext_rec_ptr->mac[i]) {
          {
          }
#line 476
          ;


          TinySecM__ciphertext_rec_ptr->validMAC = FALSE;
          TinySecM__cleartext_rec_ptr->crc = 0;
          return SUCCESS;
        }
    }
  TinySecM__cleartext_rec_ptr->crc = 1;
  return SUCCESS;
}

static 
#line 297
result_t TinySecM__decryptIncrementalInit(void)
#line 297
{
  uint8_t decrypt_iv[TinySecM__TINYSECM_MAX_BLOCK_SIZE];
  int i;
  result_t result;
  uint16_t ivLengthRemaining = sizeof  TinySecM__ciphertext_rec_ptr->addr + 
  sizeof  TinySecM__ciphertext_rec_ptr->type + 
  sizeof  TinySecM__ciphertext_rec_ptr->length;

  TinySecM__test_state = FALSE;
  if (TinySecM__decrypt_state != TinySecM__DECRYPT_IDLE || !TinySecM__initialized) {
    return FAIL;
    }
  TinySecM__decrypt_state = TinySecM__DECRYPT_BUSY;
  TinySecM__interruptEnable();

  if (ivLengthRemaining > TinySecM__blockSize - 4) {
    ivLengthRemaining = TinySecM__blockSize - 4;
    }

  nmemcpy(decrypt_iv, TinySecM__ciphertext_rec_ptr->iv, 4);

  nmemcpy(decrypt_iv + 4, & TinySecM__ciphertext_rec_ptr->addr, 
  ivLengthRemaining);


  for (i = ivLengthRemaining + 4; i < TinySecM__blockSize; i++) {
      decrypt_iv[i] = 0;
    }


  if (TinySecM__recDataLength < TinySecM__blockSize) {
      result = TinySecM__BlockCipherMode__initIncrementalDecrypt(&TinySecM__cipherModeContext, 
      decrypt_iv, 
      TinySecM__blockSize);
      {
      }
#line 331
      ;
    }
  else {
      result = TinySecM__BlockCipherMode__initIncrementalDecrypt(&TinySecM__cipherModeContext, 
      decrypt_iv, 
      TinySecM__recDataLength);
      {
      }
#line 337
      ;
    }

  TinySecM__interruptDisable();
  TinySecM__decrypt_state = TinySecM__DECRYPT_INITIALIZED;
  result = rcombine(result, TinySecM__checkQueuedCrypto());
  return result;
}

static   
# 440 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCModeM.nc"
result_t CBCModeM__BlockCipherMode__initIncrementalDecrypt(
CipherModeContext *context, 
uint8_t *IV, 
uint16_t length)
{
  CBCModeM__CBCModeContext *mcontext = (CBCModeM__CBCModeContext *)context->context;

#line 446
  if (!length) {
#line 446
    return SUCCESS;
    }
#line 447
  if (length < mcontext->bsize) {
#line 447
    return FAIL;
    }
  mcontext->remaining = length;

  if (CBCModeM__BlockCipher__encrypt(& context->cc, IV, mcontext->spill1) != 
  SUCCESS) {
      return FAIL;
    }
  CBCModeM__dumpBuffer("E(IV)", mcontext->spill1, 8);

  mcontext->offset = mcontext->completed = 0;
  mcontext->accum = FALSE;


  if (length == mcontext->bsize) {
      mcontext->state = CBCModeM__ONE_BLOCK;
    }
  else {
#line 463
    if (length <= mcontext->bsize * 2) {
        mcontext->state = CBCModeM__TWO_LEFT_A;
      }
    else 
#line 465
      {
        mcontext->state = CBCModeM__GENERAL;
      }
    }
#line 468
  return SUCCESS;
}

static   
# 267 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/SkipJackM.nc"
result_t SkipJackM__BlockCipher__decrypt(CipherContext *context, 
uint8_t *cipherBlock, 
uint8_t *plainBlock)
{
  register uint8_t counter = 32;
  register uint8_t *skey = ((SkipJackM__SJContext *)context->context)->skey + 124;
  register uint16_t w1;
#line 273
  register uint16_t w2;
#line 273
  register uint16_t w3;
#line 273
  register uint16_t w4;
#line 273
  register uint16_t tmp;
  register uint8_t bLeft;
#line 274
  register uint8_t bRight;

  SkipJackM__dumpBuffer("SkipJack.decrypt: cipherBlock", plainBlock, 8);

  w1 = (unsigned short )*cipherBlock << 8L, w1 |= (unsigned short )*(cipherBlock + 1);
  cipherBlock += 2;
  w2 = (unsigned short )*cipherBlock << 8L, w2 |= (unsigned short )*(cipherBlock + 1);
  cipherBlock += 2;
  w3 = (unsigned short )*cipherBlock << 8L, w3 |= (unsigned short )*(cipherBlock + 1);
  cipherBlock += 2;
  w4 = (unsigned short )*cipherBlock << 8L, w4 |= (unsigned short )*(cipherBlock + 1);

  while (counter > 24) 
    {
#line 287
      tmp = w1;
#line 287
      w1 = (bLeft = w2 >> 8, bRight = w2, bRight ^= F[bLeft ^ skey[3]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[0]], (bLeft << 8) | bRight);
#line 287
      w2 = (w1 ^ w3) ^ counter;
#line 287
      w3 = w4;
#line 287
      w4 = tmp;
#line 287
      counter--;
#line 287
      skey -= 4;
    }
#line 287
  ;
  while (counter > 16) 
    {
#line 289
      tmp = w4;
#line 289
      w4 = (w1 ^ w2) ^ counter;
#line 289
      w1 = (bLeft = w2 >> 8, bRight = w2, bRight ^= F[bLeft ^ skey[3]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[0]], (bLeft << 8) | bRight);
#line 289
      w2 = w3;
#line 289
      w3 = tmp;
#line 289
      counter--;
#line 289
      skey -= 4;
    }
#line 289
  ;
  while (counter > 8) 
    {
#line 291
      tmp = w1;
#line 291
      w1 = (bLeft = w2 >> 8, bRight = w2, bRight ^= F[bLeft ^ skey[3]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[0]], (bLeft << 8) | bRight);
#line 291
      w2 = (w1 ^ w3) ^ counter;
#line 291
      w3 = w4;
#line 291
      w4 = tmp;
#line 291
      counter--;
#line 291
      skey -= 4;
    }
#line 291
  ;
  while (counter > 0) 
    {
#line 293
      tmp = w4;
#line 293
      w4 = (w1 ^ w2) ^ counter;
#line 293
      w1 = (bLeft = w2 >> 8, bRight = w2, bRight ^= F[bLeft ^ skey[3]], bLeft ^= F[bRight ^ skey[2]], bRight ^= F[bLeft ^ skey[1]], bLeft ^= F[bRight ^ skey[0]], (bLeft << 8) | bRight);
#line 293
      w2 = w3;
#line 293
      w3 = tmp;
#line 293
      counter--;
#line 293
      skey -= 4;
    }
#line 293
  ;

  *plainBlock = (unsigned short )((w1 >> 8L) & 0xff), *(plainBlock + 1) = (unsigned short )(w1 & 0xff);
  plainBlock += 2;
  *plainBlock = (unsigned short )((w2 >> 8L) & 0xff), *(plainBlock + 1) = (unsigned short )(w2 & 0xff);
  plainBlock += 2;
  *plainBlock = (unsigned short )((w3 >> 8L) & 0xff), *(plainBlock + 1) = (unsigned short )(w3 & 0xff);
  plainBlock += 2;
  *plainBlock = (unsigned short )((w4 >> 8L) & 0xff), *(plainBlock + 1) = (unsigned short )(w4 & 0xff);

  SkipJackM__dumpBuffer("SkipJack.decrypt: plainBlock", plainBlock - 6, 8);
  return SUCCESS;
}

static  
# 122 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/MicaHighSpeedRadioTinySecM.nc"
void MicaHighSpeedRadioTinySecM__packetReceived(void)
#line 122
{
  TOS_MsgPtr tmp;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 125
    {

      tmp = (TOS_MsgPtr )MicaHighSpeedRadioTinySecM__rec_ptr;
      MicaHighSpeedRadioTinySecM__swapLengthAndGroup(tmp);

      MicaHighSpeedRadioTinySecM__state = MicaHighSpeedRadioTinySecM__IDLE_STATE;
    }
#line 131
    __nesc_atomic_end(__nesc_atomic); }
  tmp = MicaHighSpeedRadioTinySecM__Receive__receive(tmp);
  if (tmp != 0) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 134
        {
          MicaHighSpeedRadioTinySecM__rec_ptr = (TOS_Msg_TinySecCompat *)tmp;
        }
#line 136
        __nesc_atomic_end(__nesc_atomic); }
    }
  MicaHighSpeedRadioTinySecM__ChannelMon__startSymbolSearch();
}

static   
# 70 "C:/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR__Random__rand(void)
#line 70
{
  bool endbit;
  uint16_t tmpShiftReg;

#line 73
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
    {
      tmpShiftReg = RandomLFSR__shiftReg;
      endbit = (tmpShiftReg & 0x8000) != 0;
      tmpShiftReg <<= 1;
      if (endbit) {
        tmpShiftReg ^= 0x100b;
        }
#line 79
      tmpShiftReg++;
      RandomLFSR__shiftReg = tmpShiftReg;
      tmpShiftReg = tmpShiftReg ^ RandomLFSR__mask;
    }
#line 82
    __nesc_atomic_end(__nesc_atomic); }
  return tmpShiftReg;
}

static   
# 172 "C:/cygwin/opt/tinyos-1.x/tos/lib/TinySec/CBCModeM.nc"
result_t CBCModeM__BlockCipherMode__encrypt(CipherModeContext *context, 
uint8_t *plainBlocks, 
uint8_t *cipherBlocks, 
uint16_t numBytes, uint8_t *IV)
{
  uint8_t i;
#line 177
  uint8_t j;
#line 177
  uint8_t t;
#line 177
  uint8_t bsize;
#line 177
  uint8_t bsize2;
  uint16_t bc = 0;
  uint8_t spillblock[CBCModeM__CBCMODE_MAX_BLOCK_SIZE];
  uint8_t eIV[CBCModeM__CBCMODE_MAX_BLOCK_SIZE];

  bsize = ((CBCModeM__CBCModeContext *)context->context)->bsize;
  bsize2 = bsize + bsize;
  if (numBytes == 0) {
      return SUCCESS;
    }


  if (numBytes / 256 > bsize) {
      return FAIL;
    }

  if (numBytes < bsize) {
      return FAIL;
    }
  CBCModeM__dumpBuffer("CBC.encrypt orig", plainBlocks, numBytes);

  if (CBCModeM__BlockCipher__encrypt(& context->cc, IV, eIV) == FAIL) {
      return FAIL;
    }
  IV = eIV;


  if (numBytes == bsize) {


      for (j = 0; j < bsize; j++) {
          cipherBlocks[bc + j] = plainBlocks[bc + j] ^ IV[j];
        }

      if (
#line 210
      CBCModeM__BlockCipher__encrypt(& context->cc, cipherBlocks + bc, 
      cipherBlocks + bc) == FAIL) {
          return FAIL;
        }
      return SUCCESS;
    }



  if (numBytes > bsize2) {
      for (bc = 0; bc < numBytes - bsize2; bc += bsize) {

          for (j = 0; j < bsize; j++) {
              cipherBlocks[bc + j] = plainBlocks[bc + j] ^ IV[j];
            }

          if (
#line 225
          CBCModeM__BlockCipher__encrypt(& context->cc, cipherBlocks + bc, 
          cipherBlocks + bc) == FAIL) {
              return FAIL;
            }
          IV = cipherBlocks + bc;
        }
    }

  {
  }
#line 233
  ;
#line 249
  for (j = 0; j < bsize; j++) {
      spillblock[j] = plainBlocks[bc + j] ^ IV[j];
    }
  if (CBCModeM__BlockCipher__encrypt(& context->cc, spillblock, spillblock) == 
  FAIL) {
      return FAIL;
    }
  CBCModeM__dumpBuffer("CBC.encrypt spill:", spillblock, bsize);

  j = numBytes - bc - bsize;
  {
  }
#line 259
  ;


  for (i = 0; i < j; i++) {


      t = plainBlocks[bc + bsize + i];
      cipherBlocks[bc + bsize + i] = spillblock[i];
      spillblock[i] ^= t;
    }




  if (
#line 272
  CBCModeM__BlockCipher__encrypt(& context->cc, spillblock, 
  cipherBlocks + bc) == FAIL) {
      return FAIL;
    }
  CBCModeM__dumpBuffer("CBC.encrypt cipher:", cipherBlocks, numBytes);
  return SUCCESS;
}

# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void __attribute((interrupt))   __vector_15(void)
#line 167
{
  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 168
    {
      if (HPLClock__set_flag) {
          HPLClock__mscale = HPLClock__nextScale;
          HPLClock__nextScale |= 0x8;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLClock__nextScale;

          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLClock__minterval;
          HPLClock__set_flag = 0;
        }
    }
#line 177
    __nesc_atomic_end(__nesc_atomic); }
  HPLClock__Clock__fire();
}

static   
# 348 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__ByteComm__rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 348
{

  switch (FramerM__gRxState) {

      case FramerM__RXSTATE_NOSYNC: 
        if (data == FramerM__HDLC_FLAG_BYTE && FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length == 0) {
            FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0;
            FramerM__gRxByteCnt = FramerM__gRxRunningCRC = 0;
            FramerM__gpRxBuf = (uint8_t *)FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].pMsg;
            FramerM__gRxState = FramerM__RXSTATE_PROTO;
          }
      break;

      case FramerM__RXSTATE_PROTO: 
        if (data == FramerM__HDLC_FLAG_BYTE) {
            break;
          }
      FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Proto = data;
      FramerM__gRxRunningCRC = crcByte(FramerM__gRxRunningCRC, data);
      switch (data) {
          case FramerM__PROTO_PACKET_ACK: 
            FramerM__gRxState = FramerM__RXSTATE_TOKEN;
          break;
          case FramerM__PROTO_PACKET_NOACK: 
            FramerM__gRxState = FramerM__RXSTATE_INFO;
          break;
          default: 
            FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
          break;
        }
      break;

      case FramerM__RXSTATE_TOKEN: 
        if (data == FramerM__HDLC_FLAG_BYTE) {
            FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
          }
        else {
#line 384
          if (data == FramerM__HDLC_CTLESC_BYTE) {
              FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0x20;
            }
          else {
              FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token ^= data;
              FramerM__gRxRunningCRC = crcByte(FramerM__gRxRunningCRC, FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token);
              FramerM__gRxState = FramerM__RXSTATE_INFO;
            }
          }
#line 392
      break;


      case FramerM__RXSTATE_INFO: 
        if (FramerM__gRxByteCnt > FramerM__HDLC_MTU) {
            FramerM__gRxByteCnt = FramerM__gRxRunningCRC = 0;
            FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length = 0;
            FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0;
            FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
          }
        else {
#line 402
          if (data == FramerM__HDLC_CTLESC_BYTE) {
              FramerM__gRxState = FramerM__RXSTATE_ESC;
            }
          else {
#line 405
            if (data == FramerM__HDLC_FLAG_BYTE) {
                if (FramerM__gRxByteCnt >= 2) {
                    uint16_t usRcvdCRC = FramerM__gpRxBuf[FramerM__gRxByteCnt - 1] & 0xff;

#line 408
                    usRcvdCRC = (usRcvdCRC << 8) | (FramerM__gpRxBuf[FramerM__gRxByteCnt - 2] & 0xff);
                    if (usRcvdCRC == FramerM__gRxRunningCRC) {
                        FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length = FramerM__gRxByteCnt - 2;
                        TOS_post(FramerM__PacketRcvd);
                        FramerM__gRxHeadIndex++;
#line 412
                        FramerM__gRxHeadIndex %= FramerM__HDLC_QUEUESIZE;
                      }
                    else {
                        FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length = 0;
                        FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0;
                      }
                    if (FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length == 0) {
                        FramerM__gpRxBuf = (uint8_t *)FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].pMsg;
                        FramerM__gRxState = FramerM__RXSTATE_PROTO;
                      }
                    else {
                        FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
                      }
                  }
                else {
                    FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length = 0;
                    FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0;
                    FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
                  }
                FramerM__gRxByteCnt = FramerM__gRxRunningCRC = 0;
              }
            else {
                FramerM__gpRxBuf[FramerM__gRxByteCnt] = data;
                if (FramerM__gRxByteCnt >= 2) {
                    FramerM__gRxRunningCRC = crcByte(FramerM__gRxRunningCRC, FramerM__gpRxBuf[FramerM__gRxByteCnt - 2]);
                  }
                FramerM__gRxByteCnt++;
              }
            }
          }
#line 440
      break;

      case FramerM__RXSTATE_ESC: 
        if (data == FramerM__HDLC_FLAG_BYTE) {

            FramerM__gRxByteCnt = FramerM__gRxRunningCRC = 0;
            FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Length = 0;
            FramerM__gMsgRcvTbl[FramerM__gRxHeadIndex].Token = 0;
            FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
          }
        else {
            data = data ^ 0x20;
            FramerM__gpRxBuf[FramerM__gRxByteCnt] = data;
            if (FramerM__gRxByteCnt >= 2) {
                FramerM__gRxRunningCRC = crcByte(FramerM__gRxRunningCRC, FramerM__gpRxBuf[FramerM__gRxByteCnt - 2]);
              }
            FramerM__gRxByteCnt++;
            FramerM__gRxState = FramerM__RXSTATE_INFO;
          }
      break;

      default: 
        FramerM__gRxState = FramerM__RXSTATE_NOSYNC;
      break;
    }

  return SUCCESS;
}

static  
# 106 "SecureTOSBaseM.nc"
void SecureTOSBaseM__UARTRcvdTask(void)
#line 106
{
  result_t Result;

  {
  }
#line 109
  ;
  SecureTOSBaseM__gpTxMsg->group = TOS_AM_GROUP;
  SecureTOSBaseM__gpTxMsg->sendSecurityMode = TINYSEC_AUTH_ONLY;
  Result = SecureTOSBaseM__RadioSend__send(SecureTOSBaseM__gpTxMsg);

  if (Result != SUCCESS) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
        SecureTOSBaseM__gfTxFlags = 0;
#line 115
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      SecureTOSBaseM__Leds__greenToggle();
    }
}

static   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM__HPLUART__putDone(void)
#line 87
{
  bool oldState;

  { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 90
    {
      {
      }
#line 91
      ;
      oldState = UARTM__state;
      if (UARTM__state == TRUE) //JHALA
	  UARTM__state = FALSE;
      
    }
#line 94
    __nesc_atomic_end(__nesc_atomic); }

  if (oldState == TRUE) { //JHALA
      UARTM__ByteComm__txDone();
      UARTM__ByteComm__txByteReady(TRUE);
    }
  return SUCCESS;
}

static 
# 469 "C:/cygwin/opt/tinyos-1.x/tos/system/FramerM.nc"
result_t FramerM__TxArbitraryByte(uint8_t Byte)
#line 469
{
  if (Byte == FramerM__HDLC_FLAG_BYTE || Byte == FramerM__HDLC_CTLESC_BYTE) {
      { __blockattribute__((atomic)) __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 471
        {
          FramerM__gPrevTxState = FramerM__gTxState;
          FramerM__gTxState = FramerM__TXSTATE_ESC;
          FramerM__gTxEscByte = Byte;
        }
#line 475
        __nesc_atomic_end(__nesc_atomic); }
      Byte = FramerM__HDLC_CTLESC_BYTE;
    }

  return FramerM__ByteComm__txByte(Byte);
}

