//#define dbg(mode, format, ...) ((void)0)
//#define dbg_clear(mode, format, ...) ((void)0)
//#define dbg_active(mode) 0

/*******************************************/
/** BLAST flags, stubs etc                 */
int __BLAST_enabled __attribute__((lock));
int __BLAST_atomic_lock __attribute__((lock)) ;

void errorFn() { 
 ERROR: goto ERROR;
}



/*******************************************/







# 60 "/usr/local/avr/include/inttypes.h"
typedef signed char int8_t;





typedef unsigned char uint8_t;
# 83 "/usr/local/avr/include/inttypes.h" 3
typedef int int16_t;




typedef unsigned int uint16_t;










typedef long int32_t;




typedef unsigned long uint32_t;
#line 117
typedef long long int64_t;




typedef unsigned long long uint64_t;
#line 134
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 213 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef unsigned int size_t;
#line 325
typedef int wchar_t;
# 60 "/usr/local/avr/include/stdlib.h"
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;


typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *, const void *);
# 151 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int ptrdiff_t;
# 85 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
typedef unsigned char bool;
enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};



enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};
static inline 

uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t  result_t;
static inline 






result_t rcombine(result_t r1, result_t r2);
#line 128
enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 81 "/usr/local/avr/include/avr/pgmspace.h"
typedef void __attribute((__progmem__)) prog_void;
typedef char __attribute((__progmem__)) prog_char;
typedef unsigned char __attribute((__progmem__)) prog_uchar;
typedef int __attribute((__progmem__)) prog_int;
typedef long __attribute((__progmem__)) prog_long;
typedef long long __attribute((__progmem__)) prog_long_long;
# 124 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};
static inline 
void TOSH_wait(void);
static inline 




void TOSH_sleep(void);









typedef uint8_t __nesc_atomic_t;

__inline __nesc_atomic_t  __nesc_atomic_start(void );






__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg);
static 



__inline void __nesc_enable_interrupt(void);
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_RED_LED_PIN(void);
#line 57
static __inline void TOSH_CLR_RED_LED_PIN(void);
#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void);
static __inline void TOSH_SET_YELLOW_LED_PIN(void);
#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void);
static __inline void TOSH_SET_GREEN_LED_PIN(void);
#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void);

static __inline void TOSH_SET_UD_PIN(void);
#line 61
static __inline void TOSH_CLR_UD_PIN(void);
static __inline void TOSH_SET_INC_PIN(void);
#line 62
static __inline void TOSH_CLR_INC_PIN(void);
static __inline void TOSH_SET_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void);
#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void);
static __inline void TOSH_SET_POT_POWER_PIN(void);
#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void);
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void);
#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void);

static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void);









static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void);
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void);
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void);

static __inline void TOSH_MAKE_PW0_OUTPUT(void);
static __inline void TOSH_MAKE_PW1_OUTPUT(void);
static __inline void TOSH_MAKE_PW2_OUTPUT(void);
static __inline void TOSH_MAKE_PW3_OUTPUT(void);
static __inline void TOSH_MAKE_PW4_OUTPUT(void);
static __inline void TOSH_MAKE_PW5_OUTPUT(void);
static __inline void TOSH_MAKE_PW6_OUTPUT(void);
static __inline void TOSH_MAKE_PW7_OUTPUT(void);
#line 101
static __inline void TOSH_SET_ONE_WIRE_PIN(void);
#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void);
static inline 
void TOSH_SET_PIN_DIRECTIONS(void );
#line 146
enum __nesc_unnamed4248 {
  TOSH_ADC_PORTMAPSIZE = 12
};


enum __nesc_unnamed4249 {

  TOSH_ACTUAL_VOLTAGE_PORT = 7
};
enum __nesc_unnamed4250 {

  TOS_ADC_VOLTAGE_PORT = 7
};
# 54 "C:/cygwin/opt/tinyos-1.x/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4251 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 




  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 59 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
typedef struct __nesc_unnamed4252 {
  void (*tp)(void);
} TOSH_sched_entry_T;

enum __nesc_unnamed4253 {
  TOSH_MAX_TASKS = 8, 
  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
volatile uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;
static inline 

void TOSH_wait(void );
static inline void TOSH_sleep(void );
static inline 
void TOSH_sched_init(void );
#line 98
bool  TOS_post(void (*tp)(void));
static inline 
#line 139
bool TOSH_run_next_task(void);
static inline 
#line 162
void TOSH_run_task(void);
# 39 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.h"
enum __nesc_unnamed4254 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 1
};
# 32 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/Clock.h"
enum __nesc_unnamed4255 {
  TOS_I1000PS = 33, TOS_S1000PS = 1, 
  TOS_I100PS = 41, TOS_S100PS = 2, 
  TOS_I10PS = 102, TOS_S10PS = 3, 
  TOS_I4096PS = 1, TOS_S4096PS = 2, 
  TOS_I2048PS = 2, TOS_S2048PS = 2, 
  TOS_I1024PS = 1, TOS_S1024PS = 3, 
  TOS_I512PS = 2, TOS_S512PS = 3, 
  TOS_I256PS = 4, TOS_S256PS = 3, 
  TOS_I128PS = 8, TOS_S128PS = 3, 
  TOS_I64PS = 16, TOS_S64PS = 3, 
  TOS_I32PS = 32, TOS_S32PS = 3, 
  TOS_I16PS = 64, TOS_S16PS = 3, 
  TOS_I8PS = 128, TOS_S8PS = 3, 
  TOS_I4PS = 128, TOS_S4PS = 4, 
  TOS_I2PS = 128, TOS_S2PS = 5, 
  TOS_I1PS = 128, TOS_S1PS = 6, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4256 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 128
};
static  result_t PotM___Pot___init(uint8_t arg_0xa26e060);
static  result_t HPLPotC___Pot___finalise(void);
static  result_t HPLPotC___Pot___decrease(void);
static  result_t HPLPotC___Pot___increase(void);
static  result_t HPLInit___init(void);
static  result_t BlinkM___StdControl___init(void);
static  result_t BlinkM___StdControl___start(void);
static  result_t BlinkM___Timer___fired(void);
static   result_t TimerM___Clock___fire(void);
static  result_t TimerM___StdControl___init(void);
static  result_t TimerM___StdControl___start(void);
static  result_t TimerM___Timer___default___fired(uint8_t arg_0xa2bd460);
static  result_t TimerM___Timer___start(uint8_t arg_0xa2bd460, char arg_0xa2a5448, uint32_t arg_0xa2a55a0);
static   void HPLClock___Clock___setInterval(uint8_t arg_0xa2d6b60);
static   result_t HPLClock___Clock___setRate(char arg_0xa2d6060, char arg_0xa2d61a0);
static   uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void);
static   result_t LedsC___Leds___init(void);
static   result_t LedsC___Leds___redOff(void);
static   result_t LedsC___Leds___redToggle(void);
static   result_t LedsC___Leds___redOn(void);
static  
# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
result_t RealMain___hardwareInit(void);
static  
# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
result_t RealMain___Pot___init(uint8_t arg_0xa26e060);
static  
# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t RealMain___StdControl___init(void);
static  





result_t RealMain___StdControl___start(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void);
static  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
result_t PotM___HPLPot___finalise(void);
static  
#line 59
result_t PotM___HPLPot___decrease(void);
static  






result_t PotM___HPLPot___increase(void);
# 91 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
uint8_t PotM___potSetting;
static inline 
void PotM___setPot(uint8_t value);
static inline  
#line 106
result_t PotM___Pot___init(uint8_t initialSetting);
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___decrease(void);
static inline  







result_t HPLPotC___Pot___increase(void);
static inline  







result_t HPLPotC___Pot___finalise(void);
static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit___init(void);
static   
# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t BlinkM___Leds___init(void);
static   
#line 81
result_t BlinkM___Leds___redToggle(void);
static  
# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t BlinkM___Timer___start(char arg_0xa2a5448, uint32_t arg_0xa2a55a0);
static inline  
# 52 "BlinkM.nc"
result_t BlinkM___StdControl___init(void);
static inline  









result_t BlinkM___StdControl___start(void);
static inline  
#line 84
result_t BlinkM___Timer___fired(void);
static   
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t TimerM___PowerManagement___adjustPower(void);
static   
# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
void TimerM___Clock___setInterval(uint8_t arg_0xa2d6b60);
static   
#line 96
result_t TimerM___Clock___setRate(char arg_0xa2d6060, char arg_0xa2d61a0);
static  
# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t TimerM___Timer___fired(
# 45 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t arg_0xa2bd460);









uint32_t TimerM___mState;
uint8_t TimerM___setIntervalFlag;
uint8_t TimerM___mScale;
#line 57
uint8_t TimerM___mInterval;
int8_t TimerM___queue_head;
int8_t TimerM___queue_tail;
uint8_t TimerM___queue_size;
uint8_t TimerM___queue[NUM_TIMERS];

struct TimerM___timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM___mTimerList[NUM_TIMERS];

enum TimerM_____nesc_unnamed4257 {
  TimerM___maxTimerInterval = 230
};
static inline  result_t TimerM___StdControl___init(void);
static inline  








result_t TimerM___StdControl___start(void);
static inline  









result_t TimerM___Timer___start(uint8_t id, char type, 
uint32_t interval);
#line 116
static void TimerM___adjustInterval(void);
static inline   
#line 154
result_t TimerM___Timer___default___fired(uint8_t id);
static inline 


void TimerM___enqueue(uint8_t value);
static inline 






uint8_t TimerM___dequeue(void);
static inline  








void TimerM___signalOneTimer(void);
static inline  




void TimerM___HandleFire(void);
static inline   
#line 204
result_t TimerM___Clock___fire(void);
static   
# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLClock___Clock___fire(void);
# 54 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
uint8_t HPLClock___set_flag;
uint8_t HPLClock___mscale;
#line 55
uint8_t HPLClock___nextScale;
#line 55
uint8_t HPLClock___minterval;
static inline   
#line 87
void HPLClock___Clock___setInterval(uint8_t value);
static inline   
#line 149
result_t HPLClock___Clock___setRate(char interval, char scale);
#line 167
void __attribute((interrupt))   __vector_15(void);
# 51 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
bool HPLPowerManagementM___disabled = TRUE;

enum HPLPowerManagementM_____nesc_unnamed4258 {
  HPLPowerManagementM___IDLE = 0, 
  HPLPowerManagementM___ADC_NR = 1 << 3, 
  HPLPowerManagementM___POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM___POWER_DOWN = 1 << 3
};
static inline 


uint8_t HPLPowerManagementM___getPowerLevel(void);
static inline  
#line 85
void HPLPowerManagementM___doAdjustment(void);
static   
#line 103
uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void);
# 50 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
uint8_t LedsC___ledsOn;

enum LedsC_____nesc_unnamed4259 {
  LedsC___RED_BIT = 1, 
  LedsC___GREEN_BIT = 2, 
  LedsC___YELLOW_BIT = 4
};
static inline   
result_t LedsC___Leds___init(void);
static inline   









result_t LedsC___Leds___redOn(void);
static inline   







result_t LedsC___Leds___redOff(void);
static inline   







result_t LedsC___Leds___redToggle(void);
# 101 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_ONE_WIRE_PIN(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 5;
}

#line 101
static __inline void TOSH_MAKE_ONE_WIRE_INPUT(void)
#line 101
{
#line 101
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) &= ~(1 << 5);
}

#line 65
static __inline void TOSH_SET_BOOST_ENABLE_PIN(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 4;
}

#line 65
static __inline void TOSH_MAKE_BOOST_ENABLE_OUTPUT(void)
#line 65
{
#line 65
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 4;
}

#line 59
static __inline void TOSH_SET_GREEN_LED_PIN(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_SET_YELLOW_LED_PIN(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_SET_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}








static __inline void TOSH_MAKE_FLASH_SELECT_INPUT(void)
#line 67
{
#line 67
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 64
static __inline void TOSH_SET_POT_POWER_PIN(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) |= 1 << 7;
}











static __inline void TOSH_MAKE_RFM_TXD_OUTPUT(void)
#line 77
{
#line 77
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 3;
}

#line 79
static __inline void TOSH_MAKE_RFM_CTL1_OUTPUT(void)
#line 79
{
#line 79
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 6;
}

#line 78
static __inline void TOSH_MAKE_RFM_CTL0_OUTPUT(void)
#line 78
{
#line 78
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 7;
}

static __inline void TOSH_MAKE_PW0_OUTPUT(void)
#line 81
{
#line 81
  ;
}

#line 82
static __inline void TOSH_MAKE_PW1_OUTPUT(void)
#line 82
{
#line 82
  ;
}

#line 83
static __inline void TOSH_MAKE_PW2_OUTPUT(void)
#line 83
{
#line 83
  ;
}

#line 84
static __inline void TOSH_MAKE_PW3_OUTPUT(void)
#line 84
{
#line 84
  ;
}

#line 85
static __inline void TOSH_MAKE_PW4_OUTPUT(void)
#line 85
{
#line 85
  ;
}

#line 86
static __inline void TOSH_MAKE_PW5_OUTPUT(void)
#line 86
{
#line 86
  ;
}

#line 87
static __inline void TOSH_MAKE_PW6_OUTPUT(void)
#line 87
{
#line 87
  ;
}

#line 88
static __inline void TOSH_MAKE_PW7_OUTPUT(void)
#line 88
{
#line 88
  ;
}

#line 64
static __inline void TOSH_MAKE_POT_POWER_OUTPUT(void)
#line 64
{
#line 64
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) |= 1 << 7;
}

#line 63
static __inline void TOSH_MAKE_POT_SELECT_OUTPUT(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 5;
}

#line 59
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void)
#line 59
{
#line 59
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 1;
}

#line 58
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void)
#line 58
{
#line 58
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 0;
}

#line 57
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 2;
}

static inline 
#line 103
void TOSH_SET_PIN_DIRECTIONS(void )
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) = 0x00;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) = 0x02;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x03 + 0x20) = 0x02;
  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();
  TOSH_MAKE_POT_SELECT_OUTPUT();
  TOSH_MAKE_POT_POWER_OUTPUT();

  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();

  TOSH_MAKE_RFM_CTL0_OUTPUT();
  TOSH_MAKE_RFM_CTL1_OUTPUT();
  TOSH_MAKE_RFM_TXD_OUTPUT();
  TOSH_SET_POT_POWER_PIN();



  TOSH_MAKE_FLASH_SELECT_INPUT();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();


  TOSH_MAKE_BOOST_ENABLE_OUTPUT();
  TOSH_SET_BOOST_ENABLE_PIN();

  TOSH_MAKE_ONE_WIRE_INPUT();
  TOSH_SET_ONE_WIRE_PIN();
}

static inline  
# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit___init(void)
#line 57
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 47 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
inline static  result_t RealMain___hardwareInit(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLInit___init();
#line 47

#line 47
  return result;
#line 47
}
#line 47
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}

#line 61
static __inline void TOSH_SET_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

static inline  
# 74 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___finalise(void)
#line 74
{
  TOSH_SET_UD_PIN();
  TOSH_SET_INC_PIN();
  return SUCCESS;
}

# 74 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___finalise(void){
#line 74
  unsigned char result;
#line 74

#line 74
  result = HPLPotC___Pot___finalise();
#line 74

#line 74
  return result;
#line 74
}
#line 74
# 63 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_SET_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) |= 1 << 5;
}

#line 62
static __inline void TOSH_CLR_INC_PIN(void)
#line 62
{
#line 62
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

#line 63
static __inline void TOSH_CLR_POT_SELECT_PIN(void)
#line 63
{
#line 63
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x12 + 0x20) &= ~(1 << 5);
}

#line 61
static __inline void TOSH_CLR_UD_PIN(void)
#line 61
{
#line 61
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline  
# 65 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___increase(void)
#line 65
{
  TOSH_CLR_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 67 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___increase(void){
#line 67
  unsigned char result;
#line 67

#line 67
  result = HPLPotC___Pot___increase();
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 56 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLPotC.nc"
result_t HPLPotC___Pot___decrease(void)
#line 56
{
  TOSH_SET_UD_PIN();
  TOSH_CLR_POT_SELECT_PIN();
  TOSH_SET_INC_PIN();
  TOSH_CLR_INC_PIN();
  TOSH_SET_POT_SELECT_PIN();
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM___HPLPot___decrease(void){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLPotC___Pot___decrease();
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline 
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
void PotM___setPot(uint8_t value)
#line 93
{
  uint8_t i;

#line 95
  for (i = 0; i < 151; i++) 
    PotM___HPLPot___decrease();

  for (i = 0; i < value; i++) 
    PotM___HPLPot___increase();

  PotM___HPLPot___finalise();

  PotM___potSetting = value;
}

static inline  result_t PotM___Pot___init(uint8_t initialSetting)
#line 106
{
  PotM___setPot(initialSetting);
  return SUCCESS;
}

# 78 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
inline static  result_t RealMain___Pot___init(uint8_t arg_0xa26e060){
#line 78
  unsigned char result;
#line 78

#line 78
  result = PotM___Pot___init(arg_0xa26e060);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 76 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
void TOSH_sched_init(void )
{
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
}

static inline 
# 108 "C:/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

static inline   
# 58 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC___Leds___init(void)
#line 58
{
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 59
    {
      LedsC___ledsOn = 0;
      {
      }
#line 61
      ;
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 56 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t BlinkM___Leds___init(void){
#line 56
  unsigned char result;
#line 56

#line 56
  result = LedsC___Leds___init();
#line 56

#line 56
  return result;
#line 56
}
#line 56
static inline  
# 52 "BlinkM.nc"
result_t BlinkM___StdControl___init(void)
#line 52
{
  BlinkM___Leds___init();
  return SUCCESS;
}

static inline   
# 149 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
result_t HPLClock___Clock___setRate(char interval, char scale)
#line 149
{
  scale &= 0x7;
  scale |= 0x8;
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 152
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 0);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 1);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) |= 1 << 3;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = scale;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 1;
    }
#line 162
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 96 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t TimerM___Clock___setRate(char arg_0xa2d6060, char arg_0xa2d61a0){
#line 96
  unsigned char result;
#line 96

#line 96
  result = HPLClock___Clock___setRate(arg_0xa2d6060, arg_0xa2d61a0);
#line 96

#line 96
  return result;
#line 96
}
#line 96
static inline  
# 72 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___StdControl___init(void)
#line 72
{
  TimerM___mState = 0;
  TimerM___setIntervalFlag = 0;
  TimerM___queue_head = TimerM___queue_tail = -1;
  TimerM___queue_size = 0;
  TimerM___mScale = 3;
  TimerM___mInterval = TimerM___maxTimerInterval;
  return TimerM___Clock___setRate(TimerM___mInterval, TimerM___mScale);
}

# 63 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain___StdControl___init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = TimerM___StdControl___init();
#line 63
  result = rcombine(result, BlinkM___StdControl___init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
# 41 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t TimerM___PowerManagement___adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM___PowerManagement___adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline   
# 87 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void HPLClock___Clock___setInterval(uint8_t value)
#line 87
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = value;
}

# 105 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerM___Clock___setInterval(uint8_t arg_0xa2d6b60){
#line 105
  HPLClock___Clock___setInterval(arg_0xa2d6b60);
#line 105
}
#line 105
static inline  
# 93 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___Timer___start(uint8_t id, char type, 
uint32_t interval)
#line 94
{
  uint8_t diff;

#line 96
  if (id >= NUM_TIMERS) {
#line 96
    return FAIL;
    }
#line 97
  if (type > 1) {
#line 97
    return FAIL;
    }
#line 98
  TimerM___mTimerList[id].ticks = interval;
  TimerM___mTimerList[id].type = type;

  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
      interval += diff;
      TimerM___mTimerList[id].ticksLeft = interval;
      TimerM___mState |= 0x1 << id;
      if (interval < TimerM___mInterval) {
          TimerM___mInterval = interval;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
          TimerM___PowerManagement___adjustPower();
        }
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 59 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t BlinkM___Timer___start(char arg_0xa2a5448, uint32_t arg_0xa2a55a0){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM___Timer___start(0, arg_0xa2a5448, arg_0xa2a55a0);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline  
# 63 "BlinkM.nc"
result_t BlinkM___StdControl___start(void)
#line 63
{

  return BlinkM___Timer___start(TIMER_REPEAT, 1000);
}

static inline  
# 82 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___StdControl___start(void)
#line 82
{
  return SUCCESS;
}

# 70 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain___StdControl___start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = TimerM___StdControl___start();
#line 70
  result = rcombine(result, BlinkM___StdControl___start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline 
# 62 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM___getPowerLevel(void)
#line 62
{
  uint8_t diff;

#line 64
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {

      return HPLPowerManagementM___IDLE;
    }
  else {
#line 67
    if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM___IDLE;
      }
    else {
#line 69
      if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) & ((1 << 6) | (1 << 7))) {
          return HPLPowerManagementM___IDLE;
        }
      else {
        if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) & (1 << 7)) {
            return HPLPowerManagementM___ADC_NR;
          }
        else {
#line 75
          if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
              diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) - * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
              if (diff < 16) {
                return HPLPowerManagementM___IDLE;
                }
#line 79
              return HPLPowerManagementM___POWER_SAVE;
            }
          else 
#line 80
            {
              return HPLPowerManagementM___POWER_DOWN;
            }
          }
        }
      }
    }
}

static inline  
#line 85
void HPLPowerManagementM___doAdjustment(void)
#line 85
{
  uint8_t foo;
#line 86
  uint8_t mcu;

#line 87
  foo = HPLPowerManagementM___getPowerLevel();
  mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM___POWER_SAVE) {
      mcu |= HPLPowerManagementM___IDLE;
      while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
}

static 
# 165 "C:/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
__inline void __nesc_enable_interrupt(void)
#line 165
{
  __BLAST_enabled = 1;
   __asm volatile ("sei");}

static inline 
#line 135
void TOSH_wait(void)
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

static inline 
void TOSH_sleep(void)
{

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
   __asm volatile ("sleep");}

#line 160
__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg)
{

    * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) = oldSreg;
  __BLAST_enabled = 1;

}

#line 153
__inline __nesc_atomic_t  __nesc_atomic_start(void ) __attribute__((atomic))
{
  __nesc_atomic_t result = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20);

#line 156
  //  __asm volatile ("cli");
   __BLAST_enabled = 0;
   return result;
}

static inline 
# 139 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool TOSH_run_next_task(void)
#line 139
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  if (TOSH_sched_full == TOSH_sched_free) {

      return 0;
    }
  else {

      fInterruptFlags = __nesc_atomic_start();
      old_full = TOSH_sched_full;
      TOSH_sched_full++;
      TOSH_sched_full &= TOSH_TASK_BITMASK;
      func = TOSH_queue[(int )old_full].tp;
      TOSH_queue[(int )old_full].tp = 0;
      __nesc_atomic_end(fInterruptFlags);
      func();
      return 1;
    }
}

static inline void TOSH_run_task(void)
#line 162
{
  while (TOSH_run_next_task()) 
    ;
  TOSH_sleep();
  TOSH_wait();
}

# 116 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
static void TimerM___adjustInterval(void)
#line 116
{
  uint8_t i;
#line 117
  uint8_t val = TimerM___maxTimerInterval;

#line 118
  if (TimerM___mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM___mState & (0x1 << i) && TimerM___mTimerList[i].ticksLeft < val) {
              val = TimerM___mTimerList[i].ticksLeft;
            }
        }
      {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
        {
          TimerM___mInterval = val;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
        }
#line 128
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
        {
          TimerM___mInterval = TimerM___maxTimerInterval;
          TimerM___Clock___setInterval(TimerM___mInterval);
          TimerM___setIntervalFlag = 0;
        }
#line 135
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM___PowerManagement___adjustPower();
}

# 57 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN(void)
#line 57
{
#line 57
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

static inline   
# 69 "C:/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC___Leds___redOn(void)
#line 69
{
  {
  }
#line 70
  ;
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 71
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC___ledsOn |= LedsC___RED_BIT;
    }
#line 74
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC___Leds___redOff(void)
#line 78
{
  {
  }
#line 79
  ;
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 80
    {
      TOSH_SET_RED_LED_PIN();
      LedsC___ledsOn &= ~LedsC___RED_BIT;
    }
#line 83
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC___Leds___redToggle(void)
#line 87
{
  result_t rval;

#line 89
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 89
  {
      if (LedsC___ledsOn & LedsC___RED_BIT!=0) {
        rval = LedsC___Leds___redOff();
        }
      else {
#line 93
        rval = LedsC___Leds___redOn();
        }
      }

#line 95
    __nesc_atomic_end(__nesc_atomic); }
#line 95
  return rval;
}

# 81 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t BlinkM___Leds___redToggle(void){
#line 81
  unsigned char result;
#line 81

#line 81
  result = LedsC___Leds___redToggle();
#line 81

#line 81
  return result;
#line 81
}
#line 81
static inline  
# 84 "BlinkM.nc"
result_t BlinkM___Timer___fired(void)
{
  BlinkM___Leds___redToggle();
  return SUCCESS;
}

static inline   
# 154 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM___Timer___default___fired(uint8_t id)
#line 154
{
  return SUCCESS;
}

# 73 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t TimerM___Timer___fired(uint8_t arg_0xa2bd460){
#line 73
  unsigned char result;
#line 73

#line 73
  switch (arg_0xa2bd460) {
#line 73
    case 0:
#line 73
      result = BlinkM___Timer___fired();
#line 73
      break;
#line 73
    default:
#line 73
      result = TimerM___Timer___default___fired(arg_0xa2bd460);
#line 73
    }
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 166 "C:/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t TimerM___dequeue(void)
#line 166
{
  if (TimerM___queue_size == 0) {
    return NUM_TIMERS;
    }
#line 169
  if (TimerM___queue_head == NUM_TIMERS - 1) {
    TimerM___queue_head = -1;
    }
#line 171
  TimerM___queue_head++;
  TimerM___queue_size--;
  return TimerM___queue[(uint8_t )TimerM___queue_head];
}

static inline  void TimerM___signalOneTimer(void)
#line 176
{
  uint8_t itimer = TimerM___dequeue();

#line 178
  if (itimer < NUM_TIMERS) {
    TimerM___Timer___fired(itimer);
    }
}

static inline 
#line 158
void TimerM___enqueue(uint8_t value)
#line 158
{
  if (TimerM___queue_tail == NUM_TIMERS - 1) {
    TimerM___queue_tail = -1;
    }
#line 161
  TimerM___queue_tail++;
  TimerM___queue_size++;
  TimerM___queue[(uint8_t )TimerM___queue_tail] = value;
}

static inline  
#line 182
void TimerM___HandleFire(void)
#line 182
{
  uint8_t i;

#line 184
  TimerM___setIntervalFlag = 1;
  if (TimerM___mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM___mState & (0x1 << i)) {
              TimerM___mTimerList[i].ticksLeft -= TimerM___mInterval + 1;
              if (TimerM___mTimerList[i].ticksLeft <= 2) {
                  if (TimerM___mTimerList[i].type == TIMER_REPEAT) {
                      TimerM___mTimerList[i].ticksLeft += TimerM___mTimerList[i].ticks;
                    }
                  else 
#line 192
                    {
                      TimerM___mState &= ~(0x1 << i);
                    }
                  TimerM___enqueue(i);
                  TOS_post(TimerM___signalOneTimer);
                }
            }
        }
    }
  TimerM___adjustInterval();
}

static inline   result_t TimerM___Clock___fire(void)
#line 204
{
  TOS_post(TimerM___HandleFire);
  return SUCCESS;
}

# 180 "C:/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLClock___Clock___fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerM___Clock___fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
# 98 "C:/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool  TOS_post(void (*tp)(void))
#line 98
{
/*
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;
  TOSH_sched_free++;
  TOSH_sched_free &= TOSH_TASK_BITMASK;

  if (TOSH_sched_free != TOSH_sched_full) {
      __nesc_atomic_end(fInterruptFlags);

      TOSH_queue[tmp].tp = tp;
      return TRUE;
    }
  else {
      TOSH_sched_free = tmp;
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
*/
}

# 54 "C:/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void)
#line 54
{
  int __BLAST_NONDET;


  //RealMain___hardwareInit();
  //RealMain___Pot___init(10);
  /*
  TOSH_sched_init();
*/
  /*
  RealMain___StdControl___init();
  RealMain___StdControl___start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
  */
  //while (1) {
    while (__BLAST_NONDET) {
      if(__BLAST_NONDET) {
	if (__BLAST_enabled==1) { __BLAST_enabled = 0; __vector_15() ; }
      }
    }
    /* run tasks */
    
    switch (__BLAST_NONDET) {
      
    case 1:
      TimerM___signalOneTimer ();
      break;
    case 2:
      TimerM___HandleFire ();
      break;
    case 3:
      HPLPowerManagementM___doAdjustment ();
      break;
    default: break;
    }
    //}

}

static   
# 103 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM___PowerManagement___adjustPower(void)
#line 103
{
  uint8_t mcu;

#line 105
  if (!HPLPowerManagementM___disabled) {
    TOS_post(HPLPowerManagementM___doAdjustment);
    }
  else 
#line 107
    {
      mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM___IDLE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

# 167 "C:/cygwin/opt/tinyos-1.x/tos/platform/mica/HPLClock.nc"
void __attribute((interrupt))   __vector_15(void)
#line 167
{
  {__blockattribute__((atomic))  __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 168
    {
      if (HPLClock___set_flag) {
          HPLClock___mscale = HPLClock___nextScale;
          HPLClock___nextScale |= 0x8;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLClock___nextScale;

          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLClock___minterval;
          HPLClock___set_flag = 0;
        }
    }
#line 177
  __nesc_atomic_end(__nesc_atomic); }
  HPLClock___Clock___fire();
}

