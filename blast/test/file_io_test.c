struct _IO_marker;
# 2 "file_openclose.h"
typedef long __off_t;
# 3
typedef long long __off64_t;
# 5
typedef void _IO_lock_t;
# 8
struct _IO_FILE {
   int __BLAST_FLAG ;
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   int _mode ;
   char _unused2[(int )(15U * sizeof(int ) - 2U * sizeof(void *))] ;
};
# 38
typedef struct _IO_FILE FILE;
# 1
struct _IO_marker;
# 39
extern FILE *stderr ;
# 40
extern FILE *stdout ;
# 2 "spec.work"
int __BLAST_error  ;
# 3
void __error__(void) 
{ 

  {
# 5
  __BLAST_error = 0;
  ERROR: 
  goto ERROR;
}
}
# 8
void __BLAST___error__(void) 
{ 

  {
# 10
  __BLAST_error = 0;
  BERROR: 
  goto BERROR;
}
}
void __initialize__(void) ;
# 2 "try.c"
typedef struct _IO_FILE FILE;
# 12
struct _st {
   FILE *fp ;
   int foo ;
};
struct _IO_FILE;
# 4
extern void *malloc(int  ) ;
# 5
FILE *fopen(char * , char *___0 ) 
{ FILE *fp ;
  FILE *tmp ;

  {
  {
# 6
  tmp = (FILE *)malloc(128);
# 6
  fp = tmp;
  }
# 7
  return (fp);
}
}
# 9
extern void fprintf(FILE * , char *  , ...) ;
# 17
struct _st *fopener(void) 
{ int __BLAST_NONDET ;
  FILE *f ;
  struct _st *s ;
  struct _st *tmp ;

  {
# 19
  if (__BLAST_NONDET) {
# 20
    return ((struct _st *)0);
  } else {
    {
# 23
    tmp = (struct _st *)malloc(1024);
# 23
    s = tmp;
    }
# 24
    if (s == (struct _st *)0) {
# 24
      return ((struct _st *)0);
    }
    {
# 25
    f = fopen("fff", "w");
    {
# 17 "spec.work"
    if (f != (FILE *)0) {
# 17
      f->__BLAST_FLAG = 1;
    }
    {

    }
    }

    }
# 26 "try.c"
    if (f == (FILE *)0) {
# 27
      return ((struct _st *)0);
    }
    {
# 29
    s->fp = f;
# 30
    s->foo = 0;
    }
# 33
    return (s);
  }
}
}
# 37
int main(void) 
{ struct _st *_f ;

  {
  __initialize__();
  {
# 39
  _f = fopener();
  }
# 40
  if (_f != (struct _st *)0) {
    {

    {
# 48 "spec.work"
    if (! ((_f->fp)->__BLAST_FLAG == 1)) {
# 50
      __error__();
    }
    {

    }
    }
# 41 "try.c"
    fprintf(_f->fp, "foo");
    }
  }
# 40
  return (0);
}
}
void __initialize__(void) 
{ 

  {

}
}
