int a,b;

void f(int x, int y) __attribute__((eventhandler)) {
  if (x == y){ return;}
  if (x < y){ ERROR: goto ERROR;}
  y = y + 1;
  f(x,y);
}

void ac_main(){
  if (a > 0){
    b = 0;
    f(a,b);
  }
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
