
module Stats = Bstats
module M = Message

open Cil

(** Reads a source file into a Cil.file structure. *)
let parseOneFile (fname: string) : Cil.file =
  (* parse and convert to CIL *)
  ignore(M.msg_string M.Normal (Printf.sprintf "  Parsing %s..." fname));
  let cil = Frontc.parse fname () in
  cil;;


(** Read in program *) 
let mk (fname: string) : Cil.file =
  Cil.initCIL ();
  Cil.printCilAsIs := true ;
  (* reads the source file *)
  let file = parseOneFile fname in
  (* change names of locals so that each local has a unique name *)
  Simplemem.feature.Cil.fd_doit file ;
  file


let print_copyright () =
   M.msg_string M.Normal "EEL2Blast 1.0. Copyright  2006  Regents of the University of California"

let rec contains_call il = match il with
    [] -> false
  | Call(_) :: tl -> true
  | _ :: tl -> contains_call tl

let eel_calls = [ "eel_add_timer"; "eel_add_error"; "eel_add_error_timeout" ;
                  "eel_add_read" ; "eel_add_read_timeout" ; 
                  "eel_add_write"; "eel_add_write_timeout" ]

let rec contains_eel_call il = 
  match il with
    [] -> false
  | Call (_, Cil.Lval (Cil.Var v, Cil.NoOffset), _, _  ) :: tl -> 
    begin
      if List.mem v.Cil.vname eel_calls then true else
      contains_eel_call tl
    end
  | _ :: tl -> contains_eel_call tl

let is_eel_call il =
  match il with
  | [ Call (_, Cil.Lval (Cil.Var v, Cil.NoOffset), _, _  ) ] -> 
    begin
      List.mem v.Cil.vname eel_calls 
    end
  | _ -> false 
    
class callBBVisitor = object
  inherit nopCilVisitor

  method vstmt s =
    match s.skind with
      Instr(il) when contains_call il -> begin
        let list_of_stmts = List.map (fun one_inst ->
          mkStmtOneInstr one_inst) il in
        let block = mkBlock list_of_stmts in
        ChangeDoChildrenPost(s, (fun _ ->
          s.skind <- Block(block) ;
          s))
      end
    | _ -> DoChildren

  method vvdec _ = SkipChildren
  method vexpr _ = SkipChildren
  method vlval _ = SkipChildren
  method vtype _ = SkipChildren
end

let calls_end_basic_blocks f =
  let thisVisitor = new callBBVisitor in
  ignore ( visitCilFileSameGlobals thisVisitor f )

module Stringset = Set.Make (struct type t = string 
			   let compare  = Pervasives.compare
                          end)

let async_handlers = ref Stringset.empty

class asyncReplacer = object (self)
  inherit nopCilVisitor
 
  method vstmt s = 
    match s.Cil.skind with
      Instr(il) when is_eel_call il ->
        begin
          let c = List.hd il in
          match c with 
          | Cil.Call (retopt, fname, arglist, l) ->
            begin
              let i = match fname with
                Cil.Lval (Cil.Var v, Cil.NoOffset) ->
                  let fn = match (List.nth arglist 1) with
                    Cil.Lval (Cil.Var v, Cil.NoOffset) 
                  | Cil.CastE(_, Cil.Lval (Cil.Var v, Cil.NoOffset) )
                  | Cil.CastE (_, Cil.AddrOf   (Cil.Var v, Cil.NoOffset) ) 
                  | Cil.AddrOf   (Cil.Var v, Cil.NoOffset) ->
                      v
                  | e -> ignore (Pretty.printf "Callback: %a\n" Cil.d_exp e) ; failwith "callback is a function pointer! " 
              in
              async_handlers := Stringset.add fn.Cil.vname !async_handlers ;
                  if v.Cil.vname = "eel_add_timer" ||
                     v.Cil.vname = "eel_add_read" ||
                     v.Cil.vname = "eel_add_write" ||
                     v.Cil.vname = "eel_add_error" then
                  Cil.Call (retopt, Cil.Lval (Cil.var fn), 
                              [ List.nth arglist 0; 
                                List.nth arglist 2;
                                List.nth arglist 3;
                              ],  
                              l) 
                  else if v.Cil.vname = "eel_add_read_timeout"
                       || v.Cil.vname = "eel_add_write_timeout"
                       || v.Cil.vname = "eel_add_error_timeout"
                  then
                  Cil.Call (retopt, Cil.Lval (Cil.var fn), 
                              [ List.nth arglist 0; 
                                List.nth arglist 3;
                                List.nth arglist 4;
                              ],  
                              l) 
                  else assert false 
              | _ -> assert false
              in
              ChangeDoChildrenPost(s, (fun _ ->
                s.Cil.skind <- Cil.Instr [i] ; s) )
            end
          | _ -> assert false
        end
    | Instr([ Cil.Call (None, Cil.Lval (Cil.Var v, Cil.NoOffset), _, l) ] ) ->
        if v.Cil.vname = "eel_dispatch_loop" then
           v.Cil.vname <- "__BLAST_DISPATCH_FUNCTION"
        else () ;
        Cil.DoChildren
    | _ -> DoChildren
end

let replace_async_calls f = 
  let thisVisitor = new asyncReplacer in
  ignore ( visitCilFileSameGlobals thisVisitor f ) ;
  ()

let add_async_attribute f =
  Cil.iterGlobals 
    f
    (fun g ->  match g with
	Cil.GFun (fundec, _) -> 
          begin
            if Stringset.mem fundec.Cil.svar.Cil.vname !async_handlers then
            begin
              fundec.Cil.svar.Cil.vattr <- (Cil.Attr ("eventhandler", []) ) :: fundec.Cil.svar.Cil.vattr ;
              ()
            end
            else ()
          end
     | _ -> ()
    )

(* main function: the glue *)
let main () =
 try
  M.set_verbosity M.Default;
  Stats.reset ();
  Options.buildOptionsDatabase ();
  print_copyright () ;

  let f = Stats.time "parse" mk (List.hd (Options.getSourceFiles ()) )  in
  Stats.time "calls end bb" calls_end_basic_blocks f;
  Stats.time "replace" replace_async_calls f;
  Stats.time "add attribute" add_async_attribute f;  
    (* Now print translated file in translated.i *)
  (* Cil.lineDirectiveStyle := Some Cil.LineComment ; *)
  Cil.lineDirectiveStyle := None ; 
  
  let outc = open_out "translated.i" in
  Printf.fprintf outc "void assert(int p) {\n\tif (p==0) {\nERROR: goto ERROR;\n}\n}\n" ;
  Printf.fprintf outc "void __BLAST_assert(int p) {\n\tif (p==0) {\nBERROR: goto BERROR;\n}\n}\n" ;
  Printf.fprintf outc "int __BLAST_DISPATCH_FUNCTION() __attribute__((noreturn)) {\n DISPATCH: goto DISPATCH;\n}\n";
  Cil.dumpFile Cil.defaultCilPrinter outc f ;
  close_out outc ;
  Stats.print stderr "Timings:" ;
  ()
 with e -> M.msg_string M.Error "Ack! The gremlins again!";
           raise e
;;

main ();;
