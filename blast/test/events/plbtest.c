int gid = 0;
enum eel_return {EEL_NORMAL, EEL_ERROR };

void *malloc();

void error(){ ERROR:	goto ERROR;}

void assert(int p) {
  if (p==0){ error();}
}

int x = 0 ;

int eel_new_gid() {
	int t = x;
	x++;
	return t;
}

struct read_buf {
	int to_read;
	int already_read;
};


int get_read() {
	int x = unknown();
	if (x>0 ) return x;
	STUCK: goto STUCK;
}
int read(int fd, int bytes) {
	while(1) {
		int f = unknown();
		if (f <= bytes && 0 <= f) {
			return f;
		}
	}
}

enum eel_return async_reader(int g, int fd, struct read_buf *rbuf) __attribute__((eventhandler)) {
	int thisread;
	if (rbuf==0 ) assert(0);
	
	if (rbuf->to_read - rbuf->already_read == 0) {
		return EEL_NORMAL;
	}
	if(rbuf->to_read - rbuf->already_read < 0) assert(0);

	//thisread = read(fd, rbuf->to_read - rbuf->already_read );
	rbuf->already_read = rbuf->already_read + 1;//rbuf->to_read + thisread; 

	async_reader(g, fd, rbuf);
	return EEL_NORMAL; 	
}

enum eel_return async_listener(int g) __attribute__((eventhandler)) {
	int reader;
	int fd;
	
	reader = eel_new_gid();
	
	fd = unknown();

	struct read_buf *rbuf = malloc();	
	
	if (rbuf == 0) {return EEL_ERROR;}
	
	rbuf->to_read = get_read();
	rbuf->already_read = 0;
	async_reader(reader, fd, rbuf);
	async_listener(g);
	
	return EEL_NORMAL;
}

void ac_main(){
  int x = eel_new_gid();
  async_listener(x);
}

//STANDARD "rewrite"
void __BLAST_DISPATCH_FUNCTION(){
        DISPATCH: goto DISPATCH;
}


void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}


