int x ;
int __BLAST_NONDET;

void error(){ ERROR: goto ERROR;}

void async2() __attribute__((eventhandler)) {
  int z;
  z++;
}

void async1(int a) __attribute__((eventhandler)) {
  if (a < 0) { error();}
}

void ac_main(){
 // x = 0; //comment to add bug 
  
  async1(x);
  //x++;
  async2();
  
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
