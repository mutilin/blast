int a,b;
struct buf {
  int x;
  int y;
};

void error(){
ERROR:goto ERROR;
}

void f(struct buf *p) __attribute__((eventhandler)){
  if (p == 0) { error();}
  
  if (p->x - p->y == 0){ return;}
  
  if (p->x - p->y < 0){ error();}
  
  p->y = p->y + 1;
  
  f(p);
}

void ac_main(){
  struct buf *p;
  
  p = malloc();
  
  if (p == 0){ return;}
  
  if (a > 0){
    b = 0;
    p->x = a;
    p->y = b;
    f(p);
  }
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}

