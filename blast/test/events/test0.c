int x ;

void async1() __attribute__((eventhandler)) {
  if (x != 0) { ERROR: goto ERROR;}
  x = 1;
}


void ac_main(){
  async1 ();
  x = 0; //comment out to make unsafe
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
