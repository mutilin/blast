int a,b;
struct buf {
  int f;
};


void error(){
ERROR: goto ERROR;
}


void f(struct buf *x){
  if (x->f == 0){ error();}

  x->f++;
  return;
}

void main(){
  struct buf *p;
  
  p = malloc();

  p->f = 10;
  f(p);

  p = malloc();
  p->f = 0;
  f(p);
} 




