void __BLAST_assert(int p) {
	if (p==0) {
BERROR: goto BERROR;
}
}
int __BLAST_DISPATCH_FUNCTION() __attribute__((noreturn)) {
 DISPATCH: goto DISPATCH;
}
/* Generated by CIL v. 1.3.4 */
/* print_CIL_Input is false */

typedef unsigned int size_t;
typedef long long __quad_t;
typedef long __off_t;
typedef __quad_t __off64_t;
typedef long __time_t;
typedef long __suseconds_t;
typedef int __ssize_t;
typedef unsigned int __socklen_t;
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
   struct _IO_marker *_next ;
   struct _IO_FILE *_sbuf ;
   int _pos ;
};
struct _IO_FILE {
   int _flags ;
   char *_IO_read_ptr ;
   char *_IO_read_end ;
   char *_IO_read_base ;
   char *_IO_write_base ;
   char *_IO_write_ptr ;
   char *_IO_write_end ;
   char *_IO_buf_base ;
   char *_IO_buf_end ;
   char *_IO_save_base ;
   char *_IO_backup_base ;
   char *_IO_save_end ;
   struct _IO_marker *_markers ;
   struct _IO_FILE *_chain ;
   int _fileno ;
   int _flags2 ;
   __off_t _old_offset ;
   unsigned short _cur_column ;
   signed char _vtable_offset ;
   char _shortbuf[1] ;
   _IO_lock_t *_lock ;
   __off64_t _offset ;
   void *__pad1 ;
   void *__pad2 ;
   int _mode ;
   char _unused2[(int )(15U * sizeof(int ) - 2U * sizeof(void *))] ;
};
typedef __ssize_t ssize_t;
struct timeval {
   __time_t tv_sec ;
   __suseconds_t tv_usec ;
};
typedef __socklen_t socklen_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned short sa_family_t;
struct sockaddr {
   sa_family_t sa_family ;
   char sa_data[14] ;
};
typedef uint16_t in_port_t;
typedef uint32_t in_addr_t;
struct in_addr {
   in_addr_t s_addr ;
};
struct sockaddr_in {
   sa_family_t sin_family ;
   in_port_t sin_port ;
   struct in_addr sin_addr ;
   unsigned char sin_zero[(int )(((sizeof(struct sockaddr ) - sizeof(unsigned short )) -
                                  sizeof(in_port_t )) - sizeof(struct in_addr ))] ;
};
struct eel_event;
struct eel_event;
struct eel_group;
struct eel_group;
typedef struct eel_group *eel_group_id;
typedef struct eel_event *eel_event_id;
enum eel_cb_return_value_enum {
    EEL_SUCCESS = 0,
    EEL_ERROR = -1
};
typedef enum eel_cb_return_value_enum eel_cb_return_value;
struct params {
   int server ;
   unsigned int num_connections ;
   struct timeval delay ;
   char const   *protocol_file ;
   struct sockaddr_in address ;
   unsigned long ports_start ;
   unsigned long ports_end ;
   struct in_addr bind_addr ;
};
struct state_node {
   unsigned char const   *data ;
   size_t length ;
   int timeout ;
};
struct dispatch_state {
   unsigned int num_conn ;
};
struct conn_state {
   int fd ;
   int state ;
   unsigned char *buffer ;
   size_t buffsize ;
   size_t datasize ;
};
struct option {
   char const   *name ;
   int has_arg ;
   int *flag ;
   int val ;
};
struct addrinfo {
   int ai_flags ;
   int ai_family ;
   int ai_socktype ;
   int ai_protocol ;
   socklen_t ai_addrlen ;
   struct sockaddr *ai_addr ;
   char *ai_canonname ;
   struct addrinfo *ai_next ;
};
typedef unsigned long long __u_quad_t;
typedef __u_quad_t __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned int __nlink_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
struct timespec {
   __time_t tv_sec ;
   long tv_nsec ;
};
struct stat {
   __dev_t st_dev ;
   unsigned short __pad1 ;
   __ino_t st_ino ;
   __mode_t st_mode ;
   __nlink_t st_nlink ;
   __uid_t st_uid ;
   __gid_t st_gid ;
   __dev_t st_rdev ;
   unsigned short __pad2 ;
   __off_t st_size ;
   __blksize_t st_blksize ;
   __blkcnt_t st_blocks ;
   struct timespec st_atim ;
   struct timespec st_mtim ;
   struct timespec st_ctim ;
   unsigned long __unused4 ;
   unsigned long __unused5 ;
};
void assert(int p ) 
{ 

  {
  if (p == 0) {
    ERROR: 
    goto ERROR;
  } else {

  }
  return;
}
}


/* #pragma merger(0,"/tmp/cil-thheRf54.i","") */
extern struct _IO_FILE *stderr ;
extern int fprintf(FILE * __restrict  __stream , char const   * __restrict  __format 
                   , ...) ;
extern void perror(char const   *__s ) ;
extern  __attribute__((__nothrow__)) void *malloc(size_t __size )  __attribute__((__malloc__)) ;
extern  __attribute__((__nothrow__)) void *calloc(size_t __nmemb , size_t __size )  __attribute__((__malloc__)) ;
extern  __attribute__((__nothrow__)) void *realloc(void *__ptr , size_t __size )  __attribute__((__malloc__)) ;
extern  __attribute__((__nothrow__)) void free(void *__ptr ) ;
extern  __attribute__((__nothrow__)) void *memmove(void *__dest , void const   *__src ,
                                                   size_t __n )  __attribute__((__nonnull__(1,2))) ;
extern  __attribute__((__nothrow__)) void bzero(void *__s , size_t __n )  __attribute__((__nonnull__(1))) ;
extern int close(int __fd ) ;
extern ssize_t read(int __fd , void *__buf , size_t __nbytes ) ;
extern ssize_t write(int __fd , void const   *__buf , size_t __n ) ;
extern int fcntl(int __fd , int __cmd  , ...) ;
extern  __attribute__((__nothrow__)) int *__errno_location(void)  __attribute__((__const__)) ;
extern  __attribute__((__nothrow__)) int socket(int __domain , int __type , int __protocol ) ;
extern  __attribute__((__nothrow__)) int bind(int __fd , struct sockaddr  const  *__addr ,
                                              socklen_t __len ) ;
extern int connect(int __fd , struct sockaddr  const  *__addr , socklen_t __len ) ;
extern  __attribute__((__nothrow__)) uint16_t htons(uint16_t __hostshort )  __attribute__((__const__)) ;
extern int eel_initialize(int exit_on_signal ) ;
extern int eel_uninitialize(void) ;
extern int __BLAST_DISPATCH_FUNCTION(void) ;
extern eel_event_id eel_add_timer(eel_group_id gid , eel_cb_return_value (*cb)(eel_group_id gid ,
                                                                               void *arg ,
                                                                               int fd ) ,
                                  void *cb_arg , int timeout_milliseconds ) ;
extern eel_event_id eel_add_read(eel_group_id gid , eel_cb_return_value (*cb)(eel_group_id gid ,
                                                                              void *arg ,
                                                                              int fd ) ,
                                 void *cb_arg , int fd ) ;
extern eel_event_id eel_add_write(eel_group_id gid , eel_cb_return_value (*cb)(eel_group_id gid ,
                                                                               void *arg ,
                                                                               int fd ) ,
                                  void *cb_arg , int fd ) ;
extern eel_group_id eel_new_group_id(void) ;
extern void eel_delete_group_id(eel_group_id gid ) ;
struct params options ;
int parse_cmdline(int argc , char **argv ) ;
struct state_node *states  ;
int num_states  ;
int parsefile(char const   *file ) ;
unsigned char const   *binbin(unsigned char const   *haystack , size_t haystack_size ,
                              unsigned char const   *needle , size_t needle_length ) ;
eel_cb_return_value conn_dispatcher(eel_group_id gid , struct dispatch_state *state ,
                                    int fd )  __attribute__((__eventhandler__)) ;
void init_libs(eel_group_id start_gid ) 
{ struct dispatch_state *dispatch ;
  void *tmp ;

  {
  {
  eel_initialize(1);
  parsefile(options.protocol_file);
  dispatch = (struct dispatch_state *)calloc(1U, sizeof((*dispatch)));
  dispatch->num_conn = options.num_connections;
  conn_dispatcher(start_gid, (void *)dispatch, 0);
  }
  return;
}
}
void new_conn(void) ;
eel_cb_return_value conn_dispatcher(eel_group_id gid , struct dispatch_state *state ,
                                    int fd )  __attribute__((__eventhandler__)) ;
eel_cb_return_value conn_dispatcher(eel_group_id gid , struct dispatch_state *state ,
                                    int fd ) 
{ struct dispatch_state *dstate ;

  {
  {
  dstate = state;
  //assert((unsigned int )dstate != (unsigned int )((struct dispatch_state *)0));
  //CHECK 1 : UNSAFE
  //if ((unsigned int )dstate == (unsigned int )((struct dispatch_state *)0)){ assert(0);}
  
  }
  if (dstate->num_conn == 0U) {
    if (options.delay.tv_sec != 0L) {
      {
      dstate->num_conn = options.num_connections;
      conn_dispatcher(gid, (void *)dstate, (int )(options.delay.tv_sec * 1000L + options.delay.tv_usec /
                                                                                 1000L));
      }
    } else {
      if (options.delay.tv_usec != 0L) {
        {
        dstate->num_conn = options.num_connections;
        conn_dispatcher(gid, (void *)dstate, (int )(options.delay.tv_sec * 1000L +
                                                    options.delay.tv_usec / 1000L));
        }
      } else {

      }
    }
  } else {
    {
    new_conn();
    dstate->num_conn = dstate->num_conn - 1U;
    conn_dispatcher(gid, (void *)dstate, 0);
    }
  }
  return ((enum eel_cb_return_value_enum )0);
}
}
static void conn_handle(eel_group_id gid , int fd , short event , struct conn_state *state ) ;
eel_cb_return_value conn_handle_read(eel_group_id gid , struct conn_state *state ,
                                     int fd )  __attribute__((__eventhandler__)) ;
eel_cb_return_value conn_handle_read(eel_group_id gid , struct conn_state *state ,
                                     int fd ) 
{ 

  {
  {
  conn_handle(gid, fd, (short)2, state);
  }
  return ((enum eel_cb_return_value_enum )0);
}
}
eel_cb_return_value conn_handle_write(eel_group_id gid , struct conn_state *state ,
                                      int fd )  __attribute__((__eventhandler__)) ;
eel_cb_return_value conn_handle_write(eel_group_id gid , struct conn_state *state ,
                                      int fd ) 
{ 
  {
  {
  conn_handle(gid, fd, (short)1, state);
  }
  return ((enum eel_cb_return_value_enum )0);
}
}
extern int eel_combine_OR() ;
static void conn_handle(eel_group_id gid , int fd , short event , struct conn_state *state ) 
{ struct conn_state *cstate ;
  unsigned char *buffer ;
  unsigned char const   *needle ;
  eel_event_id tmp ;
  eel_event_id tmp___0 ;
  ssize_t tmp___1 ;
  eel_event_id tmp___2 ;
  eel_event_id tmp___3 ;
  void *tmp___4 ;
  struct state_node *mem_14 ;
  struct state_node *mem_15 ;
  struct state_node *mem_16 ;
  struct state_node *mem_17 ;
  struct state_node *mem_18 ;
  struct state_node *mem_19 ;
  struct state_node *mem_20 ;
  struct state_node *mem_21 ;
  struct state_node *mem_22 ;

  {
  {
  //assert((unsigned int )state != (unsigned int )((struct conn_state *)0));
  //CHECK 2 : SAFE
  if ((unsigned int )state == (unsigned int )((struct conn_state *)0)){assert(0);}

  cstate = state;
  }
  if (cstate->state == num_states) {
    {
    eel_delete_group_id(gid);
    close(cstate->fd);
    free((void *)cstate->buffer);
    free((void *)state);
    }
    return;
  } else {

  }
  if (cstate->state % 2 == 0) {
    if (((int )event & 1) != 0) {
      {
      mem_14 = states + cstate->state;
      mem_15 = states + cstate->state;
      write(cstate->fd, (void const   *)mem_14->data, mem_15->length);
      cstate->state = cstate->state + 1;
      conn_handle_read(gid, (void *)state, fd);
      }
    } else {

    }
  } else {

  }
  if (cstate->state % 2 == 1) {
    if (((int )event & 2) != 0) {
      {
      cstate->buffsize = cstate->datasize + 16384U;
      buffer = (unsigned char *)realloc((void *)cstate->buffer, cstate->buffsize);
      }
      if ((unsigned int )buffer != (unsigned int )((void *)0)) {
        cstate->buffer = buffer;
      } else {
        {
        fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Couldn\'t allocate buffer space\n");
        cstate->state = num_states;
        tmp = (eel_event_id )conn_handle_write(gid, (void *)state, fd);
        tmp___0 = (eel_event_id )conn_handle_read(gid, (void *)state, fd);
        eel_combine_OR(tmp___0, tmp);
        }
        return;
      }
      {
      tmp___1 = read(cstate->fd, (void *)(cstate->buffer + cstate->datasize), cstate->buffsize -
                                                                              cstate->datasize);
      cstate->datasize = cstate->datasize + (unsigned int )tmp___1;
      mem_16 = states + cstate->state;
      mem_17 = states + cstate->state;
      needle = binbin((unsigned char const   *)cstate->buffer, cstate->datasize, mem_16->data,
                      mem_17->length);
      }
      if ((unsigned int )needle != (unsigned int )((void *)0)) {
        {
        mem_18 = states + cstate->state;
        cstate->datasize = cstate->datasize - ((unsigned int )(needle - (unsigned char const   *)cstate->buffer) +
                                               mem_18->length);
        mem_19 = states + cstate->state;
        memmove((void *)cstate->buffer, (void const   *)(needle + mem_19->length),
                cstate->datasize);
        cstate->state = cstate->state + 1;
        tmp___2 = (eel_event_id )conn_handle_write(gid, (void *)state, fd);
        tmp___3 = (eel_event_id )conn_handle_read(gid, (void *)state, fd);
        eel_combine_OR(tmp___3, tmp___2);
        }
      } else {
        {
        mem_20 = states + cstate->state;
        mem_21 = states + cstate->state;
        memmove((void *)cstate->buffer, (void const   *)((cstate->buffer + cstate->datasize) -
                                                         mem_20->length), mem_21->length);
        mem_22 = states + cstate->state;
        cstate->datasize = mem_22->length;
        conn_handle_read(gid, (void *)state, fd);
        }
      }
    } else {

    }
  } else {

  }
  return;
}
}
static unsigned short port  =    (unsigned short)0;
void new_conn(void) 
{ struct conn_state *newconn ;
  struct conn_state *tmp ;
  int error ;
  int tmp___0 ;
  struct sockaddr_in local ;
  unsigned short tmp___1 ;
  int tmp___2 ;
  int *tmp___3 ;
  int tmp___4 ;
  int *tmp___5 ;
  eel_group_id new_gid ;
  eel_group_id tmp___6 ;
  eel_event_id tmp___7 ;
  eel_event_id tmp___8 ;
  int tmp___9 ;
  void *tmp___10 ;

  {
  {
  tmp = (struct conn_state *)malloc(sizeof(struct conn_state ));
  newconn = tmp;
  error = 0;
  }
  if ((unsigned int )newconn == (unsigned int )((void *)0)) {
    return;
  } else {

  }
  {
  newconn->buffer = (unsigned char *)((void *)0);
  newconn->buffsize = 0U;
  newconn->datasize = 0U;
  newconn->fd = socket(2, 1, 0);
  }
  if (newconn->fd < 0) {
    {
    perror("socket creation failed");
    error = 1;
    }
  } else {

  }
  if (! error) {
    {
    tmp___0 = fcntl(newconn->fd, 4, 2048);
    }
    if (tmp___0 != 0) {
      {
      perror("fcntl failed");
      error = 1;
      }
    } else {

    }
  } else {

  }
  if (! error) {
    if (options.ports_start != 0UL) {
      {
      bzero((void *)(& local), sizeof(local));
      }
      if ((unsigned long )port < options.ports_start) {
        port = (unsigned short )options.ports_start;
      } else {
        if ((unsigned long )port >= options.ports_end) {
          port = (unsigned short )options.ports_start;
        } else {

        }
      }
      {
      local.sin_family = (unsigned short)2;
      tmp___1 = port;
      port = (unsigned short )((int )port + 1);
      local.sin_port = htons(tmp___1);
      local.sin_addr = options.bind_addr;
      tmp___2 = bind(newconn->fd, (struct sockaddr  const  *)(& local), sizeof(local));
      }
      if (tmp___2 != 0) {
        {
        perror("local port binding failed");
        error = 1;
        }
      } else {

      }
    } else {

    }
  } else {

  }
  if (! error) {
    {
    tmp___4 = connect(newconn->fd, (struct sockaddr  const  *)((struct sockaddr *)(& options.address)),
                      sizeof(options.address));
    }
    if (tmp___4 != 0) {
      {
      tmp___5 = __errno_location();
      }
      if ((*tmp___5) != 115) {
        {
        tmp___3 = __errno_location();
        fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"%d\n",
                (*tmp___3));
        perror("connect failed");
        error = 1;
        }
      } else {

      }
    } else {

    }
  } else {

  }
  newconn->state = 0;
  if (! error) {
    {
    tmp___6 = eel_new_group_id();
    new_gid = tmp___6;
    // JHALA:reaches here assert(0);
    tmp___7 = (eel_event_id )conn_handle_write(new_gid, (void *)newconn, newconn->fd);
    // JHALA: REACHES HERE assert(0);
    tmp___8 = (eel_event_id )conn_handle_read(new_gid, (void *)newconn, newconn->fd);
    tmp___9 = eel_combine_OR(tmp___8, tmp___7);
    }
    if ((unsigned int )((void *)0) == (unsigned int )tmp___9) {
      {
      eel_delete_group_id(new_gid);
      perror("eel_combine_OR failed");
      error = 1;
      }
    } else {

    }
  } else {

  }
  if (error) {
    if (newconn->fd >= 0) {
      {
      close(newconn->fd);
      }
    } else {

    }
    {
    free((void *)newconn);
    }
  } else {

  }
  return;
}
}
int main(int argc , char **argv ) 
{ eel_group_id start_id ;
  eel_group_id tmp ;

  {
  {
  parse_cmdline(argc, argv);
  tmp = eel_new_group_id();
  start_id = tmp;
  init_libs(start_id);
  __BLAST_DISPATCH_FUNCTION();
  eel_delete_group_id(start_id);
  eel_uninitialize();
  }
  return (0);
}
}
/* #pragma merger(0,"/tmp/cil-Iu9BkyDw.i","") */
extern char *optarg ;
extern int optind ;
extern  __attribute__((__nothrow__)) int getopt_long(int ___argc , char * const  *___argv ,
                                                     char const   *__shortopts , struct option  const  *__longopts ,
                                                     int *__longind ) ;
extern int getaddrinfo(char const   * __restrict  __name , char const   * __restrict  __service ,
                       struct addrinfo  const  * __restrict  __req , struct addrinfo ** __restrict  __pai ) ;
extern  __attribute__((__nothrow__)) char const   *gai_strerror(int __ecode ) ;
extern int printf(char const   * __restrict  __format  , ...) ;
extern  __attribute__((__nothrow__)) unsigned long strtoul(char const   * __restrict  __nptr ,
                                                           char ** __restrict  __endptr ,
                                                           int __base )  __attribute__((__nonnull__(1))) ;
extern  __attribute__((__nothrow__, __noreturn__)) void exit(int __status ) ;
extern  __attribute__((__nothrow__)) void *memset(void *__s , int __c , size_t __n )  __attribute__((__nonnull__(1))) ;
extern  __attribute__((__nothrow__)) char *strchr(char const   *__s , int __c )  __attribute__((__pure__,
__nonnull__(1))) ;
void print_usage(void) 
{ 

  {
  {
  printf((char const   */* __restrict  */)"Usage: nch <options> address port protocol_file\nOptions are:\n--help\t\tprint this screen\n--listen (-l)\twork in server mode (not yet implemented)\n-n\t\tnumber of connections to initiate - default 1\n-w\t\tDelay (in milliseconds) between connection initiations - default once\n--port beg:end\tSource port range to bind to\n--bind ip\tLocal IP address to bind to\n");
  }
  return;
}
}
struct params options  = 
     {0, 0U, {0L, 0L}, (char const   *)0, {(unsigned short)0, (unsigned short)0, {0U},
                                         {(unsigned char)0, (unsigned char)0, (unsigned char)0,
                                          (unsigned char)0, (unsigned char)0, (unsigned char)0,
                                          (unsigned char)0, (unsigned char)0}}, 0UL,
    0UL, {0U}};
static struct option  const  long_options[5]  = {      {"help", 0, (int *)0, 1}, 
        {"listen", 0, (int *)0, 'l'}, 
        {"port", 1, (int *)0, 'p'}, 
        {"bind", 1, (int *)0, 2}, 
        {(char const   *)0, 0, (int *)0, 0}};
static char const   optionstr[8]  = 
  {      (char const   )'l',      (char const   )'n',      (char const   )':',      (char const   )'w', 
        (char const   )':',      (char const   )'p',      (char const   )':',      (char const   )'\000'};
extern int inet_aton() ;
int parse_cmdline(int argc , char **argv ) 
{ int c ;
  int option_index ;
  unsigned int time ;
  unsigned int tmp ;
  int tmp___0 ;
  char *seperator ;
  char *tmp___1 ;
  char *tmp___2 ;
  struct addrinfo hints ;
  struct addrinfo *res ;
  int error ;
  char const   *tmp___3 ;
  unsigned long tmp___4 ;
  unsigned long tmp___5 ;
  char **mem_17 ;
  char **mem_18 ;
  struct sockaddr_in *mem_19 ;
  char **mem_20 ;

  {
  options.num_connections = 1U;
  options.delay.tv_sec = 0L;
  options.delay.tv_usec = 0L;
  options.bind_addr.s_addr = 0U;
  while (1) {
    {
    c = getopt_long(argc, (char * const  *)argv, optionstr, long_options, & option_index);
    }
    if (! (c != -1)) {
      break;
    } else {

    }
    switch (c) {
    case 1: 
    {
    print_usage();
    exit(0);
    }
    break;
    case 108: 
    options.server = 1;
    break;
    case 110: 
    {
    options.num_connections = (unsigned int )strtoul((char const   */* __restrict  */)optarg,
                                                     (char **/* __restrict  */)((void *)0),
                                                     0);
    }
    break;
    case 119: 
    {
    tmp = (unsigned int )strtoul((char const   */* __restrict  */)optarg, (char **/* __restrict  */)((void *)0),
                                 0);
    time = tmp;
    options.delay.tv_sec = (long )(time / 1000U);
    options.delay.tv_usec = (long )((time % 1000U) * 1000U);
    }
    break;
    case 2: 
    {
    tmp___0 = inet_aton(optarg, & options.bind_addr);
    }
    if (tmp___0 == 0) {
      {
      fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Error in parsing --bind parameter\n");
      exit(0);
      }
    } else {

    }
    break;
    case 112: 
    {
    tmp___1 = strchr((char const   *)optarg, ':');
    seperator = tmp___1;
    }
    if ((unsigned int )seperator == (unsigned int )((void *)0)) {
      {
      fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Error parsing arguments: --port does not have a range\n");
      exit(0);
      }
    } else {

    }
    {
    tmp___2 = seperator;
    seperator = seperator + 1;
    (*tmp___2) = (char )'\000';
    options.ports_start = strtoul((char const   */* __restrict  */)optarg, (char **/* __restrict  */)((void *)0),
                                  10);
    options.ports_end = strtoul((char const   */* __restrict  */)seperator, (char **/* __restrict  */)((void *)0),
                                10);
    }
    if (options.ports_start == 0UL) {
      {
      fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Error parsing --port range\n");
      exit(0);
      }
    } else {
      if (options.ports_end == 0UL) {
        {
        fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Error parsing --port range\n");
        exit(0);
        }
      } else {

      }
    }
    break;
    }
  }
  if (! options.server) {
    if (optind <= argc - 3) {
      {
      res = (struct addrinfo *)((void *)0);
      memset((void *)(& hints), 0, sizeof(hints));
      hints.ai_family = 2;
      hints.ai_socktype = 1;
      mem_17 = argv + optind;
      mem_18 = argv + (optind + 1);
      error = getaddrinfo((char const   */* __restrict  */)(*mem_17), (char const   */* __restrict  */)(*mem_18),
                          (struct addrinfo  const  */* __restrict  */)(& hints), (struct addrinfo **/* __restrict  */)(& res));
      }
      if (error != 0) {
        {
        tmp___3 = gai_strerror(error);
        fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Error in parsing host and port names: %s\n",
                tmp___3);
        exit(error);
        }
      } else {

      }
      mem_19 = (struct sockaddr_in *)res->ai_addr;
      options.address = (*mem_19);
      mem_20 = argv + (optind + 2);
      options.protocol_file = (char const   *)(*mem_20);
    } else {
      {
      fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Must supply IP, port and protocol file\n");
      exit(1);
      }
    }
  } else {
    {
    fprintf((FILE */* __restrict  */)stderr, (char const   */* __restrict  */)"Server mode not yet implemented\n");
    exit(1);
    }
  }
  return (0);
}
}
/* #pragma merger(0,"/tmp/cil-3DbDzCaH.i","") */
extern  __attribute__((__nothrow__)) void *mmap(void *__addr , size_t __len , int __prot ,
                                                int __flags , int __fd , __off_t __offset ) ;
extern  __attribute__((__nothrow__)) int __fxstat(int __ver , int __fildes , struct stat *__stat_buf )  __attribute__((__nonnull__(3))) ;
__inline static  __attribute__((__nothrow__)) int fstat__extinline(int __fd , struct stat *__statbuf ) ;
__inline static  __attribute__((__nothrow__)) int fstat__extinline(int __fd , struct stat *__statbuf ) ;
__inline static int fstat__extinline(int __fd , struct stat *__statbuf ) 
{ int tmp ;

  {
  {
  tmp = __fxstat(3, __fd, __statbuf);
  }
  return (tmp);
}
}
extern int open(char const   *__file , int __oflag  , ...)  __attribute__((__nonnull__(1))) ;
static unsigned char const   *connect_mem  =    (unsigned char const   *)((void *)0);
static int memsize  =    0;
int parsefile(char const   *file ) 
{ int connect_fd ;
  struct stat filestat ;
  unsigned char const   *walk ;
  int i ;
  unsigned char const   *tmp ;
  unsigned char const   *tmp___0 ;
  void *tmp___1 ;
  void *tmp___2 ;
  struct state_node *mem_10 ;
  struct state_node *mem_11 ;
  struct state_node *mem_12 ;

  {
  {
  connect_fd = -1;
  connect_fd = open(file, 0);
  fstat__extinline(connect_fd, & filestat);
  memsize = (int )filestat.st_size;
  connect_mem = (unsigned char const   *)mmap((void *)0, (unsigned int )memsize, 3,
                                              2, connect_fd, 0L);
  close(connect_fd);
  num_states = 0;
  walk = connect_mem;
  }
  while (1) {
    if ((unsigned int )walk < (unsigned int )(connect_mem + memsize)) {

    } else {
      break;
    }
    if ((int const   )(*walk) == 36) {
      num_states = num_states + 1;
    } else {

    }
    walk = walk + 1;
  }
  {
  states = (struct state_node *)malloc(sizeof(struct state_node ) * (unsigned int )num_states);
  walk = connect_mem;
  i = 0;
  }
  while (1) {
    if (i < num_states) {

    } else {
      break;
    }
    mem_10 = states + i;
    mem_10->data = walk;
    while (1) {
      tmp = walk;
      walk = walk + 1;
      if (! ((int const   )(*tmp) != 36)) {
        break;
      } else {

      }
    }
    mem_11 = states + i;
    mem_12 = states + i;
    mem_11->length = (unsigned int )((walk - mem_12->data) - 1);
    while (1) {
      if ((unsigned int )walk < (unsigned int )(connect_mem + memsize)) {
        tmp___0 = walk;
        walk = walk + 1;
        if (! ((int const   )(*tmp___0) != 10)) {
          break;
        } else {

        }
      } else {
        break;
      }
    }
    i = i + 1;
  }
  return (0);
}
}
unsigned char const   *binbin(unsigned char const   *haystack , size_t haystack_size ,
                              unsigned char const   *needle , size_t needle_length ) 
{ int i ;
  unsigned char const   *mem_6 ;
  unsigned char const   *mem_7 ;

  {
  while (1) {
    if (haystack_size >= needle_length) {

    } else {
      break;
    }
    i = 0;
    i = 0;
    while (1) {
      if ((unsigned int )i < needle_length) {
        {
        mem_6 = haystack + i;
        mem_7 = needle + i;
        if (! ((int const   )(*mem_6) == (int const   )(*mem_7))) {
          break;
        } else {

        }
        }
      } else {
        break;
      }
      i = i + 1;
    }
    if ((unsigned int )i == needle_length) {
      return (haystack);
    } else {

    }
    haystack = haystack + 1;
    haystack_size = haystack_size - 1U;
  }
  return ((unsigned char const   *)((void *)0));
}
}
