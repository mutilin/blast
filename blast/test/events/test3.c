int x ;
int __BLAST_NONDET;

void error(){ ERROR: goto ERROR;}

void async2(int a) __attribute__((eventhandler)){
  a = a - 1;
  async2(a);
}
  
void async1(int a) __attribute__((eventhandler)) {
  if (a != 0) { error();}
  async2(a);

}

void ac_main(){
  x = 0; //comment to add bug 
  
  while (__BLAST_NONDET){async1(x);}
  
  x = 1; 
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
