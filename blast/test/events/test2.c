int x ;
int y;
int __BLAST_NONDET;

void error(){
ERROR: goto ERROR;
}

void async1() __attribute__((eventhandler)) {
  async2();
  x++; //comment to add bug
  y++; //comment to add bug
}

void async2() __attribute__((eventhandler)) {
  if (x != y) { error();}
}


void ac_main(){
  while (__BLAST_NONDET){
    async1 ();
  }
  x = 0; //comment to add bug 
  y = 0; //comment to add bug
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
