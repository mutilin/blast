int x ;


void error(){
ERROR: goto ERROR;
}

void async1() __attribute__((eventhandler)) {
  if (x != 0) { error();}
  async2();
  x = 1; // comment to add bug

}

void async2() __attribute__((eventhandler)) {
  if (x == 0) { error();} 
  async1();
  //x = 0; // comment to add bug
}


void ac_main(){
  async1 ();
  x = 0; //comment to add bug 
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
  ac_main();
  __BLAST_DISPATCH_FUNCTION();
}
