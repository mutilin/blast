int gid_flag;
int x ;
int __BLAST_NONDET;

int _gid = 0;
int get_new_gid(){
  int x;
  x = _gid;
  _gid++;
  return x;
}

void error(){ ERROR: goto ERROR;}

void async2(int gid, int k) __attribute__((eventhandler)) {
  int a2gid;
  
  a2gid = foo();
  
  if (a2gid == 0){gid_flag = 0;}
  
  if (k > 0){ 
    if (gid == 0){ 
      if (gid_flag != 0){ error();};  
      gid_flag = 1;
    }
    async1(a2gid,k-1);
  } else { 
    del_gid(gid);
  }




}

void async1(int gid,int k) __attribute__((eventhandler)) {
  if (gid ==0){gid_flag = 0;}
  if (gid == 0){ 
     if (gid_flag != 0){ error();};  
     gid_flag = 1;
  }
  async2(gid,k-1);
}

//STANDARD "rewrite" 
void __BLAST_DISPATCH_FUNCTION(){
	DISPATCH: goto DISPATCH;
}

void main(){
 int mygid;
 int n;

 gid_flag = 0;
 
 while (__BLAST_NONDET){
   mygid = get_new_gid();
   
   if (mygid == 0){ 
     if (gid_flag != 0){ error();};  
     gid_flag = 1;}
   async1(mygid,n);
 }
  
  __BLAST_DISPATCH_FUNCTION();
}
