int a,b;
struct buf {
  int x;
  int y;
};


void error(){
ERROR: goto ERROR;
}

void g(struct buf *p_)__attribute__((eventhandler)){
 struct buf *p;

 p = (struct buf *) p_;

 if (p -> x == 0) {error();}
}
  
void f() __attribute__((eventhandler)){
  struct buf *p;

  p = malloc();

  if (p -> x == 0) return;
  g(p);
}

//STANDARD "rewrite" 

void __BLAST_DISPATCH_FUNCTION() __attribute__((noreturn)){
	DISPATCH: goto DISPATCH;
}

void main(){
  f();
  __BLAST_DISPATCH_FUNCTION();
}
