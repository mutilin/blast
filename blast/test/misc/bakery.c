int __BLAST_NONDET;

int main()
{
  int pc1, pc2;
  int x1, x2;

  pc1 = 0;
  pc2 = 0;
  x1 = 0;
  x2 = 0;

  while (1) {
    if (__BLAST_NONDET) {
      if (pc1 == 0) {
        x1 = x2 + 1;
        pc1 = 1;
      } else if (pc1 == 1 && (x2 == 0 || x1 < x2)) {
        pc1 = 2;
      } else if (pc1 == 2) {
        pc1 = 0;
        x1 = 0;
      }
    } else {
      if (pc2 == 0) {
        x2 = x1 + 1;
        pc2 = 1;
      } else if (pc2 == 1 && (x1 == 0 || x2 < x1)) {
        pc2 = 2;
      } else if (pc2 == 2) {
        pc2 = 0;
        x2 = 0;
      }
    }
    if (pc1 == 2 && pc2 == 2) {
    ERROR: goto ERROR;
    }
  }
}

/*
would be sufficient. However, when I ran BLAST with the above predicates
given, BLAST still adds new predicates 0<=x2@main; 1<=x1@main; 2<=x2@main; 
x2@main<=2; 3<=x1@main; x1@main<=3, which I think is possible if BLAST
*/

