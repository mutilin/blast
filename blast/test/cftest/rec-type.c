struct node {
	int 	data;
	struct node* next;
} ;

void __error__(){
ERROR: 
}

void foo(struct node* x){
	x->data++;
	foo(x->next);
}


void main(){
struct node* x;

x = malloc(sizeof(struct node));
foo(x);
}
