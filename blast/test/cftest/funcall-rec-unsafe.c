int f2(int x);

int f1(int x)
{
    return f2(x+2);
}

int f2(int x)
{
    if (x <= 0) {
        return 1;
    }
    else {
        return f1(x-3);
    }
}

int fact(int x)
{
    if (x <= 0) {
        return 1;
    }
    else {
        int z;
        z = fact(x-1);
        return (z);
    }
}


int main()
{
    if (f1(-1) != 0) {
        goto ERROR;
    }

    return 0;

 ERROR: ;
}
