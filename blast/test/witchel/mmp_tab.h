#ifndef _MMP_TAB_H
#define _MMP_TAB_H

// This is a C file
#ifdef __cplusplus
extern "C" {
#endif

// This class implements MMP tables.  It is intended to be run by
// kernel software, and the talbes it writes are read by MMP
// hardware.  The ADT associates 2 bits with every _word_ address.
// The stack permisisons ADT associates a single bit with every word
// address. 
// Generic mmp table
// This is a descriptor which points to tables with pointers and
// permissions information in them.  The hardware points to the root
// table (that this descriptor also points to), and it reads 32-bit
// entries from the table.
struct mmpt;
// A table holds 32-bit entries.
typedef unsigned int mmpt_t; 
///////////////////////////////////////////////////////////////////
// What follows are the constructor and method definitions for 3
// implementations of the same ADT.  
// rle - run-length encoded entries
// bv - bit vector entries of permissions
// stack - bit vectors, but only a single bit per word address (RO/RW)
struct mmpt* mmpt_rle_alloc();
struct mmpt* mmpt_bv_alloc();
struct mmpt* mmpt_stack_alloc();

#define MMPT_NONE 0x0
#define MMPT_READ 0x1
#define MMPT_RO   0x1
#define MMPT_EX   0x2
#define MMPT_RW   0x3

// 2 bit values for RLE entries
#define RLE_PTR    0x0
#define RLE_DATA   0x1
#define RLE_BV_PTR 0x2

// Associate a data value with an address range.  Returns 1 if there
// was any non-zero value in the area written.
int mmpt_insert(struct mmpt*, unsigned long base, unsigned long len, 
                unsigned int prot);
// Set entries to zero, but don't allocate any new tables
int mmpt_delete(struct mmpt*, unsigned long base, unsigned long len);

// Get the data associated with an (word) address.
mmpt_t mmpt_get_prot(struct mmpt*, unsigned long base);
// Return the table entry which holds permissions data for address base
mmpt_t mmpt_get_entry(struct mmpt*, unsigned long base);
// Get the length from base to the end of the region that has the same
// permissions value as the value at base. This is not guaranteed to return
// the maximal length of the region. There might be abutting regions
// with identical data values.  Values before base might be in the
// same region, but that will not be reflected in the length.
unsigned int mmpt_get_len(struct mmpt* mmpt, unsigned long base);
// Get the smallest value of the region that has the same data value
// as at "base".  This is not guaranteed to return the smallest such
// value. 
unsigned long mmpt_get_base(struct mmpt* mmpt, unsigned long base);
// Print a human readable representation of the table
int mmpt_print(struct mmpt* mmpt, char* buf, int buflen, unsigned long base, 
              unsigned long len);
// Print a human readable representation of table use statistics
int mmpt_print_stats(struct mmpt* mmpt, char* buf, int len);
// Print a human readable representation of the entry that holds the
// information for this address
int mmpt_print_entry(struct mmpt* mmpt, unsigned long addr);
// Free the object
void mmpt_free(struct mmpt*);
// If there are RLE tables, verifies that the overlapping parts of
// each entry are correct 
void mmpt_verify_overlap(struct mmpt*, unsigned long base, unsigned long len);

#ifdef __cplusplus
}
#endif

#endif // _MMP_TAB_H
