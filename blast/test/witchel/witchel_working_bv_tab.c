/* Copyright 2005 Emmett Witchel */

#ifndef __KERNEL__
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "mmp_tab.h"
// This prints status messages about table insertions
// #define TRACE_INSERT
#else
#include <linux/mmp_tab.h>
#endif
#ifdef __KERNEL__
#undef TRACE_INSERT
extern void mmp_print_name_val_bochs(const char* name, unsigned long val);
static void* xmalloc(size_t size) {
   void* mem;
   mem = kmalloc(size, GFP_KERNEL);
   if(mem == 0) {
      extern void mmp_print_name_val_bochs(const char* name, unsigned long val);
      mmp_print_name_val_bochs("XXX SEG_TRACK kmalloc failed", size);
   }
   return mem;
}
static void xfree(void* ptr) {
   kfree(ptr);
}
#define DISABLE_INTERRUPTS(fl) local_irq_save(fl)
#define ENABLE_INTERRUPTS(fl) local_irq_restore(fl)
#else
static void* xmalloc(size_t size) { return malloc(size); }
static void xfree(void* ptr) { free(ptr); }
#define DISABLE_INTERRUPTS(fl) fl=fl;
#define ENABLE_INTERRUPTS(fl)
#endif // __KERNEL__

typedef unsigned int tab_t;
enum mmpt_type {
   MMPT_RLE,
   MMPT_BV,
   MMPT_STACK,
};
struct mmpt {
   int type;
   tab_t* tab;
   // Root table, first level table, leaf table
   unsigned long index_shift[3];
   unsigned long index_mask[3];
   unsigned long lg_num_subblock[3];
};

void __error__(){
ERROR: goto ERROR;
}

void __BLAST___error_(){
BERROR: goto BERROR;
}


// Mask values for each two bits of a bitvectory entry.  We start from
// the top because we set the low bit to indicate non-pointer.
static tab_t bv_mask[] = { ~(0x3 << 30),
                           ~(0x3 << 28), ~(0x3 << 26), ~(0x3 << 24),
                           ~(0x3 << 22), ~(0x3 << 20), ~(0x3 << 18),
                           ~(0x3 << 16), ~(0x3 << 14), ~(0x3 << 12),
                           ~(0x3 << 10), ~(0x3 << 8), ~(0x3 << 6),
                           ~(0x3 << 4), ~(0x3 << 2), ~(0x3 << 0),
};

// base must be a power of 2
static unsigned long round_up(unsigned long addr, int base) {
   unsigned long flat_addr = addr & ~(base-1);
   if(flat_addr != addr) return flat_addr + base;
   return flat_addr;
}

static unsigned int tab_nentries(struct mmpt* mmpt, int level) { 
   return mmpt->index_mask[level] + 1;
}
// The table length in terms of how many addresses does this table
// store data for.
static unsigned long tab_len(struct mmpt* mmpt, int level) {
   if(level == 0) return 0xFFFFFFFF;
   if(level == 3) return 1 << mmpt->index_shift[2];
   return tab_nentries(mmpt, level) * tab_len(mmpt, level + 1);
}
static inline unsigned long tab_base(struct mmpt* mmpt, unsigned long base, int level) {
   if(level == 0) return 0;
   base >>= mmpt->index_shift[level-1];
   return base << mmpt->index_shift[level-1];
}
static inline unsigned long tab_addr(struct mmpt* mmpt, unsigned long base, int idx, 
                                     int level) {
   return tab_base(mmpt, base, level) + idx * tab_len(mmpt, level+1);
}
static inline unsigned long make_addr(struct mmpt* mmpt, int i, int j, int k) {
   return i * tab_len(mmpt, 1) + j * tab_len(mmpt, 2) + k * tab_len(mmpt, 3);
}

static unsigned long subblock_len(struct mmpt* mmpt, int lev) {
   return (1UL << (mmpt->index_shift[lev] - mmpt->lg_num_subblock[lev]));
}
// Read the protections from an entry
static int entry_prot(struct mmpt* mmpt, tab_t entry, int idx) {
   switch(mmpt->type) {
   case MMPT_RLE:
      break;
   case MMPT_BV:
      // Data is stored big endian to allow low bit to be data
      // indicator in upper level entries
      assert(idx < 16);
      if (idx >= 16) { __error__();};
      
      return (entry >> (30 - 2*idx)) & 0x3;
      break;
   case MMPT_STACK:
      break;
   }
   assert(0);
   __error__();
   return -1;
}
static int entry_nonnull_data(struct mmpt* mmpt, tab_t entry, int level) {
   switch(mmpt->type) {
   case MMPT_RLE:
      break;
   case MMPT_BV:
      if(level < 2) return ((entry >> 1) != 0);
      return entry != 0;
      break;
   case MMPT_STACK:
      break;
   }
   assert(0);
   __error__();
   return -1;
}
static int interval_spans_entry(struct mmpt* mmpt, unsigned long p2base,
                                unsigned long p2len, int lev) {
   return ((p2base & (1UL << mmpt->index_shift[1])-1) == 0
           && p2len >= (1UL << mmpt->index_shift[lev]));
}
// Upper level entry is data if low bit is set
static int uentry_is_data(struct mmpt* mmpt, tab_t entry) {
   return ((entry&1) == 1);
}
static int make_idx(struct mmpt* mmpt, unsigned long base, int lev) {
   return ((base >> mmpt->index_shift[lev]) & mmpt->index_mask[lev]);
}
static int make_entry_idx(struct mmpt* mmpt, unsigned long base, int lev) {
   return ((base >> (mmpt->index_shift[lev] - mmpt->lg_num_subblock[lev]))
           & ((1 << mmpt->lg_num_subblock[lev]) - 1));
}

static tab_t* alloc_table(struct mmpt* mmpt, int level) {
   tab_t* tab = (tab_t*)xmalloc(tab_nentries(mmpt, level) * sizeof(tab_t));
   memset(tab, 0, tab_nentries(mmpt, level) * sizeof(*tab));
}
static void table_free(struct mmpt* mmpt, tab_t* tab, int lev) {
   int i;
   assert(lev > 0); //RJ: stronger invariant: lev == 1,2
   if (lev <= 0) __error__();

   if(lev == 1) {
      // Recursively free any second level tables
      for(i = 0; i < tab_nentries(mmpt, lev); ++i) {
         if(uentry_is_data(mmpt, tab[i]) == 0) {
            xfree((void*)tab[i]);
         }
      }
   }
   xfree(tab);
}

static int get_level(struct mmpt* mmpt, unsigned long p2base, unsigned long p2len) {
   if((p2base & (subblock_len(mmpt, 0)-1)) == 0
      && p2len >= subblock_len(mmpt, 0)) return 0;
   if((p2base & ((1UL << mmpt->index_shift[1])-1)) == 0
      && p2len >= subblock_len(mmpt, 1)) return 1;
   return 2;
}
static tab_t* get_mmpt_data_table(struct mmpt* mmpt, unsigned long p2base, 
                                  unsigned long p2len, int allocate_ok) {
   int lev = get_level(mmpt, p2base, p2len);
   if(lev == 0) {
      if(uentry_is_data(mmpt, mmpt->tab[make_idx(mmpt, p2base, 0)])) {
         // Entry is data.  If this write spans the entry (it might
         // only be sufficient for a subblock), then free
         // the tables pointed to and overwrite entry.  Otherwise
         // write into existant tables
         if(interval_spans_entry(mmpt, p2base, p2len, 0)) {
            table_free(mmpt, (tab_t*)mmpt->tab[make_idx(mmpt, p2base, 0)], 1);
         }
      }
      return mmpt->tab;
   } else if(lev == 1) {
      tab_t root_entry = mmpt->tab[make_idx(mmpt, p2base, 0)];
      if(root_entry == 0) {
         if(!allocate_ok) return NULL;
         // Allocate a level 1 table
         tab_t* tab = alloc_table(mmpt, 1);
         root_entry = mmpt->tab[make_idx(mmpt, p2base, 0)] = (tab_t)tab;
      } else if(uentry_is_data(mmpt, root_entry)) {
         // Need to break up this entry from 0 to 1
         assert(0);
	 __error__();
      }
      return (tab_t*)root_entry;
   }
   tab_t root_entry = mmpt->tab[make_idx(mmpt, p2base, 0)];
   if(root_entry == 0) {
      if(!allocate_ok) return NULL;
      // Allocate a level 1 table
      tab_t* tab = alloc_table(mmpt, 1);
      root_entry = mmpt->tab[make_idx(mmpt, p2base, 0)] = (tab_t)tab;
   } else if(uentry_is_data(mmpt, root_entry)) {
      // Need to break up this entry from 0 to 2
      assert(0);
      __error__();
   }
   tab_t* first_tab = (tab_t*)root_entry;
   tab_t first_entry = first_tab[make_idx(mmpt, p2base, 1)];
   if(first_entry == 0) {
      if(!allocate_ok) return NULL;
      // Allocate a level 2 table
      tab_t* tab = alloc_table(mmpt, 2);
      first_entry = first_tab[make_idx(mmpt, p2base, 1)] = (tab_t)tab;
   } else if(uentry_is_data(mmpt, first_entry)) {
      // Need to break up this entry from 1 to 2
      assert(0);
      __error__();
   }
   return (tab_t*)first_entry;
}

static unsigned int make_entry(struct mmpt* mmpt, unsigned long base, 
                               unsigned long len, unsigned int prot,
                               int lev, tab_t* _entry) {
   tab_t entry = *_entry;
   int entry_len = 0;
   int i;
   assert((prot & 0x3) == prot);
   if ((prot & 0x3) != prot) __error__();

   switch(mmpt->type) {
   case MMPT_RLE:
      
      break;
   case MMPT_BV: {
      for(i = make_entry_idx(mmpt, base, lev);
          i < (1 << mmpt->lg_num_subblock[lev])
             && len >= subblock_len(mmpt, lev); /*annoying bug when I
                                                   * forgot this*/
          ++i) {
         // If we are writing data into entry, low bit must be set
         if(lev < 2 && entry == 0) entry |= 1;
         assert(lev == 2 || uentry_is_data(mmpt, entry));
         if (lev == 2 || uentry_is_data(mmpt, entry)){}
	 else {__error__();};

	 
	 entry = (entry & bv_mask[i]) | (prot << (30 - (2 * i)));
         entry_len += subblock_len(mmpt, lev);
         len -= subblock_len(mmpt, lev);
         if(len == 0) break;
      }
   }
      break;
   case MMPT_STACK:
      break;
   }
   *_entry = entry;
   return entry_len;
}
static void mmpt_insert_pwr2(struct mmpt* mmpt, unsigned long p2base, 
                             unsigned long p2len, unsigned int prot,
                             int allocate_ok) {
   printf("INSERT %d (%#x) %d\n", p2base, p2base, p2len);
   while(p2len) {
      unsigned int idx;
      unsigned int len = 0;
      tab_t entry;
      tab_t* tab = get_mmpt_data_table(mmpt, p2base, p2len, allocate_ok);
      if(tab) {
         idx = make_idx(mmpt, p2base, get_level(mmpt, p2base, p2len));
         entry = tab[idx];
         len = make_entry(mmpt, p2base, p2len, prot, get_level(mmpt, p2base, p2len), &entry);
         tab[idx] = entry;
      }
      // printf("%d %d Entry %#x, len %d\n", p2base, p2len, entry, len);
      p2len -= len;
      p2base += len;
   }
}

static void
look_for_nonzero(struct mmpt* mmpt, tab_t* tab, int level, int* nonzero) {
   unsigned int idx;
   for(idx = 0; idx < tab_nentries(mmpt, level); ++idx) {
      if((level == 2 && tab[idx] != 0)
         || (level != 2 && uentry_is_data(mmpt, tab[idx]))) {
         *nonzero |= 1;
         break;
      }
      if(level < 2 && tab[idx] && !uentry_is_data(mmpt, tab[idx])) {
         // Recurse down
         look_for_nonzero(mmpt, (tab_t*)tab[idx], level + 1, nonzero);
      }
   }
}

// op_str is a 4 letter string indicating the operation. "CALL" and
// "FILL" are the two options, the latter referring to filling lower
// level tables with data from above.
static void
_mmpt_insert(struct mmpt* mmpt, unsigned long base, unsigned long* len,
             int prot, tab_t* tab, int level, int* nonzero,
             int allocate_ok, char* op_str) {
   unsigned int idx;
   tab_t entry;
   if(*len == 0) return;
#ifdef TRACE_INSERT
   printf("%s base (%#x) len %d lev %d prot %d\n", 
          op_str, base, *len, level, prot);
#endif
   idx = make_idx(mmpt, base, level);
   if(level < 2 
      && base == tab_base(mmpt, base, level + 1)
      && *len >= tab_len(mmpt, level + 1)) {
      // CASE A: Upper level, new region is aligned & spans at least one entry
      unsigned int entry_len;
#ifdef TRACE_INSERT
      printf("Case A len %d tlen %d ", *len, tab_len(mmpt, level+1));
#endif
      if(tab[idx] && !uentry_is_data(mmpt, tab[idx])) {
         // If this was a table pointer, the whole table can be freed
         // and replaced with an upper level entry
         look_for_nonzero(mmpt, (tab_t*)tab[idx], level, nonzero);
         // XXX Shouldn't this recusively free any level 2 tables?
         table_free(mmpt, (void*)tab[idx], level + 1);
         // Zero out old pointer, so make_entry works properly
         tab[idx] = 0;
      }
      entry = tab[idx];
      entry_len = make_entry(mmpt, base, *len, prot, level, &entry);
      tab[idx] = entry;
      *len -= entry_len;
      base += entry_len;
      // Now recurse, from the top
      _mmpt_insert(mmpt, base, len, prot, mmpt->tab, 0, nonzero, allocate_ok,
                   op_str);
   } else if(level < 2 && tab[idx] && !uentry_is_data(mmpt, tab[idx])) {
      // CASE B: Upper level, pointer entry
      // Recurse down through pointer
#ifdef TRACE_INSERT
      printf("Case B base %#x tab_base %#x len %d tab_len %d\n",
             base, tab_base(mmpt, base, level + 1),
             *len, tab_len(mmpt, level + 1));
#endif
      _mmpt_insert(mmpt, base, len, prot, (tab_t*)tab[idx], level + 1, nonzero,
                   allocate_ok, op_str);
   } else if(level < 2
             /* maddening bug && ((base & (~(subblock_len(mmpt, level)-1))) != 0*/
             && ((base & (subblock_len(mmpt, level)-1)) != 0
                 || *len < subblock_len(mmpt, level))
      ){
      // CASE C: Upper level, NULL or data entry, new region doesn't fit in
      // subblock (not aligned or not big enough)
      unsigned long upper_data_entry = tab[idx];
      unsigned int i;
#ifdef TRACE_INSERT
      printf("Case C: sublen %d(%#x) base& %#x len< %#x entry %#x\n",
             subblock_len(mmpt, level),  subblock_len(mmpt,level), 
             base & (~(subblock_len(mmpt, level)-1)),
             *len < subblock_len(mmpt, level), upper_data_entry);
#endif
      // We need to make a lower level table, and fill them with data
      // from upper level if entry was not NULL
      *nonzero |= (tab[idx] != 0);
      if(allocate_ok) {
         unsigned long sub_len;
         tab[idx] = (tab_t)xmalloc(tab_nentries(mmpt, level + 1) 
                                   * sizeof(*mmpt->tab));
         // Need to zero table, so junk in it isn't interpreted as pointers or data
         memset((tab_t*)tab[idx], 0, tab_nentries(mmpt, level + 1) 
                                   * sizeof(*mmpt->tab));
         // Fill it in with the data from the higher level
         for(i = 0; i < 1<<mmpt->lg_num_subblock[level]; ++i) {
            // Recurse to fill in table with old data.  Since these
            // are subblock chunks and we just allocated this table,
            // this will not infinite recurse.  It will waste some
            // cycles when all subblocks are the same, but at 8
            // subblocks that seems ok.  We would need at least 2
            // pieces to make sure the entry gets broken up.
            sub_len = subblock_len(mmpt, level);
            _mmpt_insert(mmpt, 
                         tab_base(mmpt, base, level+1) 
                         + i * subblock_len(mmpt, level),
                         &sub_len,
                         entry_prot(mmpt, upper_data_entry, i),
                         (tab_t*)tab[idx], level + 1, nonzero, allocate_ok, "FILL");
            assert(sub_len == 0);
	    if (sub_len != 0) __error__();
         }
         // Now recurse to fill in new data
         _mmpt_insert(mmpt, base, len, prot, (tab_t*)tab[idx], level + 1, nonzero,
                      allocate_ok, op_str);
      } else {
         unsigned int tlen = tab_len(mmpt, level + 1);
         // CASE D: Upper level, NULL or data entry, new region doesn't fit in
         // subblock (not aligned or not big enough), and not
         // allocating new tables
#ifdef TRACE_INSERT
      printf("Case D\n");
#endif
         if(tab[idx] != 0) {
         // Major bummer.  This must break a table, and we are not
         // supposed to allocate.  ERROR
            assert(0);
	    __error__();
         }
         // Recurse from top, after this entry, if necessary
         if(*len < tlen) return;
         *len -= tlen;
         _mmpt_insert(mmpt, tab_addr(mmpt, base, idx+1, level), len, prot,
                      mmpt->tab, 0, nonzero, allocate_ok, op_str);
      }
   } else {
      // CASE E: Any level, NULL or data entry, fill in the rest of
      // this table and recurse for the remainder if necessary.
#ifdef TRACE_INSERT
      printf("Case E sublen %d idx %d tab_nen %d\n", 
             subblock_len(mmpt, level), idx, tab_nentries(mmpt, level));
#endif
      for(; *len >= subblock_len(mmpt, level)
             && idx < tab_nentries(mmpt, level); idx++) {
         int entry_len;
         *nonzero |= (tab[idx] != 0);
         entry = tab[idx];
         entry_len = make_entry(mmpt, base, *len, prot, level, &entry);
         tab[idx] = entry;
         *len -= entry_len;
         base += entry_len;
         //printf("  base %#x len %d entry_len %d entry %#x\n", 
         //base, *len, entry_len, entry);
      }
      // Now recurse, from the top
      _mmpt_insert(mmpt, base, len, prot, mmpt->tab, 0, nonzero, allocate_ok, op_str);
   }
}


// Return the next power of two sized and aligned sub-segment of [base, base + len]
static void next_pwr2_seg(unsigned long *base/*I/O*/, unsigned long *len/*I/O*/, 
                          unsigned long *p2base/*OUT*/,unsigned long *p2len/*OUT*/) {
   unsigned long lg_len = 1;
   // Power of two sized region starts at base
   *p2base = *base;
   // Check for degenerate cases, so lg_len > 0
   if((*base & 0x1) != 0 || *len == 0) {
      *p2len  = 0;
      return;
   }
   // Find the largest power of two sized and aligned region starting
   // at base.  Do this by counting trailing binary zeros 
   while((*base & ((1UL<<lg_len)-1)) == 0
         && *len >= (1UL<<lg_len)) {
      lg_len++;
   }
   lg_len--; // We tested lg_len-1
   *p2len = (1UL << lg_len);
   *len -= *p2len;
   *base += *p2len;
}

static int real_mmpt_insert(struct mmpt* mmpt, unsigned long base, unsigned long len,
                            unsigned int prot, int allocate_ok) {
   // Round up length is not sufficient, because length can be
   // divisible by 4, but base is not word aligned
   len += (base&0x3);
   base &= ~0x3; 
   len = round_up(len, 4);

   while(len) {
      unsigned long p2base = 0;
      unsigned long p2len  = 0;
      next_pwr2_seg(&base, &len, &p2base, &p2len);
      mmpt_insert_pwr2(mmpt, p2base, p2len, prot, allocate_ok);
   }
}

int mmpt_insert(struct mmpt* mmpt, unsigned long base, unsigned long len, 
                unsigned int prot) {
   // real_mmpt_insert(mmpt, base, len, prot, 1);
   int nonzero = 0;
   _mmpt_insert(mmpt, base, &len, prot, mmpt->tab, 0, &nonzero, 1, "CALL");
   return nonzero;
}

int mmpt_delete(struct mmpt* mmpt, unsigned long base, unsigned long len) {
   // real_mmpt_insert(mmpt, base, len, 0, 0);
   int nonzero = 0;
   _mmpt_insert(mmpt, base, &len, 0, mmpt->tab, 0, &nonzero, 1, "CALL");
   return nonzero;
}

static char tab_data_buf[128];
static char* perm_str[] = {"- ", "R ", "X ", "W "};
static char* tab_data_str(struct mmpt* mmpt, tab_t entry, int lev) {
   int i;
   int lim = 128;
   int bw;
   char* str = tab_data_buf;
   switch(mmpt->type) {
   case MMPT_RLE:
      break;
   case MMPT_BV:
      // If this is upper level entry we assume it is data
      assert(lev == 2 || uentry_is_data(mmpt, entry));
      if (lev == 2 || uentry_is_data(mmpt, entry)){}
      else { __error__();}

      for(i = 0; i < (1<<mmpt->lg_num_subblock[lev]); ++i) {
         bw = snprintf(str, lim, "%s", 
                       perm_str[entry_prot(mmpt, entry, i)]);
         lim -= bw; str += bw;
      }
      break;
   case MMPT_STACK:
      break;
   }
   return tab_data_buf;
}
// Print a human readable representation of the table between
// addresses base and base + len
int mmpt_print(struct mmpt* mmpt, char* buf, int _strlen, unsigned long base, 
               unsigned long len) {
   int strlen = _strlen;
   int bw;
   unsigned int i;
   unsigned long flags;
   DISABLE_INTERRUPTS(flags);
   bw = snprintf(buf, strlen, 
                 "TABLE between %#x and %#x len %u (%#x)\n",
                 base, base + len, len, len);
   strlen -= bw;
   buf    += bw;
   for(i = 0; i < tab_nentries(mmpt, 0); ++i) {
      if(mmpt->tab[i] && !uentry_is_data(mmpt, mmpt->tab[i])) {
         unsigned int j;
         tab_t* tab1 = (tab_t*)mmpt->tab[i];
         for(j = 0; j < tab_nentries(mmpt, 1); ++j) {
            unsigned long jaddr = make_addr(mmpt, i, j, 0);
            if(tab1[j] && !uentry_is_data(mmpt, tab1[j])) {
               unsigned int k;
               tab_t *tab2 = (tab_t*)tab1[j];
               for(k = 0; k < tab_nentries(mmpt, 2); ++k) {
                  unsigned long kaddr = make_addr(mmpt, i, j, k);
                  // Always print the 0th entry in a table (so I know
                  // it is allocated), but don't print suceeding blank
                  // ones. 
                  if((k == 0 || entry_nonnull_data(mmpt, tab2[k], 2))
                     && kaddr >= base
                     && kaddr < base + len
                     ) {
                     bw = snprintf(buf, strlen, 
                                   "(i=%3d,j=%3d,k=%3d) %#9lx: %s\n",
                                   i, j, k, kaddr, tab_data_str(mmpt,tab2[k], 2));
                     strlen -= bw;
                     buf    += bw;
                     if(strlen <= 0) goto done;
                  }
               }
            } else if(uentry_is_data(mmpt, tab1[j])
                      && (j == 0 || entry_nonnull_data(mmpt, tab1[j], 1))
                      && jaddr >= base
                      && jaddr < base + len
               ) {
               bw = snprintf(buf, strlen, "(i=%3d,j=%3d      ) %#9lx: %s\n",
                             i, j, jaddr, tab_data_str(mmpt, tab1[j], 1));
               strlen -= bw;
               buf    += bw;
               if(strlen <= 0) goto done;
            }
         }
      } else if(uentry_is_data(mmpt, mmpt->tab[i])
                && (i == 0 || entry_nonnull_data(mmpt, mmpt->tab[i], 0))
         ) {
         bw = snprintf(buf, strlen, "(i=%3d            ) %#9lx: %s\n",
                       i, make_addr(mmpt, i, 0, 0),
                       tab_data_str(mmpt, mmpt->tab[i], 0));
         strlen -= bw;
         buf    += bw;
         if(strlen <= 0) goto done;
      }
   }
done:
   ENABLE_INTERRUPTS(flags);
   return _strlen - strlen;

}

struct mmpt* mmpt_bv_alloc() {
   struct mmpt* mmpt = (struct mmpt*)xmalloc(sizeof(struct mmpt));
   memset(mmpt, 0, sizeof(*mmpt));
   mmpt->type = MMPT_BV;
   mmpt->index_shift[0] = 22;    mmpt->index_mask[0] = 0x3FF; // 10
   mmpt->index_shift[1] = 12;    mmpt->index_mask[1] = 0x3FF; // 10
   mmpt->index_shift[2] =  6;    mmpt->index_mask[2] =  0x3F; //  6

   mmpt->lg_num_subblock[0] = 3;
   mmpt->lg_num_subblock[1] = 3;
   mmpt->lg_num_subblock[2] = 4;


   mmpt->tab = (tab_t*)xmalloc(tab_nentries(mmpt, 0) * sizeof(*mmpt->tab));
   memset(mmpt->tab, 0, tab_nentries(mmpt,0) * sizeof(*mmpt->tab));
   return mmpt;
}

char main_buf[1<<22];
int
main() {
   struct mmpt* mmpt = mmpt_bv_alloc();

   // Second level entries, not subblock aligned, straddles entries
   mmpt_insert(mmpt, 0x4, 100, MMPT_READ);
   mmpt_insert(mmpt, 60, 8, MMPT_EX);
   // First level entries, subblock aligned, straddles entries
   mmpt_insert(mmpt, 6*(1<<12) + (1<<9), (1<<9), MMPT_RW);
   mmpt_insert(mmpt, 7*(1<<12) + 7*(1<<9), 2*(1<<9), MMPT_EX);
   mmpt_insert(mmpt, 7*(1<<12) + (1<<9), 2*(1<<9), MMPT_READ);
   // Zero level entries, subblock aligned, straddles entries
   mmpt_insert(mmpt, 6*(1<<22) + (1<<19), (1<<19), MMPT_RW); // lev 3
   mmpt_insert(mmpt, 7*(1<<22) + 7*(1<<19), 2*(1<<19), MMPT_EX); // lev 3
   // First level entries, non-subblock length
   mmpt_insert(mmpt, 9*(1<<12) + 6*(1<<9), 2*(1<<9)+4, MMPT_EX);
   // First level entries, non-subblock aligned
    mmpt_insert(mmpt, 11*(1<<12) + 6*(1<<9)+4, 2*(1<<9), MMPT_RW); 
   // Add level 0 entry, overlap level 2 entry, copy data 
   // Level 0 aligned subblock, level 1 subblock length
   mmpt_insert(mmpt, 11*(1<<22) + 6*(1<<19), 2*(1<<9), MMPT_RW);
   mmpt_insert(mmpt, 11*(1<<22) + 7*(1<<19), 4, MMPT_READ);
   // Level 0 aligned, more than 1 level 1 subblock, but down to level 2
   mmpt_insert(mmpt, 11*(1<<22), (1<<12) + (1<<9) + 4, MMPT_EX); 

   mmpt_print(mmpt, main_buf, 1<<22, 0, 0xFFFFFFFF);
   printf("%s\n", main_buf);
   // Now lets overwrite some entries, overwrite level 0, freeing 4 tables
   mmpt_insert(mmpt, 11*(1<<22), (1<<22) + 4, MMPT_READ); 
   // Delete some entries in a level 2 table
   mmpt_delete(mmpt, 0xbd00 - 4, 9*(1<<6) + 8);
   // Delete some entries in a level 1 table
   mmpt_delete(mmpt, 7*(1<<12), 2*(1<<9));
   // Delete some entries in a level 2 table, causing copydown
   mmpt_delete(mmpt, 7*(1<<12) + 56 * (1<<6), 4);

   // Level 0 not aligned, length level 0 entry
   mmpt_insert(mmpt, 13*(1<<22) + 4, (1<<22), MMPT_EX); 

   mmpt_print(mmpt, main_buf, 1<<22, 0, 0xFFFFFFFF);
   printf("%s\n", main_buf);
   return 0;
}
