# 11 "srdriver.c"
typedef int NTSTATUS;




typedef int MessageType;
# 25 "srdriver.c"
typedef int StatusType;
# 45 "srdriver.c"
int fsmState = 1;
int statusFSM = 3;


void EMIT_FSM(MessageType message, StatusType status){
  if(fsmState==1){
    if(message == 0) {
      statusFSM = status;
      fsmState = 0;
      return;
    }
  } else {
    if(fsmState == 0) {
      if(statusFSM != 3){
        if(message == 1){
          fsmState = 2;
          return;
        }
      }
      else if (message == 2){
        fsmState = 3;
        return;
      }
    } else {
      if(fsmState == 2) {
        if((message == 4) && (status == statusFSM)){
          fsmState = 4;
          return;
        }
      } else {
        if(fsmState==3) {
          if(message == 3){
            fsmState = 2;
            return;
          }
        }
      }
    }
  }
  fsmState = 5;
ERROR: goto ERROR;
}




typedef int BOOLEAN;

struct Irp {
  int Status;
  int Information;
  int ListEntry;
};
typedef struct Irp *PIRP;

struct List {
  int data;
  struct List *next;
};

typedef struct List *PLIST_ENTRY;

struct Serial_Device_Extension {
  PIRP *CurrentReadIrp;
  struct List ReadQueue;
};

typedef struct Serial_Device_Extension *PSERIAL_DEVICE_EXTENSION;


struct Device {
  PIRP CurrentReadIrp;
  PSERIAL_DEVICE_EXTENSION DeviceExtension;
};
typedef struct Device *PDEVICE_OBJECT;




BOOLEAN IsListEmpty(PLIST_ENTRY foo) {
  int __BLAST_NONDET;
  if(__BLAST_NONDET) {
    return 1;
  } else {
    return 0;
  }
}

void InsertTailList(PLIST_ENTRY fooList, int *bar) {
;
}


NTSTATUS SerialIRPPrologue(PIRP irp, PSERIAL_DEVICE_EXTENSION extension) {
  StatusType s =0;
  return s;
}

void IoCompleteRequest(PIRP irp, int status) {
;
}

void IoMarkIrpPending(PIRP Irp) {
;
}

int IoGetCurrentIrpStackLocation(PIRP irp) {
  return 1;
}
# 162 "srdriver.c"
PIRP i;
PSERIAL_DEVICE_EXTENSION e;


NTSTATUS
SerialRead(
              PDEVICE_OBJECT DeviceObject,
              PIRP Irp
           ) {

  PSERIAL_DEVICE_EXTENSION extension = DeviceObject->DeviceExtension;
  NTSTATUS status;
  NTSTATUS status_success = 0;
  int tmp2;

  i = Irp;
  e = extension;

  status = SerialIRPPrologue(Irp, extension);
  if (status != 1) {
    Irp->Status = status;

    if(Irp==i) {
      EMIT_FSM(0, status);
    }

    IoCompleteRequest(Irp, 0);

    if(Irp==i) {
      EMIT_FSM(1, status);
    }


    EMIT_FSM(4, status);
    return status;
  }

  tmp2 = IoGetCurrentIrpStackLocation(Irp);

  if (tmp2!=0) {
    PLIST_ENTRY paraml = &(extension->ReadQueue);
    PIRP *paramirp = (extension->CurrentReadIrp);
    status = SerialStartOrQueue(
                                 extension,
                                 Irp,
                                 paraml,
                                 paramirp
                                 );

    EMIT_FSM(4, status);
    return(status);

   } else {
     Irp->Status = 0;

     if(Irp==i) {
       EMIT_FSM(0, 0);
     }

     IoCompleteRequest(Irp, 0);

     if(Irp==i) {
       EMIT_FSM(1, 0);
     }
     EMIT_FSM(4, 0);
     return status_success;
   }
}

NTSTATUS
SerialStartOrQueue(
                      PSERIAL_DEVICE_EXTENSION Extension,
                      PIRP Irp1,
                      PLIST_ENTRY QueueToExamine,
                      PIRP *CurrentOpIrp
                   ) {
  NTSTATUS tmpstatus;
  int tmp_empty;
  int *param_entry;


  tmp_empty = IsListEmpty(QueueToExamine);
  if ((tmp_empty==1) &&
              (*CurrentOpIrp)==0) {
    if(CurrentOpIrp == e->CurrentReadIrp) {
      e->CurrentReadIrp = &Irp1;
      *CurrentOpIrp = Irp1;
    }
    tmpstatus = SerialStartRead(Extension);
    return tmpstatus;
  } else {
    Irp1->Status = 1;

    if(Irp1==i) {
      EMIT_FSM(0, 1);
    }
    IoMarkIrpPending(Irp1);

    if(Irp1==i) {
      EMIT_FSM(2, 1);
    }

    param_entry = &(Irp1->ListEntry);
    InsertTailList(
                   QueueToExamine,
                   param_entry
                   );
    if (Irp1==i){
      EMIT_FSM(3, 1);
    }
    tmpstatus = 1;
    return tmpstatus;
  }
}


NTSTATUS
SerialStartRead(
       PSERIAL_DEVICE_EXTENSION Extension
    ) {
  PIRP *tmpirp_p = Extension->CurrentReadIrp;
  PIRP Irp2 = *tmpirp_p;

  BOOLEAN doneReading;

  int tmpstatus;





  if(doneReading==1){
    Irp2->Status = 0;

    if(Irp2==i) {
      EMIT_FSM(0, 0);
    }
    IoCompleteRequest(Irp2, 0);

    if(Irp2==i) {
      EMIT_FSM(1, 0);
    }
    EMIT_FSM(4, 0);
    tmpstatus = 0;
    return tmpstatus;
    }
    else {
      Irp2->Status = 1;

      if(Irp2==i) {
        EMIT_FSM(0, 1);
      }
      IoMarkIrpPending(Irp2);

      if(Irp2==i) {
        EMIT_FSM(0, 1);
      }






      EMIT_FSM(3, 1);
      tmpstatus = 1;
      return tmpstatus;
    }
}

PDEVICE_OBJECT dev;
PIRP irp;

int main() {
  fsmState = 1;
  statusFSM = 3;
  SerialRead(dev, irp);

}
