
int __BLAST_NONDET;

void swap1(int *a, int *b) {
	int tmp = *a;
	//*b = tmp;
	*a = *b;
       	*b = tmp;
}

void swap2(int *a, int *b) {
	*a = *a + *b;
	*b = *a - *b;
        *a = *a - *b;
}

void* malloc(int k);

void __error__(){
 ERROR: goto ERROR; 
}

void main () {
	
	int *i, *j;

	int v1, v2;

	i = malloc(4);
	j = malloc(4);

	*i = v1;
	*j = v2;

	
	  if (__BLAST_NONDET) {
	  swap1 (i, j);
	  } 
	  else{
	  swap2 (i,j);
	  }

	swap1(i,j);	
	// comment is for a v. technical reason: we find an error trace where *i = v2 BUT *j != v1 which is infeasible for various
	//reasons ... but foci is confused.

	if (__BLAST_NONDET){
	  if (*i != v2)
	    __error__();
	}
	else{
	  if (*j != v1)
	    __error__();
	}

}
