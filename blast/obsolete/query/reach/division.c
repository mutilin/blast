#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int readInt(void);

int division(int dividend, int divisor) {
  int remainder = dividend;
  int quotient = 0;
  assert(divisor > 0);
  assert(dividend > 0);
  while (divisor <= remainder) {
    remainder = remainder - divisor;
    ++quotient;
  }
L: return quotient;
  abort();
}

int main() {
  struct A {int *i;} a;
  int dividend, divisor;
  dividend = readInt();
  divisor = 0;
  divisor  = readInt();
  if(divisor  <= 0) return 1;
  if(dividend <= 0) return 1;
  printf("%d\n", division( dividend, divisor ));  

  ERROR: a.i = 0;
    *(a.i);
  return 0;

  abort();
}
