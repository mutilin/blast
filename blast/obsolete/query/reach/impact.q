// Change impact analysis.

Reach(f1, l1, f2, l2) := 
  REACH[ LOC_FUNC(f1,l1,"main"), LOC_RHSVAR(f2,l2,"j") ];
PRINT "Changed line has impact on the following uses of variable 'j':", ENDL;
PRINT Reach(_,_,file,line);

//PRINT LOC_LABEL(f1,l1,"START");
//PRINT LOC_RHSVAR(f2,l2,"j");

