// Dead code analysis.
Result(f1, l1, f2, l2) := 
  REACH[ LOC_FUNC(f1,l1,"main"), LOC_FUNC(f2,l2,_) ];
Reached(f,l) := Result(_,_,f,l);
PRINT "The following locations are not reachable within 'division':",ENDL;
PRINT !Reached(f,l) & LOC_FUNC(f,l,"main");