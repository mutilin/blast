int j;

void f(int j){
};

int compute() {
  int i;

  START: j = 1;
  i = 1;

  if (i==0) {
    f(j);      // reached if i==0
  }
  if (i==0) {
    j = 2;
  }
  if (j==2) {  // reached if i==1
    f(j);      // not reached if i==0
  }
  return 0;
}

int main() {
  compute();
  return 0;
}
