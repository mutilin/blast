// Reachability analysis for all locations.

Reach(f1, l1, f2, l2) := 
  REACH[ LOC_FUNC(f1,l1,"division"), LOC_FUNC(f2,l2,_) ];

PRINT "The following locations are used by function 'division':", ENDL;
PRINT Reach(_,_,t_file,t_line);
