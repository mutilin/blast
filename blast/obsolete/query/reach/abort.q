
// All possible locations (file,lineno).
SOURCE(file,lineno) := LOC_FUNC(file,lineno,"main");
// All locations containing abort.
TARGET(file,lineno) := EX(text, LOC_TEXT(file,lineno,text)
			      & @"abort"(text));
Result(f1,l1,f2,l2) := REACH[SOURCE(f1,l1), TARGET(f2,l2)];
PRINT Result(_,_,f2,l2);
