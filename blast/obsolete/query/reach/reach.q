// Reachability analysis for one location.

START(f,l) := LOC_FUNC(f, l, "main");
ERR(f,l) := LOC_LABEL(f, l, "ERROR"); 

Reach(f1, l1, f2, l2) := 
     REACH[ START(f1, l1), ERR(f2,l2) ];

PRINT #(Reach(f1, l1, f2, l2)), 
      " path(s) found for the following pairs:", ENDL;
PRINT Reach(f1, l1, f2, l2);
