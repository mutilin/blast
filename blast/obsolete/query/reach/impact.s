global int gDefined = 0; 
	
event {
    before
    pattern { j = $1; }
    guard {gDefined == 0}
    action { gDefined = 1; } 
}