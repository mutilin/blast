// CrocoPat is a tool for relational querying.
// This file is part of CrocoPat. 

// Copyright (C) 2002-2004  Dirk Beyer, Andreas Noack

// CrocoPat is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// CrocoPat is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with CrocoPat; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// Please find the GNU Lesser General Public License in file
// ../license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt

// Dirk Beyer    (beyer@eecs.berkeley.edu or Dirk.Beyer@web.de)
// University of California, Berkeley
// Andreas Noack (an@informatik.tu-cottbus.de or Andreas.Noack@gmx.com)
// Brandenburg Technical University, Cottbus

#ifndef _ctaObject_h
#define _ctaObject_h

#include <iostream>
using namespace std;

// The purpose of this class is to count the number
//   of objects instantiated within the Rabbit library.
// This is useful for checking memory deallocation.
class ctaObject
{
  // Static Methods.
public:
  // accessor methods
  static const int GetCount()
  {
    return(count);
  }

private:
  static long count;  // This counts the number of instatiated objects.

  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ctaObject&);

public:
  // constructor and destructor
  ctaObject()
  {
    ++count;   // We count the number of ctaObjects.
  }
  
  // We define a copy constructor for the case that
  //   an implicitly defined copy constructor of
  //   a subclass is used.
  ctaObject(const ctaObject& pObj)
  {
    ++count;			// We count the number of ctaObjects.
  }
  
  // this virtual destructor forces that all derivated components
  // also have virtual destructors without virtual declaration
  virtual ~ctaObject()
  {
    --count;
  }
};

ostream& operator << (ostream& out, const ctaObject* obj);

#endif
