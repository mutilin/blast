// CrocoPat is a tool for relational querying.
// This file is part of CrocoPat. 

// Copyright (C) 2002-2004  Dirk Beyer, Andreas Noack

// CrocoPat is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.

// CrocoPat is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with CrocoPat; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// Please find the GNU Lesser General Public License in file
// ../license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt

// Dirk Beyer    (beyer@eecs.berkeley.edu or Dirk.Beyer@web.de)
// University of California, Berkeley
// Andreas Noack (an@informatik.tu-cottbus.de or Andreas.Noack@gmx.com)
// Brandenburg Technical University, Cottbus

#ifndef _bddRelation_h_
#define _bddRelation_h_

#include "bddBdd.h"
#include "bddSymTab.h"
#include "relDataType.h"

// Set of tupels represented by a BDD.

// It is possible that there exist bit vectors which are no valid encodings
//   of tuples (because the cardinalities of ranges 
//   of attributes do not need to be powers of 2).
//   For speed, these out-of-range bit vectors are not generally excluded from
//   representation. 
//   Value mSymTab->getUniverseSize()-1 is represented by all values 
//   mSymTab->getUniverseSize()-1 ... 2**mSymTab->getBitNr()-1.
//   This invariant keeps the comparison operations 
//   (setEqual, setContains, isEmpty) correct.
//   However, methods for output or tuple counts (getTupleNr) have to
//   restrict to the set of all range tuples.

class bddRelation : public relDataType
{

private: // Private static methods.

  // Returns all tuples satisfying pVI == pConst.
  static bddRelation
  mkEqualPure(const bddSymTab* pSymTab,
              unsigned pVarId, 
              reprNUMBER pConst)
  {
    return bddRelation( pSymTab,  bddBdd(pVarId, pSymTab->getBitNr(), pConst) );
  }

public: // Public static methods.

  // Returns all tuples satisfying pVI == pConst.
  //   If (pConst) is the max value of pVI (pSymTab->getUniverseSize()-1)
  //   then the completion for out-of-range-values is applied.
  //   This is to fulfill the invariant of bddRelation 
  //   (see comment on top of bddRelation.h).
  static bddRelation
  mkEqual(const bddSymTab* pSymTab,
          unsigned pVarId, 
          reprNUMBER pConst)
  {
    // Standard operation.
    assert(pConst < pSymTab->getUniverseSize());
    bddRelation result( mkEqualPure(pSymTab, pVarId, pConst) );

    // If (pConst) is the value (pSymTab->getUniverseSize()-1)
    //   the (pVI) must equal all values greater than 
    //   (pSymTab->getUniverseSize()-1), i.e. from the interval 
    //   [ pSymTab->getUniverseSize()-1, 2^(pSymTab->getBitNr())-1 ].
    if( pConst+1 == pSymTab->getUniverseSize() )  // '+' because we use unsigned.
    { 
      // From maximal value within range to maximal value represented by BDD.
      for (unsigned lVal = pSymTab->getUniverseSize(); 
           lVal <= (unsigned) ( 1 << pSymTab->getBitNr() ) - 1; // 2^n == (1<<n).
           ++lVal)
      {
        result.unite( mkEqualPure(pSymTab, pVarId, lVal) );
      }
    }
    return result;
  }

  // Returns set of all tuples satisfying pVar1 == pVar2.
  static bddRelation
  mkEqual(const bddSymTab* pSymTab,
          const string& pVar1, 
          const string& pVar2)
  {
    bddRelation result(pSymTab, false);
    unsigned lVarId1 = pSymTab->getAttributePos(pVar1);
    unsigned lVarId2 = pSymTab->getAttributePos(pVar2);

    for(reprNUMBER lValueIt = 0;
        lValueIt < pSymTab->getUniverseSize();
        ++lValueIt)
    {
      bddRelation lValuePair = mkEqual(pSymTab, lVarId1, lValueIt);
      lValuePair.intersect(    mkEqual(pSymTab, lVarId2, lValueIt));
      result.unite(lValuePair);
    }
    return result;
  }

  // Returns set of all tuples satisfying pVar1 < pVar2.
  // Assertion: pVar1's position  <=  pVar2's position in the variable order.
  static bddRelation
  mkLess(const bddSymTab* pSymTab,
         const string& pVar1, 
         const string& pVar2)
  {
    bddRelation result(pSymTab, false);
    unsigned lVarId1 = pSymTab->getAttributePos(pVar1);
    unsigned lVarId2 = pSymTab->getAttributePos(pVar2);
    assert(lVarId1 <= lVarId2);

    bddRelation lVar2Values(pSymTab, false);
    for(int lValueIt = pSymTab->getUniverseSize()-1;
        lValueIt > 0 ;
        --lValueIt)
    {
      // Construct set {lValueIt, ..., getUniverseSize()-1}
      lVar2Values.unite( mkEqual(pSymTab, lVarId2, lValueIt) );

      // Construct relation {lValueIt-1 < lValueIt, ..., lValueIt-1 < pSymTab->getUniverseSize()-1}.
      bddRelation lRel(lVar2Values);
      lRel.intersect( mkEqual(pSymTab, lVarId1, lValueIt-1) );
      result.unite(lRel);
    }
    return result;
  }

  // Return all tupels where the
  //   value of attribute (pAttributeName) is (pAttributeValue).
  static bddRelation
  mkAttributeValue(const bddSymTab* pSymTab,
                   const string& pAttributeName, 
                   const string& pAttributeValue)
  {
    return mkEqual(pSymTab, 
                   pSymTab->getAttributePos(pAttributeName), 
                   pSymTab->getValueNum(pAttributeValue));
  }
  

private: // Attributes.

  // Symbol table (associated).
  const bddSymTab* mSymTab;
  // Set of tuples.
  bddBdd mBdd;


public: // Constructors and destructor.
  
  bddRelation(const bddSymTab* pSymTab, bool full)
    : mSymTab(pSymTab),
      mBdd(full)
  {}
  // Use the standard copy constructor. 
  // Use the standard destructor.
  // Use the standard operator '='.

private:

  bddRelation(const bddSymTab* pSymTab, const bddBdd& pBdd)
    : mSymTab(pSymTab),
      mBdd(pBdd)
  {}
  // Forbid implicite casts.
  bddRelation(void*);
  // Forbid the use of some ugly standard operators.
  void operator,(const bddRelation&);

private: // Service methods.

  // Renaming of attributes.
  //   Safe variant, without preconditions.
  void
  renameSafe(unsigned pVarIdOld, unsigned pVarIdNew, unsigned pBitNr) 
  {
    // Walk through all BDD variables.

    // For all bits of the binary encoding of both attributes.
    if( pVarIdOld > pVarIdNew )
    {
      for (unsigned lIt = 0; 
           lIt < pBitNr;
           ++lIt)
      {
        unsigned lPosOld = pVarIdOld + lIt;     
        unsigned lPosNew = pVarIdNew + lIt;     
        
        mBdd.intersect(bddBdd(lPosOld, lPosNew));
        mBdd.exists(lPosOld);
      }
    }
    else
    {
      for (int lIt = pBitNr-1; 
           lIt >= 0;
           --lIt)
      {
        unsigned lPosOld = pVarIdOld + lIt;     
        unsigned lPosNew = pVarIdNew + lIt;     
        
        mBdd.intersect(bddBdd(lPosOld, lPosNew));
        mBdd.exists(lPosOld);
      }
    }
  }


public: // Service methods.

  // Returns number of tuples, restricted to the given attributes.
  // Assuming that all free attributes are contained in 'pFree'.
  double
  getTupleNr(const set<string>& pFree) const
  {
    if (pFree.size() == 0) {
      return isEmpty() ? 0 : 1;
    }

    map<unsigned,string> lVarOrd = mSymTab->computeVariableOrder(pFree);
    unsigned lFirstVar = lVarOrd.begin()->first;
    unsigned lLastVar  = (--lVarOrd.end())->first;
    assert(lFirstVar <= lLastVar);

    // First exclude out-of-range bit vectors 
    //   (see comment at top of the class).
    bddRelation lTmp = *this;
    { // All possible values of all attributes in the given range.
      for (unsigned lVarId = lFirstVar;  
	   lVarId <= lLastVar;  
	   lVarId += mSymTab->getBitNr() ) 
      {
        bddRelation lRange(mSymTab, false);
	if (lVarOrd.find(lVarId) == lVarOrd.end()) {
          lRange.unite(mkEqualPure(mSymTab, lVarId, 0));
	} else {
	  for (reprNUMBER lValueIt = 0;
	       lValueIt < mSymTab->getUniverseSize();
	       ++lValueIt)
	  {
	    lRange.unite(mkEqualPure(mSymTab, lVarId, lValueIt));
	  }
	}
        lTmp.intersect(lRange);
      }
    }

    // No nodes allowed in front of the first variable id.
    assert( !lTmp.mBdd.testVars(0, lFirstVar - 1) );
    return lTmp.mBdd.getTupleNr(lFirstVar, lLastVar + mSymTab->getBitNr() - 1);
  }

  // Returns a value for attribute at position 'pVarId'.
  // Assumes that the relation is not empty.
  string
  getElement(unsigned pVarId)
  {
    unsigned lNumValue = mBdd.getTuple(pVarId, pVarId + mSymTab->getBitNr()-1);
    return mSymTab->getAttributeValue(lNumValue);
  }


public: // Operations.

  bool
  setEqual(const bddRelation& p) const {
    return mBdd.setEqual( p.mBdd );
  }

  // Check if (*this) contains (p).
  bool
  setContains(const bddRelation& p) const {
    return mBdd.setContains( p.mBdd );
  }
  
  bool
  isEmpty() const {
    return mBdd.isEmpty();
  }
  
  void
  complement() {
    mBdd.complement();
  }

  void
  unite(const bddRelation& p) {
    mBdd.unite(p.mBdd);
  }

  void
  intersect(const bddRelation& p) {
    mBdd.intersect(p.mBdd);
  }

  // Existential quantification of (pAttribute).
  void
  exists_old(const string pAttribute) {
    unsigned lVarId = mSymTab->getAttributePos(pAttribute);
    // Existential quantification of 'mSymTab->getBitNr()' bits,
    //   beginning at 'lVarId', i.e., for all bits of the encoding of 'pAttribute'.
    for (int i = mSymTab->getBitNr() - 1;  i >= 0;  --i)
    {
      mBdd.exists(lVarId + i);
    }
  }

  // Existential quantification of (pAttribute).
  void
  exists(const string pAttribute) {
    unsigned lVarId = mSymTab->getAttributePos(pAttribute);
    // Existential quantification of 'mSymTab->getBitNr()' bits,
    //   beginning at 'lVarId', i.e., for all bits of the encoding of 'pAttribute'.
    mBdd.exists(lVarId, lVarId + mSymTab->getBitNr() - 1);
  }

  // Check if there is any BDD node within the given range of attributes.
  bool
  testVars(string pVarFirst, string pVarLast)
  {
    unsigned lVarIdFirst = mSymTab->getAttributePos(pVarFirst);
    unsigned lVarIdLast  = mSymTab->getAttributePos(pVarLast) + mSymTab->getBitNr()-1;
    assert(lVarIdFirst <= lVarIdLast);
    return mBdd.testVars(lVarIdFirst, lVarIdLast);
  }

  // Renaming of attributes.
  void
  rename(const string pAttributeOld, const string pAttributeNew) {
    unsigned lVarIdOld = mSymTab->getAttributePos(pAttributeOld);
    unsigned lVarIdNew = mSymTab->getAttributePos(pAttributeNew);

    // Renaming of attributes. 

    // For direct renaming:
    // Forbidden range of variable ids for renaming [min, max].
    // Case 1) pVarIdOld < pVarIdNew
    unsigned lVarIdFirst = lVarIdOld + mSymTab->getBitNr();
    unsigned lVarIdLast  = lVarIdNew + mSymTab->getBitNr()-1;
    // Case 2) pVarIdOld > pVarIdNew
    if( lVarIdOld > lVarIdNew )
    {
      lVarIdFirst = lVarIdNew;
      lVarIdLast  = lVarIdOld - 1;
    }

    // Check if there is any BDD node of a varId between the variables
    //   or of a varId of the new variable.
    if( mBdd.testVars(lVarIdFirst, lVarIdLast) )
    {
      // Safe variant, without preconditions.
      renameSafe(lVarIdOld, lVarIdNew, mSymTab->getBitNr());
    }
    else
    {
      // Direct variant, 
      //   with the precondition that no BDD node exists between the variables.
      // Parameters: First varId to rename.
      //             Last varId to rename.
      //             Offset, i.e. distance to new.
      mBdd.renameVars(lVarIdOld,
                      lVarIdOld + (mSymTab->getBitNr() - 1),
                      lVarIdNew - lVarIdOld
                      );
    }
  }


public: // IO.

  void
  printBddInfo(ostream& pS) {
    // Includes garbage collection to get real values.
    unsigned lFreeNodes = mBdd.getFreeNodeNr();
    unsigned lMaxNodes  = mBdd.getMaxNodeNr();

    // Infos about BDD package.
    pS << "Number of BDD nodes: " << mBdd.getNodeNr() << endl
       << "Percentage of free nodes in BDD package: " 
       << lFreeNodes << " / " << lMaxNodes << " = " 
       << lFreeNodes * 100 / lMaxNodes << " %" << endl;
  }

  // Print set of tuples (plain) according to the given order of attributes.
  // Recursiv procedure.
  void 
  printRelation(ostream& pS, 
                const string pTuple,
                vector<string> pAttributeList) const 
  {
    // Empty relation?
    if( isEmpty() ) {
      return;
    }

    // End of Recursion.
    if( pAttributeList.empty() ) {
      pS << pTuple << endl;
      return;
    }

    // Proceed next attribute (i.e. column in relation) in given list.
    // Use a copy of this relation.
    bddRelation lRel(*this);
    
    // Output of the values of the first attribute in (pTermList),
    //   i.e. the given ordering is regarded.
    unsigned lVarId = mSymTab->getAttributePos( * pAttributeList.begin() );
    pAttributeList.erase(pAttributeList.begin());

    // For all values of the current attribute.
    while (!lRel.isEmpty()) {
      // Get next value of the attribute.
      unsigned lNumValue = lRel.mBdd.getTuple(lVarId, 
                                              lVarId + mSymTab->getBitNr()-1);

      // Compute cofactor for current value in (lTmpRel).
      bddRelation lTmpRel(lRel);
      lTmpRel.intersect( mkEqual(mSymTab, lVarId, lNumValue) );
      // Print value.
      string lStringValue( mSymTab->getAttributeValue(lNumValue) );
      if( mSymTab->isQuoted(lStringValue) )
      {
        lStringValue = '"' + lStringValue + '"';
      }
      // Recursive call for the next attribute.
      lTmpRel.printRelation(pS, pTuple + lStringValue + '\t', pAttributeList);
      
      // Delete printed tuples.
      lTmpRel.complement();  
      lRel.intersect( lTmpRel );
    }
  }
};

#endif // _bddRelation_h_
