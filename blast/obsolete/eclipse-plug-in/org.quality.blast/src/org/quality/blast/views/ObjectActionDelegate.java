
package org.quality.blast.views;
import org.quality.blast.CBlastPlugin;

import org.eclipse.ui.*;import org.eclipse.core.resources.IFile;import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.action.IAction;import org.eclipse.jface.viewers.*;
public class ObjectActionDelegate implements IObjectActionDelegate {    public  IAction mAction;	public  IWorkbenchPart mWorkbenchPart;
    private IFile mFile;    public ObjectActionDelegate() {    }    public void setActivePart(IAction pAction, IWorkbenchPart pWorkbenchPart) {        mAction = pAction;
        mWorkbenchPart = pWorkbenchPart;    }
    public void run(IAction action) {
		VerTask lVerTask =
			new VerTask(
				mFile.getName() + "_main",
				"Assertion checking",
				mFile,
				"main",
				"unchecked");

		VerTaskPropertiesDialog dialog =
			new VerTaskPropertiesDialog(mWorkbenchPart.getSite().getShell(), lVerTask);
		if (dialog.open() == Dialog.OK) {
			CBlastPlugin.getPlugin().add(lVerTask);
		}

	}    public void selectionChanged(IAction act, ISelection sel) {
	 	Object lItem = ((IStructuredSelection)sel).getFirstElement();
    	if (lItem instanceof IFile){        	mFile = (IFile) lItem;		}
    }}