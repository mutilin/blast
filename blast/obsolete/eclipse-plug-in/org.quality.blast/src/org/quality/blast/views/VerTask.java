/*
 * Created on Nov 25, 2003
 *
 * @author beyer
 */

package org.quality.blast.views;

import org.quality.blast.CBlastPlugin;

import java.io.*;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
//import org.eclipse.jface.text.*;
//import org.eclipse.swt.SWT;

public class VerTask {

	public static final String assert    = "Assertion checking";
	public static final String reach     = "Reachability analysis";
	public static final String deadcode  = "Dead code analysis";
	public static final String test      = "Test Case Generation";
	public static final String spec      = "Specification checking";

	private String  name;
	private String  description;
	private IFile   fileRes;
	private String  method;
	private String  options;
	private String  status;

	// Optional.
	// ... for reach.
	private String  label;
	
	// ... for deadcode.
	private int     deadNodes;
	private int		liveNodes; 

	// ... for spec.
	private String  specFile;

	public VerTask(String pName,
				   String pDescription,
				   IFile  pFile,
		           String pMethod,
		           String pStatus) {
		name        = pName;
		description = pDescription;
		fileRes     = pFile;
		method      = pMethod;
		status      = pStatus;
		
		options   = "";
		label     = "ERROR";
		deadNodes = 0;
		liveNodes = 0;
		specFile  = "/";
	}

	public String mkString() throws CoreException{
		return  name           + "," +
				description    + "," +
				fileRes.getProject().getDescription().getName() + "," +
				fileRes.getProjectRelativePath().toOSString()   + "," +
				method         + "," +
				options        + "," +
				status         + "," +
				label          + "," +
				deadNodes      + "," +
				liveNodes      + "," +
				specFile;
	}

	public void run(VerTaskListView tasklist) {
		String endl = System.getProperty("line.separator");

		// Perform preprocessing.
		File   file        = getFile();
		System.out.println("Checking file: " + file.getPath());
		IPath installDir  = getPlugin().getInstallDir();
		String lPathString = installDir.toOSString();
		IPath  lPath       = CBlastPlugin.getWorkspace().getPathVariableManager().getValue("PATH");
		if (lPath != null) {
			lPathString += ";" + lPath.toOSString();
		}
		System.out.println("PATH=" + lPathString);
		String env[]      = { "PATH=" + lPathString };
		String lPreProc = "gcc -E -I " + installDir.toOSString() + " " + file.getName();
		System.out.println(lPreProc);
		try {
			Runtime lRT = Runtime.getRuntime();
			Process lP  = lRT.exec(lPreProc, env, file.getParentFile());

			String lResult    = tasklist.fileToString(lP.getInputStream(), false);
			String lErrResult = tasklist.fileToString(lP.getErrorStream(), false);
			lP.waitFor();
			tasklist.stringToFile(lResult, 
								  new File(getPlugin().extBlastName(this)));
			if (lP.exitValue() != 0) {
				MessageDialog.openInformation(
					tasklist.getSite().getShell(),
					"Blast preprocessing - Error occured",
					lErrResult);
				return;
			}
		} catch (Exception e) {
			errorDialog(tasklist.getSite().getShell(), e);
		}


	    // Prepare verification.
		File absFile = new File(getPlugin().extAbsName(this));
		File logFile = new File(getPlugin().extLogName(this));
		String lVerCmdLine = installDir.append("pblast.opt").toOSString()
				+ " -nocache"
				+ " " + options
				+ " -main " + method
				+ " " + getPlugin().extBlastName(this);

		String lResult = "";
		String lErrResult = "";


		// Check whether old proof exists and is still valid (file based).
		// Only for assertion checking or reachability analysis.
		if (description.equals(assert) || description.equals(reach)) {
			if (logFile.exists()) {
				if (file.lastModified() < logFile.lastModified()) {
					// Old proof exists, and is still valid.
					MessageDialog.openInformation(
					tasklist.getSite().getShell(),
						"Blast verification results for task " + name,
						"The system is: " + status + endl +
						"-----------------------------------" + endl +
						"Existing certificate is still valid.");
					return;
				}
				// Old proof exists, but (may be) needs to be updated.
				// Support incremental verification.
				lVerCmdLine += " -loadabs " + getPlugin().extAbsName(this);
			}
		}


		// Extend command line.
		// Assert.
		if (description.equals(assert)) {
			lVerCmdLine = lVerCmdLine + " -quiet"
				+ " -plaintracefile " + getPlugin().extTraceName(this)
				+ " -dumpabs " + getPlugin().extAbsName(this);
		}
		// Reach.
		if (description.equals(reach)) {
			lVerCmdLine = lVerCmdLine + " -quiet"
				+ " -L " + label
				+ " -plaintracefile " + getPlugin().extTraceName(this)
				+ " -dumpabs " + getPlugin().extAbsName(this);
		}
		// Deadcode.
		if (description.equals(deadcode)) {
			lVerCmdLine = lVerCmdLine
					+ " -testfile " + getPlugin().extTestName(this)
					+ " -test -noblock -quiet ";
		}
		// Test.
		if (description.equals(test)) {
			lVerCmdLine = lVerCmdLine
					+ " -testfile " + getPlugin().extTestName(this)
					+ " -test -noblock -quiet ";
		}
		// Spec.
		if (description.equals(spec)) {
			// We have to do the ugly two steps here.
			lVerCmdLine = lVerCmdLine
					+ " -testfile " + getPlugin().extTestName(this)
					+ " -test -noblock -quiet ";

			MessageDialog.openInformation(
				tasklist.getSite().getShell(),
				"Blast specification checking",
				"Function not yet integrated.");
			return;

		}


		// Perform verification.
		Runtime lRT = Runtime.getRuntime();
		Process lP = null;
		System.out.println(lVerCmdLine);
		try {
			System.out.println("Waiting for child process ...");
			lP  = lRT.exec(lVerCmdLine, env, new File(file.getParent()));
			// Assume there is just a little output on stderr.
			// Otherwise we would have to read both string interleaved, to avoid a waiting pipe.
			lResult    = tasklist.fileToString(lP.getInputStream(), true);
			lErrResult = tasklist.fileToString(lP.getErrorStream(), true);

			// If Blast prints out a lot of messages to stderr, 
			//   then the stream given by lP.getErrorStream does not work.
			//   I don't know why. 
			// The following implementation of the stream handling should
			//   work, even for a huge amount of output.
			/*
			try {
				InputStream lIn  = lP.getInputStream();
				InputStream lErr = lP.getErrorStream();

				int lInChar  = 0;
				int lErrChar = 0;
				while (lInChar != -1 || lErrChar != -1) {
					lInChar = lIn.read();
					lErrChar = lErr.read();
					if(lInChar != -1) {
						lResult += (char) lInChar;
					}
					if(lErrChar != -1) {
						lErrResult += (char) lErrChar;
					}
				}
				lIn.close();
				lErr.close();
			}
			catch (Exception e)	{
				tasklist.showMessage(e.getMessage());
			}
			*/

			lP.waitFor();
			System.out.println("Done.");
		} catch (Exception e) {
			errorDialog(tasklist.getSite().getShell(), e);
		}
		lResult = "The system is: " + status 		+ endl
				+ "******************************" 	+ endl
				+ "Blast output:"					+ endl
				+ lResult							+ endl
				+ "******************************" 	+ endl
				+ "Blast error output:"				+ endl
				+ lErrResult						+ endl
				+ "******************************" 	+ endl;


		// Process results.
		// Assert and reach.
		if (description.equals(assert) || description.equals(reach)) {
			if (lP.exitValue() != 0) {
				status = "unchecked";
			} else if (lResult.indexOf("The system is safe") >= 0) {  // Found.
				status = "safe";
			} else {
				status = "unsafe";
			}
		}
		// Deadcode.
		if (description.equals(deadcode) || description.equals(test)) {
			if (lP.exitValue() == 0) {
				// Fetch node numbers somehow.
				{
					BufferedReader bufReader = new BufferedReader(new StringReader(lResult));
					String lTmpString;
					String hintLive = "Number of live locations = ";
					String hintDead = "Number of dead locations = ";
					try {
						while ((lTmpString = bufReader.readLine()) != null) {
							if (lTmpString.indexOf(hintLive) >= 0) {   // Found.
								liveNodes = Integer.parseInt(lTmpString.substring(hintLive.length()));
							}
							if (lTmpString.indexOf(hintDead) >= 0) {   // Found.
								deadNodes = Integer.parseInt(lTmpString.substring(hintDead.length()));
							}
						}
					} catch (Exception e) {
						errorDialog(tasklist.getSite().getShell(), e);
					}
				}

				// Dead code markers.
				{
					BufferedReader lIn;
					try {
						// Delete old markers.
						fileRes.deleteMarkers(null, false, 1);
						
						lIn = new BufferedReader(new InputStreamReader(
												new FileInputStream(tasklist.getPlugin().extTestNameList(this))));
						String lTmpString;
						while ((lTmpString = lIn.readLine()) != null) {
							if (lTmpString.length() > 2) {
								String[] lLine = lTmpString.split(" ");
								int lLineNo    = Integer.parseInt(lLine[1]);
								try {
									IMarker marker = fileRes.createMarker(IMarker.PROBLEM);
									if (marker.exists()) {
										marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
										marker.setAttribute(IMarker.MESSAGE, "Line contains dead code");
										//marker.setAttribute(IMarker.PRIORITY, IMarker.PRIORITY_HIGH);
										marker.setAttribute(IMarker.LINE_NUMBER, lLineNo);
									}
								} catch (CoreException e) {
									MessageDialog.openInformation(
										CBlastPlugin.getPlugin().getShell(),
										"Problem with Marker",
										"Problem with marker for resource: " + fileRes.getProjectRelativePath().toOSString());
								}
							}
						}
						lIn.close();
					}
					catch (Exception e)	{
						tasklist.showMessage(e.getMessage());
					}
				}
				// Remember status.
				status = "checked";
			} else {
				status = "unchecked";
			}
		}
		// Spec.
		if (description.equals(spec)) {
		}

		// Common.
		// Make changes of task data (status) visible.
		getPlugin().updateViews();
		// Dump results to log file and to screen.
		tasklist.stringToFile(lResult, logFile);

		//IDocument lDoc = new Document(lResult);
		//TextViewer lTextViewer = new TextViewer(tasklist.getSite().getShell(), SWT.DEFAULT);
		//lTextViewer.setDocument(lDoc);

		//System.err.println(lTextViewer.getControl().isVisible());
		//System.err.println(lTextViewer.getDocument().get());
		
		MessageDialog.openInformation(
			tasklist.getSite().getShell(),
			"Blast verification results for task " + name,
			lResult);

	}

	private void errorDialog(Shell shell, Exception e) {
		MessageDialog.openInformation(
			shell,
			"Blast verification - Java Exception",
			e.getMessage());
	}


	public CBlastPlugin getPlugin() {
		return CBlastPlugin.getPlugin();
	}



	public String getDescription() {
		return description;
	}

	public IFile getFileRes() {
		return fileRes;
	}

	public String getProjectName() {
		String result = "";
		try { 
			result = fileRes.getProject().getDescription().getName();
		} catch (Exception e) {
			MessageDialog.openInformation(
				CBlastPlugin.getPlugin().getShell(),
				"Problem with Resource",
			  	"Problem with resource: " + fileRes.getProjectRelativePath().toOSString());
		}
		return result;
	}

	public String getFileRelPath() {
		return fileRes.getProjectRelativePath().toOSString();
	}

	public File getFile() {
		return fileRes.getLocation().toFile();
	}

	public String getMethod() {
		return method;
	}

	public String getOptions() {
		return options;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public void setDescription(String string) {
		description = string;
	}

	public void setFile(String project, String file) {
		fileRes = CBlastPlugin.getWorkspace().getRoot().getProject(project).getFile(file);
	}

	public void setMethod(String string) {
		method = string;
	}

	public void setOptions(String string) {
		options = string;
	}

	public void setName(String string) {
		name = string;
	}

	public void setStatus(String string) {
		status = string;
	}

	public int getDeadNodes() {
		return deadNodes;
	}


	public void setDeadNodes(int i) {
		deadNodes = i;
	}

	public int getLiveNodes() {
		return liveNodes;
	}

	public void setLiveNodes(int i) {
		liveNodes = i;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String string) {
		label = string;
	}

	public String getSpecFile() {
		return specFile;
	}

	public void setSpecFile(String string) {
		specFile = string;
	}

}
