
package org.quality.blast.views;
import org.quality.blast.CBlastPlugin;

import org.eclipse.ui.*;import org.eclipse.core.resources.IFile;import org.eclipse.jface.action.IAction;import org.eclipse.jface.viewers.*;
public class ViewActionDelegate implements IViewActionDelegate {    public IViewPart view;    private IFile mFile;    public ViewActionDelegate() {    }    public void init(IViewPart view) {        this.view = view;    }
    public void run(IAction action) {
		VerTask lVerTask =
			new VerTask(
				mFile.getName() + "_main",
				"auto-added task",
				mFile,
				"main",
				"unchecked");
		CBlastPlugin.getPlugin().add(lVerTask);
	}    public void selectionChanged(IAction act, ISelection sel) {
	 	Object lItem = ((IStructuredSelection)sel).getFirstElement();
    	if (lItem instanceof IFile){        	mFile = (IFile) lItem;		}
    }}