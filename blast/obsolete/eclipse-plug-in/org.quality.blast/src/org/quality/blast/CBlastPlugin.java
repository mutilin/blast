package org.quality.blast;

import org.quality.blast.views.*;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.core.runtime.*;
import org.eclipse.core.resources.*;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Shell;
import java.util.*;
import java.io.*;

/**
 * The main plugin class to be used in the desktop.
 */
public class CBlastPlugin extends AbstractUIPlugin {
	//The shared instance.
	private static CBlastPlugin plugin;
	//Resource bundle.
	private ResourceBundle resourceBundle;
	
	private Viewer mViewer;

	private List mVerTaskList;
	
	public CBlastPlugin(IPluginDescriptor descriptor) {
		super(descriptor);
		plugin = this;
		mViewer = null;
		readListFromFile();
		addListeners();
		try {
			resourceBundle= ResourceBundle.getBundle("test.testPluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	private void addListeners() {
		
	  IResourceChangeListener lFileChangedListener = new IResourceChangeListener() {
		  public void resourceChanged(IResourceChangeEvent event) {

		    IResourceDeltaVisitor lStatusChanger = new IResourceDeltaVisitor() {
		        public boolean visit(IResourceDelta delta) {
			      IResource res = delta.getResource();
				  if (delta.getKind() == IResourceDelta.CHANGED) {
				    for (Iterator iter = mVerTaskList.iterator(); iter.hasNext();) {
					  VerTask task = (VerTask) iter.next();
					  if (task.getFileRes().equals(res)) {
					    task.setStatus("changed");
					  }
					}
					getPlugin().updateViews();
				  }
				  return true; // visit the children
			    }
			}; // Visitor.

		    if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
			  try {
			    event.getDelta().accept(lStatusChanger);
			  } catch (Exception e) {
			    System.out.println(e.getMessage());
			  }
		    }
		  }
	  }; // ChangeListener.
	  
      getWorkspace().addResourceChangeListener(lFileChangedListener);
	}

	public void setViewer(Viewer pViewer) {
		mViewer = pViewer;
	}
	
	public Shell getShell() {
		return mViewer.getControl().getShell();
	}
	
	public void updateViews() {
		writeListToFile();
		if (mViewer != null) {
			mViewer.refresh();
		}
	}

	public void add(VerTask pTask){
		mVerTaskList.add(pTask);
		updateViews();
	}

	public void removeAll(List pList){
		mVerTaskList.removeAll(pList);
		updateViews();
	}

	public Object[] getVerTaskList() {
		return mVerTaskList.toArray();
	}

	// Reads mVerTaskList from file.
	private void readListFromFile(){
		mVerTaskList = new LinkedList();
		if (getStateFile().exists()) {
			try {
				BufferedReader lIn = new BufferedReader( new FileReader(getStateFile()) );
				String lTmpString;
				while ((lTmpString = lIn.readLine()) != null) {
					String[] lStrTask = lTmpString.split(",");
					
					VerTask task =	new VerTask(
											lStrTask[0],
											lStrTask[1],
					CBlastPlugin.getWorkspace().getRoot().getProject(lStrTask[2]).getFile(lStrTask[3]),
											lStrTask[4],
											lStrTask[6]);
					task.setOptions  (lStrTask[5]);
					task.setLabel    (lStrTask[7]);
					task.setDeadNodes(Integer.parseInt(lStrTask[8]));
					task.setLiveNodes(Integer.parseInt(lStrTask[9]));
					task.setSpecFile (lStrTask[10]);
					mVerTaskList.add(task);
				}
				lIn.close();
			}
			catch (Exception e)	{
				System.out.println(e.getMessage());
			}
		}
	}

	// Writes mVerTaskList to file.
	private void writeListToFile(){
		try {
			PrintWriter lOut = new PrintWriter(new FileOutputStream(getStateFile()));

			Object[] lTaskArray = mVerTaskList.toArray();
			for (int i = 0; i < lTaskArray.length; ++i) {
				lOut.println( ((VerTask)lTaskArray[i]).mkString() );
			}
			lOut.close();
		}
		catch (Exception e)	{
			System.out.println("Blast plugin: Cannot write config file: "
								+ getStateFile().getPath());
		}
	}

	public static CBlastPlugin getPlugin() {
		return plugin;
	}

	private File getStateFile() {
		return CBlastPlugin.getPlugin().getStateLocation().append("state.txt").toFile();
	}

	public IPath getInstallDir() {
		IPath lInstallDir = getWorkspace().getRoot().getLocation().removeLastSegments(1)
							  .append("/plugins/org.quality.blast");
		return lInstallDir;
		// The following is better, but doesn't work.
		//return CBlastPlugin.getPlugin().getDescriptor().getInstallURL().getPath();
	}

	public String extBlastName(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".blast";
	}

	public String extLogName(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".log";
	}

	public String extAbsName(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".abs";
	}

	public String extTraceName(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".trc";
	}

	public String extTestName(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".cfa";
	}

	public String extTestNameList(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".cfa.txt";
	}

	public String extTestPS(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".cfa.eps";
	}

	public String extTestVectorList(VerTask task){
		return task.getFile().getPath() + "_" + task.getName() + ".cfa-tests";
	}

	/**
	 * Returns the workspace instance.
	 */
	public static IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle= CBlastPlugin.getPlugin().getResourceBundle();
		try {
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}
}
