/*
 * Created on Nov 21, 2003
 *
 */
package org.quality.blast.views;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.SWT;

/**
 * @author beyer
 *
 */
public class VerTaskPropertiesDialog extends Dialog {

	private VerTask task;

	private Text   mName;
	private Combo  mDescription;
	private Text   mProject;
	private Text   mFile;
	private Text   mMethod;
	private Text   mOptions;
	private Combo  mStatus;

	private Text   mLabel;
	private ProgressBar bar;
	private Text   mSpecFile;


	/**
	 * @param parentShell
	 */
	public VerTaskPropertiesDialog(Shell parentShell, VerTask pVerTask) {
		super(parentShell);
		task = pVerTask;
	}


	// Method declared on Window.
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Verification Task Properties");
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		initializeDialogUnits(composite);

		createArea(composite);

		mName       .setText(task.getName());
		mDescription.setText(task.getDescription());
		mProject    .setText(task.getProjectName());
		mFile       .setText(task.getFileRelPath());
		mMethod     .setText(task.getMethod());
		mOptions    .setText(task.getOptions());
		mStatus     .setText(task.getStatus());
	
		return composite;
	}

	private void createArea(Composite parent) {
		// Standard widgets.
		Font font = parent.getFont();
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		Label label1 = new Label(composite, SWT.NONE);
		label1.setText("Name");
		label1.setFont(font);
		int style = SWT.SINGLE | SWT.BORDER;
		mName = new Text(composite, style);
		GridData gridData1 = new GridData(GridData.FILL_HORIZONTAL);
		gridData1.widthHint = convertHorizontalDLUsToPixels(400);
		mName.setLayoutData(gridData1);
		mName.setFont(font);

		Label label3 = new Label(composite, SWT.NONE);
		label3.setText("Type");
		label3.setFont(font);
		mDescription = new Combo(composite, SWT.READ_ONLY);
		mDescription.setItems(new String[] {
			VerTask.assert,
			VerTask.reach,
			VerTask.deadcode,
			VerTask.test,
			VerTask.spec
		});
		mDescription.setFont(font);

		Label label4a = new Label(composite, SWT.NONE);
		label4a.setText("Project");
		label4a.setFont(font);
		mProject = new Text(composite, style);
		GridData gridData4a = new GridData(GridData.FILL_HORIZONTAL);
		gridData4a.widthHint = convertHorizontalDLUsToPixels(400);
		mProject.setLayoutData(gridData4a);
		mProject.setFont(font);

		Label label4 = new Label(composite, SWT.NONE);
		label4.setText("File");
		label4.setFont(font);
		mFile = new Text(composite, style);
		GridData gridData4 = new GridData(GridData.FILL_HORIZONTAL);
		gridData4.widthHint = convertHorizontalDLUsToPixels(400);
		mFile.setLayoutData(gridData4);
		mFile.setFont(font);

		Label label5 = new Label(composite, SWT.NONE);
		label5.setText("Function");
		label5.setFont(font);
		mMethod = new Text(composite, style);
		GridData gridData5 = new GridData(GridData.FILL_HORIZONTAL);
		gridData5.widthHint = convertHorizontalDLUsToPixels(400);
		mMethod.setLayoutData(gridData5);
		mMethod.setFont(font);
		
		Label label6 = new Label(composite, SWT.NONE);
		label6.setText("Options");
		label6.setFont(font);
		mOptions = new Text(composite, style);
		GridData gridData6 = new GridData(GridData.FILL_HORIZONTAL);
		gridData6.widthHint = convertHorizontalDLUsToPixels(400);
		mOptions.setLayoutData(gridData6);
		mOptions.setFont(font);
		
		Label label7 = new Label(composite, SWT.NONE);
		label7.setText("Status");
		label7.setFont(font);
		mStatus = new Combo(composite, SWT.READ_ONLY);
		mStatus.setItems(new String[] {
			"safe",
			"unsafe",
			"checked",
			"unchecked"
		});
		mStatus.setFont(font);


		// Additional widgets for particular verification tasks.
		// Reach.
		if (task.getDescription().equals(VerTask.reach)) {
			Label label8 = new Label(composite, SWT.NONE);
			label8.setText("Label name");
			label8.setFont(font);
			mLabel = new Text(composite, style);
			GridData gridData8 = new GridData(GridData.FILL_HORIZONTAL);
			gridData8.widthHint = convertHorizontalDLUsToPixels(400);
			mLabel.setLayoutData(gridData8);
			mLabel.setFont(font);
			mLabel.setText( task.getLabel() );
		}	
		
		// Deadcode.
		if (task.getDescription().equals(VerTask.deadcode) ||
		task.getDescription().equals(VerTask.test)) {
			Label label8 = new Label(composite, SWT.NONE);
			label8.setText("Analysis results");
			label8.setFont(font);
			Text localText1 = new Text(composite, style);
			GridData gridData8 = new GridData(GridData.FILL_HORIZONTAL);
			gridData8.widthHint = convertHorizontalDLUsToPixels(400);
			localText1.setLayoutData(gridData8);
			localText1.setFont(font);
			localText1.setText( "Dead nodes: "   + task.getDeadNodes() + 
								", Live nodes: " + task.getLiveNodes() +
								"  ");
			localText1.setEditable(false);

			Label label9 = new Label(composite, SWT.NONE);
			bar = new ProgressBar(composite, SWT.HORIZONTAL);
			bar.setMinimum(0);
			bar.setMaximum(task.getDeadNodes() + task.getLiveNodes());
			bar.setSelection(task.getDeadNodes());
			bar.setFont(font);
			
		}

		// Spec.
		if (task.getDescription().equals(VerTask.spec)) {
			Label label8 = new Label(composite, SWT.NONE);
			label8.setText("Specification file name");
			label8.setFont(font);
			mSpecFile = new Text(composite, style);
			GridData gridData8 = new GridData(GridData.FILL_HORIZONTAL);
			gridData8.widthHint = convertHorizontalDLUsToPixels(400);
			mSpecFile.setLayoutData(gridData8);
			mSpecFile.setFont(font);
			mSpecFile.setText( task.getSpecFile() );
		}	
	}
	
	protected void okPressed() {
		task.setName(mName.getText());
		task.setDescription(mDescription.getText());
		task.setFile(mProject.getText(), mFile.getText());
		task.setMethod(mMethod.getText());
		task.setOptions(mOptions.getText());
		task.setStatus(mStatus.getText());

		if (mLabel != null) {
			task.setLabel(mLabel.getText());
		}
		if (mSpecFile != null) {
			task.setSpecFile(mSpecFile.getText());
		}
		
		super.okPressed();
	}
}
