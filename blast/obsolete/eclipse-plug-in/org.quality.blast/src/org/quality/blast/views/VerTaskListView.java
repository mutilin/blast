
package org.quality.blast.views;

import org.quality.blast.CBlastPlugin;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.resources.*;
import org.eclipse.jface.action.*;
import org.eclipse.jface.util.Assert;
import org.eclipse.jface.viewers.*;
import org.eclipse.jface.dialogs.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.*;
import org.eclipse.ui.part.*;

import java.io.*;
import java.util.*;

/**
 * Main class for the Task List view for software verification using Blast.
 */
public class VerTaskListView extends ViewPart {

	private Table table;
	private TableEditor tableEditor;
	private MenuManager contextMenu;

	private TableViewer viewer;
	private IMemento memento;

	private Composite parent;
	private StackLayout stackLayout = new StackLayout();

	private Action runTaskAction;
	private Action removeTaskAction;
	private Action selectAllAction;
	private Action filtersAction;
	private Action propertiesAction;
	private Action showErrorTraceAction;
	private Action showLogFileAction;
	private Action showDeadNodesActionList;
	private Action showDeadNodesActionGraphic;
	private Action showTestVectorList;
	private Action clearResultsAction;

	private static String[] tableColumnProperties =
		{
			IBasicPropertyConstants.P_IMAGE,
			IMarker.DONE,
			IMarker.PRIORITY,
			IMarker.MESSAGE };

	// Persistance tags.
	private static final String TAG_COLUMN = "column";
	private static final String TAG_NUMBER = "number";
	private static final String TAG_WIDTH = "width";
	private static final String TAG_FILTER = "filter";
	private static final String TAG_SELECTION = "selection";
	private static final String TAG_ID = "id";
	private static final String TAG_MARKER = "marker";
	private static final String TAG_RESOURCE = "resource";
	private static final String TAG_TOP_INDEX = "topIndex";
	private static final String TAG_SORT_SECTION = "TaskListSortState";

	private String columnHeaders[] =
		{
			"Name",
			"Type",
			"Project",
			"File",
			"Function",
			"Status" };

	private ColumnLayoutData columnLayouts[] =
		{
			new ColumnPixelData(150),
			new ColumnPixelData(100),
			new ColumnPixelData(100),
			new ColumnPixelData(150),
			new ColumnPixelData(100),
			new ColumnPixelData(100) };

	private IPartListener partListener = new IPartListener() {
		public void partActivated(IWorkbenchPart part) {
			VerTaskListView.this.partActivated(part);
			getPlugin().updateViews();
		}
		public void partBroughtToTop(IWorkbenchPart part) {
		}
		public void partClosed(IWorkbenchPart part) {
			VerTaskListView.this.partClosed(part);
		}
		public void partDeactivated(IWorkbenchPart part) {
		}
		public void partOpened(IWorkbenchPart part) {
		}
	};
	private ISelectionChangedListener focusSelectionChangedListener =
		new ISelectionChangedListener() {
		public void selectionChanged(SelectionChangedEvent event) {
			VerTaskListView.this.focusSelectionChanged(event);
		}
	};
	private IResource[] focusResources;
	private IWorkbenchPart focusPart;
	private ISelectionProvider focusSelectionProvider;

	/**
	 * Creates a new task list view.
	 */
	public VerTaskListView() {
		super();
	}
	
	void createColumns() {
		/**
		 * This class handles selections of the column headers.
		 * Selection of the column header will cause resorting
		 * of the shown tasks using that column's sorter.
		 * Repeated selection of the header will toggle
		 * sorting order (ascending versus descending).
		 */
		SelectionListener headerListener = new SelectionAdapter() {
			/**
			 * Handles the case of user selecting the
			 * header area.
			 * <p>If the column has not been selected previously,
			 * it will set the sorter of that column to be
			 * the current tasklist sorter. Repeated
			 * presses on the same column header will
			 * toggle sorting order (ascending/descending).
			 */
			public void widgetSelected(SelectionEvent e) {
				// column selected - need to sort
				int column = table.indexOf((TableColumn) e.widget);
				viewer.refresh();
				IDialogSettings workbenchSettings = getPlugin().getDialogSettings();
				IDialogSettings settings = workbenchSettings.getSection(TAG_SORT_SECTION);
				if (settings == null)
					settings = workbenchSettings.addNewSection(TAG_SORT_SECTION);
			}
		};

		if (memento != null) {
			//restore columns width
			IMemento children[] = memento.getChildren(TAG_COLUMN);
			if (children != null) {
				for (int i = 0; i < children.length; i++) {
					Integer val = children[i].getInteger(TAG_NUMBER);
					if (val != null) {
						int index = val.intValue();
						val = children[i].getInteger(TAG_WIDTH);
						if (val != null) {
							columnLayouts[index] =
								new ColumnPixelData(val.intValue(), true);
						}
					}
				}
			}
		}

		TableLayout layout = new TableLayout();
		table.setLayout(layout);
		table.setHeaderVisible(true);

		for (int i = 0; i < columnHeaders.length; i++) {
			TableColumn tc = new TableColumn(table, SWT.NONE, i);

			tc.setText(columnHeaders[i]);
			tc.setResizable(columnLayouts[i].resizable);
			layout.addColumnData(columnLayouts[i]);
			tc.addSelectionListener(headerListener);
		}
	}
	
	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			return getPlugin().getVerTaskList();
		}
	}

	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			VerTask lVerTask = (VerTask) obj;
			switch(index){
				case 0: return lVerTask.getName();
				case 1: return lVerTask.getDescription();
				case 2: return lVerTask.getProjectName();
				case 3: return lVerTask.getFileRelPath();
				case 4: return lVerTask.getMethod();
				case 5: return lVerTask.getStatus();
			}
			return "undef";
		}
		
		public Image getColumnImage(Object obj, int index) {
			VerTask lVerTask = (VerTask) obj;
			ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();
			ImageDescriptor desc;
			Image result = null;
			if (index == 5) {
				desc = sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_WARN_TSK);
				if(lVerTask.getStatus().equals("safe")){
					desc = sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK);
				}
				if(lVerTask.getStatus().equals("unsafe")){
					desc = sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_ERROR_TSK);
				}
				result = desc.createImage(true);
			}
			return result;
		}
	}

	class NameSorter extends ViewerSorter {
	}

	public void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"Verification View",
			message);
		System.out.println(message);
	}

	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void dispose() {
		super.dispose();
		getSite().getPage().removePartListener(partListener);
	}

	public void createPartControl(Composite parent) {
		this.parent = parent;
		table = new Table(parent,
						  SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION);
		table.setLinesVisible(true);

		viewer = new TableViewer(table);
		viewer.setUseHashlookup(true);

		createColumns();
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(ResourcesPlugin.getWorkspace());
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				VerTaskListView.this.selectionChanged(event);
			}
		});
		getPlugin().setViewer(viewer);


		makeActions();

		hookDoubleClickAction();
		hookContextMenu();
		contributeToActionBars();


		// Track selection in the page.
		getSite().getPage().addPartListener(partListener);

		getSite().setSelectionProvider(viewer);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				propertiesAction.run();
			}
		});
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	void fillContextMenu(IMenuManager manager) {
		manager.add(runTaskAction);
		manager.add(new Separator());
		manager.add(showErrorTraceAction);
		manager.add(showLogFileAction);
		manager.add(showDeadNodesActionList);
		manager.add(showDeadNodesActionGraphic);
		manager.add(showTestVectorList);
		manager.add(clearResultsAction);
		manager.add(new Separator());
		manager.add(removeTaskAction);
		manager.add(new Separator());
		manager.add(selectAllAction);
		manager.add(new Separator());

		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS + "-end"));

		manager.add(propertiesAction);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(new Separator());
		manager.add(runTaskAction);
		manager.add(new Separator());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(runTaskAction);
	}

	void makeActions() {

		// Define actions.
		// run task
		final VerTaskListView tasklist = this;
		runTaskAction = new Action() {
			public void run() {
				for (Iterator iter = getSelection().iterator(); iter.hasNext();) {
					Object o = iter.next();
					if (o instanceof VerTask) {
						VerTask lTask = (VerTask) o;
						lTask.run(tasklist);
					}
				}
			}
		};

		// clear results
		clearResultsAction = new Action() {
			public void run() {
				for (Iterator iter = getSelection().iterator(); iter.hasNext();) {
					Object o = iter.next();
					if (o instanceof VerTask) {
						VerTask task = (VerTask) o;
			
						task.setStatus("unchecked");
						getPlugin().updateViews();
			
						new File(getPlugin().extBlastName(task)).delete();
						new File(getPlugin().extLogName  (task)).delete();
						new File(getPlugin().extAbsName  (task)).delete();
						new File(getPlugin().extTraceName(task)).delete();
						new File(getPlugin().extTestName (task)).delete();
						new File(getPlugin().extTestNameList  (task)).delete();
						new File(getPlugin().extTestPS        (task)).delete();
						new File(getPlugin().extTestVectorList(task)).delete();
						if (task.getDescription().equals(VerTask.deadcode) ||
							task.getDescription().equals(VerTask.test)) {
								try { task.getFileRes().deleteMarkers(null, false, 1); } catch (Exception e){
									tasklist.showMessage(e.getMessage());
								}
							}
					}
				}
			}
		};

		// show error trace
		showErrorTraceAction = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTask task = (VerTask) o;
					File file = new File(getPlugin().extTraceName(task));
					showFile("Error trace for task ", file, task);
				}
			}
		};

		// show log
		showLogFileAction = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTask task = (VerTask) o;
					File file = new File(getPlugin().extLogName(task));
					showFile("Log file for task ", file, task);
				}
			}
		};

		// show test vector list
		showTestVectorList = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTask task = (VerTask) o;
					File file = new File(getPlugin().extTestVectorList(task));
					showFile("Test vectors for task ", file, task);
				}
			}
		};

		// show dead nodes list
		showDeadNodesActionList = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTask task = (VerTask) o;
					File file = new File(getPlugin().extTestNameList(task));
					showFile("Dead nodes for task ", file, task);
				}
			}
		};

		// show dead nodes graphic
		showDeadNodesActionGraphic = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTask task = (VerTask) o;

					// Some settings.
					IPath installDir = getPlugin().getInstallDir();
						             
					// Create Postscript file.
					{
						String env[] = { "PATH=" + installDir.append("/dot").toOSString(),
										 "TEMP=" + installDir.toOSString() };
						String command = "cmd /C dot -Tps " + getPlugin().extTestName(task)
										+ " -o " + getPlugin().extTestPS(task);
						System.out.println(command);
						try {
							Runtime lRT  = Runtime.getRuntime();
							Process lP   = lRT.exec(command, env, task.getFile().getParentFile());
							lP.waitFor();
						} catch (Exception e) {
							MessageDialog.openInformation(
								getSite().getShell(),
								"Create Postscript File With DOT - Java Exception",
								e.getMessage());
						}
					}

                    // Launch Postscript viewer.
					{
						String env[] = { "PATH=" + installDir.toOSString(),
										  "TEMP=" + installDir.toOSString() };
						String command = "cmd /C start " + getPlugin().extTestName(task) + ".eps";
						System.out.println(command);
						try {
							Runtime lRT  = Runtime.getRuntime();
							Process lP   = lRT.exec(command, env, task.getFile().getParentFile());
							lP.waitFor();
						} catch (Exception e) {
							MessageDialog.openInformation(
								getSite().getShell(),
								"Display of control flow automata - Java Exception",
								e.getMessage());
						}
					}
				}
			}
		};
		
		// remove task
		removeTaskAction = new Action() {
			public void run() {
				getPlugin().removeAll(getSelection().toList());
			}
		};

		// select all
		selectAllAction = new Action() {
			public void run() {
				viewer.refresh();
				viewer.getTable().selectAll();
				viewer.setSelection(viewer.getSelection());
			}
		};

		// properties
		propertiesAction = new Action() {
			public void run() {
				Object o = getSelection().getFirstElement();
				if (o instanceof VerTask) {
					VerTaskPropertiesDialog dialog = 
						new VerTaskPropertiesDialog(getSite().getShell(), (VerTask) o);
					if (dialog.open() == Dialog.OK) {
						getPlugin().updateViews();
					}
				}
			}
		};


		// Initialize fields.
		ISharedImages sharedImages = PlatformUI.getWorkbench().getSharedImages();

		runTaskAction.setText("Run");
		runTaskAction.setToolTipText("Launching Blast Verification for Selection");
		runTaskAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		showErrorTraceAction.setText("Show Error Trace");
		showErrorTraceAction.setToolTipText("Shows a cerificate of error");
		showErrorTraceAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		showLogFileAction.setText("Show Log File");
		showLogFileAction.setToolTipText("Shows the last log file");
		showLogFileAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		showDeadNodesActionList.setText("Show Dead nodes (list)");
		showDeadNodesActionList.setToolTipText("Shows the list of dead nodes");
		showDeadNodesActionList.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		showDeadNodesActionGraphic.setText("Show Dead nodes (graphic)");
		showDeadNodesActionGraphic.setToolTipText("Shows control flow graph with dead nodes marked");
		showDeadNodesActionGraphic.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		showTestVectorList.setText("Show Test Vector List");
		showTestVectorList.setToolTipText("Shows list of generated test vectors");
		showTestVectorList.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		clearResultsAction.setText("Erase results");
		clearResultsAction.setToolTipText("Clear results for task");
		clearResultsAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK));

		removeTaskAction.setText("Delete");
		removeTaskAction.setToolTipText("Remove Task");
		removeTaskAction.setHoverImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE_HOVER));
		removeTaskAction.setImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE));
		removeTaskAction.setDisabledImageDescriptor(sharedImages.getImageDescriptor(ISharedImages.IMG_TOOL_DELETE_DISABLED));

		selectAllAction.setText("Select All");
		selectAllAction.setToolTipText("Select All Tasks");

		propertiesAction.setText("Properties");
		propertiesAction.setToolTipText("View and Change Task Properties");

		// Show only, if one task is selected.
		showErrorTraceAction.setEnabled(false);
		showLogFileAction.setEnabled(false);
		showDeadNodesActionList.setEnabled(false);
		showDeadNodesActionGraphic.setEnabled(false);
		showTestVectorList.setEnabled(false);
		propertiesAction.setEnabled(false);
	}

	private void showFile(String prefix, File file, VerTask task) {
		if(file.exists() == true){
			MessageDialog.openInformation(
				getSite().getShell(),
				prefix + task.getName(),
				fileToString(file));
		}
		else{
			MessageDialog.openInformation(
				getSite().getShell(),
				prefix + task.getName(),
				"No content available.");
		}
	}


	void selectionChanged(SelectionChangedEvent event) {
		IStructuredSelection selection =
			(IStructuredSelection) event.getSelection();

		VerTask task = null;
		Object o = selection.getFirstElement();
		if (o instanceof VerTask) {
			task = (VerTask) o;
		}

		boolean stdCond = (selection.size() == 1) && (task != null);

		showErrorTraceAction.setEnabled( stdCond  
		            && new File(getPlugin().extTraceName(task)).exists());
		
		showLogFileAction.setEnabled   ( stdCond 
		            && new File(getPlugin().extLogName(task)).exists());

		showDeadNodesActionList.setEnabled ( stdCond  
		            && new File(getPlugin().extTestNameList(task)).exists());

		showDeadNodesActionGraphic.setEnabled ( stdCond  
		            && new File(getPlugin().extTestName(task)).exists());

		showTestVectorList.setEnabled ( stdCond  
					&& new File(getPlugin().extTestVectorList(task)).exists());

		propertiesAction.setEnabled    ( stdCond );
	}

	/**
	 * The filter settings have changed.
	 * Refreshes the viewer and title bar.
	 */
	void filterChanged() {

		BusyIndicator
			.showWhile(
				viewer.getControl().getShell().getDisplay(),
				new Runnable() {
			public void run() {
				// Filter has already been updated by dialog; just refresh.
				// Don't need to update labels for existing elements 
				// since changes to filter settings don't affect them.
				viewer.getControl().setRedraw(false);
				viewer.refresh(false);
				viewer.getControl().setRedraw(true);
				// update after refresh since the content provider caches summary info
			}
		});

	}
	void focusSelectionChanged(SelectionChangedEvent event) {
		updateFocusResource(event.getSelection());
	}

	public CBlastPlugin getPlugin() {
		return CBlastPlugin.getPlugin();
	}

	public IStructuredSelection getSelection() {
		IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
		return sel;
	}

	public TableViewer getTableViewer() {
		return viewer;
	}

	IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	void handleKeyPressed(KeyEvent event) {
		if (event.character == SWT.DEL
			//&& event.stateMask == 0
			&& removeTaskAction.isEnabled())
			removeTaskAction.run();
	}

	public void init(IViewSite site, IMemento memento)
		throws PartInitException {
		super.init(site, memento);
		this.memento = memento;
	}

	void partActivated(IWorkbenchPart part) {
		if (part == focusPart)
			return;

		if (focusSelectionProvider != null) {
			focusSelectionProvider.removeSelectionChangedListener(
				focusSelectionChangedListener);
			focusSelectionProvider = null;
		}

		focusPart = part;
		if (focusPart != null) {
			focusSelectionProvider = focusPart.getSite().getSelectionProvider();
			if (focusSelectionProvider != null) {
				focusSelectionProvider.addSelectionChangedListener(
					focusSelectionChangedListener);
				updateFocusResource(focusSelectionProvider.getSelection());
			} else {
				updateFocusResource(null);
			}
		}

	}
	void partClosed(IWorkbenchPart part) {
		if (part != focusPart)
			return;
		if (focusSelectionProvider != null) {
			focusSelectionProvider.removeSelectionChangedListener(
				focusSelectionChangedListener);
			focusSelectionProvider = null;
		}
		focusPart = null;
	}

	void restoreState(IMemento memento) {
	}

	public void saveState(IMemento memento) {
	}

	/**
	 * API method which sets the current selection of this viewer.
	 *
	 * @param selection a structured selection of <code>IMarker</code> objects
	 * @param reveal <code>true</code> to reveal the selection, <false> otherwise
	 */
	public void setSelection(ISelection selection, boolean reveal) {
		Assert.isTrue(selection instanceof IStructuredSelection);
		IStructuredSelection ssel = (IStructuredSelection) selection;
		
		if (viewer != null)
			viewer.setSelection(selection, reveal);
	}

	void toggleInputSelection(boolean value) {
	}

	void updateFocusResource(ISelection selection) {
	}

	public String fileToString(File pFile){
		String lResult = "";
		try {
			lResult = fileToString(new FileInputStream(pFile), false);
		}
		catch (Exception e)	{
			showMessage(e.getMessage());
		}
		return lResult;
	}
	
	public String fileToString(InputStream pFileStream, boolean pStdOut){
		String lResult = "";
		try {
			BufferedReader lIn = new BufferedReader(
									new InputStreamReader(pFileStream));
			String lTmpString;
			while ((lTmpString = lIn.readLine()) != null) {
				lResult += lTmpString + System.getProperty("line.separator");
				if (pStdOut) {
					System.out.println(lTmpString);
				}
			}
			lIn.close();
		}
		catch (Exception e)	{
			showMessage(e.getMessage());
		}
		return lResult;
	}

	public void stringToFile(String inString, File outFile) {
		try {
			PrintWriter out = new PrintWriter(new FileOutputStream(outFile));
			out.println(inString);
			out.close();
		}
		catch (Exception e)	{
			showMessage(e.getMessage());
		}		
	}
}
