#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int division(int dividend, int divisor) {
    int remainder = dividend;
    int quotient = 0;
    assert(divisor > 0);
    while (divisor <= remainder) {
        remainder -= divisor;
        ++quotient;
    }
    return quotient;
}
int main(int argc, char *argv []) {
    int dividend, divisor;
    if (argc < 3) { exit(EXIT_FAILURE); }
    dividend = atoi(argv[1]);
    divisor = atoi(argv[2]);
    L1: if (divisor <= 0) return 2;
    printf("%d", division( dividend, divisor ));  
    return 0;
}
