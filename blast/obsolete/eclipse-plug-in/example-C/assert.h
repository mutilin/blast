
/* THE ERROR LABEL */
void __blast_assert() {
  ERROR: goto ERROR;
}

# define assert(expr) if(! (expr)) __blast_assert()

