#include "lock.h"

global int locked = 0;

event {
	pattern { init(); }
	action { locked = 0; }
}

event {
	pattern { lock(); }
	guard { locked == 0 }
	action { locked = 1; }
}

event {
	pattern { unlock(); }
	guard { locked == 1 }
	action { locked = 0; }
}
