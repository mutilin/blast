
#include<stdio.h>
#include<assert.h>

/*---------------------------------------------------------------------------*/
/*   
		  Function         : search
		  Parameters       : none
		  Input            : 8 sorted integer array elements, element e to search for
		  Output           : index of element e , if found in array,
				             or index of element preceeding where e 
				             should be placed
		  Description      : search array for element e , return index 
			                 to e or index immediately preceeding where 
                             e should be placed. Uses binary search
*/
/*---------------------------------------------------------------------------*/

int search(int* a, int e) {
	int i = 0;
	int j = 7;
	int k;

    /*
	// Check assumption that a is sorted.
	for(k=i+1; k<=j; ++k) {
		if (a[k-1] > a[k]) {
			printf("Array not sorted (%d, %d).\n", a[k-1], a[k]);
			exit(2);
		}
	}
    */

	if(e < a[0]) {
		return -1;
	}

	while(i < j)
	{
		k = (i + j + 1)/2;
		if(e < a[k]) {
			j = k-1;
		} else {
			i = k;
		}
        assert(a[i]<=e);
        assert(e<=a[j]);
	}
	return i;
}

main()
{
	int a[8];
	int e;
	int i = 0;

	printf("Enter 8 array values\n");
	while(i < 8) {
		scanf("%d", &a[i]);
		++i;
	}

	printf("Enter element\n");
	scanf("%d", &e);

	printf("Position %d\n", search(a, e));
}
