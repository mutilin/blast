#include <stdlib.h>
#include <stdio.h>
 
int readInt(void);
//{
//  int result;
//  scanf("%d", &result);
//  return result;
//}

int middle(int x, int y, int z) {
   int m = z;    
   if(y < z)
   {
     if(x < y)
       m = y;
     else if(x < z)
       m = x; 
   }
   else 
     if(x > y)
       m = y;
     else if(x > z)
       m = x; 
   return m;
}

int main() {
  int x, y, z;  
  printf("Enter the 3 numbers: ");
  x = readInt();
  y = readInt();
  z = readInt();
  printf("Middle number: %d", middle(x,y,z));
}
