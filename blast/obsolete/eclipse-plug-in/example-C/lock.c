#include "lock.h"

int f(int x)
{
  if (x == 14)
    return 1;
  else
    return 0;
}

main()
{
  init();
  lock();
  if (f(14))
    lock();
  unlock();
}
