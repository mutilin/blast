
#include <assert.h>

int main () {
  int a[5];
  int i = 0;
  while (a[i]!=0) {
    i = i + 1;
  }
  /* invariant: a[i] == 0 and \forall 0\leq j < i. a[j] != 0 */
  i = i - 1;
  /* invariant: \forall 0\leq j \leq i. a[j] != 0 */
  if (i>=0) {
    assert (a[i] != 0);
  }
}

/*
int main () {
  int a[*];
  i = 0;
  while (a[i]!=0) {
    i = i + 1;
  }
  // invariant: a[i] == 0 and \forall 0\leq j < i. a[j] != 0
  i = i - 1;
  // invariant: \forall 0\leq j \leq i. a[j] != 0
  while (i>=0) {
    assert (a[i] != 0);
    i = i - 1;
  }
  return 0;
}
*/
