(* testerBatchMain.ml
 * Process command line args and run tests for batch test runner. This should be the
 * last module on the link line. testerBatchMode must be linked ahead of the tests - it
 * sets Tester.interactive_mode to false. In other words, the link sequence should be :
 *  tester.cmo testerBatchMode.cmo <libs needed by tests> <tests> testerBatchMain.cmo
 *)
open Tester

let main () =
  let test_names = ref []
  and level = ref LogProgress
  and log_file = ref ""
  and is_list_tests_set = ref false
  in let log_level_fn = fun level_str ->
       match level_str with
	   "LogError" -> level := LogError
	 | "LogResults" -> level := LogResults
	 | "LogProgress" -> level := LogProgress
	 | "LogDebug" -> level := LogDebug
	 | "LogDetailedDebug" -> level := LogDetailedDebug
	 | "all" -> level := LogDetailedDebug
	 | _ -> raise(Arg.Bad("invalid log level " ^ level_str))
     and log_file_fn = fun arg -> log_file := arg
     and list_tests_fn = fun () -> is_list_tests_set := true
  in let spec =
      [("-loglevel", (Arg.String log_level_fn),
	"Amount of logging (LogError, LogResults, LogProgress, LogDebug, or all)");
       ("-logfile", (Arg.String log_file_fn),
	"File to write logging information (default is stdout)");
       ("-listtests", (Arg.Unit list_tests_fn),
	"Print list of tests to stdout and exit")]
  and add_test_fn = fun arg -> test_names := arg :: !test_names
  and usage_msg = "tester {-loglevel level} {-logfile file} {-listtests} <test1> <test2>..."
  in
    Arg.parse spec add_test_fn usage_msg;
    let test_to_run = get_suite_of_registered_tests "test run" !test_names
    in 
      if !is_list_tests_set = true
      then print_test_list stdout test_to_run
      else
	begin (* run the tests *)
	  let result = test_to_run#run (make_root_context stdout !level (test_to_run#get_name ()))
	  in flush stdout;
	    if result.testsFailed>0 or result.testsAborted>0 then exit 1
	    else exit 0
	end

let _ = main ()
