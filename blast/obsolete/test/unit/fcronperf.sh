#/usr/local/bin/bash
export RESDIR=`pwd`
export RUNDIR=../fileopenclose/instrumented
export OPTS="-cref -bddmin -alias bdd -nofp -predH 7 -craig 1 -scope -pred fcron-instrumented.pred fcron-instrumented.c"
cd $RUNDIR
time pblast.opt $OPTS >$RESDIR/out 2>&1
cd $RESDIR
echo $2 >$1.res
echo pblast.opt $OPTS >>$1.res
grep "Lattice data flow analysis" out >>$1.res
grep "Dataflow iterations" out >>$1.res
grep "Number of function summaries used" out >>$1.res
grep "Nb iterations of reachability" out >>$1.res
grep "Lattice dataflow analysis" out >>$1.res
grep "Nb refinment processes" out >>$1.res
echo "Predicates ruled out:" >>$1.res
grep "ruled out" out | wc -l >>$1.res
mv out $1.out

