#define FILE 12

void *malloc(int t);
void errorFn() { ERROR: goto ERROR; }
char *havoc();

typedef struct file_str {
	int is_open;
	char name[20];
} file;

file *get_stderr() {
  file *f = (file *)malloc(sizeof(file));
  f->is_open = 1;
  strcpy(f->name, "stderr");
  return f;
}

file *fopen (char *fname) {
    file *f = (file *)malloc(sizeof(file));
    f->is_open = 1;
    strcpy(f->name, fname);
    return f;
}

void fclose (file *f) {
  if (f->is_open==0) {
    errorFn(); 
  }
  f->is_open = 0;
}

void fprintf(file *f) {
  if (f->is_open==0) {
    errorFn(); 
  }
}

int main() {
  file *f, *out;
  char *outfile;
  while (1) {
    outfile = havoc(); /* force a non-deterministic choice */
    if (outfile!=0) {
      f = fopen(outfile);
      out = f; 
    } else {
      f = get_stderr();
      out = f;
    }
    fprintf(out);
    fprintf(f);
    fclose(f);
  }
}
