module C_SD = BlastCSystemDescr.C_System_Descr
module Command = BlastCSystemDescr.C_Command
module Operation = Absutil.Make_Operation(C_SD) 

module ConstLattice = (Lattice.Make_ConstantLattice (C_SD)) (Operation)

(** Define a module that implements LATTICE' *)
module ConstLattice' : LatticeChecker.LATTICE' =
  struct
    include ConstLattice
    let cast_edge (edge : C_SD.edge) = edge
  end

(** Create a lattice checker for the constant lattice *)
module CLC = LatticeChecker.Make_LatticeChecker (ConstLattice')

open Tester

let initialized = ref false

let initialize () =
  if !initialized = false then
    begin
      CLC.initialize["constTest.c"];
      ConstLattice'.initialize ();
      initialized := true
    end

let error_region = ((0, 3), [], ConstLattice'.top) (* Error line of assert in constTest.c *)

let test1 = new generic_test "const_lattice1" "basic test for const lattice, uses constTest.c:test1()"
	      (fun ctx ->
		 initialize ();
		 Lattice.SymbolTable._reset_symbolic_constants ();
		 CLC.walk_cfa "test1" error_region ctx.logger "const_lattice1.out" 3 1)
	      
let test2 = new generic_test "const_lattice2" "test coverage check, uses constTest.c:bar()"
	      (fun ctx ->
		 initialize ();
		 Lattice.SymbolTable._reset_symbolic_constants ();
		 CLC.walk_cfa "bar" error_region ctx.logger "const_lattice2.out" 2 0)
	      
let test3 = new generic_test "const_lattice3" "test function calls"
	      (fun ctx ->
		 initialize ();
		 Lattice.SymbolTable._reset_symbolic_constants ();
		 CLC.walk_cfa "test3a" error_region ctx.logger "const_lattice3.out" 2 0)
	      
let test4 = new generic_test "const_lattice4" "test function calls"
	      (fun ctx ->
		 initialize ();
		 Lattice.SymbolTable._reset_symbolic_constants ();
		 CLC.walk_cfa "test4" error_region ctx.logger "const_lattice4.out" 3 0)
	      
let test5 = new generic_test "const_lattice5" "Pointer invalidation"
	      (fun ctx ->
		 initialize ();
		 Lattice.SymbolTable._reset_symbolic_constants ();
		 CLC.walk_cfa "test5" error_region ctx.logger "const_lattice5.out" 0 0)
	      
let suite =
  new test_suite "const_lattice_tests" "verify constant lattice" [test1; test2; test3; test4; test5]


let _ = test_script_main suite LogResults

