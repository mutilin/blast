module C_SD = BlastCSystemDescr.C_System_Descr
module Command = BlastCSystemDescr.C_Command
module Operation = Absutil.Make_Operation(C_SD)

(** We need to extend the generic lattice type to expose the post_cmd function *)
module type LATTICE' =
  sig
    include BlastArch.LATTICE

    val post_cmd : lattice -> C_SD.Command.t -> string -> lattice
    val cast_edge : C_SD.edge -> edge
  end

open Tester

let initialized = ref false (** Used to ensure we only initialize once per run *)

let input_filelist = ref [] (** List of input files used to initialize *)

let source_files = Hashtbl.create 10

(** This module contains tests for the constant propagation lattice. It uses an
    example program, constTest.c to drive the tests. The heart of the tests is
    walk_cfa, which simulates the model checking algorithm. For this algorithm,
    regions are just program locations and associated lattices. This algorithm
    exercises the post, leq, meet, and join functions of the lattice.
*)
module Make_LatticeChecker = functor (DataLattice : LATTICE') ->
struct
  type location = C_SD.location
  type pc = int * int (** program counter *)
      (** The region is not correct here - we really should also track the stack! This doesn't
          matter, unless we will reach the same location through two different stack configurations. *) 
  type const_region = pc * (location list) * DataLattice.lattice (** A region is a program location,
							       the current stack, and a lattice *)
  type const_region_set =
      (pc * (location list), DataLattice.lattice) Hashtbl.t (** map between locations and lattices *)
  let is_region_covered region region_set (** return true if region is covered by set *) =
    let (loc, stack, lattice) = region
    in try
	let existing_lattice = (Hashtbl.find region_set (loc, stack))
	in
	  (* Format.fprintf log_formatter "Checking coverage with existing region:@\n";
	     DataLattice.print log_formatter existing_lattice;
	     Format.fprintf log_formatter "@?";*)
	  DataLattice.leq lattice existing_lattice
      with
	  Not_found -> false
  let join_regions (region : const_region)
                   (region_set : const_region_set)
                   log_formatter =
    (** join the specified region into the region set and return the joined lattice for the current location. *)
    let (loc, stack, lattice) = region
    in try
	let existing_lattice = (Hashtbl.find region_set (loc, stack))
	in let joined_lattice = DataLattice.join existing_lattice lattice
	in Hashtbl.replace region_set (loc, stack) joined_lattice;
	   Format.fprintf log_formatter "Location already seen. Using joined lattice:@?";
	   DataLattice.print log_formatter joined_lattice;
	   Format.fprintf log_formatter "@?";
	   joined_lattice
      with
	  Not_found -> Hashtbl.replace region_set (loc, stack) lattice; lattice
  let empty_region = ((-1, -1), [], DataLattice.bottom)
  let intersect_regions region1 region2 =
    (** Return a region representing intersection of regions, ignoring the program stacks *)
    let (region1_loc, region1_stack, region1_lattice) = region1
    and (region2_loc, region2_stack, region2_lattice) = region2
    in
      if region1_loc<>region2_loc then empty_region
      else
	let intersect_lattice = DataLattice.meet region1_lattice region2_lattice
	in (if intersect_lattice = DataLattice.bottom then
	      empty_region else (region1_loc, [], intersect_lattice))

  let print_region fmt region =
    let ((loc1, loc2), stack, lattice) = region
    in
      Format.fprintf fmt "location: (%d, %d)" loc1 loc2;
      if stack <> [] then
	begin
	  Format.fprintf fmt "  stack: [@[";
	  List.iter (fun node ->
		       let (l1, l2) = C_SD.location_coords node
		       in Format.fprintf fmt " (%d, %d)" l1 l2) stack;
	  Format.fprintf fmt " ]@]"
	end;
      Format.fprintf fmt "@?lattice: ";
      DataLattice.print fmt lattice;
      Format.fprintf fmt "@?"

  (** initialize the CFA based on the input file list *)
  let initialize file_list =
    (* The initialization can only happen once due to global state. We check whether it was already initialized.
       If so, we just check that the input file list is consistent and then return *)
    if !initialized then
      begin
	if file_list <> !input_filelist
	then failwith "Make_LatticeChecker.initialize: called with different file lists"
      end
    else
      begin
	(* We set mainsourcename to the filename prefix for the first file. This
	   duplicates the code in Options.buildOptionsDatabase. *)
	let mainsourcename =
	  match Misc.chop (List.hd file_list) "\\." with 
	      [] -> List.hd file_list
	    | head :: reset -> head
	in (Hashtbl.replace Options.optionsDatabase "mainsourcename" (Options.String mainsourcename));
	let rec cil_files = (C_SD.initialize_cil file_list)
	in C_SD.initialize_blast cil_files;
	  Options.setValueOfString "alias" "bdd"; (* force alias analysis to build database *)
	  AliasAnalyzer.constructAliasDatabase ();
	  (* build up a table of source lines *)
	  List.iter
	    (fun filename ->
	       let in_chan = open_in filename
	       and lineno = ref 0
	       and tbl = Hashtbl.create 100 in
		 try
		   while true do
		     Hashtbl.add tbl !lineno (input_line in_chan);
		     lineno := !lineno + 1
		   done
		 with End_of_file -> (Hashtbl.add source_files filename tbl; close_in in_chan))
	    file_list;
	  input_filelist := file_list;
	  initialized := true;
	  print_string "Initialized test with file list:";
	  List.iter (fun file -> print_string (" " ^ file)) file_list;
	  print_string "\n"
      end

  (** This is the main function for the test. It walks the CFA from the designated
      root function and tracks the lattice at each location.

      If a node's lattice is Bottom, the node is not reachable. The edges out of this
      node are not traversed.

      If a node is traversed a second time, the new lattice is compared against the
      lattice from the first iteration. If the new lattice is <= the old lattice,
      it is considered "covered" and not re-transversed. If the new lattice is not
      covered, it is joined with the old lattice and the result saved for future
      coverage checks. The node is then re-traversed with this new lattice.

      An error region (program location, lattice) may be passed in. It is compared
      against each region found during the search. If there is a non-empty intersection
      between a traversed region and the error region, a message is printed with this
      intersection. *)
  let walk_cfa root_fn_name error_region logger data_filename expected_bottom_cnt expected_error_cnt =
    let data_channel = open_out data_filename
    in let data_formatter = Format.formatter_of_out_channel data_channel
       and log_formatter = make_formatter_from_logger logger ""
    in
    let rec add_to_worklist start_lattice edge_list worklist =
      (* helper fn to add a set of edges to worklist for dfs traversal *)
      begin
	let rec add edge_list worklist =
	  match edge_list with
	      [] -> worklist
	    | head :: tail -> add tail ((start_lattice, head) :: worklist)
	in add (List.rev edge_list) worklist
      end
    and print_location fmt loc =
      match C_SD.get_source_position loc with
	  Some(fname, line, col) ->
	    begin
	      try 
		let code = Hashtbl.find (Hashtbl.find source_files fname) (line-1) in
		  Format.fprintf fmt "%s:%03d: %s" fname line code
	      with Not_found -> Format.fprintf fmt "%s:%03d <src not found>" fname line
	    end
	| None -> C_SD.print_location fmt loc
    and print_edge start_l edge end_l =
      (* print the edge and the lattices *)
      begin
 	let (start_fn, start_node) = C_SD.location_coords (C_SD.get_source edge)
	and (targ_fn, targ_node) = C_SD.location_coords (C_SD.get_target edge)
	in
	  Format.fprintf log_formatter "@[Edge: ";
	  print_location log_formatter (C_SD.get_source edge);
	  Format.fprintf log_formatter "@\n     --> ";
	  C_SD.Command.print log_formatter (C_SD.get_command edge);
	  Format.fprintf log_formatter "@\n     --> ";
	  print_location log_formatter (C_SD.get_target edge);
	  Format.fprintf log_formatter "@]@?";
	  Format.fprintf log_formatter "Pre lattice: ";
	  DataLattice.print log_formatter start_l;
	  Format.fprintf log_formatter "@?";
	  Format.fprintf log_formatter "Post lattice: ";
	  DataLattice.print log_formatter end_l;
	  Format.fprintf log_formatter "@?";
	  (* we write a subset of this to the data file *)
	  print_location data_formatter (C_SD.get_source edge);
	  Format.fprintf data_formatter "@\n";
	  DataLattice.print data_formatter end_l;
	  Format.fprintf data_formatter "@\n@?"
      end
    and compute_next_nodes curr_worklist curr_edge post_lattice call_stack  =
      (* Compute the next nodes to look at and return a new worklist *)
      begin
	let cmd = C_SD.edge_to_command curr_edge
	and target = (C_SD.get_target curr_edge)
	in
	  match cmd.Command.code with
	      Command.FunctionCall(expr) ->
		begin
		  let (fname, _, _) = Absutil.deconstructFunCall expr
		  in 
		    try
		      let fn_node = C_SD.lookup_entry_location fname
		      in
			push_call_stack call_stack target;
			add_to_worklist post_lattice (C_SD.get_outgoing_edges fn_node) curr_worklist
		    with C_SD.NoSuchFunctionException(fname) ->
		      Format.fprintf log_formatter "Treating %s as external function@?" fname;
		      add_to_worklist post_lattice (C_SD.get_outgoing_edges target) curr_worklist
		end
	    | Command.Block(statements) ->
		begin
		  let has_return = ref false in
		    List.iter
		      (fun statement ->
			 match statement with Command.Return(e) -> has_return := true | Command.Expr(e) -> ())
		      statements;
		    if !has_return
		    then
		      begin
			if !call_stack <> [] then
			  add_to_worklist post_lattice (C_SD.get_outgoing_edges (pop_call_stack call_stack))
			    curr_worklist
			else curr_worklist
		      end
		    else add_to_worklist post_lattice (C_SD.get_outgoing_edges target) curr_worklist
		end
	    | _ ->
		add_to_worklist post_lattice (C_SD.get_outgoing_edges target) curr_worklist
      end
    and push_call_stack stack node =
      Format.fprintf log_formatter "Pushing node onto call stack: ";
      print_location log_formatter node;
      Format.fprintf log_formatter "@?";
      Format.fprintf log_formatter "@?";
      stack := node :: !stack
    and pop_call_stack stack =
      match !stack with
	  node :: rest ->
	    Format.fprintf log_formatter "Popped node from call stack: ";
	    print_location log_formatter node;
	    Format.fprintf log_formatter "@?";
	    stack := rest;
	    node
	| [] -> failwith "popped stack too many times"
    and make_function_call_cmd fn_name arg_list =
	{ C_SD.Command.code =
	    C_SD.Command.FunctionCall(Ast.Expression.FunctionCall(
					Ast.Expression.Lval(Ast.Expression.Symbol fn_name),
					arg_list));
	  C_SD.Command.read = []; C_SD.Command.written = [] }
    and push_fcall_onto_lattice pre_lattice fn_name =
      let post_lattice =
	DataLattice.post_cmd pre_lattice (make_function_call_cmd fn_name []) ("global fn:" ^ fn_name)
      in Format.fprintf log_formatter "Call to toplevel function '%s':@?" fn_name;
	DataLattice.print log_formatter post_lattice;
	Format.fprintf log_formatter "@?";
	Format.fprintf data_formatter "Call to toplevel function '%s':" fn_name;
	DataLattice.print data_formatter post_lattice;
	Format.fprintf data_formatter "@\n@\n@?";
	post_lattice
    and compute_globals start_lattice edge_list =
      match edge_list with
	  [] -> start_lattice
	| edge :: [] ->
	    let new_lattice = DataLattice.post start_lattice (DataLattice.cast_edge edge)
	    in
	      print_edge start_lattice edge new_lattice;
	      Format.fprintf log_formatter "@?";
	      Format.fprintf data_formatter "@\n";
	      compute_globals new_lattice (C_SD.get_outgoing_edges (C_SD.get_target edge))
	| edge :: more_edges -> failwith "compute_globals should only see one outgoing edge per node"
    in
    let ini_lattice = compute_globals
			(push_fcall_onto_lattice DataLattice.top "__BLAST_initialize_constTest.c")
			(C_SD.get_outgoing_edges (C_SD.lookup_entry_location "__BLAST_initialize_constTest.c")) in
      (* the worklist is a list of (start_lattice, edge) tuples *)
      let worklist = ref (add_to_worklist (push_fcall_onto_lattice ini_lattice root_fn_name)
			    (C_SD.get_outgoing_edges (C_SD.lookup_entry_location root_fn_name)) [])
      and call_stack = ref []
      and visited_region_set = Hashtbl.create 20
      and bottom_cnt = ref 0
      and error_cnt = ref 0
      in
	(* the main loop *)
	while !worklist != [] do
	  match (!worklist) with
	      (start_lattice, curr_edge) :: rest ->
		begin
		  let post_lattice = DataLattice.post start_lattice (DataLattice.cast_edge curr_edge)
		  and target_loc = C_SD.get_target curr_edge
		  in let curr_region = (C_SD.location_coords target_loc, !call_stack, post_lattice)
		  in
		    print_edge start_lattice curr_edge post_lattice;
		    if post_lattice = DataLattice.bottom then
		      begin
			bottom_cnt := !bottom_cnt + 1;
			Format.fprintf log_formatter "Unreachable node@?";
			Format.fprintf data_formatter "Unreachable node@\n@?";
			worklist := rest
		      end
		    else
		      begin
			if (is_region_covered curr_region visited_region_set)
			then
			  begin
			    Format.fprintf log_formatter "Region is covered.@?";
			    worklist := rest
			  end
			else
			  begin
			    let error_intersect = intersect_regions curr_region error_region
			    in
			      if error_intersect<>empty_region
			      then
				begin
				  Format.fprintf log_formatter "Intersection with error region:@?";
				  print_region log_formatter error_intersect;
				  Format.fprintf log_formatter "@?";
				  Format.fprintf data_formatter "Reached an error!@\n@?";
				  error_cnt := !error_cnt + 1
				end;
			      let joined_lattice = join_regions curr_region visited_region_set log_formatter
			      in worklist := compute_next_nodes rest curr_edge joined_lattice call_stack
			  end (* region not covered *)
		      end; (* region reachable *)
		    Format.fprintf log_formatter "@?";
		    Format.fprintf data_formatter "@\n@?";
		end (* non-empty worklist *)
	    | _ -> ()
	done;
	close_out data_channel; (* close this before we throw exceptions *)
	if !bottom_cnt <> expected_bottom_cnt then
	  begin
	    raise (Test_Fail (Printf.sprintf "Reached bottom %d times, expected to reach bottom %d times"
				!bottom_cnt expected_bottom_cnt))
	  end;
	if !error_cnt <> expected_error_cnt then
	  begin
	    raise (Test_Fail (Printf.sprintf "Found error %d times, expected to find error %d times"
				!error_cnt expected_error_cnt))
	  end
end		
