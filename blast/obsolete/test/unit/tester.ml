(** Unit test driver *)

(** Log messages have one of 5 levels. LogError outputs the least information, while
    LogDetailedDebug outputs everything.
 *)
type log_level =   LogError (** Only log errors and the outermost test/test suite results *)
		 | LogResults (** Log the results of each test suite and test. *)
		 | LogProgress (** Log the start of a test, key intermediate results and files used *)
		 | LogDebug (** Log debugging information *)
		 | LogDetailedDebug (** Log very detailed debugging information *)
and test_stats = { mutable testsPassed : int;
		   mutable testsFailed : int;
		   mutable testsAborted : int;
		 }

class virtual logger_type =
object
  method virtual start_suite : string -> unit
  method virtual end_suite : string -> test_stats -> unit
  method virtual start_test : string -> unit
  method virtual test_passed : string -> unit
  method virtual test_failed : string -> string -> unit
  method virtual test_aborted : string -> string -> unit
  method virtual log_debug : string -> string -> unit
  method virtual log_detailed_debug : string -> string -> unit
end

(** Create a formatter that logs at LogDetailedDebug level
    based on the specified logger. 
 *)
let make_formatter_from_logger logger category =
  let b = Buffer.create 80
  in let flush_fn =
      (fun () -> logger#log_detailed_debug category (Buffer.contents b);
	 Buffer.reset b)
  in
    Format.make_formatter (fun str offset len -> Buffer.add_substring b str offset len)
                          flush_fn

type context = { logger : logger_type;
		parents : context list;
		name : string;
	      }

class basic_logger
  (chan : out_channel)
  (level: log_level) = 
  let prt = fun depth msg ->
    for i = 1 to depth do (output_char chan ' ') done;
    output_string chan (msg ^ "\n")
  in object
      inherit logger_type
      val mutable depth = 0
      method start_suite name =
	depth <- depth + 1;
	if (level = LogResults) or (level = LogProgress) or 
	  (level = LogDebug) or (level = LogDetailedDebug)
	then (prt depth ("Starting test suite \"" ^ name ^ "\""))
      method end_suite name results =
	if (level = LogResults) or (level = LogProgress) or
	  (level = LogDebug) or (level = LogDetailedDebug) or
	   ((level = LogError) && (depth = 1))
	then
	  begin
	    prt depth ("Completed test suite \"" ^ name ^
		       "\"  Passed: " ^ (string_of_int results.testsPassed) ^
		       "  Failed: " ^ (string_of_int results.testsFailed) ^
		       "  Aborted: " ^ (string_of_int results.testsAborted))
	  end;
	depth <- depth - 1
      method start_test name =
	depth <- depth + 1;
	if (level = LogProgress) or (level = LogDebug) or
	  (level = LogDetailedDebug)
	then (prt depth ("Starting test \"" ^ name ^ "\""))
      method test_passed name =
	if (level = LogResults) or (level = LogProgress) or
	  (level = LogDebug) or (level = LogDetailedDebug) or
	  ((level = LogError) && (depth = 1))
	then
	  begin
	    prt depth ("Test \"" ^ name ^ "\" successful")
	  end;
	depth <- depth - 1
      method test_failed name reason =
	prt depth ("Test \"" ^ name ^ "\" failed: " ^ reason);
	depth <- depth - 1
      method test_aborted name reason =
	prt depth ("Test \"" ^ name ^ "\" aborted: " ^ reason);
	depth <- depth - 1
      method log_debug category msg =
	if (level = LogDebug) or (level = LogDetailedDebug)
	then (prt (depth+1) ("(" ^ category ^ ") " ^ msg))
      method log_detailed_debug category msg =
	if (level = LogDetailedDebug)
	then
	  begin
	    if category = "" then prt (depth+2) msg
	    else prt (depth+2) ("(" ^ category ^ ") " ^ msg)
	  end
    end
    
let make_root_context =
  fun channel dbg_level testrun_name ->
    { logger = new basic_logger channel dbg_level;
      parents = [];
      name = testrun_name; }

let make_child_context =
  fun parent_ctx test_name ->
    { logger = parent_ctx.logger;
      parents = parent_ctx :: parent_ctx.parents;
      name = test_name;
    }

let make_new_test_stats =
  fun () ->
    { testsPassed = 0; testsFailed = 0; testsAborted = 0; }

let add_test_stats =
  fun a b ->
    { testsPassed = a.testsPassed + b.testsPassed;
      testsFailed = a.testsFailed + b.testsFailed;
      testsAborted = a.testsAborted + b.testsAborted; }

class virtual test_interface =
object
  method virtual get_name : unit -> string
  method virtual get_desc : unit -> string
  method virtual get_children : unit -> test_interface list
  method virtual run : context -> test_stats
end

exception Test_Fail of string
exception Test_Abort of string

class virtual test =
object (self)
  inherit test_interface
  method virtual get_name : unit -> string
  method virtual get_desc : unit -> string
  method get_children () = []
  method virtual private do_run : context -> unit
  method run parent_ctx = 
    let test_name = self#get_name ()
    in let ctx = (make_child_context parent_ctx test_name)
    in
      ctx.logger#start_test test_name;
      try
	self#do_run ctx;
	ctx.logger#test_passed test_name;
	{ testsPassed = 1; testsFailed = 0; testsAborted = 0; }
      with
	| Test_Fail (reason) ->
	    ctx.logger#test_failed test_name reason;
	    { testsPassed = 0; testsFailed = 1; testsAborted = 0; }
	| Test_Abort(reason) ->
	    ctx.logger#test_aborted test_name reason;
	    { testsPassed = 0; testsFailed = 0; testsAborted = 1; }
	| e ->
	    ctx.logger#test_aborted test_name
	      ("uncaught exception " ^ (Printexc.to_string e));
	    { testsPassed = 0; testsFailed = 0; testsAborted = 1; }
end

class generic_test
  (name : string)
  (desc : string)
  (fn : (context -> unit)) =
object
  inherit test
  method get_name () = name
  method get_desc () = desc
  method private do_run ctx = (fn ctx)
end

class test_suite
  (name : string)
  (desc : string)
  (tests : test list) =
object
  inherit test_interface
  method get_name () = name
  method get_desc () = desc
  method get_children () = tests
  method run parent_ctx = 
    let ctx = (make_child_context parent_ctx name)
    in
      ctx.logger#start_suite name;
      let result = List.fold_left
		     (fun stats tst -> add_test_stats stats (tst#run ctx))
		     (make_new_test_stats ())
		     tests
      in
	ctx.logger#end_suite name result;
	result
      
end
	    
(** Helper function for use in tests. Compares an expected value with the actual value.
   Raises a test error if not equal. *)
let assert_equal = fun exp_val act_val to_string_fn location ->
  if exp_val <> act_val then
    raise(Test_Fail("expected value: " ^ (to_string_fn exp_val) ^
		    " actual value was: " ^ (to_string_fn act_val) ^
		    " at " ^ location))
  else ()
    
(** Helper function for use in tests. Compares an expected string with the actual string.
   Raises a test error if not equal. *)
let assert_equal_string = fun exp_val act_val location ->
  (assert_equal exp_val act_val (fun x -> x) location)
  
(** Helper function for use in tests. Compares an expected int with the actual int.
   Raises a test error if not equal. *)
let assert_equal_int = fun exp_val act_val location ->
  (assert_equal exp_val act_val string_of_int location)
  
(** Helper function for use in tests. Raises an test error if value is not true *)
let assert_true = fun value errormsg ->
  if value = false then
    raise(Test_Fail errormsg)
  else ()
    
(** Helper function for use in tests. Raises an test error if value is not false *)
let assert_false = fun value errormsg ->
  if value = true then
    raise(Test_Fail errormsg)
  else ()

(** List of tests that have been registered so far (with most recently registered test first).
    This list is used in batch mode to gather the tests in a place where the main function can
    get to them. *)
let registered_test_list = ref []

(** A map of each individual test and suite that has been registered. If suite "foo" containing
    test "bar" is registered, then the keys are "foo" and "foo.bar". *)
let registered_test_map = Hashtbl.create 50

(** Register a test. *)
let register_test = fun test ->
  registered_test_list := test :: !registered_test_list;
  (* recursively add this test and all its children to map *)
  let rec add_to_map = fun parent_name tst ->
    let name = match parent_name with
	"" -> (tst#get_name ())
      | _ -> (parent_name ^ "." ^ (tst#get_name ()))
    in
      Hashtbl.add registered_test_map name tst;
      List.iter (fun tst -> (add_to_map name tst)) (tst#get_children ())
  in (add_to_map "" test)

(** Get the specified test from registered_test_map *)  
let get_registered_test = fun name ->
  try
    Hashtbl.find registered_test_map name
  with
      Not_found -> raise (Test_Abort("No test with name " ^ name ^
				     " has been registered"))

(** Build a suite of all registered tests. If only one test or suite was
    registered, just return that. *)
let get_suite_of_registered_tests = fun run_name test_name_list ->
  let test_list =
    match test_name_list with
	[] -> List.rev !registered_test_list
      | _ ->
	  List.fold_left (fun tstlst name -> (get_registered_test name) :: tstlst)
	    [] test_name_list
  in match test_list with
      [] -> raise (Test_Abort "No tests to run!")
    | [tst] -> tst
    | _ -> new test_suite run_name run_name test_list

(** print the name of the test/suite and all its children to the channel *)
let print_test_list = fun channel test ->
  let rec print_test = 
    fun indent tst ->
      output_string channel (indent ^ (tst#get_name ()) ^ " - " ^ (tst#get_desc()) ^ "\n");
      List.iter (fun child -> (print_test (indent ^ " ") child)) (tst#get_children ())
  in print_test "" test

(** Variable that controls whether we are in interactive mode or batch mode.
    See test_script_main for details. *)
let interactive_mode = ref true

(** This function should be called by the load time code (e.g. "_") of each test file.
    If interactive_mode is true, it will run the test immediately with the specified log
    level. If interactive_mode is false, we are in batch mode, and the test suite will
    just be registered through register_test. *)
let test_script_main = fun suite loglvl ->
  if !interactive_mode = true then
    let result = suite#run (make_root_context stdout loglvl "test run")
    in flush stdout
  else register_test suite
