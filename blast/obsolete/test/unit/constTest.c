// globals
int test3_global = 5;


int unknown();

void assert(int p) {
   if (!p) {
   ERROR: 1/0;
  }
}
// test1 - basic sanity check
void test1() {
  int b = 5;
  int c = 6;
  int a, d;
  a = unknown();
  if (c>b) {
    d = a + b + c;
  }
  else { // this should be unreachable
    assert(0);
  }
  while (b < c) { // loop should be traversed once (unreachable on 2nd iter)
    b = b + 1; // should fold this to 6
  }
  if (b!=6) {
    assert(0); // should be unreachable, but we cannot prove this w/o preds
  }
}
/* 031 */
// test2 - verify coverage check
void bar ()
{
  int j = 0;
  int i;
  for (i = 0; i < 5; i++) {
    if (i == j) {
      printf("bar: i==j\n"); /* Unreachable on second iteration */
    }
  }
  return i;
}
// test3 - test of function calls
void test3a()
{
  int i;
  i = test3b(1); /* i should be folded to 1 */
  if (i > test3_global) {
    assert(0); /* should not be reachable */
  }
  test3c(); /* test using a function as a statement */
  assert(test3_global == 4);
}

int test3b(int j) {
  test3_global = j + 1;
  return j;
}

void test3c() {
  test3_global = 4;
}
// test 4 - test adding predicates in if statements
void test4() {
  int a = 0;
  int b = unknown();
  int c = b;
  if (a != 0) {
    return; // unreachable
  }
  if (b != 1) {
    return; 
  }
  // should have b -> 1 and c -> 1 in lattice
  if (c != 1) { 
    assert(0); // unreachable
  }
  a = unknown(); // havoc a
  if (a == c) { // we know c, so a should get c's value (1)
    if (b!=a) {
      assert(0); // should be unreachable
    }
  }
}

// test5 - check pointer invalidation
void test5()
{
  int a = 0;
  int b = 0;
  int *p1;
  int *p2;
  dummy(); // should have a and b in lattice
  p1 = &a;
  *p1 = 1; // should rmv mapping for a from lattice (or, better, update it)
  dummy();
  p2 = &b;
  *p2 = 5; // should remove mapping for b (or replace with b = 5)
  dummy(); // break up the calls
  p2 = &a; // should rmv mpg for *p2 (or be smart enough to compute that *p2=1)
  dummy();
}

int* test6a()
{
  int* x;
  x = (int*)malloc(sizeof(int));
  *x = 1;
  return x;
}

void test6() /* test call/return in symbolic store lattice */
{
  int *y;
  y = test6a();
  assert(*y == 1);
}

void test7() /* test handling of arrays */
{
    char *f = "string";
    char *g[2];
    char *h;
    char c;

    c = f[0];
    g[0] = "str1";
    g[1] = "str2";
    h = malloc (3*sizeof(char));
    h[0] = 'c';
    h[1] = 'd';
    h[2] = '\0';
}

void main() {
  test1();
  bar();
  test3a();
  test4();
  test5();
  test6();
  test7();
}
