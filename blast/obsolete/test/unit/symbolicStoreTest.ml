module C_SD = BlastCSystemDescr.C_System_Descr
module Command = BlastCSystemDescr.C_Command
module Operation = Absutil.Make_Operation(C_SD) 
module SymbolicStoreLattice = (Lattice.Make_SymbolicStoreLattice (C_SD)) (Operation)

(** Define a module that implements LATTICE' *)
module SymbolicStoreLattice' : LatticeChecker.LATTICE' =
  struct
    include SymbolicStoreLattice
    let cast_edge (edge : C_SD.edge) = edge
  end

(** Create a lattice checker for symbolic stores *)
module SLC = LatticeChecker.Make_LatticeChecker (SymbolicStoreLattice')

open Tester
open SymbolicStore
open SymbolicStore.Value

let initialized = ref false

let initialize () =
  if !initialized = false then
    begin
      SLC.initialize["constTest.c"];
      SymbolicStoreLattice'.initialize ();
      initialized := true
    end

let store_unit_test (print_fn) =
  let rec symname_to_expr name = (Ast.Expression.Lval (Ast.Expression.Symbol name))
  and dereference_expr expr = (Ast.Expression.Lval (Ast.Expression.Dereference expr))
  and constant_int_expr num = (Ast.Expression.Constant (Ast.Constant.Int num))
  and assert_values_equal e1 e2 = Tester.assert_equal e1 e2 valueToString "SymLoc unit_test"
  and assert_addresses_equal a1 a2 = Tester.assert_equal a1 a2 addressToString "SymLoc unit_test"
  and assert_lvalues_equal lv1 lv2 = Tester.assert_equal lv1 lv2 lvalueToString "SymLoc unit_test"
  and variable_region s = BaseRegion (Variable s)
  and member_lval (var:string) (mem:string) :Ast.Expression.lval =
    (Ast.Expression.Access (Ast.Expression.Dot, symname_to_expr var, mem))
  and nested_member_lval (var:string) (mem1:string) (mem2:string) :Ast.Expression.lval =
    (Ast.Expression.Access (Ast.Expression.Dot, Ast.Expression.Lval(member_lval var mem1), mem2))
  and indexed_lval (var:string) (id:int) :Ast.Expression.lval =
    (Ast.Expression.Indexed (symname_to_expr var, Ast.Expression.Constant (Ast.Constant.Int id)))
  and symstore_options =
    { SymbolicStore.AlgorithmOptions.merge_string_constants = false;
      SymbolicStore.AlgorithmOptions.expr_is_array =
	(fun expr ->
	   match expr with
	       Ast.Expression.Lval (Ast.Expression.Symbol s)
	     | Ast.Expression.Lval (Ast.Expression.Indexed (Ast.Expression.Lval (Ast.Expression.Symbol s), _)) ->
		 if SymbolicStore.string_starts_with s "a_"
		 then (print_fn ("Treating " ^ (Ast.Expression.toString expr) ^ " as an array..."); true)
		 else false
	     | _ -> false) }
  in
    SymbolicStore._reset_counters ();
    begin
      let store = SymbolicStore.create_new_store symstore_options
      and c_of_two = Ast.Expression.Indexed ((symname_to_expr "a_c"), (constant_int_expr 2)) in
	replace store (variable_region "a") (Constant (Ast.Constant.Int 4));
	(* Store: [a => 4] *)
	print_fn ("Store: " ^ (storeToString store));
	print_fn "Checking that (eval a) -> 4 ...";
	assert_values_equal (Constant (Ast.Constant.Int 4)) (eval store (symname_to_expr "a"));
	print_fn "ok.";
	
	replace store (variable_region "x") (Pointer (MustPointer (Region (variable_region "a"))));
	(* Store: [a => 4; x => &a] *)
	print_fn ("Store: " ^ (storeToString store));
	print_fn "Checking that (eval *x) -> 4 ...";
	assert_values_equal (Constant (Ast.Constant.Int 4)) (eval store (dereference_expr (symname_to_expr "x")));
	print_fn "ok.";
    
	print_fn "Checking that (canonicize *x) -> &a ...";
	(let lval = (canonicize_lval store (Ast.Expression.Dereference (symname_to_expr "x")))
	 in assert_lvalues_equal  (Dereference (MustPointer (Region (variable_region "a")))) lval);
	print_fn "ok.";
	
	print_fn "Testing assign: b = 0 ...";
	assign store (Ast.Expression.Symbol "b") (constant_int_expr 0);
	print_fn ("Store: " ^ (storeToString store));
	assert_values_equal (Constant (Ast.Constant.Int 0)) (eval store (symname_to_expr "b"));
	print_fn "ok.";
	
	print_fn "Testing assign: *x = 1 (should assign a) ...";
	assign store (Ast.Expression.Dereference (symname_to_expr "x")) (constant_int_expr 1);
	print_fn ("Store: " ^ (storeToString store));
	assert_values_equal (Constant (Ast.Constant.Int 1)) (eval store (symname_to_expr "a"));
	print_fn "ok.";
	
	print_fn "Testing that (canonicize a_c[2]) -> a_c[*] ...";
	(let lval = (canonicize_lval store c_of_two) in
	   assert_lvalues_equal (Dereference (MustPointer (Offset (variable_region "a_c")))) lval);
	print_fn "ok.";
	
	print_fn "Testing assign: a_c[2] = 5 ...";
	assign store c_of_two (constant_int_expr 5);
	print_fn ("Store: " ^ (storeToString store));
	assert_values_equal Top (eval store (Ast.Expression.Lval c_of_two));
	print_fn "ok.";
    
	begin
	  let lv = (Ast.Expression.Dereference
		      (Ast.Expression.Binary (Ast.Expression.Plus,
					      symname_to_expr "x", 
					      constant_int_expr 1)))
	  in
	    print_fn "Testing assign: *(x + 1) = &b ...";
	    assign store lv (Ast.Expression.Unary (Ast.Expression.Address, symname_to_expr "b"));
	    print_fn ("Store: " ^ (storeToString store));
	    assert_values_equal (Pointer (MayPointer (AddressSet.singleton (Region (variable_region "b")))))
	      (eval store (Ast.Expression.Lval lv));
	    print_fn "Testing assign: *(*(x+1)) = 2 ...";
	    assign store (Ast.Expression.Dereference (Ast.Expression.Lval lv))
	      (constant_int_expr 2);
	    print_fn ("Store: " ^ (storeToString store));
	    assert_values_equal Top (eval store (symname_to_expr "b"));
	    print_fn "ok."
	end
    end;
    
    print_fn "";
    print_fn "Testing pointer invalidation...";
    begin
      let s1 = SymbolicStore.create_new_store symstore_options
      and s2 = SymbolicStore.create_new_store symstore_options
      in
	assign s1 (Ast.Expression.Symbol "a") (constant_int_expr 1);
	assign s1 (Ast.Expression.Symbol "b") (constant_int_expr 2);
	assign s1 (Ast.Expression.Symbol "c") (constant_int_expr 3);
	assign s1 (Ast.Expression.Symbol "x")
	  (Ast.Expression.Unary (Ast.Expression.Address, symname_to_expr "a"));
	assign s1 (Ast.Expression.Symbol "y")
	  (Ast.Expression.Unary (Ast.Expression.Address, symname_to_expr "a"));
	assign s2 (Ast.Expression.Symbol "a") (constant_int_expr 1);
	assign s2 (Ast.Expression.Symbol "b") (constant_int_expr 2);
	assign s2 (Ast.Expression.Symbol "c") (constant_int_expr 3);
	assign s2 (Ast.Expression.Symbol "x")
	  (Ast.Expression.Unary (Ast.Expression.Address, symname_to_expr "b"));
	assign s2 (Ast.Expression.Symbol "y")
	  (Ast.Expression.Unary (Ast.Expression.Address, symname_to_expr "c"));
	print_fn "Check backpointers of s2 before merge...";
	print_fn ("Store: " ^ (storeToString s2));
	print_fn ("Backpointers: " ^ (backpointersToString s2));
	(let pointers_to_a = get_pointers_to s2 (variable_region "a")
	 and pointers_to_b = get_pointers_to s2 (variable_region "b")
	 and pointers_to_c = get_pointers_to s2 (variable_region "c")
	 in
	   assert_true (RegionSet.mem (variable_region "y") pointers_to_c) "y not in pointers to c";
	   assert_true (pointers_to_a = RegionSet.empty) "pointers to a should be empty";
	   assert_false (RegionSet.mem (variable_region "y") pointers_to_a ) "y in pointers to a";
	   assert_false (RegionSet.mem (variable_region "x") pointers_to_c) "x in pointers to c");
	print_fn "ok.";
	begin
	  let u = SymbolicStore.union s1 s2
	  in
	    print_fn "Check backpointers after merge...";
	    print_fn ("Store: " ^ (storeToString u));
	    print_fn ("Backpointers: " ^ (backpointersToString u));
	    (let pointers_to_a = get_pointers_to u (variable_region "a")
	     and pointers_to_b = get_pointers_to u (variable_region "b")
	     and pointers_to_c = get_pointers_to u (variable_region "c")
	     in assert_true (RegionSet.mem (variable_region "x") pointers_to_a) "x not in pointers to a";
	       assert_true (RegionSet.mem (variable_region "y") pointers_to_a) "y not in pointers to a";
	       assert_true (RegionSet.mem (variable_region "x") pointers_to_b) "x not in pointers to b";
	       assert_false (RegionSet.mem (variable_region "y") pointers_to_b) "y in pointers to b";
	       assert_true (RegionSet.mem (variable_region "y") pointers_to_c) "y not in pointers to c";
	       assert_false (RegionSet.mem (variable_region "x") pointers_to_c) "x in pointers to c");
	    print_fn "ok.";
	    print_fn "Check assign through *x, a MayPointer...";
	    assign u (Ast.Expression.Dereference (symname_to_expr "x")) (constant_int_expr 4);
	    print_fn ("Store: " ^ (storeToString u));
	    assert_values_equal Top (lookup u (variable_region "a"));
	    assert_values_equal Top (lookup u (variable_region "b"));
	    assert_values_equal (Constant (Ast.Constant.Int 4))
	      (eval u (dereference_expr (symname_to_expr "x")));
	    print_fn "ok.";
	    print_fn "Check invalidation of PseudoPointers...";
	    print_fn "*y = 5 (which should convert x back to a MayPointer)";
	    assign u (Ast.Expression.Dereference (symname_to_expr "y")) (constant_int_expr 5);
	    print_fn ("Store: " ^ (storeToString u));
	    assert_values_equal Top (eval u (dereference_expr (symname_to_expr "x")));
	    assert_values_equal Top (lookup u (variable_region "c"));
	    assert_values_equal (Constant (Ast.Constant.Int 5))
	      (eval u (dereference_expr (symname_to_expr "y")));
	    print_fn "ok.";
	    print_fn "Check assignment of a PseudoPointer value...";
	    print_fn "z = y";
	    assign u (Ast.Expression.Symbol "z") (symname_to_expr "y");
	    print_fn ("Store: " ^ (storeToString u));
	    print_fn ("Backpointers: " ^ (backpointersToString u));
	    assert_values_equal (Constant (Ast.Constant.Int 5))
	      (eval u (dereference_expr (symname_to_expr "z")));
	    (let pointers_to_a = get_pointers_to u (variable_region "a")
	     and pointers_to_b = get_pointers_to u (variable_region "b")
	     and pointers_to_c = get_pointers_to u (variable_region "c")
	     in assert_true (RegionSet.mem (variable_region "z") pointers_to_a) "z not in pointers to a";
	       assert_false (RegionSet.mem (variable_region "z") pointers_to_b) "z in pointers to b";
	       assert_true (RegionSet.mem (variable_region "z") pointers_to_c) "z not in pointers to c");
	    print_fn "ok.";
	    print_fn "Check assignment to a PseudoPointer value...";
	    print_fn "*z = 6";
	    assign u (Ast.Expression.Dereference (symname_to_expr "z")) (constant_int_expr 6);
	    print_fn ("Store: " ^ (storeToString u));
	    assert_values_equal (Constant (Ast.Constant.Int 6))
	      (eval u (dereference_expr (symname_to_expr "z")));
	    assert_values_equal (Constant (Ast.Constant.Int 6))
	      (eval u (dereference_expr (symname_to_expr "y")));
	    print_fn "ok.";
	    print_fn "Check invalidation of a PseudoPointer through assignment to a target variable...";
	    print_fn "a = 7 (should convert y and z to MayPointers)";
	    assign u (Ast.Expression.Symbol "a") (constant_int_expr 7);
	    print_fn ("Store: " ^ (storeToString u));
	    assert_values_equal Top (eval u (dereference_expr (symname_to_expr "y")));
	    assert_values_equal Top (eval u (dereference_expr (symname_to_expr "z")));
	    print_fn "ok."
	end;
	begin
	  let s1 = create_new_store symstore_options
	  and s2 = create_new_store symstore_options
	  in
	    print_fn "Checking union...";
	    assign s1 (Ast.Expression.Symbol "a") (constant_int_expr 0);
	    assign s2 (Ast.Expression.Symbol "a") (constant_int_expr 0);
	    assign s1 (Ast.Expression.Symbol "b") (constant_int_expr 1);
	    assign s2 (Ast.Expression.Symbol "b") (constant_int_expr 2);
	    assign s1 (member_lval "c" "x") (constant_int_expr 3);
	    assign s2 (member_lval "c" "x") (constant_int_expr 3);
	    assign s1 (member_lval "c" "y") (constant_int_expr 4);
	    assign s2 (member_lval "c" "y") (constant_int_expr 5);
	    assign s1 (member_lval "d" "r") (constant_int_expr 4);
	    assign s2 (member_lval "d" "s") (constant_int_expr 4);
	    let res_store = union s1 s2
	    in
	      print_fn ("Input Store 1: " ^ (storeToString s1));
	      print_fn ("Input Store 2: " ^ (storeToString s2));
	      print_fn ("Result Store: " ^ (storeToString res_store));
	      iter_basic
		(fun r v ->
		   match (r, v) with
		       (BaseRegion (Variable "a"), Constant (Ast.Constant.Int 0)) -> ()
		     | (BaseRegion (Variable "c"), Structure s) ->
			 begin
			   Hashtbl.iter
			     (fun m e ->
				match (m, e) with
				    ("x", {v=Constant (Ast.Constant.Int 3);is_array=false}) -> ()
				  | _ ->
				      raise (Test_Fail ("Union test found unexpected structure member " ^ m)))
			     s
			 end
		     | _ ->
			 raise (Test_Fail ("Union test found unexpected store entry " ^ (regionToString r))))
		res_store;
	      print_fn "ok."
	end;
	begin
	  let s = create_new_store symstore_options
	  and a_dot_x_q = nested_member_lval "a" "x" "q"
	  and a_dot_q_x = nested_member_lval "a" "q" "x"
	  in
	    print_fn "Checking assign of nested structures...";
	    print_fn "Assigning 0 to a.x.q...";
	    assign s a_dot_x_q (constant_int_expr 0);
	    print_fn "Assigning 1 to a.q.x...";
	    assign s a_dot_q_x (constant_int_expr 1);
	    print_fn ("Store: " ^ (storeToString s));
	    assert_values_equal (Constant (Ast.Constant.Int 0)) (eval s (Ast.Expression.Lval a_dot_x_q));
	    assert_values_equal (Constant (Ast.Constant.Int 1)) (eval s (Ast.Expression.Lval a_dot_q_x));
	    print_fn "ok."
	end;
	begin
	  let s = create_new_store symstore_options;
	  in
	    print_fn "Checking use of string constants as pointers...";
	    assign s (Ast.Expression.Symbol "f") (Ast.Expression.Constant (Ast.Constant.String "str_val"));
	    assign s (Ast.Expression.Symbol "temp_mem")
	      (Ast.Expression.Binary (Ast.Expression.Plus, (symname_to_expr "f"),
				      (constant_int_expr 0)));
	    print_fn ("Store: " ^ (storeToString s))
	end;
	begin
	  let s = create_new_store symstore_options;
	  in
	    print_fn "Checking arrays and string constants...";
	    print_fn "Assigning f = \"str_val\" ...";
	    assign s (Ast.Expression.Symbol "f") (Ast.Expression.Constant (Ast.Constant.String "str_val"));
	    print_fn "Assigning a_g[0] = \"str_val\" ...";
	    assign s (indexed_lval "a_g" 0) (Ast.Expression.Constant (Ast.Constant.String "str_val"));
	    print_fn "Assigning a_h[0] = malloc() ...";
	    SymbolicStore.allocate_heap_region s (indexed_lval "a_h" 0) "testProgLoc";
	    print_fn "Assigning i = malloc() ...";
	    SymbolicStore.allocate_heap_region s (Ast.Expression.Symbol "i") "testProgLoc2";
	    print_fn ("Store: " ^ (storeToString s));
	    print_fn ("Canonicize_lval f[0]) = " ^
		      (SymbolicStore.lvalueToString (SymbolicStore.canonicize_lval s (indexed_lval "f" 0))));
	    assert_lvalues_equal (SymbolicStore.Dereference
				    (MustPointer (Offset (BaseRegion (Heap "Const_String:str_val")))))
		      (SymbolicStore.canonicize_lval s (indexed_lval "f" 0));
	    print_fn ("Canonicize_lval a_g[0]) = " ^
		      (SymbolicStore.lvalueToString (SymbolicStore.canonicize_lval s (indexed_lval "a_g" 0))));
	    assert_lvalues_equal (SymbolicStore.Dereference
				    (MustPointer (Offset (BaseRegion (Variable "a_g")))))
		      (SymbolicStore.canonicize_lval s (indexed_lval "a_g" 0));
	    print_fn ("Canonicize_lval a_h[0]) = " ^
		      (SymbolicStore.lvalueToString (SymbolicStore.canonicize_lval s (indexed_lval "a_h" 0))));
	    assert_lvalues_equal (SymbolicStore.Dereference
				    (MustPointer (Offset (BaseRegion (Variable "a_h")))))
	      (SymbolicStore.canonicize_lval s (indexed_lval "a_h" 0));
	    print_fn ("Canonicize_lval i[0]) = " ^
		      (SymbolicStore.lvalueToString (SymbolicStore.canonicize_lval s (indexed_lval "i" 0))));
	    assert_lvalues_equal (SymbolicStore.Dereference
				    (MustPointer (Offset (BaseRegion (Heap "testProgLoc2")))))
		      (SymbolicStore.canonicize_lval s (indexed_lval "i" 0));
	    print_fn "ok."
	end;

	print_fn "Check leq works correctly for array and heap variables...";
	begin
	  let s1 = create_new_store symstore_options
	  and s2 = create_new_store symstore_options
	  in
	    assign s1 (indexed_lval "a_a" 0) (symname_to_expr "z");
	    print_fn ((SymbolicStore.storeToString s1) ^ " should not be leq " ^
		      (SymbolicStore.storeToString s2));
	    assert_false (SymbolicStore.leq s1 s2) "leq returned true";
	    print_fn "ok."
	end;
	begin
	  let s1 = create_new_store symstore_options
	  and s2 = create_new_store symstore_options
	  in
	    SymbolicStore.allocate_heap_region s1 (Ast.Expression.Symbol "c") "foo";
	    SymbolicStore.allocate_heap_region s2 (Ast.Expression.Symbol "c") "foo";
	    (* We make s1 an array and leave s2 as a non-array *)
	    assign s1 (Ast.Expression.Dereference
			 (Ast.Expression.Binary (Ast.Expression.Plus, (symname_to_expr "c"),
						 (Ast.Expression.Constant (Ast.Constant.Int 1)))))
	      (Ast.Expression.Constant (Ast.Constant.Int 1));
	    assign s2 (Ast.Expression.Dereference (symname_to_expr "c")) (symname_to_expr "z");
	    (* ??? The following test originally returned false. However, when we made the heap
	       always an array, leq will now return true. *)
	    (*print_fn ((SymbolicStore.storeToString s1) ^ " should not be leq ");
	      print_fn (SymbolicStore.storeToString s2);
	    assert_false (SymbolicStore.leq s1 s2) "leq returned true";*)
	    print_fn (SymbolicStore.storeToString s2);
	    print_fn ((SymbolicStore.storeToString s1) ^ " should be leq ");
	    print_fn (SymbolicStore.storeToString s2);
	    assert_true (SymbolicStore.leq s1 s2) "leq returned false";

	    print_fn "ok."
	end;
	begin
	  let s1 = create_new_store symstore_options
	  and s2 = create_new_store symstore_options
	  in
	    SymbolicStore.allocate_heap_region s1 (Ast.Expression.Symbol "c") "foo";
	    (* Remove c, leaving behind the heap region *)
	    SymbolicStore.remove s1 (BaseRegion (Variable "c"));
	    print_fn ((SymbolicStore.storeToString s1) ^ " should not be leq " ^
		      (SymbolicStore.storeToString s2));
	    assert_false (SymbolicStore.leq s1 s2) "leq returned true";
	    print_fn "ok."
	end;
	begin
	  let s = create_new_store symstore_options
	  in
	    print_fn "Checking move_region_pointers...";
	    assign s (nested_member_lval "a" "b" "c") (constant_int_expr 1);
	    assign s (Ast.Expression.Symbol "x")
			(Ast.Expression.Unary (Ast.Expression.Address,
					       (Ast.Expression.Lval (nested_member_lval "a" "b" "c"))));
	    assign s (Ast.Expression.Symbol "y")
			(Ast.Expression.Unary (Ast.Expression.Address,
					       (Ast.Expression.Lval (Ast.Expression.Symbol "a"))));
	    print_fn (" Store before move: " ^ (SymbolicStore.storeToString s));
	    move_region_pointers s (BaseRegion (Variable "a")) (BaseRegion (Variable "d"));
	    print_fn (" Store after move from a to d: " ^ (SymbolicStore.storeToString s))
	end
    end


let store_test1 = new generic_test "store_unit_test" "unit test for symbolic store data structure"
		    (fun ctx ->
		       store_unit_test (fun msg -> ctx.logger#log_detailed_debug "SymbolicStore" msg))

let error_region = ((0, 3), [], SymbolicStoreLattice'.top) (* Error line of assert in constTest.c *)

let lattice_test1 = new generic_test "lattice1" "basic test for symbolic store lattice, uses constTest.c:test1()"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test1" error_region ctx.logger "symstore_lattice1.out" 3 1)

let lattice_test2 = new generic_test "lattice2" "test coverage check, uses constTest.c:bar()"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "bar" error_region ctx.logger "symstore_lattice2.out" 2 0)

let lattice_test3 = new generic_test "lattice3" "test function calls"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test3a" error_region ctx.logger "symstore_lattice3.out" 2 0)

(** Test 4 - this test should not hit any errors if we have a way of tracking equality constraints
    between regions. We have disabled symbolic constants (leq fails too often), so we now hit an error
    o2 times.*)
let lattice_test4 = new generic_test "lattice4" "test symbolic constants"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test4" error_region ctx.logger "symstore_lattice4.out" 4 2)

let lattice_test5 = new generic_test "lattice5" "Pointer invalidation"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test5" error_region ctx.logger "symstore_lattice5.out" 0 0)

let lattice_test6 = new generic_test "lattice6" "Call/return with heap allocation"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test6" error_region ctx.logger "symstore_lattice6.out" 1 0)

let lattice_test7 = new generic_test "lattice7" "handling of arrays and string constants"
		      (fun ctx ->
			 initialize ();
			 SymbolicStore._reset_counters ();
			 SLC.walk_cfa "test7" error_region ctx.logger "symstore_lattice7.out" 0 0)

let suite =
  new test_suite "symbolic_store" "verify symbolic store lattice" [store_test1; lattice_test1; lattice_test2;
								   lattice_test3; lattice_test4; lattice_test5;
								   lattice_test6; lattice_test7]

let _ = test_script_main suite LogDetailedDebug
      
