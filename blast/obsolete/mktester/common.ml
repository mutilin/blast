open Cil

let outputFileContents outf fname =
  let inf = open_in fname in
  let rec loop () =
    output_string outf (input_line inf);
    output_char outf '\n';
    loop () in
  try
    loop ()
  with End_of_file -> close_in inf

let outputFile outf file =
  List.iter (fun x -> Pretty.fprint outf 80 (Cil.printGlobal Cil.defaultCilPrinter () x);
    output_char outf '\n')
    file.globals

let printFile = outputFile stdout

let fun_defined file name =
  let rec search = function
      [] -> false
    | (GFun (fundec, _))::rest ->
	compare (fundec.svar.vname) name == 0
      || search rest
    | _::rest -> search rest
  in search file.globals

let get_fundec file name =
  let rec search = function
      [] -> raise (Failure "Function declaration not found")
    | (GFun (fundec, _))::rest ->
	if compare (fundec.svar.vname) name == 0 then
	  fundec
	else
	  search rest
    | _::rest -> search rest
  in search file.globals

let parseFile fname = Frontc.parse fname ()

