(* Instrument a C source file, given name of a function to test and a file.
 * Returns instrumented file plus string to use to call start function.
 *)
val instrument : string -> Cil.file -> Cil.file * string * int
