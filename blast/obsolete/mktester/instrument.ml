let _blastReadTest = Cil.makeGlobalVar "_blastReadTest"
    (Cil.TFun (Cil.intType, Some [], false, []))
let testLval = Cil.Lval (Cil.Var _blastReadTest, Cil.NoOffset)

class testVisitor file renameMain =
object
  inherit Cil.nopCilVisitor
      
  method vinst inst =
    match inst with
      Cil.Call ((Some _ as lv), Cil.Lval (Cil.Var vi, Cil.NoOffset), xs, loc)
      when not (Common.fun_defined file vi.Cil.vname) ->
	Cil.ChangeTo [Cil.Call (lv, testLval, [], loc)]
    | _ -> Cil.SkipChildren

  method vfunc fundec : Cil.fundec Cil.visitAction  =
    if renameMain && fundec.Cil.svar.Cil.vname = "main" then
      fundec.Cil.svar.Cil.vname <- "_main";
    Cil.DoChildren
end

let instrument func file =
  let fundec = Common.get_fundec file func in
  let renameMain = func = "main" in
  let name = if renameMain then "_main" else func in
  let conversion v = "(" ^ Pretty.sprint 999 (Cil.d_type () v.Cil.vtype) ^ ")" in
  let rec makeArgs fs i =
    match fs with
      [] -> ""
    | [v] -> conversion v ^ "par[" ^ string_of_int i ^ "]"
    | v::fs -> conversion v ^ "par[" ^ string_of_int i ^ "], " ^ makeArgs fs (i+1) in
  Cil.visitCilFile (new testVisitor file renameMain) file,
  name ^ "(" ^ makeArgs fundec.Cil.sformals 0 ^ ")",
  List.length fundec.Cil.sformals
  
  
