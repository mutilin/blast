(* THIS FILE WILL NEVER BE OVERWRITTEN.
Use it as a template for your own main module.*)
open GMain

(* we shall have an instance of the Options module here *)
(* TO DO *)
(* options save *)
(* options load *)
(* load/save stuff source, predicates *)
(* what the hell is a modal window ? *)


type query = Src | Pred | Trace | Opt

(* globals used because i dont know a better way! *)
let src_file_selected = ref ""
let pred_file_selected = ref ""
let trace_file_selected = ref ""
let options_file_selected = ref ""
let file_selected_ptr = ref Src
let demo_flag = ref false 

(* evil hack for queries -- a pointer to a pair (unit -> unit, unit -> unit) *)
let query_ans_fun_ptr = ref ((fun () -> ()), (fun () -> ()))

let current_ptr () = 
  match (!file_selected_ptr) with
      Src -> src_file_selected
    | Pred -> pred_file_selected
    | Trace -> trace_file_selected
    | Opt -> options_file_selected
    | _ -> failwith "unknown value in file_selected_ptr"

let main_window = ref None (* an annoying thing that one has to do! *)
let get_main_window () =
  match !main_window with
      None -> failwith "bad call to get_main_window"
    | Some w -> w 

exception Src_File_Undefined 


let options_to_string () = 
 (* trawls the present options and runs a command line version *)
   let pred_string = 
     if not (!pred_file_selected = "-1" or !pred_file_selected = "")
     then ("-pred "^(!pred_file_selected)) else  "" 
   in
   (*let generic_string = (self#top_window1#entry1#text)*) (* "-msvc -nofp -bddcov -nocache " in*)
   let demo_string = if (!demo_flag) then "-demo" else "" in
   let src_string = 
     if not (!src_file_selected ="-1" or !src_file_selected ="")
     then (!src_file_selected) else raise Src_File_Undefined 
   in
     Printf.sprintf "  %s %s %s  "(*(generic_string^demo_string)*) pred_string src_string ""


let blast_proc_ptr = ref None

let event_list = ref [] 

let chewline () =
  let eventlist = !event_list in
  let (ic,oc) = 
    match !blast_proc_ptr with
	Some (i,o) -> (i,o)
      | None -> failwith "chewline w/o blast_proc_ptr!"
  in
  let mw = get_main_window () in
  let (pane : GEdit.text) = mw#log_text in
  let rec _chewline () = 
    (mw)#notebook1#goto_page 0;
    try 
      let line = input_line ic in
      let _ = pane#insert (line^"\n") in
	begin
	  try 
	    let (_,callfn,cont) = (List.find (fun (x,_,_) -> Misc.is_substring line x) eventlist) 
	    in
	      (callfn (); if cont then _chewline ())
	  with Not_found -> _chewline ()
	end
    with End_of_file -> let status = Unix.close_process (ic,oc) in ()
  in
    _chewline ()
      
let rec proc_dump_pane cmd =
  let (ic,oc) = Unix.open_process cmd in
  let _ = blast_proc_ptr := Some (ic,oc) in
  let _ = 
    Gui.exit_fn_ptr := 
    (fun () -> 
       prerr_endline "done viewing trace";
       output_string oc "\n"; 
       flush oc;
       chewline ()) 
  in
    chewline ()
      
let file_dump_pane (pane : GEdit.text) fname =
  let len = pane#length in
  let _ = pane#delete_text 0 len in
  let lines = List.map (fun x -> x^"\n") (Misc.string_list_of_file fname) in
  let t = String.concat "" lines in
    pane#insert t
(*
  method on_button1_clicked () = 
    (* let _ = print_string "clicked button1 " in *)
  let cmd = self#top_window1#entry1#text in
  let _ = proc_dump_pane (self#top_window1#text1) cmd
    (* self#top_window1#text1#insert (cmd^" \n") *) in
  ()
 *)

let about_ok_ptr = ref (fun () -> ())
let about_ok_cont () =
  (!about_ok_ptr) ();
  about_ok_ptr := fun () -> ()

let set_about_ok_cont cont = 
  about_ok_ptr := cont

class customized_callbacks_about_window = object(self)
  inherit Blastgui_glade_callbacks.default_callbacks
  method on_about_ok_button_clicked () = 
    (* shouldonly be called when the other window is created -- can it be called other wise ?? *)
    self#top_about_window#about_window#destroy ();
    about_ok_cont ()
      
end


class customized_callbacks_query_window = object(self)
  inherit Blastgui_glade_callbacks.default_callbacks 
  method on_yes_button_clicked () =
    self#top_query_window#query_window#destroy ();
    (fst !query_ans_fun_ptr) ()

  method on_no_button_clicked () =
    self#top_query_window#query_window#destroy ();
    (snd !query_ans_fun_ptr) ()
end

(* t is the title of the window and s is the body of the message *)
let show_message t s = 
  let cb = new customized_callbacks_about_window (* Blastgui_glade_callbacks.default_callbacks*) in
  let abt_window = new Blastgui_glade_interface.top_about_window cb in
(*    self#set_about_window abt_window; *)
    abt_window#about_window#set_title t;
    abt_window#about_label#set_text s;
    abt_window#about_window#show ()

let show_message_cont t s cont = 
  set_about_ok_cont cont;
  show_message t s 

(* t is the title, s is the question, yesno is a pair of functions saying what to do if the yes/no buttons are clicked *)
let query t s yesno =
  let _ = query_ans_fun_ptr := yesno in
  let cb = new customized_callbacks_query_window in
  let query_window = new Blastgui_glade_interface.top_query_window cb in
    query_window#query_window#set_title t;
    query_window#query_label#set_text s;
    query_window#query_window#show ()

class customized_callbacks_options_window = object(self)
  (* all the stuff for tweaking the option database will go here *)
  inherit Blastgui_glade_callbacks.default_callbacks 
  method on_options_ok_button_clicked () =
    let _ = self#top_options_window#options_window#destroy () (* hide *) in
      ()
      
  method on_demo_check_button_toggled () = 
    demo_flag := not (!demo_flag)

  method on_options_done_button_clicked () =
  if (!demo_flag) then 
    prerr_endline "demo flag set";
  self#on_options_ok_button_clicked ()

end

let start_trace_window () = 
  let mw = get_main_window () in
  let _ = Gui.reset_trace_window_values () (* tw *) in
    try 
      begin
	Gui.load_files (* tw *) (!src_file_selected) ("error.btr");
	mw#notebook1#goto_page 3;
	prerr_endline "here here here"
      end
    with Gui.Bad_files -> show_message "Error!" "Bad source file!"


      
let show_unsafe () = 
  show_message "Unsafe!" "\n   Error Found!  :-( \n"; 
  start_trace_window ()

let show_stuck () = 
  show_message "Sorry!" " No new predicates found! ";
  start_trace_window ()
  
  
let default_event_list = 
  [(":-)",(fun () -> show_message "Safe!" "  The System is safe!  "),true);
   (":-(", show_unsafe,false);
   ("o new pred", show_stuck, false);
   ("gremlins",(fun () -> show_message "Error!" "  Please check the command line ...  "), true)
  ] 
(* TBD: add a message for badly formed options *)
 
    
class customized_callbacks_file_selection_window = object(self)
  inherit Blastgui_glade_callbacks.default_callbacks
  method on_file_select_ok_button_clicked () =
    let fn = self#top_fileselection#fileselection#get_filename in
    let _ = prerr_endline ("the file is: "^fn^":") in
    let ptr = current_ptr () in
    let _ = ptr := fn in 
    let this_window = get_main_window ()
    in
      self#top_fileselection#fileselection#destroy ();
      match (!file_selected_ptr) with
	  Src -> 
	    begin
	      (this_window)#src_entry#set_text fn;
	      try 
		file_dump_pane ((this_window)#src_text) fn;
		(this_window)#notebook1#goto_page 1;
	      with _ -> show_message "Error" (Printf.sprintf " \n File not found! : \n %s" fn)
	    end
	| Pred -> 
	    begin
	      (this_window)#pred_entry#set_text fn;
	      try 
		file_dump_pane ((this_window)#predicate_text) fn;
		(this_window)#notebook1#goto_page 2;
	      with _ -> show_message "Error" (Printf.sprintf " \n File not found! : \n %s" fn)
	    end
	| Opt -> ()
	| Trace -> ()
	| _ -> failwith "unknown value in file_selected_ptr!"
      (*  TODO: if the !file_selected_ptr == options/trace then take some measures ... *)
      
  method on_file_select_cancel_button_clicked () = 
    self#top_fileselection#fileselection#destroy ()
      
end


class customized_callbacks = object(self)
inherit Blastgui_glade_callbacks.default_callbacks

method on_about2_activate () =
  let s = "\n BLAST! was written by: \n  R. Jhala, R. Majumdar, G. Sutre    \n Copyright etc. etc. 2002-3.  \n" in
  show_message "About BLAST!" s

method on_options1_activate () = 
  let cb = new customized_callbacks_options_window in
  let opt_window = new Blastgui_glade_interface.top_options_window cb in
    (* actually now, in the above, set the various fields according to the present options database *)
  let _ = self#set_options_window opt_window in
  let _ = opt_window#options_window#show () in
    ()

method get_name_of_file title filter ptr = 
  let cb = new customized_callbacks_file_selection_window in
  let fsw = new Blastgui_glade_interface.top_fileselection cb in
  let _ = fsw#fileselection#complete ~filter:filter in
  let _ = fsw#fileselection#set_title title in
  let _ = (file_selected_ptr := ptr) in
      (* now when subsequently required, ! *_file_selected will have the required string *)
  let _ = self#set_fileselection fsw in
  let _ = fsw#fileselection#show () in
    ()

method on_load_source1_activate () = 
  self#get_name_of_file "Select Source File" "*.c" Src

method on_load_pred1_activate () =
  self#get_name_of_file "Select Predicates File" "*.pred" Pred

method on_load_options1_activate () =
  self#get_name_of_file "Select Options File" "*.opt" Opt

method on_load_trace1_activate () = 
  (* actually one has to somehow check the trace file is coherent with the source file ... *)
  if (!src_file_selected = "") then 
    show_message "Error" "\n  Please select a source file first!  \n "
  else
    self#get_name_of_file "Select Trace File" "*.btr" Trace

method on_quit1_activate () = 
  exit 0; ()

method on_file1_activate () = 
  ()

method on_run1_activate () = 
  let show_bogus () = start_trace_window ()
  in   
  let show_new_preds () = 
    let _ = Sys.command "chmod a+rw .newpred" in
    let pl = Misc.string_list_of_file ".newpred" in
    let _ = Sys.command "rm .newpred" in
    let pls = Misc.string_list_cat "\n \n" pl in
    let show_s = Printf.sprintf "\n New Predicates Found! \n %s \n" pls in
      show_message_cont "New Predicates!" show_s chewline;    
  in
    try
      let _cmd = "pblast.opt "^(self#top_window1#entry1#text)^(options_to_string ()) in
      let _ = demo_flag := if (Misc.is_substring _cmd "-demo") then true else false in
      let cmd = Printf.sprintf "echo %s ; %s" _cmd _cmd in
        self#top_window1#notebook1#goto_page 0;
      let new_event_list = 
	match !demo_flag with
	    true -> [("Spurious Counterexample Found!***", show_bogus,false);
		     ("NEW PREDICATES FOUND",show_new_preds,false) ]
	  | false -> []
      in
      let _ = event_list := new_event_list@default_event_list in
	proc_dump_pane cmd
 with Src_File_Undefined -> show_message "Error !" " \n     It helps to have a src file!    \n"

(* run blast with the appropriate options *)

method on_run_button_clicked () = 
  self#on_run1_activate ()

method on_src_button_clicked () = 
  let fn = self#top_window1#src_entry#text in
    try 
      prerr_endline ("loading src :"^fn^":");
      file_dump_pane (self#top_window1#src_text) fn;
      src_file_selected := fn;    
      self#top_window1#notebook1#goto_page 1
    with _ -> show_message "Error" "\n  Invalid Source File!  \n "
  
method on_pred_button_clicked () = 
  let fn = self#top_window1#pred_entry#text in
    try 
      file_dump_pane (self#top_window1#predicate_text) fn;
      pred_file_selected := fn;
      self#top_window1#notebook1#goto_page 2;
    with _ -> show_message "Error" "\n  Invalid Pred File!  \n "

method on_run_command1_activate () = () (* take spl. command line options and then run *)
				       
end

let main () = 
let callbacks = new customized_callbacks in
let window1 = new Blastgui_glade_interface.top_window1 callbacks in

(* twindow hook-in start ...*)
(* first lets make a twindow *)
  let tw_callbacks = new Gui.customized_callbacks in
  let twindow1 = new Project6_glade_interface.top_window1 tw_callbacks in
(* twindow done *)
  let _ = Gui.set_trace_window twindow1 in
  let _ = window1#notebook1#remove_page 3 in
  let trace_label = GMisc.label
		      ~text: "Trace"
		      ~xalign:0.5
		      ~yalign:0.5
		      ~xpad:0
		      ~ypad:0
		      ~line_wrap:false
		      ()
  in
  let _ = window1#notebook1#insert_page ~tab_label:trace_label#coerce ~pos:3 (twindow1#coerce) in
  let _ = window1#notebook1#goto_page 0 in
(* twindow hook-in complete! *)

(* You should probably remove the next line if you want to use the event masks from glade *)
let _ = GtkBase.Widget.add_events window1#window1#as_widget [`ALL_EVENTS] in
let _ = main_window := Some (window1) in
let _ = window1#window1#show() in
Main.main ()

let _ = Printexc.print main ()
