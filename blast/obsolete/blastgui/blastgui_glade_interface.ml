(* THIS IS A GENERATED FILE : DO NOT EDIT ! *)

class top_window1 callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let window1 = GWindow.window
~wm_name: "BLAST!"
~position:`NONE
~kind:`TOPLEVEL
~modal:false
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
~height:680
~width:800
()
in

let _ = window1#add_accel_group accel_group in
let vbox1 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:window1#add
()
in
let menubar1 = GMenu.menu_bar ~packing:(vbox1#pack ~padding:0
~fill:false
~expand:false
)
()
in
let file1 = GMenu.menu_item ~label: "File"
~packing:menubar1#add
()
in
let _ = tooltips#set_tip ~text:"load files" file1#coerce in
let _ = file1#misc#add_accelerator
  ~group:accel_group 
  GdkKeysyms._f
  ~sgn:{ GtkSignal.name = "activate"; 
  GtkSignal.marshaller = GtkSignal.marshal_unit }
  ~modi:[`MOD1;]
in
let file1_menu = GMenu.menu ~packing:file1#set_submenu
()
in
let load_source1 = GMenu.menu_item ~label: "Load Source"
~packing:file1_menu#add
()
in
let _ = tooltips#set_tip ~text:"Load a new source file" load_source1#coerce in
let load_pred1 = GMenu.menu_item ~label: "Load Pred"
~packing:file1_menu#add
()
in
let _ = tooltips#set_tip ~text:"Load a new predicate file" load_pred1#coerce in
let separator3 = GMenu.menu_item ~packing:file1_menu#add
()
in
let quit1 = GMenu.menu_item ~label: "Quit"
~packing:file1_menu#add
()
in
let _ = tooltips#set_tip ~text:"Duh !!" quit1#coerce in
let about2 = GMenu.menu_item ~label: "About"
~packing:menubar1#add
()
in
let _ = about2#right_justify () in
let _ = tooltips#set_tip ~text:"Random Trivia" about2#coerce in
let frame15 = GBin.frame
~packing:(vbox1#pack ~padding:0
~fill:false
~expand:false
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let hbox3 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:frame15#add
()
in
let run_button = GButton.button
~packing:(hbox3#pack ~padding:0
~fill:false
~expand:false
)
~label: "Run"
~border_width:9
()
in
let _ = GtkBase.Widget.set_can_focus run_button#as_widget true in
let vseparator1 = GMisc.separator
`HORIZONTAL~packing:(hbox3#pack ~padding:0
~fill:false
~expand:false
)

()
in
let hbox5 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox3#pack ~padding:0
~fill:true
~expand:true
)
()
in
let label7 = GMisc.label
~text: " Source:  "
~packing:(hbox5#pack ~padding:0
~fill:false
~expand:false
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let src_entry = GEdit.entry
~packing:(hbox5#pack ~padding:0
~fill:true
~expand:true
)
~max_length:40
~visibility:true
~editable:true
()
in
let _ = GtkBase.Widget.set_can_focus src_entry#as_widget true in
let src_button = GButton.button
~packing:(hbox5#pack ~padding:0
~fill:false
~expand:false
)
~label: "Load"
~border_width:9
()
in
let _ = GtkBase.Widget.set_can_focus src_button#as_widget true in
let vseparator2 = GMisc.separator
`HORIZONTAL~packing:(hbox3#pack ~padding:0
~fill:false
~expand:false
)

()
in
let hbox6 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox3#pack ~padding:0
~fill:true
~expand:true
)
()
in
let label11 = GMisc.label
~text: " Preds:  "
~packing:(hbox6#pack ~padding:0
~fill:false
~expand:false
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let pred_entry = GEdit.entry
~packing:(hbox6#pack ~padding:0
~fill:true
~expand:true
)
~max_length:40
~visibility:true
~editable:true
()
in
let _ = GtkBase.Widget.set_can_focus pred_entry#as_widget true in
let pred_button = GButton.button
~packing:(hbox6#pack ~padding:0
~fill:false
~expand:false
)
~label: "Load"
~border_width:9
()
in
let _ = GtkBase.Widget.set_can_focus pred_button#as_widget true in
let frame14 = GBin.frame
~packing:(vbox1#pack ~padding:0
~fill:false
~expand:false
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let hbox4 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:frame14#add
()
in
let command_label = GMisc.label
~text: "Options:  "
~packing:(hbox4#pack ~padding:0
~fill:false
~expand:false
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let entry1 = GEdit.entry
~packing:(hbox4#pack ~padding:0
~fill:true
~expand:true
)
~max_length:0
~visibility:true
~editable:true
()
in
let _ = GtkBase.Widget.set_can_focus entry1#as_widget true in
let notebook1 = GPack.notebook
~tab_pos:`TOP
~tab_border:8
~show_border:true
~scrollable:false
~popup:false
~border_width:5
~packing:(vbox1#pack ~padding:0
~fill:true
~expand:true
)
()
in
let _ = GtkBase.Widget.set_can_focus notebook1#as_widget true in
let scrolledwindow1 = GBin.scrolled_window
~hpolicy:`ALWAYS
~vpolicy:`ALWAYS
()
in
let log_text = GEdit.text
~packing:scrolledwindow1#add
~editable:false
()
in

 let _ = log_text#insert_text ~pos:0 "" in
let _ = GtkBase.Widget.set_can_focus log_text#as_widget true in
let log_pane = GMisc.label
~text: "Log"
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let scrolledwindow2 = GBin.scrolled_window
~hpolicy:`ALWAYS
~vpolicy:`ALWAYS
()
in
let src_text = GEdit.text
~packing:scrolledwindow2#add
~editable:false
()
in

 let _ = src_text#insert_text ~pos:0 "" in
let _ = GtkBase.Widget.set_can_focus src_text#as_widget true in
let source_pane = GMisc.label
~text: "Source"
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let scrolledwindow3 = GBin.scrolled_window
~hpolicy:`ALWAYS
~vpolicy:`ALWAYS
()
in
let predicate_text = GEdit.text
~packing:scrolledwindow3#add
~editable:true
()
in

 let _ = predicate_text#insert_text ~pos:0 "" in
let _ = GtkBase.Widget.set_can_focus predicate_text#as_widget true in
let pred_pane = GMisc.label
~text: "Predicates"
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let vbox12 = GPack.vbox
~spacing:0
~homogeneous:false
()
in
let vbox13 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:(vbox12#pack ~padding:0
~fill:true
~expand:true
)
()
in
let label14 = GMisc.label
~packing:(vbox13#pack ~padding:0
~fill:true
~expand:true
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let hbox10 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(vbox12#pack ~padding:0
~fill:false
~expand:false
)
()
in
let frame19 = GBin.frame
~packing:(hbox10#pack ~padding:0
~fill:false
~expand:true
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let button3 = GButton.button
~packing:frame19#add
~label: "OK"
~border_width:3
~width:41
()
in
let _ = GtkBase.Widget.set_can_focus button3#as_widget true in
let trace_pane = GMisc.label
~text: "Trace"
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let _ =  button3#connect#clicked
	~callback:callbacks#on_about_ok_button_clicked in
let _ =  pred_button#connect#clicked
	~callback:callbacks#on_pred_button_clicked in
let _ =  src_button#connect#clicked
	~callback:callbacks#on_src_button_clicked in
let _ =  run_button#connect#clicked
	~callback:callbacks#on_run_button_clicked in
let _ =  about2#connect#activate
	~callback:callbacks#on_about2_activate in
let _ =  quit1#connect#activate
	~callback:callbacks#on_quit1_activate in
let _ =  load_pred1#connect#activate
	~callback:callbacks#on_load_pred1_activate in
let _ =  load_source1#connect#activate
	~callback:callbacks#on_load_source1_activate in
let _ =  file1#connect#activate
	~callback:callbacks#on_file1_activate in
object(self)
method tooltips = tooltips
method window1 = window1
method accel_group = accel_group
method vbox1 = vbox1
method menubar1 = menubar1
method file1 = file1
method file1_menu = file1_menu
method load_source1 = load_source1
method load_pred1 = load_pred1
method separator3 = separator3
method quit1 = quit1
method about2 = about2
method frame15 = frame15
method hbox3 = hbox3
method run_button = run_button
method vseparator1 = vseparator1
method hbox5 = hbox5
method label7 = label7
method src_entry = src_entry
method src_button = src_button
method vseparator2 = vseparator2
method hbox6 = hbox6
method label11 = label11
method pred_entry = pred_entry
method pred_button = pred_button
method frame14 = frame14
method hbox4 = hbox4
method command_label = command_label
method entry1 = entry1
method notebook1 = notebook1
method scrolledwindow1 = scrolledwindow1
method log_text = log_text
method log_pane = log_pane
method scrolledwindow2 = scrolledwindow2
method src_text = src_text
method source_pane = source_pane
method scrolledwindow3 = scrolledwindow3
method predicate_text = predicate_text
method pred_pane = pred_pane
method vbox12 = vbox12
method vbox13 = vbox13
method label14 = label14
method hbox10 = hbox10
method frame19 = frame19
method button3 = button3
method trace_pane = trace_pane
initializer callbacks#set_window1 self;
notebook1#prepend_page 
~tab_label:trace_pane#coerce vbox12#coerce;
notebook1#prepend_page 
~tab_label:pred_pane#coerce scrolledwindow3#coerce;
notebook1#prepend_page 
~tab_label:source_pane#coerce scrolledwindow2#coerce;
notebook1#prepend_page 
~tab_label:log_pane#coerce scrolledwindow1#coerce;
notebook1#goto_page 0;
end

class top_about_window callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let about_window = GWindow.window
~wm_name: "About BLAST!"
~position:`NONE
~kind:`DIALOG
~modal:true
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
()
in

let _ = about_window#add_accel_group accel_group in
let vbox2 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:about_window#add
()
in
let vbox7 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:(vbox2#pack ~padding:0
~fill:true
~expand:true
)
()
in
let about_label = GMisc.label
~packing:(vbox7#pack ~padding:0
~fill:true
~expand:true
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let hbox7 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(vbox2#pack ~padding:0
~fill:false
~expand:false
)
()
in
let frame16 = GBin.frame
~packing:(hbox7#pack ~padding:0
~fill:false
~expand:true
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let about_ok_button = GButton.button
~packing:frame16#add
~label: "OK"
~border_width:3
~width:41
()
in
let _ = GtkBase.Widget.set_can_focus about_ok_button#as_widget true in
let _ =  about_ok_button#connect#clicked
	~callback:callbacks#on_about_ok_button_clicked in
object(self)
method tooltips = tooltips
method about_window = about_window
method accel_group = accel_group
method vbox2 = vbox2
method vbox7 = vbox7
method about_label = about_label
method hbox7 = hbox7
method frame16 = frame16
method about_ok_button = about_ok_button
initializer callbacks#set_about_window self;
end

class top_options_window callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let options_window = GWindow.window
~wm_name: "Options"
~position:`NONE
~kind:`DIALOG
~modal:true
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
()
in

let _ = options_window#add_accel_group accel_group in
let vbox6 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:options_window#add
()
in
let hbox1 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(vbox6#pack ~padding:0
~fill:true
~expand:true
)
()
in
let frame2 = GBin.frame
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:true
)
~label: "Misc"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let vbox3 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:frame2#add
()
in
let msvc_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "msvc"
~packing:(vbox3#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus msvc_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Parse Microsoft Visual C++ Code" msvc_check_button#coerce in
let nofp_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "nofp"
~packing:(vbox3#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus nofp_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"No function pointers" nofp_check_button#coerce in
let frame6 = GBin.frame
~packing:(vbox3#pack ~padding:15
~fill:true
~expand:true
)
~label: "Main"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let main_entry = GEdit.entry
~packing:frame6#add
~max_length:0
~visibility:true
~editable:true
()
in
let _ = GtkBase.Widget.set_can_focus main_entry#as_widget true in
let _ = tooltips#set_tip ~text:"The initial function from which the code runs" main_entry#coerce in
let frame7 = GBin.frame
~packing:(vbox3#pack ~padding:10
~fill:true
~expand:true
)
~label: "Proof"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let proof_entry = GEdit.entry
~packing:frame7#add
~max_length:0
~visibility:true
~editable:true
()
in
let _ = GtkBase.Widget.set_can_focus proof_entry#as_widget true in
let _ = tooltips#set_tip ~text:"Output proof file: blank indicates no proof" proof_entry#coerce in
let frame3 = GBin.frame
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:true
)
~label: "Abstraction"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let vbox4 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:frame3#add
()
in
let dc_check_button = GButton.check_button
~draw_indicator:true
~active:true
~label: "dc"
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus dc_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Dont Care heuristic" dc_check_button#coerce in
let scope_check_button = GButton.check_button
~draw_indicator:true
~active:true
~label: "scope"
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus scope_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Existential Quantification of predicates not in scope" scope_check_button#coerce in
let frame8 = GBin.frame
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
~label: "Solver"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let solver_option_menu = GMenu.option_menu
~packing:frame8#add
()
in
let _ = GtkBase.Widget.set_can_focus solver_option_menu#as_widget true in
let _ = tooltips#set_tip ~text:"Decision Procedure to be used" solver_option_menu#coerce in
let frame9 = GBin.frame
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
~label: "Pred"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let optionmenu2 = GMenu.option_menu
~packing:frame9#add
()
in
let _ = GtkBase.Widget.set_can_focus optionmenu2#as_widget true in
let _ = tooltips#set_tip ~text:"Predicate Discovery heuristic level" optionmenu2#coerce in
let frame10 = GBin.frame
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
~label: "Post"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let optionmenu3 = GMenu.option_menu
~packing:frame10#add
()
in
let _ = GtkBase.Widget.set_can_focus optionmenu3#as_widget true in
let _ = tooltips#set_tip ~text:"Abstract post image heuristic level" optionmenu3#coerce in
let frame4 = GBin.frame
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:true
)
~label: "Optimizations"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let vbox5 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:frame4#add
()
in
let frame11 = GBin.frame
~packing:(vbox5#pack ~padding:0
~fill:true
~expand:false
)
~label: "Slicing"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let optionmenu4 = GMenu.option_menu
~packing:frame11#add
()
in
let _ = GtkBase.Widget.set_can_focus optionmenu4#as_widget true in
let _ = tooltips#set_tip ~text:"Static Program reduction level" optionmenu4#coerce in
let wpsat_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "wpsat"
~packing:(vbox5#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus wpsat_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Satisfiable conjuncts in wp computation" wpsat_check_button#coerce in
let bddcov_check_button = GButton.check_button
~draw_indicator:true
~active:true
~label: "bddcov"
~packing:(vbox5#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus bddcov_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Use BDDs for cover check" bddcov_check_button#coerce in
let nocache_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "nocache"
~packing:(vbox5#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus nocache_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Don't cache theorem prover calls" nocache_check_button#coerce in
let frame5 = GBin.frame
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:true
)
~label: "Search"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let vbox6 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:frame5#add
()
in
let res_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "restart"
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus res_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"restart from scratch after every counterexample" res_check_button#coerce in
let bfs_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "bfs"
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus bfs_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Do search in BFS" bfs_check_button#coerce in
let reclaim_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "bfs w/ reclaim"
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus reclaim_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Dont keep whole tree around -- restarts from scratch on ctrex" reclaim_check_button#coerce in
let demo_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "demo mode"
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus demo_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Show each bogus counterexample" demo_check_button#coerce in
let stop_check_button = GButton.check_button
~draw_indicator:true
~active:false
~label: "stop"
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus stop_check_button#as_widget true in
let _ = tooltips#set_tip ~text:"Stop after hitting first bogus counterexample" stop_check_button#coerce in
let frame12 = GBin.frame
~packing:(vbox6#pack ~padding:15
~fill:true
~expand:false
)
~label: "Completeness"
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let optionmenu5 = GMenu.option_menu
~packing:frame12#add
()
in
let _ = GtkBase.Widget.set_can_focus optionmenu5#as_widget true in
let _ = tooltips#set_tip ~text:"Keep subtree heuristic" optionmenu5#coerce in
let frame13 = GBin.frame
~packing:(vbox6#pack ~padding:0
~fill:false
~expand:false
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let options_done_button = GButton.button
~packing:frame13#add
~label: "Done"
~border_width:2
()
in
let _ = GtkBase.Widget.set_can_focus options_done_button#as_widget true in
let _ =  options_done_button#connect#clicked
	~callback:callbacks#on_options_done_button_clicked in
let _ =  demo_check_button#connect#toggled
	~callback:callbacks#on_demo_check_button_toggled in
let _ =  scope_check_button#connect#toggled
	~callback:callbacks#on_scope_check_button_toggled in
let _ =  dc_check_button#connect#toggled
	~callback:callbacks#on_dc_check_button_toggled in
let _ =  nofp_check_button#connect#toggled
	~callback:callbacks#on_nofp_check_button_toggled in
let _ =  msvc_check_button#connect#toggled
	~callback:callbacks#on_msvc_check_button_toggled in
object(self)
method tooltips = tooltips
method options_window = options_window
method accel_group = accel_group
method vbox6 = vbox6
method hbox1 = hbox1
method frame2 = frame2
method vbox3 = vbox3
method msvc_check_button = msvc_check_button
method nofp_check_button = nofp_check_button
method frame6 = frame6
method main_entry = main_entry
method frame7 = frame7
method proof_entry = proof_entry
method frame3 = frame3
method vbox4 = vbox4
method dc_check_button = dc_check_button
method scope_check_button = scope_check_button
method frame8 = frame8
method solver_option_menu = solver_option_menu
method frame9 = frame9
method optionmenu2 = optionmenu2
method frame10 = frame10
method optionmenu3 = optionmenu3
method frame4 = frame4
method vbox5 = vbox5
method frame11 = frame11
method optionmenu4 = optionmenu4
method wpsat_check_button = wpsat_check_button
method bddcov_check_button = bddcov_check_button
method nocache_check_button = nocache_check_button
method frame5 = frame5
method vbox6 = vbox6
method res_check_button = res_check_button
method bfs_check_button = bfs_check_button
method reclaim_check_button = reclaim_check_button
method demo_check_button = demo_check_button
method stop_check_button = stop_check_button
method frame12 = frame12
method optionmenu5 = optionmenu5
method frame13 = frame13
method options_done_button = options_done_button
initializer callbacks#set_options_window self;
end

class top_fileselection callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let fileselection = GWindow.file_selection
~wm_name: "Select  File"
~position:`NONE
~modal:true
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
~border_width:10
~fileop_buttons:true
()
in
let file_select_ok_button = fileselection#ok_button in
let _ = GtkBase.Widget.set_can_focus fileselection#ok_button#as_widget true in
let _ = GtkBase.Widget.set_can_default fileselection#ok_button#as_widget true in
let file_select_cancel_button = fileselection#cancel_button in
let _ = GtkBase.Widget.set_can_focus fileselection#cancel_button#as_widget true in
let _ = GtkBase.Widget.set_can_default fileselection#cancel_button#as_widget true in
let _ =  fileselection#cancel_button#connect#clicked
	~callback:callbacks#on_file_select_cancel_button_clicked in
let _ =  fileselection#ok_button#connect#clicked
	~callback:callbacks#on_file_select_ok_button_clicked in
object(self)
method tooltips = tooltips
method fileselection = fileselection
method accel_group = accel_group
method file_select_ok_button = file_select_ok_button
method file_select_cancel_button = file_select_cancel_button
initializer callbacks#set_fileselection self;
end

class top_query_window callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let query_window = GWindow.window
~wm_name: "Question"
~position:`NONE
~kind:`DIALOG
~modal:true
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
()
in

let _ = query_window#add_accel_group accel_group in
let vbox10 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:query_window#add
()
in
let vbox11 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:(vbox10#pack ~padding:0
~fill:true
~expand:true
)
()
in
let query_label = GMisc.label
~packing:(vbox11#pack ~padding:0
~fill:true
~expand:true
)
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
~line_wrap:false
()
in
let hbox9 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(vbox10#pack ~padding:0
~fill:false
~expand:false
)
()
in
let frame18 = GBin.frame
~packing:(hbox9#pack ~padding:0
~fill:false
~expand:true
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let yes_button = GButton.button
~packing:frame18#add
~label: "Yes"
~border_width:3
~width:41
()
in
let _ = GtkBase.Widget.set_can_focus yes_button#as_widget true in
let frame20 = GBin.frame
~packing:(hbox9#pack ~padding:0
~fill:true
~expand:true
)
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let no_button = GButton.button
~packing:frame20#add
~label: "No"
~border_width:3
~width:41
()
in
let _ = GtkBase.Widget.set_can_focus no_button#as_widget true in
let _ =  no_button#connect#clicked
	~callback:callbacks#on_no_button_clicked in
object(self)
method tooltips = tooltips
method query_window = query_window
method accel_group = accel_group
method vbox10 = vbox10
method vbox11 = vbox11
method query_label = query_label
method hbox9 = hbox9
method frame18 = frame18
method yes_button = yes_button
method frame20 = frame20
method no_button = no_button
initializer callbacks#set_query_window self;
end
