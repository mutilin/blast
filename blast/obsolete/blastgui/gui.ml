(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)


(* This file has the GUI stuff which shall be filled in by and by ...*)

(*
module Predicate = Ast.Predicate
 module C_System_Descr = BlastCSystemDescr.C_System_Descr
module C_Command = BlastCSystemDescr.C_Command
module This_C_Abstraction = Abstraction.Make_C_Abstraction(C_System_Descr)
module Operation = This_C_Abstraction.Operation
module Region = This_C_Abstraction.Region
*)

(* from here on the gui stuff starts *)

type table_data = (* Pred of Predicate.predicate 
		  | Op of Operation.t *)
		  Junk of unit
		  (* fill in whatever else you want to have *)

(* some constants  *)
let grey: GDraw.color = (* GDraw.color *) (`NAME "cyan")
let white: GDraw.color = (* GDraw.color *) (`WHITE) 

(* initial values of various state variables *)

let id_table = Hashtbl.create 31 (* this table is : id -> (gui-obj,thing) *)
let key_table = Hashtbl.create 31 (* id -> (pred list : id list,stack string : id) *)
let posn_id_table:((int,int) Hashtbl.t) = Hashtbl.create 31 (* position -> id : for the elts of op_list *)
let key_id_list = ref None
let posn = ref 0
let maxpos = ref 0 
let highlight_reg = ref (-1,-1)
let ss = ref "" 
let cursor_array_ref = ref (Array.create 1 (-1,-1))
let region_array_ref = ref (Array.create 1 ([]:string list))
let chosen_pred_ref = ref None
let exit_fn_ptr = ref (fun () -> exit 0)

let add_data lbl thing = 
  let item = GList.list_item ~label: lbl () in
  let id = item#get_id in
  let _ = Hashtbl.add id_table id (item,thing,lbl) in
    id
	
let get_id_obj id = Misc.fst3 (Hashtbl.find id_table id)


let init_data (srcfile,fname) =
  let inchan = open_in_bin fname in
  let (b,op_l,reg_l) = Marshal.from_channel inchan in 
  let _ = close_in inchan in
  let _ = print_string (Misc.bool_to_string b) 
  in
    if (op_l = []) then failwith ("Bad trace : too short !") 
    else
	let process_reg (pred_string_l,stack_string) = 
	  (List.map (fun x -> add_data x (Junk ())) pred_string_l , add_data stack_string (Junk ()))
	in
	let process_op (l1,l2,op) reg = (* this processes both an op and its "post" region *)
	  let op_id = add_data op (Junk ()) in
	  let _ = Hashtbl.add key_table op_id ((l1,l2),(process_reg reg)) in
	    op_id
	in
	  (* adding cursor array stuff *)
	let _ = 
	  cursor_array_ref := 
	  begin
	    let lines = List.map (fun x -> x^"\n") (Misc.string_list_of_file srcfile) in
	    let cumulative_add l = 
	      List.rev (List.fold_left (fun x y -> (y + (List.hd x))::x) [0] l) 	
	    in
	    let cumul_len_l = cumulative_add (List.map String.length lines) in
	      ss := String.concat "" lines;
	      Array.init (List.length cumul_len_l - 1) (fun x -> (List.nth cumul_len_l x, List.nth cumul_len_l (x+1)))  
	  end
	in
	    (* adding initial location *)
	let init_id = add_data "INIT" (Junk ()) in
	let _ = Hashtbl.add key_table init_id ((-1,-1),(process_reg (List.hd reg_l))) in
	let rv = init_id::(List.map2 process_op op_l (List.tl reg_l)) in
	let _ = region_array_ref := Array.init (List.length reg_l) (fun x -> fst (List.nth reg_l x)) in
	let _ = maxpos := List.length rv in
	  key_id_list := Some(rv);
	  rv

(* starting from i ... find the first place where the function f becomes true in the region_array *)

let first_fwd arr f  start = 
  let l = Array.length arr in
  let i = ref (start+1) in
  let found = ref false in
    while (not !found && !i < l) do
     	if f (arr.(!i)) then found := true
	else i := !i + 1
    done;
    if (!found) then (!i - 1) else start
(* can also just leave the above as !i *)

let first_back arr f  start = 
  let l = Array.length arr in
  let i = ref (start-1) in
  let found = ref false in
    while (not !found && !i >= 0) do
     	if f (arr.(!i)) then found := true
	else i := !i - 1
    done;
    if (!found) then (!i + 1) else start
(* can also make this !i + 1 ... or just !i *)

(* we shall assume that the trace supplied just has single cubes per state *)

(* this produces a (string (aka pred) list, stack info string *)

let read_and_print_trace fname =
  let inchan = open_in_bin fname in
  let (b,op_l,reg_l) = Marshal.from_channel inchan in 
  let _ = close_in inchan in
  let _ = print_string (Misc.bool_to_string b) in
  let _ = List.iter 
	    (fun (x,y) -> Printf.printf "reg: And [%s] \n stack: %s \n"  (Misc.strList x) y) 
	    reg_l
  in
  let _ = List.iter  (fun x -> Printf.printf "%s \n" x) op_l in
  let _ = Printf.printf "length = %d" (List.length op_l) in
  let _ = print_string "hello LUNAA! \n" in
    ()
  

(* THIS FILE WILL NEVER BE OVERWRITTEN.
Use it as a template for your own main module.*)
open GMain

class browser =
object(self)
  val mutable browser_front = -1
  val mutable browser_current = -1
  val mutable browser_start = -1
  val browser_array = Array.create 100 0 

  val ptr_inc = fun p -> if (p = 99) then 0 else (p + 1)
  val ptr_dec = fun p -> if (p = 0) then 99 else (p - 1)
 	
  method reset () = 
    browser_front <- -1;
    browser_current <- -1;
    browser_start <- -1;
    Array.fill browser_array 0 100 0;
    ()

  method back () = 
    if (browser_current = browser_start) 
    then None
    else 
      begin
	browser_current <- ptr_dec browser_current;
	Some (browser_array.(browser_current))
      end

  method fwd () =  
    if (browser_current = browser_front) 
    then None
    else 
      begin
	browser_current <- ptr_inc browser_current;
	Some (browser_array.(browser_current))
      end

  method new_fwd id = 
    let next_pos = ptr_inc browser_current in
      browser_current <- next_pos;
      browser_front <- next_pos;
      Array.set browser_array browser_current id;
      if browser_start = browser_current then browser_start <- browser_start + 1;
      ()
      
  
end 

let my_browser = new browser 

class customized_callbacks = object(self)
inherit Project6_glade_callbacks.default_callbacks

val mutable browser_update_flag = true
(*
method setup_op_list () = 
  let l = self#top_window1#op_list#children in
  List.iter (fun x -> 
	       Hashtbl.add posn_id_table 
	       (self#top_window1#op_list#child_position x) (x#get_id)) l
*)
  
method unhighlight =
    if (not (!highlight_reg = (-1,-1))) then
    begin
    	 let (start,stop) = !highlight_reg in
   	 let (st : string) = self#top_window1#text1#get_chars ~start:start ~stop:stop in
	 let textpane = self#top_window1#text1 in
	 textpane#delete_text ~start:start ~stop:stop;
	 textpane#set_point start;
	 textpane#insert ~background:white  st; 
	     ()
    end

method highlight start stop = 
  let textpane = self#top_window1#text1 in
  let _ = self#unhighlight in
  let _ = prerr_endline "about to get chars" in
    try
      let st = textpane#get_chars ~start:start ~stop:stop in
      let _ = prerr_endline "done get_chars" in 
	textpane#delete_text ~start:start ~stop:stop; 
	textpane#set_point start;
	textpane#insert  ~background: grey  (st); 
	highlight_reg := (start,stop) 
    with
	_ -> 
	  begin
	    prerr_endline (Printf.sprintf "start %d stop %d" start stop);
	    highlight_reg := (-1,-1)
	  end

method on_quit_button_clicked () = 
  (!exit_fn_ptr) ()

method on_fwd_button_clicked () = 
  if (!posn < !maxpos) then 
    begin
      posn := !posn + 1; 
      self#top_window1#op_list#select_item !posn
    end
  else
    ()

method on_back_button_clicked () = 
  if (!posn > 0) then 
    begin 
      posn := !posn - 1;
      self#top_window1#op_list#select_item !posn
    end
  else ()

method on_pred_back_button_clicked () = 
  match (!chosen_pred_ref) with
      None -> ()
    | Some (s) -> 
	begin
	  let find_fun = (fun x -> not (List.mem s x)) in
	  let new_posn = first_back (!region_array_ref) find_fun (!posn) in
	    self#top_window1#op_list#select_item new_posn
	end
					  
method on_pred_fwd_button_clicked () =
 match (!chosen_pred_ref) with
      None -> ()
    | Some (s) -> 
	begin
	  let find_fun = (fun x -> not (List.mem s x)) in
	  let new_posn = first_fwd (!region_array_ref) find_fun (!posn) in
	    self#top_window1#op_list#select_item new_posn
	end

method on_ff_button_clicked () = 
  let curr_reg = (!region_array_ref).(!posn) in
  let find_fun = (fun x -> not (x = curr_reg)) in
  let new_posn = first_fwd (!region_array_ref) find_fun (!posn) in
    self#top_window1#op_list#select_item new_posn
				   
				   
method on_rew_button_clicked () = 
  let curr_reg = (!region_array_ref).(!posn) in
  let find_fun = (fun x -> not (x = curr_reg)) in
  let new_posn = first_back (!region_array_ref) find_fun (!posn) in
    self#top_window1#op_list#select_item new_posn

method on_browser_back_button_clicked () = 
  match (my_browser#back ()) with
      None -> ()
    | Some i -> 
	begin
	  browser_update_flag <- false;
	  self#top_window1#op_list#select_item i;
	  browser_update_flag <- true
	end
			   
method on_browser_fwd_button_clicked () = 
  match (my_browser#fwd ()) with
      None -> ()
    | Some i -> 
	begin
	  browser_update_flag <- false;
	  self#top_window1#op_list#select_item i;
	  browser_update_flag <- true
    end
	 

method on_op_list_select_child (list_elem : GList.list_item) = 
       begin
       	    let id = list_elem#get_id in
	    let _ = (posn := self#top_window1#op_list#child_position list_elem) in
	    let _ = if (browser_update_flag) then my_browser#new_fwd (!posn) else () in	   
	    let ((sta_l,sto_l),(pred_id_l,stack_id)) = Hashtbl.find key_table id in
	    let (start,stop) = 
	      match (sta_l,sto_l) with
		  (-1,-1) -> (0,0)
		| (-1,_) -> (!cursor_array_ref).(sto_l - 1) 
		| (_,-1) -> (!cursor_array_ref).(sta_l - 1)
		| (_,_) -> (!cursor_array_ref).(sta_l - 1)
(*(fst ((!cursor_array_ref).(sta_l -1)), (fst ((!cursor_array_ref).(sto_l -1))) -1 )*) 
	    in
	    let (pred_obj_l, stack_obj) = (List.map get_id_obj pred_id_l, get_id_obj stack_id) in
	    let pred_list_pane = self#top_window1#pred_list in
	    let _ = List.iter (pred_list_pane#remove) pred_list_pane#children in
	    let _ = chosen_pred_ref := None in
	    let _ = List.iter pred_list_pane#append pred_obj_l in
	    let _ = List.iter (self#top_window1#stack_list#remove) (self#top_window1#stack_list#children) in
	    let _ = self#top_window1#stack_list#append stack_obj in	    
	    let _ = self#highlight start stop in
	    ()
       (* print_string "this is also the end \n"; exit 0; () *)
       end

method on_pred_list_select_child (list_elem : GList.list_item) = 
  let s = Misc.thd3 (Hashtbl.find id_table list_elem#get_id) in
  (* let _ = Printf.printf "Chosen pred: %s \n" s in *)
  chosen_pred_ref := Some s
  (* Printf.printf "Still to implement! pred_list_select\n"; () *)

method on_stack_list_select_child (list_elem : GList.list_item) = 
  Printf.printf "Still to implement ! list9_select \n"; ()


end


exception Bad_files

let trace_window = ref None 
let set_trace_window (w: Project6_glade_interface.top_window1) = 
  trace_window := Some w

let reset_trace_window_values () =
  let window1 = match !trace_window with None -> failwith "Set trace window !" | Some w -> w
  in
  (* will zero out everything and clean out all the panes *)
  (* first clean the hashtables *)
  Hashtbl.clear id_table; 
  Hashtbl.clear key_table; 
  Hashtbl.clear posn_id_table;
  (* now reset various state variables *)
  key_id_list := None;
  posn := 0;
  maxpos := 0;
  highlight_reg := (-1,-1);
  ss := "";
  cursor_array_ref := (Array.create 1 (-1,-1));
  region_array_ref := (Array.create 1 ([]:string list));
  chosen_pred_ref := None;
  (* reset the browser *)
  my_browser#reset ();
  (* now to clean out the windows *)
  (* window1#text1 ... *)
  let l = window1#text1#length in
  let () = window1#text1#delete_text 0 l in
  let clean_list_pane lp =   
    let c = lp#children in
      List.iter (lp#remove) c 
  in
    List.iter clean_list_pane 
      [window1#op_list;window1#pred_list;window1#stack_list];
    ()
  
let load_files src_file trace_file = 
  let window1 = 
    match !trace_window with None -> failwith "Set trace window !" | Some w -> w
  in
  (* assumes that src_file and trace_file exist so ... *)
  let op_id_l = 
    try init_data (src_file, trace_file) 
    with _ -> raise Bad_files
  in
    List.iter (window1#op_list#append) (List.map get_id_obj op_id_l);
    let tpane : GEdit.text = window1#text1 in    
      tpane#insert ~background: white  !ss;
      tpane#set_word_wrap false;
      ()

(*
let gui_main fname = 
  Printf.printf "inside gui_main"; 
  read_and_print_trace fname; 
  ()

let main () = 
  let callbacks = new customized_callbacks in
  let window1 = new Project6_glade_interface.top_window1 callbacks in
    (* You should probably remove the next line if you want to use the event masks from glade *)
  let _ = GtkBase.Widget.add_events window1#window1#as_widget [`ALL_EVENTS] in
  let _ = window1#window1#show() in
  (*let _ = Options.buildOptionsDatabase () in*)
  let trace_file = Options.getValueOfString "tracefile" in
  let src_file = Options.getValueOfString "file" in
  let _ = if (src_file = "") then failwith "Please specify the sourcefile with -file srcfile" in    
  let op_id_l =  init_data (src_file, trace_file) in
  let _ = List.iter (window1#op_list#append) (List.map get_id_obj op_id_l) in
  (* let _ = window1#setup_op_list () in *)
  let _ = window1#text1#insert ~background: white  !ss in 
  let _ = window1#text1#set_word_wrap false in
  
    Main.main ()
      
let _ = Printexc.catch main ()
*)		  
