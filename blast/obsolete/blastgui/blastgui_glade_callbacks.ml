(* THIS IS A GENERATED FILE. DO NOT EDIT.*)
class default_callbacks = object(self)
val mutable top_query_window_ = (None : Blastgui_glade_interface.top_query_window option)
method top_query_window =
    match top_query_window_ with
      | None -> assert false
      | Some c -> c
method set_query_window c = top_query_window_ <- Some c
val mutable top_fileselection_ = (None : Blastgui_glade_interface.top_fileselection option)
method top_fileselection =
    match top_fileselection_ with
      | None -> assert false
      | Some c -> c
method set_fileselection c = top_fileselection_ <- Some c
val mutable top_options_window_ = (None : Blastgui_glade_interface.top_options_window option)
method top_options_window =
    match top_options_window_ with
      | None -> assert false
      | Some c -> c
method set_options_window c = top_options_window_ <- Some c
val mutable top_about_window_ = (None : Blastgui_glade_interface.top_about_window option)
method top_about_window =
    match top_about_window_ with
      | None -> assert false
      | Some c -> c
method set_about_window c = top_about_window_ <- Some c
val mutable top_window1_ = (None : Blastgui_glade_interface.top_window1 option)
method top_window1 =
    match top_window1_ with
      | None -> assert false
      | Some c -> c
method set_window1 c = top_window1_ <- Some c
method on_no_button_clicked () =
 prerr_endline "Event clicked from no_button";()

method on_file_select_cancel_button_clicked () =
 prerr_endline "Event clicked from file_select_cancel_button";()

method on_file_select_ok_button_clicked () =
 prerr_endline "Event clicked from file_select_ok_button";()

method on_options_done_button_clicked () =
 prerr_endline "Event clicked from options_done_button";()

method on_demo_check_button_toggled () =
 prerr_endline "Event toggled from demo_check_button";()

method on_scope_check_button_toggled () =
 prerr_endline "Event toggled from scope_check_button";()

method on_dc_check_button_toggled () =
 prerr_endline "Event toggled from dc_check_button";()

method on_nofp_check_button_toggled () =
 prerr_endline "Event toggled from nofp_check_button";()

method on_msvc_check_button_toggled () =
 prerr_endline "Event toggled from msvc_check_button";()

method on_about_ok_button_clicked () =
 prerr_endline "Event clicked from about_ok_button";()

method on_about_ok_button_clicked () =
 prerr_endline "Event clicked from button3";()

method on_pred_button_clicked () =
 prerr_endline "Event clicked from pred_button";()

method on_src_button_clicked () =
 prerr_endline "Event clicked from src_button";()

method on_run_button_clicked () =
 prerr_endline "Event clicked from run_button";()

method on_about2_activate () =
 prerr_endline "Event activate from about2";() 

method on_quit1_activate () =
 prerr_endline "Event activate from quit1";() 

method on_load_pred1_activate () =
 prerr_endline "Event activate from load_pred1";() 

method on_load_source1_activate () =
 prerr_endline "Event activate from load_source1";() 

method on_file1_activate () =
 prerr_endline "Event activate from file1";() 

end
