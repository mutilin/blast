(* THIS IS A GENERATED FILE : DO NOT EDIT ! *)

class top_window1 callbacks =
let blast_home = try Unix.getenv "BLASTHOME" with Not_found -> failwith
"BLASTHOME not set !" in
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let window1 = GWindow.window
~wm_name: "BLAST Trace Viewer"
~position:`NONE
~kind:`TOPLEVEL
~modal:false
~allow_shrink:false
~allow_grow:true
~auto_shrink:false
()
in

let _ = window1#add_accel_group accel_group in
let vbox3 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:window1#add
()
in
let menubar1 = GMenu.menu_bar ~packing:(vbox3#pack ~padding:0
~fill:false ~expand:false
)
()
in
let file1 = GMenu.menu_item ~label: "File"
~packing:menubar1#add
()
in
let vbox4 = GPack.vbox
~spacing:0
~homogeneous:false
~packing:(vbox3#pack ~padding:0
~fill:true
~expand:true
)
()
in
let hpaned1 = GPack.paned
 `HORIZONTAL~handle_size:10
~packing:(vbox4#pack ~padding:0
~fill:true
~expand:true
)
()
in
let hpaned2 = GPack.paned
 `HORIZONTAL~handle_size:10
~packing:(hpaned1#add )
()
in
let scrolledwindow4 = GBin.scrolled_window
~hpolicy:`NEVER
~vpolicy:`ALWAYS
~packing:(hpaned2#add )
()
in
let text1 = GEdit.text
~packing:scrolledwindow4#add
~editable:false
()
in

 let _ = text1#insert_text ~pos:0 "" in
let _ = GtkBase.Widget.set_can_focus text1#as_widget true in
let scrolledwindow2 = GBin.scrolled_window
~hpolicy:`ALWAYS
~vpolicy:`ALWAYS
~width:120
~packing:(hpaned2#add )
()
in
let viewport2 = GBin.viewport ~shadow_type:`IN
~packing:scrolledwindow2#add
()
in
let op_list = GList.liste
~selection_mode:`SINGLE
~packing:viewport2#add
()
in
let vpaned1 = GPack.paned
 `VERTICAL~handle_size:10
~packing:(hpaned1#add )
()
in
let scrolledwindow3 = GBin.scrolled_window
~hpolicy:`ALWAYS
~vpolicy:`ALWAYS
~packing:(vpaned1#add )
()
in
let viewport3 = GBin.viewport ~shadow_type:`IN
~packing:scrolledwindow3#add
()
in
let pred_list = GList.liste
~selection_mode:`SINGLE
~packing:viewport3#add
()
in
let frame6 = GBin.frame
~packing:(vpaned1#add )
~label_xalign:0.
~shadow_type:`ETCHED_IN
()
in
let stack_list = GList.liste
~selection_mode:`SINGLE
~packing:frame6#add
()
in
let hbox1 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(vbox4#pack ~padding:0
~fill:false
~expand:false
)
()
in
let hbox9 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox1#pack ~padding:0
~fill:false
~expand:false
)
()
in
let back_button = GButton.button
~packing:(hbox9#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus back_button#as_widget true in
let pixmap16 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~window:window1 ~file:(blast_home^"/pixmaps/left-arrow.xpm") ())~packing:back_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let fwd_button = GButton.button
~packing:(hbox9#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus fwd_button#as_widget true in
let pixmap17 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/right-arrow.xpm") ())~packing:fwd_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let vseparator3 = GMisc.separator
`HORIZONTAL~packing:(hbox1#pack ~padding:10
~fill:false
~expand:false
)

()
in
let hbox8 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox1#pack ~padding:0
~fill:false
~expand:false
)
()
in
let browser_back_button = GButton.button
~packing:(hbox8#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus browser_back_button#as_widget true in
let pixmap14 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/undo-arrow.xpm") ())~packing:browser_back_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let browser_fwd_button = GButton.button
~packing:(hbox8#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus browser_fwd_button#as_widget true in
let pixmap15 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/redo-arrow.xpm") ())~packing:browser_fwd_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let vseparator4 = GMisc.separator
`HORIZONTAL~packing:(hbox1#pack ~padding:10
~fill:false
~expand:false
)

()
in
let hbox7 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox1#pack ~padding:0
~fill:false
~expand:false
)
()
in
let rew_button = GButton.button
~packing:(hbox7#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus rew_button#as_widget true in
let pixmap12 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/rew-arrow.xpm") ())~packing:rew_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let ff_button = GButton.button
~packing:(hbox7#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus ff_button#as_widget true in
let pixmap13 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/ff-arrow.xpm") ())~packing:ff_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let vseparator5 = GMisc.separator
`HORIZONTAL~packing:(hbox1#pack ~padding:10
~fill:false
~expand:false
)

()
in
let hbox3 = GPack.hbox
~spacing:0
~homogeneous:false
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:false
)
()
in
let pred_back_button = GButton.button
~packing:(hbox3#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus pred_back_button#as_widget true in
let pixmap4 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/left-reg-arrow.xpm") ())~packing:pred_back_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let pred_fwd_button = GButton.button
~packing:(hbox3#pack ~padding:0
~fill:false
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus pred_fwd_button#as_widget true in
let pixmap5 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/right-reg-arrow.xpm") ())~packing:pred_fwd_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let vseparator6 = GMisc.separator
`HORIZONTAL~packing:(hbox1#pack ~padding:25
~fill:false
~expand:false
)

()
in
let quit_button = GButton.button
~packing:(hbox1#pack ~padding:0
~fill:true
~expand:false
)
()
in
let _ = GtkBase.Widget.set_can_focus quit_button#as_widget true in
let pixmap3 = GMisc.pixmap
(GDraw.pixmap_from_xpm ~file:(blast_home^"/pixmaps/quit.xpm") ())~packing:quit_button#add
~xalign:0.5
~yalign:0.5
~xpad:0
~ypad:0
()
in
let _ =  quit_button#connect#clicked
	~callback:callbacks#on_quit_button_clicked in
let _ =  pred_fwd_button#connect#clicked
	~callback:callbacks#on_pred_fwd_button_clicked in
let _ =  pred_back_button#connect#clicked
	~callback:callbacks#on_pred_back_button_clicked in
let _ =  ff_button#connect#clicked
	~callback:callbacks#on_ff_button_clicked in
let _ =  rew_button#connect#clicked
	~callback:callbacks#on_rew_button_clicked in
let _ =  browser_fwd_button#connect#clicked
	~callback:callbacks#on_browser_fwd_button_clicked in
let _ =  browser_back_button#connect#clicked
	~callback:callbacks#on_browser_back_button_clicked in
let _ =  fwd_button#connect#clicked
	~callback:callbacks#on_fwd_button_clicked in
let _ =  back_button#connect#clicked
	~callback:callbacks#on_back_button_clicked in
let _ =  file1#connect#activate
	~callback:callbacks#on_file1_activate in
let _ = op_list#connect#select_child
	~callback:callbacks#on_op_list_select_child in
let _ = pred_list#connect#select_child
	~callback:callbacks#on_pred_list_select_child in
object(self)
method tooltips = tooltips
method window1 = window1
method accel_group = accel_group
method vbox3 = vbox3
method menubar1 = menubar1
method file1 = file1
method vbox4 = vbox4
method hpaned1 = hpaned1
method hpaned2 = hpaned2
method scrolledwindow4 = scrolledwindow4
method text1 = text1
method scrolledwindow2 = scrolledwindow2
method viewport2 = viewport2
method op_list = op_list
method vpaned1 = vpaned1
method scrolledwindow3 = scrolledwindow3
method viewport3 = viewport3
method pred_list = pred_list
method frame6 = frame6
method stack_list = stack_list
method hbox1 = hbox1
method hbox9 = hbox9
method back_button = back_button
method pixmap16 = pixmap16
method fwd_button = fwd_button
method pixmap17 = pixmap17
method vseparator3 = vseparator3
method hbox8 = hbox8
method browser_back_button = browser_back_button
method pixmap14 = pixmap14
method browser_fwd_button = browser_fwd_button
method pixmap15 = pixmap15
method vseparator4 = vseparator4
method hbox7 = hbox7
method rew_button = rew_button
method pixmap12 = pixmap12
method ff_button = ff_button
method pixmap13 = pixmap13
method vseparator5 = vseparator5
method hbox3 = hbox3
method pred_back_button = pred_back_button
method pixmap4 = pixmap4
method pred_fwd_button = pred_fwd_button
method pixmap5 = pixmap5
method vseparator6 = vseparator6
method quit_button = quit_button
method pixmap3 = pixmap3
initializer callbacks#set_window1 self;
end

class top_menu1 callbacks =
let tooltips = GData.tooltips () in
let accel_group = GtkData.AccelGroup.create () in
let menu1 = GMenu.menu
()
in
object(self)
method tooltips = tooltips
method menu1 = menu1
method accel_group = accel_group
initializer callbacks#set_menu1 self;
end
