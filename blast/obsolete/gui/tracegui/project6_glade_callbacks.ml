(* THIS IS A GENERATED FILE. DO NOT EDIT.*)
class default_callbacks = object(self)
val mutable top_menu1_ = (None : Project6_glade_interface.top_menu1 option)
method top_menu1 =
    match top_menu1_ with
      | None -> assert false
      | Some c -> c
method set_menu1 c = top_menu1_ <- Some c
val mutable top_window1_ = (None : Project6_glade_interface.top_window1 option)
method top_window1 =
    match top_window1_ with
      | None -> assert false
      | Some c -> c
method set_window1 c = top_window1_ <- Some c
method on_quit_button_clicked () =
 prerr_endline "Event clicked from quit_button";()

method on_pred_fwd_button_clicked () =
 prerr_endline "Event clicked from pred_fwd_button";()

method on_pred_back_button_clicked () =
 prerr_endline "Event clicked from pred_back_button";()

method on_ff_button_clicked () =
 prerr_endline "Event clicked from ff_button";()

method on_rew_button_clicked () =
 prerr_endline "Event clicked from rew_button";()

method on_browser_fwd_button_clicked () =
 prerr_endline "Event clicked from browser_fwd_button";()

method on_browser_back_button_clicked () =
 prerr_endline "Event clicked from browser_back_button";()

method on_fwd_button_clicked () =
 prerr_endline "Event clicked from fwd_button";()

method on_back_button_clicked () =
 prerr_endline "Event clicked from back_button";()

method on_file1_activate () =
 prerr_endline "Event activate from file1";() 

end
