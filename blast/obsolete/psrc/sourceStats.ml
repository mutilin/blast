open Cil

let instloc = function
    Set (_, _, loc)
  | Call (_, _, _, loc)
  | Asm (_, _, _, _, _, loc) -> loc

let rec stmtlocs stmt =
  match stmt.skind with
    Instr insts -> List.map instloc insts
  | Return (_, loc)
  | Goto (_, loc)
  | Break loc
  | Continue loc
  | If (_, _, _, loc)
  | Switch (_, _, _, loc)
  | Loop (_, loc) -> [loc]
  | Block {bstmts = stms} -> List.concat (List.map stmtlocs stms)

class typeVisitor func =
  object
    inherit nopCilVisitor

    method vtype typ : typ visitAction =
      (match typ with
	TNamed (ti, _) -> Format.printf "TYPE_ACCESS\t\"%s\"\t\"%s\"\n" func ti.tname
      | TComp (ci, _) -> Format.printf "TYPE_ACCESS\t\"%s\"\t\"%s\"\n" func ci.cname
      | _ -> ());
      DoChildren
  end

class statsVisitor =
  let func = ref "<NONE>" in
  let curLoc = ref locUnknown in
  object
    inherit nopCilVisitor
      
    method vfunc fundec : fundec visitAction  =
      func := fundec.svar.vname;

      let vis = new typeVisitor !func in
      let visitType ty = ignore (visitCilType vis ty) in
      let visitVar vi = visitType vi.vtype in

      visitVar fundec.svar;
      List.iter visitVar fundec.slocals;

      DoChildren

    method vstmt stmt : stmt visitAction =
      let visitLoc loc = Format.printf "LOC_FUNC\t\"%s\"\t%d\t\"%s\"\n" loc.file loc.line !func in
      let locs = stmtlocs stmt in
      List.iter visitLoc locs;
      (match locs with
	[] -> ()
      | h::_ -> curLoc := h);

      let visitLabel = function
	  Label (name, loc, true) -> Format.printf "LOC_LABEL\t\"%s\"\t%d\t\"%s\"\n" loc.file loc.line name
	| _ -> () in
      List.iter visitLabel stmt.labels;

      DoChildren

    method vinst instr : instr list visitAction =
      (match instr with
	Set (lv, _, loc)
      | Call (Some lv, _, _, loc) ->
	  (match lv with
	    Var vi, _ -> Format.printf "LOC_LHSVAR\t\"%s\"\t%d\t\"%s\"\n" loc.file loc.line vi.vname
	  | _ -> ());
	  curLoc := loc
      | _ -> ());

      (match instr with
	Call (_, Lval (Var vi, _), _, _) ->
	  Format.printf "CALL\t\"%s\"\t\"%s\"\n" !func vi.vname
      | _ -> ());

      DoChildren

    method vexpr exp : exp visitAction =
      (match exp with
	Lval (Var vi, _) -> Format.printf "LOC_RHSVAR\t\"%s\"\t%d\t\"%s\"\n" !curLoc.file !curLoc.line vi.vname
      | _ -> ());
      DoChildren
  end

let printStats file = ignore (visitCilFile (new statsVisitor) file)

let printLines fname =
  let inf = open_in fname in
  let rec gen n =
    let line =
      try
	Some (input_line inf)
      with End_of_file -> None in
    match line with
      None -> ()
    | Some line ->
	Printf.printf "LOC_TEXT \"%s\" %d \"%s\"\n" fname n (String.escaped line);
	gen (n+1) in
  gen 1;
  close_in inf
      
