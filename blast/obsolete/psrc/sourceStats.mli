val printStats : Cil.file -> unit
(* Print CrocoPat relations summarizing properties of the file *)

val printLines : string -> unit
(* Convert the file into a set of CrocoPat relations associating line numbers with contents *)
  
