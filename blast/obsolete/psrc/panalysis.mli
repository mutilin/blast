(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)

(** Various utilities to get information out of a Cil file, plus some program
	slicing routines.
*)

val getVars : string -> Cil.file -> Cil.varinfo list
val getVarsOfType : Cil.typ -> Cil.file -> Cil.varinfo list

(** Find all variables of a particular typedef name (string).
Returns an empty list if there is no typedef with that name.
*)
val getVarsOfTypedef : string -> Cil.file -> Cil.varinfo list

(** Given a varinfo, return true if it is global. *)
val isGlobal : Cil.varinfo -> bool

(** given a string, see if the variable of the same name
	is a global. May raise Not_found if there is no
	variable of that name2
*)
val isGlobalString : string -> Cil.file -> bool

(** Return a list of global variables *)
val findGlobalVars : Cil.file -> Cil.varinfo list

val slice : Cil.file -> Cil.file 
val t_simplify : Cil.file -> string list -> unit
val spec_slice : Cil.file list -> Ast.Predicate.predicate -> unit



