(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)

(* This module will do some program analysis on the source program.
   The optimizations we want are:
   1. Constant propagation and constant folding.
   2. Partial evaluation based on constant folding.
   3. def/use and live vars / live preds ?
   4. Build a static call graph and get rid of extraneous conditionals.
      Slicing?
*)
open Cil
open Pretty
open Clist

open Ast
module E = Errormsg
module H = Hashtbl

(** Useful functions on the Cil file *)

(* Given a string, find the varinfo structure corresponding to that
   variable name (ie with the same vname) 

   Since, in the files we consider, all variables have unique names,
   the list vinfoL will have exactly one member. 

   Returns an empty list if there is no variable with that name.
*)
    let vinfoL = ref []
	
    class getVarinfoClass str = object
      inherit nopCilVisitor
	  
	  
      method vvdec vardec =
	if (vardec.vname = str) then
	  vinfoL := vardec :: !vinfoL
	else ();
	DoChildren
	  
    end

    let getVars str file =
      let ob = new getVarinfoClass str in
      let _ = visitCilFile ob file in
      let vL = !vinfoL
      in
      vinfoL := []; vL
(***************************************************************************)

(* Given a type, find all (variables and structure fields ) with that type *)
(* Use the (global) vinfoL list defined above *)

class getVarsOfTypeClass typ = object
  inherit nopCilVisitor

  method vvdec vardec = 
(*    
      ignore (Pretty.printf "type of %s is %a @!" vardec.Cil.vname d_plaintype vardec.vtype);
      ignore (Pretty.printf "while typ is %a @!" d_plaintype typ);
*)
    if (vardec.vtype = typ) then
      vinfoL := vardec :: !vinfoL
   else ();
   DoChildren 

end

let getVarsOfType (typ : Cil.typ) (file : Cil.file) =
  let vtc = new getVarsOfTypeClass typ in
  let _ = visitCilFile vtc file in
  let vl = !vinfoL in vinfoL := []; vl

let getVarsOfTypedef (str : string) (file : Cil.file) = 
  (* can raise Not_found if no type with the given name is found *)
  (* first get the typeinfo from the string *)
  let (* tp = ref None and*) ti = ref None in
  iterGlobals file (fun g -> match g with
          GType(tinfo,_) -> begin
            if (tinfo.tname = str) then 
	      begin
(*		tp := Some tinfo.ttype; *)
		ti := Some tinfo
	      end
	    else ()
          end
	| _ -> () 
	      );
  if (!ti = None ) then (* typedef with name "str" was not found! *)
    []
  else
    let (* ttp = match !tp with Some t -> t | _ -> failwith "getVarsOfTypedef: tp null! (Should not happen)"
    and *) tti = match !ti with Some t -> t | _ -> failwith "getVarsOfTypedef: ti null! (Should not happen)"
    in
    (* then call getVarsOfType with the type *)
(*    Misc.union 
      (getVarsOfType (ttp) file) *)
      (getVarsOfType (Cil.TNamed (tti, []) ) file)

(***************************************************************************)
(** Find if global *)
let isGlobal (vinfo : Cil.varinfo) =
	(vinfo.Cil.vglob) 

let isGlobalString (str : string ) (file : Cil.file) =
  match (getVars str file) with
    [] -> raise Not_found
  | [v] -> isGlobal v
  | _ -> failwith ("isGlobalString: Unexpected: more than one varinfo found for string "^str)

let findGlobalVars (file : Cil.file) =
  let globlist = ref [] in
  Cil.iterGlobals file
    (fun g ->
      match g with
	GVar (vinf, _, _) -> globlist := vinf :: !globlist
      |	_ -> ()
      ) ;
  !globlist

(***************************************************************************)
module OrderedVar =
  struct
    type t = Cil.lval
    let compare = compare
  end
module VarSet = Set.Make (OrderedVar)

(* Simple slicer *)
(* Rules:
   1. All variables appearing in conditionals are important.
   2. Anything that is used in the definition of an important
      variable is important.
   3. Anything aliased to an important variable is important.
*)
(* Given an expression, return a list of variables occurring in
   it
*)
let rec getAllVars ex =
  match ex with
    Const  _ -> []              (** Constant *)
  | Lval  lv -> [lv]                  (** Lvalue *)
  | SizeOf _ -> []
  | SizeOfE e -> getAllVars e
  | AlignOf typ -> []
  | AlignOfE  e -> getAllVars e
  | UnOp  (_, e, _) -> getAllVars e
  | BinOp (_, e1, e2, _) -> Misc.union (getAllVars e1) (getAllVars e2) 
  | CastE (_, e) -> getAllVars e
  | AddrOf  lv -> [lv]
  | StartOf lv -> [lv]


(* Given a set of important variables, drop any assignment to
   a variable not in the set
*)
let doOneInstr (importantVars : VarSet.t) (acc: instr clist) (i: instr) : instr clist = 
  match i with 
  | Set (target, e, _) -> 
      if (VarSet.mem target importantVars) then
        CConsR (acc, i)
      else acc    

  | i -> CConsR (acc, i)

 
class dropUnimportantVisitor importantVars = object
    inherit nopCilVisitor

    method vstmt (s: stmt) = 
      match s.skind with 
        Instr il -> 
          let il' = toList (List.fold_left (doOneInstr importantVars) Clist.empty il) in
          s.skind <- Instr il';
          SkipChildren

      | _ -> DoChildren

  method vexpr _ = SkipChildren
  method vtype _ = SkipChildren
	
end

let impVars = ref (VarSet.empty)

class getImportantVisitor = object (self)
  inherit nopCilVisitor

  method vstmt (s: stmt) =
    match s.skind with
    | Instr (il) ->
        let rec _addVar lst =
          match lst with
            [] -> ()
          | a:: rest -> 
             begin
               match a with
               | Set (t, se, _) -> 
                   let allVars = getAllVars se in
                   if (VarSet.mem t !impVars) then
                     List.iter (fun v -> impVars := VarSet.add v !impVars) allVars ;
               | _ -> ();
               _addVar rest
             end
        in
        _addVar il; DoChildren

    | If (e, _, _, _) ->
         let allVars = getAllVars e in
         (* ignore (Pretty.printf "If: %a @!" d_exp e) ;
         List.iter (fun a -> ignore (Pretty.printf "If : %a @!" d_lval a)) allVars; *)
         List.iter (fun v -> impVars := VarSet.add v !impVars) allVars ; DoChildren
    | Return (Some (e), _) -> 
         let allVars = getAllVars e in
         List.iter (fun v -> impVars := VarSet.add v !impVars) allVars ; DoChildren
    | _ -> DoChildren


  method vexpr _ = SkipChildren
  method vtype _ = SkipChildren

end

module MakeSlicer = 
  functor (A : Partial.AliasInfo) ->
  struct
    let getImportantVars f =
      let finished = ref false in
      let iter = ref 0 in
      while not !finished do
        iter := !iter + 1;
        let oldsz = VarSet.cardinal !impVars in
        let f' = visitCilFile (new getImportantVisitor) f in 
        let newsz = VarSet.cardinal !impVars in
        if (newsz = oldsz) then finished := true;
        Printf.printf "Iteration  %d (%d elements important)\n" !iter newsz;
      done; 
      (* List.iter (fun a -> ignore (Pretty.printf "%a @! " d_lval a)) (VarSet.elements !impVars) ;  *)
      ()

    let dropUnimportantVars f impVars =
      let f' = visitCilFile (new dropUnimportantVisitor impVars) f in () 

  end

module BasicSlicer = MakeSlicer(Partial.EasyAlias)


let slice (f : Cil.file) =
  try
    BasicSlicer.getImportantVars f;
    BasicSlicer.dropUnimportantVars f !impVars;
    impVars := VarSet.empty; (* satisfy the gc *)
    f
  with e -> begin Printf.printf "Error in Slicing: %s\n" (Printexc.to_string e); raise e end


(* Implement the slam trick: if the conditional or the blocks of
   a conditional do not invoke (transitively)
   any spec function, then replace the conditional by
   __BLAST_NONDET
*)
module MakeCompact =
  functor (C : Partial.CallGraph) ->
    functor (A : Partial.AliasInfo) ->
  struct
    
    let debug = false 
	
    (* We keep this information about every statement. Ideally this should
       * be put in the stmt itself, but CIL doesn't give us space. *)
    type sinfo = { (* statement info *)
        incoming_state : (int, bool) Hashtbl.t ;
              (* mapping from stmt.sid to boolean state (important/not important) *)
        reachable_succs : (int, bool) Hashtbl.t ; 
              (* basically a set of all of the stmt.sids that can be 
		 * reached from this statement *)
(*      mutable last_used_state : S.t option ; *)
              (* When we last did the Post() of this statement, what
		 * state did we use? If our new incoming state is
		 * the same, we don't have to do it again. *)
	mutable priority : int ; 
              (* Whole-program toposort priority. High means "do me first".
		 * The first stmt in "main()" will have the highest priority.
		 *)
      } 
    let sinfo_ht = Hashtbl.create 511 
    let clear_sinfo () = Hashtbl.clear sinfo_ht 
	
    (* We construct sinfo nodes lazily: if you ask for one that isn't
       * there, we build it. *)
    let get_sinfo stmt = 
      try
        Hashtbl.find sinfo_ht stmt.sid
      with _ ->
        let new_sinfo = { incoming_state = Hashtbl.create 3 ;
                          reachable_succs = Hashtbl.create 3 ; 
                          (* last_used_state = None ; *)
                          priority = (-1) ; } in
        Hashtbl.add sinfo_ht stmt.sid new_sinfo ;
        new_sinfo 
	  
    (* Topological Sort is a DFS in which you assign a priority right as 
       * you start visiting the children. While we're there we compute
       * the actual number of unique succssors for each statement.
       *)
    let toposort_counter = ref 1  
	
    let add_edge s1 s2 =
      let si1 = get_sinfo s1 in
      Hashtbl.replace si1.reachable_succs s2.sid true 
	
    let rec toposort c stmt = 
      let si = get_sinfo stmt in 
      if si.priority >= 0 then
        () (* already visited! *)
      else begin
        si.priority <- !toposort_counter ; (* currently visiting *)
        incr toposort_counter;
        (* handle function calls in this basic block *) 
        (match stmt.skind with
          (Instr(il)) -> 
            List.iter (fun i -> 
              let fd_list = match i with
                Call(_,Lval(Var(vi),NoOffset),_,_) -> 
                  begin 
                    try 
                      let fd = C.fundec_of_varinfo c vi in
                      [fd] 
                    with e -> [] (* calling external function *)
                  end 
              | Call(_,e,_,_) -> 
                  A.resolve_function_pointer e 
              | _ -> []
              in 
              List.iter (fun fd -> 
                if List.length fd.sbody.bstmts > 0 then 
                  let fun_stmt = List.hd fd.sbody.bstmts in 
                  add_edge stmt fun_stmt ; 
                  toposort c fun_stmt 
		    ) fd_list 
		) il 
        | _ -> ()); 
        List.iter (fun succ -> 
          add_edge stmt succ ; toposort c succ) stmt.succs ;
      end
	  

      (* Mark important. A statement is important if it refers to
	 an important variable.
	 *)
    let mark_important c =
      ()
	
    let dataflow 
	(file : Cil.file)         (* whole program *)
	(c : C.t)                 (* call graph *) 
        (spec_vars : Cil.lval list)
	(initial_stmt : Cil.stmt) (* entry point *)  
	= 
      begin
	      (* count the total number of statements in the program *)
	let num_stmts = ref 1 in
	iterGlobals file (fun g -> match g with
          GFun(fd,_) -> begin
            match fd.smaxstmtid with
              Some(i) -> if i > !num_stmts then num_stmts := i 
            | None -> () 
          end
	| _ -> () 
	      ) ;
	Printf.printf "Dataflow: at most %d statements in program\n" !num_stmts ; 
	
      (* create a priority queue in which to store statements *) 
	let worklist = Heap.create !num_stmts in
	
	let finished = ref false in
	let passes = ref 0 in
	(* add something to the work queue *)
      let enqueue caller callee state = begin
        let si = get_sinfo callee in 
        Hashtbl.replace si.incoming_state caller.sid state ;
        Heap.insert worklist si.priority callee
      end in 

      (* we will be finished when we complete a round of data-flow that
	 * does not change the ICFG *)
      while not !finished do 
        clear_sinfo () ; 
        incr passes ; 
	
        (* we must recompute the ordering and the predecessor information
           * because we may have changed it by removing IFs *)
        Printf.printf "Dataflow: Topological Sorting & Reachability\n" ;
        toposort c initial_stmt ;  
	()
      done;
      ()
      end


    let simplify 
	(file : Cil.file) 
	(c : C.t)  
	(fd : Cil.fundec) 
	(spec_vars : Cil.lval list) =
      dataflow file c spec_vars (List.hd fd.sbody.bstmts)


  end
	
module BasicCompact = 
  MakeCompact 
    (Partial.BasicCallGraph) 
    (Partial.EasyAlias)
    
(*********** Temporary inefficient solution ******)
    let finished = ref false 
	
    let impBlocksHt = Hashtbl.create 97
    let impFunsHt = Hashtbl.create 97
	
    class getImportantBlocksVisitor (importantVars : lval list) = object 
      inherit nopCilVisitor
	  
      val the_fun = ref None

      method vblock (b : block) = 
	let rec checkIfImportant l =
	  Printf.printf "Checking importance...";
	  match l with
	    [] -> false
	  | a :: rest ->
	      begin
		match a.skind with
		  Instr il ->
		    begin
                      Printf.printf "Case Instr\n";
		      let rec checkInstlist insl =
			match insl with 
			  [] -> checkIfImportant rest
			| i :: more ->
			    begin
			      ignore (Pretty.printf "Instruction %a @!" d_instr i); 
			      match i with
				Set (t, _, _) -> 
				  if (List.mem t importantVars) then 
				    begin
				      ignore (print_string "this is important.\n") ; true 
				    end
				  else checkInstlist more
			      |	Call (None, Lval (Var(callee), NoOffset),_,_) ->
				  if (Hashtbl.mem impFunsHt callee) then 
				    begin
				      ignore (print_string "this is important.\n") ; true 
				    end 
				  else checkInstlist more
			      |	Call (Some lvl, Lval (Var(callee), NoOffset),_,_) ->
				  if (List.mem lvl importantVars) then 
				    begin
				      ignore (print_string "this is important.\n") ; true 
				    end 
				  else
				    if (Hashtbl.mem impFunsHt callee) then true else checkInstlist more
			      |	Call _ ->
				  ignore (print_string "This case" );
				  true (* refine this! *)
			    end
		      in
		      checkInstlist il
		    end
		| If (e, b1, b2, _) ->
                    begin
		      Printf.printf "Case If\n";
		      ignore (Pretty.printf "Instruction %a @!" d_stmt a); 
		      if ((Hashtbl.mem impBlocksHt b1) or (Hashtbl.mem impBlocksHt b2)) then 
			begin
			  print_string "some body important" ;
			  true
			end
		      else 
			if (not (Misc.intersection (getAllVars e) importantVars = [])) then 
			  begin (ignore (print_string "e has a common var" ) ) ; true end
			else (checkIfImportant rest)
		    end
		| Goto (sref, _) -> 
		      (* if the target of the goto is an important block, then true else false *)
                      let getBlock sref = 
			match (!sref).skind with Block b -> b | _ -> (Pretty.printf "Stmt %a @!" d_stmt !sref) ; raise Not_found
		      in
		      (try
			if (Hashtbl.mem impBlocksHt (getBlock sref)) then true else checkIfImportant rest
		      with Not_found -> true )
		| Return (None, _) -> false
		|  Return (Some e, _)  -> 
		    if (not (Misc.intersection (getAllVars e) importantVars = [])) then true 
		    else 
		      checkIfImportant rest

		| Loop (blk, _) -> if (Hashtbl.mem impBlocksHt blk) then true else checkIfImportant rest
		| Block blk -> if (Hashtbl.mem impBlocksHt blk) then true else checkIfImportant rest
		| _ -> (* JHALA ignore (Pretty.printf "%a @!" d_stmt a); *) failwith "Here"
	      end
	in
	if (Hashtbl.mem impBlocksHt (b)) then ()
	else
	  if (checkIfImportant b.bstmts) then 
	    begin
(*
	      Printf.printf "This block is important!\n";
	      ignore (Pretty.printf "%a @!" d_stmt (mkStmt (Block b)));
	      Printf.printf "Adding this block.\n\n";
*)
	      (Hashtbl.replace impBlocksHt b true); finished := false;
	      match !the_fun with
		None -> failwith "Block outside of any function"
	      |	Some (enclosing) -> (Printf.printf "Adding function %s to imp func list.\n" enclosing.vname;
				       Hashtbl.replace impFunsHt enclosing true)
	    end	  
	  else ()
	      ;
	DoChildren

      method vfunc f = the_fun := Some (f.svar) ; DoChildren
    
      method vexpr _ = SkipChildren
      method vtype _ = SkipChildren
      method vvdec _ = SkipChildren
	  
    end 

    class dropUnimportantIfsVisitor impVars = object
      inherit nopCilVisitor
      val nondet = var (makeGlobalVar "__BLAST_NONDET__CIL" intType)
      method vstmt s = 
	begin
	  match s.skind with
	    If (e, b1, b2, l) ->
	      if ((Hashtbl.mem impBlocksHt b1) or (Hashtbl.mem impBlocksHt b2)) then ()
	      else 
		begin
		  if (not (Misc.intersection (getAllVars e) impVars = [])) then () 
		  else
		    begin
		      ignore ( (* Pretty.printf "If : %a @!" d_stmt s ;*)
			      Printf.printf "Changing this if ...\n\n"); s.skind <- If (Lval nondet, b1, b2, l)
		    end
		end
	  | _ -> ()
	end;
	DoChildren
    end

let t_simplify 
    (file : Cil.file) (impVars : string list) =
  Message.msg_string Message.Normal "Simplifying if's";
  match impVars with 
    [] -> (* No spec variables are given. Should not slice! *)
      ()
  |	_ ->
      begin
	let iter = ref 0 in
	let impv = List.concat (List.map (fun vinf -> List.map var (getVars vinf file)) impVars) in
	while (not !finished) do 
	  incr iter ;
	  finished := true; 
	  let _ = visitCilFile (new getImportantBlocksVisitor impv) file in
          ()
	done;
      (* Now drop conditions on all useless ifs *)
	ignore (visitCilFile (new dropUnimportantIfsVisitor impv) file );
	Hashtbl.clear impBlocksHt ;
	Hashtbl.clear impFunsHt
      end
	
let spec_slice (files : Cil.file list) (seedPred : Predicate.predicate) =
  Message.msg_string Message.Normal "In spec_slice"  ;
  let impvars = 
    let allexps = Predicate.allVarExps seedPred
    in
    List.map 
      (fun e ->
	let sym =
	  match e with
	    Expression.Lval l 
	  | Expression.Unary (Expression.Address, Expression.Lval l) ->
	      Expression.fullsimp_lval l
	  | _ -> failwith ("spec_slice: failing on strange expression "^(Expression.toString e))
	in
	match sym with
	  Expression.Symbol s -> s
	      )
      allexps
  in
  List.iter (fun a -> print_string a; print_newline ()) impvars ;
  print_string "ok\n";
  let process_one_file file =
    t_simplify file impvars
    in
  List.iter process_one_file files
      
(*****************************************************************************)
(* Live variable and live predicate analysis.                                
   A predicate is live if any variable appearing 
   in it is live.  

   The result of the analysis can be queried in the following way:
   given a CFA location and a variable, say if the variable is live at
   that location.
*)
(*****************************************************************************)

