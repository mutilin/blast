(** Module for representing a call stack. Used for Constant and Symbolic Store lattices *)
module CallStack =
struct
  (** A call stack frame consists of the name of the function called, a list of argument expressions
      passed to the function, and an lvalue that will receive the result of the called function. *)
  type frame = Ast.Symbol.symbol * ((string * Ast.Expression.expression) list) * Ast.Expression.lval
      (** A call stack is simply a list of frames *)
  and t = frame list

  let empty_stack = []

  exception Stack_empty

  (** Check two call stacks for equality. This comparison ignores the parameters passed into each function
    and only looks at the function names and return targets. *)
  let rec are_equal (a : t) (b : t) =
    match (a, b) with
	([], []) -> true
      | ([], first :: rest) | (first :: rest, []) -> false
      | ((n1, arglist1, retlv1) :: rest1, (n2, arglist2, retlv2) :: rest2) ->
	  if (n1 = n2) && (retlv1 = retlv2) then are_equal rest1 rest2 else false

  let push (stack : t) (f : frame) =
    f :: stack
      
  let pop (stack : t) =
    match stack with
	[] -> raise Stack_empty
      | head :: rest -> (head, rest)

  let print fmt (stack : t) =
    let rec print_frame fmt stack' =
      match stack' with
	  (fname, formals, ret_target) :: rest ->
	    Format.fprintf fmt "@[[%s = %s(" (Ast.Expression.lvalToString ret_target) fname;
	    List.iter (fun formal -> let (name, value) = formal in
			 Format.fprintf fmt "%s=%s@ " name (Ast.Expression.toString value)) formals;
	    Format.fprintf fmt ")] @ ";
	    print_frame fmt rest
	| _ -> ()
    in
      match stack with
	  [] -> Format.fprintf fmt "@[Callstack: []@]"
	| _  ->
	    Format.fprintf fmt "@[Callstack: ";
	    print_frame fmt stack;
	    Format.fprintf fmt "]@]"

end


(** Module for maintaining mappings of variables. Used by the constant lattice. *)
module SymbolTable =
struct
  type t = (Ast.Expression.lval, Ast.Expression.expression) Hashtbl.t * CallStack.t

  exception Stop_iter (** Used internally to stop an interation early
			(e.g. in calls to Hashtbl.iter) *)
  exception Inconsistent_tables (** Raised when taking the union of two inconsistent tables *)

  let make_symbol_table parent_table =
    let (old_map, old_stack) = parent_table
    in (Hashtbl.copy old_map, old_stack)

  (** Create a top-level symbol table. *)
  let make_root_symbol_table () =
    (Hashtbl.create 31,
     [("__rootfn", CallStack.empty_stack, C_SD.junkRet)])

  (** helper function to determine if a variable is trackable *)
  let rec is_trackable (table : t) (lval : Ast.Expression.lval) =
    let (map, stack) = table
    in
      match lval with
	  Ast.Expression.Symbol(sym) -> true
	| Ast.Expression.Access(op, expr, sym) ->
	    begin
	      match expr with
		  Ast.Expression.Lval(lv) -> is_trackable table lv
		| _ -> false
	    end
	| Ast.Expression.Dereference(expr) ->
	    begin
	      match expr with
		  Ast.Expression.Lval(lv) -> is_trackable table lv
		| _ -> false
	    end
	| Ast.Expression.Indexed(expr, idx_expr) -> false
	| Ast.Expression.This -> false

  let __next_constant = ref 0

  let symbolic_const_prefix = "SymbolicConst_"

  (** generate a unique symbolic constant *)
  let _get_next_symbolic_constant () =
    let name = Printf.sprintf "%s%03d" symbolic_const_prefix !__next_constant
    in __next_constant := !__next_constant + 1;
      Ast.Expression.Lval(Ast.Expression.Symbol (name))

  let _is_symbolic_constant (s : string) =
    let pfx_len = String.length symbolic_const_prefix
    in if ((String.length s)>pfx_len) && ((String.sub s 0 pfx_len)=symbolic_const_prefix) then true
      else false

  (** Reset the symbolic constant counter to zero. This is used by unit tests to ensure consistent
	names for symbolic variables. It should not be used elsewhere! *)
  let _reset_symbolic_constants () = __next_constant := 0
    
  (** Helper function to fold constants. If the input expression is not a constant,
      a symbolic constant is returned *)
  let rec fold_constants (symtable : t) (expr : Ast.Expression.expression) =
    let (map, stack) = symtable
    in let e = Ast.Expression.constantfold expr map
    in match e with
	Ast.Expression.Constant(const) -> e
      | Ast.Expression.Lval (Ast.Expression.Symbol s) ->
	  if _is_symbolic_constant s then e else _get_next_symbolic_constant ()
      | _ -> _get_next_symbolic_constant ()
  
  (** Add a new mapping (if lval is trackable) and return the new symbol table.
      The expression is constant folded, and if the result is not a constant, it is
      replaced with a new symbolic constant.

      If remove_aliases is true, any aliases for this lval are removed from the table
      (even if the lval is not trackable). Aliases should be removed for an assignment, but
      not when a mapping is added based on information obtained from a predicate. *)
  let add_mapping (table : t)
                  (lval : Ast.Expression.lval)
                  (expr : Ast.Expression.expression)
                  (remove_aliases : bool) =
    let (map, stack) = table in
    let folded_value = fold_constants table expr (* compute value before removing aliases *)
    in
    let rec remove_dereferences map lv =
      let deref_lv = Ast.Expression.Dereference (Ast.Expression.Lval lv)
      in if Hashtbl.mem map deref_lv then
	  (Hashtbl.remove map deref_lv; remove_dereferences map deref_lv)
    in
      if remove_aliases then
	begin
	  (* if any aliases for this lvalue are in the matrix, and they do
	     not have the same value as new lvalue, remove them. *)
	  List.iter
	    (fun alias_lv ->
	       (if Hashtbl.mem map alias_lv then
		  (if folded_value <> (Hashtbl.find map alias_lv)
		   then Hashtbl.remove map alias_lv)))
	    (AliasAnalyzer.get_lval_aliases lval);
	  (* if we are changing a pointer, we need to remove mappings for any dereferences of this pointer
             (e.g. if changing p, then *p, **p no longer refer to the same location) *)
	  remove_dereferences map lval;
	end;
      (* if trackable, we add the mapping *)
      if (is_trackable table lval) = false then (map, stack)
      else 
	begin
	  Hashtbl.replace map lval (fold_constants table folded_value);
	  (map, stack)
	end

  (** Replace any occurrances of the first value with the second value in the symbol table.
      Both values should have already undergone constant folding *)
  let replace_value (table : t)
                    (value1 : Ast.Expression.expression)
		    (value2 : Ast.Expression.expression) =
    let (map, stack) = table in
      Hashtbl.iter (fun k v -> if v = value1 then Hashtbl.replace map k value2) map;
      table

  let find_mapping table lval =
    let (map, stack) = table in
      if Hashtbl.mem map lval
      then ConstantLattice.Constant(Hashtbl.find map lval)
      else ConstantLattice.Top

  (** Invalidate the mapping for the specified lval as well as for any aliases it has *)
  let invalidate_mapping (table : t) (lval : Ast.Expression.lval) =
    let (map, stack) = table
    and drop_list = lval :: (AliasAnalyzer.get_lval_aliases lval)
    in
      List.iter
	(fun alias_lv -> Hashtbl.remove map alias_lv)
	drop_list;
      (map, stack)

  (** Helper function to convert a formal param name to a symbol *)
  let formal_to_symbol formal_name function_name =
    Ast.Expression.Symbol (formal_name)

  (** Push the function call onto the stack and return the new symbol table *)
  let push_fn_call (table : t) (fn_expr : Ast.Expression.expression) (is_fn_defined : string -> bool) =
    let (map, stack) = table
    and (fname, formals, target) = C_SD.deconstructFunCall fn_expr
    in
      if is_fn_defined fname
      then
	begin (* we only push if this is a real function that we will traverse *)
	  (* Add the formals from the funcall*)
	  List.iter
	    (fun f -> let (name, value) = f in
	       Hashtbl.replace map (formal_to_symbol name fname) (fold_constants table value))
	    formals;
	  (* new stack *)
	  (map, CallStack.push stack (fname, formals, target))
	end
      else
	begin (* if the function is not defined, havoc the target *)
	  if target<>C_SD.junkRet then invalidate_mapping table target
	  else (map, stack)
	end

  (** Pop a function call, removing local variables and assigning the return value target
      to rtn_val_expr (if tracked). Constants are folded from this expression before assignment.
      This function returns the new symbol table.

      The function is_local_to_fn is used to determine whether variables are local to the stack
      frame being popped.
   *)
  let pop_fn_call (table : t)
                  (rtn_val_expr : Ast.Expression.expression) 
                  (is_local_to_fn : Ast.Expression.lval -> string -> bool) =
    let (map, stack) = table
    in let ((fname, formals, ret_target), rest_of_stack) = CallStack.pop stack
        (* We have to fold the new return value before we remove the existing local
           mappings, or else we may miss out on some constant folding opportunities.
	   However, we add the mapping later, after we've popped of this stack frame. *)
       and folded_rtn_val = fold_constants table rtn_val_expr
    in
      (* Remove the formals and locals from the map *)
      Hashtbl.iter
	(fun k v -> if (is_local_to_fn k fname) then Hashtbl.remove map k)
	map;
      (* Add mapping for result, if it is trackable and not junkRet.  In either case, we
	 return the new table *)
      if (ret_target<>C_SD.junkRet) && (is_trackable table ret_target) then
	add_mapping (map, rest_of_stack) ret_target folded_rtn_val true
      else (map, rest_of_stack)
	
  (** Iterate through the mappings *)
  let iter (fn : Ast.Expression.lval -> Ast.Expression.expression -> unit) (table : t) =
    let (map, stack) = table
    in
      Hashtbl.iter fn map
	
  (** Fold on the symbol mappings *)
  let fold (fn : Ast.Expression.lval -> Ast.Expression.expression -> 'a -> 'a)
    (table : t)
    (start_val : 'a) =
    let (map, stack) = table in
      Hashtbl.fold fn map start_val
	
  (** Return true if the table has no mappings *)
  let is_empty table =
    let (map, stack) = table
    in try
	Hashtbl.iter (fun key value -> raise Stop_iter) map;
	false
      with Stop_iter -> true

  (** Return the intersection of two symbol tables. This operation is only defined if both
      tables have the same callstack. Otherwise, an exception will be thrown. *)
  let intersection t1 t2 =
    let (map1, stk1) = t1
    and (map2, stk2) = t2
    and h = Hashtbl.create 31
    in
      (if (CallStack.are_equal stk1 stk2)=false
       then failwith "Attempt to take intersection of two symbol tables with different stacks");
      Hashtbl.iter
	(fun key i -> 
	   if Hashtbl.mem map2 key then
	     begin
               let j = Hashtbl.find map2 key
	       in
		 if i = j then Hashtbl.replace h key i
	     end
	   else ())
	map1;
      (h, stk1)

  (** Return true if the two symbol tables are equal. The comparison includes both symbol tables and stacks. *)
  let is_equal t1 t2 =
    let (map1, stk1) = t1
    and (map2, stk2) = t2
    in (map1=map2) && (CallStack.are_equal stk1 stk2)

  let are_stacks_equal (t1 : t) (t2 : t) =
    let (map1, stk1) = t1
    and (map2, stk2) = t2
    in CallStack.are_equal stk1 stk2

  (** Return true if the symbol table has a mapping for "key", and the value matches "value",
      return true. *)
  let has_equal_mapping table key value =
    let (map, _) = table in
      if (Hashtbl.mem map key) = false then false
      else (Hashtbl.find map key) = value

  (** Return the union of two symbol tables. The resulting table has an
      empty call stack - it should only be used for region operations, and no
      mappings changed directly. If the two tables have an entry with inconsistent
      values, and both are constants, the exception Inconsistent_tables is raised.
      If the two values for a mapping are inconsistent, but not constant, do not
      add to map (equivalent to top) *)
  let union t1 t2 =
    let (map1, _) = t1
    and (map2, _) = t2
    and h = Hashtbl.create 31
    in
      (* First, we iterate through map1 and add any elements in map1 or both
	 to result table. *)
      Hashtbl.iter
	(fun key i -> 
	   if Hashtbl.mem map2 key then
	     begin
               let j = Hashtbl.find map2 key
	       in
		 if i = j then Hashtbl.replace h key i
		 else match (i, j) with
		     (Ast.Expression.Constant c1, Ast.Expression.Constant c2) ->
		       raise Inconsistent_tables
		   | _ -> () (* if not certain we have a conflict, don't add to map *)
	     end
	   else
	     Hashtbl.replace h key i)
	map1;
      (* Next, iterate through map2 and add all elements not in map1 *)
      Hashtbl.iter (fun key data ->
		      if (Hashtbl.mem map1 key) = false then Hashtbl.replace h key data)
	map2;
      (h, [])

  (** Return a map of lvals to expressions. It would be nice to change this to return
      a function which can be used to lookup mappings instead. This requires changing
      the signature of the constant fold function. *)
  let get_map table =
    let (map, stack) = table in map
end

  (** Lattice for tracking constants. Implemented as a map between lvals and
      expressions. If an lval is not in the hashtable, then it is assumed to be Top
  *)
module Make_ConstantLattice =
  functor(C_SD : SYSTEM_DESCRIPTION with module Command = BlastCSystemDescr.C_Command) ->
  functor (Op : OPERATION ) ->
struct
  module Command = C_SD.Command

  type lattice = Bottom (** Empty set - state is not reachable *)
		 | SymTab of SymbolTable.t (** map of lvals to known constant values, plus call stack *)
		 | Top (** Nothing is known about the value of any variables *)
		     
  type edge = C_SD.edge
    
  let top = Top 
  let bottom = Bottom
  type _query_fn = Ast.Predicate.predicate -> Ast.predicateVal
  let query_fn (lemnt:lattice) (pred:Ast.Predicate.predicate) =
    Ast.Dontknow

  let print fmt l =
    match l with
	Bottom -> Format.fprintf fmt "@[Bottom@]"
      | Top -> Format.fprintf fmt "@[Top@]"
      | SymTab(table) ->
	  Format.fprintf fmt "@[[ ";
	  SymbolTable.iter (fun lvalue const ->
			      Ast.Expression.print_lval fmt lvalue;
			      Format.fprintf fmt " = ";
                              Ast.Expression.print fmt const;
                              Format.fprintf fmt "@  ")
	    table;
	  Format.fprintf fmt "]@]"
	  (*;Format.fprintf fmt "@\n"; SymbolTable.print_callstack fmt table*)

  (** Take the union of two regions. In the case where both lattices
      are maps, this means taking the intersection of the two tables.
      This is because, for an individual element, Constant join Top =
      Top (and we do not store Top elements in the map). *)
  let join l1 l2 =
      match (l1, l2) with
      | (Top, _) | (_, Top) -> Top
      | (Bottom, Bottom) -> Bottom
      | (Bottom, l) | (l, Bottom) -> l
      | (SymTab t1, SymTab t2)  ->
	  try
	    SymTab(SymbolTable.intersection t1 t2)
	  with Failure(msg) ->
	    Message.msg_string Message.Error ("Error in post.join: " ^ msg);
	    Message.msg_string Message.Error "First input lattice: ";
	    Message.msg_printer Message.Error print l1;
	    Message.msg_string Message.Error "Second input lattice: ";
	    Message.msg_printer Message.Error print l2;
	    Message.msg_string Message.Error "";
	    failwith msg

  (** Take the intersection of two regions. In the case where both lattices
      are maps, this means taking the union of the two tables.
      This is because, for an individual element, Constant meet Top =
      Constant. If two maps have different values for the same lval, and
      both are true constants, we return Bottom. If two maps have different
      values for the same lval, and at least one is another lval or a symbolic
      constant, we use Top for that element. This is because we cannot be
      certain that the two variables are not actually aliases. Thus, we
      overapproximate. {i Is this correct???}  *)
  let meet l1 l2 =
      match (l1, l2) with
      | (Bottom, _) | (_, Bottom) -> Bottom
      | (Top, Top) -> Top
      | (Top, l) | (l, Top) -> l
      | (SymTab t1, SymTab t2)  -> 
	  try
	    let table = SymbolTable.union t1 t2 in
	      if SymbolTable.is_empty table then Top else SymTab(table)
	  with
	      SymbolTable.Inconsistent_tables -> Bottom

  (** Check if the region represented by a is contained within the region represented by b.
      When comparing two maps, if an element is in a, but not b, then it is OK, since
      Constant <= Top. However, if an element is in b, but not a, then a is not contained
      within b since Top > Constant.

      For a lattice region to be contained within the region
      of another lattice, the call stacks must be equal.
  *)
  let leq a b =
   match (a,b) with
     (Bottom, _) -> true
   | (SymTab t1, SymTab t2) ->
       begin
	 try
	   SymbolTable.iter
	     (fun key data ->
		if (SymbolTable.has_equal_mapping t1 key data) = false
		then raise SymbolTable.Stop_iter)
	     t2;
	   SymbolTable.are_stacks_equal t1 t2
	 with SymbolTable.Stop_iter -> false
       end
   | (SymTab _, Top) -> true
   | (Top, Top) -> true
   | _ -> false
 

  let eq a b =
   match (a,b) with
     (Bottom, Bottom) -> true
   | (SymTab t1, SymTab t2) -> SymbolTable.is_equal t1 t2
   | (Top, Top) -> true
   | _ -> false

  let focus () = ()

  let symtab_to_pred_list table =
    (* helper function to build a list of predicates from a map of
       constant lattice information *)
    SymbolTable.fold
      (fun lval c pred_list ->
	 Ast.Predicate.Atom(Ast.Expression.Binary(Ast.Expression.Eq,
						  Ast.Expression.Lval lval, c)) :: pred_list)
      table []  
  let is_consistent l phi =
      match l with 
	  SymTab(m) ->
	    begin
	      match symtab_to_pred_list m with
		  [] -> true
		| pred_list ->
		    let result = failwith "FUNCTOR:PREDTABLE:LATTICE" (*
                    PredTable.isSatisfiable (phi, pred_list) *)
		    in Message.msg_string Message.Error "Predicates are not consistent with lattice!" ;
		      result
	    end
	| _ -> true

  (**
    Return a new lattice based on the command associated with this edge. In the current
    implementation, the entire map is copied and updated based on the command. 

    {i Alternative Implementation} -- start with empty maps and only put in the changes for a
    given node. A parent latice pointer would be kept so that checks could search up the chain of
    predecessors until the entry for a given lvalue is found. A disadvantage of that approach is
    that, when a variable is assigned a non-constant value, we would have to assign it to "Top"
    in the map. This makes it difficult to reuse the map with Ast.Expression.constantfold.

    {b Rules for post} (based on edge's command code)
    + {b Skip}         - Return the predecessor lattice
    + {b Block}        - Sequentially run post on each statement in block (see below)
    + {b Predicate}    - Verify that conjunction of predicate and lattice is satisfiable.
                         If not, return Bottom. If satisfiable, add any equalities in the
                         predicate to the constant lattice.
    + {b FunctionCall} - Push a new function scope lattice onto the stack. Add the formals
                         and their values to this lattice. (??? not yet implemented)
    + {b Havoc}        - Set the havoced variable to Top

    {b Rules for statements}
    Statements may be either expressions or return statements. It is assumed that CIL has
    pre-processed the expressions such that side-effects only occur in assignments and
    assignments are not embedded within other expressions. This signifcantly simplifies
    the processing for expressions.

    For {b Return Statements}, the local variable scope is popped and an assignment of the
    return value to a variable added to the parent scope (or the global variable scope).

    For {b Assignments}, check whether the lval is tracked (see below for rules). If so,
    evaluate the source expression using Ast.Expression.constantfold. If a constant is
    returned, add an entry between the lval and constant to the map. If a symbol is
    returned (which implies the symbol is not already in the map), add a mapping between
    the lval and the symbol. If neither a constant nor a symbol is returned, create a
    new symbolic constant and add a mapping between the lval and the constant.

    If the lval is a global, the it is added to the global variable map. Otherwise, it
    is added to the current scope's map.

    We then call get_lval_aliases to get all the aliases for this lvalue (even if it is
    not tracked). We remove any mappings for these aliases from the map, unless they have
    the same current value as the assigned lval.

    {b {i Trackable} lvals}
    We only add an entry to the map if an lval is trackable. If an lval is not trackable, any
    use in a source expression will return Top. Here are the rules for determining this:
    {ol
       {- {b Symbol}       -  Always trackable}
       {- {b Access}      -  Never trackable (at least for now)}
       {- {b Dereference} - If the target of the dereference is trackable, then the
                            dereference is trackable. Thus *p and **p are both trackable,
                            but *(p+1) is not.}
       {- {b Indexed} -     Never trackable.}}
  *)
  let post_cmd (l : lattice) (c: C_SD.Command.t) (name:string) =
    let symtab = match l with
	SymTab(t) -> ref (SymbolTable.make_symbol_table t)
      | Bottom -> failwith "post: should not take post of Bottom"
      | _ -> ref (SymbolTable.make_root_symbol_table ())
    and print_result_and_return result =
(*
      Message.msg_string Message.Normal "  ConstantLattice.post result:";
      Message.msg_printer Message.Normal print result;
      Message.msg_string Message.Normal "";
*)
      result
    and add_mappings_from_predicate table predicate =
      (* Helper function to add mappings to the symbol table based on the predicate being true.
         Returns the new lattice (which may just be the original lattice l passed into post). *)
      begin
	match (predicate) with
	    Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq,
						       Ast.Expression.Lval lv1,
						       Ast.Expression.Lval lv2))
	  | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Ne,
									  Ast.Expression.Lval lv1,
									  Ast.Expression.Lval lv2))) ->
	      begin (* both sides of predicate are lvals - several cases to consider *)
		match ((SymbolTable.find_mapping table lv1),
		       (SymbolTable.find_mapping table lv2)) with
		    (ConstantLattice.Constant(Ast.Expression.Constant(c1)),
		     ConstantLattice.Constant(Ast.Expression.Constant(c2))) ->
		      if c1 = c2 then l (* both already constants in symtab, nothing new to add *)
		      else 
                        begin
			 Message.msg_string Message.Normal ("Predicate "^(Ast.Predicate.toString predicate)) ;
                         Message.msg_printer Message.Normal  print l ;
                         failwith "inconsistent constants in map!"
                        end
		  | (ConstantLattice.Constant(Ast.Expression.Constant(c)), _) ->
		      (* if lv1 is a constant, add a mapping between lv2 and this constant *)
		      SymTab(SymbolTable.add_mapping table lv2 (Ast.Expression.Constant c) false)
		  | (_, ConstantLattice.Constant(Ast.Expression.Constant(c))) ->
		      (* if lv2 is a constant, add a mapping between lv1 and this constant *)
		      SymTab(SymbolTable.add_mapping table lv1 (Ast.Expression.Constant c) false)
		  | (ConstantLattice.Constant(Ast.Expression.Lval(Ast.Expression.Symbol(s1))),
		     ConstantLattice.Constant(Ast.Expression.Lval(Ast.Expression.Symbol(s2)))) ->
		      (* if two symbols, pick one and replace occurances of the other *)
		      SymTab(SymbolTable.replace_value table
			       (Ast.Expression.Lval (Ast.Expression.Symbol s2))
			       (Ast.Expression.Lval (Ast.Expression.Symbol s1)))
		  | _ -> (* otherwise, we define a new symbol for both *)
		      let new_symbol = SymbolTable._get_next_symbolic_constant ()
		      in SymTab(SymbolTable.add_mapping
				  (SymbolTable.add_mapping table lv1 new_symbol false)
				  lv2 new_symbol false)
	      end
	  | Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq, Ast.Expression.Lval lv, e))
	  | Ast.Predicate.Atom (Ast.Expression.Binary (Ast.Expression.Eq, e,
						       Ast.Expression.Lval lv))
	  | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary
						     (Ast.Expression.Ne, e, Ast.Expression.Lval lv))) 
	  | Ast.Predicate.Not (Ast.Predicate.Atom (Ast.Expression.Binary
						     (Ast.Expression.Ne,
						      Ast.Expression.Lval lv, e))) ->
	      begin
		let e' = SymbolTable.fold_constants table e
		and old_val = SymbolTable.find_mapping table lv
		in
		  match e' with
		      Ast.Expression.Constant(c) -> (* lv == constant *)
			begin
			  match old_val with
			      ConstantLattice.Constant(Ast.Expression.Lval(Ast.Expression.Symbol s)) ->
				(* the old value is a symbol, we replace it everywhere with the constant *)
				SymTab(SymbolTable.replace_value table
					 (Ast.Expression.Lval (Ast.Expression.Symbol s)) e')
			    | _ -> (* old value not a symbol, just add mapping *)
				SymTab(SymbolTable.add_mapping table lv e' false)
			end
		    | _ -> l
	      end
          | _ -> l
      end
    in
(*
      Message.msg_string Message.Normal "ConstantLattice.post input: ";
      Message.msg_string Message.Normal "Lattice: ";
      Message.msg_printer Message.Normal print l;
      Message.msg_string Message.Normal "Command: ";
      Message.msg_printer Message.Normal C_SD.Command.print c;
      Message.msg_string Message.Normal "";
*)
      (* the main body of this function *)
      match c.Command.code with
	  Command.Skip -> l (* lattice unchanged *)
	| Command.Block(statements) ->
	    List.iter
	      (function
		   Command.Expr (Ast.Expression.Assignment(op, lval, src_expr)) ->
		     symtab := SymbolTable.add_mapping !symtab lval src_expr true
		 | Command.Expr(expr) -> () (* can ignore other expressions *)
		 | Command.Return(expr) ->
		     symtab := SymbolTable.pop_fn_call !symtab expr
		                 (fun lv fname -> (* JHALA *)
				    (C_SD.is_in_scope fname lv)=true && (C_SD.is_global lv)=false))
	      statements;
	    let result = SymTab(!symtab) in print_result_and_return result
	| Command.Pred(predicate) ->
            let qsat = 
              ((predicate = Ast.Predicate.False )
              || (SymbolTable.fold_constants !symtab 
                  (match predicate with Ast.Predicate.Atom e->e| Ast.Predicate.True -> Ast.Expression.Constant (Ast.Constant.Int 1)
	          | Ast.Predicate.Not (Ast.Predicate.Atom e) -> Ast.Expression.Binary(Ast.Expression.Minus, Ast.Expression.Constant (Ast.Constant.Int 1), e)
                  | p -> failwith ("duh: "^(Ast.Predicate.toString p))) = Ast.Expression.Constant (Ast.Constant.Int 0)) ) (* check *)
            in
(*
            if (sat <> not qsat) then begin
              Message.msg_string Message.Normal "sat is not the opposite of qsat" ;
              Message.msg_printer Message.Normal Ast.Predicate.print predicate ;
              Message.msg_printer Message.Normal print (SymTab !symtab) ;
            end ; 
*)

	    if (not qsat )
	    then print_result_and_return (add_mappings_from_predicate !symtab predicate)
	    else 
              begin
                Message.msg_string Message.Normal ("One more predicate ruled out by constant lattice! " ^ (Ast.Predicate.toString predicate)) ;
		print_result_and_return Bottom
              end
	| Command.FunctionCall(expr) ->
	    let result = SymTab(SymbolTable.push_fn_call !symtab expr
			        (fun fname ->
				   if C_SD.is_defined fname then true else false))
	    in print_result_and_return result
	| Command.Havoc(lval) ->
	    let result =  SymTab(SymbolTable.invalidate_mapping !symtab lval)
	    in print_result_and_return result
	| Command.HavocAll -> print_result_and_return Top
	| Command.Phi(lval, sign) -> failwith "ConstantLattice post: encountered phi command"
        | Command.SymbolicHook _ -> l
	| _ -> failwith "ConstantLattice post: encountered unknown command"
  let post (l:lattice) (e:edge) (fn:_query_fn) :(Ast.Predicate.predicate list*lattice)  =
    let tmpval = post_cmd l (C_SD.get_command e) (C_SD.get_edge_name e)
    in ([], tmpval)

  let pre a b = top

  let initialize () = ()
end
