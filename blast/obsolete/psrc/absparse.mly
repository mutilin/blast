%{
module E = VampyreErrormsg
open Ast
(* variable : | variable DOT identifier { Expression.Access(Expression.Dot, $1,$3)}
| variable DOT identifier { Expression.Access(Expression.Dot, $1,$3)} 
| identifier LINDEX constant RINDEX { Expression.Unary(Expression.Deref,Expression.Binary(Expression.Plus,Expression.Symbol ($1),$3))}
 *)

let  parse_error msg =
    E.s (E.error "Parse error at %s: %s" 
           (E.getLineCol !E.current (symbol_start ())) msg)
%}

%token <string> Id Const
%token <int> Num
%token TYPEDEF SUCC VARS 
%token GLOBAL LOCAL ENV STORE PROC 
%token PREDICATES ANNOTATIONS SEMICOLON COLON
%token EOF
%token PROC LPAREN  RPAREN LINDEX RINDEX QUESTION DOT ARROW 
%token  BITAND  BITOR  DIV  LSHIFT  MINUS  MUL 
%token  PLUS  REM  RSHIFT LSHIFT XOR  EQ  GE  GT  LE  LT 
%token  NE  INDEX  AND  OR  COMMA  NOT  BITNOT  DEREF 
%token  ADDRESS  SYMBOL 
%token UMINUS UPLUS DOTSTAR ARROWSTAR
%token ICR DECR SIZEOF
%token ASSIGN MULTassign DIVassign MODassign PLUSassign MINUSassign LSassign RSassign ANDassign XORassign ORassign
%token INT CHAR FLOAT DOUBLE LONG VOID 
%start main

%type <(((string * ((Ast.Expression.expression * bool) list)) list) * ((Ast.Expression.expression * (Ast.Expression.expression * bool) list) list option) * ((Ast.Expression.expression*string) list))> main
%type <(string * ((Ast.Expression.expression * bool) list)) list> typedef
%type <(Ast.Expression.expression * bool) list> data_set
%type <(Ast.Expression.expression * string) list> vars
%%

main:
  TYPEDEF typedef SUCC succ VARS vars EOF {($2,Some($4),$6) }
| TYPEDEF typedef VARS vars EOF      { ($2,None,$4)} 
;

typedef:
  { [] }
| identifier data_set SEMICOLON typedef {($1,$2)::($4)}
;
    
data_set:
    LPAREN ac_data_set RPAREN { $2 }
;

ac_data_set:
  constplus    { [$1] }
| constplus COMMA ac_data_set  {$1::$3}
;

constplus:
  constant     { ($1,false) }
| constant PLUS { ($1,true)}
;

variable:
variable DOT identifier { Expression.Lval (Expression.Access(Expression.Dot, $1,$3))} 
|    identifier { Expression.Lval (Expression.Symbol ($1)) }
;

constant: 
          Num       { Expression.Constant(Constant.Int ($1)) }
        ;

identifier:
          Id { $1 }
;

vars:
    { [] }
|   postfix_expression COLON identifier SEMICOLON vars  { ($1,$3)::$5 }
;

succ:
    { [] }
|   constant ARROW data_set SEMICOLON succ {($1,$3)::$5}

primary_expression:           
  identifier            { Expression.Lval (Expression.Symbol ($1)) }
| constant              { ($1) }
| LPAREN primary_expression RPAREN    { $2  }
    ;
  
  postfix_expression:           
    primary_expression { $1 }
| postfix_expression LINDEX constant RINDEX
    {  Expression.Lval (Expression.Indexed ($1,$3)) }
     
| postfix_expression LPAREN RPAREN
    { failwith "Function call not handled" }
    
| postfix_expression DOT identifier
    { Expression.Lval (Expression.Access(Expression.Dot ,$1, $3)) }
    
    ;
