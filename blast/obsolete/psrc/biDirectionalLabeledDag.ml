(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module gives an implementation of labeled bi-directional DAG.
 * Note that the DAG-ness is not really enforced (except in the iterator)
 * and so the client calling this must make ensure that (if it wants it) ... 
 * [Jhala]
 * Trees created by this module have labeled nodes and labeled edges, and
 * each node has links both to its parents and to its children.  Labels can
 * be modified and children can be added to a node.  However, the parent
 * edge cannot be changed after creation. Hence this modules enforces to
 * construct trees from the root to the leaves.
 *
 * Child removal is not currently supported.
 *)


module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module Command = BlastCSystemDescr.C_Command

type ntype = (int 
	      * (int * int) 
	      * (((Expression.lval, Expression.expression) Hashtbl.t * (Predicate.predicate list)) option))

type etype = Command.t

(* the node type *)
type ('a, 'b) node = {
  (* the label of this node *)
  mutable node_label  : 'a ;
  (* the edge from the parent of this node to this node *)
  mutable parents : (('a, 'b) edge) list;
  (* this node's children *)
  mutable children : (('a, 'b) edge) list;
}
(* the edge type *)
and ('a, 'b) edge = {
  (* the label of this edge *)
  mutable edge_label : 'b ;
  (* the source of this edge *)
  source     : ('a, 'b) node ;
  target     : ('a, 'b) node ;
}

(**
 * Accessor functions.
 *)
let get_source e = e.source 

let get_target e = e.target 

let get_node_label n = n.node_label

let get_parents n = List.map get_source n.parents

let get_children n = List.map get_target n.children

let get_edge_label e = e.edge_label
	
(**
 * Label mutator functions.
 *)
let set_node_label n nl = n.node_label <- nl

let set_edge_label e el = e.edge_label <- el


(**
 * Useful predicates on nodes.
 *)
let has_parent n = n.parents != []

let has_child n = n.children != []


(**
 * Node constructors.
 *)

(* root node creation *)
let create_root nl =
  {    node_label  = nl ;
       parents = [] ;
       children = [] }

let make_link src trg el = 
  { edge_label = el;
    source = src;
    target = trg;
  }

let add_child_edge n e = 
  assert ( n == e.source);
  if (not (List.mem e n.children)) then n.children <- e::n.children

let add_parent_edge n e = 
  assert ( n == e.target);
  if (not (List.mem e n.parents)) then n.parents <- e::n.parents

let hookup_parent_child pn cn el = 
  let e = make_link pn cn el in
    add_child_edge pn e;
    add_parent_edge cn e

let delete_parent_child pn cn = 
  (* we require that pn, cn be connected *)
  assert (List.mem pn (get_parents cn) && List.mem cn (get_children pn));
  let e = List.find (fun x -> x.target = cn) pn.children in
    pn.children <- Misc.list_remove pn.children e;
    cn.parents <- Misc.list_remove cn.parents e
      
(* child node creation *)
let create_child nl el parent =
  let child = { node_label  = nl ;
                parents = [];
                children = [] }
  in
    hookup_parent_child parent child el
 
let delete_children parent = 
  parent.children <- []; ()

(* must be supplied a non-root node else it barfs *)
let delete_child child =
  List.iter 
    (fun parent -> delete_parent_child parent child)
    (get_parents child);
  child.parents <- []

let delete_parent parent =
  List.iter
    (fun child -> delete_parent_child parent child)
    (get_children parent);
  parent.children <- []

let delete_node node = 
  delete_child node;
  delete_parent node

(* i suppose i should test this function -- RJ *)

let collect_component neighbor_fn n_list = 
  let visited_table = Hashtbl.create 31 in
  let rec _cc (n : ('a, 'b) node) = 
    match (Hashtbl.mem visited_table n) with
	true -> ()
      | false ->
	  begin
	    Hashtbl.add visited_table n true;
	    List.iter _cc (neighbor_fn n)
	  end
  in
    List.iter _cc n_list;
    (Misc.hashtbl_keys visited_table)

let connected_component n_list = 
  collect_component 
    (fun x -> ((get_parents x)@(get_children x))) n_list

let descendants n_list = collect_component (fun x -> get_children x) n_list

let ancestors n_list = collect_component (fun x -> get_parents x) n_list  
  

let dag_filter f n = 
  List.filter f (connected_component [n])
    
(** deletes all those nodes that satisfy the predicate f *)
let prune f node_list =
  let to_be_deleted = List.flatten (List.map (dag_filter f) node_list) in
  List.iter (delete_node) to_be_deleted
    
let sources n = dag_filter (fun x -> not (has_parent x))
let sinks n = dag_filter (fun x -> not (has_child x))
		
let dag_map f root = 
  List.map (fun x-> f x.node_label) (descendants [root]) 
    
let output_dag_dot nl_to_string el_to_string  cfa_file n_list = 
   if cfa_file <> "" then
     begin
       let ch = open_out cfa_file in
       let output_edge e = 
	 let n1s = nl_to_string e.source.node_label in
         let n2s = nl_to_string e.target.node_label in
         let es = el_to_string e.edge_label in
             Printf.fprintf ch "  %s -> %s [label=\"%s\"]\n" n1s n2s es 
       in  
let visited_table = Hashtbl.create 31 in
let rec output_node n  =
  let ns = nl_to_string (get_node_label n) in
  match Hashtbl.mem visited_table n with
      true -> ()
    | _ -> 
	begin
		  Hashtbl.add visited_table n true;
		  Printf.fprintf ch "  %s;\n" ns;
		  List.iter
		    (fun e -> output_edge e; output_node e.target)
		    n.children
		end
      in
        Printf.fprintf ch "digraph %s {\n" "main";
	List.iter output_node n_list;
	Printf.fprintf ch "}\n\n";
	close_out ch;
        ()
    end
	    

  
		
(* the main function we are interested in 
   given:
   0) a function to check if a node does not have its value defined yet
   1) a function f : ('a * 'b) list -> 'a, 
   2) leaf_val: 'a for the base cases, the sinks of the dag
   3) a node n in a dag, such that the value of every node is (f (child_val,edge_val) list),
   returns the value for the node n
   Note that this function replaces the node labels
*)
		
let compute_and_update_root_val is_init_fn f leaf_val root  =
  (* should the leaves already have their values assigned ? NO *)
  (* first check that no one has their values assigned *)
  let init_check = ()
(* 
    if (List.exists is_init_fn (descendants root)) 
   then failwith "DAG has some initialized nodes" *)
  in
  let rec _cnv n =
    match (is_init_fn n) with
 	true -> ()
      | false -> 
	  match (n.children) with
	      [] -> n.node_label <- leaf_val n.node_label
	    | children  -> 
		begin
		  List.iter (fun x -> _cnv x.target) children;
		  let kids_arg_list = List.map 
					(fun x -> (x.edge_label,(get_target x).node_label))
					children
		  in
		    n.node_label <- f n.node_label kids_arg_list
		end
  in
    _cnv root
      
						        
						
(* JHALA: to update from here on *)
(**
 * Iterators.
 *)
(* 
let rec iter_descendants f n =
  f n.node_label ;
  List.iter (iter_descendants f) (get_children n)

let rec count_nodes_descendants n =
  1 + Misc.list_add (List.map count_nodes_descendants n.children)

let rec count_f_descendants f n = 
  let addnow = if (f(n)) then 1 else 0 in
    addnow +  (Misc.list_add (List.map (count_f_descendants f) n.children))

let rec iter_ancestors f n =
  f n.node_label ;
  List.iter (iter_ancestors f) (get_parents n)
*)

(* we shall do this as and when we need to! *)
(*

let rec fold_left_descendants f accu n =
  List.fold_left (fold_left_descendants f) (f accu n.node_label) n.children
let rec fold_right_descendants f n accu =
  f n.node_label (List.fold_right (fold_right_descendants f) n.children accu)
let rec fold_left_ancestors f accu n =
  match n.parent_edge with
      None    -> f accu n.node_label
    | Some(e) -> fold_left_ancestors f (f accu n.node_label) e.source

  let rec fold_right_ancestors f n accu =
   match n.parent_edge with
  None    -> f n.node_label accu
  | Some(e) -> f n.node_label (fold_right_ancestors f e.source accu)
  
  
  let rec node_depth node = 
   match node.parent_edge with
  None -> 1
  |  Some(e) -> node_depth (e.source) + 1
   
 let rec path_to_root node = 
   match node.parent_edge with
     None -> [node]
  |  Some(e) -> (path_to_root e.source)@[node]

 let rec subtree_depth n =
   match n.children with
     [] -> 1
  |  l -> 1+ (Misc.list_max (List.map subtree_depth l))
*) 
