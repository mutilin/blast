(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)

{
module E = VampyreErrormsg
open Ql
open Qlparse

let currentLine = ref 0 (* the index of the current line *)
let startLine = ref 0 (* the position in the buffer where the current line starts *)

let currentLexBuf = ref (Lexing.from_string "")

let newline () = 
  incr currentLine ;
  startLine := Lexing.lexeme_start !currentLexBuf

let lexerror msg lexbuf = 
  E.s (E.unimp "%s" msg)


let display_error msg token_start token_end =
  let adjStart = 
    if token_start < !startLine then 0 else token_start - !startLine in
  let adjEnd = 
    if token_end < !startLine then 0 else token_end - !startLine in
  Message.msg_string Message.Error  
    ("[" ^ (string_of_int !currentLine) ^ ":" 
                        ^ (string_of_int adjStart) ^ "-" 
                        ^ (string_of_int adjEnd) 
                  ^ "]"
     ^ " : " ^ msg  );
  flush stderr ;
  exit 1 

let lexicon = StringHashtbl.create 211

let init_lexicon _ =
  StringHashtbl.clear lexicon ;
  List.iter (fun (key, token) -> StringHashtbl.add lexicon key token)
    [ ("if", IF) ;
      ("then", THEN) ;
      ("else", ELSE) ;
      ("print", PRINT) ;
      ("prints", PRINTSTRING)
    ]  

let init ~(inchannel: in_channel) : Lexing.lexbuf =
  init_lexicon () ;
  currentLine := 1; 
  startLine := 0 ;
  let lexbuf = Lexing.from_channel inchannel in
  currentLexBuf := lexbuf ;
  lexbuf


let scan_ident id = try StringHashtbl.find lexicon id
	with Not_found -> Id id


}

let digit    = ['0'-'9']
let letdig   = ['0'-'9' 'a'-'z' 'A'-'Z' '_' '@']
let othersyms =[ '-' '$' '#' '!' '+' '=' 
                '<' '>' ',' '?' '\'']
let alphlet  = ['A'-'Z' 'a'-'z' '_' '$' '@']
let capital  = ['A'-'Z']
let small    = ['a'-'z' '$' '_']
let ws       = [' ' '\009' '\012']
let pathname = ['a'-'z' 'A'-'Z' '0'-'9' '.' '/' '\\' '-']

rule token = parse

    "/*"                { let _ = comment lexbuf in token lexbuf }
  | '\r'                { token lexbuf}
  | '\n'		{ begin
			    E.startNewline (Lexing.lexeme_end lexbuf);
                            newline () ;
			    token lexbuf 
			  end }
  | "//"[^'!''\n']*'\n'
                        { begin
                            E.startNewline (Lexing.lexeme_end lexbuf);
			    token lexbuf
                          end }
  | ws			{ token lexbuf }
  | '('			{ LPAREN }
  | ')'			{ RPAREN }
  | '{'			{ LBRACE }
  | '}'			{ RBRACE }
  | ','			{ COMMA }
  | ';'			{ SEMICOLON }
  | "||"		{ OR }
  | "&&"		{ AND }
  | '-'			{ MINUS }
  | '+'			{ PLUS }
  | '='                 { ASSIGN }
  | "=="                { EQ }
  | "!="                { NE }
  | '>'                 { GT }
  | '<'                 { LT }
  | ">="                { GE }
  | "<="                { LE }

  | '"'                 { (* '"' *)
                          STRING (str lexbuf)
                        }
  | (digit)+	{ let str = Lexing.lexeme lexbuf in
			  let len = String.length str in
			  let zero = Char.code '0' in
			  let rec accum a d =
			    let acc c = a + (d * ((Char.code c) - zero)) in
			    function
			      0 -> let c = str.[0] in
				   if c='~' then - a else (acc c)
			    | i -> accum (acc str.[i]) (d * 10) (i - 1)
			  in
			  Num (accum 0 1 (len-1)) }
  | (alphlet)letdig*	{ scan_ident (Lexing.lexeme lexbuf) }

  | eof			{ EOF }
  | _			{ 
                          begin
                            lexerror ("Illegal Character '" ^ 
                                      (Lexing.lexeme lexbuf)) lexbuf;
			    token lexbuf
			  end }

and comment =
  parse 
    "*/"                { () }
  | '\n'                { newline () ; comment lexbuf }
  | _                   { comment lexbuf }

and str =
  parse
    '"'                   { ""  (* '"' *) }
  | _                     { let cur = Lexing.lexeme lexbuf in cur ^ (str lexbuf) }



