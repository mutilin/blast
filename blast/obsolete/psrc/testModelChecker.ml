(* Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module implements the lazy model checking algorithm.
 *)

open Ast
open BlastArch

module Stats = Bstats
    
module C_System_Descr = BlastCSystemDescr.C_System_Descr
module C_Command = BlastCSystemDescr.C_Command
    
(*module Make_Test_Model_Checker =
  functor(Abstraction : ABSTRACTION) -> 
  struct
  *)


module Abstraction = Abstraction.Make_C_Abstraction(C_System_Descr)
   
(* 
module LazyModelChecker = LazyModelChecker.Make_Lazy_Model_Checker(Abstraction) (* need this for make_shrink_auto *)
*)

module IntSet = Set.Make (struct type t = int let compare a b = b - a end )

module PrioritySet = Set.Make (struct type t = (int * (int * int)) let compare a b = (fst b) - (fst a) end )
    
module Region = Abstraction.Region
    
module Operation = Abstraction.Operation
    
module Tree = BiDirectionalLabeledTree
    
type error_path = (Region.t * (Operation.t list) * Region.t)
      

  (**
   * The time stamp ts of a processed covered node indicates that this node
   * is covered by processed uncovered nodes of time stamp lesser than ts.
   * Conversely, the time stamp ts of processed uncovered node indicates
   * that this node may partially cover any processed covered of time stamp
   * greater than ts.
   *)
type marking_data = {
    mutable time_stamp : int ;
    mutable region     : Region.t ;
  }
      
let print_marking_data fmt md =
  Format.fprintf fmt "@[time_stamp=%d;@ region=" md.time_stamp ;
  Region.print fmt md.region ;
  Format.fprintf fmt "@]"
    
  type marking =
      (* for leaves that have never been processed *)
      Unprocessed of Operation.t
      (* for (already processed) error-free leaves that are covered by
         previously processed (cf. time stamps) nodes *)
    | Processed_Covered of marking_data * (Operation.t list) option
      (* for (already processed) error-free nodes that were not covered
         by previously processed nodes at the time they were last
         processed *)
    | Processed_Uncovered of marking_data
      (* for (already processed) error-free leaves that were covered but
         by previously processed nodes that have been refined, and hence
	 have to be re-processed *)
    | Processed_Was_Covered_To_Reprocess of marking_data
	  
let print_marking fmt = function
    Unprocessed _ ->
      Format.fprintf fmt "@[Unprocessed@]"
  | Processed_Covered (md, ops) ->
      Format.fprintf fmt "@[Processed_Covered(" ;
        print_marking_data fmt md ;
      Format.fprintf fmt ")@]" ;
  | Processed_Uncovered md ->
      Format.fprintf fmt "@[Processed_Uncovered(" ;
      print_marking_data fmt md ;
      Format.fprintf fmt ")@]" ;
  | Processed_Was_Covered_To_Reprocess md ->
      Format.fprintf fmt "@[Processed_Was_Covered_To_Reprocess(" ;
      print_marking_data fmt md ;
      Format.fprintf fmt ")@]" ;
      
type node_data = {
    id           : int ;
    mutable mark : marking ;
    mutable lvalue_map : int ; (* TO DO *)
  }
      
      
let print_node_data fmt nd =
  Format.fprintf fmt "@[Data(@[id=%d;@ mark=" nd.id ;
  print_marking fmt nd.mark ;
  Format.fprintf fmt "@])@]"

type tree_node = (node_data, Operation.t) Tree.node
    
(*  
let tree_node_table = Hashtbl.create 1009
*)
    
  (* helper functions to access a tree node's data *)
let get_node_data n = Tree.get_node_label n
    
let get_op n =
  match (Tree.get_parent_edge n) with
    Some(e) ->
      (Tree.get_edge_label e)
  | None ->
          invalid_arg "get_op: input node has no parent edge"
	
let get_parent n =
  match (Tree.get_parent_edge n) with
    Some(e) ->
      (Tree.get_source e)
  | None ->
      invalid_arg "get_parent: input node has no parent edge"
	
let get_children n = Tree.get_children n
    
let get_id n = (get_node_data n).id
    
let get_marking n = (get_node_data n).mark
    
let get_marking_data n =
  match (get_marking n) with
    Unprocessed _ ->
      invalid_arg "get_marking: input node is unprocessed"
  | Processed_Covered (md, _)
  | Processed_Uncovered md
  | Processed_Was_Covered_To_Reprocess md ->
      md
	  
let get_time_stamp n =
  try (get_marking_data n).time_stamp
  with Invalid_argument "get_marking: input node is unprocessed" ->
    invalid_arg "get_time_stamp: input node is unprocessed"
      
let print_tree_node fmt n =
  Format.fprintf fmt "@[TreeNode(@[label=" ;
  print_node_data fmt (get_node_data n) ;
  begin
    match (Tree.get_parent_edge n) with
      None ->
        Format.fprintf fmt ";@ no parent"
    | Some(e) ->
            Format.fprintf fmt ";@ incoming_op=" ;
        Operation.print fmt (Tree.get_edge_label e) ;
        Format.fprintf fmt ";@ parent_id=%d"
          (get_id (Tree.get_source e))
  end ;
  let children_ids = List.map get_id (Tree.get_children n)
  in
  Format.fprintf fmt ";@ children_ids=" ;
      Misc.list_printer_from_printer Format.pp_print_int fmt children_ids ;
  Format.fprintf fmt "@])@]"
    
    
let get_region n =
  try (get_marking_data n).region
  with Invalid_argument "get_marking: input node is unprocessed" ->
    invalid_arg "get_region: input node is unprocessed"
      
  (* helper class to create nodes with appropriate ids *)
class tree_node_creator =
  object
  val mutable next_id = 0
  method create_root m =
    let this_id = next_id in
    next_id <- next_id + 1 ;
    let rv = ((Tree.create_root { id  = this_id ;
				  mark = m ;
				  lvalue_map = 0 ;
				}) : tree_node) 
    in
    (* Hashtbl.add tree_node_table this_id (ref rv); *)
    rv

  method create_child m op par =
    let this_id = next_id in
    next_id <- next_id + 1 ;
    let rv = 
      ((Tree.create_child { id  = this_id ;
			    mark = m ;
			    lvalue_map = 0 ; 
			  } op par) : tree_node)
    in
    rv
      
	(* Jhala: It would be nice to have a destroy as well -- 
	   or are we relying on some sort of Garbage collection to handle that ? *)
  method delete_child par = 
    Tree.delete_child (par : tree_node)
  method delete_children par = 
    Tree.delete_children (par : tree_node)
      
end

exception RestartException
    
  (* statistic variables *)
let stats_nb_iterations = ref 0
let stats_nb_iterations_of_outer_loop = ref 0 (* added for races *)
let stats_nb_created_nodes = ref 0
let stats_nb_refinment_processes = ref 0
let stats_nb_refined_nodes = ref 0
let stats_nb_proof_tree_nodes = ref 0
let stats_nb_proof_tree_covered_nodes = ref 0
let stats_nb_deleted_nodes = ref 0
let local_restart_counter = ref 1

let reset_stats =
  stats_nb_iterations := 0 ;
  stats_nb_refinment_processes := 0 ;
  stats_nb_created_nodes := 0 ;
  stats_nb_refined_nodes := 0 ;
  stats_nb_proof_tree_nodes := 0;
  stats_nb_proof_tree_covered_nodes := 0
       
let print_stats fmt () =
  Format.fprintf fmt "@[<v>@[Nb iterations of outer while loop:@ %d@]" !stats_nb_iterations_of_outer_loop ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[<v>@[Nb iterations of reachability:@ %d@]" !stats_nb_iterations ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb created nodes:@ %d@]" !stats_nb_created_nodes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb refinment processes:@ %d@]" !stats_nb_refinment_processes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb refined nodes:@ %d@]" !stats_nb_refined_nodes ;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb proof tree nodes:@ %d@]" !stats_nb_proof_tree_nodes;
  Format.fprintf fmt "@ " ;
  Format.fprintf fmt "@[Nb proof tree covered nodes:@ %d@]@]" !stats_nb_proof_tree_covered_nodes;
  Format.fprintf fmt "@[Nb deleted nodes:@ %d@]@]" !stats_nb_deleted_nodes;
  ()

  (************************************************************************************************)
  (** Model checker data types **)
type model_check_outcome =
  | No_error of tree_node
  | Error_reached of (error_path * tree_node)

let global_variable_table = Hashtbl.create 2 (* To keep the abstraction interface happy.
						The global variable table was used by
						the TAR algorithm.
						*)


    (* helper object to create nodes *)
let tnc = new tree_node_creator

    (* the set of pending nodes to be processed *)
let unprocessed_nodes = 
  new Search.testiterator 

    (* the global time used for time stamps *)
let time = ref 0 

    (* the reached region is initially empty *)
let reached_region = ref Region.bot


  (**************************************************************************************************)

  (**************************************************************************************************)


type error_check =
    EmptyErrorAtNode of Region.t * tree_node * (tree_node list)
  | NonEmptyErrorAtRoot of Region.t * tree_node * (tree_node list)
	
  (**
   * Checks whether a reached error region corresponds to a real error
   * path.  This function takes a node and an error region contained in
   * the node's region as arguments, and does a backward pre computation
   * from that node, along the branch from the root to that node, until
   * it gets an empty region or reaches the root.  It returns the obtained
   * region at the reached ancestor along with the path from this ancestor
   * to the initial node.
   * 
   * @param n     a node of the reachability tree
   * @param err_n a non-empty error region contained in n.region
   * @return a value of type error_check
   *)

  (* do the block-trace analysis, this will also add predicates wherever (locations) required 
     of course we want this function to return the same sort of thing that the original (i.e.
     below function returns :: EmptyErrorAtNode(reg,)) *)

let check_error node error_at_node =
  let r_path = Tree.path_to_root node in
  let  _ = Message.msg_string Message.Normal ("counterex. size:"^(string_of_int (List.length r_path))) in
  
  match Options.getValueOfBool "block" with
    true -> 
      begin
	
        let reg_l = 
	  ((List.map (get_region)(Misc.chop_last r_path))@[error_at_node] )
	in
	let id_l = List.map get_id r_path in 
	
	let op_l = List.map (get_op) ( List.tl r_path) in
	match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l op_l) id_l) with
	  (-1,a,_) -> 
		    (* the assumption is that -1 indicates it goes all the way to the top *)
	    let (root::path) = r_path in (* should never fail ! *)
	    NonEmptyErrorAtRoot(a,root,path)
	| (i,a,_) ->
		          (* "i" is the number of hops from the root the falsehood comes *)		     
            if (Options.getValueOfBool "incref" = false ) then	    
              let (n',path) = Misc.list_cut i (r_path) in
              EmptyErrorAtNode(a,n',path)
            else 
 	    begin
                    (* for loop hack *)
                        (* check to see if the path is still infeasible after for loops have
                           been omitted
                           *)
                        (* find for loop patterns *)
             Message.msg_string Message.Normal "For loop hack" ;
	     let loc_array = Array.of_list (List.map Region.get_location_id reg_l) in
	     let loc_array_len = Array.length loc_array in
             let op_array = Array.of_list op_l in
             let op_array_len = Array.length op_array in
     Message.msg_string Message.Normal "The trace is :" ;
     Array.iter (fun l -> let (i,j) = l in 
             Message.msg_string Message.Normal (Printf.sprintf "Location id (%d , %d)" i j) ;
		) loc_array ; 
                        (* a for loop pattern is l - p -> l_1 -> ... -> l - Not(p) -> *)
	      let negations p notp = (* given two predicates, check if
					one is equal or the syntactic negation of the other *)
		let remove_not p = match p with Predicate.Not x -> x | e -> e in
                let x = match p with Predicate.Not _ -> 1 | _ -> 0 in
                let y = match notp with Predicate.Not _ -> 1 | _ -> 0 in
		if (x+y = 1) && ((remove_not p) = (remove_not notp)) then true else false
              in
	      let livevars reg_l op_array = (* find out a list of lvalues that are
					   live at each point in the trace *)
			  (* return an array of length = List.length reg_l, each element in the array
			     is a list of Expression.lvalues *)
		
                let live_array = Array.make loc_array_len ([] : Expression.lval list) in
                          (* populate the array, working backwards *)
		for i = 0 to loc_array_len -2 do
		  let current_index = loc_array_len - 2 - i in
		  let curr_op = op_array.(current_index) in
		  let (used, defined) = (C_System_Descr.reads_and_writes_at_edge curr_op) 
                                          (* get the lvals defined in the current operation *)
                                          (* get the lvals used in the current operation *) 
                  in
	          live_array.(current_index) <- Misc.union used (Misc.difference live_array.(current_index+1) defined) 
                done ;
                          (* reutrn the array *)
                live_array 
		  
              in
                        (* replace each for loop pattern by l - Havoc(dead assignments)-> l - Not(p) -> *)
	      let rec find_for_loop (loc_array, loc_array_len) (op_array, op_array_len) cur_index indices =
                if cur_index > loc_array_len - 2 then indices
                else 
		  begin
		    let curloc = loc_array.(cur_index) in
		    let curop = op_array.(cur_index) in

		    match ((C_System_Descr.get_command curop).C_Command.code) with
                      C_Command.Pred p -> 
			begin
                          let i = ref (cur_index - 1) in
                          let found = ref false in
                          while ((not (!found)) && !i >= 0) do
                            if (loc_array.(!i) = curloc) then
                              begin
	                        match (C_System_Descr.get_command op_array.(!i)).C_Command.code with 
                                  C_Command.Pred p' -> 
                                    if negations p p' then
                                      found := true 
                                    else decr i 
                                | _ -> decr i
                              end 
                            else   
                              decr i ; 
                          done ;
                          if !found then
                            find_for_loop (loc_array, loc_array_len) (op_array, op_array_len) (cur_index+1) 
					  ((!i, cur_index) :: indices)
                          else
                            find_for_loop (loc_array, loc_array_len) (op_array, op_array_len) (cur_index+1) indices
                        end
                    | _ -> find_for_loop (loc_array, loc_array_len) (op_array, op_array_len) (cur_index+1) indices
                  end
			(* replace each for loop *)			
	      in
             let (for_loop_indices : (int * int) list) = find_for_loop (loc_array, loc_array_len) (op_array, op_array_len) 1 [] 
	     in 
             Message.msg_string Message.Normal "For loops are: " ;
             List.iter (fun (i,j) -> Message.msg_string Message.Normal (Printf.sprintf "Location %d to %d" i j)) for_loop_indices ; 
	     let live = livevars reg_l op_array in
             Message.msg_string Message.Normal "Live variables are: " ;
	     for j = 0 to Array.length live -1 do
               Message.msg_string Message.Normal ("Index i="^(string_of_int j)) ;
               List.iter (fun l -> Message.msg_string Message.Normal (Expression.lvalToString l)) live.(j) ;
             done ;
	     let rec replace_fors for_loop_indices live =
                let tmpvars = ref 0 in
                let brand_new_var () = let t = !tmpvars in incr tmpvars ; ("__for_gen_" ^ (string_of_int t)) in
		match for_loop_indices with
		  [] -> (loc_array, op_array)
                | (lbegin, lend) :: rest ->
		  begin
                    let vars = live.(lend+1) in
                    for i = lbegin to lend do
                      let oldcode = ((C_System_Descr.get_command op_array.(i)).C_Command.code)  in
                      let src = C_System_Descr.get_source op_array.(i) in
                      let target = C_System_Descr.get_target op_array.(i) in

		      let newcode = match oldcode with
                        C_Command.Block l ->
                          begin
                            let _xform stmt = 
	                      match stmt with
                                C_Command.Expr (Expression.Assignment (aop, target, e)) ->
                                  if (List.mem target vars) then
                                     stmt
                                  else
                                     C_Command.Expr (Expression.Assignment (aop, target, Expression.Lval (Expression.Symbol (brand_new_var ()))))
                              | _ -> stmt
                            in
                            C_Command.Block 
				(List.map (_xform) l)
                          end 
                      | _ -> oldcode
                      in
                      let newcmd = C_Command.gather_accesses newcode in
                      op_array.(i) <- C_System_Descr.make_edge src target newcmd
                    done ;
                    replace_fors rest live 
                  end
             in 
	     Message.msg_string Message.Normal "Generalizing for loops" ;
             replace_fors for_loop_indices live ;
             let (reg_l', op_l') = (reg_l, Array.to_list op_array) in
			(* run block_analyze_trace to see if resulting path is feasible *)
	      match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l' op_l') id_l) with
		(-1,a,_) -> 
				(* aha : when we generalize the loop, it is feasible! *)
                                (* return the original trace *)
                  Message.msg_string Message.Normal "Aha: Generalized for loop is feasible!" ;
                  let (root::path) = r_path in (* should never fail *)
                  NonEmptyErrorAtRoot (a, root, path)
              | (i,a,_) -> 
				(* If it is still infeasible, simply return the previous infeasibility 
		  let (n',path) = Misc.list_cut i (r_path) in
		  EmptyErrorAtNode(a,n',path)
                  *)
                  let (n',path) = Misc.list_cut i r_path in EmptyErrorAtNode (a,n',path)
		    
                    (* end for loop hack *)
	    end
      end
  | false ->
      begin
	let rec backward_pre n (err_n,subs_err_n) path =
	      match (Tree.get_parent_edge n) with
		Some(e) ->
		    (* f is n's father and op labels the edge f --> n *)
		  let f = Tree.get_source e
		  and op = Tree.get_edge_label e in
		  let f_reg = get_region f in
		      (* the error subregion at f *)
		      (* JHALA: this subregion business is rubbish. It only blows up the
			 formulas. Not required *)
		      (* JHALA: MAJOR CHANGE! dropping the "capping" in the recursive call *)
		  
		    (* JHALA: temporarily comment out!::  let _ = Abstraction.setWPCapRegion f_reg in *)
		  let pre_err_f = (Abstraction.pre err_n op global_variable_table) in
		  let err_f = Region.cap f_reg pre_err_f in
		  let pre_subs_err_f = (Abstraction.spre subs_err_n op global_variable_table) 
		  in

		  let subs_err_f = Region.cap f_reg pre_subs_err_f in
		    (* JHALA: temporarily comment out!:: let _ = Abstraction.resetWPCapRegion () in *)
		      (*let check_emptiness_flag = Abstraction.check_emptiness op in*)
		  if ((*check_emptiness_flag &&*) Stats.time "Emptiness Check:" Region.is_empty subs_err_f)   
		  then
		    begin
			  (*Message.msg_printer Message.Error print_tree_node f;*)
		      EmptyErrorAtNode(err_f, f, n::path)
		    end
		  else
		    backward_pre f (pre_err_f,pre_subs_err_f) (n::path)
		| None ->
		    (* n is the root *)
		    NonEmptyErrorAtRoot(err_n, n, path)
	in
	let prec_err_n = Abstraction.precise error_at_node in
	let rv = backward_pre node (prec_err_n,prec_err_n) [] in
	      (* double checking *)
	      (*
		let _ = 
		begin
		let reg_l = 
		((List.map (get_region)(Misc.chop_last r_path))@[error_at_node] )
		in
		let op_l = List.map (get_op) ( List.tl r_path) in
		match (Stats.time "block_analyze_trace" (Abstraction.block_analyze_trace reg_l) op_l) with
		(-1,a) -> 
			(* the assumption is that -1 indicates it goes all the way to the top *)
		let (root::path) = r_path in (* should never fail ! *)
		NonEmptyErrorAtRoot(a,root,path)
		| (i,a) ->
			(* "i" is the number of hops from the root the falsehood comes *)		     
		let (n',path) = Misc.list_cut i (r_path) in
			let _ = Message.msg_printer Message.Error print_tree_node n' in
		EmptyErrorAtNode(a,n',path)
		end
		in *)
	rv
	  end
	
let check_error_fwd node error_at_node  =
  let rec get_path node path proj_path =
    match (Tree.get_parent_edge node) with
        Some (e) ->
            (* f is node's father and op labels the edge f --> node *)
          let f = Tree.get_source e and op = Tree.get_edge_label e
          in 
          get_path f ((f, Some op):: path) (f::proj_path)
    | None -> (path, proj_path)
  in
  let (error_path, path_projected_on_tree_nodes) = get_path node [ (node, None) ] [ node ]
  in
    Message.msg_string Message.Debug "Check error forward called with" ;
  Message.msg_string Message.Debug "Path = ";
  List.iter (function node -> Message.msg_printer Message.Debug print_tree_node node) path_projected_on_tree_nodes ;
  Message.msg_string Message.Debug "end of path\n\n\n" ;
  let root = match path_projected_on_tree_nodes with
    h :: _ -> h
  | _ -> failwith "Empty error path in check_error_fwd"
  in
  let reg_true = Abstraction.precise (get_region root)
  in
    (* now do a concrete post on the error path *)
  let rec forward_post current_reg path =
      (*Message.msg_string Message.Debug ("\n\nforward post called with region "^(Region.to_string current_reg));
	*)
    match path with
        [] -> (* Valid error *) 
	  let r = (Region.cap (Abstraction.trim_region_to_globals current_reg) error_at_node) in
	  if (Region.is_empty r) then
	    failwith "Rupak: this case not handled yet"
	  else
	    NonEmptyErrorAtRoot (r , root, List.tl path_projected_on_tree_nodes)
    | (f, op) :: rest ->
        begin
          match op with
	    None -> (* reached the node where the error is without trouble *)
		  (* check if satisfiable *)
		  let err_f = Region.cap (Abstraction.trim_region_to_globals error_at_node) 
		      (Abstraction.trim_region_to_globals current_reg) in
		  if (Stats.time "Emptiness Check (Forward):" Region.is_empty err_f) then
		    begin
		      Message.msg_string Message.Debug "Emptiness check succeeds at end of path: this is a false error";
		      check_error f error_at_node
		    end
		  else
		    begin
		      Message.msg_string Message.Debug "Emptiness check fails at end of path: this is a true error";
		      NonEmptyErrorAtRoot ( err_f , root, List.tl path_projected_on_tree_nodes)
		    end
	      |	Some oper ->
		  begin
		    (* do a strongest postcondition computation.
		       if oper is an assume, do a satisfiability check
		       (find predicates if unsatisfiable).
		       otherwise recursively call forward_post on the rest of the path
		       *)
		    Message.msg_string Message.Debug ("Now doing post of "^(Operation.to_string oper)) ;
		    let next_reg = Abstraction.post current_reg oper global_variable_table 
		    in
(*
		    if (Abstraction.check_emptiness oper) then 
*)
		    if (true) then
		      if (Stats.time "Emptiness Check (Forward):" Region.is_empty next_reg) then
			begin
			  Message.msg_string Message.Debug "Emptiness check succeeds: this is a false error";
			  let nd = match rest with
			    (h, _) :: _ -> h
			  | _ -> failwith "strange error path"
			  in
			  try
			    check_error nd (get_region nd)
			  with Invalid_argument _ -> (* This node was marked Unprocessed *)
			    begin (* get region in the tedious way *)
			      let reg = Abstraction.post (get_region (get_parent nd)) (get_op nd) global_variable_table in
			      (get_node_data nd).mark <- 
				 Processed_Was_Covered_To_Reprocess { time_stamp = 0; region = reg } ;
			      check_error nd (reg)
			    end
			end
		      else
			forward_post next_reg rest
		    else
		      forward_post next_reg rest
		  end
            end
    in
    (* forward simulate starting from root and True *)
    forward_post reg_true error_path


  (* Greg: I don't like this get_op... try to add the operations in the path? *)



let rec refine_path anc path =
  match path with
    []   -> ()
  | n::p ->
      stats_nb_refined_nodes := !stats_nb_refined_nodes + 1 ;
      
      Message.msg_string Message.Debug "Refining node:" ;
      Message.msg_printer Message.Debug print_tree_node n ;
      Message.msg_string Message.Debug "whose ancestor is:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
      
      let anc_reg = get_region anc
      and n_marking_data = get_marking_data n in

(*      
      Message.msg_string Message.Debug "calling Abstraction.post with region:" ;
      Message.msg_string Message.Debug ("    region    : " ^ (Region.to_string anc_reg)) ;
      Message.msg_string Message.Debug ("    operation : " ^ (Operation.to_string (get_op n))) ;
*)      
      n_marking_data.region <- Abstraction.post anc_reg (get_op n) global_variable_table;
      
      Message.msg_string Message.Debug "The refined node looks like:" ;
      Message.msg_printer Message.Debug print_tree_node n ;

      refine_path n p
	
	
let update_tree_after_refinment root unprocessed_nodes ts =
  let update_and_compute_bfs n = 
    let nl_int = ref [] in
    let nl_leaf = ref [] in
    let reached_reg = ref Region.bot in      
    let f_int = (fun n -> reached_reg := Region.cup !reached_reg (get_region n)) in
    let f_leaf = (fun n -> nl_leaf := n::(!nl_leaf)) in
    let _ = Tree.traverse_bfs f_leaf f_int n in
    let nl_leaf = List.rev !nl_leaf in
    let _ = Message.msg_string Message.Debug "Leaf Nodes" in
    let _ = List.iter 
	(fun n -> Message.msg_printer Message.Debug  print_tree_node n) 
	nl_leaf
    in
    let process n = 
      let n_data = get_node_data n in 
      match n_data.mark with
	Unprocessed _ ->
		assert (not (Tree.has_child n)) ;
	  begin
	    match (Options.getValueOfString "comp") with
	      "path" -> ();
	    | _ -> 
                	
		unprocessed_nodes#add_element_high n ;
		Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		Message.msg_printer Message.Debug print_tree_node n ;
	  end
      |  Processed_Was_Covered_To_Reprocess md ->
		 (* --- for debugging --- *)
	  assert (not (Tree.has_child n)) ;
	  begin
	    match (Options.getValueOfString "comp") with
	      "path" -> ()
	    | _ -> 	
			 Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		Message.msg_printer Message.Debug print_tree_node n ;
		unprocessed_nodes#add_element_low n ;
		    	 (* Region.cup reached_region md.region  JHALA CHECK CHECK CHECK *)
	  end
      | Processed_Covered (md, _) ->
		(* --- for debugging --- *)
	  assert (not (Tree.has_child n));
	  if (not (Abstraction.covered md.region !reached_reg)) then
            begin
              n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
              Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element_low n 
            end
	  else
            ()
            | Processed_Uncovered md ->
		begin
		  (* it should be the case that this node, oddly enough, has NO children, i.e. this pc has no children,
		     so it doesnt matter if I add it to cover or not. Indeed I shouldnt, and I shouldnt try to add it to 
		     the list of things to process either -- RJ. *)
		  if (Abstraction.enabled_ops (get_region n) <> []) then
		    begin
		      Message.msg_string Message.Normal "About to die";
		      Message.msg_printer Message.Normal print_tree_node n ;
		      failwith "ugh! process_n_leaf gets an internal node!"
		    end
		end
    in
    let _ = List.iter process nl_leaf in
    !reached_reg
  in
  let rec update_and_compute reached_region n =
    let n_data = get_node_data n in
    match n_data.mark with
      Unprocessed _ -> 
	assert (not (Tree.has_child n)) ;
	begin
	  match (Options.getValueOfString "comp") with
	    "path" -> ();
	  | _ -> 	
	      unprocessed_nodes#add_element n ;
	      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	end;
	reached_region
	  
    | Processed_Was_Covered_To_Reprocess md ->
              (* --- for debugging --- *)
        assert (not (Tree.has_child n)) ;
	begin
	  match (Options.getValueOfString "comp") with
	    "path" -> reached_region
	  | _ -> 	
	      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element n ;
	      
	      Region.cup reached_region md.region
	end
    | Processed_Covered (md, _) ->
              (* --- for debugging --- *)
        assert (not (Tree.has_child n)) ;
          if (md.time_stamp >= ts) then
            begin
              n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
              Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element n ;
              reached_region
            end
          else
            Region.cup reached_region md.region
    | Processed_Uncovered md ->
        let new_reached_region = Region.cup reached_region md.region in
        List.fold_left update_and_compute new_reached_region (get_children n)
  in
  match (Options.getValueOfBool "bfs") with
    true -> update_and_compute_bfs root
  | false -> update_and_compute Region.bot root
	  
	  
(* ********************************************************************** *) 


(* GUI hooks *)
(* some functions that dumps the counterexample data structure to a file *)
(* the type of a trace is a (bool * Operation.t list * Region.t list) *)

   let dumptrace (b,op_l,reg_l) =
     let creg_l = List.map Region.concretize reg_l in
     let trace_chan = open_out_bin (Options.getValueOfString "tracefile") in
     let op_string = ref "" in
     let check_o2s x = 
       let (l1,l2,rv) = Abstraction.op_to_string x in
         let s = Printf.sprintf "%d :: %s :: %d \n\n" l1 rv l2 in
	   op_string := s^(!op_string);
           Message.msg_string Message.Normal (Printf.sprintf "%d :: %s :: %d \n" l1 rv l2) ; 
  	   (l1,l2,rv) 
     in
       Marshal.to_channel trace_chan (b,List.map check_o2s op_l,List.map Abstraction.reg_to_strings creg_l) [];
       close_out trace_chan;
       Misc.append_to_file "traces" ("Start trace \n "^(!op_string)^"End trace\n");
       ()

  let dumppath node region_at_node =
    let _ = Message.msg_string Message.Error "in dumppath" in
    let p_to_root = Tree.path_to_root node in
    let list_of_regs = (List.map (get_region)  (Misc.chop_last p_to_root))@[region_at_node] in
    let list_of_ops = List.map (get_op) ( List.tl p_to_root) in
    dumptrace (false, list_of_ops, list_of_regs)


let dump_trace_to_node n n_region genuine =
   let n_data   = (Tree.get_node_label n) in
   let path_to_root = Tree.path_to_root n in
   let list_of_regs = (List.map (get_region)  
			 (Misc.chop_last path_to_root))@[n_region] in
   let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
     dumptrace (genuine, list_of_ops,list_of_regs)


let print_whole_tree msg_state root = 
  Message.msg_string msg_state "Printing Reachability Tree";
  let f n = Message.msg_printer msg_state print_tree_node n
  in
  let rec _ac_print n = 
    f n; 
    List.iter _ac_print (Tree.get_children n)
  in
    _ac_print root
      

let demo_process n n_region genuine =

 match (Options.getValueOfBool "demo" (*|| genuine *)) with
      true ->
	begin
	  let s = if genuine then "Genuine" else "Spurious" in
	  let outs = Printf.sprintf "*** %s Counterexample Found!***" s in
	    Message.msg_string Message.Normal outs;
	    dump_trace_to_node n n_region genuine;
	Message.msg_string Message.Error "waiting to hear from gui";
	let _ = input_line stdin in
	  Message.msg_string Message.Error "heard from gui!";
	  ()
 end 
| false -> ()


	
(*******************************************************************************)
(*******************************************************************************)
(*******************************************************************************)
(* vanilla *)

(*******************************************************************************)
(*******************************************************************************)
  (* Parallel model checker.
     Instead of assume guarantee checking, just check all interleavings.


     *)
  let model_check tree_root err =
    Message.msg_string Message.Debug "Entering parallel_model_check" ;
    

    (* the final result of the search will be stored here *)
    let resulting_error_path = ref None
    and offending_error_node = ref None
      
    (* this boolean variable allows to stop the while-loop *)
    and continue = ref true
  in



   

    (* The function for constructing children .. and adding them to the tree *)
    let construct_children node accu op =
      (tnc#create_child (Unprocessed (op) ) op node)::accu
    in				 
						 
    let do_subtree_cut_refinement anc new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      anc_marking_data.region <- new_anc_region ;
      
      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
        (* Note that here there is no path to update as the entire subtree is going to be chopped off... *)
      Message.msg_string Message.Debug "Now deleting subtree:" ;
      tnc#delete_children anc;
      unprocessed_nodes#empty_out ();
      (* Jhala: I'm assuming that its ok to just empty this out and fill it back up when we are traversing the tree later ... this whole traversal business is not good by the way, there should be a much more incremental way to do it *)
      Message.msg_string Message.Debug "Now re-adding children:" ;
      let children = List.fold_left (construct_children anc) [] 
	  (Abstraction.enabled_ops new_anc_region) in
      Message.msg_string Message.Debug "The children are:" ;
      List.iter (function child -> Message.msg_printer Message.Debug print_tree_node child) children ;

       (* add the children to the set of pending nodes *)
      (* Jhala: This should all be done by the the subtree cut 
	 Message.msg_string Message.Debug "Adding the children to the set of pending nodes" ;
	 unprocessed_nodes#add_list children ;
	 *)
      (* add n's region to the reached region *)
      Message.msg_string Message.Debug "Updating the currently reached region" ;
      reached_region := Stats.time "update_tree_after_refinement[CUT]" 
	   (update_tree_after_refinment tree_root unprocessed_nodes) anc_marking_data.time_stamp ;
      (* mark n as processed and uncovered *)
      Message.msg_string Message.Debug "Updating the node's marking" ;
      let updated_marking_data = {anc_marking_data with region = Region.update_lattice_in_region anc_marking_data.region !reached_region} in
      anc_data.mark <- Processed_Uncovered updated_marking_data ;
      Message.msg_string Message.Debug "This node now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc
    in


    (* the refinment process *)
    let do_refinment anc path new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      Message.msg_string Message.Debug "Updating the ancestor's region..." ;
      anc_marking_data.region <- new_anc_region ;
      
      Message.msg_string Message.Debug "This ancestor now looks like:" ;
      Message.msg_printer Message.Debug print_tree_node anc ;
      
        (* update the path *)
      Message.msg_string Message.Debug "Refining the nodes along the path..." ;
      refine_path anc path ;
      
        (* update the tree and the reached region *)
      Message.msg_string Message.Debug "Updating the markings and the reached region..." ;
      reached_region := Stats.time "update after refine [PATH]" (update_tree_after_refinment tree_root
								   unprocessed_nodes) anc_marking_data.time_stamp

    in

    (* this function deletes covered nodes from the tree *)
    let rec reclaim n = 
      if (Tree.has_child n || (not (Tree.has_parent n))) then ()
      else 
	begin
	  let par = get_parent n in
	  Tree.delete_child n; 
	  reclaim par
	end
    in

      while (!continue && unprocessed_nodes#has_next ()) do
        stats_nb_iterations := !stats_nb_iterations + 1;
	Ast.check_time_out () ;

(*
	if (Random.int 1000 < 2) then
	    Abstraction.reset () 
	else ();
*)

        (* the search is not finished yet *)
	if (!stats_nb_iterations mod 100 == 0) then
	  begin
            Message.msg_string Message.Normal ("While loop tick:"^(string_of_int !stats_nb_iterations));
	    flush stdout; flush stderr;
	    Abstraction.print_abs_stats ()
	  end ;
	Message.msg_string Message.Debug "\n\n****************************************************************************\n" ;
        Message.msg_string Message.Debug "Next iteration of model-check's big while-loop" ;
	Message.msg_printer Message.Debug Format.pp_print_int (!stats_nb_iterations);
        (* Message.msg_string Message.Debug "Currently reached region:" ;
           Message.msg_printer Message.Debug Region.print !reached_region ;*)

        (* let's process the next pending node *)
        let n        = unprocessed_nodes#next () in
          Message.msg_string Message.Debug (*Debug Normal*) "Now processing tree node:" ;
          Message.msg_printer Message.Debug (*Debug Normal*) print_tree_node n ;

          let n_data   = (Tree.get_node_label n) in

          let n_marking_data = 
            match n_data.mark with
                Unprocessed _ ->
                  let op = (get_op n) in
                    { time_stamp = (time := !time + 1 ; !time) ;
                      region = Stats.time "main post" (Abstraction.post (get_region (get_parent n)) op)  global_variable_table; }
              | Processed_Was_Covered_To_Reprocess md ->
                  md
              | _ -> (print_string "LazyModelChecker.model_check: unexpected marking data" ;
		  failwith "LazyModelChecker.model_check: unexpected marking data")

          in
            (* --- for debugging --- *)
            (* assert (n_data.mark == Unprocessed) ; *)

          let n_region = n_marking_data.region in

          (* the error region at this node *)
          let err_n = Stats.time "cap" (Region.cap n_region) err in
            Message.msg_string Message.Debug ("Error region at this node: " ^ (Region.to_string err_n)) ;

            if (Region.is_empty err_n)
            then
              begin
                (* no error found at this node *)
                Message.msg_string Message.Debug "No error found at this node" ;

                (* let's test whether this node is covered *)
                Message.msg_string Message.Debug "Let's test whether this node is covered" ;
		let coverable =  
		  if (Options.getValueOfBool "cov") 
		  then Abstraction.coverable n_region
		  else true
		in
		let covered = 
		  if coverable
		  then (Stats.time "Covered check:" (Abstraction.covered n_region) !reached_region)
		  else false
		in
		if (covered)
                then
                  begin
                    (* this node is covered *)
		    Message.msg_string Message.Debug "This node is covered" ;
			
                    (* mark n as processed and covered *)
		    n_data.mark <- Processed_Covered (n_marking_data, None);
		    if (Options.getValueOfBool "reclaim") 
		    then
		      reclaim n;
		    ()
                  end
		else
		  begin
                    (* this node is not covered *)
                    Message.msg_string Message.Debug "This node is not covered" ;
                    Message.msg_string Message.Debug "Constructing its successor children..." ;
		    
                    (* we construct its successor children *)
		    
                    let children = List.fold_left (construct_children n) []
                        (Abstraction.enabled_ops n_region) 
		    in
		    
                    Message.msg_string Message.Debug (*Normal*) "The children are:" ;
                    List.iter (function child -> Message.msg_printer Message.Debug (*Normal*) print_tree_node child) children ;
		    
                      (* add the children to the set of pending nodes *)
                    Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
                    unprocessed_nodes#add_list children ;
		    Message.msg_string Message.Debug ("Remaining nodes:"^(string_of_int (unprocessed_nodes#sizeof ()))); 
		    unprocessed_nodes#iter (fun x -> Message.msg_printer Message.Debug print_tree_node x);
	            
                      (* add n's region to the reached region *)
			Message.msg_string Message.Debug "Updating the currently reached region" ;
			reached_region := Region.cup !reached_region n_region ;
                      (* mark n as processed and uncovered *)
                    let updated_marking_data = {n_marking_data with region = Region.update_lattice_in_region n_region !reached_region} in
 
                    Message.msg_string Message.Debug "Updating the node's marking" ;
                    n_data.mark <- Processed_Uncovered updated_marking_data ;
		    
                    Message.msg_string Message.Debug "This node now looks like:" ;
                    Message.msg_printer Message.Debug print_tree_node n 
		  end
              end
            else
              begin
                (* error found *)
                Message.msg_string Message.Debug "Error found : checking validity." ;
		Message.msg_string Message.Debug ("Error at depth"^(string_of_int (Tree.node_depth n))); 
	(*	List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) (Tree.path_to_root n) ; *)
		if (Options.getValueOfBool "cref") 
                then 
		  begin
		    let path_to_root = Tree.path_to_root n in
		    let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
		    let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
		    (* note that list_of_regs should be 1 longer than list of ops *)
		    let _ = 
		      try 
			Message.msg_string Message.Debug ("Found error length: "^(string_of_int (List.length list_of_regs)));
			Abstraction.analyze_trace list_of_regs list_of_ops
		      with fail_exception ->  
			begin
			  List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x)
			    path_to_root;
			  print_string "Uh Oh! Error in analyze_trace!";
			  raise fail_exception
                        end 
		    in
		    stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
		    do_subtree_cut_refinement tree_root (get_region tree_root)
		      
                  end
                else 
                  begin
                    Message.msg_string Message.Debug "Error found at this node" ;
		    
                    (* this node will have to be reprocessed after refinment *)
		    (* JHALA: why will this node have to be reprocessed after refinement ? *) 
                    Message.msg_string Message.Debug "Re-adding this node to the set of pending unprocessed nodes" ;
                    n_data.mark <- Processed_Was_Covered_To_Reprocess n_marking_data ;
		    Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		    Message.msg_printer Message.Debug print_tree_node n ;
                    unprocessed_nodes#add_element n ;
		    
                (* let's now test whether this error is a real one *)
                    Message.msg_string Message.Debug "Testing whether this error is a real one" ; 
		    let check_refinement = 
(*
		      if (Options.getValueOfString "refine" = "fwd") then
			check_error_fwd 
		      else *)
			check_error
		    in
		      try 
			match (Stats.time "check_error" (check_refinement n) err_n) with
			    EmptyErrorAtNode(err_anc, anc, path) ->
				(* this is a false error *)
			      begin
				Message.msg_string Message.Debug "This is a false error (seq)" ;
				Message.msg_string Message.Debug "The check_error function ended up with" ;
				Message.msg_string Message.Debug "     at ancestor: " ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
				demo_process n n_region false;
				
				if (Options.getValueOfBool "stop") then
				  failwith ("FALSE ERROR CHOKE");
				(* let's refine *)
				Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path" ;
				if (Options.getValueOfBool "traces") then dumppath n n_region;
				
			      	let anc_region = (get_region anc) in
				let focused_anc_region = 
				  try
				    let rv = 
				      match Options.getValueOfBool "block" with
					  false ->
					    Stats.time "focus" (Abstraction.focus anc_region) err_anc 
					| true -> Abstraction.focus_stamp anc_region 
					    (* analysis is already done! *) 
				    in
				      if (Options.getValueOfBool "localrestart") then
					local_restart_counter := !local_restart_counter + 1;
				      if (Options.getValueOfBool "restart" || ( Options.getValueOfBool "localrestart" && !local_restart_counter mod 15 == 0)) then 
					begin
					  Message.msg_string Message.Error "About to Restart";
					  					  
					  raise RestartException
					    
					end
					else 
					  begin
					    Options.setValueOfBool "localrestart" false;
					    rv
					  end
				  with Abstraction.NoNewPredicatesException ->
				    begin
 				      if (Options.getValueOfInt "predH" = 3) then
					begin
					  let path_to_root = Tree.path_to_root n in
					  let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					  let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
					  Message.msg_string Message.Debug 
					    ("\n No of regs: "^(string_of_int (List.length list_of_regs)));
					  Message.msg_string Message.Debug 
					    ("\n No of ops : "^(string_of_int (List.length list_of_ops)));
					  try 
					    (Abstraction.find_all_predicates list_of_regs list_of_ops; 
					     raise RestartException
					       )
					  with e -> 
					    
					    begin
					      if (e = RestartException) then raise e 
					      else 
						begin
						  Message.msg_string Message.Minor "This is a false error" ;
						  Message.msg_string Message.Minor "The check_error function ended up with" ;
						  Message.msg_string Message.Minor ("     an empty error: " ^ (Region.to_string err_anc)) ;
						  Message.msg_string Message.Minor "     at ancestor: " ;
						  Message.msg_printer Message.Minor print_tree_node anc ;
						  Message.msg_string Message.Minor "     along the path (from the ancestor to the current node):" ;
						  Message.msg_printer Message.Minor  print_tree_node anc ;
						  List.iter (function x -> Message.msg_printer Message.Minor print_tree_node x) path ;
						  Message.msg_string Message.Debug ("find_all_predicates raises : "^(Printexc.to_string e)) ; 
						  dumptrace (false, list_of_ops, list_of_regs);
						  failwith ("problem in find_all_predicates !") 
						end
					    end
					end
				      else 
					if (Options.getValueOfInt "predH" = 1 && 
					    (not (Abstraction.predicate_fixpoint_reached ()))) 
					then (Message.msg_string Message.Debug "Raising Restart"; raise RestartException)
					else
					  begin
					    let path_to_root = Tree.path_to_root n in
					    let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					    let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
					    dumptrace (false, list_of_ops, list_of_regs);
					    raise Abstraction.NoNewPredicatesException
(*
					      failwith ("No new preds found !-- and not running allPreds ...")
*)
					  end
				    end
				in (* for the focused_anc_region *)
				Message.msg_string Message.Debug "Done calling Focus";
				Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
				
				stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;
				begin
				match (Options.getValueOfString "comp") with
				    "path" -> 
				      begin
					Stats.time "do_ref [PATH]" (do_refinment anc path) focused_anc_region ;
					Message.msg_string Message.Error "PATH"; 
					Message.msg_string Message.Debug "The refined path looks like:" ;
					Message.msg_printer Message.Debug print_tree_node anc ;
					List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
				      end
				  | _ -> Stats.time "subtree_cut_refinement" (do_subtree_cut_refinement anc) focused_anc_region;
			      end
 end
		| NonEmptyErrorAtRoot(err_root, root, path) ->
			    (* --- for debugging --- *)
			    begin
			    assert (root == tree_root) ;
			    (* this is a real error *)
			    (* we put the error path in the appropriate variable *)
			
                              resulting_error_path := Some(err_root,
							   List.map get_op path,
							   err_n) ;
			      offending_error_node := Some (Misc.list_last path) ;
                              (* print the unknown functions on the path *)
			      Abstraction.print_debug_info (List.map get_op path);
			      demo_process n n_region true;
			      
                              (* and break the while-loop *)
                              continue := false
			    end
		      with RestartException -> 
			begin
			  stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
			  do_subtree_cut_refinement tree_root (get_region tree_root)
			end
		  end
	      end
      done ;
    Message.msg_string Message.Debug ("Depth of tree: "^(string_of_int (Tree.subtree_depth tree_root)));
    print_whole_tree Message.Debug tree_root;
    match (!resulting_error_path) with 
      Some ((_,_,err_n) as e_path) -> 
	begin
	  let err_node = 
	    match !offending_error_node with
		    Some n -> n
	    | None -> failwith "Quit while w/ error loop w/o setting offending_error_node!"
	  in
	  let path_to_root = Tree.path_to_root err_node in
	  let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[get_region err_node] in
	  let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
	  dumptrace (true,list_of_ops,list_of_regs);
	  Error_reached (e_path, tree_root)
	    
	end
     | None -> (* print the tree *) 
        begin
          No_error tree_root
        end



let load_tests () =
  let fname = Options.getValueOfString "mainsourcename" in
  let testfname = fname ^".tst" in
  Message.msg_string Message.Normal ("Initializing tests from "^testfname) ;
  let ic = open_in testfname in
  let (arr, index, sinfomap, liveht, numlive, deadht, numdead, numtests) =
    Marshal.from_channel ic in
  (arr, index, sinfomap, liveht, ref numlive, deadht, ref numdead, ref numtests)
    


let test_model_check  initialState errorPred seedPreds =
(*
  Abstraction.assumed_invariants := Stats.time "read invariants" constructInvariants();  
  Abstraction.assumed_invariants_region := This_C_Abstraction.assumed_invariants_list_to_region();
  *) 
  
  let mainFunctions = Options.getValueOfStringList "main" in
  let _ = Abstraction.initialize_abstraction () in
  let test_vector_file = Options.getValueOfString "testfile" in
  let test_vector_channel = open_out (test_vector_file^"-tests") in
  (* let _ = Message.msg_string Message.Normal "Done adding seed predicates" in*)
  
  (* Message.msg_string Message.Normal "********** Now running the tester **********" ;
  flush stdout;*)
  
  let init_region =
    let rec buildloclist lst loclist =
      match lst with [] -> loclist
      | a ::rest -> 
	  try
	    buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist) 
	  with 
	    Not_found -> 
	      (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
	       failwith ("No function "^(a)^" defined"))
    in
    let loclist = buildloclist mainFunctions [] in
    Abstraction.create_region 
      loclist false seedPreds initialState
  in	
  begin
    
    (* create an array of priority -> location option, and a map location -> priority *)
    let sinfo_map = ref (Hashtbl.create 511) in
    let get_sinfo loc =
      try
	Hashtbl.find (!sinfo_map) (C_System_Descr.location_coords loc)
      with
	_ -> Hashtbl.add (!sinfo_map) (C_System_Descr.location_coords loc) (-1) ; -1
    in
    (* depth-first number the locations of the call graph *)
    let toposort_counter = ref 0 in
    let temp_list = ref [] in
    let rec toposort loc =
      let prio = get_sinfo loc in
      if prio >= 0 then
	() (* already visited *)
      else
	begin
	  Hashtbl.replace (!sinfo_map) (C_System_Descr.location_coords loc) (!toposort_counter) ; (* currently visiting *)
	  temp_list := (!toposort_counter, loc) :: !temp_list ;
	  incr toposort_counter ;
	  (* visit children *) 
	  let succs = C_System_Descr.get_outgoing_edges loc in
	  (* handle function calls in edges *)
	  List.iter 
	    (fun e -> 
	      let c = C_System_Descr.get_command e in 
	      match c.C_Command.code with 
		C_Command.FunctionCall exp ->
		  begin
		    match exp with
		      Expression.Assignment(Expression.Assign, _, Expression.FunctionCall(e,_) ) 
		    | Expression.FunctionCall (e, _) ->
			begin
			  match e with
			    Expression.Lval (Expression.Symbol fname) ->
			      begin
				try
				  let fname_start = (C_System_Descr.lookup_entry_location fname)
				  in
				  toposort fname_start
				with e -> ()
			      end
			  | _ ->
			      if (Options.getValueOfBool "nofp") 
			      then 
				(Message.msg_string Message.Error "Warning: Ignoring function pointer call") 
			      else
				begin 
				  failwith ("Function pointer call: " ^ (Expression.toString exp)) ;
				end ;
			      ()
				
			end
		  end
	      |	_ -> ()
		    )
	    succs ;
	  List.iter 
	    (fun e -> 
	      toposort (C_System_Descr.get_target e) ) 
	    (succs) ;
	  
	  ()
	end
    in
    let startloc =  (C_System_Descr.lookup_entry_location (List.hd mainFunctions)) in
    


    let (priority_to_loc_array, index, smap, live, live_locs, dead, dead_locs, num_tests) =
      if (Options.getValueOfBool "loadtest") then 
	load_tests () 
      else
	begin
	  toposort startloc ;
    
        (* priority_to_loc_array is an array mapping priorities to locations *)
	  let p_to_loc_array = Array.make (!toposort_counter) (None) in
	  List.iter (fun (p, l) -> p_to_loc_array.(p) <- Some l ) !temp_list ;
	  temp_list := [] ; (* clear space *)
	  
	  (p_to_loc_array, !toposort_counter,
           !sinfo_map,  
                                (* Initialize two sets: dead, live *)
	   Hashtbl.create 1009, (* live *)
	   ref 0,               (* live locs *)
	   Hashtbl.create 1009, (* dead *)
	   ref 0,               (* dead locs *)
	   ref 0 )
	end
    in

        (* At this point toposort is the number of locations marked, since we started at zero *)
    let num_locs = Array.length priority_to_loc_array in
    toposort_counter := index ;
    sinfo_map := smap ;

    let dump_tests () =
      let trace_chan = open_out_bin ((Options.getValueOfString "mainsourcename")^".tst") in
      Marshal.to_channel trace_chan 
        (priority_to_loc_array, !toposort_counter, !sinfo_map, live, !live_locs, dead, !dead_locs, !num_tests) [];
      close_out trace_chan;
      ()
    in
    
    
    

    unprocessed_nodes#set_interesting 
      (fun e ->
	match (get_node_data e).mark with 
	  Unprocessed op ->
            let target_loc_id = C_System_Descr.location_coords (C_System_Descr.get_target op) in
            not (Hashtbl.mem live target_loc_id)
	| Processed_Covered (md,_) 
	| Processed_Uncovered md 
	| Processed_Was_Covered_To_Reprocess md ->
	    begin
	      try
		let reg = md.region in
		let (i1,i2) = Region.get_location_id reg in
		not (Hashtbl.mem live (i1,i2))
	      with _ ->
                true
	    end
	      ) ;
    
    if (priority_to_loc_array.(0) = None) then () else
    begin
      Hashtbl.add live (C_System_Descr.location_coords startloc) true ;
      priority_to_loc_array.(0) <- None ;
      incr live_locs ;
    end ;
    
    (* the root of the reachability tree *)
    let tree_root =
      tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init_region ; })
    in
    
    let flush_tree () =
      let tree_root =
	tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = 0 ;
                                                              region = init_region ; })
      in
      time := 0 ;
      unprocessed_nodes#empty_out () ;
      unprocessed_nodes#add_element (tree_root) ;
      reached_region := Region.bot
(*      tnc#delete_children tree_root *)
    in
    
    (* add the root to the pending unprocessed nodes *)
    unprocessed_nodes#add_element (tree_root) ;
    
    (* toposort_counter is now used as the current node to be tested *)
    let iteration_count = ref 0 in 
    

    let get_next () =
      let continue = ref true in 
      while (!toposort_counter > 0 && !continue) do
	match priority_to_loc_array.(!toposort_counter) with
	  Some loc -> (* found the next one *)
	    continue := false 
	| None -> decr toposort_counter
	done ;
      if (!continue ) then
	    (* found something *)
	raise Not_found
    in

    let anymore = ref true in
    (* skip some *)
    begin
      try
	let i = ref 0 in
	while !i< (Options.getValueOfInt "skiptest") do
	  incr i ;
	  decr toposort_counter ;
	  get_next () ;
	done 
      with Not_found -> print_string "HRERERE" ; anymore := false ;
    end;

    Message.msg_string Message.Normal "********** Running the tester's while loop **********" ;

    (* big test while loop until get_next raises Not_found *)
    while (!anymore) do 
      Message.msg_string Message.Normal "\n\n\n***** Running the next iteration of the tester's while loop *******\n" ;
      Message.msg_string Message.Normal (Format.sprintf "Iteration Number %d \n" !iteration_count );
      Message.msg_string Message.Normal (Format.sprintf "Number of locations %d \n" num_locs );
      Message.msg_string Message.Normal (Format.sprintf "Number of live locations found %d \n" !live_locs );
      Message.msg_string Message.Normal (Format.sprintf "Number of dead locations found %d \n\n" !dead_locs );
      dump_tests () ;

      incr iteration_count ;
      decr toposort_counter; 
      if (Random.float 1.0 < 0.2) then
	Abstraction.reset () ; (* random restart *)
      try 
	get_next () ;
	try
	  begin
	    let is_after_return loc =
	      (* check if this location is immediately after a return statement *)
	      let incoming = C_System_Descr.get_ingoing_edges loc in
	      List.for_all
		(fun e -> 
		  let cmd = C_System_Descr.get_command e in
		  match cmd.C_Command.code with
		    C_Command.Block l -> List.exists (fun x -> match x with C_Command.Return _ -> true | _ -> false) l
		  | _ -> false
			)
		incoming 
	    in
	    let error_loc =
	      match priority_to_loc_array.(!toposort_counter) with
	      | Some err_loc ->
		  err_loc
	      | None -> failwith "testModelCheck: Something wrong with get_next"
	    in 
	    Message.msg_string Message.Normal (Format.sprintf "Checking reachability of %s \n" (C_System_Descr.location_to_string error_loc) );
            let error_region =
	      (Abstraction.create_region [error_loc] true errorPred errorPred)
            in
	    if (is_after_return error_loc) then
		(* get the easy case: the location immediately following a return statement can
		   never be reached 
		   *)
              begin
                (* 
                Message.msg_string Message.Normal "\n\nNo error found. Location unreachable.\n" ;
		*)
		Hashtbl.add dead (C_System_Descr.location_coords error_loc) true ;
		incr dead_locs ;
              end
            else
              begin
		(* take the current tree and intersect it with the error region *)
                let intersect_error_region tree_root err_reg =
		  let rec _traverse nlist =
		    Message.msg_string Message.Debug "In _traverse " ;
                    match nlist with
                      [] -> ()
                    | n :: t ->
			begin
			  Message.msg_string Message.Debug "(In intersect_error_region) Current node is " ;
			  Message.msg_printer Message.Debug print_tree_node n ;
			  let mking = get_marking n in
			  match mking with 
			    Unprocessed _ -> _traverse t
			  | Processed_Covered (md, _)
			  | Processed_Was_Covered_To_Reprocess md
			  | Processed_Uncovered md ->
			      (* region at this node is md.region *)
			      let r_cap_err = Region.cap md.region err_reg in
			      if Region.leq r_cap_err Region.bot then
				(* does not intersect error region *)
				_traverse ( List.append t (get_children n) )
			      else
				begin
				  Message.msg_string Message.Debug "Deleting children of offending node " ;
				  tnc#delete_children n ;
				  (Tree.get_node_label n).mark <- Unprocessed (get_op n) ;
				  _traverse t
				end
			end
		  in
		  _traverse [tree_root ] ;
		  tree_root
		in
		(* update tree and reached region *)
		Message.msg_string Message.Debug "Now intersecting the current tree with the error region " ;
		print_whole_tree Message.Debug tree_root ;
		Message.msg_string Message.Debug "Now calling intersecting error region " ;
		intersect_error_region tree_root error_region ;
		Message.msg_string Message.Debug "Done. New tree is: " ;
		print_whole_tree Message.Debug tree_root ;
		Message.msg_string Message.Debug "Now updating reached region " ;
                unprocessed_nodes#empty_out ();
		reached_region := 
		   update_tree_after_refinment 
		     tree_root unprocessed_nodes 
		     ((get_marking_data tree_root).time_stamp) ;
(*
		Message.msg_string Message.Debug "Updated reached region is " ;
                Message.msg_string Message.Debug (Region.to_string !reached_region) ;
*)
		(* invariant: reached_region cap error_region is empty *)

		(* call model check on the new partial tree *)
                let mc_return_val =
		  try
		    Ast.set_time_out_signal () ;
		    (model_check tree_root error_region)
		  with Abstraction.NoNewPredicatesException ->
		    begin
		      Ast.reset_time_out_signal () ;
		      Message.msg_string Message.Error ("Exception raised by model_check!: No new predicates found.");
		      raise Abstraction.NoNewPredicatesException
		    end
		  | Ast.TimeOutException -> 
		      begin
			Ast.reset_time_out_signal () ;
			Message.msg_string Message.Error ("Exception raised by model_check!: Time out.");
			raise Ast.TimeOutException 
		      end
		in 
		Message.msg_string Message.Error "Withdrawing handler " ; 
		Ast.reset_time_out_signal () ;

		match (mc_return_val) with
		  No_error troot ->
		    let rec expand_dead_locs error_loc =
		     (* troot is the root of a complete (ie closed under posts) *)
		     (* if this tree does not contain some location, then that location
		        is also not reachable 
			*)
                      begin
		      (* This is expensive at the moment. Needs optimization *)
		      let make_set () = 
			let set_of_locs = ref PrioritySet.empty in 
			for i = 1 to !toposort_counter - 1 do
			  match priority_to_loc_array.(i) with
			    None -> ()
			  | Some l -> set_of_locs := PrioritySet.add (i,(C_System_Descr.location_coords l)) !set_of_locs
			done ;
			set_of_locs 
		      in
		      let set_of_locs = make_set () in
		      (* traverse the tree, dropping each location in the tree from set_of_locs *)
		      let f = (fun n -> 
			 try  
			  let r = get_region n 
			  in 
			  if Region.leq r Region.bot then () else
                          begin 
                            let l = try Region.get_location_id r with e -> print_string "SAY" ; print_string (Printexc.to_string e) ; raise e in
			    let prio = try Hashtbl.find (!sinfo_map) l with e -> print_string ("HERE IS THE TROUBLE"^(Printexc.to_string e) ) ; raise e in
                            set_of_locs := PrioritySet.remove (prio,l) !set_of_locs
                          end
			with e -> print_string "HMMMM" ; print_string (Printexc.to_string e) ; raise e  )
		      in
		      let _ = try
			Tree.traverse_bfs f f tree_root 
		      with e -> (Printexc.to_string e ; (failwith "Trouble in tree traverse" ) )
                      in
		      (* everything left in set_of_locs is unreachable *)
		      PrioritySet.iter (fun (p, l) -> (* print_string "Aha: dropping " ; print_int p ; print_newline () ; *) priority_to_loc_array.(p) <- None ; Hashtbl.add dead l true ; incr dead_locs ) !set_of_locs ;
                      
		      ()
                      end
		    in
		    (* Message.msg_string Message.Normal "\n\nNo error found. Location unreachable.\n" ; *)
		    Hashtbl.add dead (C_System_Descr.location_coords error_loc) true ;
		    incr dead_locs ;
		(* expand as far as possible *)
		    expand_dead_locs error_loc 
		| Error_reached ((reg_i, op_list, reg_e), troot) ->
		    (* print_string "Here" ; *)
		    let expand_error_path (reg_i, op_list, reg_e) =
		  (* should get the state at the end of the error path from the constraints *)
		  (* troot is the root of a possibly incomplete tree *) 
		      (* flush_tree () ; *)
		      ()
		    in
		    (* Message.msg_string Message.Normal "\n\nError found! The system is unsafe :-(\n" ; *)
		(* Add all locations in the error trace to the live hash table *)
		(* set all these locations to null in the priority_to_loc_array *)
		    incr num_tests ;
                  
 
		    if (Hashtbl.mem live (C_System_Descr.location_coords error_loc)) then () else
		    begin
		      Hashtbl.add live (C_System_Descr.location_coords error_loc) true ;
		      incr live_locs 
		    end;
		    List.iter 
		  (fun e -> 
		    let l = C_System_Descr.get_source e in
		    if (Hashtbl.mem live (C_System_Descr.location_coords l)) then () else
		    begin
		      Hashtbl.add live (C_System_Descr.location_coords l) true ;
		      incr live_locs 
		    end;
		    let pr = Hashtbl.find (!sinfo_map) (C_System_Descr.location_coords l) in
		    priority_to_loc_array.(pr) <- None
			 ) 
		      op_list ;
		  (* expand the error path as far as possible *)
		    expand_error_path (reg_i, op_list, reg_e) ;
		  (* generate a test *)
		  Message.msg_string Message.Normal "Test case:" ;
                  let rec _prtest l = match l with [] -> ()
                  | (sym, v) :: t ->
                    Message.msg_string Message.Normal (Expression.toString sym);
                    Message.msg_string Message.Normal (string_of_float v) ;
                    _prtest t 
                  in
                  let tests = try (Abstraction.genTests reg_i) with Failure _ ->
                    (Message.msg_string Message.Error "Test generation failed!" ;
                    [])
                  in
		  _prtest tests ;
		  let (i,j) = C_System_Descr.location_coords error_loc in

(*
		  Printf.fprintf test_vector_channel "Loc_%s_%d:%d " (C_System_Descr.get_location_fname error_loc) i j;
*)
		  let pathid = List.fold_left (fun  b a ->  
                      let l = C_System_Descr.get_source a in
                      let (i,j) = C_System_Descr.location_coords l in
                      b^(Printf.sprintf "_%d:%d" i j)
                    ) 
                    ("Path_"^(Printf.sprintf "%s::" (C_System_Descr.get_location_fname error_loc) )) op_list in
                  Printf.fprintf test_vector_channel "%s" (pathid^(Printf.sprintf "_%d:%d" i j)) ;
(*
		  List.iter (fun (a,b) -> Printf.fprintf test_vector_channel "%s %d" (Expression.toString a) (int_of_float b)) tests ;
*)
		  List.iter (fun (a,b) -> Printf.fprintf test_vector_channel " %d" (int_of_float b)) tests ;
		  Printf.fprintf test_vector_channel "\n" ;

		      
	      end ;
	    priority_to_loc_array.(!toposort_counter) <- None ;
          end
	    
	with Abstraction.NoNewPredicatesException -> 
	  begin 
	    Abstraction.dump_abs (); 
	    Message.msg_string Message.Error ("Exception raised by the model checker : No new predicates");
	    priority_to_loc_array.(!toposort_counter) <- None ; (* Set this node to visited and continue *)
	    flush_tree () 
	  end ;
	| Ast.TimeOutException -> 
	    begin
	      Abstraction.dump_abs (); 
	      Message.msg_string Message.Error ("Exception raised by the model checker : Time out");
	      priority_to_loc_array.(!toposort_counter) <- None ; (* Set this node to visited and continue *)
	      (* flush_tree () *)
	    end
	| e -> raise e
      with Not_found -> anymore := false 
    done ;
    close_out test_vector_channel ;
    Message.msg_string Message.Error "\nTest stats:" ;
    Message.msg_string Message.Error ("Number of iterations = " ^ (string_of_int !iteration_count) ) ; 
    Message.msg_string Message.Error "\nModel checker stats:" ;
    Message.msg_printer Message.Error print_stats () ;
    Message.msg_string Message.Error "\nAbstractor stats:" ;
    Message.msg_printer Message.Error Abstraction.print_stats () ;
    Message.msg_string Message.Error ("Number of locations = " ^ (string_of_int num_locs) ) ; 
    Message.msg_string Message.Error ("Number of live locations = " ^ (string_of_int !live_locs) ) ; 
    Message.msg_string Message.Error ("Number of dead locations = " ^ (string_of_int !dead_locs) ) ; 
    Message.msg_string Message.Error ("Number of tests = " ^ (string_of_int !num_tests) )  ;
    (* color code the cfa graph *)
    (* red: dead *)
    (* green: reachable *)
    (* white: syntactically unreachable *)
    let output_cfa_dot ch fundef =
      let known_locs = ref IntSet.empty in
      let rec output_loc loc =
	let locid = ((C_System_Descr.location_coords loc)) in
	let (color,style) = 
	  if (Hashtbl.mem live locid) then ("green" , "filled")
	  else
	    if (Hashtbl.mem dead locid) then ("red", "unfilled")
	    else ("blue", "filled")
	in

	Printf.fprintf ch "  %d[fontname=\"Helvetica\",fontsize=\"64\",height=\"1.5\",width=\"2\",color=%s,style=%s];\n" (snd locid) color style;
	known_locs := IntSet.add (snd locid) !known_locs;
	List.iter
          (fun e ->
	    let l = C_System_Descr.get_target e in
	    let lid = snd (C_System_Descr.location_coords l) in
	    let cmd = C_System_Descr.get_command e in
            Printf.fprintf ch "  %d -> %d [arrowsize=\"4.0\",fontname=\"Helvetica\",fontsize=\"64\",label=\"%s\"]\n"
              (snd locid) lid
              (String.escaped (C_Command.to_string cmd));
            if not (IntSet.mem lid !known_locs) then output_loc l)
          (C_System_Descr.get_outgoing_edges loc)
      in
      try
	let loc = (C_System_Descr.lookup_entry_location fundef) in
        Printf.fprintf ch "digraph %s {\n" fundef;
        Printf.fprintf ch "size=\"7.5,10\";\n" ;
        output_loc loc ;
        Printf.fprintf ch "}\n\n";
        ()
      with C_System_Descr.NoSuchFunctionException _ -> ()
    in
    let cfa_file = Options.getValueOfString "testfile" in
    let ch = open_out cfa_file in
    List.iter (output_cfa_dot ch)
      (C_System_Descr.list_of_functions ()) ;
    close_out ch ;
    (* write out a text file with the information about dead nodes *)
    let cfa_text_file = (cfa_file)^".txt" in
    let ch_txt = open_out cfa_text_file in
    let output_dead_nodes (locid1, locid2) _ =
      let loc = C_System_Descr.lookup_location_from_loc_coords (locid1, locid2) in
      (match (C_System_Descr.get_source_position loc) with
	Some (fname, lineno, colno) ->
	  Printf.fprintf ch_txt "%s %d %d :: " fname lineno colno 
      |	None -> () ) ;
      (* now write the code *)
      let edges = (C_System_Descr.get_outgoing_edges loc) in
      (match edges with
	[] -> ()
      |	e :: rest -> 
	  let ecode = (C_System_Descr.Command.to_string (C_System_Descr.edge_to_command e)) in
	  if C_System_Descr.isPredicate e then Printf.fprintf ch_txt "if ( %s )" ecode 
	  else
	    Printf.fprintf ch_txt "%s" ecode );
      Printf.fprintf ch_txt "\n" ;
      ()
    in
    Hashtbl.iter output_dead_nodes dead ;
    close_out ch_txt ;
  end  

(*****************************************************************************)
(*****************************************************************************)
(* Optimize CCured runtime checks *)
(*****************************************************************************)
let ccured_model_check  initialState errorPred seedPreds =
  let mainFunctions = Options.getValueOfStringList "main" in
  let _ = Abstraction.initialize_abstraction () in

  (* association list of ccured checks and corresponding blast calls *)
  let list_of_functions = [ 
	("__CHECK_NULL", "__BLAST__CHECK_NULL")  ;
	(* ("__CHECK_SEQ2FSEQ", "__BLAST__CHECK_SEQ2FSEQ")  ; *)
	] in 
  

  let init_region =
    let rec buildloclist lst loclist =
      match lst with [] -> loclist
      | a ::rest -> 
	  try
	    buildloclist rest ((C_System_Descr.lookup_entry_location a) :: loclist) 
	  with 
	    Not_found -> 
	      (Format.printf "Creating init_region: No function %s defined in program. Exiting\n" (a);
	       failwith ("No function "^(a)^" defined"))
    in
    let loclist = buildloclist mainFunctions [] in
    Abstraction.create_region 
      loclist false seedPreds initialState
  in	
  begin
    
    (* create an array of priority -> location option, and a map location -> priority *)
    let sinfo_map = ref (Hashtbl.create 511) in
    let get_sinfo loc =
      try
	Hashtbl.find (!sinfo_map) (C_System_Descr.location_coords loc)
      with
	_ -> Hashtbl.add (!sinfo_map) (C_System_Descr.location_coords loc) (-1) ; -1
    in
    (* depth-first number the locations of the call graph *)
    let toposort_counter = ref 0 in
    let temp_list = ref [] in
    let rec toposort loc =
      let prio = get_sinfo loc in
      if prio >= 0 then
	() (* already visited *)
      else
	begin
	  Hashtbl.replace (!sinfo_map) (C_System_Descr.location_coords loc) (!toposort_counter) ; (* currently visiting *)
	  temp_list := (!toposort_counter, loc) :: !temp_list ;
	  incr toposort_counter ;
	  (* visit children *) 
	  let succs = C_System_Descr.get_outgoing_edges loc in
	  (* handle function calls in edges *)
	  List.iter 
	    (fun e -> 
	      let c = C_System_Descr.get_command e in 
	      match c.C_Command.code with 
		C_Command.FunctionCall exp ->
		  begin
		    match exp with
		      Expression.Assignment(Expression.Assign, _, Expression.FunctionCall(e,_) ) 
		    | Expression.FunctionCall (e, _) ->
			begin
			  match e with
			    Expression.Lval (Expression.Symbol fname) ->
			      begin
				try
				  let fname_start = (C_System_Descr.lookup_entry_location fname)
				  in
				  toposort fname_start
				with e -> ()
			      end
			  | _ ->
			      if (Options.getValueOfBool "nofp") 
			      then 
				(Message.msg_string Message.Error "Warning: Ignoring function pointer call") 
			      else
				begin 
				  failwith ("Function pointer call: " ^ (Expression.toString exp)) ;
				end ;
			      ()
				
			end
		  end
	      |	_ -> ()
		    )
	    succs ;
	  List.iter 
	    (fun e -> 
	      toposort (C_System_Descr.get_target e) ) 
	    (succs) ;
	  
	  ()
	end
    in
    let startloc =  (C_System_Descr.lookup_entry_location (List.hd mainFunctions)) in
    

    let (priority_to_loc_array, index, smap, live, live_locs, dead, dead_locs, num_tests) =
      if (Options.getValueOfBool "loadtest") then 
	load_tests () 
      else
	begin
	  toposort startloc ;
    
        (* priority_to_loc_array is an array mapping priorities to locations *)
	  let p_to_loc_array = Array.make (!toposort_counter) (None) in
	  List.iter (fun (p, l) -> p_to_loc_array.(p) <- Some l ) !temp_list ;
	  temp_list := [] ; (* clear space *)
	  
	  (p_to_loc_array, !toposort_counter,
           !sinfo_map,  
                                (* Initialize two sets: dead, live *)
	   Hashtbl.create 1009, (* live *)
	   ref 0,               (* live locs *)
	   Hashtbl.create 1009, (* dead *)
	   ref 0,               (* dead locs *)
	   ref 0 )
	end
    in

        (* At this point toposort is the number of locations marked, since we started at zero *)
    let num_locs = Array.length priority_to_loc_array in
    toposort_counter := index ;
    sinfo_map := smap ;

    let dump_tests () =
      let trace_chan = open_out_bin ((Options.getValueOfString "mainsourcename")^".tst") in
      Marshal.to_channel trace_chan 
        (priority_to_loc_array, !toposort_counter, !sinfo_map, live, !live_locs, dead, !dead_locs, !num_tests) [];
      close_out trace_chan;
      ()
    in
    
    
    

    unprocessed_nodes#set_interesting 
      (fun e ->
	match (get_node_data e).mark with 
	  Unprocessed op ->
            let target_loc_id = C_System_Descr.location_coords (C_System_Descr.get_target op) in
            not (Hashtbl.mem live target_loc_id)
	| Processed_Covered (md, _) 
	| Processed_Uncovered md 
	| Processed_Was_Covered_To_Reprocess md ->
	    begin
	      try
		let reg = md.region in
		let (i1,i2) = Region.get_location_id reg in
		not (Hashtbl.mem live (i1,i2))
	      with _ ->
                true
	    end
	      ) ;
   
(* 
    if (priority_to_loc_array.(0) = None) then () else
    begin
      Hashtbl.add live (C_System_Descr.location_coords startloc) true ;
      priority_to_loc_array.(0) <- None ;
      incr live_locs ;
    end ;
*)
    
    (* the root of the reachability tree *)
    let tree_root =
      tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init_region ; })
    in
    
    let flush_tree () =
      Message.msg_string Message.Normal "Flushing tree" ;
      let tree_root =
	tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = 0 ;
                                                              region = init_region ; })
      in
      time := 0 ;
      unprocessed_nodes#empty_out () ;
      unprocessed_nodes#add_element (tree_root) ;
      reached_region := Region.bot
(*      tnc#delete_children tree_root *)
    in
    
    (* add the root to the pending unprocessed nodes *)
    unprocessed_nodes#add_element (tree_root) ;
    
    (* toposort_counter is now used as the current node to be tested *)
    let iteration_count = ref 0 in 

    let current_loc = ref (C_System_Descr.fake_location) 
    in
    let current_edge = ref (C_System_Descr.fake_edge) 
    and
	current_fn = ref "__foo__" 
    in
    let get_next () =
      let continue = ref true in 
      while (!toposort_counter > 0 && !continue) do
	match priority_to_loc_array.(!toposort_counter) with
	  Some loc -> (* found the next one *)
	    (* check if this node calls a suitable function *)
	    begin
	      let outedges = C_System_Descr.get_outgoing_edges loc in
	      try
		  let rec _find l =
		    match l with [] -> raise Not_found 
		    | e :: t ->
			begin
			let cmd = C_System_Descr.get_command e in
			match cmd.C_Command.code with
			  C_Command.FunctionCall 
			    (Expression.FunctionCall (Expression.Lval (Expression.Symbol s), args)) 
			  -> if (List.mem_assoc s list_of_functions) then begin
			    current_loc := loc ; 
			    current_edge := e;
			    current_fn := s ;
			    Message.msg_string Message.Normal ("Replacing function "^s^" with "^(List.assoc s list_of_functions)) ;
		            C_System_Descr.replace_code_in_cfa e 
		               (
		                 (Expression.FunctionCall 
			           (Expression.Lval (Expression.Symbol (List.assoc s list_of_functions )), args)));   
	                           (* replace function by appropriate BLAST call *)
	   		    Message.msg_printer Message.Debug C_System_Descr.print_system ()
			  end
			  else _find t
			| _ -> _find t
			end
		  in
		  _find outedges;
		  continue := false
	      with Not_found -> 
		(priority_to_loc_array.(!toposort_counter) <- None ;
		decr toposort_counter)
	    end
	| None -> decr toposort_counter
      done ;
      if (!continue = true ) then
	raise Not_found
      else ()
    in
    let end_next () = (* this is the companion to get_next that ensures the loose ends
                         are tied. Specifically, the current element is set to None in the
                         array, and the code is replaced back. *)
      priority_to_loc_array.(!toposort_counter) <- None ;
      (* replace code *)
      let cmd = C_System_Descr.get_command !current_edge in
      match cmd.C_Command.code with
	C_Command.FunctionCall 
			    (Expression.FunctionCall (Expression.Lval (Expression.Symbol s), args)) 
	-> C_System_Descr.replace_code_in_cfa (List.hd (C_System_Descr.get_outgoing_edges !current_loc)) 
	    ( (Expression.FunctionCall (Expression.Lval (Expression.Symbol !current_fn), args))) ;
           Message.msg_string Message.Normal ("Replacing function "^s^" with "^(!current_fn)) ;
	   Message.msg_printer Message.Debug C_System_Descr.print_system ()
      |	_ -> failwith "end_next: expected a function call!"
    in
    let anymore = ref true in
    (* skip some *)
    begin
      try
	let i = ref 0 in
	while !i< (Options.getValueOfInt "skiptest") do
	  incr i ;
	  decr toposort_counter ;
	  get_next () ;
	done 
      with Not_found -> print_string "HRERERE" ; anymore := false ;
    end;

    Message.msg_string Message.Normal "********** Running the tester's while loop **********" ;

    let error_loc = let el = C_System_Descr.get_locations_at_label (Options.getValueOfString "L") in
    match el with 
      [l] -> l | _ -> failwith "Error label not found"
	  
    in 
    Message.msg_string Message.Normal 
      (Format.sprintf "Checking reachability of %s \n" (C_System_Descr.location_to_string error_loc) );
    let error_region =
      (Abstraction.create_region [error_loc] true errorPred errorPred)
    in


    (* big test while loop until get_next raises Not_found *)
    while (!anymore) do 
      Message.msg_string Message.Normal "\n\n\n***** Running the next iteration of the tester's while loop *******\n" ;
      Message.msg_string Message.Normal (Format.sprintf "Iteration Number %d \n" !iteration_count );
      Message.msg_string Message.Normal (Format.sprintf "Number of locations %d \n" num_locs );
      Message.msg_string Message.Normal (Format.sprintf "Number of live locations found %d \n" !live_locs );
      Message.msg_string Message.Normal (Format.sprintf "Number of dead locations found %d \n\n" !dead_locs );
      dump_tests () ;

      incr iteration_count ;
      decr toposort_counter; 
      try 
	get_next () ; (* points to the next ccured test *)
	try
	  begin
(*
		(* take the current tree and intersect it with the error region *)
            let intersect_error_region tree_root err_reg =
	      let rec _traverse nlist =
		Message.msg_string Message.Debug "In _traverse " ;
                match nlist with
                  [] -> ()
                | n :: t ->
		    begin
		      Message.msg_string Message.Debug "(In intersect_error_region) Current node is " ;
		      Message.msg_printer Message.Debug print_tree_node n ;
		      let mking = get_marking n in
		      match mking with 
			Unprocessed _ -> _traverse t
		      | Processed_Covered (md, _)
		      | Processed_Was_Covered_To_Reprocess md
		      | Processed_Uncovered md ->
			      (* region at this node is md.region *)
			  let r_cap_err = Region.cap md.region err_reg in
			  if Region.leq r_cap_err Region.bot then
				(* does not intersect error region *)
			    _traverse ( List.append t (get_children n) )
			  else
			    begin
			      Message.msg_string Message.Debug "Deleting children of offending node " ;
			      tnc#delete_children n ;
			      (Tree.get_node_label n).mark <- Unprocessed (get_op n) ;
			      _traverse t
			    end
		    end
	      in
	      _traverse [tree_root ] ;
	      tree_root
	    in
		(* update tree and reached region *)
	    Message.msg_string Message.Debug "Now intersecting the current tree with the error region " ;
	    print_whole_tree Message.Debug tree_root ;
	    Message.msg_string Message.Debug "Now calling intersecting error region " ;
	    intersect_error_region tree_root error_region ;
	    Message.msg_string Message.Debug "Done. New tree is: " ;
	    print_whole_tree Message.Debug tree_root ;
	    Message.msg_string Message.Debug "Now updating reached region " ;
            unprocessed_nodes#empty_out ();
	    reached_region := 
	       update_tree_after_refinment 
		 tree_root unprocessed_nodes 
		 ((get_marking_data tree_root).time_stamp) ;
*)
	     tnc#delete_children tree_root ;
	     reached_region := Region.bot ;
      time := 0 ;
      let tree_root = tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init_region ; })
      in
      unprocessed_nodes#empty_out () ;
      unprocessed_nodes#add_element (tree_root) ;

		(* invariant: reached_region cap error_region is empty *)
	    
		(* call model check on the new partial tree *)
            let mc_return_val =
	      try
		  begin
		    Ast.set_time_out_signal () ;
		    (model_check tree_root error_region)
		  end
	      with Abstraction.NoNewPredicatesException ->
		begin
		  Ast.reset_time_out_signal () ;
		  Message.msg_string Message.Error ("Exception raised by model_check!: No new predicates found.");
		  raise Abstraction.NoNewPredicatesException
		end
	      | Ast.TimeOutException -> 
		  begin
		    Ast.reset_time_out_signal () ;
		    Message.msg_string Message.Error ("Exception raised by model_check!: Time out.");
		    raise Ast.TimeOutException 
		  end
	    in 
		(* Message.msg_string Message.Error "Withdrawing handler " ; *)
	    Ast.reset_time_out_signal () ;
	    match (mc_return_val) with
	      No_error troot ->
		    (* Message.msg_string Message.Normal "\n\nNo error found. Location unreachable.\n" ; *)
		Hashtbl.add dead (C_System_Descr.location_coords !current_loc) true ;
		incr dead_locs 
	    | Error_reached ((reg_i, op_list, reg_e), troot) ->
		(* set all these locations to null in the priority_to_loc_array *)
		if (Hashtbl.mem live (C_System_Descr.location_coords !current_loc)) then () else
		begin
		  Hashtbl.add live (C_System_Descr.location_coords !current_loc) true ;
		  incr live_locs 
		end
	    | _ -> failwith "testModelCheck: Unexpected return value from model_check"
	  end;
	  end_next ();
	with Abstraction.NoNewPredicatesException -> 
	  begin 
	    Abstraction.dump_abs (); 
	    Message.msg_string Message.Error ("Exception raised by the model checker : No new predicates");
	    end_next () 
	  end ;
	| Ast.TimeOutException -> 
	    begin
	      Abstraction.dump_abs (); 
	      Message.msg_string Message.Error ("Exception raised by the model checker : Time out");
	      end_next () ;
	    end
	| e -> raise e
      with Not_found -> anymore := false 
    done ;
    Message.msg_string Message.Error "\nTest stats:" ;
    Message.msg_string Message.Error ("Number of iterations = " ^ (string_of_int !iteration_count) ) ; 
    Message.msg_string Message.Error "\nModel checker stats:" ;
    Message.msg_printer Message.Error print_stats () ;
    Message.msg_string Message.Error "\nAbstractor stats:" ;
    Message.msg_printer Message.Error Abstraction.print_stats () ;
    Message.msg_string Message.Error ("Number of locations = " ^ (string_of_int num_locs) ) ; 
    Message.msg_string Message.Error ("Number of live locations = " ^ (string_of_int !live_locs) ) ; 
    Message.msg_string Message.Error ("Number of dead locations = " ^ (string_of_int !dead_locs) ) ; 
    (* color code the cfa graph *)
    (* red: dead *)
    (* green: reachable *)
    (* white: syntactically unreachable *)
    let output_cfa_dot ch fundef =
      let known_locs = ref IntSet.empty in
      let rec output_loc loc =
	let locid = ((C_System_Descr.location_coords loc)) in
	let (color,style) = 
	  if (Hashtbl.mem live locid) then ("green" , "filled")
	  else
	    if (Hashtbl.mem dead locid) then ("red", "unfilled")
	    else ("blue", "filled")
	in
	
	Printf.fprintf ch "  %d[fontname=\"Helvetica\",fontsize=\"64\",height=\"1.5\",width=\"2\",color=%s,style=%s];\n" (snd locid) color style;
	known_locs := IntSet.add (snd locid) !known_locs;
	List.iter
          (fun e ->
	    let l = C_System_Descr.get_target e in
	    let lid = snd (C_System_Descr.location_coords l) in
	    let cmd = C_System_Descr.get_command e in
            Printf.fprintf ch "  %d -> %d [arrowsize=\"4.0\",fontname=\"Helvetica\",fontsize=\"64\",label=\"%s\"]\n"
              (snd locid) lid
              (String.escaped (C_Command.to_string cmd));
            if not (IntSet.mem lid !known_locs) then output_loc l)
          (C_System_Descr.get_outgoing_edges loc)
      in
      try
	let loc = (C_System_Descr.lookup_entry_location fundef) in
        Printf.fprintf ch "digraph %s {\n" fundef;
        Printf.fprintf ch "size=\"7.5,10\";\n" ;
        output_loc loc ;
        Printf.fprintf ch "}\n\n";
        ()
      with C_System_Descr.NoSuchFunctionException _ -> ()
    in
    let cfa_file = Options.getValueOfString "testfile" in
    let ch = open_out cfa_file in
    List.iter (output_cfa_dot ch)
      (C_System_Descr.list_of_functions ()) ;
    close_out ch ;
    (* write out a text file with the information about dead nodes *)
    let cfa_text_file = (cfa_file)^".txt" in
    let ch_txt = open_out cfa_text_file in
    let output_dead_nodes (locid1, locid2) _ =
      let loc = C_System_Descr.lookup_location_from_loc_coords (locid1, locid2) in
      (match (C_System_Descr.get_source_position loc) with
	Some (fname, lineno, colno) ->
	  Printf.fprintf ch_txt "%s %d %d :: " fname lineno colno 
      |	None -> () ) ;
      (* now write the code *)
      let edges = (C_System_Descr.get_outgoing_edges loc) in
      (match edges with
	[] -> ()
      |	e :: rest -> 
	  let ecode = (C_System_Descr.Command.to_string (C_System_Descr.edge_to_command e)) in
	  if C_System_Descr.isPredicate e then Printf.fprintf ch_txt "if ( %s )" ecode 
	  else
	    Printf.fprintf ch_txt "%s" ecode );
      Printf.fprintf ch_txt "\n" ;
      ()
    in
    Hashtbl.iter output_dead_nodes dead ;
    close_out ch_txt ;
  end  


(*******************************************************************************)
(*******************************************************************************)
(** Code for interface synthesis *)
(*******************************************************************************)
(*******************************************************************************)

(*********************************************************************************************************************)
(***********     Tree --> Automaton Conversion stuff    ***************************************************************)
(*********************************************************************************************************************)

(* given a complete reachability tree, will 
1. make a pass and find the node that covers each leaf
2. fold the edges back ie if n' covers n then when n'' --> n then will make n'' -> n' 
3. Quantify away local state
4. Minimize automaton
*)


type auto_node_label = int * Region.abstract_data_region
type auto_edge_label = Operation.t

module EnvAutomaton = Abstraction.EnvAutomaton


let is_public_fncall e =
  match e with 
    Abstraction.ProgramOperation op ->
      begin
	match (C_System_Descr.get_command op).C_Command.code  with
	  C_Command.FunctionCall (Expression.FunctionCall (Expression.Lval (Expression.Symbol f), _)) 
	| C_Command.FunctionCall (Expression.Assignment(_,_,Expression.FunctionCall (Expression.Lval (Expression.Symbol f), _))) ->
	    C_System_Descr.is_interface f
	| _ -> false
      end
  | _ -> failwith "Ouch!!!"
    
let auto_node_to_string {EnvAutomaton.oid = i ; EnvAutomaton.oadr = adr} =
  Printf.sprintf "%d $$ %s" i (Region.abstract_data_region_to_string adr)
    
let auto_edge_to_string op =
  match op with
    Abstraction.ProgramOperation op -> Operation.to_string op
  | Abstraction.AutomatonOperation (i,j) -> (Printf.sprintf "AutomatonMove(%d,%d) " i j)
	

let shrink_node_to_string (s_l : EnvAutomaton.anode_label) = 
   let ats = (if s_l.EnvAutomaton.atomic then "*" else "")  in
   let tas = (if s_l.EnvAutomaton.task then "T" else "")  in
   let evs = (if s_l.EnvAutomaton.event then "E" else "")  in
   Printf.sprintf "%d $$ %d %s %s" s_l.EnvAutomaton.id s_l.EnvAutomaton.id
     (ats^tas^evs)
     (Region.abstract_data_region_to_string s_l.EnvAutomaton.adr)
let shrink_edge_to_string e_l = 
  match e_l with
      EnvAutomaton.Thread modl -> 
	"T: "^(Misc.strList (List.map Expression.lvalToString modl))
    | EnvAutomaton.Env -> "E"
     
  let print_graph filename root = 
        BiDirectionalLabeledGraph.output_graph_dot 
	  shrink_node_to_string 
	    shrink_edge_to_string filename
	      [root]
  
let get_auto_node_id auto_node = 
  (BiDirectionalLabeledGraph.get_node_label auto_node).EnvAutomaton.oid

let get_modified_globals auto_node =
     let nlabel = BiDirectionalLabeledGraph.get_node_label auto_node in
     let outgoing_progops = List.fold_left 
       (fun sofar thisone -> match BiDirectionalLabeledGraph.get_edge_label thisone with 
          Abstraction.ProgramOperation op -> op::sofar | _ -> sofar) [] (BiDirectionalLabeledGraph.get_out_edges auto_node) in
     let gwrites = List.map (fun op -> List.filter (fun l -> (Abstraction.can_escape l) )
       (snd (Abstraction.reads_and_writes op))) outgoing_progops in
     List.fold_left (fun sofar thislist -> Misc.union thislist sofar) [] gwrites

let get_shrink_node_id sn = 
  (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.id
    
let quantify_env_states rt = rt (* TO DO *)

(* RJ: wrapping everything inside ONE GIANT function *)
 

  
let dotted_neighbours an = [] (* there are no automaton moves! *)
  
let make_shrink_auto full_tree_root =
  Message.msg_string Message.Normal "In make_shrink_auto" ;
  let tree_root = quantify_env_states full_tree_root in
  (* this tells you the automaton node corresponding to a tree node *)
  let tree_auto_map = Hashtbl.create 1009 in

  (* this tells you the list of tree nodes corresponding to an auto node *)
  let auto_tree_map = Hashtbl.create 1009 in

  (* this map tells you whether or not a node is a back node *)
  let back_node_map = Hashtbl.create 1009 in
    
  (* this map is from int --> auto node, should one wish to directly access a node *)
  let auto_map = Hashtbl.create 1009 in

  (* returns the root of the constructed automaton which is a 
     (auto_node_label, auto_edge_label) BiDirectionalLabeledGraph.node *)
   let cov = Region.emptyCoverers () in (* this is now needed outside for the cover_set *)
     
   let tree_close root =  
    let unprocessed_nodes =  new Search.bfsiterator    in  
    let tree_size_count = ref 0 in
     
    let connect auto_node tree_node = 
      let a_id = get_auto_node_id auto_node in
      let t_id = get_id tree_node in
	Hashtbl.replace tree_auto_map t_id auto_node;
	Misc.hashtbl_update auto_tree_map a_id tree_node
    in
        (* function for phase 1: add tree nodes if required to the set of coverers *)
    let collect_coverers n = 
      let id = get_id n in
      let _ = Message.msg_string Message.Debug ("Collect coverers:"^(string_of_int id)) in
      let n_actual_region = get_region n in
      if (Region.is_empty n_actual_region) then 
	Message.msg_string Message.Debug ("Tree node empty")
      else
	let n_reg = Region.project_par_atomic (get_region n) in    		    
	let cov_fun =
	  if (Options.getValueOfBool "ecov") 
	  then Region.findExactCoverer 
	  else Region.findCoverer
	in
	let (coverer_id,is_covered) = 
	  match cov_fun cov n_reg with
	      Some i -> 
		begin
		  Message.msg_string Message.Debug ("Covered by: "^(string_of_int i));
		  (i,true)
		end
	    | None -> 
		begin
		  if (Tree.get_children n = []) then
		    Message.msg_string Message.Debug
		      ("failed findcov: "^(string_of_int id)^": "^ (Region.to_string ( get_region n))) ;
		  
		  
		  let _ = Message.msg_string Message.Debug ("New auto node: "^(string_of_int id)) 
		  in
		  let new_auto_node = 
		    BiDirectionalLabeledGraph.create_root 
		      { EnvAutomaton.oid = id;
			EnvAutomaton.oadr = (Abstraction.quantify_and_extract_abs_data_reg n_reg);
			EnvAutomaton.oregion = n_reg
		      }
		      (* RJ: at this point we can quantify out local predicates as well *)
		  in
		    Hashtbl.add auto_map id new_auto_node;
		    Region.addCoverer cov n_reg id;
		    (id,false)
		end
	in
	let auto_node = 
	  try Hashtbl.find auto_map coverer_id 
	  with Not_found -> failwith "hashtbl fails in get_auto_node"
	in
	  (* isnt tree_auto_map same as auto_map ? no ... *)
	  (*  let auto_node_id = get_auto_node_id auto_node in *) (* isnt this always coverer_id ? RJ *)
	  (* Hashtbl.replace back_node_map coverer_id true; -- this is never used *)
	  connect auto_node n;
	  ()
    in

    let get_auto_node n = 
      let id = get_id n in
      	try
	  Hashtbl.find tree_auto_map id 
	with Not_found -> failwith ("didnt see this node in collect_coverers"^(string_of_int id))
    in
      (* Another function for phase 1: add the tree edges ... *)
    let add_tree_edge (n_parent,tree_edge_label,n) =
      tree_size_count := !tree_size_count + 1;
      let n_actual_region = get_region n in
      let n_region = Region.project_par_atomic n_actual_region in
	if not (Region.is_empty n_actual_region) (* check for emptiness of the tree's actual region! *) 
	then
	  begin
            let (n_auto) = get_auto_node n in (* this will also update the maps if needed *)
	    
	    let np_id = get_id n_parent in
	    let n_id = get_id n in
	      

	    let n_parent_auto = get_auto_node n_parent in
	    let npa_id = get_auto_node_id n_parent_auto in
	    let na_id = get_auto_node_id n_auto in
	    let _ =   Message.msg_string Message.Debug 
		(Printf.sprintf "Autoedge (%d,%d) from tree edge (%d,%d)" 
		   npa_id na_id np_id n_id)
	    in
	      BiDirectionalLabeledGraph.hookup_parent_child 
		n_parent_auto n_auto 
		(Abstraction.ProgramOperation tree_edge_label)
	  end
    in
      (* first, get all the states *)
      BiDirectionalLabeledTree.iter_descendants_nodes collect_coverers root;
      (* second, add all the edges *)
      BiDirectionalLabeledTree.iter_descendants_edges add_tree_edge root;
      (* the "orig_auto" is now built! *)
      Message.msg_string Message.Normal "Main loop of make_shrink_auto done" ;
      try
	let init_node = Hashtbl.find tree_auto_map (get_id root) in
	let _  =  BiDirectionalLabeledGraph.output_graph_dot 
	    auto_node_to_string 
	      auto_edge_to_string 
		(Options.getValueOfString "auto") [init_node] in
  init_node 
    (* return the start node of the auto *)
 with
     Not_found ->
       failwith "No tree root ! Revolting !"
       
 in

 (** now we'd like to minimize the automaton to get the shrink auto which is finally what is needed *)
 (** a map from id --> shrink_node *)
 let shrink_map = Hashtbl.create 1009 in

 (** this maps a shrink_id -> auto_id list. ie this is the inverse of the next map *)
 let shrink_elts_map = Hashtbl.create 1009 in
 
 (** auto_node_id -> shrink_node id *)
 let auto_shrink_map = Hashtbl.create 1009 in
 
 let auto_shrink_node an =
   let (a_id) = get_auto_node_id an in
   Hashtbl.find shrink_map (Hashtbl.find auto_shrink_map a_id)
 in
 
 let shrink_counter = ref 0 in
 
 let new_shrink_id () = 
   shrink_counter := !shrink_counter + 1;
   !shrink_counter
 in
 
 let gvars = (Abstraction.globally_important_lvals ()) in

 let _ = Message.msg_string Message.Normal "Glob Imp lvals are:" in
 let _ = Message.msg_string Message.Normal (Misc.strList (List.map Expression.lvalToString gvars)) in
 
 let irrelev_shrink_edge e = 
   match (BiDirectionalLabeledGraph.get_edge_label e) with
     EnvAutomaton.Thread l -> List.for_all (fun lv -> not (List.mem lv gvars)) l 
   |  _ ->  true
 in
	 
 let similar_edges e = 
   let (sn,_,sn') = BiDirectionalLabeledGraph.deconstruct_edge e in
   let oe = BiDirectionalLabeledGraph.get_out_edges sn in
   List.filter (fun e' -> BiDirectionalLabeledGraph.get_target e' = sn') oe
 in
 let multiple_par a_id par_id = 
   let an = try Hashtbl.find auto_map a_id with Not_found -> failwith "unknown autonode!" in
   let pars = BiDirectionalLabeledGraph.get_parents an in
   List.exists (fun n -> 
     let n_id = get_auto_node_id n in 
     n_id <> par_id 
       && n_id <> a_id 
       && par_id <> a_id  (* self-loops dont count !*)
       ) pars 
     
 in
 let cluster_edges end_pt_fun e_list = 
   let nt = Hashtbl.create 7 in
   let proce e = 
     if Hashtbl.mem nt (end_pt_fun e) then ()
     else Hashtbl.replace nt (end_pt_fun e) e
   in
   List.iter proce e_list;
   List.map snd (Misc.hashtbl_to_list nt)
 in
 
  (** this function is used to filter nodes where we stop growing, args are auto_nodes  *)
 let stop_growing auto_root parent out_edge =
   let elabel = BiDirectionalLabeledGraph.get_edge_label out_edge in
   if (is_public_fncall elabel) then true else 
   let child = BiDirectionalLabeledGraph.get_target out_edge in
   let {EnvAutomaton.oid = c_id;
	 EnvAutomaton.oadr = c_adr;
 	 EnvAutomaton.oregion = c_reg;
       } = 
     BiDirectionalLabeledGraph.get_node_label child
   in
   let par_id = get_auto_node_id parent in
   if (* non unique parent should suffice: Hashtbl.mem back_node_map c_id*) (* RJ: check this it gives garbage! *)
     (((*not*) (multiple_par c_id par_id)) || c_id = (get_auto_node_id auto_root))
   then 
     (Message.msg_string Message.Normal ("non-unique par"^(string_of_int c_id));
      true)
   else
     let { EnvAutomaton.oadr = p_adr;
	   EnvAutomaton.oregion = p_reg
	 } = (BiDirectionalLabeledGraph.get_node_label parent)
     in
     let rv = 
       not (Region.adr_eq c_adr p_adr 
	      && ((* Options.getValueOfBool "atc" = false 
		     ||*)  (Abstraction.is_atomic c_reg = Abstraction.is_atomic p_reg)))
     in
     if rv then 
       Message.msg_string Message.Normal ("adrs differ"^(string_of_int c_id));
     rv
	   (* no need to do the modifies  check here .. because we're still in oauto land ... 
	      and all the shrink nodes have a single entry point ... 
	      *)
 in   
 
(* TBD: supply the list of vars modified at this node -- can be post-computed looking at the tree map *)
 
 let new_shrink_node auto_node = 
   let new_id = new_shrink_id () in
   let { EnvAutomaton.oid = root_id; 
	 EnvAutomaton.oadr = an_adr;
	 EnvAutomaton.oregion = oreg
       }
     = BiDirectionalLabeledGraph.get_node_label auto_node 
   in
   let label = { EnvAutomaton.id = new_id; 
		 EnvAutomaton.root = root_id; 
		 EnvAutomaton.adr = an_adr ; 
		 EnvAutomaton.atomic = Abstraction.is_atomic oreg;
		 EnvAutomaton.task = Abstraction.is_task oreg;
		 EnvAutomaton.event = Abstraction.is_event oreg;
		 EnvAutomaton.modifies = [] (* get_modified_globals auto_node *) 
	       } 
   in
   let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map new_id new_sn;
     new_sn
 in  	  

 let shrink_stuck_node = 
     let label = { EnvAutomaton.id = 0; 
		   EnvAutomaton.root = 0 (* junk *); 
		   EnvAutomaton.atomic = false ;
		   EnvAutomaton.task = false ;
		   EnvAutomaton.event = false ;
		   EnvAutomaton.adr = Region.true_adr ; 
		   EnvAutomaton.modifies = []} in
     let new_sn = BiDirectionalLabeledGraph.create_root label in
     Hashtbl.add shrink_map 0 new_sn;
     new_sn
 in
 let edge_list = ref [] in

 let add_edges () = 
   let add_edge e =
     let (an1,a_edge,an2) = BiDirectionalLabeledGraph.deconstruct_edge e in
       (* an1 should be  either a back node or should have a different adr than an2 *)
       (* we should actually trawl the auto to find all the global lvals that may have been modified
	  but for now I shall postpone that VVVVVIMP. TBD *)
       (* RJ: never add a dotted edge to itself ! *)

     let shrink_par = auto_shrink_node an1 in
     let shrink_child =  auto_shrink_node an2 in
     let es = Printf.sprintf " (%d, %d)  ... (%d,%d) " (get_auto_node_id an1) (get_auto_node_id an2)(get_shrink_node_id  shrink_par) (get_shrink_node_id shrink_child) in
     let el = EnvAutomaton.Thread (*Abstraction.type_of_edge a_edge  CHANGE THIS *) 
		(match a_edge with Abstraction.ProgramOperation op ->
                   (match (C_System_Descr.get_command op).C_Command.code with
                      C_Command.FunctionCall (Expression.FunctionCall(Expression.Lval(Expression.Symbol f),_))
                    | C_Command.FunctionCall (Expression.Assignment(_,_,Expression.FunctionCall(Expression.Lval(Expression.Symbol f),_))) ->
			if C_System_Descr.is_interface f then [Expression.Symbol f ] else []
                    | _ -> []
                   )
                )
     in
     let _ = Message.msg_string Message.Normal ("Adding edge: "^es) in 
     
	   (* if a similar edge exists dont re-add it ... just update ... *)
     BiDirectionalLabeledGraph.hookup_parent_child shrink_par shrink_child el
   in
   List.iter add_edge !edge_list
 in      
   (* connect the auto_node and the shrink node *)    
 let connect_shrink_auto sn an =
   let snlabel =BiDirectionalLabeledGraph.get_node_label sn in 
   let s_id = snlabel.EnvAutomaton.id in
   let a_id = get_auto_node_id an in
     Misc.hashtbl_update shrink_elts_map s_id a_id;
     (* 
	let modified_globals = get_modified_globals an in
	snlabel.EnvAutomaton.modifies <- Misc.union modified_globals snlabel.EnvAutomaton.modifies ;*)
     Hashtbl.replace auto_shrink_map a_id s_id
 in 
 
 let shrink_auto auto_root = 
   let _stop_growing = stop_growing auto_root in
  (* should we clear the hashtbls ? *)
  let visited_table = Hashtbl.create 1009 in
  let unprocessed_nodes = 
    if (Options.getValueOfBool "bfs") then new Search.bfsiterator
    else new Search.dfsiterator
  in
  let _ = edge_list := [] in
  let shroot = ref None in
    (* Phase 1 shrinking functions *)
  let rec grow shrink_node auto_node =
    (* a check to make sure *)
    if (Hashtbl.mem visited_table (get_auto_node_id auto_node))
    then ()
    else
      begin
	Hashtbl.add visited_table (get_auto_node_id auto_node) true;
	connect_shrink_auto shrink_node auto_node;
	let _l =  BiDirectionalLabeledGraph.get_out_edges auto_node in
	let l = 
	  List.filter (fun e -> (BiDirectionalLabeledGraph.get_target e) <> auto_node) _l 
	in
	let (l1,l2) =  Misc.filter_cut (_stop_growing  auto_node) l  in
	  List.iter (fun e -> unprocessed_nodes#add_element 
		       (BiDirectionalLabeledGraph.get_target e)) l1; 
	  (* actually should only add the unprocessed ones but... *)
	  List.iter (fun e -> edge_list := e::!edge_list) l1;
	  List.iter (grow shrink_node) (List.map BiDirectionalLabeledGraph.get_target l2) 
	    
(* INV: no  nodes in l2 is a back node i.e. no node in l2 is a coverer ..
   hence no node in l2 has been processed *)
      end
  in
  let process_auto_node node = 
    match (Hashtbl.mem visited_table (get_auto_node_id node)) with
	true -> ()
      | false -> 
	  let sn = new_shrink_node node in
	    grow sn node
	      
  in
  let gather_modifies_info () = 
    let gather_sn_info sn_id sn = 
      let mynodes = 
	List.map 
	  (fun a_id -> try Hashtbl.find auto_map a_id with Not_found -> failwith "grumble") 
	  (try 
	    (Hashtbl.find shrink_elts_map sn_id) 
	  with Not_found -> (assert (sn_id = 0) (* the stuck node... *); [])) 
      in
	(* for each node, on each edge gather successors *)
      let edge_is_local e = 
	try
	  sn_id = 
	Hashtbl.find auto_shrink_map 
	  (get_auto_node_id (BiDirectionalLabeledGraph.get_target e))
	with  Not_found -> failwith "NO HERE"
      in
      let globs_on_edge e = 
	match (BiDirectionalLabeledGraph.get_edge_label e) with
	    Abstraction.ProgramOperation op -> 
	      List.filter (fun l -> (Abstraction.can_escape l))
	      (snd (Abstraction.reads_and_writes op))
	  | _ -> []
      in
      let internal_mods an = 
	let local_e = List.filter  edge_is_local (BiDirectionalLabeledGraph.get_out_edges an) 
	in
	  Misc.compact (List.flatten (List.map globs_on_edge local_e))
      in
      let local_mods = Misc.compact (List.flatten (List.map internal_mods mynodes)) in
	Message.msg_string Message.Normal ("adding mods to : "^(string_of_int sn_id));
	print_string (Misc.strList (List.map Expression.lvalToString local_mods)); 
	(BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.modifies <- local_mods
    in
      Hashtbl.iter gather_sn_info shrink_map
  in
  let acc_at_e e = 
      match BiDirectionalLabeledGraph.get_edge_label e with
	  EnvAutomaton.Thread l -> l
	| _ -> []
  in	  
    (* Phase 2 shrinking functions *)
  let all_accesses_at_shrink_node sn = 
    (BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.modifies
    @
    (List.flatten (List.map  acc_at_e (BiDirectionalLabeledGraph.get_out_edges sn)))
  in
  let shrink_visited_table = Hashtbl.create 1009 in
 
 let unique_parent sn = 
    let pars = List.filter (fun n' -> n' <> sn) 
		 (BiDirectionalLabeledGraph.get_parents sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) pars in
      List.length (Misc.compact ids) = 1
  in
  let unique_child sn = 
    let kids = List.filter (fun n' -> n' <> sn) 
		 (BiDirectionalLabeledGraph.get_children sn) 
    in
    let ids = List.map (fun n -> get_shrink_node_id n) kids in
      List.length (Misc.compact ids) = 1
  in
 let really_unique_parent sn = 
    let pars = (BiDirectionalLabeledGraph.get_parents sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) pars in
      List.length (Misc.compact ids) = 1
  in
  let really_unique_child sn = 
    let kids =  (BiDirectionalLabeledGraph.get_children sn) in
    let ids = List.map (fun n -> get_shrink_node_id n) kids in
      List.length (Misc.compact ids) = 1
  in

    (* this checks if sn2 can be merged into sn1 *)
  let sn_adr_eq dir sn1 sn2 =
    let target_check_flag = true in
    let 
      { EnvAutomaton.adr = sn1_adr;
	EnvAutomaton.atomic = sn1_atomic;
	EnvAutomaton.task = sn1_task;
	EnvAutomaton.event = sn1_event;

      } = (BiDirectionalLabeledGraph.get_node_label sn1)
    in
    let
      { EnvAutomaton.adr = sn2_adr;
	EnvAutomaton.atomic = sn2_atomic;
	EnvAutomaton.task = sn2_task;
	EnvAutomaton.event = sn2_event;
      } = (BiDirectionalLabeledGraph.get_node_label sn2)
    in
  
    let lm mods = List.map (fun lv -> List.mem lv mods) gvars in
    let [sn1_mods;sn2_mods] = List.map all_accesses_at_shrink_node [sn1;sn2] in
 
    let (rlv1,rlv2) = (lm sn1_mods, lm sn2_mods) in
    let lv_check = 
      match Options.getValueOfBool "lvc" with
	  false -> (if dir then Misc.boolean_list_leq rlv1 rlv2
		    else Misc.boolean_list_leq rlv2 rlv1)
	| true ->  (rlv1 = rlv2)
    in
    let sn1a_tup = (sn1_atomic,sn1_task,sn1_event) in    
    let sn2a_tup = (sn2_atomic,sn2_task,sn2_event) in
    let atcheck = 
      if (Options.getValueOfBool "alt") then (sn1a_tup =sn2a_tup)
      else (sn1_atomic = sn2_atomic)
    in
      (Region.adr_eq sn1_adr sn2_adr) 
      && ( (* Options.getValueOfBool "atc" = false || *) (atcheck))
	&& (lv_check)  (* modify the same set of globally important lvals *)
      in 
  let merge_and_delete sn sn' = 
    (* sn' is going to get obliterated eventually *)

    let sn_id = get_shrink_node_id sn in
    let sn'_id = (get_shrink_node_id sn') in
    let ds = Printf.sprintf "Merging %d with %d" sn'_id sn_id in
    let _ = Message.msg_string Message.Debug ds in
    let _ = if (sn'_id = get_shrink_node_id sn) then failwith "deleting self!" in
    let an_l = List.map (Hashtbl.find auto_map) (Hashtbl.find shrink_elts_map sn'_id) in
    let _ = List.iter (connect_shrink_auto sn) an_l in 
    let _ = Hashtbl.remove shrink_map sn'_id in (* what if sn = sn' ????? *)
    let _ = Hashtbl.remove shrink_elts_map sn'_id in
    let _ = BiDirectionalLabeledGraph.delete_node sn' in
      if !shroot = Some sn'_id then shroot := Some (sn_id);
      ()
  in
 (* let merge_edge e = (* the merged node is sn, the source of the edge *)
    let (sn,e_l,sn') = BiDirectionalLabeledGraph.deconstruct_edge e in
    let sn'_id = (get_shrink_node_id sn') in
   let an_l = List.map (Hashtbl.find auto_map) (Hashtbl.find shrink_elts_map sn'_id) in
   let _ = List.iter (connect_shrink_auto sn) an_l in
    let _ = Hashtbl.remove shrink_map sn'_id in
   l et out_edges = BiDirectionalLabeledGraph.get_out_edges sn' in
    let in_edges = Misc.list_remove (BiDirectionalLabeledGraph.get_in_edges sn') e in
    let  _ = List.iter (BiDirectionalLabeledGraph.copy_edge_source sn) out_edges in
    let _  = List.iter (BiDirectionalLabeledGraph.copy_edge_target sn) in_edges in
      if !shroot = Some sn' then shroot := Some sn;
      BiDirectionalLabeledGraph.delete_node sn' 
     in *)
  
  (* this next function cannot loop forever as you dont grow if the target has multiple parents and
     in a cycle some node must have multiple parents *)
  let grow_component target_check_flag dir sn = 
    let node_l = ref [] in
    let edge_l = ref [] in
    let (endpt_fn,succ_fn,grow_fn) = 
      if dir 
      then (BiDirectionalLabeledGraph.get_target,
	    BiDirectionalLabeledGraph.get_out_edges,unique_parent)
      else (BiDirectionalLabeledGraph.get_source,
	    BiDirectionalLabeledGraph.get_in_edges,unique_child)
    in 
    let visited_table = Hashtbl.create 37 in
    let is_solid e = 
      match (BiDirectionalLabeledGraph.get_edge_label e) with
	  EnvAutomaton.Thread _ -> true
	| _ -> false
    in
    let rec _ac_grow_comp e =
      let _sn = endpt_fn e in
      let _sn_id = get_shrink_node_id _sn in
      let e_dot_ok = (Options.getValueOfBool "dotsep" = false || is_solid e) in
      let is_dot_edge = (Options.getValueOfBool "dotsep" = false && not (is_solid e)) in
      let edge_check_ok = 
	if (Options.getValueOfBool "lvc") then true (* that will deal with it ... *)
	else 
	  let all_e = BiDirectionalLabeledGraph.get_all_edges_like e in
	  let mods_imp e = (Misc.intersection (acc_at_e e) gvars) <> [] in
	    List.exists mods_imp all_e 
      in (* if some edge between these nodes changes something imp, then please dont grow... *)
      let _sn_not_root = (not dir || (Some _sn_id  <> !shroot))  in
      let grow_cond = _sn_not_root && edge_check_ok && e_dot_ok && grow_fn _sn && (sn_adr_eq dir  sn _sn) in
	if (grow_cond ) then 
	  (* maintains atomicity *)
	  begin
	    if (Hashtbl.mem visited_table (_sn_id)) then ()
	    else
	      begin
		Hashtbl.replace visited_table _sn_id true;
		node_l := _sn::!node_l;
		List.iter _ac_grow_comp (succ_fn _sn)
	      end
	  end
	else
	  edge_l := e :: !edge_l
    in
      (* this makes sure you dont add sn to the node_l which will get it deleted!! *)
      Hashtbl.replace visited_table (get_shrink_node_id sn) true;
      List.iter _ac_grow_comp (succ_fn sn);
      (!node_l,!edge_l) 
      
  (* the first is the nodes that should be merged with sn, the second, the outgoing edges of this comp*)
  in
    
  let rec dshrink target_check_flag dir sn = 
    let (succ_fn, copy_edge) = 
      if dir 
      then (BiDirectionalLabeledGraph.get_target, BiDirectionalLabeledGraph.copy_edge_source)
      else (BiDirectionalLabeledGraph.get_source, BiDirectionalLabeledGraph.copy_edge_target)
    in
    let sn_id = (get_shrink_node_id sn) in
    let _ = Message.msg_string Message.Normal ("In dshrink: "^(string_of_int sn_id)) in
    if (Hashtbl.mem shrink_visited_table sn_id) then 
      Message.msg_string Message.Normal ("In dshrink: Visited already "^(string_of_int sn_id))
    else
      begin
	let _ = Hashtbl.add shrink_visited_table sn_id true in
	Message.msg_string Message.Normal ("In dshrink: Added this nodeto the hashtbl") ;
        (*BiDirectionalLabeledGraph.output_graph_dot shrink_node_to_string shrink_edge_to_string "" [sn] ;*)
	let (nl,el) = grow_component target_check_flag dir sn in	
	Message.msg_string Message.Normal ("In dshrink: components grown") ;
	let _ = List.iter (merge_and_delete sn) nl in
	Message.msg_string Message.Normal ("In dshrink: merged and deleted ") ;
	let _ = List.iter (copy_edge sn) el in
	List.iter (dshrink target_check_flag dir) (List.map succ_fn el) (* used to be get_target *)
      end
 in

let deatomize shrink_root = 
  (* will visit each node, and see if we really need it to be atomic *)
  (* a node sn is atomic if exists a path sn -> .... sn_k -- write x ---> ... 
     where x is an imp var, and each of sn, sn1, ..., sn_k is atomic 
  
     THIS IS A HEURISTIC (no!), but sound.
  *)
  let ignorable_globals = (* JHALA TBPLDI *)
    List.map (fun s -> Expression.Symbol s) ["__BLAST_interrupt_enabled";"__BLAST_task_enabled"] 
  in
  let nice_gvars = Misc.difference gvars ignorable_globals in
  let _ = Message.msg_string Message.Normal 
	    ("Nice gvars:"^(Misc.strList (List.map Expression.lvalToString nice_gvars)))
  in
  let irrelev_atomic e = 
    match (BiDirectionalLabeledGraph.get_edge_label e) with
	EnvAutomaton.Thread l -> 
	  List.for_all (fun lv -> not (List.mem lv nice_gvars)) l 
	    |  _ ->  true
	  in
	    
  let shrink_node_is_atomic sn = 
    let sn_label = (BiDirectionalLabeledGraph.get_node_label sn) in
      sn_label.EnvAutomaton.atomic
  in

  let atomic_desc sn = 
    if not (shrink_node_is_atomic sn) then []
    else 
      List.filter shrink_node_is_atomic (BiDirectionalLabeledGraph.get_children sn)
  in

  let proc_node sn =
    if (not (shrink_node_is_atomic sn)) then ()
    else
      let sn_atomic_comp = BiDirectionalLabeledGraph.collect_component atomic_desc [sn] in
      let sn_ac_edges = List.flatten 
			  (List.map BiDirectionalLabeledGraph.get_out_edges sn_atomic_comp)
      in
	
      let new_atomic = List.exists (fun e -> not (irrelev_atomic  e)) sn_ac_edges
      in
	(BiDirectionalLabeledGraph.get_node_label sn).EnvAutomaton.atomic <- new_atomic
  in

    List.iter proc_node (BiDirectionalLabeledGraph.descendants [shrink_root])

in
  (* this keeps hitting the graph with "rewrites" until no change is possible ... *) 
let iterate_all_shrinks shrink_root = 
  let sn_adr_eq = sn_adr_eq true in

  let change_flag = ref true in

  let shrinkable_then_shrink sn = 

   let self_loop_shrink sn =
     (* remove all irrelevant self loop edges *)
     List.iter ( fun e -> 
		   if (irrelev_shrink_edge e && BiDirectionalLabeledGraph.get_target e = sn)
		   then 
		     begin
		       change_flag := true;
		       BiDirectionalLabeledGraph.delete_edge e
		     end
	       ) (BiDirectionalLabeledGraph.get_out_edges sn)
   in
   let pullin_child_shrink sn = 
     (* if there is a child with:
	1. same reg etc etc, 
	2. has a unique parent, 
	3. w/ an irrelevant edge ...
	4. is not the root
	5. all the other edges are irrelevant ...
	then delete it and copy its successors to me...
     *)
     let _ = Message.msg_string Message.Debug "In pullin_child" in
     let suck_succ e = 
       let sn' = BiDirectionalLabeledGraph.get_target e in
       let sim_edges = similar_edges e in
       if ((List.for_all irrelev_shrink_edge sim_edges) 
	   && sn_adr_eq sn sn' 
	   && (really_unique_parent sn') (* so no self loop *)
	   && (Some (get_shrink_node_id sn') <> !shroot)
	   && (sn <> sn'))
       then
	 begin
	   change_flag := true;
	   List.iter (BiDirectionalLabeledGraph.copy_edge_source sn) 
	     (BiDirectionalLabeledGraph.get_out_edges sn');
	   merge_and_delete sn sn'
         end
    in 
    let oute = BiDirectionalLabeledGraph.get_out_edges sn in
       List.iter suck_succ (cluster_edges BiDirectionalLabeledGraph.get_target oute)
  in
  let pullin_parent_shrink sn =   
    let _ = Message.msg_string Message.Debug "In pullin_parent" in
    
     (* reverse of the above -- the above is dshrink true -- this is dshrink false *)
    let suck_pred e = 
       let sn' = BiDirectionalLabeledGraph.get_source e in
       let sn = BiDirectionalLabeledGraph.get_target e in
       let sim_edges = similar_edges e in
       if ((List.for_all irrelev_shrink_edge sim_edges) 
	   && sn_adr_eq sn sn' 
	   && (really_unique_child sn') (* so no self loop *)
	   && (Some (get_shrink_node_id sn') <> !shroot)
	   && (sn <> sn'))
       then
	 begin
	   change_flag := true;
	   List.iter (BiDirectionalLabeledGraph.copy_edge_target sn) 
	     (BiDirectionalLabeledGraph.get_in_edges sn');
	   merge_and_delete sn sn'
         end
    in 
    (* if (Some (get_shrink_node_id sn) = !shroot) then ()
    else *) 
      let ine = BiDirectionalLabeledGraph.get_in_edges sn in
        List.iter suck_pred (cluster_edges BiDirectionalLabeledGraph.get_source ine)
  in

  let merge_children_shrink sn =  
    let _ = Message.msg_string Message.Debug "In merge_children_shrink" in
    let merge_children sn1 sn2 = 
      (* copy sn2's out edges to sn1, merge_and_delete sn1 sn2 *)
      let _ = List.iter (BiDirectionalLabeledGraph.copy_edge_source sn1) 
		(BiDirectionalLabeledGraph.get_out_edges sn2)
      in
      let _ = 
	List.iter 
	  (BiDirectionalLabeledGraph.hookup_wo_duplicate sn sn1) 
	  (List.map BiDirectionalLabeledGraph.get_edge_label
	     (BiDirectionalLabeledGraph.find_edges sn sn2))
      in (* this copies all edges from sn --> sn2 to sn --> sn1 *)
	merge_and_delete sn1 sn2;
	  change_flag := true
    in
    let candidates = Misc.compact (BiDirectionalLabeledGraph.get_children sn) in
    let find_appt_sibling sn1 =
      if (not (really_unique_parent sn1)) then None
      else 
	let good_sibling sn' = 
	  if (sn_adr_eq sn1 sn') && (sn' <> sn1) && (really_unique_parent sn')
	  then Some (sn') else None
	in
	  match (Misc.get_first good_sibling candidates) with
	      None -> None
	    | Some (sn') -> Some (sn1,sn')
    in
      match Misc.get_first find_appt_sibling candidates with
	  None -> ()
	| Some (sn1,sn2) -> merge_children sn1 sn2
  in

  let merge_parents_shrink sn = 
   let _ = Message.msg_string Message.Debug "In merge_parents_shrink" in
    let merge_parents sn1 sn2 = 
      (* copy sn2's in  edges to sn1, merge_and_delete sn1 sn2 *)
      let _ = List.iter (BiDirectionalLabeledGraph.copy_edge_target sn1) 
		(BiDirectionalLabeledGraph.get_in_edges sn2)
      in
      let _ = 
	List.iter 
	  (BiDirectionalLabeledGraph.hookup_wo_duplicate sn1 sn) 
	  (List.map BiDirectionalLabeledGraph.get_edge_label
	     (BiDirectionalLabeledGraph.find_edges sn2 sn))
      in (* this copies all edges from sn --> sn2 to sn --> sn1 *) 
	merge_and_delete sn1 sn2;
	  change_flag := true
    in
    let candidates = Misc.compact (BiDirectionalLabeledGraph.get_parents sn) in
    let find_appt_psibling sn1 =
      if (not (really_unique_child sn1)) then None
      else 
	let good_psibling sn' = 
	  if (sn_adr_eq sn1 sn') && (sn' <> sn1) && (really_unique_child sn')
	  then Some (sn')
	  else None
	in
	  match (Misc.get_first good_psibling candidates) with
	      None -> None
	    | Some (sn') -> Some (sn1,sn')
    in
      match Misc.get_first find_appt_psibling candidates with
	  None -> ()
	| Some (sn1,sn2) -> merge_parents sn1 sn2
  in
   (* begin the main thing in shrinkable_then_shrink *)
    self_loop_shrink sn;
    pullin_child_shrink sn;
    pullin_parent_shrink sn;
    merge_children_shrink sn;
    if (Options.getValueOfBool "nomp") then () else (merge_parents_shrink sn)
   (* end main thing in shrinkable_then_shrink *)
in
  while !change_flag do
    let all_nodes = BiDirectionalLabeledGraph.descendants [shrink_root] in
      change_flag := false;    
      Message.msg_string Message.Debug 
	("shrink_hammer: size = "^(string_of_int (List.length all_nodes)));
      List.iter shrinkable_then_shrink all_nodes
  done;
()
in				  
     
(* 1. fuse the nodes in nl to sn
   2. copy the edges in el to sn
*)

    (* phase 1: grow components till a back node or different adr is hit *)
let phase1 () = 
    Message.msg_string Message.Normal "Phase 1" ;
    unprocessed_nodes#add_element auto_root;
    while (unprocessed_nodes#has_next ()) do
      process_auto_node (unprocessed_nodes#next ())
    done;
  
 in

let dsquish_phase1 () = 
  Message.msg_string Message.Normal "Phase 1: dsq";
  let dsq_visit_table = Hashtbl.create 37 in
  
  let proc_an an = 
    if (Hashtbl.mem dsq_visit_table an) then ()
    else
      let _ = Hashtbl.replace dsq_visit_table an true in
      let shrink_node = new_shrink_node an in
      let _ = Message.msg_string Message.Normal ("dsquish: auto node:"^
	      (string_of_int (get_auto_node_id an)))
      in
      let an_cc = BiDirectionalLabeledGraph.collect_component dotted_neighbours [an] in
      let _ = List.iter (fun a -> Hashtbl.replace dsq_visit_table a true) an_cc in

      let _ = Message.msg_string Message.Normal ("dcomponent:"^(Misc.string_of_int_list (List.map get_auto_node_id an_cc))) in
      let _adr_cc = List.fold_left 
		     (fun curr b -> Region.adr_cup curr (BiDirectionalLabeledGraph.get_node_label b).EnvAutomaton.oadr )
		     ((BiDirectionalLabeledGraph.get_node_label an).EnvAutomaton.oadr) 
		     an_cc
      in
      let adr_cc = 
	if (Options.getValueOfBool "csq") 
	then Region.cartesian_adr _adr_cc else _adr_cc
      in
      let edge_leaving an' = 
          (
	    (BiDirectionalLabeledGraph.get_out_edges an') 
	    @ (BiDirectionalLabeledGraph.get_in_edges an'))
      in
      let _ = edge_list := (List.flatten (List.map edge_leaving an_cc)) @ !edge_list in
      let _ = List.iter (connect_shrink_auto shrink_node) an_cc in
      (BiDirectionalLabeledGraph.get_node_label shrink_node).EnvAutomaton.adr <- adr_cc 
	
  in
    List.iter proc_an (BiDirectionalLabeledGraph.descendants [auto_root])
in

let _ = if (Options.getValueOfBool "dsq") then dsquish_phase1 () else phase1 () in
  Message.msg_string Message.Normal "Shrink elts info";
    let il_to_s il = Misc.strList (List.map string_of_int il) in
    Hashtbl.iter (fun i elts_l -> Message.msg_string Message.Normal
		    (Printf.sprintf "%d : %s" i (il_to_s elts_l))) shrink_elts_map;
  Message.msg_string Message.Normal "Auto info";

    gather_modifies_info (); (* first, collect the modifies info for each shrink node created thus far *)
    add_edges ();
    (* phase 2: if a node has a single parent with the same adr then merge *)
    Message.msg_string Message.Normal "Phase 2" ;
    let shrink_root = auto_shrink_node auto_root in
    let _ = shroot := Some (get_shrink_node_id shrink_root) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr1" else fn) shrink_root in
    Message.msg_string Message.Normal "Phase 2.2" ;
    let _  = dshrink true true shrink_root in 
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr1" else fn) shrink_root in
    Message.msg_string Message.Normal "Phase 2.5" ;
    let all_shrunk_nodes = BiDirectionalLabeledGraph.descendants [shrink_root] in
    let _ = Hashtbl.clear shrink_visited_table in
    Message.msg_string Message.Normal "Phase 2.6" ;
    let _ = List.iter (dshrink true false) all_shrunk_nodes in
      (* how many times shall I iterate ? *)
    let _ = Hashtbl.clear shrink_visited_table in
    let _ = Message.msg_string Message.Normal "Phase 2.8" in
    
    let _ = Message.msg_string Message.Normal "Phase 3" in
    let sid = 
      match !shroot with Some i -> i | None -> failwith "Unknown shroot" in
    let shrink_root = try Hashtbl.find shrink_map sid with _ -> failwith ("shrink_root: "^(string_of_int sid)) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr2" else fn) shrink_root  
    in
    let _ = 
      if (Options.getValueOfBool "atc") then
	begin
	  Message.msg_string Message.Normal "Phase 2.9 : De-atomize";
	  deatomize shrink_root
	end
    in
    let _ = iterate_all_shrinks shrink_root in
    let sid = 
      match !shroot with Some i -> i | None -> failwith "Unknown shroot" in
    let shrink_root = try Hashtbl.find shrink_map sid with _ -> failwith ("shrink_root: "^(string_of_int sid)) in
    let _ = print_graph (let fn = (Options.getValueOfString "auto") in if fn <> "" then fn^".shr2" else fn) shrink_root  
    in
    (* update modifies information *)
(*    Hashtbl.iter 
     (fun key nd -> let list_of_real_nodes = List.map (fun d -> Hashtbl.find auto_map d) (Hashtbl.find shrink_elts_map key) in
	  let this_modifies = 
	    List.fold_left (fun modlist nextnode -> 
	      let outgoing_ops = List.fold_left 
		  (fun sofar thisone -> match BiDirectionalLabeledGraph.get_edge_label thisone with 
		    Abstraction.ProgramOperation op -> op::sofar | _ -> sofar) [] (BiDirectionalLabeledGraph.get_out_edges nextnode) in
	      let (_, writes) = Abstraction.reads_and_writes op in 
	      Misc.union writes modlist
	      ) [] list_of_real_nodes in
	    (BiDirectionalLabeledGraph.get_node_label nd).modifies <- this_modifies
		 ) shrink_map ;
*)
(*
    let some_bdd = PredTable.bddOne in
    let dummy_id_to_bdd = Array.create 0 some_bdd in
*)
    Message.msg_string Message.Normal "Phase 4" ;
Message.msg_string Message.Normal "Shrink elts info";
    let il_to_s il = Misc.strList (List.map string_of_int il) in
    Hashtbl.iter (fun i elts_l -> Message.msg_string Message.Normal
		    (Printf.sprintf "%d : %s" i (il_to_s elts_l))) shrink_elts_map;
 
    let size = !shrink_counter (*List.length (BiDirectionalLabeledGraph.descendants [shrink_root])*)  + (*stuck node *)1 in
    let _ = gather_modifies_info () in
      {
    EnvAutomaton.autom_root = shrink_root;
    EnvAutomaton.o_auto_root = auto_root;
    EnvAutomaton.number_of_states = size;
    EnvAutomaton.id_to_node_ht = shrink_map;
    EnvAutomaton.o_map = auto_map ;
    EnvAutomaton.cover_set = cov;
    EnvAutomaton.o_to_a_map = auto_shrink_map;
    EnvAutomaton.a_to_o_map = shrink_elts_map;
(*   
    EnvAutomaton.id_to_bdd = dummy_id_to_bdd;
    EnvAutomaton.stVarMap = [] ;
    EnvAutomaton.tsVarMap = [];
    EnvAutomaton.env_transrel = some_bdd;
*)
  }
 in
let s_auto = shrink_auto (tree_close tree_root) in
  Message.msg_string Message.Normal "Leaving mk_s_a";
  s_auto

let get_auto_size auto = 
  let nodes = BiDirectionalLabeledGraph.descendants [auto.EnvAutomaton.autom_root] in
    List.length nodes

  
(****************************************************************************)
 
   let write_tree_as_dot_file tree_root =
    (* print out interface as a dot file *)
     let intf_file = open_out (Options.getValueOfString "interface") in
     Printf.fprintf intf_file "digraph interface {\n" ;
     
     let output_node = (fun n -> 
       let nid = (get_id n) in
       let nreg = try (get_region n) with _ -> Region.bot in 
       let (i,j) = try (Region.get_location_id nreg) with _ -> (-1,-1) in
       let p = Predicate.toString (Abstraction.extract_pred nreg) in
       let color = (match (get_node_data n).mark with Unprocessed _ -> "red" | Processed_Covered _ -> "blue" | Processed_Uncovered _ -> "green" | _ -> "black") in
       Printf.fprintf intf_file "  %d[label=\"%d[%d,%d]:%s\",color=\"%s\"];\n" nid nid i j p color ;
       match Tree.get_parent_edge n with 
	 None -> ()
       |	Some e -> let op = C_Command.to_string (C_System_Descr.get_command (Tree.get_edge_label e)) in
         let parent = Tree.get_parent n in
         Printf.fprintf intf_file "  %d -> %d [label=\"%s\"]\n" (get_id parent) (nid) op ) 
     in 
     Tree.traverse_bfs output_node output_node tree_root;
     Printf.fprintf intf_file "} \n\n" ; 
     close_out intf_file ;
     ()
       


  let synthesize_one_interface seedPreds interface_fns err =
    (* the information about who covers whom *)
    (* For correctness:
       We maintain the invariant that all nodes in reached_region_cov are
       present in the tree. This is maintained by updating reached_region_cov
       whenever a tree node is deleted because of a false error, or because a 
       interface call is disallowed because of error.
    *)
    let reached_region_cov = ref (Region.emptyCoverers () )
    (* helper object to create nodes *)
    and tnc = new tree_node_creator

    (* the set of pending nodes to be processed *)
    and unprocessed_nodes = new Search.bfsiterator (* If you change this to a new search strategy, update_tree can break *)

    (* the global time used for time stamps *)
    and time = ref 0 in


    let (start_location, automaton_location) = C_System_Descr.build_automaton interface_fns in


    (* The function for constructing children .. and adding them to the tree *)
    let construct_children node accu op =
      (tnc#create_child (Unprocessed op) op node )::accu
    in
    let automaton_covered node reg outgoing_ops region_coverers =
      Message.msg_string Message.Normal "In automaton_covered. Argument is:" ;
      Message.msg_printer Message.Normal print_tree_node node ;
      
      
      let allcovs = Region.findAllExactCoverer region_coverers reg in
      let node_edges =  match outgoing_ops with None -> List.map get_op (Tree.get_children node)  | Some l -> l   in
      Message.msg_string Message.Normal "The outgoing edges of the current region are:\n";
      List.iter (fun e -> Message.msg_string Message.Normal (Operation.to_string (e))) node_edges ;
      let rec is_covered coverers =
	match coverers with 
          [] -> false
	| a :: rest -> (* check that the children of node are equal to the children of a *)
	    Message.msg_string Message.Normal "The current coverer: " ;
	    Message.msg_printer Message.Normal print_tree_node a ;
            let a_edges = List.map get_op (Tree.get_children a) in 
	    Message.msg_string Message.Normal "The outgoing edges of this coverer are:\n";
	    List.iter (fun e -> Message.msg_string Message.Normal (Operation.to_string (e))) a_edges ;
	(* let time_stamp = try (get_marking_data node).time_stamp with _ -> !time + 1 in *)
	    if ( (get_id a) <> (get_id node) (* (get_marking_data a).time_stamp< time_stamp*) ) && (Misc.subsetOf node_edges a_edges) 
		&& (Misc.subsetOf a_edges node_edges) 
            then
	      begin
		Message.msg_string Message.Normal "Aha: covered" ;
		true
	      end
            else is_covered rest 
      in
      let ans = is_covered allcovs 
      in
      Message.msg_string Message.Normal (if ans then "COVERED!" else "NOT COVERED!") ;
      ans
    in
    
    let update_tree n unprocessed_nodes = 
      unprocessed_nodes#empty_out();
      let nl_int = ref [] in
      let nl_leaf = ref [] in
      let reached_region_cov = Region.emptyCoverers () in
      let f_int = 
	(fun n -> let nreg = get_region n in  
	Region.addCoverer reached_region_cov nreg n) in
      let f_leaf = (fun n -> nl_leaf := n::(!nl_leaf)) in
      let _ = Tree.traverse_bfs f_leaf f_int n in
      let nl_leaf = List.rev !nl_leaf in
      let _ = Message.msg_string Message.Debug "Leaf Nodes" in
      let _ = List.iter 
	  (fun n -> Message.msg_printer Message.Debug  print_tree_node n) 
	  nl_leaf
      in
      let process n = 
	let n_data = get_node_data n in 
	match n_data.mark with
	  Unprocessed _ ->
	    if ( (Tree.has_child n)) then failwith "Unprocessed node has children!" ;
	    begin
	      unprocessed_nodes#add_element n ;
	      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	    end
	|  Processed_Was_Covered_To_Reprocess md ->
	    begin
	      Message.msg_string Message.Debug "Adding to unprocessed#nodes";
	      Message.msg_printer Message.Debug print_tree_node n ;
	      unprocessed_nodes#add_element n ;
	      tnc#delete_children n ;
	    end
	| Processed_Covered (md, ops) ->
	    let covered = 
	      let (i,j) = Region.get_location_id md.region and
		  (li,lj) =  C_System_Descr.location_coords automaton_location in
              if (i = li) then 
			(* check that 1. the current region n_region is covered by some region in reached_region_cov
		           and that 2. the outgoing edges are a subset of the outgoing edges of the coverer *)
		if (j = lj) then automaton_covered n (get_region n) ops reached_region_cov
		else false
              else
                       (* (Stats.time "Covered check:" (Abstraction.covered n_region) !reached_region)  *)
		if (Region.is_empty md.region) then true 
		else
		  match Region.findExactCoverer reached_region_cov md.region
		  with Some _ -> true | None -> false
	    in
	    if not covered then
              begin
		n_data.mark <- Processed_Was_Covered_To_Reprocess md ;
		
		Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		Message.msg_printer Message.Debug print_tree_node n ;
		unprocessed_nodes#add_element n ;
		tnc#delete_children n ;
		
              end
	    else
              ()
		
	| Processed_Uncovered md ->
	    begin
		  (* it should be the case that this node, oddly enough, has NO children, i.e. this pc has no children,
		     so it doesnt matter if I add it to cover or not. Indeed I shouldnt, and I shouldnt try to add it to 
		     the list of things to process either -- RJ. *)
	      if (Abstraction.enabled_ops (get_region n) <> []) then
		begin
		  Message.msg_string Message.Normal "About to die";
		  Message.msg_printer Message.Normal print_tree_node n ;
		  failwith "ugh! process_n_leaf gets an internal node!"
		end
	    end
      in
      let _ = List.iter process nl_leaf in
      (reached_region_cov : 'a Region.coverers) 
    in
    let do_subtree_cut_refinement tree_root anc new_anc_region =
      let anc_data = Tree.get_node_label anc in
      let anc_marking_data = get_marking_data anc in
        (* update the ancestor region *)
      anc_marking_data.region <- new_anc_region ;
      
        (* Note that here there is no path to update as the entire subtree is going to be chopped off... *)
      tnc#delete_children anc;
      
      let children = if (Region.get_location_id new_anc_region = C_System_Descr.location_coords automaton_location) then
	failwith "In do_subtree_cut_refinment: refinement at a fake node!"
               (* This should not happen ??  *)
      else List.fold_left (construct_children anc) [] (Abstraction.enabled_ops new_anc_region) 
      in
      (* mark n as processed and uncovered *)
      anc_data.mark <- Processed_Uncovered anc_marking_data ;

      Message.msg_string Message.Debug "Updating the currently reached region" ;
      (* This should also update coverers *) 
      let cov = Stats.time "update_tree"  (update_tree tree_root) unprocessed_nodes 
      in
      reached_region_cov := cov ; () 
    in


    (* this function deletes covered nodes from the tree *)
    let rec reclaim n = 
      if (Tree.has_child n || (not (Tree.has_parent n))) then ()
      else 
	begin
	  let par = get_parent n in
	  Tree.delete_child n; 
	  reclaim par
	end
    in
    let init = Abstraction.create_region [ start_location  ] false seedPreds Predicate.True in
 (* the root of the reachability tree *)
    let tree_root =
      tnc#create_root (Processed_Was_Covered_To_Reprocess { time_stamp = !time ;
                                                            region = init ; })
    in
      (* add the root to the pending unprocessed nodes *)
      unprocessed_nodes#add_element (tree_root) ;

      while (unprocessed_nodes#has_next ()) do
        stats_nb_iterations := !stats_nb_iterations + 1;

        (* the search is not finished yet *)
	if (!stats_nb_iterations mod 100 == 0) then
	  begin
            Message.msg_string Message.Major ("While loop tick:"^(string_of_int !stats_nb_iterations));
	    flush stdout; flush stderr;
	    Abstraction.print_abs_stats () ;
	    (* write_tree_as_dot_file tree_root ; *)

	  end ;
	Message.msg_string Message.Debug "\n\n****************************************************************************\n" ;
        Message.msg_string Message.Debug "Next iteration of model-check's big while-loop" ;
	Message.msg_printer Message.Debug Format.pp_print_int (!stats_nb_iterations);
        (* Message.msg_string Message.Debug "Currently reached region:" ;
           Message.msg_printer Message.Debug Region.print !reached_region ;*)

        (* let's process the next pending node *)
        let n        = unprocessed_nodes#next () in
        Message.msg_string Message.Debug "Now processing tree node:" ;
        Message.msg_printer Message.Debug print_tree_node n ;

        let n_data   = (Tree.get_node_label n) in

        let n_marking_data = 
           match n_data.mark with
               Unprocessed _ ->
		  begin
		  try
                    let op = (get_op n) in
                      { time_stamp = (time := !time + 1 ; !time) ;
			region = Stats.time "main post" (Abstraction.post (get_region (get_parent n)) op)  global_variable_table; }
	  	  with Abstraction.RecursionException ->
			begin
			  let par = get_parent n in dumppath par (get_region par); (*  n_marking_data.region; *)
			  failwith "Program is recursive!"
			end
		  end
              | Processed_Was_Covered_To_Reprocess md ->
                  md
              | _ -> failwith "Synthesize Interfaces: unexpected marking data"

          in
          let n_region = n_marking_data.region in
          (* the error region at this node *)
          let err_n = Stats.time "cap" (Region.cap n_region) err in
          Message.msg_string Message.Debug ("Error region at this node: " ^ (Region.to_string err_n)) ;

            if (Region.is_empty err_n)
            then
              begin
                (* no error found at this node *)
                Message.msg_string Message.Debug "No error found at this node" ;
                Message.msg_string Message.Debug "Constructing its successor children..." ;
                (* we construct its successor children *)
                let children = 
                    List.fold_left (construct_children n) []
                      (Abstraction.enabled_ops n_region) 
		in
		
                Message.msg_string Message.Debug "The children are:" ;
                List.iter (function child -> Message.msg_printer Message.Debug print_tree_node child) children ;
		    

                (* let's test whether this node is covered *)
                Message.msg_string Message.Debug "Let's test whether this node is covered" ;
		let covered = 
		    begin 
                    let (i,j) = Region.get_location_id n_region and
                        (li,lj) =  C_System_Descr.location_coords automaton_location in
                    if (i = li) then 
			(* check that 1. the current region n_region is covered by some region in reached_region_cov
		             and that 2. the outgoing edges are a subset of the outgoing edges of the coverer *)
                      if (j = lj) then automaton_covered n n_region None !reached_region_cov
                      else false
                    else
                       (* (Stats.time "Covered check:" (Abstraction.covered n_region) !reached_region)  *)
	               if (Region.is_empty n_region) then true 
                       else
                       match Region.findExactCoverer !reached_region_cov n_region
                       with Some _ -> true | None -> false
                    end 
		in
		if (covered) then
                  begin
                    (* this node is covered *)
		    Message.msg_string Message.Debug "This node is covered" ;
		    (* The children of this node are unnecessary. We delete them to maintain the invariant
                       that a Processed_Covered node has no children *)

		    
                    (* mark n as processed and covered *)
		    n_data.mark <- Processed_Covered (n_marking_data, Some (Abstraction.enabled_ops n_region) );
		    tnc#delete_children n ;	

		    if (Options.getValueOfBool "reclaim") 
		    then
		      reclaim n;
		    ()
                  end
		else
		  begin
                    (* this node is not covered *)
                    Message.msg_string Message.Debug "This node is not covered" ;

                      (* add the children to the set of pending nodes *)
                    Message.msg_string Message.Debug "Adding the children to the set of pending unprocessed#nodes" ;
                    unprocessed_nodes#add_list children ;
		    Message.msg_string Message.Debug ("Remaining nodes:"^(string_of_int (unprocessed_nodes#sizeof ()))); 
		    unprocessed_nodes#iter (fun x -> Message.msg_printer Message.Debug print_tree_node x);
                      (* mark n as processed and uncovered *)
                    Message.msg_string Message.Debug "Updating the node's marking" ;
                    n_data.mark <- Processed_Uncovered n_marking_data ;
		    
                    Message.msg_string Message.Debug "This node now looks like:" ;
                    Message.msg_printer Message.Debug print_tree_node n ;
	            
                      (* add n's region to the reached region *)
		    Message.msg_string Message.Debug "Updating the currently reached region" ;
		    (* maintain more structured information about reached states in reached_region_cov *)
		    Region.addCoverer !reached_region_cov n_region n
		  end
              end
            else
              begin
                (* error found *)
                Message.msg_string Message.Debug "Error found : checking validity." ;
		Message.msg_string Message.Debug ("Error at depth"^(string_of_int (Tree.node_depth n))); 
                begin
                  Message.msg_string Message.Debug "Error found at this node" ;
		    
                  (* this node will have to be reprocessed after refinment *)
		  (* JHALA: why will this node have to be reprocessed after refinement ? *) 
                  (* RUPAK: The following is not required, I think. Commenting out.
                    Message.msg_string Message.Debug "Re-adding this node to the set of pending unprocessed nodes" ;
                    n_data.mark <- Processed_Was_Covered_To_Reprocess n_marking_data ;
		    Message.msg_string Message.Debug "Adding to unprocessed#nodes";
		    Message.msg_printer Message.Debug print_tree_node n ;
                    unprocessed_nodes#add_element n ;
                  *)
		    
                    (* let's now test whether this error is a real one *)
                  Message.msg_string Message.Debug "Testing whether this error is a real one" ; 
		    try 
		      match (Stats.time "check_error" (check_error n) err_n) with
			    EmptyErrorAtNode(err_anc, anc, path) ->
				(* this is a false error *)
			      begin
				Message.msg_string Message.Debug "This is a false error (seq)" ;
				Message.msg_string Message.Debug "The check_error function ended up with" ;
				Message.msg_string Message.Debug "     at ancestor: " ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				Message.msg_string Message.Debug "     along the path (from the ancestor to the current node):" ;
				Message.msg_printer Message.Debug print_tree_node anc ;
				List.iter (function x -> Message.msg_printer Message.Debug print_tree_node x) path ;
				
				if (Options.getValueOfBool "stop") then
				  failwith ("FALSE ERROR CHOKE");
				(* let's refine *)
				Message.msg_string Message.Debug "\n\nLet's refine the nodes along that path" ;
				if (Options.getValueOfBool "traces") then dumppath n n_region;
				
			      	let anc_region = (get_region anc) in
				let focused_anc_region = 
				  try
				    let rv = 
				      match Options.getValueOfBool "block" with
					  false ->
					    Stats.time "focus" (Abstraction.focus anc_region) err_anc 
					| true -> Abstraction.focus_stamp anc_region 
					    (* analysis is already done! *) 
				    in
				    if (Options.getValueOfBool "localrestart") then
					local_restart_counter := !local_restart_counter + 1;
				    if (Options.getValueOfBool "restart" || 
                                        ( Options.getValueOfBool "localrestart" && !local_restart_counter mod 15 == 0)) then 
				      begin
					Message.msg_string Message.Error "About to Restart";
					  					  
					raise RestartException
					    
				      end
				    else 
				      begin
					Options.setValueOfBool "localrestart" false;
					rv
				      end
				  with Abstraction.NoNewPredicatesException ->
				    begin
 				      if (Options.getValueOfInt "predH" = 3) then
					begin
					  let path_to_root = Tree.path_to_root n in
					  let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					  let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
					  Message.msg_string Message.Debug 
					    ("\n No of regs: "^(string_of_int (List.length list_of_regs)));
					  Message.msg_string Message.Debug 
					    ("\n No of ops : "^(string_of_int (List.length list_of_ops)));
					  try 
					    (Abstraction.find_all_predicates list_of_regs list_of_ops; 
					     raise RestartException
					       )
					  with e -> 
					    
					    begin
					      if (e = RestartException) then raise e 
					      else 
						begin
						  Message.msg_string Message.Minor "This is a false error" ;
						  Message.msg_string Message.Minor "The check_error function ended up with" ;
						  Message.msg_string Message.Minor ("     an empty error: " ^ (Region.to_string err_anc)) ;
						  Message.msg_string Message.Minor "     at ancestor: " ;
						  Message.msg_printer Message.Minor print_tree_node anc ;
						  Message.msg_string Message.Minor "     along the path (from the ancestor to the current node):" ;
						  Message.msg_printer Message.Minor  print_tree_node anc ;
						  List.iter (function x -> Message.msg_printer Message.Minor print_tree_node x) path ;
						  Message.msg_string Message.Debug ("find_all_predicates raises : "^(Printexc.to_string e)) ; 
						  dumptrace (false, list_of_ops, list_of_regs);
						  failwith ("problem in find_all_predicates !") 
						end
					    end
					end
				      else 
					if (Options.getValueOfInt "predH" = 1 && 
					    (not (Abstraction.predicate_fixpoint_reached ()))) 
					then (Message.msg_string Message.Debug "Raising Restart"; raise RestartException)
					else
					  begin
					    let path_to_root = Tree.path_to_root n in
					    let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region] in
					    let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
					      dumptrace (false, list_of_ops, list_of_regs); Message.msg_string Message.Normal "No new predicates found!";
					      failwith ("No new preds found !-- and not running allPreds ...")
					  end
				    end
				in (* for the focused_anc_region *)
				Message.msg_string Message.Debug "Done calling Focus";
				Message.msg_string Message.Debug ("The focused region for the ancestor is: " ^ (Region.to_string focused_anc_region)) ;
				
				stats_nb_refinment_processes := !stats_nb_refinment_processes + 1 ;
				Stats.time "subtree_cut_refinement" (do_subtree_cut_refinement tree_root anc) focused_anc_region;
                              end
			| NonEmptyErrorAtRoot(err_root, root, path) ->
			    begin
			    (* --- for debugging --- *)
			      assert (root == tree_root) ;
			    (* this is a real error *)
                              (* print the unknown functions on the path *)
			      (* Abstraction.print_debug_info (List.map get_op path); *)
			       let err_node = (match path with
                                  [] -> root
                                | _ -> Misc.list_last path) in
                               let path_to_root = Tree.path_to_root err_node in
                               let list_of_regs = (List.map (get_region)  (Misc.chop_last path_to_root))@[n_region ] in
                               let list_of_ops = List.map (get_op) ( List.tl path_to_root) in
                               dumptrace (true,list_of_ops,list_of_regs);
	                       Message.msg_string Message.Normal "The list of locations along the error trace: ";
                               List.iter (fun r -> 
                                 let (i,j) = (Region.get_location_id r) in 
                                 Message.msg_string Message.Normal (Printf.sprintf "(%d, %d)" i j)) list_of_regs ;


			      
			      (* find the last interface function called and remove it *)
			      let rec find_fake path child =
				match path with [] -> failwith "Did not find any fake location along path!"
				| a :: rest ->
				    begin
			              let reg = try get_region a with _ -> n_region (* the last node raises an exception. BAD CODE *)
                                      in
				      let (i,j) = Region.get_location_id reg in
                                      let (ai, aj) = C_System_Descr.location_coords automaton_location in
                                      Message.msg_string Message.Normal (Printf.sprintf "find_fake : Path(%d,%d), automaton(%d,%d)" i j ai aj);
				      if (i = ai && j = aj) then 
					begin
					  match child with Some c -> (a,c)
					  | None -> failwith "The last location along the trace is an error!"
					end
				      else find_fake rest (Some a)
				    end
                              in
			      let (fake_node, itschild) = find_fake (List.rev path_to_root) None in
			      (* remove this function call *)
			      Message.msg_string Message.Normal "Removing the edge in the tree " ;
			      Message.msg_printer Message.Normal print_tree_node fake_node ;
			      Message.msg_printer Message.Normal print_tree_node itschild ;
			      tnc#delete_child itschild ;
			      Message.msg_string Message.Normal "Done" ;
			      reached_region_cov := Stats.time "update_tree" (update_tree tree_root) unprocessed_nodes ;
                              
                              (* at this point, we can check if this node, after the current edge has been 
		                 deleted, is covered by a previously existing node.
		                 However, we should be careful to make sure that the coverers are only nodes
                                 that appeared before this one did. We can use the timestamp to ensure this.
				 CHECK!! CHECK!!
                               *)
	                      if (automaton_covered fake_node (get_region fake_node) None !reached_region_cov) then 
                                begin
                                  Message.msg_string Message.Normal "The fake node got covered when we removed the function!" ;
   
				 (* mark this node as Processed_Covered *)
                                  let  n_data = Tree.get_node_label fake_node in
                                  let n_marking_data = get_marking_data fake_node in
                                 (* mark fake_node as processed and covered *)
                                  n_data.mark <- Processed_Covered (n_marking_data, Some (List.map (get_op) (Tree.get_children fake_node))) ;
      
				  tnc#delete_children fake_node ;



                                 (* update tree should also update coverers *)
			         let rregcov = Stats.time "update_tree" 
				   (update_tree tree_root) unprocessed_nodes in
                                 reached_region_cov := rregcov ;
                                end 
                              else (); 

                              (* and continue the search *)  
			    end
		      with RestartException -> 
			begin
			  stats_nb_refinment_processes := !stats_nb_refinment_processes + 1;
			  do_subtree_cut_refinement tree_root tree_root (get_region tree_root)
			end
		  end
	      end
      done ;
    let this_autom = (make_shrink_auto tree_root ) in
    (tree_root, this_autom )


(****************************************************************************)
(* quick and dirty mod analysis. Replace by the "right" one. *)
(* No recursion, and globals are directly modified. *)
let modref_tbl = Hashtbl.create 11 

let rec modref f =
  if Hashtbl.mem modref_tbl f then
    Hashtbl.find modref_tbl f
  else
    begin
      let find_reads_and_writes read_flag = function edge ->
	let cmd = C_System_Descr.get_command edge in
	let accessed = if read_flag then cmd.C_System_Descr.Command.read else cmd.C_System_Descr.Command.written in
	let tmplist = List.filter (C_System_Descr.is_global) accessed in
	let called = 
	  match cmd.C_System_Descr.Command.code with
	    C_System_Descr.Command.FunctionCall(Expression.FunctionCall (Expression.Lval (Expression.Symbol s), _)) 
          | C_System_Descr.Command.FunctionCall(Expression.Assignment(_, _, Expression.FunctionCall (Expression.Lval (Expression.Symbol s), _))) ->
	      let (r, w) = modref s in
	      if read_flag then r else w
	  | _ -> []
	in
	tmplist @ called
      in
      let find_read = find_reads_and_writes true in
      let find_written = find_reads_and_writes false in
      let written = List.flatten (C_System_Descr.map_edges_fname find_written f) in
      let read = List.flatten (C_System_Descr.map_edges_fname find_read f) in
      Hashtbl.add modref_tbl f (read, written) ;
      (read, written)
    end

(*****************************************************************************)

let build_graph interface_fns =
  let fun_node_table = Hashtbl.create 31 in
    (* for each interface function, list all modified, and all read *)
  List.iter (fun fn -> 
    let f_node : (string, unit) BiDirectionalLabeledGraph.node = BiDirectionalLabeledGraph.create_root fn in
    Hashtbl.add fun_node_table fn f_node)
    interface_fns ;
    (* build graph *)
    (* this is quadratic! *)
  let link_edges f allfs =
    let fn = Hashtbl.find fun_node_table f in
    let (_, read_f) = modref f in
    List.iter 
      (fun g -> let (modif_g, _) = modref g in
      if Misc.intersection modif_g read_f <> [] then 
	(let gn = Hashtbl.find fun_node_table g in 
	BiDirectionalLabeledGraph.hookup_parent_child gn fn () ) 
      else () ) allfs
  in
  List.iter (fun f -> link_edges f interface_fns) interface_fns ;
  Message.msg_string Message.Normal "**************************************************";
  Hashtbl.iter (fun key data ->
    BiDirectionalLabeledGraph.output_graph_dot (fun x -> x^"$$ []") (fun x -> "()") "" [ data ]) fun_node_table ;
  Message.msg_string Message.Normal "**************************************************";
  Hashtbl.clear modref_tbl ;
  fun_node_table
    

  let synthesize_interface seedPreds o_err =
    Message.msg_string Message.Normal "In synthesize_interface" ;
    let err = 
      match o_err with
	None -> failwith "synthesize_interface: error region not specified."
      | Some e -> Abstraction.create_region [ e ] true Predicate.True Predicate.True
    in 
    let all_interface_fns = List.filter (C_System_Descr.is_interface) (C_System_Descr.list_of_functions ()) in
    Message.msg_string Message.Normal "Interface functions are:" ;
    List.iter (fun s ->     Message.msg_string Message.Normal s) all_interface_fns ;
    Message.msg_string Message.Normal "Building dependence graph" ;
    (* build a graph, with an edge from f to g if f uses some data
	modified by g,
	*)
    let dep_graph = build_graph all_interface_fns in
    Message.msg_string Message.Normal "Finished Building dependence graph" ;
    (* a source is a node with no parents (incoming edges) *)
    (*  traverse the graph from sources, and generate reachable parts
	of the graph
	For each such reachable part, call synthesize_one_interface
       *)
    let source_nodes = Hashtbl.fold 
	(fun key data current -> if (Misc.subsetOf (BiDirectionalLabeledGraph.get_parents data) [ data ]) then data :: current else current) 
	dep_graph []  in
    Message.msg_string Message.Normal "Source nodes are:" ;
    List.iter (fun s -> Message.msg_string Message.Normal (BiDirectionalLabeledGraph.get_node_label s)) source_nodes ;
    List.iter (fun sn -> 
      let reachable_nodes = BiDirectionalLabeledGraph.descendants [ sn ] in
      let connected_fns = List.map (fun nd -> BiDirectionalLabeledGraph.get_node_label nd) reachable_nodes in 
      Message.msg_string Message.Normal "Building interfaces for:" ;
      List.iter (fun s -> Message.msg_string Message.Normal s ) connected_fns ;
      let (tree_root, autom) = synthesize_one_interface seedPreds connected_fns err in
      Message.msg_string Message.Debug ("Depth of tree: "^(string_of_int (Tree.subtree_depth tree_root)));
      print_whole_tree Message.Normal tree_root;
      write_tree_as_dot_file tree_root )
      source_nodes ;
    ()
      

