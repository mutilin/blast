(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)


(* important globals/state variables:
   visited_table,
   error_location_id
*)

module Stats = VampyreStats

(*****************************************************************************************)
(*****************************************************************************************)
(*****************************************************************************************)
(*                                                                                       *)
(*                                     The WP DAG MODULE                                 *)
(*                                                                                       *)
(*****************************************************************************************)
(*****************************************************************************************)
(*****************************************************************************************)

(**
 * This module implements a procedure that when given a DAG cfa, with a single sink,
  (and a single root ?) pulls the wp back from the sink to the root  .
 *)
 
(* here we go ... *)
module Symbol = Ast.Symbol
module Constant = Ast.Constant
module Expression = Ast.Expression
module Predicate = Ast.Predicate
module C_SD = BlastCSystemDescr.C_System_Descr
module Command = BlastCSystemDescr.C_Command

(*****************************************************************************************)
(************************************ MAP functions **************************************) 
(*****************************************************************************************)

(* first the functions that deal with the variable map i.e. the v: lval --> lval
   where the v(l) = the "current" value of l 
*)

let mapsize = 31
let loc_table_size = 31 


(* visited table is another global variable that is a map from loc_id -> locations *)

let visited_table = Hashtbl.create loc_table_size

let error_location_id = ref None
let get_err_loc_id () = 
  match !error_location_id with
      None -> failwith "Error location not set!"
    | Some x -> x

let ctr = ref 0
  
let new_map () = 
  Hashtbl.create mapsize

let copy_update_map v changed_lvals = 
  (* we assume the precondition that every table has one 1 binding per key *)
  (* we assume that the list changed_lvals does not have copies *)
  let new_table = new_map () in
  let _ = List.iter (fun x -> Hashtbl.replace new_table x (Expression.fresh_lval x)) changed_lvals 
  in
  let _ = Hashtbl.iter 
	    (fun x y -> 
	       if Hashtbl.mem new_table x then () 
	       else Hashtbl.replace new_table x y
	    ) v  
  in
    new_table

(** join v_l -> v,D where v_l is a list of maps and v is their "join" and D the variables that need to be updated *)
(** IMP: go over this to double check that all maps have single bindings *)

let join v_l = 
  let temp_table = new_map () in
  let diff = ref [] in
  let proc key data =
    match Hashtbl.find_all temp_table key with
	[] -> Hashtbl.replace temp_table key data
      | [lval] -> if not (lval = data) then diff := key::(!diff)
      | _ -> failwith "inconsistent temp_table"
  in
    List.iter (fun t -> Hashtbl.iter proc t) v_l;
    (copy_update_map temp_table !diff, !diff)

(*****************************************************************************************)
(***** a replace function, that takes an expression and computes e [v] for a substable v *)
(*****************************************************************************************)

let map_of_v v = 
  fun x -> 
    try Hashtbl.find v x
    with Not_found -> Expression.Lval x
  
let eqcons e1 e2 =
  Predicate.Atom (Expression.Binary(Expression.Eq,e1,e2))

(** Which variables in the domain of v are possibly aliased to lval *)
let aliases v lval = 
  List.filter  
    (fun x -> 
       let rv = 
	 if x <> lval then
	   AliasAnalyzer.queryAlias 
	     (Expression.Lval x) 
	     (Expression.Lval lval) 
	 else 
	   true 
       in
	 Message.msg_string Message.Debug (Printf.sprintf "Aliascheck: %s %s : %b" (Expression.lvalToString lval) (Expression.lvalToString x) rv);
	 rv
    ) 
    (Misc.hashtbl_keys v) 

(*****************************************************************************************)
(************************************Different Pres **************************************) 
(*****************************************************************************************)

let pre_v_asn v lval =
  let changed_vars = aliases v lval in
    copy_update_map v changed_vars   

let pre_v_asm v lval_l =
  let v' = copy_update_map v [] in
    List.fold_right 
      (fun lval tbl -> 
	 if Hashtbl.mem tbl lval then () 
	 else Hashtbl.replace tbl lval (Expression.Lval lval);
	 tbl
      ) lval_l v'

let phi i = Predicate.Atom (Expression.Lval (Expression.Symbol (Printf.sprintf "ND%d" i)))

let pre (op,n) =
  let (n_id_fmla, n_v) = 
    match n with 
	(id,_,Some(v,_)) -> (phi id,v)
      | _ -> failwith "invalid data at successor node"
  in
  let acp asn (fmla,v) =
    match asn with
      | Command.Expr (Expression.Assignment(Expression.Assign,x,e)) -> 
	  let v' = pre_v_asn v x in	  
	  let mapv = map_of_v v in
	  let mapv' = map_of_v v' in
	  let e' = Expression.deep_alpha_convert (map_of_v v') e in
	  let lx = Expression.Lval x in
	  let con = 
	    (eqcons (Expression.deep_alpha_convert mapv lx) e')::
	    (List.map
	       (fun y ->
		  let (ly) = (Expression.Lval y) in
		  let addrcons = (AliasAnalyzer.generateAliasConstraints x y) in
		  let pos = 
		    Predicate.conjoinL 
		      [addrcons;eqcons (Expression.deep_alpha_convert mapv ly) e'] 
		  in
		  let neg = 
		    Predicate.conjoinL 
		      [Predicate.Not addrcons;
		       eqcons (Expression.deep_alpha_convert mapv ly) (Expression.deep_alpha_convert mapv' ly)]
		  in
		    Predicate.disjoinL [pos;neg]
	       )
	       (aliases v x))
	  in
(* Jhala: check if you should use "deep" alpha convert here or just the lookup in the hashtable *)
(* VVV IMP ! *)
	    ((* v(x) == e[v'] ::constraints*) con@fmla, v') (* TBD: constraints for aliased variables *)
      | _ -> failwith "invalid statement fed to acp"
  in
    match op.Command.code with
	Command.Pred p -> ((Predicate.deep_alpha_convert (map_of_v n_v) p)::[n_id_fmla], 
		     pre_v_asm n_v (Predicate.allVarExps_deep p)) (* returns lvals ? *)
      | Command.Block l -> List.fold_right acp l ([n_id_fmla],n_v)
      | _ -> failwith "ugh: bad code!"

(** diffcons v' d v gives the constraints connecting the changed variables of v' with their changed values in v *)
let diffcons v' d v =
  let mapv = map_of_v v in (* should we be using the map here ? *)
  let mapv' = map_of_v v' in
  let cons lv = eqcons 
		  (Expression.deep_alpha_convert mapv (Expression.Lval lv)) 
		  (Expression.deep_alpha_convert mapv' (Expression.Lval lv))
  in
    Predicate.conjoinL (List.map cons d)
  
(** These are the  the main function that is passed to the dag-iterator *)
(* the argument passed is a list of edge_labels, node_labels *)

(***************************************************************************************************)
(******************************** THe functions passed to the DAG iterator *************************)
(***************************************************************************************************)

let f n_label e_n_l =
  let boolean_dot l1 l2 = 
    if not (List.length l1 = List.length l2) then failwith "boolean_dot: bad args !"
    else (*Predicate.disjoinL*) (List.map2 (fun x y -> Predicate.conjoinL [x;y]) l1 l2)
  in
  let (pref_l,prev_l) = Misc.unzip (List.map pre e_n_l) in
  let (n_v,d) = join prev_l in
  let cons_l = List.map (fun v -> diffcons v d n_v) prev_l in
    (Misc.fst3 n_label,
     Misc.snd3 n_label,
     Some (n_v ,boolean_dot cons_l (List.map Predicate.conjoinL pref_l))  )
    

(* RJ: This needs to be changed to be True only if the leaf corresponds to error_location *)
let leaf_val error_coords (i,xy,_) = 
  (i,xy,Some(new_map (),if xy = error_coords then [Predicate.True] else []))


let is_init_fn n =
  match BiDirectionalLabeledDag.get_node_label n with
      (_,_,Some(_)) -> true
    | (_,_,None) -> false


(************************************************************************************************)
(*************   The main function that returns the fmla for the wp of a dag   ******************)
(************************************************************************************************)


(* 1. hit the root of the dag with the above 3 functions 
   2. go over the dag and collect nodes, and take the fmla component
   3. and all the fmlas *)

let cons_node n =
  match BiDirectionalLabeledDag.get_node_label n with
      (_,_,Some(_,f)) -> f
    | _ -> failwith "Cons_node given unprocessed node!"

let map_node n = 
    match BiDirectionalLabeledDag.get_node_label n with
      (_,_,Some(v,_)) -> v
    | _ -> failwith "map_node given unprocessed node!"

let weakest_pre root  = 
  let error_coords = get_err_loc_id () in
  let _ = BiDirectionalLabeledDag.compute_and_update_root_val is_init_fn f (leaf_val error_coords)  root
  in
  let (root_id,_,_) = BiDirectionalLabeledDag.get_node_label root in 
  let constr_node x = 
    match x with
	(id,_,Some(_,f)) -> Predicate.Iff (phi id, (Predicate.disjoinL f))
      | _ -> Predicate.True
  in
    Predicate.conjoinL ((phi root_id)::(BiDirectionalLabeledDag.dag_map constr_node root)) 
  
(***********************************************************************************************)
(***************************All that remains is to get the unrolled DAG! ***********************)
(***********************************************************************************************)

(********************** Hmm ... now the somewhat more iffy code for the unrolling **************)

let dag_ctr = ref 0
let new_node_id () = dag_ctr := !dag_ctr + 1; !dag_ctr

(* as usual we will do everything backwards ... *)
let copy_map map = 
    let _t = Hashtbl.create loc_table_size in
    let _ = Hashtbl.iter (fun x y -> Hashtbl.replace _t x y) map in
      _t

let is_not_latest map n = 
  let (_,loc_id,_) = BiDirectionalLabeledDag.get_node_label n in
    not (n = Hashtbl.find map loc_id)
    
let oe_pairs map e_list =
  let rec _oep el =
    match el with
	[] -> []
      | e::t -> 
	  let e_targ_id = C_SD.location_coords (C_SD.get_target e) in
	    if (Hashtbl.mem map e_targ_id)
	    then (e,Hashtbl.find map e_targ_id)::(_oep t)
	    else _oep t
  in
    _oep e_list
      

let make_node (old_map,new_map) loc_id loc =
  (* we will NOT create a new node if the children of the old one are all "new" *)
  let (i1,i2) = loc_id in
  let _ = Message.msg_string Message.Debug (Printf.sprintf "in make_node %d %d" i1 i2) in
  let oe = C_SD.get_outgoing_edges loc in  
  let succ_list = oe_pairs old_map oe in
  let really_make = 
    if succ_list <> [] then
      begin (* there is some child that is in the dag *) 
	(* claim: the set of enabled children grows monotonically with each level *)
	if Hashtbl.mem old_map loc_id then
	  (* an old node exists *)
	  let on = Hashtbl.find old_map loc_id in
	  let children = BiDirectionalLabeledDag.get_children on in
	    (List.length children < List.length succ_list) || (List.exists (is_not_latest old_map) children)
	else (* no node exists! shock! instantiate a copy ... *)
	  true
      end
    else (* no children already exist in the dag *)
      false
  in
    (* the second conjunct is to not add extra nodes for the error location.
       could tweak the CFA so that the error location has no outgoing edges but
       I dont want to mess with that code ! *)
    match really_make && (loc_id <> get_err_loc_id ()) with
	false -> Message.msg_string Message.Debug "No node made"
      | true ->
	  begin
	    let _ = Message.msg_string Message.Debug "Making node" in
	    let nn = BiDirectionalLabeledDag.create_root (new_node_id (), loc_id, None) in
	    let _ = List.map 
		      (fun (e,e_n) -> 
			 BiDirectionalLabeledDag.hookup_parent_child nn e_n (C_SD.get_command e))
		      succ_list
	    in
	      Hashtbl.replace new_map loc_id nn
	  end
	  


let fill_visited_table fname = 
  let rec traverse_location_and_edges loc =
    let (loc_id) = C_SD.location_coords loc in
      if (Hashtbl.mem visited_table loc_id) then
        ()
      else
        begin
	  let _ = Hashtbl.replace visited_table loc_id loc in
	  let outgoing_edges = List.rev (C_SD.get_outgoing_edges loc) in
	    (List.iter (fun e -> traverse_location_and_edges (C_SD.get_target e)) 
	       outgoing_edges)          
	end 
  in    
    traverse_location_and_edges (C_SD.lookup_entry_location fname)
  
  (* this obliterates the "old tables" *)
let rec update_dag map k = 
  Message.msg_string Message.Debug ("in update_dag depth: "^(string_of_int k));
  if k = 0 then map
  else
    begin
      let new_map = copy_map map in
	Hashtbl.iter (make_node (map,new_map)) visited_table;
	Hashtbl.iter (fun x y -> Hashtbl.replace map x y) new_map;
	update_dag map (k - 1)
    end

(* precondition: error_location_id should be set already -- i.e. = Some(x,y) *)
 
let build_dag fname k = 
  (* after this point visited_table : loc_id -> loc *)
  let init_map = Hashtbl.create loc_table_size in
  let e_id = get_err_loc_id () (*C_SD.location_coords err_loc*) in
  let _ = Hashtbl.replace init_map e_id 
	    (BiDirectionalLabeledDag.create_root (new_node_id (), e_id, None))  
  in
    update_dag init_map k


let prune_fun sink_list =
  let keep_table = Hashtbl.create loc_table_size in
  let reachable_ancestors = BiDirectionalLabeledDag.ancestors sink_list in
  let _ = List.iter (fun x -> Hashtbl.replace keep_table x true) reachable_ancestors in
    fun x -> not (Hashtbl.mem keep_table x)

(********************************************************************************************)
(******************************** The region data structure         *************************)
(********************************************************************************************)


(* this uses the visited_table data structure *)

  (* a region is basically an  Hashtbl from pc -> predicate *)
let new_region start_loc_id = 
  let new_reg = Hashtbl.create loc_table_size in
    Hashtbl.iter (fun x y -> Hashtbl.replace new_reg x Predicate.False) visited_table;
    Hashtbl.replace new_reg start_loc_id Predicate.True;
    new_reg


let reset_region reg start_loc_id = 
  Hashtbl.iter (fun x y -> Hashtbl.replace reg x Predicate.False) reg;
  Hashtbl.replace reg start_loc_id Predicate.True

let contains reg (loc,pred) =
  let old = try Hashtbl.find reg loc with Not_found -> Predicate.False in
    TheoremProver.interpolant_implies pred old
	
	
  let cup reg (loc,pred) =
    try
      let old = Hashtbl.find reg loc in 
	Hashtbl.replace reg loc (Predicate.disjoinL [pred;old])
    with Not_found -> 
      Hashtbl.replace reg loc pred
      
(* this updates the reg (hashtbl) and returns true if new states are added and false o.w. *)
  let cup_check reg (loc,pred) = 
    try
      let old = Hashtbl.find reg loc in
	(* shall we check here .. hmm ... ? *)
	if TheoremProver.interpolant_implies pred old then false
	else 
	  begin
	    Hashtbl.replace reg loc (Predicate.disjoinL [pred;old]);
	    true
	  end
    with Not_found -> 
      (Hashtbl.replace reg loc pred; false)

  let cup_replace reg (loc,pred) =
    (* Precondition, which is not being checked, reg.loc => pred *)
    Hashtbl.replace reg loc pred
      
  let loc_reg reg loc = 
    try 
      Hashtbl.find reg loc
    with Not_found -> Predicate.False

  let print_reg reg = 
    let print_loc_reg loc_id pred = 
      Message.msg_string Message.Normal (Printf.sprintf "Location: (%d,%d)" (fst loc_id) (snd loc_id));
      Message.msg_string Message.Normal (Predicate.toString pred);
    in
      Hashtbl.iter print_loc_reg reg

(******************************* end region data structure **********************************)


(********************************************************************************************)
(****************************   Tying it all Together  **************************************)
(********************************************************************************************)

let dag_node_to_string n_label = 
			let (i,(l1,l2),_) = n_label in
			  string_of_int i

let dag_edge_to_string e_label = 
   Command.to_string e_label


let dump_wplist wplist node_coord_list = 
  Message.msg_string Message.Normal "***************************************************************";
  Message.msg_string Message.Normal "***************************************************************";
  Message.msg_string Message.Normal "Dumping WP formulas for the various roots of the DAG";
  let ac_dump (phi,(id1,id2)) = 
    Message.msg_string Message.Normal "***************************************************************";
    Message.msg_string Message.Normal (Printf.sprintf "Node: (%d, %d)" id1 id2);
    Message.msg_string Message.Normal (Predicate.toString phi);
    ()
  in
    try
      List.iter ac_dump (List.combine wplist node_coord_list) 
    with _ -> failwith "bad args to dump_wplist"
      
      



(* this is where all the action is. The function doesnt actually end -- it raises an exception of some sort*)
(* first a small helper that lets one "clean predicates" ... should this be in ast.ml ? *)

let clean_predicate = 
  let transformer p = 
    match p with
	Predicate.Atom e -> Predicate.Atom (Expression.replaceChlvals e)
      | p' -> p'
  in 
    Predicate.deep_transform transformer

exception Counterexample
exception UnrollMore


let model_check fname err_loc =
  let depth = Options.getValueOfInt "depth" in
  (* set the error_location_id for future use *)
  let _ = error_location_id := Some (C_SD.location_coords err_loc) in
  let _ = fill_visited_table fname in
  let k = ref depth in
  let c = ref 0 in
  let start_loc = (C_SD.location_coords (C_SD.lookup_entry_location fname)) in
  let reached_reg = new_region start_loc in
  let dag_root_table = (build_dag fname !k) in
    (* sanity check *)
    if not (Hashtbl.mem dag_root_table start_loc) then failwith "Init Depth too low ...!";
    let inc_flag = ref true in
      while !inc_flag = true do
	begin
	  Message.msg_string Message.Normal (Printf.sprintf "Unroll Depth = %d" !k);
	  inc_flag := false;
	  (* the regions will be changing within a round as well ... does this matter ? *)
	  (* foreach (pc,node) do: *) 
	  let process_dag_root loc_id node = 
	    let process_child parent_reg node edge_cons =
	      let node_wp = weakest_pre node in(* could make this faster ... *)
	      let split_phi = (Predicate.conjoinL [parent_reg;edge_cons], node_wp)  in 
		(* Note for Ken: should allow me to not have to reassert things to the solver *)
		match (TheoremProver.interpolant_sat split_phi) with
		    TheoremProver.Sat ->
	              begin
			if !c = 0 then raise Counterexample
			else raise UnrollMore;
	              end 
		  | TheoremProver.Unsat(i1) ->
	              begin
			let inc = clean_predicate i1 in
			let node_loc_id = Misc.snd3 (BiDirectionalLabeledDag.get_node_label node) in
			  inc_flag := !inc_flag  || cup_check reached_reg (node_loc_id,inc) 
	              end
	    in
	    let mapv = map_of_v (map_node node) in
	    let loc_id_reg = Predicate.deep_alpha_convert mapv 
			       ((*Reg.*)loc_reg reached_reg loc_id) in 
	      if loc_id_reg <> Predicate.False then
		List.iter2 (process_child loc_id_reg) 
		  (BiDirectionalLabeledDag.get_children node) 
		  (cons_node node)
	      else ()
	  in
	    try 
	      Hashtbl.iter process_dag_root dag_root_table
	    with
		Counterexample -> 
		  begin
		    (* should be finding the counterexamplea and spitting it out ... *)
		    Message.msg_string Message.Error "Counterexample found: system unsafe!";
		    exit 0 ;
		  end
	      | UnrollMore ->
		  begin
		    	k := !k + !c;
			update_dag dag_root_table !c;
			reset_region reached_reg start_loc;
			c := 0;
			inc_flag := true;
			(* continue with a longer unrolling ... *)
		  end
	end
      done;
      (* if we get out of the loop to this point, it means that a safe fixpoint has been found ... *)
      Message.msg_string Message.Normal "System is safe";
      Message.msg_string Message.Normal "Invariant is:";
      (*Reg.*)print_reg reached_reg;
      exit 0

  
let main () = 
  Message.set_verbosity Message.Default; 
  Stats.reset (); 
  Options.buildOptionsDatabase ();
  let reachLabel = (Options.getValueOfString "L")
  and source_files = (Options.getSourceFiles ()) in
    if(reachLabel = "") then
       begin
         Printf.printf "No error label specified!\n" ;
         exit 1
       end
    else
      () ; 
    if (source_files == [])
    then 
      begin
        Message.msg_string Message.Error ("Error: no input source file specified") ;
        exit 1
      end  ;
    Message.msg_string Message.Normal ("Parsing files: " ^ (Misc.strList source_files)) ;
    AliasAnalyzer.constructAliasDatabase () ;  
    Stats.time "Parse:" C_SD.initialize source_files ;
    let error_location =
      match (C_SD.get_locations_at_label reachLabel) with
          [l] -> l
         | []  ->
             Message.msg_string Message.Error ("Error: label '" ^ reachLabel ^
                                              "' was not found in the source file(s)") ;
            exit 1
        | _   ->
            Message.msg_string Message.Error ("Error: label '" ^ reachLabel ^
                                              "' appears multiple times in the source file(s)") ;
            exit 1
    in
    let fname = (List.hd (Options.getValueOfStringList "main")) in
      model_check fname error_location

(* To write.
Theoremprover.interpolant_implies
Theoremprover.interpolant_sat
*)
(*
  let error_coords = C_SD.location_coords error_location in
  let dag_root_table = build_dag (List.hd (Options.getValueOfStringList "main")) error_location 
  (Options.getValueOfInt "depth") 
  in 
  let dag_root_list = List.map (Hashtbl.find dag_root_table) (Misc.hashtbl_keys dag_root_table) 
  in
  let _ = BiDirectionalLabeledDag.output_dag_dot 
  dag_node_to_string dag_edge_to_string "dag1.dot" dag_root_list in
  http://www.guardian.co.uk/wto/article/0,2763,1035805,00.html
  let reachable_list = BiDirectionalLabeledDag.descendants dag_root_list in

  let rsize1 = List.length reachable_list in
  let error_nodes = List.filter 
  (fun x -> let (_,c,_) = BiDirectionalLabeledDag.get_node_label x in c = error_coords) 
  reachable_list
  in
  let _ = BiDirectionalLabeledDag.prune (prune_fun error_nodes) dag_root_list in
  let rsize2 = List.length (BiDirectionalLabeledDag.descendants dag_root_list) in
  let _ = Printf.printf "Size1 = %d, Size2 = %d \n" rsize1 rsize2 in
  let _ = BiDirectionalLabeledDag.output_dag_dot 
  dag_node_to_string dag_edge_to_string "dag.dot" dag_root_list in
	(* all the bogus nodes are now gone *)
  let wp_list = List.map (weakest_pre error_coords) dag_root_list in
  dump_wplist wp_list 
  (List.map (fun x -> Misc.snd3 (BiDirectionalLabeledDag.get_node_label x))
  dag_root_list)
	(* hmm... will this work if the dag_root_list has size more than 1 ? since 
  nodes will get state etc etc ? Shouldnt we have to "reset" the tables or 
  something ? or NO! the Dynamic Programming deals with all of that!*)
	(* maybe one should print out the formulas or something ? *)
*)
	   
let () = main ()   


