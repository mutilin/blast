(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)


(* this file has the hacky code that will insert the hooks as desired -- for now, I
insist that this be done on the interactive shell *)


(* requires misc.ml *)


(* assumes that all if conditions lie on one line ! ie: ... if (...) ... *)
let hook_list = ref []
(* the above is a list of type: ((string list * string) list) *)

let line_counter = ref 0
		     
let set_hook_list l = 
  (hook_list := l)
  
let reg_if = Str.regexp "if \(.*\){" 
	
let reg_semicol = Str.regexp ";"


(* the main function ...*)
let add_hooks fname r s = 
  let l = Misc.string_list_of_file source_file in
  let file_string = Misc.string_list_cat "\n" l in
  let chopped_list = Str.split (Str.regexp ";") file_string in
  let ic = open_in fname in
  let oc = open_out (fname^".out") in  
  let doneflag = ref false in
  let lastline = ref "" in
  let thisline = ref "" in
  let newline = ref "" in
  let linecount = ref 0 in
    while not (!doneflag) do
      try
	thisline := input_line ic ;
	let matched = Str.string_match r (!thisline) 0 in
	  if not matched then (newline := !thisline) 
	  else
	    begin
	      let _ = print_string 
			"Match at line: "
		      ^(string_of_int !linecount)^": Trigger ? \n"
	      in
		(* if you want a trigger just press return *)  	 
		if (read_line () = "") then
		  begin
		    (newline := !thisline)
		  end
		else
	      	  (* 2 cases, 
		     (a) match inside an "if" condition : place trigger just before if
		     (b) match in an assignment, place trigger after next semicolon
		  *)
		  let bind = Str.matched_string (!thisline) in
		    match (ifininf bind !thisline) with
			true -> 
			  newline := Misc.insert_before "if" !thisline "

	  end
