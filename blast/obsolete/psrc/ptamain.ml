(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)

(*
module Predicate = Ast.Predicate
module Expression = Ast.Expression
*)
module C_System_Descr = BlastCSystemDescr.C_System_Descr
(*
module C_Command = BlastCSystemDescr.C_Command
*)

(* [Greg] Very dirty hack because we renamed Stats to VampyreStats in
   vampyre...  We should clean this stats business anyway. *)
module Stats = VampyreStats

(*****************************************************************************************)


(* main function: the glue *)
let main () =
(* try *)
  Message.set_verbosity Message.Default;
  Stats.reset ();
  Options.buildOptionsDatabase ();

  begin
  let source_files = (Options.getSourceFiles ()) in
    (* No reachLabel has been specified on the command line *)
    if (source_files == [])
    then
      begin
        Message.msg_string Message.Error ("Error: no input source file specified") ;
        exit 1
      end ;
    Message.msg_string Message.Normal ("Parsing files: " ^ (Misc.strList source_files)) ;
    Stats.time "Parse:" C_System_Descr.initialize source_files ;

  if (Options.getValueOfString "pta" = "bddflow") then
    Stats.time "Bdd alias (flow sensitive)" Bddptsto.do_bdd_alias true 
  else 
    Stats.time "Bdd alias (flow insensitive)" Bddptsto.do_bdd_alias false ;
  Stats.print stderr "Timings:\n" 
  end
  
let () = 	Printexc.catch main ()
