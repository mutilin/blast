open Ast
open BlastArch

module C_SD = BlastCSystemDescr.C_System_Descr

(** A block is a sequence of statements with the control falling through from 
    one element to the next *)
type block = 
      stmt list      (** The statements comprising the block*)


(** {b Statements}. 
QL statements are the strutural elements that make the CFG. They are 
represented using the type {!QL.stmt}. Every
statement has a (possibly empty) list of labels. The
{!Cil.stmtkind} field of a statement indicates what kind of statement it 
is.

*)
(** Statements. *)
and stmt = 
    Skip
  | Assign of Expression.expression * Expression.expression

  | PrintString of string

  | Print of Expression.expression

  | If of Expression.expression * block * block 
   (** A conditional. Two successors, the "then" and the "else" branches. 
    * Both branches fall-through to the successor of the If statement. *)

  | Block of block                      
    (** Just a block of statements. *)


(** Relations *)
type 'a relation = {
    mutable name : string ;                          (** the name, duh! *)
    mutable arity : int ;                            (** The arity of the relation *)
    mutable tuples : ('a list) list ; (** A tuple is maintained as a list of length !arity *)
(*    mutable number_of_entries : int ; *)
  } 

(** Contexts *)
(* Contexts are maps from names of relations to relations. *)
(*
** Keyword hashtable
*)
module HashString =
struct
	type t = string
	let equal (s1 : t) (s2 : t) = s1 = s2
	let hash (s : t) = Hashtbl.hash s
end
module StringHashtbl = Hashtbl.Make(HashString)

exception RelationNotFound of string

type 'a queryOutcome =
    Relation of 'a relation
  | Bool of bool
  | Int of int
  | String of string

(*************************************************************************)
(** Builtin-Function implementations for the query language             **)
(*  The type of each function is context -> Expression.expression list -> queryOutcome *)


let ql_GetEntry context args = (* may raise NoSuchFunctionException *)
  if List.length args <> 1 then failwith "GetEntry called with strange arguments" ;
  match List.hd args with
    Expression.Constant (Constant.String fname) ->
      Relation { name= "__anon__"; arity = 1; tuples = [ [C_SD.lookup_entry_location fname] ];  }
  | _ -> failwith "GetEntry: requires a string argument"

let ql_IsEmpty context args =
    if List.length args <> 1 then failwith "IsEmpty called with strange arguments" ;
    match List.hd args with
      Expression.Constant (Constant.String rn) ->
	begin
	  try 
	    
	    let reln = StringHashtbl.find context rn in
	    Bool (reln.tuples = [])
	  with Not_found -> raise (RelationNotFound(rn) )
	end
    | _ -> failwith "IsEmpty: Requires a string argument"


(*************************************************************************)

let printRelation r =
  Printf.printf "Relation %s (%d)\n" r.name r.arity ;
  List.iter (fun tuple -> ()) r.tuples

(*************************************************************************)


let known_functions = 
  [ 
    (*("Reach", ql_model_check);*) 
    ("GetEntry", ql_GetEntry);
    ("IsEmpty", ql_IsEmpty); 
  ] 

let rec eval_exp context e = (* the top level eval_exp always returns a relation *)
  match e with
    Expression.FunctionCall(Expression.Lval (Expression.Symbol fname), arglist) ->
      begin
	try
          let f = List.assoc fname known_functions in
	  f context arglist
	with Not_found ->
	  (* not a builtin function, look up this relation *)
	  begin
	    try Relation (StringHashtbl.find context fname) 
	    with Not_found -> raise (RelationNotFound (fname))
	  end
      end
  | Expression.Binary (binop, e1, e2) -> 
      begin
	let check_args r1 r2 =
	  match (r1, r2) with
	    (Relation rel1, Relation rel2) ->
	      if rel1.arity = rel2.arity then (rel1, rel2) else
	      failwith "Binary operation: Incompatible relations"
	  | _ -> failwith "Binary operation: Not a relation or Incompatible relations"
	in
	let eval_e1 = eval_exp context e1 in
	let eval_e2 = eval_exp context e2 in
	match binop with
	  Expression.Or ->
	    let (r1, r2) = check_args eval_e1 eval_e2 in
	    Relation { name = "__anon__"; arity = r1.arity; tuples = r1.tuples @ r2.tuples }  
	| Expression.And ->
	    let (r1, r2) = check_args eval_e1 eval_e2 in
	    Relation { name = "__anon__"; arity = r1.arity; tuples = r1.tuples @ r2.tuples }  
	| Expression.Minus ->
	    let (r1, r2) = check_args eval_e1 eval_e2 in
	    Relation { name = "__anon__"; arity = r1.arity; tuples = r1.tuples @ r2.tuples }  
	| Expression.Eq ->
	    let (r1, r2) = check_args eval_e1 eval_e2 in
	    Bool (r1.tuples = r2.tuples)
	| Expression.Ne ->
	    let (r1, r2) = check_args eval_e1 eval_e2 in
	    Bool (r1.tuples <> r2.tuples)
	| _ -> failwith ("Unknown operator in "^ (Expression.toString e))
      end
  | _ -> failwith ("eval_exp: Strange expression "^(Expression.toString e))

let runSkip context = 
  Message.msg_string Message.Normal "runSkip" ;
  context

let runAssign context (lhs, rhs) =
  Message.msg_string Message.Normal "runAssign" ;
  let eval_rhs = eval_exp context rhs in
  match eval_rhs with
    Relation r ->
      let (lhs_name, lhs_arity) = match lhs with
	Expression.FunctionCall (Expression.Lval (Expression.Symbol rname), args) ->
	  if (List.exists (fun (a,b) -> a = rname) known_functions) then
	    failwith "Name of relation cannot be a builtin function"
	  else (rname, List.length args)
      |	_ -> failwith "LHS not of proper type "
      in
      r.name <- lhs_name ;
      if (r.arity <> lhs_arity) then failwith "Arities are different" ;
      StringHashtbl.add  context lhs_name r ;
      context
  | _ -> failwith "TBDD"
	
let runPrintString context s = 
  Printf.printf "%s\n" s ;
  context  

let runPrint context e =
  Message.msg_string Message.Normal "runPrint" ;
  (match eval_exp context e with
    Relation r ->
      printRelation r
  | Bool a -> Printf.printf "%s\n" (if a then "true" else "false")
  | Int  n -> Printf.printf "%d\n" n
  | String s -> Printf.printf "%s\n" s
  ); 
  context 

let rec runIf context (be, thenblock, elseblock) =
  Message.msg_string Message.Normal "runIf" ;
  let eval_be = eval_exp context be in
  match eval_be with
    Bool a ->
      runBlock context (if a then thenblock else elseblock)
  | _ -> failwith "runIf: Conditional is not a boolean expression"

and runBlock context bl =
  Message.msg_string Message.Normal "runBlock" ;
  List.fold_left (fun a b -> runStmt a b) context bl  

and runStmt context stmt =
  match stmt with
    Skip -> runSkip context 
  | Assign (lhs, rhs) -> runAssign context (lhs, rhs)
  | PrintString s -> runPrintString context s
  | Print e -> runPrint context e
  | If (be, thenblock, elseblock) -> runIf context (be,thenblock,elseblock)
  | Block b -> runBlock context b

let runStmts stmts =
  Message.msg_string Message.Normal "Now running statements" ;
  Message.msg_string Message.Normal ("There are " ^ (string_of_int (List.length stmts)) ^ " statements" );
  let context = StringHashtbl.create 13 in

  let finalcontext = List.fold_left (fun a stmt -> runStmt a stmt) context stmts in
  ()
