val currentLine : int ref 

val display_error : string -> int -> int -> unit

val init: inchannel:in_channel -> Lexing.lexbuf

val token: Lexing.lexbuf -> Qlparse.token
