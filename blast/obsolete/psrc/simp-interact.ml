


let ic,oc,ec = Unix.open_process_full "Simplify -nosc" 
			   (Unix.environment ()) 

let simp_assume s = 
  let s = ("(BG_PUSH "^s^" )\n") in
    output_string oc s ;
    flush oc
      
let simp_pop () = 
  let s = "(BG_POP)\n" in
  output_string oc s;
  flush oc;
  ()


let rec isValid () =
  let line = input_line ic in
  let result = if String.contains line 'V' then 
    true
  else if String.contains line 'I' then
    false
  else isValid ()
  in
    result

let _is_contra () =
  try
    let s = "FALSE\n"  in
      output_string oc s ;
      flush oc ;
      isValid () 
  with e ->
    failwith ("Simplify raised exception "^(Printexc.to_string e)^" in _is_contra")


simp_assume "(EQ (NEQ x 0) (NEQ y 0))";;
input_line ic ;;
(* input_line ic will cause it to freeze ... *)

simp_assume "(EQ x 0)";;
input_line ic ;;
