
%{
module E = VampyreErrormsg
open Ast
open Ql

let  parse_error msg =
  Message.msg_string Message.Error ("Parse error! "^msg) ;
  Qlflex.display_error msg (Parsing.symbol_start ()) (Parsing.symbol_end ())
(*
    E.s (E.error "Parse error at %s: %s" 
           (E.getLineCol !E.current (symbol_start ())) msg)
*)

let remove_quotes str = String.sub str 1 ((String.length str) - 2)



%}

%token <string> Id STRING
%token <int> Num
%token EOF
%token LPAREN  RPAREN LBRACE RBRACE COMMA SEMICOLON
%token  MINUS PLUS
%token ASSIGN NE EQ  GE  GT  LE  LT 
%token  AND  OR 
%token IF THEN ELSE PRINT PRINTSTRING

/* operator precedence */
%nonassoc IF
%nonassoc ELSE

%left  COMMA
%right ASSIGN
%left EQ NE GT GE LT LE
%left AND  OR
%left PLUS MINUS
%left Id

%start main

%type <(Ql.stmt list)> main
%type <string> str

%%


main:
  statements EOF   { $1 }
  ;

statements:
  statement statements { $1 :: $2 }
| /* empty */          { [] }
  ;
 
statement:
  SEMICOLON                            { Ql.Skip }
| PRINTSTRING str SEMICOLON            { Ql.PrintString $2 }
| PRINT expression SEMICOLON               { Ql.Print $2 } 
| regvar ASSIGN expression SEMICOLON   { Ql.Assign ($1, $3) }
| block                                { Ql.Block $1 }
| IF boolexpression THEN statement %prec IF  { Ql.If ($2, [ $4 ],  [ Ql.Skip ]) }
| IF boolexpression THEN statement ELSE statement { Ql.If ($2, [ $4 ], [ $6 ]) }
  ;

block:
  LBRACE statements RBRACE             { $2  }

expression:
  identifier LPAREN arglist RPAREN /* function call */
      { Expression.FunctionCall (Expression.Lval (Expression.Symbol $1), $3)
      }
| expression OR expression
      { Expression.Binary (Expression.Or, $1, $3) }
| expression AND expression
      { Expression.Binary (Expression.And, $1, $3) }
| expression MINUS expression
      { Expression.Binary (Expression.Minus, $1, $3) }
| expression EQ expression
      { Expression.Binary (Expression.Eq, $1, $3) }
| expression NE expression
      { Expression.Binary (Expression.Ne, $1, $3) }
| LPAREN expression RPAREN  { $2 }
| identifier                { Expression.Lval (Expression.Symbol $1 ) }
| str { Expression.Constant (Constant.String $1) }
| number { Expression.Constant ($1) }
  ;


boolexpression:
  expression { $1 }
  ;

arglist: 
  expression COMMA arglist   { $1 :: $3 } 
| expression          { [ $1 ] }

regvar:
  identifier LPAREN idlist RPAREN  { Expression.FunctionCall (Expression.Lval (Expression.Symbol $1),
							      List.map (fun s -> Expression.Lval (Expression.Symbol s)) $3) }
  ;

idlist:
  identifier COMMA idlist          { $1 :: $3 }
| identifier                       { [ $1 ] }

identifier:
          Id { $1 }
        ;

number: 
    Num { Constant.Int $1 }
str:
          STRING { $1 }

