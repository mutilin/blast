(*
 * Copyright � 1990-2002 The Regents of the University of California. All rights reserved. 
 *
 * Permission is hereby granted, without written agreement and without 
 * license or royalty fees, to use, copy, modify, and distribute this 
 * software and its documentation for any purpose, provided that the 
 * above copyright notice and the following two paragraphs appear in 
 * all copies of this software. 
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY 
 * FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES 
 * ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN 
 * IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE. 
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS 
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION 
 * TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 *)



(**
 * This module gives an implementation of labeled bi-directional dags.
 * Trees created by this module have labeled nodes and labeled edges, and
 * each node has links both to its parent and to its children.  Labels can
 * be modified and children can be added to a node.  However, the parent
 * edge cannot be changed after creation. Hence this modules enforces to
 * construct trees from the root to the leaves.
 *
 * Child removal is not currently supported.
 *)

type ntype = (int 
	      * (int * int) 
	      * (((Ast.Expression.lval, Ast.Expression.expression) Hashtbl.t * (Ast.Predicate.predicate list)) option))

type etype = BlastCSystemDescr.C_Command.t

(** the node type *)
type ('a, 'b) node

(** the edge type *)
and  ('a, 'b) edge

(** root node creation *)
val create_root  : 'a -> ('a, 'b) node

val compute_and_update_root_val : 
  ((ntype, etype) node -> bool) -> 
    (ntype -> (etype * ntype) list -> ntype) ->
      (ntype -> ntype) -> 
	(ntype, etype) node 
	-> unit
						
val get_node_label : ('a, 'b) node -> 'a

val dag_map : ( ntype -> 'a) -> (ntype , etype) node -> 'a list

val get_children : ('a, 'b) node -> ('a,'b) node list

val hookup_parent_child : ('a, 'b) node -> ('a, 'b) node -> 'b -> unit

val get_node_label : ('a, 'b) node -> 'a

val ancestors : ('a, 'b) node list -> ('a, 'b) node list

val descendants : ('a, 'b) node list -> ('a, 'b) node list

val prune : (('a, 'b) node -> bool) -> ('a, 'b) node list  -> unit 

val output_dag_dot : ('a -> string) -> ('b -> string ) -> string -> ('a,'b) node list -> unit
