open BlastArch

module Make = functor(Region : REGION) -> struct
  module Region = Region

  type 'a lock_predicate = {
      local_lp_region : Region.t;
      lp_region : Region.t;
      lp_node : 'a;
    }
  type 'a t  = {
      conjuncts : 'a lock_predicate list;
      whole : Region.t; (* whole = disjunction of the lp_region field in conjuncts *)
    }

  type 'a add_region_outcome =
    | New_region of 'a t
    | Error_at of 'a lock_predicate * Region.t

  let empty = {
    conjuncts = [];
    whole = Region.bot;
  }

  let concrete ar = ar.whole

  let add_region n local_r r phi incompatible =
    (* If the new region is already contained in phi, we don't need to do
       anything. *)
    if Region.leq r phi.whole then New_region phi else
    let conjuncts' = {local_lp_region = local_r; lp_region = r; lp_node = n} :: phi.conjuncts in
    let rec check = function
        [] ->
          (* No access violation on this x *)
          Message.msg_string Message.Debug "\n\n***************************Adding region to phi*******************************\n" ;
          Message.msg_string Message.Debug (Region.to_string r);
	  Message.msg_string Message.Debug "\n\n****************************************************************************\n" ;
          New_region { conjuncts = conjuncts';
                       whole = Region.cup phi.whole r } 
      | conjunct :: tail ->
          match incompatible conjunct.lp_region r with
	    None -> check tail
	  | Some reg -> Error_at (conjunct, reg)
    in
    check conjuncts'

  let to_string ar = Region.to_string ar.whole
end



