open BlastArch

module Make : functor(R : REGION) -> ACCESS_REGION with module Region = R
